Ext.ns('NE.Informacion');
Ext.define('ModelCatalogo', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'clave', type: 'string'},
        {name: 'descripcion',  type: 'string'}
    ]
});

// para el paso 1 obtener Informaci�n General
NE.Informacion.FormDatosGeranes= Ext.extend(Ext.form.FormPanel,{
	objDatoInicial: null,
	campo_activo:'S',
	clave_solicitud:null,
	ESTATUS_SOLICITUD:null,
	initComponent : function(){
		 Ext.apply(this, {
			itemId:	'datosGralesV',
			width: 945,
			height:450,
			frame: false,   
			border: true,
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,padding:6px;,text-align:left',
			autoScroll: true,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-5'
			},
			listeners:{
				afterrender: function(obj){
					obj.generarCampos( obj);
				}
			}
		});
		
		NE.Informacion.FormDatosGeranes.superclass.initComponent.call(this);
	},
	generarCampos: function(fp){
	
		var INFO_INICIAL 	= 	fp.objDatoInicial;
		var campo_activo 	= 	fp.campo_activo;
		var IC_ASPIRANTE 	= 	INFO_INICIAL[0].IC_ASPIRANTE;
		var RFC 			= 	INFO_INICIAL[0].RFC;
		var RAZON 			=	INFO_INICIAL[0].RAZON;
		var NOMCOM 			=	INFO_INICIAL[0].NOMCOM;
		var CG_CALLE 		=	INFO_INICIAL[0].CG_CALLE;
		var CG_COLONIA 		=	INFO_INICIAL[0].CG_COLONIA;
		var IC_PAIS 		=	INFO_INICIAL[0].IC_PAIS;
		var IC_ESTADO 		=	INFO_INICIAL[0].IC_ESTADO;
		var IC_MUNICIPIO 	=	INFO_INICIAL[0].IC_MUNICIPIO;
		var CG_CP 			=	INFO_INICIAL[0].CG_CP;
		var CG_CONTACTO 	=	INFO_INICIAL[0].CG_CONTACTO;
		var CG_EMAIL 		=	INFO_INICIAL[0].CG_EMAIL;
		var CG_TELEFONO 	=	INFO_INICIAL[0].CG_TELEFONO;
		
		Ext.define('ModelCatalogo', {
		extend: 'Ext.data.Model',
		fields: [
			{name: 'clave', type: 'string'},
			{name: 'descripcion',  type: 'string'}
		]
		});
		var catalogoEstado = Ext.create('Ext.data.Store', {
				model: 'ModelCatalogo',
				proxy: {
					type: 'ajax',
					url: '39IfnbCalificate.data.jsp',
					reader: {
						type: 'json',
						root: 'registros'
					},
					extraParams: {
						informacion: 'catalogoEstado'
					},
					listeners: {
						exception: NE.util.mostrarProxyAjaxError
					}
				}		
		});
		var catalogoMunicipio = Ext.create('Ext.data.Store', {
				model: 'ModelCatalogo',
				proxy: {
					type: 'ajax',
					url: '39IfnbCalificate.data.jsp',
					reader: {
						type: 'json',
						root: 'registros'
					},
					extraParams: {
						informacion: 'catalogoMunicipio'					
					},
					listeners: {
						exception: NE.util.mostrarProxyAjaxError
					}
				}		
		});
		catalogoEstado.load();
		if(IC_ESTADO!=''){
			catalogoMunicipio.load({
				params: Ext.apply({
					cmbEdo:IC_ESTADO
				})				 
			});								 
		}
		var elementosMens=[ 
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 810,
				items: [
					{
						xtype:'label',
						style:    {'font-size' : '12px'},
						html:'<center>Le agradecemos nos proporcione la siguiente informaci�n con la finalidad de registrar su solicitud y tener sus datos de contacto.</center><br><br>'
					}
				]
			}
		];
		var elementosFormaDG=[ 
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype: 'hidden',
						fieldLabel: 'IC_aspirante',
						itemId: 'ic_aspirante',
						name:'ic_aspirante',
						
						labelWidth: 150,
						width: 300
					}
				]
			},
			{
				xtype:'textfield',
				itemId:'txtRazon_aux',
				width: 100,
				hidden:true
			},
			{
				xtype:'textfield',
				itemId:'guardar_activo',
				width: 100,
				value:'N',
				hidden:true
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					
					{
						xtype:		'textfield',
						name:			'txtRazon',
						itemId:		'txtRazon',
						fieldLabel: 'Raz�n Social',
						allowBlank:	false,
						maxLength:100,
						anchor:			'100%',
						msgTarget:     'side',
						width: 500,
						disabled: false,
						value:(RAZON!=''?RAZON:''),
						labelWidth: 150,
						margins: '0 20 0 0',
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txtRazon_aux')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txtRazon_aux')[0].getValue();
								var valNuevo = Ext.ComponentQuery.query('#txtRazon')[0].getValue();
								if(valAnterior!=valNuevo){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
								}
							}
						}
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype:		'textfield',
						name:			'txtNomCom',
						itemId:		'txtNomCom',
						allowBlank:	false,
						msgTarget:		'side',
						width: 500,
						labelWidth: 150,
						maxLength:100,
						value:(NOMCOM!=''?NOMCOM:''),
						msgTarget:     'side',
						readOnly: (campo_activo=='S')?true:false,
						margins: '0 20 0 0',
						fieldLabel: 'Nombre Comercial',
						anchor:			'60%',
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txtRazon_aux')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txtRazon_aux')[0].getValue();
								var valNuevo = Ext.ComponentQuery.query('#txtNomCom')[0].getValue();
								if(valAnterior!=valNuevo){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
								}
							}
						}
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype:		'textfield',
						name:			'txtRFC',
						itemId:		'txtRFC',
						allowBlank:	false,
						value:(RFC!=''?RFC:''),
						readOnly: (campo_activo=='S')?true:false,
						msgTarget:		'side',
						width: 500,
						labelWidth: 150,
						maxLength:20,
						msgTarget:     'side',
						margins: '0 20 0 0',
						regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
						regexText	:'Por favor escriba correctamente el RFC en may�sculas  separado por '+
						'guiones en el formato NNN-AAMMDD-XXX donde:<br>'+
						'NNN:son las iniciales del nombre de la empresa<br>'+
						'AAMMDD: es la fecha de creaci�n de la empresa menor al d�a de hoy.<br>'+
						'XXX:es la homoclave',
						fieldLabel: 'R.F.C empresarial',
						anchor:			'60%',
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txtRazon_aux')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txtRazon_aux')[0].getValue();
								var valNuevo = Ext.ComponentQuery.query('#txtRFC')[0].getValue();
								if(valAnterior!=valNuevo){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
								}
							}
						}
						
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype:		'textfield',
						name:			'txtDomi',
						itemId:		'txtDomi',
						allowBlank:	false,
						value:(CG_CALLE!=''?CG_CALLE:''),
						msgTarget:		'side',
						width: 500,
						readOnly: (campo_activo=='S')?true:false,
						labelWidth: 150,
						maxLength:100,
						msgTarget:     'side',
						margins: '0 20 0 0',
						fieldLabel: 'Domicilio(Calle y N�mero)',
						anchor:			'60%',
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txtRazon_aux')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txtRazon_aux')[0].getValue();
								var valNuevo = Ext.ComponentQuery.query('#txtDomi')[0].getValue();
								if(valAnterior!=valNuevo){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
								}
							}
						}
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype:		'textfield',
						name:			'txtColonia',
						itemId:		'txtColonia',
						allowBlank:	false,
						msgTarget:		'side',
						width: 500,
						labelWidth: 150,
						maxLength:100,
						value:(CG_COLONIA!=''?CG_COLONIA:''),
						readOnly: (campo_activo=='S')?true:false,
						msgTarget:     'side',
						margins: '0 20 0 0',
						fieldLabel: 'Colonia',
						anchor:			'60%',
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txtRazon_aux')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txtRazon_aux')[0].getValue();
								var valNuevo = Ext.ComponentQuery.query('#txtColonia')[0].getValue();
								if(valAnterior!=valNuevo){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
								}
							}
						}
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype: 'combo',
						fieldLabel: 'Estado',
						itemId: 'cmbEdo',
						name: 'cmbEdo',
						hiddenName: 'cmbEdo',
						forceSelection: true,
						hidden: false, 
						labelWidth: 150,
						width: 500,
						value:(IC_ESTADO!=''?IC_ESTADO:''),
						store: catalogoEstado,
						emptyText: 'Seleccione...',
						queryMode: 'local',
						allowBlank: false,
						displayField: 'descripcion',
						valueField: 'clave',
						readOnly: (campo_activo=='S')?true:false,			
						listeners:{
							select: function(combo, record, index) {
								if(combo.getValue()!=''){
									catalogoMunicipio.load({
										 params: Ext.apply({
											cmbEdo:combo.getValue()
										 })				 
									 });
								}							 
							},
							change:function(obj, newValue, oldValue){
								
								if(oldValue!=undefined){
									//se habilitaan todas las respuestas
									if(newValue!=oldValue){
										Ext.ComponentQuery.query('#btnGuardar')[0].enable();
										Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
									}
													
									
								}
																		
							}
							
						}
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype: 'combo',
						fieldLabel: 'Delegaci�n o municipio',
						itemId: 'cmbDeleg',
						name: 'cmbDeleg',
						hiddenName: 'cmbDeleg',
						forceSelection: true,
						hidden: false, 
						labelWidth: 150,
						value:(IC_MUNICIPIO!=''?IC_MUNICIPIO:''),
						width: 500,
						store: catalogoMunicipio,
						emptyText: 'Seleccione...',
						queryMode: 'local',
						allowBlank: false,
						displayField: 'descripcion',
						valueField: 'clave',
						readOnly: (campo_activo=='S')?true:false,
						listeners:{
							change:function(obj, newValue, oldValue){
								if(newValue!=undefined&&oldValue!=undefined){
								//se habilitaan todas las respuestas
									if(newValue!=oldValue){
										Ext.ComponentQuery.query('#btnGuardar')[0].enable();
										Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
									}
														
										
								}
																			
							}
						}
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype:		'textfield',
						name:			'txtCP',
						itemId:		'txtCP',
						allowBlank:	false,
						msgTarget:		'side',
						width: 500,
						value:(CG_CP!=''?CG_CP:''),
						labelWidth: 150,
						maskRe:			/[0-9]/,
						readOnly: (campo_activo=='S')?true:false,
						maxLength:5,
						msgTarget:     'side',
						margins: '0 20 0 0',
						fieldLabel: 'C�digo Postal empresarial',
						anchor:			'60%',
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txtRazon_aux')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txtRazon_aux')[0].getValue();
								var valNuevo = Ext.ComponentQuery.query('#txtCP')[0].getValue();
								if(valAnterior!=valNuevo){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
								}
							}
						}
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype:		'textfield',
						name:			'txtNomContacto',
						itemId:		'txtNomContacto',
						allowBlank:	false,
						value:(CG_CONTACTO!=''?CG_CONTACTO:''),
						readOnly: (campo_activo=='S')?true:false,
						msgTarget:		'side',
						width: 500,
						labelWidth: 150,
						maxLength:60,
						msgTarget:     'side',
						margins: '0 20 0 0',
						fieldLabel: 'Nombre de Contacto',
						anchor:			'100%',
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txtRazon_aux')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txtRazon_aux')[0].getValue();
								var valNuevo = Ext.ComponentQuery.query('#txtNomContacto')[0].getValue();
								if(valAnterior!=valNuevo){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
								}
							}
						}
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype:		'textfield',
						name:			'txtEmailEmp',
						itemId:		'txtEmailEmp',
						allowBlank:	false,
						width: 500,
						labelWidth: 150,
						value:(CG_EMAIL!=''?CG_EMAIL:''),
						maxLength:50,
						msgTarget:     'side',
						disabled: (campo_activo=='S')?true:false,
						margins: '0 20 0 0',
						vtype			: 'email',
						fieldLabel: 'Correo Electr�nico empresarial',
						anchor:			'60%',
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txtRazon_aux')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txtRazon_aux')[0].getValue();
								var valNuevo = Ext.ComponentQuery.query('#txtEmailEmp')[0].getValue();
								if(valAnterior!=valNuevo){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
								}
							}
						}
						
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 500,
				style: 'margin:0 auto;',
				items: [
					{
						xtype:		'textfield',
						name:			'txtTel',
						itemId:		'txtTel',
						allowBlank:	false,
						width: 500,
						value:(CG_TELEFONO!=''?CG_TELEFONO:''),
						labelWidth: 150,
						readOnly: (campo_activo=='S')?true:false,
						maxLength:30,
						msgTarget:     'side',
						margins: '0 20 0 0',
						fieldLabel: 'Tel�fono empresarial',
						anchor:			'100%',
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txtRazon_aux')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txtRazon_aux')[0].getValue();
								var valNuevo = Ext.ComponentQuery.query('#txtTel')[0].getValue();
								if(valAnterior!=valNuevo){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo')[0].setValue('S');
								}
							}
						}
						
					}
				]
			}
		];	
		var panelDatos = new Ext.Panel({
				itemId: 'pnDatos',
				frame: false,
				layout:'form',
				width:810,
				border	: false,
				bodyPadding: '0 10 0 200',
				items: elementosFormaDG			
			});
		
		fp.add(elementosMens);
		fp.add(panelDatos);
		fp.doLayout();
	
	}
});

NE.Informacion.FormInformacionCualitativa = Ext.extend(Ext.form.FormPanel,{
	formaPreguntas: null,
	formaRespuestas: null,
	titulo: null,
	objDataResp: null,
	objDataPreg:null,
	numPregReal: 0,
	modif: '',
	campo_activo:'S',
	initComponent : function(){
		 Ext.apply(this, {
			width: 918,
			height:600,
			itemId:	'infCualitativaV',
			frame: true,   
			border: false, 
			monitorValid: true,
			autoScroll: true,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side'
			},
			listeners:{
				afterrender: function(obj){
					obj.generarCamposC( obj);
				}
			}
		});
		
		NE.Informacion.FormInformacionCualitativa.superclass.initComponent.call(this);
	},
	generarCamposC: function(fp){
		var objPreg = fp.objDataPreg;
		var objResp = fp.objDataResp;
		var modif = fp.modif;
		var totalPreg = Number(fp.numPregReal)-1;
		fp.numPregReal = objPreg.length;
		var solo_read =fp.campo_activo;
		//** Funci�n para validad 
		var totalPreg = Number(fp.numPregReal);
		function validaPorcentaje(num_preg,nNumOpciones){ // No es necesario pasarle el par�metro
			var total = 0.0; //Es importante definir el tipo de dato y el numero de decimales
			var suma = 0.0;
			for(var n = 0; n< Number(nNumOpciones); n++){
				var campo_aux = '';
				campo_aux = Ext.ComponentQuery.query('#'+num_preg+'cajaTexto'+n)[0].getValue();
				
				suma = Number((campo_aux==''?0.0:campo_aux));
				total += suma;
			}
			// Al terminar de   se valida el porcentaje
			if(total < 100.00 || total > 100.00){ 
				
				return false;	
			}else{
				for(var n = 0; n< Number(nNumOpciones); n++){
					var campo_aux = '';
					campo_aux = Ext.ComponentQuery.query('#'+num_preg+'cajaTexto'+n)[0];
					campo_aux.clearInvalid();
				}
				return true;	
			}
		}
	function limpiarPregunta(num_pregunta){
		var arrayInfCualitativa = Ext.ComponentQuery.query('#infCualitativa panel[identificador=renglonRespuesta]');
		for(var r=0; r<arrayInfCualitativa.length;r++){
			var pregunta = arrayInfCualitativa[r].numRespuesta;
			var opcion = arrayInfCualitativa[r].numOpcion;
			var tipo_respuesta = arrayInfCualitativa[r].tipo_resp;
			if(pregunta==num_pregunta){
				if(Number(tipo_respuesta)==2){
					var ic_opcion_resp 	= arrayInfCualitativa[r].query('#'+pregunta+'ic_opcion_resp'+opcion);
					var addCajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'addCajaTexto'+opcion);
					var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
					ic_opcion_resp[0].setValue('');
					addCajaTexto[0].setValue('');
					cajaTexto[0].setValue('');
				}else if(Number(tipo_respuesta)==1){
					var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
					cajaTexto[0].setValue('');
				}else if(Number(tipo_respuesta)==3){
					var ic_opcion_resp 	= arrayInfCualitativa[r].query('#'+pregunta+'ic_opcion_resp');
						var addCajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'addCajaTexto'+opcion);
						var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
						ic_opcion_resp[0].setValue(false);
						addCajaTexto[0].setValue('');
						cajaTexto[0].setValue('');
				}
			}	
		}
	}
	Ext.apply(Ext.form.field.VTypes, {
		vPorcentaje: function(val, field) {
			
				return validaPorcentaje(field.numPreg,field.nNumOpciones);
		},
		vPorcentajeText: 'El total de porcentaje a cubrir por todas las opciones elegidas debe ser del 100%'//,
		//vPorcentajeMask: /100.00/i
	});
		if(objPreg.length!='') {
		
		for(var r=0; r<objPreg.length;r++){
			var dataPreg = objPreg[r];
			var vOrdenRenglon =  dataPreg.IGORDENPREG;
			var vNumPregunta =  dataPreg.IGNUMPREG;
			var vTipoPreg = dataPreg.TIPOPREG; 
			var vTipoResp = dataPreg.TIPORESP; 
			var vCgPregunta = dataPreg.CGPREGUNTA;
			var vNumOpc = dataPreg.IGNUMOPC;
			var vEstado = dataPreg.ICESTADO;
			if(vEstado=='S'){
			var tamColum1 = 100;
			var tamColum2 = 100;
			var tamColum3 = 100;
			//var est= '';
			var est = 'class="marginLeft"';
			if(vTipoResp==1){
				tamColum1 = 30;
				tamColum2 = 500;
				tamColum3 = 300;
			} else if(vTipoResp==2){
				if(vNumPregunta==2){
					tamColum1 = 30;
					tamColum2 = 470;
					tamColum3 = 330;
					est= '';
				}else{
					tamColum1 = 30;
					tamColum2 = 500;
					tamColum3 = 300;
				}
			}else{
				tamColum1 = 30;
				tamColum2 = 500;
				tamColum3 = 300;
			}
		var elementosAyuda =[
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 650,
				items: [
					{
						xtype:'label',
						style: {  'font-size' : '12px'  },
						width: 650,
						html:'<table width="600"  ><tr><td><div><p style=�text-align: justify;� >Para definir el tama�o de las empresas, NAFINSA aplica los criterios que al efecto establece la Secretar�a de Econom�a, los cuales se presentan a continuaci�n:<br></p></td></tr></table><br>'
					}
				]
			},
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 600,
				items: [
					{
						xtype:'label', style:  {  'font-size' : '12px'  },
						html:'<table width="600" border="2" >'+
										' <tr style="border-bottom:2pt solid black;" bgcolor="#BDBDBD" >'+
											'<td colspan = "5" width="100%"  height= "30" ><p  style="text-align:center;  font-size:100%; "><b>Estratificaci�n</b></p></td>'+
										'</tr>'+
										'<tr style="border-bottom:2pt solid black;" height= "30" bgcolor="#BDBDBD"  >'+
											'<td colspan = "1" width="10%" ><p  style="text-align:center;  font-size:100%; "><b>Tama�o</b></p></td>'+
											'<td colspan = "1" width="20%" ><p  style="text-align:center;  font-size:100%; "><b>Sector</b></p></td>'+
											'<td colspan = "1" width="20%" ><p  style="text-align:center;  font-size:100%; "><b>Rango de n�mero de trabajadores</b></p></td>'+
											'<td colspan = "1" width="30%"><p  style="text-align:center;  font-size:100%; "><b>Rango de monto de ventas anuales(mdp)</b></p></td>'+
											'<td colspan = "1" width="10%" ><p  style="text-align:center;  font-size:100%; "><b>Tope m�ximo combinado*</b></p></td>'+
										'</tr>'+
										'<tr style="border-bottom:2pt solid black;" height= "30" >'+
											'<td colspan = "1" width="10%" ><p  style="text-align:center;  font-size:100%">Micro</p></td>'+
											'<td colspan = "1" width="20%" ><p  style="text-align:center;  font-size:100%">Todas</p></td>'+
											'<td colspan = "1" width="20%" ><p  style="text-align:center;  font-size:100%">Hasta 10</p></td>'+
											'<td colspan = "1" width="30%"><p  style="text-align:center;  font-size:100%">Hasta $4</p></td>'+
											'<td colspan = "1" width="10%" ><p  style="text-align:center;  font-size:100%">4.6</p></td>'+
										'</tr>'+
										
										'<tr style="border-bottom:2pt solid black;" height= "30" >'+
											'<td rowspan = "2" width="10%" ><p  style="text-align:center;  font-size:100%">Peque�a</p></td>'+
											'<td colspan = "1" width="20%" ><p  style="text-align:center;  font-size:100%">Comercio</p></td>'+
											'<td colspan = "1" width="20%" ><p  style="text-align:center;  font-size:100%">Desde 11 hasta 30</p></td>'+
											'<td colspan = "1" width="30%"><p  style="text-align:center;  font-size:100%">Desde $4.01 hasta $100</p></td>'+
											'<td colspan = "1" width="10%" ><p  style="text-align:center;  font-size:100%">93</p></td>'+
										'</tr>'+
										
										'<tr style="border-bottom:2pt solid black;" height= "30">'+
											'<td colspan = "1" width="20%" ><p  style="text-align:center;  font-size:100%">Industrial y Servicios</p></td>'+
											'<td colspan = "1" width="20%" ><p  style="text-align:center;  font-size:100%">Desde 11 hasta 50</p></td>'+
											'<td colspan = "1" width="30%"><p  style="text-align:center;  font-size:100%">Desde $4.01 hasta $100</p></td>'+
											'<td colspan = "1" width="10%" ><p  style="text-align:center;  font-size:100%">95</p></td>'+
										'</tr>'+
										
										'<tr style="border-bottom:2pt solid black;" height= "30">'+
											'<td rowspan = "3" width="20%" style="border-bottom:2pt solid black;" ><p  style="text-align:center;  font-size:100%">Mediana</p></td>'+
											'<td colspan = "1" width="20%" style="border-bottom:2pt solid black;" ><p  style="text-align:center;  font-size:100%">Comercio</p></td>'+
											'<td colspan = "1" width="30%" style="border-bottom:2pt solid black;"><p  style="text-align:center;  font-size:100%">Desde 31 hasta 100</p></td>'+
											'<td rowspan = "2" width="10%" style="border-bottom:2pt solid black;"><p  style="text-align:center;  font-size:100%">Desde $100.01 hasta $250 </p></td>'+
											'<td rowspan = "2" width="10%" style="border-bottom:2pt solid black;"><p  style="text-align:center;  font-size:100%">235</p></td>'+
										'</tr>'+
										
										'<tr style="border-bottom:2pt solid black;" height= "30">'+
											'<td colspan = "1" width="20%" ><p  style="text-align:center;  font-size:100%">Servicio</p></td>'+
											'<td colspan = "1" width="30%"><p  style="text-align:center;  font-size:100%">Desde 51 hasta 100</p></td>'+
										'</tr>'+
										
										'<tr style="border-bottom:2pt solid black;" height= "30">'+
											'<td colspan = "1" width="20%"><p  style="text-align:center;  font-size:100%">Industrial</p></td>'+
											'<td colspan = "1" width="30%"><p  style="text-align:center;  font-size:100%">Desde 51 hasta 100</p></td>'+
											'<td colspan = "1" width="10%" ><p  style="text-align:center;  font-size:100%">Desde $100.01 hasta $250</p></td>'+
											'<td colspan = "1" width="10%" ><p  style="text-align:center;  font-size:100%">250</p></td>'+										
										'</tr>'+										
								'</table>'						
					}
				]
			},
			
			{
				xtype: 'fieldcontainer',
				combineErrors: false,
				msgTarget: 'side',
				width: 600,
				items: [
					{
						xtype:'label', 
						width: 600,
						style:  {   'font-size' : '12px' },
						html:'<table width="600"  ><tr><td><div><p style=�text-align: justify;� >El estrato de microempresa incluye a las Personas F�sicas con Actividad Empresarial (PFAE), y Micro productor incluye a las PFAE sin cuenta de cheques bancaria.<br><br>'+
						'*Tope M�ximo Combinado = (Trabajadores) x 10% + (Ventas Anuales) x 90%.<br><br>'+
						'El tama�o de la empresa se determinar� a partir del puntaje obtenido conforme a la siguiente f�rmula:<br>'+
						'Puntaje de la empresa = (N�mero de trabajadores) x 10% + (Monto de Ventas Anuales) x 90%, el cual debe ser igual o menor al Tope M�ximo Combinado de su categor�a.<br><br>'+
						'&nbsp;&nbsp;&nbsp;&nbsp;<b>NOTA:</b> Estos criterios son una reproducci�n exacta de los publicados el 30 de     junio de 2009 por la Secretar�a de Econom�a en el Diario Oficial de la Federaci�n.<br><br>'+
						'Se considera gran empresa al estrato que exceda los l�mites de la estratificaci�n de mediana empresa.</p></div><br><br></td></tr></table>'
					}
				]
			}
		
		];
		var fpAyuda = new Ext.form.FormPanel({
			itemId			: 'fpAyuda',	
			layout			: 'form',
			width				: 650,			
			height			: 550,
			style				: ' margin:0 auto;',
			frame				: false,
			hidden			: false,
			collapsible		: false,
			titleCollapse	: false	,
			labelWidth	: 200,
			bodyStyle		: 'padding: 8px',
			defaults			: { msgTarget: 'side',anchor: '-20' },
			items: elementosAyuda
		});
	var ventanaAyuda = new Ext.Window({
		modal: true,
		resizable: false,
		layout: 'form',
		x: 400,
		width: 650,
		height : 550,
		itemId: 'winVistaPrev',
		closable: true,
		closeAction: 'hide',
		items: fpAyuda
			
	});
			var panelPregunta = new Ext.Panel({
				itemId: 'pnlPreg'+vNumPregunta,
				frame: false,
				layout:'table',
				border	: true,	
				autoHeight:true,
				identificador_preg:'renglonPreguntas',
				layoutConfig: {columns:3},
				monitorValid: true,
				items:[
					{xtype:'hidden', name: vNumPregunta+'_numPreg', value:vNumPregunta },
					{xtype:'hidden', name: vNumPregunta+'_numOpc', value:vNumOpc},
					{xtype:'hidden', name: vNumPregunta+'_tipoResp', value:vTipoResp},
					{xtype:'hidden', name: vNumPregunta+'_resp_inicial_radio', itemId: vNumPregunta+'_resp_inicial_radio',value:''},
					{xtype:'hidden', name: vNumPregunta+'_opcion_check', itemId: vNumPregunta+'_opcion_check',value:''},
					{xtype:'hidden', name: vNumPregunta+'_preg_excluyente', itemId: vNumPregunta+'_preg_excluyente',value:''},
					{xtype:'hidden', name: vNumPregunta+'pregExcluyante', itemId: vNumPregunta+'pregExcluyante',value:'N'},
					{
						xtype:'textfield',
						itemId:'txt_aux_InfCuali',
						width: 100,
						hidden:true
					},
					{
						xtype:'textfield',
						itemId:'guardar_activo_InfCuali',
						width: 100,
						value:'N',
						hidden:true
					},
					{	
						xtype: 'button',	
						itemId: 'btnAyuda'+vNumPregunta,	
						iconCls: 'icoAyuda',
						width:tamColum1, 
						hidden:(vNumPregunta==2)?false:true,
						icPregunta: vCgPregunta,
						handler: function(obj){
							ventanaAyuda.setTitle('<p '+est+'><b>'+obj.icPregunta+'</b></p>');
							ventanaAyuda.setVisible(true);
						}
					},
					{ 	
						width:tamColum2, 
						xtype: 'displayfield', 
						value: '<p '+est+'>'+vCgPregunta+':</p>'	
					}					
				]
				
			});
			
			var panelOpcPreg = new Ext.Panel({
				itemId: 'pnlOpcPreg'+vNumPregunta,
				frame: false,
				layout:'form',
				width:tamColum3,
				border	: false,
				
				items:[ 
				]				 
			});
			
			
			
			var nNumOpciones = Number(vNumOpc);
			for(var z=0; z<nNumOpciones; z++){// Se obtine la parametrizaci�n de la configuraci�n de las preguntas 
				var objData=null;
				objData = eval("objResp.p"+vNumPregunta);
				objData = objData[z];	
				if(objData!=null){
					
				var IC_RESP = objData.IC_RESP;
				var IC_PREGUNTA = objData.IC_PREGUNTA; 
				var TIPO_RESP = objData.TIPO_RESP; 
				var OPC_RESP = objData.OPC_RESP; 
				var PREG_EXC = objData.PREG_EXC; 
				
				var LIBRE_OBLIG = objData.LIBRE_OBLIG; 
				var RESP_ABIERTA_ASO = objData.RESP_ABIERTA_ASO; 
				var LIBRE_TIPO_DATO = objData.LIBRE_TIPO_DATO; 
				var LIBRE_LONG = objData.LIBRE_LONG; 
				var CS_ACTIVO = objData.CS_ACTIVO;
				var OPCION_OTROS = objData.OPCION_OTROS;
				
				var IC_SOLICITUD = objData.IC_SOLICITUD;
				var CG_RESPUESTA_LIBRE = objData.CG_RESPUESTA_LIBRE;
				var CG_NOMBRE_OTRO = objData.CG_NOMBRE_OTRO;
				var CHECK_OPCION_RESP = objData.CHECK_OPCION_RESP;
				var valorMaximo =0;
				var ban = 0;
				var entero =0;
				var decimal = 0;
				if(vTipoResp == 3||vTipoResp == 1){ 
					if(LIBRE_TIPO_DATO == 'N'&&LIBRE_LONG >30  ){
						valorMaximo = 30;
						ban = 1;
					}
					if(LIBRE_TIPO_DATO == 'A'&&LIBRE_LONG >400){
						valorMaximo = 400;
						ban = 1;
					}
					if(ban != 1){
						valorMaximo=LIBRE_LONG;
					}
				}else{
					
					
					
				}
				if(vTipoResp ==3 || vTipoResp ==2){
					if(vTipoResp == 2){ 
						panelOpcPreg.add([
						 {
							  xtype:'panel',
							  frame: false,
							 
							  border	: false,
							 
							  height: (RESP_ABIERTA_ASO=='S'&&OPCION_OTROS=='S')?65:30,
							  items: [
								{
									xtype:'hidden',
									name: vNumPregunta+'opcion_resp_inicial'+z,
									value:IC_RESP
								},
								{
									  xtype:'panel',
									  frame: false,
									  border	: false,
									  identificador: 'renglonRespuesta',
									  numRespuesta :vNumPregunta,
									  numOpcion :z,
									  tipo_resp : vTipoResp,
									  layout:(RESP_ABIERTA_ASO=='S')?'table':'form',
									  items: [
											{
												xtype:'textfield',
												itemId: vNumPregunta+'ban_guarda_check'+z,
												value:(CHECK_OPCION_RESP=='S')?true:false,
												hidden:true
											},
											{
												xtype:'textfield',
												itemId: vNumPregunta+'ban_guarda_inicial'+z,
												value:(CHECK_OPCION_RESP=='S')?true:false,
												hidden:true
											},
											{ 
												boxLabel: OPC_RESP,
												name: vNumPregunta+'ic_opcion_resp'+z,
												inputValue: IC_RESP,
												xtype: 'checkbox',
												numPreg:vNumPregunta,
												
												abierta:RESP_ABIERTA_ASO,
												otra_opc:OPCION_OTROS,
												hidden:(CS_ACTIVO=='N')?true:false,
												opcion:z,
												checked: (CHECK_OPCION_RESP=='S')?true:false,
												width:'auto',
												PREG_EXC:PREG_EXC,
												valor_ant:(CHECK_OPCION_RESP=='S')?false:true,
												itemId : vNumPregunta+'ic_opcion_resp'+z,
												listeners: {
													change : {
														fn : function(checkbox, checked){
															var campo = Ext.ComponentQuery.query('#'+checkbox.numPreg+'cajaTexto'+checkbox.opcion)[0];
															var campoAdd = Ext.ComponentQuery.query('#'+checkbox.numPreg+'addCajaTexto'+checkbox.opcion)[0];
															Ext.ComponentQuery.query('#btnGuardar')[0].enable();
															Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('S');
															Ext.ComponentQuery.query('#btnGuardar')[0].enable();
															Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('S');
															if(checked==true){
															
																if(checkbox.abierta=='S'){
																	campo.setDisabled(false);
																	campo.allowBlank= false;
																	campo.validate();
																}
																if(checkbox.otra_opc=='S'){
																	campoAdd.setDisabled(false);
																	campoAdd.allowBlank= false;
																	campoAdd.validate();
																}
																var PREG_EXC = checkbox.PREG_EXC;
																if(PREG_EXC!=''){
																	var index = (PREG_EXC).search("-");
																	var rang1=0;
																	var rang2=0;
																	if(index != -1){
																	
																		rang1 = (PREG_EXC).substring(0,index);
																		rang2 = (PREG_EXC).substring(index+1);
																		rang1=rang1;
																		rang2=rang2;
																		for(var w=rang1; w<=rang2; w++){
																			var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
																			limpiarPregunta(w);
																			Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
																			respPrev[0].setDisabled(false);//pnlOpcPreg
																		}
																	}else{
																		rang1 = PREG_EXC;
																		var w=rang1;
																		var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
																		limpiarPregunta(w);
																		Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
																		respPrev[0].setDisabled(false);
																	}
																}
																checkbox.valor_ant= true;
															}else if(checked==false){
																campo.setValue('');
																campo.setDisabled(true);
																campo.allowBlank= true;
																campo.validate();
																campoAdd.setValue('');
																campoAdd.setDisabled(true);
																campoAdd.allowBlank= false;
																campoAdd.validate();
																
																var PREG_EXC = checkbox.PREG_EXC;
																PREG_EXC = Ext.ComponentQuery.query('#'+checkbox.numPreg+'ic_opcion_resp'+checkbox.opcion)[0].PREG_EXC;
																	if(PREG_EXC!=''){
																		var index = (PREG_EXC).search("-");
																		var rang1=0;
																		var rang2=0;
																		if(index != -1){
																			rang1 = (PREG_EXC).substring(0,index);
																			rang2 = (PREG_EXC).substring(index+1);
																			rang1=rang1;
																			rang2=rang2;
																			for(var w=rang1; w<=rang2; w++){
																				var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
																				Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('N');
																				limpiarPregunta(w);
																				respPrev[0].setDisabled(true);//pnlOpcPreg
																				
																			}
																		}else{
																			rang1 = PREG_EXC;
																			var w=rang1;
																			var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
																			Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('N');
																			limpiarPregunta(w);
																			respPrev[0].setDisabled(true);
																		}
																	}
																	checkbox.valor_ant= false;
															}
															
														 }
														
													}
											}
											},
											{
												xtype:'displayfield',
												width: 10
											},
											{
												xtype:'textfield',
												itemId:'CampoDeRespaldo'+vNumPregunta+z,
												name: 'CampoDeRespaldo'+vNumPregunta+z,
												width: 30,
												hidden:true
											},
											{
												xtype: 'textfield',
												itemId: vNumPregunta+'addCajaTexto'+z,
												name: vNumPregunta+'addCajaTexto'+z,
												numPreg:vNumPregunta,
												maxLength:400,
												readOnly:(CS_ACTIVO=='N')?true:false,
												width: 100,
												disabled: (CHECK_OPCION_RESP=='S')?false:true,
												width:100,
												msgTarget: 'side',
												margins: '0 20 0 0', 
												value : (CHECK_OPCION_RESP=='S')?CG_NOMBRE_OTRO:'',
												hidden:((CS_ACTIVO=='N'))?true:(OPCION_OTROS =='S')?false:true,
												maskRe : '!\"#$%&\'()*+,-./0123456789:;<=>?@',
												listeners: {
													focus: function(fld) {
														Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].setValue(fld.getValue());
													},
													blur: function(fld, value){
														var valAnterior = Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].getValue();
														if(valAnterior!=fld.getValue()){
															Ext.ComponentQuery.query('#btnGuardar')[0].enable();
															Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('S');
														}
													}
												}
												
											},
											{
												xtype:'displayfield',
												width: 10
											},
											{
												xtype:'hidden',
												width: 10
											},
											{
												xtype: 'textfield',
												itemId:  vNumPregunta+'cajaTexto'+z,
												name:  vNumPregunta+'cajaTexto'+z,
												numPreg:vNumPregunta,
												numOpcion :z,
												libre_tipo_dato:LIBRE_TIPO_DATO,
												libre_long:LIBRE_LONG,
												msgTarget: 'side',
												width:80,
												vtype:'vPorcentaje',
												disabled: (CHECK_OPCION_RESP=='S')?false:true,
												//margins: '0 20 0 0', 
												banPorcentaje:'N',
												value : (CHECK_OPCION_RESP=='S')?CG_RESPUESTA_LIBRE:'',
												nNumOpciones:nNumOpciones,
												hidden:((CS_ACTIVO=='N'))?true:(RESP_ABIERTA_ASO =='S')?false:true,
												maskRe:(LIBRE_TIPO_DATO=='A')? '!\"#$%&\'()*+,-./0123456789:;<=>?@':/[0-9\.]/,
												
												listeners: {
													focus: function(fld) {
														Ext.ComponentQuery.query('#CampoDeRespaldo'+fld.numPreg+fld.numOpcion)[0].setValue(fld.getValue());
														Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].setValue(fld.getValue());
														
													},
													blur: function(fld, value){
															var valAnterior = Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].getValue();
															var objInto = Ext.ComponentQuery.query('#'+fld.numPreg+'cajaTexto'+fld.numOpcion)[0];
															objInto.regex = '';
															objInto.regexText = '';
															var libre_tipo_dato = fld.libre_tipo_dato;
															var libre_long = fld.libre_long.replace('.', ',');
															if(libre_tipo_dato=='N'){
																var posicion = libre_long.indexOf(',');
																entero = libre_long.substring(0,posicion);
																decimal = libre_long.substring(posicion+1);
																objInto.regex = new RegExp("(^(([0-9]{1,"+entero+"}))(\\.[0-9]{1,"+decimal+"})$)|(^((\\d){1,"+entero+"})$)");
																objInto.regexText = 'El tama�o m�ximo del campos es de '+entero+' enteros '+decimal+' decimales';
																objInto.validate();
																
															}else{
																var valorMaximo = 0;
																var ban =0;
																if(Number(libre_long) >400){
																	valorMaximo = 400;
																	ban = 1;
																}
																	if(ban != 1){
																		valorMaximo=libre_long;
																	}
																	re =  new RegExp("^.{1,"+valorMaximo+"}$");
																	objInto.regex = re;
																	objInto.regexText='El tama�o m�ximo del campos es de '+valorMaximo;
																	objInto.validate();
															}
														
															if(valAnterior!=fld.getValue()){
																Ext.ComponentQuery.query('#btnGuardar')[0].enable();
																Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('S');
															}
															
															
															
													}
												}
												
												
											},
											{
												xtype:'displayfield',
												width: 10,
												hidden:(RESP_ABIERTA_ASO =='N')?true:false,
												value:(RESP_ABIERTA_ASO=='S')?' %':''
											}
										]
									}
									
								]
						 }
						
						
						]);
						
					}else{
						if(CHECK_OPCION_RESP=='S'){
							Ext.ComponentQuery.query('#'+vNumPregunta+'_resp_inicial_radio')[0].setValue((CHECK_OPCION_RESP=='S')?IC_RESP:'');
							Ext.ComponentQuery.query('#'+vNumPregunta+'_opcion_check')[0].setValue(z);
							Ext.ComponentQuery.query('#'+vNumPregunta+'_preg_excluyente')[0].setValue(PREG_EXC);
						}
						panelOpcPreg.add([
						 {
							  xtype:'panel',
							  frame: false,
							  border	: false,
							  hidden:(CS_ACTIVO=='S')?false:true,
							  items: [
								
								{
									  xtype:'panel',
									  frame: false,
									  border	: false,
									  identificador: 'renglonRespuesta',
									  numRespuesta :vNumPregunta,
									  numOpcion :z,
									  tipo_resp : vTipoResp,
									  layout:(RESP_ABIERTA_ASO=='S')?'table':'form',
									  items: [
											
											{	
												xtype:'radiogroup',	
												itemId:vNumPregunta+'radioGroup'+z,	
												name:vNumPregunta+'radioGroup'+z,
												numPreg:vNumPregunta,
												opcion:z,
												hidden:(CS_ACTIVO=='N')?true:false,
												PREG_EXC:PREG_EXC,
												abierta:RESP_ABIERTA_ASO,
												otra_opc:OPCION_OTROS,
												items: 
													[
														{	
															boxLabel	: OPC_RESP,
															itemId:vNumPregunta+'ic_opcion_resp', 
															name : vNumPregunta+'ic_opcion_resp',
															readOnly:(CS_ACTIVO=='N')?true:false,
															inputValue: IC_RESP,
															PREG_EXC:PREG_EXC,
															width: 80,
															checked: (CHECK_OPCION_RESP=='S')?true:false
														},
														{
															xtype:'textfield',
															name: vNumPregunta+'opcion_resp_inicial'+z,
															value:IC_RESP,
															hidden:true
														},
														{
															xtype:'textfield',
															itemId: vNumPregunta+'ban_guarda_check'+z,
															value:(CHECK_OPCION_RESP=='S')?true:false,
															hidden:true
														},
														{
															xtype:'textfield',
															itemId: vNumPregunta+'ban_guarda_inicial'+z,
															value:(CHECK_OPCION_RESP=='S')?true:false,
															hidden:true
														},
														{
															xtype:'displayfield',
															width: 10
														},
														{
															xtype: 'textfield',
															itemId: vNumPregunta+'addCajaTexto'+z,
															name: vNumPregunta+'addCajaTexto'+z,
															numPreg:vNumPregunta,
															readOnly:(CS_ACTIVO=='N')?true:false,
															maxLength:400,
															width: 100,
															msgTarget: 'side',
															margins: '0 20 0 0', 
															value : (CHECK_OPCION_RESP=='S')?CG_NOMBRE_OTRO:'',
															hidden:(OPCION_OTROS =='S')?false:true,
															maskRe : '!\"#$%&\'()*+,-./0123456789:;<=>?@',
															listeners: {
																focus: function(fld) {
																	Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].setValue(fld.getValue());
																},
																blur: function(fld, value){
																	var valAnterior = Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].getValue();
																	if(valAnterior!=fld.getValue()){
																		Ext.ComponentQuery.query('#btnGuardar')[0].enable();
																		Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('S');
																	}
																}
															}
															
														},
														{
															xtype:'displayfield',
															width: 10,
															hidden:(vNumPregunta ==7)?true:false
														},
														{
															xtype:'displayfield',
															width: 'auto',
															hidden:(vNumPregunta ==7)?false:true,
															width: 'auto',
															value:(RESP_ABIERTA_ASO=='S')?'�Cu�ntos a�os?':''
														},
														{
															xtype: 'textfield',
															itemId:  vNumPregunta+'cajaTexto'+z,
															name:  vNumPregunta+'cajaTexto'+z,
															numPreg:vNumPregunta,
															readOnly:(CS_ACTIVO=='N')?true:false,
															maxLength:valorMaximo,
															msgTarget: 'side',
															disabled:(CHECK_OPCION_RESP=='S')?false:true,
															width:80,
															value : (CHECK_OPCION_RESP=='S')?CG_RESPUESTA_LIBRE:'',
															margins: '0 20 0 0', 
															hidden:(RESP_ABIERTA_ASO =='N')?true:false,
															maskRe : (LIBRE_TIPO_DATO =='N')?/[0-9]/:'!\"#$%&\'()*+,-./0123456789:;<=>?@',
															listeners: {
																focus: function(fld) {
																	Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].setValue(fld.getValue());
																},
																blur: function(fld, value){
																	var valAnterior = Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].getValue();
																	if(valAnterior!=fld.getValue()){
																		Ext.ComponentQuery.query('#btnGuardar')[0].enable();
																		Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('S');
																	}
																}
															}
															
														}
													],
													listeners: {
														change: function (field, newValue, oldValue) {
															var campo = Ext.ComponentQuery.query('#'+field.numPreg+'cajaTexto'+field.opcion)[0];
															var campoAdd = Ext.ComponentQuery.query('#'+field.numPreg+'addCajaTexto'+field.opcion)[0];
															var PREG_EXC = field.PREG_EXC;
															Ext.ComponentQuery.query('#btnGuardar')[0].enable();
															Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('S');
															if(newValue[field.numPreg+'ic_opcion_resp']!=undefined){
																Ext.ComponentQuery.query('#'+field.numPreg+'_opcion_check')[0].setValue(field.opcion);
																if(field.abierta=='S'){
																	campo.setDisabled(false);
																	campo.allowBlank= false;
																	campo.validate();
																}
																if(field.otra_opc=='S'){
																	campoAdd.setDisabled(false);
																	campoAdd.allowBlank= false;
																	campoAdd.validate();
																}

																if(PREG_EXC!=''){
																	var index = (PREG_EXC).search("-");
																	var rang1=0;
																	var rang2=0;
																	if(index != -1){
																	
																		rang1 = (PREG_EXC).substring(0,index);
																		rang2 = (PREG_EXC).substring(index+1);
																		rang1=rang1;
																		rang2=rang2;
																		for(var w=rang1; w<=rang2; w++){
																			var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
																			limpiarPregunta(w);
																			Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
																			respPrev[0].setDisabled(false);//pnlOpcPreg
																		}
																	}else{
																		rang1 = PREG_EXC;
																		var w=rang1;
																		var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
																		limpiarPregunta(w);
																		Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
																		respPrev[0].setDisabled(false);
																	}
																}else{
																	if(field.opcion==0){
																		PREG_EXC = Ext.ComponentQuery.query('#'+field.numPreg+'radioGroup1')[0].PREG_EXC;
																	}else{
																		PREG_EXC = Ext.ComponentQuery.query('#'+field.numPreg+'radioGroup0')[0].PREG_EXC;
																	}
																	if(PREG_EXC!=''){
																		var index = (PREG_EXC).search("-");
																		var rang1=0;
																		var rang2=0;
																		if(index != -1){
																			rang1 = (PREG_EXC).substring(0,index);
																			rang2 = (PREG_EXC).substring(index+1);
																			rang1=rang1;
																			rang2=rang2;
																			for(var w=rang1; w<=rang2; w++){
																				var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
																				Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('N');
																				respPrev[0].setDisabled(true);//pnlOpcPreg
																				
																			}
																		}else{
																			rang1 = PREG_EXC;
																			var w=rang1;
																			var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
																			Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('N');
																			respPrev[0].setDisabled(true);
																		}
																	}
																}
																
															}else{
																campo.setValue('');
																campo.setDisabled(true);
																campo.allowBlank= true;
																campo.validate();
																campoAdd.setValue('');
																campoAdd.setDisabled(true);
																campoAdd.allowBlank= true;
																campoAdd.validate();
															}
														}
													}
											}
										]
									}
								]
						 }
						
						
						]);
						
					}
					panelOpcPreg.doLayout();//ventanaAyuda
					panelPregunta.add(panelOpcPreg);
					panelPregunta.doLayout();
					
				}else{
					var valorMaximo =0;
					var ban = 0;
					
					if(TIPO_RESP == 'A'&&LIBRE_LONG >400){
						valorMaximo = 400;
						ban = 1;
					}
					if(ban != 1){
						valorMaximo=LIBRE_LONG;
					}
					
					panelPregunta.add(
						{
							xtype:'panel',
							frame: false,
							order	: false,
							border :false,
							identificador: 'renglonRespuesta',
							numRespuesta :vNumPregunta,
							numOpcion :z,
							tipo_resp : vTipoResp,
							layout:'table',
							hidden:(CS_ACTIVO=='S')?false:true,
							items: [
								{
									xtype:'hidden',
									name: vNumPregunta+'ic_opcion_resp',
									value:IC_RESP
								},
								{
									xtype:'displayfield',
									width: 20,
									value:(CHECK_OPCION_RESP=='S')?'$':''
								},
								{
									xtype: 'textfield',
									itemId:  vNumPregunta+'cajaTexto'+z,
									name:  vNumPregunta+'cajaTexto'+z,
									width:tamColum3,
									numPreg:vNumPregunta,
									numOpcion:z,
									readOnly: (solo_read=='S')?true:false,
									align:'center',
									libre_tipo_dato:LIBRE_TIPO_DATO,
									libre_long:LIBRE_LONG,
									//maxLength:valorMaximo,
									value:(CHECK_OPCION_RESP=='S')?CG_RESPUESTA_LIBRE:'',
									width: (LIBRE_TIPO_DATO =='N')?150:250,
									allowBlank: (LIBRE_OBLIG==1)?false:true,
									msgTarget: 'side',
									margins: '0 20 0 0', 
									hidden:(LIBRE_TIPO_DATO =='N'||LIBRE_TIPO_DATO =='A')?false:true,
									//maskRe : (LIBRE_TIPO_DATO =='N')?/[0-9]/:'!\"#$%&\'()*+,-./0123456789:;<=>?@',
									anchor:			'60%',
									listeners: {
										focus: function(fld) {
											Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].setValue(fld.getValue());
										},
										blur: function(fld, value){
											var valAnterior = Ext.ComponentQuery.query('#txt_aux_InfCuali')[0].getValue();
											if(valAnterior!=fld.getValue()){
												Ext.ComponentQuery.query('#btnGuardar')[0].enable();
												Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('S');
											}
											var objInto = Ext.ComponentQuery.query('#'+fld.numPreg+'cajaTexto'+fld.numOpcion)[0];
											objInto.regex = new RegExp("");
											objInto.regexText = '';
											
											var libre_tipo_dato = fld.libre_tipo_dato;
											var libre_long = fld.libre_long.replace('.', ',');
											if(libre_tipo_dato=='N'){
												var posicion = libre_long.indexOf(',');
												
												if(posicion!=-1){
													entero = libre_long.substring(0,posicion);
													decimal = libre_long.substring(posicion+1);
												
													objInto.regex = new RegExp("(^(([0-9]{1,"+entero+"}))(\\.[0-9]{1,"+decimal+"})$)|(^([0-9]{1,"+entero+"})$)");
													objInto.regexText = 'El tama�o m�ximo del campos es de '+entero+' enteros '+decimal+' decimales';
													objInto.validate();
												}else{
													objInto.regex = new RegExp("(^(([0-9]{1,"+libre_long+"}))$)");
													objInto.regexText = '<div > <table width="100" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center>El tama�o m�ximo del campos es de '+libre_long+' enteros</center></td></tr> </table> </div>';
													objInto.validate();
												}
														
											}else{
												var valorMaximo = 0;
												var ban =0;
												if(Number(libre_long) >400){
													valorMaximo = 400;
													ban = 1;
												}
												if(ban != 1){
													valorMaximo=libre_long;
												}
												var re =  new RegExp("^.{1,"+valorMaximo+"}$");
												objInto.regex = re;
												objInto.regexText='El tama�o m�ximo del campos es de '+valorMaximo;
												objInto.validate();
											}
										}
									}
								}
										
							]
						}
					);
					
				}
				}
			}
			panelPregunta.doLayout();
			objFilaResp =  new Ext.Panel({
				itemId: 'pnlVista'+vNumPregunta,
				identificador: 'renglon',
				
				frame: false
					
			});
			objFilaResp.add(panelPregunta);
			objFilaResp.doLayout();
			fp.add(objFilaResp);
			fp.doLayout();
			}
		}//for
		
		}
	}
});
// para el paso 3 ontener el cr�dito vinculado 
NE.Informacion.FormCreditoVinculado= Ext.extend(Ext.form.FormPanel,{
	objDatoInicial: null,
	campo_activo:'S',
	initComponent : function(){
		 Ext.apply(this, {
			width: 800,
			itemId:	'ceditoVinculadoV',
			height:400,
			frame: false,   
			border: false,
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,padding:6px;,text-align:left',
			autoScroll: true,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side'
			},
			listeners:{
				afterrender: function(obj){
					obj.generarCampos( obj);
				}
			}
		});
		
		NE.Informacion.FormCreditoVinculado.superclass.initComponent.call(this);
	},
	generarCampos: function(fp){
		var lstDatosIni = fp.objDatoInicial;
		var info = lstDatosIni[0];
		var solo_read =fp.campo_activo;
		var CS_CREDITO_VINCULADO = info.CS_CREDITO_VINCULADO;
		var FG_MONTO_CRED_VINCULADO =  info.FG_MONTO_CRED_VINCULADO;
	//FG_MONTO_CRED_VINCULADO = Ext.util.Format.usMoney(FG_MONTO_CRED_VINCULADO.replace(/[^0-9\.]/g, ''));
	
		
		var IC_SOLICITUD =  info.IC_SOLICITUD;
		var panelCredVinc = new Ext.Panel({
				itemId: 'pnCredVinc',
				frame: false,
				layout:'form',
				width:790,
				border	: false,	
				items:[
					{
						xtype:'textfield',
						itemId:'txt_aux_cred',
						width: 100,
						hidden:true
					},
					{
						xtype:'textfield',
						itemId:'guardar_activo_cred',
						width: 100,
						value:'N',
						hidden:true
					},
					{
						xtype: 'fieldcontainer',
						combineErrors: false,
						msgTarget: 'side',
						width: 790,
						items: [
							{
								xtype:'label',
								style: {  'font-size' : '12px'  },
								 html:'<table width="790">'+
										'<tr><td height="30"  ><p style="text-align:justify; font-size:100%">�El Intermediario cuenta con alg�n <b>cr�dito vinculado</b>? Entendido �ste como:</p></td></tr>'+
										'<tr><td height="70"><p style="text-align:justify; font-size:100%">Se deber� considerar como una sola persona a aquella que por sus nexos patrimoniales o de responsabilidad constituyan un riesgo com�n para el Intermediario y que este riesgo se conforma cuando se presenta la relaci�n que a continuaci�n se indica, entre el deudor y las siguientes personas:</p></td></tr>'+
										'<tr><td height="30"><p style="text-align:justify; font-size:100%">A) Cuando el deudor sea persona f�sica:</p></td></tr>'+
										'<tr><td height="30" ><p style="text-align:justify;	text-indent: 25px;   font-size:100%">1. Las personas f�sicas que dependan econ�micamente de �ste.</p></td></tr>'+
										'<tr><td height="30" ><p style="text-align:justify;  text-indent: 25px;   font-size:100%">2. Las personas morales que sean controladas directa o indirectamente por el propio deudor, con la independencia de que pertenezcan o no a &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;un mismo Grupo Empresarial o Consorcio</p></td></tr>'+
										'<tr><td height="30" ><p style="text-align:justify; font-size:100%">B)	Cuando el deudor sea persona moral:</p></td></tr>'+
										'<tr><td height="40" ><p style="text-align:justify; text-indent: 25px;   font-size:100%">1. La persona o grupo de personas f�sicas y morales que act�en en forma concertada y ejerzan directa o indirectamente la administraci�n o &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t�tulo de due�o, o el control de la persona moral acreditada.</p></td></tr>'+
										'<tr><td height="40" ><p style="text-align:justify; text-indent: 25px;   font-size:100%">2. Las personas morales que sean controladas directa o indirectamente por el propio deudor, con la independencia de que pertenezca o no a un &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mismo Grupo Empresarial y, en su caso, Consorcio.</p></td></tr>'+
										'<tr><td height="30" ><p style="text-align:justify;  text-indent: 25px; font-size:100%">3. Las personas morales que pertenezcan al mismo Grupo Empresarial o, en su caso, Consorcio.</p></ul></td></tr>'+
									 '</table><br>'
							}
						]
					},
					{
						xtype:'panel',
						frame: false,
						layout:'table',
						border	: false,
						width:800,
						items: [
							{
								xtype: 'combo',
								itemId:  'tieneCredVinc',
								name:  'tieneCredVinc',
								width: 150,
								forceSelection:true,
								style: 'margin:0 auto;',
								mode:				'local',
								displayField:	'descripcion',
								valueField:		'clave',
								typeAhead:		true,
								readOnly: (solo_read=='S')?true:false,
								triggerAction:	'all',
								allowBlank: false,
								value:(CS_CREDITO_VINCULADO!=''?CS_CREDITO_VINCULADO:''),
								store: new Ext.data.ArrayStore(
									{
										fields: [
											'clave',
											'descripcion'
										],
										data: [['S', 'S�'], ['N', 'No']],
										listeners: {
											load: function(store, records, success) 
											{
												if(records.length >0 &&  success){
													var monto = Ext.ComponentQuery.query('#montoCredVinc')[0];
													//if(Ext.isEmpty(monto.getValue())){
													
												//	}
												}
											}
										}
									}
									
								),
								listeners:{
									select:{
									  fn:function(combo, value) {
											if(combo.getValue()=='N'){
												Ext.ComponentQuery.query('#montoCredVinc')[0].setDisabled(true);
												Ext.ComponentQuery.query('#montoCredVinc')[0].setValue('');
											}else{
												Ext.ComponentQuery.query('#montoCredVinc')[0].setDisabled(false);
											}
									  }
									},
									change:function(obj, newValue, oldValue){
										
										if(oldValue!=''){
											//se habilitaan todas las respuestas
											if(newValue!=oldValue){
												Ext.ComponentQuery.query('#btnGuardar')[0].enable();
												Ext.ComponentQuery.query('#guardar_activo_cred')[0].setValue('S');
											}
															
											
										}
																				
									}
								}
							},
							{
								xtype:'displayfield',
								width: 10
							},
							{	//Nuevo componente 
								xtype: 			'bigdecimal',
								itemId		:'montoCredVinc',
								name			:'montoCredVinc',
								fieldLabel: 'Monto',
								allowBlank: false,
								maxText:'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
								maxValue: 		'99999999999999999.99',
								minValue: 		'0',								
								format:			'00000000000000000.00',
								width: 300,
								value: FG_MONTO_CRED_VINCULADO,
								disabled:((CS_CREDITO_VINCULADO=='S')?false:true),
								listeners: {
									focus: function(fld) {
										Ext.ComponentQuery.query('#txt_aux_cred')[0].setValue(fld.getValue());
									},
									blur: function(fld, value){
										var valAnterior = Ext.ComponentQuery.query('#txt_aux_cred')[0].getValue();
										var valNuevo = Ext.ComponentQuery.query('#montoCredVinc')[0].getValue();
										if(valAnterior!=valNuevo){
											Ext.ComponentQuery.query('#btnGuardar')[0].enable();
											Ext.ComponentQuery.query('#guardar_activo_cred')[0].setValue('S');
										}
									}
								}
							},
							{
								xtype:'displayfield',
								width: 150,
								value:'<center>Cifras en miles de pesos</center>'
							}
						]
					}
				]				
			});
		
		fp.add(panelCredVinc);
		fp.doLayout();
	
	}
});





// para el paso 4 Informaci�n financiera
NE.Informacion.FormInfoFinanciera= Ext.extend(Ext.form.FormPanel,{
	objDatoInicial: null,
	campo_activo:'S',
	initComponent : function(){
		 Ext.apply(this, {
			width: 945,
			itemId:	'infoFinancieraV',
			height:450,
			frame: false,   
			border: false,
			monitorValid: true,
			autoScroll: false,
			style: 'margin:0 auto;',
			listeners:{
				afterrender: function(obj){
					obj.generarCampos( obj);
				}
			}
		});
		
		NE.Informacion.FormInfoFinanciera.superclass.initComponent.call(this);
	},
	generarCampos: function(fp){
		var lstDatosIni = fp.objDatoInicial;
		var info = lstDatosIni[0];
		var anio =  info.IG_ANIO_INF_FINANCIERA;
		var mes =  info.IG_MES_INF_FINANCIERA;
		var solo_read =fp.campo_activo;		
		var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		var mesyanio= meses[mes-1];
		var inicio_combo = 0;
		Ext.define('ListaRegistros',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'CS_CONCEPTO'     },
				{name: 'ANIO_1'     },
				{name: 'ANIO_2'     },
				{name: 'ANIO_3'     },
				{name: 'CAMPO'     },
				{name: 'ID_CAMPO'     }
			]
		});
		var consultaInfoFinan = Ext.create('Ext.data.Store',{
			model: 'ListaRegistros',
			storeId:'consultaInfoFinan',
			itemId :'storeInfFinan',
			proxy: {
				type: 'ajax',
				url:  '39IfnbCalificate.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'Consulta_Grid_InfoFinanciera',
					ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
					
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: true
		});
		var gridInformaFinan = Ext.create('Ext.grid.Panel',{
			selType: 'cellmodel',
			plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
			itemId:         'gridInformaFinan',
			store: Ext.data.StoreManager.lookup('consultaInfoFinan'),
			width:          510,
			height:         280,
			margins: '0 20 0 0',
			columns: [
				{
					header: '<center>Concepto</center>', 
					tooltip: 'Concepto',
					dataIndex: 'CS_CONCEPTO',
					//sortable: true,
					width: 150,							
					resizable: true,				
					align: 'left',
					itemId:'cam_concepto'
				},
				{
					header: '<center>*</center>',
					dataIndex: 'ANIO_3',
					sortable: true,
					width: 100,
					resizable: true,
					align: 'right',
					renderer:function(value,metadata,registro){ 
						 var aux = Ext.util.Format.number(value,'$0,000.00');
						// var aux = value;
						if(value< 0) {
							return '<span style="color:red;">' + aux + '</span>';
						}else {
							return aux;
						}
					},
					itemId:'cam_anio3',
					editor: 
					{
						
						xtype: 'textfield',
						allowBlank: false,
						width: '50',
						regexText:"El tama�o m�ximo del campos es de 17 enteros 2 decimales",
						regex:/(^(((\d){1,17}))(\.\d{1,2})$)|(^((\d){1,17})$)|(^((-){1}(\d){1,17})$)|(^(((-){1}(\d){1,17}))(\.\d{1,2})$)/,
						maskRe: /[0-9\.\-]/,
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txt_aux_InfF')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txt_aux_InfF')[0].getValue();
								if(valAnterior!=fld.getValue()){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo_InfF')[0].setValue('S');
								}
							}
						}
						
						
						
					} 			
				},
				{
					header: '<center>*</center>',
					dataIndex: 'ANIO_2',
					sortable: true,
					width: 100,
					resizable: true,
					align: 'right',
					itemId:'cam_anio2',					
					renderer:function(value,metadata,registro){                                
						 var aux = Ext.util.Format.number(value,'$0,000.00');
						if(value< 0) {
							return '<span style="color:red;">' + aux + '</span>';
						}else {
							return aux;
						}
					},
					editor: 
					{
						/*xtype: 'bigdecimal', 
						allowBlank: false,
						msgTarget: 'side',
						maxText:'El tama�o m�ximo del campos<br>es de 17 enteros 2 decimales',	
						maxValue: 		'99999999999999999.99',
						minValue: 		'0',								
						format:			'00000000000000000.00',
						readOnly: (solo_read=='S')?true:false,						
						margins		: '0 20 0 0'*/
						xtype: 'textfield',
						//maxLength:19,
						allowBlank: false,
						width: '50',
						regexText:"El tama�o m�ximo del campos es de 17 enteros 2 decimales",
						regex:/(^(((\d){1,17}))(\.\d{1,2})$)|(^((\d){1,17})$)|(^((-){1}(\d){1,17})$)|(^(((-){1}(\d){1,17}))(\.\d{1,2})$)/,
						maskRe: /[0-9\.\-]/,
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txt_aux_InfF')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txt_aux_InfF')[0].getValue();
								if(valAnterior!=fld.getValue()){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo_InfF')[0].setValue('S');
								}
							}
						}
					
					} 		
				},
				{
					header: '<center> </center>',
					dataIndex: 'ANIO_1',
					sortable: true,
					width: 150,
					resizable: true,					
					itemId:'cam_anio1',
					align: 'right',
					renderer:function(value,metadata,registro){                                
						 var aux = Ext.util.Format.number(value,'$0,000.00');
						if(value< 0) {
							return '<span style="color:red;">' + aux + '</span>';
						}else {
							return aux;
						}
					},
					editor: 
					{
						/*xtype: 'bigdecimal', 
						allowBlank: false,
						msgTarget: 'side',
						maxText:'El tama�o m�ximo del campos es de 17 enteros 2 decimales',	
						maxValue: 		'99999999999999999.99',
						minValue: 		'0',								
						format:			'00000000000000000.00',
						readOnly: (solo_read=='S')?true:false,						
						margins		: '0 20 0 0'*/
						xtype: 'textfield',
						//maxLength:19,
						allowBlank: false,
						width: '50',
						regexText:"El tama�o m�ximo del campos es de 17 enteros 2 decimales",
						regex:/(^(((\d){1,17}))(\.\d{1,2})$)|(^((\d){1,17})$)|(^((-){1}(\d){1,17})$)|(^(((-){1}(\d){1,17}))(\.\d{1,2})$)/,
						maskRe: /[0-9\.\-]/,
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txt_aux_InfF')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txt_aux_InfF')[0].getValue();
								if(valAnterior!=fld.getValue()){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo_InfF')[0].setValue('S');
								}
							}
						}
					
					} 				
				}
	

			]
		});
		
		var procesarCatalogo = function(store, arrRegistros, opts) {
		
			Ext.ComponentQuery.query('#txt_aux_Com')[0].setValue((mes-1)+'-'+anio);
			
			Ext.ComponentQuery.query('#anio_selec')[0].setValue(anio);
			Ext.ComponentQuery.query('#mes_selec')[0].setValue(mes);
			var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
			var mesyanio= meses[mes-1];
			if(mes!= '') {
			
				var anio_ant = anio;
				var colum1 = Ext.ComponentQuery.query('#cam_anio3')[0];
				colum1.setText('* '+(anio_ant-2));
				var colum2 = Ext.ComponentQuery.query('#cam_anio2')[0];
				colum2.setText('* '+(anio_ant-1));
				var colum3 = Ext.ComponentQuery.query('#cam_anio1')[0];
				colum3.setText(''+mesyanio+'-'+anio );
				Ext.ComponentQuery.query('#mes_selec')[0].setValue(mesyanio);
				mesAnio ='<center>Cifras en pesos a '+mesyanio+'-'+anio+' </center>' ;	
					
				
				Ext.ComponentQuery.query('#lblCifras')[0].update(mesAnio);				
				
				Ext.ComponentQuery.query('#mns_periodo')[0].setValue((anio_ant-2)+' y '+mesyanio+'-'+anio);
				inicio_combo = 1;
			}else  {				
				mesAnio ='<center>Cifras en pesos a  </center>' ;											
				Ext.ComponentQuery.query('#lblCifras')[0].update(mesAnio);
			}
			Ext.ComponentQuery.query('#fechaInfinanc')[0].setValue((mes-1)+'-'+anio);
										
		}
		var panelInfoFinanciera = new Ext.Panel({
				itemId: 'pnInfoFinanciera',
				frame: false,
				layout:'form',
				//margins: '0 18 0 0',
				//width:900,
				autoHeight:true,
				border	: false,	
				items:[
					{
						xtype:'textfield',
						itemId:'txt_aux_InfF',
						width: 100,
						hidden:true
					},
					{
						xtype:'textfield',
						itemId:'txt_aux_Com',
						width: 100,
						hidden:true
					},
					{
						xtype:'textfield',
						itemId:'guardar_activo_InfF',
						width: 100,
						value:'N',
						hidden:true
					},
					{
						xtype: 'fieldcontainer',
						combineErrors: false,
						msgTarget: 'side',
						width: 900,
						margins: '0 0 0 0',
						items: [
							{
								xtype:'label',
								style:  {  'font-size' : '12px'  },
								width: 900,
								html:'<br><center><b>La informaci�n que nos proporcione nos permitir� conocer, de manera general, su situaci�n financiera:</b></center><br>'
							}
						]
					},
					{
						xtype: 'hidden',
						itemId: 'anio_selec',
						name: 'anio_selec'
					},
					{
						xtype: 'hidden',
						itemId: 'mes_selec',
						name: 'mes_selec'
					},
					{
						xtype: 'hidden',
						itemId: 'mns_periodo',
						name: 'mns_periodo'
					},
					{
						xtype:'panel',
						frame: false,
						layout:'table',
						border	: false,
						style:'margin:0 auto;',
						width:800,
						bodyPadding: '0 250 0 200',
						items: [
							{
								xtype: 'combo',
								itemId:  'fechaInfinanc',
								fieldLabel: 'Fecha de informaci�n Financiera',
								name:  'fechaInfinanc',
								width: 350,
								forceSelection:true,
								style: 'margin:0 auto;',
								labelWidth: 200,
								mode:				'local',
								displayField:	'descripcion',
								valueField:		'clave',
								readOnly: (solo_read=='S')?true:false,
								typeAhead:		true,
								value:1,
								triggerAction:	'all',
								allowBlank: false,
								value:(mes-1)+'-'+anio,
								valorIni:mesyanio+'-'+anio,
								anchor:			'100%',
								store: Ext.create('Ext.data.Store', {
									model:'ModelCatalogo',
									itemId:'fechaInfFinan',
									autoLoad: true,
									proxy: {
										type: 'ajax',
										url: '39IfnbCalificate.data.jsp',
										reader: {
											type: 'json',
											root: 'registros'
										},
										extraParams: {
											informacion: 'fechaInformacionFinanciera'
										},
											totalProperty:  'total'
										},
										listeners: {
											load : procesarCatalogo,
											exception: NE.util.mostrarProxyAjaxError
										}
									}),
									listeners: {select: function(combo, record, index) {
											if(combo.getValue()!=''){
												var fecha = combo.getRawValue();
												var posicion = fecha.lastIndexOf('-');
												var anio  = fecha.substring(posicion+1);
												Ext.ComponentQuery.query('#anio_selec')[0].setValue(anio);
												var anio_ant = anio;
												var colum1 = Ext.ComponentQuery.query('#cam_anio3')[0];
												colum1.setText('* '+(anio_ant-2));
												var colum2 = Ext.ComponentQuery.query('#cam_anio2')[0];
												colum2.setText('* '+(anio_ant-1));
												var colum3 = Ext.ComponentQuery.query('#cam_anio1')[0];
												colum3.setText(''+combo.getRawValue() );
												mesAnio ='<center>Cifras en pesos a '+combo.getRawValue()+' </center>' ;											
												Ext.ComponentQuery.query('#lblCifras')[0].update(mesAnio);
												Ext.ComponentQuery.query('#mns_periodo')[0].setValue((anio_ant-2)+' y '+combo.getRawValue());
												//Ext.ComponentQuery.query('#txt_aux_InfF')[0].setValue((mes-1)+'-'+anio);
												
												
											}else{
												var colum1 = Ext.ComponentQuery.query('#cam_anio3')[0];
												colum1.setText('* ');
												var colum2 = Ext.ComponentQuery.query('#cam_anio2')[0];
												colum2.setText('* ');
												var colum3 = Ext.ComponentQuery.query('#cam_anio1')[0];
												colum3.setText('' );
												
											}
											if(inicio_combo==1){
												Ext.ComponentQuery.query('#btnGuardar')[0].disable();
												Ext.ComponentQuery.query('#guardar_activo_InfF')[0].setValue('N');
											}
											
										},
										change:function(obj, newValue, oldValue){
											if(inicio_combo==1){
												inicio_combo = 0;
											}
											if(oldValue!=undefined&&inicio_combo==1){
											
														Ext.ComponentQuery.query('#btnGuardar')[0].disable();
														Ext.ComponentQuery.query('#guardar_activo_InfF')[0].setValue('N');
											
											}else if(newValue!=undefined&&inicio_combo==0){
												if(newValue!=oldValue){
														Ext.ComponentQuery.query('#btnGuardar')[0].enable();
														Ext.ComponentQuery.query('#guardar_activo_InfF')[0].setValue('S');
														
													}
											}
																					
										}
									}
							}
						]
					},
					{
						xtype:'panel',
						frame: false,
						layout:'form',
						border	: false,
						style:'margin:0 auto;',
						width:900,
						bodyPadding: '0 50 0 20',
						items: [
							{
								xtype: 'fieldcontainer',
								combineErrors: false,
								msgTarget: 'side',
								//width: 750,
								items: [
									{
										xtype:'label',
										id: 'lblCifras',
										align: 'center',
										style: {  'font-size' : '12px' },
										fieldLabel: 'Cifras en pesos a',
										text: 'Cifras en pesos a'								
									}
								]
							}
						]
					},
					{
						xtype:'panel',
						frame: false,
						layout:'form',
						border	: false,
						style:'margin:0 auto;',
						width:800,
						bodyPadding: '0 100 0 120',
						align:'center',
						items: [
							
							{
								xtype: 'fieldcontainer',
								combineErrors: false,
								msgTarget: 'side',
								width: 520,
								items: [
									gridInformaFinan
								]
							}
							
						]
					},
					{
						xtype:'panel',
						frame: false,
						layout:'form',
						border	: false,
						style:'margin:0 auto;',
						width:800,
						bodyPadding: '0 100 0 150',
						//align:'center',
						items: [
							
							{
								xtype: 'fieldcontainer',
								combineErrors: false,
								msgTarget: 'side',
								width: 800,
								items: [
									{
										xtype:'label',
										style:
										 {
											  'font-size' : '12px'
										 },
										html:'<div > <table width="800" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><left>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Las cifras financieras de dos a�os anteriores deben estar dictaminados.</left></td></tr><tr> <td  ><left>**Favor de registrar las cifras financieras negativas con un signo negativo al inicio.</left></td></tr> </table> </div>'
									}
								]
							}
						]
					}
				]				
			});
		
		fp.add(panelInfoFinanciera);
		fp.doLayout();
		Ext.ComponentQuery.query('#mes_selec')[0].setValue(mesyanio);
		Ext.ComponentQuery.query('#anio_selec')[0].setValue(anio);
	}
});


// para el paso 4 Saldos
NE.Informacion.FormSaldos= Ext.extend(Ext.form.FormPanel,{
	objDatoInicial: null,
	campo_activo:'S',
	INFORMACION_FINANCIERA:null,
	initComponent : function(){
		 Ext.apply(this, {
			frame: false, 
			itemId:	'infoSaldosV',
			width: 945,
			border:true,
			height:550,
			bodyPadding: '10 10 10 10',
			autoHeight:true,
			monitorValid: true,
			autoScroll: false,
			listeners:{
				afterrender: function(obj){
					obj.generarCampos( obj);
				}
			}
		});
		
		NE.Informacion.FormSaldos.superclass.initComponent.call(this);
	},
	generarCampos: function(fp){
	
		var lstDatosIni = fp.objDatoInicial;
		var infoFina = fp.INFORMACION_FINANCIERA;
		var info = lstDatosIni[0];
		var infoFinanciera = infoFina[0];
		var solo_read =fp.campo_activo;
		var SALDO_VIGENTE_PM =  info.FG_SALDO_VIGENTE_PRINC_PM;
		var SALDO_VIGENTE_PF =  info.FG_SALDO_VIGENTE_PRINC_PF;
		var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		var mes1 = infoFinanciera.IG_MES_INF_FINANCIERA;
		var anio = infoFinanciera.IG_ANIO_INF_FINANCIERA;
		
		var  mesAnio ='Cifras en pesos a '+meses[mes1-1]+'-'+anio;
		Ext.define('ListaAcreditados',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_SOLICITUD'     },
				{name: 'CC_TIPO_ACREDITADO'     },
				{name: 'IC_SALDO_ACREDITADO'     },
				{name: 'CS_SALDO_ACREDITADO'     },
				{name: 'FG_SALDO'     }
			]
		});
		
		var consultaAcreditados = Ext.create('Ext.data.Store',{
			model: 'ListaAcreditados',
			storeId:'consultaAcreditados',
			itemId :'storeInfAcreditados',
			proxy: {
				type: 'ajax',
				url:  '39IfnbCalificate.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'Consulta_lista_acreditados',
					tipo_acreditado :'V',
					ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
					
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: true
		});
	var gridInforAcreditados = Ext.create('Ext.grid.Panel',{
			selType: 'cellmodel',
			plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
			itemId:         'gridInforAcreditados',
			store: Ext.data.StoreManager.lookup('consultaAcreditados'),
			width:          335,
			height:         270,
			margins: '0 20 0 0',
			columns: [
				{
					header: 'Acreditados Vigentes',
					tooltip: 'Acreditados Vigentes',
					dataIndex: 'CS_SALDO_ACREDITADO',					
					sortable: true,
					width: 130,			
					resizable: true,				
					align: 'center',					
					itemId:'cam_concepto'					
				},
				{
					header: 'Saldo',					
					dataIndex: 'FG_SALDO',						
					sortable: true,
					width: 200,
					resizable: true,					
					itemId:'saldoAcreditado',					
					align: 'right',
					renderer:function(value,metadata,registro){ 
						 var aux = Ext.util.Format.number(value,'$0,000.00');
						if(value< 0) {
							return '<span style="color:red;">' + aux + '</span>';
						}else {
							return aux;
						}
					},
					editor: 
					{
						/*xtype: 'bigdecimal', 																											
						maxText:'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
						maxValue: 		'99999999999999999.99',
						minValue: 		'0',								
						format:			'00000000000000000.00',
						allowBlank: false,
						msgTarget: 'side',
						readOnly: (solo_read=='S')?true:false,
						maxLength: 200,
						margins		: '0 20 0 0'*/
						
						xtype: 'textfield',
						//maxLength:19,
						allowBlank: false,
						width: '50',
						readOnly: (solo_read=='S')?true:false,
						regexText:"El tama�o m�ximo del campos es de 17 enteros 2 decimales",
						regex:/(^(((\d){1,17}))(\.\d{1,2})$)|(^((\d){1,17})$)|(^((-){1}(\d){1,17})$)|(^(((-){1}(\d){1,17}))(\.\d{1,2})$)/,
						maskRe: /[0-9\.\-]/,
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txt_aux_Sal')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txt_aux_Sal')[0].getValue();
								if(valAnterior!=fld.getValue()){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo_Sal')[0].setValue('S');
								}
							}
						}
					} 				
				}
	

			]
		});
		
		Ext.define('ListaIncumplidos',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_SOLICITUD'     },
				{name: 'CC_TIPO_ACREDITADO'     },
				{name: 'IC_SALDO_ACREDITADO'     },
				{name: 'CS_SALDO_ACREDITADO'     },
				{name: 'FG_SALDO'     }
			]
		});
		var consultaIncumplidos = Ext.create('Ext.data.Store',{
			model: 'ListaIncumplidos',
			storeId:'consultaIncumplidos',
			itemId :'storeInfIncumplidos',
			proxy: {
				type: 'ajax',
				url:  '39IfnbCalificate.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'Consulta_lista_acreditados',
					tipo_acreditado :'I',
					ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: true
		});
	
	var gridInforIncumplidos = Ext.create('Ext.grid.Panel',{
			selType: 'cellmodel',
			plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
			itemId:         'gridInforIncumplidos',
			store: Ext.data.StoreManager.lookup('consultaIncumplidos'),
			width:          405,
			height:         270,
			margins: '0 20 0 0',
			cls: 'customGridHeader',
			columns: [
				{
					header: 'Acreditados en incumplimiento de pago',						
					tooltip: 'Acreditados en<br>incumplimiento de<br>pago',
					dataIndex: 'CS_SALDO_ACREDITADO',
					sortable: true,
					width: 200,
					resizable: true,
					align: 'center'
				},
				{
					header: 'Saldo',											
					dataIndex: 'FG_SALDO',
					sortable: true,
					width: 200,
					resizable: true,										
					itemId:'saldoIncumplido',
					align: 'right',
					renderer:function(value,metadata,registro){ 
						 var aux = Ext.util.Format.number(value,'$0,000.00');
						if(value< 0) {
							return '<span style="color:red;">' + aux + '</span>';
						}else {
							return aux;
						}
					},
					editor: 
					{
						/*xtype: 'bigdecimal', 
						allowBlank: false,
						msgTarget: 'side',																													
						maxText:'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
						maxValue: 		'99999999999999999.99',
						minValue: 		'0',								
						format:			'00000000000000000.00',
						maxLength: 200,
						readOnly: (solo_read=='S')?true:false,						
						margins		: '0 20 0 0'*/
						
						xtype: 'textfield',
						//maxLength:19,
						allowBlank: false,
						width: '50',
						readOnly: (solo_read=='S')?true:false,
						regexText:"El tama�o m�ximo del campos es de 17 enteros 2 decimales",
						regex:/(^(((\d){1,17}))(\.\d{1,2})$)|(^((\d){1,17})$)|(^((-){1}(\d){1,17})$)|(^(((-){1}(\d){1,17}))(\.\d{1,2})$)/,
						maskRe: /[0-9\.\-]/,
						listeners: {
							focus: function(fld) {
								Ext.ComponentQuery.query('#txt_aux_Sal')[0].setValue(fld.getValue());
							},
							blur: function(fld, value){
								var valAnterior = Ext.ComponentQuery.query('#txt_aux_Sal')[0].getValue();
								if(valAnterior!=fld.getValue()){
									Ext.ComponentQuery.query('#btnGuardar')[0].enable();
									Ext.ComponentQuery.query('#guardar_activo_Sal')[0].setValue('S');
								}
							}
						}
					} 				
				}
	

			]
		});
		
		var panelInfoSaldos = new Ext.Panel({
				itemId: 'pnInfoSaldos',
				frame: false,
				layout:'form',
				width:900,
				autoHeight:true,
				border	: false,	
				margins: '0 20 0 0',
				items:[
					{
						xtype: 'fieldcontainer',
						combineErrors: false,
						msgTarget: 'side',
						width: 900,
						items: [
							{
								xtype:'label',
								style: { 'font-size' : '12px'  },
								width: 900,
								html:'<br><center><b>Esta informaci�n nos permitira conocer el grado de concentraci�n de su cartera.<br>'+
								'Favor de registrar el saldo de sus principales acreditados.</b></center><br>'
							}
						]
					},
					{
						xtype:'textfield',
						itemId:'txt_aux_Sal',
						width: 100,
						hidden:true
					},
					{
						xtype:'textfield',
						itemId:'guardar_activo_Sal',
						width: 100,
						value:'N',
						hidden:true
					},
					{
						 xtype:'panel',
						 frame: false,
						 border	: false,
						 width: 900,
						 layout:'form',
						 style:'margin:0 auto;',
						 bodyPadding: '5 10 0 350',						
						 items: [
							
							{
								xtype: 'fieldcontainer',
								combineErrors: false,
								msgTarget: 'side',
								items: [
									{
										xtype:'displayfield',
										itemId: 'lblCifrasSaldos',
										align: 'center',
										width : 200,
										//labelWidth: 200,
										style: {  'font-size' : '12px' },
										value: mesAnio							
									}
								]
							}
						]
					},
					
					{
						 xtype:'panel',
						 frame: false,
						 border	: false,
						 width: 900,
						 layout:'table',
						 //style:'margin:0 auto;',
						 bodyPadding: '10 10 12 45',						
						 layoutConfig:{ columns: 2 },
						 items: [
							
							gridInforAcreditados,
							{
								xtype:'displayfield',
								width: 40
							},
							gridInforIncumplidos
						]
					},
					
					{
						 xtype:'panel',
						 frame: false,
						 border	: false,
						 width: 900,
						 layout:'form',
						// bodyPadding: '10 200 0 80',
						 margins: '30 0 0 30',
						 items: [
							{
								xtype: 'fieldcontainer',
								combineErrors: false,
								msgTarget: 'side',
								items: [
									{
										xtype:'label',
										style:	 {	  'font-size' : '12px' },
										html:'<center>Saldo Vigente de la Principal Persona Moral acreditada con actividad Empresarial:</center>'
									}
								]
							},
							{
								 xtype:'panel',
								 frame: false,
								 border	: false,
								 width: 900,
								 layout:'form',
								 bodyPadding: '5 0 0 270',
								 margins: '10 0 10 0',
								 items: [
									{
										xtype: 'container',			
										layout: 'hbox', 
										items: [										
												{
													xtype: 'displayfield',													
													itemId: 'lblmontoPm',											
													align: 'center',
													width: 210,
													labelWidth: 200,
													style: {  'font-size' : '12px' },
													value: mesAnio	
												},				
												
												/*{
													xtype: 'displayfield',
													value: ' &nbsp;  &nbsp;',
													width: 10
												},	*/								
												{
														xtype: 'bigdecimal',
														itemId: 'montoPm',										
														name:  'montoPm',																				
														maxText:'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
														maxValue: 		'99999999999999999.99',
														minValue: 		'0',								
														format:			'00000000000000000.00',
														width: 150,
														readOnly: (solo_read=='S')?true:false,
														allowBlank: false,									
														//value: Ext.util.Format.usMoney(SALDO_VIGENTE_PM.replace(/[^0-9\.]/g, ''))
														value: SALDO_VIGENTE_PM,
														listeners: {
															focus: function(fld) {
																Ext.ComponentQuery.query('#txt_aux_Sal')[0].setValue(fld.getValue());
															},
															blur: function(fld, value){
																var valAnterior = Ext.ComponentQuery.query('#txt_aux_Sal')[0].getValue();
																if(valAnterior!=fld.getValue()){
																	Ext.ComponentQuery.query('#btnGuardar')[0].enable();
																	Ext.ComponentQuery.query('#guardar_activo_Sal')[0].setValue('S');
																}
															}
														}
																		
												}
											]
										}
									]
								}
					
							]	
					},
					{
						 xtype:'panel',
						 frame: false,
						 border	: false,
						 width: 900,
						 layout:'form',
						 //bodyPadding: '0 200 10 80',
						 margins: '30 0 30 0',
						 items: [
							{
								xtype: 'fieldcontainer',
								combineErrors: false,
								msgTarget: 'side',
								items: [
									{
										xtype:'label',
										style: {  'font-size' : '12px'},
										html:'<center>Saldo Vigente de la Principal Persona F�sica acreditada con actividad Empresaria:</center>'
									}
								]
							},
							{
								 xtype:'panel',
								 frame: false,
								 border	: false,
								 width: 900,
								 layout:'form',
								 bodyPadding: '5 0 0 270',
								 margins: '10 0 10 0',
								 items: [
									{
										xtype: 'container',			
										layout: 'hbox',
										items: [
											
											{
													xtype: 'displayfield',													
													itemId: 'lblmontoPf',											
													align: 'center',
													width: 210,
													labelWidth: 200,
													style: {  'font-size' : '12px' },
													value: mesAnio	
											},	
											/*{
												xtype: 'displayfield',
												value: ' &nbsp;  &nbsp;',
												width: 10
											},*/
											{
												xtype: 'bigdecimal',
												itemId: 'montoPf',																				
												fieldLabel: '',
												name:  'montoPf',										
												maxText:'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
												maxValue: 		'99999999999999999.99',
												minValue: 		'0',								
												format:			'00000000000000000.00',
												width: 150,
												readOnly: (solo_read=='S')?true:false,
												allowBlank: false,																			
												//value: Ext.util.Format.usMoney(SALDO_VIGENTE_PM.replace(/[^0-9\.]/g, ''))
												value: SALDO_VIGENTE_PF,
												listeners: {
															focus: function(fld) {
																Ext.ComponentQuery.query('#txt_aux_Sal')[0].setValue(fld.getValue());
															},
															blur: function(fld, value){
																var valAnterior = Ext.ComponentQuery.query('#txt_aux_Sal')[0].getValue();
																if(valAnterior!=fld.getValue()){
																	Ext.ComponentQuery.query('#btnGuardar')[0].enable();
																	Ext.ComponentQuery.query('#guardar_activo_Sal')[0].setValue('S');
																}
															}
														}
														
											}
										]
									}
								]
							}
						 ]
					}
			 ]
			});
		
		fp.add(panelInfoSaldos);
		fp.doLayout();
		
			
	
	}
});
// Resultado Indicaores Financieros
NE.Informacion.FormIndicador= Ext.extend(Ext.form.FormPanel,{
	objDatoInicial: null,
	anio: null,
	mes: null,
	initComponent : function(){
		 Ext.apply(this, {
			width: 945,
			height:400,
			itemId:	'IndicadorFinancieroV',
			frame: false,
			border: true, 
			monitorValid: true,
			bodyPadding: '0 0 0 10',
			//bodyStyle: 'padding-left:5px;,padding-top:10px;,padding:6px;,text-align:left',
			autoScroll: true,
			//style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side'/*,
				anchor: '-30'*/
			},
			listeners:{
				afterrender: function(obj){
					obj.generarCampos( obj);
				}
			}
		});
		
		NE.Informacion.FormIndicador.superclass.initComponent.call(this);
	},
	generarCampos: function(fp){
		var lstDatosIni = fp.objDatoInicial;
		var IC_ANIO1;
		var IC_ANIO2;
		var IC_ANIO3;
		
		if(lstDatosIni!=''){
			IC_ANIO1 = lstDatosIni[0].IC_ANIO1;
			IC_ANIO2 = lstDatosIni[1].IC_ANIO2;
			IC_ANIO3 = lstDatosIni[2].IC_ANIO3;
		}
		
		var anio_sele = fp.anio;
		var mes_sele =  fp.mes;
		
		
		
		var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		Ext.define('ListaIndicadorFinan',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'RAZON_FINANCIERA'     },
				{name: 'INDICADOR_CUMPLI'     },
				{name: 'ANIO_2'     },
				{name: 'ANIO_1'     },
				{name: 'ANIO_ELEGIDO'     },
				{name: 'ic_solicitud'     },
				{name: 'INDICADOR_2'     },
				{name: 'INDICADOR_1'     },
				{name: 'INDICADOR_S'     }
			]
		});
		Ext.define('ListaAnioDictaminado',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'CRITERIO'     },
				{name: 'INDICADOR_CUMPLIMIENTO'     },
				{name: 'RESULTADO'     },
				{name: 'INDICADOR'     }
			]
		});
	var procesarConsultaIndicador = function(store, arrRegistros, success, opts){
		var gridIndicador = Ext.ComponentQuery.query('#gridIndicador')[0];
		gridIndicador.el.unmask();
		//Ext.ComponentQuery.query('#btnActualizar')[0].setIconCls('icoActualizar');
		//Ext.ComponentQuery.query('#btnActualizar')[0].enable();
		if (arrRegistros != null){
			
			if(store.getTotalCount() > 0){
			} else{
				gridIndicador.getView().getEl().mask('No se encontraron registros');
			}
		}
	}	
	var procesarConsultaAnioDictaminado = function(store, arrRegistros, success, opts){
		var gridInfoFin = Ext.ComponentQuery.query('#gridAnioDictaminado')[0];
		gridInfoFin.el.unmask();
		//Ext.ComponentQuery.query('#btnActualizar')[0].setIconCls('icoActualizar');
		//Ext.ComponentQuery.query('#btnActualizar')[0].enable();
		if (arrRegistros != null){
			
			if(store.getTotalCount() > 0){
			} else{
				gridInfoFin.getView().getEl().mask('No se encontraron registros');
			}
		}
	}	
	var consultaIndicadorFinan = Ext.create('Ext.data.Store',{
			model: 'ListaIndicadorFinan',
			storeId:'consultaIndicadorFinan',
			itemId :'storeIndicador',
			proxy: {
				type: 'ajax',
				url:  '39IfnbCalificate.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'Consulta_Indicador_Financiero',
					ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
				
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: true,
			listeners: {
				load: procesarConsultaIndicador
			}
		});
		var consulAnioDictaminado = Ext.create('Ext.data.Store',{
			model: 'ListaAnioDictaminado',
			storeId:'consulAnioDictaminado',
			itemId :'storeIndicador',
			proxy: {
				type: 'ajax',
				url:  '39IfnbCalificate.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'Consulta_anio_dictaminado',
					ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
				
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: true,
			listeners: {
				load: procesarConsultaAnioDictaminado
			}
		});
	
	var gridIndicador=Ext.create('Ext.grid.Panel', {
		 store: Ext.data.StoreManager.lookup('consultaIndicadorFinan'),
		 plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		 xtype: 'cell-editing',
		 itemId:         'gridIndicador',
		 frame: true,
		 //border: true,
		 columns: [
			  { header: 'Raz�n Financiera',tooltip: 'Raz�n Financiera',dataIndex: 'RAZON_FINANCIERA',width: 150},
			  { header: '<center>Indicador de<br>cumplimiento</center>',tooltip: 'Indicador de<br>cumplimiento', dataIndex: 'INDICADOR_CUMPLI',width: 100},
			  { 
					header: IC_ANIO3,
					tooltip: IC_ANIO3, 
					dataIndex: 'ANIO_2',
					width: 100,
					xtype: 			'actioncolumn',
					align:'center',
					renderer:function(value,metadata,registro){                                
							return value;
					},
					items: [
						{
							getClass: function(value,metadata,record,rowIndex,colIndex,store){
								 if ( record.get('INDICADOR_2') == 'S' ) {
									return 'icoAceptar';
								 }else{
									return 'icoRechazar';
								 }
							}
						}
					]
			 },
			 { 
				header: IC_ANIO2,
				tooltip: IC_ANIO2,
				dataIndex: 'ANIO_1',
				width: 100,
				align:'center',
				xtype: 			'actioncolumn',
				renderer:function(value,metadata,registro){                                
							return value;
					},
					items: [
						{
							getClass: function(value,metadata,record,rowIndex,colIndex,store){
								 if ( record.get('INDICADOR_1') == 'S' ) {
									return 'icoAceptar';
								 }else{
									return 'icoRechazar';
								 }
							}
						}
					]
			},
			{
				header: IC_ANIO1,
				header:meses[mes_sele-1]+' - '+IC_ANIO1,
				tooltip: meses[mes_sele-1]+' - '+IC_ANIO1,
				dataIndex: 'ANIO_ELEGIDO',
				width: 100,
				align:'center',
				xtype: 			'actioncolumn',
				renderer:function(value,metadata,registro){                                
							return value;
					},
					items: [
						{
							getClass: function(value,metadata,record,rowIndex,colIndex,store){
								 if ( record.get('INDICADOR_S') == 'S' ) {
									return 'icoAceptar';
								 }else{
									return 'icoRechazar';
								 }
							}
						}
					]
			}
		 ],
		 height: 120,
		 width: 570
	});
	var gridAnioDictaminado=Ext.create('Ext.grid.Panel', {
		 store: Ext.data.StoreManager.lookup('consulAnioDictaminado'),
		 itemId:         'gridAnioDictaminado',
		 plugins:[
			Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})
		 ],
		 xtype: 'cell-editing',
		 frame: true,
		 columns: [
			  { 
					header: 'Criterio',tooltip: 'Raz�n Financiera',dataIndex: 'CRITERIO',width: 250},
			  { header: 'Indicador de cumplimiento',tooltip: 'Indicador de<br>cumplimiento', dataIndex: 'INDICADOR_CUMPLIMIENTO',width: 350},
			  { 
					header: 'Resultado '+meses[mes_sele-1]+' - '+anio_sele,
					tooltip: 'Resultado '+meses[mes_sele-1]+' - '+anio_sele,
					xtype: 			'actioncolumn',
					dataIndex: 'RESULTADO',
					width: 220,
					align:'center',
					renderer:function(value,metadata,record,rowIndex,colIndex,store){ 
						return Ext.util.Format.number(value,'0,0.00');
					},
					items: [
						{
							getClass: function(value,metadata,record,rowIndex,colIndex,store){
								 if ( record.get('INDICADOR') == 'S' ) {
									return 'icoAceptar';
								 }else if ( record.get('INDICADOR') == 'N' ) {
									return 'icoRechazar';
								 }
							}
						}
					]
				}
		 ],
		 height: 150,
		 width: 800
	});
		
		var panelIndicador = new Ext.Panel({
				itemId: 'pnIndicador',
				frame: false,
				layout:'form',
				width:900,
				border	: false,	
				items:[
					{
						xtype: 'fieldcontainer',
						combineErrors: false,
						msgTarget: 'side',
						width: 800,
						items: [
							{
								xtype:'label',
								style:
								 {
									  'font-size' : '12px'
								 },
								html:'<br><div > <table width="800" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td  ><center><b>Indicadores Financieros</b></center></td></tr> </table> </div><br>'
							}
						]
					},
					{
						xtype:'displayfield',
						width: 20,
						itemId:'valueBalance',
						name:'valueBalance',
						hidden :true,
						allowBlank: false
				
					},
					
					{
						xtype:'displayfield',
						width: 20,
						itemId:'valueEdoResultados',
						name:'valueEdoResultados',
						hidden :true,
						allowBlank: false
				
					},
					
					{
						xtype:'displayfield',
						width: 20,
						itemId:'valueRFC',
						name:'valueRFC',
						hidden :true,
						allowBlank: false
					},
					{
						xtype:'panel',
						frame: false,
						layout:'form',
						border	: false,
						bodyPadding: '0 0 10 170',
						width:900,
						items: [
							gridIndicador
						]
					},
					{
						xtype:'panel',
						frame: false,
						layout:'form',
						border	: false,
						bodyPadding: '0 0 0 50',
						width:900,
						items: [
							gridAnioDictaminado,
							{
								xtype: 'fieldcontainer',
								combineErrors: false,
								msgTarget: 'side',
								width:800,
								items: [
									{
										xtype:'label',
										style:
										 {
											  'font-size' : '12px'
										 },
										html:'<table width="800">'+
													' <tr>'+
														'<td height="30" ><p style=�text-align: justify;� >*El equivalente en Moneda Nacional del importe en Unidades de Inversi�n, considera el valor de la UDI al 31 de Diciembre del a�o inmediato anterior a esta solicitud.</p></td>'+
													'</tr>'+
												'</table>'
									}
								]
							}
						]
					}
				]				
			});
		
		fp.add(panelIndicador);
		fp.doLayout();
	
	}
});


