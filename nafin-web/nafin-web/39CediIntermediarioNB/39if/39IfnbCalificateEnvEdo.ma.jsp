 <%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*,  
	java.io.*,  
	java.text.*,  
	com.netro.exception.*,
	netropology.utilerias.*,
	netropology.utilerias.usuarios.*,
	javax.naming.*,
	org.apache.commons.logging.Log,
	org.apache.commons.fileupload.disk.*,
	org.apache.commons.fileupload.servlet.*,
	org.apache.commons.fileupload.*,	
	com.netro.cedi.*,	
	net.sf.json.JSONArray,	
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/39CediIntermediarioNB/39secsession_extjs.jspf"%>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
		
	String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
		
	System.out.println("nombre del archivo "+tipoArchivo);
	
	 
	String path_destino = strDirectorioTemp;	
	int  tamaniArch_1 =5060720,  tamanioMB =5, tamanio = 0;
	String mensaje ="",mensajeL ="", infoRegresar ="", itemArchivo = "", resultadoT ="N";
	ParametrosRequest req = null;
	boolean ok_file = true;
	String nombreArchivo = "";
	
	tamanioMB = 5; 
	tamaniArch_1 =5060720;
	mensajeL = "El tamaño del archivo excede el límite permitido que es de 5MB";
	
	JSONObject jsonObj = new JSONObject();
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));



	if (ServletFileUpload.isMultipartContent(request)) {
	System.out.println(" *************** >>>>>>>>>><< inicio ");
	try {	
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		factory.setRepository(new File(strDirectorioTemp));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// Set overall request size constraint
		upload.setSizeMax(tamanioMB * tamaniArch_1);	//MB
		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));		
		// Process the uploaded items
		FileItem item = (FileItem)req.getFiles().get(0);
		
		itemArchivo		= (String)item.getName();
		nombreArchivo= Comunes.cadenaAleatoria(16)+ ".pdf";
		tamanio			= (int)item.getSize();
		if(tamanio > tamaniArch_1) {					    
			mensaje= mensajeL;
			ok_file=false;
		}else  {
			path_destino = strDirectorioTemp+nombreArchivo;
			item.write(new File(path_destino));
			log.debug("path_destino ======"+path_destino);
		
		}
		if(tipoArchivo.equals("Balance")){
			jsonObj.put("nombreArchivo",nombreArchivo);
			jsonObj.put("tipoArchivo",tipoArchivo);
		}else if(tipoArchivo.equals("Edo_Resultado")){
			jsonObj.put("url",nombreArchivo);
			jsonObj.put("nombreArchivo",tipoArchivo);
		}else if(tipoArchivo.equals("RFC")){
			jsonObj.put("url",nombreArchivo);
			jsonObj.put("nombreArchivo",tipoArchivo);
		}
		System.out.println(" *************** >>>>>>>>>><< fin ");
	} catch(Exception e) {		
		mensaje= mensajeL;
		e.printStackTrace();  
		System.out.println("Error al actualizar archivo"+e);
  }
 	
   
	jsonObj.put("mensaje", mensaje);	
	infoRegresar =jsonObj.toString();  
  System.out.println("infoRegresar "+infoRegresar);
	
}

%>
<%=infoRegresar%>