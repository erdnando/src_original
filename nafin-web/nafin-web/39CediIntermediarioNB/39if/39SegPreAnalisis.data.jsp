<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*,  
	java.io.*,  
	java.text.*,  
	com.netro.exception.*,	
	netropology.utilerias.*,
	netropology.utilerias.usuarios.*,
	javax.naming.*,	
	org.apache.commons.logging.Log,
	com.netro.cedi.*,
	net.sf.json.JSONArray,
	org.apache.commons.logging.Log,
	org.apache.commons.fileupload.disk.*,
	org.apache.commons.fileupload.servlet.*,
	org.apache.commons.fileupload.*,	
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/39CediIntermediarioNB/39secsession_extjs.jspf"%>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
	String ic_doc_chekList = (request.getParameter("ic_doc_chekList")!=null)?request.getParameter("ic_doc_chekList"):"";
	String extension = (request.getParameter("extension")!=null)?request.getParameter("extension"):"";
	String estatus = (request.getParameter("estatus")!=null)?request.getParameter("estatus"):"";
	
	UtilUsr utilUsr = new UtilUsr();
	Usuario usuario = utilUsr.getUsuario(strLogin);
	String rfc_IFNB = usuario.getRfc();
	String nombreUsuario = strLogin +" - "+ usuario.getNombre()+" "+usuario.getApellidoPaterno()+" "+usuario.getApellidoMaterno();		
	
	log.debug("rfc_IFNB  "+rfc_IFNB); 
	
	String infoRegresar="", mensaje =  "",	acceso = "", consulta ="";
	JSONObject jsonObj = new JSONObject();
	
	ConsSegPreAnalisis paginador = new ConsSegPreAnalisis();		
	paginador.setRfc_IFNB(rfc_IFNB); 

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);	
  
  Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 

 String lbAyuda=  "<table width='600' align='CENTER' >"+ 
"<tr tr align='CENTER' ><td align='left'>"+
"<OL>"+
"	<LI><b>Antecedentes</b>"+ 
"		<UL type = disk> "+
"			<LI>Cuando se constituyó (Intermediario Financiero)</LI>"+
"			<LI>Hechos relevantes </LI>"+
"		</UL> "+
"	</LI>"+
"	<p>&nbsp;</p>"+
"	<LI><b>Descripción del negocio </b>"+
"		<UL type = disk> "+
"				<LI>Segmento de mercado que atiende (mercado objetivo)</LI>"+
"				<LI>No. de sucursales y ubicación </LI>"+
"				<LI>No. empleados </LI>"+
"				<LI>No. de clientes </LI>"+
"		</UL> "+
"	</LI>"+
"	<p>&nbsp;</p>"+
"	<LI><b>Estructura corporativa</b>"+
"		<UL type = disk> "+
"			<LI>Estructura accionaria y breve reseña acerca de los principales accionistas.</LI>"+
"			<LI>Integración de órganos de gobierno y breve reseña profesional de los miembros del consejo de administración (descartar experiencia en el sector financiero).</LI>"+
"		</UL>"+
"	</LI>"+
"	<p>&nbsp;</p>"+
"	<LI><b>Descripción de los productos que el Intermediario ofrece y del producto a fondear por NAFIN, así como el desglose de la cartera por Tipo de Producto. </b>"+
"		<UL type = disk> "+
"			<LI>Tipo de crédito </LI> "+
"			<LI>Destino </LI> "+
"			<LI>Plazo </LI> "+
"			<LI>Tasa de interés al acreditado </LI> "+
"			<LI>Monto máximo </LI> "+
"			<LI>Comisiones </LI> "+
"			<LI>Garantías </LI> "+
"		</UL>"+
"	</LI>"+
"	<p>&nbsp;</p>"+
"	<LI>	<b>Describir el proceso de Crédito de acuerdo con su manual de Crédito (detallar las siguientes etapas): </b>"+
"		<UL type = disk>  "+
"			<LI>Originación </LI> "+
"			<LI>Evaluación </LI> "+
"			<LI>Autorización </LI> "+
"			<LI>Formalización </LI> "+
"			<LI>Cobranza </LI> "+
"			<LI>Recuperación </LI>"+
"		</UL>"+ 
"	</LI>	"+
"	<p>&nbsp;</p>"+
"	<LI><b>Breve descripción del sistema de administración de cartera.</b></LI>	"+
"</OL>"+
"	<p>&nbsp;</p>"+
"</td></tr></table>";

if (informacion.equals("validaAcceso")){

	
	List datos  = paginador.getEstatus(rfc_IFNB );  //solicitudes con estatus 4, 5
	System.out.println("datos  "+datos);   		
	if(datos.size()>0){	
		estatus =  datos.get(0).toString();
		no_solicitud =  datos.get(1).toString();		
	}	
	 System.out.println("--------------------------------------------  ");  
	System.out.println("estatus  "+estatus);
	System.out.println("no_solicitud  "+no_solicitud);   
	System.out.println("--------------------------------------------  ");  
	
	if(estatus.equals("8")  || estatus.equals("9")){ 
		mensaje= "<br> </br> <table width='600' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED'> "+
					" Su solicitud no ha sido considerada viable para incorporarse a la red de Intermediarios Financieros de Nacional Financiera, ingrese al menú “Califícate” si desea realizar un nuevo registro."+
					" <br></br> Si desea mayor información favor de comunicarse al correo electrónico ​cedi@nafin.gob.mx o al teléfono 01 800 NAFINSA (01 800 623 4672).  "+
					" </td></tr></table>"; 	
		acceso="N";
	}else  if(estatus.equals("1") || estatus.equals("2") || estatus.equals("3")   || estatus.equals("7")  ){
		mensaje= "<br> </br> <table width='600' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED'> "+
				" Su solicitud aún no ha sido considerada viable o está siendo analizada por un Ejecutivo, por favor regrese al menú “Califícate” para concluir su registro o espere un correo de notificación para continuar con el proceso de viabilidad de incorporación a Nacional Financiera, S.N.C.  "+
				" </td></tr></table>"; 		
		acceso="N";   
	}else  if(estatus.equals("6")  ){
		mensaje= "<br> </br> <table width='600' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED'> "+
					" Su solicitud ha sido considerada viable para incorporarse a la red de Intermediarios Financieros de Nacional Financiera."+
					"<br></br>Si desea mayor información favor de comunicarse al correo electrónico cedi@nafin.gob.mx o al teléfono 01 800 NAFINSA (01 800 623 4672)"+
					" </td></tr></table>"; 	
		acceso="N";		
	}else  if(estatus.equals("4") || estatus.equals("5") ){
		acceso = "S";   
		mensaje= "<br> </br> <table width='600' align='CENTER' ><tr align='CENTER'><td align='CENTER'> "+
				" <b>Envío de documentación complementaria para pre-análisis.</b>"+			
				" </td></tr></table>"; 
	}
		String tabla_reg ="";	
	
	if(acceso.equals("S") ){
		HashMap  info =  paginador.getDatosIFNB(rfc_IFNB,  no_solicitud );
		
		tabla_reg = "<table width='590' align='JUSTIFIED' >"+
			" <tr align='LEFT'> "+
			" <td align='LEFT' width='190'>&nbsp; No. Solicitud </td>"+
			" <td align='LEFT' width='50' >&nbsp;</td>"+
			" <td align='LEFT' width='350'> "+info.get("NO_SOLICITUD").toString() +" </td>"+				
			" </tr>"+
			"  <tr align='LEFT'> "+
			" <td align='LEFT' width='190'>&nbsp; Razón social </td>"+
			" <td align='LEFT' width='50' >&nbsp;</td>"+
			" <td align='LEFT' width='350'> "+info.get("RAZON_SOCIAL").toString() +" </td>"+				
			" </tr>"+
			" <tr align='LEFT'> "+
			" <td align='LEFT' width='190'>&nbsp; R.F.C. </td>"+
			" <td align='LEFT' width='50' >&nbsp;</td>"+
			" <td align='LEFT' width='350'> "+info.get("RFC").toString() +" </td>"+				
			" </tr>"+
			" <tr align='LEFT'> "+
			" <td align='LEFT' width='190'>&nbsp; Destino de los recursos</td>"+
			" <td align='LEFT' width='50' >&nbsp;</td>"+
			" <td align='LEFT' width='350'> "+info.get("TIPO_DESTINO").toString() +" </td>"+				
			" </tr>"+
			" <tr align='LEFT'> "+
			" <td align='LEFT' width='190'>&nbsp; Fecha de registro</td>"+
			" <td align='LEFT' width='50' >&nbsp;</td>"+
			" <td align='LEFT' width='350'> "+info.get("FECHA_REGISTRO").toString() +" </td>"+				
			" </tr>"+
			" <tr align='LEFT'> "+
			" <td align='LEFT' width='190'>&nbsp; Usuario</td>"+
			" <td align='LEFT' width='50' >&nbsp;</td>"+
			" <td align='LEFT' width='350'> "+nombreUsuario +" </td>"+				
			" </tr>"+
			"</table>"; 
		}
	jsonObj = new JSONObject();	
	jsonObj.put("acceso", acceso);	  
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("lbMensaje", mensaje);
	jsonObj.put("lbAyuda", lbAyuda);
	jsonObj.put("estatus", estatus);
	jsonObj.put("tabla_reg", tabla_reg);
	infoRegresar = jsonObj.toString(); 

}else if (informacion.equals("consDoctos")){
	
	Registros reg	=	queryHelper.doSearch();	
	
	int totalPre=0;
	while (reg.next())	{	
		String longitud = (reg.getString("LONGITUD_PRE") == null) ? "0" : reg.getString("LONGITUD_PRE");		
		if(!longitud.equals("0")) {
			totalPre++;
		}
	}
	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);	
	jsonObj.put("totalPre", totalPre);
	infoRegresar = jsonObj.toString(); 
	
}else  if ( informacion.equals("descargaDoctosPre")  ){
	
	String nombreArchivo  =  	cediBean.desArchSolicPrevios(no_solicitud, strDirectorioTemp, ic_doc_chekList, extension );
  
	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 


}else  if ( informacion.equals("descargaDoctos")  ){
	
	String nombreArchivo  =  	cediBean.desArchSolicCheckList(no_solicitud, strDirectorioTemp, ic_doc_chekList, extension );
  
	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 

}else  if ( informacion.equals("descargaAyuda")  ){
 
		String  nombreArchivo =""; 
	
	 if(ic_doc_chekList.equals("1"))  {
		nombreArchivo = " ";	 
	 }else  if(ic_doc_chekList.equals("2"))  {		
		nombreArchivo = " ";	 
	 }else  if(ic_doc_chekList.equals("3"))  {
		nombreArchivo = "Desglose de pasivos bancarios.xls";
	}else  if(ic_doc_chekList.equals("4"))  {
		nombreArchivo = "Antigüedad de la cartera vencida.xlsx";	
	}else  if(ic_doc_chekList.equals("5")  &&  extension.equals("pdf"))  {
		nombreArchivo = "Portafolio de la cartera.pdf";
	}else  if(ic_doc_chekList.equals("5")  &&  extension.equals("xls")) {
		nombreArchivo = "Portafolio de la cartera.xls";
	}
	
	System.out.println("strDirecVirtualTrabajo  "+strDirecVirtualTrabajo); 
	
	
	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTrabajo+nombreArchivo);
	infoRegresar = jsonObj.toString(); 


}else  if ( informacion.equals("EnviarArchivo")  ){	
		
	String doc_chekList[] = request.getParameterValues("doc_chekList");	
		
	boolean valor =   cediBean.enviarArchivo(no_solicitud, doc_chekList, estatus, strLogin );	
	
	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(valor));	
	infoRegresar = jsonObj.toString(); 

}


%>

<%= infoRegresar%>

