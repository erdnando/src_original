Ext.onReady(function(){

	var globallbAyuda;
		
	var procesaValidaAcceso = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {

			var  jsonData = Ext.JSON.decode(response.responseText);
				
				if(jsonData.acceso=='N')  {
					Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
					Ext.ComponentQuery.query('#fpCedula')[0].hide();	
					
				}else  if(jsonData.acceso=='S')  {
					Ext.ComponentQuery.query('#gridConsulta')[0].show();	
					Ext.ComponentQuery.query('#fpCedula')[0].show();
					
					consultas();				
				}
					var mensajeCont = Ext.ComponentQuery.query('#mensajeCont')[0];
					mensajeCont.query('#lbMensaje')[0].update(jsonData.lbMensaje);	
					Ext.ComponentQuery.query('#mensajeCont')[0].show();
				
					globallbAyuda= jsonData.lbAyuda;
					var  fpCedula =   Ext.ComponentQuery.query('#fpCedula')[0];
					fpCedula.query('#estatus')[0].setValue(jsonData.estatus);
					
				
				   Ext.ComponentQuery.query('#tabla_reg')[0].update(jsonData.tabla_reg);	  
				
	
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
		
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();							
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var descargaDoctosPrevio = function (grid,rowIndex,colIndex,item,event){
	
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		var ic_doc_chekList = registro.get('IC_CHECKLIST');
		var extension = registro.get('EXTENSION_PRE');
		
		if(registro.get('LONGITUD_PRE')!='0' &&  registro.get('VISTA_PREVIA')=='N'  ){
			Ext.Ajax.request({
				url: '39SegPreAnalisis.data.jsp',
				params: {		
					informacion:'descargaDoctosPre',
					no_solicitud:no_solicitud,				
					ic_doc_chekList:ic_doc_chekList, 
					extension: extension
				},
				callback: procesarDescargaArchivos
			});	
		}
	}
	
	
	
	var descargaDoctos = function (grid,rowIndex,colIndex,item,event){
	
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		var ic_doc_chekList = registro.get('IC_CHECKLIST');
		var extension = registro.get('EXTENSION');
		if(registro.get('LONGITUD')!='0' &&  registro.get('FECHA_ULTIMO_ENVIO')!='' ){
			Ext.Ajax.request({
				url: '39SegPreAnalisis.data.jsp',
				params: {		
					informacion:'descargaDoctos',
					no_solicitud:no_solicitud,				
					ic_doc_chekList:ic_doc_chekList, 
					extension: extension
				},
				callback: procesarDescargaArchivos
			});	
		}
	}
	
	//************************************Carga de Archivo******************************************
	
	var no_solicitud;
	var doc_chekList =  [];

	var cancelar =  function() {  
		no_solicitud;
		doc_chekList =  [];
	}
		
	var  procesaEnviodeArchivos = function(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.JSON.decode(response.responseText);
		
			Ext.Msg.alert(	'Aviso',	'<center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Archivos enviados exitosamente.</center>',	function(btn, text){
				consultas();									
			});
			
		} else {						
			NE.util.mostrarErrorPeticion(response); 
		}
	}
	
	var procesarEnviar = function() {
		var gridConsulta =  Ext.ComponentQuery.query('#gridConsulta')[0];		
		var store = gridConsulta.getStore();	
		var  total =0;
		
		var  fpCedula =   Ext.ComponentQuery.query('#fpCedula')[0];
		var estatus = fpCedula.query('#estatus')[0].getValue();
					
		cancelar(); //limpia las variables
		store.each(function(record){		
			if(record.get('SELECCIONAR')== true  &&  record.data['LONGITUD_PRE']!='0' ){
				no_solicitud = record.get('NO_SOLICITUD');
				doc_chekList.push(record.get('IC_CHECKLIST'));
				total++;											
			}								
								
		});
		if(total==0){
			Ext.Msg.alert(	'Aviso',	'<center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seleccione al menos un documento</center>');
			return;
		}else if(total>0){
		
			Ext.Ajax.request({
				url: '39SegPreAnalisis.data.jsp',
				params: 	{
					informacion: 'EnviarArchivo',
					no_solicitud:no_solicitud,
					doc_chekList:doc_chekList,
					estatus:estatus
				},
				callback: procesaEnviodeArchivos
			});		
		}
			
	}
	
	var enviarArchivo = function() {
		
		var fpCargaArchivo = Ext.ComponentQuery.query('#fpCargaArchivo')[0];
					
		var archivo = fpCargaArchivo.query('#archivoCarga')[0];
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}
		
		var  fpCedula =   Ext.ComponentQuery.query('#fpCedula')[0];
		no_solicitud= fpCedula.query('#no_solicitud')[0].getValue();
		ic_doc_chekList = fpCedula.query('#ic_doc_chekList')[0].getValue(); 
		ic_fijo = fpCedula.query('#ic_fijo')[0].getValue(); 
				
		var  parametros = "no_solicitud="+no_solicitud+"&ic_doc_chekList="+ic_doc_chekList+"&ic_fijo="+ic_fijo;
		
		fpCargaArchivo.getForm().submit({
			url: '39SegPreAnalisis.ma.jsp?'+parametros,			
			waitMsg: 'Enviando datos...',
			waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;
				if(resp.mensaje!=''){
					Ext.Msg.alert(	'Mensaje','<center>'+resp.mensaje+'</center>');					
				}else  {	
					Ext.Msg.alert(	'Aviso',	'<center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El archivo se carg� con �xito.</center>',	function(btn, text){
					Ext.ComponentQuery.query('#VentanaCarga')[0].destroy();
					consultas();									
					});
				}
			}
			,failure: 	function(form, action){ 
				var resp = action.result;	
				var mensaje = resp.mensaje;
				if(resp.mensaje!=''){
					//Ext.Msg.alert(	'Mensaje','<center>'+resp.mensaje+'</center>');
					alert(resp.mensaje);
				}			
			}			
		});
		
	
	}
	
	var CargaArchivoIFNB= function (grid,rowIndex,colIndex,item,event){
	
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		var ic_doc_chekList = registro.get('IC_CHECKLIST');
		var ic_fijo = registro.get('FIJO');
				
		var  fpCedula =   Ext.ComponentQuery.query('#fpCedula')[0];
		fpCedula.query('#no_solicitud')[0].setValue(no_solicitud);
		fpCedula.query('#ic_doc_chekList')[0].setValue(ic_doc_chekList); 
		fpCedula.query('#ic_fijo')[0].setValue(ic_fijo); 
		
		new Ext.Window({
			modal: true,
			width: 610,
			resizable: false,
			closable:true,
			id: 'VentanaCarga',
			autoDestroy:false,
			closeAction: 'destroy',
			items: [
				fpCargaArchivo
			]
		}).show().setTitle('');
	}
	
	
	 Ext.apply(Ext.form.field.VTypes, {		 	
        archformato: function(val, field) {			
			var myRegex = /^.+\.(?:(?:[dD][oO][cC][xX]?)|(?:[pP][pP][tT][xX]?)|(?:[xX][lL][sS][xX]?)|(?:[pP][dD][fF]?)|(?:[cC][sS][vV]?))$/;			
			var  fpCedula =   Ext.ComponentQuery.query('#fpCedula')[0];
			var doc_check = fpCedula.query('#ic_doc_chekList')[0].getValue(); 
		
			if(doc_check==1){//Manual de Cr�dito				
				myRegex = /^.+\.(?:(?:[dD][oO][cC][xX]?)|(?:[pP][dD][fF]?))$/;				
			}else  if(doc_check==2){//Presentaci�n Corporativa.				
				myRegex = /^.+\.(?:(?:[pP][pP][tT][xX]?)|(?:[pP][dD][fF]?))$/;
				
			}else  if(doc_check==3   || doc_check==4   ){//Desglose de pasivos bancarios  y Antig�edad de la cartera vencida				
				myRegex = /^.+\.(?:(?:[xX][lL][sS][xX]?))$/;	
			}else  if(doc_check==5){ //7Portafolio de la Cartera.				
				myRegex = /^.+\.(?:(?:[xX][lL][sS][xX]?)|(?:[cC][sS][vV]?))$/;
			}			
			return myRegex.test(val);
		 
        }
		 ,archformatoText 	: 'El formato del archivo de origen no es el correcto.'
		
    });
	
	
	var fpCargaArchivo = {
		xtype: 'form',
		id:'fpCargaArchivo',	
		width: 600,		
		frame: true,	
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 120,
		defaultType: 'textfield',
		defaults: { msgTarget: 'side', anchor: '-20' },	
		monitorValid: true,
		items: [	
			{
				xtype: 'fileuploadfield',
				id: 'archivoCarga',
				width: 150,	  
				emptyText: 'Seleccionar Archivo',
				fieldLabel: 'Seleccionar Archivo',
				name: 'archivoCarga',
				buttonText: 'Examinar',						
				anchor: '90%',
				vtype: 'archformato'				
			},
			{
				xtype:  'label',         
				width:	'100%',
				itemId: 'lbExtension',	
				fieldLabel: '',				
				text: '',
				listeners: {
					render:function(){
						
						var myRegex = /^.+\.(?:(?:[dD][oO][cC][xX]?)|(?:[pP][pP][tT][xX]?)|(?:[xX][lL][sS][xX]?)|(?:[pP][dD][fF]?)|(?:[cC][sS][vV]?))$/;
				
						var  fpCedula =   Ext.ComponentQuery.query('#fpCedula')[0];
						var doc_check = fpCedula.query('#ic_doc_chekList')[0].getValue(); 
						var  tipoArch = '.pdf, .xlsx, .docx, .pptx o .csv';
						
						var  fpCargaArchivo =   Ext.ComponentQuery.query('#fpCargaArchivo')[0];
						fpCargaArchivo.query('#lbExtension')[0].update('<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* El documento debe de ser de formato '+tipoArch+'</i>');	
				
						if(doc_check==1){//Manual de Cr�dito
							tipoArch = '.pdf o .docx';						
							fpCargaArchivo.query('#lbExtension')[0].update('<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* El documento debe de ser de formato '+tipoArch+'</i>');	
						
					
						}else  if(doc_check==2){//Presentaci�n Corporativa.						
							tipoArch = '.pdf o .pptx';						
							fpCargaArchivo.query('#lbExtension')[0].update('<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* El documento debe de ser de formato '+tipoArch+'</i>');	
						
							
						}else  if(doc_check==3   || doc_check==4   ){//Desglose de pasivos bancarios  y Antig�edad de la cartera vencida						
							tipoArch = 'xlsx';						
							fpCargaArchivo.query('#lbExtension')[0].update('<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* El documento debe de ser de formato '+tipoArch+'</i>');	
						
						
						}else  if(doc_check==5){ //7Portafolio de la Cartera. 						 
							 tipoArch = '.xlsx o .csv';						
							 fpCargaArchivo.query('#lbExtension')[0].update('<i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* El documento debe de ser de formato '+tipoArch);	
						}
					}
				}
			}	
		],
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivo				
			}
		]		 
		
	};
	
	
	//** VENTANA DE AYUDA 
	var VentanaAyuda = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		
		if(registro.get('IC_CHECKLIST')==2) {
		
			new Ext.Window({
				modal: true,
				width: 700,
				resizable: false,
				closable:true,
				id: 'VentanaAyuda',
				autoDestroy:false,
				closeAction: 'destroy',
				items: [			
						{
							xtype:  'label',
							itemId: 'lbAyuda',						
							style:  'text-align:left;margin:15px;color:black;',
							html: globallbAyuda
						}		
				]			
			}).show().setTitle('Presentaci�n Corporativa');			
		}
	}

	//********************+GRID QUE MUESTRA LOS ARCHIVOS A CARGAR **********************
	
	var procesarConsultar = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  fpCedula =   Ext.ComponentQuery.query('#fpCedula')[0];
			fpCedula.el.unmask();			
			
			Ext.ComponentQuery.query('#gridConsulta')[0].show();
			
			if(json.totalPre==0) {
				Ext.ComponentQuery.query('#btnEnviar')[0].disable();	
			}else if(json.totalPre!=0) {
				Ext.ComponentQuery.query('#btnEnviar')[0].enable();	
			}
			if(store.getTotalCount() > 0) {				
			   Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(true);
			}else  {				
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(false);
			}

		}
	};
	
	Ext.define('LisDoctos', {
		extend: 'Ext.data.Model',
		fields: [						
			{ name: 'IC_CHECKLIST'},
			{ name: 'NO_SOLICITUD'},
			{ name: 'NOMBRE_DOCTO'},
			{ name: 'LONGITUD'},
			{ name: 'EXTENSION'},
			{ name: 'LONGITUD_PRE'},
			{ name: 'EXTENSION_PRE'},
			{ name: 'FECHA_ULTIMO_ENVIO'},
			{ name: 'VISTA_PREVIA'},
			{ name: 'SELECCIONAR'},
			{ name: 'FIJO'}			
		 ]
	});


	var consDoctosData = Ext.create('Ext.data.Store', {
		model: 'LisDoctos',
		proxy: {
			type: 'ajax',
			url: '39SegPreAnalisis.data.jsp',
			reader: 		 { type: 'json',  root: 'registros' 	},
			extraParams: { informacion: 'consDoctos' 	},
			listeners:   { exception: NE.util.mostrarProxyAjaxError 	}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultar
		}
	});
	
	
	var  gridConsulta = Ext.create('Ext.grid.Panel',{	
	   plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridConsulta',
		store: consDoctosData, 				 		
      title: 'Documentos Solicitados', 
      frame: true,
		hidden: true,
		style: 'margin: 10px auto 0px auto;',		
		xtype: 'cell-editing',		
		columns: [
			 {
				xtype: 'checkcolumn',
            header: 'Selec.',
            dataIndex: 'SELECCIONAR',
            width: 40,	
				sortable: true,
				resizable: false,
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					var cssPrefix = Ext.baseCSSPrefix,
					cls = cssPrefix + 'grid-checkcolumn';
				  if(  record.data['SELECCIONAR'] ==false)  { 
						if(record.data['LONGITUD_PRE']!='0' &&  record.data['VISTA_PREVIA']=='N'  ){							
							cls = cssPrefix + 'grid-checkcolumn';															
						}else {							
							metaData.tdCls += ' ' + this.disabledCls;								
						}	
				  }else  if(  record.data['SELECCIONAR'] ==true)  { 
					 cls += ' ' + cssPrefix + 'grid-checkcolumn-checked';							 
				  }
					return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>'; 
				}
			},
			{
				header: 'Nombre del Documento',
				dataIndex: 'NOMBRE_DOCTO',
				sortable: true,
				width: 230,
				resizable: false,
				align: 'left'
         },		
			{
				xtype		: 'actioncolumn',
				header: 'Ayuda',
				tooltip: 'Ayuda',				
				sortable: true,
				width: 100,			
				resizable: false,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){					
							if(record.get('IC_CHECKLIST') ==2) {							
								return 'icoAyuda';
							}
						}
						,handler: VentanaAyuda
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){					
							if(record.get('IC_CHECKLIST') ==3 || record.get('IC_CHECKLIST') ==4  || record.get('IC_CHECKLIST') ==5 ) {
								return 'icoXls';
							}
						}
						,handler: function(grid,rowIndex,colIndex,item,event){	
						
							var registro = grid.getStore().getAt(rowIndex);
							var ic_doc_chekList = registro.get('IC_CHECKLIST');
							
							if(ic_doc_chekList ==3 || ic_doc_chekList ==4  || ic_doc_chekList ==5 ) {						
								Ext.Ajax.request({
									url: '39SegPreAnalisis.data.jsp',
									params: {		
										informacion:'descargaAyuda',						
										ic_doc_chekList:ic_doc_chekList,
										extension:'xls'
									},
									callback: procesarDescargaArchivos
								});	
							}
						}
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){					
							if(record.get('IC_CHECKLIST') ==5) {								
								return 'icoPdf';
							}
						}						
						,handler: function(grid,rowIndex,colIndex,item,event){	
						
							var registro = grid.getStore().getAt(rowIndex);
							var ic_doc_chekList = registro.get('IC_CHECKLIST');
							if(ic_doc_chekList ==5) {
								Ext.Ajax.request({
									url: '39SegPreAnalisis.data.jsp',
									params: {		
										informacion:'descargaAyuda',						
										ic_doc_chekList:ic_doc_chekList,
										extension:'pdf'
									},
									callback: procesarDescargaArchivos
								});	
							}
						}
					}	
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Carga Archivo',
				tooltip: 'Carga Archivo',				
				sortable: true,
				width: 100,			
				resizable: false,				
				align: 'center', 
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							return 'autorizar';
						}
						,handler: CargaArchivoIFNB
					}					
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Vista previa del <BR>archivo a enviar',
				tooltip: 'Vista previa del archivo a enviar',				
				sortable: true,
				width: 120,			
				resizable: false,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							var icono;
							if(record.data['LONGITUD_PRE']!='0' &&  record.data['VISTA_PREVIA']=='N'  ){
								if(record.data['EXTENSION_PRE']=='pdf' ){   icono='icoPdf'; }
								if(record.data['EXTENSION_PRE']=='xls' || record.data['EXTENSION_PRE']=='xlsx'  || record.data['EXTENSION_PRE']=='csv'  ){   icono='icoXls'; }
								if(record.data['EXTENSION_PRE']=='doc' || record.data['EXTENSION_PRE']=='docx' ){   icono='icoDoc'; }
								if(record.data['EXTENSION_PRE']=='ppt'  || record.data['EXTENSION_PRE']=='pptx'   ){   icono='icoPpt'; }								
								return icono; 
							}
						}
						,handler: descargaDoctosPrevio
					}				
				]
			},
			{
				xtype		: 'actioncolumn',
				header: '�ltimo archivo <br>enviado',
				tooltip: '�ltimo archivo enviado',				
				sortable: true,
				width: 100,			
				resizable: false,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							var icono;
							if(record.data['LONGITUD']!='0' &&  record.data['FECHA_ULTIMO_ENVIO']!='' ){
								if(record.data['EXTENSION']=='pdf'){   icono='icoPdf'; }
								if(record.data['EXTENSION']=='xls' || record.data['EXTENSION']=='xlsx'  ||  record.data['EXTENSION']=='csv' ){   icono='icoXls'; }
								if(record.data['EXTENSION']=='doc' || record.data['EXTENSION']=='docx' ){   icono='icoDoc'; }
								if(record.data['EXTENSION']=='ppt' ||  record.data['EXTENSION']=='pptx' ){   icono='icoPpt'; }
								return icono; 
							}
						}
						//,handler: descargaDoctos
						,handler: function() {
							descargaDoctos('S');
						}
					}							
				]
			},
			{
				header: 'Fecha de �ltimo envio',
				dataIndex: 'FECHA_ULTIMO_ENVIO',
				sortable: true,
				width: 150,
				resizable: false,
				align: 'center'
         }
			
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		width: 850,
		bbar: [
			'->','-',			
			{
				text: 'Enviar',
				id: 'btnEnviar',
				iconCls: 'aceptar',					
				handler: procesarEnviar				
			}
		]
		
	});
	
	//******* Informaci�n del Intermediario No Bancario*********************
	Ext.define('LisIFNB', {
		extend: 'Ext.data.Model',
		fields: [						
			{ name: 'DESCRIPCION'},
			{ name: 'VALOR'}			
		 ]
	});

	
	var elementosCedula =[
		{
			xtype: 'panel',
			frame: false,
			layout:'form',
			border	: false,
			width:590,
			items: [
				{
					xtype: 'fieldcontainer',
					combineErrors: false,
					msgTarget: 'side',
					width:590,
					items: [
						{
							xtype:'label',
							style:  'font-weight:bold;text-align:center;margin:15px;color:black;',							
							html:'<table width="580" >'+
									' <tr><td><b>Para poder continuar con su proceso de incorporaci�n es necesario que nos env�e la siguiente documentaci�n:</b></td>'+
									'</tr></table>'
						}
					]
				}
			]
		},
		{
			xtype: 'panel',
			frame: false,
			layout:'form',
			border	: false,
			width:590,
			items: [
				{
					xtype: 'fieldcontainer',
					combineErrors: false,
					msgTarget: 'side',
					width:590,
					items: [
						{
							xtype:'label',
							itemId: 'tabla_reg',							
							html:''
						}
					]
				}
			]
		},		
		{
			xtype: 'panel',
			frame: false,
			layout:'form',
			border	: false,
			width:690,
			style:  'text-align:JUSTIFIED;',
			items: [
				{
					xtype: 'fieldcontainer',
					combineErrors: false,
					msgTarget: 'side',
					width: 680,
					style:  'text-align:JUSTIFIED;',
					items: [
						{
							xtype:'label',							
							style:  'text-align:JUSTIFIED;',								
							html:'<table width="580" align="JUSTIFIED">'+
										' <tr align="JUSTIFIED" ><td align="JUSTIFIED"> &nbsp; <b>El env�o de esta informaci�n es para complementar el ejercicio de viabilidad para incorporarse a la red de Intermediarios Financieros de NAFINSA, por lo que no es una autorizaci�n de cr�dito, ni un monto de l�nea por otorgar. La solicitud requiere an�lisis experto y autorizaci�n del Comit� correspondiente en NAFINSA.</b></td>'+
										'</tr></table>'
						}
					]
				}
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'no_solicitud', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'ic_doc_chekList', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'ic_fijo', 	value: '' }	,
		{ 	xtype: 'textfield',  hidden: true, id: 'estatus', 	value: '' }	
	];
	
	
	var fpCedula = new Ext.form.FormPanel({
		itemId		: 'fpCedula',	
		layout		: 'form',
		width			: 600,
		height: 'auto',		
		style			: ' margin:0 auto;',
		frame			: true,
		hidden		: true,		
		collapsible	: false,
		titleCollapse	: false	,
		labelWidth	: 200,		
		defaults		: { msgTarget: 'side',anchor: '-20' },
		items: elementosCedula
	});
	
	///*****componenetes de Mensajes a mostrar de acuerdo a los estatus en los que se encuentra la solicitud
	var mensajeCont = Ext.create('Ext.container.Container', {
		width:	600,
		heigth:	'auto',
		bodyPadding: '30 50 12 50',
		style: 'margin:0 auto;',
		hidden:true,
		itemId: 'mensajeCont',
		items: [
			{
				xtype:  'label',         
				width:	'100%',
				itemId: 'lbMensaje',		
				fieldLabel: '',
				text: ''				
			}			
		]
	});
	
	

	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 920,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [		
			NE.util.getEspaciador(20),
			mensajeCont	,
			NE.util.getEspaciador(20),
			fpCedula, 			
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
			
		]
	});
	
	
	
	Ext.Ajax.request({
		url: '39SegPreAnalisis.data.jsp',
		params: {		
			informacion:'validaAcceso'
		},
		callback: procesaValidaAcceso
	});	
	
	
	var consultas = function() {	
		consDoctosData.load({ });	
	
	}
	
	
});
