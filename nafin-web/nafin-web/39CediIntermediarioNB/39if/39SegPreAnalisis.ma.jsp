<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*,  
	java.io.*,  
	java.text.*,  
	com.netro.exception.*,
	netropology.utilerias.*,
	netropology.utilerias.usuarios.*,
	javax.naming.*,
	org.apache.commons.logging.Log,
	org.apache.commons.fileupload.disk.*,
	org.apache.commons.fileupload.servlet.*,
	org.apache.commons.fileupload.*,	
	com.netro.cedi.*,	
	net.sf.json.JSONArray,	
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/39CediIntermediarioNB/39secsession_extjs.jspf"%>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%   
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
	String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
	String ic_doc_chekList = (request.getParameter("ic_doc_chekList")!=null)?request.getParameter("ic_doc_chekList"):"";
	String ic_fijo = (request.getParameter("ic_fijo")!=null)?request.getParameter("ic_fijo"):"";
		
	Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 
	 
	String path_destino = strDirectorioTemp;	
	int  tamaniArch_1 =5060720,  tamanioMB =5, tamanio = 0;
	String mensaje ="",mensajeL ="", infoRegresar ="", itemArchivo = "", resultadoT ="N";
	ParametrosRequest req = null;
	boolean ok_file = true;
	boolean valor = true;
	// 1 Manual de Crédito
	// 2 Presentación Corporativa.
	// 3 Desglose de pasivos bancarios
	// 4 Antigüedad de la cartera vencida
	// 5 Portafolio de la Cartera.
	if(ic_fijo.equals("S"))  {
		if(ic_doc_chekList.equals("1") ||  ic_doc_chekList.equals("2")  ||  ic_doc_chekList.equals("5")  ) {  
			tamanioMB = 20; 
			tamaniArch_1 =20242880;
			mensajeL = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El tamaño del archivo excede el límite <br> permitido que es de 20MB.";
		}else if(ic_doc_chekList.equals("3") ||  ic_doc_chekList.equals("4")  ) {  
			tamanioMB = 5; 
			tamaniArch_1 =5060720;
			mensajeL = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El tamaño del archivo excede el límite <br> permitido que es de 5MB";
		}
	}else  {  // Valida el tamaño para los archivos que no son fijos 
		tamanioMB = 20; 
		tamaniArch_1 =20242880;		
		mensajeL = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El tamaño del archivo excede el límite <br> permitido que es de 20MB.";
	} 
		
	JSONObject jsonObj = new JSONObject();

	if (ServletFileUpload.isMultipartContent(request)) {

	try {	
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		factory.setRepository(new File(strDirectorioTemp));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// Set overall request size constraint
		upload.setSizeMax(tamanioMB * tamaniArch_1);	//MB
		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));		
		// Process the uploaded items
		FileItem item = (FileItem)req.getFiles().get(0);
		
		itemArchivo		= (String)item.getName();
		tamanio			= (int)item.getSize();
		
		int  top  = itemArchivo.length();
		int  pos  = itemArchivo.indexOf(".");		
		String extension=itemArchivo.substring(pos, top);
		String nombreArchivo= Comunes.cadenaAleatoria(16)+extension;
		
		log.debug("nombreArchivo ="+nombreArchivo+"  tamanio ="+tamanio+   "tamaniArch_1 ="+tamaniArch_1);
				
		if(tamanio > tamaniArch_1) {					    
			mensaje= mensajeL;
			ok_file=false;
		}else  {
			path_destino = strDirectorioTemp+nombreArchivo;
			item.write(new File(path_destino));			
			valor = cediBean.guardarArchivoSolic( no_solicitud,   ic_doc_chekList ,  path_destino  );
			
		}
		 
	} catch(Exception e) {		
		mensaje= mensajeL;
		valor =false;
		e.printStackTrace();  
		System.out.println("Error al actualizar archivo"+e);
  }

	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
	
   jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(valor));
	jsonObj.put("mensaje", mensaje);	
	infoRegresar =jsonObj.toString();
}
log.debug( infoRegresar); 

%>
<%=infoRegresar%>
