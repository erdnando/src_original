Ext.onReady(function(){
	var fpDG = null;
	var fpIC = null;
	var fpCredVinc = null;
	var fpInfFinanciera = null; 
	var fpSaldos = null;
	var fpIndicadorF = null;
	
	var varGlobAcre = function  (){
		listSaldoAcre = [];
		listSaldoA= [];
	}
	var varGlobInc = function  (){
		listSaldoAcreI = [];
		listSaldoI= [];
	}
	var varGlob = function  (){
		listAnioInfFinan = [];
		listAnio_1 = [];
		listAnio_2 = [];
		listCampo= [];
	}
	var campo_activo = null;
	
	var estatus_sol_cargaIni = 1;
	var solicitud_viable = '';
	var estatus_sol = 1;
	var ban_doctos_enviados = '';
	
	
	var cedula_activa = false;
	var ban_actualizar ='';
	var click_finalizar = 'N';
	var activa_acualizar = '';
	
	var existe_inf_p1 = 0;
	var existe_inf_p2 = 0;
	var existe_inf_p3 = 0;
	var existe_inf_p4 = 0;
	var existe_inf_p5 = 0;
	
	var btn_continuar = 'N';
	var pestania_activa = '';
	
	Ext.apply(Ext.form.VTypes, {
		archivopdf: function(v) {
			var myRegex = /^.+\.([Pp][dD][Ff])$/;
			return myRegex.test(v);
		},
		archivopdfText: 'El formato del archivo de origen no es el correcto. Formato Soportado: PDF.'
	});
	
	function procesaGeneraArchivoCedula(opts, success, response){

		// DEFINO QUE ICONO SE MOSTRAR� EN EL BOT�N QUE GENER� LA DESCARGA
		var aplicaBtn = opts.params.aplica_btn // Me dice si la petici�n viene de un bot�n o de una columna
		console.log('aplicaBtn: ' + aplicaBtn);
		if(aplicaBtn == ''){
			var icono = '';
			var boton = Ext.getCmp(opts.params._botonId);
			var tipoArchivo = opts.params.tipo
			if(tipoArchivo == 'PDF'){
				icono = 'icoPdf';
			} else{
				icono = 'icoXls';
			}
		}
		// PARSEAR RESPUESTA DEL SERVIDOR
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			if(aplicaBtn == ''){ // Si la petici�n viene de un bot�n
				boton.setIconCls(icono);
				boton.enable();
			} else{ // Si la petici�n viene de una columna del grid
				main.el.unmask();
			}
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:  'input',
					type: 'hidden',
					id:   'nombreArchivo',
					name: 'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {
			if(resp.mensaje == ''){
				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
			} else{
				Ext.Msg.alert('Mensaje', resp.mensaje); // El campo es nulo y no se pudo descargar el archivo
			}

		}

		if(aplicaBtn == ''){ // Si la petici�n viene de un bot�n
			boton.setIconCls(icono);
			boton.enable();
		} else{ // Si la petici�n viene de una columna del grid
			main.el.unmask();
		}

	}
	
	function procesaGeneraPDF(opts, success, response){
		// PARSEAR RESPUESTA DEL SERVIDOR
		Ext.ComponentQuery.query('#btnGerarPDF')[0].setIconCls('icoPdf');
		Ext.ComponentQuery.query('#btnGerarPDF')[0].enable();
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			if(aplicaBtn == ''){ // Si la petici�n viene de un bot�n
				boton.setIconCls(icono);
				boton.enable();
			} else{ // Si la petici�n viene de una columna del grid
				main.el.unmask();
			}
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:  'input',
					type: 'hidden',
					id:   'nombreArchivo',
					name: 'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {
			if(resp.mensaje == ''){
				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
			} else{
				Ext.Msg.alert('Mensaje', resp.mensaje); // El campo es nulo y no se pudo descargar el archivo
			}

		}

	}
	
	// Descarga los archivos: Balance, Resultados, RFC
	function descargaArchivoSolic(tipo_archivo){
		Ext.Ajax.request({
			url: '39IfnbCalificate.data.jsp',
			params: Ext.apply({
				informacion:  'Descarga_Archivos_Solicitud',
				tipo_archivo: tipo_archivo, 
				rutaEdoResultados: Ext.ComponentQuery.query('#rutaEdoResultados')[0].getValue(),
				rutaBalance:Ext.ComponentQuery.query('#rutaBalance')[0].getValue(),
				rutaRFC:Ext.ComponentQuery.query('#rutaRFC')[0].getValue()
			}),
			callback: procesaGeneraArchivoCedula
		});
	}
	
	// Funci�n para colocar componenes en solo lectura
	function soloLectura(activo, pestania){
	
		if(pestania=='datosGrales'&&fpDG!=null){
			Ext.ComponentQuery.query('#txtRazon')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#txtNomCom')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#txtRFC')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#txtDomi')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#cmbEdo')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#txtColonia')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#cmbDeleg')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#txtCP')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#txtNomContacto')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#txtEmailEmp')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#txtTel')[0].setReadOnly(activo);
		}else if(pestania=='infCualitativa'&&fpIC!=null){
			var arrayInfCualitativa = Ext.ComponentQuery.query('#infCualitativaV panel[identificador=renglonRespuesta]');
			for(var r=0; r<arrayInfCualitativa.length;r++){
				var pregunta = arrayInfCualitativa[r].numRespuesta;
				var opcion = arrayInfCualitativa[r].numOpcion;
				var tipo_respuesta = arrayInfCualitativa[r].tipo_resp;
				if(Number(tipo_respuesta)==2){
					var ic_opcion_resp 	= arrayInfCualitativa[r].query('#'+pregunta+'ic_opcion_resp'+opcion);
					var addCajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'addCajaTexto'+opcion);
					var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
					ic_opcion_resp[0].setReadOnly(activo);
					addCajaTexto[0].setReadOnly(activo);
					cajaTexto[0].setReadOnly(activo);
				}else if(Number(tipo_respuesta)==1){
					var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
					cajaTexto[0].setReadOnly(activo);
				}else if(Number(tipo_respuesta)==3){
					var ic_opcion_resp 	= arrayInfCualitativa[r].query('#'+pregunta+'ic_opcion_resp');
					var addCajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'addCajaTexto'+opcion);
					var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
					ic_opcion_resp[0].setReadOnly(activo);
					addCajaTexto[0].setReadOnly(activo);
					cajaTexto[0].setReadOnly(activo);
				}
				
			}
		}else if(pestania=='ceditoVinculado'&&fpCredVinc!=null){
			Ext.ComponentQuery.query('#tieneCredVinc')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#montoCredVinc')[0].setReadOnly(activo);
			
		}else if(pestania=='infoFinanciera'&&fpInfFinanciera!=null){
			var gridInfoFin = Ext.ComponentQuery.query('#gridInformaFinan')[0];
			gridInfoFin.setDisabled(activo);
			Ext.ComponentQuery.query('#fechaInfinanc')[0].setReadOnly(activo);
			
		}else if(pestania=='infoSaldos'&&fpSaldos!=null){
		
			var gridInforAcreditados = Ext.ComponentQuery.query('#gridInforAcreditados')[0];
			gridInforAcreditados.setDisabled(activo);
			var gridInforIncumplidos = Ext.ComponentQuery.query('#gridInforIncumplidos')[0];
			gridInforIncumplidos.setDisabled(activo);
			Ext.ComponentQuery.query('#montoPm')[0].setReadOnly(activo);
			Ext.ComponentQuery.query('#montoPf')[0].setReadOnly(activo);
		}
		
		
		
	}
	
	
	// Funci�n para colocar componenes en solo lectura
	function limpiarPestania(pestania){
		if(pestania=='infCualitativa'){
			var arrayInfCualitativa = Ext.ComponentQuery.query('#infCualitativa panel[identificador=renglonRespuesta]');
			for(var r=0; r<arrayInfCualitativa.length;r++){
				var pregunta = arrayInfCualitativa[r].numRespuesta;
				var opcion = arrayInfCualitativa[r].numOpcion;
				var tipo_respuesta = arrayInfCualitativa[r].tipo_resp;
				if(Number(tipo_respuesta)==2){
					var ic_opcion_resp 	= arrayInfCualitativa[r].query('#'+pregunta+'ic_opcion_resp'+opcion);
					var addCajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'addCajaTexto'+opcion);
					var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
					ic_opcion_resp[0].setValue('');
					addCajaTexto[0].setValue('');
					cajaTexto[0].setValue('');
				}else if(Number(tipo_respuesta)==1){
					var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
					cajaTexto[0].setValue('');
				}else if(Number(tipo_respuesta)==3){
					var ic_opcion_resp 	= arrayInfCualitativa[r].query('#'+pregunta+'ic_opcion_resp');
					var addCajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'addCajaTexto'+opcion);
					var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
					ic_opcion_resp[0].setValue(false);
					addCajaTexto[0].setValue('');
					cajaTexto[0].setValue('');
				}
				
			}
		}else if(pestania=='ceditoVinculado'){
			Ext.ComponentQuery.query('#tieneCredVinc')[0].setValue('');
			Ext.ComponentQuery.query('#montoCredVinc')[0].setValue('');
		}else if(pestania=='infoFinanciera'){
			Ext.ComponentQuery.query('#fechaInfinanc')[0].setValue('');
			var gridInfoFin = Ext.ComponentQuery.query('#gridInformaFinan')[0];
			var store = gridInfoFin.getStore();
			store.each(function(r){
				r.set('ANIO_1','');
				r.set('ANIO_2','');
				r.set('ANIO_3','');
			});
			store.commitChanges();
			Ext.ComponentQuery.query('#cam_anio3')[0].setText('* ');
			Ext.ComponentQuery.query('#cam_anio2')[0].setText('* ');
			Ext.ComponentQuery.query('#cam_anio1')[0].setText('* ');
		}else if(pestania=='infoSaldos'){
			var gridInforAcreditados = Ext.ComponentQuery.query('#gridInforAcreditados')[0];
			var store = gridInforAcreditados.getStore();
			store.each(function(r){
				r.set('FG_SALDO','');
			});
			store.commitChanges();
			var gridInforIncumplidos = Ext.ComponentQuery.query('#gridInforIncumplidos')[0];
			var storeInfor = gridInforIncumplidos.getStore();
			storeInfor.each(function(r){
				r.set('FG_SALDO','');
			});
			storeInfor.commitChanges();
			Ext.ComponentQuery.query('#montoPm')[0].setValue('');
			Ext.ComponentQuery.query('#montoPf')[0].setValue('');
		}
		
	}
	function deshabilitaPreguntas(){
			var arrayInfCualitativa = Ext.ComponentQuery.query('#infCualitativa panel[identificador=renglonRespuesta]');
			for(var r=0; r<arrayInfCualitativa.length;r++){
				var pregunta = arrayInfCualitativa[r].numRespuesta;
				var opcion = arrayInfCualitativa[r].numOpcion;
				var tipo_respuesta = arrayInfCualitativa[r].tipo_resp;
				
				if(Number(tipo_respuesta)==3){
					var radioGroup 	= arrayInfCualitativa[r].query('#'+pregunta+'radioGroup'+opcion);
					var ic_opcion_resp 	= arrayInfCualitativa[r].query('#'+pregunta+'ic_opcion_resp');
					var PREG_EXC  = radioGroup[0].PREG_EXC ;
					if(PREG_EXC!=''&&ic_opcion_resp[0].getValue()==false){
						var index = (PREG_EXC).search("-");
						var rang1=0;
						var rang2=0;
						if(index != -1){
															
							rang1 = (PREG_EXC).substring(0,index);
							rang2 = (PREG_EXC).substring(index+1);
							rang1=rang1;
							rang2=rang2;
							for(var w=rang1; w<=rang2; w++){
								var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
								Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
								respPrev[0].setDisabled(true);//pnlOpcPreg
							}
						}else{
							rang1 = PREG_EXC;
							var w=rang1;
							var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
							Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
							respPrev[0].setDisabled(true);
						}
					}
				}else if(Number(tipo_respuesta)==2){
					var ic_opcion_resp 	= arrayInfCualitativa[r].query('#'+pregunta+'ic_opcion_resp'+opcion);
					var addCajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'addCajaTexto'+opcion);
					var cajaTexto 	= arrayInfCualitativa[r].query('#'+pregunta+'cajaTexto'+opcion);
					var PREG_EXC  = ic_opcion_resp[0].PREG_EXC ;
					
					if(PREG_EXC!=''&&ic_opcion_resp[0].getValue()==false){
						
							var index = (PREG_EXC).search("-");
							var rang1=0;
							var rang2=0;
							if(index != -1){
																
								rang1 = (PREG_EXC).substring(0,index);
								rang2 = (PREG_EXC).substring(index+1);
								rang1=rang1;
								rang2=rang2;
								for(var w=rang1; w<=rang2; w++){
									var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
									Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
									respPrev[0].setDisabled(true);//pnlOpcPreg
								}
							}else{
								rang1 = PREG_EXC;
								var w=rang1;
								var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
								Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
								respPrev[0].setDisabled(true);
							}
					}
				}
				
			}
			
		
	}
	function validaPreguntaObligatoria(){
			var arrayInfCualitativa = Ext.ComponentQuery.query('#infCualitativa panel[identificador=renglonRespuesta]');
			var arrayPreguntas = Ext.ComponentQuery.query('#infCualitativa panel[identificador_preg=renglonPreguntas]');
			var resultado = true;
			for(var i=0; i<arrayInfCualitativa.length;i++){
				var pregunta = arrayInfCualitativa[i].numRespuesta;
				var opcion = arrayInfCualitativa[i].numOpcion;
				var tipo_respuesta = arrayInfCualitativa[i].tipo_resp;
				var pregExcluyante = Ext.ComponentQuery.query('#'+pregunta+'pregExcluyante')[0];
				var cont = 0;
				var ban_obligatoria = false;
				var rang1=0;
				var rang2=0;
				var existe_rango = 0;
				
				for(var j=0; j<arrayInfCualitativa.length;j++){
					var pregunta1 = arrayInfCualitativa[j].numRespuesta;
					var opcion1 = arrayInfCualitativa[j].numOpcion;
					var tipo_respuesta1 = arrayInfCualitativa[j].tipo_resp;
					if(pregunta==pregunta1){
						if(Number(tipo_respuesta1)==2){
							var ic_opcion_resp 	= arrayInfCualitativa[j].query('#'+pregunta+'ic_opcion_resp'+opcion1);
							
							if(ic_opcion_resp[0].getValue()==true){
								ban_obligatoria = true;
								var exclu_check = ic_opcion_resp[0].PREG_EXC;
								
								if(exclu_check!=''){
									var index = (exclu_check).search("-");
									
									if(index != -1){
										existe_rango = 1;
										rang1 = (exclu_check).substring(0,index);
										rang2 = (exclu_check).substring(index+1);
										rang1=rang1;
										rang2=rang2;
										for(var w=rang1; w<=rang2; w++){
											Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('N');
										}
									}else{
										rang1 = exclu_check;
										Ext.ComponentQuery.query('#'+rang1+'pregExcluyante')[0].setValue('N');
									}
								}
							}else if(ic_opcion_resp[0].getValue()==false){
							
								var exclu_check = ic_opcion_resp[0].PREG_EXC;
								
								if(exclu_check!=''){
									var index = (exclu_check).search("-");
									
									if(index != -1){
										existe_rango = 1;
										rang1 = (exclu_check).substring(0,index);
										rang2 = (exclu_check).substring(index+1);
										rang1=rang1;
										rang2=rang2;
										for(var w=rang1; w<=rang2; w++){
											Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
										}
									}else{
										rang1 = exclu_check;
										Ext.ComponentQuery.query('#'+rang1+'pregExcluyante')[0].setValue('S');
									}
								}
								
							}
						}else if(Number(tipo_respuesta1)==1){
							var cajaTexto 	= arrayInfCualitativa[j].query('#'+pregunta+'cajaTexto'+opcion1);
							if(cajaTexto[0].getValue()!=''){
								ban_obligatoria = true;
							}
						}else if(Number(tipo_respuesta1)==3){
							var ic_opcion_resp 	= arrayInfCualitativa[j].query('#'+pregunta+'ic_opcion_resp');
							
							if(ic_opcion_resp[0].getValue()==true){
								ban_obligatoria = true;
							}
							
						}
						cont++;
					}
				}
				if(pregExcluyante.getValue()=='S'&&ban_obligatoria==false){
					ban_obligatoria = true;
				}
				if(ban_obligatoria==false){
					Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Verificar si todas las preguntas est�n seleccionadas</center></td><td></td></table> </div>');
					resultado=false;
					return;
				}
				i = i+(cont-1);
			}
		
		return resultado;
	}
	function mostrarBotones(pestania, estatus, cargar,existe_rechazo,valor_dictamen,cedula_activa,ban_actualizar,activa_acualizar,click_finalizar){
		
		if(cargar=='S'||((estatus==2&&cedula_activa==false)||estatus==3 ||estatus==4||estatus==5)){
			Ext.ComponentQuery.query('#btnGerarPDF')[0].hide();
			Ext.ComponentQuery.query('#btnSalir')[0].hide();
			Ext.ComponentQuery.query('#btnCancelar')[0].hide();
			Ext.ComponentQuery.query('#btnGuardar')[0].hide();
			Ext.ComponentQuery.query('#btnLimpiar')[0].hide();
			Ext.ComponentQuery.query('#btnContinuar')[0].hide();
			Ext.ComponentQuery.query('#btnRegresar')[0].hide();
			Ext.ComponentQuery.query('#btnMensaje')[0].hide();
			Ext.ComponentQuery.query('#btnAceptar')[0].hide();
			Ext.ComponentQuery.query('#btnActualizar')[0].hide();
			Ext.ComponentQuery.query('#btnVerCedRechazo')[0].hide();
			
			Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].hide();
			Ext.ComponentQuery.query('#btnAceptar2v')[0].hide();
			if(pestania=='IndicadorFinanciero'){
				if(((estatus==2&&cedula_activa==false)||estatus==3 ||estatus==4||estatus==5)){
					Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].setText("Ver C�dula de Aceptaci�n");
					Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].show();	
				}else{
					Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].hide();
				}
			}else{
				Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].hide();
			}
		}else{
			if(cedula_activa==true){
				Ext.ComponentQuery.query('#btnGerarPDF')[0].show();
				Ext.ComponentQuery.query('#btnSalir')[0].show();
				tp.child('#ceditoVinculado').tab.setDisabled(false);
				Ext.ComponentQuery.query('#btnCancelar')[0].hide();
				Ext.ComponentQuery.query('#btnGuardar')[0].hide();
				Ext.ComponentQuery.query('#btnLimpiar')[0].hide();
				Ext.ComponentQuery.query('#btnContinuar')[0].hide();
				Ext.ComponentQuery.query('#btnRegresar')[0].hide();
				Ext.ComponentQuery.query('#btnMensaje')[0].hide();
				Ext.ComponentQuery.query('#btnAceptar')[0].hide();
				Ext.ComponentQuery.query('#btnActualizar')[0].hide();
				Ext.ComponentQuery.query('#btnVerCedRechazo')[0].hide();
				Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].hide();	
				Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].hide();
				Ext.ComponentQuery.query('#btnAceptar2v')[0].hide();
			}else { 
				
				if(pestania!='IndicadorFinanciero'){
					if(pestania!='datosGrales'){
						Ext.ComponentQuery.query('#btnRegresar')[0].show();
					}else{
						Ext.ComponentQuery.query('#btnRegresar')[0].hide();
					}
					if(pestania=='infoSaldos' ){
						Ext.ComponentQuery.query('#btnContinuar')[0].setText("Finalizar");
					}else{
						Ext.ComponentQuery.query('#btnContinuar')[0].setText("Continuar");
					}
					Ext.ComponentQuery.query('#btnCancelar')[0].show();
					Ext.ComponentQuery.query('#btnGuardar')[0].show();
					Ext.ComponentQuery.query('#btnGuardar')[0].setDisabled(true);
					Ext.ComponentQuery.query('#btnLimpiar')[0].show();
					Ext.ComponentQuery.query('#btnContinuar')[0].show();
					Ext.ComponentQuery.query('#btnMensaje')[0].hide();
					Ext.ComponentQuery.query('#btnAceptar')[0].hide();
					Ext.ComponentQuery.query('#btnActualizar')[0].hide();
					Ext.ComponentQuery.query('#btnVerCedRechazo')[0].hide();
					Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].hide();	
					Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].hide();
					Ext.ComponentQuery.query('#btnAceptar2v')[0].hide();
				}else{
					if(existe_rechazo=='N'){
						if(valor_dictamen=='S'){
							Ext.ComponentQuery.query('#btnMensaje')[0].setValue('<div > <table width="600" border="0" align="left" cellpadding="0" cellspacing="0"><tr> <td ><p style="text-align:center;  font-size:15px; color:red ">Por favor env�e los Estados Financieros a la brevedad para concluir con el registro de su solicitud</p></td></table> </div>');
							Ext.ComponentQuery.query('#btnMensaje')[0].show();
							Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].show();
								
							Ext.ComponentQuery.query('#btnCancelar')[0].hide();
							Ext.ComponentQuery.query('#btnGuardar')[0].hide();
							Ext.ComponentQuery.query('#btnLimpiar')[0].hide();
							Ext.ComponentQuery.query('#btnContinuar')[0].hide();
							Ext.ComponentQuery.query('#btnRegresar')[0].hide();
							Ext.ComponentQuery.query('#btnActualizar')[0].hide();
							Ext.ComponentQuery.query('#btnVerCedRechazo')[0].hide();
							Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].hide();	
							Ext.ComponentQuery.query('#btnAceptar')[0].hide();
							Ext.ComponentQuery.query('#btnAceptar2v')[0].hide();
								
						}else if(valor_dictamen=='N'){
							Ext.ComponentQuery.query('#btnMensaje')[0].setValue('<div > <table width="600" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><p style="text-align:center;  font-size:15px; color:red ">Por favor de clic en �Aceptar� para concluir correctamente con el registro de su solicitud</p></td></table> </div>');
							Ext.ComponentQuery.query('#btnMensaje')[0].show();
							Ext.ComponentQuery.query('#btnAceptar')[0].show();
							Ext.ComponentQuery.query('#btnAceptar')[0].setDisabled(false);
							
							Ext.ComponentQuery.query('#btnCancelar')[0].hide();
							Ext.ComponentQuery.query('#btnGuardar')[0].hide();
							Ext.ComponentQuery.query('#btnLimpiar')[0].hide();
							Ext.ComponentQuery.query('#btnContinuar')[0].hide();
							Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].hide();
							Ext.ComponentQuery.query('#btnRegresar')[0].hide();
							Ext.ComponentQuery.query('#btnVerCedRechazo')[0].hide();
							Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].hide();	
							Ext.ComponentQuery.query('#btnActualizar')[0].hide();
							Ext.ComponentQuery.query('#btnAceptar2v')[0].hide();
						}
					}else if(existe_rechazo=='S'){
						if(valor_dictamen=='S'){
							Ext.ComponentQuery.query('#btnMensaje')[0].setValue('<div > <table width="600" border="0" align="left" cellpadding="0" cellspacing="0"><tr> <td ><p style="text-align:center;  font-size:15px; color:red ">Por favor env�e los Estados Financieros a la brevedad para concluir con el registro de su solicitud</p></td></table> </div>');
							Ext.ComponentQuery.query('#btnMensaje')[0].show();
							Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].show();
							Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].show();
							
							Ext.ComponentQuery.query('#btnCancelar')[0].hide();
							Ext.ComponentQuery.query('#btnGuardar')[0].hide();
							Ext.ComponentQuery.query('#btnLimpiar')[0].hide();
							Ext.ComponentQuery.query('#btnContinuar')[0].hide();
							Ext.ComponentQuery.query('#btnRegresar')[0].hide();
							Ext.ComponentQuery.query('#btnActualizar')[0].hide();
							Ext.ComponentQuery.query('#btnVerCedRechazo')[0].hide();
							Ext.ComponentQuery.query('#btnAceptar')[0].hide();
							Ext.ComponentQuery.query('#btnAceptar2v')[0].hide();
						}else if(valor_dictamen=='N'){
							if(click_finalizar=='S'){
								Ext.ComponentQuery.query('#btnMensaje')[0].setValue('<div > <table width="600" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><p style="text-align:center;  font-size:15px; color:red ">Por favor de clic en �Aceptar� para concluir correctamente con el registro de su solicitud</p></td></table> </div>');
								Ext.ComponentQuery.query('#btnMensaje')[0].show();
								Ext.ComponentQuery.query('#btnAceptar')[0].show();
								Ext.ComponentQuery.query('#btnAceptar')[0].setDisabled(false);
								
								Ext.ComponentQuery.query('#btnCancelar')[0].hide();
								Ext.ComponentQuery.query('#btnGuardar')[0].hide();
								Ext.ComponentQuery.query('#btnLimpiar')[0].hide();
								Ext.ComponentQuery.query('#btnContinuar')[0].hide();
								Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].hide();
								Ext.ComponentQuery.query('#btnRegresar')[0].hide();
								Ext.ComponentQuery.query('#btnVerCedRechazo')[0].hide();
								Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].hide();	
								Ext.ComponentQuery.query('#btnActualizar')[0].hide();
								Ext.ComponentQuery.query('#btnAceptar2v')[0].hide();	
							}else {
								
								
								Ext.ComponentQuery.query('#btnMensaje')[0].setValue('<div > <table width="600" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><p style="text-align:center;  font-size:15px; color:red ">Por favor de clic en �Aceptar� para concluir correctamente con el registro de su solicitud</p></td></table> </div>');
								Ext.ComponentQuery.query('#btnMensaje')[0].show();
								
								Ext.ComponentQuery.query('#btnAceptar2v')[0].show();
								Ext.ComponentQuery.query('#btnActualizar')[0].show();
								if(ban_actualizar=='S'){
									Ext.ComponentQuery.query('#btnAceptar2v')[0].setDisabled(false);
									Ext.ComponentQuery.query('#btnActualizar')[0].setDisabled(true);
									
								}else{
									Ext.ComponentQuery.query('#btnAceptar2v')[0].setDisabled(true);
									if(activa_acualizar=='S'){
										Ext.ComponentQuery.query('#btnActualizar')[0].setDisabled(false);
									}else{
										Ext.ComponentQuery.query('#btnActualizar')[0].setDisabled(true);
									}
								}
								
								
								
								Ext.ComponentQuery.query('#btnVerCedRechazo')[0].show();
								Ext.ComponentQuery.query('#btnVerCedRechazo2v')[0].hide();	
								Ext.ComponentQuery.query('#btnCancelar')[0].hide();
								Ext.ComponentQuery.query('#btnGuardar')[0].hide();
								Ext.ComponentQuery.query('#btnLimpiar')[0].hide();
								Ext.ComponentQuery.query('#btnAceptar')[0].hide();
								Ext.ComponentQuery.query('#btnContinuar')[0].hide();
								Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].hide();
								Ext.ComponentQuery.query('#btnRegresar')[0].hide();
							}
								
						}
					}
				}
			}
					
		}
	}
	function getDatosPest(pestania){
		var info = pestania;
		Ext.ComponentQuery.query('#'+info)[0].el.mask('Esperar .... ');
		mostrarBotones('','','S','','','','');
		Ext.Ajax.request({
			url: '39IfnbCalificate.data.jsp',
			params: {
				informacion: info
			},
			callback: procesarDatosIniciales
		});
		
	}

	
	var procesarDatosIniciales = function(opts, success, response) {
		var json=Ext.JSON.decode(response.responseText);
		var pestania = json.pestania;
		Ext.ComponentQuery.query('#'+pestania)[0].unmask();
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var InfoInic = json.InfoInicial;
			var pestConInf = json.pestConInf;
			
			var info_finan = pestConInf.INFORMACION_FINANCIERA;
			var dictamen  = info_finan[0] ;
			var INFO_INICIAL = InfoInic.INFORMACION_INICIAL;
			var EXISTE_INFO_P2 = pestConInf.EXISTE_INFO_P2;
			var EXISTE_INFO_P3 = pestConInf.EXISTE_INFO_P3;
			var EXISTE_INFO_P4 = pestConInf.EXISTE_INFO_P4;
			var EXISTE_INFO_P5 = pestConInf.EXISTE_INFO_P5;
			var existe_dictamen = dictamen.CS_DICTAMEN_CONFIRMADO;
			
			
			
			var InfoGeneral = json.InfoGeneral;
			var clave_solicitud = InfoGeneral.CLAVE_SOLICITUD;
			var IC_ESTATUS_SOLICITUD = InfoGeneral.IC_ESTATUS_SOLICITUD;
			
			Ext.ComponentQuery.query('#ic_solicitud')[0].setValue(clave_solicitud);
			Ext.ComponentQuery.query('#estatus_solic_inicial')[0].setValue(IC_ESTATUS_SOLICITUD);
			
			var existe_rechazo = json.existe_rechazo;
			var valor_dictamen = json.valor_dictamen;
			
			
			if(IC_ESTATUS_SOLICITUD == 6 ){
				mnsInicioPantalla.show();
				tp.hide();
			}else{
				if(pestania=='datosGrales'){
					mostrarBotones(pestania,IC_ESTATUS_SOLICITUD,'',existe_rechazo,valor_dictamen,cedula_activa,ban_actualizar,'','');
					
					var p1_tp = Ext.ComponentQuery.query('#datosGrales')[0];
					fpDG =  new NE.Informacion.FormDatosGeranes({objDatoInicial:INFO_INICIAL,campo_activo:campo_activo});
					p1_tp.add(fpDG);
					p1_tp.doLayout();
					 // *******  Se obtiene informaci�n del aspirante *****  
					 var IC_ASPIRANTE 	= 	INFO_INICIAL[0].IC_ASPIRANTE;
					 var RFC 				= 	INFO_INICIAL[0].RFC;
					 var RAZON 				=	INFO_INICIAL[0].RAZON;
					 var NOMCOM 			=	INFO_INICIAL[0].NOMCOM;
					 var CG_CALLE 			=	INFO_INICIAL[0].CG_CALLE;
					 var CG_COLONIA 		=	INFO_INICIAL[0].CG_COLONIA;
					 var IC_PAIS 			=	INFO_INICIAL[0].IC_PAIS;
					 var IC_ESTADO 		=	INFO_INICIAL[0].IC_ESTADO;
					 var IC_MUNICIPIO 	=	INFO_INICIAL[0].IC_MUNICIPIO;
					 var CG_CP 				=	INFO_INICIAL[0].CG_CP;
					 var CG_CONTACTO 		=	INFO_INICIAL[0].CG_CONTACTO;
					 var CG_EMAIL 			=	INFO_INICIAL[0].CG_EMAIL;
					 var CG_TELEFONO 		=	INFO_INICIAL[0].CG_TELEFONO;
					if(CG_COLONIA!='' &&NOMCOM!=''&&CG_CALLE!=''&&CG_COLONIA!=''&&IC_ESTADO!=''&&IC_MUNICIPIO!=''&&CG_CP!=''&&CG_CONTACTO!=''&&CG_EMAIL!=''&&CG_TELEFONO!=''){				
						tp.child('#datosGrales').setTitle( ' <div > <table width="180" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 1. Datos Generales</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
						Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(false);
					}
					
					
					tp.setActiveTab(0);
				}else if(pestania=='infCualitativa'){
					mostrarBotones(pestania,IC_ESTATUS_SOLICITUD,'',existe_rechazo,valor_dictamen,cedula_activa,ban_actualizar,'','');
					var IC_ESTATUS_SOLICITUD = InfoGeneral.IC_ESTATUS_SOLICITUD;
					if(IC_ESTATUS_SOLICITUD == 2 || IC_ESTATUS_SOLICITUD == 3 || IC_ESTATUS_SOLICITUD == 4 || IC_ESTATUS_SOLICITUD == 5 ){
						campo_activo = 'S';	
					 }else{
					
						campo_activo = 'N'; 
					 }
					var INFO_RESPUESTAS = InfoGeneral.INFO_RESPUESTAS;
					var infoParamPreguntas = InfoGeneral.INFOPARAMETRIZACION;
					var numPreg = InfoGeneral.TOTAL_PREGUNTAS;
					//var EXISTE_INFO_P2 = InfoGeneral.EXISTE_INFO_P2;
					var clave_solicitud = InfoGeneral.CLAVE_SOLICITUD;
					var IC_ESTATUS_SOLICITUD = InfoGeneral.IC_ESTATUS_SOLICITUD;
					var p2 = Ext.ComponentQuery.query('#infCualitativa')[0];
					fpIC =  new NE.Informacion.FormInformacionCualitativa({objDataResp:INFO_RESPUESTAS,objDataPreg:infoParamPreguntas,numOjetos:1, numPregReal:numPreg,  modif:'s', campo_activo:campo_activo});
					p2.add(fpIC);
					p2.doLayout();
					deshabilitaPreguntas();
				}else if(pestania=='ceditoVinculado'){
					mostrarBotones(pestania,IC_ESTATUS_SOLICITUD,'',existe_rechazo,valor_dictamen,cedula_activa,ban_actualizar,'','');
					var IC_ESTATUS_SOLICITUD = InfoGeneral.IC_ESTATUS_SOLICITUD;
					if(IC_ESTATUS_SOLICITUD == 2 || IC_ESTATUS_SOLICITUD == 3 || IC_ESTATUS_SOLICITUD == 4 || IC_ESTATUS_SOLICITUD == 5 ){
						campo_activo = 'S';	
					 }else{
					
						campo_activo = 'N'; 
					 }
					var INFORMACION_CREDITO_VINCULADO = InfoGeneral.INFORMACION_CREDITO_VINCULADO;
					var p3 = Ext.ComponentQuery.query('#ceditoVinculado')[0];
					fpCredVinc =  new NE.Informacion.FormCreditoVinculado({objDatoInicial:INFORMACION_CREDITO_VINCULADO, campo_activo:campo_activo});
					p3.add(fpCredVinc);
					p3.doLayout();
					
				}else if(pestania=='infoFinanciera'){
					mostrarBotones(pestania,IC_ESTATUS_SOLICITUD,'',existe_rechazo,valor_dictamen,cedula_activa,ban_actualizar,'','');
					var IC_ESTATUS_SOLICITUD = InfoGeneral.IC_ESTATUS_SOLICITUD;
					if(IC_ESTATUS_SOLICITUD == 2 || IC_ESTATUS_SOLICITUD == 3 || IC_ESTATUS_SOLICITUD == 4 || IC_ESTATUS_SOLICITUD == 5 ){
						campo_activo = 'S';	
					 }else{
					
						campo_activo = 'N'; 
					 }
					var INFORMACION_FINANCIERA = InfoGeneral.INFORMACION_FINANCIERA;
					var p4 = Ext.ComponentQuery.query('#infoFinanciera')[0];
					fpInfFinanciera =  new NE.Informacion.FormInfoFinanciera({objDatoInicial:INFORMACION_FINANCIERA, campo_activo:campo_activo});
					p4.add(fpInfFinanciera);
					p4.doLayout();
				}else if(pestania=='infoSaldos'){
					mostrarBotones(pestania,IC_ESTATUS_SOLICITUD,'',existe_rechazo,valor_dictamen,cedula_activa,ban_actualizar,'','');
					var IC_ESTATUS_SOLICITUD = InfoGeneral.IC_ESTATUS_SOLICITUD;
					if(IC_ESTATUS_SOLICITUD == 2 || IC_ESTATUS_SOLICITUD == 3 || IC_ESTATUS_SOLICITUD == 4 || IC_ESTATUS_SOLICITUD == 5 ){
						campo_activo = 'S';	
					 }else{
						campo_activo = 'N'; 
					 }
					var INFORMACION_SALDOS = InfoGeneral.INFORMACION_SALDOS;
					var INFORMACION_FINANCIERA = InfoGeneral.INFORMACION_FINANCIERA;
					var p5 = Ext.ComponentQuery.query('#infoSaldos')[0];
					fpSaldos =  new NE.Informacion.FormSaldos({objDatoInicial:INFORMACION_SALDOS, campo_activo:campo_activo,INFORMACION_FINANCIERA:INFORMACION_FINANCIERA});
					p5.add(fpSaldos);
					p5.doLayout();
				}else if(pestania=='IndicadorFinanciero'){
					var LISTA_ANIO = InfoGeneral.LISTA_ANIO;
					var INFORMACION_FINANCIERA = InfoGeneral.INFORMACION_FINANCIERA;
					var infoFinan = INFORMACION_FINANCIERA[0];
					click_finalizar = click_finalizar;
					var p6 = Ext.ComponentQuery.query('#IndicadorFinanciero')[0];
					fpIndicadorF =  new NE.Informacion.FormIndicador({objDatoInicial:LISTA_ANIO,anio:infoFinan.IG_ANIO_INF_FINANCIERA ,mes:infoFinan.IG_MES_INF_FINANCIERA });
					p6.add(fpIndicadorF);
					p6.doLayout();
					mostrarBotones(pestania,IC_ESTATUS_SOLICITUD,'',existe_rechazo,valor_dictamen,cedula_activa,ban_actualizar,activa_acualizar,click_finalizar);
				}
				
				if(EXISTE_INFO_P2 > 0 ){
					tp.child('#infCualitativa').tab.setDisabled(false);
					tp.child('#infCualitativa').setTitle( ' <div > <table width="180" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 2. Informaci�n Cualitativa</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
							
				}else{
					if(btn_continuar=='S' 	&& pestania_activa==='infCualitativa' ){
						tp.child('#infCualitativa').tab.setDisabled(false);
					}
				}
				if(EXISTE_INFO_P3 > 0 ){
					tp.child('#ceditoVinculado').tab.setDisabled(false);
					tp.child('#ceditoVinculado').setTitle( ' <div > <table width="180" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 3. Cr�dito Vinculado</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
				}else{
					if(btn_continuar=='S'&& pestania_activa==='ceditoVinculado'){
						tp.child('#ceditoVinculado').tab.setDisabled(false);
					}
					
				}
				if(EXISTE_INFO_P4 > 0){
					tp.child('#infoFinanciera').tab.setDisabled(false);
					tp.child('#infoFinanciera').setTitle( ' <div > <table width="180" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 4. Informaci�n Financiera</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
				}else{
					if(btn_continuar=='S'&&pestania_activa==='infoFinanciera'){
						tp.child('#infoFinanciera').tab.setDisabled(false);
					}
				}
				if(EXISTE_INFO_P5 > 0){
					tp.child('#infoSaldos').tab.setDisabled(false);
						
					tp.child('#infoSaldos').setTitle( ' <div > <table width="120" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 5. Saldos</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
						
				}else{
					if(btn_continuar=='S'&&pestania_activa==='infoSaldos'){
						tp.child('#infoSaldos').tab.setDisabled(false);
					}
				}
				
				if(existe_dictamen!=''){
					tp.child('#IndicadorFinanciero').tab.setDisabled(false);
				}
				Ext.ComponentQuery.query('#datosGrales')[0].tab.setDisabled(false);
				
				
				if(cedula_activa==true||IC_ESTATUS_SOLICITUD==2	||IC_ESTATUS_SOLICITUD==3 ||IC_ESTATUS_SOLICITUD==4||IC_ESTATUS_SOLICITUD==5){
					soloLectura(true,pestania);
				}
				btn_continuar = 'N';
				pestania_activa = '';
				}
		} else {
			NE.util.mostrarConnError( 
				response,
				opts, 
				function(){	} 	
			);
		}
		
	}
	function descargaArchivoCedula(){
		Ext.Ajax.request({
			url: '39IfnbCalificate.data.jsp',
			params: Ext.apply({
				informacion:  'Descarga_ArchivoPDF_Cedula',
				ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
				estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
				
			}),
			callback: procesaGeneraArchivoCedula
		});
	}
	
	var procesarGuardarSaldos = function(opts, success, response) {
		
		Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
		Ext.ComponentQuery.query('#btnRegresar')[0].enable();
		Ext.ComponentQuery.query('#btnContinuar')[0].enable();
		Ext.ComponentQuery.query('#btnGuardar')[0].setDisabled(true);
		Ext.ComponentQuery.query('#guardar_activo_Sal')[0].setValue('N')
	
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var json=Ext.JSON.decode(response.responseText);
			Ext.Msg.alert('Mensaje','&nbsp;&nbsp;'+json.exito);
			activa_acualizar = json.activa_actualiza;
			existe_inf_p5 = 1;
			Ext.ComponentQuery.query('#btnAceptar')[0].setDisabled(false);
			//Ext.ComponentQuery.query('#btnActualizar')[0].setDisabled(false);
			var gridInforAcre = Ext.ComponentQuery.query('#gridInforAcreditados')[0];
			var storeAcred = gridInforAcre.getStore(); 
				storeAcred.load({
					params: Ext.apply(
					{
						informacion: 'Consulta_lista_acreditados',
						tipo_acreditado :'V',	
						ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
					})
				});
			var gridInforIn = Ext.ComponentQuery.query('#gridInforIncumplidos')[0];
			var storeIncu = gridInforIn.getStore(); 
					storeIncu.load({
						params: Ext.apply(
								{
									informacion: 'Consulta_lista_acreditados',
									tipo_acreditado :'I',	
									ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
								})
					});
			
		} else {
			NE.util.mostrarConnError( response,		opts, function(){	} 	 );
		}
		
	}
	var procesarFinalizarReg = function(opts, success, response) {
	
		Ext.ComponentQuery.query('#btnContinuar')[0].setIconCls('icoContinuar');
		Ext.ComponentQuery.query('#btnContinuar')[0].enable();
		Ext.ComponentQuery.query('#btnRegresar')[0].enable();
		Ext.ComponentQuery.query('#btnContinuar')[0].enable();
		Ext.ComponentQuery.query('#btnGuardar')[0].setDisabled(true);
		Ext.ComponentQuery.query('#guardar_activo_Sal')[0].setValue('N');
		
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
						
			var json=Ext.JSON.decode(response.responseText);
			ban_actualizar = json.ban_actualizar;
			click_finalizar = json.click_finalizar;
			Ext.ComponentQuery.query('#btnAceptar2v')[0].setDisabled(true);
			if(existe_inf_p5==1){
				tp.child('#infoSaldos').setTitle( ' <div > <table width="180" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 5. Saldos</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
			}
			tp.setActiveTab('IndicadorFinanciero');
			
			tp.child('#datosGrales').tab.setDisabled(true);
			tp.child('#infCualitativa').tab.setDisabled(true);
			tp.child('#ceditoVinculado').tab.setDisabled(true);
			tp.child('#infoFinanciera').tab.setDisabled(true);
			tp.child('#infoSaldos').tab.setDisabled(true);
		}
		
	}
	var procesaActualizaSaldos = function(opts, success, response) {
		Ext.ComponentQuery.query('#btnActualizar')[0].setIconCls('icoActualizar');
		Ext.ComponentQuery.query('#btnActualizar')[0].enable();
		var json=Ext.JSON.decode(response.responseText);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var gridInfoFin = Ext.ComponentQuery.query('#gridAnioDictaminado')[0];
			var storeInfoFin = gridInfoFin.getStore(); 
			//main.el.mask('Procesando...', 'x-mask-loading');
			storeInfoFin.load({
					params: Ext.apply(
					{
						informacion: 'Consulta_anio_dictaminado',
						ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
					})
				});
		} else {
			NE.util.mostrarConnError( 
				response,
				opts, 
				function(){	} 	
			);
		}
		
	}
	var procesarEnviaEdoFinan = function(opts, success, response) {
		Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].hide();
		Ext.ComponentQuery.query('#btnEnviar')[0].setIconCls('icoContinuar');
		Ext.ComponentQuery.query('#btnEnviar')[0].enable();
		var win = Ext.ComponentQuery.query('#winDocPreliminar')[0].destroy();
		
		  var json=Ext.JSON.decode(response.responseText);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			
			estatus_sol = json.estatus;
			var existe_rech = json.existe_rech;
			var credito_vinculado = json.credito_vinculado
			var leyCred = '';
			ban_doctos_enviados = json.ban_doctos_enviados;
			Ext.Msg.alert('Mensaje', json.mns);
			if(credito_vinculado=='S'){
				leyCred = 'El Intermediario Financieros SI cuenta con Cr�ditos Vinculados, se recomienda su revisi�n';
			}else{
				leyCred = 'El Intermediario NO cuenta con cr�ditos vinculados';
			}
			
			var leyenda = new Ext.Panel({
				frame: true,
				layout:'form',
				border	: false,	
				items:[
					{
							xtype:'label',
							style:
							 {
								  'font-size' : '12px'
							 },
							 html:'<table >'+
									'<tr><td height="30"><p style="text-align:justify; font-size:100%">La informaci�n que fue proporcionada cumple con los requisitos m�nimos solicitados, en breve un Ejecutivo se pondr� en contacto con usted para continuar con su ejercicio de viabilidad para formar parte de la red de Intermediarios de NAFINSA.</p></td></tr>'+
									'<tr><td height="30"><p style="text-align:justify; font-size:100%">Para cualquier duda o aclaraci�n favor de contactarnos al correo electr�nico cedi@nafin.gob.mx o al tel�fono 01 800 NAFINSA (623 4672).</p></td></tr>'+
									'<tr><td height="30"><p style="text-align:justify; font-size:100%; padding-left: 25px;"><b>Nota</b>:'+leyCred+'</p></td></tr>'+
									'<tr><td height="30" ><p style="text-align:justify; font-size:100%"><b>*El resultado de esta c�dula es un ejercicio de elegibilidad, por lo que no es una autorizaci�n de cr�dito ni un monto de l�nea a otorgar. La solicitud requiere an�lisis experto y autorizaci�n del Comit� correspondiente en NAFINSA.</b><br></p></td></tr>'+
								 '</table>'
						}
				]
			});
			if(estatus_sol==2){
				title ='C�DULA ELECTR�NICA DE ACEPTACI�N';
				Ext.ComponentQuery.query('#leyendas')[0].add(leyenda);
				Ext.ComponentQuery.query('#leyendas')[0].doLayout();
				if(existe_rech=='S'){
				
					Ext.ComponentQuery.query('#leyendaRechazo')[0].destroy();
					Ext.ComponentQuery.query('#leyendas')[0].doLayout();
				}
			}
			Ext.ComponentQuery.query('#pnlAspirante')[0].setTitle('<center>'+title+'</center>');
			consulInfoAspirante.load();
			consulIndicadores.load();
			consulInfoCuali.load();
			conteCedula.show();
			gridIndicadorCedula.show();
			gridInfoCuali.show();
			
			
			
			var pestDatosGerales = tp.child('#datosGrales');
			tp.setActiveTab(pestDatosGerales);
			cedula_activa = true;
			
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
		
	}
	var validaBalance = function(pasivo, capital, activos_totales){
		var sume = Number(pasivo) + Number(capital) ;
		if(sume == Number(activos_totales)){
			return true;
		}else{
			return false;
		}
	}
	var guardarInfFinanciera = function(boton, evento){
	
		var gridInfoFin = Ext.ComponentQuery.query('#gridInformaFinan')[0];
		var store = gridInfoFin.getStore();
		var selected = store.getRange(); 
		var numRegistros=0;		
		varGlob();
		var  valor = true;
		var fechaInfinanc =  Ext.ComponentQuery.query('#fechaInfinanc')[0];
		var activo_total_1=0;
		var pasivo_total_1=0;
		var capital_Contable_1=0;
		
		var activo_total_2=0;
		var pasivo_total_2=0;
		var capital_Contable_2=0;
		
		var activo_total_3=0;
		var pasivo_total_3=0;
		var capital_Contable_3=0;
		
		if(Ext.isEmpty(fechaInfinanc.getValue())){
			fechaInfinanc.markInvalid('Este Campo es Obligatorio');
			fechaInfinanc.focus();
			return false;
		}
		Ext.each(selected, function(item) {
			 listAnioInfFinan.push(item.data.ANIO_1);
			 listAnio_1.push(item.data.ANIO_2);
			 listAnio_2.push(item.data.ANIO_3);
			 listCampo.push(item.data.CAMPO);
			 numRegistros++;
		}, this);
		Ext.each(selected, function(item) {
			numReg = store.indexOf(item);
			if(item.data.ANIO_1==''){
				var editor = gridInfoFin.plugins[0];
				var fila = numReg;
				Ext.Msg.alert('Aviso','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Este campo es obligatorio</center></td><td></td></table> </div>',
				function(){
					editor.startEditByPosition({row: fila, column:3});
				});
				valor= false
				return false;
			}
			if(item.data.ID_CAMPO=='FG_PASIVOS_TOTALES'&&item.data.ANIO_1!=''){
				pasivo_total_1 = item.data.ANIO_1;
			}else if(item.data.ID_CAMPO=='FG_CAPITAL_CONTABLE'&&item.data.ANIO_1!=''){
				capital_Contable_1 = item.data.ANIO_1;
			}else if(item.data.ID_CAMPO=='FG_ACTIVOS_TOTALES'&&item.data.ANIO_1!=''){
				activo_total_1 = item.data.ANIO_1;
			}
			
		});
		Ext.each(selected, function(item) {
			numReg = store.indexOf(item);
			if(item.data.ANIO_2==''){
				var editor = gridInfoFin.plugins[0];
				var fila = numReg;
				Ext.Msg.alert('Error','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Este campo es obligatorio</center></td><td></td></table> </div>',
				function(){
					editor.startEditByPosition({row: fila, column:2});
				});
				valor= false
				return false;
			}
			if(item.data.ID_CAMPO=='FG_PASIVOS_TOTALES'&&item.data.ANIO_2!=''){
				pasivo_total_2 = item.data.ANIO_2;
			}else if(item.data.ID_CAMPO=='FG_CAPITAL_CONTABLE'&&item.data.ANIO_2!=''){
				capital_Contable_2 = item.data.ANIO_2;
			}else if(item.data.ID_CAMPO=='FG_ACTIVOS_TOTALES'&&item.data.ANIO_2!=''){
				activo_total_2 = item.data.ANIO_2;
			}
		});
		Ext.each(selected, function(item) {
			numReg = store.indexOf(item);
			if(item.data.ANIO_3==''){
				var editor = gridInfoFin.plugins[0];
				var fila = numReg;
				Ext.Msg.alert('Error','<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Este campo es obligatorio</center></td><td></td></table> </div>',
				function(){
					editor.startEditByPosition({row: fila, column:1});
				});
				valor= false
				return false;
			}
			if(item.data.ID_CAMPO=='FG_PASIVOS_TOTALES'&&item.data.ANIO_3!=''){
				pasivo_total_3 = item.data.ANIO_3;
			}else if(item.data.ID_CAMPO=='FG_CAPITAL_CONTABLE'&&item.data.ANIO_3!=''){
				capital_Contable_3 = item.data.ANIO_3;
			}else if(item.data.ID_CAMPO=='FG_ACTIVOS_TOTALES'&&item.data.ANIO_3!=''){
				activo_total_3 = item.data.ANIO_3;
			}
		});
		var mes1 = Ext.ComponentQuery.query('#mes_selec')[0].getValue();
		var anio = Ext.ComponentQuery.query('#anio_selec')[0].getValue();
		var fecha_combo = Ext.ComponentQuery.query('#fechaInfinanc')[0];
		
		var ban_valida_1 = 0;
		var ban_valida_2 = 0;
		var ban_valida_3 = 0;
		if(!validaBalance(pasivo_total_1,capital_Contable_1,activo_total_1)){
			ban_valida_1 = 1;
			valor= false;
		}
		
		if(!validaBalance(pasivo_total_2,capital_Contable_2,activo_total_2)){
			ban_valida_2 = 1;
			valor= false;
		}
		if(!validaBalance(pasivo_total_3,capital_Contable_3,activo_total_3)){
			ban_valida_3 = 1;
			valor= false;
		}
		if(ban_valida_3==1&&ban_valida_2==0&&ban_valida_1==0){
			var editor = gridInfoFin.plugins[0];
			Ext.Msg.alert('Aviso','Los Activos totales no son iguales a la suma de los Pasivos totales y el Capital Contable del a�o '+(anio-2),
				function(){
					
			});
			Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(true);
			Ext.ComponentQuery.query('#btnRegresar')[0].setDisabled(true);
		}else if(ban_valida_3==0&&ban_valida_2==1&&ban_valida_1==0){
			var editor = gridInfoFin.plugins[0];
			Ext.Msg.alert('Aviso','Los Activos totales no son iguales a la suma de los Pasivos totales y el Capital Contable del a�o '+(anio-1),
				function(){
					
			});
			Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(true);
			Ext.ComponentQuery.query('#btnRegresar')[0].setDisabled(true);
		}else if(ban_valida_3==0&&ban_valida_2==0&&ban_valida_1==1){
			var editor = gridInfoFin.plugins[0];
			
			Ext.Msg.alert('Aviso','Los Activos totales no son iguales a la suma de los Pasivos totales y el Capital Contable del a�o '+fecha_combo.getRawValue(),
				function(){
					
			});
			Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(true);
			Ext.ComponentQuery.query('#btnRegresar')[0].setDisabled(true);
		}else if(ban_valida_3==1&&ban_valida_2==1&&ban_valida_1==1){
			var editor = gridInfoFin.plugins[0];
			Ext.Msg.alert('Aviso','Los Activos totales no son iguales a la suma de los Pasivos totales y el Capital Contable del a�o '+(anio-2)+', '+(anio-1)+' y '+fecha_combo.getRawValue(),
				function(){
					
			});
			Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(true);
			Ext.ComponentQuery.query('#btnRegresar')[0].setDisabled(true);
		}else if(ban_valida_3==1&&ban_valida_2==1&&ban_valida_1==0){
			var editor = gridInfoFin.plugins[0];
			Ext.Msg.alert('Aviso','Los Activos totales no son iguales a la suma de los Pasivos totales y el Capital Contable del a�o '+(anio-2)+' y '+(anio-1),
				function(){
					
			});
			Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(true);
			Ext.ComponentQuery.query('#btnRegresar')[0].setDisabled(true);
		}else if(ban_valida_3==0&&ban_valida_2==1&&ban_valida_1==1){
			var editor = gridInfoFin.plugins[0];
			Ext.Msg.alert('Aviso','Los Activos totales no son iguales a la suma de los Pasivos totales y el Capital Contable del a�o '+(anio-1)+' y '+fecha_combo.getRawValue(),
				function(){
					
			});
			Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(true);
			Ext.ComponentQuery.query('#btnRegresar')[0].setDisabled(true);
		}else if(ban_valida_3==1&&ban_valida_2==0&&ban_valida_1==1){
			var editor = gridInfoFin.plugins[0];
			Ext.Msg.alert('Aviso','Los Activos totales no son iguales a la suma de los Pasivos totales y el Capital Contable del a�o '+(anio-2)+' y '+fecha_combo.getRawValue(),
				function(){
					
			});
			Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(true);
			Ext.ComponentQuery.query('#btnRegresar')[0].setDisabled(true);
		}else{
			Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(false);
			Ext.ComponentQuery.query('#btnRegresar')[0].setDisabled(false);
		}
		var infFina = Ext.ComponentQuery.query('#infoFinancieraV')[0];
		if(valor) {
			boton.disable();
			boton.setIconCls('x-mask-msg-text');
			Ext.Ajax.request({
				url: '39IfnbCalificate.data.jsp',
					params:Ext.apply(infFina.getForm().getValues(),{
					informacion: 'GuardarInfoFinanciera',
					listAnioInfFinan:listAnioInfFinan,
					listAnio_1:listAnio_1,
					listAnio_2 : listAnio_2,
					listCampo:listCampo,
					numeroRegistros : numRegistros,
					fechaInfinanc:Ext.ComponentQuery.query('#fechaInfinanc')[0].getValue(),
					ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
					estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
				}),
				callback: procesarGuardarInfFi
			});	
		}
	}
	
	var guardarSaldos= function(){
		var gridAcreditado = Ext.ComponentQuery.query('#gridInforAcreditados')[0];
		var gridIncumplido= Ext.ComponentQuery.query('#gridInforIncumplidos')[0];
		
		var storeAcre = gridAcreditado.getStore();
		var selectedAcre = storeAcre.getRange(); 
		var numReAcre=0;
		
		var storeInc = gridIncumplido.getStore();
		var selectedInc = storeInc.getRange(); 
		var numReInc=0;
		var valor=true;
		
		varGlobAcre();
		varGlobInc();
		Ext.each(selectedAcre, function(item) {
			 listSaldoAcre.push(item.data.IC_SALDO_ACREDITADO);
			 listSaldoA.push(item.data.FG_SALDO);			 
			 numReAcre++;
		}, this);
		
		Ext.each(selectedInc, function(item) {
			 listSaldoAcreI.push(item.data.IC_SALDO_ACREDITADO);
			 listSaldoI.push(item.data.FG_SALDO);
			 numReInc++;
		}, this);
		Ext.each(selectedInc, function(item) {
			numReg = storeInc.indexOf(item);
			if(item.data.FG_SALDO==''){
				var editor = gridIncumplido.plugins[0];
				var fila = numReg;
				Ext.Msg.alert('Validaci�n','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Este campo es obligatorio',
				function(){
					editor.startEditByPosition({row: fila, column:1});
				});
			valor= false
			return false;
								}
		});
		Ext.each(selectedAcre, function(item) {
			numReg = storeAcre.indexOf(item);
			if(item.data.FG_SALDO==''){
				var editor = gridAcreditado.plugins[0];
				var fila = numReg;
				Ext.Msg.alert('Validaci�n','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Este campo es obligatorio',
				function(){
					editor.startEditByPosition({row: fila, column:1});
				});
			valor= false
			return false;
								}
		});
		
		
		var  forma =   Ext.ComponentQuery.query('#pnInfoSaldos')[0];
		var montoPf =  forma.query('#montoPf')[0];
		var montoPm =  forma.query('#montoPm')[0];
		if(Ext.isEmpty(montoPm.getValue())){
			montoPm.markInvalid('Este Campo es Obligatorio');
			montoPm.focus();
			valor= false;
			return false;
		}
				
		if(Ext.isEmpty(montoPf.getValue())){
			montoPf.markInvalid('Este Campo es Obligatorio');
			montoPf.focus();
			valor= false;
			return false;
		}
		
		if(valor==true){	
			Ext.ComponentQuery.query('#btnGuardar')[0].disable();
			Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('x-mask-msg-text');	
			//Ext.ComponentQuery.query('#btnRegresar')[0].disable();
			Ext.ComponentQuery.query('#btnContinuar')[0].disable();
			var infFina = Ext.ComponentQuery.query('#infoSaldosV')[0];
			Ext.Ajax.request({
				url: '39IfnbCalificate.data.jsp',
					params:Ext.apply(infFina.getForm().getValues(),{
					informacion: 'GuardarSaldo',
					listSaldoAcreI:listSaldoAcreI,
					listSaldoI:listSaldoI,
					listSaldoAcre:listSaldoAcre,
					listSaldoA:listSaldoA,
					numReAcre:numReAcre,
					numReInc:numReInc,
					ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
					estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
				}),
				callback: procesarGuardarSaldos
			});
		}
	}
	var funcionFinalizar= function(){
		var gridAcreditado = Ext.ComponentQuery.query('#gridInforAcreditados')[0];
		var gridIncumplido= Ext.ComponentQuery.query('#gridInforIncumplidos')[0];
		
		var storeAcre = gridAcreditado.getStore();
		var selectedAcre = storeAcre.getRange(); 
		var numReAcre=0;
		
		var storeInc = gridIncumplido.getStore();
		var selectedInc = storeInc.getRange(); 
		var numReInc=0;
		var valor=true;
		
		
		Ext.each(selectedInc, function(item) {
			numReg = storeInc.indexOf(item);
			if(item.data.FG_SALDO==''){
				var editor = gridIncumplido.plugins[0];
				var fila = numReg;
				Ext.Msg.alert('Validaci�n','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Este campo es obligatorio',
				function(){
					editor.startEditByPosition({row: fila, column:1});
				});
			valor= false
			return false;
								}
		});
		Ext.each(selectedAcre, function(item) {
			numReg = storeAcre.indexOf(item);
			if(item.data.FG_SALDO==''){
				var editor = gridAcreditado.plugins[0];
				var fila = numReg;
				Ext.Msg.alert('Validaci�n','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Este campo es obligatorio',
				function(){
					editor.startEditByPosition({row: fila, column:1});
				});
			valor= false
			return false;
								}
		});
		
		
		var  forma =   Ext.ComponentQuery.query('#pnInfoSaldos')[0];
		var montoPf =  forma.query('#montoPf')[0];
		var montoPm =  forma.query('#montoPm')[0];
		if(Ext.isEmpty(montoPm.getValue())){
			montoPm.markInvalid('Este Campo es Obligatorio');
			montoPm.focus();
			valor= false;
			return false;
		}
				
		if(Ext.isEmpty(montoPf.getValue())){
			montoPf.markInvalid('Este Campo es Obligatorio');
			montoPf.focus();
			valor= false;
			return false;
		}
		
			
		if(valor==true){	
			Ext.ComponentQuery.query('#btnContinuar')[0].disable();
			Ext.ComponentQuery.query('#btnContinuar')[0].setIconCls('x-mask-msg-text');	
			Ext.ComponentQuery.query('#btnRegresar')[0].disable();
			Ext.ComponentQuery.query('#btnGuardar')[0].disable();
			
			Ext.Ajax.request({
				url: '39IfnbCalificate.data.jsp',
					params:Ext.apply({
						informacion:'Finaliza_Registro',
						ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
						estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
					}),
				callback: procesarFinalizarReg
			});
		}
	}
	
	var procesarGuardarInformacion = function(opts, success, response) {
	
		Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
		Ext.ComponentQuery.query('#btnGuardar')[0].setDisabled(true);
		Ext.ComponentQuery.query('#guardar_activo')[0].setValue('N')
		var json=Ext.JSON.decode(response.responseText);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert( 'Aviso',json.exito);	
			activa_acualizar = json.activa_actualiza;
			existe_inf_p1 = 1;
			//Ext.ComponentQuery.query('#btnActualizar')[0].setDisabled(false);
		} else {
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
	}
	var procesarGuardarInfFi = function(opts, success, response) {
	
		Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
		Ext.ComponentQuery.query('#btnGuardar')[0].setDisabled(true);
		Ext.ComponentQuery.query('#guardar_activo_InfF')[0].setValue('N')
		var json=Ext.JSON.decode(response.responseText);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert( 'Aviso',json.exito);	
			Ext.ComponentQuery.query('#btnContinuar')[0].setDisabled(false);
			//Ext.ComponentQuery.query('#btnActualizar')[0].setDisabled(false);
		
			activa_acualizar = json.activa_actualiza;
			existe_inf_p4 = 1;
			
		} else {
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
	}
	var procesarGuardarInfoCualitativa = function(opts, success, response) {
		Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
		Ext.ComponentQuery.query('#btnGuardar')[0].setDisabled(true);
		Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('N');	
		 var json=Ext.JSON.decode(response.responseText);
		
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje', '<center>'+json.exito+'</center>');
			//Ext.ComponentQuery.query('#btnActualizar')[0].setDisabled(false);
			 activa_acualizar = json.activa_actualiza;
			 existe_inf_p2 = 1;
		} else {
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
		
	}
	var procesarGuardarCreditoV = function(opts, success, response) {
		Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
		Ext.ComponentQuery.query('#btnGuardar')[0].setDisabled(true);
		Ext.ComponentQuery.query('#guardar_activo_cred')[0].setValue('N');	
		 var json=Ext.JSON.decode(response.responseText);
		
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje', '<center>'+json.exito+'</center>');
			//Ext.ComponentQuery.query('#btnActualizar')[0].setDisabled(false);
			activa_acualizar = json.activa_actualiza; 
			existe_inf_p3 = 1; 
		} else {
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
		
	}
	var procesaEstatusSolicitus = function(opts, success, response) {
		Ext.ComponentQuery.query('#btnAceptar')[0].setIconCls('icoAceptar');
		Ext.ComponentQuery.query('#btnAceptar')[0].enable();	
		var json=Ext.JSON.decode(response.responseText);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			var estatus = json.estatus;
			cedula_activa = true;
			
			
			consulInfoAspirante.load();
			consulIndicadores.load();
			consulInfoCuali.load();
			conteCedula.show();
			gridIndicadorCedula.show();
			gridInfoCuali.show();
			var title ="";
			if(estatus==2){
				var pestDatosGerales = tp.child('#IndicadorFinanciero');
				tp.setActiveTab(pestDatosGerales);
			}else{
				title = 'C�DULA ELECTR�NICA DE RECHAZO';
				Ext.ComponentQuery.query('#leyendaRechazo')[0].show();
				
				Ext.ComponentQuery.query('#pnlAspirante')[0].setTitle('<center>'+title+'</center>');
				var pestDatosGerales = tp.child('#datosGrales');
				tp.setActiveTab(pestDatosGerales);
			}
			
		}else {
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
		
	}
	var procesaEstatusSolicitus2V = function(opts, success, response) {
		Ext.ComponentQuery.query('#btnAceptar2v')[0].setIconCls('icoAceptar');
		Ext.ComponentQuery.query('#btnAceptar2v')[0].enable();	
		var json=Ext.JSON.decode(response.responseText);
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			var estatus = json.estatus;
			var dictame = json.dictame;
			var rechazo = json.rechazo;
			
			if((estatus==7||estatus==8)&&dictame=='N'){
				var title ="";
				cedula_activa = true;
				consulInfoAspirante.load();
				consulIndicadores.load();
				consulInfoCuali.load();
				conteCedula.show();
				gridIndicadorCedula.show();
				gridInfoCuali.show();
				title = 'C�DULA ELECTR�NICA DE RECHAZO';
				Ext.ComponentQuery.query('#leyendaRechazo')[0].show();
				Ext.ComponentQuery.query('#pnlAspirante')[0].setTitle('<center>'+title+'</center>');
				var pestDatosGerales = tp.child('#datosGrales');
				tp.setActiveTab(pestDatosGerales);
			}else if(dictame=='S'){
				var pestDatosGerales = tp.child('#IndicadorFinanciero');
				tp.setActiveTab(pestDatosGerales);
				mostrarBotones('IndicadorFinanciero',estatus,'',rechazo,dictame,cedula_activa,'','');
				
			} 
			
		} else {
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}
		
	}
	var procesarInformacionCedula= function(){
		Ext.ComponentQuery.query('#btnAceptar')[0].setDisabled(true);
		Ext.ComponentQuery.query('#btnAceptar')[0].setIconCls('x-mask-msg-text');
		click_finalizar = 'N';			
		Ext.Ajax.request({
			url: '39IfnbCalificate.data.jsp',
			params: Ext.apply({
				informacion:  'Aceptar_proceso',
				ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
				estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
			}),
			callback: procesaEstatusSolicitus
		});
		
			
	}

	
	
	//*****************************************FUNCION CALBACK GENERAL INGRESAR*********************************
	//****** ELEMENTOS PARA LA INFORMACI�N CUALITATIVA 
	
	
	
	// ********** ELEMENTOS PARA LOS DATOS GENERALES ********
	var elementosDocumento =[
		{
			xtype: 'fieldcontainer',
			combineErrors: false,
			msgTarget: 'side',
			width: 550,
			items: [
				{
					xtype:'label',
					style:
					 {
						  'font-size' : '12px'
					 },
					html:' <div > <table width="500" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td align="right" valign="top" ><center><b>Favor de enviar copia de la siguiente documentaci�n para concluir el registro de su solicitud y generar su C�dula Electr�nica de Viabilidad de incorporaci�n a la red de Intermediarios de NAFINSA.</b></center></td></tr> </table> </div><br> '
				}
			]
		},
		{
			xtype:'displayfield',
			width: 25
		},
		{
			xtype:'panel',
			frame: true,
			border: true,	
			cls: 'panel-sin-borde',
			layout:'table',
			width:      550,
			bodyPadding: '0 50 0 140',
			layoutConfig:{ columns: 2 },
			items: [
				{
					xtype: 'fieldcontainer',
					combineErrors: false,
					msgTarget: 'side',
					//width: 550,
					items: [
						{
							xtype:'label',
							style:
							 {
								  'font-size' : '12px'
							 },
							html:' <div > <table  border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td align="right" valign="top" ><center>Estado de Situaci�n Financiera(Balance).pdf</center></td></tr> </table> </div>'
						}
					]
				},
				{
					xtype: 'fieldcontainer',
					combineErrors: false,
					msgTarget: 'side',
					//width: 550,
					items: [
						{	
							xtype: 'button',	
							itemId: 'btnBalanceDesc',
							disabled:true,
							iconCls: 'icoPdf',
							width:20,
							handler: function( button, event ){
								descargaArchivoSolic('Balance');
							}
						}
					]
				}
				
			]
		},
		{
			xtype:'displayfield',
			width: 20,
			itemId:'rutaBalance',
			name:'rutaBalance',
			hidden :true,
			allowBlank: false
	
		},
		
		{
			xtype: 'fieldcontainer',
			combineErrors: false,
			msgTarget: 'side',
			width: 750,
			items: [
				{
					xtype: 'filefield',
					fieldLabel: '&nbsp; Cargar Archivo de ',
					name:   'EdoSutFinan',
					itemId:'EdoSutFinan',
					labelWidth	: 150,
					msgTarget:  'side',
					//allowBlank: 	false,
					anchor:     '-20',
					buttonText: null,
					vtype: 			'archivopdf',
					width:      550,
					emptyText:  'Seleccione un archivo...',
					bodyPadding: '30 20 12 30',
					bandera :'N',
					buttonConfig:  { iconCls: 'upload-icon' },
					listeners: {
						change: function(fld, value) {
							if(value !=''){
									var formaCargarArchivo  = Ext.ComponentQuery.query("#fpDocumentacion" )[0];
									var params = formaCargarArchivo.getForm().getValues();
									
									formaCargarArchivo.getForm().submit({
										 clientValidation: true,
										 url: 		'39IfnbCalificate.data.jsp?informacion=cargaArchivosEstadoFinanciero&tipoArchivo=Balance',
										 params:		params,
										 success:	function(form, action){ 
											var resp = action.result;
											 if(!resp.ok_file){
												Ext.ComponentQuery.query("#btnBalanceDesc")[0].setDisabled(true); 
												
												Ext.ComponentQuery.query("#valueBalance" )[0].setValue('');
												Ext.ComponentQuery.query("#EdoSutFinan" )[0].markInvalid(resp.mensaje);
												
											 }else{
												Ext.ComponentQuery.query("#btnBalanceDesc")[0].setDisabled(false);
												Ext.ComponentQuery.query('#rutaBalance')[0].setValue(resp.nombreArchivo);
												Ext.ComponentQuery.query("#valueBalance" )[0].setValue(resp.nombreArchivo);
											}
										 },
										 failure: 	function(form, action){
											var resp = action.result;	
											var mensaje = resp.mensaje;
											if(resp.mensaje!=''){
												Ext.ComponentQuery.query("#btnBalanceDesc")[0].setDisabled(true); 
												Ext.ComponentQuery.query("#EdoSutFinan" )[0].markInvalid(resp.mensaje);
												Ext.ComponentQuery.query("#valueBalance" )[0].setValue('');
											}
										 }
									});
								}
							}
					}
				}
			]
		},
		{
			xtype:'panel',
			frame: true,
			border: true,	
			cls: 'panel-sin-borde',
			layout:'table',
			width:      550,
			itemId:'pnlEdoRes',
			
			bodyPadding: '0 0 0 200',
			layoutConfig:{ columns: 2 },
			items: [
				{
					xtype: 'fieldcontainer',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:'label',
							style:
							 {
								  'font-size' : '12px'
							 },
							html:' <div > <table  border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td align="right" valign="top" ><center>Estado de Resultados.pdf</center></td></tr> </table> </div> '
						}
					]
				},
				{
					xtype: 'fieldcontainer',
					combineErrors: false,
					msgTarget: 'side',
					//width: 550,
					items: [
						{	
							xtype: 'button',	
							itemId: 'btnResultadoDesc',	
							iconCls: 'icoPdf',
							disabled:true,
							width:20,
							handler: function( button, event ){
								descargaArchivoSolic('Edo_Resultado');
							}
						}
					]
				}
				
			]
		},
		{
			xtype:'displayfield',
			width: 20,
			itemId:'rutaEdoResultados',
			name:'rutaEdoResultados',
			hidden :true,
			allowBlank: false
	
		},
		
		{
			xtype: 'fieldcontainer',
			combineErrors: false,
			msgTarget: 'side',
			width: 550,
			items: [
				{
					xtype: 'filefield',
					name:   'EdoResultados',
					width:      550,
					itemId:'EdoResultados',
					fieldLabel: '&nbsp; Cargar Archivo de ',
					emptyText:  'Seleccione un archivo...',
					anchor:     '-20',
					msgTarget:  'side',
					//allowBlank: 	false,
					bodyPadding: '30 20 12 20',
					vtype: 			'archivopdf',
					labelWidth	: 150,
					bandera :'N',
					buttonConfig:  { iconCls: 'upload-icon' },
					buttonText: null,
					listeners: {
					  change: function (fld, value) {
							
							if(value !=''){
								var formaCargarArchivo  = Ext.ComponentQuery.query("#fpDocumentacion" )[0];
								var params = formaCargarArchivo.getForm().getValues();
								formaCargarArchivo.getForm().submit({
									 clientValidation: true,
									 url: 		'39IfnbCalificate.data.jsp?informacion=cargaArchivosEstadoFinanciero&tipoArchivo=Edo_Resultado',
									 params:		params,
									 success:	function(form, action){
										
										var resp = action.result;
										
										if(!resp.ok_file){
											
											Ext.ComponentQuery.query("#btnResultadoDesc")[0].setDisabled(true); 
											Ext.ComponentQuery.query('#EdoResultados')[0].markInvalid(resp.mensaje);
											Ext.ComponentQuery.query("#valueEdoResultados" )[0].setValue('');
											
										 }else{
											
											Ext.ComponentQuery.query("#btnResultadoDesc")[0].setDisabled(false); 
											Ext.ComponentQuery.query('#rutaEdoResultados')[0].setValue(resp.nombreArchivo);
											Ext.ComponentQuery.query("#valueEdoResultados" )[0].setValue(resp.nombreArchivo);
											
										 }
											 
									 },
									 failure: 	function(form, action){ 
										fld.bandera = 'N';
										var resp = action.result;	
										var mensaje = resp.mensaje;
										if(resp.mensaje!=''){
											Ext.ComponentQuery.query("#btnResultadoDesc")[0].setDisabled(true); 
											Ext.ComponentQuery.query('#EdoResultados')[0].markInvalid(resp.mensaje);
											Ext.ComponentQuery.query("#valueEdoResultados" )[0].setValue('');
										}
									}
								});
					  }
					  			
					  }
					}
			 }
			]
		},
		{
			xtype:'panel',
			frame: true,
			border: true,	
			cls: 'panel-sin-borde',
			layout:'table',
			width:      550,
			itemId:'pnlRfc',
			bodyPadding: '0 0 0 140',
			layoutConfig:{ columns: 2 },
			items: [
				
				{
					xtype: 'fieldcontainer',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype:'label',
							style:
							 {
								  'font-size' : '12px'
							 },
							//width:      800,
							html:' <div > <table border="0" align="right" cellpadding="0" cellspacing="0"><tr> <td align="left" valign="top" ><center>Copia del Registro Federal de Contribuyentes.pdf</center></td></tr> </table> </div> '
						}
					]
				},
				{
					xtype: 'fieldcontainer',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{	
							xtype: 'button',	
							itemId: 'btnRegFedDesc',
							disabled:true,
							iconCls: 'icoPdf',
							width:20,
							handler: function( button, event ){
								//main.el.mask('Procesando...', 'x-mask-loading');
								descargaArchivoSolic('RFC');
							}
						}
					]
				}
				
			]
		},
		{
			xtype:'displayfield',
			width: 20,
			itemId:'rutaRFC',
			name:'rutaRFC',
			hidden :true,
			allowBlank: false
		},
		
		{
			xtype: 'fieldcontainer',
			combineErrors: false,
			msgTarget: 'side',
			width: 550,
			items: [
				{
					xtype: 'filefield',
					name:   'regFedContri',
					width:      550,
					itemId:'regFedContri',
					fieldLabel: '&nbsp; Cargar Archivo de ',
					emptyText:  'Seleccione un archivo...',
					anchor:     '-20',
					msgTarget:  'side',
					//allowBlank: 	false,
					bodyPadding: '30 20 12 20',
					labelWidth	: 150,
					vtype: 			'archivopdf',
					buttonConfig:  { iconCls: 'upload-icon' },
					buttonText: '',
					bandera :'N',
					listeners: {
					  change: function (fld, value) {
							
							if(value !=''){
								var formaCargarArchivo  = Ext.ComponentQuery.query("#fpDocumentacion" )[0];
								var params = formaCargarArchivo.getForm().getValues();
								
							formaCargarArchivo.getForm().submit({
								 clientValidation: true,
								  url: 		'39IfnbCalificate.data.jsp?informacion=cargaArchivosEstadoFinanciero&tipoArchivo=RFC',
								 params:		params,
								 success:	function(form, action){ 
									var resp = action.result;
									if(!resp.ok_file){
										
										Ext.ComponentQuery.query("#btnRegFedDesc")[0].setDisabled(true); 
										Ext.ComponentQuery.query('#regFedContri')[0].markInvalid(resp.mensaje);
										Ext.ComponentQuery.query('#rutaRFC')[0].setValue('');
									 }else{
										Ext.ComponentQuery.query("#valueRFC" )[0].setValue(value);
										Ext.ComponentQuery.query("#btnRegFedDesc")[0].setDisabled(false); 
										Ext.ComponentQuery.query('#rutaRFC')[0].setValue(resp.nombreArchivo);
										
									 }
								},
								 failure: 	function(form, action){ 
									var resp = action.result;	
									var mensaje = resp.mensaje;
									if(resp.mensaje!=''){
										Ext.ComponentQuery.query('#rutaRFC')[0].setValue('');
										Ext.ComponentQuery.query("#btnRegFedDesc")[0].setDisabled(true); 
										Ext.ComponentQuery.query('#regFedContri')[0].markInvalid(resp.mensaje);
									}
								}
							});
					  }
					  
					  }
					}
			 }
			]
		}
	];
	
	function enviaEdosFinancieros(){
		
		var winCambioPerfil =Ext.ComponentQuery.query('winDocPreliminar')[0];
		if(winCambioPerfil){
			
			winCambioPerfil.show();
		} else{
					new Ext.Window({
								layout: 'fit',
								modal: true,
								width: 610,
								frame:       false,
								constrain:   true,
								height: 370,
								resizable: false,
								closable: true,
								x: 450,
								itemId: 'winDocPreliminar',
								autoDestroy:false,
								closeAction: 'destroy',
								items: [
									fpDocumentacion
								],
								title: 'Documentaci�n preliminar<br>'
						}).show();	
		}
		Ext.ComponentQuery.query("#valueBalance" )[0].setValue('');
		Ext.ComponentQuery.query("#valueEdoResultados" )[0].setValue('');
		Ext.ComponentQuery.query("#valueRFC" )[0].setValue('');
		Ext.ComponentQuery.query("#btnBalanceDesc")[0].setDisabled(true); 
		Ext.ComponentQuery.query("#btnResultadoDesc")[0].setDisabled(true);
		Ext.ComponentQuery.query("#btnRegFedDesc")[0].setDisabled(true);
			
		Ext.ComponentQuery.query("#EdoSutFinan" )[0].setRawValue('');
		Ext.ComponentQuery.query("#EdoResultados" )[0].setRawValue('');
		Ext.ComponentQuery.query("#regFedContri" )[0].setRawValue('');
		
		Ext.ComponentQuery.query("#rutaBalance" )[0].setRawValue('');
		Ext.ComponentQuery.query("#rutaEdoResultados" )[0].setRawValue('');
		Ext.ComponentQuery.query("#rutaRFC" )[0].setRawValue('');
		
		
	}
	
	
	var fpDocumentacion = {
		xtype: 'form',
		itemId					: 'fpDocumentacion',	
		width				: 600,
		height: 360,
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		//bodyPadding: '30 50 12 50',
		fileUpload: true,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 200,
		bodyStyle		: 'padding: 8px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items: elementosDocumento,
		buttons: [
			{
				text: 'Enviar',
				itemId: 'btnEnviar',
				iconCls: 'icoContinuar',
				hidden: false,
				formBind: true,
				handler: function( boton, evento){
					var nombreBalance = Ext.ComponentQuery.query('#rutaBalance')[0];
					var nombreResultado = Ext.ComponentQuery.query('#rutaEdoResultados')[0];
					var nombreRegFed = Ext.ComponentQuery.query('#rutaRFC')[0];
					
					if( !Ext.isEmpty(nombreBalance.getValue() ) && !Ext.isEmpty(nombreResultado.getValue() )&&!Ext.isEmpty(nombreRegFed.getValue() )){
						var formaCargarArchivo  = Ext.ComponentQuery.query("#fpDocumentacion" )[0];
						if(formaCargarArchivo.getForm().isValid()){
							boton.disable();
							boton.setIconCls('x-mask-msg-text');
							Ext.Ajax.request({
									url: '39IfnbCalificate.data.jsp',
										params:Ext.apply({
										informacion: 'Enviar_estado_Finnancieros',
										ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
										estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue(),
										nombreBalance:nombreBalance.getValue(),
										nombreResultado:nombreResultado.getValue(),
										nombreRegFed:nombreRegFed.getValue()
									}),
									callback: procesarEnviaEdoFinan
								});
						}
					}else{
						Ext.Msg.alert('Mensaje','Debe adjuntar todos los documentos para efectuar �sta acci�n');
						return;
					}
				}
			}			
		]	
	};
	
	var tp = Ext.create('Ext.tab.Panel', {
		itemId:'id_tp',
		border: true,
		frame: false,
		plain:true,
		activeTab:0,
		style:			'margin:0 auto;',
		width: 'auto',
		items: 
		[
			{
				title:	'<p align="center">Paso 1. Datos Generales</p>',
				itemId:	'datosGrales',
				width: 800,
				height:450,
				disabled:true,
				items: 
				[
					{
						xtype:'textfield',
						width: 100,
						itemId: 'ic_solicitud',
						name:'ic_solicitud',
						hidden:true
					},
					{
						xtype:'textfield',
						width: 100,
						itemId: 'estatus_solic_inicial',
						name:'estatus_solic_inicial',
						hidden:true
					}
				]
			},
			{
				title:	'<p align="center"><b>Paso 2. Informaci�n Cualitativa</b></p>',
				itemId:	'infCualitativa',
				disabled:true,
				width: 800,
				height:600,
				items: 
				[
				
				]
			},
			{
				title:	'<p align="center"><b>Paso 3. Cr�dito Vinculado</b></p>',
				itemId:	'ceditoVinculado',
				disabled:true,
				width: 800,
				height:400,
				items: 
				[
				
				]
			},
			{
				title:	'<p align="center"><b>Paso 4. informaci�n Financiera</b></p>',
				itemId:	'infoFinanciera',
				disabled:true,
				width: 800,
				height:450,
				items: 
				[
				
				]
			},
			{
				title:	'<p align="center"><b>Paso 5. Saldos</b></p>',
				itemId:	'infoSaldos',
				disabled:true,
				width: 800,
				height:550,
				items: 
				[
				
				]
			},
			{
				title:	'<p align="center"><b>Resultado Indicadores Financieros</b></p>',
				itemId:	'IndicadorFinanciero',
				disabled:true,
				width: 800,
				height:400,
				items: 
				[
				
				]
			}
			
			
		],
		defaults: {
			listeners: {
				activate: function(tab, eOpts) {
					if(tab.getItemId()!='datosGrales'){
						if(fpDG!=null){
							var p1 = Ext.ComponentQuery.query('#datosGrales')[0];
							fpDG.destroy();
							p1.doLayout();
						}
					}
					if(tab.getItemId()!='infCualitativa'){
						if(fpIC!=null){
							var p2 = Ext.ComponentQuery.query('#infCualitativa')[0];
							fpIC.destroy();
							p2.doLayout();
						}
					}
					if(tab.getItemId()!='ceditoVinculado'){
						if(fpCredVinc!=null){
							var p3 = Ext.ComponentQuery.query('#ceditoVinculado')[0];
							fpCredVinc.destroy();
							p3.doLayout();
						}
					}
					if(tab.getItemId()!='infoFinanciera'){
						if(fpInfFinanciera!=null){
							var p4 = Ext.ComponentQuery.query('#infoFinanciera')[0];
							fpInfFinanciera.destroy();
							p4.doLayout();
						}
					}
					if(tab.getItemId()!='infoSaldos'){
						if(fpSaldos!=null){
							var p5 = Ext.ComponentQuery.query('#infoSaldos')[0];
							fpSaldos.destroy();
							p5.doLayout();
						}
					}
					if(tab.getItemId()!='IndicadorFinanciero'){
						if(fpIndicadorF!=null){
							var p6 = Ext.ComponentQuery.query('#IndicadorFinanciero')[0];
							fpIndicadorF.destroy();
							p6.doLayout();
						}
					}
					tab.setDisabled(false);
					getDatosPest(tab.getItemId());
					
					
				}
			}
		},
		buttons: 
		[
			{
				 text: 'Cancelar',
				 itemId:'btnCancelar',
				 hidden:true,
				 iconCls:'icoCancelar',
				 handler: function() {
					Ext.Msg.confirm('Confirmaci�n','<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>�Confirma cancelar el ejercicio de autoevaluaci�n  que permite saber su  viabilidad de incorporaci�n a la red de Intermediarios Financieros de NAFIN?</center></td><td></td></table> </div>',
						function(res){
							if(res=='no' || res == 'NO'){
								return;
							}else{
								window.location.href='/nafin/01principal/01cedi/index.jsp';
							}
						});
				 }
			},
			{
				 text: 'Guardar',
				 itemId:'btnGuardar',
				 iconCls:'icoGuardar',
				 disabled: true,
				 hidden:true,
				 handler: function(boton, evento) {	
					var activeTab = Ext.ComponentQuery.query('#id_tp')[0].getActiveTab();
					if(activeTab.getItemId()=='datosGrales' ){
						var infGeral = Ext.ComponentQuery.query('#datosGralesV')[0];
						var guarda = Ext.ComponentQuery.query('#guardar_activo')[0].getValue();
						if(infGeral.getForm().isValid()&&guarda=='S') {
							boton.disable();
							boton.setIconCls('x-mask-msg-text');
							
							Ext.Ajax.request({
								url: '39IfnbCalificate.data.jsp',
								params:Ext.apply(infGeral.getForm().getValues(), {
									informacion: 'GuardarInformacion',
									ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
									estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
								}),
								callback: procesarGuardarInformacion
							});
						}	
					}else if(activeTab.getItemId()=='infCualitativa'){
						var infCuali = Ext.ComponentQuery.query('#infCualitativaV')[0];
						var guarda = Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].getValue();
						if(infCuali.getForm().isValid()&&validaPreguntaObligatoria()&&guarda=='S'){
							Ext.Ajax.request({
								url: '39IfnbCalificate.data.jsp',
									params:Ext.apply(infCuali.getForm().getValues(),{
									informacion: 'GuardarInfoCualitativa',
									numPregReal: fpIC.numPregReal,
									ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
									estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
								}),
								callback: procesarGuardarInfoCualitativa
							});
						}
						
					}else if(activeTab.getItemId()=='ceditoVinculado'){
						var cedVinc = Ext.ComponentQuery.query('#ceditoVinculadoV')[0];	
						var guarda = Ext.ComponentQuery.query('#guardar_activo_cred')[0].getValue();
						if(cedVinc.getForm().isValid()&&guarda=='S') {
							boton.disable();
							boton.setIconCls('x-mask-msg-text');
							Ext.Ajax.request({
								url: '39IfnbCalificate.data.jsp',
									params:Ext.apply(cedVinc.getForm().getValues(),{
									informacion: 'GuardarCreditoVinculado',
									ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
									estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
								}),
								callback: procesarGuardarCreditoV
							});
						}else{
							Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
							Ext.ComponentQuery.query('#btnGuardar')[0].enable();
							return;
						}
						
					}else if(activeTab.getItemId()=='infoFinanciera'){
						var infFina = Ext.ComponentQuery.query('#infoFinancieraV')[0];
						var guarda = Ext.ComponentQuery.query('#guardar_activo_InfF')[0].getValue();
						
						if(infFina.getForm().isValid()&&guarda=='S') {
						
							guardarInfFinanciera(boton, evento);
						}
						
					}else if(activeTab.getItemId()=='infoSaldos'){
						var infFina = Ext.ComponentQuery.query('#infoSaldosV')[0];
						var guarda = Ext.ComponentQuery.query('#guardar_activo_Sal')[0].getValue();
						if(infFina.getForm().isValid()&&guarda=='S') {
							guardarSaldos();
						}
						
					}
											
				}	
				
			},
			{
				 text: 'Limpiar',
				 itemId:'btnLimpiar',
				 iconCls:'icoLimpiar',
				 hidden:true,
				 handler: function() {
					var activeTab = Ext.ComponentQuery.query('#id_tp')[0].getActiveTab();
					if(activeTab.getItemId()=='datosGrales' ){
						fpDG.getForm().reset();
					}else if(activeTab.getItemId()=='infCualitativa'){
						limpiarPestania('infCualitativa');
					}else if(activeTab.getItemId()=='ceditoVinculado'){
						limpiarPestania('ceditoVinculado');
					}else if(activeTab.getItemId()=='infoFinanciera'){
						limpiarPestania('infoFinanciera');
					}else if(activeTab.getItemId()=='infoSaldos'){
						limpiarPestania('infoSaldos');	
					}
				
				 }				
			},
			{
				 text: 'Regresar',
				 itemId:'btnRegresar',
				 renderTo: Ext.getBody(),
				 iconCls:'icoRegresar',
				 hidden:true,
				 handler: function(boton,evento) {
					
						
						var activeTab = Ext.ComponentQuery.query('#id_tp')[0].getActiveTab();
						if(activeTab.getItemId()=='infCualitativa' ){
							var infCuali = Ext.ComponentQuery.query('#infCualitativaV')[0];
							var guardar_activo = Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0];
							if(infCuali.getForm().isValid()&&validaPreguntaObligatoria()&&guardar_activo.getValue()=='N'){
								
								tp.setActiveTab('datosGrales');
							}
							if(guardar_activo.getValue()=='S'){
								Ext.Msg.alert('Aviso','Debe guardar la informaci�n para regresar ');
								return;
							}
						}else if(activeTab.getItemId()=='ceditoVinculado' ){
							
							var cedVinc = Ext.ComponentQuery.query('#ceditoVinculadoV')[0];
							var guardar_activo = Ext.ComponentQuery.query('#guardar_activo_cred')[0];
							if(cedVinc.getForm().isValid()&&guardar_activo.getValue()=='N') {
								
								tp.setActiveTab('infCualitativa');
								
							}
							if(guardar_activo.getValue()=='S'){
								Ext.Msg.alert('Aviso','Debe guardar la informaci�n para regresar ');
								return;
							}
							
						}else if(activeTab.getItemId()=='infoFinanciera' ){
							var gridInfoFin = Ext.ComponentQuery.query('#gridInformaFinan')[0];
							var store = gridInfoFin.getStore();
							var selected = store.getRange(); 
							var fechaInfinanc =  Ext.ComponentQuery.query('#fechaInfinanc')[0];
							if(Ext.isEmpty(fechaInfinanc.getValue())){
								fechaInfinanc.markInvalid('Este Campo es Obligatorio');
								fechaInfinanc.focus();
								return false;
							}
							var valor = true;
							Ext.each(selected, function(item) {
								numReg = store.indexOf(item);
								if(item.data.ANIO_1==''){
									var editor = gridInfoFin.plugins[0];
									var fila = numReg;
									Ext.Msg.alert('Error','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Este campo es obligatorio',
									function(){
										editor.startEditByPosition({row: fila, column:3});
									});
									valor= false
									return false;
								}
							});
							Ext.each(selected, function(item) {
								numReg = store.indexOf(item);
								if(item.data.ANIO_2==''){
									var editor = gridInfoFin.plugins[0];
									var fila = numReg;
									Ext.Msg.alert('Error','Este campo es obligatorio',
									function(){
										editor.startEditByPosition({row: fila, column:2});
									});
									valor= false
									return false;
								}
							});
							Ext.each(selected, function(item) {
								numReg = store.indexOf(item);
								if(item.data.ANIO_3==''){
									var editor = gridInfoFin.plugins[0];
									var fila = numReg;
									Ext.Msg.alert('Error','Este campo es obligatorio',
									function(){
										editor.startEditByPosition({row: fila, column:1});
									});
									valor= false
									return false;
								}
							});
							var infFina = Ext.ComponentQuery.query('#infoFinancieraV')[0];
							var guardar_activo = Ext.ComponentQuery.query('#guardar_activo_InfF')[0];
							if(valor&&infFina.getForm().isValid()&&guardar_activo.getValue()=='N'){
								
								tp.setActiveTab('infoSaldos');
							}
							if(guardar_activo.getValue()=='S'){
								Ext.Msg.alert('Aviso','Debe guardar la informaci�n para regresar ');
								return;
							}
						}else if(activeTab.getItemId()=='infoSaldos' ){
							var infSaldo = Ext.ComponentQuery.query('#infoSaldosV')[0];	
							var gridAcreditado = Ext.ComponentQuery.query('#gridInforAcreditados')[0];
							var gridIncumplido= Ext.ComponentQuery.query('#gridInforIncumplidos')[0];
							
							var storeAcre = gridAcreditado.getStore();
							var selectedAcre = storeAcre.getRange(); 
							
							var storeInc = gridIncumplido.getStore();
							var selectedInc = storeInc.getRange(); 
							var valor=true;
							
							
							Ext.each(selectedInc, function(item) {
								numReg = storeInc.indexOf(item);
								if(item.data.FG_SALDO==''){
									var editor = gridIncumplido.plugins[0];
									var fila = numReg;
									Ext.Msg.alert('Validaci�n','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Este campo es obligatorio',
									function(){
										editor.startEditByPosition({row: fila, column:1});
									});
								valor= false
								return false;
													}
							});
							Ext.each(selectedAcre, function(item) {
								numReg = storeAcre.indexOf(item);
								if(item.data.FG_SALDO==''){
									var editor = gridAcreditado.plugins[0];
									var fila = numReg;
									Ext.Msg.alert('Validaci�n','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Este campo es obligatorio',
									function(){
										editor.startEditByPosition({row: fila, column:1});
									});
								valor= false
								return false;
													}
							});
							
							var guardar_activo = Ext.ComponentQuery.query('#guardar_activo_Sal')[0];	
							if(valor&&infSaldo.getForm().isValid()&&guardar_activo.getValue()=='N') {
								
								tp.setActiveTab('infoFinanciera');
								
								
							}
							if(guardar_activo.getValue()=='S'){
								Ext.Msg.alert('Aviso','Debe guardar la informaci�n para regresar');
								return;
							}
						}
						Ext.ComponentQuery.query('#btnRegresar')[0].setIconCls('icoRegresar');
						Ext.ComponentQuery.query('#btnRegresar')[0].enable();
				 }
				
			},
			{
				 text: 'Continuar',
				 itemId:'btnContinuar',
				 iconCls:'icoContinuar',
				 hidden:true,
				 formBind: true,
				 handler: function(boton, evento) {
						btn_continuar = 'S';
						
						var activeTab = Ext.ComponentQuery.query('#id_tp')[0].getActiveTab();
						if(activeTab.getItemId()=='datosGrales' ){
							var infGeral = Ext.ComponentQuery.query('#datosGralesV')[0];
							var guardar_activo = Ext.ComponentQuery.query('#guardar_activo')[0];
							if(infGeral.getForm().isValid()&&guardar_activo.getValue()=='N') {
								if(existe_inf_p1==1){
									tp.child('#datosGrales').setTitle( ' <div > <table width="180" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 1. Datos Generales</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
								}
								pestania_activa = 'infCualitativa';
								tp.setActiveTab('infCualitativa');
							}
							if(guardar_activo.getValue()=='S'){
								Ext.Msg.alert('Aviso', '<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Debe guardar la informaci�n para continuar</td><td></td></table> </div>');
								return;
							}
							
						}else if(activeTab.getItemId()=='infCualitativa' ){
							var infCuali = Ext.ComponentQuery.query('#infCualitativaV')[0];
							var guardar_activo = Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0];
							if(infCuali.getForm().isValid()&&validaPreguntaObligatoria()&&guardar_activo.getValue()=='N'){
								if(existe_inf_p2==1){
									tp.child('#infCualitativa').setTitle( ' <div > <table width="180" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 2. Informaci�n Cualitativa</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
								}
								pestania_activa = 'ceditoVinculado';
								tp.setActiveTab('ceditoVinculado');
								
							}
							if(guardar_activo.getValue()=='S'){
								Ext.Msg.alert('Aviso', '<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Debe guardar la informaci�n para continuar</td><td></td></table> </div>');
								return;
							}
					
						}else if(activeTab.getItemId()=='ceditoVinculado' ){
							var cedVinc = Ext.ComponentQuery.query('#ceditoVinculadoV')[0];	
							var guardar_activo = Ext.ComponentQuery.query('#guardar_activo_cred')[0];
							if(cedVinc.getForm().isValid()&&guardar_activo.getValue()=='N') {
								if(existe_inf_p3==1){
									tp.child('#ceditoVinculado').setTitle( ' <div > <table width="180" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 3. Cr�dito Vinculado</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
								}
								pestania_activa = 'infoFinanciera';
								tp.setActiveTab('infoFinanciera');
							}
							
							Ext.ComponentQuery.query('#btnContinuar')[0].setIconCls('icoContinuar');
							Ext.ComponentQuery.query('#btnContinuar')[0].enable();
							if(guardar_activo.getValue()=='S'){
								Ext.Msg.alert('Aviso', '<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Debe guardar la informaci�n para continuar</td><td></td></table> </div>');
								return;
							}
						
					}else if(activeTab.getItemId()=='infoFinanciera' ){
						
							var gridInfoFin = Ext.ComponentQuery.query('#gridInformaFinan')[0];
							var store = gridInfoFin.getStore();
							var selected = store.getRange(); 
							var fechaInfinanc =  Ext.ComponentQuery.query('#fechaInfinanc')[0];
							if(Ext.isEmpty(fechaInfinanc.getValue())){
								fechaInfinanc.markInvalid('Este Campo es Obligatorio');
								fechaInfinanc.focus();
								return false;
							}
							var valor = true;
							Ext.each(selected, function(item) {
								numReg = store.indexOf(item);
								if(item.data.ANIO_1==''){
									var editor = gridInfoFin.plugins[0];
									var fila = numReg;
									Ext.Msg.alert('Error','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Este campo es obligatorio',
									function(){
										editor.startEditByPosition({row: fila, column:3});
									});
									valor= false
									return false;
								}
							});
							Ext.each(selected, function(item) {
								numReg = store.indexOf(item);
								if(item.data.ANIO_2==''){
									var editor = gridInfoFin.plugins[0];
									var fila = numReg;
									Ext.Msg.alert('Error','Este campo es obligatorio',
									function(){
										editor.startEditByPosition({row: fila, column:2});
									});
									valor= false
									return false;
								}
							});
							Ext.each(selected, function(item) {
								numReg = store.indexOf(item);
								if(item.data.ANIO_3==''){
									var editor = gridInfoFin.plugins[0];
									var fila = numReg;
									Ext.Msg.alert('Error','Este campo es obligatorio',
									function(){
										editor.startEditByPosition({row: fila, column:1});
									});
									valor= false
									return false;
								}
							});
							var infFina = Ext.ComponentQuery.query('#infoFinancieraV')[0];
							var guardar_activo = Ext.ComponentQuery.query('#guardar_activo_InfF')[0];
							if(valor&&infFina.getForm().isValid()&&guardar_activo.getValue()=='N'){
								if(existe_inf_p4==1){
									tp.child('#infoFinanciera').setTitle( ' <div > <table width="180" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Paso 4. Informaci�n Financiera</td><td><p class="icoGuardarCEDI"/></td></table> </div> ' );
								}
								pestania_activa = 'infoSaldos';
								tp.setActiveTab('infoSaldos');
								
							}
							if(guardar_activo.getValue()=='S'){
								Ext.Msg.alert('Aviso', '<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Debe guardar la informaci�n para continuar</td><td></td></table> </div>');
								return;
							}
					}else if(activeTab.getItemId()=='infoSaldos' ){
						var guardar_activo = Ext.ComponentQuery.query('#guardar_activo_Sal')[0];
						var infSaldo = Ext.ComponentQuery.query('#infoSaldosV')[0];			
						if(infSaldo.getForm().isValid()&&guardar_activo.getValue()=='N') {
							funcionFinalizar();	
						}
						if(guardar_activo.getValue()=='S'){
							Ext.Msg.alert('Aviso', '<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td > Debe guardar la informaci�n para continuar</td><td></td></table> </div>');
							return;
						}
						
					}
				}
			},
			{
				xtype:'displayfield',
				width: 600,
				hidden:true,
				itemId:'btnMensaje',
				value:''
			},
			{
				 text: 'Aceptar',
				 itemId:'btnAceptar',
				 iconCls:'icoAceptar',
				 hidden:true,
				 handler: function() {
					var estatus =Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue();
					cedula_activa = true;
					click_finalizar = "N";
					consulInfoAspirante.load();
					consulIndicadores.load();
					consulInfoCuali.load();
					conteCedula.show();
					gridIndicadorCedula.show();
					gridInfoCuali.show();
					var title ="";
					if(estatus==2){
						var pestDatosGerales = tp.child('#IndicadorFinanciero');
						tp.setActiveTab(pestDatosGerales);
					}else if(estatus==7){
						title = 'C�DULA ELECTR�NICA DE RECHAZO';
						Ext.ComponentQuery.query('#leyendaRechazo')[0].show();
						Ext.ComponentQuery.query('#pnlAspirante')[0].setTitle('<center>'+title+'</center>');
						var pestDatosGerales = tp.child('#datosGrales');
						tp.setActiveTab(pestDatosGerales);
					}
				 }
				
			},
			{
				 text: 'Aceptar',
				 itemId:'btnAceptar2v',
				 iconCls:'icoAceptar',
				 hidden:true,
				 handler: function(boton, evento) {
					boton.disable();
					boton.setIconCls('x-mask-msg-text');
								
					Ext.Ajax.request({
						url: '39IfnbCalificate.data.jsp',
						params: Ext.apply({
							informacion:  'Aceptar_proceso',
							ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
							estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
						}),
						callback: procesaEstatusSolicitus2V
					});
					
					
				 }
				
			},
			{
				 text: 'Actualizar',
				 itemId:'btnActualizar',
				 iconCls:'icoActualizar',
				 disabled:true,
				 handler: function(boton, evento) {
					//boton.disable();
					//boton.setIconCls('x-mask-msg-text');
					var gridInfoFin = Ext.ComponentQuery.query('#gridAnioDictaminado')[0];
					var storeInfoFin = gridInfoFin.getStore(); 
					var gridIndicador = Ext.ComponentQuery.query('#gridIndicador')[0];
					gridInfoFin.el.mask('Actualizando...', 'x-mask-loading');
					gridIndicador.el.mask('Actualizando...', 'x-mask-loading');
					var storeIndicador = gridIndicador.getStore(); 
					storeIndicador.load({
						params: Ext.apply(
								{
									informacion: 'Consulta_Indicador_Financiero',
									ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
								})
					});
					storeInfoFin.load({
						params: Ext.apply(
								{
									informacion: 'Consulta_anio_dictaminado',
									ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue()
								})
					});
					Ext.ComponentQuery.query('#btnAceptar2v')[0].setDisabled(false);
				}
				
			},
			{
				 text: 'Ver C�dula Previa',
				 itemId:'btnVerCedRechazo',
				 renderTo: Ext.getBody(),
				 iconCls:'icoPdf',
				 hidden:true,
				  handler: function(boton, evento) {
					 //boton.disable();
					// boton.setIconCls('x-mask-msg-text');
					 Ext.Ajax.request({
						url: '39IfnbCalificate.data.jsp',
						params: Ext.apply({
							informacion:  'Descarga_ArchivoPDF_Cedula',
							ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
							estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
				
						}),
						callback: procesaGeneraPDF
					});
				}
				
			},
			{
				 text: 'Enviar Estados Financieros',
				 itemId:'btnEnviarEdoFinan',
				 renderTo: Ext.getBody(),
				 iconCls:'upload-icon',
				 hidden:true,
				 handler: function() {
					  enviaEdosFinancieros();
				}
				
			},
			{
				 text: 'Ver C�dula Previa',
				 itemId:'btnVerCedRechazo2v',
				 renderTo: Ext.getBody(),
				 iconCls:'icoPdf',
				 hidden:true,
				 handler: function(boton, evento) {
					// boton.disable();
					 //boton.setIconCls('x-mask-msg-text');
					 Ext.Ajax.request({
						url: '39IfnbCalificate.data.jsp',
						params: Ext.apply({
							informacion:  'Descarga_ArchivoPDF_Cedula',
							ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
							estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
				
						}),
						callback: procesaGeneraPDF
					});
				}
				
			},
			{
				 text: 'Generar PDF',
				 itemId:'btnGerarPDF',
				 renderTo: Ext.getBody(),
				 iconCls:'icoPdf',
				 hidden:true,
				 handler: function(boton, evento) {
					 boton.disable();
					 boton.setIconCls('x-mask-msg-text');
					 Ext.Ajax.request({
						url: '39IfnbCalificate.data.jsp',
						params: Ext.apply({
							informacion:  'Descarga_ArchivoPDF_Cedula',
							ic_solicitud:Ext.ComponentQuery.query('#ic_solicitud')[0].getValue(),
							estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
				
						}),
						callback: procesaGeneraPDF
					});
				}
				
			},
			{
				 text: 'Salir',
				 itemId:'btnSalir',
				 renderTo: Ext.getBody(),
				 iconCls:'icoSalir',
				 hidden:true,
				 handler: function() {
					var tab = Ext.ComponentQuery.query('#id_tp')[0].getActiveTab();
					if(tab.getItemId()!='datosGrales'){
						if(fpDG!=null){
							var p1 = Ext.ComponentQuery.query('#datosGrales')[0];
							fpDG.destroy();
							p1.doLayout();
						}
					}
					if(tab.getItemId()!='infCualitativa'){
						if(fpIC!=null){
							var p2 = Ext.ComponentQuery.query('#infCualitativa')[0];
							fpIC.destroy();
							p2.doLayout();
						}
					}
					if(tab.getItemId()!='ceditoVinculado'){
						if(fpCredVinc!=null){
							var p3 = Ext.ComponentQuery.query('#ceditoVinculado')[0];
							fpCredVinc.destroy();
							p3.doLayout();
						}
					}
					if(tab.getItemId()!='infoFinanciera'){
						if(fpInfFinanciera!=null){
							var p4 = Ext.ComponentQuery.query('#infoFinanciera')[0];
							fpInfFinanciera.destroy();
							p4.doLayout();
						}
					}
					if(tab.getItemId()!='infoSaldos'){
						if(fpSaldos!=null){
							var p5 = Ext.ComponentQuery.query('#infoSaldos')[0];
							fpSaldos.destroy();
							p5.doLayout();
						}
					}
					if(tab.getItemId()!='IndicadorFinanciero'){
						if(fpIndicadorF!=null){
							var p6 = Ext.ComponentQuery.query('#IndicadorFinanciero')[0];
							fpIndicadorF.destroy();
							p6.doLayout();
						}
					}
					cedula_activa = false;
					tp.show();
					conteCedula.hide();
					gridIndicadorCedula.hide();
					gridInfoCuali.hide();
					activa_acualizar = 'N';
					ban_actualizar =  'N';
					if(estatus_sol==7||estatus_sol==8){
						ban_doctos_enviados = '';
						solicitud_viable ='';
					}
					
					getDatosPest(tab.getItemId());
					
					Ext.ComponentQuery.query('#btnMensaje')[0].hide();
					Ext.ComponentQuery.query('#btnEnviarEdoFinan')[0].hide();
					Ext.ComponentQuery.query('#btnGerarPDF')[0].hide();
					Ext.ComponentQuery.query('#btnSalir')[0].hide();
					Ext.ComponentQuery.query('#btnGuardar')[0].enable();
					
				}
				
			}
		]
	});
	
	//*** -- Elementos de la C�dula de aceptaci�n --***
		
	var mensajeCont = Ext.create('Ext.container.Container', {
		width:	800,
		heigth:	'auto',
		bodyPadding: '30 50 12 50',
		style: 'margin:0 auto;',
		hidden:true,
		items: [	
			{
				xtype:'label',
					style:
                {
                    'font-size' : '12px'
                },
				width:	750,
				html:'<center><b>Resultado de elegibilidad.</b></center><br><br>'
			}				
		]
	});
	Ext.define('ListaInfoAspirante',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'DATOS_INFO'     },
				{name: 'REQUISITO_INFO'     }
			]
	});
	var consulInfoAspirante = Ext.create('Ext.data.Store',{
			model: 'ListaInfoAspirante',
			storeId:'consulInfoAspirante',
			itemId :'storeInfoAspitante',
			proxy: {
				type: 'ajax',
				url:  '39IfnbCalificate.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'informacion_aspirante_store',
					estatus_solic_inicial:Ext.ComponentQuery.query('#estatus_solic_inicial')[0].getValue()
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: false
		});
	var gridInfoAspirante=Ext.create('Ext.grid.Panel', {
		 store: Ext.data.StoreManager.lookup('consulInfoAspirante'),
		hidden:false,
		border	: true,
		hideHeaders:true,
		columns: [
			  {
				dataIndex: 'REQUISITO_INFO',
				width: 263
			  },
			  {
				dataIndex: 'DATOS_INFO',
				width: 263
			  } 
		],
		 height: 220,
		 width: 540
	});
	var elementosCedula =[
		
		{
			xtype: 'panel',
			frame: true,
			itemId:'pnlAspirante',
			border	: false,
			 width: 540,
			 title:'',
			items: gridInfoAspirante
		},
		{
			xtype: 'panel',
			frame: true,
			border	: false,
			itemId:'leyendas',
			width:540,
			items: [
				{
					xtype: 'fieldcontainer',
					itemId:'leyendaRechazo',
					hidden:true,
					items: [
						{
							xtype:'label',
							style:
							 {
								  'font-size' : '12px'
							 },
							 html:'<table >'+
									'<tr><td height="30"><p style="text-align:justify; font-size:100%"><CENTER>El proyecto no es elegible para financiamiento.</CENTER></p></td></tr>'+
									'<tr><td height="50"><p style="text-align:justify; font-size:100%">En respuesta a la informaci�n Financiera que amablemente requisit�, y con base al estudio pertinente realizado, el resultado no fue favorable para incorporarse a la Red de Intermediarios Financieros de Nacional Financiera.</p></td></tr>'+
									'<tr><td height="30"><p style="text-align:justify; font-size:100%">Para cualquier duda o aclaraci�n favor de contactarnos al correo electr�nico <u>cedi@nafin.gob.mx</u> o al tel�fono 01 800 NAFINSA (623 4672).</p></td></tr>'+
									'<tr><td height="30" ><p style="text-align:justify; font-size:100%">Agradecemos su inter�s en trabajar con Nacional Financiera.<br></p></td></tr>'+
								 '</table>'
						}
					]
				}
				
			]
		}
		
		
		
	];
		
	var conteCedula = new Ext.form.FormPanel({
		itemId					: 'fpConteCedula',	
	//	layout			: 'form',
		width				: 550,
		height: 'auto',
		style				: ' margin:0 auto;',
		frame				: true,
		hidden			: false,
		collapsible		: false,
		titleCollapse	: false	,
		labelWidth	: 200,
		hidden:true,
		
		items:elementosCedula 
	});
	
	
	Ext.define('ListaIndicadorCed',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_ELEMENTO_CEDULA'     },
				{name: 'CG_NOMBRE'     },
				{name: 'CS_RESULTADO'     },
				{name: 'CS_VIABLE'     },
				{name: 'CS_REQUISITO'     }
			]
	});
	var consulIndicadores = Ext.create('Ext.data.Store',{
			model: 'ListaIndicadorCed',
			storeId:'consulIndicadores',
			itemId :'storeIndicadorCed',
			proxy: {
				type: 'ajax',
				url:  '39IfnbCalificate.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'informacion_indicadores'
				},
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: false
		});
	var gridIndicadorCedula=Ext.create('Ext.grid.Panel', {
		 store: Ext.data.StoreManager.lookup('consulIndicadores'),
		 frameHeader:true,
		 title:'Indicadores y Criterios Financieros',
		 hidden:true,
		 columns: [
			  { 
				header: 'Raz�n Financiera',
				tooltip: 'Raz�n Financiera',
				dataIndex: 'CG_NOMBRE',
				sortable: true,
				width: 250
			 },
			  { 
				header: 'Su resultado',
				tooltip: 'Su resultado', 
				dataIndex: 'CS_RESULTADO',
				width: 100,
				sortable: true,
				align:'center'
			 },
			  { 
				header: 'Validaci�n',
				tooltip: 'Validaci�n', 
				dataIndex: 'CS_VIABLE',
				width: 110,
				sortable: true,
				align:'center'
			 },
			  { 
				header: 'Requisitos m�nimos para incorporaci�n',
				tooltip: 'Requisitos m�nimos para incorporaci�n', 
				dataIndex: 'CS_REQUISITO',
				width: 450,
				sortable: true,
				align:'left',
				renderer: function(value, metaData) {              
				  metaData.tdAttr = metaData.tdAttr || '';              
				  metaData.tdAttr = 'data-qtip="' + value + '"';
				  return value;
				}
			 }
		 ],
		 height: 220,
		 width: 915
	});
	
	Ext.define('ListaInfoCuali',{
			extend: 'Ext.data.Model',
			fields: [
				{name: 'IC_ELEMENTO_CEDULA'     },
				{name: 'CG_NOMBRE'     },
				{name: 'CS_RESULTADO'     },
				{name: 'CS_VIABLE'     },
				{name: 'CS_REQUISITO'     }
			]
	});
	var consulInfoCuali = Ext.create('Ext.data.Store',{
			model: 'ListaInfoCuali',
			storeId:'consulInfoCuali',
			itemId :'storeInfoCuali',
			proxy: {
				type: 'ajax',
				url:  '39IfnbCalificate.data.jsp',
				reader: {
					type: 'json',
					root: 'registros'
				},
				extraParams: {
					informacion: 'informacion_cualitativa_store'
				},
				totalProperty:   'total',
				messageProperty: 'msg',
				autoLoad:        false,
				listeners: {
					exception: NE.util.mostrarProxyAjaxError
				}
			},
			autoLoad: false
		});
	var gridInfoCuali=Ext.create('Ext.grid.Panel', {
		 store: Ext.data.StoreManager.lookup('consulInfoCuali'),
		 frameHeader:true,
		 title:'Informaci�n Cualitativa',
		 hidden:true,
		 columns: [
			  { 
				header: 'Raz�n Financiera',
				tooltip: 'Raz�n Financiera',
				dataIndex: 'CG_NOMBRE',
				width: 450,
				sortable: true
			  },
			  { 
				header: 'Su resultado',
				tooltip: 'Su resultado', 
				dataIndex: 'CS_RESULTADO',
				width: 100,
				sortable: true,
				align:'center'
			  },
			  { 
				header: 'Validaci�n',
				tooltip: 'Validaci�n', 
				dataIndex: 'CS_VIABLE',
				sortable: true,
				width: 120,
				align:'center'
			  },
			  { 
				header: 'Requisitos m�nimos para incorporaci�n',
				tooltip: 'Requisitos m�nimos para incorporaci�n', 
				dataIndex: 'CS_REQUISITO',
				width: 240,
				sortable: true,
				align:'left'
			 }
		 ],
		 height: 320,
		 width: 915
	});
	var mnsInicioPantalla = Ext.create('Ext.container.Container', {
		width:	800,
		heigth:	'auto',
		bodyPadding: '30 50 12 50',
		style: 'margin:0 auto;',
		hidden:true,
		items: [	
			{
				xtype:'panel',
				frame: false,
				border	: true,
				width: 750,
				layout:'form',
				bodyPadding: '30 100 12 100',
				bodyStyle: 'padding: 6px',
				items: [
					{
						xtype:'label',
							style:
							 {
								  'font-size' : '12px'
							 },
						width:	400,
						html:'<center>Su solicitud ha sido considerada viable para incorporarse a la red de Intermediarios Financieros de Nacional Financiera.<br><br>Si desea mayor informaci�n favor de comunicarse al correo electr�nico cedi@nafin.gob.mx o al tel�fono 01 800 NAFINSA (01 800 623 4672)</center>'
					}
				]
			}
		]
	});
	
	
	//*** TERMINO DE ELEMENTOS DE LA C�DULA ***
	

	var main = Ext.create('Ext.container.Container', {
		itemId: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 940,
		
        padding: '10 10 10 10',
		items: [	
			//Elemetos de la c�dula	
			NE.util.getEspaciador(10),
			conteCedula,
			NE.util.getEspaciador(10),
			gridIndicadorCedula,
			NE.util.getEspaciador(10),
			gridInfoCuali,
			NE.util.getEspaciador(10),	
			tp,
			//Elementos del Tab.Panel
			NE.util.getEspaciador(10),			
			mensajeCont,
			mnsInicioPantalla
		]
	});
	
	var panel = new Ext.Panel({
		renderTo:'areaLeyenda',
		width: 945,
		frame: false,
		html: '<table border=2><tr><td><p style="text-align:justify; background:#F3F4F5; font-size:80%">La informaci�n debidamente obtenida y autorizada por el administrador de la plataforma de NAFINET (Nacional Financiera S.N.C., I.B.D) '+
				'tendr� valor probatorio en juicio, en tanto que la obtenci�n de informaci�n almacenada en la base de datos y archivos de la misma, ' + 
				' sin contar con la autorizaci�n correspondiente, o el uso indebido de dicha informaci�n, ser� sancionada en t�rminos de Ley de Instituciones ' +
				' de Cr�dito y dem�s ordenamientos legales aplicables. </p></td></tr></table>'
	})
	
});
