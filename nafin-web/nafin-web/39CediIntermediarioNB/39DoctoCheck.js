
Ext.ns('NE.DoctosCheck');

		
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};		
			
			var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();							
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var descargaDoctos = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		var ic_doc_chekList = registro.get('IC_CHECKLIST');
		var extension = registro.get('CG_EXTENSION');
		
		Ext.Ajax.request({
			url: '/nafin/39CediIntermediarioNB/39DoctoCheck.data.jsp',
			params: {		
				informacion:'descargaDoctos',
				no_solicitud:no_solicitud,				
				ic_doc_chekList:ic_doc_chekList, 
				extension: extension
			},
			callback: procesarDescargaArchivos
		});		
	}
			
	Ext.define('LisDoctos', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'NO_SOLICITUD'},
			{ name: 'IC_CHECKLIST'},
			{ name: 'CG_EXTENSION'},
			{ name: 'NOMBRE_DOCTO'}	
		 ]
	});

	var consDoctosData = Ext.create('Ext.data.Store', {
		model: 'LisDoctos',
		proxy: {
			type: 'ajax',
			url: '/nafin/39CediIntermediarioNB/39DoctoCheck.data.jsp',
			reader: 		 { type: 'json',  root: 'registros' 	},
			extraParams: { informacion: 'consDoctosData' 	},
			listeners:   { exception: NE.util.mostrarProxyAjaxError 	}
		},
		autoLoad: false		
	});
		
	
	var gridDoctos = {		
		itemId: 'gridDoctos',
		store: consDoctosData,
		height: 300,
		width: 320,
		xtype: 'grid',
		style: 'margin: 10px auto 0px auto;',		     
      frame: true,				
		columns: [
			{
				xtype		: 'actioncolumn',
				header: '<center>Documentos Adicionales</center>',
				tooltip: 'Documentos Adicionales',
				dataIndex: 'NOMBRE_DOCTO',
				sortable: true,
				width: 300,			
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,record){ 
					return record.data['NOMBRE_DOCTO'];				
				},
				items: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							var icono;
							if(record.data['CG_EXTENSION']=='pdf'){   icono='icoPdf'; }
							if(record.data['CG_EXTENSION']=='csv' || record.data['CG_EXTENSION']=='xls' || record.data['CG_EXTENSION']=='xlsx'){   icono='icoXls'; }
							if(record.data['CG_EXTENSION']=='doc'  || record.data['CG_EXTENSION']=='docx' ){   icono='icoDoc'; }
							if(record.data['CG_EXTENSION']=='pptx' || record.data['CG_EXTENSION']=='ppt' ){   icono='icoPpt'; }
							this.items[0].tooltip = 'Ver';
							return icono; 
						}
						,handler: descargaDoctos
					}
				]			
			}
		]				 
	}
	
	NE.DoctosCheck.VistaDoctos = function (no_solicitud ){
	
		consDoctosData.load({
			params: {							
				no_solicitud:no_solicitud				
			}
		});
		
		new Ext.Window({
			modal: true,
			width: 330,
			resizable: false,
			closable:true,
			id: 'Vista',
			autoDestroy:false,
			closeAction: 'destroy',			
			items: [
				gridDoctos
			]
		}).show().setTitle('Documentos Adicionales');
	
	}

	