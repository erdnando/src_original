<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		com.netro.cedi.*,
		java.sql.*,		
		netropology.utilerias.*,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="39secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
String ic_doc_chekList = (request.getParameter("ic_doc_chekList")!=null)?request.getParameter("ic_doc_chekList"):"";
String extension = (request.getParameter("extension")!=null)?request.getParameter("extension"):"";

String infoRegresar	=	"";
JSONObject jsonObj = new JSONObject();	


Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 

if ( informacion.equals("consDoctosData")  ){
	
	List datos =  cediBean.getCheckList (no_solicitud ,  "N"  ); 
	JSONArray registros = new JSONArray();
	registros = registros.fromObject(datos);
	
	String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";	
	jsonObj = new JSONObject();	
	jsonObj = jsonObj.fromObject(consulta);	
  
	jsonObj.put("success", new Boolean(true));	
	infoRegresar = jsonObj.toString(); 
	
	
}else  if ( informacion.equals("descargaDoctos")  ){
	
	String nombreArchivo  =  	cediBean.desArchSolicCheckList(no_solicitud, strDirectorioTemp, ic_doc_chekList, extension );
  
	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 
	

}
   

%>
<%=infoRegresar%> 


