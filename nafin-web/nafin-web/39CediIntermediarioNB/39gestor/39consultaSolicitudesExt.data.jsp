<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.text.*,
	java.util.*,
	java.io.File,
	org.apache.commons.logging.Log,
	com.netro.pdf.*,
	com.netro.exception.*,
	netropology.utilerias.usuarios.Usuario,
	netropology.utilerias.usuarios.UtilUsr,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	com.netro.cedi.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/39CediIntermediarioNB/39secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion    = request.getParameter("informacion")     == null?"":(String)request.getParameter("informacion");
String operacion      = request.getParameter("operacion")       == null?"":(String)request.getParameter("operacion");
String razonSocial    = request.getParameter("razon_social")    == null?"":(String)request.getParameter("razon_social");
String ejecutivo      = request.getParameter("ejecutivo")       == null?"":(String)request.getParameter("ejecutivo");
String estatus        = request.getParameter("estatus")         == null?"":(String)request.getParameter("estatus");
String numFolio       = request.getParameter("num_folio")       == null?"":(String)request.getParameter("num_folio");
String fechaIni       = request.getParameter("fecha_ini")       == null?"":(String)request.getParameter("fecha_ini");
String fechaFin       = request.getParameter("fecha_fin")       == null?"":(String)request.getParameter("fecha_fin");
String infoIf         = request.getParameter("info_if")         == null?"":(String)request.getParameter("info_if");
String icSolicitud    = request.getParameter("ic_solicitud")    == null?"":(String)request.getParameter("ic_solicitud");
String tipoArchivo    = request.getParameter("tipo_archivo")    == null?"":(String)request.getParameter("tipo_archivo");
String icDocumento    = request.getParameter("ic_documento")    == null?"":(String)request.getParameter("ic_documento");
String extension      = request.getParameter("extension")       == null?"":(String)request.getParameter("extension");

String consulta      = "";
String mensaje       = "";
String infoRegresar  = "";
String nombreArchivo = "";
JSONObject resultado = new JSONObject();
boolean success      = true;

int start = 0;
int limit = 0;

if(!fechaIni.equals("") && fechaIni.length() > 10){
	fechaIni = fechaIni.substring(8, 10) + "/" + fechaIni.substring(5, 7) + "/" + fechaIni.substring(0, 4);
}
if(!fechaFin.equals("") && fechaFin.length() > 10){
	fechaFin = fechaFin.substring(8, 10) + "/" + fechaFin.substring(5, 7) + "/" + fechaFin.substring(0, 4);
}

log.debug("<<<<<<<<<<<<<<<");
log.debug("informacion:  <" + informacion + ">");
log.debug("razon_social: <" + razonSocial + ">"); 
log.debug("ejecutivo:    <" + ejecutivo   + ">");
log.debug("estatus:      <" + estatus     + ">");
log.debug("num_folio:    <" + numFolio    + ">");
log.debug("fecha_ini:    <" + fechaIni    + ">");
log.debug("fecha_fin:    <" + fechaFin    + ">");
log.debug("info_if:      <" + infoIf      + ">");
log.debug("ic_solicitud: <" + icSolicitud + ">");
log.debug(">>>>>>>>>>>>>>>");

if(informacion.equals("Catalogo_Razon_Social")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("CEDI_IFNB_ASPIRANTE");
	cat.setCampoClave("IC_IFNB_ASPIRANTE");
	cat.setCampoDescripcion("CG_RAZON_SOCIAL");
	cat.setOrden("CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Catalogo_Ejecutivo")){

	JSONArray jsObjArray = new JSONArray();
	List lstCatalogoEjecCEDI = new ArrayList();
	List usuarios = new ArrayList();
	UtilUsr utilUsr = new UtilUsr();

	usuarios = utilUsr.getUsuariosxAfiliado("EJE CEDI", "N");
	if(usuarios.size()>0){
		HashMap hmData = new HashMap();
		for(int i=0;i<usuarios.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuarios.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
			hmData.put("clave", loginUsuarioOPOP);
			hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);

			lstCatalogoEjecCEDI.add(hmData);
		}
	}

	usuarios = new ArrayList();
	usuarios = utilUsr.getUsuariosxAfiliado("GESTOR CEDI", "N");

	if(usuarios.size()>0){
		HashMap hmData = new HashMap();
		for(int i=0;i<usuarios.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuarios.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
			hmData.put("clave", loginUsuarioOPOP);
			hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);

			lstCatalogoEjecCEDI.add(hmData);
		}
	}

	jsObjArray = JSONArray.fromObject(lstCatalogoEjecCEDI);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

} else if(informacion.equals("Catalogo_Estatus")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("CEDICAT_ESTATUS_SOLICITUD");
	cat.setCampoClave("IC_ESTATUS_SOLICITUD");
	cat.setCampoDescripcion("CG_NOMBRE");
	cat.setOrden("CG_NOMBRE");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Consulta_Data") || informacion.equals("Descarga_Reporte_General")){

	ConsultaSolicitudes paginador = new ConsultaSolicitudes();
	paginador.setRazonSocial(razonSocial);
	paginador.setEjecutivo(ejecutivo);
	paginador.setEstatus(estatus);
	paginador.setNumFolio(numFolio);
	paginador.setFechaInicio(fechaIni);
	paginador.setFechaFin(fechaFin);
	paginador.setInfoIf(infoIf);
	paginador.setDirectorioPlantillaXlsx( strDirectorioPublicacion + "00archivos/plantillas/39CediIntermediarioNB/");
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros listaRegistros = new Registros();

	if(informacion.equals("Consulta_Data")){
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
			//System.out.println("start: " + start);
			//System.out.println("limit: " + limit);
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos (star y limit)", e);
		}

		//Esta consulta no usa paginación
/*
		try{
			listaRegistros = queryHelper.doSearch();
			consulta = listaRegistros.getJSONData();
			resultado.put("total", "" + listaRegistros.getNumeroRegistros());
			resultado.put("registros", consulta);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error en la paginacion", e);
		}
*/
		// Esta consulta es con paginación normal
/*		try {
			if (operacion.equals("generar")){
				queryHelper.executePKQuery(request);
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			log.debug("consulta: " + consulta);
			resultado = JSONObject.fromObject(consulta);
			resultado.put("perfil", strPerfil);
		} catch(Exception e) {
			throw new AppException(" Error en la paginacion", e);
		}
*/
		try {
			if (operacion.equals("generar")){
				queryHelper.executePKQuery(request);
			}
			Registros registros = queryHelper.getPageResultSet(request,start,limit);
			String loginEjecutivoCedi = "";

			// Obtengo la lista de los ejecutivos
			List usuarios       = new ArrayList();
			List usuarios1      = new ArrayList();
			UtilUsr utilUsr     = new UtilUsr();
			UtilUsr utilUsr1    = new UtilUsr();
			String loginUsuario = "";
			String nombre       = "";
			usuarios            = utilUsr.getUsuariosxAfiliado("EJE CEDI", "N");
			usuarios1           = utilUsr1.getUsuariosxAfiliado("GESTOR CEDI", "N");
			boolean usuarioEncontrado = false;
			while (registros.next()){
				usuarioEncontrado = false;
				loginEjecutivoCedi = registros.getString("LOGIN_EJECUTIVO_CEDI").toString();
				for(int i=0;i<usuarios.size();i++){
					loginUsuario = (String)usuarios.get(i);
					//System.err.println("0.- loginEjecutivoCedi: <" + loginEjecutivoCedi + ">. loginUsuario: <" + loginUsuario + ">"); //TODO: Debug
					if(loginEjecutivoCedi.equals(loginUsuario)){
						Usuario usuario = utilUsr.getUsuario(loginUsuario);
						nombre = usuario.getApellidoPaterno()+" "+usuario.getApellidoMaterno()+" "+usuario.getNombre();
						usuarioEncontrado = true;
						break;
					} else{
						usuarioEncontrado = false;
						nombre = "";
					}
				}
				if(usuarioEncontrado == false){
					for(int i=0;i<usuarios1.size();i++){
						loginUsuario = (String)usuarios1.get(i);
						//System.err.println("1.- loginEjecutivoCedi: <" + loginEjecutivoCedi + ">. loginUsuario: <" + loginUsuario + ">"); // TODO:Debug
						if(loginEjecutivoCedi.equals(loginUsuario)){
							Usuario usuario = utilUsr1.getUsuario(loginUsuario);
							nombre = usuario.getApellidoPaterno()+" "+usuario.getApellidoMaterno()+" "+usuario.getNombre();
							break;
						} else{
							nombre = "";
						}
					}
				}

				registros.setObject("EJECUTIVO_CEDI", nombre);
			}
			consulta = "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
			resultado = JSONObject.fromObject(consulta);
			resultado.put("perfil", strPerfil);
		} catch(Exception e) {
			throw new AppException(" Error en la paginacion", e);
		}

	} else if(informacion.equals("Descarga_Reporte_General")){
		try{
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "XLSX");
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}

	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consulta_Data_Bitacora") || informacion.equals("Descarga_Bitacora_Movimientos")){

	String loginUsr     = "";
	String nombreUsr    = "";
	Usuario usuario     = null;
	UtilUsr utilUsr     = new UtilUsr();
	Registros registros = new Registros();
	HashMap usuarios    = new HashMap();

	ConsultaMovimientos movimientos = new ConsultaMovimientos();
	movimientos.setIcSolicitud(icSolicitud);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(movimientos);

	if(informacion.equals("Consulta_Data_Bitacora")){
		try{
			// Realizo la consulta
			registros = queryHelper.doSearch();
			// Obtengo el nombre del usuario
			while (registros.next()){
				loginUsr = registros.getString("LOGIN");
				if(!loginUsr.equals("")){
					usuario = utilUsr.getUsuario(loginUsr);
					nombreUsr = usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
				} else{
					nombreUsr = "";
				}
				registros.setObject("NOMBRE_USUARIO", nombreUsr);
			}
			consulta = registros.getJSONData();
			resultado.put("total", "" + registros.getNumeroRegistros());
			resultado.put("registros", consulta);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error en la paginacion", e);
		}
	} else if(informacion.equals("Descarga_Bitacora_Movimientos")){
		try{
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Exception e) {
			success = false;
			throw new AppException("Error al generar el PDF", e);
		}
	}

	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Descarga_Archivos_Solicitud")){

	/* Los tipos de archivo son: balance, resultados, rfc */
	try{
		Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class);
		nombreArchivo = cediBean.descargaArchivosSolic(icSolicitud, strDirectorioTemp, tipoArchivo);
		if(nombreArchivo.equals("")){
			mensaje = "No existe archivo para esta solicitud";
			success = false;
		} else{
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	} catch(Exception e) {
		success = false;
		throw new AppException("Error al generar el PDF", e);
	}
	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Descarga_Cedula_Electronica")){

	try{
		Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class);
		List datoGrales = new ArrayList();
		datoGrales.add((String)session.getAttribute("strPais"));
		datoGrales.add("1");
		datoGrales.add(strNombre);
		datoGrales.add(strNombreUsuario);
		datoGrales.add(strLogo);
		datoGrales.add(strDirectorioTemp);
		datoGrales.add(strDirectorioPublicacion);
		datoGrales.add(icSolicitud);
		datoGrales.add(strPerfil);
		datoGrales.add(strTipoUsuario);
		datoGrales.add(estatus);
		datoGrales.add("Consulta de Solicitudes");
		nombreArchivo = cediBean.generarArchivoCedula(datoGrales);
		if(nombreArchivo.equals("")){
			mensaje = "No existe archivo para esta solicitud";
			success = false;
		} else{
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	} catch(Exception e) {
		success = false;
		throw new AppException("Error al generar el PDF", e);
	}
	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Descarga_Archivos_CheckList")){

	try{
		Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class);
		nombreArchivo = cediBean.desArchSolicCheckList(icSolicitud, strDirectorioTemp, icDocumento, extension);
		if(nombreArchivo.equals("")){
			mensaje = "No existe archivo para esta solicitud";
			success = false;
		} else{
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	} catch(Exception e) {
		success = false;
		throw new AppException("Error al generar el PDF", e);
	}
	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("ViabilidadPDF")){

	ConsAtencionSeguimiento paginador = new ConsAtencionSeguimiento();
	paginador.setTipoConsulta(informacion);
	paginador.setTxtFolioSolicitud(icSolicitud);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try {
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		resultado.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
	} catch(Throwable e){
		success = false;
		throw new AppException("Error al generar el archivo", e);
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("IndicadoresFinancieros")){

	ReportesConsultaSolicitudes reporteador = new ReportesConsultaSolicitudes();
	reporteador.setDirectorioPlantillaXlsx( strDirectorioPublicacion + "00archivos/plantillas/39CediIntermediarioNB/");
	reporteador.setRazonSocial(razonSocial);
	reporteador.setEjecutivo(ejecutivo);
	reporteador.setEstatus(estatus);
	reporteador.setNumFolio(numFolio);
	reporteador.setFechaInicio(fechaIni);
	reporteador.setFechaFin(fechaFin);
	reporteador.setInfoIf(infoIf);

	try {
		nombreArchivo = reporteador.generaReporteIndicadoresFinancieros(request, strDirectorioTemp, "XLSX");
		if(nombreArchivo.equals("")){
			mensaje = "No existe archivo para esta solicitud";
			success = false;
		} else{
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	} catch(Throwable e){
		success = false;
		throw new AppException("Error al generar el archivo", e);
	}
	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Informacion_Cualitativa")){

	ReportesConsultaSolicitudes reporteador = new ReportesConsultaSolicitudes();
	reporteador.setDirectorioPlantillaXlsx( strDirectorioPublicacion + "00archivos/plantillas/39CediIntermediarioNB/");
	reporteador.setRazonSocial(razonSocial);
	reporteador.setEjecutivo(ejecutivo);
	reporteador.setEstatus(estatus);
	reporteador.setNumFolio(numFolio);
	reporteador.setFechaInicio(fechaIni);
	reporteador.setFechaFin(fechaFin);
	reporteador.setInfoIf(infoIf);

	try {
		nombreArchivo = reporteador.generaReporteInformacionCualitativa(request, strDirectorioTemp, "XLSX");
		if(nombreArchivo.equals("")){
			mensaje = "No existe archivo para esta solicitud";
			success = false;
		} else{
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	} catch(Throwable e){
		success = false;
		throw new AppException("Error al generar el archivo", e);
	}
	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Cumplimiento_Indicadores")){

	ReportesConsultaSolicitudes reporteador = new ReportesConsultaSolicitudes();
	reporteador.setDirectorioPlantillaXlsx( strDirectorioPublicacion + "00archivos/plantillas/39CediIntermediarioNB/");
	reporteador.setRazonSocial(razonSocial);
	reporteador.setEjecutivo(ejecutivo);
	reporteador.setEstatus(estatus);
	reporteador.setNumFolio(numFolio);
	reporteador.setFechaInicio(fechaIni);
	reporteador.setFechaFin(fechaFin);
	reporteador.setInfoIf(infoIf);

	try {
		nombreArchivo = reporteador.generaReporteCumplimientoIndicadores(request, strDirectorioTemp, "XLSX");
		if(nombreArchivo.equals("")){
			mensaje = "No existe archivo para esta solicitud";
			success = false;
		} else{
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	} catch(Throwable e){
		success = false;
		throw new AppException("Error al generar el archivo", e);
	}
	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Directorio")){

	ReportesConsultaSolicitudes reporteador = new ReportesConsultaSolicitudes();
	reporteador.setDirectorioPlantillaXlsx( strDirectorioPublicacion + "00archivos/plantillas/39CediIntermediarioNB/");
	reporteador.setRazonSocial(razonSocial);
	reporteador.setEjecutivo(ejecutivo);
	reporteador.setEstatus(estatus);
	reporteador.setNumFolio(numFolio);
	reporteador.setFechaInicio(fechaIni);
	reporteador.setFechaFin(fechaFin);
	reporteador.setInfoIf(infoIf);

	try {
		nombreArchivo = reporteador.generaReporteDirectorio(request, strDirectorioTemp, "XLSX");
		if(nombreArchivo.equals("")){
			mensaje = "No existe archivo para esta solicitud";
			success = false;
		} else{
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	} catch(Throwable e){
		success = false;
		throw new AppException("Error al generar el archivo", e);
	}
	resultado.put("mensaje", mensaje);
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

}
%>
<%=infoRegresar%>