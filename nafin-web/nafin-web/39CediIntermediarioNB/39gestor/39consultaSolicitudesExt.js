Ext.onReady(function(){

	var winConsulta;

//------------------------------ Handlers ------------------------------
	// Vuelve a cargar la p�gina
	function limpiar(){
		this.up('form').getForm().reset();
		var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
		gridConsulta.hide();
	}

	// Cierra la ventana de Bit�cora de Movimientos
	function cerrarBitacora(){
		Ext.ComponentQuery.query('#ventanaMostrar')[0].hide();
	}

	// Realiza la consulta para llenar el grid
	function buscar(){

		// Valido la forma principal
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		if(!this.up('form').getForm().isValid()){
			return;
		}

		// Los siguientes ifs son para validar las fechas
		var fecha_inicio = Ext.getCmp('fecha_inicio').getValue();
		var fecha_final = Ext.getCmp('fecha_final').getValue();
		if(fecha_inicio == null)
			fecha_inicio = '';
		if(fecha_final == null)
			fecha_final = '';
		if(fecha_inicio == '' && fecha_final != ''){
			Ext.getCmp('fecha_inicio').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}
		if(fecha_final == '' && fecha_inicio != ''){
			Ext.getCmp('fecha_final').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}

		// Realiza la consulta del grid
		main.el.mask('Procesando...', 'x-mask-loading');
		Ext.ComponentQuery.query('#grid')[0].hide();
		consultaData.load({
			params: Ext.apply({
				operacion:    'generar',
				razon_social: forma.query('#razon_social_id')[0].getValue(),
				ejecutivo:    forma.query('#ejecutivo_id')[0].getValue(),
				estatus:      forma.query('#estatus_id')[0].getValue(),
				num_folio:    forma.query('#num_folio_id')[0].getValue(),
				fecha_ini:    forma.query('#fecha_inicio')[0].getValue(),
				fecha_fin:    forma.query('#fecha_final')[0].getValue(),
				info_if:      forma.query('#info_if_id')[0].getValue(),
				start:        0,
				limit:        15
			})
		});

	}

	function muestraColumnasDinamicas(bandera){

		var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
		if(bandera == true){
			gridConsulta.down("gridcolumn[dataIndex=DOMICILIO]").show();
			gridConsulta.down("gridcolumn[dataIndex=MUNICIPIO]").show();
			gridConsulta.down("gridcolumn[dataIndex=CODIGO_POSTAL]").show();
			gridConsulta.down("gridcolumn[dataIndex=CONTACTO]").show();
			gridConsulta.down("gridcolumn[dataIndex=EMAIL_CONTACTO]").show();
			gridConsulta.down("gridcolumn[dataIndex=TELEFONO]").show();
		} else{
			gridConsulta.down("gridcolumn[dataIndex=DOMICILIO]").hide();
			gridConsulta.down("gridcolumn[dataIndex=MUNICIPIO]").hide();
			gridConsulta.down("gridcolumn[dataIndex=CODIGO_POSTAL]").hide();
			gridConsulta.down("gridcolumn[dataIndex=CONTACTO]").hide();
			gridConsulta.down("gridcolumn[dataIndex=EMAIL_CONTACTO]").hide();
			gridConsulta.down("gridcolumn[dataIndex=TELEFONO]").hide();
		}

	}

	// Realiza la consulta de la bit�cora de movimientos
	var consultaBitacora = function(grid, rowIndex, colIndex){
		main.el.mask('Procesando...', 'x-mask-loading');
		var rec = consultaData.getAt(rowIndex);
		var ic_solicitud = rec.get('FOLIO_SOLICITUD');
		Ext.getCmp('hid_ic_solicitud').setValue(ic_solicitud);
		consultaDataBitacora.load({
			params: Ext.apply({
				ic_solicitud: ic_solicitud
			})
		});
	}

	// Se muestra la ventana que contiene el grid 'Bit�cora de Movimientos'
	function mostrarVentanaBitacora(){
		if (!winConsulta){
			winConsulta = new Ext.widget('window',{
				width:       720,
				itemId:      'ventanaMostrar',
				closeAction: 'hide',
				layout:      'fit',
				frame:       false,
				modal:       true,
				autoHeight:  true,
				resizable:   false,
				constrain:   true,
				closable:    false,
				autoScroll:  true,
				items:       gridBitacora
			})
		}
		winConsulta.show().setTitle('Bit�cora de Movimientos por Solicitud: ' + Ext.getCmp('hid_ic_solicitud').getValue());
	}

	//Muestra el visor para desplegar todos los documentos adicionales
	var descDoctoAdicional = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('FOLIO_SOLICITUD');
		var win = NE.DoctosCheck.VistaDoctos(no_solicitud);
	}

	// Genera el Reporte General
	function descargaReporteGeneral(){
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var grid  = Ext.ComponentQuery.query('#grid')[0];
		var boton = grid.query('#btnReporteGeneral')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '39consultaSolicitudesExt.data.jsp',
			params: Ext.apply({
				informacion:  'Descarga_Reporte_General',
				tipo:         'XLS',
				aplica_btn:   '',
				razon_social: forma.query('#razon_social_id')[0].getValue(),
				ejecutivo:    forma.query('#ejecutivo_id')[0].getValue(),
				estatus:      forma.query('#estatus_id')[0].getValue(),
				num_folio:    forma.query('#num_folio_id')[0].getValue(),
				fecha_ini:    forma.query('#fecha_inicio')[0].getValue(),
				fecha_fin:    forma.query('#fecha_final')[0].getValue(),
				info_if:      forma.query('#info_if_id')[0].getValue(),
				_botonId:     boton.getId()
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Genera el archivo de Indicadores Financieros
	function descargaIndFinancieros(){
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var grid  = Ext.ComponentQuery.query('#grid')[0];
		var boton = grid.query('#btnIndFinancieros')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '39consultaSolicitudesExt.data.jsp',
			params: Ext.apply({
				informacion:  'IndicadoresFinancieros',
				tipo:         'XLS',
				aplica_btn:   '',
				razon_social: forma.query('#razon_social_id')[0].getValue(),
				ejecutivo:    forma.query('#ejecutivo_id')[0].getValue(),
				estatus:      forma.query('#estatus_id')[0].getValue(),
				num_folio:    forma.query('#num_folio_id')[0].getValue(),
				fecha_ini:    forma.query('#fecha_inicio')[0].getValue(),
				fecha_fin:    forma.query('#fecha_final')[0].getValue(),
				info_if:      forma.query('#info_if_id')[0].getValue(),
				_botonId:     boton.getId()
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Genera el archivo de btnInfCualitativa
	function descargaInfCualitativa(){
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var grid  = Ext.ComponentQuery.query('#grid')[0];
		var boton = grid.query('#btnInfCualitativa')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '39consultaSolicitudesExt.data.jsp',
			params: Ext.apply({
				informacion:  'Informacion_Cualitativa',
				tipo:         'XLS',
				aplica_btn:   '',
				razon_social: forma.query('#razon_social_id')[0].getValue(),
				ejecutivo:    forma.query('#ejecutivo_id')[0].getValue(),
				estatus:      forma.query('#estatus_id')[0].getValue(),
				num_folio:    forma.query('#num_folio_id')[0].getValue(),
				fecha_ini:    forma.query('#fecha_inicio')[0].getValue(),
				fecha_fin:    forma.query('#fecha_final')[0].getValue(),
				info_if:      forma.query('#info_if_id')[0].getValue(),
				_botonId:     boton.getId()
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Genera el archivo de Cumplimiento de indicadores
	function descargaCumplIndicador(){
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var grid  = Ext.ComponentQuery.query('#grid')[0];
		var boton = grid.query('#btnCumplIndicador')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '39consultaSolicitudesExt.data.jsp',
			params: Ext.apply({
				informacion:  'Cumplimiento_Indicadores',
				tipo:         'XLS',
				aplica_btn:   '',
				razon_social: forma.query('#razon_social_id')[0].getValue(),
				ejecutivo:    forma.query('#ejecutivo_id')[0].getValue(),
				estatus:      forma.query('#estatus_id')[0].getValue(),
				num_folio:    forma.query('#num_folio_id')[0].getValue(),
				fecha_ini:    forma.query('#fecha_inicio')[0].getValue(),
				fecha_fin:    forma.query('#fecha_final')[0].getValue(),
				info_if:      forma.query('#info_if_id')[0].getValue(),
				_botonId:     boton.getId()
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Genera el Directorio
	function descargaDirectorio(){
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var grid  = Ext.ComponentQuery.query('#grid')[0];
		var boton = grid.query('#btnDirectorio')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '39consultaSolicitudesExt.data.jsp',
			params: Ext.apply({
				informacion:  'Directorio',
				tipo:         'XLS',
				aplica_btn:   '',
				razon_social: forma.query('#razon_social_id')[0].getValue(),
				ejecutivo:    forma.query('#ejecutivo_id')[0].getValue(),
				estatus:      forma.query('#estatus_id')[0].getValue(),
				num_folio:    forma.query('#num_folio_id')[0].getValue(),
				fecha_ini:    forma.query('#fecha_inicio')[0].getValue(),
				fecha_fin:    forma.query('#fecha_final')[0].getValue(),
				info_if:      forma.query('#info_if_id')[0].getValue(),
				_botonId:     boton.getId()
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Descarga los archivos: Balance, Resultados, RFC
	function descargaArchivoSolic(ic_solicitud, tipo_archivo){
		Ext.Ajax.request({
			url: '39consultaSolicitudesExt.data.jsp',
			params: Ext.apply({
				informacion:  'Descarga_Archivos_Solicitud',
				ic_solicitud: ic_solicitud,
				tipo_archivo: tipo_archivo,
				aplica_btn:   'NO_APLICA'
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Descarga la c�dula electr�nica de aceptaci�n o rechazo.
	function descargaCedulaElectronica(ic_solicitud, estatus){
		Ext.Ajax.request({
			url: '39consultaSolicitudesExt.data.jsp',
			params: Ext.apply({
				informacion:  'Descarga_Cedula_Electronica',
				ic_solicitud: ic_solicitud,
				estatus:      estatus,
				aplica_btn:   'NO_APLICA'
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Descarga los archivos de la tabla cedi_documento_checklist
	function descargaArchivoCheckList(ic_solicitud, ic_documento, extension){
		Ext.Ajax.request({
			url: '39consultaSolicitudesExt.data.jsp',
			params: Ext.apply({
				informacion:  'Descarga_Archivos_CheckList',
				ic_solicitud: ic_solicitud,
				extension:    extension,
				ic_documento: ic_documento,
				aplica_btn:   'NO_APLICA'
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Descarga el dictamen de viabilidad
	var descargaDictamen = function(grid,rowIndex,colIndex,item,event){
		var registro = consultaData.getAt(rowIndex);
		if(registro.get('DICTAMEN') == 'S'){
			Ext.Ajax.request({
				url: '39consultaSolicitudesExt.data.jsp',
				params: Ext.apply({
					informacion:  'ViabilidadPDF',
					ic_solicitud: registro.get('FOLIO_SOLICITUD'),
					aplica_btn:   'NO_APLICA'
				}),
				callback: procesaGeneraArchivo
			});
		} else{
			return;
		}
	}

	// Descarga el pdf de la Bit�cora de Movimientos
	function descargarPdfBitacora(){
		var grid  = Ext.ComponentQuery.query('#gridBitacora')[0];
		var boton = grid.query('#btnDescargarPdfBitacora')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '39consultaSolicitudesExt.data.jsp',
			params: Ext.apply({
				informacion:  'Descarga_Bitacora_Movimientos',
				ic_solicitud: Ext.getCmp('hid_ic_solicitud').getValue(),
				tipo:         'PDF',
				aplica_btn:   '',
				_botonId:     boton.getId()
			}),
			callback: procesaGeneraArchivo
		});
	}
//------------------------------ Callback --------------------------------

	// Proceso posterior a la consulta del grid general
	var procesarConsultaData = function(store, arrRegistros, success, opts){
		main.el.unmask();
		var gridConsulta = Ext.ComponentQuery.query('#grid')[0];
		var perfil = store.proxy.reader.jsonData.perfil;

		if (arrRegistros != null){
			gridConsulta.show();
			if(store.getTotalCount() > 0){

				//Funcionalidad para mostrar/ocultar la informaci�n del intermediario
				var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
				var mostrarDatos = forma.query('#info_if_id')[0].getValue();
				muestraColumnasDinamicas(mostrarDatos);

				// Solo el perfil 'GESTOR CEDI' puede descargar la Bit�cora
				if(perfil == 'GESTOR CEDI'){
					gridConsulta.down("gridcolumn[dataIndex=VER_BITACORA]").show();
				} else{
					gridConsulta.down("gridcolumn[dataIndex=VER_BITACORA]").hide();
				}

				gridConsulta.query('#btnReporteGeneral')[0].enable();
				gridConsulta.query('#btnIndFinancieros')[0].enable();
				gridConsulta.query('#btnInfCualitativa')[0].enable();
				gridConsulta.query('#btnCumplIndicador')[0].enable();
				gridConsulta.query('#btnDirectorio')[0].enable();

			} else{
				gridConsulta.getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				gridConsulta.query('#btnReporteGeneral')[0].disable();
				gridConsulta.query('#btnIndFinancieros')[0].disable();
				gridConsulta.query('#btnInfCualitativa')[0].disable();
				gridConsulta.query('#btnCumplIndicador')[0].disable();
				gridConsulta.query('#btnDirectorio')[0].disable();
			}
		}
	}

	// Proceso posterior a la consulta de la bit�cora de movimientos
	var procesarConsultaDataBitacora = function(store, arrRegistros, success, opts){
		main.el.unmask();
		if (arrRegistros != null){
			var grid  = Ext.ComponentQuery.query('#gridBitacora')[0];
			var boton = grid.query('#btnDescargarPdfBitacora')[0];
			mostrarVentanaBitacora();
			grid.show();
			if(store.getTotalCount() > 0){
				boton.enable();
			} else{
				boton.disable();
				grid.getView().getEl().mask('No se encontraron registros');
			}
		}
	}

	// Handler que procesa respuesta creacion archivo
	function procesaGeneraArchivo(opts, success, response){

		// DEFINO QUE ICONO SE MOSTRAR� EN EL BOT�N QUE GENER� LA DESCARGA
		var aplicaBtn = opts.params.aplica_btn // Me dice si la petici�n viene de un bot�n o de una columna
		if(aplicaBtn == ''){
			var icono = '';
			var boton = Ext.getCmp(opts.params._botonId);
			var tipoArchivo = opts.params.tipo
			if(tipoArchivo == 'PDF'){
				icono = 'icoPdf';
			} else{
				icono = 'icoXls';
			}
		}
		// PARSEAR RESPUESTA DEL SERVIDOR
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			if(aplicaBtn == ''){ // Si la petici�n viene de un bot�n
				boton.setIconCls(icono);
				boton.enable();
			} else{ // Si la petici�n viene de una columna del grid
				main.el.unmask();
			}
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:   'input',
					type:  'hidden',
					id:    'nombreArchivo',
					name:  'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {

				NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n

		}

		if(aplicaBtn == ''){ // Si la petici�n viene de un bot�n
			boton.setIconCls(icono);
			boton.enable();
		} else{ // Si la petici�n viene de una columna del grid
			main.el.unmask();
		}

	}

//------------------------------ Stores ------------------------------
	// Se crea el MODEL para el cat�logo 'Raz�n Social'
	Ext.define('ModelCatologoRazonSocial',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);

	// Se crea el MODEL para el cat�logo 'Ejecutivo CEDI'
	Ext.define('ModelCatologoEjecutivo',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);

	// Se crea el MODEL para el cat�logo 'Estatus'
	Ext.define('ModelCatologoEstatus',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);

	// Se crea el MODEL para el grid 'Consulta Bit�cora'
	Ext.define('ListaBitacora',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'IC_SOLICITUD'         },
			{name: 'LOGIN'                },
			{name: 'NOMBRE_USUARIO'       },
			{name: 'ESTATUS'              },
			{name: 'MOVIMIENTO'           },
			{name: 'FECHA_HORA_MOVIMIENTO'}
		]
	});

	// Se crea el MODEL para el grid 'Consulta'
	Ext.define('ListaSolicitudes',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'IC_IFNB'             },
			{name: 'DICTAMEN'            },
			{name: 'LOGIN_EJECUTIVO_CEDI'},
			{name: 'RAZON_SOCIAL'        },
			{name: 'RFC'                 },
			{name: 'DOMICILIO'           },
			{name: 'ESTADO'              },
			{name: 'MUNICIPIO'           },
			{name: 'CODIGO_POSTAL'       },
			{name: 'CONTACTO'            },
			{name: 'EMAIL_CONTACTO'      },
			{name: 'TELEFONO'            },
			{name: 'FOLIO_SOLICITUD'     },
			{name: 'FECHA_HORA_REGISTRO' },
			{name: 'EJECUTIVO_CEDI'      },
			{name: 'ESTATUS_SOLICITUD'   },
			{name: 'BALANCE'             },
			{name: 'EDO_RESULTADOS'      },
			{name: 'BI_RFC'              },
			{name: 'MANUAL_CREDITO'      },
			{name: 'PRESEN_CORPORATIVA'  },
			{name: 'PASIVOS_BANCARIOS'   },
			{name: 'CARTERA_VENCIDA'     },
			{name: 'PORTAFOLIO_CARTERA'  },
			{name: 'VER_BITACORA'        },
			{name: 'EXISTE_CEDULA'       }
		]
	});

	// Se crea el STORE para el cat�logo 'Raz�n Social'
	var catalogoRazonSocial = Ext.create('Ext.data.Store',{
		model: 'ModelCatologoRazonSocial',
		proxy: {
			type: 'ajax',
			url: '39consultaSolicitudesExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Catalogo_Razon_Social'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});

	// Se crea el STORE para el cat�logo 'Ejecutivo CEDI'
	var catalogoEjecutivo = Ext.create('Ext.data.Store',{
		model: 'ModelCatologoEjecutivo',
		proxy: {
			type: 'ajax',
			url: '39consultaSolicitudesExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Catalogo_Ejecutivo'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});

	// Se crea el STORE para el cat�logo 'Estatus'
	var catalogoEstatus = Ext.create('Ext.data.Store',{
		model: 'ModelCatologoEstatus',
		proxy: {
			type: 'ajax',
			url: '39consultaSolicitudesExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Catalogo_Estatus'
			},
			autoLoad: true,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: true
	});

	// Se crea el STORE del grid 'Consulta Bit�cora'
	var consultaDataBitacora = Ext.create('Ext.data.Store',{
		model: 'ListaBitacora',
		proxy: {
			type: 'ajax',
			url:  '39consultaSolicitudesExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Data_Bitacora'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataBitacora
		}
	});

	// Se crea el STORE del grid 'Consulta'
	var consultaData = Ext.create('Ext.data.Store',{
		storeId: 'consultaData',
		model: 'ListaSolicitudes',
		pageSize: 15,
		proxy: {
			type: 'ajax',
			url:  '39consultaSolicitudesExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Data'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultaData
		}
	});

//------------------------------ Componentes ------------------------------

	// Se crea el grid 'Bit�cora de Movimientos'
	var gridBitacora = Ext.create('Ext.grid.Panel',{
		store:            consultaDataBitacora,
		height:           250,
		width:            '100%',
		itemId:           'gridBitacora',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		frame:            false,
		stripeRows:       true,
		autoScroll:       true,
		hidden:           false,
		enableColumnMove: false,
		enableColumnHide: false,
		columns: [
			{width:120, align:'center', sortable:true, resizable: true, dataIndex: 'LOGIN',                 header:'<div align="center"> Login </div>'                     },
			{width:180, align:'left',   sortable:true, resizable: true, dataIndex: 'NOMBRE_USUARIO',        header:'<div align="center"> Nombre Usuario </div>'            },
			{width:100, align:'center', sortable:true, resizable: true, dataIndex: 'ESTATUS',               header:'<div align="center"> Estatus </div>'                   },
			{width:130, align:'left',   sortable:true, resizable: true, dataIndex: 'MOVIMIENTO',            header:'<div align="center"> Movimiento </div>'                },
			{width:170, align:'center', sortable:true, resizable: true, dataIndex: 'FECHA_HORA_MOVIMIENTO', header:'<div align="center"> Fecha y hora de movimiento </div>'}
		],
		bbar: ['->','-',{
			xtype:       'button',
			text:        'Descargar Archivo',
			itemId:      'btnDescargarPdfBitacora',
			iconCls:     'icoPdf',
			handler:     descargarPdfBitacora
		},'-',{
			xtype:       'button',
			text:        'Cerrar',
			itemId:      'btnCerrarBitacora',
			iconCls:     'icoLimpiar',
			handler:     cerrarBitacora
		},'-']
	});

	// Se crea el grid 'Consulta'
	var grid = Ext.create('Ext.grid.Panel',{
		store:            Ext.data.StoreManager.lookup('consultaData'),
		height:           440,
		width:            '90%',
		itemId:           'grid',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'Consulta',
		border:           true,
		stripeRows:       true,
		autoScroll:       true,
		hidden:           true,
		frame:            false,
		enableColumnMove: false,
		enableColumnHide: false,
		columns: [{
			width:         200,
			dataIndex:     'RAZON_SOCIAL',
			header:        'Raz�n Social',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         160,
			dataIndex:     'RFC',
			header:        'R.F.C. empresarial',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         200,
			dataIndex:     'DOMICILIO',
			header:        'Domicilio',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         235,
			dataIndex:     'MUNICIPIO',
			header:        'Estado y Delegaci�n o Municipio empresarial',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         180,
			dataIndex:     'CODIGO_POSTAL',
			header:        'C�digo Postal empresarial',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         200,
			dataIndex:     'CONTACTO',
			header:        'Contacto',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         190,
			dataIndex:     'EMAIL_CONTACTO',
			header:        'Correo Electr�nico Empresarial',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'TELEFONO',
			header:        'Tel�fono empresarial',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         110,
			dataIndex:     'FOLIO_SOLICITUD',
			header:        'Folio Solicitud',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         180,
			dataIndex:     'FECHA_HORA_REGISTRO',
			header:        'Fecha y hora de registro',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         180,
			dataIndex:     'EJECUTIVO_CEDI',
			header:        'Ejecutivo CEDI responsable',
			align:         'left',
			sortable:      true,
			resizable:     true
		},{
			width:         220,
			xtype:         'actioncolumn',
			header:        'C�dula electr�nica de aceptaci�n o rechazo',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass:   function(value,metadata,record,rowIndex,colIndex,store){
					if (parseInt(record.get('EXISTE_CEDULA')) > 0){ // Mayor que 0 indica que hay por lo menos un registro
						return 'icoPdf';
					}
				},
				handler:    function(grid, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if (parseInt(rec.get('EXISTE_CEDULA')) > 0){ // Mayor que 0 indica que hay por lo menos un registro
						main.el.mask('Procesando...', 'x-mask-loading');
						var ic_solicitud = rec.get('FOLIO_SOLICITUD');
						var estatus = rec.get('ESTATUS_SOLICITUD');
						descargaCedulaElectronica(ic_solicitud, estatus);
					}
				}
			}]
		},{
			width:         80,
			xtype:         'actioncolumn',
			header:        'Balance',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					if (record.get('BALANCE') == 'S'){
						return 'icoPdf';
					}
				},
				handler:    function(grid, rowIndex, colIndex){
					main.el.mask('Procesando...', 'x-mask-loading');
					var rec = consultaData.getAt(rowIndex);
					var ic_solicitud = rec.get('FOLIO_SOLICITUD');
					descargaArchivoSolic(ic_solicitud, 'balance');
				}
			}]
		},{
			width:         90,
			xtype:         'actioncolumn',
			header:        'Edo. Resultados',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					if (record.get('EDO_RESULTADOS') == 'S'){
						return 'icoPdf';
					}
				},
				handler:    function(grid, rowIndex, colIndex){
					main.el.mask('Procesando...', 'x-mask-loading');
					var rec = consultaData.getAt(rowIndex);
					var ic_solicitud = rec.get('FOLIO_SOLICITUD');
					descargaArchivoSolic(ic_solicitud, 'resultados');
				}
			}]
		},{
			width:         80,
			xtype:         'actioncolumn',
			header:        'Copia RFC',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					if (record.get('BI_RFC') == 'S'){
						return 'icoPdf';
					}
				},
				handler:    function(grid, rowIndex, colIndex){
					main.el.mask('Procesando...', 'x-mask-loading');
					var rec = consultaData.getAt(rowIndex);
					var ic_solicitud = rec.get('FOLIO_SOLICITUD');
					descargaArchivoSolic(ic_solicitud, 'rfc');
				}
			}]
		},{
			width:         120,
			xtype:         'actioncolumn',
			header:        'Manual de Cr�dito',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					var icono = '';
					if (record.get('MANUAL_CREDITO') != ''){
						if(record.get('MANUAL_CREDITO') == 'pdf') icono = 'icoPdf';
						else if(record.get('MANUAL_CREDITO') == 'csv') icono = 'icoXls';
						else if(record.get('MANUAL_CREDITO') == 'xls' || record.get('MANUAL_CREDITO') == 'xlsx') icono = 'icoXls';
						else if(record.get('MANUAL_CREDITO') == 'doc' || record.get('MANUAL_CREDITO') == 'docx') icono = 'icoDoc';
						else if(record.get('MANUAL_CREDITO') == 'ppt' || record.get('MANUAL_CREDITO') == 'pptx') icono = 'icoPpt';
					}
					return icono;
				},
				handler:    function(grid, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if (rec.get('MANUAL_CREDITO') != ''){
						main.el.mask('Procesando...', 'x-mask-loading');
						var ic_solicitud = rec.get('FOLIO_SOLICITUD');
						descargaArchivoCheckList(ic_solicitud, 1, '');
					}
				}
			}]
		},{
			width:         150,
			xtype:         'actioncolumn',
			header:        'Presentaci�n Corporativa',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					var icono = '';
					if (record.get('PRESEN_CORPORATIVA') != ''){
						if(record.get('PRESEN_CORPORATIVA') == 'pdf') icono = 'icoPdf';
						else if(record.get('PRESEN_CORPORATIVA') == 'csv') icono = 'icoXls';
						else if(record.get('PRESEN_CORPORATIVA') == 'xls' || record.get('PRESEN_CORPORATIVA') == 'xlsx') icono = 'icoXls';
						else if(record.get('PRESEN_CORPORATIVA') == 'doc' || record.get('PRESEN_CORPORATIVA') == 'docx') icono = 'icoDoc';
						else if(record.get('PRESEN_CORPORATIVA') == 'ppt' || record.get('PRESEN_CORPORATIVA') == 'pptx') icono = 'icoPpt';
					}
					return icono;
				},
				handler:    function(grid, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if (rec.get('PRESEN_CORPORATIVA') != ''){
						main.el.mask('Procesando...', 'x-mask-loading');
						var ic_solicitud = rec.get('FOLIO_SOLICITUD');
						descargaArchivoCheckList(ic_solicitud, 2, '');
					}
				}
			}]
		},{
			width:         160,
			xtype:         'actioncolumn',
			header:        'Desglose de Pasivos Bancarios',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					var icono = '';
					if (record.get('PASIVOS_BANCARIOS') != ''){
						if(record.get('PASIVOS_BANCARIOS') == 'pdf') icono = 'icoPdf';
						else if(record.get('PASIVOS_BANCARIOS') == 'csv') icono = 'icoXls';
						else if(record.get('PASIVOS_BANCARIOS') == 'xls' || record.get('PASIVOS_BANCARIOS') == 'xlsx') icono = 'icoXls';
						else if(record.get('PASIVOS_BANCARIOS') == 'doc' || record.get('PASIVOS_BANCARIOS') == 'docx') icono = 'icoDoc';
						else if(record.get('PASIVOS_BANCARIOS') == 'ppt' || record.get('PASIVOS_BANCARIOS') == 'pptx') icono = 'icoPpt';
					}
					return icono;
				},
				handler:    function(grid, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if (rec.get('PASIVOS_BANCARIOS') != ''){
						main.el.mask('Procesando...', 'x-mask-loading');
						var ic_solicitud = rec.get('FOLIO_SOLICITUD');
						descargaArchivoCheckList(ic_solicitud, 3, '');
					}
				}
			}]
		},{
			width:         180,
			xtype:         'actioncolumn',
			header:        'Antig�edad de la cartera vencida',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					var icono = '';
					if (record.get('CARTERA_VENCIDA') != ''){
						if(record.get('CARTERA_VENCIDA') == 'pdf') icono = 'icoPdf';
						else if(record.get('CARTERA_VENCIDA') == 'csv') icono = 'icoXls';
						else if(record.get('CARTERA_VENCIDA') == 'xls' || record.get('CARTERA_VENCIDA') == 'xlsx') icono = 'icoXls';
						else if(record.get('CARTERA_VENCIDA') == 'doc' || record.get('CARTERA_VENCIDA') == 'docx') icono = 'icoDoc';
						else if(record.get('CARTERA_VENCIDA') == 'ppt' || record.get('CARTERA_VENCIDA') == 'pptx') icono = 'icoPpt';
					}
					return icono;
				},
				handler:    function(grid, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if (rec.get('CARTERA_VENCIDA') != ''){
						main.el.mask('Procesando...', 'x-mask-loading');
						var ic_solicitud = rec.get('FOLIO_SOLICITUD');
						descargaArchivoCheckList(ic_solicitud, 4, '');
					}
				}
			}]
		},{
			width:         140,
			xtype:         'actioncolumn',
			header:        'Portafolio de la Cartera',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					var icono = '';
					if (record.get('PORTAFOLIO_CARTERA') != ''){
						if(record.get('PORTAFOLIO_CARTERA') == 'pdf') icono = 'icoPdf';
						else if(record.get('PORTAFOLIO_CARTERA') == 'csv') icono = 'icoXls';
						else if(record.get('PORTAFOLIO_CARTERA') == 'xls' || record.get('PORTAFOLIO_CARTERA') == 'xlsx') icono = 'icoXls';
						else if(record.get('PORTAFOLIO_CARTERA') == 'doc' || record.get('PORTAFOLIO_CARTERA') == 'docx') icono = 'icoDoc';
						else if(record.get('PORTAFOLIO_CARTERA') == 'ppt' || record.get('PORTAFOLIO_CARTERA') == 'pptx') icono = 'icoPpt';
					}
					return icono;
				},
				handler:    function(grid, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if (rec.get('PORTAFOLIO_CARTERA') != ''){
						main.el.mask('Procesando...', 'x-mask-loading');
						var ic_solicitud = rec.get('FOLIO_SOLICITUD');
						descargaArchivoCheckList(ic_solicitud, 5, '');
					}
				}
			}]
		},{
			width:         100,
			xtype:         'actioncolumn',
			header:        'Docto. Adicional',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				iconCls:    'icoBuscar',
				tooltip:    'Descargar archivo',
				handler:    descDoctoAdicional
			}]
		},{
			width:         130,
			xtype:         'actioncolumn',
			header:        'Dictamen de Viabilidad',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				getClass: function(value,metadata,record,rowIndex,colIndex,store){
					if (record.get('DICTAMEN') == 'S'){
						return 'icoPdf';
					}
				},
				handler: descargaDictamen
				
			}]
		},{
			width:         140,
			dataIndex:     'ESTATUS_SOLICITUD',
			header:        'Estatus',
			align:         'center',
			sortable:      true,
			resizable:     true
		},{
			width:         150,
			dataIndex:     'VER_BITACORA',
			xtype:         'actioncolumn',
			header:        'Bit�cora de movimientos',
			align:         'center',
			sortable:      false,
			resizable:     false,
			menuDisabled:  true,
			items: [{
				iconCls:    'icoBuscar',
				tooltip:    'Bit�cora de movimientos',
				handler:    consultaBitacora
			}]
		}],
		bbar: [
			Ext.create('Ext.PagingToolbar',{
				store:      Ext.data.StoreManager.lookup('consultaData'),
				itemId:     'barraPaginacion',
				displayMsg: '{0} - {1} de {2}',
				emptyMsg:   'No se encontraron registros',
				displayInfo:true,
				border:     false
			}),
			'-',{xtype:'button', itemId:'btnReporteGeneral', iconCls:'icoXls', text:'Reporte <br>General',             tooltip:'Reporte General.',             handler:descargaReporteGeneral},
			'-',{xtype:'button', itemId:'btnIndFinancieros', iconCls:'icoXls', text:'Indicadores <br>Financieros',     tooltip:'Indicadores Financieros.',     handler:descargaIndFinancieros},
			'-',{xtype:'button', itemId:'btnInfCualitativa', iconCls:'icoXls', text:'Informaci�n <br>Cualitativa',     tooltip:'Inf. Cualitativa.',            handler:descargaInfCualitativa},
			'-',{xtype:'button', itemId:'btnCumplIndicador', iconCls:'icoXls', text:'Cumplimiento <br>de indicadores', tooltip:'Cumplimiento de indicadores.', handler:descargaCumplIndicador},
			'-',{xtype:'button', itemId:'btnDirectorio',     iconCls:'icoXls', text:'Directorio',                      tooltip:'Directorio.',                  handler:descargaDirectorio    }
		]
	});

	// Se crea el form principal
	var formaPrincipal = Ext.create( 'Ext.form.Panel',{
		width:                     600,
		itemId:                    'formaPrincipal',
		title:                     'Consulta de solicitudes',
		bodyPadding:               '12 6 12 6',
		style:                     'margin: 0px auto 0px auto;',
		frame:                     true,
		border:                    true,
		fieldDefaults: {
			msgTarget:              'side',
			labelWidth:             90
		},
		items:[
		{
			anchor:                 '95%',
			xtype:                  'combobox',
			itemId:                 'razon_social_id',
			name:                   'razon_social',
			hiddenName:             'razon_social',
			fieldLabel:             'Raz�n Social',
			emptyText:              'Seleccione ...',
			displayField:           'descripcion',
			valueField:             'clave',
			queryMode:              'local',
			triggerAction:          'all',
			listClass:              'x-combo-list-small',
			typeAhead:              true,
			selectOnTab:            true,
			lazyRender:             true,
			forceSelection:         true,
			hidden:                 false,
			editable:               true,
			store:                  catalogoRazonSocial
		},{
			anchor:                 '95%',
			xtype:                  'combobox',
			itemId:                 'ejecutivo_id',
			name:                   'ejecutivo',
			hiddenName:             'ejecutivo',
			fieldLabel:             'Ejecutivo CEDI',
			emptyText:              'Seleccione ...',
			displayField:           'descripcion',
			valueField:             'clave',
			queryMode:              'local',
			triggerAction:          'all',
			listClass:              'x-combo-list-small',
			typeAhead:              true,
			selectOnTab:            true,
			lazyRender:             true,
			forceSelection:         true,
			hidden:                 false,
			editable:               true,
			store:                  catalogoEjecutivo
		},{
			anchor:                 '95%',
			xtype:                  'combobox',
			itemId:                 'estatus_id',
			name:                   'estatus',
			hiddenName:             'estatus',
			fieldLabel:             'Estatus',
			emptyText:              'Seleccione ...',
			displayField:           'descripcion',
			valueField:             'clave',
			queryMode:              'local',
			triggerAction:          'all',
			listClass:              'x-combo-list-small',
			typeAhead:              true,
			selectOnTab:            true,
			lazyRender:             true,
			forceSelection:         true,
			hidden:                 false,
			editable:               true,
			store:                  catalogoEstatus
		},{
			anchor:                 '95%',
			xtype:                  'numberfield',
			itemId:                 'num_folio_id',
			name:                   'num_folio',
			hiddenName:             'num_folio',
			fieldLabel:             'Folio Solicitud',
			allowDecimals:          true,
			maxLength:              12,
			minValue:               0
			},{
			xtype:                  'container',
			layout:                 'hbox',
			anchor:                 '95%',
			margin:                 '0 0 5 0',
			items:[{
				width:               263,
				xtype:               'datefield',
				id:                  'fecha_inicio',
				name:                'fecha_inicio',
				hiddenName:          'fecha_ini',
				fieldLabel:          'Fecha Solicitud',
				msgTarget:           'side',
				vtype:               'rangofecha',
				minValue:            '01/01/1901',
				campoFinFecha:       'fecha_final',
				margins:             '0 20 0 0',
				allowBlank:          true,
				startDay:            0
			},{
				width:               263,
				xtype:               'datefield',
				id:                  'fecha_final',
				name:                'fecha_final',
				hiddenName:          'fecha_fin',
				fieldLabel:          '&nbsp;&nbsp;&nbsp; hasta',
				msgTarget:           'side',
				vtype:               'rangofecha',
				minValue:            '01/01/1901',
				campoInicioFecha:    'fecha_inicio',
				margins:             '0 20 0 0',
				allowBlank:          true,
				startDay:            1
			}]
		},{
			xtype:                  'container',
			layout:                 'hbox',
			anchor:                 '95%',
			margin:                 '0 0 5 0',
			items: [{
				width:               200,
				xtype:               'displayfield',
				value:               '&nbsp;'
			},{
				xtype:               'checkbox',
				itemId:              'info_if_id',
				name:                'info_if',
				hiddenName:          'info_if',
				boxLabel:            '&nbsp; Mostrar informaci�n del Intermediario',
				listeners:           {
					change:           function(){
						muestraColumnasDinamicas(this.getValue());
					}
				}	
			}]
		},{
			anchor:                 '95%',
			xtype:                  'textfield',
			id:                     'hid_ic_solicitud',
			name:                   'hid_ic_solicitud',
			fieldLabel:             'hid_ic_solicitud',
			hidden:                 true
		}],
		buttons: [{
			text:                   'Buscar',
			itemId:                 'btnBuscar',
			iconCls:                'icoBuscar',
			handler:                buscar
		},{
			text:                   'Limpiar',
			itemId:                 'btnLimpiar',
			iconCls:                'icoLimpiar',
			handler:                limpiar
		}]
	});

	// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:      949,
		minHeight:  650,
		autoHeight: true,
		id:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		style:      'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			formaPrincipal,
			NE.util.getEspaciador(20),
			grid
		]
	});
});