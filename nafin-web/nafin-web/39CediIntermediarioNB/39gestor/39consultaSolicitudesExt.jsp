<!DOCTYPE html>
<%@page contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/39CediIntermediarioNB/39secsession.jspf" %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<%@ include file="/extjs4.jspf" %>
		<%@ include file="/01principal/menu.jspf"%>
		<script type="text/javascript" src="39consultaSolicitudesExt.js?<%=session.getId()%>"></script>
		<script type="text/javascript" src="/nafin/39CediIntermediarioNB/39DoctoCheck.js"></script>
		<title>Nafi@net</title>
	</head>
	<body>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<div id='areaContenido'></div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
	</body>
</html>