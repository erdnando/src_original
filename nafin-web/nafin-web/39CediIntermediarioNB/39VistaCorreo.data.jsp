<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		com.netro.cedi.*,
		java.sql.*,
		com.netro.model.catalogos.CatalogoSimple,		
		com.netro.pdf.*,
		netropology.utilerias.*,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="39secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
String txtObservacionesCEDI = (request.getParameter("txtObservacionesCEDI")!=null)?request.getParameter("txtObservacionesCEDI"):"";
String pantalla = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";
String no_ifnb = (request.getParameter("no_ifnb")!=null)?request.getParameter("no_ifnb"):"";
String pdfViabilidad = (request.getParameter("pdfViabilidad")!=null)?request.getParameter("pdfViabilidad"):"";

String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
String feActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String diaActual    = feActual.substring(0,2);
String mesActual    = meses[Integer.parseInt(feActual.substring(3,5))-1];
String anioActual   = feActual.substring(6,10);
String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
String  fechaActual =  "México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" -- "+horaActual;

UtilUsr utilUsr = new UtilUsr();
Usuario usuario = utilUsr.getUsuario(strLogin);
String destinatario = usuario.getEmail();						
String nombreDe = usuario.getNombre()+" "+usuario.getApellidoPaterno()+" "+usuario.getApellidoMaterno();		
					
String  lbNombreUsuario = "Lic. "+nombreDe;
String lbCorreoUsuario  =  destinatario;

String infoRegresar	=	"", consulta ="";
JSONObject jsonObj = new JSONObject();	
List cuentas = utilUsr.getUsuariosxAfiliado(no_ifnb, "IFNB CEDI");
Iterator itCuentas = cuentas.iterator();	
HashMap	parametros = new HashMap();
	
parametros.put("RUTA", strDirectorioTemp);		
parametros.put("NO_SOLICITUD", no_solicitud);
parametros.put("LBFECHAACTUAL", fechaActual);
parametros.put("OBSERVACIONES", txtObservacionesCEDI);
parametros.put("NOMBREUSUARIO", "Lic."+strNombreUsuario +"\n"+destinatario);	
parametros.put("PANTALLA", pantalla);
parametros.put("no_ifnb", no_ifnb);
parametros.put("pdfViabilidad", pdfViabilidad);
parametros.put("path", strDirectorioTemp); 

Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 
 
HashMap  datosIF =  cediBean.getDatosIF(no_solicitud  );
String rfc_empresarial = datosIF.get("RFC").toString();
String razon_social = datosIF.get("NOMBRE_IF").toString();

//esta parte es para la generación del archivo de Dictament de Viabilidad por eso va por defaul el valor ViabilidadPDF en el campos tipoConsulta
ConsAtencionSeguimiento paginador = new ConsAtencionSeguimiento();		
paginador.setTxtFolioSolicitud(no_solicitud);
paginador.setRfc_empresarial(rfc_empresarial);
paginador.setRazon_social(razon_social); 
paginador.setTipoConsulta("ViabilidadPDF"); 


CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);	
	 
   
if (informacion.equals("VistaPreviaCorreo")){
	
	
	String  lbListaDoctos ="", lbNombreIFNB ="", lbTitulo = "",  lbNombreUsuario_1 =  "", lbfechaActual =  "",
			  lbMensaje1 ="", lbMensaje2 ="", lbMensaje3 ="", lbMensaje4 ="", lbMensaje5 ="";
	
	lbMensaje3 +=" <form>" + 	
					 "<br> <table width='650' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED'> ";
	
	
	if (pantalla.equals("PreAnalisis_Aceptada")){	
		lbTitulo =  "<table width='500' align='center' ><tr align='center'><td align='center' ><H4>Seguimiento de Pre-Análisis  </H4></td></tr></table> ";
	
		lbMensaje1= "<br> </br> <table width='650' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED' > "+
		" Agradecemos su interés por formar parte de la red de intermediarios Financieros de NAFINSA. <br>  </br> "+
		" Por este medio se hace de su conocimiento que, para continuar  con el pre-análisis de su solicitud se requiere la siguiente documentación, lo cual permitirá validar su viabilidad de incorporación a NAFINSA:  "+
		" </td></tr></table>"; 	
		
		lbMensaje2= " <br> </br> <table width='650' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED'>"+
		"  Ingrese nuevamente a la herramienta electrónica de  <b>NAFINET </b>, en el menú  <b>“Seguimiento Pre-análisis” </b> para enviar los documentos antes mencionados. "+
		" <br> </br> Folio de Solicitud: "+no_solicitud+
		" <br> </br> Observaciones Ejecutivo CEDI "+
		" </td></tr></table>"; 	 
	
	}else if (pantalla.equals("PreAnalisis_Rechazada")){
		lbTitulo =  "<table width='500' align='center' ><tr align='center'><td align='center' ><H4>Seguimiento de Pre-Análisis  - Solicitud Rechazada </H4></td></tr></table> ";
	
	lbMensaje1= "<br> </br> <table width='650' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED'>"+
	" Agradecemos su interés por formar parte de la red de Intermediarios Financieros de NAFINSA."+
	" <br> </br> Por este medio se hace de su conocimiento que, de acuerdo con la solicitud y la información que proporcionó mediante nuestra herramienta electrónica, la viabilidad de incorporación a Nacional Financiera, S.N.C. no es satisfactoria por lo siguiente :"+
	" </td></tr></table> ";

	lbMensaje3 += " Esperamos contar con su interés en otro momento.  <br> </br> ";
	
	
	}else if (pantalla.equals("Seguimiento_Aceptada")){
	
	lbTitulo =  "<table width='500' align='center' ><tr align='center'><td align='center'><H4>  Solicitud Aceptada </H4></td></tr></table> ";
	
	
	lbMensaje1= "<br> </br> <table width='650' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED'> "+
				" Agradecemos su interés por formar parte de la red de Intermediarios Financieros de NAFINSA."+
				" <br> </br> Por este medio se le informa que su solicitud, con número de Folio <b>"+no_solicitud+"</b>, es viable para su incorporación a Nacional Financiera, S.N.C. "+
				" <br> </br>  En breve un Ejecutivo se comunicará con usted para continuar con el procedimiento. "+
				" <br> </br> Adicionalmente, se le proporciona un Dictamen que registrará la Viabilidad de su solicitud anexo a este correo, con el objeto de integrar el expediente necesario para iniciar su trámite de incorporación a la red e Intermediarios de NAFINSA. "+
				" </td></tr></table> ";
	
	lbMensaje2= "<br> </br> <table width='650' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED'>"+
		" Observaciones Ejecutivo CEDI "+
		" </td></tr></table>"; 	 
		
	}else if (pantalla.equals("Seguimiento_Rechazada")){
	
		lbTitulo =  "<table width='500' align='center' ><tr align='center'><td align='center' ><H4>  Solicitud Rechazada </H4></td></tr></table> ";
	
		lbMensaje1= "<br> </br> <table width='650' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED'> "+
				" Agradecemos su interés por formar parte de la red de Intermediarios Financieros de NAFINSA. "+ 
				"  <br> </br>Por este medio se hace de su conocimiento que, de acuerdo con la solicitud y la información que proporcionó mediante nuestra herramienta electrónica, su solicitud de incorporación a la red de Intermediarios de Nacional Financiera, S.N.C. no es viable, por lo siguiente :"+
				" </td></tr></table> ";
		
		lbMensaje4= "<br> </br> <table width='650' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED' > "+
				"  Adicionalmente, se le proporciona un Dictamen que detalla los motivos por los que su solicitud no resulto Viable anexo a este correo, con el objeto de integrar el expediente necesario para iniciar su trámite de incorporación a la red de Intermediarios de NAFINSA. " +
				" </td></tr></table> ";
				
		lbMensaje3 += " Esperamos contar con su interés en otro momento.  <br> </br> ";
				
	}
	
	
	List doctos=   cediBean.getDoctosCheckListSolicitud( no_solicitud) ; 
	int total =doctos.size();
	lbListaDoctos = "<table width='600' align='CENTER' ><tr align='CENTER'><td align='left'><UL type = disk >";
		
	for( int i =0; i<total; i++) {
		lbListaDoctos += "<LI>"+doctos.get(i).toString()+"</LI>";			
	}
	
	lbListaDoctos += "</UL> </td></tr></table>";
		
	
	lbNombreIFNB  =   "<br> <table width='500' align='LEFT' ><tr align='LEFT'><td align='LEFT'><H4> "+razon_social+" </H4></td></tr></table> ";
 
	lbfechaActual  =   "<br> <table width='500' align='RIGHT' ><tr align='RIGHT'><td align='RIGHT' > "+fechaActual+" </td></tr></table> ";
 
  	
	
	lbMensaje3 +=" Sin otro particular, reciba un cordial saludo."+
	" <br> Para cualquier duda o aclaración favor de contactarnos al 01 800 NAFINSA (623 4672)."+
	 " <br> </br> ATENTAMENTE "+	
	" <br> </br> "+lbNombreUsuario  +"<br>"+lbCorreoUsuario+"</td></tr></table>"+
	//" <br> </br> Con fundamento en el artículo 142 de la Ley de Instituciones de Crédito, 14 fracción I y 15 de la Ley Federal de Transparencia y Acceso a la información Pública Gubernamental, así como al artículo 30 de su reglamento, el contenido del presente mensaje de correo electrónico es de carácter Reservado. "+
	" </td></tr></table>";	  
		
	lbMensaje5= "<br> </br> <table width='650' align='JUSTIFIED' ><tr align='JUSTIFIED'><td align='JUSTIFIED' > "+
				"  Con fundamento en el artículo 142 de la Ley de Instituciones de Crédito, 14 fracción I y 15 de la Ley Federal de Transparencia y Acceso a la información Pública Gubernamental, así como al artículo 30 de su reglamento, el contenido del presente mensaje de correo electrónico es de carácter Reservado." +
				" </td></tr></table> ";		
	
	if (pantalla.equals("Seguimiento_Aceptada")  || pantalla.equals("Seguimiento_Rechazada")){
		pdfViabilidad = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	
	}
	
	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("no_solicitud", no_solicitud);
	
	jsonObj.put("lbTitulo", lbTitulo); 
	jsonObj.put("lbfechaActual", lbfechaActual); 
	jsonObj.put("lbNombreIFNB", lbNombreIFNB);
	jsonObj.put("lbListaDoctos", lbListaDoctos); 
	jsonObj.put("lbMensaje1", lbMensaje1); 
	jsonObj.put("lbMensaje2", lbMensaje2); 
	jsonObj.put("lbMensaje3", lbMensaje3); 
	jsonObj.put("lbMensaje4", lbMensaje4); 
	jsonObj.put("lbMensaje5", lbMensaje5);
		
	jsonObj.put("lbNombreUsuario", lbNombreUsuario_1); 
	jsonObj.put("lbCorreoUsuario", lbCorreoUsuario); 	
	jsonObj.put("pantalla", pantalla); 
	
	jsonObj.put("pdfViabilidad", pdfViabilidad); 
	
	infoRegresar = jsonObj.toString(); 
	
}else  if (informacion.equals("generarPDF")){

	
	String nombreArchivo =  cediBean.generarPDFCorreo(request, parametros);
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString(); 

	

}else  if (informacion.equals("confirmarEnviar")){
	
   boolean  valor  =   cediBean.cambioEstatusConfirma (  parametros  );
	
	//se capturan los movimientos	
	if (pantalla.equals("PreAnalisis_Aceptada")) {
		//5 Doctos. Solicitados al Intermediario  -  movimientos		
		cediBean.setMovimientoSolicitus( no_solicitud, strLogin, "4", "5", "") ; 
	} else if (pantalla.equals("PreAnalisis_Rechazada")) {		
		// 10 Solicitud Rechazada Solicitud Rechazada
		cediBean.setMovimientoSolicitus( no_solicitud, strLogin, "8", "10", "") ; 
	} else if (pantalla.equals("Seguimiento_Aceptada")) {		
		// 8 Solicitud Aceptada  Solicitud Rechazada
		cediBean.setMovimientoSolicitus( no_solicitud, strLogin, "6", "8", "") ; 
	} else if (pantalla.equals("Seguimiento_Rechazada")) {
		// 10 Solicitud Rechazada Solicitud Rechazada
		cediBean.setMovimientoSolicitus( no_solicitud, strLogin, "9", "10", "") ; 
	}
			
	
			   
	jsonObj.put("success", new Boolean(valor));
	jsonObj.put("pantalla", pantalla); 
	infoRegresar = jsonObj.toString(); 
	

}else  if (informacion.equals("ViabilidadPDF")){ 

	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}		

	infoRegresar = jsonObj.toString();
	
	
}
   

%>
<%=infoRegresar%> 

