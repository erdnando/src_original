<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		com.netro.cedi.*,
		com.netro.model.catalogos.*,		
		com.netro.pdf.*,
		netropology.utilerias.*,
		com.netro.seguridad.*,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../39secsession_extjs.jspf" %>
<%

 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_intermediario = (request.getParameter("ic_intermediario")!=null)?request.getParameter("ic_intermediario"):"";
String txtFechaSoliIni = (request.getParameter("txtFechaSoliIni")!=null)?request.getParameter("txtFechaSoliIni"):"";
String txtFechaSoliFinal = (request.getParameter("txtFechaSoliFinal")!=null)?request.getParameter("txtFechaSoliFinal"):"";
String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
String txtObservacionesCEDI = (request.getParameter("txtObservacionesCEDI")!=null)?request.getParameter("txtObservacionesCEDI"):"";
String accion = (request.getParameter("accion")!=null)?request.getParameter("accion"):"";

String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String diaActual    = fechaActual.substring(0,2);
String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
String anioActual   = fechaActual.substring(6,10);
String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
String  lbfechaActual =  "México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual;

UtilUsr utilUsr = new UtilUsr();
Usuario usuarioObj = utilUsr.getUsuario(strLogin);
String destinatario = usuarioObj.getEmail();						
			
String  lbNombreUsuario = "Lic."+strNombreUsuario;
String lbCorreoUsuario  =  destinatario;

String infoRegresar	=	"", consulta ="";
JSONObject jsonObj = new JSONObject();	


Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 

 
ConsAtencionSolic paginador = new ConsAtencionSolic();		

paginador.setIc_intermediario(ic_intermediario);
paginador.setTxtFolioSolicitud(no_solicitud);
paginador.setTxtFechaSoliIni(txtFechaSoliIni);
paginador.setTxtFechaSoliFinal(txtFechaSoliFinal);
paginador.setTipoConsulta(informacion); 
paginador.setEjecutivoAsignado(strLogin); 


CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);	
  

if (informacion.equals("catIntermediarioData")){

	CatalogoIFNB_CEDI catalogo = new CatalogoIFNB_CEDI();
	catalogo.setCampoClave("IC_IFNB_ASPIRANTE");
	catalogo.setCampoDescripcion("CG_RAZON_SOCIAL");
	catalogo.setEstatus("3");
	catalogo.setEjecutivoAsignado(strLogin);	
	catalogo.setOrden("IC_IFNB_ASPIRANTE");
	
	infoRegresar = catalogo.getJSONElementos();	
	
}else  if (informacion.equals("Consultar")  ||  informacion.equals("ConsCheckDoctos")  ){
	
	if ( informacion.equals("ConsCheckDoctos")  ){
		String tieneDoctos =paginador.hayDoctosChekList(no_solicitud, "");
		paginador.setTieneDoctos(tieneDoctos);
	}

	Registros reg	=	queryHelper.doSearch();
		
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString(); 
	
}else  if (informacion.equals("descargaCedula")){ 
	
	List datoGrales = new ArrayList();
	datoGrales.add((String)session.getAttribute("strPais"));//0
	datoGrales.add("-"); //1
	datoGrales.add(strNombre);//2
	datoGrales.add(strNombreUsuario); //3
	datoGrales.add(strLogo); //4
	datoGrales.add(strDirectorioTemp); //5
	datoGrales.add(strDirectorioPublicacion); //6
	datoGrales.add(no_solicitud); //7
	datoGrales.add(""); //8
	datoGrales.add("");//9
	datoGrales.add("3"); //10
	datoGrales.add("ATENCION_SOLICITUD"); //11
	
	
	
	String nombreArchivo  = cediBean.generarArchivoCedula(datoGrales);	
	
	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 
	
		
}else  if (informacion.equals("descargaBalance")  || informacion.equals("descargaEdoResultados")  || informacion.equals("descargacopiaRFC" ) )  {

	String nombreArchivo  =  cediBean.descargaArchivosSolic(no_solicitud, strDirectorioTemp, tipoArchivo );
   jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 
	
}else  if (informacion.equals("guardarCheck")){ 

	String ic_documentos[] = request.getParameterValues("ic_documentos");	
	String nombreDoctos[] = request.getParameterValues("nombreDoctos");
	String activos[] = request.getParameterValues("activos");
				
	boolean  exito =  paginador.guardarDoctosCheck(no_solicitud ,  ic_documentos,  activos ,  nombreDoctos ) ;
	
   jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("accion", accion);
	jsonObj.put("no_solicitud", no_solicitud); 
	infoRegresar = jsonObj.toString(); 
	
	
}
   

%>
<%=infoRegresar%> 