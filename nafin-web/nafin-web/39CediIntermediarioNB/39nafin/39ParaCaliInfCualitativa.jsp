<!DOCTYPE html>
<%@ page import="java.util.*" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/39CediIntermediarioNB/39secsession.jspf" %>
<% String version = (String)session.getAttribute("version"); %>

<html>
<head>
<title>Nafi@net - Autorizaciones de Solicitudes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<%@ include file="/extjs4.jspf" %>
<%if(version!=null){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<script language="JavaScript" src="/nafin/39CediIntermediarioNB/39nafin/39admPreguntas.js"></script>
 <script type="text/javascript" src="39ParaCaliInfCualitativa.js"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%if(version!=null){%>
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<%}%>
		<div id="areaContenido"></div>                                                                                   
   </div>
</div>
<%if(version!=null){%>
<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>