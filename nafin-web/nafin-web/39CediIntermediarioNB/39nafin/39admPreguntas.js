Ext.ns('NE.Informacion');
Ext.define('ModelCatalogo', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'clave', type: 'string'},
        {name: 'descripcion',  type: 'string'}
    ]
});
 

NE.Informacion.FormPreguntas = Ext.extend(Ext.form.FormPanel,{
	numeroPreguntas: 1,
	numeroPregMax: 0,
	modif: '',
	banPreg:1,
	objDataPreg:null,
	agregarPreg:'',
	ID_PREG_MAX:0,
	collapsible: true,
	obj_numPreg:null,
	initComponent : function(){
		 Ext.apply(this, {
			itemId: 'fpPregInform',
			width: 880,
			height:600,
			title: '<p align="center">Preguntas<br><br></p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			defaultType: 'textarea',
			labelWidth: 150,
			autoScroll: true,
			style: 'margin:0 auto;',			
			listeners:{
				afterrender: function(obj){
					if(obj.modif=='s'){
						obj.generarCamposMod(obj, 0, obj.numeroPreguntas, obj.agregarPreg);
					}
				}
			},
			buttons: this.generarBotones(this)
		});
		
		NE.Informacion.FormPreguntas.superclass.initComponent.call(this);
	},
	generarCamposMod: function(fp, newNumPreg, oldNumPreg, agregarPreg){
		var procesaEliminaPregunta = function(options, success, response) {
			
			if (success == true && Ext.JSON.decode(response.responseText).success == true) {
				
				window.location = '39ParaCaliInfCualitativa.jsp';	
				
				//tooltip
			} else {
				NE.util.mostrarErrorPeticion(response);
			}
		}

		var modif = fp.modif;
		var newNumPreg = fp.numeroPregMax;
		var oldNumPreg = fp.numeroPreguntas;
		var id_preg_max = fp.ID_PREG_MAX;
		var lstDataPreg = fp.objDataPreg;
		
		var objInic = fp.obj_numPreg;
		var num_preg_ini = Ext.ComponentQuery.query('#num_real_preg')[0].getValue();
		 var num_preg_final =  Ext.ComponentQuery.query('#numPreg')[0].getValue();
		if(agregarPreg!='S'){
			if(lstDataPreg.length>0){
				var p = lstDataPreg.length;
				var pregAsoc = 1;
				for(var i=0; i<lstDataPreg.length; i++){
					var dataPreg = lstDataPreg[i];
					var vOrdenRenglon =  dataPreg.IGORDENPREG;
					var vNumPregunta =  dataPreg.IGNUMPREG;
					//var vTipoPreg = dataPreg.TIPOPREG; 
					
					var vCgPregunta = dataPreg.CGPREGUNTA;
					
					var vTipoResp = dataPreg.TIPORESP; 
					
					var vNumOpc = dataPreg.IGNUMOPC;
					var vEstado = dataPreg.ICESTADO;
					fp.numeroPregMax = vNumPregunta;
					if(i==0){
						var filaCabPregunta = new Ext.Panel({
							itemId: 'pnlPreg'+vNumPregunta,
							name: 'pnlPreg'+vNumPregunta,
							identificador: 'renglon',
							identificador2: 'renglonPreguntaCab',
							frame: false,
							layout:'table',
							layoutConfig: {columns:5},
							indiceObj: i,
							items: [
								
								{
									xtype:'panel',
									frame: false,
									width:350,
									
									title:'<p align="center">Preguntas</p>'
								},
								{
									xtype:'panel',
									frame: false,
									width:200,									
									title:'<p align="center">Tipo de Respuesta</p>'
								},
								{
									xtype:'panel',
									frame: false,
									width:150,
									title:'<p align="center">N�mero de opciones</p>',
									style: 'margin:0 auto;'
								},
								{
									xtype:'panel',
									frame: false,
									width:150,
									title:'<p align="center">Estado</p>',
									style: 'margin:0 auto;'
								}
							]
						});
						
						fp.add(filaCabPregunta);
						fp.doLayout();
					}
						var filaPregunta;
						var ocultar=false
						
						if(vNumPregunta==4 || vNumPregunta==5  || vNumPregunta==6 || vNumPregunta==7 || vNumPregunta==8  || vNumPregunta==9 ||  vNumPregunta==10   ||  vNumPregunta==11 
						||  vNumPregunta==12	||  vNumPregunta==13  ||  vNumPregunta==16 || vNumPregunta==17   || vNumPregunta==18  || vNumPregunta==19 || vNumPregunta==20  ){
							ocultar=true;							
						}		
					
					 						 
							pregAsoc++;
							filaPregunta = new Ext.Panel({
								itemId: 'pnlPreg'+vNumPregunta,
								identificador: 'renglon',
								identificador2: 'renglonPregunta',
								frame: false,
								layout:'table',
								layoutConfig: {columns:5},
								indiceObj: vNumPregunta,
								items: [
									{
										xtype:'panel',
										frame: true,
										width:350,
										height: 60,
										layout:'hbox',
										title:'',
										items:[
											{
												xtype: 'textfield',
												itemId: 'fila_'+i,
												name: 'fila_'+i,
												value: vNumPregunta,
												hidden:true
											},
											
											{
												xtype: 'textfield',
												itemId: 'ic_pregunta'+vNumPregunta,
												name: 'ic_pregunta'+vNumPregunta,
												value: vNumPregunta,
												hidden:true
											},
											{
												xtype: 'textfield',
												itemId: 'tipo_resp_inicial'+vNumPregunta,
												name: 'tipo_resp_inicial'+vNumPregunta,
												value: vTipoResp,
												hidden:true
											},
											{
												xtype: 'textfield',
												itemId: 'num_opc_inicial'+vNumPregunta,
												name: 'num_opc_inicial'+vNumPregunta,
												value: vNumOpc,
												hidden:true
											},
											{
												xtype: 'hidden',
												itemId: 'fpHideNumeros1'+vNumPregunta,
												name: 'fpHideNumeros',
												value: vNumPregunta
												
											},
											{
												xtype: 'displayfield',
												itemId: 'numPregFila'+vNumPregunta,
												value: i+1,
												width: 20
											},
											{
												xtype: 'hidden',
												id: 'fpNumPreg1'+vNumPregunta,
												name: 'fpNumPreg'+vNumPregunta,
												value: vNumPregunta
											},
											{
												xtype: 'textarea',
												itemId: 'fpPregunta1'+vNumPregunta,
												name: 'fpPregunta'+vNumPregunta,
												allowBlank: false,
												height: 50,
												valueIni: vCgPregunta,
												value: vCgPregunta,
												width: 300,
												maxLength: 300,
												msgTarget:'qtip',
												maxLengthText: 'El tama�o permitido para este campo es de 300 posiciones.'
				
											}]
									},
									{
										xtype:'panel',
										frame: true,
										width:200,
										height: 60,
										layout:'anchor',
										border: false,
										style: 'margin:0 auto;',
										title:'',
										items:[
													{
														xtype:			'combo',
														itemId:				'fpTipoResp1'+vNumPregunta,
														name:				'fpTipoResp'+vNumPregunta,
														hiddenName:		'fpTipoResp'+vNumPregunta,
														fieldLabel:		'',
														style: 'margin:0 auto;',
														emptyText:		'Seleccionar. . .',
														mode:				'local',
														displayField:	'descripcion',
														valueField:		'clave',
														forceSelection:true,
														typeAhead:		true,
														triggerAction:	'all',
														minChars:		1,														
														allowBlank: true,
														anchor: '100%',
														consecutivo: vNumPregunta,
														valueIni: vTipoResp,
														value: vTipoResp,														
														modif: modif,
														readOnly: (vNumPregunta>21)?false:true,
														store: new Ext.data.ArrayStore(
															{
																fields: [
																	'clave',
																	'descripcion'
																],
																data: [
																	['1', 'Libre'], 
																	['2', 'Opci�n Multiple'],
																	['3','Opci�n Vinculada']
																]
															}
															
														),
														listeners: {
															 select: function(combo, record, index) {
																var tipoPreg = Ext.ComponentQuery.query('#fpTipoResp1'+combo.consecutivo)[0];//fpTipoResp1
																var numResp =  Ext.ComponentQuery.query('#fpNumOpc1'+combo.consecutivo)[0];
																var numPregFila = Ext.ComponentQuery.query('#numPregFila'+combo.consecutivo)[0].getValue();																
																if( (combo.getValue()=='' || Number(combo.getValue()))==1){
																	
																	numResp.markInvalid('El n�mero m�ximo de opciones deber� ser 1 para el tipo de respuesta Libre');
																	numResp.focus();
																}else if((combo.getValue()=='' || Number(combo.getValue()))==3){
																	
																	numResp.markInvalid('S�lo pueden existir 2 opciones');
																	numResp.focus();
																	
																}
																
																
															 }
														}
													}
												]
									},
									{
										xtype:'panel',
										frame: true,
										width:150,
										height: 60,
										title:'',
										style: 'margin:0 auto;',
										items:[
											{
												xtype: 'numberfield',
												itemId: 'fpNumOpc1'+vNumPregunta,
												name: 'fpNumOpc'+vNumPregunta,
												allowBlank: false,
												allowDecimals: false,
												valueIni: vNumOpc,
												value: vNumOpc,
												disabled:false,
												maxLength:2,
												autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '2'},
												width:40,
												consecutivo: vNumPregunta,
												modif: modif,
												readOnly: (vNumPregunta==4)?true:false,
												listeners:{
													blur: function(obj){
														
														var vTipoResp = Ext.ComponentQuery.query('#fpTipoResp1'+obj.consecutivo)[0].getValue();
														
														if(vTipoResp=='1' && (obj.getValue()=='' || Number(obj.getValue()))!=1){
															Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0" ><tr> <td ><center>El n�mero m�ximo de opciones deber� ser 1 para el tipo de respuesta Libre</center></td><td></td></table> </div>');
															obj.setValue(1);
														}else if(vTipoResp=='3' && (obj.getValue()=='' || Number(obj.getValue()))!=2){
															Ext.Msg.alert('Aviso', '<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>S�lo pueden existir 2 opciones</center></td><td></td></table> </div>');
															obj.setValue(2);
														}else if(vTipoResp=='2' && Number(obj.valueIni)>Number(obj.getValue())){
															Ext.Msg.alert('Aviso', '<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El nuevo valor debe ser mayor al actual</center></td><td></td></table> </div>');
															obj.setValue(obj.valueIni);
														}
														if(vTipoResp=='1'&&Number(obj.getValue()=='0')){
															Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0" ><tr> <td ><center>El n�mero m�ximo de opciones deber� ser 1 para el tipo de respuesta Libre</center></td><td></td></table> </div>');
															obj.setValue(1);
														}
														
													}
												}
											}
										]
									},
									{
										xtype:'panel',
										frame: true,
										width:150,
										height: 60,
										title:'',
										layout:'table',
										layoutConfig: {columns:2},
										style: 'margin:0 auto;',
										items:[
											{
												xtype: 'checkbox',
												itemId: 'fpSelecccion1'+vNumPregunta,
												name: 'fpSelecccion'+vNumPregunta,
												allowBlank: true,
												boxLabel :(vEstado=='S')?'Activa':'Inactiva', 
												valueIni: (vEstado=='S')?true:false,
												checked   :(vEstado=='S')?true:false,
												width:70,
												consecutivo: vNumPregunta,
												modif: modif, 
												hidden:ocultar												
										  },
										  {
												xtype:'displayfield',
												width: 70,
												hidden:(ocultar==true)?false:true
										 },
										  {
												xtype:'button',
												itemId:'eliminaPregunta'+vNumPregunta,
												name:'eliminaPregunta'+vNumPregunta,
												iconCls:'borrar',
												tooltip :'Eliminar Pregunta',
												width:25,
												numPregunta : vNumPregunta,
												hidden:(vNumPregunta>21)?false:true,
												tipoResp:vTipoResp,
												handler: function(obj) {
													var btnElimina = Ext.ComponentQuery.query('#eliminaPregunta'+obj.numPregunta)[0];
													
													Ext.Msg.confirm('Confirmaci�n','<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>�Desea eliminar la pregunta?</center></td><td></td></table> </div>',
														function(res){
															if(res=='no' || res == 'NO'){
																return;
															}else{	
																
																Ext.Ajax.request({
																			url: '39ParaCaliInfCualitativa.data.jsp',
																			params: Ext.apply({
																				informacion:  'ELIMINAR_PREGUNTA',
																				numPregunta:obj.numPregunta,
																				tipo_dato:obj.tipoResp
																			}),
																			callback: procesaEliminaPregunta
																		});
															}
													});
																
												}
											}
										]
									}
								]
							});
						//}
						
						fp.add(filaPregunta);
						fp.doLayout();
					}
				//}
			}
		
		}
		
		for(oldNumPreg; oldNumPreg< newNumPreg; oldNumPreg++){
				var j = oldNumPreg;
				j ++;
				id_preg_max++;
				filaPregunta  = new Ext.Panel({
					itemId: 'pnlPreg'+id_preg_max,
					identificador: 'renglon',
					identificador2: 'renglonPregunta',
					frame: false,
					layout:'table',
					layoutConfig: {columns:5},
					indiceObj: id_preg_max,
					items: [
						{
							xtype:'panel',
							frame: true,
							width:350,
							height: 60,
							layout:'hbox',
							title:'',
							items:[
								{
									xtype: 'textfield',
									itemId: 'fpHideNumeros1'+id_preg_max,
									name: 'fpHideNumeros',
									value: id_preg_max,
									hidden:true
								},
								{
									xtype: 'textfield',
									itemId: 'ic_pregunta'+id_preg_max,
									name: 'ic_pregunta'+id_preg_max,
									value: id_preg_max,
									hidden:true
								},
								{
									xtype: 'textfield',
									itemId: 'fila_'+oldNumPreg,
									name: 'fila_'+oldNumPreg,
									value: id_preg_max,
									hidden:true
								},
								{
									xtype: 'displayfield',
									itemId: 'numPregFila'+id_preg_max,
									value: j,
									width: 20
								},
								{
									xtype: 'hidden',
									id: 'fpNumPreg1'+id_preg_max,
									name: 'fpNumPreg'+id_preg_max,
									value: id_preg_max
								},
								{
									xtype: 'textarea',
									itemId: 'fpPregunta1'+id_preg_max,
									name: 'fpPregunta'+id_preg_max,
									//fieldLabel: vNumPregunta,
									allowBlank: false,
									maxLength: 300,
									height: 50,
									width: 300
								}
							]
						},
						{
							xtype:'panel',
							frame: true,
							width:200,
							height: 60,
							layout:'anchor',
							border: false,
							style: 'margin:0 auto;',
							title:'',
							items:[
								{
									xtype:			'combo',
									itemId:				'fpTipoResp1'+id_preg_max,
									name:				'fpTipoResp'+id_preg_max,
									hiddenName:		'fpTipoResp'+id_preg_max,
									fieldLabel:		'',
									style: 'margin:0 auto;',
									emptyText:		'Seleccionar. . .',
									mode:				'local',
									displayField:	'descripcion',
									valueField:		'clave',
									forceSelection:true,
									typeAhead:		true,
									triggerAction:	'all',
									minChars:		1,
									allowBlank: false,
									anchor: '100%',
									consecutivo: id_preg_max,
									valueIni: '',
									value: '',
									modif: '',
									store: new Ext.data.ArrayStore(
										{
											fields: [
												'clave',
												'descripcion'
											],
											data: [
												['1', 'Libre'], 
												['2', 'Opci�n Multiple'],
												['3','Opci�n Vinculada']
											]
										}
													
									),
									listeners: {
										select: function(combo, record, index) {
										
												var numResp =  Ext.ComponentQuery.query('#fpNumOpc1'+combo.consecutivo)[0];
												if( (combo.getValue()=='' || Number(combo.getValue()))==1){
													if(numResp.getValue()!=1){		
														numResp.markInvalid('El n�mero m�ximo de opciones deber� ser 1 para el tipo de respuesta Libre');
														numResp.focus();
													}
												}else if((combo.getValue()=='' || Number(combo.getValue()))==3){
													if(numResp.getValue()!=2){								
														numResp.markInvalid('S�lo pueden existir 2 opciones');
														numResp.focus();
													}
																	
												}
															
									 }
									}
								}
							]
						},
						{
							xtype:'panel',
							frame: true,
							width:150,
							height: 60,
							title:'',
							style: 'margin:0 auto;',
							items:[
								{
									xtype: 'numberfield',
									itemId: 'fpNumOpc1'+id_preg_max,
									name: 'fpNumOpc'+id_preg_max,
									allowBlank: false,
									value: 1,
									maxLength:2,
									autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '2'},
									width:40,
									consecutivo: id_preg_max,
									listeners:{
													blur: function(obj){
														var vTipoResp = Ext.ComponentQuery.query('#fpTipoResp1'+obj.consecutivo)[0].getValue();
														
														if(vTipoResp=='1' && (obj.getValue()=='' || Number(obj.getValue()))!=1){
															Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0" ><tr> <td ><center>El n�mero m�ximo de opciones deber� ser 1 para el tipo de respuesta Libre</center></td><td></td></table> </div>');
															obj.setValue(1);
														}else if(vTipoResp=='3' && (obj.getValue()=='' || Number(obj.getValue()))!=2){
															Ext.Msg.alert('Aviso', '<div > <table width="200" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>S�lo pueden existir 2 opciones</center></td><td></td></table> </div>');
															obj.setValue(2);
														}else if(vTipoResp=='2' && Number(obj.valueIni)>Number(obj.getValue())){
															Ext.Msg.alert('Aviso', '<div > <table width="250" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>El nuevo valor debe ser mayor al actual</center></td><td></td></table> </div>');
															obj.setValue(obj.valueIni);
														}
														if(vTipoResp=='1'&&Number(obj.getValue()=='0')){
															Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0" ><tr> <td ><center>El n�mero m�ximo de opciones deber� ser 1 para el tipo de respuesta Libre</center></td><td></td></table> </div>');
															obj.setValue(1);
														}
														
													}
												}
								}
							]
						},
						{
							xtype:'panel',
							frame: true,
							width:150,
							height: 60,
							title:'',
							style: 'margin:0 auto;',
							items:[
								{
									xtype: 'checkbox',
									itemId: 'fpSelecccion1'+id_preg_max,
									name: 'fpSelecccion'+id_preg_max,
									boxLabel :'Inactiva', 														
									width:300,
									consecutivo: id_preg_max,
									modif: ''
								 }
							]
						}
					]
				});
				fp.add(filaPregunta);
				fp.doLayout();
			}
		
		
	},
	generarBotones: function(fpa){
		return [
				{
					text: 'Guardar',
					itemId: 'btnGuardar',
					formBind: true,
					iconCls:'icoGuardar',
					disabled:true
				},
				{
					text: 'Siguiente',
					formBind: true,
					itemId: 'btnSiguiente'
				}
			]
	},
	setHandlerBtnSiguiente: function(fn){
		var btnLoginSig =Ext.ComponentQuery.query('#btnSiguiente')[0];
			btnLoginSig.setHandler(fn);
	},
	setHandlerBtnGuardar: function(fn){
		var btnGuardar =Ext.ComponentQuery.query('#btnGuardar')[0];
			btnGuardar.setHandler(fn);
	}


	
});



NE.Informacion.FormConfResp = Ext.extend(Ext.form.FormPanel,{
	formaPreguntas: null,
	objFilaResp : null,
	numPregReal: 0,
	objDataResp:null,
	modif:'',
	num_preg_ini:0,
	num_preg_final:0,
	initComponent : function(){
		 Ext.apply(this, {
			itemId: 'fpConfResp',
			width: 910,
			height:600,
			title: '<p align="center">Configurador de respuestas</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			defaultType: 'textarea',
			autoScroll: true,
			style: 'margin:0 auto;',
			buttons: this.generarBotones(this),
			listeners:{
				afterrender: function(obj){
					obj.generarCampos(this.formaPreguntas, obj);
				}
			}
		});
		
		NE.Informacion.FormConfResp.superclass.initComponent.call(this);
	},
	generarCampos: function(fpPreguntas, fpResp){
		var objFilaResp = null;
		var objResp = fpResp.objDataResp;
		var modif = fpResp.modif;
		var arrayRenglones = Ext.ComponentQuery.query('#fpPregInform panel[identificador2=renglonPregunta]');
		fpResp.numPregReal = arrayRenglones.length;
		var totalPreg = Number(fpResp.numPregReal)-1;
		 var num_preg_ini = Ext.ComponentQuery.query('#num_real_preg')[0].getValue();
		 var num_preg_final =  Ext.ComponentQuery.query('#numPreg')[0].getValue();
	
		var procesaEliminaOpc = function(options, success, response) {
			
			if (success == true && Ext.JSON.decode(response.responseText).success == true) {
				var  jsonData = Ext.JSON.decode(response.responseText);	
				var numPre = jsonData.numPre;
				var numOpcion = jsonData.numOpcion;
				var activo = jsonData.activo;
				var opcInactivaG = Ext.ComponentQuery.query('#opcInactivaG'+numPre+numOpcion)[0].setValue(activo);
				var btnElimina = Ext.ComponentQuery.query('#inhaResOpc'+numPre+numOpcion)[0];
				btnElimina.enable();
				var infRes =  Ext.ComponentQuery.query('#infResOpc'+numPre+numOpcion)[0];
				infRes.resInactiva = activo;
				btnElimina.resp_inactiva = activo;
				if(activo=='S'){
					btnElimina.setIconCls( 'borrar' );
					btnElimina.setTooltip('Inactivar respuesta');
					infRes.setReadOnly(false);
				}else{
					btnElimina.setIconCls( 'icoAceptar' );
					btnElimina.setTooltip('Activar respuesta');
					infRes.setReadOnly(true);
				}
			} else {
				NE.util.mostrarErrorPeticion(response);
			}
		}
		function createTooltip(view,resp_inactiva, inicio) {
			if(resp_inactiva=='N'){
				view.tip = Ext.create('Ext.tip.ToolTip', {
					target: view.getEl(),
					html: 'Respuesta Inactiva'
				});
			}else{
				if(inicio=='N'){
					view.tip.setDisabled(true);
				}
			}
		}
		
		for(var r=0; r<arrayRenglones.length;r++){
			var indice = arrayRenglones[r].indiceObj;
			var objNumPreg 	= arrayRenglones[r].query('#fpNumPreg1'+indice);
			var objNumPregF   = arrayRenglones[r].query('#numPregFila'+indice);
			var objPregunta 	= arrayRenglones[r].query('#fpPregunta1'+indice);
			var objTipoResp 	= arrayRenglones[r].query('#fpTipoResp1'+indice);
			var objNumOpc		= arrayRenglones[r].query('#fpNumOpc1'+indice);
			
			
			var mostrar = true;
			if(objNumPreg[0].getValue()==4 || objNumPreg[0].getValue()==5  || objNumPreg[0].getValue()==6 || objNumPreg[0].getValue()==7 || objNumPreg[0].getValue()==8  || objNumPreg[0].getValue()==9 ||  objNumPreg[0].getValue()==10   ||  objNumPreg[0].getValue()==11 
						||  objNumPreg[0].getValue()==12	||  objNumPreg[0].getValue()==13  ||  objNumPreg[0].getValue()==16 || objNumPreg[0].getValue()==17   || objNumPreg[0].getValue()==18  || objNumPreg[0].getValue()==19 || objNumPreg[0].getValue()==20  ){
				mostrar=false;
			}else{
				mostrar=false;
			}
			if(indice!=4){
				var objEstado		= arrayRenglones[r].query('#fpSelecccion1'+indice);
			}
			
			var nump = objNumPregF[0].getValue();
			var deshabilitar =false;
			if(nump==4) { deshabilitar =true;   }
		
			var fieldPregunta = new Ext.form.FieldSet({				
				title:'<p style="width: 70em;  word-wrap: break-word;">'+ nump+') '+objPregunta[0].getValue()+'</p>',
				collapsible: true,
				autoHeight:true,
				//disabled:deshabilitar,
				labelWidth: (objTipoResp[0].getValue()==5)?110:10,
				defaultType: 'textfield',
				itemId:'confPreg'+nump,
				hidden:mostrar,
				items:[
					{xtype:'hidden', name: objNumPreg[0].getValue()+'_numPreg', value:objNumPreg[0].getValue() },
					{xtype:'hidden', name: objNumPreg[0].getValue()+'_tipoResp',itemId: objNumPreg[0].getValue()+'_tipoResp', value:objTipoResp[0].getValue()},
					{xtype:'hidden', name: objNumPreg[0].getValue()+'_RESP_EXCL',itemId: objNumPreg[0].getValue()+'_RESP_EXCL', value:''},
					{xtype:'hidden', name: objNumPreg[0].getValue()+'_numOpc', value:objNumOpc[0].getValue()},
					{
						xtype: 'textfield',
						itemId: 'fila_'+r,
						name: 'fila_'+r,
						value: objNumPreg[0].getValue(),
						hidden:true
					}
				]
			});
			
			var nTextPreg = objPregunta[0].getValue();
			var oTextPreg = objPregunta[0].valueIni;
			
			var nTipoResp = Number(objTipoResp[0].getValue());
			var oTipoResp = Number(objTipoResp[0].valueIni);
			
			var nNumOpciones = Number(objNumOpc[0].getValue());
			var oNumOpciones = Number(objNumOpc[0].valueIni);
			
			if(indice!=4){
				var nEstado = objEstado[0].getValue();
			}
			var oEstado = objEstado[0].valueIni;
			
			var vnumPreg = objNumPreg[0].getValue();
			var existCambio = false;
			var cambio_num_opc= false;
			var cambio_tipo_resp= false;
			if(indice!=4){
				if(nTextPreg!=oTextPreg ||nTipoResp!=oTipoResp || nNumOpciones!=oNumOpciones|| nEstado!=oEstado){
					if((nTextPreg!=oTextPreg)&&((nTipoResp==oTipoResp)&&(nNumOpciones==oNumOpciones))){
						existCambio = true;
					}else if((nTipoResp==oTipoResp)&&(nNumOpciones!=oNumOpciones)){
						cambio_num_opc= true;
					}else if(nTipoResp!=oTipoResp){
						cambio_tipo_resp= true;
					}
				}
			}else{
				if(nTextPreg!=oTextPreg ||nTipoResp!=oTipoResp || nNumOpciones!=oNumOpciones){
					existCambio = true;
				}
			}
			var imagen = '';
			if(nTipoResp=='2')imagen ='<div class="icoCheck" style="float:left;width:15px;">&#160</div>';
			if(nTipoResp=='3')imagen ='<div class="icoRadio" style="float:left;width:15px;">&#160</div>';
			var imagInactiva = '';
			if(nTipoResp=='2')imagInactiva ='<div class="borrar" style="float:left;width:15px;">&#160</div>';
			if(nTipoResp=='3')imagInactiva ='<div class="borrar" style="float:left;width:15px;">&#160</div>';
			var lonMaxina= null;
			for(var z=0; z<nNumOpciones; z++){
				var objData=null;
				var valorMaximo =0;
				var ban = 0;
				
				if(existCambio== true ||(cambio_tipo_resp==false && cambio_num_opc== true)){
					if(z<oNumOpciones){
						objData = eval("objResp.p"+vnumPreg);						
						if(objData != undefined) {					
							objData = objData[z];
						}
					 }else{
						objData=null; 
					 }
				}else if(cambio_tipo_resp==true){
					objData=null; 
				}else if(existCambio== false &&  cambio_tipo_resp== false && cambio_num_opc== false ){
					objData = eval("objResp.p"+vnumPreg);						
					if(objData != undefined) {					
						objData = objData[z];
					}
				}
				if(objData!=null){
					if(objData.PREG_EXC!=''){
						 Ext.ComponentQuery.query('#'+objNumPreg[0].getValue()+'_RESP_EXCL')[0].setValue('S');
					}
					
				}
					if(objTipoResp[0].getValue()=='1'){
						
						fieldPregunta.add([						
							{
								xtype: 'fieldcontainer',
								fieldLabel: '',
								disabled:deshabilitar,
								labelWidth: 100,
								layout: 'hbox',
								itemId:'libre'+objNumPreg[0].getValue()+z,
								items: [
									{	xtype:'hidden', 
										itemId:  objNumPreg[0].getValue()+'IC_RESP'+z,
										name:  objNumPreg[0].getValue()+'IC_RESP'+z,
										value:((objData==null)?'':objData.IC_RESP)
									},
									{
										xtype: 'textfield',
										itemId:'opcInactivaG'+objNumPreg[0].getValue()+z,
										name:'opcInactivaG'+objNumPreg[0].getValue()+z,
										value:(objData==null)?'S':objData.CS_ACTIVO,
										hidden:true
									},
									{
										xtype:'displayfield',
										width: 100
									},
									{
										xtype: 'textfield',
										itemId:  objNumPreg[0].getValue()+'_resp1_'+z,
										name:  objNumPreg[0].getValue()+'_resp_'+z,
										maxLength:300,
										width: 150,
										value: 'Respuesta abierta '+objNumPreg[0].getValue()+z,
										disabled: true
										
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'combo',
										itemId:  'tipoRes'+objNumPreg[0].getValue()+z,
										name:  'tipoRes'+objNumPreg[0].getValue()+z,
										width: 150,
										value: ((objData==null)?'':objData.LIBRE_TIPO_DATO),
										forceSelection:true,
										style: 'margin:0 auto;',
										mode:				'local',
										displayField:	'descripcion',
										valueField:		'clave',
										typeAhead:		true,
										triggerAction:	'all',
										allowBlank: false,
										
										numPregunta : objNumPreg[0].getValue(),
										opcionPreg : z,
										lonMaxina:valorMaximo,
										store: new Ext.data.ArrayStore({
											itemId:	'storeCatTipoRes'+objNumPreg[0].getValue()+z,
											modif: modif,
											fields: [
												'clave',
												'descripcion'
											],
											data: [['N', 'Num�rico'], ['A', 'Alfanum�rico'], ['F', 'Fecha']]
										}),
										listeners:{
											select:{
											  fn:function(combo, value) {
													var longRes = Ext.ComponentQuery.query('#longRes'+combo.numPregunta+combo.opcionPreg)[0];
													
													if(combo.getValue()=='N'){
														longRes.setValue('');
														longRes.maskRe=/[0-9\,]/;
														longRes.regex = /(^(((\d){1,17}))(\,\d{1,2})$)|(^((\d){1,17})$)/;
														longRes.regexText="<div > <table width=100><tr> <td >El tama�o m�ximo del campos es de 17 enteros 2 decimales</td><td></td></table> </div>";
													}else{
														longRes.setValue('');
														longRes.maskRe='!\"#$%&\'()*+,-./0123456789:;<=>?@';
														longRes.regex = /^.{1,400}$/;
														longRes.regexText="<div > <table width=100><tr> <td >El tama�o m�ximo del campos es de 400</td><td></td></table> </div>";
														
													
													}
											  }
											}
										}
										
										
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'textfield',
										itemId: 'longRes'+objNumPreg[0].getValue()+z,
										name:  'longRes'+objNumPreg[0].getValue()+z,
										numPregunta : objNumPreg[0].getValue(),
										opcionPreg : z,
										width: 100,
										value: ((objData==null)?'':objData.LIBRE_LONG.replace(".", ",")),
										allowBlank: false
										
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'combo',
										itemId: 'obliRes'+objNumPreg[0].getValue()+z,
										name:  'obliRes'+objNumPreg[0].getValue()+z,
										width: 150,
										value: ((objData==null)?'':(objData.LIBRE_OBLIG=='N'||objData.LIBRE_OBLIG=='S')?objData.LIBRE_OBLIG:''),
										forceSelection:true,
										style: 'margin:0 auto;',
										mode:				'local',
										displayField:	'descripcion',
										valueField:		'clave',
										typeAhead:		true,
										triggerAction:	'all',
										allowBlank: false,
										store: new Ext.data.ArrayStore({
											itemId:	'storeCatObliRes'+objNumPreg[0].getValue()+z,
											modif: modif,
											fields: [
												'clave',
												'descripcion'
											],
											data: [['S', 'Obligatorio'], ['N', 'No obligatorio']]
										})
									}
								]
							}
						]);
						if(objData!=null){
							var longRes = Ext.ComponentQuery.query('#'+'longRes'+objNumPreg[0].getValue()+z)[0];	
							longRes.maskRe=(objData.LIBRE_TIPO_DATO=='N')?/[0-9\,]/:'!\"#$%&\'()*+,-./0123456789:;<=>?@';
							longRes.regex = (objData.LIBRE_TIPO_DATO=='N')?/(^(((\d){1,17}))(\,\d{1,2})$)|(^((\d){1,17})$)/:/^.{1,400}$/;
							longRes.regexText=(objData.LIBRE_TIPO_DATO=='N')?"<div > <table width=100><tr> <td >El tama�o m�ximo del campos es de 17 enteros 2 decimales</td><td></td></table> </div>":"<div > <table width=100><tr> <td >El tama�o m�ximo del campos es de 400</td><td></td></table> </div>"; 
						}
					}else if(objTipoResp[0].getValue()=='2'){
					console.log(objNumPreg[0].getValue()+' - '+z);
						fieldPregunta.add([							
							{
								xtype: 'fieldcontainer',
								fieldLabel: '',								
								//disabled:deshabilitar,
								layout: 'hbox',
								itemId:'opcMult'+objNumPreg[0].getValue()+z,
								items: [
									{	xtype:'hidden', 
										itemId:  objNumPreg[0].getValue()+'IC_RESP'+z,
										name:  objNumPreg[0].getValue()+'IC_RESP'+z,
										value:((objData==null)?'':objData.IC_RESP)
									},
									{
										xtype:'textfield',
										itemId:'banRespAb'+objNumPreg[0].getValue()+z,
										name:'banRespAb'+objNumPreg[0].getValue()+z,
										width: 100,
										value:((objData==null)?'N':objData.RESP_ABIERTA_ASO),
										hidden:true
										
									},
									{
										xtype:'textfield',
										width: 10,
										itemId:'banRespExclu'+objNumPreg[0].getValue()+z,
										name:	 'banRespExclu'+objNumPreg[0].getValue()+z,
										value:((objData==null)?'N':objData.BAN_EXCLUIR),
										hidden:true
									},
									{
										xtype:'button',
										itemId:'inhaResOpc'+objNumPreg[0].getValue()+z,
										name:'btnEliminaReg'+objNumPreg[0].getValue()+z,
										iconCls:((objData==null)?'borrar':((objData.CS_ACTIVO=='N')?'icoAceptar':'borrar')),
										width:23,
										disabled:deshabilitar,
										resp_inactiva:(objData==null)?'S':objData.CS_ACTIVO,
										tooltip :((objData==null)?'Activar respuesta':((objData.CS_ACTIVO=='S')?'Activar respuesta':'Inactivar respuesta')),
										numPregunta : objNumPreg[0].getValue(),
										numOpcion :z,
										handler: function(obj) {
											var btnElimina = Ext.ComponentQuery.query('#inhaResOpc'+obj.numPregunta+obj.numOpcion)[0];
											var inactiva = '';
														
											var opcInactivaG = Ext.ComponentQuery.query('#opcInactivaG'+obj.numPregunta+obj.numOpcion)[0];
											var infRes =  Ext.ComponentQuery.query('#infResOpc'+obj.numPregunta+obj.numOpcion)[0];
											if(obj.resp_inactiva=='N'){
												inactiva = 'S';
												opcInactivaG.setValue('S');
												obj.resp_inactiva= 'S';
												btnElimina.setIconCls( 'borrar' );
												
												btnElimina.setTooltip('Activar respuesta');
												infRes.setReadOnly(false);
												createTooltip(infRes,'S','N');
											}else if(obj.resp_inactiva=='S'){
												inactiva = 'N';
												opcInactivaG.setValue('N');
												obj.resp_inactiva= 'N';
												btnElimina.setIconCls( 'icoAceptar' );
												btnElimina.setTooltip('Inactivar respuesta');
												infRes.setReadOnly(true);
												createTooltip(infRes,'N','N');
											}
											
														
										}
									},
									{
										xtype: 'hidden',
										itemId:'opcInactivaG'+objNumPreg[0].getValue()+z,
										name:'opcInactivaG'+objNumPreg[0].getValue()+z,
										//value:objData.CS_ACTIVO
										value:(objData==null)?'S':objData.CS_ACTIVO
										
									},
									{
										xtype: 'displayfield',
										value: imagen,
										itemId:'selecResOpc'+objNumPreg[0].getValue()+z,
										width: (nTipoResp=='2' || nTipoResp=='3')?20:0
									},
									{
										xtype: 'textfield',
										itemId:'infResOpc'+objNumPreg[0].getValue()+z,
										name: 'infResOpc'+ objNumPreg[0].getValue()+z,
										readOnly:deshabilitar,
										numPre : objNumPreg[0].getValue(),
										opcion : z,
										resInactiva : ((objData==null)?'':objData.CS_ACTIVO),
										maxLength:300,
										value: ((objData==null)?'':objData.OPC_RESP),
										width: 200,
										//tip :(objData==null)?'':((objData.CS_ACTIVO=='N')?'Respuesta Inactiva':''),
										listeners: {
											render: function(c) {
												 if(c.resInactiva=='N'){
												  createTooltip(c,'N','S');
												}
											}
										}
									},
									{
										xtype:'button',
										name:'btnResAbierta'+objNumPreg[0].getValue()+z,
										iconCls: 'aceptar',
										width:23,
										disabled:deshabilitar,
										tooltip :'Agregar respuesta abierta',
										itemId:'addResAbObc'+objNumPreg[0].getValue()+z,
										numPregunta:objNumPreg[0].getValue(),
										numOpcion:z,
										handler: function(obj){
											var opcionAbierta = Ext.ComponentQuery.query('#libre'+obj.numPregunta+obj.numOpcion)[0];
											opcionAbierta.show();
											var btnOcultarAbi = Ext.ComponentQuery.query('#btnOcultarAbi'+obj.numPregunta+obj.numOpcion)[0];
											btnOcultarAbi.show();
											var banRespAb = Ext.ComponentQuery.query('#banRespAb'+obj.numPregunta+obj.numOpcion)[0];
											banRespAb.setValue('S');
										}
										
									},
									{
										xtype:'button',
										itemId:'btnExcluOpc'+objNumPreg[0].getValue()+z,
										name:  'btnExcluOpc'+objNumPreg[0].getValue()+z,
										iconCls: 'icotag_blue_add',
										width:23,
										tooltip :'<div > <table width=150><tr> <td >Registrar pregunta vinculada</td><td></td></table> </div>',
										numPregunta:objNumPreg[0].getValue(),
										numOpcion:z,
										//hidden:(objNumPreg[0].getValue()=='4')?false:true,
										handler: function(obj){
											var opcionInhabi= Ext.ComponentQuery.query('#excluOpc'+obj.numPregunta+obj.numOpcion)[0];
											opcionInhabi.show();
											
											
											var btnOcultarExclu = Ext.ComponentQuery.query('#btnOcultarExclu'+obj.numPregunta+obj.numOpcion)[0];
											
											btnOcultarExclu.show();
											var banRespExclu = Ext.ComponentQuery.query('#banRespExclu'+obj.numPregunta+obj.numOpcion)[0];
											banRespExclu.setValue('S');
											
											var pregExcOpc = Ext.ComponentQuery.query('#pregExcOpc'+obj.numPregunta+obj.numOpcion)[0];
											pregExcOpc.allowBlank= false;
											pregExcOpc.validate();
											
										}
									}
								]
							},
							{
								xtype: 'fieldcontainer',
								fieldLabel: '',
								labelWidth: 100,
								hidden:true,
								layout: 'hbox',
								hidden:((objData==null)?true:(objData.RESP_ABIERTA_ASO=='N')?true:false),
								itemId:'libre'+objNumPreg[0].getValue()+z,
								items: [
									{
										xtype:'displayfield',
										width: 100
									},
									{
										xtype:'button',
										itemId:'btnOcultarAbi'+objNumPreg[0].getValue()+z,
										name:'btnOcultarAbi'+objNumPreg[0].getValue()+z,
										iconCls: 'borrar',
										width:23,
										disabled:deshabilitar,
										tooltip :'Inactivar respuesta',
										numPregunta : objNumPreg[0].getValue(),
										numOpcion :z,
										handler: function(obj) {
											Ext.ComponentQuery.query('#tipoRes'+obj.numPregunta+obj.numOpcion)[0].setValue('');
											Ext.ComponentQuery.query('#longRes'+obj.numPregunta+obj.numOpcion)[0].setValue('');
											Ext.ComponentQuery.query('#addOtro'+obj.numPregunta+obj.numOpcion)[0].setValue(false);
											var libre = Ext.ComponentQuery.query('#libre'+obj.numPregunta+obj.numOpcion)[0];
											var btnOcultarAbi = Ext.ComponentQuery.query('#btnOcultarAbi'+obj.numPregunta+obj.numOpcion)[0];
											var banRespAb = Ext.ComponentQuery.query('#banRespAb'+obj.numPregunta+obj.numOpcion)[0];
											banRespAb.setValue('N');
											btnOcultarAbi.hide();
											libre.hide();
											
										}
									},
									{
										xtype: 'textfield',
										itemId:  objNumPreg[0].getValue()+'_resp1_'+z,
										name:  objNumPreg[0].getValue()+'_resp_'+z,
										maxLength:300,
										width: 150,
										value: 'Respuesta abierta',
										disabled: true
										
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'combo',
										itemId:  'tipoRes'+objNumPreg[0].getValue()+z,
										name:  'tipoRes'+objNumPreg[0].getValue()+z,
										width: 150,
										value: ((objData==null)?'':objData.LIBRE_TIPO_DATO),
										forceSelection:true,
										numPreg : ((objData==null)?'':objNumPreg[0].getValue()),
										opcion: z,
										style: 'margin:0 auto;',
										mode:				'local',
										displayField:	'descripcion',
										valueField:		'clave',
										typeAhead:		true,
										triggerAction:	'all',
										//allowBlank: true,										
										hidden:((nump==4)?true:false),
										store: new Ext.data.ArrayStore({
											itemId:	'storeCatTipoRes'+objNumPreg[0].getValue()+z,
											modif: modif,
											fields: [
												'clave',
												'descripcion'
											],
											data: [['N', 'Num�rico'], ['A', 'Alfanum�rico'], ['F', 'Fecha']]
										}),
										listeners:{
											select:{
											  fn:function(combo, value) {
													var longRes = Ext.ComponentQuery.query('#'+'longRes'+combo.numPreg+combo.opcion)[0];
													
													if(combo.getValue()=='N'){
														longRes.setValue('');
														longRes.maskRe=/[0-9\,]/;
														longRes.regex = /(^(([0-3]{1}))(\,[0-2]{1})$)/;
														longRes.regexText="<div > <table width=50><tr> <td >El valor maxim� es 3,2</td><td></td></table> </div>";
													}else{
														longRes.setValue('');
														longRes.maskRe='!\"#$%&\'()*+,-./0123456789:;<=>?@';
														longRes.regex = /^.{1,30}$/;
														longRes.regexText="<div > <table width=100><tr> <td >El tama�o m�ximo del campos es de 30</td><td></td></table> </div>";
														
													
													}
											  }
											}
										}
										
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'textfield',
										itemId: 'longRes'+objNumPreg[0].getValue()+z,
										name:  'longRes'+objNumPreg[0].getValue()+z,
										numPregunta : objNumPreg[0].getValue(),
										opcionPreg : z,
										width: 100,
										value: ((objData==null)?'':objData.LIBRE_LONG.replace(".", ",")),
										hidden:((nump==4)?true:false)
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'checkbox',
										itemId: 'addOtro'+objNumPreg[0].getValue()+z,
										name: 'addOtro'+objNumPreg[0].getValue()+z,
										boxLabel :'Otra Respuesta',
										//hidden:((nump==4)?true:false),	
										checked   :((objData==null)?false:(objData.OPCION_OTROS!='S')?false:true),
										width:300,								
										modif: modif
									}
								]
							},
							{
								xtype: 'fieldcontainer',
								fieldLabel: '',
								labelWidth: 100,
								layout: 'hbox',
								hidden:(objData==null)?true:(objData.PREG_EXC=='')?true:false,
								itemId:'excluOpc'+objNumPreg[0].getValue()+z,
								items: [
									{
										xtype:'displayfield',
										width: 100
									},
									{
										xtype:'button',
										itemId:'btnOcultarExclu'+objNumPreg[0].getValue()+z,
										name:'btnOcultarExclu'+objNumPreg[0].getValue()+z,
										iconCls: 'borrar',
										width:23,
										tooltip :'Inactivar respuesta',
										numPregunta : objNumPreg[0].getValue(),
										numOpcion :z,
										handler: function(obj) {
											var excluOpc = Ext.ComponentQuery.query('#excluOpc'+obj.numPregunta+obj.numOpcion)[0];
											var btnOcultarExclu = Ext.ComponentQuery.query('#btnOcultarExclu'+obj.numPregunta+obj.numOpcion)[0];
											var banRespExclu = Ext.ComponentQuery.query('#banRespExclu'+obj.numPregunta+obj.numOpcion)[0];
											Ext.ComponentQuery.query('#pregExcOpc'+obj.numPregunta+obj.numOpcion)[0].setValue('');
											var pregExcOpc = Ext.ComponentQuery.query('#pregExcOpc'+obj.numPregunta+obj.numOpcion)[0];
											pregExcOpc.allowBlank= true;
											pregExcOpc.validate();
											banRespExclu.setValue('N');
											btnOcultarExclu.hide();
											excluOpc.hide();
											
											
											
										}
									},
									{
										xtype: 'displayfield',
										value: 'Preguntas Vinculadas:',
										width: 150
									},
									
									{
										xtype: 'textfield',
										itemId:  'pregExcOpc'+objNumPreg[0].getValue()+z,
										name:'pregExcOpc'+objNumPreg[0].getValue()+z,
										value: ((objData==null)?'':objData.PREG_EXC),
										numPregunta : objNumPreg[0].getValue(),
										numOpcion :z,
										PREG_EXC:((objData==null)?'':objData.PREG_EXC),
										
										maxLength:7,
										width: '50',
										regexText:"Favor de ingresar correctamente el rango de preguntas. Rangos correctos: [4], [1-4] ",
										regex: /(^(((\d){1,3}))((-){1}(\d){1,3})$)|(^((\d){1,3})$)/,
										listeners: {
											blur: function(fld, value){
												numPregunta =fld.numPregunta;
												numOpcion =fld.numOpcion;
											
												var PREG_EXC = fld.getValue();
												if(PREG_EXC!=''){
													var index = (PREG_EXC).search("-");
													var rang1=0;
													var rang2=0;
													if(index != -1){
																
														rang1 = (PREG_EXC).substring(0,index);
														rang2 = (PREG_EXC).substring(index+1);
														rang1=rang1;
														rang2=rang2;
														for(var w=rang1; w<=rang2; w++){
															var numPreg = fld.numPregunta;
														
															if(Number(totalPreg)+1<Number(w)){
																fld.allowBlank= false;
																Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Favor de verificar que las preguntas a excluir existan dentro del listado</center></td><td></td></table> </div>');
																fld.setValue('');
																fld.validate();
																return;
																
															}else if(Number(w)<Number(numPreg)){
																fld.allowBlank= false;
																Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>S�lo se permite parametrizar preguntas posteriores a la Pregunta Vinculada</center></td><td></td></table> </div>');
																fld.setValue('');
																fld.validate();
																return;
																
															}else{
																var RESP_EXCL = Ext.ComponentQuery.query('#'+w+'_RESP_EXCL')[0];
																if(RESP_EXCL.getValue()){
																	fld.allowBlank= false;
																	Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Favor de verificar, est� vinculando preguntas parametrizadas como Respuestas Vinculadas</center></td><td></td></table> </div>');
																	fld.setValue('');
																	fld.validate();
																	return;
																}
																
															}
														}
													}else{
														rang1 = PREG_EXC;
														var w=rang1;
														var numPreg = fld.numPregunta;
														
														if(Number(totalPreg)+1<Number(w)){
															fld.allowBlank= false;
															Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Favor de verificar que las preguntas a excluir existan dentro del listado</center></td><td></td></table> </div>');
															fld.setValue('');
															fld.validate();
															return;
															
														}else if(Number(w)<Number(numPreg)){
															fld.allowBlank= false;
															Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>S�lo se permite parametrizar preguntas posteriores a la Pregunta Vinculada</center></td><td></td></table> </div>');
															fld.setValue('');
															fld.validate();
															return;
															
														}else{
															var RESP_EXCL = Ext.ComponentQuery.query('#'+w+'_RESP_EXCL')[0];
																if(RESP_EXCL.getValue()){
																	fld.allowBlank= false;
																	Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Favor de verificar, est� vinculando preguntas parametrizadas como Respuestas Vinculadas</center></td><td></td></table> </div>');
																	fld.setValue('');
																	fld.validate();
																	return;
																}
														}
													}
												}else{
													fld.allowBlank= false;
													fld.validate();
													
												}
												
											}
										}
									}
								]
							}
						]);
						if(objData!=null){
							var longRes = Ext.ComponentQuery.query('#'+'longRes'+objNumPreg[0].getValue()+z)[0];	
							longRes.regexText=(objData.LIBRE_TIPO_DATO=='N')?'<div > <table width=100><tr> <td >El valor maxim� es 3,2</td><td></td></table> </div>':'El valor maxim� es 30',
							longRes.regex=(objData.LIBRE_TIPO_DATO=='N')?/(^(([0-3]{1}))(\,[0-3]{1})$)/:/^.{1,30}$/,
							longRes.maskRe=(objData.LIBRE_TIPO_DATO=='N')?/[0-9\,]/:'!\"#$%&\'()*+,-./0123456789:;<=>?@'
						}
						
					}else{
					
					fieldPregunta.add([							
							{
								xtype: 'fieldcontainer',
								fieldLabel: '',
								
								disabled:deshabilitar,
								layout: 'hbox',
								itemId:'opcExcluyente'+objNumPreg[0].getValue()+z,
								items: [
									{	xtype:'hidden', 
										itemId:  objNumPreg[0].getValue()+'IC_RESP'+z,
										name:  objNumPreg[0].getValue()+'IC_RESP'+z,
										value:((objData==null)?'':objData.IC_RESP)
									},
									{
										xtype:'textfield',
										width: 90,
										itemId:'banRespAb'+objNumPreg[0].getValue()+z,
										name:	  'banRespAb'+objNumPreg[0].getValue()+z,
										value:((objData==null)?'N':objData.RESP_ABIERTA_ASO),
										hidden:true
									},
									{
										xtype:'textfield',
										width: 10,
										itemId:'banRespExclu'+objNumPreg[0].getValue()+z,
										name:	 'banRespExclu'+objNumPreg[0].getValue()+z,
										value:((objData==null)?'N':objData.BAN_EXCLUIR),
										hidden:true
									},
									
									{
										xtype:'button',
										itemId:'inhaResOpc'+objNumPreg[0].getValue()+z,
										name:'btnEliminaReg'+objNumPreg[0].getValue()+z,
										iconCls:((objData==null)?'borrar':((objData.CS_ACTIVO=='N')?'icoAceptar':'borrar')),
										width:23,
										disabled:deshabilitar,
										resp_inactiva:(objData==null)?'S':objData.CS_ACTIVO,
										tooltip :((objData==null)?'Activar respuesta':((objData.CS_ACTIVO=='S')?'Activar respuesta':'Inactivar respuesta')),
										numPregunta : objNumPreg[0].getValue(),
										numOpcion :z,
										handler: function(obj) {
											var btnElimina = Ext.ComponentQuery.query('#inhaResOpc'+obj.numPregunta+obj.numOpcion)[0];
											var inactiva = '';
														
											var opcInactivaG = Ext.ComponentQuery.query('#opcInactivaG'+obj.numPregunta+obj.numOpcion)[0];
											var infRes =  Ext.ComponentQuery.query('#infResOpc'+obj.numPregunta+obj.numOpcion)[0];
											if(obj.resp_inactiva=='N'){
												inactiva = 'S';
												opcInactivaG.setValue('S');
												obj.resp_inactiva= 'S';
												btnElimina.setIconCls( 'borrar' );
												btnElimina.setTooltip('Activar respuesta');
												infRes.setReadOnly(false);
												createTooltip(infRes,'S','N');
											}else if(obj.resp_inactiva=='S'){
												inactiva = 'N';
												opcInactivaG.setValue('N');
												obj.resp_inactiva= 'N';
												btnElimina.setIconCls( 'icoAceptar' );
												
												btnElimina.setTooltip('Inactivar respuesta');
												infRes.setReadOnly(true);
												createTooltip(infRes,'N','N');
											}
										}
									},
									{
										xtype: 'hidden',
										itemId:'opcInactivaG'+objNumPreg[0].getValue()+z,
										name:'opcInactivaG'+objNumPreg[0].getValue()+z,
										//value:objData.CS_ACTIVO
										value:(objData==null)?'S':objData.CS_ACTIVO
									},
									{
										xtype: 'displayfield',
										value: imagen,
										itemId:'selecResOpc'+objNumPreg[0].getValue()+z,
										name:'selecResOpc'+objNumPreg[0].getValue()+z,
										width: (nTipoResp=='2' || nTipoResp=='3')?20:0
									},
									{
										xtype: 'textfield',
										itemId:'infResOpc'+objNumPreg[0].getValue()+z,
										name:  'infResOpc'+objNumPreg[0].getValue()+z,
										maxLength:300,
										numPre : objNumPreg[0].getValue(),
										opcion : z,
										resInactiva : ((objData==null)?'':objData.CS_ACTIVO),
										value: ((objData==null)?'':objData.OPC_RESP),
										width: 200,
										//tip: 'Respuesta Inactiva',
										listeners: {
											render: function(c) {
												 if(c.resInactiva=='N'){
												  createTooltip(c,'N','S');
												}
											}
										}
									},
									{
										xtype:'button',
										iconCls: 'aceptar',
										width:23,
										tooltip :'Agregar respuesta abierta',
										itemId:'addResAbOpc'+objNumPreg[0].getValue()+z,
										name:  'addResAbOpc'+objNumPreg[0].getValue()+z,
										numPregunta:objNumPreg[0].getValue(),
										numOpcion:z,
										handler: function(obj){
											var opcionAbierta = Ext.ComponentQuery.query('#libre'+obj.numPregunta+obj.numOpcion)[0];
											opcionAbierta.show();
											var btnOcultarAbi = Ext.ComponentQuery.query('#btnOcultarAbi'+obj.numPregunta+obj.numOpcion)[0];
											btnOcultarAbi.show();
											var banRespAb = Ext.ComponentQuery.query('#banRespAb'+obj.numPregunta+obj.numOpcion)[0];
											banRespAb.setValue('S');
										}
									},
									{
										xtype:'button',
										itemId:'btnExcluOpc'+objNumPreg[0].getValue()+z,
										name:  'btnExcluOpc'+objNumPreg[0].getValue()+z,
										iconCls: 'icotag_blue_add',
										width:23,
										tooltip :'<div > <table width=150><tr> <td >Registrar pregunta vinculada</td><td></td></table> </div>',
										numPregunta:objNumPreg[0].getValue(),
										numOpcion:z,
										handler: function(obj){
											var opcionInhabi= Ext.ComponentQuery.query('#excluOpc'+obj.numPregunta+obj.numOpcion)[0];
											opcionInhabi.show();
											
											
											var btnOcultarExclu = Ext.ComponentQuery.query('#btnOcultarExclu'+obj.numPregunta+obj.numOpcion)[0];
											btnOcultarExclu.show();
											var banRespExclu = Ext.ComponentQuery.query('#banRespExclu'+obj.numPregunta+obj.numOpcion)[0];
											banRespExclu.setValue('S');
											
											var pregExcOpc = Ext.ComponentQuery.query('#pregExcOpc'+obj.numPregunta+obj.numOpcion)[0];
											pregExcOpc.allowBlank= false;
											pregExcOpc.validate();
										}
									}
								]
							},
							{
								xtype: 'fieldcontainer',
								fieldLabel: '',
								//combineErrors: false,
								//msgTarget: 'side',
								labelWidth: 100,
								layout: 'hbox',
								hidden:((objData==null)?true:(objData.RESP_ABIERTA_ASO=='N')?true:false),
								itemId:'libre'+objNumPreg[0].getValue()+z,
								items: [
									{
										xtype:'displayfield',
										width: 100
									},
									{
										xtype:'button',
										itemId:'btnOcultarAbi'+objNumPreg[0].getValue()+z,
										name:'btnOcultarAbi'+objNumPreg[0].getValue()+z,
										iconCls: 'borrar',
										width:23,
										tooltip :'Inactivar respuesta',
										numPregunta : objNumPreg[0].getValue(),
										numOpcion :z,
										handler: function(obj) {
											var libre = Ext.ComponentQuery.query('#libre'+obj.numPregunta+obj.numOpcion)[0];
											var btnOcultarAbi = Ext.ComponentQuery.query('#btnOcultarAbi'+obj.numPregunta+obj.numOpcion)[0];
											var banRespAb = Ext.ComponentQuery.query('#banRespAb'+obj.numPregunta+obj.numOpcion)[0];
											Ext.ComponentQuery.query('#tipoRes'+obj.numPregunta+obj.numOpcion)[0].setValue('');
											Ext.ComponentQuery.query('#longRes'+obj.numPregunta+obj.numOpcion)[0].setValue('');
											Ext.ComponentQuery.query('#addOtro'+obj.numPregunta+obj.numOpcion)[0].setValue(false);
										
											banRespAb.setValue('N');
											btnOcultarAbi.hide();
											libre.hide();
											
										}
									},
									{
										xtype: 'textfield',
										itemId:  objNumPreg[0].getValue()+'_resp1_'+z,
										name:  objNumPreg[0].getValue()+'_resp_'+z,
										maxLength:300,
										width: 150,
										value: 'Respuesta abierta',
										disabled: true
										
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'combo',
										itemId:  'tipoRes'+objNumPreg[0].getValue()+z,
										name:  'tipoRes'+objNumPreg[0].getValue()+z,
										width: 150,
										value: ((objData==null)?'':objData.LIBRE_TIPO_DATO),
										forceSelection:true,
										style: 'margin:0 auto;',
										mode:				'local',
										displayField:	'descripcion',
										valueField:		'clave',
										typeAhead:		true,
										triggerAction:	'all',
										//allowBlank: true,
										store: new Ext.data.ArrayStore({
											itemId:	'storeCatTipoRes'+objNumPreg[0].getValue()+z,
											modif: modif,
											fields: [
												'clave',
												'descripcion'
											],
											data: [['N', 'Num�rico'], ['A', 'Alfanum�rico'], ['F', 'Fecha']]
										})
										
										
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'textfield',
										itemId: 'longRes'+objNumPreg[0].getValue()+z,
										name:  'longRes'+objNumPreg[0].getValue()+z,
										maxLength:30,
										numPregunta : objNumPreg[0].getValue(),
										opcionPreg : z,
										width: 100,
										value: ((objData==null)?'':objData.LIBRE_LONG),
										//allowBlank: true,
										listeners: {
											blur: function(fld, value){
												var tipoResp = Ext.ComponentQuery.query('#'+'tipoRes'+fld.numPregunta+fld.opcionPreg)[0];
													if(tipoResp.getValue()=='N'){
													if(Number(fld.getValue())>30){
														fld.markInvalid('El valor maximo para est� campo es de 30');
														fld.focus();
														return;
													}
												}else if(tipoResp.getValue()=='A'){
													if(Number(fld.getValue())>400){
														fld.markInvalid('El valor maximo para est� campo es de 400');
														fld.focus();
														return;
														
													}
												}else{
													fld.markInvalid('El tama�o maximo para est� campo es de 10');
													fld.focus();
													return;
												}
											}
										}
										
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'checkbox',
										itemId: 'addOtro'+objNumPreg[0].getValue()+z,
										name: 'addOtro'+objNumPreg[0].getValue()+z,
										boxLabel :'Otra Respuesta', 
										//valueIni: vEstado,
										checked   :((objData==null)?false:(objData.OPCION_OTROS!='S')?false:true),
										width:300,
									//	consecutivo: vNumPregunta,
										modif: modif
									}
								]
								
							},
							{
								xtype: 'fieldcontainer',
								fieldLabel: '',
								labelWidth: 100,
								layout: 'hbox',
								hidden:((objData==null)?true:(objData.BAN_EXCLUIR=='S')?false:true),
								itemId:'excluOpc'+objNumPreg[0].getValue()+z,
								items: [
									{
										xtype:'displayfield',
										width: 100
									},
									{
										xtype:'button',
										itemId:'btnOcultarExclu'+objNumPreg[0].getValue()+z,
										name:'btnOcultarExclu'+objNumPreg[0].getValue()+z,
										iconCls: 'borrar',
										width:23,
										tooltip :'Inactivar respuesta',
										numPregunta : objNumPreg[0].getValue(),
										numOpcion :z,
										handler: function(obj) {
											var excluOpc = Ext.ComponentQuery.query('#excluOpc'+obj.numPregunta+obj.numOpcion)[0];
											var btnOcultarExclu = Ext.ComponentQuery.query('#btnOcultarExclu'+obj.numPregunta+obj.numOpcion)[0];
											var banRespExclu = Ext.ComponentQuery.query('#banRespExclu'+obj.numPregunta+obj.numOpcion)[0];
											Ext.ComponentQuery.query('#pregExcOpc'+obj.numPregunta+obj.numOpcion)[0].setValue('');
											var pregExcOpc = Ext.ComponentQuery.query('#pregExcOpc'+obj.numPregunta+obj.numOpcion)[0];
											pregExcOpc.allowBlank=true;
											pregExcOpc.validate();
											
											banRespExclu.setValue('N');
											btnOcultarExclu.hide();
											excluOpc.hide();
											
											
											
										}
									},
									{
										xtype: 'displayfield',
										value: 'Preguntas Vinculadas:',
										width: 150
									},
									
									{
										xtype: 'textfield',
										itemId:  'pregExcOpc'+objNumPreg[0].getValue()+z,
										name:'pregExcOpc'+objNumPreg[0].getValue()+z,
										value: ((objData==null)?'':objData.PREG_EXC),
										numPregunta : objNumPreg[0].getValue(),
										numOpcion :z,
										maxLength:7,
										PREG_EXC:((objData==null)?'':objData.PREG_EXC),
										width: '50',
										regexText:"Favor de ingresar correctamente el rango de preguntas. Rangos correctos: [4], [1-4] ",
										regex: /(^(((\d){1,3}))((-){1}(\d){1,3})$)|(^((\d){1,3})$)/,
										listeners: {
											blur: function(fld, value){
											//fld.getValue()
												var PREG_EXC = fld.getValue();
												
												if(PREG_EXC!=''){
													
													var index = (PREG_EXC).search("-");
													var rang1=0;
													var rang2=0;
													if(index != -1){
																
														rang1 = (PREG_EXC).substring(0,index);
														rang2 = (PREG_EXC).substring(index+1);
														rang1=rang1;
														rang2=rang2;
														for(var w=rang1; w<=rang2; w++){
															var numPreg = fld.numPregunta;
														
															if(Number(totalPreg)+1<Number(w)){
																fld.allowBlank= false;
																Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Favor de verificar que las preguntas a excluir existan dentro del listado</center></td><td></td></table> </div>');
																fld.setValue('');
																fld.validate();
																return;
																
															}else if(Number(w)<Number(numPreg)){
																fld.allowBlank= false;
																Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>S�lo se permite parametrizar preguntas posteriores a la Pregunta Vinculada</center></td><td></td></table> </div>');
																fld.setValue('');
																fld.validate();
																return;
																
															}else{
																var RESP_EXCL = Ext.ComponentQuery.query('#'+w+'_RESP_EXCL')[0];
																if(RESP_EXCL.getValue()){
																	fld.allowBlank= false;
																	Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Favor de verificar, est� vinculando preguntas parametrizadas como Respuestas Vinculadas</center></td><td></td></table> </div>');
																	fld.setValue('');
																	fld.validate();
																	return;
																}
															}
														}
													}else{
														rang1 = PREG_EXC;
														var w=rang1;
														var numPreg = fld.numPregunta;
														
														if(Number(totalPreg)+1<Number(w)){
															fld.allowBlank= false;
															Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Favor de verificar que las preguntas a excluir existan dentro del listado</center></td><td></td></table> </div>');
															fld.setValue('');
															fld.validate();
															return;
															
														}else if(Number(w)<Number(numPreg)){
															fld.allowBlank= false;
															Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>S�lo se permite parametrizar preguntas posteriores a la Pregunta Vinculada</center></td><td></td></table> </div>');
															fld.setValue('');
															fld.validate();
															return;
															
														}else{
															var RESP_EXCL = Ext.ComponentQuery.query('#'+w+'_RESP_EXCL')[0];
																if(RESP_EXCL.getValue()){
																	fld.allowBlank= false;
																	Ext.Msg.alert('Aviso', '<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>Favor de verificar, est� vinculando preguntas parametrizadas como Respuestas Vinculadas</center></td><td></td></table> </div>');
																	fld.setValue('');
																	fld.validate();
																	return;
																}
														}
													}
												}else{
													fld.allowBlank= false;
													fld.validate();
													
												}
												
											}
										}
									}
								]
							}
					]);
						
				}
			}
			fieldPregunta.doLayout();
			objFilaResp =  new Ext.Panel({
				itemId: 'pnlResp'+objNumPreg[0].getValue(),
				identificador: 'renglon',
				frame: false
				
			});
			objFilaResp.add(fieldPregunta);
			objFilaResp.doLayout();
				
			fpResp.add(objFilaResp);
			fpResp.doLayout();
		
		}
	},
	generarBotones: function(fpa){
		return [
				{
					text: 'Vista Previa',
					itemId: 'btnVistaPrevia'
				},
				{
					text: 'Terminar',
					formBind: true,
					itemId: 'btnTerminarResp'
				},
				{
					text: 'Cancelar',
					itemId: 'btnCancelarResp'
				}
			]
	},
	setHandlerBtnTermina: function(fn){
		var btnTerminar = Ext.ComponentQuery.query('#btnTerminarResp')[0];
			btnTerminar.setHandler(fn);
	},
	setHandlerBtnCancelar: function(fn){
		var btnCancelar = Ext.ComponentQuery.query('#btnCancelarResp')[0];
			btnCancelar.setHandler(fn);			
	},
	setHandlerBtnVistaPrevia: function(fn){
		var btnVista = Ext.ComponentQuery.query('#btnVistaPrevia')[0];
			btnVista.setHandler(fn);
	}
});

NE.Informacion.FormVistaPrev = Ext.extend(Ext.form.FormPanel,{
	formaPreguntas: null,
	formaRespuestas: null,
	titulo: null,
	objDataResp: null,
	numPregReal: 0,
	modif: '',
	initComponent : function(){
		 Ext.apply(this, {
			itemId: 'fpVistaprev',
			width: 810,
			height:400,
			title: this.titulo,			
			frame: true,   
			border: true, 
			monitorValid: true,			
			autoScroll: true,
			style: 'margin:0 auto;',
			listeners:{
				afterrender: function(obj){
					obj.generarCampos(this.formaPreguntas,this.formaRespuestas, obj);
				}
			}
		});
		
		NE.Informacion.FormVistaPrev.superclass.initComponent.call(this);
	},
	generarCampos: function(fpPreguntas,fpConfig,fpResp){
		var fpConfPreg = Ext.ComponentQuery.query('#fpConfResp');
		
		var objFilaResp = null;
		var fieldRubroInicial = null;
		var fieldRubroActual = null;
		var modif = fpResp.modif;
		var arrayRenglones = Ext.ComponentQuery.query('#fpPregInform panel[identificador2=renglonPregunta]');
		fpResp.numPregReal = arrayRenglones.length;
		var totalPreg = Number(fpResp.numPregReal)-1;
		for(var r=0; r<arrayRenglones.length;r++){
			fieldRubroInicial = null;
			fieldRubroActual = null;
			var indice = arrayRenglones[r].indiceObj;
			var idRubros = arrayRenglones[r].itemId;
			//var arrayRubro = fpPreguntas.find('preguntaAsoc',idRubros);
			
			//preguntas-----------------------------------------------------------
			var objNumPreg 	= arrayRenglones[r].query('#fpNumPreg1'+indice);
			var objPregunta 	= arrayRenglones[r].query('#fpPregunta1'+indice);
			//var objTipoPreg 	= arrayRenglones[r].query('#fpTipoPreg1'+indice);
			var objTipoResp 	= arrayRenglones[r].query('#fpTipoResp1'+indice);
			var objNumOpc		= arrayRenglones[r].query('#fpNumOpc1'+indice);
			var objEstado		= arrayRenglones[r].query('#fpSelecccion1'+indice);
			var objNumPregF   = arrayRenglones[r].query('#numPregFila'+indice);
			
			//se genera fila 
	
			
			var nTipoResp = Number(objTipoResp[0].getValue());
			var oTipoResp = Number(objTipoResp[0].valueIni);
			
			var nNumOpciones = Number(objNumOpc[0].getValue());
			var oNumOpciones = Number(objNumOpc[0].valueIni);
			
			var vnumPreg = objNumPreg[0].getValue();
			
			var mostrar = true;
			if(objNumPreg[0].getValue()==4 || objNumPreg[0].getValue()==5  || objNumPreg[0].getValue()==6 || objNumPreg[0].getValue()==7 || objNumPreg[0].getValue()==8  || objNumPreg[0].getValue()==9 ||  objNumPreg[0].getValue()==10   ||  objNumPreg[0].getValue()==11 
						||  objNumPreg[0].getValue()==12	||  objNumPreg[0].getValue()==13  ||  objNumPreg[0].getValue()==16 || objNumPreg[0].getValue()==17   || objNumPreg[0].getValue()==18  || objNumPreg[0].getValue()==19 || objNumPreg[0].getValue()==20  ){
				mostrar=false;
			}else{
				var objEdo		= arrayRenglones[r].query('#fpSelecccion1'+indice);
				if(objEdo[0].getValue()==true){
					mostrar=false;		
				}else{
					mostrar=true;		
				}
				
			}
			var nump = objNumPregF[0].getValue();
			var deshabilitar =false;
			if(nump==4) { deshabilitar =true;   }
		if(objEdo[0].getValue()==true){	
			var fieldPregunta = new Ext.form.FieldSet({
					title: (r+1)+') '+objPregunta[0].getValue(),
					collapsible: true,
					hidden:mostrar,
					autoHeight:true//,
					
			});
			for(var z=0; z<nNumOpciones; z++){
			
					var  valorSigno ='';
					//if(objTipoResp[0].getValue()==1) {  valorSigno ='$'; }
					if(objTipoResp[0].getValue()==2 || objTipoResp[0].getValue()==3  ) {  valorSigno ='%'; }
										
				if(objTipoResp[0].getValue()==3|| objTipoResp[0].getValue()==2){
					
					if((objTipoResp[0].getValue()==2)){
						
						var opcionMult = fpConfig.query('#opcMult'+objNumPreg[0].getValue()+z);
						
						var datoOpcion = opcionMult[0].query('#infResOpc'+objNumPreg[0].getValue()+z);
						var banRespAb = opcionMult[0].query('#banRespAb'+objNumPreg[0].getValue()+z);
						
						var valorDatoInfo = datoOpcion[0].getValue();
						var valorBanRespAb = banRespAb[0].getValue();// Para ver si se selecciono respuesta abierta
						var libre = fpConfig.query('#libre'+objNumPreg[0].getValue()+z);
						
						var tipoDato = libre[0].query('#tipoRes'+objNumPreg[0].getValue()+z);
						var longResp = libre[0].query('#longRes'+objNumPreg[0].getValue()+z);
						var addOtro = libre[0].query('#addOtro'+objNumPreg[0].getValue()+z);
						
						var valorTipoDato = tipoDato[0].getValue();
						var valorLongResp = Number(longResp[0].getValue());
						var valorAddOtro = addOtro[0].getValue();
						
						var valorMaximo =0;
						var ban = 0;
						if(valorTipoDato == 'N'&&valorLongResp >30  ){
							valorMaximo = 30;
							ban = 1;
						}
						if(valorTipoDato == 'A'&&valorLongResp >400){
							valorMaximo = 400;
							ban = 1;
						}
						if(ban != 1){
							valorMaximo=valorLongResp;
						}
						fieldPregunta.add([
							{
								xtype: 'fieldcontainer',
								fieldLabel: '',
								//combineErrors: false,
								//msgTarget: 'side',
								labelWidth: 100,
								hidden:false,
								layout: 'hbox',
								//itemId:'confOpcMult'+objNumPreg[0].getValue()+z,
								items: [
									{
										xtype: 'checkbox',
										name:   'checkOpcion'+objNumPreg[0].getValue()+z,
										itemId: 'checkOpcion'+objNumPreg[0].getValue()+z,
										boxLabel :' '+valorDatoInfo,
										width: 150
									},
									{
										xtype:'displayfield',
										width: 25										
									},									
									{
										xtype: 'textfield',
										itemId:  objNumPreg[0].getValue()+'cajaTexto'+z,
										name:  objNumPreg[0].getValue()+'cajaTexto'+z,
										numPreg:objNumPreg[0].getValue(),
										numOpcion:z,
										maxLength:valorMaximo,
										width: (valorTipoDato =='N')?150:300,
										allowBlank: true,
										msgTarget: 'side',
										margins: '0 20 0 0', 
										hidden:(valorBanRespAb =='N')?true:false,
										maskRe : (valorTipoDato =='N')?/[0-9]/:'!\"#$%&\'()*+,-./0123456789:;<=>?@'										
									},
									{
										xtype:'displayfield',
										width: 25,
										hidden:(valorBanRespAb =='N')?true:false,
										value:valorSigno	
									},
									{
										xtype: 'textfield',
										itemId:  objNumPreg[0].getValue()+'addCajaTexto'+z,
										name:  objNumPreg[0].getValue()+'addCajaTexto'+z,
										numPreg:objNumPreg[0].getValue(),
										numOpcion:z,
										maxLength:400,
										width: 150,
										allowBlank: true,
										msgTarget: 'side',
										margins: '0 20 0 0', 
										hidden:(valorAddOtro ==true)?false:true,
										maskRe : '!\"#$%&\'()*+,-./0123456789:;<=>?@'
									}
								]
							}
						]);
						
					}else{
						var opcExcluyente = fpConfig.query('#opcExcluyente'+objNumPreg[0].getValue()+z);
						
						var datoOpcion = opcExcluyente[0].query('#infResOpc'+objNumPreg[0].getValue()+z);
						var banRespAb = opcExcluyente[0].query('#banRespAb'+objNumPreg[0].getValue()+z);
						var banRespExclu = opcExcluyente[0].query('#banRespExclu'+objNumPreg[0].getValue()+z);
						
						var valorDatoInfo = datoOpcion[0].getValue();
						var valorBanRespAb = banRespAb[0].getValue();// Para ver si se selecciono respuesta abierta
						var valorBanRespExclu = banRespExclu[0].getValue();
						// Informaci�n libre
						var libre = fpConfig.query('#libre'+objNumPreg[0].getValue()+z);
						
						var tipoDato = libre[0].query('#tipoRes'+objNumPreg[0].getValue()+z);
						var longResp = libre[0].query('#longRes'+objNumPreg[0].getValue()+z);
						var addOtro = libre[0].query('#addOtro'+objNumPreg[0].getValue()+z);
						
						var valorTipoDato = tipoDato[0].getValue();
						var valorLongResp = Number(longResp[0].getValue());
						var valorAddOtro = addOtro[0].getValue();
						// Informaci�n excluyente
						var excluyente = fpConfig.query('#excluOpc'+objNumPreg[0].getValue()+z);
						var pregExcOpc = excluyente[0].query('#pregExcOpc'+objNumPreg[0].getValue()+z);
						var valorPregExcOpc = pregExcOpc[0].getValue();
						
						var valorMaximo =0;
						var ban = 0;
						if(valorTipoDato == 'N'&&valorLongResp >30  ){
							valorMaximo = 30;
							ban = 1;
						}
						if(valorTipoDato == 'A'&&valorLongResp >400){
							valorMaximo = 400;
							ban = 1;
						}
						if(ban != 1){
							valorMaximo=valorLongResp;
						}
						fieldPregunta.add([
							{
								xtype: 'radiogroup',
								itemId: 'radioGrupo'+objNumPreg[0].getValue()+z,
								name: 'radioGrupo'+objNumPreg[0].getValue()+z,
								//width:100,
								valExcluyente: valorPregExcOpc,
								numPregEx : objNumPreg[0].getValue(),
								numOpc :z,
								//PREG_EXC:PREG_EXC,
								//abierta:RESP_ABIERTA_ASO,
								//otra_opc:OPCION_OTROS,
								items: 
								[
									{	
										xtype: 'radiofield',
										boxLabel	:valorDatoInfo,
										name : 'radio'+objNumPreg[0].getValue(),
										itemId: 'radioGrupo'+objNumPreg[0].getValue()+z,
										width:100,
										inputValue: objNumPreg[0].getValue()+'radio'+z
									},
									{
										xtype: 'textfield',										
										itemId:  objNumPreg[0].getValue()+'cajaTexto'+z,
										name:  objNumPreg[0].getValue()+'cajaTexto'+z,
										numPreg:objNumPreg[0].getValue(),
										numOpcion:z,
										maxLength:valorMaximo,
										width: (valorTipoDato =='N')?150:300,
										allowBlank:true,
										msgTarget: 'side',
										margins: '0 20 0 0', 
										hidden:(valorBanRespAb =='S')?false:true,
										maskRe : (valorTipoDato =='N')?/[0-9]/:'!\"#$%&\'()*+,-./0123456789:;<=>?@'
												
									},
									{
										xtype:'displayfield',
										width: 25
									},
									{
										xtype: 'textfield',										
										itemId:  objNumPreg[0].getValue()+'addCajaTexto'+z,
										name:  objNumPreg[0].getValue()+'addCajaTexto'+z,
										numPreg:objNumPreg[0].getValue(),
										numOpcion:z,
										maxLength:400,
										
										width: 150,
										allowBlank: true,
										msgTarget: 'side',
										margins: '0 20 0 0', 
										hidden:(valorAddOtro ==true)?false:true,
										maskRe : '!\"#$%&\'()*+,-./0123456789:;<=>?@'										
										
									}
								],
								listeners:{
									change:function(obj, newValue, oldValue, eOpts){
										/*if(newValue['radio'+obj.numPregEx]!=undefined){
													//se habilitaan todas las respuestas
													for(var w=0; w<=totalPreg; w++){
														var respPrev = Ext.ComponentQuery.query('#pnlVista'+w);
														respPrev[0].enable();
													
													}
													
													//se inhabitan respuestas si existe un rango capturado de preguntas excluyentes
													if(obj.valExcluyente!=''){
														var index = (obj.valExcluyente).search("-");
														var rang1=0;
														var rang2=0;
																		
														if(index != -1){
														
															rang1 = (obj.valExcluyente).substring(0,index);
															rang2 = (obj.valExcluyente).substring(index+1);
															rang1=rang1 -1;
															rang2=rang2 -1;
															for(var w=rang1; w<=rang2; w++){
																var respPrev = Ext.ComponentQuery.query('#pnlVista'+w);
																respPrev[0].disable(true);
															}
														}else{
															rang1 = obj.valExcluyente;
															var w=rang1-1;
															var respPrev = Ext.ComponentQuery.query('#pnlVista'+w);
															respPrev[0].setDisabled(true);
														}
													}
											}*/
										/*var campo = Ext.ComponentQuery.query('#'+field.numPreg+'cajaTexto'+field.opcion)[0];
										var campoAdd = Ext.ComponentQuery.query('#'+field.numPreg+'addCajaTexto'+field.opcion)[0];
										var PREG_EXC = field.PREG_EXC;
										Ext.ComponentQuery.query('#btnGuardar')[0].enable();
										Ext.ComponentQuery.query('#guardar_activo_InfCuali')[0].setValue('S');
										if(newValue[field.numPreg+'ic_opcion_resp']!=undefined){
											Ext.ComponentQuery.query('#'+field.numPreg+'_opcion_check')[0].setValue(field.opcion);
											if(field.abierta=='S'){
												campo.setDisabled(false);
												campo.allowBlank= false;
												campo.validate();
											}
											if(field.otra_opc=='S'){
												campoAdd.setDisabled(false);
												campoAdd.allowBlank= false;
												campoAdd.validate();
											}
											if(PREG_EXC!=''){
												var index = (PREG_EXC).search("-");
												var rang1=0;
												var rang2=0;
												if(index != -1){
												
												rang1 = (PREG_EXC).substring(0,index);
												rang2 = (PREG_EXC).substring(index+1);
												rang1=rang1;
												rang2=rang2;
												for(var w=rang1; w<=rang2; w++){
													var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
													limpiarPregunta(w);
													Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
													respPrev[0].setDisabled(true);//pnlOpcPreg
												}
											}else{
												rang1 = PREG_EXC;
												var w=rang1;
												var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
												limpiarPregunta(w);
												Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('S');
												respPrev[0].setDisabled(true);
											}
										}else{
											if(field.opcion==0){
												PREG_EXC = Ext.ComponentQuery.query('#'+field.numPreg+'radioGroup1')[0].PREG_EXC;
											}else{
												PREG_EXC = Ext.ComponentQuery.query('#'+field.numPreg+'radioGroup0')[0].PREG_EXC;
											}
											if(PREG_EXC!=''){
												var index = (PREG_EXC).search("-");
												var rang1=0;
												var rang2=0;
												if(index != -1){
													rang1 = (PREG_EXC).substring(0,index);
													rang2 = (PREG_EXC).substring(index+1);
													rang1=rang1;
													rang2=rang2;
													for(var w=rang1; w<=rang2; w++){
														var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
														Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('N');
														respPrev[0].setDisabled(false);//pnlOpcPreg
																	
													}
												}else{
													rang1 = PREG_EXC;
													var w=rang1;
													var respPrev = Ext.ComponentQuery.query('#pnlPreg'+w);
													Ext.ComponentQuery.query('#'+w+'pregExcluyante')[0].setValue('N');
													respPrev[0].setDisabled(false);
												}
											}
										}
														
									}else{
										campo.setValue('');
										campo.setDisabled(true);
										campo.allowBlank= true;
										campo.validate();
										campoAdd.setValue('');
										campoAdd.setDisabled(true);
										campoAdd.allowBlank= true;
										campoAdd.validate();
									}*/
														
																	
								}
							}
			
						}
						
						]);
						
						
					}
				
				
				}else{
					
					var libre = fpConfig.query('#libre'+objNumPreg[0].getValue()+z);
					
					var tipoDato = libre[0].query('#tipoRes'+objNumPreg[0].getValue()+z);
					var longResp = libre[0].query('#longRes'+objNumPreg[0].getValue()+z);
					var obliRes = libre[0].query('#obliRes'+objNumPreg[0].getValue()+z);
					
					var valorTipoDato = tipoDato[0].getValue();
					var valorLongResp = Number(longResp[0].getValue());
					var valorObli = Number(obliRes[0].getValue());
					
					var valorMaximo =0;
					var ban = 0;
					if(valorTipoDato == 'N'&&valorLongResp >30  ){
						valorMaximo = 30;
						ban = 1;
					}
					if(valorTipoDato == 'A'&&valorLongResp >400){
						valorMaximo = 400;
						ban = 1;
					}
					if(ban != 1){
						valorMaximo=valorLongResp;
					}
					var valorObliResp = obliRes[0].getValue()
					fieldPregunta.add([
							{
								xtype: 'fieldcontainer',
								fieldLabel: '',
								combineErrors: false,
								msgTarget: 'side',
								labelWidth: 100,
								layout: 'hbox',
								items: [
									{
										xtype:'displayfield',
										width: 25,
										value:valorSigno	
									},
									{										
										xtype: 'textfield',
										itemId:  objNumPreg[0].getValue()+'cajaTexto'+z,
										name:  objNumPreg[0].getValue()+'cajaTexto'+z,
										numPreg:objNumPreg[0].getValue(),
										numOpcion:z,
										maxLength:valorMaximo,
										width: (valorTipoDato =='N')?150:300,
										allowBlank: (valorObli==1)?false:true,
										msgTarget: 'side',
										margins: '0 20 0 0', 
										hidden:(valorTipoDato =='N'||valorTipoDato =='A')?false:true,
										maskRe : (valorTipoDato =='N')?/[0-9]/:'!\"#$%&\'()*+,-./0123456789:;<=>?@'																				
									},
									{
										xtype: 'datefield',
										itemId:  objNumPreg[0].getValue()+'date'+z,
										name:  objNumPreg[0].getValue()+'date'+z,
										numPreg:objNumPreg[0].getValue(),
										allowBlank: (valorObli==1)?false:true,
										startDay: 0,
										width: 100,
										minValue: '01/01/1901',
										msgTarget: 'side',
										vtype: 'rangofecha', 
										campoFinFecha: 'fechaSolic2',
										margins: '0 20 0 0', 
										hidden:(valorTipoDato =='F')?false:true
									},
									{
										xtype:'displayfield',
										width: 25,
										value:''
									}
								]
							}
						]);
				
				}
			}
			fieldPregunta.doLayout();
			objFilaResp =  new Ext.Panel({
				itemId: 'pnlVista'+r,
				identificador: 'renglon',
				frame: false
				
			});
			objFilaResp.add(fieldPregunta);
			objFilaResp.doLayout();
			fpResp.add(objFilaResp);
			fpResp.doLayout();
			
			}	
		}
	}
});
