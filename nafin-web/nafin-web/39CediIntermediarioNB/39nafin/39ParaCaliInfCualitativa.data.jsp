<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*,  
	java.io.*,  
	java.text.*,  
	com.netro.exception.*,
	com.netro.cadenas.*, 
	java.sql.*,  
	com.netro.afiliacion.*, 
	netropology.utilerias.*,
	com.netro.model.catalogos.CatalogoSimple,
	org.apache.commons.logging.Log,
	com.netro.cedi.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/39CediIntermediarioNB/39secsession.jspf"%>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):""; 
String infoRegresar="", consulta="", msg ="", mensaje ="";
JSONObject jsonObj = new JSONObject();
Cedi cediEjb = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 

				
if(informacion.equals("Inicializacion")){
	Map InfoGeneral = null;
	InfoGeneral = cediEjb.getPreguntasIFNB();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("InfoGeneral", InfoGeneral);
	infoRegresar = jsonObj.toString();
}else if (informacion.equals("CatalogoTipoPregunta")){

	List reg =  cediEjb.getCatalogoTipoPregunta(); 
	
   jsonObj = new JSONObject();
   jsonObj.put("success", new Boolean(true));
   jsonObj.put("registros", reg);
   infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("ModificarEncuesta")  ){
	List lstPreguntas = new ArrayList();
	List lstRespuestas = new ArrayList();
	String num_preg_inicial = "0";
	String numPregReal = "0";
	String pregunta = "";
	numPregReal = (request.getParameter("txtNumPreg")!=null)?request.getParameter("txtNumPreg"):"0";
	num_preg_inicial = (request.getParameter("num_real_preg")!=null)?request.getParameter("num_real_preg"):"0";
	pregunta = (request.getParameter("pregunta")!=null)?request.getParameter("pregunta"):"";
	//Parametros de la Forma Preguntas
	String fpPregunta = "";
	String fpTipoResp = "";
	String fpNumOpc = "";
	String fpEstado = "";
	String icpregunta = "", ic_tipo_resp_inicial="", num_opc_inicial="";
	String fila = "";
	int num_preg_inic = Integer.parseInt(num_preg_inicial);
	int num_total_preg  = Integer.parseInt(numPregReal);
	if(num_total_preg>0){
		HashMap dataPreguntas = new HashMap();
		HashMap dataRespuestas = new HashMap();
		HashMap datafila = new HashMap();
		int numPregunta = 0;
		
		//SE OBTIENE LISTADO DE PREGUNTAS PARAMTERIZADAS
		for(int x=0; x<Integer.parseInt(numPregReal); x++){
		
			dataPreguntas = new HashMap();
			datafila = new HashMap();
			fila = (request.getParameter("fila_"+x)!=null)?request.getParameter("fila_"+x):"";
			fpPregunta = (request.getParameter("fpPregunta"+fila)!=null)?request.getParameter("fpPregunta"+fila):"";
			fpTipoResp = (request.getParameter("fpTipoResp"+fila)!=null)?request.getParameter("fpTipoResp"+fila):"";
			fpNumOpc = (request.getParameter("fpNumOpc"+fila)!=null)?request.getParameter("fpNumOpc"+fila):"";
			fpEstado = (request.getParameter("fpSelecccion"+fila)!=null)?request.getParameter("fpSelecccion"+fila):"N";
			icpregunta = (request.getParameter("ic_pregunta"+fila)!=null)?request.getParameter("ic_pregunta"+fila):"";
			ic_tipo_resp_inicial = (request.getParameter("tipo_resp_inicial"+fila)!=null)?request.getParameter("tipo_resp_inicial"+fila):"";
			num_opc_inicial = (request.getParameter("num_opc_inicial"+fila)!=null)?request.getParameter("num_opc_inicial"+fila):"";
			if(fpEstado.equals("on")){
				fpEstado="S";
			}
			
			dataPreguntas.put("ORDENPREG",String.valueOf(++num_total_preg));
			dataPreguntas.put("PREGUNTA",fpPregunta);
			dataPreguntas.put("TIPORESP",fpTipoResp);
			dataPreguntas.put("NUMOPC",fpNumOpc);
			dataPreguntas.put("NUMPREG",String.valueOf(fila));
			dataPreguntas.put("ESTADO",fpEstado);
			dataPreguntas.put("icpregunta",icpregunta);	
			dataPreguntas.put("ic_tipo_resp_inicial",ic_tipo_resp_inicial);
			dataPreguntas.put("num_opc_inicial",num_opc_inicial);
			datafila.put("dato_pregunta",dataPreguntas);
			lstRespuestas = new ArrayList();
			if(pregunta.equals("N")){
				int contResp =0;
				int numOpciones = Integer.parseInt(fpNumOpc);
				for(int y=0; y<numOpciones;y++){
					contResp ++;
					dataRespuestas = new HashMap();
					//libre
					String CS_ACTIVO = (request.getParameter("opcInactivaG"+fila+y)!=null)?request.getParameter("opcInactivaG"+fila+y):"N";
					
					String tipoRes = (request.getParameter("tipoRes"+fila+y)!=null)?request.getParameter("tipoRes"+fila+y):"";
					String longRes = (request.getParameter("longRes"+fila+y)!=null)?request.getParameter("longRes"+fila+y):"";
					String obliRes = (request.getParameter("obliRes"+fila+y)!=null)?request.getParameter("obliRes"+fila+y):"";
						//Multip
					String banRespAb = (request.getParameter("banRespAb"+fila+y)!=null)?request.getParameter("banRespAb"+fila+y):"N";
					String infResOpc = (request.getParameter("infResOpc"+fila+y)!=null)?request.getParameter("infResOpc"+fila+y):"";
					String addOtro = (request.getParameter("addOtro"+fila+y)!=null)?request.getParameter("addOtro"+fila+y):"N";
					String IC_RESP = (request.getParameter(fila+"IC_RESP"+y)!=null)?request.getParameter(fila+"IC_RESP"+y):"";				
					
					if(addOtro.equals("on")){
						addOtro="S";
					}
					// Radio
					String banRespExclu = (request.getParameter("banRespExclu"+fila+y)!=null)?request.getParameter("banRespExclu"+fila+y):"N";
					String pregExcOpc = (request.getParameter("pregExcOpc"+fila+y)!=null)?request.getParameter("pregExcOpc"+fila+y):"";
					
					dataRespuestas.put("IC_RESPUESTA",String.valueOf(contResp));								
					dataRespuestas.put("IC_PREG", fila);
					dataRespuestas.put("TIPO_RESP",fpTipoResp);
					
					//LIBRE
					dataRespuestas.put("LIBRE_TIPO_DATO",tipoRes);
					dataRespuestas.put("LIBRE_LONG",longRes);
					dataRespuestas.put("LIBRE_OBLIG",obliRes);
					
						//MULTIPLE
					dataRespuestas.put("INF_OPC_RESP",infResOpc);
					dataRespuestas.put("RESP_ABIERTA_ASO",banRespAb);
					dataRespuestas.put("OPCION_OTROS",addOtro);
						//RADIO
					dataRespuestas.put("PREG_EXC",pregExcOpc);
					dataRespuestas.put("IC_PREGUNTA",pregExcOpc);
					dataRespuestas.put("CS_ACTIVO",CS_ACTIVO);
					dataRespuestas.put("IC_RESP",IC_RESP);
					
					dataRespuestas.put("icpregunta",icpregunta);
					dataRespuestas.put("ic_tipo_resp_inicial",ic_tipo_resp_inicial);
					dataRespuestas.put("num_opc_inicial",num_opc_inicial);
								
					lstRespuestas.add(dataRespuestas);
					
					
				}
				datafila.put("dato_resp",lstRespuestas);
			}else{
				
				datafila.put("dato_resp",lstRespuestas);
			}
			lstPreguntas.add(datafila);
		}
		
		
	}// fin IF

	mensaje = cediEjb.modificacionEncuestaGral(lstPreguntas,lstRespuestas,num_preg_inicial);
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mns", "Actualizado con éxito");
	jsonObj.put("accion", informacion);
	infoRegresar = jsonObj.toString();
	
}else if ( informacion.equals("GuardarPreguntas")   ){ 

	List lstPreguntas = new ArrayList();	
	List lstRespuestas = new ArrayList();
	String num_preg_inicial = "0";
	String numPregReal = "0";
	 numPregReal = (request.getParameter("txtNumPreg")!=null)?request.getParameter("txtNumPreg"):"0";
	num_preg_inicial = (request.getParameter("num_real_preg")!=null)?request.getParameter("num_real_preg"):"0";
	
	String fpPregunta = "", fpTipoResp = "",  fpNumOpc = "",  fpEstado = "", icpregunta = "", ic_tipo_resp_inicial="";
	String fila = "";
	
	 int num_preg_inic = Integer.parseInt(num_preg_inicial);
	 int num_total_preg  = Integer.parseInt(numPregReal);
	if(Integer.parseInt(numPregReal)>0){
		HashMap dataPreguntas = null;
		int numPregunta = 0;
		
		//SE OBTIENE LISTADO DE PREGUNTAS PARAMTERIZADAS
		for(int x=0; x<num_total_preg; x++){
			dataPreguntas = new HashMap();
			fila = "";
			fila = (request.getParameter("fila_"+x)!=null)?request.getParameter("fila_"+x):"";
			dataPreguntas.put("NUMPREG",String.valueOf(fila));
				
			fpPregunta = (request.getParameter("fpPregunta"+fila)!=null)?request.getParameter("fpPregunta"+fila):"";
			fpTipoResp = (request.getParameter("fpTipoResp"+fila)!=null)?request.getParameter("fpTipoResp"+fila):"";
			fpNumOpc = (request.getParameter("fpNumOpc"+fila)!=null)?request.getParameter("fpNumOpc"+fila):"";
			fpEstado = (request.getParameter("fpSelecccion"+fila)!=null)?request.getParameter("fpSelecccion"+fila):"N";	
			icpregunta = (request.getParameter("ic_pregunta"+fila)!=null)?request.getParameter("ic_pregunta"+fila):"";
			ic_tipo_resp_inicial = (request.getParameter("tipo_resp_inicial"+fila)!=null)?request.getParameter("tipo_resp_inicial"+fila):"";			
			if(fpEstado.equals("on")){
				fpEstado="S";
			}
			
			
			
			dataPreguntas.put("ORDENPREG",String.valueOf(++numPregunta));
			dataPreguntas.put("PREGUNTA",fpPregunta);
			dataPreguntas.put("TIPORESP",fpTipoResp);
			dataPreguntas.put("NUMOPC",fpNumOpc);
			dataPreguntas.put("X",x);
			dataPreguntas.put("ESTADO",fpEstado);	
			dataPreguntas.put("icpregunta",icpregunta);	
			dataPreguntas.put("ic_tipo_resp_inicial",ic_tipo_resp_inicial);	
			lstPreguntas.add(dataPreguntas);
		}
	}// fin IF
	mensaje = cediEjb.modificacionEncuestaGral(lstPreguntas,lstRespuestas,num_preg_inicial);
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mns", "Actualizado correctamente.");
	jsonObj.put("accion", informacion);
	infoRegresar = jsonObj.toString();
}else if (informacion.equals("ELIMINA_OPCION")){
	String IC_RESP = (request.getParameter("IC_RESP")!=null)?request.getParameter("IC_RESP"):"";
	String activo = (request.getParameter("opcInactivaG")!=null)?request.getParameter("opcInactivaG"):"";
	String numPre = (request.getParameter("numPre")!=null)?request.getParameter("numPre"):"";
	String numOpcion = (request.getParameter("numOpcion")!=null)?request.getParameter("numOpcion"):"";
	boolean res =  cediEjb.updateConfPreg(IC_RESP,activo); 
	
   jsonObj = new JSONObject(); 
   jsonObj.put("success", new Boolean(true));
    jsonObj.put("numPre", numPre);
	 jsonObj.put("numOpcion", numOpcion);
	 jsonObj.put("activo", activo);
   infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("ELIMINAR_PREGUNTA")){
	String numPregunta = (request.getParameter("numPregunta")!=null)?request.getParameter("numPregunta"):"";
	String tipo_dato = (request.getParameter("tipo_dato")!=null)?request.getParameter("tipo_dato"):"";
	
	boolean res =  cediEjb.eliminarPregunta(numPregunta,tipo_dato); 
	
   jsonObj = new JSONObject(); 
   jsonObj.put("success", new Boolean(true));
   
   infoRegresar = jsonObj.toString();
	
}
	

%>
<%= infoRegresar%>

