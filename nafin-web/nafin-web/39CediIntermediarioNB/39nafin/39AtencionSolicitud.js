Ext.onReady(function(){
	var clave_solicitud;
	var no_ifnb;
					
	
	Ext.apply(Ext.form.field.VTypes, {
		rangoFechas: function(val, field) {
			var date = field.parseDate(val);
	
			if (!date) {
				return false;
			}
			if (field.startDateField && (!this.rangoFechasMax || (date.getTime() != this.rangoFechasMax.getTime()))) {
				var start = field.up('form').down('#' + field.startDateField);
				start.setMaxValue(date);
				start.validate();
				this.rangoFechasMax = date;
			} else if (field.endDateField && (!this.rangoFechasMin || (date.getTime() != this.rangoFechasMin.getTime()))) {
				var end = field.up('form').down('#' + field.endDateField);
				end.setMinValue(date);
				end.validate();
				this.rangoFechasMin = date;
			}
			return true;
		},
		rangoFechasText: 'La fecha de inicio debe ser menor que la fecha final'
	});
	
	
	var ic_documentos = [];
	var activos = [];
	var nombreDoctos = [];	
	
	var cancelarGuardar =  function() { 
	 ic_documentos = [];
	 activos = [];
	 nombreDoctos = [];	 
	}
	
	
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};		
			
		  var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();					
		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
//////////////Check list de documentos a solicitar Financiero  /////////////////
 
 	var procesarguardarCheck = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {

			var  jsonData = Ext.JSON.decode(response.responseText);
			if(jsonData.accion=='Solicitar') {
				window.location = '/nafin/39CediIntermediarioNB/39VistaCorreo.jsp?no_solicitud='+clave_solicitud+'&no_ifnb='+no_ifnb+'&pantalla=PreAnalisis_Aceptada'; 
			
			}else  {
				Ext.Msg.alert(	'Aviso',	'&nbsp;&nbsp;&nbsp;&nbsp;La Informaci�n se guard� con �xito',	function(btn, text){
					Ext.ComponentQuery.query('#btnGuardarCheck')[0].enable();	
					Ext.ComponentQuery.query('#btnSolicitar')[0].disable();		
					consCheckData.load({
						params: {
							no_solicitud:jsonData.no_solicitud									
						}
					});
		
				});
			}
						
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	var procesarAceptar = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		clave_solicitud = registro.get('NO_SOLICITUD');
		no_ifnb = registro.get('IC_IFNB');
					
		consCheckData.load({
			params: {
				no_solicitud:clave_solicitud									
			}
		});
				
						
		new Ext.Window({
			modal: true,
			width: 400,
			resizable: false,
			closable:true,
			id: 'VentanaCheck',
			autoDestroy:false,
			closeAction: 'destroy',
			items: [
				gridCheck				
			],			
			bbar:{			 
				items: [
					'-',
					'->', 
					{
					xtype: 'button',
					text: 'Solicitar',
					id: 'btnSolicitar',
					iconCls: 'autorizar',
					handler: function(button,event) {
						guardarCheck('Solicitar');												
					}
				}
			]
			}
		}).show().setTitle('Check list de documentos a solicitar al IF');			
	}


	var guardarCheck = function( accion ) {
		var gridCheck =  Ext.ComponentQuery.query('#gridCheck')[0];		
		var store = gridCheck.getStore();		
		cancelarGuardar(); 
		
		if(accion=='Guardar'){
			Ext.ComponentQuery.query('#btnGuardarCheck')[0].disable();
			Ext.ComponentQuery.query('#btnSolicitar')[0].disable();	
		}else if(accion=='Solicitar'){
			Ext.ComponentQuery.query('#btnSolicitar')[0].disable();
		}
		
		
		store.each(function(record) {  			
			ic_documentos.push(record.data['IC_DOCUMENTO']);
						
			if( record.data['ACTIVO']=='' || record.data['ACTIVO']=='N' || record.data['ACTIVO']==false)  { 				
				activos.push("N");
			}else if( record.data['ACTIVO']=='S' || record.data['ACTIVO']==true)  { 
				activos.push("S");				
			}
			nombreDoctos.push(record.data['NOMBRE']);
						
		});
		
				
		Ext.Ajax.request({
			url: '39AtencionSolicitud.data.jsp',
			params: {
				informacion: 'guardarCheck',
				no_solicitud:clave_solicitud,
				ic_documentos:ic_documentos,
				activos:activos,
				nombreDoctos:nombreDoctos,
				accion:accion			
			},
			callback: procesarguardarCheck
		});
				
	}


	Ext.define('LisCheck', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'IC_DOCUMENTO'},
			{ name: 'IC_SOLICITUD'},
			{ name: 'NOMBRE'},
			{ name: 'FIJO'},
			{ name: 'ACTIVO'}
		 ]
	});

	var procesarConsultarCheck = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  forma =   Ext.ComponentQuery.query('#forma')[0];
			forma.el.unmask();
				
			
			if(store.getTotalCount() > 0) {
				Ext.ComponentQuery.query('#btnSolicitar')[0].enable();	
			}
		}
	};
	
	var consCheckData = Ext.create('Ext.data.Store', {
		model: 'LisCheck',
		proxy: {
			type: 'ajax',
			url: '39AtencionSolicitud.data.jsp',
			reader: 		 { type: 'json',  root: 'registros' 	},
			extraParams: { informacion: 'ConsCheckDoctos' 	},
			listeners:   { exception: NE.util.mostrarProxyAjaxError 	}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultarCheck
		}		
	});
	

	var  gridCheck = {
		xtype: 'grid',		
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridCheck',
		store: consCheckData,
		height: 300,
		width: 395,
		style: 'margin: 10px auto 0px auto;',	     
      frame: true,		
		clicksToEdit: 1,
		dockedItems: [ 
			{
			xtype: 'toolbar',
			id: 'Barra_boton',	
         items: [
				{
					iconCls: 'correcto',
					text: 'Agregar Registro',
					id: 'btnAgregarRegis',	
					iconCls: 'icoAgregar',
					scope: this,
					handler: function(){
						var gridCheck =  Ext.ComponentQuery.query('#gridCheck')[0];		
						var store = gridCheck.getStore();								
						totalReg= store.getCount();
						var solicitud;
						
						store.each(function(record){						
							if(store.indexOf(record) ==1) { no_solicitud = record.get('IC_SOLICITUD'); }
						});
												
						var reg = Ext.create('LisCheck', {								
							IC_DOCUMENTO:'',
							NOMBRE:'',
							IC_SOLICITUD:no_solicitud
						});
						consCheckData.insert(totalReg,reg);    						
						consCheckData.commitChanges();	 							 
							
					}
				}
			]
		}
		],			
		columns: [		
			{
				xtype: 'checkcolumn',
            header: 'Selec.',
            dataIndex: 'ACTIVO',
            width: 80,          
				renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
					var cssPrefix = Ext.baseCSSPrefix,
					cls = cssPrefix + 'grid-checkcolumn';					
					if(  record.data['ACTIVO'] =='S'  ||  record.data['ACTIVO']==true )  {												
						cls += ' ' + cssPrefix + 'grid-checkcolumn-checked';						
					}else if(   record.data['ACTIVO'] =='N' ||  record.data['ACTIVO']==false   )  {		
						cls = cssPrefix + 'grid-checkcolumn';
					}
					return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>';
				}
			},				
			{
				header: 'Documentos',
				dataIndex: 'NOMBRE',
				sortable: true,
				width: 300,
				resizable: true,
				align: 'left',
				renderer:function(v) {
					return v.replace(/\n/g,'<br>'
				)},
				editor: {
					xtype: 'textarea', 
					allowBlank: false,
					msgTarget: 'side',
					minValue: 0, 
					maxLength: 100,
					height:50,
					margins		: '0 20 0 0'
				} 				
			}			
		],
		listeners: {	
			cellclick: function(view, td, cellIndex, record, tr, rowIndex, e, eOpts) {	 					
				var fieldName = view.getGridColumns()[cellIndex].dataIndex;
				if(fieldName == 'NOMBRE' &&   record.data['FIJO']=='S' ){
					return false;
				}else  {
					return true;					
				}
				
			}
		},
		bbar:{
			items: [
				{
					xtype: 'button',
					text: 'Guardar Check List',
					id: 'btnGuardarCheck',
					iconCls: 'icoGuardar',					
					handler: function(button,event) {
						guardarCheck('Guardar');											
					}
				}
			]
		}		
	}	 

	 
	 
	var procesarRechaza = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');  
		var no_ifnb = registro.get('IC_IFNB');  
		
		window.location = '/nafin/39CediIntermediarioNB/39VistaCorreo.jsp?no_solicitud='+no_solicitud+'&no_ifnb='+no_ifnb+'&pantalla=PreAnalisis_Rechazada';
	}
	
	//////////////Descarga de los Achivos  /////////////////
	
	var descargaCedula = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		if(registro.get('LON_CEDULA_COMPLETA')!='0' ) {	
			Ext.Ajax.request({
				url: '39AtencionSolicitud.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{				
					informacion:'descargaCedula',
					no_solicitud:no_solicitud				
				}),
				callback: procesarDescargaArchivos
			});
		}
	}
	
	var descargaBalance = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicitud.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descargaBalance',
				no_solicitud:no_solicitud,
				tipoArchivo:'balance'
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	var descargaEdoResultados = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicitud.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descargaEdoResultados',
				no_solicitud:no_solicitud,
				tipoArchivo:'resultados'
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	var descargacopiaRFC = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicitud.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descargacopiaRFC',
				no_solicitud:no_solicitud,
				tipoArchivo:'rfc'
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	
	
	//////////////Consulta General  /////////////////
	var procesarConsultar = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  forma =   Ext.ComponentQuery.query('#forma')[0];
			forma.el.unmask();
			
			Ext.ComponentQuery.query('#gridConsulta')[0].show();	
			
			if(store.getTotalCount() > 0) {
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(true);
			}else  {				
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(false);
			}

		}
	};

	
	Ext.define('LisRegistros', {
		extend: 'Ext.data.Model',
		fields: [
		   { name: 'NO_SOLICITUD'},
			{ name: 'IC_IFNB'},
			{ name: 'RAZON_SOCIAL'},
			{ name: 'RFC_EMPRESARIAL'},			
			{ name: 'FECHA_HORA_REG'},
			{ name: 'ESTATUS'},
			{ name: 'SELECCIONAR'},
			{ name: 'IC_ESTATUS'},
			{ name: 'LON_BALANCE'},
			{ name: 'LON_RESULTADOS'},
			{ name: 'LON_RFC'},
			{ name: 'LON_CEDULA_COMPLETA'}			
		]
	});
	
	var consConsultaData = Ext.create('Ext.data.Store', {
		model: 'LisRegistros',
		proxy: {
			type: 'ajax',
			url: '39AtencionSolicitud.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consultar'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultar
			}
	});
	
	var  gridConsulta = Ext.create('Ext.grid.Panel',{
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridConsulta',
		store: consConsultaData,
		height: 600,		
		hidden:true,
		style: 'margin: 10px auto 0px auto;',		
		xtype: 'cell-editing',
      title: 'Consulta Solicitudes',
      frame: true,							
		columns: [			
			{
				header: 'Raz�n Social',
				dataIndex: 'RAZON_SOCIAL',
				sortable: true,
				width: 190,
				resizable: true
         },
			{
				header: 'RFC Empresarial',
				dataIndex: 'RFC_EMPRESARIAL',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         },
			{
				header: 'Folio Solicitud',
				dataIndex: 'NO_SOLICITUD',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         },
			{
				header: 'Fecha y hora de registro',
				dataIndex: 'FECHA_HORA_REG',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         },
			{
				xtype		: 'actioncolumn',
				header: 'C�dula de Aceptaci�n',
				tooltip: 'C�dula de Aceptaci�n',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_CEDULA_COMPLETA']!='0' ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargaCedula
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Balance',
				tooltip: 'Balance',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_BALANCE']!='0' ) {	
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargaBalance
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Edo. Resultados',
				tooltip: 'Edo. Resultados',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_RESULTADOS']!='0' ) {	
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargaEdoResultados
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Copia R.F.C.',
				tooltip: 'Copia R.F.C.',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_RFC']!='0' ) {	
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargacopiaRFC
					}
				]
			},			
			{
				header: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         },
			{
				xtype		: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Aceptar';
							return 'icoAceptar';
						}
						,handler: procesarAceptar
					},
						{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Rechazar';
							return 'borrar';
						}
						,handler: procesarRechaza
					}
				]
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: [
			'->','-'			
		] 
	});
	
	
	//*****************Formas  **************
	
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
	
	
	var  catIntermediarioData = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '39AtencionSolicitud.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catIntermediarioData'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});

	
	

	
	
	var  elementosForma = [
		{
			xtype: 'combo',
			fieldLabel: 'Raz�n Social',
			itemId: 'ic_intermediario1',
			name: 'ic_intermediario',
			hiddenName: 'ic_intermediario',
			forceSelection: true,
			hidden: false, 
			width: 450,
			store: catIntermediarioData,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave'
		},	
		{
			xtype: 'numberfield',
			fieldLabel: 'Folio Solicitud',
			itemId: 'txtFolioSolicitud1',
			name: 'txtFolioSolicitud',
			maxLength: 12,
			width: 300				
		}	,
		{
			xtype: 'container',			
			layout: 'hbox',
			width: 500,
			items: [
				{
					xtype: 'datefield',					
					fieldLabel: ' Fecha Solicitud',
					name: 'txtFechaSoliIni',
					itemId: 'txtFechaSoliIni',
					vtype: 'rangoFechas',
					endDateField: 'txtFechaSoliFinal',
					margins: '0 20 0 0',
					minValue: '01/01/1901',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side'
				},
				{
					xtype: 'displayfield',
					value: ' &nbsp;  al &nbsp;',
					width: 30
				},
				{
					xtype: 'datefield',					
					labelAlign: 'right',					
					name: 'txtFechaSoliFinal',
					itemId: 'txtFechaSoliFinal',
					vtype: 'rangoFechas',
					startDateField: 'txtFechaSoliIni',
					margins: '0 20 0 0',
					minValue: '01/01/1901',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side'	
				}
			]
		}		
	];


	var fp = Ext.create('Ext.form.Panel',	{		
      itemId: 'forma',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: 'Pre-an�lisis ',
      width: 550,
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 120
		},		
		items:elementosForma, 
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBucar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(){
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
					
					var txtFechaSoliIni =  forma.query('#txtFechaSoliIni')[0]; 
					var txtFechaSoliFinal =  forma.query('#txtFechaSoliFinal')[0]; 
					
					if(!Ext.isEmpty(txtFechaSoliIni.getValue()) || !Ext.isEmpty(txtFechaSoliFinal.getValue()) ){
						if(Ext.isEmpty(txtFechaSoliIni.getValue())){
							txtFechaSoliIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtFechaSoliIni.focus();
							return;
						}
						if(Ext.isEmpty(txtFechaSoliFinal.getValue())){
							txtFechaSoliFinal.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtFechaSoliFinal.focus();
							return;
						}
					}
					
					forma.el.mask('Consultando...', 'x-mask-loading');	
					consConsultaData.load({
						params: {							
							ic_intermediario:forma.query('#ic_intermediario1')[0].getValue(),							
							no_solicitud:forma.query('#txtFolioSolicitud1')[0].getValue(),
							txtFechaSoliIni:Ext.util.Format.date(forma.query('#txtFechaSoliIni')[0].getValue(),'d/m/Y'),
							txtFechaSoliFinal:Ext.util.Format.date(forma.query('#txtFechaSoliFinal')[0].getValue(),'d/m/Y')	  
									
						}
					});
						
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',				
				handler: function(){
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
					
					forma.query('#txtFechaSoliIni')[0].setValue('');
					forma.query('#txtFechaSoliFinal')[0].setValue('');
					forma.query('#txtFolioSolicitud1')[0].setValue('');
					forma.query('#ic_intermediario1')[0].setValue('');					
					Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
					
				}
			}
		]
	});
	
	
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 920,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [							
			fp,			
			NE.util.getEspaciador(20),
			gridConsulta		
			
		]
	});
	
	catIntermediarioData.load();	


					
});