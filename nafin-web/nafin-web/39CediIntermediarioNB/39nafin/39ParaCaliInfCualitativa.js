Ext.onReady(function(){
	var fpModPreguntas = null;
	var fpConfPreguntas = null;
	var num_preg_ini = null;
	var num_preg_final = null;
	var fp = null;
	
	
	 
	 var processSuccessFailureGuardaModifEnc = function(options, success, response) {
		
		Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
		Ext.ComponentQuery.query('#btnGuardar')[0].enable();
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {

			var  jsonData = Ext.JSON.decode(response.responseText);				
				Ext.MessageBox.alert('Aviso','<CENTER>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+jsonData.mns+'</CENTER>',	function() {														
				//if(jsonData.accion=='GuardarPreguntas') {
					window.location = '39ParaCaliInfCualitativa.jsp';										
				//}
			});
	
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	 var processSuccessFailureGuardaModifTermi = function(options, success, response) {
		
		Ext.ComponentQuery.query('#btnGuardar')[0].setIconCls('icoGuardar');
		Ext.ComponentQuery.query('#btnGuardar')[0].enable();
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {

			var  jsonData = Ext.JSON.decode(response.responseText);				
				Ext.MessageBox.alert('Aviso','<CENTER>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+jsonData.mns+'</CENTER>',	function() {														
				window.location = '39ParaCaliInfCualitativa.jsp';		
			});
	
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	var procesarAgregarPregunta = function (opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var InfoGeneral = infoR.InfoGeneral;//infoParamPreguntas
			var infoParamPreguntas = InfoGeneral.INFOPARAMETRIZACION;//Se obtiene la información de la parametrización de las preguntas
			var numPreg = InfoGeneral.TOTAL_PREGUNTAS;
			var ID_PREG_MAX = InfoGeneral.ID_PREG_MAX;
			var INFO_RESPUESTAS = InfoGeneral.INFO_RESPUESTAS;
			var numeroDPreguntas = Ext.ComponentQuery.query('#numPreg')[0];
			numeroDPreguntas.valueIni = numPreg;
			var num_pre_final = Ext.ComponentQuery.query('#numPreg')[0].getValue();
			fpModPreguntas = new NE.Informacion.FormPreguntas({numeroPreguntas:numPreg, modif: 's',objDataPreg:infoParamPreguntas,agregarPreg:'N',obj_numPreg:fp,numeroPregMax:num_pre_final,ID_PREG_MAX:ID_PREG_MAX});
			
			fpModPreguntas.setHandlerBtnSiguiente(function(){
				
				if(fpModPreguntas.getForm().isValid()){
					var numPregReal = Ext.ComponentQuery.query('#numPreg')[0].getValue();
					fpConfPreguntas = new NE.Informacion.FormConfResp({formaPreguntas:fpModPreguntas,numPregReal:numPregReal, objDataResp:INFO_RESPUESTAS,modif:'s',obj_numPreg:fp});
					fpModPreguntas.hide();
					fp.hide();
					main.add(fpConfPreguntas);
					main.doLayout();
						fpConfPreguntas.setHandlerBtnVistaPrevia(function(){
							 fpVistaPrevia = new NE.Informacion.FormVistaPrev({formaPreguntas:fpModPreguntas,formaRespuestas:fpConfPreguntas, numOjetos:1,   modif:'s'});
					
									new Ext.Window({
										modal: true,
										resizable: false,
										layout: 'form',								
										width: 820,
										style: 'margin:0 auto;',
										itemId: 'winVistaPrev',
										closable: true,
										closeAction: 'close',
										items: [ fpVistaPrevia],
										title: 'Vista Previa'
									}).show();
								
						});
					fpConfPreguntas.setHandlerBtnTermina(function(){
							Ext.Msg.confirm('Confirmación','<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>¿Confirma actualizar la configuración de respuestas?</center></td><td></td></table> </div>',
									function(res){
										if(res=='no' || res == 'NO'){
											return;
										}else{										
											Ext.Ajax.request({
												url: '39ParaCaliInfCualitativa.data.jsp',
												params:Ext.apply(fpModPreguntas.getForm().getValues(), Ext.apply(fpConfPreguntas.getForm().getValues()),{
													informacion: 'ModificarEncuesta',
													 txtNumPreg:Ext.ComponentQuery.query('#numPreg')[0].getValue(),
													 num_real_preg:Ext.ComponentQuery.query('#num_real_preg')[0].getValue(),
													 pregunta :'N'
												}),
												callback: processSuccessFailureGuardaModifTermi
											});
										}
								});
					}
					);
						
					fpConfPreguntas.setHandlerBtnCancelar(function(){				
						window.location = '39ParaCaliInfCualitativa.jsp';	
						
					});	
				}
			});	
			
			fpModPreguntas.setHandlerBtnGuardar(function(){
				var btnGuardar = Ext.ComponentQuery.query('#btnGuardar')[0];
				var numPregReal = Ext.ComponentQuery.query('#numPreg')[0].getValue();
				btnGuardar.disable();
				btnGuardar.setIconCls('x-mask-msg-text');
				Ext.Ajax.request({
					url: '39ParaCaliInfCualitativa.data.jsp',
					params:Ext.apply(fpModPreguntas.getForm().getValues(),{
						informacion: 'ModificarEncuesta',
						txtNumPreg:Ext.ComponentQuery.query('#numPreg')[0].getValue(),
						num_real_preg:Ext.ComponentQuery.query('#num_real_preg')[0].getValue(),
						pregunta :'S'
					}),
					callback: processSuccessFailureGuardaModifEnc
				});
			});
			
			main.add(fpModPreguntas);
			main.doLayout();
			fp.unmask();
			Ext.ComponentQuery.query("#btnGuardar")[0].setDisabled(true); 
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarCreaPanelPreguntas = function (opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var InfoGeneral = infoR.InfoGeneral;//infoParamPreguntas
			var infoParamPreguntas = InfoGeneral.INFOPARAMETRIZACION;//Se obtiene la información de la parametrización de las preguntas
			var numPreg = InfoGeneral.TOTAL_PREGUNTAS;
			var ID_PREG_MAX = InfoGeneral.ID_PREG_MAX;
			var INFO_RESPUESTAS = InfoGeneral.INFO_RESPUESTAS;
			Ext.ComponentQuery.query('#num_real_preg')[0].setValue(numPreg);
			Ext.ComponentQuery.query('#num_preg_crea')[0].setValue(numPreg);
			Ext.ComponentQuery.query('#id_preg_max')[0].setValue(ID_PREG_MAX);
			var numeroDPreguntas = Ext.ComponentQuery.query('#numPreg')[0];
			Ext.ComponentQuery.query('#numPreg')[0].setValue(numPreg);
			numeroDPreguntas.valueIni = numPreg;
			fpModPreguntas = new NE.Informacion.FormPreguntas({numeroPreguntas:numPreg, modif: 's',objDataPreg:infoParamPreguntas,agregarPreg:'N',obj_numPreg:fp});
			
			fpModPreguntas.setHandlerBtnSiguiente(function(){
				
				if(fpModPreguntas.getForm().isValid()){
					var numPregReal = Ext.ComponentQuery.query('#numPreg')[0].getValue();
					fpConfPreguntas = new NE.Informacion.FormConfResp({formaPreguntas:fpModPreguntas,numPregReal:numPregReal, objDataResp:INFO_RESPUESTAS,modif:'s',obj_numPreg:fp});
					fpModPreguntas.hide();
					fp.hide();
					main.add(fpConfPreguntas);
					main.doLayout();
						fpConfPreguntas.setHandlerBtnVistaPrevia(function(){
							 fpVistaPrevia = new NE.Informacion.FormVistaPrev({formaPreguntas:fpModPreguntas,formaRespuestas:fpConfPreguntas, numOjetos:1,   modif:'s'});
					
									new Ext.Window({
										modal: true,
										resizable: false,
										layout: 'form',								
										width: 820,
										style: 'margin:0 auto;',
										itemId: 'winVistaPrev',
										closable: true,
										closeAction: 'close',
										items: [ fpVistaPrevia],
										title: 'Vista Previa'
									}).show();
								
						});
					fpConfPreguntas.setHandlerBtnTermina(function(){
							Ext.Msg.confirm('Confirmación','<div > <table width="350" border="0" align="center" cellpadding="0" cellspacing="0"><tr> <td ><center>¿Confirma actualizar la configuración de respuestas?</center></td><td></td></table> </div>',
									function(res){
										if(res=='no' || res == 'NO'){
											return;
										}else{										
											Ext.Ajax.request({
												url: '39ParaCaliInfCualitativa.data.jsp',
												params:Ext.apply(fpModPreguntas.getForm().getValues(), Ext.apply(fpConfPreguntas.getForm().getValues()),{
													informacion: 'ModificarEncuesta',
													 txtNumPreg:Ext.ComponentQuery.query('#numPreg')[0].getValue(),
													 num_real_preg:Ext.ComponentQuery.query('#num_real_preg')[0].getValue(),
													 pregunta :'N'
												}),
												callback: processSuccessFailureGuardaModifTermi
											});
										}
								});
					}
					);
						
					fpConfPreguntas.setHandlerBtnCancelar(function(){				
						window.location = '39ParaCaliInfCualitativa.jsp';	
						
					});	
				}
			});	
			
			fpModPreguntas.setHandlerBtnGuardar(function(){
				var btnGuardar = Ext.ComponentQuery.query('#btnGuardar')[0];
				var numPregReal = Ext.ComponentQuery.query('#numPreg')[0].getValue();
				btnGuardar.disable();
				btnGuardar.setIconCls('x-mask-msg-text');
				Ext.Ajax.request({
					url: '39ParaCaliInfCualitativa.data.jsp',
					params:Ext.apply(fpModPreguntas.getForm().getValues(),{
						informacion: 'ModificarEncuesta',
						txtNumPreg:Ext.ComponentQuery.query('#numPreg')[0].getValue(),
						num_real_preg:Ext.ComponentQuery.query('#num_real_preg')[0].getValue(),
						pregunta :'S'
					}),
					callback: processSuccessFailureGuardaModifEnc
				});
			});
			
			main.add(fpModPreguntas);
			main.doLayout();
			fp.unmask();
			Ext.ComponentQuery.query("#btnGuardar")[0].setDisabled(true); 
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var elementosForma=[
	{
        xtype: 'fieldcontainer',
        fieldLabel: 'Número de Preguntas',
        layout: 'hbox',
		combineErrors: false,
		msgTarget: 'side',
        items: [
				{
					xtype:		'textfield',
					name:			'txtNumPreg',
					itemId:			'numPreg',
					maskRe:		/[0-9]/,
					allowBlank:	true,
					valueIni:'',
					maxLength:3,
					msgTarget:		'side',
					maxLengthText: 'El tamaño permitido para este campo es de 3 posiciones.'
				},
				{
					xtype:'displayfield',
					width: 25
				},
				{
					xtype: 'textfield',
					itemId: 'num_real_preg',
					name: 'num_real_preg',
					hidden:true
				},
				{
					xtype: 'textfield',
					itemId: 'num_preg_crea',
					name: 'num_preg_crea',
					hidden:true
				},
				{
					xtype: 'textfield',
					itemId: 'id_preg_max',
					name: 'id_preg_max',
					hidden:true
				},
				{
					xtype:	'button',
					itemId:		'btnAceptar',
					text:		' Aceptar ',
					width: 100,
					frame: false,
					formBind: true,
					handler: function(boton, evento) {
						var numPreguntas = Ext.ComponentQuery.query('#numPreg')[0].getValue();
						var valorIni = Ext.ComponentQuery.query('#num_real_preg')[0].getValue();
						var valorIni_crea = Ext.ComponentQuery.query('#num_preg_crea')[0].getValue();
						
						var id_preg_max =Ext.ComponentQuery.query('#id_preg_max')[0].getValue();
						var myMask = new Ext.LoadMask(main, {msg:"Please wait..."});
						if(Number(valorIni_crea) > Number(numPreguntas)){
							myMask.hide();
							Ext.ComponentQuery.query('#btnSiguiente')[0].disable();
							Ext.Msg.alert('Aviso', 'El número de preguntas no puede ser menor al actual, favor de verificar');
							return;
						}else{
							fpModPreguntas.hide();
							fpModPreguntas.destroy();
							main.doLayout();
							fp.getEl().mask("Inicializando...", 'x-mask-loading');
							Ext.Ajax.request({
								url: '39ParaCaliInfCualitativa.data.jsp',
									params: Ext.apply({							
										informacion: 'Inicializacion'
									}),	
									callback: procesarAgregarPregunta
							});
						
						}
					}
				}
			]
		}
	];
	
	fp = Ext.create('Ext.form.Panel',	{
		layout: 'form',
		itemId: 'forma',
		frame: true,
		border: true,
		bodyPadding: '12 6 12 6',
		title: '<center>Modificación de Preguntas</center>',
		width: 880,     
		style: 'margin:0 auto;',
		hidden: false,
		monitorValid: true,
		fieldDefaults: { msgTarget: 'side', labelWidth: 150 },
		layout: 'anchor',
		items: elementosForma
	});
	

	var main = Ext.create('Ext.container.Container', {
		itemId: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',		
		items: [	
			NE.util.getEspaciador(20),
			fp
		]
	});
	fp.getEl().mask("Inicializando...", 'x-mask-loading');
		Ext.Ajax.request({
			url: '39ParaCaliInfCualitativa.data.jsp',
			params: Ext.apply({							
				informacion: 'Inicializacion'
			}),	
			callback: procesarCreaPanelPreguntas
		});
	
});