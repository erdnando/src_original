Ext.onReady(function(){
	
	var no_solicitud =  Ext.getDom('no_solicitud').value;	
	var no_ifnb =  Ext.getDom('no_ifnb').value;	
   var pantalla =  Ext.getDom('pantalla').value;
	
	var rfc_empresarial =  Ext.getDom('rfc_empresarial').value;
	var razon_social =  Ext.getDom('razon_social').value;

	var ic_documentos = [];
	var activos = [];
	var nombreDoctos = [];
	
	
	var cancelarGuardar =  function() { 
	 ic_documentos = [];
	 activos = [];
	 nombreDoctos = [];	 
	}
	
	
	
	var procesarConfirmarEnviar = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {

			var  jsonData = Ext.JSON.decode(response.responseText);

			var button = 	Ext.ComponentQuery.query('#btnConfirmarEnviar')[0];
			button.enable();
			button.setIconCls('icoBotonEnviarCorreo');
		
			if( jsonData.pantalla=='Seguimiento_Rechazada'  )  {	
				
				Ext.Msg.alert(	'Aviso',	'<center>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; La Solicitud fue Rechazada</center>',	function(btn, text){
					window.location = '/nafin/39CediIntermediarioNB/39pki/39nafin/39AtencionSolicSeguimiento.jsp';
				});
				
			}else  {
				
				Ext.Msg.alert(	'Aviso',	'<center> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; Correo enviado con éxito</center>',	function(btn, text){
					
					if(jsonData.pantalla=='PreAnalisis_Aceptada' || jsonData.pantalla=='PreAnalisis_Rechazada' )  {
								window.location = '/nafin/39CediIntermediarioNB/39nafin/39AtencionSolicitud.jsp';	
					}else  if(jsonData.pantalla=='Seguimiento_Aceptada' || jsonData.pantalla=='Seguimiento_Rechazada' )  {
						window.location = '/nafin/39CediIntermediarioNB/39pki/39nafin/39AtencionSolicSeguimiento.jsp';
					}
				
				});
			
				
			}
			
			
			
					
		
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
		
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
		  var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();					
		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		var procesarVistaCorreo = function(options, success, response) {
	
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var  jsonData = Ext.JSON.decode(response.responseText);
			
			var panelVistaCorreo = Ext.ComponentQuery.query('#panelVistaCorreo')[0];
			
			panelVistaCorreo.show();
			panelVistaCorreo.el.unmask();	
			
			panelVistaCorreo.query('#no_solicitud')[0].setValue(jsonData.no_solicitud);
			panelVistaCorreo.query('#pdfViabilidad')[0].setValue(jsonData.pdfViabilidad);
			
			panelVistaCorreo.query('#lbTitulo')[0].update(jsonData.lbTitulo);			
			
						
			panelVistaCorreo.query('#lbfechaActual')[0].update(jsonData.lbfechaActual);
			panelVistaCorreo.query('#lbNombreIFNB')[0].update(jsonData.lbNombreIFNB);
			panelVistaCorreo.query('#lbMensaje1')[0].update(jsonData.lbMensaje1);
						 
			panelVistaCorreo.query('#lbMensaje2')[0].update(jsonData.lbMensaje2); 
			panelVistaCorreo.query('#lbMensaje3')[0].update(jsonData.lbMensaje3); 
			
			panelVistaCorreo.query('#lbMensaje5')[0].update(jsonData.lbMensaje5);
			
			if(jsonData.pantalla=='PreAnalisis_Aceptada')  {				
				panelVistaCorreo.query('#lbListaDoctos')[0].update(jsonData.lbListaDoctos);
				panelVistaCorreo.query('#lbListaDoctos')[0].show();	
				
			}
			if(jsonData.pantalla=='Seguimiento_Aceptada')  {		
				panelVistaCorreo.query('#btnDescargDictamen')[0].show();	
				panelVistaCorreo.query('#lbMensaje4')[0].update(jsonData.lbMensaje4);
			}
			if( jsonData.pantalla=='Seguimiento_Rechazada'  )  {		
				panelVistaCorreo.query('#btnDescargDictamen_n')[0].show();	
				panelVistaCorreo.query('#lbMensaje4')[0].update(jsonData.lbMensaje4);
			}
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	
	var elementosPanelCorreo = [
		{
			xtype:  'label',         
         width:	'100%',
			itemId: 'lbTitulo',		
			fieldLabel: '',
			text: '',
         style:  'font-weight:bold;text-align:center;margin:15px;color:black;'		
		},		
		{
			xtype:  'label',
         itemId: 'lbfechaActual',
         width:	'100%',       
			style:  'text-align:right;margin:15px;color:black;'
		},
		{
			xtype:  'label',
         itemId: 'lbNombreIFNB',
         fieldLabel: '',
		   text: '',
         style:  'font-weight:bold;text-align:left;margin:15px;color:black;'			
		},		
		{
			xtype:  'label',         
         width:	'100%',
			itemId: 'lbMensaje1',
         style:  'text-align:JUSTIFIED;margin:15px;color:black;',
			fieldLabel: '',
		   text: ''
		},		
		{
			xtype:  'label',
         itemId: 'lbListaDoctos',
         width:	'100%',
         style:  'text-align:left;margin:15px;color:black;',
			html: ''
		},
		{
			xtype: 'button',
			text: 'Dictamen de viabilidad de incorporación',			
			iconCls: 'icoPdf',
			id: 'btnDescargDictamen',
			hidden: true, 
			handler: function(boton, evento) {
			
				Ext.Ajax.request({
					url: '39VistaCorreo.data.jsp',
						params: {			
						informacion:'ViabilidadPDF',
						no_solicitud:no_solicitud,
						rfc_empresarial:rfc_empresarial,
						razon_social:razon_social
					},
					callback: procesarDescargaArchivos
				});	
							
			}
		},		
		{
			xtype:  'label',         
         width:	'100%',
			itemId: 'lbMensaje2',
         style:  'text-align:JUSTIFIED;margin:15px;color:black;',
			fieldLabel: '',
		   text: ''
		},
		{
			xtype: 'textarea',
			height: 70,
			inputAttrTpl: 'wrap="off" style="overflow:scroll"', // Para que se muestre el scroll horizontal.
			itemId: 'txtObservacionesCEDI',
			name: 'txtObservacionesCEDI',			
			width: 600				
		 }	,	
		 {
			xtype:  'label',         
         width:	'100%',
			itemId: 'lbMensaje4',
         style:  'text-align:JUSTIFIED;margin:15px;color:black;',
			fieldLabel: '',
		   text: ''
		},
		{
			xtype: 'button',
			text: 'Dictamen de no viabilidad de incorporación',			
			iconCls: 'icoPdf',
			id: 'btnDescargDictamen_n',
			hidden: true, 
			handler: function(boton, evento) {
			
				Ext.Ajax.request({
					url: '39VistaCorreo.data.jsp',
						params: {			
						informacion:'ViabilidadPDF',
						no_solicitud:no_solicitud,
						rfc_empresarial:rfc_empresarial,
						razon_social:razon_social
					},
					callback: procesarDescargaArchivos
				});	
							
			}
		},
		{
			xtype:  'label',         
         width:	'100%',
			itemId: 'lbMensaje3',
         style:  'text-align:JUSTIFIED;margin:15px;color:black;',
			fieldLabel: '',
		   text: ''
		},
		{
			xtype:  'label',         
         width:	'100%',
			itemId: 'lbMensaje5',
         style:  'text-align:JUSTIFIED;margin:15px;color:black;',
			fieldLabel: '',
		   text: ''
		},
		{ 	xtype: 'textfield',   id: 'no_solicitud', hidden:true, 	value: '' },
		{ 	xtype: 'textfield',   id: 'pdfViabilidad', width: 600 ,  hidden:true, 	value: '' }	
		
	];
	
	
	var  panelVistaCorreo = Ext.create( 'Ext.form.Panel', {      
      itemId: 'panelVistaCorreo',
      frame: true,
		border: false,
      bodyPadding: '12 6 12 6',
      width: 690,
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 100
		},
		//layout: 'form',
		title:		'Vista Previa del Correo',
		items:		elementosPanelCorreo,
		buttons: [
			{
				text: 'Confirmar y Enviar',
				id: 'btnConfirmarEnviar',
				iconCls: 'icoBotonEnviarCorreo',
				formBind: true,
				handler: function(){
				var panelVistaCorreo = Ext.ComponentQuery.query('#panelVistaCorreo')[0];
				var  procede = true;
				
					if (pantalla=='PreAnalisis_Rechazada'  ||  pantalla=='Seguimiento_Rechazada'    ){					
						if(panelVistaCorreo.query('#txtObservacionesCEDI')[0].getValue()=='')  {
							Ext.Msg.alert(	'Aviso',	'Las causas de rechazo son obligatorias');
							procede = false;
						}
					}
					if(procede==true){
						
						var button = Ext.ComponentQuery.query('#btnConfirmarEnviar')[0];
						button.disable();
						button.setIconCls('x-mask-msg-text');
						
						Ext.Ajax.request({
							url: '39VistaCorreo.data.jsp',
							params: Ext.apply({				
								informacion:'confirmarEnviar',
								no_solicitud:panelVistaCorreo.query('#no_solicitud')[0].getValue()	 ,
								txtObservacionesCEDI:panelVistaCorreo.query('#txtObservacionesCEDI')[0].getValue(),						
								pantalla:pantalla,
								no_ifnb:no_ifnb,
								pdfViabilidad:panelVistaCorreo.query('#pdfViabilidad')[0].getValue()	
								
							}),
							callback: procesarConfirmarEnviar
						});	
					}
				}
			},
			{
				text: 'Regresar',
				id: 'btnRegresar',
				iconCls: 'icoLimpiar',
				formBind: true,
				handler: function(){
					if(pantalla=='PreAnalisis_Aceptada' || pantalla=='PreAnalisis_Rechazada' )  {
						window.location = '/nafin/39CediIntermediarioNB/39nafin/39AtencionSolicitud.jsp';	
					}else  if(pantalla=='Seguimiento_Aceptada' || pantalla=='Seguimiento_Rechazada' )  {
						window.location = '/nafin/39CediIntermediarioNB/39pki/39nafin/39AtencionSolicSeguimiento.jsp';
					}
				}
			},
			{
				text: 'Generar PDF',
				id: 'btnGenerar PDF',
				iconCls: 'icoPdf',
				formBind: true,
				handler: function(){
					var panelVistaCorreo = Ext.ComponentQuery.query('#panelVistaCorreo')[0];
					
					Ext.Ajax.request({
						url: '39VistaCorreo.data.jsp',
						params: Ext.apply({				
							informacion:'generarPDF',
							no_solicitud:panelVistaCorreo.query('#no_solicitud')[0].getValue()	 ,
							txtObservacionesCEDI:panelVistaCorreo.query('#txtObservacionesCEDI')[0].getValue(),						
							pantalla:pantalla
							
						}),
						callback: procesarDescargaArchivos
					});	
						
				
				}
			}
		]
	});
	
 
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 920,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [										
			panelVistaCorreo			
		]
	});
	
	
	panelVistaCorreo.el.mask('Consultando Información ...', 'x-mask-loading');	
	Ext.Ajax.request({
		url: '39VistaCorreo.data.jsp',
		params: Ext.apply({				
			informacion:'VistaPreviaCorreo',
			no_solicitud:no_solicitud,				
			pantalla:pantalla,			
			rfc_empresarial:rfc_empresarial,
			razon_social:razon_social
		}),
		callback: procesarVistaCorreo
	});		
						


					
});