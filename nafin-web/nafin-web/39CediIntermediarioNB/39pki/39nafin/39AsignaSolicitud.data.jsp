<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		com.netro.cedi.*,		
		com.netro.model.catalogos.*,		
		com.netro.pdf.*,
		netropology.utilerias.*,
		com.netro.seguridad.*,
		com.netro.exception.*, 
		netropology.utilerias.usuarios.*,
		org.apache.commons.logging.Log, 
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../../39secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_intermediario = (request.getParameter("ic_intermediario")!=null)?request.getParameter("ic_intermediario"):"";
String txtRFC = (request.getParameter("txtRFC")!=null)?request.getParameter("txtRFC"):"";
String txtFolioSolicitud = (request.getParameter("txtFolioSolicitud")!=null)?request.getParameter("txtFolioSolicitud"):"";
String txtFechaSoliIni = (request.getParameter("txtFechaSoliIni")!=null)?request.getParameter("txtFechaSoliIni"):"";
String txtFechaSoliFinal = (request.getParameter("txtFechaSoliFinal")!=null)?request.getParameter("txtFechaSoliFinal"):"";
String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
String ic_ejecutivoCEDI = (request.getParameter("ic_ejecutivoCEDI")!=null)?request.getParameter("ic_ejecutivoCEDI"):"";
String solicAsignadas = (request.getParameter("solicAsignadas")!=null)?request.getParameter("solicAsignadas"):""; 
String no_estatus = (request.getParameter("no_estatus")!=null)?request.getParameter("no_estatus"):"";  

String infoRegresar	=	"", consulta ="";
JSONObject jsonObj = new JSONObject();	
UtilUsr 		utilUsr 			= new UtilUsr();

Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 

ConsSolicitudes paginador = new ConsSolicitudes();		

paginador.setIc_intermediario(ic_intermediario);
paginador.setTxtRFC(txtRFC);
paginador.setTxtFolioSolicitud(txtFolioSolicitud);
paginador.setTxtFechaSoliIni(txtFechaSoliIni);
paginador.setTxtFechaSoliFinal(txtFechaSoliFinal);
paginador.setSolicAsignadas(solicAsignadas); 
 
CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);	


if (informacion.equals("catIntermediarioData")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_IFNB_ASPIRANTE");
	catalogo.setCampoDescripcion("CG_RAZON_SOCIAL");
	catalogo.setTabla("CEDI_IFNB_ASPIRANTE");		
	catalogo.setOrden("IC_IFNB_ASPIRANTE");	
	infoRegresar = catalogo.getJSONElementos();	
	
		
}else  if (informacion.equals("catEjecutivoCEDIData")){

	JSONArray jsObjArray = new JSONArray();
   List lstCatalogoEjecCEDI = new ArrayList();
	List usuarios = new ArrayList();

    usuarios = utilUsr.getUsuariosxAfiliado("EJE CEDI", "N");  
	if(usuarios.size()>0)  {
		HashMap hmData = new HashMap();
		for(int i=0;i<usuarios.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuarios.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
			hmData.put("clave", loginUsuarioOPOP);
			hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
			
			lstCatalogoEjecCEDI.add(hmData);
		}				
	}
	
	usuarios = new ArrayList();
	usuarios = utilUsr.getUsuariosxAfiliado("GESTOR CEDI", "N"); 	
	
	if(usuarios.size()>0)  {
		HashMap hmData = new HashMap();
		for(int i=0;i<usuarios.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuarios.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
			hmData.put("clave", loginUsuarioOPOP);
			hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
			
			lstCatalogoEjecCEDI.add(hmData);
		}		
	}
		
	 jsObjArray = JSONArray.fromObject(lstCatalogoEjecCEDI);
    infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else  if (informacion.equals("Consultar")  ||  informacion.equals("ConsultarEje")  ){
	
	Registros reg	=	queryHelper.doSearch();
	
	if (  informacion.equals("ConsultarEje")  ){
		
		while(reg.next()){
			
			String  txtLogin  = (reg.getString("LOGIN_EJECUTIVO") == null) ? "" : reg.getString("LOGIN_EJECUTIVO");
			
			Usuario usuarioEpo = utilUsr.getUsuario(txtLogin);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
			reg.setObject("EJECUTIVO_CEDI",nombre +"- "+txtLogin);		
		 }
	}
		
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString(); 
	
}else  if (informacion.equals("descargaCedula")){ 
	
	List datoGrales = new ArrayList();
	datoGrales.add((String)session.getAttribute("strPais")); //0
	datoGrales.add("-");  //1
	datoGrales.add(strNombre); //2
	datoGrales.add(strNombreUsuario); //3
	datoGrales.add(strLogo); //4
	datoGrales.add(strDirectorioTemp); //5
	datoGrales.add(strDirectorioPublicacion); //6
	datoGrales.add(no_solicitud);//7
	datoGrales.add("-");  //8
	datoGrales.add("-");  //9	
	datoGrales.add(no_estatus);//10
	datoGrales.add("ASIGNA_SOLICITUD"); //11 
		
	String nombreArchivo  = cediBean.generarArchivoCedula(datoGrales);	
	
	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 
	
	
}else  if (informacion.equals("descargaBalance")){

	String nombreArchivo  =  cediBean.descargaArchivosSolic(no_solicitud, strDirectorioTemp, tipoArchivo );
   jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 
	

}else  if (informacion.equals("descargaEdoResultados")){

	String nombreArchivo  =  cediBean.descargaArchivosSolic(no_solicitud, strDirectorioTemp, tipoArchivo );
   jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 
	
	
}else  if (informacion.equals("descargacopiaRFC")){

	String nombreArchivo  =  cediBean.descargaArchivosSolic(no_solicitud, strDirectorioTemp, tipoArchivo );
   jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 


}else  if (informacion.equals("AsignaEjecutivoCedu")){

	String ic_solicitud[] = request.getParameterValues("ic_solicitud");	
	String ic_estatus[] = request.getParameterValues("ic_estatus");	
	String usuario = 	strLogin+" - "+strNombreUsuario;
	
   String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
	
	HashMap datos =  paginador.asignarEjecutivoCEDI(ic_solicitud , ic_estatus,  ic_ejecutivoCEDI, strLogin );
			
	jsonObj.put("success", new Boolean(true));	
	infoRegresar = jsonObj.toString(); 
	jsonObj.put("acuse",datos.get("ACUSE"));	
	jsonObj.put("fecha",fechaHoy);	
	jsonObj.put("hora",horaActual);	
	jsonObj.put("usuario",usuario);	
	jsonObj.put("solicAsignadas",datos.get("SOLICASIGNADAS"));	 
	
	infoRegresar = jsonObj.toString();	

System.out.println("  SOLICASIGNADAS "+datos.get("SOLICASIGNADAS")); 

}
   

%>
<%=infoRegresar%> 