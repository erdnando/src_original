Ext.onReady(function(){
	var rfc_empresarial;
	var razon_social;
	var no_solicitud;
	
	Ext.apply(Ext.form.field.VTypes, {
		rangoFechas: function(val, field) {
			var date = field.parseDate(val);
	
			if (!date) {
				return false;
			}
			if (field.startDateField && (!this.rangoFechasMax || (date.getTime() != this.rangoFechasMax.getTime()))) {
				var start = field.up('form').down('#' + field.startDateField);
				start.setMaxValue(date);
				start.validate();
				this.rangoFechasMax = date;
			} else if (field.endDateField && (!this.rangoFechasMin || (date.getTime() != this.rangoFechasMin.getTime()))) {
				var end = field.up('form').down('#' + field.endDateField);
				end.setMinValue(date);
				end.validate();
				this.rangoFechasMin = date;
			}
			return true;
		},
		rangoFechasText: 'La fecha de inicio debe ser menor que la fecha final'
	});
	
	var ic_elemento_cedula = [];
	var viabilidad = [];
	
	var cancelarGuardar =  function() { 
		ic_elemento_cedula = [];
		viabilidad = [];
	}
	
	
	// ---- Descarga de Archivos -------------
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};		
			
		  var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();					
		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	// ---- Guardar Cedula  -------------
	
	var procesarguardarViabilidad = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {

			Ext.Msg.alert(	'Aviso',	'La Informaci�n se guard� con �xito',	function(btn, text){
				Ext.ComponentQuery.query('#btnGuardar')[0].enable();											
			});				
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	var guardarCedula = function(  ) {
		var gridCedula =  Ext.ComponentQuery.query('#gridCedula')[0];		
		var store = gridCedula.getStore();		
		var no_solicitud;
		
		cancelarGuardar(); 
		
		store.each(function(record) {  			
			ic_elemento_cedula.push(record.data['IC_CEDULA']);
			viabilidad.push(record.data['VIABILIDAD']);
			no_solicitud = record.get('NO_SOLICITUD');
		});
		
		
		Ext.ComponentQuery.query('#btnGuardar')[0].disable();	
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: {
				informacion: 'GuardarViabilidad',
				ic_elemento_cedula:ic_elemento_cedula,
				viabilidad:viabilidad,
				no_solicitud:no_solicitud
			},
			callback: procesarguardarViabilidad
		});
				
	}
	
	// ---- Confirmar la Cedula  -------------
	var procesarConfirmarCedula = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			Ext.Msg.alert(	'Aviso',	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dictamen confirmado.',	function(btn, text){
				
				Ext.ComponentQuery.query('#VentanaCedula')[0].destroy();	
				//recargo la consulta 
				var  forma =   Ext.ComponentQuery.query('#forma')[0];
				consConsultaData.load({
					params: {							
						ic_intermediario:forma.query('#ic_intermediario1')[0].getValue(),							
						no_solicitud:forma.query('#txtFolioSolicitud1')[0].getValue(),
						txtFechaSoliIni:Ext.util.Format.date(forma.query('#txtFechaSoliIni')[0].getValue(),'d/m/Y'),
						txtFechaSoliFinal:Ext.util.Format.date(forma.query('#txtFechaSoliFinal')[0].getValue(),'d/m/Y')	  									
					}
				});					
			});
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	var confirmarCedula = function(  ) {
	
		var  forma =   Ext.ComponentQuery.query('#forma')[0];	
		var  no_solicitud = forma.query('#no_solicitud')[0].getValue();	
		
		Ext.ComponentQuery.query('#btnConfirmar_Dictamen')[0].disable();	
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: {
				informacion: 'ConfirmarCedula',				
				no_solicitud:no_solicitud
			},
			callback: procesarConfirmarCedula
		});
				
	}
	//var VentanaCedula = Ext.getCmp('VentanaCedula');	
	
	var VistaDictamenViabilidad = function (grid,rowIndex,colIndex,item,event){
		
		var registro = grid.getStore().getAt(rowIndex);
		no_solicitud = registro.get('NO_SOLICITUD');
		rfc_empresarial = registro.get('RFC_EMPRESARIAL');
		razon_social = registro.get('RAZON_SOCIAL');
		
		var  forma =   Ext.ComponentQuery.query('#forma')[0];	
		forma.query('#no_solicitud')[0].setValue(no_solicitud);	
				
		consCedulaData.load({
			params: {							
				no_solicitud:no_solicitud
			}
		});	
		new Ext.Window({
				layout: 'fit',
				modal: true,				
				width: 780,
				height: 850,												
				resizable: false,
				closable:true,				
				autoDestroy:true,
				closeAction: 'destroy',				
				id: 'VentanaCedula',
				items: [
					gridCedula				
				]			
			}).show().setTitle('Dictamen de Viabilidad de incorporaci�n � Folio Solicitud: '+no_solicitud);	
	
	}
	
	

	var procesarCedula = function(store,  arrRegistros, success, opts) {
				
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			
			var gridCedula =  Ext.ComponentQuery.query('#gridCedula')[0];	
			
			if(store.getTotalCount() > 0) {	
				Ext.ComponentQuery.query('#gridCedula')[0].getView().setAutoScroll(true);				
				Ext.ComponentQuery.query('#btnGenerarPDF')[0].show();				
				Ext.ComponentQuery.query('#btnGuardar')[0].show();
				Ext.ComponentQuery.query('#btnConfirmar_Dictamen')[0].show();
					
			}else  {									
				Ext.ComponentQuery.query('#gridCedula')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridCedula')[0].getView().setAutoScroll(false);
					
				Ext.ComponentQuery.query('#btnGenerarPDF')[0].hide();				
				Ext.ComponentQuery.query('#btnGuardar')[0].hide();
				Ext.ComponentQuery.query('#btnConfirmar_Dictamen')[0].hide();
				
			}

		}
	};
	Ext.define('LisCedula', {
		extend: 'Ext.data.Model',
		fields: [			
			{ name: 'IC_RUBRO'},
			{ name: 'DESCRIPCION_RUBRO'},
			{ name: 'IC_CEDULA'},
			{ name: 'NO_SOLICITUD'},			
			{ name: 'DESCRIPCION'},
			{ name: 'RESULTADO'},
			{ name: 'VIABILIDAD'}			
		 ]
	});
	var consCedulaData = Ext.create('Ext.data.Store', {
		model: 'LisCedula',		
		groupField:'IC_RUBRO',
		proxy: {
			type: 'ajax',
			url: '39AtencionSolicSeguimiento.data.jsp',
			reader: 		 { 
				type: 'json',  
				root: 'registros' 	
			},
			extraParams: { 
				informacion: 'ConsCedula' 	
			},
			listeners:   { exception: NE.util.mostrarProxyAjaxError 	}
		},
		autoLoad: false,
		listeners: {
			load: procesarCedula
		}
	});
	
	var groupingFeature = Ext.create('Ext.grid.feature.Grouping',{
        groupHeaderTpl: ' {name}'
	});
	
	
	var groupingFeature =  {
		ftype: 'grouping',
	  groupHeaderTpl: ' {name}'
	}
	
	function cambiosRenderer(val){
    return '<div style="white-space:normal !important;">'+ val +'</div>';
	}
	
	var  gridCedula = {
		xtype: 'grid',	
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridCedula',
		store: consCedulaData,
		height: 800,
		width: 770,	
		style: 'margin: 10px auto 0px auto;',		   
		frame: false,		
		scroll:true,
		selType: 'cellmodel',		
		features: [groupingFeature],			
		columns: [		
			{
				header: 'Secci�n',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 370,									
				align: 'left',				
				renderer:cambiosRenderer
         },		
			{
				header: 'Resultado de C�dula Electr�nica',
				dataIndex: 'RESULTADO',
				sortable: true,
				width: 200,
				resizable: true,
				align: 'center',
				renderer:function(value,metadata,registro){	
						var letra = registro.data['RESULTADO'].charAt(0); 
						var color;
						if(letra =='-') { color = 'color:red;' }

					if( registro.data['IC_CEDULA']==1 || registro.data['IC_CEDULA']==2 || registro.data['IC_CEDULA']==4  )  {						
						return  Ext.util.Format.number(value, '0.00%');						
					}else if( registro.data['IC_CEDULA']==5 || registro.data['IC_CEDULA']==5  )  {
						return  value +' Veces'; 
					}else  if(  ( registro.data['IC_CEDULA']>=33  &&  registro.data['IC_CEDULA']<=68 )  || registro.data['IC_CEDULA']==12  ) {						
						return  '<span style="'+color+'">' +Ext.util.Format.number(value, '0.00%')+'</span>';
					}else  {						
						return  value;  
					}
				}	
			},
			{
				header: 'Resultado Final de Viabilidad',
				dataIndex: 'VIABILIDAD',
				sortable: true,
				width: 200,
				resizable: true,
				align: 'center',
				tdCls: 'wrap',
				renderer:cambiosRenderer,
				editor: {
					xtype: 'textarea', 
					allowBlank: false,
					msgTarget: 'side',
					minValue: 0, 
					maxLength: 200,
					height:50,
					margins		: '0 20 0 0'
				} 				
			}
		],		
		emptyMsg: "No hay registros",
		border: false,
		displayInfo: true,
		bbar: [
			'-',
			'->', 
			{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDF',
				iconCls: 'icoPdf',				
				handler: function(button,event) {
					descDictamen( no_solicitud,  rfc_empresarial , razon_social );
				}
			},
			{
				xtype: 'button',
				text: 'Cancelar',
				id: 'btnCancelar',							
				iconCls: 'icoLimpiar',
				handler: function(button,event) {
					Ext.ComponentQuery.query('#VentanaCedula')[0].destroy();							
				}
			},
			{
				xtype: 'button',
				text: 'Guardar',
				id: 'btnGuardar',					
				iconCls: 'icoGuardar',
				handler: guardarCedula
			},
			{
				xtype: 'button',
				text: 'Confirmar Dictamen',
				id: 'btnConfirmar_Dictamen',
				iconCls: 'autorizar',				
				handler: confirmarCedula	
			}
		]				
	}
	

	
	 
	var  descDictamen =  function ( no_solicitud, rfc_empresarial , razon_social  ){
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'ViabilidadPDF',
				no_solicitud:no_solicitud,
				rfc_empresarial:rfc_empresarial,
				razon_social:razon_social
			}),
			callback: procesarDescargaArchivos
		});		
							
	}
	
	
	var procesarAceptar = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		no_solicitud = registro.get('NO_SOLICITUD');  
		var no_ifnb = registro.get('IC_IFNB');  
		 rfc_empresarial = registro.get('RFC_EMPRESARIAL');
		 razon_social = registro.get('RAZON_SOCIAL');
		
		if(registro.get('DICTAMEN')=='N' || registro.get('DICTAMEN')=='' ) {
			Ext.MessageBox.alert(	'Aviso',	'<center>A�n no ha realizado la revisi�n al Dictamen de <br> viabilidad de incorporaci�n.</center>');
			return;
		}else if(registro.get('DICTAMEN')=='S') {
			window.location = '/nafin/39CediIntermediarioNB/39VistaCorreo.jsp?no_solicitud='+no_solicitud+'&no_ifnb='+no_ifnb+'&pantalla=Seguimiento_Aceptada&rfc_empresarial='+rfc_empresarial+'&razon_social='+razon_social;
		}
	}
	
	var procesarRechaza = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');  
		var no_ifnb = registro.get('IC_IFNB');  
		
		if(registro.get('DICTAMEN')=='N' || registro.get('DICTAMEN')=='' ) {
			Ext.MessageBox.alert(	'Aviso',	'<center>A�n no ha realizado la revisi�n al Dictamen de <br> viabilidad de incorporaci�n.</center>');
			return;
		}else if(registro.get('DICTAMEN')=='S') {
			window.location = '/nafin/39CediIntermediarioNB/39VistaCorreo.jsp?no_solicitud='+no_solicitud+'&no_ifnb='+no_ifnb+'&pantalla=Seguimiento_Rechazada'; 
		}
	}
	
	//////////////Descarga de los Achivos  /////////////////
	



	var descManualCredito = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descManualCredito',
				no_solicitud:no_solicitud,				
				ic_doc_chekList:'1', 
				extension: registro.get('MANUAL_CREDITO')							
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	

	var descPresenCorporativa = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descPresenCorporativa',
				no_solicitud:no_solicitud,				
				ic_doc_chekList:'2', 
				extension: registro.get('PRESENTACION_CORPO')	
			}),
			callback: procesarDescargaArchivos
		});		
	}
	

	var descPasivosBancarios = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descPasivosBancarios',
				no_solicitud:no_solicitud,				
				ic_doc_chekList:'3', 
				extension: registro.get('DESGLOSE_PASIVO')	
			}),
			callback: procesarDescargaArchivos
		});		
	}
	

	var descCarteraVencida = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descCarteraVencida',
				no_solicitud:no_solicitud,				
				ic_doc_chekList:'4', 
				extension: registro.get('CARTERA_VENCIDA')	
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	var descPortafolioCartera = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descPortafolioCartera',
				no_solicitud:no_solicitud,				
				ic_doc_chekList:'5', 
				extension: registro.get('PORTAFOLIO')	
			}),
			callback: procesarDescargaArchivos
		});		
	}
	

	var descDoctoAdicional = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');  

		var win = NE.DoctosCheck.VistaDoctos(no_solicitud );
	
	}
	

	
	var descargaCedula = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		if(registro.get('LON_CEDULA_COMPLETA')!='0' ) {	
			Ext.Ajax.request({
				url: '39AtencionSolicSeguimiento.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{				
					informacion:'descargaCedula',
					no_solicitud:no_solicitud				
				}),
				callback: procesarDescargaArchivos
			});
		}
	}
	
	var descargaBalance = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descargaBalance',
				no_solicitud:no_solicitud,
				tipoArchivo:'balance'
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	var descargaEdoResultados = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descargaEdoResultados',
				no_solicitud:no_solicitud,
				tipoArchivo:'resultados'
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	var descargacopiaRFC = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AtencionSolicSeguimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descargacopiaRFC',
				no_solicitud:no_solicitud,
				tipoArchivo:'rfc'
			}),
			callback: procesarDescargaArchivos
		});		
	}
		
	
	//////////////Consulta General  /////////////////
	var procesarConsultar = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			var  forma =   Ext.ComponentQuery.query('#forma')[0];
			forma.el.unmask();
			
			var gridConsulta =  Ext.ComponentQuery.query('#gridConsulta')[0];	
			
			gridConsulta.show();	
						
			var datosIF = 	forma.query('#datosIF')[0].getValue();
						
			if(datosIF==true) {				
				gridConsulta.down("gridcolumn[dataIndex=DOMICILIO]").show();	
				gridConsulta.down("gridcolumn[dataIndex=ESTADO]").show();
				gridConsulta.down("gridcolumn[dataIndex=CODIGO_POSTAL]").show();
				gridConsulta.down("gridcolumn[dataIndex=CONTACTO]").show();
				gridConsulta.down("gridcolumn[dataIndex=EMAIL]").show();
				gridConsulta.down("gridcolumn[dataIndex=TELEFONO]").show();
			}else  {					
				gridConsulta.down("gridcolumn[dataIndex=DOMICILIO]").hide();	
				gridConsulta.down("gridcolumn[dataIndex=ESTADO]").hide();
				gridConsulta.down("gridcolumn[dataIndex=CODIGO_POSTAL]").hide();
				gridConsulta.down("gridcolumn[dataIndex=CONTACTO]").hide();
				gridConsulta.down("gridcolumn[dataIndex=EMAIL]").hide();
				gridConsulta.down("gridcolumn[dataIndex=TELEFONO]").hide();
			}
			
			if(store.getTotalCount() > 0) {
			
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(true);
			}else  {				
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(false);
			}

		}
	};

	
	Ext.define('LisRegistros', {
		extend: 'Ext.data.Model',
		fields: [
		   { name: 'NO_SOLICITUD'},
			{ name: 'IC_IFNB'},
			{ name: 'RAZON_SOCIAL'},
			{ name: 'RFC_EMPRESARIAL'},			
			{ name: 'FECHA_HORA_REG'},
			{ name: 'ESTATUS'},			
			{ name: 'IC_ESTATUS'},
			{ name: 'DOMICILIO'},
			{ name: 'ESTADO'},
			{ name: 'CODIGO_POSTAL'},
			{ name: 'CONTACTO'},
			{ name: 'EMAIL'},
			{ name: 'TELEFONO'},
			{ name: 'MANUAL_CREDITO'},
			{ name: 'PRESENTACION_CORPO'},
			{ name: 'DESGLOSE_PASIVO'},
			{ name: 'CARTERA_VENCIDA'},
			{ name: 'PORTAFOLIO'},
			{ name: 'DICTAMEN'},
			{ name: 'LON_BALANCE'},
			{ name: 'LON_RESULTADOS'},
			{ name: 'LON_RFC'},
			{ name: 'LON_CEDULA_COMPLETA'}			
		 ]
	});
	
	var consConsultaData = Ext.create('Ext.data.Store', {
		model: 'LisRegistros',
		proxy: {
			type: 'ajax',
			url: '39AtencionSolicSeguimiento.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consultar'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultar
			}
	});
	
	

	var  gridConsulta = Ext.create('Ext.grid.Panel',{
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridConsulta',
		store: consConsultaData,
		height: 600,		
		hidden:true,
		style: 'margin: 10px auto 0px auto;',		
		xtype: 'cell-editing',
      title: '',
      frame: true,					
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		columns: [			
			{
				header: 'Raz�n Social',
				dataIndex: 'RAZON_SOCIAL',
				sortable: true,
				width: 200,
				resizable: true,	
				align: 'left',	
				renderer:function(v) {
					return v.replace(/\n/g,'<br>'
				)}
         },
			{
				header: 'R.F.C Empresarial',
				dataIndex: 'RFC_EMPRESARIAL',
				sortable: true,
				width: 180,
				resizable: true,				
				align: 'center',
				renderer:function(v) {
					return v.replace(/\n/g,'<br>'
				)}				
         },
			{
				header: 'Domicilio',
				dataIndex: 'DOMICILIO',
				sortable: true,
				width: 200,
				resizable: true,
				hidden:true,
				align: 'left',
				renderer:function(v) {
					return v.replace(/\n/g,'<br>'
				)}	
         },		
			{
				header: 'Estado y Delegaci�n o Municipio', 
				dataIndex: 'ESTADO',
				sortable: true,
				width: 200,
				resizable: true,
				hidden:true,
				align: 'left',
				renderer:function(v) {
					return v.replace(/\n/g,'<br>'
				)}
         },
			{
				header: 'C�digo Postal empresarial',
				dataIndex: 'CODIGO_POSTAL',
				sortable: true,
				width: 150,
				resizable: true,
				hidden:true,
				align: 'center'
         },			
			{
				header: 'Contacto',
				dataIndex: 'CONTACTO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden:true,
				align: 'left',
				renderer:function(v) {
					return v.replace(/\n/g,'<br>'
				)}
         },
			{
				header: 'Correo Electr�nico ',
				dataIndex: 'EMAIL',
				sortable: true,
				width: 150,
				resizable: true,
				hidden:true,
				align: 'left',
				renderer:function(v) {
					return v.replace(/\n/g,'<br>'
				)}
         },
			{
				header: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden:true,
				align: 'center'
         },	
			
			///-------------------
			{
				header: 'Folio Solicitud',
				dataIndex: 'NO_SOLICITUD',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         },
			{
				header: 'Fecha y hora de registro',
				dataIndex: 'FECHA_HORA_REG',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         },
			{
				xtype		: 'actioncolumn',
				header: 'C�dula de Aceptaci�n',
				tooltip: 'C�dula de Aceptaci�n',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_CEDULA_COMPLETA']!='0' ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargaCedula
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Balance',
				tooltip: 'Balance',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_BALANCE']!='0' ) {	
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargaBalance
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Edo. Resultados',
				tooltip: 'Edo. Resultados',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_RESULTADOS']!='0' ) {	
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargaEdoResultados
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Copia R.F.C.',
				tooltip: 'Copia R.F.C.',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_RFC']!='0' ) {	
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargacopiaRFC
					}
				]
			},	
			{
				xtype		: 'actioncolumn',
				header: 'Manual de Cr�dito',
				tooltip: 'Manual de Cr�dito',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return record.get('MANUAL_CREDITO');
						}
						,handler: descManualCredito
					}
				]
			},	
			{
				xtype		: 'actioncolumn',
				header: 'Presentaci�n Corporativa',
				tooltip: 'Presentaci�n Corporativa',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return record.get('PRESENTACION_CORPO');
						}
						,handler: descPresenCorporativa
					}
				]
			},	
			{
				xtype		: 'actioncolumn',
				header: 'Desglose de pasivos bancarios',
				tooltip: 'Desglose de pasivos bancarios',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return record.get('DESGLOSE_PASIVO');
						}
						,handler: descPasivosBancarios
					}
				]
			},	
			{
				xtype		: 'actioncolumn',
				header: 'Antig�edad de la cartera vencida',
				tooltip: 'Antig�edad de la cartera vencida',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return record.get('CARTERA_VENCIDA');
						}
						,handler: descCarteraVencida
					}
				]
			},	
			{
				xtype		: 'actioncolumn',
				header: 'Portafolio de la Cartera',
				tooltip: 'Portafolio de la Cartera',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return record.get('PORTAFOLIO');
						}
						,handler: descPortafolioCartera
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Doctos Adicionales',
				tooltip: 'Doctos Adicionales',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'icoBuscar';
						}
						,handler: descDoctoAdicional
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Dictamen de viabilidad ',
				tooltip: 'Dictamen de viabilidad ',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.get('DICTAMEN')=='N' || record.get('DICTAMEN')==''  )  {
								this.items[0].tooltip = 'Ver Dictamen de Viabilidad';
								return 'icoBotonReporte';
							}
						}
						,handler: VistaDictamenViabilidad
					},
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.get('DICTAMEN')=='S')  {
								this.items[1].tooltip = 'Dictamen de Viabilidad';
								return 'icoPdf';
							}
						}
						,handler: function (grid,rowIndex,colIndex,item,event){
							var registro = grid.getStore().getAt(rowIndex);
							no_solicitud = registro.get('NO_SOLICITUD');
							rfc_empresarial = registro.get('RFC_EMPRESARIAL');
							razon_social = registro.get('RAZON_SOCIAL');
							if(registro.get('DICTAMEN')=='S')  {
								descDictamen( no_solicitud,  rfc_empresarial , razon_social );
							}
						}
					}
				]
			},			
			{
				header: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         },
			{
				xtype		: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Aceptar';
							return 'icoAceptar';
						}
						,handler: procesarAceptar
					},
						{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							this.items[1].tooltip = 'Rechazar';
							return 'borrar';
						}
						,handler: procesarRechaza
					}
				]
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		//stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false		
	});
	
	
	//*****************Formas  **************
	
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
	
	var  catIntermediarioData = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '39AtencionSolicSeguimiento.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catIntermediarioData'
			},
			listeners: {				
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var  elementosForma = [
		{
			xtype: 'combo',
			fieldLabel: 'Raz�n Social',
			itemId: 'ic_intermediario1',
			name: 'ic_intermediario',
			hiddenName: 'ic_intermediario',
			forceSelection: true,
			hidden: false, 
			width: 450,
			store: catIntermediarioData,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave'
		},	
		{
			xtype: 'numberfield',
			fieldLabel: 'Folio Solicitud',
			itemId: 'txtFolioSolicitud1',
			name: 'txtFolioSolicitud',
			maxLength: 12,
			width: 300				
		},
		{
			xtype: 'container',			
			layout: 'hbox', 
			width: 500,
			items: [
				{
					xtype: 'datefield',					
					fieldLabel: ' Fecha Solicitud',
					name: 'txtFechaSoliIni',
					itemId: 'txtFechaSoliIni',
					vtype: 'rangoFechas',
					endDateField: 'txtFechaSoliFinal',
					margins: '0 20 0 0',
					minValue: '01/01/1901',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side'
				},
				{
					xtype: 'displayfield',
					value: ' &nbsp;  al &nbsp;',
					width: 30
				},
				{
					xtype: 'datefield',					
					labelAlign: 'right',					
					name: 'txtFechaSoliFinal',
					itemId: 'txtFechaSoliFinal',
					vtype: 'rangoFechas',
					startDateField: 'txtFechaSoliIni',
					margins: '0 20 0 0',
					minValue: '01/01/1901',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side'
				}
			]
		},	
		{
			xtype:                  'container',
			anchor:                 '95%',
			layout:                 'column',
			items:[
				{
					xtype: 'container',columnWidth: .2, layout:'anchor', 
					items: [
						{	xtype: 'displayfield', value:'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', width: 100 }
					]
				},
				{
					xtype:  'container',	columnWidth:  .7,	layout:'anchor',
					items: [
						{ anchor: '100%',	xtype:  'checkbox',
						itemId:  'datosIF',					
						boxLabel:  '&nbsp; &nbsp; Mostrar informaci�n del Intermediario'						
						}
					]
				}
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'no_solicitud', 	value: '' }	
	];


	var fp = Ext.create('Ext.form.Panel',	{		
      itemId: 'forma',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: 'Seguimiento Pre-an�lisis de solicitudes',
      width: 550,
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 96
		},		
		items:elementosForma, 
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBucar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(){
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
					
					var txtFechaSoliIni =  forma.query('#txtFechaSoliIni')[0]; 
					var txtFechaSoliFinal =  forma.query('#txtFechaSoliFinal')[0]; 
					
					if(!Ext.isEmpty(txtFechaSoliIni.getValue()) || !Ext.isEmpty(txtFechaSoliFinal.getValue()) ){
						if(Ext.isEmpty(txtFechaSoliIni.getValue())){
							txtFechaSoliIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtFechaSoliIni.focus();
							return;
						}
						if(Ext.isEmpty(txtFechaSoliFinal.getValue())){
							txtFechaSoliFinal.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtFechaSoliFinal.focus();
							return;
						}
					}
					
					forma.el.mask('Consultando...', 'x-mask-loading');	
					
					consConsultaData.load({
						params: {							
							ic_intermediario:forma.query('#ic_intermediario1')[0].getValue(),							
							no_solicitud:forma.query('#txtFolioSolicitud1')[0].getValue(),
							txtFechaSoliIni:Ext.util.Format.date(forma.query('#txtFechaSoliIni')[0].getValue(),'d/m/Y'),
							txtFechaSoliFinal:Ext.util.Format.date(forma.query('#txtFechaSoliFinal')[0].getValue(),'d/m/Y')	  									
						}
					});
						
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',				
				handler: function(){
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
					
					forma.query('#txtFechaSoliIni')[0].setValue('');
					forma.query('#txtFechaSoliFinal')[0].setValue('');
					forma.query('#txtFolioSolicitud1')[0].setValue('');
					forma.query('#ic_intermediario1')[0].setValue('');	
					forma.query('#datosIF')[0].setValue(false);		
					Ext.ComponentQuery.query('#gridConsulta')[0].hide();		
				}
			}
		]
	});
	
	
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 920,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [							
			fp,			
			NE.util.getEspaciador(20),
			gridConsulta				
		]
	});
	
	catIntermediarioData.load();	


					
});