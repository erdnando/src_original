Ext.onReady(function(){


	Ext.apply(Ext.form.field.VTypes, {
		rangoFechas: function(val, field) {
			var date = field.parseDate(val);
	
			if (!date) {
				return false;
			}
			if (field.startDateField && (!this.rangoFechasMax || (date.getTime() != this.rangoFechasMax.getTime()))) {
				var start = field.up('form').down('#' + field.startDateField);
				start.setMaxValue(date);
				start.validate();
				this.rangoFechasMax = date;
			} else if (field.endDateField && (!this.rangoFechasMin || (date.getTime() != this.rangoFechasMin.getTime()))) {
				var end = field.up('form').down('#' + field.endDateField);
				end.setMinValue(date);
				end.validate();
				this.rangoFechasMin = date;
			}
			return true;
		},
		rangoFechasText: 'La fecha de inicio debe ser menor que la fecha final'
	});
	 
	var ic_solicitud =  [];
	var ic_estatus =  [];

	var cancelar =  function() {  
		ic_solicitud =  [];
		ic_estatus =  [];
	}
	
	var  procesaAsignaEjecutivoCedu = function(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.JSON.decode(response.responseText);
			var  forma =   Ext.ComponentQuery.query('#forma')[0];
				consConsultaEjeData.load({
					params: {							
						solicAsignadas:resp.solicAsignadas,
						ic_intermediario:forma.query('#ic_intermediario1')[0].getValue(),
						txtRFC:forma.query('#txtRFC1')[0].getValue(),
						txtFolioSolicitud:forma.query('#txtFolioSolicitud1')[0].getValue(),
						txtFechaSoliIni:Ext.util.Format.date(forma.query('#txtFechaSoliIni')[0].getValue(),'d/m/Y'),
						txtFechaSoliFinal:Ext.util.Format.date(forma.query('#txtFechaSoliFinal')[0].getValue(),'d/m/Y')	 	  
					}
				});
				
			var acuseCifras = [
				['N�mero de Acuse', resp.acuse],
				['Fecha de Asignaci�n ', resp.fecha],
				['Hora de Asignaci�n',  resp.hora],
				['Usuario ', resp.usuario]
			];
		
			storeCifrasData.loadData(acuseCifras);	
			Ext.ComponentQuery.query('#gridCifras')[0].show();	
					
			Ext.ComponentQuery.query('#gridConsulta')[0].hide();
			Ext.ComponentQuery.query('#fpEje')[0].hide();
			Ext.ComponentQuery.query('#forma')[0].hide();	
			
		} else {						
			NE.util.mostrarErrorPeticion(response); 
		}
	}
	

	var proAsignacionEje = function() {
		var gridConsulta =  Ext.ComponentQuery.query('#gridConsulta')[0];		
		var store = gridConsulta.getStore();	
		var total =0;
		var  fpEje =   Ext.ComponentQuery.query('#fpEje')[0];	
		cancelar(); //limpia las variables
		
		store.each(function(record){							
			if(record.get('SELECCIONAR')){
				ic_solicitud.push(record.get('NO_SOLICITUD'));
				ic_estatus.push(record.get('IC_ESTATUS'));
				total++;											
			}								
								
		});
		if(total==0){
			Ext.Msg.alert(	'Aviso',	'&nbsp;&nbsp;&nbsp;&nbsp; Seleccione al menos una solicitud');
			return;
		}else if(total>0){
					
			if(fpEje.query('#ic_ejecutivoCEDI1')[0].getValue()=='null' || fpEje.query('#ic_ejecutivoCEDI1')[0].getValue()==null ) {				
				Ext.Msg.alert(	'Aviso',	' &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; Seleccione un Ejecutivo CEDI'); 				
			}else  {
			
				
			Ext.MessageBox.confirm('Confirmaci�n','�Confirma asignar la(s) <br> &nbsp;&nbsp;&nbsp; solicitud(es) <br>  al Ejecutivo CEDI elegido?', function(resp){
				if(resp =='yes'){		
						Ext.ComponentQuery.query('#btnAsignarEje')[0].disable();					
						Ext.ComponentQuery.query('#btnAsignarEje')[0].setIconCls('x-mask-msg-text');
						
						Ext.Ajax.request({
							url: '39AsignaSolicitud.data.jsp',
							params: 	{
								informacion: 'AsignaEjecutivoCedu',
								ic_solicitud:ic_solicitud,
								ic_estatus:ic_estatus, 
								ic_ejecutivoCEDI:fpEje.query('#ic_ejecutivoCEDI1')[0].getValue()		
							},
							callback: procesaAsignaEjecutivoCedu
						});
					}			
				});
				
				
			}	
		}
	}
	
	var procesarConsultaEje = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			
			Ext.ComponentQuery.query('#gridConsEje')[0].show();	
			
			if(store.getTotalCount() > 0) {			
			
			}else  {				
				Ext.ComponentQuery.query('#gridConsEje')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
			}
		}
	};

	
	Ext.define('LisRegistros', {
		extend: 'Ext.data.Model',
		fields: [
		   { name: 'NO_SOLICITUD'},
			{ name: 'EJECUTIVO_CEDI'},		
			{ name: 'ESTATUS'}				
		 ]
	});
	
	var consConsultaEjeData = Ext.create('Ext.data.Store', {
		model: 'LisRegistros',
		proxy: {
			type: 'ajax',
			url: '39AsignaSolicitud.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'ConsultarEje'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultaEje
			}
	});
	 
	
	var  gridConsEje = Ext.create('Ext.grid.Panel',{
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridConsEje',
		store: consConsultaEjeData,		
		hidden:true,
		style: 'margin: 10px auto 0px auto;',		
		xtype: 'cell-editing',
      title: '<center>La asignaci�n se llev� acabo con �xito </center> ',
      frame: true,					
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		columns: [		
			{
				header: 'Folio Solicitud',
				dataIndex: 'NO_SOLICITUD',
				sortable: true,
				width: 100,
				resizable: true,
				align: 'center'
         },			
			{
				header: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 200,
				resizable: true,
				align: 'center'
         },
			{
				header: 'Ejecutivo CEDI',
				dataIndex: 'EJECUTIVO_CEDI',
				sortable: true,
				width: 300,
				resizable: true
         }
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 'auto',
		width: 620,
		align: 'center',
		frame: false
	});
	

	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'ETIQUETA'},
			  {name: 'INFORMACION'}
		  ]
	 });
	 
	 
	var  gridCifras = Ext.create('Ext.grid.Panel',{	
		itemId: 'gridCifras',
		store: storeCifrasData,		
		hidden:true,		
		columns: [		
			{				
				dataIndex: 'ETIQUETA',
				sortable: false,
				width: 200,
				menuDisabled :true,
				resizable: false
         },			
			{
			  dataIndex: 'INFORMACION',
				sortable: false,
				width: 417,
				menuDisabled :true,
				resizable: false
         }
		],		
		height: 150,
		width: 620,
		align: 'center',
		frame: false,
		bbar: [
			'->','-',
			{
				text: 'Salir',
				id: 'btnSalir',				
				handler: function(){
					window.location = '39AsignaSolicitud.jsp';		
				}
			}
		]
	});
	





	//************Descarga de los Achivos **********************+			
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};		
			
		  var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();					
		
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	
	var descargaCedula = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		var no_estatus = registro.get('IC_ESTATUS');
		if(registro.get('LON_CEDULA_COMPLETA')!='0' ) {		
			Ext.Ajax.request({
				url: '39AsignaSolicitud.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{				
					informacion:'descargaCedula',
					no_solicitud:no_solicitud,
					no_estatus:no_estatus
				}),
				callback: procesarDescargaArchivos
			});
		}		
	}
	
	var descargaBalance = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AsignaSolicitud.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descargaBalance',
				no_solicitud:no_solicitud,
				tipoArchivo:'balance'
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	var descargaEdoResultados = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AsignaSolicitud.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descargaEdoResultados',
				no_solicitud:no_solicitud,
				tipoArchivo:'resultados'
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	var descargacopiaRFC = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '39AsignaSolicitud.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'descargacopiaRFC',
				no_solicitud:no_solicitud,
				tipoArchivo:'rfc'
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	
	
  // Modelos y Consultas

	
	var procesarConsultar = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var json =  store.proxy.reader.rawData;
			
			var  forma =   Ext.ComponentQuery.query('#forma')[0];
			forma.el.unmask();
			Ext.ComponentQuery.query('#gridConsulta')[0].show();	
			
			if(store.getTotalCount() > 0) {
			
				Ext.ComponentQuery.query('#btnAsignarEje')[0].enable();
				Ext.ComponentQuery.query('#fpEje')[0].show();
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(true);
			}else  {
				Ext.ComponentQuery.query('#btnAsignarEje')[0].disable();
				Ext.ComponentQuery.query('#fpEje')[0].hide();
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				Ext.ComponentQuery.query('#gridConsulta')[0].getView().setAutoScroll(false);
			}

		}
	};

	
	Ext.define('LisRegistros', {
		extend: 'Ext.data.Model',
		fields: [
		   { name: 'NO_SOLICITUD'},
			{ name: 'RAZON_SOCIAL'},
			{ name: 'RFC_EMPRESARIAL'},			
			{ name: 'FECHA_HORA_REG'},
			{ name: 'ESTATUS'},
			{ name: 'SELECCIONAR'},
			{ name: 'IC_ESTATUS'},
			{ name: 'LON_BALANCE'},
			{ name: 'LON_RESULTADOS'},
			{ name: 'LON_RFC'},
			{ name: 'LON_CEDULA_COMPLETA'}			
		 ]
	});
	
	var consConsultaData = Ext.create('Ext.data.Store', {
		model: 'LisRegistros',
		proxy: {
			type: 'ajax',
			url: '39AsignaSolicitud.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consultar'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultar
			}
	});
	
	var  gridConsulta = Ext.create('Ext.grid.Panel',{
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridConsulta',
		store: consConsultaData,
		height: 600,		
		hidden:true,
		style: 'margin: 10px auto 0px auto;',		
		xtype: 'cell-editing',
      title: 'Solicitudes',
      frame: true,					
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		columns: [
			{
				xtype: 'checkcolumn',
            header: 'Selec.',
            dataIndex: 'SELECCIONAR',				
            width: 40,
            stopSelection: false	
			},
			{
				header: 'Raz�n Social',
				dataIndex: 'RAZON_SOCIAL',
				sortable: true,
				width: 190,
				resizable: true
         },
			{
				header: 'RFC Empresarial',
				dataIndex: 'RFC_EMPRESARIAL',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         },
			{
				header: 'Folio Solicitud',
				dataIndex: 'NO_SOLICITUD',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         },
			{
				header: 'Fecha y hora de registro',
				dataIndex: 'FECHA_HORA_REG',
				sortable: true,
				width: 190,
				resizable: true
         },
			{
				xtype		: 'actioncolumn',
				header: 'C�dula de Aceptaci�n',
				tooltip: 'C�dula de Aceptaci�n',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){							
							if(record.data['LON_CEDULA_COMPLETA']!='0' ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargaCedula
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Balance',
				tooltip: 'Balance',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_BALANCE']!='0' ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargaBalance
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Edo. Resultados',
				tooltip: 'Edo. Resultados',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_RESULTADOS']!='0' ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargaEdoResultados
					}
				]
			},
			{
				xtype		: 'actioncolumn',
				header: 'Copia R.F.C.',
				tooltip: 'Copia R.F.C.',				
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				items		: [
					{
						getClass: function(value,metadata,record,rowIndex,colIndex,store){
							if(record.data['LON_RFC']!='0' ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}
						}
						,handler: descargacopiaRFC
					}
				]
			},			
			{
				header: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 190,
				resizable: true,
				align: 'center'
         }
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: [
			'->','-',
			{
				text: 'Asignar Ejecutivo CEDI',
				id: 'btnAsignarEje',	
				iconCls: 'icoLimpiar',
				handler: proAsignacionEje
			}
		] 
	});
	
	
	//*****************Formas  **************
	
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', 					type: 'string' },
			{ name: 'descripcion', 			type: 'string' }				
		]
	});
	
	
	var  catIntermediarioData = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '39AsignaSolicitud.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catIntermediarioData'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	var  catEjecutivoCEDIData = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		proxy: {
			type: 'ajax',
			url: '39AsignaSolicitud.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catEjecutivoCEDIData'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}		
	});
	
	
	
	var fpEje = Ext.create('Ext.form.Panel',	{
		layout: 'form',
      itemId: 'fpEje',
      frame: true,
		border: true,
		hidden: true,
      bodyPadding: '12 6 12 6',
		title: '',
      width: 550,
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 160
		},
		layout: 'anchor',	
		items:[
			{
				xtype: 'combo',
				fieldLabel: 'Asignar Ejecutivo CEDI',
				itemId: 'ic_ejecutivoCEDI1',
				name: 'ic_ejecutivoCEDI',
				hiddenName: 'ic_ejecutivoCEDI',
				forceSelection: true,
				hidden: false, 
				width: 450,
				store: catEjecutivoCEDIData,
				emptyText: 'Seleccione...',
				queryMode: 'local',
				allowBlank: true,
				displayField: 'descripcion',
				valueField: 'clave'
			}
		]
	});
	
	
	var  elementosForma = [
		{
			xtype: 'combo',
			fieldLabel: 'Raz�n Social',
			itemId: 'ic_intermediario1',
			name: 'ic_intermediario',
			hiddenName: 'ic_intermediario',
			mode: 'local',
			autoLoad: false,
			forceSelection: true,
			hidden: false, 
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 450,
			store: catIntermediarioData,
			emptyText: 'Seleccione...',
			queryMode: 'local',
			allowBlank: true,
			displayField: 'descripcion',
			valueField: 'clave'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'R.F.C. Empresarial',
			itemId: 'txtRFC1',
			name: 'txtRFC',			
			width: 300,	
			maxLength: 20,
			regex			:/^([A-Z|&amp;]{3,4})-(([0-9]{2})([0][123456789]|[1][012])([0][1-9]|[12][\d]|[3][0])|([0-9]{2})([0][13578]|[1][02])([0][1-9]|[12][\d]|[3][01])|([02468][048]|[13579][26])([0][2])([0][1-9]|[12][\d])|([1-9]{2})([0][2])([0][1-9]|[12][0-8]))-([A-Z0-9]{3}$)/,
			regexText	:'Por favor escriba correctamente el RFC en may�sculas  separado por '+
			'guiones en el formato NNN-AAMMDD-XXX donde:<br>'+
			'NNN:son las iniciales del nombre de la empresa<br>'+
			'AAMMDD: es la fecha de creaci�n de la empresa menor al d�a de hoy.<br>'+
			'XXX:es la homoclave'			
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'Folio Solicitud',
			itemId: 'txtFolioSolicitud1',
			name: 'txtFolioSolicitud',
			maxLength: 12,
			width: 300				
		}	,
		{
			xtype: 'container',			
			layout: 'hbox', 
			width: 500,
			items: [
				{
					xtype: 'datefield',					
					fieldLabel: ' Fecha Solicitud',
					name: 'txtFechaSoliIni',
					itemId: 'txtFechaSoliIni',
					vtype: 'rangoFechas',
					endDateField: 'txtFechaSoliFinal',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side',				
					minValue: '01/01/1901',					
					margins: '0 20 0 0'	
				},
				{
					xtype: 'displayfield',
					value: '&nbsp;al &nbsp;', 
					width: 30
				},
				{
					xtype: 'datefield',					
					labelAlign: 'right',					
					name: 'txtFechaSoliFinal',
					itemId: 'txtFechaSoliFinal',
					vtype: 'rangoFechas',
					startDateField: 'txtFechaSoliIni',
					allowBlank: true,
					startDay: 1,					
					msgTarget: 'side',				
					minValue: '01/01/1901',					
					margins: '0 20 0 0'	
				}
			]
		}					
	];


	var fp = Ext.create('Ext.form.Panel',	{		
      itemId: 'forma',
      frame: true,
		border: true,
      bodyPadding: '12 6 12 6',
		title: 'Solicitudes Aceptadas en la autoevaluaci�n de Intermediario',
      width: 550,
      style: 'margin: 0px auto 0px auto;',     
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 120
		},		
		items:elementosForma, 
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBucar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(){
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
					
					var txtFechaSoliIni =  forma.query('#txtFechaSoliIni')[0]; 
					var txtFechaSoliFinal =  forma.query('#txtFechaSoliFinal')[0]; 
					
					if(!Ext.isEmpty(txtFechaSoliIni.getValue()) || !Ext.isEmpty(txtFechaSoliFinal.getValue()) ){
						if(Ext.isEmpty(txtFechaSoliIni.getValue())){
							txtFechaSoliIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtFechaSoliIni.focus();
							return;
						}
						if(Ext.isEmpty(txtFechaSoliFinal.getValue())){
							txtFechaSoliFinal.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							txtFechaSoliFinal.focus();
							return;
						}
					}
					
					forma.el.mask('Consultando...', 'x-mask-loading');	
					consConsultaData.load({
						params: {							
							ic_intermediario:forma.query('#ic_intermediario1')[0].getValue(),
							txtRFC:forma.query('#txtRFC1')[0].getValue(),
							txtFolioSolicitud:forma.query('#txtFolioSolicitud1')[0].getValue(),
							txtFechaSoliIni:Ext.util.Format.date(forma.query('#txtFechaSoliIni')[0].getValue(),'d/m/Y'),
							txtFechaSoliFinal:Ext.util.Format.date(forma.query('#txtFechaSoliFinal')[0].getValue(),'d/m/Y')	  
									
						}
					});
						
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',				
				handler: function(){
					var  forma =   Ext.ComponentQuery.query('#forma')[0];
					forma.query('#txtFechaSoliIni')[0].setValue('');
					forma.query('#txtFechaSoliFinal')[0].setValue(''); 
					forma.query('#txtFolioSolicitud1')[0].setValue(''); 
					forma.query('#txtRFC1')[0].setValue(''); 
					forma.query('#ic_intermediario1')[0].setValue(''); 
					
					var  fpEje =   Ext.ComponentQuery.query('#fpEje')[0];	
					Ext.ComponentQuery.query('#fpEje')[0].hide();	
					fpEje.query('#ic_ejecutivoCEDI1')[0].setValue('')	
				
					Ext.ComponentQuery.query('#gridConsulta')[0].hide();	
					
					
				}
			}
		]
	});
	
	
	
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 920,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [							
			fp,			
			NE.util.getEspaciador(20),
			gridConsulta,
			gridConsEje,
			gridCifras,
			NE.util.getEspaciador(20),
			fpEje
			
		]
	});
	
	catIntermediarioData.load();
	catEjecutivoCEDIData.load();

/*	consConsultaData.load({
		params: {							
			informacion: 'Consultar'			
		}
	});
	*/
					
});