<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		com.netro.cedi.*,
		com.netro.model.catalogos.*,		
		com.netro.pdf.*,
		netropology.utilerias.*,
		com.netro.seguridad.*,
		java.sql.*,
		org.apache.commons.logging.Log,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/39CediIntermediarioNB/39secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_intermediario = (request.getParameter("ic_intermediario")!=null)?request.getParameter("ic_intermediario"):"";
String txtFechaSoliIni = (request.getParameter("txtFechaSoliIni")!=null)?request.getParameter("txtFechaSoliIni"):"";
String txtFechaSoliFinal = (request.getParameter("txtFechaSoliFinal")!=null)?request.getParameter("txtFechaSoliFinal"):"";
String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
String txtObservacionesCEDI = (request.getParameter("txtObservacionesCEDI")!=null)?request.getParameter("txtObservacionesCEDI"):"";
String ic_doc_chekList = (request.getParameter("ic_doc_chekList")!=null)?request.getParameter("ic_doc_chekList"):"";
String extension = (request.getParameter("extension")!=null)?request.getParameter("extension"):"";

String rfc_empresarial = (request.getParameter("rfc_empresarial")!=null)?request.getParameter("rfc_empresarial"):"";
String razon_social = (request.getParameter("razon_social")!=null)?request.getParameter("razon_social"):"";

									

String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String diaActual    = fechaActual.substring(0,2);
String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
String anioActual   = fechaActual.substring(6,10);
String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
String  lbfechaActual =  "México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual;

UtilUsr utilUsr = new UtilUsr();
Usuario usuarioObj = utilUsr.getUsuario(strLogin);
String destinatario = usuarioObj.getEmail();						
			
String  lbNombreUsuario = "Lic."+strNombreUsuario;
String lbCorreoUsuario  =  destinatario;

String infoRegresar	=	"", consulta ="";
JSONObject jsonObj = new JSONObject();	


Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 

 
ConsAtencionSeguimiento paginador = new ConsAtencionSeguimiento();		

paginador.setIc_intermediario(ic_intermediario);
paginador.setTxtFolioSolicitud(no_solicitud);
paginador.setTxtFechaSoliIni(txtFechaSoliIni);
paginador.setTxtFechaSoliFinal(txtFechaSoliFinal);
paginador.setTipoConsulta(informacion); 
paginador.setRfc_empresarial(rfc_empresarial);
paginador.setRazon_social(razon_social); 
paginador.setEjecutivoAsignado(strLogin); 


CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);	
  
if (informacion.equals("catIntermediarioData")){

	CatalogoIFNB_CEDI catalogo = new CatalogoIFNB_CEDI();
	catalogo.setCampoClave("IC_IFNB_ASPIRANTE");
	catalogo.setCampoDescripcion("CG_RAZON_SOCIAL");
	catalogo.setEstatus("5");
	catalogo.setEjecutivoAsignado(strLogin);	
	catalogo.setOrden("IC_IFNB_ASPIRANTE");	
	infoRegresar = catalogo.getJSONElementos();	
	
	
}else  if (informacion.equals("Consultar")   || informacion.equals("ConsCedula")      ){
	
	
	Registros reg	=	queryHelper.doSearch();
	
	if (informacion.equals("Consultar") )  {
		
		while (reg.next()) {
			 
			List checks =  cediBean.getCheckList ( reg.getString("NO_SOLICITUD") ,  "S"  ); 
			String manual_credito =  "", presentacion="", desglose_pasivo ="", carteraVencida ="", portafolio =   "", icono ="vacio";
			
			for (int i = 0; i <checks.size(); i++) {
				icono ="vacio";
				HashMap  datos = (HashMap)checks.get(i);
				String 	ext  = datos.get("CG_EXTENSION").toString();
				String 	ic_check  = datos.get("IC_CHECKLIST").toString();	
				
				
				if(ext.equals("pdf") )  { icono  ="icoPdf"; } 
				if(ext.equals("csv") || ext.equals("xlsx")  || ext.equals("xls"))  { icono  ="icoXls"; }
				if(ext.equals("doc") || ext.equals("docx") )  { icono  ="icoDoc"; }	
				if(ext.equals("pptx") || ext.equals("ppt") )  { icono  ="icoPpt"; }
				
				if(ic_check.equals("1")  ) {  manual_credito=  icono;  }
				if(ic_check.equals("2")) {  presentacion=  icono;  }
				if(ic_check.equals("3")) {  desglose_pasivo=  icono;  }
				if(ic_check.equals("4")) {  carteraVencida=  icono;  }	
				if(ic_check.equals("5")) {  portafolio=  icono;  }	
			}
			reg.setObject("MANUAL_CREDITO", manual_credito);	
			reg.setObject("PRESENTACION_CORPO", presentacion);	
			reg.setObject("DESGLOSE_PASIVO", desglose_pasivo);	
			reg.setObject("CARTERA_VENCIDA", carteraVencida);	
			reg.setObject("PORTAFOLIO", portafolio);			
		}	
	}else  if (informacion.equals("ConsCedula") )  {
		
			String  anioFinaciero ="";
		
			while (reg.next()) {
				
				int  ic_cedula = Integer.parseInt( reg.getString("IC_CEDULA") ) ;
				String mes_anio_financiero =  reg.getString("MES_ANIO_FINANCIERO")==null?"":reg.getString("MES_ANIO_FINANCIERO");
				int  anio_finaciero = Integer.parseInt( reg.getString("ANIO_FINANCIERO") ) ;
				
				//Para los Rubros  
				if(ic_cedula>=1 &  ic_cedula<=11 ) { 
					
					reg.setObject("IC_RUBRO", "1-Indicadores Financieros");
					reg.setObject("DESCRIPCION_RUBRO", "Indicadores Financieros");
					
				}else  if(ic_cedula>=12 &  ic_cedula<=32 ) { 
					
					reg.setObject("IC_RUBRO", "2-Información Cualitativa");
					reg.setObject("DESCRIPCION_RUBRO", "Información Cualitativa");				
					
				}else  if(ic_cedula>=33 &  ic_cedula<=33 ) { 
					
					reg.setObject("IC_RUBRO", "3-Crédito Vinculado");
					reg.setObject("DESCRIPCION_RUBRO", "Crédito Vinculado");
					
				}else  if(ic_cedula>=34 &  ic_cedula<=44 ) { 
					
					anioFinaciero = String.valueOf(anio_finaciero);
					
					reg.setObject("IC_RUBRO", "4-Información Financiera " +mes_anio_financiero);
					reg.setObject("DESCRIPCION_RUBRO", "Información Financiera " +mes_anio_financiero );
				
				}else  if(ic_cedula>=45 &  ic_cedula<=55 ) { 
				
					anio_finaciero = anio_finaciero-1;
					anioFinaciero = String.valueOf(anio_finaciero);
					
					reg.setObject("IC_RUBRO", "5-Información Financiera "+anio_finaciero);
					reg.setObject("DESCRIPCION_RUBRO", "Información Financiera "+anio_finaciero);
					
				}else  if(ic_cedula>=56 &  ic_cedula<=66 ) { 
				
					anio_finaciero = anio_finaciero-2;
					anioFinaciero = String.valueOf(anio_finaciero);
					
					reg.setObject("IC_RUBRO", "6-Información Financiera "+anio_finaciero);
					reg.setObject("DESCRIPCION_RUBRO", "Información Financiera "+anio_finaciero);
					
				}else  if(ic_cedula>=67 &  ic_cedula<=68 ) { 
					reg.setObject("IC_RUBRO", "7-Saldos");
					reg.setObject("DESCRIPCION_RUBRO", "Saldos");
				}
				
				// Información Cualitativa
				if(ic_cedula>=12 &  ic_cedula<=32 ) { 
						//Monto de línea solicitado  IC_PREGUNTA =1 , IC_TIPO_RESPUESTA  = 1
						if(ic_cedula==12)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "1","1" ));   
						}
						//'Tipo de empresas apoyadas IC_PREGUNTA =2 ,  IC_TIPO_RESPUESTA  = 2
						if(ic_cedula==13)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "2","2" ));   
						}
						// Sector  IC_PREGUNTA =3 ,  IC_TIPO_RESPUESTA  = 2
						if(ic_cedula==14)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "3","2" ));   
						}
						// Destino de recursos solicitados  IC_PREGUNTA =4 ,  IC_TIPO_RESPUESTA  = 2
						if(ic_cedula==15)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "4","2" ));   
						}
						// ¿Cuenta con al menos 50 MDP de cartera en un mismo producto con destino PyME? IC_PREGUNTA =5 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==16)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "5","3" ));   
						}
						// ¿Cuenta con al menos 25 MDP de cartera en un mismo producto con destino Microcrédito? IC_PREGUNTA =6 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==17)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "6","3" ));   
						}
						// 'Experiencia en años en el otorgamiento de crédito IC_PREGUNTA =7 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==18)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "7","3" ));   
						}
						
						// ¿Realiza consultas y reportes en el Círculo o Buró de Crédito de sus acreditados? IC_PREGUNTA =8 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==19)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "8","3" ));   
						}
						// ¿Cuenta con instalaciones, equipos y sistemas para controlar y administrar la cartera de crédito? IC_PREGUNTA =9 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==20)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "9","3" ));   
						}
						// ¿Cuenta con un Manual de Crédito que considere todas las etapas del proceso de crédito: desde la promoción y originación, hasta el seguimiento y la recuperación de créditos? IC_PREGUNTA =10 ,  IC_TIPO_RESPUESTA  = 3 
						if(ic_cedula==21)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "10","3" ));   
						}
						// ¿Su manual contiene las características, criterios y metodología de evaluación del producto que desea NAFIN fondee? IC_PREGUNTA =11 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==22)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "11","3" ));   
						}
						// ¿Cuenta con Comité de Crédito con facultades claramente delimitadas? IC_PREGUNTA =12 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==23)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "12","3" ));   
						}
						// ¿Cuenta con Consejo de Administración? IC_PREGUNTA =13 ,  IC_TIPO_RESPUESTA  = 2
						if(ic_cedula==24)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "13","2" ));   
						}
						
						// ¿Cuenta con otro Comité? IC_PREGUNTA =14 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==25)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "14","3" ));   
						}
						//¿Cómo es su metodología de calificación de cartera? IC_PREGUNTA =15 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==26)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "15","3" ));   
						}
						// ¿El registro contable lo realiza con base en los criterios de la Comisión Nacional Bancario y de Valores (CNBV)? IC_PREGUNTA =16 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==27)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "16","3" ));   
						}
						// ¿Su metodología de calificación de cartera y creación de reservas es conforme a los criterios de la Comisión Nacional Bancaria y de Valores (CNBV)? IC_PREGUNTA =17 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==28)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "17","3" ));   
						}
						// ¿Registra cartera vencida y traspasos conforme a los criterios de la Comisión Nacional Bancaria y de Valores (CNBV)?  IC_PREGUNTA =18 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==29)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "18","3" ));   
						}
						// Da tratamiento a las renovaciones y reestructuras de sus créditos otorgados conforme a los criterios de la Comisión Nacional Bancaria y de Valores (CNBV)? IC_PREGUNTA =19 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==30)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "19","3" ));   
						}
						// ¿Cuenta con Fondeo de la Banca Comercial? IC_PREGUNTA =20 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==31)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "20","3" ));   
						}
						// ¿Cuenta con Fondeo de la Banca de Desarrollo? IC_PREGUNTA =21 ,  IC_TIPO_RESPUESTA  = 3
						if(ic_cedula==32)  {	
							reg.setObject("RESULTADO", paginador.getInfoCualitativa ( no_solicitud , "21","3" ));   
						}
				}
				
				//para sacar la Información Finaciera ) 
				if(ic_cedula>=34 &  ic_cedula<=66 ) { 
					
					List info =  paginador.getInfoFinanciera ( reg.getString("NO_SOLICITUD")  );
					
					for (int i = 0; i <info.size(); i++) {
						HashMap  datos = (HashMap)info.get(i);	
						
						String anio =  datos.get("IC_ANIO").toString();
						String activosTotales =  datos.get("FG_ACTIVOS_TOTALES").toString();
						String inversionCaja =  datos.get("FG_INVERSION_CAJA").toString();
						String carteraVigente =  datos.get("FG_CARTERA_VIGENTE").toString();
						String carteraVencida =  datos.get("FG_CARTERA_VENCIDA").toString();
						String carteraTotal =  datos.get("FG_CARTERA_TOTAL").toString();
						String estimacionReservas =  datos.get("FG_ESTIMACION_RESERVAS").toString();
						String pasivosBancarios =  datos.get("FG_PASIVOS_BANCARIOS").toString();
						String pasivosTotales =  datos.get("FG_PASIVOS_TOTALES").toString();
						String capitalSocial =  datos.get("FG_CAPITAL_SOCIAL").toString();
						String capitalContable =  datos.get("FG_CAPITAL_CONTABLE").toString();
						String utilidadNeta =  datos.get("FG_UTILIDAD_NETA").toString();
						String nioSolic =  datos.get("IG_ANIO_INF_FINANCIERA").toString();
						
						//Activos Totales 
						if(ic_cedula==34 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", activosTotales);
						}else if( ic_cedula==45  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", activosTotales);
						}else if( ic_cedula==56 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", activosTotales); 		}
						
						
						//Inversiones y Caja 
						if(ic_cedula==35 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", inversionCaja);
						}else if( ic_cedula==46  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", inversionCaja);
						}else if( ic_cedula==57 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", inversionCaja); 		}
						
						//Cartera Vigentea 
						if(ic_cedula==36 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", carteraVigente);
						}else if( ic_cedula==47  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", carteraVigente);
						}else if( ic_cedula==58 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", carteraVigente); 		}
						
						//Cartera vencida 
						if(ic_cedula==37 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", carteraVencida);
						}else if( ic_cedula==48  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", carteraVencida);
						}else if( ic_cedula==59 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", carteraVencida); 		}
						
						//Cartera Total 
						if(ic_cedula==38 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", carteraTotal);
						}else if( ic_cedula==49  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", carteraTotal);
						}else if( ic_cedula==60 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", carteraTotal); 		}
						
						//Estimación de Reservas
						if(ic_cedula==39 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", estimacionReservas);
						}else if( ic_cedula==50  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", estimacionReservas);
						}else if( ic_cedula==61 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", estimacionReservas); 		}
						
						//Pasivos Bancarios
						if(ic_cedula==40 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", pasivosBancarios);
						}else if( ic_cedula==51  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", pasivosBancarios);
						}else if( ic_cedula==62 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", pasivosBancarios); 		}
						
						//Pasivos Totales
						if(ic_cedula==41 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", pasivosTotales);
						}else if( ic_cedula==52  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", pasivosTotales);
						}else if( ic_cedula==63 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", pasivosTotales); 		}
						
						//Capital Social
						if(ic_cedula==42 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", capitalSocial);
						}else if( ic_cedula==53  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", capitalSocial);
						}else if( ic_cedula==64 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", capitalSocial); 		}
												
						//Capital Contable
						if(ic_cedula==43 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", capitalContable);
						}else if( ic_cedula==54  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", capitalContable);
						}else if( ic_cedula==65 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", capitalContable); 		}
												
						//Utilidad Neta						
						if(ic_cedula==44 && anio.equals(anioFinaciero) ){ 				reg.setObject("RESULTADO", utilidadNeta);
						}else if( ic_cedula==55  && anio.equals(anioFinaciero) ){ 	reg.setObject("RESULTADO", utilidadNeta);
						}else if( ic_cedula==66 && anio.equals(anioFinaciero) ) { 	reg.setObject("RESULTADO", utilidadNeta); 		}
												
					}
				}	
				
			}	
	}
	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString(); 
	
}else  if (informacion.equals("descargaCedula")){ 
	
	List datoGrales = new ArrayList();
	datoGrales.add((String)session.getAttribute("strPais"));//0
	datoGrales.add("-"); //1
	datoGrales.add(strNombre); //2
	datoGrales.add(strNombreUsuario); //3
	datoGrales.add(strLogo); //4
	datoGrales.add(strDirectorioTemp); //5
	datoGrales.add(strDirectorioPublicacion); //6
	datoGrales.add(no_solicitud); //7
	datoGrales.add("");//8
	datoGrales.add(""); //9
	datoGrales.add("5"); //10
	datoGrales.add("ATENCION_SEGUIMIENTO"); //11 
	
	String nombreArchivo  = cediBean.generarArchivoCedula(datoGrales);	
	
	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 
	
	
}else  if (informacion.equals("descargaEdoResultados") || informacion.equals("descargacopiaRFC") || informacion.equals("descargaBalance") ){

	String nombreArchivo  =  cediBean.descargaArchivosSolic(no_solicitud, strDirectorioTemp, tipoArchivo );
   jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 
	
}else  if ( informacion.equals("descManualCredito") ||  informacion.equals("descPresenCorporativa")  
|| informacion.equals("descPasivosBancarios") || informacion.equals("descCarteraVencida") || informacion.equals("descPortafolioCartera")  )  { 

	String nombreArchivo  = cediBean.desArchSolicCheckList( no_solicitud,  strDirectorioTemp,  ic_doc_chekList,  extension );
	
   jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 

}else  if (informacion.equals("ViabilidadPDF")){ 

	try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}		

	infoRegresar = jsonObj.toString();
	
}else  if (informacion.equals("GuardarViabilidad")){ 

	String ic_elemento_cedula[] = request.getParameterValues("ic_elemento_cedula");	
	String viabilidad[] = request.getParameterValues("viabilidad");	

	paginador.guardarDictamen( no_solicitud , ic_elemento_cedula, viabilidad );

	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("no_solicitud",no_solicitud);
	infoRegresar = jsonObj.toString(); 
	
}else  if (informacion.equals("ConfirmarCedula")){ 

	
	paginador.confirmarCedula( no_solicitud );

	jsonObj = new JSONObject();	
	jsonObj.put("success", new Boolean(true));	
	infoRegresar = jsonObj.toString(); 

}
   

%>
<%=infoRegresar%> 

