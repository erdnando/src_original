<!DOCTYPE html>
<%@ page import="java.util.*" 
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/39CediIntermediarioNB/39secsession.jspf" %>
<%@ include file="/39CediIntermediarioNB/certificado.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs4.jspf" %> 
<script type="text/javascript" src="39AsignaSolicitud.js?<%=session.getId()%>"></script> 

</head>

<%@ include file="/01principal/menu.jspf"%>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01nafin/cabeza.jspf"%>

<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
      <div id="areaContenido"></div>
   </div>
</div>

<%@ include file="/01principal/01nafin/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'>  </form>
<form id='formParametros' name="formParametros">  </form>

</body>
</html>