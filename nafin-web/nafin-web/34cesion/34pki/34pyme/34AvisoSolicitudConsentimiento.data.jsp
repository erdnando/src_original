<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		com.netro.exception.NafinException,
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar ="";
JSONArray registros = new JSONArray();

try {
	
	String clavePyme = (String)request.getSession().getAttribute("iNoCliente");

	//Instanciacion para el uso del EJB
	CesionEJB cesionEJB = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

	HashMap solicitudes = cesionEJB.consultaAvisoSolicitudConsentimientoPyme(clavePyme);

	HashMap solicitudAceptadaEpo = new HashMap();

	for (int i = 0; i <=solicitudes.size(); i++) {		
	
		HashMap solicitud = (HashMap)solicitudes.get("solicitudAceptadaEpo" + i);
		
		if(solicitud !=null) {
			solicitudAceptadaEpo = new HashMap();
			solicitudAceptadaEpo.put("NOMBREEPO",solicitud.get("nombreEpo"));
			solicitudAceptadaEpo.put("NOMBRECESIONARIO", solicitud.get("nombreCesionario"));
			//FODEA-024-2014 MOD()
			String cedente = (String)solicitud.get("nombreCedente");
			String empresas = (String)solicitud.get("empresas");
			empresas=empresas!=null?empresas:"";
			String cedenteLista = cedente+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):"");

			solicitudAceptadaEpo.put("NOMBRECEDENTE", cedenteLista);
			//
			solicitudAceptadaEpo.put("RFCCENDENTE", solicitud.get("rfcCendente"));
			solicitudAceptadaEpo.put("NUMEROPROVEEDOR", solicitud.get("numeroProveedor"));
			solicitudAceptadaEpo.put("FECHASOLICITUD", solicitud.get("fechaSolicitud"));
			solicitudAceptadaEpo.put("NUMEROCONTRATO", solicitud.get("numeroContrato"));
			solicitudAceptadaEpo.put("MONTOSPORMONEDA", solicitud.get("montosPorMoneda"));
			solicitudAceptadaEpo.put("FIRMA_CONTRATO", solicitud.get("firma_contrato"));
			solicitudAceptadaEpo.put("VENCE_PRORROGA", solicitud.get("vence_prorroga"));
			registros.add(solicitudAceptadaEpo);	
		}						
	}
		
	
	infoRegresar = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
	
} catch(Exception e) {
		e.printStackTrace();
		
} finally {
	
}
	

%>
<%=infoRegresar%>