Ext.onReady(function() {

	//var multiUsrPyme1;
	var paramSolConsEpo1;	
	var solicAutoXUsuario1;
	var numSolAut1;		
	var numSolEnv1;
	var soliEnviadasPorUsuario1;
	var csPrincipalAdmPyme;
	var clavePyme;
	
	function procesaVerificaParamGrupoCesion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			clavePyme = resp.clavePyme;
			if(resp.strPerfil=='ADMIN PYME'){
				csPrincipalAdmPyme = true;
			}else{
				csPrincipalAdmPyme = false;
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Elimina la Solicitud 
	var procesarSuccessFailureBorrar =  function(opts, success, response) {		
		var grid = Ext.getCmp('grid');
		grid.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			grid.getStore().commitChanges();
			Ext.MessageBox.alert('Mensaje','Solicitud borrada exitosamente.');
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					operacion: 'Generar', 								
						start: 0,
						limit: 15
					})
				});						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
		//Elimina la Solicitud 
	var procesaAccionBorrar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		var grid = Ext.getCmp('grid');
		var store = grid.getStore();
		
			grid.el.mask('Actualizando...', 'x-mask-loading');
			grid.stopEditing();
			
			Ext.Ajax.request({
				url : '34ConsultaSolicitudConsentimiento.data.jsp',
				params : {
					informacion: 'BORRAR',
					clave_solicitud:clave_solicitud							
				},
				callback: procesarSuccessFailureBorrar
			});				
	}

//redireccionar a la pagina de Modificar 
	var procesaAccionModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		document.location.href  = "34ModificaSolicitudConsentimientoext.jsp?clave_solicitud="+clave_solicitud;									
	}


//GENERAR ARCHIVO TODO PDF ACUSE
	var procesarSuccessFailureGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		btnGenerarPDFAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			btnBajarPDFAcuse.show();
			btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
// Procesa la generaccion del Acuse 
	function procesarSuccessFailureAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			if (jsondeAcuse != null){
				var acuse= jsondeAcuse.acuse;
				var clave_solicitud= jsondeAcuse.clave_solicitud;	
				var mostrarError= jsondeAcuse.mostrarError;	
			}	
			
			var btnGenerarAc = Ext.getCmp('btnGenerarPDFAcuse');
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');			
			if(mostrarError==''){				
				btnGenerarAc.enable();
				btnBajarPDFAcuse.hide();
			}else{
				btnGenerarAc.disable();
				btnBajarPDFAcuse.hide();
			}
			
			var bodyPanel = Ext.getCmp('AcuseFirma').body;
			var mgr = bodyPanel.getUpdater();		
			mgr.update({
				url: '34Solicitud_AcuseExt.jsp',	
				scripts: true,
				params: {
					clave_solicitud:clave_solicitud,
					acuse:acuse
				},
				indicatorText: 'Cargando Acuse de Contrato de Cesi�n'
			});	
		
		consultaData.load({
			params: Ext.apply(fp.getForm().getValues(),{
				operacion: 'Generar',
				start: 0,
				limit: 15
				
			})
		});	
			
			var btnGenerarAc = Ext.getCmp('btnGenerarPDFAcuse');
			btnGenerarAc.setHandler(
				function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');								
					Ext.Ajax.request({
						url: '34Solicitud_Pre_AcusePDF.jsp',
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion:'ACUSE',
							acuse_carga: acuse,	
							clave_solicitud: clave_solicitud	
						}),
						callback: procesarSuccessFailureGenerarPDFAcuse
					});
				}
			);
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		//Procesa la Generacion del Acuse 
	var procesaAcuse = function(pkcs7, textoFirmado, clave_solicitud, claveNueva, accion) {
	
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
			
		}else  {			
				
			var verPreAcuse2 = Ext.getCmp('verPreAcuse');
			if (verPreAcuse2) {		
				verPreAcuse2.destroy();		
			} 
			var ventana = Ext.getCmp('verAcuse');	
			if (ventana) {				
				ventana.show();	
			} else {	
				new Ext.Window({
					layout: 'fit',
					width: 800,
					height: 305,			
					id: 'verAcuse',
					closeAction: 'hide',
					items: [					
						AcuseFirma
					],
					title: 'Acuse Solicitud de Consentimiento',
					bbar: {
					xtype: 'toolbar',
					buttons: [	
						'->',
						'-',						
						{
							xtype: 'button',
							text: 'Generar PDF',
							id: 'btnGenerarPDFAcuse'											
						},
						'-',
						{
							xtype: 'button',
							text: 'Bajar PDF',
							id: 'btnBajarPDFAcuse',
							hidden: true
						}					
					]
				}			
				}).show();
			}
			
			var bodyPanel = Ext.getCmp('AcuseFirma').body;
			var mgr = bodyPanel.getUpdater();
			mgr.on('failure', 
				function(el, response) {
					bodyPanel.update('');
					NE.util.mostrarErrorResponse(response);
				}
			);		
				
			Ext.Ajax.request({
				url : '34ConsultaSolicitudConsentimiento.data.jsp',
				params : {
					informacion: accion,
					pkcs7: pkcs7,
					textoFirmado: textoFirmado,
					clave_solicitud:clave_solicitud,
					claveNueva:claveNueva					
				},
				callback: procesarSuccessFailureAcuse
			});	
			
		}
	}
	
	//  Enviar 
	var procesaAccionEnviar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var claveGrupoCesion = registro.get('IC_GRUPO_CESION');
		var multiUsrPyme1 = 'N';
		if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme1 = 'S';
		
		var claveNueva;
		if(multiUsrPyme1 =='S'  && paramSolConsEpo1 =='S'  && registro.get('CLAVE_ESTATUS') ==1){
			 claveNueva = "13";
		}	else if(multiUsrPyme1 =='N' &&  paramSolConsEpo1 =='S'  &&  registro.get('CLAVE_ESTATUS')==1) {
			 claveNueva = "2";
		}				
		
		var ventana = Ext.getCmp('verPreAcuse');		 
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 800,
				height: 305,			
				id: 'verPreAcuse',
				closeAction: 'hide',
				items: [					
					PreAcuse
				],
				title: 'Preacuse de Solicitud de Consentimiento',
				bbar: {
				xtype: 'toolbar',
				buttons: [	
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Enviar Solicitud',
						id: 'btnEnviar'											
					}									
				]
			}			
			}).show();
			
		}
		
		var bodyPanel = Ext.getCmp('PreAcuse').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure', 
			function(el, response) {
				x.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		mgr.update({
			url: '34Solicitud_Pre_AcuseExt.jsp',	
			scripts: true,
			params: {
				clave_solicitud: clave_solicitud,
				tipoPreAcuse: 'Consentimiento',
				claveGrupo: claveGrupoCesion
			},
			indicatorText: 'Cargando Preacuse de Solicitud de Consentimiento'
		});	
		var textoFirmado = " Dependencia | Intermediario Financiero(Cesionario)|PYME(Cedente)|No. de Contrato| Monto / Moneda| "+
		"Tipo de Contrataci�n| Fecha Inicio Contrato| Fecha Final Contrato| "+
		"Plazo del Contrato|clasificacionEpo |Ventanilla de Pago| "+
		" Supervisor/Administrador/Residente de Obra| Tel�fono| "+
		" Objeto del Contrato| Estatus|\n";
			
		textoFirmado += 	registro.get('DEPENDENCIA') + "|" +
			registro.get('NOMBRE_CESIONARIO') + "|" +
			registro.get('NOMBRE_CEDENTE') + "|" +
			registro.get('NUMERO_CONTRATO') + "|" +
			registro.get('TIPO_CONTRATACION') + "|" +
			registro.get('FECHA_INICIO_CONTRATO') + "|" +
			registro.get('FECHA_FIN_CONTRATO') + "|" +
			registro.get('PLAZO_CONTRATO') + "|" +
			registro.get('CLASIFICACION_EPO') + "|" +
			registro.get('CAMPO_ADICIONAL_1') + "|" +
			registro.get('CAMPO_ADICIONAL_2') + "|" +
			registro.get('CAMPO_ADICIONAL_3') + "|" +
			registro.get('CAMPO_ADICIONAL_4') + "|" +
			registro.get('CAMPO_ADICIONAL_5') + "|" +
			registro.get('VENANILLA_PAGO') + "|" +
			registro.get('SUP_ADM_RESOB') + "|" +
			registro.get('NUMERO_TELEFONO') + "|" +
			registro.get('OBJETO_CONTRATO') + "|" +
			registro.get('COMENTARIOS') + "|" +
			registro.get('ESTATUS_SOLICITUD') + "|" +
			registro.get('CAUSAS_RECHAZO') + "|" +
			registro.get('NUMERO_TELEFONO') + "|" +
			registro.get('NUMERO_TELEFONO') + "|" +"\n";			
		var  accion  ="ENVIAR";
		///para la Firma 			
		var btnEnviar = Ext.getCmp('btnEnviar');
		btnEnviar.setHandler(
			function(boton, evento) {	
				
				NE.util.obtenerPKCS7(procesaAcuse,textoFirmado, clave_solicitud, claveNueva, accion);
				
				
			}	);		
	}

	//Aceptar
	var procesaAccionAceptar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var claveGrupoCesion = registro.get('IC_GRUPO_CESION');
		var claveNueva;
		var recordFound =false;
		var enviosPendientes = Number(registro.get('NUM_REP')) - Number(registro.get('NUM_ENVIO_REP'));
		var multiUsrPyme1 = 'N';	
		if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme1 = 'S';
		if (multiUsrPyme1 =='S') {
			if (paramSolConsEpo1 =='S' ) {
				if (registro.get('CLAVE_ESTATUS') == '13') {					
					for( var i =0; i<numSolEnv1; i++){																
						var enviadasPorUsuario = eval('soliEnviadasPorUsuario1.numeroSolicitud'+i);										
						if(enviadasPorUsuario ==registro.get('CLAVE_SOLICITUD')){
							recordFound = true;
						}
					}
					if(recordFound ==false && enviosPendientes==1 ){
						claveNueva ="2";			
					}else{
						claveNueva ="13";
					}
				}
			}
		}		
		var ventana = Ext.getCmp('verPreAcuse');		 
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 800,
				height: 305,			
				id: 'verPreAcuse',
				closeAction: 'hide',
				items: [					
					PreAcuse
				],
				title: 'Preacuse de Solicitud de Consentimiento',
				bbar: {
				xtype: 'toolbar',
				buttons: [	
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Enviar Solicitud',
						id: 'btnEnviar'											
					}									
				]
			}			
			}).show();			
		}
		
		var bodyPanel = Ext.getCmp('PreAcuse').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure', 
			function(el, response) {
				bodyPanel.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		mgr.update({
			//url: '34ContratoCesionPreacuseExt.jsp',	
			url: '34Solicitud_Pre_AcuseExt.jsp',
			scripts: true,
			params: {
				clave_solicitud: clave_solicitud,
				tipoPreAcuse: 'Consentimiento',
				claveGrupo: claveGrupoCesion
			},
			indicatorText: 'Cargando Autorizaci�n  de Solicitud de Consentimiento'
		});	
		
		var textoFirmado = " Dependencia | Intermediario Financiero(Cesionario)|PYME(Cedente)|No. de Contrato| Monto / Moneda| "+
		"Tipo de Contrataci�n| Fecha Inicio Contrato| Fecha Final Contrato| "+
		"Plazo del Contrato|clasificacionEpo |Ventanilla de Pago| "+
		" Supervisor/Administrador/Residente de Obra| Tel�fono| "+
		" Objeto del Contrato| Estatus|\n";
				
			
		textoFirmado += 	registro.get('DEPENDENCIA') + "|" +
					registro.get('NOMBRE_CESIONARIO') + "|" +
					registro.get('NOMBRE_CEDENTE') + "|" +
					registro.get('NUMERO_CONTRATO') + "|" +
					registro.get('TIPO_CONTRATACION') + "|" +
					registro.get('FECHA_INICIO_CONTRATO') + "|" +
					registro.get('FECHA_FIN_CONTRATO') + "|" +
					registro.get('PLAZO_CONTRATO') + "|" +
					registro.get('CLASIFICACION_EPO') + "|" +
					registro.get('CAMPO_ADICIONAL_1') + "|" +
					registro.get('CAMPO_ADICIONAL_2') + "|" +
					registro.get('CAMPO_ADICIONAL_3') + "|" +
					registro.get('CAMPO_ADICIONAL_4') + "|" +
					registro.get('CAMPO_ADICIONAL_5') + "|" +
					registro.get('VENANILLA_PAGO') + "|" +
					registro.get('SUP_ADM_RESOB') + "|" +
					registro.get('NUMERO_TELEFONO') + "|" +
					registro.get('OBJETO_CONTRATO') + "|" +
					registro.get('COMENTARIOS') + "|" +
					registro.get('ESTATUS_SOLICITUD') + "|" +
					registro.get('CAUSAS_RECHAZO') + "|" +
					registro.get('NUMERO_TELEFONO') + "|" +
					registro.get('NUMERO_TELEFONO') + "|" +"\n";	
					
			var  accion  ="ACEPTAR";
	///para la Firma 			
		var btnEnviar = Ext.getCmp('btnEnviar');
		btnEnviar.setHandler(
			function(boton, evento) {	
				
				NE.util.obtenerPKCS7(procesaAcuse,textoFirmado, clave_solicitud, claveNueva, accion);				
				
			}	);
		
	}
	
 var PreAcuse = {
	xtype: 'panel',
	id: 'PreAcuse',
	width: 600,
	height: 'auto',
	hidden: false,
	align: 'center',
	autoScroll: true
};


var AcuseFirma = new Ext.Panel({
	id: 'AcuseFirma',
	width: 600,
	height: 'auto',
	hidden: false,
	align: 'center',
	autoScroll: true
});

// Imprimir archivo acuse  de  Autorizar Solicitud 
 var procesarSuccessFailureGenerarPDFAcuseAu =  function(opts, success, response) {
		var btnGenerarPDFAcuseAuto = Ext.getCmp('btnGenerarPDFAcuseAuto');
		btnGenerarPDFAcuseAuto.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuseAuto = Ext.getCmp('btnBajarPDFAcuseAuto');
			btnBajarPDFAcuseAuto.show();
			btnBajarPDFAcuseAuto.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFAcuseAuto.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPDFAcuseAuto.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//procesar Acuse de  Autorizar Solicitud 
	function procesarSuccessFailureAcuseAuto(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			if (jsondeAcuse != null){
				var acuse= jsondeAcuse.acuse;
				var clave_solicitudAcuse= jsondeAcuse.clave_solicitud;						
			}		
					
			var btnGenerarPDFAcuseAuto = Ext.getCmp('btnGenerarPDFAcuseAuto');
			var btnBajarPDFAcuseAuto = Ext.getCmp('btnBajarPDFAcuseAuto');
			if(acuse!=''){				
				btnGenerarPDFAcuseAuto.enable();
				btnBajarPDFAcuseAuto.hide();
			}else{
				btnGenerarPDFAcuseAuto.disable();
				btnBajarPDFAcuseAuto.hide();
			}
			
			var bodyPanel = Ext.getCmp('AcuseAutorizado').body;
			var mgr = bodyPanel.getUpdater();		
			mgr.update({
				url: '34ContratoCesionAcuseExt.jsp',	
				scripts: true,
				params: {
					clave_solicitud:clave_solicitudAcuse,
					acuse:acuse
				},
				indicatorText: 'Cargando Acuse de Contrato de Cesi�n'
			});	
		
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					operacion: 'Generar',
					start: 0,
					limit: 15
				})
			});	
		
			var btnGenerarPDFAcuseAuto = Ext.getCmp('btnGenerarPDFAcuseAuto');
			btnGenerarPDFAcuseAuto.setHandler(
				function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');								
					Ext.Ajax.request({
						url: '34ContratoCesionPDFCSV.jsp',
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion:'ACUSE',
							acuse_carga: acuse,	
							clave_solicitud: clave_solicitudAcuse	
						}),
						callback: procesarSuccessFailureGenerarPDFAcuseAu
					});
				}
			);
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
var procesaAutorizarPreacuse = function(pkcs7, textoFirmado, clave_solicitud, claveNueva) {
		var verPreAcuseAu2 = Ext.getCmp('verPreAcuseAu');
		if (verPreAcuseAu2) {		
			verPreAcuseAu2.destroy();		
		} 
		var ventana = Ext.getCmp('verAcuseAuto');	
			if (ventana) {				
				ventana.show();	
			} else {	
				new Ext.Window({
					layout: 'fit',
					width: 800,
					height: 305,			
					id: 'verAcuseAuto',
					closeAction: 'hide',
					items: [					
						AcuseAutorizado
					],
					title: 'Acuse de Contrato de Cesi�n',
					bbar: {
					xtype: 'toolbar',
					buttons: [	
						'->',
						'-',						
						{
							xtype: 'button',
							text: 'Generar PDF',
							id: 'btnGenerarPDFAcuseAuto'											
						},
						'-',
						{
							xtype: 'button',
							text: 'Bajar PDF',
							id: 'btnBajarPDFAcuseAuto',
							hidden: true
						}					
					]
				}			
				}).show();
			}
			
			var bodyPanel = Ext.getCmp('AcuseAutorizado').body;
			var mgr = bodyPanel.getUpdater();
			mgr.on('failure', 
				function(el, response) {
					x.update('');
					NE.util.mostrarErrorResponse(response);
				}
			);		
				
			Ext.Ajax.request({
				url : '34ContratoCesionext.data.jsp',
				params : {
					informacion: 'GeneraAcuse',
					pkcs7: pkcs7,
					textoFirmado: textoFirmado,
					clave_solicitud:clave_solicitud,
					claveNueva:claveNueva					
				},
				callback: procesarSuccessFailureAcuseAuto
			});		
	}
	
	
		//procesar Accion de  Autorizar Solicitud 
	var procesaAccionAutorizar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');	
		var claveNueva;
		var multiUsrPyme1 = 'N';	
		if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme1 = 'S';
		if (multiUsrPyme1 =='S') {
			if (paramSolConsEpo1 =='N') {
				if(registro.get('CLAVE_ESTATUS') ==1){	
					claveNueva = "14"; 
				}else if(registro.get('CLAVE_ESTATUS') ==14){	
					var recordFound = false;
					for( var i =0; i<numSolAut1; i++){
						var datos = 'numeroSolicitud'+i;
						var datoAutoXUsuario = eval('solicAutoXUsuario1.numeroSolicitud'+i);											
						if(datoAutoXUsuario ==registro.get('CLAVE_SOLICITUD')){
							recordFound = true;
						}												
						if (recordFound== false) {
							claveNueva = "7"; 
						}												
					}										
				}
			}
		}else if (multiUsrPyme1 =='N') {
			if (paramSolConsEpo1 =='S') {
				if(registro.get('CLAVE_ESTATUS') ==3){	
					claveNueva = "7"; 
				}
			}else if (paramSolConsEpo1 =='N') {
				if(registro.get('CLAVE_ESTATUS') ==1){	
					claveNueva = "7"; 
				}							
			}
		}
		
		var ventana = Ext.getCmp('verPreAcuseAu');		 
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 800,
				height: 305,			
				id: 'verPreAcuseAu',
				closeAction: 'hide',
				items: [					
					AutorizarPreAcuse
				],
				title: 'Preacuse de Solicitud de Consentimiento',
				bbar: {
				xtype: 'toolbar',
				buttons: [	
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Autorizar',
						id: 'btnAutorizar'											
					}									
				]
			}			
			}).show();			
		}
		
		var bodyPanel = Ext.getCmp('AutorizarPreAcuse').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure', 
			function(el, response) {
				x.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		mgr.update({
			url: '34ContratoCesionPreacuseExt.jsp',	
			scripts: true,
			params: {
				clave_solicitud: clave_solicitud			
			},
			indicatorText: 'Cargando Autorizaci�n  de Solicitud de Consentimiento'
		});	
			
		
		var textoFirmado = " Dependencia | Intermediario Financiero(Cesionario)|No. de Contrato| Monto / Moneda| "+
		"Tipo de Contrataci�n| Fecha Inicio Contrato| Fecha Final Contrato| "+
		"Plazo del Contrato|clasificacionEpo |Ventanilla de Pago| "+
		" Supervisor/Administrador/Residente de Obra| Tel�fono| "+
		" Objeto del Contrato| Estatus|\n";
				
			
		textoFirmado += 	registro.get('DEPENDENCIA') + "|" +
					registro.get('NOMBRE_CESIONARIO') + "|" +
					registro.get('NUMERO_CONTRATO') + "|" +
					registro.get('TIPO_CONTRATACION') + "|" +
					registro.get('FECHA_INICIO_CONTRATO') + "|" +
					registro.get('FECHA_FIN_CONTRATO') + "|" +
					registro.get('PLAZO_CONTRATO') + "|" +
					registro.get('CLASIFICACION_EPO') + "|" +
					registro.get('CAMPO_ADICIONAL_1') + "|" +
					registro.get('CAMPO_ADICIONAL_2') + "|" +
					registro.get('CAMPO_ADICIONAL_3') + "|" +
					registro.get('CAMPO_ADICIONAL_4') + "|" +
					registro.get('CAMPO_ADICIONAL_5') + "|" +
					registro.get('VENANILLA_PAGO') + "|" +
					registro.get('SUP_ADM_RESOB') + "|" +
					registro.get('NUMERO_TELEFONO') + "|" +
					registro.get('OBJETO_CONTRATO') + "|" +
					registro.get('COMENTARIOS') + "|" +
					registro.get('ESTATUS_SOLICITUD') + "|" +
					registro.get('CAUSAS_RECHAZO') + "|" +
					registro.get('NUMERO_TELEFONO') + "|" +
					registro.get('NUMERO_TELEFONO') + "|" +"\n";			  	
			
	///para la Firma 			
		var btnAutorizar = Ext.getCmp('btnAutorizar');
		btnAutorizar.setHandler(
			function(boton, evento) {	
				var pkcs7 = NE.util.firmar(textoFirmado);
				if (Ext.isEmpty(pkcs7)) {
					return;	//Error en la firma. Termina...
				}			
				procesaAutorizarPreacuse(pkcs7, textoFirmado, clave_solicitud, claveNueva);
			}	);
		
	}

 var AutorizarPreAcuse = {
	xtype: 'panel',
	id: 'AutorizarPreAcuse',
	width: 600,
	height: 'auto',
	hidden: false,
	align: 'center',
	autoScroll: true
};

var AcuseAutorizado = new Ext.Panel({
	id: 'AcuseAutorizado',
	width: 600,
	height: 'auto',
	hidden: false,
	align: 'center',
	autoScroll: true
});


	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//GENERAR ARCHIVO TODO CSV
	var procesarSuccessFailureCSV =  function(opts, success, response) {
		var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');
		btnImprimirCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//muestra el archivo de la Columna Contrato 
	var procesarSuccessFailureContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
		var descargaArchivoContrato = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
			url: '34ConsultaSolicitudConsentimiento.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContrato
		});
	}
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var jsonData = store.reader.jsonData;	
		var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
		var  clasificacionEpo = jsonData.clasificacionEpo;	
		var jsonData = store.reader.jsonData;
		//multiUsrPyme1 = jsonData.multiUsrPyme;	
		paramSolConsEpo1 = jsonData.paramSolConsEpo;	
		solicAutoXUsuario1 = jsonData.solicitudesAutorizadasPorUsuario
		soliEnviadasPorUsuario1 = jsonData.solicitudesEnviadasPorUsuario;
		numSolAut1 = jsonData.numSolAut;
		numSolEnv1 = jsonData.numSolEnv;				
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}			
		
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = grid.getColumnModel();
			
			if(clasificacionEpo ==''){		
				grid.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), true);					
			}else{
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
			}
					

			if(hayCamposAdicionales=='0'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='1'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)				
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
			}
			if(hayCamposAdicionales=='2'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
			}
			if(hayCamposAdicionales=='3'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
			}
			if(hayCamposAdicionales=='4'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.Campo3);
			}
				
			if(hayCamposAdicionales=='5'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), false);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.Campo3);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_5'),jsonData.Campo4);
			}
			
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');	
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			
			
			var el = grid.getGridEl();						
				if(store.getTotalCount() > 0) {
					 var cedente = '';
					if(store.getTotalCount()== 1){
						cedente = jsonData.registros[0].NOMBRE_CEDENTE;
					}
					
				btnImprimirPDF.enable();
				btnImprimirCSV.enable();					
				btnBajarPDF.hide();
				btnBajarCSV.hide();
				
				
				//para mostrar  panel de Nota 
				/*
				var bodyPanel = Ext.getCmp('Nota').body;
				Nota.show();
				var mgr = bodyPanel.getUpdater();
				mgr.on('failure', 
					function(el, response) {
						bodyPanel.update('');
						NE.util.mostrarErrorResponse(response);
					}
				);		
				mgr.update({
					url: '34ConsultaSolicitudConsentimientoNotaExt.jsp',	
					scripts: true,
					params: {
						noEPO: '1',
						nombreEpo: Ext.getCmp('clave_epo2').lastSelectionText,
						cedente : cedente
					},
					indicatorText: 'Cargando Nota'
				});	*/
						
				el.unmask();
			} else {	
				
				btnImprimirPDF.disable();
				btnImprimirCSV.disable();
				btnBajarPDF.hide();
				btnBajarCSV.hide();
				
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
		}
	}
	
	var mostrarAutorizacionConsentimiento = function(grid, rowIndex, colIndex, item, event) {
		//var registro = grid.getStore().getAt(rowIndex);
		//var clave_solicitud = registro.get('CLAVE_SOLICITUD');
	
		var registro = grid.getStore().getAt(rowIndex);
		var ic_solicitud = registro.get('CLAVE_SOLICITUD');        
		var win = new NE.AutoConsorcio.VisorConsorcio(ic_solicitud, 'Autorizacion_Consorcio','Solicitud Consentimiento' );
	}	
//-------------		
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ConsultaSolicitudConsentimiento.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ConsultaSolicitudConsentimiento.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ConsultaSolicitudConsentimiento.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
		var catalogoContratacionData = new Ext.data.JsonStore({
		id: 'catalogoContratacionDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ConsultaSolicitudConsentimiento.data.jsp',
		baseParams: {
			informacion: 'CatalogoContratacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoTipoPlazoData = new Ext.data.JsonStore({
		id: 'catalogoTipoPlazonDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ConsultaSolicitudConsentimiento.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
		
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34ConsultaSolicitudConsentimiento.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
			fields: [
					{name: 'DEPENDENCIA'},
					{name: 'NOMBRE_CESIONARIO'},
					{name: 'NUMERO_CONTRATO'},
					{name: 'TIPO_CONTRATACION'},
					{name: 'FECHA_INICIO_CONTRATO'},
					{name: 'FECHA_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION_EPO'},
					{name: 'CAMPO_ADICIONAL_1'},
					{name: 'CAMPO_ADICIONAL_2'},
					{name: 'CAMPO_ADICIONAL_3'},
					{name: 'CAMPO_ADICIONAL_4'},
					{name: 'CAMPO_ADICIONAL_5'},
					{name: 'VENANILLA_PAGO'},
					{name: 'SUP_ADM_RESOB'},
					{name: 'NUMERO_TELEFONO'},
					{name: 'OBJETO_CONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'ESTATUS_SOLICITUD'},
					{name: 'CAUSAS_RECHAZO'},
					{name: 'CLAVE_ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'FIRMA_CONTRATO'},
					{name: 'IC_GRUPO_CESION'},
					{name: 'NUM_REP'},
					{name: 'NUM_ENVIO_REP'},
					{name: 'PYME_PRINCIPAL'},
					{name: 'NOMBRE_CEDENTE'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,	
				beforeLoad:	{
					fn: function(store, options){
						Ext.apply(options.params, {						
							clave_epo: Ext.getCmp("clave_epo2").getValue()
						});
					}	
				},
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
		
		var grid = new Ext.grid.EditorGridPanel({
		id: 'grid',
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		title: 'Consulta Solicitud de Consentimiento',
		columns: [
			{
				header: 'Dependencia', 
				tooltip: 'Dependencia',
				dataIndex: 'DEPENDENCIA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Intermediario Financiero(Cesionario)',
				tooltip: 'Intermediario Financiero(Cesionario)',
				dataIndex: 'NOMBRE_CESIONARIO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},				
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
	       xtype: 'actioncolumn',
				 header: 'Monto / Moneda',
				 tooltip: 'Monto / Moneda',
				 dataIndex: 'CLAVE_SOLICITUD',
         width: 130,
				 align: 'left',
				 renderer: function(value, metadata, record, rowindex, colindex, store) {
				 	var jsonData = store.reader.jsonData;	
					var valor = record.get('CLAVE_SOLICITUD');
					var Xmoneda = eval('jsonData.moneda'+valor);	
					return Xmoneda;									
			}
			}	,						
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Fecha Inicio Contrato',
				tooltip: 'Fecha Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
				
			},
			{
				header: 'Fecha Final Contrato',
				tooltip: 'Fecha Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},
			{
				header: 'Plazo del Contrato',
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			}, 
			{
				header: 'CLASIFICACION_EPO',
				tooltip: 'CLASIFICACION_EPO',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'CAMPO 1',
				tooltip: 'CAMPO 1',
				dataIndex: 'CAMPO_ADICIONAL_1',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},
			{
				header: 'CAMPO 2',
				tooltip: 'CAMPO 2',
				dataIndex: 'CAMPO_ADICIONAL_2',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'CAMPO 3',
				tooltip: 'CAMPO 3',
				dataIndex: 'CAMPO_ADICIONAL_3',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'CAMPO 4',
				tooltip: 'CAMPO 4',
				dataIndex: 'CAMPO_ADICIONAL_4',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},
			{
				header: 'CAMPO 5',
				tooltip: 'CAMPO 5',
				dataIndex: 'CAMPO_ADICIONAL_5',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},
			{
				header: 'Ventanilla de Pago',
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENANILLA_PAGO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			},			
			{
				header: 'Supervisor/Administrador/Residente de Obra de Pago',
				tooltip: 'Supervisor/Administrador/Residente de Obra de Pago',
				dataIndex: 'SUP_ADM_RESOB',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'				
			},
			{
				header: 'Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex: 'NUMERO_TELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			},
			{
				header: 'Objeto del Contrato',
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			},
			{
				header: 'Comentarios',
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS_SOLICITUD',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			}	,
			{
				header: 'Causa del Rechazo',
				tooltip: 'Causa del Rechazo',
				dataIndex: 'CAUSAS_RECHAZO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},	
			{
	      xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	handler: descargaArchivoContrato
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Autorizaci�n Consentimiento',
				tooltip: 'Contrato',
				width: 150,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					var claveEstatus =registro.get('CLAVE_ESTATUS');
					if(claveEstatus==5){
						return 'N/A';
					}else{
						return '';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							var claveEstatus =registro.get('CLAVE_ESTATUS');
							if(claveEstatus!=5){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else{
								return '';
							}
						},
						handler: mostrarAutorizacionConsentimiento
					}
				]				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',
        width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					var jsonData = store.reader.jsonData;	
					var multiUsrPyme = 'N';
					if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S';
					//var multiUsrPyme = jsonData.multiUsrPyme;	
					var paramSolConsEpo = jsonData.paramSolConsEpo;	
					var solicAutoXUsuario = jsonData.solicitudesAutorizadasPorUsuario;
					var numSolAut = jsonData.numSolAut;	
					var mostrarNA ="";
					var soliEnviadasPorUsuario = jsonData.solicitudesEnviadasPorUsuario;
					var numSolEnv = jsonData.numSolEnv;
					var claveEstatus =registro.get('CLAVE_ESTATUS');
					var recordFound =false;
					
					if(multiUsrPyme =='S'  && paramSolConsEpo =='S'  && registro.get('CLAVE_ESTATUS') ==1){
								mostrarNA = 'Enviar';							
					}	else if(multiUsrPyme =='N' &&  paramSolConsEpo =='S'  &&  registro.get('CLAVE_ESTATUS') ==1) {
						mostrarNA = 'Enviar';															
					}
			
					if(multiUsrPyme =='S'  && paramSolConsEpo =='S'  && registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){									
						mostrarNA = 'Modificar';
					}else if(multiUsrPyme =='S'  && paramSolConsEpo =='N'  && registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){
							mostrarNA = 'Modificar';
					}else if(multiUsrPyme =='N'  && paramSolConsEpo =='S'  && registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){
						mostrarNA = 'Modificar';
					}else if(multiUsrPyme =='N'  && paramSolConsEpo =='N'  && registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){
						mostrarNA = 'Modificar';
					}	
						
					if(multiUsrPyme =='S' ){
						if(paramSolConsEpo =='S'){
							if(registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='') ){
								mostrarNA == 'Borrar';
							}else if(registro.get('CLAVE_ESTATUS') ==13 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){
								mostrarNA = 'Borrar';
							}
						}else if(paramSolConsEpo =='N'){
							if(registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
								mostrarNA = 'Borrar';
							}else	if(registro.get('CLAVE_ESTATUS') ==14 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
									mostrarNA = 'Borrar';
							}	
						}
					}else if(multiUsrPyme =='N' ){
						if (paramSolConsEpo =='S') {
							if(registro.get('CLAVE_ESTATUS') ==1 && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
								mostrarNA = 'Borrar';
							}else if(registro.get('CLAVE_ESTATUS') ==3 && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
								mostrarNA = 'Borrar';
							}								
						}	if (paramSolConsEpo =='N') {
							if(registro.get('CLAVE_ESTATUS') ==1 && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
								mostrarNA ='Borrar';
							}							
						}
					}		
					
					if (multiUsrPyme =='S') {
						if (paramSolConsEpo =='S' ) {
							if (claveEstatus == '13') {
								for( var i =0; i<numSolEnv; i++){																
									var enviadasPorUsuario = eval('soliEnviadasPorUsuario.numeroSolicitud'+i);										
									if(enviadasPorUsuario ==registro.get('CLAVE_SOLICITUD')){
										recordFound = true;
									}		
								}
								if(recordFound ==false){									
									mostrarNA = 'Aceptar';								
								}		
							}
						}
					}
					if (multiUsrPyme =='S') {
						if (paramSolConsEpo =='N') {
							if(registro.get('CLAVE_ESTATUS') ==1){											
								mostrarNA = 'Autorizar';
							}else if(registro.get('CLAVE_ESTATUS') ==14){	
								var recordFound = false;
								for( var i =0; i<numSolAut; i++){
									var datos = 'numeroSolicitud'+i;
									var datoAutoXUsuario = eval('solicAutoXUsuario.numeroSolicitud'+i);											
									if(datoAutoXUsuario ==registro.get('CLAVE_SOLICITUD')){
										recordFound = true;
									}												
									if (recordFound== false) {
										mostrarNA =	'Autorizar';
									}												
								}										
							}
						}
					}else if (multiUsrPyme =='N') {
						if (paramSolConsEpo =='S') {
							if(registro.get('CLAVE_ESTATUS') ==3){	
								mostrarNA = 'Autorizar';									
							}
						}else if (paramSolConsEpo =='N') {
							if(registro.get('CLAVE_ESTATUS') ==1){	
								mostrarNA = 'Autorizar';
							}							
						}
					}
					
				if(mostrarNA !=""){  	mostrarNA =""; 	}else { mostrarNA ="N/A" }
				
				return mostrarNA;
				
				},
					
				items: [
						{ //para enviar 
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
								var jsonData = store.reader.jsonData;	
								//var multiUsrPyme = jsonData.multiUsrPyme;
								var multiUsrPyme = 'N';	
								if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S';
								var paramSolConsEpo = jsonData.paramSolConsEpo;			
								if(multiUsrPyme =='S'  && paramSolConsEpo =='S'  && registro.get('CLAVE_ESTATUS') ==1){
									this.items[0].tooltip = 'Enviar Consentimiento';
									return 'icoAceptar';											
								}	else if(multiUsrPyme =='N' &&  paramSolConsEpo =='S'  &&  registro.get('CLAVE_ESTATUS') ==1) {
									this.items[0].tooltip = 'Enviar Consentimiento';
									return 'icoAceptar';									
								}							
							},
							handler: procesaAccionEnviar					
						},
							// Modificar 
							{
								getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
									var jsonData = store.reader.jsonData;	
									//var multiUsrPyme = jsonData.multiUsrPyme;	
									var multiUsrPyme = 'N';	
									if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S';
									var paramSolConsEpo = jsonData.paramSolConsEpo;	
									var solicAutoXUsuario = jsonData.solicitudesAutorizadasPorUsuario;
									var numSolAut = jsonData.numSolAut;								
									
									if(multiUsrPyme =='S'  && paramSolConsEpo =='S'  && registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){									
										this.items[1].tooltip = 'Modificar Consentimiento';
										return 'modificar';									
									}else if(multiUsrPyme =='S'  && paramSolConsEpo =='N'  && registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){
										this.items[1].tooltip = 'Modificar Consentimiento';
										return 'modificar';									
									}else if(multiUsrPyme =='N'  && paramSolConsEpo =='S'  && registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){
										this.items[1].tooltip = 'Modificar Consentimiento';
										return 'modificar';									
									}else if(multiUsrPyme =='N'  && paramSolConsEpo =='N'  && registro.get('CLAVE_ESTATUS') ==1 && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){
										this.items[1].tooltip = 'Modificar Consentimiento';
										return 'modificar';									
									}	
								},
								handler: procesaAccionModificar	
							},			
						//para Borrar									
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
								var jsonData = store.reader.jsonData;	
								//var multiUsrPyme = jsonData.multiUsrPyme;
								var multiUsrPyme = 'N';	
								if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S';
								var paramSolConsEpo = jsonData.paramSolConsEpo;			
								if(multiUsrPyme =='S' ){
									if(paramSolConsEpo =='S'){
										if(registro.get('CLAVE_ESTATUS') ==1  && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){
											this.items[2].tooltip = 'Borrar Consentimiento';
											return 'borrar';
										}else if(registro.get('CLAVE_ESTATUS') ==13  && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){
											this.items[2].tooltip = 'Borrar Consentimiento';
											return 'borrar';
										}
									}else if(paramSolConsEpo =='N'){
										if(registro.get('CLAVE_ESTATUS') ==1  && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
											this.items[2].tooltip = 'Borrar Consentimiento';
											return 'borrar';	
										}else	if(registro.get('CLAVE_ESTATUS') ==14  && csPrincipalAdmPyme && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
											this.items[2].tooltip = 'Borrar Consentimiento';
											return 'borrar';	
										}	
									}
								}else if(multiUsrPyme =='N' ){
									if (paramSolConsEpo =='S') {
										if(registro.get('CLAVE_ESTATUS') ==1  && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
											this.items[2].tooltip = 'Borrar Consentimiento';
											return 'borrar';	
										}else if(registro.get('CLAVE_ESTATUS') ==3  && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
											this.items[2].tooltip = 'Borrar Consentimiento';
											return 'borrar';	
										}								
								}	if (paramSolConsEpo =='N') {
									if(registro.get('CLAVE_ESTATUS') ==1  && (registro.get('PYME_PRINCIPAL')==clavePyme || registro.get('PYME_PRINCIPAL')=='')){	
											this.items[2].tooltip = 'Borrar Consentimiento';
											return 'borrar ';	
									}							
								}
							}
						},
							handler: procesaAccionBorrar												
					},
					// Aceptar 
					{
					getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
					var jsonData = store.reader.jsonData;	
					//var multiUsrPyme = jsonData.multiUsrPyme;
					var multiUsrPyme = 'N';	
					if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S';
					var paramSolConsEpo = jsonData.paramSolConsEpo;	
					var soliEnviadasPorUsuario = jsonData.solicitudesEnviadasPorUsuario;
					var numSolEnv = jsonData.numSolEnv;
					var claveEstatus =registro.get('CLAVE_ESTATUS');						
					var recordFound =false;
					if (multiUsrPyme =='S') {
						if (paramSolConsEpo =='S' ) {
							if (claveEstatus == '13') {
								for( var i =0; i<numSolEnv; i++){																
									var enviadasPorUsuario = eval('soliEnviadasPorUsuario.numeroSolicitud'+i);										
									if(enviadasPorUsuario ==registro.get('CLAVE_SOLICITUD')){
										recordFound = true;
									}		
								}
								if(recordFound ==false){
									this.items[3].tooltip = 'Aceptar';
									return 'aceptar';								
								}
							}
						}
					}
				}
				,handler: procesaAccionAceptar	
				},		
				//Autorizar
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							var jsonData = store.reader.jsonData;	
							//var multiUsrPyme = jsonData.multiUsrPyme;
							var multiUsrPyme = 'N';	
							if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S';
							var paramSolConsEpo = jsonData.paramSolConsEpo;	
							var solicAutoXUsuario = jsonData.solicitudesAutorizadasPorUsuario;
							var numSolAut = jsonData.numSolAut;	
							if (multiUsrPyme =='S') {
								if (paramSolConsEpo =='N') {
									if(registro.get('CLAVE_ESTATUS') ==1){											
										this.items[4].tooltip = 'Autorizar';
										return 'autorizar';											
									}else if(registro.get('CLAVE_ESTATUS') ==14){	
										var recordFound = false;
										for( var i =0; i<numSolAut; i++){
											var datos = 'numeroSolicitud'+i;
											var datoAutoXUsuario = eval('solicAutoXUsuario.numeroSolicitud'+i);											
											if(datoAutoXUsuario ==registro.get('CLAVE_SOLICITUD')){
													recordFound = true;
												}												
												if (recordFound== false) {
													this.items[4].tooltip = 'Autorizar';
													return 'autorizar';
												}												
										}										
									}
								}
							}else if (multiUsrPyme =='N') {
								if (paramSolConsEpo =='S') {
									if(registro.get('CLAVE_ESTATUS') ==3){	
										this.items[4].tooltip = 'Autorizar';
										return 'autorizar';
									}
								}else if (paramSolConsEpo =='N') {
									if(registro.get('CLAVE_ESTATUS') ==1){	
										this.items[4].tooltip = 'Autorizar';
										return 'autorizar';
									}							
							}
						}
					}
					,handler: procesaAccionAutorizar	
					}					
				]				
			}		
			],
			stripeRows: true,
			loadMask: true,
			height: 400,
			width: 943,		
			frame: true,
			bbar: {		
				xtype: 'paging',
				pageSize: 15,
				buttonAlign: 'left',
				id: 'barraPaginacion',
				displayInfo: true,
				store: consultaData,				
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros.",
				items: [
					'->',	
					'-',
					{
						xtype: 'button',
						text: 'Generar PDF',
						id: 'btnImprimirPDF',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '34ConsultaSolicitudConsentimiento.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoPDF',
									tipo_archivo: 'PDF',
									nombreEpo: Ext.getCmp('clave_epo2').lastSelectionText
								}),
								callback: procesarSuccessFailurePDF
							});
						}
					},					
					{
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDF',
						hidden: true
					},
					'-',
					{
						xtype: 'button',
						text: 'Descargar Archivo',
						id: 'btnImprimirCSV',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '34ConsultaSolicitudConsentimiento.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoCSV',
									tipo_archivo: 'CSV'
								}),
								callback: procesarSuccessFailureCSV
							});
						}
					},					
					{
						xtype: 'button',
						text: 'Bajar Archivo',
						id: 'btnBajarCSV',
						hidden: true
					}
					]
			}
			
			});
		
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'clave_epo',
			id: 'clave_epo2',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_epo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var cmbIF = Ext.getCmp('clave_if2');
						cmbIF.setValue('');
						cmbIF.setDisabled(false);
						cmbIF.store.load({
								params: {
									clave_epo:combo.getValue()
								}
						});
					}
				}
			}			
		},
		{
			xtype: 'combo',
			name: 'clave_if',
			id: 'clave_if2',
			fieldLabel: 'Intermediario Financiero(Cesionario) (IF)',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_if',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoIFData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Contrato',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'numero_contrato',
					id: 'numero_contrato2',
					allowBlank: true,
					width: 100,
					maxLength: 25,
					msgTarget: 'side',					
					margins: '0 20 0 0'  
				}
			]
		},			
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Monto ',
			combineErrors: false,
			msgTarget: 'side',
				items: [
					{
						xtype: 'displayfield',
						value: 'Monto Definido',
						width: 100
					},						
					{
						xtype: 'checkbox',					
						name: 'csTipoMontoChkD',
						id: 'csTipoMontoChkD1',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side'	,					
						margins: '0 20 0 0'  
				},	
					{
						xtype: 'displayfield',
						value: 'Monto Min y Max',
						width: 100
					},					
					{
						xtype: 'checkbox',						
						name: 'csTipoMontoChkM',
						id: 'csTipoMontoChkM1',
						allowBlank: true,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						margins: '0 20 0 0'  
					}
				]
		}	,			
		{
			xtype: 'combo',
			name: 'claveMoneda',
			id: 'claveMoneda2',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveMoneda',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},		
		{
			xtype: 'combo',
			name: 'claveTipoContratacion',
			id: 'claveTipoContratacion2',
			fieldLabel: 'Tipo de Contrataci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveTipoContratacion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoContratacionData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vigencia de Contrato',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_vigencia_ini',
					id: 'fecha_vigencia_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_vigencia_fin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_vigencia_fin',
					id: 'fecha_vigencia_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_vigencia_ini',
					margins: '0 20 0 0'  
					}
				]
			}	,			
			
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo del Contrato',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'plazoContrato',
					id: 'plazoContrato',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0'  
				},			
				{
					xtype: 'combo',
					name: 'claveTipoPlazo',
					id: 'claveTipoPlazo2',
					fieldLabel: '',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'claveTipoPlazo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoTipoPlazoData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		}
		
		];	
		
		var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 885,
		title: 'Criterios de B�squeda.',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {		
					var clave_epo = Ext.getCmp("clave_epo2");
					if (Ext.isEmpty(clave_epo.getValue()) ) {
						clave_epo.markInvalid('El valor de la EPO es requerido.');
						return;
					}		
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', 								
								start: 0,
								limit: 15
							})
						});			
						
				var verPreAcuse1 = Ext.getCmp('verPreAcuse');
				if (verPreAcuse1) {		
					verPreAcuse1.destroy();		
				}
				
				var verAcuse1 = Ext.getCmp('verAcuse');
				if (verAcuse1) {		
					verAcuse1.destroy();		
				}
				
		
				var verPreAcuseAu1 = Ext.getCmp('verPreAcuseAu');
				if (verPreAcuseAu1) {		
					verPreAcuseAu1.destroy();		
				}
				
				var verAcuseAuto1 = Ext.getCmp('verAcuseAuto');
				if (verAcuseAuto1) {		
					verAcuseAuto1.destroy();		
				}
				
				
				
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34ConsultaSolicitudConsentimientoExt.jsp';
				}
			}
		]			
	});
	
	var Nota = new Ext.Panel({
	id: 'Nota',
	width: 949,
	height: '600',
	hidden: true,
	align: 'center'	
});

	var cBoton = new Ext.Container({
		layout: 'table',
		layoutConfig: {
			columns: 2
		},
		width:	'890',
		heigth:	'auto',
		items: [
			{
				xtype: 'button',
				text		: 'Captura',
				tooltip	: 'Captura',
				id			: 'Captura',		
				handler: function(){				
					document.location.href  = "34Solic_Consentimientoext.jsp";							
				}
			},	
			{
				xtype: 'button',
				text		: 'Consulta',
				tooltip	: 'Consulta',
				id			: 'Consulta',		
				handler: function(){				
				document.location.href  = "34ConsultaSolicitudConsentimientoExt.jsp";							
				}
			}
		]
	});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		items: [		
			//cBoton,
			NE.util.getEspaciador(20),
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			grid,
			NE.util.getEspaciador(20),
			//Nota,
			NE.util.getEspaciador(20)
		]
		});
	
	catalogoEPOData.load();	
	catalogoMonedaData.load();
	catalogoContratacionData.load();
	catalogoTipoPlazoData.load();
	
	Ext.Ajax.request({
		url: '34ConsultaSolicitudConsentimiento.data.jsp',
		params: {
			informacion: "verificaParamGrupoCesion"
		},
		callback: procesaVerificaParamGrupoCesion
	});
	
});