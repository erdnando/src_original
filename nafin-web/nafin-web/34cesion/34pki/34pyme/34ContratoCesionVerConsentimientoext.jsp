<%@ page 	import="
		java.util.*,
		java.sql.*,
		com.netro.cesion.*,
		netropology.utilerias.*,
		com.netro.exception.NafinException"
	errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
String clave_contratacion = (request.getParameter("clave_contratacion")!=null)?request.getParameter("clave_contratacion"):"";
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";

String nombre_epo = (request.getParameter("nombre_epo")!=null)?request.getParameter("nombre_epo"):"";
String objeto_contrato = (request.getParameter("objeto_contrato")!=null)?request.getParameter("objeto_contrato"):"";
String montospormoneda = (request.getParameter("montospormoneda")!=null)?request.getParameter("montospormoneda"):"";
String plazocontrato = (request.getParameter("plazocontrato")!=null)?request.getParameter("plazocontrato"):"";
String fechasolicitud = (request.getParameter("fechasolicitud")!=null)?request.getParameter("fechasolicitud"):"";
String numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String nombre_if = (request.getParameter("nombre_if")!=null)?request.getParameter("nombre_if"):"";
montospormoneda=montospormoneda.replaceAll("<br/>","\t");


String nombreUsuario ="", nombrePyme= "", empresas = "";
StringBuffer textoConsentimiento = new StringBuffer();

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

String banderaConsentimiento=cesionBean.getBanderaConsentimiento(clave_solicitud);

if (clave_solicitud!= null && !clave_solicitud.equals("")) {
	HashMap usuarios = cesionBean.consultaContratoCesionPyme(clave_solicitud); 

	if(usuarios.size()>1){
		if(usuarios.get("nombreUsrEpoAutCesion") !=null){
			nombreUsuario  =  usuarios.get("nombreUsrEpoAutCesion").toString();
		}
		nombrePyme  =  usuarios.get("nombre_pyme").toString();  
		empresas  =  usuarios.get("CG_EMPRESAS_REPRESENTADAS").toString();  
	}
}	

if(banderaConsentimiento==null||!banderaConsentimiento.equals("S")){

%>		

		<table width="750" border="0" cellspacing="1" cellpadding="1">
			<tr>
				<td align="center" width="300">&nbsp;</td>
				<td align="center"><img src="/nafin/00archivos/15cadenas/15archcadenas/logos/nafinsa.gif" border=0></td>
				<td align="center" width="300">&nbsp;</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td>
					<table align="center" style="text-align:justify"  width="900" readonly="readonly">
              <%textoConsentimiento.append( nombrePyme+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):"") + "<br/><br/>");%>
							<%textoConsentimiento.append("Presente<br/><br/>");%>
							<%textoConsentimiento.append("Consentimiento para Ceder los Derechos de Cobro respecto del Contrato No."+ numero_contrato +"<br/><br/>");%>
							
							<%textoConsentimiento.append(""+nombrePyme+((empresas!="")?", "+empresas.replaceAll(";",", "):"") + "<br/>");%>
							<%textoConsentimiento.append("Objeto: "+objeto_contrato + "<br/>");%>
							<%textoConsentimiento.append("Monto: "+montospormoneda + "<br/>");%>
							<%textoConsentimiento.append("Plazo: "+plazocontrato + "<br/>");%>
							
							<%textoConsentimiento.append("Fecha de Solicitud: "+fechasolicitud + "<br/><br/>");%>
                            
							<%textoConsentimiento.append(" En atención a su solicitud, se le informa que con base en la información recabada al interior de ésta Empresa Productiva Subsidiaria "+ nombre_epo+", se otorga el consentimiento para ceder los derechos de cobro respecto del Contrato de antecedentes,"+
							" con fundamento en: ");%>
							
							<%textoConsentimiento.append("<br/><br/> El Artículo 46 de la Ley de Adquisiciones, Arrendamientos y Servicios del Sector Público.  ");%>
							<%textoConsentimiento.append(" El Artículo 47 de la Ley de Obras Públicas y Servicios Relacionados con las Mismas.  ");%>
							<%textoConsentimiento.append(" Los Artículos 53, fracción XVII, de la Ley de Petróleos Mexicanos, 48 de su Reglamento, y 54 de las Disposiciones administrativas de contratación en materia de adquisiciones, arrendamientos, obras y servicios de las actividades sustantivas de carácter productivo de Petróleos Mexicanos y  Organismos Subsidiarios. ");%>
							<%textoConsentimiento.append(" El Artículo 36 de las Disposiciones Generales de Contratación para Petróleos Mexicanos y sus Empresas Producctivas Subsidiarias, según corresponda, así como en la Cláusulas relativas a la cesión de derechos Cobro del Contrato.");%>
							
							<%textoConsentimiento.append("<br/><br/> Es importante señalar que el Transitorio Octavo, Apartado A, fracción VII de la Ley de Petróleos Mexicanos publicada en el Diario Oficial de la Federación el 11 de agosto de 2014,");%>
							<%textoConsentimiento.append(" establece que las nuevas Empresas Productivas Subsidiarias se subrogan en todos los derechos y obligaciones de los Organismos Subsidiarios, según corresponda, anteriores y posteriores a la fecha de entrada en vigor de los Acuerdos de Creación que al efecto se expidan,");%>
							<%textoConsentimiento.append(" por lo que una vez que entraron en vigor dichos Acuerdos, las Empresas Productivas Subsidiarias se subrogaron en los derechos y obligaciones del Organismos Subsidiario que formalizó el contrato, incluyendo los convenios modificatorios que de él deriven.</br></br> ");%>
							
							<%textoConsentimiento.append("<br/><br/> No obstante lo anterior, el Consentimiento estará Condicionado a que: <br/><br/>");%>
							
							<%textoConsentimiento.append("1.- "+nombrePyme+((empresas!="")?", "+empresas.replaceAll(";",", "):"")+", en lo sucesivo la CEDENTE, deberá celebrar con el INTERMEDIARIO FINANCIERO "+ nombre_if	+" en su carácter de Cesionario un  Contrato de Cesión Electrónica de Derechos de Cobro, mismo que deberá ser firmado electrónicamente por el o los representantes legales o apoderados de la CEDENTE que cuente(n) con facultades generales o especiales para actos de dominio y que dicho instrumento "+
							" contractual sea notificado a la Empresa a través del sistema NAFIN para que surta efectos  legales,"+
							" en términos de lo establecido en el artículo 390 del Código de Comercio, en el entendido que de no efectuarse la mencionada notificación dentro de los 30 (treinta) días hábiles siguientes a la fecha en que sea notificada en el sistema NAFIN el presente Consentimiento,"+
							" en el entendido que de no efectuarse la mencionada notificación dentro del plazo señalado, el consentimiento de que se trata quedará sin efectos sin necesidad de que la Empresa le comunique dicha situación, quedando bajo su responsabilidad que dicha notificación se realice en los términos indicados. "); %>
							
							<%textoConsentimiento.append(" <br/><br/> 2.- La Cesión de Derechos de Cobro será por el 100% (cien por ciento) de los derechos de cobro del Contrato señalado y la cesión surtirá sus efectos apartir de la aceptación de la notificación del correspondiente Contrato de Cesión Electrónica de Derechos de Cobro de la Empresa por medio del Sistema NAFIN. <br/><br/>");%>
							
							<%textoConsentimiento.append("3.- La Cesión de Derechos de Cobro incluirá los incrementos al monto del contrato arriba señalado que, en su caso, se pacten con posterioridad mediante convenios modificatorios; y que la Cesión de Derechos de Cobro única y exclusivamente procederá con respecto de las facturas pendientes de cobro por los trabajos/servicios ejecutados, aceptados e instruidos por la Empresa, ");%>
							<%textoConsentimiento.append("en términos de los pactado en el Contrato. <br/><br/>");%>
							
							<%textoConsentimiento.append(" 4.- Que el Cedente, bajo protesta de decir verdad, garantiza que los derechos de cobro objeto de cesión, no han sido dados en garantía, pignorados, embargados, ni gravados en forma alguna y que no existe controversia sobre los mismos. <br/><br/>");%>
							
							<%textoConsentimiento.append(" 5.- Que el Cedente y el Cesionario reconocen que el Cedente es el único responsable de garantizar que los derechos de cobro están libres de gravamen o disputa.<br/><br/>");%>
							
							<%textoConsentimiento.append(" 6.- En caso de embargo es obligación exclusiva del Cedente, notificar de forma inmediata al Cesionario de cualquier acción de cobro, embargo o cualquier gravamen.<br/><br/>");%>

							<%textoConsentimiento.append(" 7.- El Cedente queda obligado a sacar en paz y a salvo a la Empresa de cualquier controversia relacionada con los gravámenes o las disputas que se originen por los derechos de cobro objeto de cesión. Lo anterior, sin menoscabo de las acciones que correspondan al Cesionario en contra del Cedente.<br/><br/>");%>
							
							<%textoConsentimiento.append(" 8.- Para el caso de que la Empresa, con posterioridad a la cesión de derechos de cobro, llegare a efectuar cualquier pago al Cedente en lugar del Cesionario respecto de dichos derechos de cobro, el Cedente quedará constituido en depositario de dicho pago, para todos los efectos a que haya lugar y se obliga a restituirlo, en forma inmediata al Cesionario, sin necesidad de que medie aviso o requerimiento para ello. <br/><br/>");%>

							<%textoConsentimiento.append(" 9.- Una vez cedidos los derechos de cobro, en caso de que la Empresa sea requerida por autoridad judicial o administrativa para retener o exhibir cualquier cantidad relacionada con los derechos de cobro objeto de la presente cesión, la Empresa, informará a la autoridad emisora que los derechos de cobro derivados del Contrato se encuentran cedidos a favor del Cesionario; y en caso de que dicha autoridad confirme "+
							" a la Empresa que ésta retenga y/o exhiba cualquier cantidad relacionada con los derechos de cobro objeto de la presente cesión, la Empresa comunicará dicha situación al Cedente y al Cesionario, para que hagan valer lo que a su derecho convenga ante dicha autoridad.<br/>"+
							" El Cedente y Cesionario renuncian en este acto y en los términos más amplios que en derecho proceda, al ejercicio de cualquier acción que les asista o que les pudiera corresponder en contra de la Empresa, derivado del cumplimiento que éste realice a cualquier requerimiento formulado o instado por las autoridades.<br/><br/>");%>

							<%textoConsentimiento.append(" 10.- El Cesionario acepta y está de acuerdo en que el pago de los derechos de cobro estará sujeto a que, en su caso, la Empresa realice las deducciones y/o retenciones que deban hacerse al cedente por adeudos con la misma, penas convencionales, pagos en exceso; cualquier requerimiento de pago derivado de laudo firme ordenado por las Juntas Locales o Federales de Conciliación y Arbitraje, "+
							 " con motivo de los juicios laborales instaurados en contra de la Empresa en relación con el Cedente; o cualquier otra deducción derivada del Contrato o de la ley de la materia en relación únicamente con el mismo, en cuyos supuestos se harán las deducciones y/o retenciones correspondientes. En tal virtud, la Empresa quedará liberada de cualquier responsabilidad por las deducciones y/o retenciones a que haya lugar. <br/><br/>");%>

							<%textoConsentimiento.append(" 11.- El Cedente y Cesionario reconocen y están de acuerdo en que el consentimiento otorgado por la Empresa para ceder los derechos de cobro del Contrato que nos ocupa, incluidos los incrementos al monto original del mismo no constituye por su parte una garantía de pago ni reconocimiento de cualquier derecho del Cesionario frente al Cedente, siendo la única obligación de la Empresa, la de pagar cantidades"+
							" líquidas que sean exigibles conforme al Contrato, una vez hechas las deducciones y/o retenciones antes mencionadas. <br/><br/>");%>

							<%textoConsentimiento.append(" 12.- Se deberá considerar la clave del Registro Federal de Contribuyentes (RFC) del Cesionario y los datos de la Cuenta Única previamente registrada la Empresa en que se realizarán los pagos correspondientes.<br/><br/>");%>
							
							<%textoConsentimiento.append(" 13.- Que para dejar sin efectos la cesión de los derechos de cobro previo a la extinción natural de dicha cesión, será necesario que el Cesionario así lo comunique a la Empresa en los mismos términos previstos para la notificación del Contrato de Cesión Electrónica de Derechos de Cobro para que surta los efectos legales que le correspondan. "+
							" A la notificación que se practique a la Empresa mediante el Sistema NAFIN, NAFIN deberá anexar la siguiente documentación: <br/>"+
							"  <br/> i) un ejemplar del Contrato de Cesión Electrónica de Derechos de Cobro con las firmas electrónicas de los representantes de la Cedente y del Cesionario; <br/>"+
							"  <br/> ii) copia certificada por fedatario público de la documentación con la que se acredite la personalidad del o de los representantes legales o apoderados del Cedente con las facultades generales o especiales para actos de dominio, y  <br/> "+
							"  <br/> iii) copia certificada por fedatario público de la documentación con la que se acredite la personalidad del o de los representantes legales o apoderados del Cesionario con las facultades suficientes para la celebración del Contrato de Cesión Electrónica de Derechos de Cobro en cuestión. <br/><br/>");%>
							
							<%textoConsentimiento.append(" En el caso de que no se anexe a la notificación la documentación señalada en los numerales anteriores, la misma no se considerará recibida por la Empresa y por lo tanto no surtirá efectos la cesión de derechos de cobro, considerando además de que el término de 30 (treinta) días hábiles para realizar la notificación del Contrato de Cesión Electrónica de Derechos de Cobro no se interrumpirá. <br/><br/>");%>
							
							<%textoConsentimiento.append(" El presente consentimiento, quedará sin efecto en caso de que la Empresa antes de la fecha de notificación a que se refiere el punto número '1' del presente documento, reciba alguna notificación de autoridad judicial o administrativa que incida sobre el Contrato cuyos derechos de cobro serán materia de cesión. Bajo este supuesto, la Empresa lo informará inmediatamente  "+
							" a través del Sistema  NAFIN al Cedente o al Cesionario, una vez que haya recibido formalmente la notificación de que se trate.  <br/><br/>");%>

						 <%textoConsentimiento.append(nombreUsuario);%>
							<tr>
								<td>&nbsp;</td>
								<td>	
								</td>	
									<td align="center" style="text-align:justify" width="900" readonly="readonly"  name="clave_contratacion"><%=textoConsentimiento.toString()%></td>									
								
								<td>&nbsp;</td>
							</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>	
	</table>
<%}else{
	%>
	<table align="center" width="750" border="0" cellspacing="1" cellpadding="1">
	<embed src="<%=strDirecVirtualTemp+cesionBean.descargaConsentimiento(clave_solicitud,strDirectorioTemp)%>" width="770" height="500"></embed>
	</table>
<%
}%>