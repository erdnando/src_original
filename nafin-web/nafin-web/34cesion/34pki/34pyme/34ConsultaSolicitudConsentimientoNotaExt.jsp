<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		com.netro.cesion.*,
		netropology.utilerias.usuarios.UtilUsr, 
		netropology.utilerias.usuarios.*,
		netropology.utilerias.*,
		com.netro.exception.NafinException"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<%
String nombresRepresentantesPyme = "";
String mensajeNuevo="";
String nombreEpo = (request.getParameter("nombreEpo")!=null)?request.getParameter("nombreEpo"):""; // esta la tengo que obtener del nombre que seleccione en el combo nombreEpo
String cedente = (request.getParameter("cedente")!=null)?request.getParameter("cedente"):"";
String cedenteAux[]=cedente.split(";");
//(String) session.getAttribute("strNombre"),
if("".equals(cedente)){
	cedente = (String) session.getAttribute("strNombre")+ " y su(s) empresa(s) representada(s)";
}else {
	if(cedenteAux.length == 1){
		cedente = cedente.replaceAll(";","");
	}

}



try {
	
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);


String clavePyme = (String)request.getSession().getAttribute("iNoCliente");
UtilUsr utilUsr = new UtilUsr();
List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");

for (int i = 0; i < usuariosPorPerfil.size(); i++) {
String loginUsrPyme = (String)usuariosPorPerfil.get(i);
Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre() + ",";
}
 
 mensajeNuevo="<br><p> De conformidad con lo señalado en el(los) Contrato(s) cuyos datos se señalan en esta pantalla,  solicito(tamos) a "+nombreEpo+" su consentimiento para ceder a favor del INTERMEDIARIO FINANCIERO señalado para estos efectos, los derechos "+
 " de cobro sobre la(s) factura(s) por trabajos ejecutados, por el 100% (cien por ciento) del monto de dicho Contrato. </p><br>"+
" En virtud de lo anterior una vez que cuente con el consentimiento que por este medio le es solicitado, celebraremos el contrato de cesión de derechos de cobro con el INTERMEDIARIO FINANCIERO, quien procederá a notificar mediante el Sistema NAFIN, en términos de Código de Comercio.</p><br>";
 
} catch (Exception e) {
	throw new AppException("Error en el Nota en la Pantalla de Consulta o ", e);
} finally {
	//Termina proceso de imprimir
}

	%>		
<html>
	<head>
	<title>Nafinet</title>		
	</head>
	<table border="0" width="930" cellpadding="0" cellspacing="0">
		<tr>
		<td align="center">
			<table align="center" border="0" width="930">
				<tr>
					<td class="formas" style="text-align:justify"><%=mensajeNuevo %>
						<!--De conformidad con lo señalado en el (los) Contrato(s) cuyos datos se señalan en esta pantalla, <%=nombresRepresentantesPyme%> como representante(s) legal(es) de la empresa, 
						solicito(amos) a <%=nombreEpo%> su consentimiento para ceder el 100% de los derechos de cobro que lleguen a derivarse por la ejecución y cumplimiento 
						del (los) referido(s) Contrato(s) y de los convenios que se lleguen a formalizar, a favor del Intermediario Financiero seleccionado, en su carácter 
						de cesionario.<br/><br/>
						En virtud de lo anterior una vez que cuente mi representada con el consentimiento solicitado a través de éste medio, aceptará y reconocerá el contrato 
						de Cesión de Derechos de Cobro correspondiente a través del sistema de Cadenas Productivas.<br/><br/>
						Al mismo tiempo manifiesto (amos) que cuento (amos) con las facultades para actos de dominio necesarias para llevar a cabo la cesión de derechos de cobro conforme se acredita mediante los poderes anexos a la presente, mismos que no han sido revocados, modificados o limitados en forma alguna a la presente fecha.<br/>-->
					</td>
				</tr>
			</table>
</html>		