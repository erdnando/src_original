	//var texto2 = ['A continuaci�n se muestran los contratos de los cuales ha recibido el consentimiento por parte de la Entidad y solicitudes de pr�rroga. '];
	var texto2 = ['A continuaci�n se muestran los contratos de los cuales ha recibido el consentimiento por parte de la Entidad.'];

	Ext.onReady(function() {

		//GRID GENERAL
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		var el = grid.getGridEl();	
	//	fp.el.unmask();

		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}			
								
				if(store.getTotalCount() > 0) {
					el.unmask();
				} else {					
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	
	
var consultaData = new Ext.data.GroupingStore({
	root: 'registros',
	url: '34AvisoSolicitudConsentimiento.data.jsp',
	baseParams: {
		informacion: 'Ajuste'
	},
	reader: new Ext.data.JsonReader({
	root : 'registros',	totalProperty: 'total',
	fields: [
				{name: 'NOMBREEPO'},
				{name: 'NOMBRECESIONARIO'}, 
				{name: 'RFCCENDENTE'}, 
				{name: 'NUMEROPROVEEDOR'},
				{name: 'FECHASOLICITUD'}, 
				{name: 'NUMEROCONTRATO'}, 
				{name: 'MONTOSPORMONEDA'},
				{name: 'NOMBRECEDENTE'},
				{name: 'FIRMA_CONTRATO'},
				{name: 'VENCE_PRORROGA'}
				
	]
	}),
	groupField: 'VENCE_PRORROGA',	sortInfo:{field: 'VENCE_PRORROGA', direction: "DESC"},
	groupDir: 'DESC',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
	
	/*var consultaData = new Ext.data.JsonStore({
			root : 'registros',
			url : '34AvisoSolicitudConsentimiento.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
				{name: 'NOMBREEPO'},
				{name: 'NOMBRECESIONARIO'}, 
				{name: 'RFCCENDENTE'}, 
				{name: 'NUMEROPROVEEDOR'},
				{name: 'FECHASOLICITUD'}, 
				{name: 'NUMEROCONTRATO'}, 
				{name: 'MONTOSPORMONEDA'},
				{name: 'NOMBRECEDENTE'},
				{name: 'FIRMA_CONTRATO'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});*/
		
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: false,
		margins: '20 0 0 0',
		title: 'Solicitud Aceptada y Pr�ximas a vencer o vencidas',
		columns: [
			{
				header: 'Solicitudes',
				tooltip: 'Solicitudes',
				hidden: true,
				dataIndex: 'VENCE_PRORROGA',	
				width: 150,	
				align: 'center', 
				hideable:false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
									if (value=='true'){
										return 'Aceptadas';
									}else{
										//return 'Pr�ximas a vencer o vencidas, que requieren pr�rroga';
										return 'Pr�ximas a vencer';
									}

								}
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Intermediarios Financiero (Cesionario)',
				tooltip: 'Intermediarios Financiero (Cesionario)',
				dataIndex: 'NOMBRECESIONARIO',
				sortable: true,
				resizable: true,
				width: 300,			
				align: 'left'
			},
			{
				header: 'Pyme (Cedente)',
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'NOMBRECEDENTE',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFCCENDENTE',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center'				
			},
			{
				header: 'No.Proveedor',
				tooltip: 'No. Proveedor',
				dataIndex: 'NUMEROPROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'			 
			},					
			{
				header: 'Fecha de Solicitud Pyme',
				tooltip: 'Fecha de Solicitud Pyme',
				dataIndex: 'FECHASOLICITUD',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},	
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NUMEROCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'				 
			},	
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda',
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTOSPORMONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'		
			}
			],			
			view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
			stripeRows: true,
			loadMask: true,
			height: 400,
			width: 943,				
			frame: true
		});
					
	//panel para mostrar  el texto 
	var fpE = new Ext.form.FormPanel({		
			id: 'forma1',
			width: 949,			
			frame: true,		
			titleCollapse: false,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',			
			html: texto2.join('')
			});
			
	var cBoton = new Ext.Container({
		layout: 'table',
		layoutConfig: {
			columns: 1
		},
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
			
			{
				xtype: 'button',
				text		: 'Enterado',
				tooltip	: 'Enterado',
				id			: 'Enterado',		
				handler: function(){				
				document.location.href  = "34ContratoCesionext.jsp";							
				}
			}			
		]
	});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		items: [			
			fpE,
			NE.util.getEspaciador(20),
			grid,			
			cBoton,
			NE.util.getEspaciador(20)
		]
		});
		
	//-------------------------------- ----------------- -----------------------------------

	consultaData.load();
	
	});