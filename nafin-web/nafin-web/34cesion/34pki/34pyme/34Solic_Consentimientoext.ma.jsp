<%@ page
		import="	java.util.*, 
				netropology.utilerias.*,
				java.io.*,
				com.netro.cesion.*,
				org.apache.commons.fileupload.disk.*,
				org.apache.commons.fileupload.servlet.*,
				org.apache.commons.fileupload.*,
				com.netro.descuento.*,
				net.sf.json.*
				"
	contentType="text/html; charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"							
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%
String  numeroProceso = (request.getParameter("numeroProceso")!=null)?request.getParameter("numeroProceso"):"";
String  accion = (request.getParameter("accion")!=null)?request.getParameter("accion"):"";
String  clavePyme = (String)request.getSession().getAttribute("iNoCliente");
JSONObject resultadoProceso = new JSONObject();

String rutaArchivoTemporal = null;
ParametrosRequest req = null;
String infoRegresar = "";
	System.out.println("numeroProceso---"+numeroProceso+"accion---"+accion);
	
	
	System.out.println("----"+ServletFileUpload.isMultipartContent(request));
	
if (ServletFileUpload.isMultipartContent(request)) {
	
	System.out.println("infoRegresar ---> "+infoRegresar);
	
	// Create a factory for disk-based file items
	DiskFileItemFactory factory = new DiskFileItemFactory();
	
	// Set factory constraints
	//factory.setSizeThreshold(yourMaxMemorySize);
	factory.setRepository(new File(strDirectorioTemp));
	// Create a new file upload handler
	ServletFileUpload upload = new ServletFileUpload(factory);
	// Set overall request size constraint
	upload.setSizeMax(10 * 2097152);	//10 Kb
	// Parse the request
	req = new ParametrosRequest(upload.parseRequest(request));
	
	// Process the uploaded items
	FileItem item = (FileItem)req.getFiles().get(0);
	
	//se ingresa la imagen del archivo	
	ImagenDocumentoContrato imagenDocumentoContrato = new ImagenDocumentoContrato();
	String tamanio = Long.toString (item.getSize());
	int  tamaniot = Integer.parseInt(tamanio);
	imagenDocumentoContrato.setSize(tamaniot);
	
	if(accion.equals("Agregar")) {
	imagenDocumentoContrato.setNumeroProceso(numeroProceso);
	imagenDocumentoContrato.setClavePyme(clavePyme);
	imagenDocumentoContrato.setTipoExpediente("11");
	imagenDocumentoContrato.guardarArchivoContratoTemp(item.getInputStream());
	
	}else if(accion.equals("Modificar")) {
		imagenDocumentoContrato.setSolicitud(numeroProceso);
		imagenDocumentoContrato.guardarImagenIndividual(item.getInputStream());
	}
			
	resultadoProceso.put("success", new Boolean(true));
	resultadoProceso.put("resultado", "ArchivoGuardado");

	infoRegresar	 =resultadoProceso.toString();
}

%>
<%=infoRegresar%>

<% System.out.println("infoRegresar ---> "+infoRegresar); %>