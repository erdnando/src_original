<%@ page 	import="
		java.util.*,
		java.sql.*,		
		java.text.*, 
		com.netro.cesion.*,
		netropology.utilerias.*,
		com.netro.exception.NafinException"
	errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<%@ page contentType="text/html;charset=windows-1252"%>
<%
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
String noPyme = (String)request.getSession().getAttribute("iNoCliente");
StringBuffer texto_plano = new StringBuffer();


CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

Calendar calendario = Calendar.getInstance();
SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
String fechaSolicitudCesion = formatoFecha.format(calendario.getTime());

HashMap contratoCesion = cesionBean.consultaContratoCesionPyme(clave_solicitud);
String clave_epo = (String)contratoCesion.get("clave_epo");
String clave_if = (String)contratoCesion.get("clave_if");
			
HashMap campos_adicionales = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
int indice_camp_adic = Integer.parseInt((String)campos_adicionales.get("indice"));

StringBuffer clausulado_parametrizado = cesionBean.consultaClausuladoParametrizado(clave_if);
String clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);

String sello_digital  = cesionBean.generaSelloDigital( clave_solicitud, strNombreUsuario );

%>

	<br/>
	<br/>
	<table align="center" width="750" border="0" cellspacing="1" cellpadding="1">
	<embed src="<%=strDirecVirtualTemp+cesionBean.generarContrato(clave_solicitud,strDirectorioTemp)%>" width="770" height="500"></embed>
	</table>
	<!--table align="center" border="0" width="625" cellpadding="0" cellspacing="0">
		<tr><td>&nbsp;</td></tr>
		<tr><td align="center" class="formas"><b><font size="2">CONTRATO DE CESI�N DE DERECHOS</font></b></td></tr>
		<tr><td>&nbsp;</td></tr>
	  <tr>
		 <td align="center">
			<table align="center" border="0" width="600" cellpadding="0" cellspacing="0">
				<tr><td align="center" class="menusup2">INFORMACI�N DE LA PYME CEDENTE</td></tr>
				<tr>
					<td align="left" class="formas">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="left" class="formas" width="300">NOMBRE:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombre_pyme")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">RFC:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("rfcPyme")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">REPRESENTANTE PYME:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrAutRep1")==null?strNombreUsuario:contratoCesion.get("nombreUsrAutRep1")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">REPRESENTANTE PYME:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrAutRep2")==null?(contratoCesion.get("nombreUsrAutRep1")==null?"":strNombreUsuario):contratoCesion.get("nombreUsrAutRep2")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">NUM. PROVEEDOR:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("numeroProveedor")%></td>
							</tr>
							<tr>
								<td align="center" class="formas" colspan="2">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td align="center" class="menusup2">INFORMACI�N DEL CONTRATO</td></tr>
				<tr>
					<td align="left" class="formas">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="left" class="formas" width="300">N�MERO DE CONTRATO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("numero_contrato")%></td>
							</tr>							
							<tr>
								<td align="left" class="formas" width="300">MONTO / MONEDA:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("montosPorMoneda")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">FECHA DE VIGENCIA:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%="N/A".equals(contratoCesion.get("fecha_inicio_contrato"))?"N/A":(contratoCesion.get("fecha_inicio_contrato") + "&nbsp; a &nbsp;" + contratoCesion.get("fecha_fin_contrato"))%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">PLAZO DEL CONTRATO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("plazoContrato")%></td>
							</tr>
							<%-- FODEA 016 - 2011 - F --%>
							<tr>
								<td align="left" class="formas" width="300">TIPO DE CONTRATACI�N:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("tipo_contratacion").toString().toUpperCase()%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">OBJETO DEL CONTRATO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("objeto_contrato").toString().toUpperCase()%></td>
							</tr>
							<%for (int i = 0; i < indice_camp_adic; i++) {%>
								<tr>
									<td align="left" class="formas" width="300"><%=campos_adicionales.get("nombre_campo_"+i).toString().toUpperCase()%>:&nbsp;</td>
									<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("campo_adicional_"+(i+1)).toString().toUpperCase()%></td>
								</tr>
							<%}%>
							<tr>
								<td align="center" class="formas" colspan="2">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td align="center" class="menusup2">INFORMACI�N DE LA EMPRESA DE PRIMER ORDEN</td></tr>
				<tr>
					<td align="left" class="formas">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="left" class="formas" width="300">NOMBRE:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombre_epo")%></td>
							</tr>
							<tr>
								<td align="center" class="formas" colspan="2">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td align="center" class="menusup2">INFORMACI�N DEL CESIONARIO (IF)</td></tr>
				<tr>
					<td align="left" class="formas">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="left" class="formas" width="300">NOMBRE:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombre_if")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">BANCO DE DEP�SITO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("bancoDeposito")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">N�MERO DE CUENTA PARA DEP�SITO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("numeroCuenta")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">N�MERO DE CUENTA CLABE PARA DEP�SITO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("numeroCuentaClabe")%></td>
							</tr>
							<tr>
								<td align="center" class="formas" colspan="2">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td align="center" class="menusup2">INFORMACI�N DE LA CESI�N DE DERECHOS</td></tr>
				<tr>
					<td align="left" class="formas">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="left" class="formas" width="300">FECHA DE SOLICITUD DE CONSENTIMIENTO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaSolicitudConsentimiento")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">FECHA DE CONSENTIMIENTO DE LA EPO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaAceptacionEpo")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">PERSONA QUE OTORG� EL CONSENTIMIENTO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrEpoAutCesion")==null?"":contratoCesion.get("nombreUsrEpoAutCesion")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">FECHA DE FORMALIZACI�N DEL CONTRATO DE CESI�N:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaFormalContrato")==null?"":contratoCesion.get("fechaFormalContrato")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE DE CESIONARIO (IF):&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrIf")==null?"":contratoCesion.get("nombreUsrIf")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrTest1")==null?"":contratoCesion.get("nombreUsrTest1")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrTest2")==null?"":contratoCesion.get("nombreUsrTest2")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">FECHA DE NOTIFICACI�N ACEPTADA POR LA EPO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaNotifEpo")==null?"":contratoCesion.get("fechaNotifEpo")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">PERSONA QUE ACEPT� LA NOTIFICACI�N:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrNotifEpo")==null?"":contratoCesion.get("nombreUsrNotifEpo")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">FECHA DE INFORMACI�N A VENTANILLA DE PAGOS:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">FECHA DE RECEPCI�N EN VENTANILLA:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">FECHA DE LA APLICACI�N DEL REDIRECCIONAMIENTO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaRedirecccion")==null?"":contratoCesion.get("fechaRedirecccion")%></td>
							</tr>
							<tr>
								<td align="left" class="formas" width="300">PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:&nbsp;</td>
								<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrRedirec")==null?"":contratoCesion.get("nombreUsrRedirec")%></td>
							</tr>
							<tr>
								<td align="center" class="formas" colspan="2">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br/>
			<%texto_plano.append("CONTRATO DE CESI�N DE DERECHOS\n\n");%>
			<%texto_plano.append("INFORMACI�N DE LA PYME CEDENTE\n");%>
			<%texto_plano.append("NOMBRE:|"+contratoCesion.get("nombre_pyme")+"\n");%>
			<%texto_plano.append("RFC:|"+contratoCesion.get("rfcPyme")+"\n");%>
			<%texto_plano.append("REPRESENTANTE PYME:|"+(contratoCesion.get("nombreUsrAutRep1")==null?strNombreUsuario:contratoCesion.get("nombreUsrAutRep1"))+"\n");%>
			<%texto_plano.append("REPRESENTANTE PYME:|"+(contratoCesion.get("nombreUsrAutRep2")==null?(contratoCesion.get("nombreUsrAutRep1")==null?"":strNombreUsuario):contratoCesion.get("nombreUsrAutRep2"))+"\n");%>
			<%texto_plano.append("NUM. PROVEEDOR:|"+contratoCesion.get("numeroProveedor")+"\n");%>
			
			<%texto_plano.append("INFORMACI�N DEL CONTRATO");%>
			<%texto_plano.append("N�MERO DE CONTRATO:|"+contratoCesion.get("numero_contrato")+"\n");%>
		
			<%texto_plano.append("MONTO / MONEDA:|"+contratoCesion.get("montosPorMoneda").toString().replaceAll("<br/>", " ")+"\n");%>
			<%texto_plano.append("FECHA DE VIGENCIA:|"+("N/A".equals(contratoCesion.get("fecha_inicio_contrato"))?"N/A":(contratoCesion.get("fecha_inicio_contrato") + " a " + contratoCesion.get("fecha_fin_contrato")))+"\n");%>
			<%texto_plano.append("PLAZO DEL CONTRATO:|"+contratoCesion.get("plazoContrato")+"\n");%>
			<%texto_plano.append("TIPO DE CONTRATACI�N:|"+contratoCesion.get("tipo_contratacion").toString().toUpperCase()+"\n");%>
			<%texto_plano.append("OBJETO DEL CONTRATO:|"+contratoCesion.get("objeto_contrato").toString().toUpperCase()+"\n");%>
			<%for (int i = 0; i < indice_camp_adic; i++) {%>
				<%texto_plano.append(campos_adicionales.get("nombre_campo_"+i).toString().toUpperCase()+":|"+contratoCesion.get("campo_adicional_"+(i+1)).toString().toUpperCase()+"\n");%>
			<%}%>
			<%texto_plano.append("INFORMACI�N DE LA EMPRESA DE PRIMER ORDEN\n\n");%>
			<%texto_plano.append("NOMBRE:|"+contratoCesion.get("nombre_epo")+"\n");%>
			<%texto_plano.append("INFORMACI�N DEL CESIONARIO (IF)\n\n");%>
			<%texto_plano.append("NOMBRE:|"+contratoCesion.get("nombre_if")+"\n");%>
			<%texto_plano.append("BANCO DE DEP�SITO:|"+contratoCesion.get("bancoDeposito")+"\n");%>
			<%texto_plano.append("N�MERO DE CUENTA PARA DEP�SITO:|"+contratoCesion.get("numeroCuenta")+"\n");%>
			<%texto_plano.append("N�MERO DE CUENTA CLABE PARA DEP�SITO:|"+contratoCesion.get("numeroCuentaClabe")+"\n");%>
			
			<%texto_plano.append("INFORMACI�N DE LA CESI�N DE DERECHOS\n\n");%>
			<%texto_plano.append("FECHA DE SOLICITUD DE CONSENTIMIENTO:|"+contratoCesion.get("fechaSolicitudConsentimiento")+"\n");%>
			<%texto_plano.append("FECHA DE CONSENTIMIENTO DE LA EPO:|"+contratoCesion.get("fechaAceptacionEpo")+"\n");%>
			<%texto_plano.append("PERSONA QUE OTORG� EL CONSENTIMIENTO:|"+(contratoCesion.get("nombreUsrEpoAutCesion")==null?"":contratoCesion.get("nombreUsrEpoAutCesion"))+"\n");%>
			<%texto_plano.append("FECHA DE FORMALIZACI�N DEL CONTRATO DE CESI�N:|"+contratoCesion.get("fechaFormalContrato")+"\n");%>
			<%texto_plano.append("NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):|"+(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1"))+"\n");%>
			<%texto_plano.append("NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):|"+(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2"))+"\n");%>
			<%texto_plano.append("NOMBRE DEL FIRMANTE DE CESIONARIO (IF):|"+(contratoCesion.get("nombreUsrIf")==null?"":contratoCesion.get("nombreUsrIf"))+"\n");%>
			<%texto_plano.append("NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:|"+(contratoCesion.get("nombreUsrTest1")==null?"":contratoCesion.get("nombreUsrTest1"))+"\n");%>
			<%texto_plano.append("NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:|"+(contratoCesion.get("nombreUsrTest2")==null?"":contratoCesion.get("nombreUsrTest2"))+"\n");%>
			<%texto_plano.append("FECHA DE NOTIFICACI�N ACEPTADA POR LA EPO:|"+(contratoCesion.get("fechaNotifEpo")==null?"":contratoCesion.get("fechaNotifEpo"))+"\n");%>
			<%texto_plano.append("PERSONA QUE ACEPT� LA NOTIFICACI�N:|"+(contratoCesion.get("nombreUsrNotifEpo")==null?"":contratoCesion.get("nombreUsrNotifEpo"))+"\n");%>
			<%texto_plano.append("FECHA DE INFORMACI�N A VENTANILLA DE PAGOS:|"+(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla"))+"\n");%>
			<%texto_plano.append("FECHA DE RECEPCI�N EN VENTANILLA:|"+(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla"))+"\n");%>
			<%texto_plano.append("FECHA DE LA APLICACI�N DEL REDIRECCIONAMIENTO:|"+(contratoCesion.get("fechaRedirecccion")==null?"":contratoCesion.get("fechaRedirecccion"))+"\n");%>
			<%texto_plano.append("PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:|"+(contratoCesion.get("nombreUsrRedirec")==null?"":contratoCesion.get("nombreUsrRedirec"))+"\n");%>
		 </td>
	  </tr>
	</table>
	<br/>
	<table align="center" border="0" width="625" cellpadding="0" cellspacing="0">
		<tr><td align="center" class="formas"><b>SE SUJETA A LAS SIGUIENTES CL�USULAS</b></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="center">
				<table align="center" border="1" width="600">
					<tr>
						<td class="formas" align="left">
							<%=clausulado_parametrizado.toString().replaceAll("\n", "<br/>")%>
						</td>
					</tr>
					<%texto_plano.append("\nSE SUJETA A LAS SIGUIENTES CL�USULAS\n");%>
					<%texto_plano.append("\n"+clausulado_parametrizado.toString());%>
					<%texto_plano.append("\n\n");%>
					<%texto_plano.append("Sello Digital de "+contratoCesion.get("nombre_pyme")+"\n");%>
					<%texto_plano.append(sello_digital);%>
				</table>
				<div id="div_texto_plano" style="display:none">					
					<textarea cols=140 rows=20 name="texto_plano"><%=texto_plano.toString()%></textarea>
				</div>
			</td>
		</tr>
	</table>
	<br/>
	<table border="0" width="625" cellpadding="0" cellspacing="0">
		<tr><td>&nbsp;</td></tr>
		<tr><td align="center" class="formas"><b>En mi car�cter de cesionario, Acepto T�rminos y Condiciones.</b></td></tr>
		<tr><td>&nbsp;</td></tr>
	</table-->
	<br/>	
	<br/>
