<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.cesion.*,
		com.netro.cesion.ConsulSolicitudConsentimiento,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%
String informacion = (request.getParameter("informacion"))!=null?request.getParameter("informacion"):"";
String infoRegresar = "";
int start 				= 0;
int limit 				= 0;
JSONObject 	jsonObj	= new JSONObject();

	if(informacion.equals("valoresIniciales")){
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("strDirectorioPublicacion",strDirectorioPublicacion);
		jsonObj.put("strTipoUsuario",strTipoUsuario); 
		infoRegresar = jsonObj.toString();
	}
	else if(informacion.equals("ConsultaPrincipal")){
		CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
		List registros = cesionBean.monitorCesionDerechos(strTipoUsuario, iNoCliente);
	
		String  regestatus = "";
		String  estatus = "";
		String  numero = "";					
		int posicion = 0;
		int longitud = 0;
		int solicitar = 0;
		int enproceso = 0;
		int aceptadas = 0; 
		int rechazadas= 0;
		int vencida = 0;
		int reactivada = 0;  
		int aceptadopyme = 0;
		int aceptadoIF = 0;
		int rechazadoIF = 0;
		int notiAceptada = 0;
		int notiVentanilla = 0;
		int notiRechazada = 0;
		int preSolicitada = 0;
		int preAceptada = 0;
		int extincionPresolicitada = 0;
		int extincionSolicitada = 0;
		int extincionAceptadaIf = 0;
		int extincionNotificadoVentanilla = 0;
		int extincionNotificaAceptada = 0;
		int extincionRedirecAplic = 0;
		int totalSolicitudes = 0;
		int totalContrato = 0;
		int totalNotificaciones = 0;
		int solicitudes = 0;
		int totalExtinciones = 0;
		int canceladaNafin = 0;
		int rechazadaExpirada = 0;
	
		for (int i = 0; i < registros.size(); i++) {						
			regestatus     =((registros.get(i))==null)?"":(registros.get(i).toString());
			longitud = regestatus.length();
			posicion =regestatus.indexOf("e");					 
			estatus= regestatus.substring(0, posicion);					
			numero= regestatus.substring(posicion+1, longitud);
	
			if (estatus.equals("1") ){
				solicitar = (Integer.parseInt(numero));
			} else if (estatus.equals("2") ){
				enproceso = (Integer.parseInt(numero));
			} else if (estatus.equals("3")) {
				aceptadas = (Integer.parseInt(numero));
			}	else if (estatus.equals("5")) {
				rechazadas = (Integer.parseInt(numero));
			}	else if (estatus.equals("6")) {
				reactivada = (Integer.parseInt(numero));
			}	else if (estatus.equals("7")) {
				aceptadopyme = (Integer.parseInt(numero));
			}	else if (estatus.equals("8")) {
				vencida = (Integer.parseInt(numero));
			}	else if (estatus.equals("9")) {
				aceptadoIF = (Integer.parseInt(numero));
			}	else if (estatus.equals("10")) {
				rechazadoIF = (Integer.parseInt(numero));
			}	else if (estatus.equals("11")) {
				notiAceptada = (Integer.parseInt(numero));
			}	else if (estatus.equals("16")) {
				notiVentanilla = (Integer.parseInt(numero));
			}	else if (estatus.equals("12")) {
				notiRechazada = (Integer.parseInt(numero));
			} else if (estatus.equals("13")) {
				preSolicitada = (Integer.parseInt(numero));
			} else if (estatus.equals("14")) {
				preAceptada = (Integer.parseInt(numero));
			} else if (estatus.equals("18")) {
				extincionPresolicitada = (Integer.parseInt(numero));
			} else if (estatus.equals("19")) {
				extincionSolicitada = (Integer.parseInt(numero));
			} else if (estatus.equals("20")) {
				extincionAceptadaIf = (Integer.parseInt(numero));
			} else if (estatus.equals("23")) {
				extincionNotificaAceptada = (Integer.parseInt(numero));
			} else if (estatus.equals("21")) {
				extincionNotificadoVentanilla = (Integer.parseInt(numero));
			} else if (estatus.equals("22")) {
				extincionRedirecAplic = (Integer.parseInt(numero));
			}	else if (estatus.equals("24")) {	 //Cancelada por  Nafin  F023-2015
				canceladaNafin = (Integer.parseInt(numero));
			}	else if (estatus.equals("25")) {	 //Rechazada Expirada  F023-2015
				rechazadaExpirada = (Integer.parseInt(numero));			 
			
		}
	}
		totalSolicitudes = solicitar+ enproceso + aceptadas + rechazadas + vencida + preSolicitada;
		totalContrato =  preAceptada + aceptadas + aceptadopyme + aceptadoIF + rechazadoIF;
		totalNotificaciones = aceptadoIF + notiVentanilla + notiAceptada + notiRechazada;	
		solicitudes = aceptadas + rechazadas + vencida;
		totalExtinciones = extincionPresolicitada + extincionSolicitada + extincionAceptadaIf + extincionNotificaAceptada + extincionNotificadoVentanilla + extincionRedirecAplic;
		//}
		JSONObject jsonObj0 = new JSONObject();
			jsonObj0.put("Descripcion","SOLICITUDES DE CONSENTIMIENTO");
			jsonObj0.put("Total",new Integer(totalSolicitudes));
		JSONObject jsonObj1 = new JSONObject();
			jsonObj1.put("Descripcion","POR SOLICITAR");
			jsonObj1.put("Total",new Integer(solicitar));
		JSONObject jsonObj2 = new JSONObject();
			jsonObj2.put("Descripcion","PRE-SOLICITADA");
			jsonObj2.put("Total",new Integer(preSolicitada));	
		JSONObject jsonObj3 = new JSONObject();
			jsonObj3.put("Descripcion","EN PROCESO");
			jsonObj3.put("Total",new Integer(enproceso));
		JSONObject jsonObj4 = new JSONObject();
			jsonObj4.put("Descripcion","TOTAL SOLICITUDES");
			jsonObj4.put("Total",new Integer(solicitudes));
		JSONObject jsonObj5 = new JSONObject();
			jsonObj5.put("Descripcion","ACEPTADAS");
			jsonObj5.put("Total",new Integer(aceptadas));
		JSONObject jsonObj6 = new JSONObject();
			jsonObj6.put("Descripcion","RECHAZADAS");
			jsonObj6.put("Total",new Integer(rechazadas));
		JSONObject jsonObj7 = new JSONObject();
			jsonObj7.put("Descripcion","VENCIDAS");
			jsonObj7.put("Total",new Integer(vencida));	
		
		JSONObject jsonObj25 = new JSONObject(); //F023-2015
		jsonObj25.put("Descripcion", "CANCELADA POR NAFIN");
		jsonObj25.put("Total", new Integer(canceladaNafin));
			
			
		JSONObject jsonObj9 = new JSONObject();
			jsonObj9.put("Descripcion","CONTRATOS DE CESIÓN");
			jsonObj9.put("Total",new Integer(totalContrato));
		JSONObject jsonObj10 = new JSONObject();
			jsonObj10.put("Descripcion","SOLICITUDES PRE-ACEPTADAS");
			jsonObj10.put("Total",new Integer(preAceptada));
		JSONObject jsonObj11 = new JSONObject();
			jsonObj11.put("Descripcion","ACEPTADOS PYME");
			jsonObj11.put("Total",new Integer(aceptadopyme));
		JSONObject jsonObj12 = new JSONObject();
			jsonObj12.put("Descripcion","ACEPTADOS IF");
			jsonObj12.put("Total",new Integer(aceptadoIF));
		JSONObject jsonObj13 = new JSONObject();
			jsonObj13.put("Descripcion","RECHAZADOS IF");
			jsonObj13.put("Total",new Integer(rechazadoIF));
			
		JSONObject jsonObj26 = new JSONObject();   //F023-2015
			jsonObj26.put("Descripcion","RECHAZADA EXPIRADA");
			jsonObj26.put("Total", new Integer(rechazadaExpirada));
			

		JSONObject jsonObj14 = new JSONObject();
			jsonObj14.put("Descripcion","NOTIFICACIONES");
			jsonObj14.put("Total",new Integer(totalNotificaciones));
		JSONObject jsonObj15 = new JSONObject();
			jsonObj15.put("Descripcion","NOTIFICACIONES ACEPTADAS");
			jsonObj15.put("Total",new Integer(notiAceptada));
		JSONObject jsonObj16 = new JSONObject();
			jsonObj16.put("Descripcion","NOTIFICADO VENTANILLA");
			jsonObj16.put("Total",new Integer(notiVentanilla));
		JSONObject jsonObj17 = new JSONObject();
			jsonObj17.put("Descripcion","RECHAZADAS");
			jsonObj17.put("Total",new Integer(notiRechazada));
			
		JSONObject jsonObj18 = new JSONObject();
			jsonObj18.put("Descripcion","EXTINCIÓN DEL CONTRATO");
			jsonObj18.put("Total",new Integer(totalExtinciones));
		JSONObject jsonObj19 = new JSONObject();
			jsonObj19.put("Descripcion","PRE-SOLICITADA");
			jsonObj19.put("Total",new Integer(extincionPresolicitada));
		JSONObject jsonObj20 = new JSONObject();
			jsonObj20.put("Descripcion","SOLICITADA");
			jsonObj20.put("Total",new Integer(extincionSolicitada));
		JSONObject jsonObj21 = new JSONObject();
			jsonObj21.put("Descripcion","ACEPTADA IF");
			jsonObj21.put("Total",new Integer(extincionAceptadaIf));
		JSONObject jsonObj22 = new JSONObject();
			jsonObj22.put("Descripcion","NOTIFICACIONES ACEPTADAS");
			jsonObj22.put("Total",new Integer(extincionNotificaAceptada));
		JSONObject jsonObj23 = new JSONObject();
			jsonObj23.put("Descripcion","NOTIFICADO VENTANILLA");
			jsonObj23.put("Total",new Integer(extincionNotificadoVentanilla));
		JSONObject jsonObj24 = new JSONObject();
			jsonObj24.put("Descripcion","REDIRECCIONAMIENTO APLICADO");
			jsonObj24.put("Total",new Integer(extincionRedirecAplic));

		JSONArray jsonArray = new JSONArray();
		jsonArray.add(jsonObj0);
		jsonArray.add(jsonObj1);
		jsonArray.add(jsonObj2);
		jsonArray.add(jsonObj3);
		jsonArray.add(jsonObj4);
		jsonArray.add(jsonObj5);
		jsonArray.add(jsonObj6);
		jsonArray.add(jsonObj7);
		jsonArray.add(jsonObj25); //Fodea 23-2015 

		jsonArray.add(jsonObj9);
		jsonArray.add(jsonObj10);
		jsonArray.add(jsonObj11);
		jsonArray.add(jsonObj12);
		jsonArray.add(jsonObj13);
		jsonArray.add(jsonObj26);//Fodea 23-2015 

		jsonArray.add(jsonObj14);
		jsonArray.add(jsonObj15);
		jsonArray.add(jsonObj16);
		jsonArray.add(jsonObj17);

		jsonArray.add(jsonObj18);
		jsonArray.add(jsonObj19);
		jsonArray.add(jsonObj20);
		jsonArray.add(jsonObj21);
		jsonArray.add(jsonObj22);
		jsonArray.add(jsonObj23);
		jsonArray.add(jsonObj24);

		infoRegresar =	"{\"success\": true, \"total\":" + jsonArray.size() + ", \"registros\": " + jsonArray.toString()+"}";
	}
	else if (informacion.equals("Consulta")||informacion.equals("ArchivoTotalPDF")) {
		CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	
		String tipo = (request.getParameter("tipo")==null)?"":request.getParameter("tipo");
		ConsulSolicitudConsentimiento paginador = new ConsulSolicitudConsentimiento();
		
		if (strTipoUsuario.equals("PYME")) {	
			paginador.setClavePyme(iNoCliente);
		} else if  (strTipoUsuario.equals("EPO")) {
			paginador.setClaveEpo(iNoCliente);
		}
		paginador.setClaveEstatusSolicitud(tipo);
		
	
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if(informacion.equals("Consulta")){
			String operacion = (request.getParameter("operacion")== null)?"":request.getParameter("operacion");
			try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
					throw new AppException("Error en los parámetros recibidos",e);
			}
			try {
					if (operacion.equals("Generar")) {
						queryHelper.executePKQuery(request);
					}
					Registros registros = queryHelper.getPageResultSet(request,start,limit);
					while(registros.next()){
						StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(registros.getString("CLAVE_SOLICITUD"));
						registros.setObject("MONTO_MONEDA",montosMonedaSol.toString());
					}
										
				String  consulta =	"{\"success\": true, \"total\": \""+ queryHelper.getIdsSize() +"\", \"registros\": " + registros.getJSONData()+"}";
				
				jsonObj = new JSONObject();
				jsonObj = JSONObject.fromObject(consulta);
				jsonObj.put("tipo",tipo);		
				infoRegresar = jsonObj.toString();  
				
			} catch(Exception e){
					throw new AppException("Error en la paginación",e);
			}
		} else if (informacion.equals("ArchivoTotalPDF")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
			}
		}	
	}else if(informacion.equals("CONTRATO_PDF")){
		CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
		CreaArchivo creaArchivo = new CreaArchivo();
		String numeroSolicitud = (request.getParameter("numeroSolicitud"))!=null?request.getParameter("numeroSolicitud"):"";
		
		String nombreArchivo = cesionBean.descargaContratoCesion(numeroSolicitud, strDirectorioTemp);
		
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	
	}
%>
<%=infoRegresar%>
