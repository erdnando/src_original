	Ext.onReady(function() {
	var csProrrogaContrato = '';
	
	
	
	var procesarConsultaCatContratacion = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		csProrrogaContrato = jsonData.CDER_CS_PRORROGA_CONTRATO;
	}
	//muestra el archivo de la Columna Contrato 
	var procesarSuccessFailureContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
		
	var descargaArchivoContrato = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '34ContratoCesionPDFCSV.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContrato
		});
	}
	
	//Muestra el archivo de la Columna Contrato de Cesion 
	var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
		
	var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '34ContratoCesionPDFCSV.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}
	
	//Ventana Para ver el Contrato de Consentimiento
	var verConsentimiento = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_contratacion = registro.get('CLAVETIPOCONTRATACION');
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		var nombre_epo = registro.get('NOMBRE_EPO');
		var objeto_contrato = registro.get('OBJETO_CONTRATO');
		var montospormoneda = registro.get('MONTOSPORMONEDA');
		var plazocontrato = registro.get('PLAZOCONTRATO');
		var fechasolicitud = registro.get('FECHASOLICITUD');
		var numero_contrato = registro.get('NUMERO_CONTRATO');
		var nombre_if = registro.get('NOMBRE_IF');
		
		
		
		var ventana = Ext.getCmp('verConsentimiento');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 940,
				height: 655,
				id: 'verConsentimiento',
				closeAction: 'hide',
				items: [					
					Consentimiento
				],
				title: 'Consentimiento'	
			}).show();
		}
		var bodyPanel = Ext.getCmp('Consentimiento').body;
			var mgr = bodyPanel.getUpdater();
			mgr.on('failure', 
				function(el, response) {
					x.update('');
					NE.util.mostrarErrorResponse(response);
				}
			);		
			mgr.update({
				url: '34ContratoCesionVerConsentimientoext.jsp',	
				scripts: true,
				params: {
					clave_contratacion: clave_contratacion,
					clave_solicitud:clave_solicitud,
					nombre_epo:nombre_epo,
					objeto_contrato:objeto_contrato,
					montospormoneda:montospormoneda,
					plazocontrato:plazocontrato,
					fechasolicitud:fechasolicitud,
					numero_contrato:numero_contrato,
					nombre_if:nombre_if
				},
				indicatorText: 'Cargando Consentimiento'
			});	
	}
	
	// Procesa la generaccion del Acuse 
	function procesarSuccessFailureAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			if (jsondeAcuse != null){
				var acuse= jsondeAcuse.acuse;
				var clave_solicitudAcuse= jsondeAcuse.clave_solicitud;	
				var mostrarError= jsondeAcuse.mostrarError;
				
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'				
					})
				});
				
			}		
					
			var btnGenerarAc = Ext.getCmp('btnGenerarPDFAcuse');
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			if(mostrarError==''){				
				btnGenerarAc.enable();
				btnBajarPDFAcuse.hide();
			}else{
				btnGenerarAc.disable();
				btnBajarPDFAcuse.hide();
			}
			
			var bodyPanel = Ext.getCmp('AcuseFirma').body;
			
			
			/*var mgr = bodyPanel.getUpdater();		
			mgr.update({
				url: '34ContratoCesionPreacuseExt.jsp',	
				scripts: true,
				params: {
					clave_solicitud:clave_solicitudAcuse,
					acuse:acuse
				},	
				indicatorText: 'Cargando Acuse de Contrato de Cesi�n'
			});	*/
		
			var boton = Ext.getCmp('btnGenerarPDFAcuse');
			
					//boton.disable();
					//boton.setIconClass('loading-indicator');								
					Ext.Ajax.request({
						url: '34ContratoCesionPDFCSV.jsp',
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion:'ACUSE',
							acuse_carga: acuse,	
							clave_solicitud: clave_solicitudAcuse	
						}),
						callback: procesarSuccessFailureGenerarPDFAcuse
					});
				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	
	//Procesa la Generacion del Acuse 
	var procesaAcuse = function(pkcs7, textoFirmado, clave_solicitud, claveNueva, sello_digital) {
	
		if (Ext.isEmpty(pkcs7)) {
				
			return;
		}else  {	
		
			var verPreAcuse2 = Ext.getCmp('verPreAcuse');
			if (verPreAcuse2) {		
				verPreAcuse2.destroy();		
			} 
			var ventana = Ext.getCmp('verAcuse');	
			if (ventana) {				
				ventana.show();	
			} else {	
				new Ext.Window({
					layout: 'fit',
					width: 940,
					height: 655,
					id: 'verAcuse',
					closeAction: 'hide',
					items: [					
						AcuseFirma
					],
					title: 'Acuse de Contrato de Cesi�n',
					bbar: {
					xtype: 'toolbar',
					buttons: [	
						'->',					
							{
								xtype: 'button',
								text: 'Generar PDF',
								hidden: true,
								id: 'btnGenerarPDFAcuse'											
							},
							'-',
							{
								xtype: 'button',
								text: 'Descargar PDF',
								id: 'btnBajarPDFAcuse',
								hidden: false
							}					
						]
					}			
				}).show();
			}
				
			AcuseFirma.el.mask('Procesando...', 'x-mask-loading');
			var bodyPanel = Ext.getCmp('AcuseFirma').body;
			
			Ext.Ajax.request({
				url : '34ContratoCesionext.data.jsp',
				params : {
					informacion: 'GeneraAcuse',
					pkcs7: pkcs7,
					textoFirmado: textoFirmado,
					clave_solicitud:clave_solicitud,
					claveNueva:claveNueva,
					sello_digital:sello_digital	
				},
				callback: procesarSuccessFailureAcuse
			});		
				
		}
	}
	
	var solicitarProrroga =function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud =registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
				url : '34ContratoCesionext.data.jsp',
				params : {
					informacion: 'solicitarProrroga',
					clave_solicitud:clave_solicitud,
					contador: registro.get('CONTADOR_PRORROGA')
				},
				callback: function(opts, success, response){
					if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
						json = Ext.util.JSON.decode(response.responseText);
						Ext.Msg.alert(
									'Mensaje',
									'La pr�rroga ha sido solicitada con �xito',
									function(){
										location.reload();
									}
								);
						}
						else{
							NE.util.mostrarConnError(response,opts);
						}
					}	
			});		
	}
	
	var validaCedenteCesionario =function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_if = registro.get('CLAVE_IF');	
		var clave_pyme = registro.get('CLAVEPYME');
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
				url : '34ContratoCesionPDFCSV.jsp',
				params : {
					informacion: 'VALIDA_CEDENTE_CESIONARIO',
					clave_if:clave_if,
					clave_pyme:clave_pyme,
					clave_solicitud: clave_solicitud
				},
				callback: function(opts, success, response){
					if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
						json = Ext.util.JSON.decode(response.responseText);
						if(json.cedente&&json.cesionario){
							
							if(item.tooltip=='Autorizar Contrato')
								{
									procesaAccion(grid, rowIndex, colIndex, item, event);									
								}else{
									descargarContratoCesion(grid, rowIndex, colIndex, item, event);	
								}
						}else{
							if(!json.cedente&&!json.cesionario){
							Ext.MessageBox.alert("Mensaje",'El Contrato de Cesi�n de Derechos no pudo ser generado, no cuenta con la informaci�n del Cedente y el Cesionario');
							}else
							if(!json.cedente){
								Ext.MessageBox.alert("Mensaje",'El Contrato de Cesi�n de Derechos no pudo ser generado, no cuenta con la informaci�n del Cedente');
							}else{
								Ext.MessageBox.alert("Mensaje",'El Contrato de Cesi�n de Derechos no pudo ser generado, no cuenta con la informaci�n del Cesionario');
							}
							
						}
					}
				
				}
			});		
	}
	
	var muestraFirmaContrato =function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_solicitud = registro.get('CLAVE_SOLICITUD');        
		var win = new NE.AutoConsorcio.VisorConsorcio(ic_solicitud, 'Contrato_cesion','Estatus de Firmas', 'N' );		
	}
	//ventana para ver el Preacuse 
	var procesaAccion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var textoFirmado = registro.get('TEXTOFIRMAR');	
		var claveNueva = registro.get('NUEVOESTATUS');
		var sello_digital = registro.get('SELLODIGIDAL');
		var claveNueva = registro.get('NUEVOESTATUS'); 				
		var pantallaOrigen='CONTRATO';	
		
		var ventana = Ext.getCmp('verPreAcuse');		 
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 940,	
				height: 655,			
				id: 'verPreAcuse',
				closeAction: 'hide',
				items: [					
					ColumnaAccion
				],
				title: 'Preacuse de Contrato de Cesi�n',
				bbar: {
				xtype: 'toolbar',
				buttons: [	
					'->',
					'-',
					{
						xtype: 'button',
						text: 'Autorizar',
						id: 'btnAutorizar'											
					}									
				]
			}			
			}).show();
			
		}
		
		var bodyPanel = Ext.getCmp('ColumnaAccion').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure', 
			function(el, response) {
				x.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		mgr.update({
			//url: '34ContratoCesionPreacuseExt.jsp',	
			url: '34Solicitud_Pre_AcuseExt.jsp',
			scripts: true,
			params: {
				clave_solicitud: clave_solicitud,
				tipoPreAcuse: 'Contrato'
			},
			indicatorText: 'Cargando Preacuse de Contrato de Cesi�n'
		});	
						
		///para la Firma 
			
		var btnAutorizar = Ext.getCmp('btnAutorizar');
		btnAutorizar.setHandler(
			function(boton, evento) {					
				
				NE.util.obtenerPKCS7(procesaAcuse, textoFirmado, clave_solicitud, claveNueva, sello_digital);
			}
		);
	}
	
	//GRID GENERAL
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var  clasificacionEpo = jsonData.clasificacionEpo;	
		var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
			
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}	
			//edito el titulo de la columna  de clasificacion EPO y la muestro 
			var cm = grid.getColumnModel();
			if(clasificacionEpo ==''){		
				grid.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), true);					
			}else{
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
			}
				
			if(hayCamposAdicionales=='0'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='1'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)				
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
			}
			if(hayCamposAdicionales=='2'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
			}
			if(hayCamposAdicionales=='3'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
			}
			if(hayCamposAdicionales=='4'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.Campo3);
			}
				
			if(hayCamposAdicionales=='5'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), false);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.Campo3);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_5'),jsonData.Campo4);
			}
		
			var el = grid.getGridEl();
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');	
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
				
			if(store.getTotalCount() > 0) {				
				btnImprimirPDF.enable();
				btnImprimirCSV.enable();					
				btnBajarPDF.hide();
				btnBajarCSV.hide();
				el.unmask();
			} else {	
				btnImprimirPDF.disable();
				btnImprimirCSV.disable();
				btnBajarPDF.hide();
				btnBajarCSV.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}				
		}			
	}
	
	//GENERAR ARCHIVO TODO PDF ACUSE
	var procesarSuccessFailureGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		//btnGenerarPDFAcuse.setIconClass('');
		AcuseFirma.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			 Ext.getCmp('AcuseFirma').update('<table align="center" width="750" border="0" cellspacing="1" cellpadding="1"><embed src="'+ Ext.util.JSON.decode(response.responseText).urlArchivo+'" width="770" height="500"></embed></table>');
			btnBajarPDFAcuse.show();
			//btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			//btnGenerarPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailureCSV =  function(opts, success, response) {
		var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');
		btnImprimirCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	
	var catalogoEstatusData = new Ext.data.JsonStore({
		id: 'catalogoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	var catalogoContratacionData = new Ext.data.JsonStore({
		id: 'catalogoContratacionDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionext.data.jsp',
		baseParams: {
			informacion: 'CatalogoContratacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaCatContratacion,	
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	var catalogoTipoPlazoData = new Ext.data.JsonStore({
		id: 'catalogoTipoPlazonDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'clave_epo',
			id: 'clave_epo2',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_epo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var tipoContra = Ext.getCmp('clave_contratacion2');
						tipoContra.setValue('');
						tipoContra.setDisabled(false);
						tipoContra.store.load({
								params: {
									clave_epo:combo.getValue()
								}
						});
					}
				}
			}			
		},
		{
			xtype: 'combo',
			name: 'clave_if',
			id: 'clave_if2',
			fieldLabel: 'Nombre del Cesionario',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_if',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoIFData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Contrato', 
			combineErrors: false,
			msgTarget: 'side',
			width: 120,
			items: [
			{
				xtype: 'textfield',
				name: 'numero_contrato',
				id: 'numero_contrato2',
				fieldLabel: 'N�mero de Contrato',
				allowBlank: true,
				hidden: false,
				maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			}
			]			
		},
		{
			xtype: 'combo',
			name: 'clave_moneda',
			id: 'clave_moneda2',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_moneda',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'clave_estatus_sol',
			id: 'clave_estatus_sol2',
			fieldLabel: 'Estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_estatus_sol',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEstatusData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'clave_contratacion',
			id: 'clave_contratacion2',
			fieldLabel: 'Tipo de Contrataci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_contratacion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoContratacionData,
			tpl : NE.util.templateMensajeCargaCombo
		},		
		{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de Vigencia de Contrato',
				combineErrors: false,
				msgTarget: 'side',
				items: [
					{
						xtype: 'datefield',
						name: 'fecha_vigencia_ini',
						id: 'fecha_vigencia_ini',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoFinFecha: 'df_operacion_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'al',
						width: 20
					},
					{
						xtype: 'datefield',
						name: 'df_operacion_fin',
						id: 'df_operacion_fin',
						allowBlank: true,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoInicioFecha: 'fecha_vigencia_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			}	,				
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo del Contrato',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'plazoContrato',
					id: 'plazoContrato',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},			
				{
					xtype: 'combo',
					name: 'claveTipoPlazo',
					id: 'claveTipoPlazo2',
					fieldLabel: '',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'claveTipoPlazo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoTipoPlazoData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		}
	];		
		
	var consultaData = new Ext.data.JsonStore({
	root : 'registros',
	url : '34ContratoCesionext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
		fields: [
			{name: 'CLAVE_SOLICITUD',type: 'float'},
			{name: 'CLAVEEPO',type: 'float'}, 
			{name: 'CLAVEPYME',type: 'float'}, 
			{name: 'CLAVETIPOCONTRATACION'}, 
			{name: 'NOMBRE_EPO'}, 
			{name: 'NOMBRE_IF'}, 
			{name: 'NUMERO_CONTRATO'}, 				
			{name: 'MONTOSPORMONEDA' }, 
			{name: 'TIPO_CONTRATACION'}, 
			{name: 'FECHA_INICIO_CONTRATO'},
			{name: 'FECHA_FIN_CONTRATO'},
			{name: 'PLAZOCONTRATO'},
			{name: 'CLASIFICACION_EPO'},
			{name: 'CAMPO_ADICIONAL_1'},
			{name: 'CAMPO_ADICIONAL_2'},
			{name: 'CAMPO_ADICIONAL_3'},
			{name: 'CAMPO_ADICIONAL_4'},
			{name: 'CAMPO_ADICIONAL_5'},
			{name: 'VENTANILLAPAGO'},
			{name: 'SUPADMRESOB'},
			{name: 'NUMEROTELEFONO'},
			{name: 'OBJETO_CONTRATO'},
			{name: 'FECHAACEPTACION'},
			{name: 'CLAVE_ESTATUS'},
			{name: 'ESTATUS_SOLICITUD'},
			{name: 'COMENTARIOS'},
			{name: 'DIASMINIMOSNOTIFICACION'},
			{name: 'ACUSEENVIOSOLICEPO'},
			{name: 'CLAVEUSREPOAUTCESION'},
			{name: 'NOMBREUSREPOAUTCESION'},
			{name: 'FECHAEPOAUTCESION'},
			{name: 'HORAEPOAUTCESION'},
			{name: 'CONTRATOCESION'},
			{name: 'NUEVOESTATUS'},
			{name: 'MENSAJE'}	,	
			{name: 'TEXTOFIRMAR'},
			{name: 'SELLODIGIDAL'},
			{name: 'FECHASOLICITUD'},
			{name: 'CLAVE_IF'},
			{name: 'FIRMA_CONTRATO'},
			{name: 'FECHA_PRORROGA'},
			{name: 'ESTATUS_PRORROGA'},
			{name: 'CAUSAS_RETORNO'},
			{name: 'CONTADOR_PRORROGA'},
			{name: 'TERMINO_PRORROGA'}
			
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
			//Seleccionado Pyme a Rechazado IF
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		title: '',
		columns: [
			{
				header: 'Dependencia',
				tooltip: 'Dependencia',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Intermediario Financiero (Cesionario)',
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda',
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTOSPORMONEDA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Fecha Inicio Contrato',
				tooltip: 'Fecha Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},			
			{
				header: 'Fecha Final Contrato',
				tooltip: 'Fecha Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Plazo del Contrato',
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZOCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				//header: '',
				//tooltip: '',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 130,				
				align: 'left'
			},			
			{
				header: 'Campo 1',
				tooltip: 'Campo 1',
				dataIndex: 'CAMPO_ADICIONAL_1',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Campo 2',
				tooltip: 'Campo 2',
				dataIndex: 'CAMPO_ADICIONAL_2',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 130,				
				align: 'center'
			},			
			{
				header: 'Campo 3',
				tooltip: 'Campo 3',
				dataIndex: 'CAMPO_ADICIONAL_3',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Campo 4',
				tooltip: 'Campo 4',
				dataIndex: 'CAMPO_ADICIONAL_4',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Campo 5',
				tooltip: 'Campo 5',
				dataIndex: 'CAMPO_ADICIONAL_5',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 130,				
				align: 'center'
			},						
			{
				header: 'Ventanilla de Pago',
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENTANILLAPAGO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra',
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUPADMRESOB',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex: 'NUMEROTELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Objeto del Contrato',
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Comentarios',
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Fecha L�mite de Firma',
				tooltip: 'Fecha L�mite de Firma',
				dataIndex: 'FECHAACEPTACION',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center '
			},
			{
				header: 'Estatus Actual',
				tooltip: 'Estatus Actual',
				dataIndex: 'ESTATUS_SOLICITUD',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
	      xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},
						handler: descargaArchivoContrato
					}
				]				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Consentimiento',
				tooltip: 'Consentimiento',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						this.items[0].tooltip = 'Ver';
							return 'iconoLupa';												
						},
						handler: verConsentimiento
					}
				]				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Contrato de Cesi�n',
				tooltip: 'Contrato de Cesi�n',
				width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						if (registro.get('CLAVE_ESTATUS') !=1 && registro.get('CLAVE_ESTATUS') !=2  && registro.get('CLAVE_ESTATUS') !=5 && registro.get('CLAVE_ESTATUS') !=13) {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler: validaCedenteCesionario
					}
				]
			},
			{
	      xtype: 'actioncolumn',
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						if (registro.get('CLAVE_ESTATUS') !=1 && registro.get('CLAVE_ESTATUS') !=2  && registro.get('CLAVE_ESTATUS') !=5 && registro.get('CLAVE_ESTATUS') !=13) {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler: muestraFirmaContrato
					}
				]
			},
			{
				header: 'Fecha Vencimiento Pr�rroga',
				tooltip: 'Fecha Vencimiento Pr�rroga',
				dataIndex: 'FECHA_PRORROGA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					if(csProrrogaContrato == 'N'){
						return 'N/A';
					}else if (record.get('CONTADOR_PRORROGA') <1){
						return 'N/A';
					}else{
						return value;
					}
				}	
				
			},
			{
				header: 'Estatus Pr�rroga',
				tooltip: 'Estatus Pr�rroga',
				dataIndex: 'ESTATUS_PRORROGA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
					if(csProrrogaContrato == 'N'){
						return 'N/A';
					}else if (record.get('CONTADOR_PRORROGA') <1){
						
					}else{
						if(value=='P'){
							return 'Solicitud Pr�rroga';
						}else if(value=='A'){
							return 'Pr�rroga Aceptada';
						}else{
							return 'Pr�rroga Rechazada';
						}
					}
				}
			},
			{
				header: 'Causas Retorno a PYME',
				tooltip: 'Causas Retorno a PYME',
				dataIndex: 'CAUSAS_RETORNO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
	      xtype: 'actioncolumn',
			header: 'Acci�n',
			tooltip: 'Acci�n',
			width: 130,
			align: 'center',
			renderer: function(value, metadata, registro, rowindex, colindex, store) {
				if(registro.get('NUEVOESTATUS') !=''){
					return registro.get('MENSAJE');
				}else{
					return 'N/A';
				}
			},					
			items: [
				{
					getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {						
						if(registro.get('NUEVOESTATUS') !=''){
							this.items[0].tooltip = 'Autorizar Contrato';
							return 'correcto';
						}
					},
					handler: validaCedenteCesionario
				},{
					getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {						
						if(  (registro.get('CLAVE_ESTATUS') =='3'  ||  registro.get('CLAVE_ESTATUS') =='7' ||  
								registro.get('CLAVE_ESTATUS') =='9' ||  registro.get('CLAVE_ESTATUS') =='14' )  && 
								registro.get('CONTADOR_PRORROGA') <2 && registro.get('ESTATUS_PRORROGA')!='P' && csProrrogaContrato == 'S'){
							this.items[1].tooltip = 'Solicitar Pr�rroga';
							return 'icoReloj';
						}
					},
					handler: solicitarProrroga
				}
			]				
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {		
			buttonAlign: 'left',
			id: 'barraPaginacion',
			store: consultaData,
			emptyMsg: "No hay registros.",
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Imprimir PDF',
					id: 'btnImprimirPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url: '34ContratoCesionPDFCSV.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								tipo_archivo: 'CONSULTA'
							}),
							callback: procesarSuccessFailurePDF
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnImprimirCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url: '34ContratoCesionPDFCSV.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								tipo_archivo: 'CONSULTA'
							}),
							callback: procesarSuccessFailureCSV
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}
				]
			}
			
	});
	
	
 var Consentimiento = new Ext.Panel({
	id: 'Consentimiento',
	width: 600,
	height: 'auto',
	hidden: false,
	align: 'center',
	autoScroll: true
});

 var ColumnaAccion = {
	xtype: 'panel',
	id: 'ColumnaAccion',
	width: 600,
	height: 'auto',
	hidden: false,
	align: 'center',
	autoScroll: true
};

var AcuseFirma = new Ext.Panel({
	id: 'AcuseFirma',
	width: 940,
	height: 550,	
	height: 'auto',
	hidden: false,
	align: 'center',
	autoScroll: true
});




			
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 800,
		title: 'Criterios de B�squeda',
		frame: true,
		style: 'margin:0 auto;',
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {		
				
				var clave_epo = Ext.getCmp("clave_epo2");
				if (Ext.isEmpty(clave_epo.getValue()) ) {
					clave_epo.markInvalid('El valor de la EPO es requerido.');
					return;
				}		
					
				fp.el.mask('Enviando...', 'x-mask-loading');					
				
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'				
					})
				});
				
				Ext.getCmp('btnImprimirPDF').disable();
				Ext.getCmp('btnBajarPDF').hide();
				Ext.getCmp('btnImprimirCSV').disable();
				Ext.getCmp('btnBajarCSV').hide();
				
				var verPreAcuse1 = Ext.getCmp('verPreAcuse');
				if (verPreAcuse1) {		
					verPreAcuse1.destroy();		
				} 
				var verAcuse1 = Ext.getCmp('verAcuse');
				if (verAcuse1) {		
					verAcuse1.destroy();		
				} 
	
				
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34ContratoCesionext.jsp';
				}
			}
		]			
	});
		
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		style: 'margin:0 auto;',
		items: [		
			fp,
			NE.util.getEspaciador(20),
			grid,
			NE.util.getEspaciador(20)	
		]
		});
		
	//-------------------------------- ----------------- -----------------------------------
	catalogoEPOData.load();
	catalogoIFData.load();
	catalogoMonedaData.load();
	catalogoEstatusData.load();	
	catalogoTipoPlazoData.load();
	
	});