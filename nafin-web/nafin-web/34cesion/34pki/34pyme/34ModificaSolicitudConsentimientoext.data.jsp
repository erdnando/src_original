<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.text.*, 
		netropology.utilerias.usuarios.UtilUsr,
		netropology.utilerias.usuarios.Usuario,
		com.netro.exception.NafinException,
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray,
		java.io.File,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clavePyme = (String)request.getSession().getAttribute("iNoCliente");
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String infoRegresar = "";
//parametros para Modificar 
String clave_if = (request.getParameter("clave_if")!=null)?request.getParameter("clave_if"):"";
String clasiEpo = (request.getParameter("clasiEpo")!=null)?request.getParameter("clasiEpo"):"";
String numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String claveMoneda = (request.getParameter("claveMoneda")!=null)?request.getParameter("claveMoneda"):"";
String csTipoMontoChk = (request.getParameter("csTipoMontoChk")!=null)?request.getParameter("csTipoMontoChk"):"";
String montoDefinido = (request.getParameter("montoDefinido")!=null)?request.getParameter("montoDefinido"):"";
String claveTipoContratacion = (request.getParameter("claveTipoContratacion")!=null)?request.getParameter("claveTipoContratacion"):"";
String plazoContrato = (request.getParameter("plazoContrato")!=null)?request.getParameter("plazoContrato"):"";
String claveTipoPlazo = (request.getParameter("claveTipoPlazo")!=null)?request.getParameter("claveTipoPlazo"):"";
String fecha_vigencia_ini = (request.getParameter("fecha_vigencia_ini")!=null)?request.getParameter("fecha_vigencia_ini"):"";
String fecha_vigencia_fin = (request.getParameter("fecha_vigencia_fin")!=null)?request.getParameter("fecha_vigencia_fin"):"";
String claveVentanillaPago = (request.getParameter("claveVentanillaPago")!=null)?request.getParameter("claveVentanillaPago"):"";
String supAdmResob = (request.getParameter("supAdmResob")!=null)?request.getParameter("supAdmResob"):"";
String telefono = (request.getParameter("telefono")!=null)?request.getParameter("telefono"):"";
String objetoContrato = (request.getParameter("objetoContrato")!=null)?request.getParameter("objetoContrato"):"";
String comentarios = (request.getParameter("comentarios")!=null)?request.getParameter("comentarios"):"";
String CampoAdicional_1 = (request.getParameter("CampoAdicional_1")!=null)?request.getParameter("CampoAdicional_1"):"";
String CampoAdicional_2 = (request.getParameter("CampoAdicional_2")!=null)?request.getParameter("CampoAdicional_2"):"";
String CampoAdicional_3 = (request.getParameter("CampoAdicional_3")!=null)?request.getParameter("CampoAdicional_3"):"";
String CampoAdicional_4 = (request.getParameter("CampoAdicional_4")!=null)?request.getParameter("CampoAdicional_4"):"";
String CampoAdicional_5 = (request.getParameter("CampoAdicional_5")!=null)?request.getParameter("CampoAdicional_5"):"";
String archivo = (request.getParameter("archivo")!=null)?request.getParameter("archivo"):"";
String  montoContratoMin= (request.getParameter("montoContratoMin")!=null)?request.getParameter("montoContratoMin"):"";
String  montoContratoMax= (request.getParameter("montoContratoMax")!=null)?request.getParameter("montoContratoMax"):"";
String  montoN= (request.getParameter("montoN")!=null)?request.getParameter("montoN"):"";
String  montoD= (request.getParameter("montoD")!=null)?request.getParameter("montoD"):"";
String  montoE= (request.getParameter("montoE")!=null)?request.getParameter("montoE"):"";

//Nuevos Cambios Cesion Derechos Multimoneda.
String  montoContratoMinDL= (request.getParameter("montoContratoMinDL")!=null)?request.getParameter("montoContratoMinDL"):"";
String  montoContratoMaxDL= (request.getParameter("montoContratoMaxDL")!=null)?request.getParameter("montoContratoMaxDL"):"";
String  montoContratoMinEU= (request.getParameter("montoContratoMinEU")!=null)?request.getParameter("montoContratoMinEU"):"";
String  montoContratoMaxEU= (request.getParameter("montoContratoMaxEU")!=null)?request.getParameter("montoContratoMaxEU"):"";
//Fin Multimoneda
String  firmaContrato= (request.getParameter("firma_contrato")!=null)?request.getParameter("firma_contrato"):"";

//Fodea XXX-2014 empresas Representadas
String empresas= (request.getParameter("empresas")!=null)?request.getParameter("empresas"):"";
/*
System.out.println("clave_solicitud--"+clave_solicitud);
System.out.println("archivo--"+archivo);
System.out.println("clave_epo--"+clave_epo);
System.out.println("claveif --"+clave_if);
System.out.println("clasiEpo--"+clasiEpo);
System.out.println("numero_contrato--"+numero_contrato);
System.out.println("claveMoneda--"+claveMoneda);
System.out.println("csTipoMontoChk--"+csTipoMontoChk);
System.out.println("montoDefinido--"+montoDefinido);
System.out.println("claveTipoContratacion--"+claveTipoContratacion);
System.out.println("plazoContrato--"+plazoContrato);
System.out.println("claveTipoPlazo--"+claveTipoPlazo);
System.out.println("fecha_vigencia_ini--"+fecha_vigencia_ini);
System.out.println("fecha_vigencia_fin--"+fecha_vigencia_fin);
System.out.println("claveVentanillaPago--"+claveVentanillaPago);
System.out.println("supAdmResob--"+supAdmResob);
System.out.println("telefono--"+telefono);
System.out.println("objetoContrato--"+objetoContrato);
System.out.println("comentarios--"+comentarios);
System.out.println("CampoAdicional_1--"+CampoAdicional_1);
System.out.println("CampoAdicional_2--"+CampoAdicional_2);
System.out.println("CampoAdicional_3--"+CampoAdicional_3);
System.out.println("CampoAdicional_4--"+CampoAdicional_4);
System.out.println("CampoAdicional_5--"+CampoAdicional_5);
System.out.println("montoN--"+montoN);
System.out.println("montoD--"+montoD);
System.out.println("montoE--"+montoE);

System.out.println("************************************");
System.out.println("informacion--"+informacion);
System.out.println("************************************");
*/
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	
if(informacion.equals("valoresIniciales")  && !clave_solicitud.equals("") ){
	
	HashMap consultaAcuseSolicitud = cesionBean.consultarSolicitudConsentimiento(clave_solicitud);
	HashMap montosMonedasSolicitud = cesionBean.getMontosMonedasSolicitud(clave_solicitud);
	
	clave_epo = consultaAcuseSolicitud.get("clave_epo").toString();
	int indice =0;
	
	int numeroCampos=0;
		HashMap camposAdicionalesParametrizados = null;
		if(!clave_epo.equals("")){		
			
			camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
			numeroCampos = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));
			
			String clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
		
			indice = Integer.parseInt(montosMonedasSolicitud.get("indice")==null?"0":montosMonedasSolicitud.get("indice").toString());
			
				
		}	
		
		HashMap montosMoneda = (HashMap)montosMonedasSolicitud.get("montosMonedas"+0);		
	
		clave_epo  = consultaAcuseSolicitud.get("clave_epo").toString();
		JSONObject 	jsonObj	= new JSONObject();
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("clave_solicitud",consultaAcuseSolicitud.get("clave_solicitud").toString());			
		jsonObj.put("clave_epo",consultaAcuseSolicitud.get("clave_epo").toString());	
		jsonObj.put("clave_if",consultaAcuseSolicitud.get("clave_if").toString());	
		jsonObj.put("nombre_epo",consultaAcuseSolicitud.get("nombre_epo").toString());	
		jsonObj.put("nombre_if",consultaAcuseSolicitud.get("nombre_if").toString());
		
		jsonObj.put("firma_contrato",consultaAcuseSolicitud.get("firma_contrato").toString());
		jsonObj.put("clave_grupo_cesion",consultaAcuseSolicitud.get("clave_grupo_cesion").toString());
		//FODEA-024-2014 MOD()
		String nombrePyme=(String)consultaAcuseSolicitud.get("nombre_pyme").toString();
		String nombrePymeAux[]=nombrePyme.split(";");
		if(nombrePymeAux.length == 1){
			nombrePyme = nombrePyme.replaceAll(";","");
		}else{
			//nombrePyme = nombrePyme.replaceAll(";","");
		}
		jsonObj.put("nombre_pyme",nombrePyme);		
		//
		jsonObj.put("clasiEpo",consultaAcuseSolicitud.get("claveClasificacionEpo").toString());
		jsonObj.put("DesclasiEpo",consultaAcuseSolicitud.get("clasificacionEpo").toString());
		
		jsonObj.put("numero_contrato",consultaAcuseSolicitud.get("numero_contrato").toString());
		
		System.out.println("indice-----> "+indice);
		boolean csMultomonedaMontoDef = false;
		if(indice !=2){
			jsonObj.put("nombreMoneda",montosMoneda.get("nombreMoneda").toString());
			jsonObj.put("claveMoneda",montosMoneda.get("claveMoneda").toString());
		  jsonObj.put("montoDefinido",montosMoneda.get("montoContrato").toString());
		}else{
			jsonObj.put("nombreMoneda","MULTIMONEDA");
			jsonObj.put("claveMoneda","0");
			for(int n=0; n<indice; n++){
				montosMoneda = (HashMap)montosMonedasSolicitud.get("montosMonedas"+n);
				String valor = "";
				String valor2 = "";
				String valor3 = "";
				if (montosMoneda.get("claveMoneda").toString().equals("1")){
					valor = "monto0";
					valor2 = "montoMin0";
					valor3 = "montoMax0";
				}else if (montosMoneda.get("claveMoneda").toString().equals("54")){
					valor = "monto1";
					valor2 = "montoMin1";
					valor3 = "montoMax1";
				}else{
					valor = "monto2";
					valor2 = "montoMin2";
					valor3 = "montoMax2";
				}
				
				
				if(!montosMoneda.get("montoContrato").toString().equals("")){
					csMultomonedaMontoDef=true;
				}
				
				if(!montosMoneda.get("montoContratoMin").toString().equals("") || !montosMoneda.get("montoContratoMax").toString().equals("")){
					csMultomonedaMontoDef=false;
				}
				
				jsonObj.put(valor,(montosMoneda.get("montoContrato").toString()).replaceAll(",",""));
				jsonObj.put(valor2,(montosMoneda.get("montoContratoMin").toString()).replaceAll(",",""));
				jsonObj.put(valor3,(montosMoneda.get("montoContratoMax").toString()).replaceAll(",",""));
				
				}	
			jsonObj.put("montoDefinido","");
		}

		
		jsonObj.put("montoContratoMin",(montosMoneda.get("montoContratoMin").toString()).replaceAll(",",""));
		jsonObj.put("montoContratoMax",(montosMoneda.get("montoContratoMax").toString()).replaceAll(",",""));
		if((!montosMoneda.get("montoContrato").toString().equals("") && indice !=2) || (indice==2 && csMultomonedaMontoDef)){
			jsonObj.put("csTipoMontoChk","D");
		}else if((montosMoneda.get("montoContrato").toString().equals("") &&  indice !=2)  || (indice==2 && !csMultomonedaMontoDef)){
			jsonObj.put("csTipoMontoChk","M");
		}else {
			jsonObj.put("csTipoMontoChk","A");
		}
		jsonObj.put("claveTipoContratacion",consultaAcuseSolicitud.get("claveTipoContratacion").toString());
		jsonObj.put("plazoContrato",consultaAcuseSolicitud.get("plazoContrato").toString());
		String plazoContrato2  = consultaAcuseSolicitud.get("plazoContrato").toString();
		int tamanio  = plazoContrato2.length();		
		plazoContrato = plazoContrato2.substring(1, tamanio);
		jsonObj.put("claveTipoPlazo",consultaAcuseSolicitud.get("clavePlazoContrato").toString() );
		jsonObj.put("claveVentanillaPago",consultaAcuseSolicitud.get("claveVentanillaPago").toString());
		jsonObj.put("telefono",consultaAcuseSolicitud.get("numeroTelefono").toString());
		jsonObj.put("objetoContrato",consultaAcuseSolicitud.get("objeto_contrato").toString());
		jsonObj.put("comentarios",consultaAcuseSolicitud.get("comentarios").toString());
		jsonObj.put("empresas",consultaAcuseSolicitud.get("empresas").toString());
		jsonObj.put("fecha_vigencia_ini",consultaAcuseSolicitud.get("fecha_inicio_contrato").toString());
		jsonObj.put("fecha_vigencia_fin",consultaAcuseSolicitud.get("fecha_fin_contrato").toString());
		jsonObj.put("supAdmResob",consultaAcuseSolicitud.get("supAdmResob").toString());
		jsonObj.put("numeroCampos",String.valueOf(numeroCampos));		
		
		
		StringBuffer campo = new StringBuffer();
		for(int y = 0; y < numeroCampos; y++){
			int x = y+1;
			String idCampo = "CampoAdicional_"+x;
			String nombreCampo = camposAdicionalesParametrizados.get("nombre_campo_"+y).toString();			
			String tipoDato = camposAdicionalesParametrizados.get("tipo_dato_"+y).toString();
			String longitud = camposAdicionalesParametrizados.get("longitud_campo_"+y).toString();
			String camposAdicionales = "camposAdicionales"+x;
			String valorCampo = consultaAcuseSolicitud.get("campo_adicional_"+x).toString();
		
			String tiposdDato ="";
			if(tipoDato.equals("A"))  {  tiposdDato = "textfield"; }
			if(tipoDato.equals("N"))  {  tiposdDato = "numberfield"; }
			campo = new StringBuffer();
			campo.append( "	{ "+
							"	xtype: '"+tiposdDato+"',"+
							"	name: '"+idCampo+"',"+
							"	id: '"+idCampo+"',"+
							"	fieldLabel: '"+nombreCampo+"',"+
							"	allowBlank: true, "+
							"	hidden: false,"+
							"	maxLength: "+longitud+","+
							"	width: 150,"+
							"	msgTarget: 'side',"+
							//"	hidden: true, "+
							"	margins: '0 20 0 0'  "+
							"}		");			
					
			jsonObj.put(camposAdicionales,campo.toString());
			jsonObj.put(idCampo,consultaAcuseSolicitud.get("campo_adicional_"+x).toString());	
		
		}
		
		infoRegresar = jsonObj.toString();	
	
} else if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setCesionDerechos("S");
	infoRegresar = cat.getJSONElementos();	
	
} else  if(informacion.equals("CatalogoIF")  &&  !clave_epo.equals("")){
	
	CatalogoIF cat = new CatalogoIF();
	cat.setClave("ic_if");
	cat.setDescripcion("cg_razon_social");
	cat.setG_cs_cesion_derechos("S");
	cat.setIc_epo(clave_epo);
	infoRegresar = cat.getJSONElementos();

}else if(informacion.equals("CatalogoMoneda") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setOrden("ic_moneda");
	cat.setValoresCondicionIn("0,1,54,25", Integer.class);
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoContratacion") ){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_contratacion");
	cat.setCampoDescripcion("cg_nombre");
	cat.setTabla("cdercat_tipo_contratacion");
	cat.setOrden("ic_tipo_contratacion");
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoTipoPlazo") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("cdercat_tipo_plazo");
	cat.setCampoClave("ic_tipo_plazo");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("ic_tipo_plazo");
	infoRegresar = cat.getJSONElementos();	
		
}else if(informacion.equals("CatClasificacionEPO")  && !clave_epo.equals("") ){
	
	CatalogoClasificacionEPO cat = new CatalogoClasificacionEPO();
	cat.setCampoClave("ic_clasificacion_epo");
	cat.setCampoDescripcion("cg_area");
	cat.setClaveEpo(clave_epo);
	
	infoRegresar = cat.getJSONElementos();	
	
}else  if(informacion.equals("CatVentanillaPagoData")  && !clave_epo.equals("") ){
	
		CatalogoVentanillaPago cat = new CatalogoVentanillaPago();
		cat.setCampoClave("ic_ventanilla_pago");
		cat.setCampoDescripcion("cg_descripcion");
		cat.setClaveEpo(clave_epo);
		infoRegresar = cat.getJSONElementos();		

}else  if(informacion.equals("Modificar") ){

	List camposAdicionales  = new ArrayList();
	List montoMoneda  = new ArrayList();
	List claveMonedaHid = new ArrayList();
	
	if(!montoN.equals("")) {	montoMoneda.add(montoN);  claveMonedaHid.add("1"); }
	if(!montoE.equals("")) {	montoMoneda.add(montoE);  claveMonedaHid.add("25"); }
	if(!montoD.equals(""))  {	montoMoneda.add(montoD);  claveMonedaHid.add("54"); }
	
	if(!CampoAdicional_1.equals("") ) {  camposAdicionales.add(CampoAdicional_1); }
	if(!CampoAdicional_2.equals("") ) {  camposAdicionales.add(CampoAdicional_2); }
	if(!CampoAdicional_3.equals("") ) {  camposAdicionales.add(CampoAdicional_3); }
	if(!CampoAdicional_4.equals("") ) {  camposAdicionales.add(CampoAdicional_4); }
	if(!CampoAdicional_5.equals("") ) {  camposAdicionales.add(CampoAdicional_5); }
		
		

			HashMap datosCapturaSolicitud = new HashMap();
			datosCapturaSolicitud.put("numeroSolicitud", clave_solicitud);
			datosCapturaSolicitud.put("claveEpo", clave_epo);
			datosCapturaSolicitud.put("claveIfCesionario", clave_if);
			datosCapturaSolicitud.put("claveClasificacionEpo", clasiEpo);
			datosCapturaSolicitud.put("numeroContrato", numero_contrato);
			datosCapturaSolicitud.put("montoContrato", montoDefinido);
			datosCapturaSolicitud.put("claveMoneda", claveMoneda);
			datosCapturaSolicitud.put("claveTipoContratacion", claveTipoContratacion);
			datosCapturaSolicitud.put("fechaVigenciaContratoIni", fecha_vigencia_ini);
			datosCapturaSolicitud.put("fechaVigenciaContratoFin", fecha_vigencia_ini);
			datosCapturaSolicitud.put("objetoContrato", objetoContrato);
			datosCapturaSolicitud.put("comentariosSolicitud", comentarios);
			datosCapturaSolicitud.put("empresas", empresas);
			datosCapturaSolicitud.put("camposAdicionales", camposAdicionales); 
			datosCapturaSolicitud.put("csTipoMonto", csTipoMontoChk);
			datosCapturaSolicitud.put("montoContratoMin", montoContratoMin);
			datosCapturaSolicitud.put("montoContratoMax", montoContratoMax);
			datosCapturaSolicitud.put("claveMonedaHid", claveMonedaHid);
			datosCapturaSolicitud.put("montoMoneda", montoMoneda);//es es una Lista 
			datosCapturaSolicitud.put("plazoContrato", plazoContrato);
			datosCapturaSolicitud.put("claveTipoPlazo", claveTipoPlazo);
			datosCapturaSolicitud.put("claveVentanillaPago", claveVentanillaPago);
			datosCapturaSolicitud.put("supervisorAdministradorResidente", supAdmResob);
			datosCapturaSolicitud.put("numeroTelefono", telefono);
			//Agrega multimoneda Cesion Derechos
			datosCapturaSolicitud.put("montoContratoMinDL", montoContratoMinDL);
			datosCapturaSolicitud.put("montoContratoMaxDL", montoContratoMaxDL);
			datosCapturaSolicitud.put("montoContratoMinEU", montoContratoMinEU);
			datosCapturaSolicitud.put("montoContratoMaxEU", montoContratoMaxEU);
			datosCapturaSolicitud.put("firmaContrato", firmaContrato);
			
			
			
			cesionBean.modificarSolicitudConsentimiento(datosCapturaSolicitud);
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("numeroSolicitud",clave_solicitud);
			infoRegresar = jsonObj.toString();
			
}else if(informacion.equals("BuscaContrato") ){

	JSONObject 	resultado	= new JSONObject();
	boolean existe = cesionBean.existeNumeroContrato(clavePyme, numero_contrato, clave_epo);
	resultado.put("success", new Boolean(true));
	resultado.put("existe", new Boolean(existe));
	infoRegresar = resultado.toString();

}else if(informacion.equals("BuscaContratoCedido") ){

	JSONObject 	resultado	= new JSONObject();
	boolean existe = cesionBean.existeNumeroContratoCedido(numero_contrato, clave_epo);

	resultado.put("success", new Boolean(true));
	resultado.put("existe", new Boolean(existe));
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>

