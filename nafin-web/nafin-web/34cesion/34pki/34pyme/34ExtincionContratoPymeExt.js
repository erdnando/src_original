Ext.onReady(function(){
	var cedente="";//FODEA-024-2014 MOD()
	var MENSAJE_NUEVO="";//FODEA-024-2014 MOD()
	var cedente_em='';//FODEA-024-2014 MOD()
	var bandera = true;
	var numeroAcuse = '';
	var fechaCarga = '';
	var horaCarga = '';
	var usuarioCaptura = '';
	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
		
	var VisorAutoConsorcio = function (gridEditable,rowIndex,colIndex,item,event){
		var registro = gridEditable.getStore().getAt(rowIndex);
		var ic_solicitud = registro.get('CLAVE_SOLICITUD');
		var win = new NE.AutoConsorcio.VisorConsorcio(ic_solicitud, 'Firma_Convenio_Extincion', 'Estatus de Firmas Extinci�n');
	}

	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarPoderesIF = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		//forma.action = '/nafin/00archivos/22cotizador/instrucciones.pdf';
					Ext.Ajax.request({
							url: '../../34DescargaPoderes.data.jsp',
							params: Ext.apply({informacion:'PoderesExtincionContrato',clave_solicitud:clave_solicitud }),
							callback: procesarArchivoSuccess
						});
		}
		
	var cancelado = new Ext.form.FormPanel({
		id: 'cancelado',
		width: 400,			
		frame: true,
		hidden: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 400,
		style: 'margin:0 auto;',
		defaultType: 'textfield',			
		items:
			[
				{ 
					xtype:   'label',  
					html:		'La autentificaci�n no se llev� a cabo.<br/><b>PROCESO CANCELADO', 
					cls:		'x-form-item', 
					style: { 
						width:   		'100%', 
						textAlign: 		'center'
					} 
				}
			],
			monitorValid: true,
			buttons: [		
				{
					text: 'Salir',				
					handler: function() {
						window.location = '34ExtincionContratoPymeExt.jsp';
					}
				}
			]
	});	
	
	var textoFirmado;
	var titulo;
	var nuevoEstatus;
	var fechaExtincionContrato;
	var numeroCuenta;
	var numeroCuentaClabe;
	var bancoDeposito;
	var clave_solicitud;
	var descripcionEstatus;
	
	
		//Muestra el archivo de la Columna Contrato de Cesion 
	var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
		
	var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '34ContratoCesionPDFCSV.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}
	
	
		var procesarContratoExtincionPDF =  function(opts, success, response) {
		//var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		//btnGenerarPDFAcuse.setIconClass('');
		//AcuseFirma.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			 Ext.getCmp('Convenio').update('<table align="center" width="750" border="0" cellspacing="1" cellpadding="1"><embed src="'+ Ext.util.JSON.decode(response.responseText).urlArchivo+'" width="770" height="500"></embed></table>');
			//btnBajarPDFAcuse.show();
			//btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			
		} else {
			//btnGenerarPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailurePDFAcuse =  function(opts, success, response) {
		var btnImprimirPDFAcuse = Ext.getCmp('btnImprimirPDFAcuse');
		btnImprimirPDFAcuse.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			btnBajarPDFAcuse.show();
			btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFAcuse.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//procesar  
	var procesarEnviar = function(grid, rowIndex, colIndex, item, event) {
		var store = grid.getStore();	
		var jsonData = store.reader.jsonData;
		var clasificacionEpo = jsonData.clasificacionEpo;		
		var hayCamposAdicionales = jsonData.hayCamposAdicionales;	
		var registro = grid.getStore().getAt(rowIndex);
		var multiUsrPyme = 'N';//jsonData.multiUsrPyme; // F024-2015
		var pymePrincipal = jsonData.PYME_PRINCIPAL; //F024-2015
		var perfil = jsonData.PERFIL; //F024-2015
		var claveEstatus = registro.get('CLAVE_ESTATUS'); //F024-2015
		var firmasPendientes = parseInt(registro.get('FIRMAS_PENDIENTES')); //F024-2015
		cedente_em = registro.get('EMPRESAS');
		//estas son las variables globales para enviar al Preacuse
		fechaExtincionContrato = Ext.util.Format.date(registro.get('FECHA_EXTINCION'),'d/m/Y');
		numeroCuenta = registro.get('NUMERO_CUENTA');
		numeroCuentaClabe = registro.get('CUENTA_CLABE');
		bancoDeposito = registro.get('BANCO_DEPOSITO');
		clave_solicitud =registro.get('CLAVE_SOLICITUD');
		if(claveEstatus !=18){
			if( registro.get('FECHA_EXTINCION')==''  ) {
				 Ext.MessageBox.alert('Mensaje','El valor de la Fecha de Extinci�n de Contrato es requerido.');
				 return true; 		
			}
			if( registro.get('NUMERO_CUENTA')==''  ) {
				 Ext.MessageBox.alert('Mensaje','El valor de la Cuenta es requerido.');
				 return true; 		
			}
			if( registro.get('CUENTA_CLABE')=='' ) {
				Ext.MessageBox.alert('Mensaje','El valor de la Cuenta CLABE es requerido.');
				return true; 	
			}
			if( registro.get('BANCO_DEPOSITO')=='' ) {
				Ext.MessageBox.alert('Mensaje','El valor de el Banco de Dep�sito es requerido.');
				return true; 	
			}
		}
		var Xmoneda = eval('jsonData.moneda'+clave_solicitud);	
		registro.data['MONTO_MONEDA']=Xmoneda;
		registro.commit();				
	
		//aqui asigno los datos que se van a  modificar en el; 	
		if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S'; // F024-2015
		if(multiUsrPyme == 'S'){

			if(firmasPendientes > 1){
				if(claveEstatus == 17 && pymePrincipal == 'S' && perfil == 'ADMIN PYME'){
					nuevoEstatus = 18;
					descripcionEstatus = 'Extinci�n Pre-Solicitada';
				} else if(claveEstatus == 17 && pymePrincipal == 'S' && perfil == 'ADMIN CESION'){
					Ext.MessageBox.alert('Mensaje','Debe ser autorizado previamente por la PYME principal.');
					return; 
				} else if(claveEstatus == 17 && pymePrincipal != 'S'){
					Ext.MessageBox.alert('Mensaje','Debe ser autorizado previamente por la PYME principal.');
					return; 
				} else{
					nuevoEstatus = 18;
					descripcionEstatus = 'Extinci�n Pre-Solicitada';
				}
			} else if(firmasPendientes == 1){
				if(claveEstatus == 18){
					nuevoEstatus = 19;
					descripcionEstatus = 'Extinci�n Solicitada';
				}
			} else{
					Ext.MessageBox.alert('Mensaje','No se detectaron firmas pendientes.<br>Favor de comunicarse con sistemas.');
					return; 
			}

		} else if(multiUsrPyme =='N'){
			if(claveEstatus == 17){
				nuevoEstatus = 19;
				descripcionEstatus = 'Extinci�n Solicitada';
			}
		}

		var registrosPreAcu = []; //para agregar los registros al Preacuse  y acuse 		
		registrosPreAcu.push(registro);
		//se le carga informacion al grid de preacuse y al grid de totales
		consultaPreAcuseData.add(registrosPreAcu);
		titulo = 'PreAcuse de Extinci�n de Contrato';
		gridPreAcuse.show();
		gridEditable.hide();
		fp.hide();
		mensaje1.hide();		
		
		//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
		var cm = gridPreAcuse.getColumnModel();
		if(clasificacionEpo ==''){		
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), true);					
		}else{
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
		}
		var tmpEmpresas = replaceAll(registro.get('EMPRESAS'),';','<br>');
		textoFirmado = " Dependencia|Intermediario Financiero (Cesionario)|Pyme(Cedente)|No. Contrato|Monto / Moneda|Tipo de Contrataci�n|"+
									"	Fecha Inicio Contrato|Fecha Final Contrato|Plazo del Contrato|"+clasificacionEpo;												

		if(hayCamposAdicionales=='0'){
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);			
		}
		if(hayCamposAdicionales=='1'){
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)				
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
			textoFirmado +="|"+jsonData.Campo0;
		}
		if(hayCamposAdicionales=='2'){
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
			textoFirmado +="|"+jsonData.Campo0+"|"+jsonData.Campo1;
		}
		if(hayCamposAdicionales=='3'){
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)	
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
			textoFirmado +="|"+jsonData.Campo0+"|"+jsonData.Campo1+"|"+jsonData.Campo2;
		}
		if(hayCamposAdicionales=='4'){
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.Campo3);
			textoFirmado +="|"+jsonData.Campo0+"|"+jsonData.Campo1+"|"+jsonData.Campo2+"|"+jsonData.Campo3;
		}
		if(hayCamposAdicionales=='5'){
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);
			gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), false);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.Campo3);
			gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_5'),jsonData.Campo4);
			textoFirmado +="|"+jsonData.Campo0+"|"+jsonData.Campo1+"|"+jsonData.Campo2+"|"+jsonData.Campo3+"|"+jsonData.Campo4;
		}
		textoFirmado +=" Objeto del Contrato|Fecha de Extinci�n del Contrato|Cuenta|Cuenta CLABE|Banco de Dep�sito|Estatus\n";
		textoFirmado += 	registro.get('DEPENDENCIA') + "|" + registro.get('NOMBRE_CESIONARIO') + "|" + tmpEmpresas + "|" +
											registro.get('NUMERO_CONTRATO') + "|" +  registro.get('MONTO_MONEDA') + "|" +
											registro.get('TIPO_CONTRATACION') + "|" +  registro.get('FECHA_INICIO_CONTRATO') + "|" +	
											registro.get('FECHA_FIN_CONTRATO') + "|" + registro.get('PLAZO_CONTRATO') + "|" +	
											registro.get('CLASIFICACION_EPO') + "|" +	registro.get('CAMPO_ADICIONAL_1') + "|" +	
											registro.get('CAMPO_ADICIONAL_2') + "|" +	registro.get('CAMPO_ADICIONAL_3') + "|" +	
											registro.get('CAMPO_ADICIONAL_4') + "|" +	registro.get('CAMPO_ADICIONAL_5') + "|" +												
											registro.get('OBJETO_CONTRATO') + "|" +	registro.get('FECHA_EXTINCION') + "|" +	
											registro.get('NUMERO_CUENTA') + "|" +	registro.get('CUENTA_CLABE') + "|" +	
											registro.get('BANCO_DEPOSITO') + "|" + registro.get('CAUSAS_RECHAZO') + "|" +	
											registro.get('ESTATUS_SOLICITUD') + "|" ;
											
		Ext.getCmp('btnImprimirPDFAcuse').hide();
		Ext.getCmp('btnBajarPDFAcuse').hide();
		Ext.getCmp('btnSalir').hide();	

		// Mando a llamar al mensaje del preacuse
		Ext.Ajax.request({
			url: '34ExtincionContratoPyme.data.jsp',
			params: {
				informacion: 'MENSAJE_EXTINCION',
				nombreEPO: registro.get('DEPENDENCIA'),
				claveGrupo: registro.get('IC_GRUPO'),
				nombreCesionario: registro.get('NOMBRE_CESIONARIO')
			},
			callback: procesarMuestraMensaje
		});

	}

	function procesarMuestraMensaje(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			MENSAJE_NUEVO = Ext.util.JSON.decode(response.responseText).mensajeNuevo;
			mensaje2.update(MENSAJE_NUEVO);
			mensaje2.show();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//muestra el acuse en caso de que si se genero 
	function procesarSuccessFailureAcuse (opts, success, response) {
		var acuse;
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			if (jsondeAcuse != null){
				acuse = jsondeAcuse.acuse;								
			}		
			if(acuse !=''){
				numeroAcuse = jsondeAcuse.acuse;
				fechaCarga = jsondeAcuse.fecCarga;
				horaCarga = jsondeAcuse.horaCarga;
				usuarioCaptura = jsondeAcuse.captUser;
				var acuseCifras = [
						['N�mero de Acuse', jsondeAcuse.acuse],
						['Fecha de Carga', jsondeAcuse.fecCarga],
						['Hora de Carga', jsondeAcuse.horaCarga],
						['Usuario de Captura', jsondeAcuse.captUser]
					];
					
				storeCifrasData.loadData(acuseCifras);				
				var store = gridPreAcuse.getStore();			
				store.each(function(record) {							
						record.data['ESTATUS_SOLICITUD']=descripcionEstatus;
					record.commit();	
				});				
		
				gridPreAcuse.show();
				gridCifrasControl.show();
				gridEditable.hide();
				fp.hide();
				mensaje1.hide();
				
				Ext.getCmp('btnImprimirPDFAcuse').show();
				Ext.getCmp('btnBajarPDFAcuse').hide();
				Ext.getCmp('btnSalir').show();
				Ext.getCmp('btnEnviar').hide();
				Ext.getCmp('btnCancelar').hide();
			
			}else {						
				cancelado.show();
				gridPreAcuse.hide();
				gridCifrasControl.hide();
				gridEditable.hide();
				fp.hide();
				mensaje1.hide();
				//mensaje2.hide();
			}		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	// solicita el certificado para generar el acuse 
	var procesaAcuse = function(pkcs7, vtextoFirmado){
		
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else {
			Ext.Ajax.request({
				url : '34ExtincionContratoPyme.data.jsp',
				params : {
					informacion: 'ACUSE_CONTRATO',
					pkcs7: pkcs7,
					nuevoEstatus: nuevoEstatus,
					textoFirmado: vtextoFirmado,
					fechaExtincionContrato: fechaExtincionContrato,
					numeroCuenta: numeroCuenta,
					numeroCuentaClabe: numeroCuentaClabe,
					bancoDeposito: bancoDeposito,
					clave_solicitud: clave_solicitud	
				},
				callback: procesarSuccessFailureAcuse
			});		
		}
	}
	
	//cancelar solicitud 
	function procesarSuccessFailureCancelar (opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			gridEditable.getStore().commitChanges();
			Ext.MessageBox.alert('Cancelaci�n','Solicitud de Extinci�n de Contrato cancelada exitosamente.');
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Consultar',
					operacion: 'Generar', 								
					start: 0,
					limit: 15
				})
			});					
		} else {
			NE.util.mostrarConnError(response,opts);
		}				
	};
	
	
	//Cancelar proceso 
	var procesarCancelar = function(grid, rowIndex, colIndex, item, event) {
			var registro = grid.getStore().getAt(rowIndex);
			var solicitud = registro.get('CLAVE_SOLICITUD');
				Ext.Ajax.request({
			url: '34ExtincionContratoPyme.data.jsp',
			params: {
					informacion: 'CANCELAR',					
					clave_solicitud: solicitud	
				},
				callback: procesarSuccessFailureCancelar
			});	
	}

	//GENERAR ARCHIVO TODO CSV DE LA CONSULTA 
	var procesarSuccessFailureCSV =  function(opts, success, response) {
		var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');
		btnImprimirCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//GENERAR ARCHIVO TODO PDF DE LA CONSULTA 
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//descarga el Archivo de Contrato
	var procesarSuccessFailureContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	//descarga el Archivo de Contrato
	var descargarArchivoContrato = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '34ExtincionContratoPyme.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO',			
				clave_solicitud: clave_solicitud							
			}),
			callback: procesarSuccessFailureContrato
		});
	}
	
	//descarga archivo de Extincion Contrato Pyme 	
	var procesarSuccessFailureContratoExtincion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
		
	//descarga archivo de Extincion Contrato Pyme 
	var descargaContratoCesionPyme = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');
		var clave_if = registro.get('CLAVE_IF');
		Ext.Ajax.request({
			url: '34ExtincionContratoPyme.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO_CESION_PYME',
				tipo_archivo: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud,
				clave_epo: clave_epo,
				clave_if: clave_if				
			}),
			callback: procesarSuccessFailureContratoExtincion
		});
	}

	//Ventana Para ver el Contrato de Consentimiento
	var verConvenioExtincion = function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');		
		var ventana = Ext.getCmp('verConvenio');	
		if (ventana){
			ventana.show();	
		} else{
			new Ext.Window({
				layout: 'fit',
				width: 940,
				height: 550,			
				id: 'verConvenio',
				closeAction: 'hide',
				items: [					
					Convenio
				],
				title: 'Convenio de Extinci�n'	
			}).show();
		}
		var bodyPanel = Ext.getCmp('Convenio').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure',
			function(el, response) {
				bodyPanel.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		Ext.Ajax.request({
				url: '../34ConvenioExtincionCesionDerechosExt.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'CONTRATO_EXTINCION',
					clave_solicitud: clave_solicitud,
					clave_epo: clave_epo		
				}),
				callback: procesarContratoExtincionPDF
			});
		/*
		mgr.update({
			url: '/nafin/34cesion/34pki/34ConvenioExtincionCesionDerechosExt.jsp',	
			scripts: true,
			params: {
				clave_solicitud: clave_solicitud,
				clave_epo: clave_epo				
			},
			indicatorText: 'Convenio de Extinci�n'
		});	*/
	}
	
	function formatoPymes(val){
		if(val != ''){
			return replaceAll(val,';','<br>');
		}
		return val;
	}
	
	var Convenio = new Ext.Panel({
		id: 'Convenio',
		width: 600,
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true
	});

	// Poderes de la Pyme 
	var VerPoderesPyme = function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLAVE_PYME');
		var solicitud = registro.get('CLAVE_SOLICITUD');
		var rfcPyme = registro.get('RFC_PYME');	
		var grupo_cesion = registro.get('IC_GRUPO');	
		var no_contrato = registro.get('NUMERO_CONTRATO');	
		
		var parametros = "clavePyme="+clavePyme+"&rfcPyme="+rfcPyme+"&grupo_cesion="+grupo_cesion+"&no_contrato="+no_contrato;
		 
		
		var ventana = Ext.getCmp('VerPoderesPyme');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
					layout: 'fit',
					width: 400,
					height: 200,		
					style: 'margin:0 auto;',
					id: 'VerPoderesPyme',
					closeAction: 'hide',
					items: [					
						PanelPoderesPyme
					],
					title: 'Ver Poderes Pyme  '					
			}).show();
		}	
		
	var pabelBody = Ext.getCmp('PanelPoderesPyme').body;
		var mgr = pabelBody.getUpdater();
		mgr.on('failure', 
		function(el, response) {
			pabelBody.update('');
			NE.util.mostrarErrorResponse(response);
		});		
		mgr.update({
			url: '34SolicConsVerPoderesPymeExt.jsp?'+parametros,						
			indicatorText: 'Cargando Ver Poderes'
		});	
	}
	
	var PanelPoderesPyme = new Ext.Panel({
		id: 'PanelPoderesPyme',
		width: 400,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	});
	
	//GRID GENERAL
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var  clasificacionEpo = jsonData.clasificacionEpo;	
		var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
			
		if (arrRegistros != null) {
			if (!gridEditable.isVisible()) {
				gridEditable.show();
				mensaje1.show();
				cedente=jsonData.CEDENTE;

			}	
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridEditable.getColumnModel();
			if(clasificacionEpo ==''){		
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), true);					
			}else{
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
			}
			if(hayCamposAdicionales=='0'){
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)				
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
			}
			if(hayCamposAdicionales=='2'){
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
			}
			if(hayCamposAdicionales=='3'){
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true)	
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
			}
			if(hayCamposAdicionales=='4'){
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.Campo3);
			}				
			if(hayCamposAdicionales=='5'){
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);
				gridEditable.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), false);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.Campo0);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.Campo1);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.Campo2);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.Campo3);
				gridEditable.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_5'),jsonData.Campo4);
			}		
			
			var el = gridEditable.getGridEl();
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');	
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
				
			if(store.getTotalCount() > 0) {				
				btnImprimirPDF.enable();
				btnImprimirCSV.enable();					
				btnBajarPDF.hide();
				btnBajarCSV.hide();
				el.unmask();
			} else {	
				btnImprimirPDF.disable();
				btnImprimirCSV.disable();
				btnBajarPDF.hide();
				btnBajarCSV.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}				
		}			
	}
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		fields: [
			{	name: 'etiqueta'},
			{	name: 'informacion'}
		  ]
	 });	 
	
	//Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 150,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 520,
		height: 250,
		style: 'margin:0 auto;',
		autoHeight : true,
		title: 'Cifras de Control',
		frame: true
	});
		
	var consultaPreAcuseData = new Ext.data.ArrayStore({
	fields: [
		{name: 'CLAVE_SOLICITUD',type: 'float'},
		{name: 'CLAVE_EPO',type: 'float'},
		{name: 'CLAVE_IF',type: 'float'},
		{name: 'CLAVE_PYME',type: 'float'},
		{name: 'DEPENDENCIA'},
		{name: 'NOMBRE_CESIONARIO'},
		{name: 'EMPRESAS'},
		{name: 'NUMERO_CONTRATO'},
		{name: 'MONTO_MONEDA'},
		{name: 'TIPO_CONTRATACION'},
		{name: 'FECHA_INICIO_CONTRATO' ,type: 'date', dateFormat: 'd/m/Y'},
		{name: 'FECHA_FIN_CONTRATO' ,type: 'date', dateFormat: 'd/m/Y'},
		{name: 'PLAZO_CONTRATO'},
		{name: 'CLASIFICACION_EPO'},
		{name: 'CAMPO_ADICIONAL_1'},
		{name: 'CAMPO_ADICIONAL_2'},
		{name: 'CAMPO_ADICIONAL_3'},
		{name: 'CAMPO_ADICIONAL_4'},
		{name: 'CAMPO_ADICIONAL_5'},
		{name: 'OBJETO_CONTRATO'},
		{name: 'FECHA_EXTINCION' ,type: 'date', dateFormat: 'd/m/Y'},
		{name: 'NUMERO_CUENTA'},
		{name: 'CUENTA_CLABE'},
		{name: 'BANCO_DEPOSITO'},
		{name: 'CAUSAS_RECHAZO'},
		{name: 'ESTATUS_SOLICITUD'},			
		{name: 'CLAVE_ESTATUS'},
		{name: 'CEDENTE'}//FODEA-024-2014
		]
	});
	
	var gridPreAcuse = new Ext.grid.EditorGridPanel({
		store: consultaPreAcuseData,
		hidden: true,
		margins: '20 0 0 0',
		title: titulo,
		columns: [
			{
				header: 'Dependencia',
				tooltip: 'Dependencia',
				dataIndex: 'DEPENDENCIA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Intermediario Financiero (Cesionario)',
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'NOMBRE_CESIONARIO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Pyme (Cedente)',
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'EMPRESAS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left',
				renderer: formatoPymes
			},
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			}	,	
			{
				header: 'Monto / Moneda',
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'			
			},
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Fecha Inicio Contrato',
				tooltip: 'Fecha Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha Final Contrato',
				tooltip: 'Fecha Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}	,
			{
				header: 'Plazo del Contrato',
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			}	,
			{
				header: 'clasificacionEpo', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'clasificacionEpo',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			}	,
			{
				header: 'campo Adicional 1', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 1',
				dataIndex: 'CAMPO_ADICIONAL_1',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},	
			{
				header: 'campo Adicional 2', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 2',
				dataIndex: 'CAMPO_ADICIONAL_2',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'campo Adicional 3', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 3',
				dataIndex: 'CAMPO_ADICIONAL_3',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			},
			{
				header: 'campo Adicional 4', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 4',
				dataIndex: 'CAMPO_ADICIONAL_4',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'campo Adicional 5', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 5',
				dataIndex: 'CAMPO_ADICIONAL_5',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'Objeto del Contrato', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			},
			{				
				header : 'Fecha de Extinci�n del Contrato', // es es editable 
				tooltip: 'Fecha de Extinci�n del Contrato',
				dataIndex : 'FECHA_EXTINCION',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: campoFechas,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Cuenta', // es es editable 
				tooltip: 'Cuenta',
				dataIndex : 'NUMERO_CUENTA',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'
			},
			{				
				header : 'Cuenta CLABE', // es es editable 
				tooltip: 'Cuenta CLABE',
				dataIndex : 'CUENTA_CLABE',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'
			},
			{				
				header : 'Banco de Dep�sito', // es es editable 
				tooltip: 'Banco de Dep�sito',
				dataIndex : 'BANCO_DEPOSITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center'				
			},		
			{
				header: 'Estatus', 
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS_SOLICITUD',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 943,	
		style: 'margin:0 auto;',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Enviar',
					id: 'btnEnviar',					
					handler: function(boton, evento) {					
					
						NE.util.obtenerPKCS7(procesaAcuse, textoFirmado);
						
					}
				},	
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnImprimirPDFAcuse',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34ExtincionContratoPyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ARCHIVO_ACUSE',
								tipo_archivo: 'ACUSE_ENVIO_SOL',
								clave_solicitud:clave_solicitud,
								cedente:cedente,//FODEA-024-2014 MOD(),
								cedente_em:cedente_em,//FODEA-024-2014 MOD(),
								numeroAcuse: numeroAcuse, //F024-2015
								fechaCarga: fechaCarga, //F024-2015
								horaCarga: horaCarga, //F024-2015
								usuarioCaptura: usuarioCaptura, //F024-2015
								mensaje:MENSAJE_NUEVO
							}),
							callback: procesarSuccessFailurePDFAcuse
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFAcuse',
					hidden: true
				},
				'-',
				{
					text: 'Cancelar',
					xtype: 'button',
					id: 'btnCancelar',
					handler: function() {						
						window.location = '34ExtincionContratoPymeExt.jsp';
					}
				},
				{
					text: 'Salir',
					xtype: 'button',
					id: 'btnSalir',
					handler: function() {						
						window.location = '34ExtincionContratoPymeExt.jsp';
					}
				}				
			]	
		}
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34ExtincionContratoPyme.data.jsp',
		baseParams: {
			informacion: 'Consultar',
			operacion: 'Generar'
		},
		fields: [
			{name: 'EMPRESAS'},
			{name: 'CLAVE_SOLICITUD',type: 'float'},
			{name: 'CLAVE_EPO',type: 'float'},
			{name: 'CLAVE_IF',type: 'float'},
			{name: 'CLAVE_PYME',type: 'float'},
			{name: 'DEPENDENCIA'},
			{name: 'NOMBRE_CESIONARIO'},
			{name: 'NUMERO_CONTRATO'},
			{name: 'MONTO_MONEDA'},
			{name: 'TIPO_CONTRATACION'},
			{name: 'FECHA_INICIO_CONTRATO' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_FIN_CONTRATO' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO_CONTRATO'},
			{name: 'CLASIFICACION_EPO'},
			{name: 'CAMPO_ADICIONAL_1'},
			{name: 'CAMPO_ADICIONAL_2'},
			{name: 'CAMPO_ADICIONAL_3'},
			{name: 'CAMPO_ADICIONAL_4'},
			{name: 'CAMPO_ADICIONAL_5'},
			{name: 'OBJETO_CONTRATO'},
			{name: 'FECHA_EXTINCION' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NUMERO_CUENTA'},
			{name: 'CUENTA_CLABE'},
			{name: 'BANCO_DEPOSITO'},
			{name: 'CAUSAS_RECHAZO'},
			{name: 'ESTATUS_SOLICITUD'},	 					
			{name: 'CLAVE_ESTATUS'}, //F024-2015
			{name: 'FIRMAS_PENDIENTES'}, //F024-2015
			{name: 'IC_GRUPO'}, //F024-2015
			{name: 'CENDENTE'}, //FODEA-024-2014 MOD()
			{name: 'RFC_PYME'},
			{name: 'NUM_REP'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,	
				beforeLoad:	{ // esto es para realizar la actualizacion  en el grid
					fn: function(store, options){
						Ext.apply(options.params, {						
							clave_epo: Ext.getCmp("clave_epo2").getValue(),
							clave_if: Ext.getCmp("clave_if2").getValue(),
							numero_contrato: Ext.getCmp("numero_contrato2").getValue(),
							clave_moneda: Ext.getCmp("clave_moneda2").getValue(),
							clave_contratacion: Ext.getCmp("clave_contratacion2").getValue()
						});
					}	
				},
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	
	//Seleccionado Pyme a Rechazado IF
	var gridEditable = new Ext.grid.EditorGridPanel({
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		title: '',
		columns: [
			{
				header: 'Dependencia',
				tooltip: 'Dependencia',
				dataIndex: 'DEPENDENCIA',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Intermediario Financiero (Cesionario)',
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'NOMBRE_CESIONARIO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda',
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
				 	var jsonData = store.reader.jsonData;	
					var valor = record.get('CLAVE_SOLICITUD');
					var Xmoneda = eval('jsonData.moneda'+valor);	
					return Xmoneda;									
				}
			},
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Fecha Inicio Contrato',
				tooltip: 'Fecha Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha Final Contrato',
				tooltip: 'Fecha Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Plazo del Contrato',
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'clasificacionEpo', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'Clasificaci�n EPO',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'				
			},
			{
				header: 'campo Adicional 1', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 1',
				dataIndex: 'CAMPO_ADICIONAL_1',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'				
			},	
			{
				header: 'campo Adicional 2', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 2',
				dataIndex: 'CAMPO_ADICIONAL_2',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'campo Adicional 3', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 3',
				dataIndex: 'CAMPO_ADICIONAL_3',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'				
			},
			{
				header: 'campo Adicional 4', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 4',
				dataIndex: 'CAMPO_ADICIONAL_4',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'campo Adicional 5', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'campo Adicional 5',
				dataIndex: 'CAMPO_ADICIONAL_5',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'Objeto del Contrato', //ESTE TITULO ES DINAMICO DEACUERDO A UNA PARAMETRIZACION
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Contrato de Cesi�n',
				tooltip: 'Contrato de Cesi�n',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},
						handler: descargarContratoCesion
					}
				]				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Convenio de Extinci�n',
				tooltip: 'Convenio de Extinci�n',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	
						handler: verConvenioExtincion
					}
				]				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Firma Convenio Extinci�n',
				tooltip: 'Firma Convenio Extinci�n',
				width: 140,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	
						handler: VisorAutoConsorcio
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	
						handler: descargarArchivoContrato
					}
				]				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Poderes',
				tooltip: 'Poderes',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	
						handler: VerPoderesPyme
					}
				]
			},
			{
	      xtype: 'actioncolumn',
				header: 'Poderes IF Extinci�n',
				tooltip: 'Poderes IF Extinci�n',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	
						handler: procesarPoderesIF
					}
				]
			},
			{				
				header : 'Fecha de Extinci�n del Contrato', // es es editable 
				tooltip: 'Fecha de Extinci�n del Contrato',
				dataIndex : 'FECHA_EXTINCION',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: campoFechas,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{				
				header : 'Cuenta', // es es editable 
				tooltip: 'Cuenta',
				dataIndex : 'NUMERO_CUENTA',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: {
					xtype : 'textfield',
					//maxValue : 999999999999.99,
					allowBlank: false
				}
			},
			{				
				header : 'Cuenta CLABE', // es es editable 
				tooltip: 'Cuenta CLABE',
				dataIndex : 'CUENTA_CLABE',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: {
					xtype : 'textfield',
					//maxValue : 999999999999.99,
					
					allowBlank: false
				}
			},
			{
				header : 'Banco de Dep�sito', // es es editable 
				tooltip: 'Banco de Dep�sito',
				dataIndex : 'BANCO_DEPOSITO',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: {
					xtype : 'textfield',					
					allowBlank: false
				}
			},
			{
				header: 'Causa del Rechazo', 
				tooltip: 'Causa del Rechazo',
				dataIndex: 'CAUSAS_RECHAZO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},
			{
				header: 'Estatus', 
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS_SOLICITUD',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'				
			},
			{
				xtype: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					var jsonData = store.reader.jsonData;	
					var mostrarNA = "";
					var recordFound = true;
					var multiUsrPyme = 'N';//jsonData.multiUsrPyme; // F024-2015
					var pymePrincipal = jsonData.PYME_PRINCIPAL;
					var perfil = jsonData.PERFIL;
					var claveEstatus = registro.get('CLAVE_ESTATUS');
					var firmasPendientes = parseInt(registro.get('FIRMAS_PENDIENTES')); //F024-2015
					var solicAutoXUsuario = jsonData.solicitudesAutorizadasPorUsuario;
					var numSolAut = jsonData.numSolAut;						
					if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S'; // F024-2015
					if(multiUsrPyme == 'S'){
						if(firmasPendientes > 0){
							if(claveEstatus != 19){
								if(pymePrincipal == 'S'){
									if(perfil == 'ADMIN PYME'){
										if(claveEstatus == 18){
											mostrarNA = "N/A";
										}
									} else{
										if(claveEstatus == 17){
										mostrarNA = "N/A";
									} else if(claveEstatus == 18){
											recordFound = false;
											for( var i =0; i<numSolAut; i++){
												var datoAutoXUsuario = eval('solicAutoXUsuario.numeroSolicitud'+i);
												if(datoAutoXUsuario ==registro.get('CLAVE_SOLICITUD')){
													recordFound = true;
												}
											}
											if (recordFound== true){
												mostrarNA = "N/A";
											}
										}
									}
								} else{
									if(claveEstatus == 17){
										mostrarNA = "N/A";
									} else if(claveEstatus == 18){
							recordFound = false;
							for( var i =0; i<numSolAut; i++){									
								var datoAutoXUsuario = eval('solicAutoXUsuario.numeroSolicitud'+i);											
								if(datoAutoXUsuario ==registro.get('CLAVE_SOLICITUD')){
									recordFound = true;
								}	
							}							
										if (recordFound== true){
								mostrarNA =	"N/A";
							}
									}
								}
							} else{
								mostrarNA = "N/A";
							}//if(claveEstatus != 19)
						} else{
							mostrarNA = "N/A";
						}				
					} else if(multiUsrPyme == 'N'){
						if(claveEstatus != 17){
							mostrarNA = "N/A";
						}
					}
					return mostrarNA;						
				},
				items: [
					{ //para enviar 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store){
							var jsonData = store.reader.jsonData;	
							var mostrarNA = "";
							var recordFound = false;
							var multiUsrPyme = 'N';//jsonData.multiUsrPyme; // F024-2015
							var claveEstatus = registro.get('CLAVE_ESTATUS');
							var firmasPendientes = parseInt(registro.get('FIRMAS_PENDIENTES')); //F024-2015
							var solicAutoXUsuario = jsonData.solicitudesAutorizadasPorUsuario;
							var numSolAut = jsonData.numSolAut;	
							var pymePrincipal = jsonData.PYME_PRINCIPAL;
							var perfil = jsonData.PERFIL;
							if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S'; // F024-2015
							if(multiUsrPyme =='S'){
								if(firmasPendientes > 0){
									if(claveEstatus != 19){
										if(pymePrincipal == 'S'){
											if(perfil == 'ADMIN PYME'){
												if(claveEstatus == 17){
									this.items[0].tooltip = 'Enviar';
									return 'icoAceptar';	
								}
											} else{
												if(claveEstatus == 18){
													recordFound = false;
													for( var i =0; i<numSolAut; i++){
														var datoAutoXUsuario = eval('solicAutoXUsuario.numeroSolicitud'+i);
														if(datoAutoXUsuario ==registro.get('CLAVE_SOLICITUD')){
															recordFound = true;
							}	
													}
													if (recordFound== false){
														this.items[0].tooltip = 'Enviar';
														return 'icoAceptar';
													}
												}
											}
										} else{
											if(claveEstatus == 18){
								recordFound = false;
								for( var i =0; i<numSolAut; i++){									
									var datoAutoXUsuario = eval('solicAutoXUsuario.numeroSolicitud'+i);											
									if(datoAutoXUsuario ==registro.get('CLAVE_SOLICITUD')){
										recordFound = true;
									}		
								}								
												if (recordFound== false){
													this.items[0].tooltip = 'Enviar';
													return 'icoAceptar';
												}
											}
										}
									}//if(claveEstatus != 19)
								}
							} else if(multiUsrPyme == 'N'){
								if((perfil == 'ADMIN PYME' || perfil == 'ADMIN CESION') && claveEstatus == 17){
									this.items[0].tooltip = 'Enviar';
									return 'icoAceptar';									
								}								
							}
						},
						handler: procesarEnviar
					},
					//Cancelar
					{  
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							var jsonData = store.reader.jsonData;	
							var perfil = jsonData.PERFIL;
							var mostrarNA ="";
							var recordFound =false;
							var multiUsrPyme = 'N';//jsonData.multiUsrPyme; // F024-2015
							var claveEstatus =registro.get('CLAVE_ESTATUS');	
							var solicAutoXUsuario = jsonData.solicitudesAutorizadasPorUsuario;
							var numSolAut = jsonData.numSolAut;	
							if(registro.get('NUM_REP')!='0' && registro.get('NUM_REP')!='1')multiUsrPyme = 'S'; // F024-2015
							if(perfil != 'ADMIN CESION' && multiUsrPyme =='S'  &&  claveEstatus ==18 ){
								recordFound = false;
								for( var i =0; i<numSolAut; i++){									
									var datoAutoXUsuario = eval('solicAutoXUsuario.numeroSolicitud'+i);											
									if(datoAutoXUsuario ==registro.get('CLAVE_SOLICITUD')){
										recordFound = true;
									}	
								}										
								if (recordFound== false) {									
									this.items[1].tooltip = 'Cancelar';
									return 'borrar';									
								}								
							}						
						},
						handler: procesarCancelar
					}
				]				
			}			
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,	
		style: 'margin:0 auto;',
		frame: true,
		listeners: {	
			beforeedit: function(e){	
			//esto es para evitar que los campos sean editables cuando el estatus es 18
			
					var record = e.record;
					var gridEditable = e.gridEditable; 
					var campo= e.field;
					if(campo == 'BANCO_DEPOSITO' || campo == 'CUENTA_CLABE' 
						|| campo == 'NUMERO_CUENTA'  || campo == 'FECHA_EXTINCION'){
						var accion=record.data['CLAVE_ESTATUS'];
						if(accion=='18'){//retorno
							return false;
						}else {
							return true;
						}
					}				
				}
			
				
			
		},	
		
		bbar: {		
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,				
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnImprimirPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url: '34ExtincionContratoPyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								tipo_archivo: 'PDF'
							}),
							callback: procesarSuccessFailurePDF
						});
					}
				},				
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnImprimirCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url: '34ExtincionContratoPyme.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								tipo_archivo: 'CSV'
							}),
							callback: procesarSuccessFailureCSV
						});
					}
				},				
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}
			]
		}			
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ExtincionContratoPyme.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ExtincionContratoPyme.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ExtincionContratoPyme.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoContratacionData = new Ext.data.JsonStore({
		id: 'catalogoContratacionDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ExtincionContratoPyme.data.jsp',
		baseParams: {
			informacion: 'CatalogoContratacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma = [{
			xtype: 'combo',
			name: 'clave_epo',
			id: 'clave_epo2',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
		displayField:   'descripcion',
		valueField:     'clave',
		hiddenName:     'clave_epo',
			emptyText: 'Seleccione...',					
		triggerAction:  'all',
		forceSelection: true,
			typeAhead: true,
			allowBlank: true,
		minChars:       1,
		store:          catalogoEPOData,
		tpl:            NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
				fn: function(combo){
						var tipoContra = Ext.getCmp('clave_contratacion2');
						tipoContra.setValue('');
						tipoContra.setDisabled(false);
						tipoContra.store.load({
						params:{
								clave_epo:combo.getValue()
							}
						});
					}
				}
			}			
	},{
			xtype: 'combo',
			name: 'clave_if',
			id: 'clave_if2',
			fieldLabel: 'Nombre del Cesionario',
			mode: 'local', 
		displayField:   'descripcion',
		valueField:     'clave',
		hiddenName:     'clave_if',
			emptyText: 'Seleccione...',					
		triggerAction:  'all',
		forceSelection: true,
			typeAhead: true,
			allowBlank: true,
		minChars:       1,
		store:          catalogoIFData,
		tpl:            NE.util.templateMensajeCargaCombo
	},{
		xtype:          'textfield',
			name: 'numero_contrato',
			id: 'numero_contrato2',
			fieldLabel: 'N�mero de Contrato',
			allowBlank: true,
			hidden: false,
			maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
	},{
			xtype: 'combo',
			name: 'clave_moneda',
			id: 'clave_moneda2',
			fieldLabel: 'Moneda',
			mode: 'local', 
		displayField:   'descripcion',
		valueField:     'clave',
		hiddenName:     'clave_moneda',
			emptyText: 'Seleccione...',					
		forceSelection: true,
		triggerAction:  'all',
			typeAhead: true,
		minChars:       1,
			allowBlank: true,
		store:          catalogoMonedaData,
		tpl:            NE.util.templateMensajeCargaCombo
	},{
			xtype: 'combo',
			name: 'clave_contratacion',
			id: 'clave_contratacion2',
			fieldLabel: 'Tipo de Contrataci�n',
			mode: 'local', 
		displayField:   'descripcion',
		valueField:     'clave',
		hiddenName:     'clave_contratacion',
			emptyText: 'Seleccione...',					
		forceSelection: true,
		triggerAction:  'all',
			typeAhead: true,
		minChars:       1,
		allowBlank:     true,
		store:          catalogoContratacionData,
		tpl:            NE.util.templateMensajeCargaCombo
	},{
		xtype:              'compositefield',
		fieldLabel:         'Fecha Firma de Contrato',
		msgTarget:          'side',
		combineErrors:      false,
		items: [{
			width:           95,
			xtype:           'datefield',
			id:              'fecha_inicio',
			name:            'fecha_inicio',
			msgTarget:       'side',
			vtype:           'rangofecha',
			minValue:        '01/01/1901',
			campoFinFecha:   'fecha_final',
			margins:         '0 20 0 0',
			allowBlank:      true,
			startDay:        0
		},{
			xtype:           'displayfield',
			value:           'a',
			width:           20
		},{
			width:           95,
			xtype:           'datefield',
			id:              'fecha_final',
			name:            'fecha_final',
			msgTarget:       'side',
			vtype:           'rangofecha',
			minValue:        '01/01/1901',
			campoInicioFecha:'fecha_inicio',
			margins:         '0 20 0 0',
			allowBlank: true,
			startDay:        1
		},{
			xtype:           'displayfield',
			value:           'dd/mm/aaaa',
			width:           30
		}]
	}];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		style: 'margin:0 auto;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
				
					var clave_epo = Ext.getCmp("clave_epo2");
					if (Ext.isEmpty(clave_epo.getValue()) ) {
						clave_epo.markInvalid('El valor de la EPO es requerido.');
						return;
					}	
					
					var clave_if = Ext.getCmp("clave_if2");
					if (Ext.isEmpty(clave_if.getValue()) ) {
						clave_if.markInvalid('El valor de la Cesionario es requerido.');
						return;
					}		
					
					// Estos if son para validar las fechas
					if(!Ext.getCmp('forma').getForm().isValid()){
						return;
					}
					if(Ext.getCmp('fecha_inicio').getValue() == '' && !Ext.getCmp('fecha_final').getValue() == ''){
						Ext.getCmp('fecha_inicio').markInvalid('El campo es obligatorio.');
						return;
					}
					if(Ext.getCmp('fecha_final').getValue() == '' && !Ext.getCmp('fecha_inicio').getValue() == ''){
						Ext.getCmp('fecha_final').markInvalid('El campo es obligatorio.');
						return;
					}

					fp.el.mask('Enviando...', 'x-mask-loading');					
					bandera = true;
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							operacion: 'Generar', 								
									start: 0,
									limit: 15
						})
					});
					
					Ext.getCmp('btnImprimirPDF').disable();
					Ext.getCmp('btnBajarPDF').hide();
					Ext.getCmp('btnImprimirCSV').disable();
					Ext.getCmp('btnBajarCSV').hide();
				
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34ExtincionContratoPymeExt.jsp';
				}
			}
		]			
	});
	
	var mensaje1 = new Ext.Panel({
		name: 'mensaje1',
		id: 'mensaje1',
		width: 943,
		hidden: true,
		style: 'margin:0 auto;',
		frame: true,
		html:'	* Favor de Verificar el N�mero de Cuenta en el contrato con la Dependencia	'
	});
	
	var mensaje2 = new Ext.Panel({
		name: 'mensaje2',
		id: 'mensaje2',
		width: 943,
		hidden: true,
		style: 'margin:0 auto;',
		frame: true,
		html:'De conformidad con lo se�alado en el (los) Contrato(s) cuyos datos se se�alan en esta pantalla, yo como representante legal de la empresa, solicito a AF BANREGIO, S.A. DE C.V. SOFOM E.R. su validaci�n/aceptaci�n para extinguir el Contrato de Derechos de Cobro. <br>'+
				'Al mismo tiempo manifiesto (amos) que cuento (amos) con las facultades para actos de dominio necesarias para llevar a cabo la cesi�n de derechos de cobro conforme se acredita mediante los poderes anexos a la presente, mismos que no han sido revocados, modificados o limitados en forma alguna a la presente fecha.'
		});		
		
		
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		items: [		
			fp,
			NE.util.getEspaciador(20),
			mensaje1,
			NE.util.getEspaciador(20),
			gridEditable,
			gridPreAcuse, 
			cancelado,
			NE.util.getEspaciador(20),
			gridCifrasControl,
			NE.util.getEspaciador(20),
			mensaje2
		]
	});
		
	catalogoEPOData.load();
	catalogoIFData.load();
	catalogoMonedaData.load();	
		
	});