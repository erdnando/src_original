<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.text.*, 		
		com.netro.exception.NafinException,
		net.sf.json.JSONArray,
		java.io.File,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=strDirecVirtualCSS%><%=strClase%>">
<%
Map documentacionPyme = null;
String hayDocumentos = "";
try {
	
	
	String clavePyme = request.getParameter("clavePyme") == null?"":(String)request.getParameter("clavePyme");
	String solicitud = request.getParameter("solicitud") == null?"":(String)request.getParameter("solicitud");
	String rfcPyme = request.getParameter("rfcPyme") == null?"":(String)request.getParameter("rfcPyme");
	String grupo_cesion = request.getParameter("grupo_cesion") == null?"":(String)request.getParameter("grupo_cesion");
	String no_contrato = request.getParameter("no_contrato") == null?"":(String)request.getParameter("no_contrato");
	if(grupo_cesion.equals("0") ) { grupo_cesion=""; } 
	String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");
	
	
	System.out.println("**************************");
	System.out.println("clavePyme  "+clavePyme);
	System.out.println("rfcPyme  "+rfcPyme);
	System.out.println("grupo_cesion  "+grupo_cesion);
	System.out.println("no_contrato  "+no_contrato); 
	System.out.println("**************************"); 

	ImagenDocumentoContrato imagenDocumentoContrato = new ImagenDocumentoContrato();
	imagenDocumentoContrato.setClavePyme(clavePyme);
	imagenDocumentoContrato.setRfcPyme(rfcPyme);
	imagenDocumentoContrato.setGrupo_cesion(grupo_cesion);
	imagenDocumentoContrato.setNo_contrato(no_contrato);
	imagenDocumentoContrato.setLoginUsuario(loginUsuario);
	imagenDocumentoContrato.setTipoExpediente("11");
	
	documentacionPyme = imagenDocumentoContrato.consultarImagenesEFile(strDirectorioTemp);
	
	if (documentacionPyme != null && documentacionPyme.size() >0) {
		hayDocumentos ="S";
	} else {
		hayDocumentos ="N";
	}
} catch (Exception e) {
		System.out.println("Error al obtener expediente Efile:" + e.getMessage());
		e.printStackTrace();
		hayDocumentos ="N";
}
	
%>
<br/>
<br/>
	<table align="center" width="300" border="0" cellspacing="2" cellpadding="2">
		<tr>
			<td align="center" width="100">&nbsp;</td>
			<td align="center"><img src="/nafin/00archivos/15cadenas/15archcadenas/logos/nafinsa.gif" border=0></td>
			<td align="center" width="100">&nbsp;</td>
		</tr>
		<tr>
			<td align="center" colspan="3"><hr/></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<table align="center" border="0" cellpadding="2" cellspacing="2" width="300">
					<% //if (!documentacionPyme.isEmpty() && 
					if(hayDocumentos.equals("S")) {%>
					<tr>
						<td align="center" class="formas" colspan="2">Documentos PyME &nbsp; <a href="<%=strDirecVirtualTemp + documentacionPyme.get("6")%>" target="_new"><img src="/nafin/00utils/gif/pdfdoc.png" border="0"></a></td>
					</tr>
					<%} else {%>
					<tr>
						<td align="center" class="celda01" colspan="2">No se encontraron registros.</td>
					</tr>
					<%}%>
				</table>
			</td>
			<td>&nbsp;</td>
		</tr>	
	</table>
	<br/>

