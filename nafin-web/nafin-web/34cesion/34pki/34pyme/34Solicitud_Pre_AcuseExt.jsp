<%@ page 
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.text.*, 
		netropology.utilerias.usuarios.UtilUsr,
		netropology.utilerias.usuarios.Usuario,
		com.netro.exception.NafinException,
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray,
		java.io.File,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<%
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
String tipoPreAcuse =  (request.getParameter("tipoPreAcuse")!=null)?request.getParameter("tipoPreAcuse"):"";
String claveGrupo =   (request.getParameter("claveGrupo")!=null)?request.getParameter("claveGrupo"):"";
String clavePyme = (String)request.getSession().getAttribute("iNoCliente");

UtilUsr utilUsr = new UtilUsr();
List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

HashMap consultaPreacuseEnvioSolicitud = cesionBean.consultarSolicitudConsentimiento(clave_solicitud);
String claveEpo = (String)consultaPreacuseEnvioSolicitud.get("clave_epo");


HashMap camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
int indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));

String clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);

String nombresRepresentantesPyme = "";
for (int i = 0; i < usuariosPorPerfil.size(); i++) {
	String loginUsrPyme = (String)usuariosPorPerfil.get(i);
	Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
	nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre() + ",";
}
//FODEA-024-2014 MOD()
String cedente =(String)consultaPreacuseEnvioSolicitud.get("nombre_pyme");
String empresas =(String)consultaPreacuseEnvioSolicitud.get("empresas");

//cedente+((representadas!="")?""+", "+representadas.replaceAll(";",", "):"")+", "+
String empresasRepresentadas = cedente+((empresas!="")?""+", "+empresas.replaceAll(";",", "):"");
		
if("Contrato".equals(tipoPreAcuse)){
%>

<table align="center" width="750" border="0" cellspacing="1" cellpadding="1">
	<embed src="<%=strDirecVirtualTemp+cesionBean.generarContrato(clave_solicitud,strDirectorioTemp)%>" width="770" height="500"></embed>
	</table>

<%}else if("Consentimiento".equals(tipoPreAcuse)){%>
<html>
	<head>
		<title>Cesi�n de Derechos</title>

		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">			
	</head>
	<body bgcolor="FFFFFF" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
	<br/>
	<table border="0" width="900" cellpadding="0" cellspacing="0">
	  <tr>
		 <td align="center">
			<table align="center" border="1" width="850" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" class="celda01">Dependencia</td>
					<td align="center" class="celda01">Intermediario<br/>Financiero(Cesionario)</td>
					<td align="center" class="celda01">PYME<br/>(Cedente)</td>
					<td align="center" class="celda01">No. de <br/>Contrato</td>
					<td align="center" class="celda01">Firma <br/>Contrato</td>
					<td align="center" class="celda01">Monto / Moneda</td>
					<td align="center" class="celda01">Tipo de <br/>Contrataci�n</td>
					<td align="center" class="celda01">Fecha Inicio<br/>Contrato</td>
					<td align="center" class="celda01">Fecha Final<br/>Contrato</td>
					<td align="center" class="celda01">Plazo del<br/>Contrato</td>
					<%if(!clasificacionEpo.equals("")){ %>
						<td align="center" class="celda01"><%=clasificacionEpo%></td>
					<%}%>
					<% if(indiceCamposAdicionales >0) { %>
						<%for(int i = 0; i < indiceCamposAdicionales; i++){%>
								<td align="center" class="celda01"><%=camposAdicionalesParametrizados.get("nombre_campo_"+i)%></td>
						<%}%>
					<%}%>
					<td align="center" class="celda01">Ventanilla de Pago</td>
					<td align="center" class="celda01">Supervisor/<br/>Administrador/<br/>Residente de Obra</td>
					<td align="center" class="celda01">Tel�fono</td>
					<td align="center" class="celda01">Objeto del <br/>Contrato</td>
					<td align="center" class="celda01">Estatus</td>
				</tr>				
				<tr>
					<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("nombre_epo")%></td>
					<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("nombre_if")%></td>
					<td align="center" class="formas"><%=empresasRepresentadas%></td>
					<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("numero_contrato")%></td>
					<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("firma_contrato")==null?"":consultaPreacuseEnvioSolicitud.get("firma_contrato")%></td>
					<td align="left" class="formas" nowrap="nowrap"><%=consultaPreacuseEnvioSolicitud.get("montosPorMoneda").toString().replaceAll("\n", "<br/>")%></td><%-- FODEA 016 - 2011 ACF --%>
					<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("tipo_contratacion")%></td>
					<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("fecha_inicio_contrato")%></td>
					<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("fecha_fin_contrato")%></td>
					<td align="center" class="formas" nowrap="nowrap"><%=consultaPreacuseEnvioSolicitud.get("plazoContrato")%></td>
					<%if(!clasificacionEpo.equals("")){ %>
						<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("clasificacionEpo")%></td>
					<%}%>
					<% if(indiceCamposAdicionales >0) { %>
						<%for(int i = 0; i < indiceCamposAdicionales; i++){%>
							<td align="center" class="formas">&nbsp;<%=consultaPreacuseEnvioSolicitud.get("campo_adicional_"+(i+1))%></td>
						<%}%>
					<%}%>
					<td align="center" class="formas">&nbsp;<%=consultaPreacuseEnvioSolicitud.get("venanillaPago")%></td>
					<td align="center" class="formas">&nbsp;<%=consultaPreacuseEnvioSolicitud.get("supAdmResob")%></td>
					<td align="center" class="formas">&nbsp;<%=consultaPreacuseEnvioSolicitud.get("numeroTelefono")%></td>
					<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("objeto_contrato")%></td>
					<td align="center" class="formas"><%=consultaPreacuseEnvioSolicitud.get("estatus_solicitud")%></td>
				</tr>				
				
			</table>
			<br/>
		 </td>
	  </tr>
	</table>
	<br/>
	<table border="0" width="900" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center">
				<table align="center" border="1" width="850">
					<tr>
						<td class="formas" align="left">
							<%    
								String textRepGrupo = "";
								if(!"".equals(claveGrupo)){
									List lstEmpresasXGrupo = cesionBean.getEmpresasXGrupoCder(claveGrupo);
									String cvePymeGrupo = "";
									String nombrePymeGrupo = "";
									String representantes = "";
									for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
										cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
										if(!clavePyme.equals(cvePymeGrupo)){
											nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
											representantes = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("representantes");
											textRepGrupo += representantes+" como representante(s) legal(es) de la Empresa  "+nombrePymeGrupo+",  ";
										}
									}
								}
							
							%>
							De conformidad con lo se�alado en el(los) Contrato(s) cuyos datos se se�alan en esta pantalla, 
							<%=nombresRepresentantesPyme%>, como representante(s) legal(es) de la Empresa <%=strNombre%>, <%=textRepGrupo%> 							  
							solicito(tamos) a <%=consultaPreacuseEnvioSolicitud.get("nombre_epo")%> su consentimiento para ceder a favor del INTERMEDIARIO FINANCIERO 
							se�alado para estos efectos, los derechos de cobro sobre la(s) factura(s) por trabajos ejecutados, 
							por el 100% (cien por ciento) del monto de dicho Contrato.
							<br/><br/>
							En virtud de lo anterior una vez que cuente con el consentimiento que por este medio le es solicitado, 
							celebraremos el contrato de cesi�n de derechos de cobro con el INTERMEDIARIO FINANCIERO, quien proceder� a notificar mediante el 
							Sistema NAFIN, en t�rminos de C�digo de Comercio.						
									
						</td>			
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br/>
	<br/>
	</html>
	</body>
</html>
<%}%>