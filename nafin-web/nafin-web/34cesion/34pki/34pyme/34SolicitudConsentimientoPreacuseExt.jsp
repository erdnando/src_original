<!DOCTYPE html>
<%@ page import="java.util.*,
		com.netro.cesion.*,
		netropology.utilerias.*" 
		contentType="text/html;charset=windows-1252"
		errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<% String numeroProceso = (request.getParameter("numeroProceso")!=null)?request.getParameter("numeroProceso"):"";%>
<% String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";%>
<% String clave_if = (request.getParameter("clave_if")!=null)?request.getParameter("clave_if"):"";%>
<% String clave_grupo = (request.getParameter("clave_grupo")!=null)?request.getParameter("clave_grupo"):"";%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%-- El jsp que contiene el dise�o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<script type="text/javascript" src="34SolicitudConsentimientoPreacuseExt.js?<%=session.getId()%>"></script>
	
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%@ include file="/01principal/01pyme/cabeza.jspf"%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01pyme/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:190px"></div></div>
		</div>
	</div>
	<%@ include file="/01principal/01pyme/pie.jspf"%>
				
<form id='formAux' name="formAux" target='_new'></form>
<form id='formParametros' name="formParametros">
	<input type="hidden" id="numeroProceso" name="numeroProceso" value="<%=numeroProceso%>" />
	<input type="hidden" id="clave_epo" name="clave_epo" value="<%=clave_epo%>" />
	<input type="hidden" id="clave_if" name="clave_if" value="<%=clave_if%>" />
	<input type="hidden" id="clave_grupo" name="clave_grupo" value="<%=clave_grupo%>" />
</form>
</body>
</html>