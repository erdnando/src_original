<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.io.File,
		com.netro.exception.NafinException,		
		com.netro.pdf.*,
		net.sf.json.JSONArray,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String clave_if = (request.getParameter("clave_if")!=null)?request.getParameter("clave_if"):"";
String numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String clave_moneda = (request.getParameter("clave_moneda")!=null)?request.getParameter("clave_moneda"):"";
String clave_estatus_sol = (request.getParameter("clave_estatus_sol")!=null)?request.getParameter("clave_estatus_sol"):"";
String clave_contratacion = (request.getParameter("clave_contratacion")!=null)?request.getParameter("clave_contratacion"):"";
String fecha_vigencia_ini = (request.getParameter("fecha_vigencia_ini")!=null)?request.getParameter("fecha_vigencia_ini"):"";
String fecha_vigencia_fin = (request.getParameter("fecha_vigencia_fin")!=null)?request.getParameter("fecha_vigencia_fin"):"";
String plazoContrato = (request.getParameter("plazoContrato")!=null)?request.getParameter("plazoContrato"):"";
String claveTipoPlazo = (request.getParameter("claveTipoPlazo")!=null)?request.getParameter("claveTipoPlazo"):"";
String tipo_archivo = (request.getParameter("tipo_archivo")!=null)?request.getParameter("tipo_archivo"):"";
String clave_pyme = (String)request.getSession().getAttribute("iNoCliente");
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";

String infoRegresar ="";
String contentType = "text/html;charset=ISO-8859-1";
String 		nombreArchivo 	= null;	
CreaArchivo archivo 				= null;
archivo  = new CreaArchivo();
JSONObject jsonObj = new JSONObject();		
StringBuffer strBuff = new StringBuffer();		

System.out.println("*****************************  ");
System.out.println("informacion -->"+informacion);
System.out.println("clave_solicitud -->"+clave_solicitud);
System.out.println("***************************** ");
		
try {
	
		
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

if(informacion.equals("ArchivoCSV") ||  informacion.equals("ArchivoPDF")){

	List campos_adicionales = new ArrayList();
	List tipo_dato = new ArrayList();

	HashMap parametros_consulta = new HashMap();
			
	parametros_consulta.put("clave_pyme", clave_pyme);
	parametros_consulta.put("clave_epo", clave_epo);
	parametros_consulta.put("clave_if", clave_if.trim());
	parametros_consulta.put("numero_contrato", numero_contrato);
	parametros_consulta.put("clave_moneda", clave_moneda);
	parametros_consulta.put("clave_estatus_sol", clave_estatus_sol);
	parametros_consulta.put("clave_contratacion", clave_contratacion);
	parametros_consulta.put("fecha_vigencia_ini", fecha_vigencia_ini);
	parametros_consulta.put("fecha_vigencia_fin", fecha_vigencia_fin);
	parametros_consulta.put("plazoContrato", plazoContrato);//FODEA 016 - 2011 ACF
	parametros_consulta.put("claveTipoPlazo", claveTipoPlazo);//FODEA 016 - 2011 ACF
	parametros_consulta.put("campos_adicionales", campos_adicionales);
	parametros_consulta.put("strTipoUsuario", strTipoUsuario);
	parametros_consulta.put("tipo_dato", tipo_dato);
				
	HashMap campos_adicionales_param = new HashMap();
	int num_campos_adicionales = 0;
				
	if(clave_epo != null && !clave_epo.equals("")){
		campos_adicionales_param = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
		num_campos_adicionales = Integer.parseInt((String)campos_adicionales_param.get("indice"));
	}
				
	String clasificacionEpo = "";
				
	if(!clave_epo.equals("")){
		clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
	}
				
	HashMap consultaContratosCesion = cesionBean.consultaContratosCesionPyme(parametros_consulta);
	int num_registros = Integer.parseInt((String)consultaContratosCesion.get("indice"));
	

			
	if(informacion.equals("ArchivoCSV")  ){
	 
		if(tipo_archivo.equals("CONSULTA")){
		
			strBuff.append("Dependencia,");
			strBuff.append("Intermediario Financiero (Cesionario),");
			strBuff.append("No. Contrato,");
			strBuff.append("Firma Contrato,");//FODEA 016 - 2011 ACF
			strBuff.append("Monto / Moneda,");//FODEA 016 - 2011 ACF
			strBuff.append("Tipo de Contratación,");
			strBuff.append("Fecha Inicio Contrato,");
			strBuff.append("Fecha Final Contrato,");
			strBuff.append("Plazo del Contrato,");//FODEA 016 - 2011 ACF
			if(clasificacionEpo != null && !clasificacionEpo.equals("")){
				strBuff.append(clasificacionEpo.toString().replaceAll(",","")+",");
			}
			for(int i = 0; i < num_campos_adicionales; i++){strBuff.append(campos_adicionales_param.get("nombre_campo_"+i) + ",");}
				strBuff.append("Ventanilla de Pago,");
				strBuff.append("Supervisor/Administrador/Residente de Obra,");
				strBuff.append("Teléfono,");
				strBuff.append("Objeto del Contrato,");
				strBuff.append("Comentarios,");
				strBuff.append("Fecha Límite de Firma,");
				strBuff.append("Estatus Actual,");
				strBuff.append("Fecha Vencimiento Prórroga,");
				strBuff.append("Estatus Prórroga,");
				strBuff.append("Causas Retorno a PYME\n");
				
					
				for(int i = 0; i < num_registros; i++){
					HashMap contratoCesion = (HashMap)consultaContratosCesion.get("contratoCesion"+i);
					int dias_notificacion = Integer.parseInt(contratoCesion.get("diasMinimosNotificacion")==null?"0":(String)contratoCesion.get("diasMinimosNotificacion"));
					strBuff.append(contratoCesion.get("nombre_epo")==null?"":contratoCesion.get("nombre_epo").toString().replaceAll(",", "") + ",");
					strBuff.append(contratoCesion.get("nombre_if")==null?"":contratoCesion.get("nombre_if").toString().replaceAll(",", "") + ",");
					strBuff.append(contratoCesion.get("numero_contrato")==null?"":contratoCesion.get("numero_contrato").toString().replaceAll(",", "") + ",");
					strBuff.append(contratoCesion.get("firma_contrato")==null?"":contratoCesion.get("firma_contrato").toString().replaceAll(",", "") + ",");
					strBuff.append(contratoCesion.get("montosPorMoneda")==null?"":contratoCesion.get("montosPorMoneda").toString().replaceAll(",", "").replaceAll("<br/>", "|") + ",");//FODEA 016 - 2011 ACF
					strBuff.append(contratoCesion.get("tipo_contratacion")==null?"":contratoCesion.get("tipo_contratacion") + ",");
					strBuff.append(contratoCesion.get("fecha_inicio_contrato")==null?"":contratoCesion.get("fecha_inicio_contrato") + ",");
					strBuff.append(contratoCesion.get("fecha_fin_contrato")==null?"":contratoCesion.get("fecha_fin_contrato") + ",");
					strBuff.append(contratoCesion.get("plazoContrato")==null?"":contratoCesion.get("plazoContrato") + ",");//FODEA 016 - 2011 ACF
					if(clasificacionEpo != null && !clasificacionEpo.equals("")){
						strBuff.append(contratoCesion.get("clasificacion_epo")==null?"":contratoCesion.get("clasificacion_epo").toString().replaceAll(",","") + ",");
					}
					for(int j = 0; j < num_campos_adicionales; j++){
						String temporal=j<2?"'":"";
						strBuff.append(contratoCesion.get("campo_adicional_"+(j + 1))==null?"":temporal+contratoCesion.get("campo_adicional_"+(j + 1)) + ",");
					}
					strBuff.append((contratoCesion.get("venanillaPago")==null?"":(String)contratoCesion.get("venanillaPago").toString().replaceAll(",", "")) + ",");
					strBuff.append((contratoCesion.get("supAdmResob")==null?"":(String)contratoCesion.get("supAdmResob")) + ",");
					strBuff.append((contratoCesion.get("numeroTelefono")==null?"":(String)contratoCesion.get("numeroTelefono")) + ",");
					String objetoContrato = contratoCesion.get("objeto_contrato")==null?"":contratoCesion.get("objeto_contrato").toString().replaceAll(",", "");
					String comentarios = contratoCesion.get("comentarios")==null?"":contratoCesion.get("comentarios").toString().replaceAll(",", "");
					objetoContrato = objetoContrato.replaceAll("\n", "");
					objetoContrato = objetoContrato.replaceAll("\r", "");
					comentarios = comentarios.replaceAll("\n", "");
					comentarios = comentarios.replaceAll("\r", "");
					strBuff.append(objetoContrato + ",");
					strBuff.append(comentarios + ",");
					strBuff.append(contratoCesion.get("fechaAceptacion")==null?"":Fecha.sumaFechaDiasHabiles((String)contratoCesion.get("fechaAceptacion"), "dd/MM/yyyy", dias_notificacion) + ",");
					strBuff.append(contratoCesion.get("estatus_solicitud")==null?"":contratoCesion.get("estatus_solicitud") + ",");
					String fechaVenc=contratoCesion.get("fecha_prorroga")==null?"":(String)contratoCesion.get("fecha_prorroga");
					
					String contadorProrroga =contratoCesion.get("contador_prorroga")==null?"0":(String)contratoCesion.get("contador_prorroga");
					String causasRetorno=contratoCesion.get("causas_retorno")==null?"":(String)contratoCesion.get("causas_retorno");
					causasRetorno= causasRetorno.replaceAll(",","").replaceAll("\n","").replaceAll("\r","");
					String estatusProrroga=contratoCesion.get("estatus_prorroga")==null?"":(String)contratoCesion.get("estatus_prorroga");
					
					
					//fechaVenc="N/A";
					//estatusProrroga="N/A";
					Map mpParams = cesionBean.getParametrosEpo(clave_epo);
					String csProrroga = (String)mpParams.get("CDER_CS_PRORROGA_CONTRATO");
					
					if("N".equals(csProrroga)){
						fechaVenc="N/A";
						estatusProrroga="N/A";
					}else if(Integer.parseInt(contadorProrroga)<1){
						fechaVenc="N/A";
						estatusProrroga="N/A";
					}else if(estatusProrroga.equals("P")){
						estatusProrroga="Solicitud Prórroga";
					}else if(estatusProrroga.equals("A")){
						estatusProrroga="Prórroga Aceptada";
					}else if(estatusProrroga.equals("R")){
						estatusProrroga="Prórroga Rechazada";
					}
						
					
					strBuff.append(fechaVenc + ",");
					strBuff.append(estatusProrroga + ",");
					strBuff.append(causasRetorno + "\n");
					
				}
		}
	
	
		if (!archivo.make(strBuff.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		
	
	}
	 
	 
	if(informacion.equals("ArchivoPDF")  ){
	 
		ComunesPDF docPdf = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		java.text.SimpleDateFormat formatoHora2 = new java.text.SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a"); 
		String hoyFecha = (formatoHora2.format(new java.util.Date()));
		
		archivo 			    = new CreaArchivo();
		nombreArchivo 	  = archivo.nombreArchivo()+".pdf";
		docPdf = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		//FODEA-024-2014 MOD()
		String cedente = "";
		String representante = "";
			for(int i = 0; i < num_registros; i++){
					HashMap contratoCesion = (HashMap)consultaContratosCesion.get("contratoCesion"+i);
					String empresas = contratoCesion.get("empresas")==null?"":contratoCesion.get("empresas").toString();
					if(num_registros>1){
						cedente=(String) session.getAttribute("strNombre");
					}else{
						representante = "";
						if(!"".equals(empresas)){
							cedente =(String) session.getAttribute("strNombre")+"\n"+empresas.replaceAll(";","\n"); 
						}else{
							cedente =(String) session.getAttribute("strNombre"); 
						}
					}
			}
		//
		docPdf.encabezadoConImagenes(docPdf,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		cedente,//FODEA-024-2014 MOD()
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
		docPdf.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			docPdf.setCell("Fecha Vencimiento Prórroga","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Estatus Prórroga","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Causas Retorno a PYME","celda01",ComunesPDF.CENTER);
		
	 
		if(tipo_archivo.equals("CONSULTA")){
		int noColumnas  =18;
			if(clasificacionEpo != null && !clasificacionEpo.equals("")){
			noColumnas +=1;
			}
			if(num_campos_adicionales>0){
				noColumnas +=num_campos_adicionales;
			}
			
			docPdf.setTable(++noColumnas);
			docPdf.setCell("Dependencia","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Intermediario Financiero (Cesionario)","celda01",ComunesPDF.CENTER);
			docPdf.setCell("No. Contrato","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Firma Contrato","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Monto / Moneda","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Tipo de Contratación","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Fecha Inicio Contrato","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Fecha Final Contrato","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Plazo del Contrato","celda01",ComunesPDF.CENTER);
			if(clasificacionEpo != null && !clasificacionEpo.equals("")){
				docPdf.setCell(clasificacionEpo.toString(),"celda01",ComunesPDF.CENTER);
						
			}
			for(int i = 0; i < num_campos_adicionales; i++){
					docPdf.setCell(campos_adicionales_param.get("nombre_campo_"+i).toString(),"celda01",ComunesPDF.CENTER);
			}
				docPdf.setCell("Ventanilla de Pago","celda01",ComunesPDF.CENTER);
				docPdf.setCell("Supervisor/Administrador/Residente de Obra","celda01",ComunesPDF.CENTER);
				docPdf.setCell("Teléfono","celda01",ComunesPDF.CENTER);
				docPdf.setCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
				docPdf.setCell("Comentarios","celda01",ComunesPDF.CENTER);
				docPdf.setCell("Fecha Límite de Firma","celda01",ComunesPDF.CENTER);
				docPdf.setCell("Estatus","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Fecha Vencimiento Prórroga","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Estatus Prórroga","celda01",ComunesPDF.CENTER);
			docPdf.setCell("Causas Retorno a PYME","celda01",ComunesPDF.CENTER);
			
				for(int i = 0; i < num_registros; i++){
					HashMap contratoCesion = (HashMap)consultaContratosCesion.get("contratoCesion"+i);
					int dias_notificacion = Integer.parseInt(contratoCesion.get("diasMinimosNotificacion")==null?"0":(String)contratoCesion.get("diasMinimosNotificacion"));
					
					docPdf.setCell(contratoCesion.get("nombre_epo")==null?"":contratoCesion.get("nombre_epo").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("nombre_if")==null?"":contratoCesion.get("nombre_if").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("numero_contrato")==null?"":contratoCesion.get("numero_contrato").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("firma_contrato")==null?"":contratoCesion.get("firma_contrato").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("montosPorMoneda")==null?"":contratoCesion.get("montosPorMoneda").toString().replaceAll("<br/>", "\n"),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("tipo_contratacion")==null?"":contratoCesion.get("tipo_contratacion").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("fecha_inicio_contrato")==null?"":contratoCesion.get("fecha_inicio_contrato").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("fecha_fin_contrato")==null?"":contratoCesion.get("fecha_fin_contrato").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("plazoContrato")==null?"":contratoCesion.get("plazoContrato").toString(),"formas",ComunesPDF.CENTER);
					if(clasificacionEpo != null && !clasificacionEpo.equals("")){
						docPdf.setCell(contratoCesion.get("clasificacion_epo")==null?"":contratoCesion.get("clasificacion_epo").toString(),"formas",ComunesPDF.CENTER);
					}
					for(int j = 0; j < num_campos_adicionales; j++){					
						docPdf.setCell(contratoCesion.get("campo_adicional_"+(j + 1))==null?"":contratoCesion.get("campo_adicional_"+(j + 1)).toString(),"formas",ComunesPDF.CENTER);
					}
					
					docPdf.setCell(contratoCesion.get("venanillaPago")==null?"":contratoCesion.get("venanillaPago").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("supAdmResob")==null?"":contratoCesion.get("supAdmResob").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("numeroTelefono")==null?"":contratoCesion.get("numeroTelefono").toString(),"formas",ComunesPDF.CENTER);
				  String objetoContrato = contratoCesion.get("objeto_contrato")==null?"":contratoCesion.get("objeto_contrato").toString().replaceAll(",", "");
					String comentarios = contratoCesion.get("comentarios")==null?"":contratoCesion.get("comentarios").toString().replaceAll(",", "");
					objetoContrato = objetoContrato.replaceAll("\n", "");
					objetoContrato = objetoContrato.replaceAll("\r", "");
					comentarios = comentarios.replaceAll("\n", "");
					comentarios = comentarios.replaceAll("\r", "");
					docPdf.setCell(objetoContrato.toString(),"formas",ComunesPDF.CENTER);
				
					docPdf.setCell(comentarios.toString(),"formas",ComunesPDF.CENTER);				
					docPdf.setCell(contratoCesion.get("fechaAceptacion")==null?"":contratoCesion.get("fechaAceptacion").toString(),"formas",ComunesPDF.CENTER);
					docPdf.setCell(contratoCesion.get("estatus_solicitud")==null?"":contratoCesion.get("estatus_solicitud").toString(),"formas",ComunesPDF.CENTER);
					
				String fechaVenc=contratoCesion.get("fecha_prorroga")==null?"":(String)contratoCesion.get("fecha_prorroga");
				String contadorProrroga =contratoCesion.get("contador_prorroga")==null?"0":(String)contratoCesion.get("contador_prorroga");
				String causasRetorno=contratoCesion.get("causas_retorno")==null?"":(String)contratoCesion.get("causas_retorno");
				causasRetorno= causasRetorno.replaceAll(",","").replaceAll("\n","").replaceAll("\r","");
				String estatusProrroga=contratoCesion.get("estatus_prorroga")==null?"":(String)contratoCesion.get("estatus_prorroga");
				
				
				Map mpParams = cesionBean.getParametrosEpo(clave_epo);
				String csProrroga = (String)mpParams.get("CDER_CS_PRORROGA_CONTRATO");
					
				if("N".equals(csProrroga)){
						fechaVenc="N/A";
						estatusProrroga="N/A";
				}else if(Integer.parseInt(contadorProrroga)<1){
					fechaVenc="N/A";
					estatusProrroga="N/A";
				}else if(estatusProrroga.equals("P")){
					estatusProrroga="Solicitud Prórroga";
				}else if(estatusProrroga.equals("A")){
					estatusProrroga="Prórroga Aceptada";
				}else if(estatusProrroga.equals("R")){
					estatusProrroga="Prórroga Rechazada";
				}
				
				docPdf.setCell(fechaVenc.toString(),"formas",ComunesPDF.CENTER);				
				docPdf.setCell(estatusProrroga.toString(),"formas",ComunesPDF.CENTER);				
				docPdf.setCell(causasRetorno.toString(),"formas",ComunesPDF.CENTER);				
				
				}
				docPdf.addTable();	
				docPdf.endDocument();
		}
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}


} else if(informacion.equals("CONTRATO_CESION_PYME")){

	boolean banderaArchivo1=false,banderaArchivo2=false;
	HashMap contratoCesion = cesionBean.consultaContratoCesionPyme(clave_solicitud);
		
	clave_epo = (String)contratoCesion.get("clave_epo");
	clave_if = (String)contratoCesion.get("clave_if");
	String  clave_estatus = (String)contratoCesion.get("clave_estatus");
	
				
	HashMap campos_adic = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
	//StringBuffer clausulado_parametrizado = cesionBean.consultaClausuladoParametrizado(clave_if);
	String clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
				
	int indice_camp_adic = Integer.parseInt((String)campos_adic.get("indice"));
			
	nombreArchivo = archivo.nombreArchivo()+".pdf";
	String nombreArchivo2;
	String nombreArchivo3=archivo.nombreArchivo()+".pdf";
	ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, false);
	
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
	String empresas = (String)contratoCesion.get("CG_EMPRESAS_REPRESENTADAS");
	String pymeEmpresas ="";			
	
	if(strTipoUsuario.equals("IF")  || strTipoUsuario.equals("EPO")   ) {
		pymeEmpresas = nombre; 
	}else{
		pymeEmpresas = ((String)contratoCesion.get("nombre_pyme"))+((empresas!="")?";"+empresas:"");			
	}
			 
	pdfDoc.setEncabezado(pais, noCliente, "", pymeEmpresas.replaceAll(";","\n"), nombreUsr, logo, strDirectorioPublicacion, "");
	
		float anchoCelda4[] = {100f};
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		//====================================================================>> TESTIGO 1 IF
	if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
		pdfDoc.setTable(1, 70, anchoCelda4);
		//pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
		pdfDoc.setCell("NOTIFICACION " , "contrato", ComunesPDF.CENTER, 1, 1, 0);		
		pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "contrato", ComunesPDF.LEFT, 1, 1, 0);	 //nombre de la EPo
		pdfDoc.setCell("Con base en lo dispuesto  en el Código de Comercio, por este conducto me permito notificar a la Empresa, por conducto de esa Gerencia Jurídica, el Contrato de Cesión Electrónica de Derechos de Cobro, que mediante el uso de firmas electrónicas hemos formalizado con el Carácter de Cesionario con la Empresa Cedente, como INTERMEDIARIO FINANCIERO,  respecto del Contrato referido en el apartado inmediato anterior.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);    
		pdfDoc.setCell("Por lo anterior, me permito solicitar a usted gire sus instrucciones a fin de que una vez que surta efectos la presente, se registre la Cesión Electrónica de Derechos de Cobro de referencia a efecto de que la empresa   "+contratoCesion.get("nombre_epo")+", realice el pago de las facturas derivadas del cumplimiento y ejecución del Contrato referido en el apartado inmediato anterior a la cuenta del suscrito INTERMEDIARIO FINANCIERO en su carácter de cesionario.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);   
		pdfDoc.setCell("Los datos de la cuenta en la que se deben depositar los pagos derivados de esta operación se detallan en el apartado inmediato anterior como Nombre del INTERMEDIARIO FINANCIERO, Banco de Depósito,  Número de Cuenta  'PEMEX'  para Depósito y Número de Cuenta CLABE.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);   
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	//====================================================================>> TESTIGO 1 IF
	if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	
	//====================================================================>> REPRESENTANTE LEGAL IF 2
	if (contratoCesion.get("claveUsrIfRep2") != null && !contratoCesion.get("claveUsrIfRep2").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIfRep2") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIfRep2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo2=true;
	}
	
	if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);					
		pdfDoc.setCell((String)contratoCesion.get("firmaTest1"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	//====================================================================>> TESTIGO 2 IF
	if (contratoCesion.get("claveUsrTest2") != null && !contratoCesion.get("claveUsrTest2").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	
	pdfDoc.endDocument();	
	
	if (cesionBean.getBanderaContrato(clave_solicitud).equals("S")){
		nombreArchivo2 = cesionBean.descargaContratoCesionNuevo(clave_solicitud,strDirectorioTemp);
	}else{
		nombreArchivo2 = cesionBean.generarContrato(clave_solicitud,strDirectorioTemp);		
	}
		
	 ComunesPDF pdfDoc2 = new ComunesPDF(2, strDirectorioTemp + nombreArchivo3, "", false, false, false);
	 
	 if(!clave_estatus.equals("3")) { 
		//====================================================================>> REPRESENTANTE LEGAL 1 PYME
		  if (contratoCesion.get("firmaIf") != null && !contratoCesion.get("firmaIf").toString().equals("")) {
			pdfDoc2.setTable(1,70);
			pdfDoc2.setCell("Enteradas del contenido y alcance del presente contrato, las partes lo firman de conformidad, en la Ciudad de México, Distrito Federal, a los "+contratoCesion.get("char_fecha_if"),"contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc2.addTable();
		 }
		pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
			
		//Fodea 023-2015  Cuando la solicitud tiene un grupo de pymes (E)
		String claveGrupo =(String)contratoCesion.get("ic_grupo_cesion");	
		
		UtilUsr utilUsr = new UtilUsr();
		if(!claveGrupo.equals("")){			
			List  lstEmpresasXGrupo = cesionBean.getEmpresasXGrupoCder(claveGrupo);
			if(lstEmpresasXGrupo.size()>0)  {
				for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
					String cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
					String nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
					
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(cvePymeGrupo, "P");
					
					for (int i = 0; i < usuariosPorPerfil.size(); i++) {						
						String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
						Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
						String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
						HashMap datosRep= cesionBean.getRepresentantesCEDENTE( clave_solicitud, cvePymeGrupo, loginUsrPyme );
						if(datosRep.size()>0) {						
							if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
								
								pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
								pdfDoc2.addText(" Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePymeGrupo, "formas", ComunesPDF.CENTER);
								pdfDoc2.setTable(1, 40, anchoCelda4);
								pdfDoc2.setCell( datosRep.get("CG_FIRMA_REPRESENTANTE").toString() , "formas", ComunesPDF.CENTER, 1);
								pdfDoc2.addTable();
								banderaArchivo2=true;
								
							}
						}
					}				
				}
			}	
			//Fodea 023-2015  Cuando la solicitud tiene un grupo de pymes (S)
		}else  {
					clave_pyme = (String)contratoCesion.get("clave_pyme");
					String  nombrePyme=(String)contratoCesion.get("nombre_pyme");
					
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");
					
					for (int i = 0; i < usuariosPorPerfil.size(); i++) {						
						String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
						Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
						String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
						HashMap datosRep= cesionBean.getRepresentantesCEDENTE( clave_solicitud, clave_pyme, loginUsrPyme );
						if(datosRep.size()>0) {				
							if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
								
								pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
								pdfDoc2.addText(" Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePyme, "formas", ComunesPDF.CENTER);
								pdfDoc2.setTable(1, 40, anchoCelda4);
								pdfDoc2.setCell( datosRep.get("CG_FIRMA_REPRESENTANTE").toString() , "formas", ComunesPDF.CENTER, 1);
								pdfDoc2.addTable();
								banderaArchivo2=true;
								
							}
						}
					}
				}
		
		
		//====================================================================>> REPRESENTANTE LEGAL IF
		if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
			pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
			pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
			pdfDoc2.setTable(1, 40, anchoCelda4);
			pdfDoc2.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
			pdfDoc2.addTable();
			banderaArchivo2=true;
		}
		
		//====================================================================>> REPRESENTANTE LEGAL IF 2
		if (contratoCesion.get("claveUsrIfRep2") != null && !contratoCesion.get("claveUsrIfRep2").toString().equals("")) {
			pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
			pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIfRep2") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
			pdfDoc2.setTable(1, 40, anchoCelda4);
			pdfDoc2.setCell((String)contratoCesion.get("firmaIfRep2"), "formas", ComunesPDF.CENTER, 1);
			pdfDoc2.addTable();
			banderaArchivo2=true;
		}
	}else  {
		pdfDoc2.setTable(1,70);
		pdfDoc2.addText("\n","contrato",ComunesPDF.CENTER);
		pdfDoc2.addTable();
		banderaArchivo2=true;
	}

	pdfDoc2.endDocument();
	
	ArrayList listaA = new ArrayList();
	
	if(banderaArchivo1){listaA.add(strDirectorioTemp+nombreArchivo);}
	listaA.add(strDirectorioTemp+nombreArchivo2);
	if(banderaArchivo2){listaA.add(strDirectorioTemp+nombreArchivo3);}
	String nombreArchivoNevo = archivo.nombreArchivo()+".pdf";
	listaA.add(strDirectorioTemp+nombreArchivoNevo);
	String args[]= new String[listaA.size()];
	for (int i=0; i<listaA.size(); i++)
		{
			args[i]=(String)listaA.get(i);
		}
	//String args[] =listaA.toArray();//  {strDirectorioTemp+nombreArchivo,strDirectorioTemp+nombreArchivo2,strDirectorioTemp+nombreArchivo3,strDirectorioTemp+nombreArchivoNevo};
	
	if(banderaArchivo1||banderaArchivo2){
		nombreArchivo=nombreArchivoNevo;
		ComunesPDF.doMerge(args);
		
	}else{
		nombreArchivo=nombreArchivo2;
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	
}else if(informacion.equals("CONTRATO_CESION_IF")){

	boolean banderaArchivo1=false,banderaArchivo2=false;
	HashMap contratoCesion = cesionBean.consultaContratoCesionPyme(clave_solicitud);
		
	clave_epo = (String)contratoCesion.get("clave_epo");
	clave_if = (String)contratoCesion.get("clave_if");
				
	HashMap campos_adic = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
	//StringBuffer clausulado_parametrizado = cesionBean.consultaClausuladoParametrizado(clave_if);
	String clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
				
	int indice_camp_adic = Integer.parseInt((String)campos_adic.get("indice"));
			
	nombreArchivo = archivo.nombreArchivo()+".pdf";
	String nombreArchivo2;
	String nombreArchivo3=archivo.nombreArchivo()+".pdf";
	ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, false);
				
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
	String empresas = (String)contratoCesion.get("CG_EMPRESAS_REPRESENTADAS");
	String pymeEmpresas ="";	
	if(strTipoUsuario.equals("IF")  || strTipoUsuario.equals("EPO")   ) {
		pymeEmpresas = nombre; 
	}else{
		pymeEmpresas = nombre+((empresas!="")?";"+empresas:"");			
	}
	
	pdfDoc.setEncabezado(pais, noCliente, "", /*nombre*/pymeEmpresas.replaceAll(";","\n"), nombreUsr, logo, strDirectorioPublicacion, "");
			
	//pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
	
		float anchoCelda4[] = {100f};
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		//====================================================================>> TESTIGO 1 IF
	if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
		pdfDoc.setTable(1, 70, anchoCelda4);
		//pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
		pdfDoc.setCell("NOTIFICACION " , "contrato", ComunesPDF.CENTER, 1, 1, 0);		
		pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "contrato", ComunesPDF.LEFT, 1, 1, 0);	 //nombre de la EPo
		pdfDoc.setCell("Con base en lo dispuesto  en el Código de Comercio, por este conducto me permito notificar a la Empresa, por conducto de esa Gerencia jurídica, el Contrato de Cesión Electrónica de Derechos de Cobro, que mediante el uso de firmas electrónicas hemos formalizado con el carácter de Cesionario con la Empresa Cedente como Intemediario Financiero, respecto del Contrato referido en el apartado inmediato anterior", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);    
		pdfDoc.setCell("Por lo anterior, me permito solicitar a usted gire sus instrucciones a fin de que una vez que surta efectos la presente, se registre la Cesión Electrónica de Derechos de Cobro de referencia, a efecto de que la Empresa "+contratoCesion.get("nombre_epo")+", realice el pago de las facturas derivadas del cumplimiento y ejecución del Contrato referido en el apartado inmediato anterior a la cuenta del suscrito INTERMEDIARIO FINANCIERO en su caracter de Cesionario.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);   
		pdfDoc.setCell("Los datos de la cuenta en la que se deben depositar los pagos derivados de esta operación se detallan en el apartado inmediato anterior  como Nombre del INTERMEDIARIO FINANCIERO, Banco de Depósito,  Número de Cuenta 'Pemex' para Depósito y Número de Cuenta CLABE.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);   
		pdfDoc.addTable();
		banderaArchivo1=true;
}
	
		//====================================================================>> REPRESENTANTE LEGAL IF
	if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo2=true;
	}
//====================================================================>> REPRESENTANTE LEGAL IF 2
	if (contratoCesion.get("claveUsrIfRep2") != null && !contratoCesion.get("claveUsrIfRep2").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIfRep2") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIfRep2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	
	if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);					
		pdfDoc.setCell((String)contratoCesion.get("firmaTest1"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	//====================================================================>> TESTIGO 2 IF
	if (contratoCesion.get("claveUsrTest2") != null && !contratoCesion.get("claveUsrTest2").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	
	pdfDoc.endDocument();
	
	if (cesionBean.getBanderaContrato(clave_solicitud).equals("S")){
		nombreArchivo2 = cesionBean.descargaContratoCesionNuevo(clave_solicitud,strDirectorioTemp);
	}else{
		nombreArchivo2 = cesionBean.generarContrato(clave_solicitud,strDirectorioTemp);
	}
	
	 ComunesPDF pdfDoc2 = new ComunesPDF(2, strDirectorioTemp + nombreArchivo3, "", false, false, false);
		if (contratoCesion.get("firmaIf") != null && !contratoCesion.get("firmaIf").toString().equals("")) {
		pdfDoc2.setTable(1,70);
		pdfDoc2.setCell("Enteradas del contenido y alcance del presente contrato, las partes lo firman de conformidad, en la Ciudad de México, Distrito Federal, a los "+contratoCesion.get("char_fecha_if"),"formas",ComunesPDF.JUSTIFIED,1,1,0);
		pdfDoc2.addTable();
	 }
	//====================================================================>> REPRESENTANTE LEGAL 1 PYME
	pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
	if (contratoCesion.get("claveUsrAutRep1") != null && !contratoCesion.get("claveUsrAutRep1").toString().equals("")) {
		pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep1") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
		pdfDoc2.setTable(1, 40, anchoCelda4);
		pdfDoc2.setCell((String)contratoCesion.get("firmaRep1"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc2.addTable();
		banderaArchivo2=true;
	}
	//====================================================================>> REPRESENTANTE LEGAL 2 PYME
	if (contratoCesion.get("claveUsrAutRep2") != null && !contratoCesion.get("claveUsrAutRep2").toString().equals("")) {
		pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep2") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
		pdfDoc2.setTable(1, 40, anchoCelda4);
		pdfDoc2.setCell((String)contratoCesion.get("firmaRep2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc2.addTable();
		banderaArchivo2=true;
	}
	//====================================================================>> REPRESENTANTE LEGAL IF
	if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
		pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc2.setTable(1, 40, anchoCelda4);
		pdfDoc2.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc2.addTable();
		banderaArchivo2=true;
	}
	
	//====================================================================>> REPRESENTANTE LEGAL IF 2
	if (contratoCesion.get("claveUsrIfRep2") != null && !contratoCesion.get("claveUsrIfRep2").toString().equals("")) {
		pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIfRep2") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc2.setTable(1, 40, anchoCelda4);
		pdfDoc2.setCell((String)contratoCesion.get("firmaIfRep2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc2.addTable();
		banderaArchivo2=true;
	}

	pdfDoc2.endDocument();
	
	ArrayList listaA = new ArrayList();
	
	if(banderaArchivo1){listaA.add(strDirectorioTemp+nombreArchivo);}
	listaA.add(strDirectorioTemp+nombreArchivo2);
	if(banderaArchivo2){listaA.add(strDirectorioTemp+nombreArchivo3);}
	String nombreArchivoNevo = archivo.nombreArchivo()+".pdf";
	listaA.add(strDirectorioTemp+nombreArchivoNevo);
	String args[]= new String[listaA.size()];
	for (int i=0; i<listaA.size(); i++)
		{
			args[i]=(String)listaA.get(i);
		}
	//String args[] =listaA.toArray();//  {strDirectorioTemp+nombreArchivo,strDirectorioTemp+nombreArchivo2,strDirectorioTemp+nombreArchivo3,strDirectorioTemp+nombreArchivoNevo};
	
	if(banderaArchivo1||banderaArchivo2){
		nombreArchivo=nombreArchivoNevo;
		ComunesPDF.doMerge(args);
		
	}else{
		nombreArchivo=nombreArchivo2;
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	
} else if(informacion.equals("VALIDA_CEDENTE_CESIONARIO")){
	
	
	//jsonObj.put("cedente", new Boolean(cesionBean.existeCedente(clave_if,clave_pyme)));
	jsonObj.put("cedente", new Boolean(cesionBean.existeCedenteXSolic(clave_solicitud)));
	jsonObj.put("cesionario", new Boolean(cesionBean.existeCesionario(clave_if)));
	
	jsonObj.put("success", new Boolean(true));
	
	
} else if(informacion.equals("CONTRATO")){
	
	//File file = null;		
	String  file = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+file);
	
}else if (informacion.equals("ACUSE")) {

	String acuse_carga = (request.getParameter("acuse_carga")!=null)?request.getParameter("acuse_carga"):"";
	String sello_digital = (request.getParameter("sello_digital")!=null)?request.getParameter("sello_digital"):"";
	String login_usuario = (String)request.getSession().getAttribute("Clave_usuario");
		
	System.out.println("***************************** ");	
	System.out.println("clave_solicitud "+clave_solicitud);
	System.out.println("acuse_carga "+acuse_carga);	
	System.out.println("***************************** ");
	
	HashMap contratoCesion = cesionBean.consultaContratoCesionPyme(clave_solicitud, login_usuario);
	
	clave_epo = (String)contratoCesion.get("clave_epo");
	clave_if = (String)contratoCesion.get("clave_if");
				
	HashMap campos_adic = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
	//StringBuffer clausulado_parametrizado = cesionBean.consultaClausuladoParametrizado(clave_if);
	String clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
				
	int indice_camp_adic = Integer.parseInt((String)campos_adic.get("indice"));
				
	nombreArchivo = archivo.nombreArchivo()+".pdf";
				
	ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, false);
	String cedente = "";
	if(!"".equals(contratoCesion.get("CG_EMPRESAS_REPRESENTADAS"))){
		cedente = contratoCesion.get("nombre_pyme")+";"+contratoCesion.get("CG_EMPRESAS_REPRESENTADAS");
		cedente = cedente.replaceAll(";","\n");
	}else{
		cedente  = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	}
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
				
	pdfDoc.setEncabezado(pais, noCliente, "", cedente/*nombre*/, nombreUsr, logo, strDirectorioPublicacion, "");
		
	pdfDoc.addText("La autentificación se llevó a cabo con éxito \n Recibo: "+acuse_carga, "titulo", ComunesPDF.CENTER);

	float anchoCelda1[] = {50f, 50f};
	pdfDoc.setTable(2, 50, anchoCelda1);
				
	pdfDoc.setCell("Cifras de Control", "celda02", ComunesPDF.CENTER, 2);
				
	pdfDoc.setCell("Número de Acuse", "formas", ComunesPDF.RIGHT);
	pdfDoc.setCell((String)contratoCesion.get("acuseFirmaRep1"), "formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.RIGHT);
	pdfDoc.setCell((String)contratoCesion.get("fechaAutRep1"), "formas", ComunesPDF.LEFT);
	pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.RIGHT);
	pdfDoc.setCell((String)contratoCesion.get("horaAutRep1"), "formas", ComunesPDF.LEFT);
	pdfDoc.setCell("Usuario", "formas", ComunesPDF.RIGHT);
	pdfDoc.setCell(contratoCesion.get("claveUsrAutRep2")==null?(contratoCesion.get("claveUsrAutRep1") + " - " + contratoCesion.get("nombreUsrAutRep1")):(contratoCesion.get("claveUsrAutRep2") + " - " + contratoCesion.get("nombreUsrAutRep2")), "formas",ComunesPDF.LEFT);
	pdfDoc.addTable();
	
	pdfDoc.endDocument();
	
	float anchoCelda3[] = {100f};
	
	//pdfDoc.setTable(1, 70, anchoCelda3);
	//pdfDoc.setCell(clausulado_parametrizado.toString(), "formas", ComunesPDF.LEFT, 1);
	//pdfDoc.addTable();
	
	String nombreArchivo2 = cesionBean.descargaContratoCesionNuevo(clave_solicitud,strDirectorioTemp);
	String nombreArchivo3=archivo.nombreArchivo()+".pdf";
	pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo3, "", false, false, false);
	float anchoCelda4[] = {100f};
	
	//====================================================================>> REPRESENTANTE LEGAL 1 PYME
	List lstFirmaRep = cesionBean.getFirmasRepresentantes(clave_solicitud);
	
	for(int x=0; x<lstFirmaRep.size(); x++){
		HashMap hm = (HashMap)lstFirmaRep.get(x);
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + hm.get("cg_nombre_usuario") + " representante legal de " + hm.get("cg_razon_social"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)hm.get("cg_firma_representante"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
	}
	/*
	if (contratoCesion.get("claveUsrAutRep1") != null && !contratoCesion.get("claveUsrAutRep1").toString().equals("")) {
	pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
	pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep1") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
	pdfDoc.setTable(1, 40, anchoCelda4);
	pdfDoc.setCell((String)contratoCesion.get("firmaRep1"), "formas", ComunesPDF.CENTER, 1);
	pdfDoc.addTable();
	}
	//====================================================================>> REPRESENTANTE LEGAL 2 PYME
	if (contratoCesion.get("claveUsrAutRep2") != null && !contratoCesion.get("claveUsrAutRep2").toString().equals("")) {
	pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
	pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep2") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
	pdfDoc.setTable(1, 40, anchoCelda4);

	pdfDoc.setCell((String)contratoCesion.get("firmaRep2"), "formas", ComunesPDF.CENTER, 1);
	pdfDoc.addTable();
	}*/
	pdfDoc.endDocument();
	
	String nombreArchivoNevo = archivo.nombreArchivo()+".pdf";
	
	String listaA[]={strDirectorioTemp+nombreArchivo,strDirectorioTemp+nombreArchivo2,strDirectorioTemp+nombreArchivo3,strDirectorioTemp+nombreArchivoNevo};
	ComunesPDF.doMerge(listaA);
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivoNevo);
	
}
			
 	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>

<%=jsonObj%>