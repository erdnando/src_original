	<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.io.File,
		com.netro.exception.NafinException,		
		com.netro.pdf.*,
		net.sf.json.JSONArray,
		netropology.utilerias.usuarios.UtilUsr, 
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%
	String contentType = "text/html;charset=ISO-8859-1";
	String 		nombreArchivo 	= null;	
	CreaArchivo archivo  = new CreaArchivo();
	JSONObject jsonObj = new JSONObject();		

	String numeroSolicitud = (request.getParameter("numeroSolicitud")!=null)?request.getParameter("numeroSolicitud"):"";
	String cedente = (request.getParameter("cedente")!=null)?request.getParameter("cedente"):"";
	String claveGrupo = (request.getParameter("clave_grupo")!=null)?request.getParameter("clave_grupo"):"";
	
	
	

try {	
	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	
			
	StringBuffer strbuffLeyendaLegal = new StringBuffer();
	HashMap consultaAcuseSolicitud = cesionBean.consultarSolicitudConsentimiento(numeroSolicitud);
	//FODEA-024-2014 MOD()
	cedente = (String)consultaAcuseSolicitud.get("nombre_pyme");
	String empresas = (String)consultaAcuseSolicitud.get("empresas");
	String cedenteAux[]=cedente.split(";");
	String cedenteLista = cedente+((empresas!="")?"\n"+empresas.replaceAll(";","\n"):"");
	//				
	String claveEpo = (String)consultaAcuseSolicitud.get("clave_epo");
	String claveIfCesionario = (String)consultaAcuseSolicitud.get("clave_if");
					
	HashMap camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
	StringBuffer clausuladoParametrizado = cesionBean.consultaClausuladoParametrizado(claveIfCesionario);
	String clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
	int indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));
	
	archivo 			    = new CreaArchivo();
	nombreArchivo 	  = archivo.nombreArchivo()+".pdf";
				
	ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
					
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
		
	pdfDoc.setEncabezado(pais, noCliente, "", cedenteLista/*nombre*/, nombreUsr, logo, strDirectorioPublicacion, "");
				
	float anchoCelda1[] = new float[0];
	int numeroColumnas = 0;
				
	if (indiceCamposAdicionales == 1) {
		float anchoCelda[] = {6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f, 6.66f};
		numeroColumnas = 17;
		anchoCelda1 = anchoCelda;
	} else if (indiceCamposAdicionales == 2) {
		float anchoCelda[] = {6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.66f, 6.66f};
		numeroColumnas = 18;
		anchoCelda1 = anchoCelda;
	} else if (indiceCamposAdicionales == 3) {
		float anchoCelda[] = {5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 6.66f, 6.66f};
		numeroColumnas = 19;
		anchoCelda1 = anchoCelda;
	} else if (indiceCamposAdicionales == 4) {
		float anchoCelda[] = {5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 5.55f, 6.66f, 6.66f};
		numeroColumnas = 20;
		anchoCelda1 = anchoCelda;
	} else if (indiceCamposAdicionales == 5) {
		float anchoCelda[] = {5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 5.26f, 6.66f, 6.66f};
		numeroColumnas = 21;
		anchoCelda1 = anchoCelda;
	} else {
		float anchoCelda[] = {7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f, 7.69f};
		numeroColumnas = 16;
		anchoCelda1 = anchoCelda;
	}
	//numeroColumnas++;//Para la fecha de firma del contrato
			
	pdfDoc.setTable(numeroColumnas, 100, anchoCelda1);
	pdfDoc.setCell("Dependencia", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell("Intermediario\nFinanciero(Cesionario)", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell("PYME(Cedente)", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell("No. de\nContrato", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell("Firma de \nContrato", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell("Monto \noriginal del\n contrato / \nMoneda", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell("Tipo de\nContratación", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell("Fecha Inicio\nContrato", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell("Fecha Final\nContrato", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell("Plazo \noriginal \ndel contrato", "celda02", ComunesPDF.CENTER, 1);
	pdfDoc.setCell(clasificacionEpo, "celda02", ComunesPDF.CENTER, 1);
	for (int i = 0; i < indiceCamposAdicionales; i++) {
		pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_"+i), "celda02", ComunesPDF.CENTER, 1);
	}
	pdfDoc.setCell("Ventanilla de Pago", "celda02", ComunesPDF.CENTER, 1);
		pdfDoc.setCell("Supervisor/\nAdministrador/\nResidente de Obra", "celda02", ComunesPDF.CENTER, 1);
		pdfDoc.setCell("Teléfono", "celda02", ComunesPDF.CENTER, 1);
		pdfDoc.setCell("Objeto del\nContrato", "celda02", ComunesPDF.CENTER, 1);
		pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER, 1);

		pdfDoc.setHeaders();

		pdfDoc.setCell((String)consultaAcuseSolicitud.get("nombre_epo"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("nombre_if"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell(cedenteLista, "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("numero_contrato"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("firma_contrato"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((((String)consultaAcuseSolicitud.get("montosPorMoneda"))).replaceAll("<br/>","\n"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("tipo_contratacion"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("fecha_inicio_contrato"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("fecha_fin_contrato"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("plazoContrato"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("clasificacionEpo"), "formas", ComunesPDF.CENTER, 1);
			for (int i = 0; i < indiceCamposAdicionales; i++) {
				pdfDoc.setCell((String)consultaAcuseSolicitud.get("campo_adicional_"+(i+1)), "formas", ComunesPDF.CENTER, 1);
			}
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("venanillaPago"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("supAdmResob"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("numeroTelefono"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("objeto_contrato"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("estatus_solicitud"), "formas", ComunesPDF.CENTER, 1);
			
		pdfDoc.addTable();
			
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

		float anchoCelda2[] = {35f, 65f};
		pdfDoc.setTable(2, 40, anchoCelda2);
		pdfDoc.setCell("Cifras de Control", "celda02", ComunesPDF.CENTER, 2);
		pdfDoc.setCell("Número de Acuse", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("acuse1"), "formas",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("fechaCarga"), "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("horaCarga"), "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("Usuario", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell((String)consultaAcuseSolicitud.get("usuarioCarga"), "formas",ComunesPDF.CENTER);
		pdfDoc.addTable();

		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.newPage();

		float anchoCelda3[] = {100f};
		pdfDoc.setTable(1, 100, anchoCelda3);
		//FODEA-024-2014 MOD()
	
		String nombresRepresentantesPyme = "";
		String clavePyme = (String)request.getSession().getAttribute("iNoCliente");
		UtilUsr utilUsr = new UtilUsr();
		List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");
		
		for (int i = 0; i < usuariosPorPerfil.size(); i++) {
			String loginUsrPyme = (String)usuariosPorPerfil.get(i);
			Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
			nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre() + ",";
		}
			
		String textRepGrupo = "";
		if(!"".equals(claveGrupo)){
			List lstEmpresasXGrupo = cesionBean.getEmpresasXGrupoCder(claveGrupo);
			String cvePymeGrupo = "";
			String nombrePymeGrupo = "";
			String representantes = "";
			for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
				cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
				if(!clavePyme.equals(cvePymeGrupo)){
					nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
					representantes = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("representantes");
					textRepGrupo += representantes+" como representante(s) legal(es) de la Empresa  "+nombrePymeGrupo+",  ";
				}
			}
		}
		
		String mensajeNuevo="\n De conformidad con lo señalado en el(los) Contrato(s) cuyos datos se señalan en esta pantalla "+nombresRepresentantesPyme+" como representante(s) legal(es) de la Empresa  "+cedente+", "+textRepGrupo+
		 "  solicito(tamos) a  "+consultaAcuseSolicitud.get("nombre_epo")+", su consentimiento para ceder a favor del INTERMEDIARIO FINANCIERO señalado para estos efectos, los derechos de cobro sobre la(s) factura(s) por trabajos ejecutados, por el 100% (cien por ciento) del monto de dicho Contrato.  \n\n"+
		 " En virtud de lo anterior una vez que cuente con el consentimiento que por este medio le es solicitado, celebraremos el contrato de cesión de derechos de cobro con el INTERMEDIARIO FINANCIERO, quien procederá a notificar mediante el Sistema NAFIN, en términos de Código de Comercio.\n\n"; 

 
  
		pdfDoc.setCell(mensajeNuevo, "formas", ComunesPDF.JUSTIFIED, 1, 1, 1);//FODEA-024-2014 MOD(strbuffLeyendaLegal.toString())
		pdfDoc.addTable();

		pdfDoc.endDocument();

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	
	
	 	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>

<%=jsonObj%>