<%@ page import="
		javax.naming.*,
		java.util.*,
		java.sql.*,		
		java.text.*, 
		com.netro.cesion.*,
		netropology.utilerias.*,
		com.netro.exception.NafinException"
	errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<%@ page contentType="text/html;charset=windows-1252"%>
<%
	String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
	String _acuse = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
		
	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			
	HashMap contratoCesion = cesionBean.consultaContratoCesionPyme(clave_solicitud);
	
	String clave_epo = (String)contratoCesion.get("clave_epo");
	String clave_if = (String)contratoCesion.get("clave_if");
		
	HashMap campos_adicionales = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
	int indice_camp_adic = Integer.parseInt((String)campos_adicionales.get("indice"));
	StringBuffer clausulado_parametrizado = cesionBean.consultaClausuladoParametrizado(clave_if);
	String clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
	contratoCesion.put("campos_adicionales", campos_adicionales);
	contratoCesion.put("clausulado_parametrizado", clausulado_parametrizado);
	contratoCesion.put("clasificacionEpo", clasificacionEpo);
	String huboError ="", mostrarError ="";
	if(!_acuse.equals("")){
		int tamanio2 =	_acuse.length();
			 huboError =  _acuse.substring(0, 1);	
			 mostrarError  = _acuse.substring(1, tamanio2);	
	}	
%>

<%if(!huboError.equals("|")){%>
	<br/>
		<br/>	
		<br/>
		<br/>
		<table align="center" border="0" width="625" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table align="center" width="600" border="0" cellspacing="0" cellpadding="0">
						<tr><td class="formas" align="center">&nbsp;</td></tr>
						<tr>
							<td class="titulo" align="center"><b>La autentificaci�n se llev� a cabo con �xito <br/> Recibo: <%=_acuse%></b></td>
							<html:hidden property="acuse_carga" value='<%=_acuse%>'/>
							<html:hidden property="numero_folio" value='<%=request.getAttribute("folioCert")%>'/>
							<html:hidden property="fecha_carga" value='<%=request.getAttribute("fecha_carga")%>'/>
							<html:hidden property="hora_carga" value='<%=request.getAttribute("hora_carga")%>'/>
						</tr>
						<tr><td class="formas" align="center">&nbsp;</td></tr>
						<tr>
							<%--<td class="formas" width="600" align="center">--%>
							<td class="formas" align="center">
								<table align="center" border="1" cellspacing="0" cellpadding="0" width="350">
									<tr>
										<td class="menusup2" align="center" colspan="2">Cifras de Control</td>
									<tr>
									<tr>
										<td class="formas" width="150" align="left">N�mero de Acuse</td>
										<td class="formas" width="200" align="right"><%=contratoCesion.get("acuseFirmaRep1")%></td>
									<tr>
									<tr>
										<td class="formas" width="150" align="left">Fecha de Carga</td>
										<td class="formas" width="200" align="right"><%=contratoCesion.get("fechaAutRep1")%></td>
									<tr>
									<tr>
										<td class="formas" width="150" align="left">Hora de Carga</td>
										<td class="formas" width="200" align="right"><%=contratoCesion.get("horaAutRep1")%></td>
									<tr>
									<tr>
										<td class="formas" width="150" align="left">Usuario de Captura</td>
										<td class="formas" width="200" align="right"><%=contratoCesion.get("claveUsrAutRep2")==null?(contratoCesion.get("claveUsrAutRep1") + " - " + contratoCesion.get("nombreUsrAutRep1")):(contratoCesion.get("claveUsrAutRep2") + " - " + contratoCesion.get("nombreUsrAutRep2"))%></td>
									<tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td align="center" class="formas"><b><font size="2">CONTRATO DE CESI�N DE DERECHOS</font></b></td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
						 <td align="center">
							<table align="center" border="0" width="600" cellpadding="0" cellspacing="0">
								<tr><td align="center" class="menusup2">INFORMACI�N DE LA PYME CEDENTE</td></tr>
								<tr>
									<td align="left" class="formas">
										<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td align="left" class="formas" width="300">NOMBRE:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombre_pyme")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">RFC:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("rfcPyme")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">REPRESENTANTE PYME:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">REPRESENTANTE PYME:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">NUM. PROVEEDOR:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("numeroProveedor")%></td>
											</tr>
											<tr>
												<td align="center" class="formas" colspan="2">&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr><td align="center" class="menusup2">INFORMACI�N DEL CONTRATO</td></tr>
								<tr>
									<td align="left" class="formas">
										<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td align="left" class="formas" width="300">N�MERO DE CONTRATO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("numero_contrato")%></td>
											</tr>
												<tr>
												<td align="left" class="formas" width="300">MONTO / MONEDA:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("montosPorMoneda")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">FECHA DE VIGENCIA:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%="N/A".equals(contratoCesion.get("fecha_inicio_contrato"))?"N/A":(contratoCesion.get("fecha_inicio_contrato") + "&nbsp; a &nbsp;" + contratoCesion.get("fecha_fin_contrato"))%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">PLAZO DEL CONTRATO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("plazoContrato")%></td>
											</tr>
											<%-- FODEA 016 - 2011 - F --%>
											<tr>
												<td align="left" class="formas" width="300">TIPO DE CONTRATACI�N:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("tipo_contratacion").toString().toUpperCase()%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">OBJETO DEL CONTRATO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("objeto_contrato").toString().toUpperCase()%></td>
											</tr>
											<%for (int i = 0; i < indice_camp_adic; i++) {%>
												<tr>
													<td align="left" class="formas" width="300"><%=campos_adicionales.get("nombre_campo_"+i).toString().toUpperCase()%>:&nbsp;</td>
													<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("campo_adicional_"+(i+1)).toString().toUpperCase()%></td>
												</tr>
											<%}%>
											<tr>
												<td align="center" class="formas" colspan="2">&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr><td align="center" class="menusup2">INFORMACI�N DE LA EMPRESA DE PRIMER ORDEN</td></tr>
								<tr>
									<td align="left" class="formas">
										<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td align="left" class="formas" width="300">NOMBRE:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombre_epo")%></td>
											</tr>
											<tr>
												<td align="center" class="formas" colspan="2">&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr><td align="center" class="menusup2">INFORMACI�N DEL CESIONARIO (IF)</td></tr>
								<tr>
									<td align="left" class="formas">
										<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td align="left" class="formas" width="300">NOMBRE:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombre_if")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">BANCO DE DEP�SITO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("bancoDeposito")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">N�MERO DE CUENTA PARA DEP�SITO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("numeroCuenta")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">N�MERO DE CUENTA CLABE PARA DEP�SITO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("numeroCuentaClabe")%></td>
											</tr>
											<tr>
												<td align="center" class="formas" colspan="2">&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr><td align="center" class="menusup2">INFORMACI�N DE LA CESI�N DE DERECHOS</td></tr>
								<tr>
									<td align="left" class="formas">
										<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
											<tr>
												<td align="left" class="formas" width="300">FECHA DE SOLICITUD DE CONSENTIMIENTO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaSolicitudConsentimiento")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">FECHA DE CONSENTIMIENTO DE LA EPO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaAceptacionEpo")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">PERSONA QUE OTORG� EL CONSENTIMIENTO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrEpoAutCesion")==null?"":contratoCesion.get("nombreUsrEpoAutCesion")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">FECHA DE FORMALIZACI�N DEL CONTRATO DE CESI�N:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaFormalContrato")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE DE CESIONARIO (IF):&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrIf")==null?"":contratoCesion.get("nombreUsrIf")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrTest1")==null?"":contratoCesion.get("nombreUsrTest1")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrTest2")==null?"":contratoCesion.get("nombreUsrTest2")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">FECHA DE NOTIFICACI�N ACEPTADA POR LA EPO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaNotifEpo")==null?"":contratoCesion.get("fechaNotifEpo")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">PERSONA QUE ACEPT� LA NOTIFICACI�N:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrNotifEpo")==null?"":contratoCesion.get("nombreUsrNotifEpo")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">FECHA DE INFORMACI�N A VENTANILLA DE PAGOS:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">FECHA DE RECEPCI�N EN VENTANILLA:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">FECHA DE LA APLICACI�N DEL REDIRECCIONAMIENTO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("fechaRedirecccion")==null?"":contratoCesion.get("fechaRedirecccion")%></td>
											</tr>
											<tr>
												<td align="left" class="formas" width="300">PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:&nbsp;</td>
												<td align="left" class="formas" width="300">&nbsp;<%=contratoCesion.get("nombreUsrRedirec")==null?"":contratoCesion.get("nombreUsrRedirec")%></td>
											</tr>
											<tr>
												<td align="center" class="formas" colspan="2">&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<br/>
						</td>
					</tr>
					<tr>
						<td>
							<table align="center" border="0" width="600" cellpadding="0" cellspacing="0">
								<tr><td>&nbsp;</td></tr>
								<tr><td align="center" class="formas"><b>SE SUJETA A LAS SIGUIENTES CL�USULAS</b></td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td align="center">
										<table align="center" border="1" width="600">
											<tr>
												<td class="formas" align="left">
													<%=clausulado_parametrizado.toString().replaceAll("\n", "<br/>")%>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<%if (contratoCesion.get("claveUsrAutRep1") != null && !contratoCesion.get("claveUsrAutRep1").toString().equals("")) {%>
					<tr>
						<td>
							<table border="0" width="600" cellpadding="0" cellspacing="0">
								<tr><td>&nbsp;</td></tr>
								<tr><td align="left" class="formas"><b>Sello Digital de <%=contratoCesion.get("nombreUsrAutRep1")%> representante legal de <%=contratoCesion.get("nombre_pyme")%></b></td></tr>
								<tr>
									<td align="left">
										<table align="left" border="1" width="300">
											<tr><td class="formas" align="left"><%=contratoCesion.get("firmaRep1")%></td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<%}%>
					<%if (contratoCesion.get("claveUsrAutRep2") != null && !contratoCesion.get("claveUsrAutRep2").toString().equals("")) {%>
					<tr>
						<td>
							<table border="0" width="600" cellpadding="0" cellspacing="0">
								<tr><td>&nbsp;</td></tr>
								<tr><td align="left" class="formas"><b>Sello Digital de <%=contratoCesion.get("nombreUsrAutRep2")%> representante legal de <%=contratoCesion.get("nombre_pyme")%></b></td></tr>
								<tr>
									<td align="left">
										<table align="left" border="1" width="300">
											<tr><td class="formas" align="left"><%=contratoCesion.get("firmaRep2")%></td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<%}%>
					</table>
				</td>
			</tr>
		</table>
		<br/>
		<br/>	
		<br/>
		<br/>
<%}else{%>
      <table align="center" width="600" border="0" cellspacing="0" cellpadding="2">
        <tr>
          <td class="formas" colspan="2" align="center">&nbsp;</td>
        </tr>
        <tr>
          <td class="titulo" colspan="2" align="center"><b>La autentificaci�n no se llev� a cabo.<br/>PROCESO CANCELADO</b></td>
        </tr>
				<tr>
          <td class="formas" colspan="2" align="center">&nbsp;</td>
        </tr>   
				<tr>
          <td class="titulo" colspan="2" align="center"><b><%=mostrarError%></b></td>
        </tr>
        <tr>
          <td class="formas" colspan="2" align="center">&nbsp;</td>
        </tr>               
      </table>
<%}%>