<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,		
		java.text.*, 
		com.netro.cesion.*,
		netropology.utilerias.usuarios.UtilUsr,
		netropology.utilerias.usuarios.Usuario,
		netropology.utilerias.*,
		com.netro.exception.NafinException"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>

<%
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
String acuseEnvio = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
String fechaCarga = (request.getParameter("fechaCarga")!=null)?request.getParameter("fechaCarga"):"";
String horaCarga = (request.getParameter("horaCarga")!=null)?request.getParameter("horaCarga"):"";
String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");
		
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

HashMap consultaAcuseEnvioSolicitud = cesionBean.consultarSolicitudConsentimiento(clave_solicitud, loginUsuario);
String claveEpo = (String)consultaAcuseEnvioSolicitud.get("clave_epo");


HashMap camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
int indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));

String clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);

String nombresRepresentantesPyme = "";
String clavePyme = (String)request.getSession().getAttribute("iNoCliente");	
UtilUsr utilUsr = new UtilUsr();
List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");
			
for (int i = 0; i < usuariosPorPerfil.size(); i++) {
	String loginUsrPyme = (String)usuariosPorPerfil.get(i);
	Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
	nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre() + ",";
}

String huboError ="", mostrarError ="";
	if(!acuseEnvio.equals("")){
		int tamanio2 =	acuseEnvio.length();
			 huboError =  acuseEnvio.substring(0, 1);	
			 mostrarError  = acuseEnvio.substring(1, tamanio2);	
	}	
	
	//FODEA-024-2014 MOD()
String cedente =(String)consultaAcuseEnvioSolicitud.get("nombre_pyme");
String empresas =(String)consultaAcuseEnvioSolicitud.get("empresas");

String claveGrupo =(String)consultaAcuseEnvioSolicitud.get("clave_grupo_cesion");

//cedente+((representadas!="")?""+", "+representadas.replaceAll(";",", "):"")+", "+
String empresasRepresentadas = cedente+((empresas!="")?""+", "+empresas.replaceAll(";",", "):"");

	
%>

<html>
	<head>
		<title>Cesión de Derechos</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="<%=strDirecVirtualCSS%><%=strClase%>">
	</head>
	<body bgcolor="FFFFFF" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
	<br/>
	<%if(!huboError.equals("|")){%>
	<table border="0" width="900" cellpadding="0" cellspacing="0">
	  <tr>
		 <td align="center">
			<table align="center" border="1" width="850" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" class="celda01">Dependencia</td>
					<td align="center" class="celda01">Intermediario<br/>Financiero(Cesionario)</td>
					<td align="center" class="celda01">PYME<br/>(Cedente)</td>
					<td align="center" class="celda01">No. de <br/>Contrato</td>
					<td align="center" class="celda01">Firma <br/>Contrato</td>
					<td align="center" class="celda01">Monto / Moneda</td>
					<td align="center" class="celda01">Tipo de <br/>Contratación</td>
					<td align="center" class="celda01">Fecha Inicio<br/>Contrato</td>
					<td align="center" class="celda01">Fecha Final<br/>Contrato</td>
					<td align="center" class="celda01">Plazo del<br/>Contrato</td>
					<% if(!clasificacionEpo.equals("")){ %>
						<td align="center" class="celda01"><%=clasificacionEpo%></td>
					<%}%>
					<%for(int i = 0; i < indiceCamposAdicionales; i++){%>
							<td align="center" class="celda01"><%=camposAdicionalesParametrizados.get("nombre_campo_"+i)%></td>
					<%}%>
					<td align="center" class="celda01">Ventanilla de Pago</td>
					<td align="center" class="celda01">Supervisor/<br/>Administrador/<br/>Residente de Obra</td>
					<td align="center" class="celda01">Teléfono</td>
					<td align="center" class="celda01">Objeto del <br/>Contrato</td>
					<td align="center" class="celda01">Estatus</td>
				</tr>
				<tr>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("nombre_epo")%></td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("nombre_if")%></td>
					<td align="center" class="formas"><%=empresasRepresentadas%></td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("numero_contrato")%></td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("firma_contrato")==null?"":consultaAcuseEnvioSolicitud.get("firma_contrato")%></td>
					<td align="left" class="formas" nowrap="nowrap"><%=consultaAcuseEnvioSolicitud.get("montosPorMoneda").toString().replaceAll("\n", "<br/>")%></td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("tipo_contratacion")%></td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("fecha_inicio_contrato")%></td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("fecha_fin_contrato")%></td>
					<td align="center" class="formas" nowrap="nowrap"><%=consultaAcuseEnvioSolicitud.get("plazoContrato")%></td>
					<% if(!clasificacionEpo.equals("")){ %>
						<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("clasificacionEpo")%></td>
					<%}%>
					<%for(int i = 0; i < indiceCamposAdicionales; i++){%>
						<td align="center" class="formas">&nbsp;<%=consultaAcuseEnvioSolicitud.get("campo_adicional_"+(i+1))%></td>
					<%}%>
					<td align="center" class="formas">&nbsp;<%=consultaAcuseEnvioSolicitud.get("venanillaPago")%></td>
					<td align="center" class="formas">&nbsp;<%=consultaAcuseEnvioSolicitud.get("supAdmResob")%></td>
					<td align="center" class="formas">&nbsp;<%=consultaAcuseEnvioSolicitud.get("numeroTelefono")%></td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("objeto_contrato")%></td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("estatus_solicitud")%></td>
				</tr>
			</table>
			<br/>
		 </td>
	  </tr>
	</table>
	<br/>
	<br/>
	<table border="0" width="900" cellpadding="0" cellspacing="0">
	  <tr>
		 <td align="center">
			<table align="center" border="1" width="300" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" class="celda01" colspan="2">Cifras de Control</td>
				</tr>
				<tr>
					<td align="center" class="formas">Número de Acuse</td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("acuseEnvioRep")%></td>
				</tr>
				<tr>
					<td align="center" class="formas">Fecha de Carga</td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("fechaEnvRep")%></td>
				</tr>
				<tr>
					<td align="center" class="formas">Hora de Carga</td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("horaEnvRep")%></td>
				</tr>
				<tr>
					<td align="center" class="formas">Usuario de Captura</td>
					<td align="center" class="formas"><%=consultaAcuseEnvioSolicitud.get("usuarioEnvRep")%></td>
				</tr>
			</table>
			<br/>
		 </td>
	  </tr>
	</table>
	<br/>
	<table border="0" width="900" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center">
				<table align="center" border="1" width="850">
					<tr>
						<td class="formas" align="left">
							<%    
								String textRepGrupo = "";
								if(!"".equals(claveGrupo)){
									List lstEmpresasXGrupo = cesionBean.getEmpresasXGrupoCder(claveGrupo);
									String cvePymeGrupo = "";
									String nombrePymeGrupo = "";
									String representantes = "";
									for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
										cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
										if(!clavePyme.equals(cvePymeGrupo)){
											nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
											representantes = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("representantes");
											textRepGrupo += representantes+" como representante(s) legal(es) de la Empresa  "+nombrePymeGrupo+",  ";
										}
									}
								}
							
							%>
							De conformidad con lo señalado en el(los) Contrato(s) cuyos datos se señalan en esta pantalla, 
							<%=nombresRepresentantesPyme%>, como representante(s) legal(es) de la Empresa <%=strNombre%>, <%=textRepGrupo%> 							  						  
							solicito(tamos) a <%=consultaAcuseEnvioSolicitud.get("nombre_epo")%> su consentimiento para ceder a favor del INTERMEDIARIO FINANCIERO 
							señalado para estos efectos, los derechos de cobro sobre la(s) factura(s) por trabajos ejecutados, 
							por el 100% (cien por ciento) del monto de dicho Contrato.
							<br/><br/>
							En virtud de lo anterior una vez que cuente con el consentimiento que por este medio le es solicitado, 
							celebraremos el contrato de cesión de derechos de cobro con el INTERMEDIARIO FINANCIERO, quien procederá a notificar mediante el 
							Sistema NAFIN, en términos de Código de Comercio.				
							
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br/>
	<br/>
	<%} else {%>
      <table align="center" width="600" border="0" cellspacing="0" cellpadding="2">
        <tr>
          <td class="formas" colspan="2" align="center">&nbsp;</td>
        </tr>
        <tr>
          <td class="titulo" colspan="2" align="center"><b>La autentificación no se llevó a cabo.<br/>PROCESO CANCELADO</b></td>
        </tr>
        <tr>
          <td class="formas" colspan="2" align="center">&nbsp;</td>
        </tr> 
				<tr>
          <td class="titulo" colspan="2" align="center"><b><%=mostrarError%></b></td>
        </tr>
				<tr>
          <td class="formas" colspan="2" align="center">&nbsp;</td>
        </tr> 				
      </table>
    <%}%>
	<br/>
	</body>
</body>
</html>

