<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		netropology.utilerias.usuarios.UtilUsr,
		netropology.utilerias.usuarios.Usuario,
		com.netro.cesion.*,
		java.text.*, 
		com.netro.exception.NafinException,
		com.netro.model.catalogos.*,
		net.sf.json.*,		
		java.io.File,		
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%@ include file="../certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<% 
String informacion        = (request.getParameter("informacion")        !=null)?request.getParameter("informacion")       :"";
String operacion          = (request.getParameter("operacion")          !=null)?request.getParameter("operacion")         :"";
String clave_epo          = (request.getParameter("clave_epo")          !=null)?request.getParameter("clave_epo")         :"";
String clave_if           = (request.getParameter("clave_if")           !=null)?request.getParameter("clave_if")          :"";
String numero_contrato    = (request.getParameter("numero_contrato")    !=null)?request.getParameter("numero_contrato")   :"";
String clave_moneda       = (request.getParameter("clave_moneda")       !=null)?request.getParameter("clave_moneda")      :"";
String clave_contratacion = (request.getParameter("clave_contratacion") !=null)?request.getParameter("clave_contratacion"):"";
String tipo_archivo       = (request.getParameter("tipo_archivo")       !=null)?request.getParameter("tipo_archivo")      :"";
String clave_solicitud    = (request.getParameter("clave_solicitud")    !=null)?request.getParameter("clave_solicitud")   :"";
String fecha_inicio       = (request.getParameter("fecha_inicio")       !=null)?request.getParameter("fecha_inicio")      :"";
String fecha_final        = (request.getParameter("fecha_final")        !=null)?request.getParameter("fecha_final")       :"";
String nombreEPO          = (request.getParameter("nombreEPO")          !=null)?request.getParameter("nombreEPO")         :"";
String claveGrupo         = (request.getParameter("claveGrupo")         !=null)?request.getParameter("claveGrupo")        :"";
String clave_pyme = (String)request.getSession().getAttribute("iNoCliente");
String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");

String numeroAcuse      = (request.getParameter("numeroAcuse")      !=null)?request.getParameter("numeroAcuse")       :"";
String fechaCarga       = (request.getParameter("fechaCarga")       !=null)?request.getParameter("fechaCarga")        :"";
String horaCarga        = (request.getParameter("horaCarga")        !=null)?request.getParameter("horaCarga")         :"";
String usuarioCaptura   = (request.getParameter("usuarioCaptura")   !=null)?request.getParameter("usuarioCaptura")    :"";
String nombreCesionario = (request.getParameter("nombreCesionario") !=null)?request.getParameter("nombreCesionario")  :"";

int start = 0;
int limit = 0;
String tipoExtincion = ""; // Fodea 04-2016
String infoRegresar = "";
String fechaHoy     = ""; 
//String horaCarga    = "";

try{
	fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	horaCarga = new java.text.SimpleDateFormat("HH:mm").format(new java.util.Date());
} catch(Exception e){
	fechaHoy = "";
	horaCarga = "";
}

String pais             = (String)session.getAttribute("strPais");
String nafinElectronico = ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString());
String noExterno = 	(String)session.getAttribute("sesExterno");
String nombre = 	(String) session.getAttribute("strNombre");
String nombreUsua =	(String) session.getAttribute("strNombreUsuario");
String logo = (String)session.getAttribute("strLogo");
String directorioPubli = (String) application.getAttribute("strDirectorioPublicacion");
		
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);


if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setCs_cesion_derechos("S");
	infoRegresar = cat.getJSONElementos();	

} else if(informacion.equals("CatalogoIF")){

		CatalogoIF cat = new CatalogoIF();
		cat.setClave("ic_if");
		cat.setDescripcion("cg_razon_social");
		cat.setG_cs_cesion_derechos("S");
		infoRegresar = cat.getJSONElementos();	

} else if(informacion.equals("CatalogoMoneda")){

	CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("0,1,54,25", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	

} else if(informacion.equals("CatalogoContratacion") && !clave_epo.equals("") ){

	JSONArray registros = new JSONArray();
	HashMap info = new HashMap();
	List catalogoTipoContratacion = cesionBean.obtenerComboTipoContratacion(clave_epo);

	for(int i=0; i<catalogoTipoContratacion.size(); i++){
	
		String valor  = (String)catalogoTipoContratacion.get(i).toString(); 
		String clave =valor.substring(0,1);
		String descripcion =valor.substring(2,valor.length());
		info = new HashMap();
		info.put("clave", clave);
		info.put("descripcion", descripcion);	
		registros.add(info);
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

} else if(informacion.equals("Consultar") || informacion.equals("ArchivoPDF") || informacion.equals("ArchivoCSV") ){

	tipoExtincion = cesionBean.getTipoExtincion(clave_epo); // Fodea 04-2016

	ConsultaContratosExtincionPyme paginador =  new ConsultaContratosExtincionPyme();
	
	paginador.setClavePyme(clave_pyme);
	paginador.setClaveEpo(clave_epo);
	paginador.setClaveIfCesionario(clave_if);
	paginador.setNumeroContrato(numero_contrato);
	paginador.setClaveMoneda(clave_moneda);
	paginador.setClaveTipoContratacion(clave_contratacion);
	paginador.setFechaInicio(fecha_inicio);
	paginador.setFechaFin(fecha_final);

	if("L".equals(tipoExtincion)){

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros registros  =null;
	
	if (informacion.equals("Consultar")){
		String consulta          = "";
		String cedente           = "";
		String empresas          = "";
		String nombre_cesionario = "";
		String pymePrincipal     = "";
		JSONObject 	resultado	= new JSONObject();
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")){ //Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					
				consulta  = queryHelper.getJSONPageResultSet(request,start,limit);	
				registros 	= queryHelper.getPageResultSet(request,start,limit);						
				resultado = JSONObject.fromObject(consulta);

				while (registros.next()){
					String montosMonedaSo2 =  cesionBean.getMontoMoneda(registros.getString("clave_solicitud")).toString().replaceAll(",", "");				
					String moneda = "moneda"+registros.getString("clave_solicitud");	
					cedente = registros.getString("cedente").toString();
					empresas = registros.getString("empresas").toString();
					nombre_cesionario = registros.getString("nombre_cesionario").toString();
					resultado.put(moneda, montosMonedaSo2);						
				}	
					
				//obtencion la Clasificacion EPO
				String clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
				if(clasificacionEpo ==null) { clasificacionEpo=""; }
				resultado.put("clasificacionEpo", clasificacionEpo);	
			
				//obtencion de campos Adicionales
				HashMap camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
				int indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));
				resultado.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));	 //numero de campos Adicionales
				for(int y = 0; y < indiceCamposAdicionales; y++){	
					String nombreCampo = ("Campo"+y);
					resultado.put(nombreCampo,camposAdicionalesParametrizados.get("nombre_campo_"+y));						
				}			
/*
				String multiUsrPyme ="N";
				UtilUsr utilUsr = new UtilUsr();
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");
				log.info("usuariosPorPerfil.size(): " + usuariosPorPerfil.size());
				if (usuariosPorPerfil.size() > 1) {
					multiUsrPyme = "S";
				}				
				resultado.put("multiUsrPyme", multiUsrPyme);	
*/
			HashMap parametrosConsulta = new HashMap();
			parametrosConsulta.put("loginUsuario", loginUsuario);
			parametrosConsulta.put("clavePyme", clave_pyme);
			parametrosConsulta.put("claveEpo", clave_epo);
			parametrosConsulta.put("claveIfCesionario", clave_if);
			parametrosConsulta.put("numeroContrato", numero_contrato);
			parametrosConsulta.put("claveMoneda", clave_moneda);
			parametrosConsulta.put("claveTipoContratacion", clave_contratacion);
			parametrosConsulta.put("perfilUsuario", strPerfil);
								
			HashMap solicitudesAutorizadasPorUsuario = cesionBean.obtenerExtincionesPorUsuario(parametrosConsulta);
			int numSolAut = Integer.parseInt((String)solicitudesAutorizadasPorUsuario.get("numeroRegistros"));
			resultado.put("numSolAut", String.valueOf(numSolAut));	
			resultado.put("solicitudesAutorizadasPorUsuario", solicitudesAutorizadasPorUsuario);	

				//Determina si la pyme es la principal
				boolean esPrincipal = cesionBean.getEmpresaPrincipalGrupoCDer(clave_pyme);
				if(esPrincipal == true){
					pymePrincipal = "S";
				} else{
					pymePrincipal = "N";
				}
				resultado.put("PYME_PRINCIPAL", pymePrincipal);
				resultado.put("PERFIL",strPerfil);
			resultado.put("CEDENTE",cedente);

			infoRegresar = resultado.toString();
			
			} 
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

	} else if (informacion.equals("ArchivoPDF") ||  informacion.equals("ArchivoCSV") ) {
		try {
		
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipo_archivo);
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();		
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF o CSV", e);
		}
	} 

	} else{
		infoRegresar =  "{\"success\": true, \"total\":0, \"registros\":[]}";
	}
 
} else if(informacion.equals("CONTRATO_CESION_PYME")){
	
	
	
		List datos = new ArrayList();
		datos.add(pais);
		datos.add(nafinElectronico);
		datos.add(noExterno);
		datos.add(nombre);
		datos.add(nombreUsua);
		datos.add(logo);
		datos.add(strDirectorioTemp);
		datos.add(directorioPubli);
		datos.add(tipo_archivo);
		datos.add(clave_solicitud);
		datos.add(clave_epo);
		datos.add(clave_if);
		
		String nombreArchivo = cesionBean.DescargaContratoCesionPyme(datos);
		JSONObject jsonObjC = new JSONObject();
		jsonObjC.put("success", new Boolean(true));
		jsonObjC.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObjC.toString();	
		
 

}if(informacion.equals("CONTRATO")){
	
		JSONObject jsonObj = new JSONObject();	
		//File file = null;		
		String file =cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+file);
		infoRegresar = jsonObj.toString();	
		
}if(informacion.equals("ACUSE_CONTRATO")){

	Calendar calendario = Calendar.getInstance();
	SimpleDateFormat formato_cert = new SimpleDateFormat("ddMMyyyyHHmmss");
	SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat formato_hora = new SimpleDateFormat("hh:mm aa");
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
	String textoFirmado = (request.getParameter("textoFirmado")!=null)?request.getParameter("textoFirmado"):"";
	String nuevoEstatus = (request.getParameter("nuevoEstatus")!=null)?request.getParameter("nuevoEstatus"):"";
	String fechaExtincionContrato = (request.getParameter("fechaExtincionContrato")!=null)?request.getParameter("fechaExtincionContrato"):"";
	String numeroCuenta = (request.getParameter("numeroCuenta")!=null)?request.getParameter("numeroCuenta"):"";
	String numeroCuentaClabe = (request.getParameter("numeroCuentaClabe")!=null)?request.getParameter("numeroCuentaClabe"):"";
	String bancoDeposito = (request.getParameter("bancoDeposito")!=null)?request.getParameter("bancoDeposito"):"";
	String folioCert = formato_cert.format(calendario.getTime());
	
	HashMap parametrosFirma = new HashMap();
	parametrosFirma.put("_serial",                _serial                 );
	parametrosFirma.put("pkcs7",                  pkcs7                   );
	parametrosFirma.put("textoPlano",             textoFirmado            );
	parametrosFirma.put("validCert",              String.valueOf(false)   );
	parametrosFirma.put("folioCert",              folioCert               );
	parametrosFirma.put("loginUsuario",           loginUsuario            );
	parametrosFirma.put("nombreUsuario",          strNombreUsuario        );
	parametrosFirma.put("claveSolicitud",         clave_solicitud         );
	parametrosFirma.put("nuevoEstatus",           nuevoEstatus            );
	parametrosFirma.put("fechaExtincionContrato", fechaExtincionContrato  );
	parametrosFirma.put("numeroCuenta",           numeroCuenta            );
	parametrosFirma.put("numeroCuentaClabe",      numeroCuentaClabe       );
	parametrosFirma.put("bancoDeposito",          bancoDeposito           );
	parametrosFirma.put("strDirectorioTemp",      strDirectorioTemp       );
	
	String acuse = cesionBean.acuseEnvioSolicitudExtincion(parametrosFirma);
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success",   new Boolean(true)              );
	jsonObj.put("acuse",     acuse                          );
	jsonObj.put("fecCarga",  fechaHoy                       );
	jsonObj.put("horaCarga", horaCarga                      );
	jsonObj.put("captUser",  iNoUsuario+" "+strNombreUsuario);
	infoRegresar = jsonObj.toString();	

}if(informacion.equals("ARCHIVO_ACUSE")){
	
	String cedente = (request.getParameter("cedente")!=null)?request.getParameter("cedente"):"";
	String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";	
	String cedente_em = (request.getParameter("cedente_em")!=null)?request.getParameter("cedente_em"):"";	
	mensaje= mensaje.replaceAll("<br>", "\n");	
	mensaje= mensaje.replaceAll("<p>", "");	
	mensaje= mensaje.replaceAll("</p>", "");
	
		List datos = new ArrayList();
		datos.add(pais);
		datos.add(nafinElectronico);
		datos.add(noExterno);		
		datos.add(cedente.replaceAll("<br>", "\n"));
		datos.add(nombreUsua);
		datos.add(logo);
		datos.add(strDirectorioTemp);
		datos.add(directorioPubli);
		datos.add(tipo_archivo);
		datos.add(clave_solicitud);
		datos.add(clave_epo);
		datos.add(clave_if);
		datos.add(clave_pyme);
		datos.add(mensaje);
		datos.add(cedente_em.replaceAll(";", "\n"));
		datos.add(numeroAcuse);
		datos.add(fechaCarga);
		datos.add(horaCarga);
		datos.add(usuarioCaptura);

	String nombreArchivo = cesionBean.DescargaContratoCesionAcuse(datos);
	JSONObject jsonObjC = new JSONObject();
	jsonObjC.put("success", new Boolean(true));
	jsonObjC.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObjC.toString();	
		
}if(informacion.equals("CANCELAR")){

	cesionBean.cancelarSolicitudExtincion(clave_solicitud);
	
	JSONObject jsonObjC = new JSONObject();
	jsonObjC.put("success", new Boolean(true));	
	infoRegresar = jsonObjC.toString();	
	


} if(informacion.equals("MENSAJE_EXTINCION")){

	String nombresRepresentantesPyme = "";
	String clavePyme = (String)request.getSession().getAttribute("iNoCliente");
	UtilUsr utilUsr = new UtilUsr();
	List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");
	for (int i = 0; i < usuariosPorPerfil.size(); i++) {
		String loginUsrPyme = (String)usuariosPorPerfil.get(i);
		Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
		nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre() + ",";
	}

	String textRepGrupo = "";
	if(!"".equals(claveGrupo)){
		List lstEmpresasXGrupo = cesionBean.getEmpresasXGrupoCder(claveGrupo);
		String cvePymeGrupo = "";
		String nombrePymeGrupo = "";
		String representantes = "";
		for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
			cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
			if(!clavePyme.equals(cvePymeGrupo)){
				nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
				representantes = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("representantes");
				textRepGrupo += representantes+" como representante(s) legal(es) de la Empresa  "+nombrePymeGrupo+",  ";
			}
		}
	}

	String mensajeNuevo="<br><p>De conformidad con lo señalado en el(los) Contrato(s) cuyos datos se señalan en esta pantalla, "+
							nombresRepresentantesPyme+" como representante(s) legal(es) de la Empresa  "+strNombre+", "+textRepGrupo+
							" solicito(tamos) a  "+nombreCesionario+" su validación/aceptación para extinguir el contrato de Derechos de Cobro.</p><br>"+
							" Al mismo tiempo manifiesto(amos) que se cuenta con las facultades para actos de dominio necesarias para llevar a cabo la Cesión  "+
							"de Derechos de Cobro conforme se acredita mediante los poderes anexos a la presente, mismos que no han sido revocados, modificados o  "+
							"limitados en forma alguna a la presente fecha.</p><br>";

	JSONObject jsonObjC = new JSONObject();
	jsonObjC.put("success", new Boolean(true));
	jsonObjC.put("mensajeNuevo",mensajeNuevo);
	infoRegresar = jsonObjC.toString();	

}

%>

<%=infoRegresar%>