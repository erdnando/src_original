<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.text.*, 
		netropology.utilerias.usuarios.UtilUsr,
		netropology.utilerias.usuarios.Usuario,
		com.netro.exception.NafinException,
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray,
		java.io.File,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>

<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String clave_if = (request.getParameter("clave_if")!=null)?request.getParameter("clave_if"):"";
String numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String csTipoMontoChkD = (request.getParameter("csTipoMontoChkD")!=null)?request.getParameter("csTipoMontoChkD"):""; 
String csTipoMontoChkM = (request.getParameter("csTipoMontoChkM")!=null)?request.getParameter("csTipoMontoChkM"):""; 
String claveTipoContratacion = (request.getParameter("claveTipoContratacion")!=null)?request.getParameter("claveTipoContratacion"):"";
String plazoContrato = (request.getParameter("plazoContrato")!=null)?request.getParameter("plazoContrato"):"";
String claveTipoPlazo = (request.getParameter("claveTipoPlazo")!=null)?request.getParameter("claveTipoPlazo"):"";
String claveMoneda = (request.getParameter("claveMoneda")!=null)?request.getParameter("claveMoneda"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String fecha_vigencia_ini = (request.getParameter("fecha_vigencia_ini")!=null)?request.getParameter("fecha_vigencia_ini"):"";
String fecha_vigencia_fin = (request.getParameter("fecha_vigencia_fin")!=null)?request.getParameter("fecha_vigencia_fin"):"";
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";

String nombreEpo = (request.getParameter("nombreEpo")!=null)?request.getParameter("nombreEpo"):"";	

String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");
String clavePyme = (String)request.getSession().getAttribute("iNoCliente");
String infoRegresar = "";
int start = 0;
int limit = 0;
String csTipoMonto ="";
if(csTipoMontoChkD.equals("") && csTipoMontoChkM.equals("") ) 	csTipoMonto ="";
if(!csTipoMontoChkD.equals("") && !csTipoMontoChkM.equals("") ) 	csTipoMonto ="A"; ;
if(!csTipoMontoChkD.equals("") && csTipoMontoChkM.equals("") ) csTipoMonto ="D";
if(csTipoMontoChkD.equals("") && !csTipoMontoChkM.equals("") ) 	csTipoMonto ="M";		
		
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);


if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setCs_cesion_derechos("S");
	infoRegresar = cat.getJSONElementos();	
	

}else if(informacion.equals("CatalogoIF")   && !clave_epo.equals("")){

	CatalogoIF cat = new CatalogoIF();
	cat.setClave("ic_if");
	cat.setDescripcion("cg_razon_social");
	cat.setG_cs_cesion_derechos("S");
	cat.setIc_epo(clave_epo);
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoMoneda") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setOrden("ic_moneda");
	cat.setValoresCondicionIn("0,1,54,25", Integer.class);
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoContratacion") ){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_contratacion");
	cat.setCampoDescripcion("cg_nombre");
	cat.setTabla("cdercat_tipo_contratacion");
	cat.setOrden("ic_tipo_contratacion");
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoTipoPlazo") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("cdercat_tipo_plazo");
	cat.setCampoClave("ic_tipo_plazo");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("ic_tipo_plazo");
	infoRegresar = cat.getJSONElementos();	

}else  if(informacion.equals("verificaParamGrupoCesion") ){
	
	boolean csActivaParamGrupo = cesionBean.getEmpresaPrincipalGrupoCDer(clavePyme);
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("csActivaParamGrupo", new Boolean(csActivaParamGrupo));
	jsonObj.put("clavePyme", clavePyme);
	jsonObj.put("strPerfil", strPerfil);
	infoRegresar = jsonObj.toString();


} else if(informacion.equals("Consultar")   
	|| informacion.equals("ArchivoPDF")  
	|| informacion.equals("ArchivoCSV") 
	|| informacion.equals("CONTRATO") 	){

	String consulta ="";
	JSONObject 	resultado	= new JSONObject();
	
	
		ConsulSolicitudConsentimiento paginador = new com.netro.cesion.ConsulSolicitudConsentimiento();
		
	//pasar parametros al Paginador 	
		paginador.setClaveEpo(clave_epo);
		paginador.setClaveIfCesionario(clave_if);
		paginador.setNumeroContrato(numero_contrato);
	 	paginador.setCsTipoMonto(csTipoMonto);		
		paginador.setClaveTipoContratacion(claveTipoContratacion);
		paginador.setPlazoContrato(plazoContrato);
		paginador.setClaveTipoPlazo(claveTipoPlazo);
		paginador.setClaveTipoPlazo(claveTipoPlazo);
		if (strTipoUsuario.equals("PYME")) {	
			paginador.setClavePyme(clavePyme);
		}
		paginador.setClaveMoneda(claveMoneda);
		paginador.setFechaVigenciaContratoIni(fecha_vigencia_ini);
		paginador.setFechaVigenciaContratoFin(fecha_vigencia_fin);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		Registros registros  =null;
	if(informacion.equals("Consultar")) {
		try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
		
	
		try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
			consulta  = queryHelper.getJSONPageResultSet(request,start,limit);	
			registros 	= queryHelper.getPageResultSet(request,start,limit);						
			resultado = JSONObject.fromObject(consulta);
			
				
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
			
			HashMap montosMonedaSol = new HashMap();
			while (registros.next()) {
				String montosMonedaSo2 =  cesionBean.getMontoMoneda(registros.getString("clave_solicitud")).toString().replaceAll(",", "");				
				String moneda = "moneda"+registros.getString("clave_solicitud");							
				resultado.put(moneda, montosMonedaSo2);						
			}	
				
			if(!clave_epo.equals("")){
				//esto para obtener  parametrizaciones	
				String paramSolConsEpo = "N";
				String multiUsrPyme ="N";
				HashMap parametrosConsulta = new HashMap();
				parametrosConsulta.put("loginUsuario", loginUsuario);
				parametrosConsulta.put("clavePyme", clavePyme);
				parametrosConsulta.put("claveEpo", clave_epo);
				parametrosConsulta.put("claveIfCesionario", clave_if);
				parametrosConsulta.put("numeroContrato", numero_contrato);
				parametrosConsulta.put("claveMoneda", claveMoneda);
				parametrosConsulta.put("claveTipoContratacion", claveTipoContratacion);
				parametrosConsulta.put("fechaVigenciaContratoIni", fecha_vigencia_ini);
				parametrosConsulta.put("fechaVigenciaContratoFin", fecha_vigencia_fin);
				parametrosConsulta.put("csTipoMonto", csTipoMonto);
				parametrosConsulta.put("plazoContrato", plazoContrato);
				parametrosConsulta.put("claveTipoPlazo", claveTipoPlazo);
								
				HashMap solicitudesEnviadasPorUsuario = cesionBean.obtieneSolicitudesEnviadasPorUsuario(parametrosConsulta);
				int numSolEnv = Integer.parseInt((String)solicitudesEnviadasPorUsuario.get("numeroRegistros"));
				resultado.put("numSolEnv", String.valueOf(numSolEnv) );	
				resultado.put("solicitudesEnviadasPorUsuario", solicitudesEnviadasPorUsuario);	
						
				HashMap solicitudesAutorizadasPorUsuario = cesionBean.obtieneSolicitudesAutorizadasPorUsuario(parametrosConsulta);
				int numSolAut = Integer.parseInt((String)solicitudesAutorizadasPorUsuario.get("numeroRegistros"));
				resultado.put("numSolAut", String.valueOf(numSolAut));	
			  resultado.put("solicitudesAutorizadasPorUsuario", solicitudesAutorizadasPorUsuario);	
					
				ArrayList parametrosEpoCesion = cesionBean.getParametrosEPO(clave_epo);
				if (parametrosEpoCesion.size() > 0) {
					paramSolConsEpo = parametrosEpoCesion.get(0)==null?"N":parametrosEpoCesion.get(0).toString();
				}		
				
				
				resultado.put("paramSolConsEpo", paramSolConsEpo);	
				
				UtilUsr utilUsr = new UtilUsr();
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");
				if (usuariosPorPerfil.size() > 1) {
					multiUsrPyme = "S";
				}
				
				resultado.put("multiUsrPyme", multiUsrPyme);	
					
					
				String nombresRepresentantesPyme = "";
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {
					String loginUsrPyme = (String)usuariosPorPerfil.get(i);
					Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
					nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre() + ",";
				}
				
				//obtencion la Clasificacion EPO
				String clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
				resultado.put("clasificacionEpo", clasificacionEpo);	
			
				//obtencion de campos Adicionales
				HashMap camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
				int indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));
				resultado.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));	 //numero de campos Adicionales
				for(int y = 0; y < indiceCamposAdicionales; y++){	
					String nombreCampo = ("Campo"+y);
					resultado.put(nombreCampo,camposAdicionalesParametrizados.get("nombre_campo_"+y));						
				}
			}	
			
			
			infoRegresar = resultado.toString(); 
										
			
	} else 	if (informacion.equals("ArchivoPDF")) {
				try {
																								
					String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
					
					JSONObject jsonObj = new JSONObject();
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					infoRegresar = jsonObj.toString();
					
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo PDF", e);
				}
				
				
	} else 	if (informacion.equals("ArchivoCSV")) {	 
	
		try {
																								
					String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
					
					JSONObject jsonObj = new JSONObject();
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					infoRegresar = jsonObj.toString();
					
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo CSV", e);
				}		
	

	}else if (informacion.equals("CONTRATO")) {
		JSONObject jsonObj = new JSONObject();			
		
		//File file = null;		
		String file = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+file);
	
		infoRegresar = jsonObj.toString();
	
	}
}else if (informacion.equals("BORRAR")) {

	JSONObject jsonObj = new JSONObject();
	cesionBean.eliminaSolicitud(clave_solicitud);	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();


}else if (informacion.equals("ENVIAR")  ||  informacion.equals("ACEPTAR") ) { 
	%><%@ include file="../certificado.jspf" %><%
	Calendar calendario = Calendar.getInstance();
	SimpleDateFormat formato_cert = new SimpleDateFormat("ddMMyyyyHHmmss");
	SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat formato_hora = new SimpleDateFormat("hh:mm aa");
			
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
	String textoFirmado = (request.getParameter("textoFirmado")!=null)?request.getParameter("textoFirmado"):"";
	String claveNueva = (request.getParameter("claveNueva")!=null)?request.getParameter("claveNueva"):"";	
	
	String fecha_carga = formato_fecha.format(calendario.getTime());
	String hora_carga = formato_hora.format(calendario.getTime());
	String login_usuario = (String)request.getSession().getAttribute("Clave_usuario");
	String nombre_usuario = (String)request.getSession().getAttribute("strNombreUsuario");
	String folioCert = formato_cert.format(calendario.getTime());
		
	HashMap parametrosEnvio = new HashMap();
	parametrosEnvio.put("_serial", _serial);
	parametrosEnvio.put("textoFirmado", textoFirmado);
	parametrosEnvio.put("pkcs7", pkcs7);
	parametrosEnvio.put("folioCert", folioCert);
	parametrosEnvio.put("validCert", String.valueOf(false));
	parametrosEnvio.put("numeroSolicitud", clave_solicitud);
	parametrosEnvio.put("nuevoEstatus", claveNueva);
	parametrosEnvio.put("loginUsuario", loginUsuario);
	parametrosEnvio.put("nombreUsuario", nombre_usuario);
	parametrosEnvio.put("fechaCarga", fecha_carga);
	parametrosEnvio.put("horaCarga", hora_carga);
			
	String acuseEnvio = cesionBean.enviaSolicitudConsentimiento(parametrosEnvio);
	String huboError ="", mostrarError ="";
	if(!acuseEnvio.equals("")){
		int tamanio2 =	acuseEnvio.length();
			 huboError =  acuseEnvio.substring(0, 1);	
			 if(huboError.equals("|")){
					mostrarError = "Error";	
			 }
	}	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("acuse",acuseEnvio);	
	jsonObj.put("mostrarError",mostrarError);	 
	jsonObj.put("clave_solicitud",clave_solicitud);	
	
	infoRegresar = jsonObj.toString();	
	

}	

%>

<%=infoRegresar%>
