
Ext.onReady(function() {

//procesar el Acuse 
	function procesarSuccessFailureAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
				Ext.MessageBox.alert('Mensaje','Solicitud guardada con �xito estatus actual: Por Solicitar');
			  var numeroSolicitud= jsondeAcuse.numeroSolicitud;	
			 	var clave_epo = Ext.getDom('clave_epo').value;
				var clave_grupo= Ext.getDom('clave_grupo').value
				var parametros = "numeroSolicitud="+numeroSolicitud+"&clave_epo="+clave_epo+"&clave_grupo="+clave_grupo;
				document.location.href = "34SolicitudConsentimientoAcuseExt.jsp?"+parametros;					
			}		
		}
	}
		
	var Guardar =function(){		
		Ext.Ajax.request({
			url : '34Solic_Consentimientoext.data.jsp',
			params : {
				informacion: "Acuse",
				numeroProceso: Ext.getDom('numeroProceso').value,
				clave_epo: Ext.getDom('clave_epo').value								
			},
			callback: procesarSuccessFailureAcuse
		});
		
	}				
				
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var clasificacionEpo = jsonData.clasificacionEpo;	
		var hayCamposAdicionales = jsonData.hayCamposAdicionales;
		var nombreEPO = jsonData.nombreEPO;
		var cedente = jsonData.registros[0].nombre_pyme;//CORRECCION() FODEA-024-2014
		var representadas = jsonData.registros[0].empresas;//CORRECCION() FODEA-024-2014
		var fp = Ext.getCmp('forma');
		var el = grid.getGridEl();	
		//fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}	
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro 
			var cm = grid.getColumnModel();
			if(clasificacionEpo ==''){		
				grid.getColumnModel().setHidden(cm.findColumnIndex('clasificacionEpo'), true);					
			}else{
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('clasificacionEpo'),clasificacionEpo);
			}
			
			if(hayCamposAdicionales=='0'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_1'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_5'), true);
			}
			if(hayCamposAdicionales=='1'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_5'), true)				
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_1'),jsonData.Campo0);
			}
			if(hayCamposAdicionales=='2'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_5'), true)
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_2'),jsonData.Campo1);
			}
			if(hayCamposAdicionales=='3'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_2'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_3'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_5'), true)	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_2'),jsonData.Campo1);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_3'),jsonData.Campo2);
			}
			if(hayCamposAdicionales=='4'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_2'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_3'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_4'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_5'), true);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_2'),jsonData.Campo1);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_3'),jsonData.Campo2);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_4'),jsonData.Campo3);
			}
				
			if(hayCamposAdicionales=='5'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_1'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_2'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_3'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_4'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('campo_adicional_5'), false);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_1'),jsonData.Campo0);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_2'),jsonData.Campo1);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_3'),jsonData.Campo2);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_4'),jsonData.Campo3);
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('campo_adicional_5'),jsonData.Campo4);
			}
			
			
			//para mostrar  panel de Nota 
				var bodyPanel = Ext.getCmp('Nota').body;
				Nota.show();
				var mgr = bodyPanel.getUpdater();
				mgr.on('failure', 
					function(el, response) {
						bodyPanel.update('');
						NE.util.mostrarErrorResponse(response);
					}
				);		
				mgr.update({
					url: '34SolicitudConsentimientoPreacuseNotaext.jsp',
					
					scripts: true,
					params: {
						nombreEPO: nombreEPO,
						cedente :cedente,
						representadas:representadas,
						clave_grupo: Ext.getDom('clave_grupo').value
					},
					indicatorText: 'Cargando Nota'
				});	
				
				
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
		

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34Solic_Consentimientoext.data.jsp',
		baseParams: {
			informacion: "Preacuse",
			numeroProceso: Ext.getDom('numeroProceso').value,
			clave_epo: Ext.getDom('clave_epo').value,
			clave_if: Ext.getDom('clave_if').value			
		},
		fields: [
			{name: 'nombre_epo'},
			{name: 'nombre_if'}, 
			{name: 'numero_contrato'}, 
			{name: 'montosPorMoneda'},
			{name: 'tipo_contratacion'}, 
			{name: 'fecha_inicio_contrato'}, 
			{name: 'fecha_fin_contrato'},
			{name: 'plazoContrato'},
			{name: 'clasificacionEpo'},	
			{name: 'campo_adicional_1'},
			{name: 'campo_adicional_2'},
			{name: 'campo_adicional_3'},
			{name: 'campo_adicional_4'},
			{name: 'campo_adicional_5'},
			{name: 'venanillaPago'},
			{name: 'supAdmResob'},
			{name: 'numeroTelefono'},
			{name: 'objeto_contrato'},
			{name: 'estatus_solicitud'},
			{name: 'firma_contrato'},
			{name: 'empresas'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});
	
	
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: false,
		margins: '20 0 0 0',
		title: 'Solicitud de Consentimiento PreAcuse',
		columns: [
			{
				header: 'Dependencia',
				tooltip: 'Dependencia',
				dataIndex: 'nombre_epo',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Intermediario Financiero (Cesionario)',
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'nombre_if',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'PYME(Cedente)',
				tooltip: 'PYME(Cedente)',
				dataIndex: 'empresas',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'No. de Contrato',
				tooltip: 'No. de Contrato',
				dataIndex: 'numero_contrato',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'firma_contrato',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto original del contrato / Moneda',
				tooltip: 'Monto original del contrato / Moneda',
				dataIndex: 'montosPorMoneda',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'tipo_contratacion',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Fecha Inicio Contrato',
				tooltip: 'Fecha Inicio Contrato',
				dataIndex: 'fecha_inicio_contrato',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Fecha Final Contrato',
				tooltip: 'Fecha Final Contrato',
				dataIndex: 'fecha_fin_contrato',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Plazo original del contrato',
				tooltip: 'Plazo original del contrato',
				dataIndex: 'plazoContrato',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'leyenda dinamica',
				tooltip: 'leyenda dinamica',
				dataIndex: 'clasificacionEpo',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},			
			{
				header: 'campo 1 ',
				tooltip: 'campo 1',
				dataIndex: 'campo_adicional_1',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'campo 2 ',
				tooltip: 'campo 2',
				dataIndex: 'campo_adicional_2',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'campo 3 ',
				tooltip: 'campo 3',
				dataIndex: 'campo_adicional_3',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'campo 4 ',
				tooltip: 'campo 4',
				dataIndex: 'campo_adicional_4',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'campo 5 ',
				tooltip: 'campo 5',
				dataIndex: 'campo_adicional_5',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Ventanilla de Pago',
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'venanillaPago',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra',
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'supAdmResob',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex: 'numeroTelefono',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Objeto del Contrato',
				tooltip: 'Objeto del Contrato',
				dataIndex: 'objeto_contrato',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'estatus_solicitud',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			}
			],			
			stripeRows: true,
			loadMask: true,
			height: 200,
			width: 943,				
			frame: true
		});
			

	var cBoton = new Ext.Container({
		layout: 'table',
		layoutConfig: {
			columns: 3
		},
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
			{
				xtype: 'button',
				text		: 'Guardar Solicitud',
				tooltip	: 'Guardar Solicitud',
				id			: 'Guardar',
				handler: Guardar 
			},
			{
				xtype: 'button',
				text		: 'Cancelar',
				tooltip	: 'Cancelar',
				id			: 'Cancelar',
				handler: 	function(){				
					document.location.href  = "34Solic_Consentimientoext.jsp";							
				}				
			}
		]
	});
	

	var Nota = new Ext.Panel({
	id: 'Nota',
	width: 943,
	height: '600',
	hidden: true,
	align: 'center'	
});
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		items: [			
			grid,
			NE.util.getEspaciador(20),
			Nota, 
			NE.util.getEspaciador(20),
			cBoton,
			NE.util.getEspaciador(20)
		]
		});
		
		
		
		
	consultaData.load();
	
	
});