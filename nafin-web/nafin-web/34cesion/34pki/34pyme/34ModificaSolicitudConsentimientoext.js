Ext.onReady(function() {
	var camposAdicionales;
	var cveGrupo;
	var numContratoOrig;
	
	function setLabel(field,label){
		var el = field.el.dom.parentNode.parentNode;
		if( el.children[0].tagName.toLowerCase() === 'label' ) {
			el.children[0].innerHTML =label;
		}else if( el.parentNode.children[0].tagName.toLowerCase() === 'label' ){
			el.parentNode.children[0].innerHTML =label;
		}
		return setLabel; //just for fun
	}
	
	function verificaFechas(fec,fec2){
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
	}	
	
	function procesaBuscaContrato(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.existe){
				var noContrato = Ext.getCmp('numero_contrato1');
				Ext.Msg.alert('Mensaje','El contrato capturado ya fue cedido',function(){
					noContrato.setValue(numContratoOrig);
					noContrato.fireEvent('focus');
				});
				return;
			}else{

				fp.el.mask('Buscando...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '34ModificaSolicitudConsentimientoext.data.jsp',
					params: {
						informacion:		'BuscaContratoCedido',
						numero_contrato:	opts.params.numero_contrato,
						clave_epo:			opts.params.clave_epo,
						evento:				opts.params.evento
					},
					callback: procesaBuscaContratoCedido
				});

			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesaBuscaContratoCedido(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.existe){
				var noContrato = Ext.getCmp('numero_contrato1');
				Ext.Msg.alert('Mensaje','El contrato capturado ya fue cedido',function(){
					noContrato.setValue(numContratoOrig);
					noContrato.fireEvent('focus');
				});
				return;
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGuardar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
		
		var datos = Ext.util.JSON.decode(response.responseText);
		var numeroProceso =datos.numeroSolicitud;		
		var parametros = "numeroProceso="+numeroProceso+"&accion=Modificar";
		var resultado; 
		var fp = Ext.getCmp('forma');		
		var forma = fp.getForm();
		//aqui se guarda el documento
		forma.submit({
				url: '34Solic_Consentimientoext.ma.jsp?'+parametros,
				waitMsg: 'Enviando datos...',
				success: function(form, action) {				
				Ext.MessageBox.alert('Cambios Guardados','La actualizaci�n ha sido realizada con �xito');
				document.location.href  = "34ConsultaSolicitudConsentimientoExt.jsp";				
			},
			failure: NE.util.mostrarSubmitError
			});			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var myValidFn = function(v) {
		var myRegex = /^.+\.([pP][dD][fF])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivopdf 		: myValidFn,
		archivopdfText 	: 'El formato de el Contrato no es v�lido. Formato(s) soportado(s): PDF.'
	});
				
		var Modificar = function() {		
		
			var clave_epo = Ext.getCmp("clave_epo1");
			if (Ext.isEmpty(clave_epo.getValue()) ) {
				clave_epo.markInvalid('El valor de la EPO es requerido.');	
				return;
			}
			var clave_if = Ext.getCmp("clave_if1");
			if (Ext.isEmpty(clave_if.getValue()) ) {
				clave_if.markInvalid('El valor de el Cesionario (IF) es requerido.');	
				return;
			}
			var clasiEpo = Ext.getCmp("clasiEpo1");
			if (Ext.isEmpty(clasiEpo.getValue()) ) {
				clasiEpo.markInvalid('El valor de la Ciudad de Firma del Contrato es requerido.');	
				return;
			}
			
			var numero_contrato = Ext.getCmp("numero_contrato1");
			if (Ext.isEmpty(numero_contrato.getValue()) ) {
				numero_contrato.markInvalid('El valor de el N�mero de Contrato es requerido.');	
				return;
			}
		var firmaContrato = Ext.getCmp('firma_contrato');
		if(firmaContrato.getValue()==''){
			firmaContrato.markInvalid('La fecha de firma del contrato es requerida.');	
			return;
		}	
		
			var claveMoneda = Ext.getCmp("claveMoneda1");
		if (Ext.isEmpty(claveMoneda.getValue()) ) {
			claveMoneda.markInvalid('El valor de la Moneda es requerido.');	
			return;
		}
		var montoN = Ext.getCmp("montoN1");
		var montoD1 = Ext.getCmp("montoD1");
		var montoE1 = Ext.getCmp("montoE1");
		var radioBoton = Ext.getCmp("csTipoMontoChk");
		var montoDefinido1 = 	Ext.getCmp('montoDefinido1');
		var montoContratoMin = 	Ext.getCmp('montoContratoMin1');
		var montoContratoMax = 	Ext.getCmp('montoContratoMax1');
		var contador =0; 		
		if ( radioBoton.isVisible()&& radioBoton.getValue().inputValue=='S') 	{ 
				radioBoton.markInvalid('El valor de el Tipo de Monto es requerido');	
				return;
			}
		if ( claveMoneda.getValue()==0 ) {	
			if(radioBoton.getValue().inputValue=='D'){
				if (Ext.isEmpty(montoN.getValue()) ) 	{ contador ++; }
				if (Ext.isEmpty(montoE1.getValue()) ) { contador ++; }
				if (Ext.isEmpty(montoD1.getValue()) ) { contador ++; 	}
				if(contador!=1){
					claveMoneda.markInvalid('Favor de ingresar el Monto de dos Monedas');	
					return;
				}
			}else if(radioBoton.getValue().inputValue=='M'){
				if(!verificaFechas('montoContratoMin1DL','montoContratoMax1DL')||!verificaFechas('montoContratoMin1EU','montoContratoMax1EU')||!verificaFechas('montoContratoMin1','montoContratoMax1')){
					return;
				}
				contador=0;
				if (Ext.isEmpty(Ext.getCmp('montoContratoMin1DL').getValue()) ) 	{ contador ++; }
				if (Ext.isEmpty(Ext.getCmp('montoContratoMin1EU').getValue()) ) { contador ++; }
				if (Ext.isEmpty(Ext.getCmp('montoContratoMax1').getValue()) ) { contador ++; 	}
				if(contador!=1){
					claveMoneda.markInvalid('Favor de ingresar el Monto de dos Monedas');	
					return;
				}
			}
		}else if (claveMoneda.getValue() !=0 ) {	
			
			if(radioBoton.getValue().inputValue=='D') {
				if (Ext.isEmpty(montoDefinido1.getValue()) ) 	{ 
					montoDefinido1.markInvalid('El valor de el Monto del Contrato es requerido');	
					return;
				}
			}else if(radioBoton.getValue().inputValue=='M') {
			
				if(claveMoneda.getValue()==25){
					 montoContratoMin = 	Ext.getCmp('montoContratoMin1EU');
					 montoContratoMax = 	Ext.getCmp('montoContratoMax1EU');
				}else if(claveMoneda.getValue()==54){
					 montoContratoMin = 	Ext.getCmp('montoContratoMin1DL');
					 montoContratoMax = 	Ext.getCmp('montoContratoMax1DL');
				}
				
				if ( Ext.isEmpty(montoContratoMin.getValue())  && Ext.isEmpty(montoContratoMax.getValue()) ) 	{ 
					montoContratoMin.markInvalid('El valor de el Monto M�nimo del Contrato es requerido.');	
					montoContratoMax.markInvalid('El valor de el Monto M�ximo del Contrato es requerido');	
					return;
				}else if (!Ext.isEmpty(montoContratoMin.getValue()) && Ext.isEmpty(montoContratoMax.getValue()) ) 	{
					montoContratoMax.markInvalid('El valor de el Monto M�ximo del Contrato es requerido');	
					return;
				}else if (Ext.isEmpty(montoContratoMin.getValue()) && !Ext.isEmpty(montoContratoMax.getValue()) ) 	{
					montoContratoMin.markInvalid('El valor de el Monto M�nimo del Contrato es requerido.');	
					return;
				}
			}
		}
		
			var claveTipoContratacion = Ext.getCmp("claveTipoContratacion1");
			if (Ext.isEmpty(claveTipoContratacion.getValue()) ) {
				claveTipoContratacion.markInvalid('El valor de el Tipo de Contrataci�n es requerido.');	
				return;
			}
			
			var plazoContrato = Ext.getCmp("plazoContrato1");
			var claveTipoPlazo = Ext.getCmp("claveTipoPlazo1");
			var fecha_vigencia_ini = Ext.getCmp("fecha_vigencia_ini1");
			var fecha_vigencia_fin = Ext.getCmp("fecha_vigencia_fin1");
			if (!Ext.isEmpty(plazoContrato.getValue()) && !Ext.isEmpty(claveTipoPlazo.getValue())
					&& !Ext.isEmpty(fecha_vigencia_ini.getValue()) && !Ext.isEmpty(fecha_vigencia_fin.getValue())
					) {
						fecha_vigencia_ini.markInvalid('S�lo puede ingresar el valor de las fechas inicial y final de Vigencia del Contrato o el Plazo del Contrato, no ambas.');
						fecha_vigencia_fin.markInvalid('S�lo puede ingresar el valor de las fechas inicial y final de Vigencia del Contrato o el Plazo del Contrato, no ambas.');
				
						return;
				}		
			
			if (  ( Ext.isEmpty(plazoContrato.getValue()) && Ext.isEmpty(claveTipoPlazo.getValue()) ) 
					&&  ( Ext.isEmpty(fecha_vigencia_ini.getValue()) &&  Ext.isEmpty(fecha_vigencia_fin.getValue()))
					) {
						plazoContrato.markInvalid('El valor de el Plazo del Contrato es requerido');
						claveTipoPlazo.markInvalid('El valor de el Tipo de Plazo del Contrato es requerido.');	
						fecha_vigencia_ini.markInvalid('El valor de la Fecha de Vigencia del Contrato inicial es requerido.');
						fecha_vigencia_fin.markInvalid('El valor de la Fecha de Vigencia del Contrato final es requerido');
						return;
				}										
			
				var claveVentanillaPago = Ext.getCmp("claveVentanillaPago1");
				if (Ext.isEmpty(claveVentanillaPago.getValue()) ) {
					claveVentanillaPago.markInvalid('El valor de la Ventanilla de Pago es requerido.');	
					return;
				}
				var supAdmResob = Ext.getCmp("supAdmResob1");
				if (Ext.isEmpty(supAdmResob.getValue()) ) {
					supAdmResob.markInvalid('El valor de el Supervisor/Administrador/Residente de Obra es requerido.');	
					return;
				}
				var telefono = Ext.getCmp("telefono1");
				if (Ext.isEmpty(telefono.getValue()) ) {
					telefono.markInvalid('El valor de el Tel�fono es requerido.');	
					return;
				}
				var objetoContrato = Ext.getCmp("objetoContrato1");
				if (Ext.isEmpty(objetoContrato.getValue()) ) {
					objetoContrato.markInvalid('El valor de el Objeto del Contrato es requerido.');	
					return;
				}
			
			var archivo = Ext.getCmp("archivo");
				if (Ext.isEmpty(archivo.getValue()) ) {
					archivo.markInvalid('El valor de la Ruta del Contrato es requerido.');	
					return;
				}
							
			fp.el.mask('Enviando...', 'x-mask-loading');	
						
			Ext.Ajax.request({
				url : '34ModificaSolicitudConsentimientoext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
				informacion:'Modificar',
				clave_solicitud:Ext.getDom('clave_solicitud').value
				}),
				callback: procesarSuccessFailureGuardar
			});			
	}
		// valores iniciales al Modificar la solicitud 
	function procesaValoresIniciales(opts, success, response) {
			
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
		
			if (jsonValoresIniciales != null){
				catalogoEPOData.load();	
				catalogoMonedaData.load();
				catalogoContratacionData.load();
				catalogoTipoPlazoData.load();

				numContratoOrig = jsonValoresIniciales.numero_contrato;
				Ext.getCmp("numero_contrato1").setValue(jsonValoresIniciales.numero_contrato);
				Ext.getCmp("numero_contrato2").setValue(jsonValoresIniciales.numero_contrato);
				Ext.getCmp("cbo_num_contrato1").setValue(jsonValoresIniciales.clave_grupo_cesion);
				var noEPO = jsonValoresIniciales.clave_epo;
				
				cveGrupo = jsonValoresIniciales.clave_grupo_cesion
				
				if(noEPO !='') {				
				catalogoClasificacionEPOData.load({
						params: {
							informacion: 'CatClasificacionEPO',							
							clave_epo:	noEPO					
						}
					});		
					
					catalogoIFData.load({
						params: {
							informacion: 'CatalogoIF',							
							clave_epo:noEPO					
						}
					});							
					catalogoVentanillaPagoData.load({
						params: {
							informacion: 'CatVentanillaPagoData',							
							clave_epo:noEPO					
						}
					});	
				}							
				Ext.getCmp("montoDefinido1").setValue(jsonValoresIniciales.montoDefinido);
				Ext.getCmp("plazoContrato1").setValue(jsonValoresIniciales.plazoContrato);							
				Ext.getCmp("comentarios1").setValue(jsonValoresIniciales.comentarios);
				Ext.getCmp("empresas1").setValue(jsonValoresIniciales.empresas);
				Ext.getCmp("objetoContrato1").setValue(jsonValoresIniciales.objetoContrato);
				Ext.getCmp("telefono1").setValue(jsonValoresIniciales.telefono);
				Ext.getCmp("fecha_vigencia_ini1").setValue(jsonValoresIniciales.fecha_vigencia_ini);
				Ext.getCmp("fecha_vigencia_fin1").setValue(jsonValoresIniciales.fecha_vigencia_fin);					
				Ext.getCmp("fecha_vigencia_fin1").setValue(jsonValoresIniciales.fecha_vigencia_fin);	
				Ext.getCmp("supAdmResob1").setValue(jsonValoresIniciales.supAdmResob);	
				var claveMoneda =  jsonValoresIniciales.claveMoneda;				
				Ext.getCmp("montoContratoMin1").setValue(jsonValoresIniciales.montoContratoMin);
				Ext.getCmp("montoContratoMax1").setValue(jsonValoresIniciales.montoContratoMax);
				Ext.getCmp('claveMoneda1').setValue(claveMoneda);
				Ext.getCmp('firma_contrato').setValue(jsonValoresIniciales.firma_contrato);
				
				/*if(jsonValoresIniciales.montoDefinido !='') {
						Ext.getCmp("csTipoMontoChk").setValue("D");
				}
				if(jsonValoresIniciales.montoContratoMin !=''  && jsonValoresIniciales.montoContratoMax !='' ) {
						Ext.getCmp("csTipoMontoChk").setValue("M");
				}*/
				
				Ext.getCmp("csTipoMontoChk").setValue(jsonValoresIniciales.csTipoMontoChk);
			
				if(claveMoneda !=0){
					Ext.getCmp('csTipoMontoChk').show();
					Ext.getCmp('MonedaN').hide();
					Ext.getCmp('MonedaE').hide();	
					Ext.getCmp('MonedaD').hide();						
				}else if(claveMoneda==0){
					Ext.getCmp('csTipoMontoChk').show();					
					Ext.getCmp('MonedaN').show();	
					Ext.getCmp('MonedaE').show();	
					Ext.getCmp('MonedaD').show();	
					Ext.getCmp('montoDefinido1').hide();
					
					Ext.getCmp("montoN1").setValue(jsonValoresIniciales.monto0);
					Ext.getCmp("montoD1").setValue(jsonValoresIniciales.monto1);
					Ext.getCmp("montoE1").setValue(jsonValoresIniciales.monto2);
					
					Ext.getCmp("montoContratoMin1").setValue(jsonValoresIniciales.montoMin0);
					Ext.getCmp("montoContratoMax1").setValue(jsonValoresIniciales.montoMax0);
					
					Ext.getCmp("montoContratoMin1DL").setValue(jsonValoresIniciales.montoMin1);
					Ext.getCmp("montoContratoMax1DL").setValue(jsonValoresIniciales.montoMax1);
					
					Ext.getCmp("montoContratoMin1EU").setValue(jsonValoresIniciales.montoMin2);
					Ext.getCmp("montoContratoMax1EU").setValue(jsonValoresIniciales.montoMax2);
				}
				
				if(jsonValoresIniciales.csTipoMontoChk=='D'){
					Ext.getCmp('montoDefinido1').show();
					Ext.getCmp('MonedaN').hide();	
					Ext.getCmp('MonedaE').hide();	
					Ext.getCmp('MonedaD').hide();
				}else if(jsonValoresIniciales.csTipoMontoChk=='M'){
					Ext.getCmp('montos').show();
					
				}else if(jsonValoresIniciales.csTipoMontoChk=='A'){			
					//Ext.getCmp('montoDefinido1').hide();
				}
				var valor= jsonValoresIniciales.csTipoMontoChk;
				//	alert(valor+' '+claveMoneda);
				if(valor=='D'&&claveMoneda==0)
					{
						Ext.getCmp('montoDefinido1').hide();
						Ext.getCmp('MonedaN').show();	
						Ext.getCmp('MonedaE').show();	
						Ext.getCmp('MonedaD').show();
						Ext.getCmp('montos').hide();
						Ext.getCmp('montosD').hide();
						Ext.getCmp('montosE').hide();
						
					}else if(valor=='M'&&claveMoneda==0) {
						Ext.getCmp('montoDefinido1').hide();
						Ext.getCmp('MonedaN').hide();	
						Ext.getCmp('MonedaE').hide();	
						Ext.getCmp('MonedaD').hide();
						Ext.getCmp('montos').show();
						Ext.getCmp('montosD').show();
						Ext.getCmp('montosE').show();
					}
					else	if(valor=='D') {
						
						Ext.getCmp('montoDefinido1').show();
						Ext.getCmp('montos').hide();
						Ext.getCmp('montosD').hide();
						Ext.getCmp('montosE').hide();
						
					}else if(valor=='M') {
						Ext.getCmp('montoDefinido1').hide();
						Ext.getCmp('montos').hide();
						Ext.getCmp('montosD').hide();
						Ext.getCmp('montosE').hide();
						
						if(claveMoneda==1){
							Ext.getCmp('montos').show();	
						}else if(claveMoneda==25){
							Ext.getCmp('montosE').show();
						}else if(claveMoneda==54){
							Ext.getCmp('montosD').show();
						}
					}
							
				var  numeroCampos = jsonValoresIniciales.numeroCampos;	
				for(var i= 1; i<=numeroCampos; i++) {			
					if(i==1){		
					var camposAdicionales1 =  Ext.decode(jsonValoresIniciales.camposAdicionales1);						
						var indice1 = fp.items.indexOfKey('comentarios1')+1;
						fp.insert(indice1,camposAdicionales1);
						fp.doLayout();
						Ext.getCmp("CampoAdicional_1").setValue(jsonValoresIniciales.CampoAdicional_1);
					}
					
					if(i==2){	
						var camposAdicionales2 =  Ext.decode(jsonValoresIniciales.camposAdicionales2);
						var indice2 = fp.items.indexOfKey('CampoAdicional_1')+1;
						fp.insert(indice2,camposAdicionales2);
						fp.doLayout();	
						Ext.getCmp("CampoAdicional_2").setValue(jsonValoresIniciales.CampoAdicional_2);
					}
					if(i==3){					
						var camposAdicionales3 =  Ext.decode(jsonValoresIniciales.camposAdicionales3);
						var indice3 = fp.items.indexOfKey('CampoAdicional_2')+1;
						fp.insert(indice3,camposAdicionales3);
						fp.doLayout();
						Ext.getCmp("CampoAdicional_3").setValue(jsonValoresIniciales.CampoAdicional_3);
					}
					if(i==4){		
						var camposAdicionales4 =  Ext.decode(jsonValoresIniciales.camposAdicionales4);
						var indice4 = fp.items.indexOfKey('CampoAdicional_3')+1;
						fp.insert(indice4,camposAdicionales4);
						fp.doLayout();	
						Ext.getCmp("CampoAdicional_4").setValue(jsonValoresIniciales.CampoAdicional_4);
					}
						if(i==5){	
						var camposAdicionales5 =  Ext.decode(jsonValoresIniciales.camposAdicionales5);
						var indice5 = fp.items.indexOfKey('CampoAdicional_4')+1;
						fp.insert(indice5,camposAdicionales5);
						fp.doLayout();	
						Ext.getCmp("CampoAdicional_5").setValue(jsonValoresIniciales.CampoAdicional_5);
					}
				}
				
				if(cveGrupo==''){
					Ext.getCmp('numero_contrato1').show();
					Ext.getCmp('cboNumContrato1').hide();
					Ext.getCmp('clave_epo1').enable();
					Ext.getCmp('cboNumContrato1').enable();
					Ext.getCmp('empresas1').enable();
					setLabel(Ext.getCmp('empresas1'), 'Empresas Representadas<br/>Separar cada empresa por (;)');
					
				}else{
					Ext.getCmp('numero_contrato1').hide();
					Ext.getCmp('cboNumContrato1').show();
					Ext.getCmp('clave_epo1').disable();
					Ext.getCmp('cboNumContrato1').disable();
					Ext.getCmp('empresas1').disable();
					setLabel(Ext.getCmp('empresas1'), 'Grupo de Empresas:');
					Ext.getCmp('notaEmpresas').setValue('Nota: Favor de Verificar si las Empresas que conforman el grupo son las correctas. En caso de no ser las indicadas favor de comunicarse al 01-800-CADENAS (22-23-36-27).');
				}
			
			}	
					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ModificaSolicitudConsentimientoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function (store, arrRegistros, opciones) {
				if(arrRegistros !='') {
					if(Ext.isEmpty(arrRegistros[0].get('loadMsg'))) {
						Ext.getCmp("clave_epo1").setValue(jsonValoresIniciales.clave_epo);
					}
				}
			}
		}		
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ModificaSolicitudConsentimientoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function (store, arrRegistros, opciones) {
				if(arrRegistros !='') {
					if(Ext.isEmpty(arrRegistros[0].get('loadMsg'))) {
						Ext.getCmp("clave_if1").setValue(jsonValoresIniciales.clave_if);
					}
				}
			}
		}		
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ModificaSolicitudConsentimientoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function (store, arrRegistros, opciones) {
				if(arrRegistros !='') {
					if(Ext.isEmpty(arrRegistros[0].get('loadMsg'))) {
						Ext.getCmp("claveMoneda1").setValue(jsonValoresIniciales.claveMoneda);
					}
				}
			}
		}		
	});
	
		var catalogoContratacionData = new Ext.data.JsonStore({
		id: 'catalogoContratacionDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ModificaSolicitudConsentimientoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoContratacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function (store, arrRegistros, opciones) {
				if(arrRegistros !='') {
					if(Ext.isEmpty(arrRegistros[0].get('loadMsg'))) {
						Ext.getCmp("claveTipoContratacion1").setValue(jsonValoresIniciales.claveTipoContratacion);
					}
				}
			}
		}		
	});
	
	var catalogoTipoPlazoData = new Ext.data.JsonStore({
		id: 'catalogoTipoPlazonDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ModificaSolicitudConsentimientoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function (store, arrRegistros, opciones) {
				if(arrRegistros !='') {
					if(Ext.isEmpty(arrRegistros[0].get('loadMsg'))) {
						Ext.getCmp("claveTipoPlazo1").setValue(jsonValoresIniciales.claveTipoPlazo);
					}
				}
			}
		}		
	});
		
	var catalogoClasificacionEPOData = new Ext.data.JsonStore({
		id: 'catalogoClasificacionEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ModificaSolicitudConsentimientoext.data.jsp',
		baseParams: {
			informacion: 'CatClasificacionEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function (store, arrRegistros, opciones) {
				if(arrRegistros !='') {
					if(Ext.isEmpty(arrRegistros[0].get('loadMsg'))) {
						Ext.getCmp("clasiEpo1").setValue(jsonValoresIniciales.clasiEpo);
					}
				}
			}		
		}		
	});
		
		var catalogoVentanillaPagoData = new Ext.data.JsonStore({
		id: 'catalogoVentanillaPagoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ModificaSolicitudConsentimientoext.data.jsp',
		baseParams: {
			informacion: 'CatVentanillaPagoData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: function (store, arrRegistros, opciones) {
				if(arrRegistros !='') {
					if(Ext.isEmpty(arrRegistros[0].get('loadMsg'))) {
						Ext.getCmp("claveVentanillaPago1").setValue(jsonValoresIniciales.claveVentanillaPago);
					}
				}
			}		
		}		
	});			

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'clave_epo',
			id: 'clave_epo1',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_epo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						//para el combo de IF	
						var cmbIF = Ext.getCmp('clave_if1');
						cmbIF.setValue('');
						cmbIF.setDisabled(false);
						cmbIF.store.load({
							params: {
								clave_epo:combo.getValue()
							}
						});
						
						//para el combo de Ventanilla						
						var cmbVentanilla = Ext.getCmp('claveVentanillaPago1');
						cmbVentanilla.setValue('');
						cmbVentanilla.setDisabled(false);						
						cmbVentanilla.store.load({
							params: {
								clave_epo:combo.getValue()
							}
						});						
						//para el combo Ciudad de Firma del Contrato					
						var cmbclasiEpo = Ext.getCmp('clasiEpo1');
						cmbclasiEpo.setValue('');
						cmbclasiEpo.setDisabled(false);						
						cmbclasiEpo.store.load({
							params: {
								clave_epo:combo.getValue()
							}
						});						
						ComponentesCamposAdicionales(combo.getValue());
					}
				}
			}			
		},
		{
			xtype: 'textfield',
			name: 'numero_contrato',
			id: 'numero_contrato1',
			fieldLabel: 'N�mero de Contrato',
			allowBlank: true,
			hidden: false,
			maxLength: 25,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0',
			listeners:{
				blur:{
					fn:function(field){
							var cboEpo = Ext.getCmp('clave_epo1').getValue();

							if(	!Ext.isEmpty(cboEpo) && !Ext.isEmpty(field.getValue())	){
								if(numContratoOrig != field.getValue()){
									fp.el.mask('Buscando...', 'x-mask-loading');
									Ext.Ajax.request({
										url: '34ModificaSolicitudConsentimientoext.data.jsp',
										params: {
											informacion:		'BuscaContrato',
											numero_contrato:	field.getValue(),
											clave_epo:			cboEpo,
											evento:				'blur'
										},
										callback: procesaBuscaContrato
									});
								}

							}
						}
				}
			}
		},
		{
			xtype: 'compositefield',
			id: 'cboNumContrato1',
			fieldLabel: 'N�mero de Contrato',
			combineErrors: false,
			msgTarget: 'side',
			hidden: true,
			items: [
				{
					xtype: 'textfield',
					name: 'numero_contrato2',
					id: 'numero_contrato2',
					fieldLabel: 'N�mero de Contrato',
					allowBlank: true,
					hidden: false,
					maxLength: 25,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'hidden',
					name: 'cbo_num_contrato',
					id: 'cbo_num_contrato1',
					fieldLabel: 'N�mero de Contrato',
					allowBlank: true,
					hidden: false,
					maxLength: 25,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'button',
					text: 'Ver Empresas que Conforman el Grupo',
					iconCls: 'icoLupa',
					handler: function(){
						var id_grupo = Ext.getCmp('cbo_num_contrato1').getValue();  
						var win = new NE.AutoConsorcio.VisorConsorcio('', 'Empresas','Empresas que Forman el Grupo','N', id_grupo );
					}
				}
			]
		},
		{
			xtype: 'displayfield',
			id: 'notaEmpresas',
			value: 'Nota: Este campo ser� llenado cada vez que exista m�s de una empresa representada, incluyendo solo las empresas adicionales a la que est� firmando la solicitud.',
			//value: 'Nota: Favor de Verificar si las Empresas que conforman el grupo son las correctas. En caso de no ser las indicadas favor de comunicarse al 01-800-CADENAS (22-23-36-27).',
			width: 35
		},
		{
			xtype: 'textarea',
			name: 'empresas',
			id: 'empresas1',
			fieldLabel: 'Empresas Representadas<br/>Separar cada empresa por (;)',
			allowBlank: true,
			maxLength: 400,	//ver el tama�o maximo del numero en BD para colocar este igual CORRECCION(250) FODEA-024-2014
			hidden: false,
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'combo',
			name: 'clave_if',
			id: 'clave_if1',
			fieldLabel: 'Intermdiario Financiero(Cesionario) (IF)',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_if',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoIFData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'clasiEpo',
			id: 'clasiEpo1',
			fieldLabel: 'Ciudad de Firma del Contrato',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clasiEpo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoClasificacionEPOData,
			tpl : NE.util.templateMensajeCargaCombo
		},	
		{
			xtype: 'datefield',
			name: 'firma_contrato',
			id: 'firma_contrato',
			allowBlank: true,
			startDay: 0,
			fieldLabel: 'Firma del Contrato',
			anchor: '40%',
			allowBlank: true,
			startDay: 0,
			minValue: '01/01/1901',
			width: 100,
			msgTarget: 'side', 
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'combo',
			name: 'claveMoneda',
			id: 'claveMoneda1',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveMoneda',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
					var valor = combo.getValue();				
							Ext.getCmp('csTipoMontoChk').show();
							Ext.getCmp('csTipoMontoChk').setValue('S');
							Ext.getCmp('MonedaN').hide();
							Ext.getCmp('MonedaE').hide();	
							Ext.getCmp('MonedaD').hide();	
							Ext.getCmp('montos').hide();
							Ext.getCmp('montoDefinido1').hide();
							Ext.getCmp('montos').hide();
							Ext.getCmp('montosD').hide();
							Ext.getCmp('montosE').hide();
							
							Ext.getCmp('montos').reset();
							Ext.getCmp('montosD').reset();
							Ext.getCmp('montosE').reset();
						
					}
				}
			}			
		},	
		{  
			xtype:  'radiogroup',   
			fieldLabel: "Tipo de Monto",    
			name: 'csTipoMontoChk',   
			id: 'csTipoMontoChk',  
			value: 'A',  
			
			hidden: true,   
			columns:  2,			
			items:         
				[         
					{ 
						boxLabel:    "Monto Definido",             
						name:        'csTipoMontoChk',   
						//checked: true,            
						inputValue:  "D" ,
						width: 20
					},         
					{           
						boxLabel: "Monto Min y Max",             
						name: 'csTipoMontoChk',             
						inputValue:  "M",  
						width: 20	
					},
					{           
						
						hidden: true,//Objeto para limpiar el radio
						inputValue:  "S", 
						checked: true,
						name: 'csTipoMontoChk',   
						width: 20	
					}
				],   
				style: {      
						paddingLeft: '10px'   
				},
				listeners : {
					change : {
							fn: function(radio) {							
								var valor = radio.getValue().inputValue;
								var claveMoneda1 = Ext.getCmp('claveMoneda1').getValue();
								if(valor=='D'&&claveMoneda1==0)
								{
									Ext.getCmp('MonedaN').show();	
									Ext.getCmp('MonedaE').show();	
									Ext.getCmp('MonedaD').show();
									Ext.getCmp('montos').hide();
									Ext.getCmp('montosD').hide();
									Ext.getCmp('montosE').hide();
									
								}else if(valor=='M'&&claveMoneda1==0) {
									Ext.getCmp('MonedaN').hide();	
									Ext.getCmp('MonedaE').hide();	
									Ext.getCmp('MonedaD').hide();
									Ext.getCmp('montos').show();
									Ext.getCmp('montosD').show();
									Ext.getCmp('montosE').show();
								}
								else	if(valor=='D') {
									Ext.getCmp('montoDefinido1').show();
									Ext.getCmp('montos').hide();
									Ext.getCmp('montosD').hide();
									Ext.getCmp('montosE').hide();
									
								}else if(valor=='M') {
									Ext.getCmp('montoDefinido1').hide();
									Ext.getCmp('montos').hide();
									Ext.getCmp('montosD').hide();
									Ext.getCmp('montosE').hide();
									
									if(claveMoneda1==1){
										Ext.getCmp('montos').show();	
									}else if(claveMoneda1==25){
										Ext.getCmp('montosE').show();
									}else if(claveMoneda1==54){
										Ext.getCmp('montosD').show();
									}
							}
						}
					}
				}	
		}, 
	
		{
			xtype: 'numberfield',
			name: 'montoDefinido',
			id: 'montoDefinido1',
			fieldLabel: 'Monto original del contrato',
			//fieldLabel: 'Monto',
			allowBlank: true,
			hidden: true,
			maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'compositefield',
			id: 'montos',
			fieldLabel: 'Moneda Nacional',
			combineErrors: false,
			msgTarget: 'side',
			hidden: true,
			items: [
				{
					xtype: 'displayfield',
					value: 'MIN',
					width: 80
				},						
				{
					xtype: 'numberfield',					
					name: 'montoContratoMin',
					id: 'montoContratoMin1',
					maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
					allowBlank: true,
					width: 100,
					msgTarget: 'side'	,					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'MAX',
					width: 80
				},						
				{
					xtype: 'numberfield',					
					name: 'montoContratoMax',
					id: 'montoContratoMax1',
					maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
					allowBlank: true,
					width: 100,
					msgTarget: 'side'	,					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}	,
		{
			xtype: 'compositefield',
			id: 'montosD',
			fieldLabel: 'Dolar Americano',
			combineErrors: false,
			msgTarget: 'side',
			hidden: true,
			items: [
				{
					xtype: 'displayfield',
					value: 'MIN',
					width: 80
				},						
				{
					xtype: 'numberfield',					
					name: 'montoContratoMinDL',
					id: 'montoContratoMin1DL',
					maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
					allowBlank: true,
					width: 100,
					msgTarget: 'side'	,					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'MAX',
					width: 80
				},						
				{
					xtype: 'numberfield',					
					name: 'montoContratoMaxDL',
					id: 'montoContratoMax1DL',
					maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
					allowBlank: true,
					width: 100,
					msgTarget: 'side'	,					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}	,
		{
			xtype: 'compositefield',
			id: 'montosE',
			fieldLabel: 'Moneda Euro',
			combineErrors: false,
			msgTarget: 'side',
			hidden: true,
			items: [
				{
					xtype: 'displayfield',
					value: 'MIN',
					width: 80
				},						
				{
					xtype: 'numberfield',					
					name: 'montoContratoMinEU',
					id: 'montoContratoMin1EU',
					maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
					allowBlank: true,
					width: 100,
					msgTarget: 'side'	,					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'MAX',
					width: 80
				},						
				{
					xtype: 'numberfield',					
					name: 'montoContratoMaxEU',
					id: 'montoContratoMax1EU',
					maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
					allowBlank: true,
					width: 100,
					msgTarget: 'side'	,					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}	,
		
		{
			xtype: 'compositefield',
			id: 'MonedaN',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			hidden: true,
			items: [
				{
					xtype: 'displayfield',
					value: 'MONEDA NACIONAL',
					width: 200
				},						
				{
					xtype: 'numberfield',					
					name: 'montoN',
					id: 'montoN1',
					allowBlank: true,
					maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
					startDay: 0,
					width: 100,
					msgTarget: 'side'	,					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}				
			]
		}	,	
		{
			xtype: 'compositefield',
			id: 'MonedaD',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			hidden: true,
			items: [
				{
					xtype: 'displayfield',
					value: 'DOLAR AMERICANO',
					width: 200
				},						
				{
					xtype: 'numberfield',					
					name: 'montoD',
					id: 'montoD1',
					maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side'	,					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}				
			]
		},	
		{
			xtype: 'compositefield',
			id: 'MonedaE',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			hidden: true,
			items: [
				{
					xtype: 'displayfield',
					value: 'MONEDA EURO',
					width: 200
				},						
				{
					xtype: 'numberfield',					
					name: 'montoE',
					id: 'montoE1',
					maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side'	,					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}				
			]
		}	,
		{
			xtype: 'combo',
			name: 'claveTipoContratacion',
			id: 'claveTipoContratacion1',
			fieldLabel: 'Tipo de Contrataci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveTipoContratacion',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoContratacionData,
			tpl : NE.util.templateMensajeCargaCombo
		},			
		{
			xtype: 'compositefield',
			//fieldLabel: 'Plazo del Contrato',
			fieldLabel: 'Plazo original del contrato',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'plazoContrato',
					id: 'plazoContrato1',
					allowBlank: true,
					maxLength: 4,	//ver el tama�o maximo del numero en BD para colocar este igual
					startDay: 0,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},			
				{
					xtype: 'combo',
					name: 'claveTipoPlazo',
					id: 'claveTipoPlazo1',
					fieldLabel: '',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'claveTipoPlazo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoTipoPlazoData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vigencia de Contrato',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_vigencia_ini',
					id: 'fecha_vigencia_ini1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_vigencia_fin1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_vigencia_fin',
					id: 'fecha_vigencia_fin1',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_vigencia_ini1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'combo',
			name: 'claveVentanillaPago',
			id: 'claveVentanillaPago1',
			fieldLabel: 'Ventanilla de Pago',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'claveVentanillaPago',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoVentanillaPagoData,
			tpl : NE.util.templateMensajeCargaCombo
		},	
		{
			xtype: 'textfield',
			name: 'supAdmResob',
			id: 'supAdmResob1',
			fieldLabel: 'Supervisor/Administrador/ Residente de Obra',
			allowBlank: true,
			hidden: false,
			maxLength: 250,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 250,			
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'numberfield',
			name: 'telefono',
			id: 'telefono1',
			fieldLabel: 'Tel�fono',
			allowBlank: true,
			hidden: false,
			maxLength: 20,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textarea',
			name: 'objetoContrato',
			id: 'objetoContrato1',
			fieldLabel: 'Objeto del Contrato',
			allowBlank: true,
			maxLength: 4500,	//ver el tama�o maximo del numero en BD para colocar este igual
			hidden: false,		
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textarea',
			name: 'comentarios',
			id: 'comentarios1',
			fieldLabel: 'Comentarios',
			allowBlank: true,
			maxLength: 150,	//ver el tama�o maximo del numero en BD para colocar este igual
			hidden: false,			
			width: 150,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},		
		{
		  xtype: 'fileuploadfield',
      id: 'archivo',
	  width: 150,	  
      emptyText: 'Ruta del Contrato',
      fieldLabel: 'Archivo Digital del Contrato',
      name: 'archivoCesion',
      buttonText: '',
      buttonCfg: {
			  iconCls: 'upload-icon'
      },
	  anchor: '90%',
	  vtype: 'archivopdf'
    }	
		
	];	

		
		var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 800,
		title: 'Modificaci�n Solicitud de Consentimiento',
		frame:true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Guardar',
				iconCls: 'icoGuardar',
				formBind: true,
				handler: 	Modificar		
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34ModificaSolicitudConsentimientoext.jsp';
				}
			}
		]			
	});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		items: [			
			fp,
			NE.util.getEspaciador(20)		
		]
		});
		

	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '34ModificaSolicitudConsentimientoext.data.jsp',
		params: {
			informacion: "valoresIniciales",
			clave_solicitud:Ext.getDom('clave_solicitud').value
		},
		callback: procesaValoresIniciales
	});
	
	
	});