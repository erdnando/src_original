<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.text.*, 
		java.io.*,		
		com.netro.exception.NafinException,
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray,
		java.io.File,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>

<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String clave_if= (request.getParameter("clave_if")!=null)?request.getParameter("clave_if"):"";
String clasiEpo = (request.getParameter("clasiEpo")!=null)?request.getParameter("clasiEpo"):"";
String  numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String  grupoCDer = (request.getParameter("cbo_num_contrato")!=null)?request.getParameter("cbo_num_contrato"):"";
String  claveMoneda = (request.getParameter("claveMoneda")!=null)?request.getParameter("claveMoneda"):"";
String  csTipoMontoChk= (request.getParameter("csTipoMontoChk")!=null)?request.getParameter("csTipoMontoChk"):"";
String  MonedaN= (request.getParameter("MonedaN")!=null)?request.getParameter("MonedaN"):"";
String  montoN= (request.getParameter("montoN")!=null)?request.getParameter("montoN"):"";
String  MonedaD= (request.getParameter("MonedaD")!=null)?request.getParameter("MonedaD"):"";
String  montoD= (request.getParameter("montoD")!=null)?request.getParameter("montoD"):"";
String  MonedaE= (request.getParameter("MonedaE")!=null)?request.getParameter("MonedaE"):"";
String  montoE= (request.getParameter("montoE")!=null)?request.getParameter("montoE"):"";
String  claveTipoContratacion= (request.getParameter("claveTipoContratacion")!=null)?request.getParameter("claveTipoContratacion"):"";
String  plazoContrato= (request.getParameter("plazoContrato")!=null)?request.getParameter("plazoContrato"):"";
String  claveTipoPlazo= (request.getParameter("claveTipoPlazo")!=null)?request.getParameter("claveTipoPlazo"):"";
String  fecha_vigencia_ini= (request.getParameter("fecha_vigencia_ini")!=null)?request.getParameter("fecha_vigencia_ini"):"";
String  fecha_vigencia_fin= (request.getParameter("fecha_vigencia_fin")!=null)?request.getParameter("fecha_vigencia_fin"):"";
String  claveVentanillaPago= (request.getParameter("claveVentanillaPago")!=null)?request.getParameter("claveVentanillaPago"):"";
String  supAdmResob= (request.getParameter("supAdmResob")!=null)?request.getParameter("supAdmResob"):"";
String  telefono= (request.getParameter("telefono")!=null)?request.getParameter("telefono"):"";
String  objetoContrato= (request.getParameter("objetoContrato")!=null)?request.getParameter("objetoContrato"):"";
String  comentarios= (request.getParameter("comentarios")!=null)?request.getParameter("comentarios"):"";
String  CampoAdicional_1= (request.getParameter("CampoAdicional_1")!=null)?request.getParameter("CampoAdicional_1"):"";
String  CampoAdicional_2= (request.getParameter("CampoAdicional_2")!=null)?request.getParameter("CampoAdicional_2"):"";
String  CampoAdicional_3= (request.getParameter("CampoAdicional_3")!=null)?request.getParameter("CampoAdicional_3"):"";
String  CampoAdicional_4= (request.getParameter("CampoAdicional_4")!=null)?request.getParameter("CampoAdicional_4"):"";
String  CampoAdicional_5= (request.getParameter("CampoAdicional_5")!=null)?request.getParameter("CampoAdicional_5"):"";
String  montoContratoMin= (request.getParameter("montoContratoMin")!=null)?request.getParameter("montoContratoMin"):"";
String  montoContratoMax= (request.getParameter("montoContratoMax")!=null)?request.getParameter("montoContratoMax"):"";
String  montoDefinido= (request.getParameter("montoDefinido")!=null)?request.getParameter("montoDefinido"):"";
String  numeroProceso = (request.getParameter("numeroProceso")!=null)?request.getParameter("numeroProceso"):"";
String  numeroSolicitud = (request.getParameter("numeroSolicitud")!=null)?request.getParameter("numeroSolicitud"):"";
//Nuevos Cambios Cesion Derechos Multimoneda.
String  montoContratoMinDL= (request.getParameter("montoContratoMinDL")!=null)?request.getParameter("montoContratoMinDL"):"";
String  montoContratoMaxDL= (request.getParameter("montoContratoMaxDL")!=null)?request.getParameter("montoContratoMaxDL"):"";
String  montoContratoMinEU= (request.getParameter("montoContratoMinEU")!=null)?request.getParameter("montoContratoMinEU"):"";
String  montoContratoMaxEU= (request.getParameter("montoContratoMaxEU")!=null)?request.getParameter("montoContratoMaxEU"):"";
//Fin Multimoneda
String  firmaContrato= (request.getParameter("firma_contrato")!=null)?request.getParameter("firma_contrato"):"";

//Fodea XXX-2014 empresas Representadas
String empresas= (request.getParameter("empresas")!=null)?request.getParameter("empresas"):"";
empresas = (request.getParameter("empresas2")!=null  && !request.getParameter("empresas2").equals(""))?request.getParameter("empresas2"):empresas;



String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");
String clavePyme = (String)request.getSession().getAttribute("iNoCliente");

String infoRegresar = "";
String clasificacionEpo ="";	
int numeroCampos =0;
HashMap camposAdicionalesParametrizados =null;

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);


if(!clave_epo.equals("")  ){
	//obtengo los campos adicionales 
	 camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
	 numeroCampos = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));
	//obtengo la descripcion de la clasificacion EPO
	 clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);	
}

	
if(informacion.equals("CatalogoEPO")){
	String rgpoTipoFirma = request.getParameter("rgpoTipoFirma")==null?"":request.getParameter("rgpoTipoFirma");
	
	if("I".equals(rgpoTipoFirma)){
		CatalogoEPO cat = new CatalogoEPO();
		cat.setClave("ic_epo");
		cat.setDescripcion("cg_razon_social");
		cat.setCs_cesion_derechos("S");
		infoRegresar = cat.getJSONElementos();
	}else if("G".equals(rgpoTipoFirma)){
		CatalogoEpoGpoCDer cat = new CatalogoEpoGpoCDer();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setCsPymePrincipal("S");
		cat.setCvePyme(clavePyme);
		cat.setCampoDescripcion("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	}

}else if(informacion.equals("CatalogoContrato")){
	
	CatalogoContratoGpoCDer cat = new CatalogoContratoGpoCDer();
	cat.setCampoClave("ic_grupo_cesion");
	cat.setCampoDescripcion("cg_no_contrato");
	cat.setCsPymePrincipal("S");
	cat.setCvePyme(clavePyme);
	cat.setCveEpo(clave_epo); 
	cat.setCampoDescripcion("cg_no_contrato");
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("CatalogoIF") && !clave_epo.equals("")){

	CatalogoIF cat = new CatalogoIF();
	cat.setClave("ic_if");
	cat.setDescripcion("cg_razon_social");
	cat.setG_cs_cesion_derechos("S");
	cat.setIc_epo(clave_epo); 
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoMoneda") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setOrden("ic_moneda");
	cat.setValoresCondicionIn("0,1,54,25", Integer.class);
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoContratacion") ){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_contratacion");
	cat.setCampoDescripcion("cg_nombre");
	cat.setTabla("cdercat_tipo_contratacion");
	cat.setOrden("cg_nombre");
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoTipoPlazo") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("cdercat_tipo_plazo");
	cat.setCampoClave("ic_tipo_plazo");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("ic_tipo_plazo");
	infoRegresar = cat.getJSONElementos();	
		

}else if(informacion.equals("CatClasificacionEPO") && !clave_epo.equals("") ){
	
	CatalogoClasificacionEPO cat = new CatalogoClasificacionEPO();
	cat.setClave("ic_clasificacion_epo");
	cat.setDescripcion("cg_area");
	cat.setSeleccion(clave_epo);
	
	infoRegresar = cat.getJSONElementos();	
	
}else  if(informacion.equals("CatVentanillaPagoData")  && !clave_epo.equals("") ){
	
	CatalogoVentanillaPago cat = new CatalogoVentanillaPago();
	cat.setCampoClave("ic_ventanilla_pago");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setClaveEpo(clave_epo);	
	infoRegresar = cat.getJSONElementos();		

}else  if(informacion.equals("verificaParamGrupoCesion") ){
	
	boolean csActivaParamGrupo = cesionBean.getEmpresaPrincipalGrupoCDer(clavePyme);
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("csActivaParamGrupo", new Boolean(csActivaParamGrupo));
	infoRegresar = jsonObj.toString();

}else  if(informacion.equals("obtieneEmpresasXgrupo") ){

	String cveGrupo = request.getParameter("clave_grupo")==null?"":request.getParameter("clave_grupo");
	List lstEmpresasXGrupo = cesionBean.getEmpresasXGrupoCder(cveGrupo);
	String empresasXGrupo = "";
	
	for(int x=0; x<lstEmpresasXGrupo.size(); x++){
		if(!((String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme")).equals(clavePyme))
			empresasXGrupo += (empresasXGrupo.equals("")?"":"; ")+(String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
	}
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("empresasXGrupo", empresasXGrupo);
	jsonObj.put("nombrePyme", strNombre);
	infoRegresar = jsonObj.toString();

}else  if(informacion.equals("valoresIniciales")  && !clave_epo.equals("") ){

	StringBuffer campo = new StringBuffer();
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("numeroCampos",String.valueOf(numeroCampos));	
	for(int y = 0; y < numeroCampos; y++){
			int x = y+1;
			String idCampo = "CampoAdicional_"+x;
			String nombreCampo = camposAdicionalesParametrizados.get("nombre_campo_"+y).toString();			
			String tipoDato = camposAdicionalesParametrizados.get("tipo_dato_"+y).toString();
			String longitud = camposAdicionalesParametrizados.get("longitud_campo_"+y).toString();
			String camposAdicionales = "camposAdicionales"+x;
			
			String tiposdDato ="";
			if(tipoDato.equals("A"))  {  tiposdDato = "textfield"; }
			if(tipoDato.equals("N"))  {  tiposdDato = "numberfield"; }
			
			campo = new StringBuffer();
			campo.append( "	{ "+
							"	xtype: '"+tiposdDato+"',"+
							"	name: '"+idCampo+"',"+
							"	id: '"+idCampo+"',"+
							"	fieldLabel: '"+nombreCampo+"',"+
							"	allowBlank: true, "+
							"	hidden: false,"+
							"	maxLength: "+longitud+","+
							"	width: 150,"+
							"	msgTarget: 'side',"+
							//"	hidden: true, "+
							"	margins: '0 20 0 0'  "+
							"}		");			
					
			jsonObj.put(camposAdicionales,campo.toString());
		}			
		
	infoRegresar = jsonObj.toString();


}else if(informacion.equals("Guardar") ){

	ImagenDocumentoContrato imagenDocumentoContrato = new ImagenDocumentoContrato();
	imagenDocumentoContrato.setClavePyme(clavePyme);
	imagenDocumentoContrato.setTipoExpediente("11");
		
	List camposAdicionales  = new ArrayList();
	List montoMoneda  = new ArrayList();
	List claveMonedaHid = new ArrayList();
	
	if(!montoN.equals("")) {	montoMoneda.add(montoN);  claveMonedaHid.add("1"); }
	if(!montoE.equals("")) {	montoMoneda.add(montoE);  claveMonedaHid.add("25"); }
	if(!montoD.equals(""))  {	montoMoneda.add(montoD);  claveMonedaHid.add("54"); }
	
	
	
	if(!CampoAdicional_1.equals("") ) {  camposAdicionales.add(CampoAdicional_1); }
	if(!CampoAdicional_2.equals("") ) {  camposAdicionales.add(CampoAdicional_2); }
	if(!CampoAdicional_3.equals("") ) {  camposAdicionales.add(CampoAdicional_3); }
	if(!CampoAdicional_4.equals("") ) {  camposAdicionales.add(CampoAdicional_4); }
	if(!CampoAdicional_5.equals("") ) {  camposAdicionales.add(CampoAdicional_5); }
			
	HashMap datosCapturaSolicitud = new HashMap();
	datosCapturaSolicitud.put("clavePyme", clavePyme);
	datosCapturaSolicitud.put("claveEpo", clave_epo);
	datosCapturaSolicitud.put("claveIfCesionario", clave_if);
	datosCapturaSolicitud.put("claveClasificacionEpo", clasiEpo);
	datosCapturaSolicitud.put("numeroContrato", numero_contrato);
	datosCapturaSolicitud.put("montoContrato", montoDefinido);
	datosCapturaSolicitud.put("claveMoneda", claveMoneda);
	datosCapturaSolicitud.put("claveTipoContratacion", claveTipoContratacion);
	datosCapturaSolicitud.put("fechaVigenciaContratoIni", fecha_vigencia_ini);
	datosCapturaSolicitud.put("fechaVigenciaContratoFin", fecha_vigencia_fin);
	datosCapturaSolicitud.put("objetoContrato", objetoContrato);
	datosCapturaSolicitud.put("comentariosSolicitud", comentarios);
	datosCapturaSolicitud.put("camposAdicionales", camposAdicionales);
	datosCapturaSolicitud.put("csTipoMonto", csTipoMontoChk);
	datosCapturaSolicitud.put("montoContratoMin", montoContratoMin);
	datosCapturaSolicitud.put("montoContratoMax", montoContratoMax);
	datosCapturaSolicitud.put("claveMonedaHid", claveMonedaHid);
	datosCapturaSolicitud.put("montoMoneda", montoMoneda);
	datosCapturaSolicitud.put("plazoContrato", plazoContrato);
	datosCapturaSolicitud.put("claveTipoPlazo", claveTipoPlazo);
	datosCapturaSolicitud.put("claveVentanillaPago", claveVentanillaPago);
	datosCapturaSolicitud.put("supervisorAdministradorResidente", supAdmResob);
	datosCapturaSolicitud.put("numeroTelefono", telefono);
	//Agrega multimoneda Cesion Derechos
	datosCapturaSolicitud.put("montoContratoMinDL", montoContratoMinDL);
	datosCapturaSolicitud.put("montoContratoMaxDL", montoContratoMaxDL);
	datosCapturaSolicitud.put("montoContratoMinEU", montoContratoMinEU);
	datosCapturaSolicitud.put("montoContratoMaxEU", montoContratoMaxEU);
	datosCapturaSolicitud.put("firmaContrato", firmaContrato);
	datosCapturaSolicitud.put("empresas", empresas);
	
	datosCapturaSolicitud.put("grupoCDer", grupoCDer);
	


	
	//inserta la solicitud de Consentimiento 
	String numeroProceso0 = cesionBean.insertarSolicitudConsentimientoTemp(datosCapturaSolicitud);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("numeroProceso", numeroProceso0);
	jsonObj.put("clave_epo", clave_epo);
	jsonObj.put("clave_if", clave_if);	
	
	infoRegresar = jsonObj.toString();
	 
}else if(informacion.equals("Preacuse") ){

		JSONArray registrosPreacuse = new JSONArray();
		JSONObject 	resultado	= new JSONObject();
	
		//obtengo la consulta del preacuse
		HashMap consultaPreacuseSolicitud = cesionBean.consultarSolicitudConsentimientoTemp(numeroProceso);
		String nombrePyme = ((String)consultaPreacuseSolicitud.get("nombre_pyme"));
		String empresasCed = ((String)consultaPreacuseSolicitud.get("empresas"));
		empresasCed = nombrePyme+"; "+empresasCed;
		consultaPreacuseSolicitud.put("empresas",empresasCed.replaceAll(";","<BR>"));
		registrosPreacuse.add(consultaPreacuseSolicitud);
		String consulta = "{\"success\": true, \"total\": \"" + registrosPreacuse.size() + "\", \"registros\": " + registrosPreacuse.toString()+"}";
		
		String nombreEPO = consultaPreacuseSolicitud.get("nombre_epo").toString();
		
		resultado = JSONObject.fromObject(consulta);		
		resultado.put("nombreEPO",nombreEPO);
		resultado.put("clasificacionEpo",clasificacionEpo);
		resultado.put("hayCamposAdicionales",String.valueOf(numeroCampos));	 //numero de campos Adicionales
	
		for(int y = 0; y < numeroCampos; y++){	
		String nombreCampo = ("Campo"+y);
		resultado.put(nombreCampo,camposAdicionalesParametrizados.get("nombre_campo_"+y));		
		}
		
		infoRegresar = resultado.toString();


}else if(informacion.equals("Acuse") ){
	
	HashMap datosPreacuseSolicitud = new HashMap();
	datosPreacuseSolicitud.put("numeroProceso", numeroProceso);
	datosPreacuseSolicitud.put("loginUsuario", loginUsuario);
	datosPreacuseSolicitud.put("nombreUsuario", strNombreUsuario);
	
	//inserta la Solicitud de Consentimiento	
	String Solicitud = cesionBean.insertarSolicitudConsentimiento(datosPreacuseSolicitud);
	

	JSONObject 	resultado	= new JSONObject();
	resultado.put("success", new Boolean(true));
	resultado.put("numeroSolicitud",Solicitud);
	infoRegresar  = resultado.toString();


}else if(informacion.equals("MostrarAcuse")  ){

	JSONArray registrosAcuse = new JSONArray();
	JSONObject 	resultado	= new JSONObject();
	
	//mostrar datos del Acuse de Solicitud de Consentimiento
	HashMap consultaAcuseSolicitud = cesionBean.consultarSolicitudConsentimiento(numeroSolicitud);
	String nombrePyme = ((String)consultaAcuseSolicitud.get("nombre_pyme"));
	String empresasCed = ((String)consultaAcuseSolicitud.get("empresas"));
	empresasCed = nombrePyme+"; "+empresasCed;
	
	consultaAcuseSolicitud.put("empresas",empresasCed.replaceAll(";","<BR>"));
	
	registrosAcuse.add(consultaAcuseSolicitud);
	String consulta = "{\"success\": true, \"total\": \"" + registrosAcuse.size() + "\", \"registros\": " + registrosAcuse.toString()+"}";
	String nombreEPO = consultaAcuseSolicitud.get("nombre_epo").toString();
		
	resultado = JSONObject.fromObject(consulta);		
	resultado.put("nombreEPO",nombreEPO);
	resultado.put("clasificacionEpo",clasificacionEpo);
	resultado.put("hayCamposAdicionales",String.valueOf(numeroCampos));	 //numero de campos Adicionales
		
	for(int y = 0; y < numeroCampos; y++){	
		String nombreCampo = ("Campo"+y);
		resultado.put(nombreCampo,camposAdicionalesParametrizados.get("nombre_campo_"+y));		
	}
	
	infoRegresar = resultado.toString();
	
			
}else if(informacion.equals("MostrarDatosAcuse") ){
	
	//mostrar datos del Acuse de Solicitud de Consentimiento
	HashMap consultaAcuseSolicitud = cesionBean.consultarSolicitudConsentimiento(numeroSolicitud);
	JSONArray registroAcuse = new JSONArray();
	HashMap datosAcuse = new HashMap();
		for(int i=0; i<4; i++) {
			if(i==0) {
				datosAcuse.put("descripcion", "Número de Acuse");
				datosAcuse.put("datos",consultaAcuseSolicitud.get("acuse1"));
			}				
			if(i==1) {				
				datosAcuse.put("descripcion", "Fecha de Carga");
				datosAcuse.put("datos",consultaAcuseSolicitud.get("fechaCarga"));
			}			
			if(i==2) {
				datosAcuse.put("descripcion", "Hora de Carga");
				datosAcuse.put("datos",consultaAcuseSolicitud.get("horaCarga"));
			}
			if(i==3) {			
				datosAcuse.put("descripcion", "Usuario de Captura");
				datosAcuse.put("datos",consultaAcuseSolicitud.get("usuarioCarga"));
			}
			registroAcuse.add(datosAcuse);
		}
			
		infoRegresar = "{\"success\": true, \"total\": \"" + registroAcuse.size() + "\", \"registrosAcuse\": " + registroAcuse.toString()+"}";
	
}else if(informacion.equals("BuscaContrato") ){

	JSONObject 	resultado	= new JSONObject();
	boolean existe = cesionBean.existeNumeroContrato(clavePyme, numero_contrato, clave_epo);
	resultado.put("success", new Boolean(true));
	resultado.put("existe", new Boolean(existe));
	infoRegresar = resultado.toString();

}else if(informacion.equals("BuscaContratoCedido") ){

	JSONObject 	resultado	= new JSONObject();
	boolean existe = cesionBean.existeNumeroContratoCedido(numero_contrato, clave_epo);

	resultado.put("success", new Boolean(true));
	resultado.put("existe", new Boolean(existe));
	infoRegresar = resultado.toString();

}

%>

<%=infoRegresar%>

