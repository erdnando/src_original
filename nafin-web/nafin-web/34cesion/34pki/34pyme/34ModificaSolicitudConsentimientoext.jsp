<!DOCTYPE html>
<%@ page import="java.util.*,
		com.netro.cesion.*,
		netropology.utilerias.*" 
		contentType="text/html;charset=windows-1252"
		errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<% String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<%-- El jsp que contiene el dise?o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<script type="text/javascript" src="../../34AutoConsorcio.js"></script>
<script type="text/javascript" src="34ModificaSolicitudConsentimientoext.js?<%=session.getId()%>"></script>
	<!--style type=text/css> 
        .upload-icon {
            background: url('/nafin/00utils/extjs/file_add.png') no-repeat 0 0 !important;
        }
		  .icon-save { 
		  		background-image: url(/nafin/00utils/extjs/save.gif) !important; background-repeat: no-repeat; 
		  }
		  .silk-add { 
		  		background-image: url(/nafin/00utils/extjs/add.gif) !important; background-repeat: no-repeat; 
		  }
        #fi-button-msg {
            border: 2px solid #ccc;
            padding: 5px 10px;
            background: #eee;
            margin: 5px;
            float: left;
        }
		</style--> 
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01pyme/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01pyme/menuLateralFlotante.jspf"%>
	<div id="areaContenido"><div style="height:190px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01pyme/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
<form id='formParametros' name="formParametros">
	<input type="hidden" id="clave_solicitud" name="clave_solicitud" value="<%=clave_solicitud%>" />
</form>
</body>
</html>