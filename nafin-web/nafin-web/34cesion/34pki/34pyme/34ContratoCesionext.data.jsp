<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.text.*, 
		netropology.utilerias.usuarios.UtilUsr,
		netropology.utilerias.usuarios.Usuario,
		com.netro.exception.NafinException,
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>

<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String clave_if = (request.getParameter("clave_if")!=null)?request.getParameter("clave_if"):"";
String numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String clave_moneda = (request.getParameter("clave_moneda")!=null)?request.getParameter("clave_moneda"):"";
String clave_estatus_sol = (request.getParameter("clave_estatus_sol")!=null)?request.getParameter("clave_estatus_sol"):"";
String clave_contratacion = (request.getParameter("clave_contratacion")!=null)?request.getParameter("clave_contratacion"):"";
String fecha_vigencia_ini = (request.getParameter("fecha_vigencia_ini")!=null)?request.getParameter("fecha_vigencia_ini"):"";
String fecha_vigencia_fin = (request.getParameter("fecha_vigencia_fin")!=null)?request.getParameter("fecha_vigencia_fin"):"";
String plazoContrato = (request.getParameter("plazoContrato")!=null)?request.getParameter("plazoContrato"):"";
String claveTipoPlazo = (request.getParameter("claveTipoPlazo")!=null)?request.getParameter("claveTipoPlazo"):"";
String contador = (request.getParameter("contador")!=null)?request.getParameter("contador"):"0";

String clave_pyme = (String)request.getSession().getAttribute("iNoCliente");
String infoRegresar ="";

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);


	
if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setCs_cesion_derechos("S");
	infoRegresar = cat.getJSONElementos();	


}else if(informacion.equals("CatalogoIF")){

		CatalogoIF cat = new CatalogoIF();
		cat.setClave("ic_if");
		cat.setDescripcion("cg_razon_social");
		cat.setG_cs_cesion_derechos("S");
		infoRegresar = cat.getJSONElementos();	


}else if(informacion.equals("CatalogoMoneda")){

	CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("0,1,54,25", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	

}else if(informacion.equals("CatalogoEstatus")){

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus_solic");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("cdercat_estatus_solic");
		catalogo.setOrden("ic_estatus_solic");
		catalogo.setValoresCondicionIn("3,7,9,10,14,25", Integer.class);		
		infoRegresar = catalogo.getJSONElementos();	
	
	System.out.println("infoRegresar***** "+infoRegresar);


}else if(informacion.equals("CatalogoContratacion") && !clave_epo.equals("") ){


	JSONArray registros = new JSONArray();
	HashMap info = new HashMap();
	List catalogoTipoContratacion = cesionBean.obtenerComboTipoContratacion(clave_epo);

	for(int i=0; i<catalogoTipoContratacion.size(); i++){
	
		String valor  = (String)catalogoTipoContratacion.get(i).toString(); 
		String clave =valor.substring(0,1);
		String descripcion =valor.substring(2,valor.length());
		info = new HashMap();
		info.put("clave", clave);
		info.put("descripcion", descripcion);	
		registros.add(info);
	}
	Map mpParams = cesionBean.getParametrosEpo(clave_epo);	
	String csProrroga = (String)mpParams.get("CDER_CS_PRORROGA_CONTRATO");
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+", \"CDER_CS_PRORROGA_CONTRATO\":\""+csProrroga+"\"}";


}else if(informacion.equals("CatalogoTipoPlazo")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setTabla("cdercat_tipo_plazo");
	catalogo.setCampoClave("ic_tipo_plazo");
	catalogo.setCampoDescripcion("cg_descripcion");
	catalogo.setOrden("ic_tipo_plazo");
	catalogo.getListaElementos();
	infoRegresar = catalogo.getJSONElementos();		
	
}else if(informacion.equals("Consultar")){

	JSONObject 	resultado	= new JSONObject();

	String clasificacionEpo ="";
	
	/*estos ya no se usan porque no se estas tomando 
	encuenta para los criterios de busqueda  los campos Adicionales*/
	List campos_adicionales = new ArrayList();	
	List tipo_dato = new ArrayList(); 	

	HashMap parametros_consulta = new HashMap();
	JSONArray registros = new JSONArray();
	parametros_consulta.put("clave_pyme", iNoCliente);
	parametros_consulta.put("clave_epo", clave_epo);
	parametros_consulta.put("clave_if", clave_if.trim());
	parametros_consulta.put("numero_contrato", numero_contrato);
	parametros_consulta.put("clave_moneda", clave_moneda);
	parametros_consulta.put("clave_estatus_sol", clave_estatus_sol);
	parametros_consulta.put("clave_contratacion", clave_contratacion);
	parametros_consulta.put("fecha_vigencia_ini", fecha_vigencia_ini);
	parametros_consulta.put("fecha_vigencia_fin", fecha_vigencia_fin);
	parametros_consulta.put("plazoContrato", plazoContrato);
	parametros_consulta.put("claveTipoPlazo", claveTipoPlazo);
	parametros_consulta.put("campos_adicionales", campos_adicionales);
	parametros_consulta.put("tipo_dato", tipo_dato);
	parametros_consulta.put("strTipoUsuario", strTipoUsuario);
	
	HashMap contratoCesionE = new HashMap();
	
	HashMap consultaContratosCesion = cesionBean.consultaContratosCesionPyme(parametros_consulta);

	//esta opcion es para la Columna de Accion
	String  multiUsrPyme ="";
	/*UtilUsr utilUsr = new UtilUsr();
	List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");
	if (usuariosPorPerfil.size() > 1) {
		multiUsrPyme ="S";
	}else{
		multiUsrPyme = "N";
	}*/
	
		String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");
		HashMap parametrosConsulta = new HashMap();
		parametrosConsulta.put("loginUsuario", loginUsuario);
		parametrosConsulta.put("clavePyme", clave_pyme);
		parametrosConsulta.put("claveEpo", clave_epo);
		parametrosConsulta.put("claveIfCesionario", clave_if.trim());
		parametrosConsulta.put("numeroContrato", numero_contrato);
		parametrosConsulta.put("claveMoneda", clave_moneda);
		parametrosConsulta.put("claveTipoContratacion", clave_contratacion);
		parametrosConsulta.put("fechaVigenciaContratoIni", fecha_vigencia_ini);
		parametrosConsulta.put("fechaVigenciaContratoFin", fecha_vigencia_fin);
					
		HashMap solicitudesAutorizadasPorUsuario = cesionBean.obtieneSolicitudesAutorizadasPorUsuario(parametrosConsulta);
		int numSolAut = Integer.parseInt((String)solicitudesAutorizadasPorUsuario.get("numeroRegistros"));

		//para saber si hay campos parametrizados para la EPO	
		HashMap campos_adicionales2 = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");	
		int numeroCampos = Integer.parseInt((String)campos_adicionales2.get("indice"));
		
		
		
	for (int i = 0; i <=consultaContratosCesion.size(); i++) {		
	
		HashMap contratoCesion = (HashMap)consultaContratosCesion.get("contratoCesion"+i);
		
		if(contratoCesion !=null) {
			contratoCesionE = new HashMap();
			contratoCesionE.put("CLAVE_SOLICITUD",contratoCesion.get("clave_solicitud"));
			contratoCesionE.put("CLAVEEPO", contratoCesion.get("clave_epo"));
			contratoCesionE.put("CLAVEPYME", contratoCesion.get("clavePyme"));
			contratoCesionE.put("CLAVETIPOCONTRATACION", contratoCesion.get("claveTipoContratacion"));
			contratoCesionE.put("NOMBRE_EPO", contratoCesion.get("nombre_epo"));
			contratoCesionE.put("NOMBRE_IF", contratoCesion.get("nombre_if"));
			contratoCesionE.put("NUMERO_CONTRATO", contratoCesion.get("numero_contrato"));
			contratoCesionE.put("MONTOSPORMONEDA", contratoCesion.get("montosPorMoneda"));
			contratoCesionE.put("TIPO_CONTRATACION", contratoCesion.get("tipo_contratacion"));
			contratoCesionE.put("FECHA_INICIO_CONTRATO", contratoCesion.get("fecha_inicio_contrato"));
			contratoCesionE.put("FECHA_FIN_CONTRATO", contratoCesion.get("fecha_fin_contrato"));
			contratoCesionE.put("PLAZOCONTRATO", contratoCesion.get("plazoContrato"));
			contratoCesionE.put("CLASIFICACION_EPO", contratoCesion.get("clasificacion_epo"));
			contratoCesionE.put("CAMPO_ADICIONAL_1", contratoCesion.get("campo_adicional_1"));
			contratoCesionE.put("CAMPO_ADICIONAL_2", contratoCesion.get("campo_adicional_2"));
			contratoCesionE.put("CAMPO_ADICIONAL_3", contratoCesion.get("campo_adicional_3"));
			contratoCesionE.put("CAMPO_ADICIONAL_4", contratoCesion.get("campo_adicional_4"));
			contratoCesionE.put("CAMPO_ADICIONAL_5", contratoCesion.get("campo_adicional_5"));
			contratoCesionE.put("VENTANILLAPAGO", contratoCesion.get("venanillaPago"));
			contratoCesionE.put("SUPADMRESOB", contratoCesion.get("supAdmResob"));
			contratoCesionE.put("NUMEROTELEFONO", contratoCesion.get("numeroTelefono"));
			contratoCesionE.put("OBJETO_CONTRATO", contratoCesion.get("objeto_contrato"));
			contratoCesionE.put("FECHAACEPTACION", contratoCesion.get("fechaAceptacion"));
			contratoCesionE.put("CLAVE_ESTATUS", contratoCesion.get("clave_estatus"));
			contratoCesionE.put("ESTATUS_SOLICITUD", contratoCesion.get("estatus_solicitud"));
			contratoCesionE.put("COMENTARIOS", contratoCesion.get("comentarios"));
			contratoCesionE.put("DIASMINIMOSNOTIFICACION", contratoCesion.get("diasMinimosNotificacion"));
			contratoCesionE.put("ACUSEENVIOSOLICEPO", contratoCesion.get("acuseEnvioSolicEpo"));
			contratoCesionE.put("CLAVEUSREPOAUTCESION", contratoCesion.get("claveUsrEpoAutCesion"));
			contratoCesionE.put("NOMBREUSREPOAUTCESION", contratoCesion.get("nombreUsrEpoAutCesion"));
			contratoCesionE.put("FECHAEPOAUTCESION", contratoCesion.get("fechaEpoAutCesion"));
			contratoCesionE.put("HORAEPOAUTCESION", contratoCesion.get("horaEpoAutCesion"));
			contratoCesionE.put("CONTRATOCESION", contratoCesion.get("contratoCesion"));	
			contratoCesionE.put("FECHASOLICITUD", contratoCesion.get("fechasolicitud"));
			contratoCesionE.put("CLAVE_IF", contratoCesion.get("clave_if"));
			contratoCesionE.put("FIRMA_CONTRATO", contratoCesion.get("firma_contrato"));
			contratoCesionE.put("FECHA_PRORROGA", contratoCesion.get("fecha_prorroga"));
			contratoCesionE.put("ESTATUS_PRORROGA", contratoCesion.get("estatus_prorroga"));
			contratoCesionE.put("CAUSAS_RETORNO", contratoCesion.get("causas_retorno"));
			contratoCesionE.put("CONTADOR_PRORROGA", contratoCesion.get("contador_prorroga"));
			contratoCesionE.put("TERMINO_PRORROGA", contratoCesion.get("termino_prorroga"));
			System.out.println(contratoCesion.get("termino_prorroga").toString()+"\n\n\n\n\n");
			String ic_estatus = contratoCesion.get("clave_estatus").toString();
			
			String num_rep = contratoCesion.get("num_rep").toString();
			String num_firma_rep = contratoCesion.get("num_firma_rep").toString();
			if(Integer.parseInt(num_rep)>1){
				multiUsrPyme="S";
			}else{
				multiUsrPyme="N";
			}
			
			int contFirmas = Integer.parseInt(num_rep)-Integer.parseInt(num_firma_rep);
			
			if (multiUsrPyme.equals("S")) {
				if (ic_estatus.equals("3")) {
					contratoCesionE.put("NUEVOESTATUS", "14");					
					
				} else if (ic_estatus.equals("14")) {
					boolean recordFound = false;
					for (int j = 0; j < numSolAut; j++) {
						if (contratoCesion.get("clave_solicitud").toString().equals(solicitudesAutorizadasPorUsuario.get("numeroSolicitud" + j).toString())) {
							recordFound = true;
						}
					}
					
					if(contFirmas==1 && !recordFound){
						contratoCesionE.put("NUEVOESTATUS", "7");
						contratoCesionE.put("MENSAJE", "Autorizar");	
					}else if(recordFound){
						contratoCesionE.put("NUEVOESTATUS", "");
						contratoCesionE.put("MENSAJE", "N/A");	
					}else{
						contratoCesionE.put("NUEVOESTATUS", "14");
						contratoCesionE.put("MENSAJE", "");	
					}
				}
			} else {			
				if (ic_estatus.equals("3")) {
					contratoCesionE.put("NUEVOESTATUS", "7");
					contratoCesionE.put("MENSAJE", "Autorizar");						
				} else {
					contratoCesionE.put("NUEVOESTATUS", "");
					if(contratoCesion.get("estatus_prorroga").toString().equals("P")){						
						contratoCesionE.put("MENSAJE", "N/A");		
					}
				}
			}		
			
			//se Obtienen los datos para la Firma 
			String login_usuario = (String)request.getSession().getAttribute("Clave_usuario");
			String  clave_solicitud  = contratoCesion.get("clave_solicitud").toString();
			HashMap contratoCesion1 = cesionBean.consultaContratoCesionPyme(clave_solicitud, login_usuario);
			String claveEpo = (String)contratoCesion1.get("clave_epo");
			String claveIf = (String)contratoCesion1.get("clave_if");
			System.out.println(claveIf);
			StringBuffer clausulado_parametrizado = cesionBean.consultaClausuladoParametrizado(claveIf);	
			String sello_digital = cesionBean.generaSelloDigital( clave_solicitud, strNombreUsuario );

			StringBuffer texto_plano = new StringBuffer();
			texto_plano.append("MONTO / MONEDA:|"+contratoCesion1.get("montosPorMoneda").toString().replaceAll("<br/>", " ")+"\n");
			texto_plano.append("FECHA DE VIGENCIA:|"+("N/A".equals(contratoCesion1.get("fecha_inicio_contrato"))?"N/A":(contratoCesion1.get("fecha_inicio_contrato") + " a " + contratoCesion1.get("fecha_fin_contrato")))+"\n");
			texto_plano.append("PLAZO DEL CONTRATO:|"+contratoCesion1.get("plazoContrato")+"\n");
			texto_plano.append("TIPO DE CONTRATACIÓN:|"+contratoCesion1.get("tipo_contratacion").toString().toUpperCase()+"\n");
			texto_plano.append("OBJETO DEL CONTRATO:|"+contratoCesion1.get("objeto_contrato").toString().toUpperCase()+"\n");
			for (int z = 0; z < numeroCampos; z++) {
				texto_plano.append(campos_adicionales2.get("nombre_campo_"+z).toString().toUpperCase()+":|"+contratoCesion1.get("campo_adicional_"+(z+1)).toString().toUpperCase()+"\n");
			}
			texto_plano.append("INFORMACIÓN DE LA EMPRESA DE PRIMER ORDEN\n\n");
			texto_plano.append("NOMBRE:|"+contratoCesion1.get("nombre_epo")+"\n");
			texto_plano.append("INFORMACIÓN DEL CESIONARIO (IF)\n\n");
			texto_plano.append("NOMBRE:|"+contratoCesion1.get("nombre_if")+"\n");
			texto_plano.append("BANCO DE DEPÓSITO:|"+contratoCesion1.get("bancoDeposito")+"\n");
			texto_plano.append("NÚMERO DE CUENTA PARA DEPÓSITO:|"+contratoCesion1.get("numeroCuenta")+"\n");
			texto_plano.append("NÚMERO DE CUENTA CLABE PARA DEPÓSITO:|"+contratoCesion1.get("numeroCuentaClabe")+"\n");
			texto_plano.append("INFORMACIÓN DE LA CESIÓN DE DERECHOS\n\n");
			texto_plano.append("FECHA DE SOLICITUD DE CONSENTIMIENTO:|"+contratoCesion1.get("fechaSolicitudConsentimiento")+"\n");
			texto_plano.append("FECHA DE CONSENTIMIENTO DE LA EPO:|"+contratoCesion1.get("fechaAceptacionEpo")+"\n");
			texto_plano.append("PERSONA QUE OTORGÓ EL CONSENTIMIENTO:|"+(contratoCesion1.get("nombreUsrEpoAutCesion")==null?"":contratoCesion1.get("nombreUsrEpoAutCesion"))+"\n");			
			texto_plano.append("FECHA DE FORMALIZACIÓN DEL CONTRATO DE CESIÓN:|"+(contratoCesion1.get("fechaFormalContrato")==null?"":contratoCesion1.get("fechaFormalContrato"))+"\n");			
			texto_plano.append("NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):|"+(contratoCesion1.get("nombreUsrAutRep1")==null?"":contratoCesion1.get("nombreUsrAutRep1"))+"\n");
			texto_plano.append("NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):|"+(contratoCesion1.get("nombreUsrAutRep2")==null?"":contratoCesion1.get("nombreUsrAutRep2"))+"\n");
			texto_plano.append("NOMBRE DEL FIRMANTE DE CESIONARIO (IF):|"+(contratoCesion1.get("nombreUsrIf")==null?"":contratoCesion1.get("nombreUsrIf"))+"\n");
			texto_plano.append("NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:|"+(contratoCesion1.get("nombreUsrTest1")==null?"":contratoCesion1.get("nombreUsrTest1"))+"\n");
			texto_plano.append("NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:|"+(contratoCesion1.get("nombreUsrTest2")==null?"":contratoCesion1.get("nombreUsrTest2"))+"\n");
			texto_plano.append("FECHA DE NOTIFICACIÓN ACEPTADA POR LA EPO:|"+(contratoCesion1.get("fechaNotifEpo")==null?"":contratoCesion1.get("fechaNotifEpo"))+"\n");
			texto_plano.append("PERSONA QUE ACEPTÓ LA NOTIFICACIÓN:|"+(contratoCesion1.get("nombreUsrNotifEpo")==null?"":contratoCesion1.get("nombreUsrNotifEpo"))+"\n");
			texto_plano.append("FECHA DE INFORMACIÓN A VENTANILLA DE PAGOS:|"+(contratoCesion1.get("fechaNotifVentanilla")==null?"":contratoCesion1.get("fechaNotifVentanilla"))+"\n");
			texto_plano.append("FECHA DE RECEPCIÓN EN VENTANILLA:|"+(contratoCesion1.get("fechaNotifVentanilla")==null?"":contratoCesion1.get("fechaNotifVentanilla"))+"\n");
			texto_plano.append("FECHA DE LA APLICACIÓN DEL REDIRECCIONAMIENTO:|"+(contratoCesion1.get("fechaRedirecccion")==null?"":contratoCesion1.get("fechaRedirecccion"))+"\n");
			texto_plano.append("PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:|"+(contratoCesion1.get("nombreUsrRedirec")==null?"":contratoCesion1.get("nombreUsrRedirec"))+"\n");
			texto_plano.append("\nSE SUJETA A LAS SIGUIENTES CLÁUSULAS\n");
			texto_plano.append("\n"+clausulado_parametrizado.toString());
			texto_plano.append("\n\n");
			texto_plano.append("Sello Digital de "+contratoCesion1.get("nombre_pyme")+"\n");
			texto_plano.append(sello_digital);
			
			
			contratoCesionE.put("TEXTOFIRMAR",texto_plano.toString());
			contratoCesionE.put("SELLODIGIDAL",sello_digital.toString());
			registros.add(contratoCesionE);	
		}		//if(contratoCesion !=null) {				
			
	}//concluye For para obtener los registros
	
	

	String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
	resultado = JSONObject.fromObject(consulta);
	
	//Obtengo la descripcion de Clasificacion EPO
	if(!clave_epo.equals("")){
		 clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
	}
	resultado.put("clasificacionEpo",clasificacionEpo);
	resultado.put("hayCamposAdicionales",String.valueOf(numeroCampos));	 //numero de campos Adicionales
	
	for(int y = 0; y < numeroCampos; y++){	
	String nombreCampo = ("Campo"+y);
	resultado.put(nombreCampo,campos_adicionales2.get("nombre_campo_"+y));		
	
	}
	
	infoRegresar = resultado.toString();	

} else if(informacion.equals("GeneraAcuse")){
	%><%@ include file="../certificado.jspf" %><%
	Calendar calendario = Calendar.getInstance();
	SimpleDateFormat formato_cert = new SimpleDateFormat("ddMMyyyyHHmmss");
	SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat formato_hora = new SimpleDateFormat("hh:mm aa");
			
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
	String textoFirmado = (request.getParameter("textoFirmado")!=null)?request.getParameter("textoFirmado"):"";
	String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
	String claveNueva = (request.getParameter("claveNueva")!=null)?request.getParameter("claveNueva"):"";
	String sello_digital = (request.getParameter("sello_digital")!=null)?request.getParameter("sello_digital"):"";
	if(sello_digital.equals("")){
		 sello_digital = cesionBean.generaSelloDigital( clave_solicitud, strNombreUsuario );
	}
	String fecha_carga = formato_fecha.format(calendario.getTime());
	String hora_carga = formato_hora.format(calendario.getTime());
	String login_usuario = (String)request.getSession().getAttribute("Clave_usuario");
	String nombre_usuario = (String)request.getSession().getAttribute("strNombreUsuario");
	String folioCert = formato_cert.format(calendario.getTime());
			
	HashMap parametros_firma = new HashMap();
	parametros_firma.put("_serial", _serial);
	parametros_firma.put("pkcs7", pkcs7);
	parametros_firma.put("texto_plano", textoFirmado);
	parametros_firma.put("validCert", String.valueOf(false));
	parametros_firma.put("folioCert", folioCert);
	parametros_firma.put("clave_solicitud", clave_solicitud);
	parametros_firma.put("nuevoEstatus", claveNueva);
	parametros_firma.put("sello_digital", sello_digital);
	parametros_firma.put("fecha_carga", fecha_carga);
	parametros_firma.put("hora_carga", hora_carga);
	parametros_firma.put("login_usuario", login_usuario);
	parametros_firma.put("nombre_usuario", nombre_usuario);
	parametros_firma.put("strDirectorioTemp",strDirectorioTemp);
	String _acuse = cesionBean.acuseFirmaContratoCesionPyme(parametros_firma);
	String huboError ="", mostrarError ="";
	if(!_acuse.equals("")){
		int tamanio2 =	_acuse.length();
			 huboError =  _acuse.substring(0, 1);	
			 if(huboError.equals("|")){
					mostrarError = "Error";	
			 }
	}		
	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("acuse",_acuse);	 	
	jsonObj.put("mostrarError",mostrarError);	 	
	jsonObj.put("clave_solicitud",clave_solicitud);		
			
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("solicitarProrroga") ){

	String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
	cesionBean.solicitarProrroga(clave_solicitud, contador);
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	
	infoRegresar = jsonObj.toString();
	
}
%>

<%=infoRegresar%>