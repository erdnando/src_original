<!DOCTYPE html>
<%@ page import="java.util.*,
		com.netro.cesion.*,
		netropology.utilerias.*" 
		contentType="text/html;charset=windows-1252"
		errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<%
	String clavePyme = (String)request.getSession().getAttribute("iNoCliente");
	//Instanciacion para el uso del EJB
	CesionEJB cesionEJB = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

	HashMap solicitudesAceptadasEpo = cesionEJB.consultaAvisoSolicitudConsentimientoPyme(clavePyme);
	
	
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%-- El jsp que contiene el dise?o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<%if(solicitudesAceptadasEpo.size()>1){%>
<script type="text/javascript" src="34AvisoSolicitudConsentimientoext.js?<%=session.getId()%>"></script>
<%}else{%>
<script type="text/javascript" src="../../34AutoConsorcio.js"></script>
<script type="text/javascript" src="34ContratoCesionext.js?<%=session.getId()%>"></script>
<%@ include file="/00utils/componente_firma.jspf" %>
<%}%>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%@ include file="/01principal/01pyme/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01pyme/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:190px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01pyme/pie.jspf"%>

<form id='formAux' name="formAux" target='_new'></form>


</body>
</html>