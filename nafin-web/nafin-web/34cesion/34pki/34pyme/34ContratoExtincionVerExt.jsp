<%@ page 	import="
		java.util.*,
		java.sql.*,
		com.netro.cesion.*,
		netropology.utilerias.*,
		com.netro.exception.NafinException"
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<%
String clave_contratacion = (request.getParameter("clave_contratacion")!=null)?request.getParameter("clave_contratacion"):"";
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";

String nombre_epo = (request.getParameter("nombre_epo")!=null)?request.getParameter("nombre_epo"):"";
String objeto_contrato = (request.getParameter("objeto_contrato")!=null)?request.getParameter("objeto_contrato"):"";
String montospormoneda = (request.getParameter("montospormoneda")!=null)?request.getParameter("montospormoneda"):"";
String plazocontrato = (request.getParameter("plazocontrato")!=null)?request.getParameter("plazocontrato"):"";
String fechasolicitud = (request.getParameter("fechasolicitud")!=null)?request.getParameter("fechasolicitud"):"";
String numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String nombre_if = (request.getParameter("nombre_if")!=null)?request.getParameter("nombre_if"):"";
montospormoneda=montospormoneda.replaceAll("<br/>","\t");


String nombreUsuario ="", nombrePyme= "";
StringBuffer textoConsentimiento = new StringBuffer();

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

String banderaConsentimiento=cesionBean.getBanderaConsentimiento(clave_solicitud);

if (clave_solicitud!= null && !clave_solicitud.equals("")) {
	HashMap usuarios = cesionBean.consultaContratoCesionPyme(clave_solicitud); 

	if(usuarios.size()>1){
		if(usuarios.get("nombreUsrEpoAutCesion") !=null){
			nombreUsuario  =  usuarios.get("nombreUsrEpoAutCesion").toString();
		}
		nombrePyme  =  usuarios.get("nombre_pyme").toString();  
	}
}	

if(banderaConsentimiento==null||!banderaConsentimiento.equals("S")){

%>		

		<table width="750" border="0" cellspacing="1" cellpadding="1">
			<tr>
				<td align="center" width="300">&nbsp;</td>
				<td align="center"><img src="/nafin/00archivos/15cadenas/15archcadenas/logos/nafinsa.gif" border=0></td>
				<td align="center" width="300">&nbsp;</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td>
					<table align="center" style="text-align:justify"  width="900" readonly="readonly">
              <%textoConsentimiento.append(nombrePyme + "<br/><br/>");%>
							<%textoConsentimiento.append("Presente<br/><br/>");%>
							<%textoConsentimiento.append("Consentimiento Condicionado para Ceder los Derechos de Cobro respecto del Contrato No."+ numero_contrato +"<br/><br/>");%>
							
							<%textoConsentimiento.append(""+nombrePyme + "<br/>");%>
							<%textoConsentimiento.append("Objeto: "+objeto_contrato + "<br/>");%>
							<%textoConsentimiento.append("Monto: "+montospormoneda + "<br/>");%>
							<%textoConsentimiento.append("Plazo: "+plazocontrato + "<br/>");%>
							
							<%textoConsentimiento.append("Fecha de Solicitud: "+fechasolicitud + "<br/><br/>");%>
							
							<%textoConsentimiento.append(" En atenci?n a su solicitud, "+ nombre_epo +", le informa que con base en la informaci?n recabada al interior del organismo, se otorga el consentimiento que solicita, con fundamento en: El Art?culo 46 de la Ley de Adquisiciones, Arrendamientos y Servicios del Sector P?blico.  El Art?culo 47 de la Ley de Obras P?blicas y Servicios Relacionados con las Mismas.  Los Art?culos 53, fracci?n XVII, de la Ley de Petr?leos Mexicanos,"+
							"48 de su Reglamento, y 54 de las Disposiciones administrativas de contrataci?n en materia de adquisiciones, arrendamientos, obras y servicios de las actividades sustantivas de car?cter productivo de Petr?leos Mexicanos y  Organismos Subsidiarios, seg?n corresponda, as? como en la cl?usula relativa a la Cesi?n de derechos de Cobro del Contrato.  <br/><br/>");%>
							<%textoConsentimiento.append("No obstante lo anterior, el Consentimiento estar? Condicionado a que: <br/><br/>");%>
							<%textoConsentimiento.append("1.- "+nombrePyme+", en lo sucesivo la CEDENTE, deber? celebrar con el "+ nombre_if	+" un Contrato de Cesi?n Electr?nica de Derechos de Cobro, mismo que deber? ser suscrito por el o los representantes legales o apoderados de la CEDENTE que cuente(n) con facultades generales o especiales para actos de dominio y que dicho instrumento contractual sea notificado a la Entidad a trav?s de este sistema NAFIN para que surta efectos  legales,"+
							" en t?rminos de lo establecido en el art?culo 390 del C?digo de Comercio, en el entendido que de no efectuarse la mencionada notificaci?n dentro de los 15 (quince) d?as h?biles siguientes a la fecha en que sea notificada en el sistema NAFIN el presente Consentimiento Condicionado, ?ste quedar? sin efecto alguno, sin ninguna responsabilidad para la Entidad. <br/><br/>");%>
							<%textoConsentimiento.append("2.- La Cesi?n de Derechos de Cobro incluir? los incrementos al monto del contrato arriba se?alado que, en su caso, se pacten con posterioridad mediante convenios modificatorios; y que la Cesi?n de Derechos de Cobro ?nica y exclusivamente proceder? con respecto a facturas pendientes de cobro por trabajos ejecutados/servicios prestados/bienes entregados en t?rminos de lo pactado en el contrato y que hayan sido instruidos por la Entidad.<br/><br/>");%>
							<%textoConsentimiento.append("3.- La Cesi?n de Derechos de Cobro ser? por el 100% (cien por ciento) de los derechos de cobro que se deriven por el cumplimiento en t?rminos del mencionado contrato, a partir de la fecha en que surta efectos la notificaci?n del correspondiente Contrato de Cesi?n Electr?nica de Derechos de Cobro. <br/><br/>");%>
							<%textoConsentimiento.append("4.- El INTERMEDIARIO FINANCIERO acepte y est? de acuerdo en que el pago de los derechos de cobro estar? sujeto a que, en su caso, la Entidad realice las deducciones y/o retenciones que deban hacerse a la CEDENTE por adeudos con la Entidad; penas convencionales; pagos en exceso; cualquier requerimiento de pago derivado de laudo firme ordenado por las Juntas Locales o Federales de Conciliaci?n y Arbitraje,"+
							" con motivo de los juicios laborales instaurados en contra de Petr?leos Mexicanos y sus Organismos Subsidiarios, en relaci?n con la CEDENTE; o cualquier otra deducci?n derivada del contrato mencionado o de la Ley de la materia en relaci?n con el  contrato celebrado  con la Entidad y que es objeto de la presente cesi?n de derechos de cobro, en cuyos supuestos se har?n las deducciones y/o retenciones correspondientes. En tal virtud, la Entidad quedar? liberada de cualquier responsabilidad por las deducciones y/o retenciones a que haya lugar. <br/><br/>");%>
							<%textoConsentimiento.append("5.- El Consentimiento Condicionado otorgado por la Entidad, no constituye una garant?a de pago ni reconocimiento de cualquier derecho del INTERMEDIARIO FINANCIERO frente a la CEDENTE, siendo la ?nica obligaci?n de la Entidad, la de pagar cantidades l?quidas que sean exigibles por el cumplimiento del citado contrato una vez hechas las deducciones y/o retenciones antes mencionadas.<br/><br/>");%>
							<%textoConsentimiento.append("6.- Se deber?n proporcionar los siguientes datos:  Nombre del INTERMEDIARIO FINANCIERO, Banco de Dep?sito, N?mero de Cuenta para Dep?sito y N?mero de Cuenta CLABE, a la cual se realizar?n los pagos. <br/><br/>");%>
							<%textoConsentimiento.append("7.- A la notificaci?n que se practique a la Entidad mediante este sistema NAFIN, NAFIN deber? subir al mismo: i) un ejemplar del Contrato de Cesi?n Electr?nica de Derechos de Cobro con las firmas electr?nicas de los representantes de la CEDENTE y del INTERMEDIARIO FINANCIERO; ii) copia certificada por fedatario p?blico de la documentaci?n con la que se acredite la personalidad del o de los representantes legales o apoderados de la CEDENTE con las facultades generales o especiales para actos de dominio, y iii) copia certificada por fedatario p?blico de la documentaci?n con la que se acredite la personalidad del o de los representantes legales o apoderados del INTERMEDIARIO FINANCIERO con las facultades suficientes para la celebraci?n del Contrato de Cesi?n Electr?nica de Derechos de Cobro en cuesti?n. <br/><br/>");%>
							<%textoConsentimiento.append("8.- El presente Consentimiento Condicionado, quedar? sin efecto en caso de que la Entidad antes de la fecha de notificaci?n a que se refiere el punto n?mero ?1? del presente Consentimiento Condicionado, reciba alguna notificaci?n de autoridad judicial o administrativa que incida sobre el contrato cuyos derechos de cobro ser?n materia de cesi?n. Bajo este supuesto, la Entidad lo informar? inmediatamente mediante este sistema NAFIN a la CEDENTE y al INTERMEDIARIO FINANCIERO, una vez que haya recibido formalmente la notificaci?n de que se trate. <br/><br/>");%>
							<%textoConsentimiento.append("9.- El Contrato de Cesi?n Electr?nica de Derechos de Cobro quedar? sin efectos si y s?lo si la CEDENTE y el INTERMEDIARIO FINANCIERO celebran un Convenio de Extinci?n de la Cesi?n Electr?nica de Derechos de Cobro, en la inteligencia de que sus representantes legales o apoderados deber?n contar con las facultades suficientes para ello, y en su oportunidad notificarlo a la Entidad en los mismos t?rminos previstos para la notificaci?n del Contrato de Cesi?n Electr?nica de Derechos de Cobro para que surta los efectos legales que le correspondan. <br/><br/>");%>
							<%textoConsentimiento.append(nombreUsuario);%>
							<tr>
								<td>&nbsp;</td>
								<td>	
								</td>	
									<td align="center" style="text-align:justify" width="900" readonly="readonly"  name="clave_contratacion"><%=textoConsentimiento.toString()%></td>									
								
								<td>&nbsp;</td>
							</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>	
	</table>
<%}else{
	%>
	<table align="center" width="750" border="0" cellspacing="1" cellpadding="1">
	<embed src="<%=strDirecVirtualTemp+cesionBean.descargaConsentimiento(clave_solicitud,strDirectorioTemp)%>" width="770" height="500"></embed>
	</table>
<%
}%>