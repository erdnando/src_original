<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.text.SimpleDateFormat,
		netropology.utilerias.*,				
		com.netro.cesion.*, 	
		netropology.utilerias.usuarios.*, 
		com.netro.model.catalogos.*,		
		net.sf.json.JSONArray, 
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/34cesion/34secsession_ext.jspf"%>
<%
String informacion        = (request.getParameter("informacion")       !=null)?request.getParameter("informacion"):"";
String clave_epo          = (request.getParameter("clave_epo")         !=null)?request.getParameter("clave_epo"):"";
String clave_pyme         = (request.getParameter("clave_pyme")        !=null)?request.getParameter("clave_pyme"):"";
String clave_contratacion = (request.getParameter("clave_contratacion")!=null)?request.getParameter("clave_contratacion"):"";
String clave_moneda       = (request.getParameter("clave_moneda")      !=null)?request.getParameter("clave_moneda"):"";
String numero_contrato    = (request.getParameter("numero_contrato")   !=null)?request.getParameter("numero_contrato"):"";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String clave_solicitud = (request.getParameter("clave_solicitud") != null) ? request.getParameter("clave_solicitud") : "";
String tipoArchivo        = (request.getParameter("tipoArchivo")       !=null)?request.getParameter("tipoArchivo"):"";
String nuevoEstatus       = (request.getParameter("nuevoEstatus")      !=null)?request.getParameter("nuevoEstatus"):"";
String acuse              = (request.getParameter("acuse")             !=null)?request.getParameter("acuse"):"";
String fechaCarga         = (request.getParameter("fechaCarga")        !=null)?request.getParameter("fechaCarga"):"";
String horaCarga          = (request.getParameter("horaCarga")         !=null)?request.getParameter("horaCarga"):"";
String usuario            = (request.getParameter("usuario")           !=null)?request.getParameter("usuario"):"";
String operacionAcuse     = (request.getParameter("operacionAcuse")    != null) ? request.getParameter("operacionAcuse"): "";
String fecha_inicio       = (request.getParameter("fecha_inicio")      !=null)?request.getParameter("fecha_inicio"):"";
String fecha_final        = (request.getParameter("fecha_final")       !=null)?request.getParameter("fecha_final"):"";
	
String tipoExtincion = ""; // Fodea 04-2015
String existeRepIf2  = ""; // Fodea 04-2015
int icIFRep2         = 0; // Fodea 04-2015

int start= 0, limit =0, indiceCamposAdicionales =0;
String infoRegresar	=	"", consulta ="", clasificacionEpo ="", mensaje ="", nombreIfCesionario ="",
cedente="", dependencia="",empresas = "", //FODEA-024-2014 MOD() nombreContactoAviso="",
campo01 ="", campo02 ="", campo03 ="", campo04 ="", campo05="", nombrePyme ="";
StringBuffer nombreContactoAviso = new StringBuffer();
JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

if(!clave_epo.equals(""))  {
	clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
	Vector lovDatos = cesionBean.getCamposAdicionales(clave_epo, "5");	
	indiceCamposAdicionales = lovDatos.size();
	for(int i = 0; i < indiceCamposAdicionales; i++){
		List campos =(List)lovDatos.get(i);
		if(i==0) 	campo01=  campos.get(1).toString();
		if(i==1) 	campo02=  campos.get(1).toString();
		if(i==2) 	campo03=  campos.get(1).toString();
		if(i==3) 	campo04=  campos.get(1).toString();
		if(i==4) 	campo05=  campos.get(1).toString();			
	}	
}

CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();

if(informacion.equals("Consultar") || informacion.equals("ConsultarPreAcuse") || informacion.equals("ConsultarAcusePDF")){

	tipoExtincion = cesionBean.getTipoExtincion(clave_epo); // Fodea 04-2015
	icIFRep2      = cesionBean.getIcIFRep2(iNoUsuario); // Fodea 04-2015
	existeRepIf2  = cesionBean.getIfRepresentante2(iNoCliente); // Fodea 04-2015
	// Cuando ya firmaron los dos representantes el estatus cambia a 19, por lo tanto se envía el estatus para realizar la consulta
/*
if(tipoExtincion.equals("C")){
		if((operacionAcuse.equals("ACUSE") || informacion.equals("ConsultarAcusePDF")) && icIFRep2 > 0){
			nuevoEstatus = "19";
		} else{
			nuevoEstatus = "";
		}
	}
*/
	if(tipoExtincion.equals("C")){
		if((operacionAcuse.equals("ACUSE") || informacion.equals("ConsultarAcusePDF")) && existeRepIf2.equals("S")){ // Valido si existe un 2o representante
			if(icIFRep2 > 0){ // Si existe el 2o representante, valido si ya firmó
				nuevoEstatus = "19";
			} else{
				nuevoEstatus = "";
			}
		} else if((operacionAcuse.equals("ACUSE") || informacion.equals("ConsultarAcusePDF")) && !existeRepIf2.equals("S")){
			nuevoEstatus = "19";
		}
	}
 
	ConsultaContratosExtincionIf  paginador  = new ConsultaContratosExtincionIf();
	paginador.setClavePyme(clave_pyme);
	paginador.setClaveEpo(clave_epo);
	paginador.setClaveTipoContratacion(clave_contratacion);
	paginador.setNumeroContrato(numero_contrato);
	paginador.setClaveMoneda(clave_moneda);
	paginador.setClaveIfCesionario(iNoCliente);
	paginador.setClasificacionEpo(clasificacionEpo);
	paginador.setClave_solicitud(clave_solicitud);
	paginador.setIc_estatus(nuevoEstatus);
	paginador.setCampo1(campo01);
	paginador.setCampo2(campo02);
	paginador.setCampo3(campo03);
	paginador.setCampo4(campo04);
	paginador.setCampo5(campo05);			
	paginador.setIndiceCamposAdicionales(String.valueOf(indiceCamposAdicionales));	
	paginador.setAcuse(acuse);		
	paginador.setFechaCarga(fechaCarga);	
	paginador.setHoraCarga(horaCarga);	
	paginador.setUsuarioCarga(usuario);	
	paginador.setFechaInicio(fecha_inicio);
	paginador.setFechaFin(fecha_final);
	paginador.setTipoExtincion(tipoExtincion);

	queryHelper = new CQueryHelperRegExtJS(paginador);
		
}

if(informacion.equals("ConsultarAvisos")) {

	int numeroRegistros  =0;
	HashMap solicitudesExtincion = cesionBean.consultaAvisoSolicitudExtincionIf(iNoCliente);
	numeroRegistros = Integer.parseInt((String)solicitudesExtincion.get("numeroRegistros"));
	for (int i = 0; i < numeroRegistros; i++) {
	HashMap solicitudExtincion = (HashMap)solicitudesExtincion.get("solicitudExtincion" + i);
	String pyme		 = (solicitudExtincion.get("nombreCedente").toString() != null) ? solicitudExtincion.get("nombreCedente").toString() : "";
	empresas = (solicitudExtincion.get("empresa").toString() != null) ? solicitudExtincion.get("empresa").toString() : "";
	String pymeEmpresas = pyme+((empresas!="")?";"+empresas:"");
	datos = new HashMap();
	datos.put("NOMBRE_EPO",solicitudExtincion.get("nombreEpo"));
	datos.put("NOMBRE_PYME",pymeEmpresas.replaceAll(";","<br>") );
	datos.put("RFC",solicitudExtincion.get("rfcCendente"));
	datos.put("FECHA_SOLICITUD",solicitudExtincion.get("fechaSolicitud"));
	datos.put("NO_CONTRATO",solicitudExtincion.get("numeroContrato"));
	datos.put("MONTO_MONEDA",solicitudExtincion.get("montosPorMoneda"));
	registros.add(datos);	
	}
	consulta = "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("numeroRegistros", String.valueOf(numeroRegistros));
	infoRegresar = jsonObj.toString();

}else  if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setCs_cesion_derechos("S"); 
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoPYME")){

	//CatalogoPymeCesion cat = new CatalogoPymeCesion();
	CatalogoPymeExtincion cat = new CatalogoPymeExtincion();
	cat.setCampoClave("DISTINCT PYME.IC_PYME");
	cat.setCampoDescripcion("PYME.CG_RAZON_SOCIAL");
	cat.setClaveIf(iNoCliente);  
	cat.setOrden("PYME.CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();  

}else if(informacion.equals("CatalogoContratacion")  && !clave_epo.equals("") ){
	
	List contratacion=  cesionBean.obtenerComboTipoContratacion(clave_epo);
	registros = new JSONArray();
	registros = registros.fromObject(contratacion);
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();


}else if(informacion.equals("CatalogoMoneda") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setOrden("ic_moneda");
	cat.setValoresCondicionIn("0,1,54,25", Integer.class);
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("Consultar") ||  informacion.equals("ConsultarArchivoPDF")   ){
	
	HashMap parametrosConsulta = new HashMap();
	parametrosConsulta.put("loginUsuario", strLogin);
    parametrosConsulta.put("clavePyme", clave_pyme);
    parametrosConsulta.put("claveEpo", clave_epo);
    parametrosConsulta.put("claveIfCesionario", iNoCliente);
    parametrosConsulta.put("numeroContrato", numero_contrato);
    parametrosConsulta.put("claveMoneda", clave_moneda);
    parametrosConsulta.put("claveTipoContratacion", clave_contratacion);
	if("L".equals(tipoExtincion)){
    	parametrosConsulta.put("estatusSolicitud", "19");
	 } else{
		parametrosConsulta.put("estatusSolicitud", "17");
	}
    parametrosConsulta.put("perfilUsuario", "ADMIN IF");
    HashMap solicitudesAutorizadasPorUsuario = cesionBean.obtenerExtincionesPorUsuario(parametrosConsulta);
	int numSolAut = Integer.parseInt((String)solicitudesAutorizadasPorUsuario.get("numeroRegistros"));
	String repIF2 = cesionBean.getloginRepresentante2(iNoCliente);
	existeRepIf2  = cesionBean.getIfRepresentante2(iNoCliente); // Fodea 04-2015
	
	if (operacion.equals("Generar") || operacion.equals("ArchivoPDF")){
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	}
	if (operacion.equals("Generar")){
		try {
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		
			Registros reg = queryHelper.getPageResultSet(request,start,limit);
			while(reg.next()){
				//FODEA-024-2014 MOD()
				cedente=reg.getString("nombre_cedente");
				nombrePyme=reg.getString("nombre_cedente");
				empresas = ((reg.getString("empresas")!=null)?";"+reg.getString("empresas"):"");
				String pymeEmpresas = cedente +empresas;
				reg.setObject("nombre_cedente",pymeEmpresas.replaceAll(";","<br>"));
				dependencia=reg.getString("dependencia");
			
				//se obtiene nombre de Cesionario
				nombreIfCesionario = reg.getString("nombre_cesionario");
				//Se obtiene el Monto Moneda
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(reg.getString("clave_solicitud"));
				reg.setObject("MONTO_MONEDA",montosMonedaSol.toString());				
			
				//Se obtiene el representante legal de la Pyme
				String noProveedor = (reg.getString("clave_pyme")==null)?"":(reg.getString("clave_pyme"));
						
				StringBuffer nombreContacto = new StringBuffer();
				
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
				nombreContactoAviso = new StringBuffer();//FODEA-024-2014 MOD()
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
				for(int i=0;i<usuariosPorPerfil.size();i++){
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true; 
					if(i>0){
						nombreContacto.append(" / ");
						nombreContactoAviso.append(" / ");//FODEA-024-2014 MOD()
					}
					nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
					nombreContacto.append("\n");
					//FODEA-024-2014 MOD()
					nombreContactoAviso.append(usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getNombre()+" ");
					nombreContactoAviso.append("\n");
				}	
				reg.setObject("REPRESENTATE_LEGAL",nombreContacto.toString());							
				String  cc_acuse_ext_if=reg.getString("CC_ACUSE_EXT_IF")==null?"":reg.getString("CC_ACUSE_EXT_IF");
				String cc_acuse_ext_if2=reg.getString("CC_ACUSE_EXT_IF2")==null?"":reg.getString("CC_ACUSE_EXT_IF2");
				
				// para  las condiciones de Columna Accion
				String claveEstatus = reg.getString("clave_estatus");
				String recordFound = "S";
				if("L".equals(tipoExtincion)){
					
					if(existeRepIf2.equals("S")){
						if (claveEstatus.equals("19")){
					
							if (  !cc_acuse_ext_if.equals("") && !repIF2.equals("")  &&  !repIF2.equals(strLogin)){
						recordFound = "N";
					}else if (  !cc_acuse_ext_if2.equals("") && !repIF2.equals("")  &&  repIF2.equals(strLogin)  ) {
						recordFound = "N";
					}
					for (int i = 0; i < numSolAut; i++) {
						if (reg.getString("clave_solicitud").equals(solicitudesAutorizadasPorUsuario.get("numeroSolicitud" + i).toString())) {
							recordFound = "N";
                  }
               }

               if (recordFound.equals("S")) {
						reg.setObject("COLUMNA_ACCION","S");	
					} else {
						reg.setObject("COLUMNA_ACCION","N");
					}
				} else {
					reg.setObject("COLUMNA_ACCION","N");
				}			
					} else{
						if (claveEstatus.equals("19")) {
							if (!cc_acuse_ext_if.equals("")) {
								recordFound = "N";
							}
							for (int i = 0; i < numSolAut; i++) {
								if (reg.getString("clave_solicitud").equals(solicitudesAutorizadasPorUsuario.get("numeroSolicitud" + i).toString())) {
									recordFound = "N";
								}
							}
							if (recordFound.equals("S")) {
								reg.setObject("COLUMNA_ACCION","S");
							} else {
								reg.setObject("COLUMNA_ACCION","N");
							}
						} else {
							reg.setObject("COLUMNA_ACCION","N");
						}
					}

				} else if("C".equals(tipoExtincion)){

					if(existeRepIf2.equals("S")){
						if (!cc_acuse_ext_if.equals("") && !repIF2.equals("") && !repIF2.equals(strLogin)){
							recordFound = "N";
						} else if (!cc_acuse_ext_if2.equals("") && !repIF2.equals("") && repIF2.equals(strLogin)){
							recordFound = "N";
						}
						for (int i = 0; i < numSolAut; i++){
							if (reg.getString("clave_solicitud").equals(solicitudesAutorizadasPorUsuario.get("numeroSolicitud" + i).toString())){
								recordFound = "N";
							}
						}

						if (recordFound.equals("S")){
							reg.setObject("COLUMNA_ACCION","S");
						} else{
							reg.setObject("COLUMNA_ACCION","N");
						}
					} else{
						if (!cc_acuse_ext_if.equals("")){
							recordFound = "N";
						}
						for (int i = 0; i < numSolAut; i++){
							if (reg.getString("clave_solicitud").equals(solicitudesAutorizadasPorUsuario.get("numeroSolicitud" + i).toString())){
								recordFound = "N";
							}
						}

						if (recordFound.equals("S")){
							reg.setObject("COLUMNA_ACCION","S");
						} else{
							reg.setObject("COLUMNA_ACCION","N");
						}
					}

				}

			}//while(reg.next()){
			consulta = 	"{\"success\": true, \"total\": \""+ queryHelper.getIdsSize() +"\", \"registros\": " + reg.getJSONData()+"}";
		
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		String [] cedente1= cedente.replaceAll("<br>",",").replaceAll(",","").split(";");
		
		if(cedente1.length==1 ){
			cedente=cedente.replaceAll("<br>",";");
		}else {
			cedente=cedente.replaceAll("<br>",";");
		}
		//FODEA-024-2014 MOD()
		mensaje=	"<table  style='text-align: justify;' border='1' width='700' cellpadding='0' cellspacing='0'>"+
				"<tr>"+
				"<td class='formas' align='left'>"+
				"De conformidad con lo señalado en el (los) Contrato(s) cuyos datos se señalan en esta pantalla, "+
				nombreContactoAviso.toString() +" ,como representante(s) legal(s) de "+nombrePyme+" y empresa(s) representada(s) "+
				"solicito(tamos) a "+dependencia+" ,su consentimiento para ceder el 100% de los derechos de cobro"+
				" que lleguen a derivarse por la ejecución y cumplimiento del(los) referido(s) Contrato(s) y de los convenios que se lleguen a formalizar, a favor del Intermediario Financiero seleccionado, en su carácter de cesionario.<br><br>"+
				"En virtud de lo anterior una vez que cuente mi representada con el consentimiento solicitado a través de éste medio, aceptará y reconocerá el contrato de Cesión de Derechos de Cobro correspondiente a través del sistema de Cadenas Productivas.<br><br>"+
				"Al mismo tiempo manifiesto(amos) que cuento(amos) con las facultades para actos de dominio necesarias para llevar a cabo la cesión de derechos de cobro conforme se acredita mediante los poderes anexos a la presente, mismos que no han sido revocados, modificados o limitados en forma alguna a la presente fecha.<br><br>"+
				"</td>"+
				"</tr>"+
				"</table>";
				
		/*mensaje=	"<table  style='text-align: justify;' border='1' width='700' cellpadding='0' cellspacing='0'>"+
				"<tr>"+
				"<td class='formas' align='left'>"+
				"De conformidad con lo señalado en el (los) Contrato(s) cuyos datos se señalan en esta pantalla, "+
				"yo como representante legal de la empresa, "+
				"solicito a "+nombreIfCesionario+" su validación/aceptación para extinguir el Contrato de Derechos de Cobro.<br/><br/>"+
				"Al mismo tiempo manifiesto (amos) que cuento (amos) con las facultades para actos de dominio necesarias para llevar a cabo la cesión de derechos de cobro conforme se acredita mediante los poderes anexos a la presente, mismos que no han sido revocados, modificados o limitados en forma alguna a la presente fecha."+
				"</td>"+
				"</tr>"+
				"</table>";*/
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("mensaje",              mensaje                                );
		jsonObj.put("clasificacionEpo",     clasificacionEpo                       );
		jsonObj.put("hayCamposAdicionales", String.valueOf(indiceCamposAdicionales));
		jsonObj.put("campo01",              campo01                                );
		jsonObj.put("campo02",              campo02                                );
		jsonObj.put("campo03",              campo03                                );
		jsonObj.put("campo04",              campo04                                );
		jsonObj.put("campo05",              campo05                                );
	
	} else if(operacion.equals("ArchivoPDF")){
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, tipoArchivo);			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		
	} else if (operacion.equals("ArchivoCSV")  ) {	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}				
	} 
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("CONTRATO")  ) {	
	try {
		String 			nombreArchivo = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);			
		String nombreArchivo2 = nombreArchivo.substring(nombreArchivo.lastIndexOf("on/")+3, nombreArchivo.length());	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
					
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo de Contrato", e);
	}	

}else if(informacion.equals("CONTRATO_CESION_PYME")  ){
   		
	List parametros = new ArrayList();
	parametros.add((String)session.getAttribute("strPais"));
	parametros.add(iNoCliente);
	parametros.add((String)session.getAttribute("sesExterno"));
	parametros.add((String) session.getAttribute("strNombre"));
	parametros.add((String) session.getAttribute("strNombreUsuario"));
	parametros.add((String)session.getAttribute("strLogo"));
	parametros.add(strDirectorioTemp);
	parametros.add((String) application.getAttribute("strDirectorioPublicacion"));
	parametros.add(tipoArchivo);
	parametros.add(clave_solicitud);
	parametros.add(clave_epo);
	parametros.add(iNoCliente);
		
	String nombreArchivo = cesionBean.DescargaContratoCesionPyme(parametros);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();  

 }else if( informacion.equals("ConsultarPreAcuse")){
 
	try {
		Registros reg	=	queryHelper.doSearch();
		while(reg.next()){
			//FODEA-024-2014 MOD()
				cedente=reg.getString("nombre_cedente");
				empresas = ((reg.getString("empresas")!=null)?reg.getString("empresas"):"");
				cedente = cedente+((empresas!="")?";"+empresas:"");
				reg.setObject("nombre_cedente",cedente.replaceAll(";","<br>"));
				dependencia=reg.getString("dependencia");
			//se obtiene nombre de Cesionario
			nombreIfCesionario = reg.getString("nombre_cesionario");
			//Se obtiene el Monto Moneda
			StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(reg.getString("clave_solicitud"));
			reg.setObject("MONTO_MONEDA",montosMonedaSol.toString());				
			//Se obtiene el representante legal de la Pyme
			String noProveedor = (reg.getString("clave_pyme")==null)?"":(reg.getString("clave_pyme"));
						
			StringBuffer nombreContacto = new StringBuffer();
			UtilUsr utilUsr = new UtilUsr();
			boolean usuarioEncontrado = false;
			boolean perfilIndicado = false;
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
			for(int i=0;i<usuariosPorPerfil.size();i++){
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
				Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
				perfilIndicado = true;
				if(i>0){
					nombreContacto.append(" / ");
				}
				nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
				nombreContacto.append("\n");
			}	
			reg.setObject("REPRESENTATE_LEGAL",nombreContacto.toString());							
				
		}//while(reg.next()){
		consulta = 	"{\"success\": true, \"total\": \""+ reg.getNumeroRegistros() +"\", \"registros\": " + reg.getJSONData()+"}";
			
	}	catch(Exception e) {
		throw new AppException("Error en la consulta del PreAcuse y Acuse  ", e);
	}	
		mensaje=	"<table  style='text-align: justify;' border='1' width='700' cellpadding='0' cellspacing='0'>"+
				"<tr>"+
				"<td class='formas' align='left'>"+
				"Con base en lo dispuesto en el Código de Comercio, por este conducto me permito notificar a esa Empresa, por conducto de esa Gerencia Jurídica, el Convenio de Extinción del Contrato de Cesión Electrónica de Derechos de Cobro respecto del Contrato referido en el apartado inmediato anterior que hemos formalizado con la Empresa Cedente.<br><br>"+
				"Por lo anterior, me permito solicitar a usted una vez que surta efectos la extinción de derechos de cobro, gire sus instrucciones a fin de que se deje sin efectos la Cesión Electrónica de Derechos de Cobro de referencia y la Empresa realice el pago de las facturas derivadas de la ejecución y cumplimiento del Contrato referido a la Empresa Cedente en la cuenta señalada originalmente en dicho Contrato.<br><br>"+
				"</td>"+
				"</tr>"+
				"</table>";
/*    
		mensaje=	"<table  style='text-align: justify;' border='1' width='700' cellpadding='0' cellspacing='0'>"+
				"<tr>"+
				"<td class='formas' align='left'>"+
				"Con base a lo dispuesto en el Código de Comercio por este conducto me permito notificar a esa Entidad "+
				"la Extinción del Contrato de Cesión de Derechos de Cobro que fue formalizado con la(s) empresa(s) "+
				"CEDENTE respecto al contrato que aquí mismo se detalla.<br><br>"+
				"Por lo anterior, me permito solicitar a usted, se registre la Extinción del Contrato de Cesión de "+
				"Derechos de Cobro de referencia a efecto de que esa Empresa, realice el(los) pago(s) derivado(s) del "+
				"Contrato aquí señalado a favor del CEDENTE.<br><br>"+
				"Los datos de la Cuenta bancaria del CEDENTE en la que se deberán depositar los pagos derivados de "+
				"esta operación son los que aquí se detallan.<br><br>"+
				"Sobre el particular, por este medio les notifico, la Extinción del Contrato de Cesión de Derechos "+
				"de Cobro antes mencionado, para todos los efectos legales a que haya lugar."+
				"</td>"+
				"</tr>"+
				"</table>";
*/
/*	
	mensaje=	"<table  style='text-align: justify;' border='1' width='700' cellpadding='0' cellspacing='0'>"+
				"<tr>"+
				"<td class='formas' align='left'>"+
				"Con base a lo dispuesto en el Código de Comercio por este conducto me permito notificar a esa Entidad la Extinción del Contrato de Cesión de Derechos"+
				"de Cobro que fue formalizado con la empresa CEDENTE respecto al contrato que aquí mismo se detalla.<br/><br/>"+
				"Por lo anterior, me permito solicitar a usted, se registre la Extinción del Contrato de Cesión de Derechos de Cobro de referencia a efecto de que esa "+
				"Entidad, realice el(los) pago(s) derivados del Contrato aquí señalado a favor del PROVEEDOR.<br/><br/>"+
				"Los datos de la Cuenta bancaria de la Pyme en la que se deberán depositar los pagos derivados de esta operación son los que aquí se detallan.<br/><br/>"+
				"Sobre el particular, por este medio les notifico, la Extinción del Contrato de Cesión de Derechos de Cobro antes mencionado, para todos los efectos "+
				"legales a que haya lugar.<br/>"+
				"</td>"+
				"</tr>"+
				"</table>";
*/		
	StringBuffer textoFirmado = new StringBuffer();
	textoFirmado.append("Dependencia|Pyme (Cedente)|RFC|Representante Legal|No. Contrato|Monto / Moneda| Tipo de Contratación|"+
							"Fecha Inicio Contrato|Fecha Final Contrato|Plazo del Contrato|");
	if (clasificacionEpo != null && !clasificacionEpo.equals("")) {  textoFirmado.append(clasificacionEpo+"|"); 	}
	if (campo01 != null && !campo01.equals("")) {  textoFirmado.append(campo01+"|"); 	}
	if (campo02 != null && !campo02.equals("")) {  textoFirmado.append(campo02+"|"); 	}
	if (campo03 != null && !campo03.equals("")) {  textoFirmado.append(campo03+"|"); 	}
	if (campo04 != null && !campo04.equals("")) {  textoFirmado.append(campo04+"|"); 	}
	if (campo05 != null && !campo05.equals("")) {  textoFirmado.append(campo05+"|"); 	}
	textoFirmado.append("Objeto del Contrato|Fecha de Extinción del Contrato|Cuenta|Cuenta CLABE|"+
								"Banco de Depósito|Estatus|");
	
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("mensaje", mensaje);	
	jsonObj.put("clasificacionEpo", clasificacionEpo);	
	jsonObj.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
	jsonObj.put("campo01",campo01);
	jsonObj.put("campo02",campo02);
	jsonObj.put("campo03",campo03);
	jsonObj.put("campo04",campo04);
	jsonObj.put("campo05",campo05);	
	jsonObj.put("operacion",operacion);
	jsonObj.put("clave_solicitud",clave_solicitud); 
	jsonObj.put("textoFirmar",textoFirmado.toString());			
	jsonObj.put("operacionAcuse", operacionAcuse);	
	infoRegresar = jsonObj.toString();  
	
 }else if( informacion.equals("GenerarAcuse")){ 
	%><%@ include file="../certificado.jspf" %><%

	String textoFirmar            = (request.getParameter("textoFirmar")           !=null)?request.getParameter("textoFirmar"):"";
	String pkcs7                  = (request.getParameter("pkcs7")                 !=null)?request.getParameter("pkcs7"):"";
	String causasRechazoExtincion = (request.getParameter("causasRechazoExtincion")!=null)?request.getParameter("causasRechazoExtincion"):"";
	String fechaExtincion         = (request.getParameter("fechaExtincion")        !=null)?request.getParameter("fechaExtincion"):"";
	String numeroCuenta           = (request.getParameter("numeroCuenta")          !=null)?request.getParameter("numeroCuenta"):"";
	String cuentaClabe            = (request.getParameter("cuentaClabe")           !=null)?request.getParameter("cuentaClabe"):"";
	String bancoDeposito          = (request.getParameter("bancoDeposito")         !=null)?request.getParameter("bancoDeposito"):"";
	
	String folioCert      = new SimpleDateFormat("ddMMyy").format(new java.util.Date());	
	fechaCarga            = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
	horaCarga             = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
	String login_usuario  = iNoUsuario;
	String nombre_usuario = strNombreUsuario;
	String mensajeAuto    = "";

	tipoExtincion = cesionBean.getTipoExtincion(clave_epo); // Fodea 04-2015
	existeRepIf2  = cesionBean.getIfRepresentante2(iNoCliente); // Fodea 04-2015
	
	HashMap parametrosFirma = new HashMap();
	parametrosFirma.put("_serial",                _serial                );
	parametrosFirma.put("pkcs7",                  pkcs7                  );
	parametrosFirma.put("textoPlano",             textoFirmar            );
	parametrosFirma.put("validCert",              String.valueOf(false)  );
	parametrosFirma.put("folioCert",              folioCert              );
	parametrosFirma.put("loginUsuario",           login_usuario          );
	parametrosFirma.put("nombreUsuario",          nombre_usuario         );
	parametrosFirma.put("claveSolicitud",         clave_solicitud        );
	parametrosFirma.put("nuevoEstatus",           nuevoEstatus           );
	parametrosFirma.put("causasRechazoExtincion", causasRechazoExtincion );
	parametrosFirma.put("strDirectorioTemp",      strDirectorioTemp      );
	parametrosFirma.put("tipoExtincion",          tipoExtincion          );
	parametrosFirma.put("fechaExtincion",         fechaExtincion         );
	parametrosFirma.put("numeroCuenta",           numeroCuenta           );
	parametrosFirma.put("cuentaClabe",            cuentaClabe            );
	parametrosFirma.put("bancoDeposito",          bancoDeposito          );
	parametrosFirma.put("existeRepIf2",           existeRepIf2           );
	
	 acuse = cesionBean.acuseExtincionContratoIf(parametrosFirma);

	if( acuse.equals("")){
		mensajeAuto="<b>La autentificación no se llevó a cabo.<br/>PROCESO CANCELADO</b>";
	}
	
	jsonObj.put("success",         new Boolean(true)              );
	jsonObj.put("fechaCarga",      fechaCarga                     );
	jsonObj.put("horaCarga",       horaCarga                      );
	jsonObj.put("acuse",           acuse                          );
	jsonObj.put("usuario",         iNoUsuario+" "+nombre_usuario  );
	jsonObj.put("clave_solicitud", clave_solicitud                );
	jsonObj.put("operacion",       operacion                      );
	jsonObj.put("nuevoEstatus",    nuevoEstatus                   );
	jsonObj.put("mensajeAuto",     mensajeAuto                    );
	jsonObj.put("clave_epo",       clave_epo                      );
	infoRegresar = jsonObj.toString();  
		
 }else if( informacion.equals("ConsultarAcusePDF")){

	if (operacion.equals("ArchivoPDF")  ) {	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}				
	}
	infoRegresar = jsonObj.toString();  
	
}
	

%>
<%=infoRegresar%>