<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, 
		java.text.SimpleDateFormat,
		netropology.utilerias.*,				
		java.math.*,
		com.netro.pdf.*,
		java.io.File,
		com.netro.model.catalogos.*,
		netropology.utilerias.usuarios.*, 
		com.netro.cesion.*, 
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/34cesion/34secsession_ext.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clave_pyme = (request.getParameter("clave_pyme")!=null)?request.getParameter("clave_pyme"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String clave_moneda = (request.getParameter("clave_moneda")!=null)?request.getParameter("clave_moneda"):"";
String clave_estatus_sol = (request.getParameter("clave_estatus_sol")!=null)?request.getParameter("clave_estatus_sol"):"";
String clave_contratacion = (request.getParameter("clave_contratacion")!=null)?request.getParameter("clave_contratacion"):"";
String fecha_vigencia_ini = (request.getParameter("fecha_vigencia_ini")!=null)?request.getParameter("fecha_vigencia_ini"):"";
String fecha_vigencia_fin = (request.getParameter("fecha_vigencia_fin")!=null)?request.getParameter("fecha_vigencia_fin"):"";
String plazoContrato = (request.getParameter("plazoContrato")!=null)?request.getParameter("plazoContrato"):"";
String claveTipoPlazo = (request.getParameter("claveTipoPlazo")!=null)?request.getParameter("claveTipoPlazo"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
String monto_credito = (request.getParameter("monto_credito")!=null)?request.getParameter("monto_credito"):"";
String referencia = (request.getParameter("referencia")!=null)?request.getParameter("referencia"):"";
String fecha_vencimiento = (request.getParameter("fecha_vencimiento")!=null)?request.getParameter("fecha_vencimiento"):"";
String banco_deposito = (request.getParameter("banco_deposito")!=null)?request.getParameter("banco_deposito"):"";
String numero_cuenta = (request.getParameter("numero_cuenta")!=null)?request.getParameter("numero_cuenta"):"";
String clabe = (request.getParameter("clabe")!=null)?request.getParameter("clabe"):"";
String tipoOperacion = (request.getParameter("tipoOperacion")!=null)?request.getParameter("tipoOperacion"):"";
String tipo_archivo = (request.getParameter("tipo_archivo")!=null)?request.getParameter("tipo_archivo"):"";
String recibo_carga = (request.getParameter("recibo_carga")!=null)?request.getParameter("recibo_carga"):"";
String hora_carga = (request.getParameter("hora_carga")!=null)?request.getParameter("hora_carga"):"";
String fecha_carga = (request.getParameter("fecha_carga")!=null)?request.getParameter("fecha_carga"):"";
String aut_if_operativo= (request.getParameter("aut_if_operativo")!=null)?request.getParameter("aut_if_operativo"):"";
String causas_retorno= (request.getParameter("causas_retorno")!=null)?request.getParameter("causas_retorno"):"";


StringBuffer texto_plano = new StringBuffer();
StringBuffer sellos = new StringBuffer();
String infoRegresar	=	"", consulta  ="", campo01  ="", campo02="", campo03 ="",  campo04 ="", campo05 ="",
		campo11 ="", campo22 ="", campo33 ="", campo44 ="", campo55 ="",  clasificacionEpo ="", clave_if ="",
		mensajeAuto ="";
			 
int indiceCamposAdicionales =0;	 

JSONObject jsonObj = new JSONObject();
HashMap hash = new HashMap();

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

if(!clave_epo.equals(""))  {
	clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
	Vector lovDatos = cesionBean.getCamposAdicionales(clave_epo, "5");	
	indiceCamposAdicionales = lovDatos.size();
	for(int i = 0; i < indiceCamposAdicionales; i++){
		List campos =(List)lovDatos.get(i);
		if(i==0) 	campo01=  campos.get(1).toString();
		if(i==1) 	campo02=  campos.get(1).toString();
		if(i==2) 	campo03=  campos.get(1).toString();
		if(i==3) 	campo04=  campos.get(1).toString();
		if(i==4) 	campo05=  campos.get(1).toString();			
	}	
}
	
if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setCs_cesion_derechos("S"); 
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoPYME")){

	CatalogoPymeCesion cat = new CatalogoPymeCesion();
	cat.setCampoClave("pyme.ic_pyme");
	cat.setCampoDescripcion("pyme.cg_razon_social");		
	/*if(strPerfil.equals("IF OPERACION"))
	{
		cat.setIfOperacion("S");
	}*/
	cat.setClaveIF(iNoCliente);  
	cat.setOrden("pyme.cg_razon_social");
	infoRegresar = cat.getJSONElementos();  

}else if(informacion.equals("CatalogoMoneda") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setOrden("ic_moneda");
	cat.setValoresCondicionIn("0,1,54,25", Integer.class);
	infoRegresar = cat.getJSONElementos();	
	
} else  if(informacion.equals("CatalogoEstatus")) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_solic");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setTabla("cdercat_estatus_solic");
	cat.setOrden("cg_descripcion");
	cat.setValoresCondicionIn(" 7, 9, 10 ,11, 12, 15, 16, 17, 25 ", Integer.class);
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoTipoPlazo") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("cdercat_tipo_plazo");
	cat.setCampoClave("ic_tipo_plazo");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("ic_tipo_plazo");
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoContratacion")  && !clave_epo.equals("") ){
	
	List contratacion=  cesionBean.obtenerComboTipoContratacion(clave_epo);
	JSONArray registros = new JSONArray();
	registros = registros.fromObject(contratacion);
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("Consultar") ){
	String  acc_representante2 = "N"; 
 String mensaje  = " <table  style='text-align: justify;' border='1' width='700' cellpadding='0' cellspacing='0'>  "+
						 " <tr> "+
						 " <td align='justify' class='formas'>"+
						 " Con base a lo dispuesto en el Código de Comercio por este conducto me permito notificar a esa Entidad el contrato de cesión de Derechos de Cobro que hemos formalizado con la empresa CEDENTE respecto al contrato que aquí mismo se detalla.<br/> " +
						 "	Por lo anterior, me permito solicitar a usted, se registre la Cesión de Derechos de Cobro de referencia a efecto de que esa Entidad, realice el(los) pago(s) derivados del Contrato aquí señalado a favor del INTERMEDIARIO FINANCIERO (CESIONARIO), en la inteligencia de que éste, está de acuerdo con lo establecido en el Convenio Marco de Cesión Electrónica de Derechos  de Cobro.<br/>"+
						 " Los datos de nuestra cuenta bancaria en la que se deben depositar los pagos derivados de esta operación son los que aquí se detallan.<br/>"+
						 "	Sobre el particular y de conformidad con lo señalado en la emisión de la conformidad para ceder los derechos de cobro emitida por esa Entidad, por este medio les notifico, la Cesión de Derechos de Cobro del Contrato antes mencionado, para todos los efectos legales a que haya lugar, adjunto, en forma electrónica.<br/> "+
						 "	</td> "+
						 " </tr> "+
						 " </table> ";

	ConsContratoCesionIF paginador = new ConsContratoCesionIF();
	paginador.setClave_pyme(clave_pyme);
	paginador.setClave_epo(clave_epo);
	paginador.setNumero_contrato(numero_contrato);
	paginador.setClave_moneda(clave_moneda) ;
	paginador.setClave_estatus_sol(clave_estatus_sol);
	paginador.setClave_contratacion(clave_contratacion);
	paginador.setFecha_vigencia_ini(fecha_vigencia_ini);
	paginador.setFecha_vigencia_fin(fecha_vigencia_fin);  
	paginador.setPlazoContrato(plazoContrato);
	paginador.setClaveTipoPlazo(claveTipoPlazo);
	paginador.setClave_if(iNoCliente);
	paginador.setClasificacionEpo(clasificacionEpo);
	paginador.setCampo1(campo01);
	paginador.setCampo2(campo02);
	paginador.setCampo3(campo03);
	paginador.setCampo4(campo04);
	paginador.setCampo5(campo05);
	paginador.setIndiceCamposAdicionales(String.valueOf(indiceCamposAdicionales));	
	paginador.setAutIfOperativo(aut_if_operativo);	

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if (operacion.equals("Generar")){
		try{
			Registros reg = queryHelper.doSearch();
			while(reg.next()){
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(reg.getString("CLAVE_SOLICITUD"));
				reg.setObject("MONTO_MONEDA",montosMonedaSol.toString());				
				//Se obtiene el representante legal de la Pyme
				String noProveedor = (reg.getString("CLAVE_PYME")==null)?"":(reg.getString("CLAVE_PYME"));
				//FODEA-024-2014 MOD()
				String pyme_cedente = (reg.getString("PYME_CEDENTE")==null)?"":(reg.getString("PYME_CEDENTE"));	
				String empresas = (reg.getString("EMPRESAS")==null)?"":(reg.getString("EMPRESAS"));	
				reg.setObject("PYME_CEDENTE",pyme_cedente+"<BR>"+empresas.replaceAll(";","<BR>"));					
					//
				StringBuffer nombreContacto = new StringBuffer();
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
				for(int i=0;i<usuariosPorPerfil.size();i++){
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true;
					if(i>0){
						nombreContacto.append(" / ");
					}
					nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
					nombreContacto.append("\n");
				}										
				reg.setObject("REPRESENTANTE",nombreContacto.toString());
				
				// Para los  campos editables 
				String ic_estatus = (reg.getString("IC_ESTATUS")==null)?"":(reg.getString("IC_ESTATUS"));
				String firmado_if = (reg.getString("FIRMADO_IF")==null)?"":(reg.getString("FIRMADO_IF"));
				String login_rep2 = (reg.getString("LOGIN_REPRESENTANTE2")==null)?"":(reg.getString("LOGIN_REPRESENTANTE2"));
				if(login_rep2.equals(strLogin))  { acc_representante2 ="S"; }
				
				if( (ic_estatus.equals("7") || ic_estatus.equals("12") ) &&  firmado_if.equals("N")&&aut_if_operativo.equals("N") ){					
					reg.setObject("MONTO_CREDITO","");
					reg.setObject("REFERENCIA_CREDITO","");
					reg.setObject("FECHA_VENCIMIENTO","");
					reg.setObject("BANCO_DEPOSITO","");
					reg.setObject("CUENTA","");
					//reg.setObject("CUENTA_CLABE","");						  
				}				
				
			}//while(reg.next()){ 
			consulta = 	"{\"success\": true, \"total\": \""+ reg.getNumeroRegistros() +"\", \"registros\": " + reg.getJSONData()+"}";
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("acc_representante2",acc_representante2);
			
		}catch(Exception e){
			throw new AppException("Error en la obtención de los datos",e);
		}
		
	} else if (operacion.equals("ArchivoPDF") ||  operacion.equals("ArchivoCSV") ) {
	
		String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}				
	}
	
	if (operacion.equals("Generar")){  
		jsonObj.put("mensaje", mensaje);	
		jsonObj.put("clasificacionEpo", clasificacionEpo);	
		jsonObj.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
		jsonObj.put("campo01",campo01);
		jsonObj.put("campo02",campo02);
		jsonObj.put("campo03",campo03);
		jsonObj.put("campo04",campo04);
		jsonObj.put("campo05",campo05);	
	}
	infoRegresar=jsonObj.toString();	
	
}else if(informacion.equals("CONTRATO") ){
	try {
		String 			nombreArchivo = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);			
		//String nombreArchivo2 = nombreArchivo.substring(nombreArchivo.lastIndexOf("on/")+3, nombreArchivo.length());	
		jsonObj.put("success", new Boolean(true));
		//jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo2);
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
					
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo de Contrato", e);
	}		

 }else if(informacion.equals("CONTRATO_CESION_IF")  ){
   		
	HashMap parametros = new HashMap();
	parametros.put("clave_solicitud", clave_solicitud);	
	parametros.put("tipo_archivo", tipo_archivo);
	parametros.put("strDirectorioTemp", strDirectorioTemp);
	parametros.put("pais", (String)session.getAttribute("strPais"));
	parametros.put("noCliente", iNoCliente);
	parametros.put("nombre", (String)session.getAttribute("strNombre"));
	parametros.put("nombreUsr", (String)session.getAttribute("strNombreUsuario"));
	parametros.put("logo", (String)session.getAttribute("strLogo"));
	parametros.put("strDirectorioPublicacion", strDirectorioPublicacion);
	
  String nombreArchivo =cesionBean.archConCesionIF (parametros);
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();  
  
}else if(informacion.equals("Autorizar")   ||   informacion.equals("Rechazar")   ){

	String campo_01  ="", campo_02  ="", campo_03 ="",  campo_04 ="",  campo_05 ="", firma1 ="", firma2="", firma3 ="";
			
	//esta es la consulta para mostrar en PreAcuse
	HashMap consultaContratoCesion = cesionBean.consultaContratoCesionIf(clave_solicitud);
	
	clave_epo = (String)consultaContratoCesion.get("clave_epo");
	clave_if = (String)consultaContratoCesion.get("clave_if");
	HashMap campos_adicionales = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "5");
	StringBuffer clausulado_parametrizado = cesionBean.consultaClausuladoParametrizado(clave_if);
	int dias_notificacion = cesionBean.getDiasMinimosNotificacion(clave_epo);
	clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
	
	HashMap info_credito = new HashMap();
	info_credito.put("monto_credito", monto_credito);
	info_credito.put("referencia", referencia);
	info_credito.put("fecha_vencimiento",fecha_vencimiento);
	info_credito.put("banco_deposito", banco_deposito);
	info_credito.put("numero_cuenta", numero_cuenta);
	info_credito.put("clabe", clabe);
	
	consultaContratoCesion.put("campos_adicionales", campos_adicionales);
	consultaContratoCesion.put("clausulado_parametrizado", clausulado_parametrizado);
	consultaContratoCesion.put("dias_notificacion", Integer.toString(dias_notificacion));
	consultaContratoCesion.put("info_credito", info_credito);
	consultaContratoCesion.put("clasificacionEpo", clasificacionEpo);
	
	String sello_digital = cesionBean.generaSelloDigital(clave_solicitud, strNombreUsuario );
			
	String lblFechaVigencia=	"N/A".equals(consultaContratoCesion.get("fecha_inicio_contrato"))?"N/A":(consultaContratoCesion.get("fecha_inicio_contrato") +"  a " + consultaContratoCesion.get("fecha_fin_contrato"));
		
	if(!campo01.equals(""))  {   campo11= consultaContratoCesion.get("campo_adicional_1").toString().toUpperCase();  }
	if(!campo02.equals(""))  {   campo22= consultaContratoCesion.get("campo_adicional_2").toString().toUpperCase();  }
	if(!campo03.equals(""))  {   campo33= consultaContratoCesion.get("campo_adicional_3").toString().toUpperCase();  }
	if(!campo04.equals(""))  {   campo44= consultaContratoCesion.get("campo_adicional_4").toString().toUpperCase();  }
	if(!campo05.equals(""))  {   campo55= consultaContratoCesion.get("campo_adicional_5").toString().toUpperCase();  }
	
			
	boolean banderaArchivo1=false,banderaArchivo2=false;
	CreaArchivo archivo 				= null;
	archivo  = new CreaArchivo();
	String nombreArchivo = archivo.nombreArchivo()+".pdf";
	String nombreArchivo2;
	String nombreArchivo3=archivo.nombreArchivo()+".pdf";
	ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, false);
	HashMap contratoCesion = cesionBean.consultaContratoCesionPyme(clave_solicitud);
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
			
	pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
	
		float anchoCelda4[] = {100f};
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		//====================================================================>> TESTIGO 1 IF
	if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")&&false) { //Se pone false por que no van las notificaciones se dejan por si en un futuro se necesitan
		pdfDoc.setTable(1, 70, anchoCelda4);
		//pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
		pdfDoc.setCell("NOTIFICACION " , "contrato", ComunesPDF.CENTER, 1, 1, 0);		
		pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "contrato", ComunesPDF.LEFT, 1, 1, 0);	 //nombre de la EPo
		pdfDoc.setCell("Con base en lo dispuesto  en el Código de Comercio, por este conducto me permito notificar a la Entidad, por conducto de esa Unidad Administrativa adscrita a la Dirección Jurídica de Petróleos Mexicanos, el Contrato de Cesión Electrónica de Derechos de Cobro, que mediante el uso de firmas electrónicas hemos formalizado con la Empresa Cedente, respecto del Contrato referido en el apartado siguiente", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);    
		pdfDoc.setCell("Por lo anterior, me permito solicitar a usted gire sus instrucciones a fin de que una vez que surta efectos la presente Cesión Electrónica de Derechos de Cobro, se registre la misma a efecto de que "+contratoCesion.get("nombre_epo")+", realice el pago de las facturas derivadas del cumplimiento y ejecución del Contrato referido en el apartado inmediato anterior a la cuenta del suscrito INTERMEDIARIO FINANCIERO.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);   
		pdfDoc.setCell("Los datos de la cuenta en la que se deben depositar los pagos derivados de esta operación se detallan en el apartado siguiente como Nombre del INTERMEDIARIO FINANCIERO, Banco de Depósito,  Número de Cuenta para Depósito y Número de Cuenta CLABE.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);   
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	//====================================================================>> TESTIGO 1 IF
	if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")&&false) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")&&false) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);					
		pdfDoc.setCell((String)contratoCesion.get("firmaTest1"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	//====================================================================>> TESTIGO 2 IF
	if (contratoCesion.get("claveUsrTest2") != null && !contratoCesion.get("claveUsrTest2").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	
	pdfDoc.endDocument();
	
	if (cesionBean.getBanderaContrato(clave_solicitud).equals("S")){
		nombreArchivo2 = cesionBean.descargaContratoCesionNuevo(clave_solicitud,strDirectorioTemp);
	}else{
		nombreArchivo2 = cesionBean.generarContrato(clave_solicitud,strDirectorioTemp);
	}
	
	 ComunesPDF pdfDoc2 = new ComunesPDF(2, strDirectorioTemp + nombreArchivo3, "", false, false, false);
	 if (contratoCesion.get("firmaIf") != null && !contratoCesion.get("firmaIf").toString().equals("")) {
		pdfDoc2.setTable(1,70);
		pdfDoc2.setCell("Enteradas del contenido y alcance del presente contrato, las partes lo firman de conformidad, en la Ciudad de México, Distrito Federal, a los "+contratoCesion.get("char_fecha_if"),"contrato",ComunesPDF.JUSTIFIED,1,1,0);
		pdfDoc2.addTable();
	 }
	
	 pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
	
	String claveGrupo =(String)contratoCesion.get("ic_grupo_cesion");	
	UtilUsr utilUsr = new UtilUsr();
	if(!claveGrupo.equals("")){		
		List  lstEmpresasXGrupo = cesionBean.getEmpresasXGrupoCder(claveGrupo);
		if(lstEmpresasXGrupo.size()>0)  {
			for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
				String cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
				String nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
					
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(cvePymeGrupo, "P");
					
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {						
					String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
					Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
					String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
					HashMap datosRep= cesionBean.getRepresentantesCEDENTE( clave_solicitud, cvePymeGrupo, loginUsrPyme );
					if(datosRep.size()>0) {						
							if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
							
								pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
								pdfDoc2.addText("Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePymeGrupo, "formas", ComunesPDF.CENTER);
							pdfDoc2.setTable(1, 40, anchoCelda4);
								pdfDoc2.setCell(datosRep.get("CG_FIRMA_REPRESENTANTE").toString(), "formas", ComunesPDF.CENTER, 1);
							pdfDoc2.addTable();
							banderaArchivo2=true;
						}
					}
				}				
			}
		}	
			//Fodea 023-2015  Cuando la solicitud tiene un grupo de pymes (S)
	}else  {
		clave_pyme = (String)contratoCesion.get("clave_pyme");
		String  nombrePyme=(String)contratoCesion.get("nombre_pyme");					
					
		List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");					
					
		for (int i = 0; i < usuariosPorPerfil.size(); i++) {						
			String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
			Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
			String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
			HashMap datosRep= cesionBean.getRepresentantesCEDENTE( clave_solicitud, clave_pyme, loginUsrPyme );
			if(datosRep.size()>0) {	
				if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
						pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
						pdfDoc2.addText("Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePyme, "formas", ComunesPDF.CENTER);
					pdfDoc2.setTable(1, 40, anchoCelda4);
						pdfDoc2.setCell(datosRep.get("CG_FIRMA_REPRESENTANTE").toString(), "formas", ComunesPDF.CENTER, 1);
					pdfDoc2.addTable();
					banderaArchivo2=true;							
								
				}
			}
			}					
		} 
			
	
	//====================================================================>> REPRESENTANTE LEGAL IF
	if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
		pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc2.setTable(1, 40, anchoCelda4);
		pdfDoc2.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc2.addTable();
		banderaArchivo2=true;
	}
	
	//====================================================================>> REPRESENTANTE LEGAL IF 2
	if (contratoCesion.get("claveUsrIfRep2") != null && !contratoCesion.get("claveUsrIfRep2").toString().equals("")) {
		pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIfRep2") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc2.setTable(1, 40, anchoCelda4);
		pdfDoc2.setCell((String)contratoCesion.get("firmaIfRep2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc2.addTable();
		banderaArchivo2=true;
	}

	pdfDoc2.endDocument();
	
	ArrayList listaA = new ArrayList();
	
	if(banderaArchivo1){listaA.add(strDirectorioTemp+nombreArchivo);}
	listaA.add(strDirectorioTemp+nombreArchivo2);
	if(banderaArchivo2){listaA.add(strDirectorioTemp+nombreArchivo3);}
	String nombreArchivoNevo = archivo.nombreArchivo()+".pdf";
	listaA.add(strDirectorioTemp+nombreArchivoNevo);
	String args[]= new String[listaA.size()];
	for (int i=0; i<listaA.size(); i++)
		{
			args[i]=(String)listaA.get(i);
		}
	//String args[] =listaA.toArray();//  {strDirectorioTemp+nombreArchivo,strDirectorioTemp+nombreArchivo2,strDirectorioTemp+nombreArchivo3,strDirectorioTemp+nombreArchivoNevo};
	
	if(banderaArchivo1||banderaArchivo2){
		nombreArchivo=nombreArchivoNevo;
		ComunesPDF.doMerge(args);
		
	}else{
		nombreArchivo=nombreArchivo2;
	}
	
	sellos.append("<table align='center' width='750' border='0' cellspacing='1' cellpadding='1'>"+
	"<embed src='"+strDirecVirtualTemp+nombreArchivo+"' width='770' height='500'></embed>"+
	"</table>");
	
	
	if (consultaContratoCesion.get("claveUsrAutRep1") != null && !consultaContratoCesion.get("claveUsrAutRep1").toString().equals("")) {
		firma1= "Sello Digital de "+consultaContratoCesion.get("nombreUsrAutRep1") + " representante legal de " +consultaContratoCesion.get("nombre_pyme") +
				  "\n"+consultaContratoCesion.get("firmaRep1");
	}
	if (consultaContratoCesion.get("claveUsrAutRep2") != null && !consultaContratoCesion.get("claveUsrAutRep2").toString().equals("")) {
		firma2 ="Sello Digital de "+consultaContratoCesion.get("nombreUsrAutRep2") + " representante legal de " +consultaContratoCesion.get("nombre_pyme")+
					"\n"+consultaContratoCesion.get("firmaRep2");
	}
	firma3 ="Sello Digital de " + strNombreUsuario + " representante legal de " +consultaContratoCesion.get("nombre_if");

	
		
		campo_01  =
		"	{ "+
		"	xtype: 'displayfield', "+
		"	id: 'lblCampo1', "+	
		"	style: 'text-align:left;',"+
		"	fieldLabel: '"+campo01+"',"+
		"	value: '"+campo11+"'"+
		"} ";
		
		campo_02  =
		"	{ "+
		"	xtype: 'displayfield', "+
		"	id: 'lblCampo2', "+	
		"	style: 'text-align:left;',"+
		"	fieldLabel: '"+campo02+"',"+
		"	value: '"+campo22+"'"+
		"} ";
		
		campo_03  =
		"	{ "+
		"	xtype: 'displayfield', "+
		"	id: 'lblCampo3', "+	
		"	style: 'text-align:left;',"+
		"	fieldLabel: '"+campo03+"',"+
		"	value: '"+campo33+"'"+
		"} ";
		
		campo_04  =
		"	{ "+
		"	xtype: 'displayfield', "+
		"	id: 'lblCampo4', "+	
		"	style: 'text-align:left;',"+
		"	fieldLabel: '"+campo04+"',"+
		"	value: '"+campo44+"'"+
		"} ";
		
		campo_05  =
		"	{ "+
		"	xtype: 'displayfield', "+
		"	id: 'lblCampo5', "+	
		"	style: 'text-align:left;',"+
		"	fieldLabel: '"+campo05+"',"+
		"	value: '"+campo55+"'"+
		"} ";
						
	jsonObj.put("success", new Boolean(true));		
	jsonObj.put("campo_01", campo_01);
	jsonObj.put("campo_02", campo_02);
	jsonObj.put("campo_03", campo_03);
	jsonObj.put("campo_04", campo_04);
	jsonObj.put("campo_05", campo_05);
	jsonObj.put("firma1", firma1);
	jsonObj.put("firma2", firma2);
	jsonObj.put("firma3", firma3);	
	jsonObj.put("lblNombrePyme", consultaContratoCesion.get("nombre_pyme"));
	jsonObj.put("lblRFC", consultaContratoCesion.get("rfcPyme"));
	jsonObj.put("lblRepresentante1", consultaContratoCesion.get("nombreUsrAutRep1")==null?"":consultaContratoCesion.get("nombreUsrAutRep1"));
	jsonObj.put("lblRepresentante2", consultaContratoCesion.get("nombreUsrAutRep2")==null?"":consultaContratoCesion.get("nombreUsrAutRep2"));
	jsonObj.put("lblNumProveedor", consultaContratoCesion.get("numeroProveedor"));	
	jsonObj.put("lblNumContrato", consultaContratoCesion.get("numero_contrato"));
	jsonObj.put("lblMonto_Moneda", consultaContratoCesion.get("monto_moneda"));
	jsonObj.put("lblFechaVigencia", lblFechaVigencia);
	jsonObj.put("lblPlazoContrato", consultaContratoCesion.get("plazo_contrato"));
	jsonObj.put("lblTipoContra", consultaContratoCesion.get("tipo_contratacion").toString().toUpperCase());
	jsonObj.put("lblObjetoContrato", consultaContratoCesion.get("objeto_contrato").toString().toUpperCase());
	jsonObj.put("lblNombreEmpresa", consultaContratoCesion.get("nombre_epo"));
	jsonObj.put("lblCesionarioIF", consultaContratoCesion.get("nombre_if"));
	jsonObj.put("lblBancoDeposito", banco_deposito);
	jsonObj.put("lblCuenta", numero_cuenta);
	jsonObj.put("lblCuentaClabe", clabe);
	jsonObj.put("lblFechaSolic", consultaContratoCesion.get("fechaSolicitudConsentimiento"));
	jsonObj.put("lblFechaConsent", consultaContratoCesion.get("fechaAceptacionEpo"));
	jsonObj.put("lblPersonaConsent", consultaContratoCesion.get("nombreUsrEpoAutCesion")==null?"":consultaContratoCesion.get("nombreUsrEpoAutCesion"));
	jsonObj.put("lblFechaFormali", consultaContratoCesion.get("fechaFormalContrato"));
	jsonObj.put("lblFirmaPyme1", consultaContratoCesion.get("nombreUsrAutRep1")==null?"":consultaContratoCesion.get("nombreUsrAutRep1"));
	jsonObj.put("lblFirmaPyme2", consultaContratoCesion.get("nombreUsrAutRep2")==null?"":consultaContratoCesion.get("nombreUsrAutRep2"));
	jsonObj.put("lblFirmaCesionarioIF", consultaContratoCesion.get("nombreUsrIf")==null?(tipoOperacion.equals("ACEPTAR")?strNombreUsuario:""):consultaContratoCesion.get("nombreUsrIf"));
	jsonObj.put("lblFirmaTestigo1", consultaContratoCesion.get("nombreUsrTest1")==null?"":consultaContratoCesion.get("nombreUsrTest1"));
	jsonObj.put("lblFirmaTestigo2", consultaContratoCesion.get("nombreUsrTest2")==null?"":consultaContratoCesion.get("nombreUsrTest2"));
	jsonObj.put("lblFechaNotiEPO", consultaContratoCesion.get("fechaNotifEpo")==null?"":consultaContratoCesion.get("fechaNotifEpo"));
	jsonObj.put("lblPersonaEPO", consultaContratoCesion.get("nombreUsrNotifEpo")==null?"":consultaContratoCesion.get("nombreUsrNotifEpo"));
	jsonObj.put("lblFechaInfVenPag", consultaContratoCesion.get("fechaNotifVentanilla")==null?"":consultaContratoCesion.get("fechaNotifVentanilla"));
	jsonObj.put("lblFechaRecepVen", consultaContratoCesion.get("fechaNotifVentanilla")==null?"":consultaContratoCesion.get("fechaNotifVentanilla"));
	jsonObj.put("lblFechaAplicaRedirec", consultaContratoCesion.get("fechaRedirecccion")==null?"":consultaContratoCesion.get("fechaRedirecccion"));
	jsonObj.put("lblPerCuentaEPO", consultaContratoCesion.get("nombreUsrRedirec")==null?"":consultaContratoCesion.get("nombreUsrRedirec"));
	jsonObj.put("lblSellos", sellos.toString());
	jsonObj.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
	jsonObj.put("tipoOperacion", tipoOperacion); 
	jsonObj.put("texto_plano", texto_plano.toString());
	jsonObj.put("sello_digital", sello_digital.toString());	
	jsonObj.put("monto_credito", monto_credito);	
	jsonObj.put("referencia", referencia);	
	jsonObj.put("fecha_vencimiento", fecha_vencimiento);	  
	jsonObj.put("banco_deposito", banco_deposito);	
	jsonObj.put("numero_cuenta", numero_cuenta);	
	jsonObj.put("clabe", clabe);	
	jsonObj.put("tipoOperacion", tipoOperacion);
	jsonObj.put("clave_solicitud", clave_solicitud);
	jsonObj.put("clave_epo", clave_epo);
	jsonObj.put("clausulado_parametrizado", clausulado_parametrizado.toString());
		
  infoRegresar = jsonObj.toString();    
	  
 
 }else if(informacion.equals("GenerarAcuse")  ){
	%><%@ include file="../certificado.jspf" %><%
	String texto_plano2 = (request.getParameter("texto_plano")!=null)?request.getParameter("texto_plano"):"";
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
	String sello_digital = (request.getParameter("sello_digital")!=null)?request.getParameter("sello_digital"):"";
	String autRep2 = (request.getParameter("autRep2")!=null)?request.getParameter("autRep2"):"";
	String folioCert =new SimpleDateFormat("ddMMyy").format(new java.util.Date());	
	String fechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
	String horaCarga = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
	String login_usuario = iNoUsuario;
	String nombre_usuario = strNombreUsuario;
	String recibo ="", acuse  ="";
	HashMap parametros_firma = new HashMap();
	parametros_firma.put("_serial", _serial);
	parametros_firma.put("pkcs7", pkcs7);
	parametros_firma.put("texto_plano", texto_plano2);
	parametros_firma.put("validCert", String.valueOf(false));
	parametros_firma.put("folioCert", folioCert);
	parametros_firma.put("clave_solicitud", clave_solicitud);
	parametros_firma.put("sello_digital", sello_digital);
	parametros_firma.put("fecha_carga", fechaCarga);
	parametros_firma.put("hora_carga", horaCarga);
	parametros_firma.put("login_usuario", login_usuario);
	parametros_firma.put("nombre_usuario", nombre_usuario);
	parametros_firma.put("tipoOperacion", tipoOperacion);
	parametros_firma.put("monto_credito", monto_credito);
	parametros_firma.put("referencia", referencia);
	parametros_firma.put("fecha_vencimiento", fecha_vencimiento);
	parametros_firma.put("banco_deposito", banco_deposito);
	parametros_firma.put("numero_cuenta", numero_cuenta);
	parametros_firma.put("clabe", clabe);  
	
	try  {
	if(autRep2.equals("S")){
		recibo = cesionBean.acuseFirmaContratoCesionIFRep2(parametros_firma);
	}else{
		recibo = cesionBean.acuseFirmaContratoCesionIF(parametros_firma);
	}
	}catch(Exception e){
		recibo= "";
		throw new AppException("Error en la obtención de los datos", e);
	}
		         
	HashMap consultaContratoCesion = cesionBean.consultaContratoCesionIf(clave_solicitud);
	clave_epo = (String)consultaContratoCesion.get("clave_epo");
	clave_if = (String)consultaContratoCesion.get("clave_if");
	HashMap campos_adicionales = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "5");
	StringBuffer clausulado_parametrizado = cesionBean.consultaClausuladoParametrizado(clave_if);
		
		
	if(recibo != null && !recibo.equals("")){
		mensajeAuto="<b>La autentificación se llevó a cabo con éxito <br/> Recibo:"+recibo+"</b>";
	}else {
		mensajeAuto="<b>La autentificación no se llevó a cabo.<br/>PROCESO CANCELADO</b>";
	}
		
	boolean banderaArchivo1=false,banderaArchivo2=false;
	CreaArchivo archivo 				= null;
	archivo  = new CreaArchivo();
	String nombreArchivo = archivo.nombreArchivo()+".pdf";
	String nombreArchivo2;
	String nombreArchivo3=archivo.nombreArchivo()+".pdf";
	ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, false);
	HashMap contratoCesion = cesionBean.consultaContratoCesionPyme(clave_solicitud);
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
			
	pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
	
		float anchoCelda4[] = {100f};
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		//====================================================================>> TESTIGO 1 IF
	if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")&&false) { //Se pone false por que no van las notificaciones se dejan por si en un futuro se necesitan
		pdfDoc.setTable(1, 70, anchoCelda4);
		//pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
		pdfDoc.setCell("NOTIFICACION " , "contrato", ComunesPDF.CENTER, 1, 1, 0);		
		pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "contrato", ComunesPDF.LEFT, 1, 1, 0);	 //nombre de la EPo
		pdfDoc.setCell("Con base en lo dispuesto  en el Código de Comercio, por este conducto me permito notificar a la Entidad, por conducto de esa Unidad Administrativa adscrita a la Dirección Jurídica de Petróleos Mexicanos, el Contrato de Cesión Electrónica de Derechos de Cobro, que mediante el uso de firmas electrónicas hemos formalizado con la Empresa Cedente, respecto del Contrato referido en el apartado siguiente", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);    
		pdfDoc.setCell("Por lo anterior, me permito solicitar a usted gire sus instrucciones a fin de que una vez que surta efectos la presente Cesión Electrónica de Derechos de Cobro, se registre la misma a efecto de que "+contratoCesion.get("nombre_epo")+", realice el pago de las facturas derivadas del cumplimiento y ejecución del Contrato referido en el apartado inmediato anterior a la cuenta del suscrito INTERMEDIARIO FINANCIERO.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);   
		pdfDoc.setCell("Los datos de la cuenta en la que se deben depositar los pagos derivados de esta operación se detallan en el apartado siguiente como Nombre del INTERMEDIARIO FINANCIERO, Banco de Depósito,  Número de Cuenta para Depósito y Número de Cuenta CLABE.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);   
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	//====================================================================>> TESTIGO 1 IF
	if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")&&false) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")&&false) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);					
		pdfDoc.setCell((String)contratoCesion.get("firmaTest1"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	//====================================================================>> TESTIGO 2 IF
	if (contratoCesion.get("claveUsrTest2") != null && !contratoCesion.get("claveUsrTest2").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	
	pdfDoc.endDocument();
	
	if (cesionBean.getBanderaContrato(clave_solicitud).equals("S")){
		nombreArchivo2 = cesionBean.descargaContratoCesionNuevo(clave_solicitud,strDirectorioTemp);
	}else{
		nombreArchivo2 = cesionBean.generarContrato(clave_solicitud,strDirectorioTemp);
	}
	
	 ComunesPDF pdfDoc2 = new ComunesPDF(2, strDirectorioTemp + nombreArchivo3, "", false, false, false);
	 if (contratoCesion.get("firmaIf") != null && !contratoCesion.get("firmaIf").toString().equals("")) {
		pdfDoc2.setTable(1,70);
		pdfDoc2.setCell("Enteradas del contenido y alcance del presente contrato, las partes lo firman de conformidad, en la Ciudad de México, Distrito Federal, a los "+contratoCesion.get("char_fecha_if"),"contrato",ComunesPDF.JUSTIFIED,1,1,0);
		pdfDoc2.addTable();
	 }
	 
	  pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
	String claveGrupo =(String)contratoCesion.get("ic_grupo_cesion");	
	UtilUsr utilUsr = new UtilUsr();
	if(!claveGrupo.equals("")){		
		List  lstEmpresasXGrupo = cesionBean.getEmpresasXGrupoCder(claveGrupo);
		if(lstEmpresasXGrupo.size()>0)  {
			for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
				String cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
				String nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
					
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(cvePymeGrupo, "P");
					
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {						
					String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
					Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
					String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
					HashMap datosRep= cesionBean.getRepresentantesCEDENTE( clave_solicitud, cvePymeGrupo, loginUsrPyme );
					if(datosRep.size()>0) {						
						if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
							
							pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
							pdfDoc2.addText("Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePymeGrupo, "formas", ComunesPDF.CENTER);
							pdfDoc2.setTable(1, 40, anchoCelda4);
							pdfDoc2.setCell(datosRep.get("CG_FIRMA_REPRESENTANTE").toString(), "formas", ComunesPDF.CENTER, 1);
							pdfDoc2.addTable();
							banderaArchivo2=true;
						}
					}
				}				
			}
		}	
			//Fodea 023-2015  Cuando la solicitud tiene un grupo de pymes (S)
	}else  {
		clave_pyme = (String)contratoCesion.get("clave_pyme");
		String  nombrePyme=(String)contratoCesion.get("nombre_pyme");					
		List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");					
					
		for (int i = 0; i < usuariosPorPerfil.size(); i++) {						
			String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
			Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
			String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
			HashMap datosRep= cesionBean.getRepresentantesCEDENTE( clave_solicitud, clave_pyme, loginUsrPyme );
			if(datosRep.size()>0) {	
					if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
						pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
						pdfDoc2.addText("Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePyme, "formas", ComunesPDF.CENTER);
						pdfDoc2.setTable(1, 40, anchoCelda4);
						pdfDoc2.setCell(datosRep.get("CG_FIRMA_REPRESENTANTE").toString(), "formas", ComunesPDF.CENTER, 1);
						pdfDoc2.addTable();
						banderaArchivo2=true;							
								
				}
			}
			}					
		}			
					 
	
	//====================================================================>> REPRESENTANTE LEGAL IF
	if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
		pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc2.setTable(1, 40, anchoCelda4);
		pdfDoc2.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc2.addTable();
		banderaArchivo2=true;
	}
	
	//====================================================================>> REPRESENTANTE LEGAL IF 2
	if (contratoCesion.get("claveUsrIfRep2") != null && !contratoCesion.get("claveUsrIfRep2").toString().equals("")) {
		pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIfRep2") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc2.setTable(1, 40, anchoCelda4);
		pdfDoc2.setCell((String)contratoCesion.get("firmaIfRep2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc2.addTable();
		banderaArchivo2=true;
	}

	pdfDoc2.endDocument();
	
	ArrayList listaA = new ArrayList();
	
	if(banderaArchivo1){listaA.add(strDirectorioTemp+nombreArchivo);}
	listaA.add(strDirectorioTemp+nombreArchivo2);
	if(banderaArchivo2){listaA.add(strDirectorioTemp+nombreArchivo3);}
	String nombreArchivoNevo = archivo.nombreArchivo()+".pdf";
	listaA.add(strDirectorioTemp+nombreArchivoNevo);
	String args[]= new String[listaA.size()];
	for (int i=0; i<listaA.size(); i++)
		{
			args[i]=(String)listaA.get(i);
		}
	//String args[] =listaA.toArray();//  {strDirectorioTemp+nombreArchivo,strDirectorioTemp+nombreArchivo2,strDirectorioTemp+nombreArchivo3,strDirectorioTemp+nombreArchivoNevo};
	
	if(banderaArchivo1||banderaArchivo2){
		nombreArchivo=nombreArchivoNevo;
		ComunesPDF.doMerge(args);
		
	}else{
		nombreArchivo=nombreArchivo2;
	}
	
	sellos.append("<table align='center' width='750' border='0' cellspacing='1' cellpadding='1'>"+
	"<embed src='"+strDirecVirtualTemp+nombreArchivo+"' width='770' height='500'></embed>"+
	"</table>");

		if(autRep2.equals("S")){
			acuse = 	consultaContratoCesion.get("acuseFirmaIfRep2").toString();	
	  }else{
			acuse = 	consultaContratoCesion.get("acuseFirmaIf").toString();
	  }

		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("fechaCarga", fechaCarga);
		jsonObj.put("horaCarga", horaCarga);
		jsonObj.put("recibo", recibo);
		jsonObj.put("acuse", acuse);
		jsonObj.put("sello_digital", sello_digital);
		jsonObj.put("usuario", iNoUsuario+" "+nombre_usuario);
		jsonObj.put("mensajeAuto", mensajeAuto);
		jsonObj.put("lblSellos", sellos.toString());
		jsonObj.put("clave_solicitud", clave_solicitud);
		jsonObj.put("tipoOperacion", tipoOperacion);		
		infoRegresar = jsonObj.toString();  
	 
	 
 }else if(informacion.equals("ARCHIVO_ACUSE")  ){
   
		
	HashMap parametros = new HashMap();
	parametros.put("nombre_usuario", iNoUsuario+" "+strNombreUsuario);
	parametros.put("recibo_carga", recibo_carga);
	parametros.put("fecha_carga", fecha_carga);
	parametros.put("hora_carga", hora_carga);
	parametros.put("clave_solicitud", clave_solicitud);	
	parametros.put("tipoOperacion", tipoOperacion);
	parametros.put("strDirectorioTemp", strDirectorioTemp);
	parametros.put("pais", (String)session.getAttribute("strPais"));
	parametros.put("noCliente", iNoCliente);
	parametros.put("nombre", (String)session.getAttribute("strNombre"));
	parametros.put("nombreUsr", (String)session.getAttribute("strNombreUsuario"));
	parametros.put("logo", (String)session.getAttribute("strLogo"));
	parametros.put("strDirectorioPublicacion", strDirectorioPublicacion);
	parametros.put("tipo_archivo", tipo_archivo);
	
  String nombreArchivo =cesionBean.archConCesionIF (parametros);
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();  
  
}else if(informacion.equals("RetornarOperativo") ){

	cesionBean.retornarSolicOperativo(clave_solicitud,causas_retorno,"3");
	jsonObj.put("success", new Boolean(true));	
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("AutorizarOperativo") ){

	cesionBean.aceptarIfOperativo(clave_solicitud,banco_deposito,numero_cuenta,clabe,monto_credito,referencia,fecha_vencimiento, iNoUsuario);// F024-2015
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if(informacion.equals("RechazarOperativo") ){

	cesionBean.rechazarIfOperativo(clave_solicitud);	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}


%>
<%=infoRegresar%>

