Ext.onReady(function(){


	//descargar archivo PDF de Acuse  
	var procesarGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFAcuse');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFAcuse');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
		var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '../34pyme/34ContratoCesionPDFCSV.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}
	
	
	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarPoderesIF = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		//forma.action = '/nafin/00archivos/22cotizador/instrucciones.pdf';
					Ext.Ajax.request({
							url: '../../34DescargaPoderes.data.jsp',
							params: Ext.apply({
								informacion:'PoderesExtincionContrato',
								clave_solicitud:clave_solicitud
							}),
							callback: procesarArchivoSuccess
						});
		}
	
	var Convenio = new Ext.Panel({
		id: 'Convenio',
		width: 600,
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true
	});
	
		var verConvenioExtincion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');		
		var ventana = Ext.getCmp('verConvenio');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 940,
				height: 550,			
				id: 'verConvenio',
				closeAction: 'hide',
				items: [					
					Convenio
				],
				title: 'Convenio de Extinci�n'	
			}).show();
		}
		var bodyPanel = Ext.getCmp('Convenio').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure',
			function(el, response) {
				bodyPanel.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		Ext.Ajax.request({
				url: '../34ConvenioExtincionCesionDerechosExt.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'CONTRATO_EXTINCION',
					clave_solicitud: clave_solicitud,
					clave_epo: clave_epo		
				}),
				callback: procesarContratoExtincionPDF
			});
		}
	
		var procesarContratoExtincionPDF =  function(opts, success, response) {
		//var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		//btnGenerarPDFAcuse.setIconClass('');
		//AcuseFirma.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			 Ext.getCmp('Convenio').update('<table align="center" width="750" border="0" cellspacing="1" cellpadding="1"><embed src="'+ Ext.util.JSON.decode(response.responseText).urlArchivo+'" width="770" height="500"></embed></table>');
			//btnBajarPDFAcuse.show();
			//btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			
		} else {
			//btnGenerarPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarSuccessFailureAcuse(opts, success, response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
								
				if(jsonData.acuse !='') {				
				
				consPreAcuseData.load({
					params:{
						clave_solicitud:jsonData.clave_solicitud,
						clave_epo:jsonData.clave_epo,
						operacion:jsonData.operacion,
						nuevoEstatus:jsonData.nuevoEstatus,
						operacionAcuse:'ACUSE'
					}
				});	
				
					var acuseCifras = [
						['N�mero de Acuse', jsonData.acuse],
						['Fecha de Carga', jsonData.fechaCarga],
						['Hora de Carga', jsonData.horaCarga],
						['Usuario de Captura', jsonData.usuario]
					];
					
					storeCifrasData.loadData(acuseCifras);						
					Ext.getCmp('btnSalirA').show();
					Ext.getCmp('btnCancelar').hide();
					Ext.getCmp('rechazar').hide();
					Ext.getCmp('autorizar').hide();					
					Ext.getCmp('btnGenerarPDFAcuse').show();	
					Ext.getCmp('gridCifrasControl').show();						
					Ext.getCmp("clave_solicitud").setValue(jsonData.clave_solicitud);
					Ext.getCmp("acuse").setValue(jsonData.acuse);
					Ext.getCmp("fechaCarga").setValue(jsonData.fechaCarga);
					Ext.getCmp("horaCarga").setValue(jsonData.horaCarga);
					Ext.getCmp("usuario").setValue(jsonData.usuario);
				
				}else if(jsonData.acuse =='') {
					Ext.getCmp('gridPreAcuse').hide();	
					Ext.getCmp('mensajeConsulta').hide();
					Ext.getCmp('fpBotones').show();	
					Ext.getCmp("mensajeAuto").setValue(jsonData.mensajeAuto);					
					Ext.getCmp('mensajeAutorizacion').show();	
				}
				
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
		
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 150,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 370,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 150,
		style: 'margin:0 auto;',
		frame: true
	});


	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'150',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{
				xtype: 'button',
				text: 'Salir',			
				id: 'btnSalir',		 
				handler: function() {
					window.location = '34ExtincionContratoIfext.jsp?pantalla=SolicExtincion';
				}
			}	
		]
	});
	
	var mensajeAutorizacion = new Ext.Container({
			layout: 'table',		
			id: 'mensajeAutorizacion',							
			width:	'250',
			heigth:	'auto',
			hidden: true,
			style: 'margin:0 auto;',
			items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeAuto', 	value: '' }			
			]
		});
		
		
	var confirmarAcuse = function(pkcs7, textoFirmar,  clave_solicitud, clave_epo,nuevoEstatus, causasRechazoExtincion, operacion, fechaExtincion, numeroCuenta, cuentaClabe, bancoDeposito ){
	
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnTerminaCap").enable();	
			return;
		}else  {
			
			Ext.Ajax.request({
				url : '34ExtincionContratoIf.data.jsp',
				params : {
					informacion:            'GenerarAcuse',
					clave_solicitud:        clave_solicitud,
					clave_epo:              clave_epo,
					textoFirmar:            textoFirmar,
					pkcs7:                  pkcs7,
					nuevoEstatus:           nuevoEstatus,
					causasRechazoExtincion: causasRechazoExtincion,
					operacion:              operacion,
					fechaExtincion:         fechaExtincion,
					numeroCuenta:           numeroCuenta,
					cuentaClabe:            cuentaClabe,
					bancoDeposito:          bancoDeposito
				},
				callback: procesarSuccessFailureAcuse
			});					
		}
	}
		
		
	var procesarAutorizarRechazarAcuse = function() {
		var  gridPreAcuse = Ext.getCmp('gridPreAcuse');
		var store = gridPreAcuse.getStore();
		var operacion = Ext.getCmp('operacion').getValue();
		var clave_solicitud;
		var clave_epo;
		var nuevoEstatus;
		var causasRechazoExtincion;
		var fechaExtincion;
		var numeroCuenta;
		var cuentaClabe;
		var bancoDeposito;
		
		if(operacion =='AUTORIZAR') { nuevoEstatus =19 }
		if(operacion =='RECHAZAR')  { nuevoEstatus =17 }
				
		var textoFirmar = Ext.getCmp('textoFirmar').getValue()+'\n';
			store.each(function(record) {			
				textoFirmar += record.data['DEPENDENCIA'] + "|" +
									record.data['NOMBRE_CEDENTE'] + "|" +
									record.data['RFC_CEDENTE'] + "|" +
									record.data['REPRESENTATE_LEGAL'].replace('\n', '') + "|" +  
									record.data['NUMERO_CONTRATO'] + "|" +
									record.data['MONTO_MONEDA'].replace('<br/>', '') + "|" +
									record.data['TIPO_CONTRATACION'] + "|" +
									record.data['FECHA_INICIO_CONTRATO'] + "|" +
									record.data['FECHA_FIN_CONTRATO'] + "|" +
									record.data['PLAZO_CONTRATO'] + "|" +
									record.data['CLASIFICACION_EPO'] + "|";								
			if(Ext.getCmp('campo01').getValue() !='') { textoFirmar +=	record.data['CAMPO_ADICIONAL_1'] + "|"; }
			if(Ext.getCmp('campo02').getValue() !='') {	textoFirmar +=	record.data['CAMPO_ADICIONAL_2'] + "|"; }
			if(Ext.getCmp('campo03').getValue() !='') {	textoFirmar +=	record.data['CAMPO_ADICIONAL_3'] + "|"; }
			if(Ext.getCmp('campo04').getValue() !='') {	textoFirmar +=	record.data['CAMPO_ADICIONAL_4'] + "|"; }
			if(Ext.getCmp('campo05').getValue() !='') {	textoFirmar +=	record.data['CAMPO_ADICIONAL_5'] + "|"; }
					
			fechaExtincion = record.data['FECHA_EXTINCION'];
			numeroCuenta   = record.data['NUMERO_CUENTA'];
			cuentaClabe    = record.data['CUENTA_CLABE'];
			bancoDeposito  = record.data['BANCO_DEPOSITO'];

			textoFirmar +=	record.data['OBJETO_CONTRATO'] + "|" +
								record.data['FECHA_EXTINCION'] + "|" +
								record.data['NUMERO_CUENTA'] + "|" +
								record.data['CUENTA_CLABE'] + "|" +
								record.data['BANCO_DEPOSITO'] + "|" +
								record.data['ESTATUS_SOLICITUD'] + "\n" ;
									
			clave_solicitud = record.data['CLAVE_SOLICITUD'];
			clave_epo = record.data['CLAVE_EPO'];
			causasRechazoExtincion = record.data['CAUSA_RECHAZO'];
		});
		
		if(operacion =='RECHAZAR')  {		
			if(causasRechazoExtincion==''){
				Ext.MessageBox.alert("Mensaje",'El valor de las Causas del Rechazo es requerido.');
				return;			
			}
		}
		
			
		
				    
			
		
		NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar,  clave_solicitud, clave_epo,nuevoEstatus, causasRechazoExtincion, operacion, fechaExtincion, numeroCuenta, cuentaClabe, bancoDeposito );
		
	}
	
	
	//campos editable de Rechazode Solicitud	
	var causasRechazo = new Ext.form.TextField({
		name: 'causasRechazo',
		id:'txtCausasRechazo',
		fieldLabel: '',
		allowBlank: true,
		maxLength: 100,
		width: 80,
		msgTarget: 'side',
		margins: '0 20 0 0'
	});
	
	var procesarPreAcuseData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridPreAcuse = Ext.getCmp('gridPreAcuse');	
	
		if (arrRegistros != null) {
			if (!gridPreAcuse.isVisible()) {
				gridPreAcuse.show();
			}						
			//edito el titulo de la columna
			var el = gridPreAcuse.getGridEl();
			var cm = gridPreAcuse.getColumnModel();
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  clasificacionEpo = jsonData.clasificacionEpo;	
			var  operacion = jsonData.operacion;	
			var  operacionAcuse = jsonData.operacionAcuse;
			
			if(operacion=='AUTORIZAR' && operacionAcuse =='' ) {
				Ext.getCmp('autorizar').show();
				Ext.getCmp('rechazar').hide();
				Ext.getCmp('btnCancelar').show();				
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAUSA_RECHAZO'), true);		
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAUSAS_RECHAZO'), true);
			}else if(operacion=='RECHAZAR' && operacionAcuse =='') {
				Ext.getCmp('autorizar').hide();
				Ext.getCmp('rechazar').show();
				Ext.getCmp('btnCancelar').show();
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAUSA_RECHAZO'), false);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAUSAS_RECHAZO'), true);
				
			}else if(jsonData.operacionAcuse=='ACUSE') {			
				Ext.getCmp('autorizar').hide();
				Ext.getCmp('rechazar').hide();
				Ext.getCmp('btnCancelar').hide();
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAUSAS_RECHAZO'), false);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAUSA_RECHAZO'), true);
			}				
			
			if(clasificacionEpo !=''){				
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), false);	
			}
			
			if(hayCamposAdicionales=='0'){
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='2'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.campo02);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='3'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.campo02);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.campo03);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='4'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.campo02);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.campo03);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.campo04);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='5'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.campo02);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.campo03);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.campo04);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_5'),jsonData.campo05);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), false);
			}					
				
			Ext.getCmp("mensaje").setValue(jsonData.mensaje);
			Ext.getCmp("textoFirmar").setValue(jsonData.textoFirmar);
			Ext.getCmp("campo01").setValue(jsonData.campo01);
			Ext.getCmp("campo02").setValue(jsonData.campo02);
			Ext.getCmp("campo03").setValue(jsonData.campo03);
			Ext.getCmp("campo04").setValue(jsonData.campo04);
			Ext.getCmp("campo05").setValue(jsonData.campo05);	
			Ext.getCmp("operacion").setValue(jsonData.operacion);	
			Ext.getCmp("clave_solicitud").setValue(jsonData.clave_solicitud);
			
			Ext.getCmp("mensajeConsulta").show();
			Ext.getCmp('btnBajarPDFConsu').hide();
			Ext.getCmp('btnBajarCSVConsu').hide();
			Ext.getCmp('btnGenerarPDFConsu').hide();
			Ext.getCmp('btnGenerarPDFConsu').hide();
			Ext.getCmp('gridConsulta').hide();
			
			Ext.getCmp('forma').hide();
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
		
	var consPreAcuseData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '34ExtincionContratoIf.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse'
		},		
		fields: [	
			{name: 'CLAVE_SOLICITUD'},
			{name: 'CLAVE_PYME'},
			{name: 'CLAVE_EPO'},
			{name: 'DEPENDENCIA'},
			{name: 'NOMBRE_CEDENTE'},
			{name: 'RFC_CEDENTE'},
			{name: 'REPRESENTATE_LEGAL'},
			{name: 'NUMERO_CONTRATO'},
			{name: 'MONTO_MONEDA'},
			{name: 'TIPO_CONTRATACION'},
			{name: 'FECHA_INICIO_CONTRATO'},
			{name: 'FECHA_FIN_CONTRATO'},
			{name: 'PLAZO_CONTRATO'},
			{name: 'CLASIFICACION_EPO'},
			{name: 'CAMPO_ADICIONAL_1'},
			{name: 'CAMPO_ADICIONAL_2'},
			{name: 'CAMPO_ADICIONAL_3'},
			{name: 'CAMPO_ADICIONAL_4'},
			{name: 'CAMPO_ADICIONAL_5'},
			{name: 'OBJETO_CONTRATO'},
			{name: 'FECHA_EXTINCION'},
			{name: 'NUMERO_CUENTA'},
			{name: 'CUENTA_CLABE'},
			{name: 'BANCO_DEPOSITO'},
			{name: 'CAUSAS_RECHAZO'},
			{name: 'ESTATUS_SOLICITUD'},
			{name: 'CLAVE_ESTATUS'},
			{name: 'CAUSA_RECHAZO'}					
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarPreAcuseData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarPreAcuseData(null, null, null);					
				}
			}
		}		
	});
	
	var gridPreAcuse = new Ext.grid.EditorGridPanel({
		store: consPreAcuseData,
		id: 'gridPreAcuse',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Extinci�n de Contrato',
		columns: [	
			{
				header: 'Dependencia',
				tooltip: 'Dependencia',
				dataIndex: 'DEPENDENCIA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Pyme(Cendente)',
				tooltip: 'Pyme(Cendente)',
				dataIndex: 'NOMBRE_CEDENTE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC_CEDENTE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Representante Legal',
				tooltip: 'Representante Legal',
				dataIndex: 'REPRESENTATE_LEGAL',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto / Moneda',
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha Inicio Contrato',
				tooltip: 'Fecha Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Fecha Final Contrato',
				tooltip: 'Fecha Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Plazo del Contrato',
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'CLASIFICACION_EPO',
				tooltip: 'CLASIFICACION_EPO',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_1',
				tooltip: 'CAMPO_ADICIONAL_1',
				dataIndex: 'CAMPO_ADICIONAL_1',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_2',
				tooltip: 'CAMPO_ADICIONAL_2',
				dataIndex: 'CAMPO_ADICIONAL_2',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_3',
				tooltip: 'CAMPO_ADICIONAL_3',
				dataIndex: 'CAMPO_ADICIONAL_3',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_4',
				tooltip: 'CAMPO_ADICIONAL_4',
				dataIndex: 'CAMPO_ADICIONAL_4',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_5',
				tooltip: 'CAMPO_ADICIONAL_5',
				dataIndex: 'CAMPO_ADICIONAL_5',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Objeto del Contrato',
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha de Extinci�n del Contrato',
				tooltip: 'Fecha de Extinci�n del Contrato',
				dataIndex: 'FECHA_EXTINCION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Cuenta',
				tooltip: 'Cuenta',
				dataIndex: 'NUMERO_CUENTA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Cuenta CLABE',
				tooltip: 'Cuenta CLABE',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Banco de Dep�sito',
				tooltip: 'Banco de Dep�sito',
				dataIndex: 'BANCO_DEPOSITO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Causas de Rechazo',
				tooltip: 'Causas de Rechazo',
				dataIndex: 'CAUSAS_RECHAZO',
				sortable: true,
				width: 130,				
				resizable: true,	
				hidden: true,
				align: 'left'	
			},			
			{
				header: 'Causas de Rechazo', 
				tooltip: 'Causas de Rechazo',
				dataIndex: 'CAUSA_RECHAZO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left',
				hidden: true,
				editor: causasRechazo,
				renderer: NE.util.colorCampoEdit
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS_SOLICITUD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			}			
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 300,
		width: 943,		
		frame: true,
		bbar: {
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					hidden:true,
					id: 'btnGenerarPDFAcuse',
					handler: function(boton, evento) {		
					
						var operacion = Ext.getCmp('operacion').getValue();
						var clave_solicitud = Ext.getCmp('clave_solicitud').getValue();
						var nuevoEstatus;
						if(operacion =='AUTORIZAR') { nuevoEstatus =19 }
						if(operacion =='RECHAZAR')  { nuevoEstatus =17 }		
						boton.disable();
						boton.setIconClass('loading-indicator');						
						Ext.Ajax.request({
							url: '34ExtincionContratoIf.data.jsp',							
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ConsultarAcusePDF',
								operacion:'ArchivoPDF',
								tipoArchivo:'PDF',
								nuevoEstatus:nuevoEstatus,
								clave_solicitud:clave_solicitud,
								acuse:Ext.getCmp("acuse").getValue(),
								fechaCarga:Ext.getCmp("fechaCarga").getValue(),
								horaCarga:Ext.getCmp("horaCarga").getValue(),
								usuario:Ext.getCmp("usuario").getValue(),
								clave_epo: Ext.getCmp("clave_epo1").getValue()
							}),							 
							callback: procesarGenerarPDFAcuse
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFAcuse',
					hidden: true
				},
				{
					text: 'Autorizar',
					id: 'autorizar',
					iconCls: 'icoAceptar',
					hidden:true,
					handler: procesarAutorizarRechazarAcuse
				},
				{
					text: 'Rechazar',
					id: 'rechazar',
					iconCls: 'icoRechazar',
					hidden:true,
					handler: procesarAutorizarRechazarAcuse
				},
				{
					text: 'Cancelar',
					id: 'btnCancelar',
					iconCls: 'icoLimpiar',
					hidden: true,
					handler: function() {
						window.location = '34ExtincionContratoIfext.jsp?pantalla=SolicExtincion';
					}
				},
				{
					text: 'Salir',
					id: 'btnSalirA',
					iconCls: 'icoLimpiar',
					hidden: true,
					handler: function() {
						window.location = '34ExtincionContratoIfext.jsp?pantalla=SolicExtincion';
					}
				}
			]
		}
	});
	
	//********************************ProcesarPreAcuse  AUTORIZAR ************
	var procesoAutorizar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');
		var fp = Ext.getCmp('forma');
		fp.el.mask('Enviando...', 'x-mask-loading');	
		consPreAcuseData.load({
			params:{
				clave_solicitud:	clave_solicitud,
				clave_epo:clave_epo,
				operacion:'AUTORIZAR'
			}
		});				
	}
	
	//********************************ProcesarPreAcuse RECHAZAR ************
	var procesoRechazar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');
		var fp = Ext.getCmp('forma');
		fp.el.mask('Enviando...', 'x-mask-loading');	
		consPreAcuseData.load({
			params:{
				clave_solicitud:	clave_solicitud,
				clave_epo:clave_epo,
				operacion:'RECHAZAR'
			}
		});				
	}
	
//********************************Convenio de Extinci�n ************

	var verConvenioExtincion2 = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');		
		var ventana = Ext.getCmp('verConvenio');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				title: 'Convenio de Extinci�n',	
				width: 661,	height: 700,	
				id: 'verConvenio',	
				closeAction: 'hide',
				autoScroll: true,	
				resizable:false,	
				modal:true,
				items: [	
				{	xtype:'panel',	id: 'Convenio',	resizable:false, width: 630,	height: 'auto'}	
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: [
						{	
							xtype: 'button',	buttonAlign:'right',  text: 'Cerrar',		id: 'btnCerrarV',
							handler: function(){
								Ext.getCmp('verConvenio').hide();
							} 
						}
					]
				}
			}).show();
		}
		var bodyPanel = Ext.getCmp('Convenio').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure',
			function(el, response) {
				bodyPanel.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		mgr.update({
			url: '/nafin/34cesion/34pki/34ConvenioExtincionCesionDerechosExt.jsp',
			scripts: true,
			params: {
				clave_solicitud: clave_solicitud,
				clave_epo: clave_epo
			},
			indicatorText: 'Convenio de Extinci�n'
		});	
	}
	

	
	
	
	//************columna Descarga Archivo Contrato de Cesi�n	******************
	function procesarGenerarPDFContratoCecion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var descargaArchivoContratoCecion = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');
		
			Ext.Ajax.request({
			url: '34ExtincionContratoIf.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
			informacion: 'CONTRATO_CESION_PYME',
			tipoArchivo:'CONTRATO_CESION_PYME',
			clave_solicitud:clave_solicitud,
			clave_epo:clave_epo
			}),
			callback: procesarGenerarPDFContratoCecion
		});		
	}
	
	//************columna Descarga Archivo Contrato	******************
	function procesarSuccessFailureContrato(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var descargaArchivoContrato = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
			url: '34ExtincionContratoIf.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContrato
		});
	}
		
	//****************para ver los poderes de la pyme***************
	var VerPoderesPyme = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLAVE_PYME');
		var solicitud = registro.get('CLAVE_SOLICITUD');
		var rfcPyme = registro.get('RFC_CEDENTE');	
		var grupo_cesion = registro.get('GRUPO_CESION');	
		var no_contrato = registro.get('NUMERO_CONTRATO');	
		
		var parametros = "clavePyme="+clavePyme+"&rfcPyme="+rfcPyme+"&grupo_cesion="+grupo_cesion+"&no_contrato="+no_contrato;
		
		
		var ventana = Ext.getCmp('VerPoderesPyme');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
					layout: 'fit',
					width: 500,
					height: 200,
					closeAction: 'hide',
					autoDestroy:true,
					closable:false,
					modal:true,
					id: 'VerPoderesPyme',
					closeAction: 'hide',
					items: [					
						PanelVerPoderesPyme
					],
					title: 'Ver Poderes',
					bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{
							xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrar', 
							handler: function(){
								Ext.getCmp('PanelVerPoderesPyme').hide();
								Ext.getCmp('VerPoderesPyme').destroy();
							} 
						}
					]
				}
			}).show();
		}	
		
		var pabelBody = Ext.getCmp('PanelVerPoderesPyme').body;
			var mgr = pabelBody.getUpdater();
			mgr.on('failure', 
			function(el, response) {
				pabelBody.update('');
				NE.util.mostrarErrorResponse(response);
			});		
			mgr.update({
				url: '/nafin/34cesion/34pki/34pyme/34SolicConsVerPoderesPymeExt.jsp?'+parametros,						
				indicatorText: 'Cargando Ver Poderes'
			});					
	}
	
	var PanelVerPoderesPyme = {
		xtype: 'panel',
		id: 'PanelVerPoderesPyme',
		width: 700,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	};
	
//*********************CONSLULTA ********************************


	//descargar archivo PDF dela Consulta 
	var procesarGenerarPDFConsu =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFConsu');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFConsu');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//descargar archivo CSV dela Consulta
	var procesarGenerarCSVConsu =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSVConsu');
		btnGenerarCSV.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSVConsu');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  clasificacionEpo = jsonData.clasificacionEpo;	
			
			if(clasificacionEpo !=''){				
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), false);	
			}
			
			if(hayCamposAdicionales=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='2'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.campo02);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='3'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.campo03);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='4'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.campo04);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(hayCamposAdicionales=='5'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.campo04);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_5'),jsonData.campo05);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), false);
			}
						
				
			
			Ext.getCmp('btnBajarPDFConsu').hide();
			Ext.getCmp('btnBajarCSVConsu').hide();
								
			if(store.getTotalCount() > 0) {	
				Ext.getCmp("mensaje").setValue(jsonData.mensaje);
				//Ext.getCmp("mensajeConsulta").show();
				Ext.getCmp('btnGenerarPDFConsu').enable();
				Ext.getCmp('btnGenerarCSVConsu').enable();
				
				el.unmask();					
			} else {	
				Ext.getCmp('btnGenerarPDFConsu').disable();
				Ext.getCmp('btnGenerarCSVConsu').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '34ExtincionContratoIf.data.jsp',
		baseParams: {
			informacion: 'Consultar',
			operacion: 'Generar'
		},		
		fields: [	
			{name: 'CLAVE_SOLICITUD'},
			{name: 'CLAVE_PYME'},
			{name: 'CLAVE_EPO'},
			{name: 'DEPENDENCIA'},
			{name: 'NOMBRE_CEDENTE'},
			{name: 'RFC_CEDENTE'},
			{name: 'REPRESENTATE_LEGAL'},
			{name: 'NUMERO_CONTRATO'},
			{name: 'MONTO_MONEDA'},
			{name: 'TIPO_CONTRATACION'},
			{name: 'FECHA_INICIO_CONTRATO'},
			{name: 'FECHA_FIN_CONTRATO'},
			{name: 'PLAZO_CONTRATO'},
			{name: 'CLASIFICACION_EPO'},
			{name: 'CAMPO_ADICIONAL_1'},
			{name: 'CAMPO_ADICIONAL_2'},
			{name: 'CAMPO_ADICIONAL_3'},
			{name: 'CAMPO_ADICIONAL_4'},
			{name: 'CAMPO_ADICIONAL_5'},
			{name: 'OBJETO_CONTRATO'},
			{name: 'FECHA_EXTINCION'},
			{name: 'NUMERO_CUENTA'},
			{name: 'CUENTA_CLABE'},
			{name: 'BANCO_DEPOSITO'},
			{name: 'CAUSAS_RECHAZO'},
			{name: 'ESTATUS_SOLICITUD'},
			{name: 'CLAVE_ESTATUS'},
			{name: 'COLUMNA_ACCION'},
			{name: 'GRUPO_CESION'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Extinci�n de Contrato',
		columns: [	
			{
				header: 'Dependencia',
				tooltip: 'Dependencia',
				dataIndex: 'DEPENDENCIA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Pyme(Cendente)',
				tooltip: 'Pyme(Cendente)',
				dataIndex: 'NOMBRE_CEDENTE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'
				
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC_CEDENTE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Representante Legal',
				tooltip: 'Representante Legal',
				dataIndex: 'REPRESENTATE_LEGAL',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto / Moneda',
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha Inicio Contrato',
				tooltip: 'Fecha Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Fecha Final Contrato',
				tooltip: 'Fecha Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Plazo del Contrato',
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'CLASIFICACION_EPO',
				tooltip: 'CLASIFICACION_EPO',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_1',
				tooltip: 'CAMPO_ADICIONAL_1',
				dataIndex: 'CAMPO_ADICIONAL_1',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_2',
				tooltip: 'CAMPO_ADICIONAL_2',
				dataIndex: 'CAMPO_ADICIONAL_2',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_3',
				tooltip: 'CAMPO_ADICIONAL_3',
				dataIndex: 'CAMPO_ADICIONAL_3',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_4',
				tooltip: 'CAMPO_ADICIONAL_4',
				dataIndex: 'CAMPO_ADICIONAL_4',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'CAMPO_ADICIONAL_5',
				tooltip: 'CAMPO_ADICIONAL_5',
				dataIndex: 'CAMPO_ADICIONAL_5',
				sortable: true,
				hidden:true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Objeto del Contrato',
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato de Cesi�n',
				tooltip: 'Contrato de Cesi�n',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						} ,handler: descargarContratoCesion
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Convenio de Extinci�n',
				tooltip: 'Convenio de Extinci�n',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						} ,handler: verConvenioExtincion
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						} ,handler: descargaArchivoContrato
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Poderes',
				tooltip: 'Poderes',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	handler: VerPoderesPyme
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Poderes IF Extinci�n',
				tooltip: 'Poderes IF Extinci�n',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	handler: procesarPoderesIF
					}
				]				
			},
			{
				header: 'Fecha de Extinci�n del Contrato',
				tooltip: 'Fecha de Extinci�n del Contrato',
				dataIndex: 'FECHA_EXTINCION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Cuenta',
				tooltip: 'Cuenta',
				dataIndex: 'NUMERO_CUENTA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Cuenta CLABE',
				tooltip: 'Cuenta CLABE',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Banco de Dep�sito',
				tooltip: 'Banco de Dep�sito',
				dataIndex: 'BANCO_DEPOSITO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Causa del Rechazo',
				tooltip: 'Causa del Rechazo',
				dataIndex: 'CAUSAS_RECHAZO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS_SOLICITUD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},			
			{
				xtype: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('COLUMNA_ACCION') =='N') {
						return 'N/A ';					
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('COLUMNA_ACCION') =='S') {
							this.items[0].tooltip = 'Autorizar';
								return 'autorizar';	
							}
						},	handler: procesoAutorizar
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('COLUMNA_ACCION') =='S') {
								this.items[1].tooltip = 'Rechazar';
								return 'icoRechazar';
							}
						},handler: procesoRechazar
					}
				]				
			}			
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",				
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFConsu',
					handler: function(boton, evento) {
					
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						boton.disable();
						boton.setIconClass('loading-indicator');						
						Ext.Ajax.request({
							url: '34ExtincionContratoIf.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF',
								tipoArchivo:'PDF',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize
							}),							
							callback: procesarGenerarPDFConsu
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFConsu',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVConsu',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');						
						Ext.Ajax.request({
							url: '34ExtincionContratoIf.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV',
								tipoArchivo:'CSV'
							}),							
							callback: procesarGenerarCSVConsu
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo ',
					id: 'btnBajarCSVConsu',
					hidden: true
				}
			]	
		}		
	});


	var mensajeConsulta = new Ext.Container({
		layout: 'table',		
		id: 'mensajeConsulta',							
		width:	'700',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' }	,
		{ 	xtype: 'displayfield', hidden: true, id: 'textoFirmar', 	value: '' },
		{ 	xtype: 'displayfield', hidden: true, id: 'campo01', 	value: '' },
		{ 	xtype: 'displayfield', hidden: true, id: 'campo02', 	value: '' },
		{ 	xtype: 'displayfield', hidden: true, id: 'campo03', 	value: '' },
		{ 	xtype: 'displayfield', hidden: true, id: 'campo04', 	value: '' },
		{ 	xtype: 'displayfield', hidden: true, id: 'campo05', 	value: '' },
		{ 	xtype: 'displayfield', hidden: true, id: 'operacion', 	value: '' },
		{ 	xtype: 'displayfield', hidden: true, id: 'acuse', 	value: '' },
		{ 	xtype: 'displayfield', hidden: true, id: 'clave_solicitud', 	value: '' },	
		{ 	xtype: 'displayfield', hidden: true, id: 'fechaCarga', 	value: '' }	,
		{ 	xtype: 'displayfield', hidden: true, id: 'horaCarga', 	value: '' }	,
		{ 	xtype: 'displayfield', hidden: true, id: 'usuario', 	value: '' }			
		]
	});
	
//*********************FORMA ********************************

	var procesarCatalogoEPOData= function(store, records, oprion){
		if(store.getTotalCount()==0){
		 var clave_epo = Ext.getCmp('clave_epo1');
		 if(clave_epo.getValue()==''){
		  clave_epo.setValue('No existen EPOS con el producto');
		 }
		}
	}
	var procesarCatalogoPymeData= function(store, records, oprion){
		if(store.getTotalCount()==0){
		 var clave_pyme = Ext.getCmp('clave_pyme1');
		 if(clave_pyme.getValue()==''){
		  clave_pyme.setValue('No hay PyMEs con solicitudes pendientes');
		 }
		}
	}
	
	var catalogoContratacionData = new Ext.data.JsonStore({
		id: 'catalogoContratacionData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ExtincionContratoIf.data.jsp',
		baseParams: {
			informacion: 'CatalogoContratacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ExtincionContratoIf.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatalogoPymeData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPOData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ExtincionContratoIf.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load:procesarCatalogoEPOData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ExtincionContratoIf.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var  elementosForma =  [	
		{
			xtype: 'combo',
			name: 'clave_epo',
			id: 'clave_epo1',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_epo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var cmbContratacion = Ext.getCmp('clave_contratacion1');
						cmbContratacion.setValue('');
						cmbContratacion.setDisabled(false);
						cmbContratacion.store.load({
							params: {
								clave_epo:combo.getValue()
							}
						});
					}
				}
			}
		}, 	
		{
			xtype: 'combo',
			name: 'clave_pyme',
			id: 'clave_pyme1',
			fieldLabel: 'PYME',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_pyme',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoPYMEData,
			tpl : NE.util.templateMensajeCargaCombo					
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Contrato',			
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'numero_contrato',
					id: 'numero_contrato1',
					allowBlank: true,
					width: 100,
					maxLength: 25,
					msgTarget: 'side',					
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda ',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'combo',
					name: 'clave_moneda',
					id: 'clave_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'clave_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					width: 240,
					allowBlank: true,
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},	
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Contrataci�n',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'combo',
					name: 'clave_contratacion',
					id: 'clave_contratacion1',
					fieldLabel: 'Tipo de Contrataci�n',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'clave_contratacion',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 240,
					minChars : 1,
					allowBlank: true,
					store : catalogoContratacionData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},{
		xtype:              'compositefield',
		fieldLabel:         'Fecha Firma de Contrato',
		msgTarget:          'side',
		combineErrors:      false,
		items: [{
			width:           98,
			xtype:           'datefield',
			id:              'fecha_inicio',
			name:            'fecha_inicio',
			msgTarget:       'side',
			vtype:           'rangofecha',
			minValue:        '01/01/1901',
			campoFinFecha:   'fecha_final',
			margins:         '0 20 0 0',
			allowBlank:      true,
			startDay:        0
		},{
			xtype:           'displayfield',
			value:           'a',
			width:           20
		},{
			width:           98,
			xtype:           'datefield',
			id:              'fecha_final',
			name:            'fecha_final',
			msgTarget:       'side',
			vtype:           'rangofecha',
			minValue:        '01/01/1901',
			campoInicioFecha:'fecha_inicio',
			margins:         '0 20 0 0',
			allowBlank:      true,
			startDay:        1
		},{
			xtype:           'displayfield',
			value:           'dd/mm/aaaa',
			width:           30
		}]
		}	
	];
	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
				
					var clave_epo =  Ext.getCmp("clave_epo1");
					if (Ext.isEmpty(clave_epo.getValue()) ){
						clave_epo.markInvalid('El valor de la EPO es requerido.');
						return;
					}
					if(catalogoPYMEData.getTotalCount()==0){
						Ext.getCmp('clave_pyme1').markInvalid('No hay PyMEs con solicitudes pendientes');
						return;
					}															

					// Estos if son para validar las fechas
					if(!Ext.getCmp('forma').getForm().isValid()){
						return;
					}
					if(Ext.getCmp('fecha_inicio').getValue() == '' && !Ext.getCmp('fecha_final').getValue() == ''){
						Ext.getCmp('fecha_inicio').markInvalid('El campo es obligatorio.');
						return;
					}
					if(Ext.getCmp('fecha_final').getValue() == '' && !Ext.getCmp('fecha_inicio').getValue() == ''){
						Ext.getCmp('fecha_final').markInvalid('El campo es obligatorio.');
						return;
					}

					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15
						})
					});
				
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34ExtincionContratoIfext.jsp?pantalla=SolicExtincion';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [	
			NE.util.getEspaciador(15),
			fp,
			mensajeAutorizacion,
			gridPreAcuse,
			NE.util.getEspaciador(10),
			gridCifrasControl,
			NE.util.getEspaciador(10),
			mensajeConsulta,
			NE.util.getEspaciador(10),
			fpBotones,
			gridConsulta,			
			NE.util.getEspaciador(10)
		]
	});
	
	
	catalogoEPOData.load();
	catalogoPYMEData.load();
	catalogoMonedaData.load();
	
});