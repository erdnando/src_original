Ext.onReady(function(){

//F23-2015 
	var VisorAutoConsorcio = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var ic_solicitud = registro.get('CLAVE_SOLICITUD');	
		var ic_estatus = registro.get('CLAVE_ESTATUS');	
		var titulo;
		var tipoVisor;
		
		if(ic_estatus==24) {   titulo = 'Solicitud de Consentimiento' ; tipoVisor = 'Autorizacion_Consorcio'; } 
		if(ic_estatus==25) {   titulo = 'Firma Consorcio' ;    tipoVisor = 'Contrato_cesion';  } 
			
		var win = new NE.AutoConsorcio.VisorConsorcio(ic_solicitud, tipoVisor, titulo );
		
	}
	
	//Ventana Para ver el Contrato de Consentimiento
	var verConsentimiento = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_contratacion = registro.get('CLAVECONTRATACION');
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		var nombre_epo = registro.get('DEPENDENCIA');
		var objeto_contrato = registro.get('OBJETO_CONTRATO');
		var montospormoneda = registro.get('MONTO_MONEDA');
		var plazocontrato = registro.get('PLAZO_CONTRATO');
		var fechasolicitud = registro.get('FECHASOLICITUD');
		var numero_contrato = registro.get('NUMERO_CONTRATO');
		var nombre_if = registro.get('NOMBRE_CESIONARIO');
		
				
		
		var ventana = Ext.getCmp('verConsentimiento');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 940,
				height: 550,			
				id: 'verConsentimiento',
				closeAction: 'hide',
				items: [					
					Consentimiento
				],
				title: 'Consentimiento'	
			}).show();
		}
		var bodyPanel = Ext.getCmp('Consentimiento').body;
			var mgr = bodyPanel.getUpdater();
			mgr.on('failure', 
				function(el, response) {
					x.update('');
					NE.util.mostrarErrorResponse(response);
				}
			);		
			mgr.update({
				url: '/nafin/34cesion/34pki/34pyme/34ContratoCesionVerConsentimientoext.jsp',	
				scripts: true,
				params: {
					clave_contratacion: clave_contratacion,
					clave_solicitud:clave_solicitud,
					nombre_epo:nombre_epo,
					objeto_contrato:objeto_contrato,
					montospormoneda:montospormoneda,
					plazocontrato:plazocontrato,
					fechasolicitud:fechasolicitud,
					numero_contrato:numero_contrato,
					nombre_if:nombre_if
				},
				indicatorText: 'Cargando Consentimiento'
			});	
	}	
	
	 var Consentimiento = new Ext.Panel({
		id: 'Consentimiento',
		width: 600,
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true
	});

		//Muestra el archivo de la Columna Contrato de Cesion 
	var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '../34pyme/34ContratoCesionPDFCSV.jsp',
			params: Ext.apply({
				informacion: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}
	
	//Descarga archivo contrato
	function procesarSuccessFailureContrato(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var descargaArchivoContrato = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
			url: '34MonitorIFext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContrato
		});
	}
	
//*****************************************CONSULTA CONTRATOS MONITOR 
		var mostrarGridB = function(grid,rowIndex,colIndex,item,event){
		var titulo = "";	
		var tipo ="";	
			
		if(rowIndex==12){
			location.href="/nafin/34cesion/34pki/34if/34ExtincionContratoIfext.jsp";
		}		
		if(rowIndex!=12){		
			if (rowIndex==13){ 
				titulo="SOLICITADA";
				tipo="19";
			}else if (rowIndex==14){ 
				titulo="ACEPTADA IF";
				tipo="20";
			}else if (rowIndex==15){ 
				titulo="NOTIFICACIONES ACEPTADAS";
				tipo="23";
			}else if (rowIndex==16){ 
				titulo="NOTIFICADA A VENTANILLA";
				tipo="21";
			}else if (rowIndex==17){ 
				titulo="REDIRECCINAMIENTO APLICADO";
				tipo="22";
			}
			
			consultaDataB.load({
				params:{
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15,
					estatus: tipo
				}
			});
			
			var ventana = Ext.getCmp('ventanaB');		
			if(ventana){
				ventana.setTitle(titulo);
				ventana.show();
			}else{
				new Ext.Window({
					layout: 'fit',
					modal: true,
					width: 800,
					height: 400,
					id: 'ventanaB',
					closeAction: 'hide',
					items: [						
						gridB
						]
				}).show().setTitle(titulo);
			}		
		}		
	}
	var mostrarGridA = function(grid,rowIndex,colIndex,item,event){
		var titulo = "";	
		var tipo ="";
				
		if(rowIndex==0){ //Contratos de Cesion
			location.href="/nafin//34cesion/34pki/34if/34ContratoCesionext.jsp  ";
		}else if(rowIndex==6){
			location.href="/nafin/34cesion/34if/34notificacionesIfext.jsp ";
		}
				
		if(rowIndex!=0&&rowIndex!=6){
		
			if(rowIndex==1){ 
				titulo="SOLICITUDES ACEPTADAS";
				tipo="3";
			}else if (rowIndex==2){ 
				titulo="ACEPTADOS PYME";
				tipo="7";
			}else if (rowIndex==3){ 
				titulo="ACEPTADOS IF";
				tipo="9";
			}else if (rowIndex==4){ 
				titulo="RECHAZADOS IF";
				tipo="10";
			}else if (rowIndex==5){ 
				titulo="CANCELADA POR NAFIN";
				tipo="24";
			}else if (rowIndex==7){ 
				titulo="NO ACEPTADO POR VENTANILLA";
				tipo="15";
			}else if (rowIndex==8){ 
				titulo="NOTIFICACIONES ACEPTADAS";
				tipo="11";
			}else if (rowIndex==9){ 
				titulo="NOTIFICADO A VENTANILLA";
				tipo="16";
			}else if (rowIndex==10){ 
				titulo="RECHAZADAS";
				tipo="12";
			}else if (rowIndex==11){ 
				titulo="RECHAZADA EXPIRADA";
				tipo="25";
			}
						
			consultaDataA.load({
				params:{
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15,
					estatus: tipo
				}
			});
			
			var ventana = Ext.getCmp('ventanaA');		
			if(ventana){
				ventana.setTitle(titulo);
				ventana.show();
			}else{
				new Ext.Window({
					layout: 'fit',
					modal: true,
					width: 800,
					height: 400,
					id: 'ventanaA',
					closeAction: 'hide',
					items: [
						gridA						
						]
				}).show().setTitle(titulo);
			}		
		}
		
	}
	
	var procesarConsultaDataB = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridB = Ext.getCmp('gridB');
			var el = gridB.getGridEl();
			var store = consultaDataB;
			if(store.getTotalCount()>0){
				el.unmask();
			}else {
				el.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
	
	var consultaDataB = new Ext.data.JsonStore({
		root: 'registros',
		url: '34MonitorIFext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'DEPENDENCIA'},
			{name: 'NOMBRE_CEDENTE'},
			{name: 'NUMERO_CONTRATO'},
			{name: 'MONTO_MONEDA'},	
			{name: 'TIPO_CONTRATACION'},
			{name: 'FECHA_INICIO_CONTRATO'},
			{name: 'FECHA_FIN_CONTRATO'},
			{name: 'PLAZO_CONTRATO'},
			{name: 'OBJETO_CONTRATO'},
			{name: 'FECHA_EXTINCION'},
			{name: 'NUMERO_CUENTA'},
			{name: 'CUENTA_CLABE'},
			{name: 'BANCO_DEPOSITO'},
			{name: 'CAUSAS_RECHAZO_EXT'},
			{name: 'ESTATUS_SOLICITUD'},
			{name: 'CLAVE_SOLICITUD'},
			{name: 'CLAVECONTRATACION'},
			{name: 'FIRMA_CONTRATO'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				beforeLoad:	{fn: function(store, options){
						Ext.apply(options.params, { 						
						operacion: 'Generar'
						
						});
					}},
				load: procesarConsultaDataB,
				exception: {
					fn: function(proxy,type,action,optionRequest,response,args){
						NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
						procesarConsultaDataB(null,null,null);
					}
				}
			}
	});
	
	var gridB = {
		xtype: 'grid',
		store: consultaDataB,
		id: 'gridB',
		columns: [
			{
				header: 'Dependencia',
				tooltip: 'Dependencia',
				dataIndex: 'DEPENDENCIA',
				align: 'left',
				width: 130
			},
			{
				header: 'Pyme (Cedente)',
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'NOMBRE_CEDENTE',
				align: 'left',				
				width: 130
			},			
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				align: 'center',
				width: 130
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto/Moneda',
				tooltip: 'Monto/Moneda',
				dataIndex: 'MONTO_MONEDA',
				align: 'left',
				width: 130
			},
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				align: 'left',
				width: 130
			},
			{
				header: 'Fecha Inicio Contrato',
				tooltip: 'Fecha Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				align: 'center',
				width: 130
			},
			{
				header: 'Fecha Final Contrato',
				tooltip: 'Fecha Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				align: 'center',
				width: 130
			},
			{
				header: 'Plazo del Contrato',
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				align: 'center',
				width: 130
			},
			{
				header: 'Objeto del Contrato',
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				align: 'left',
				width: 130
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
				dataIndex: 'CLAVE_SOLICITUD',
				align: 'center',
				width: 130,
				items: [
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa'
						}
						,	handler: descargaArchivoContrato
					}
				]
			},			
			{
				header: 'Fecha de Extincion del Contrato',
				tooltip: 'Fecha de Extincion del Contrato',
				dataIndex: 'FECHA_EXTINCION',
				align: 'center',
				width: 130
			},
			{
				header: 'Cuenta',
				tooltip: 'Cuenta',
				dataIndex: 'NUMERO_CUENTA',
				align: 'center',				
				width: 130
			},		
			{
				header: 'Cuenta CLABE',
				tooltip: 'Cuenta CLABE',
				dataIndex: 'CUENTA_CLABE',
				align: 'center',				
				width: 130
			},
			{
				header: 'Banco de D�posito',
				tooltip: 'Banco de D�posito',
				dataIndex: 'BANCO_DEPOSITO',
				align: 'left',				
				width: 130
			},
			{
				header: 'Causa del Rechazo',
				tooltip: 'Causa del Rechazo',
				dataIndex: 'CAUSAS_RECHAZO_EXT',
				align: 'left',				
				width: 130
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS_SOLICITUD',
				align: 'center',				
				width: 130
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 885,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion1',
			displayInfo: true,
			store: consultaDataB,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "Registros"
		}
	}
		
	var procesarConsultaDataA = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridA = Ext.getCmp('gridA');
			var el = gridA.getGridEl();
			var store = consultaDataA;
			
			var cm = gridA.getColumnModel();
			var jsonData = store.reader.jsonData;
				
			if(store.getTotalCount()>0){  
			
				if(jsonData.estatus=='24') {
					gridA.getColumnModel().setHidden(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'), false);	
					gridA.getColumnModel().setColumnHeader(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'),'Autorizaci�n Consentimiento');
					gridA.getColumnModel().setHidden(cm.findColumnIndex('MOTIVOS_CANCELACION'), false);	
				}else  if(jsonData.estatus=='25') {
					gridA.getColumnModel().setHidden(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'), false);	
					gridA.getColumnModel().setColumnHeader(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'),'Firma Contrato');		
					gridA.getColumnModel().setHidden(cm.findColumnIndex('MOTIVOS_CANCELACION'), true);	
				}else {
					gridA.getColumnModel().setHidden(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'), true);
					gridA.getColumnModel().setHidden(cm.findColumnIndex('MOTIVOS_CANCELACION'), true);	
				}				
				
			}else {
				el.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
	
	var consultaDataA = new Ext.data.JsonStore({
		root: 'registros',
		url: '34MonitorIFext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'DEPENDENCIA'},
			{name: 'NOMBRE_CECIONARIO'},
			{name: 'NUMERO_CONTRATO'},
			{name: 'MONTO_MONEDA'},			
			{name: 'TIPO_CONTRATACION'},
			{name: 'FECHA_INICIO_CONTRATO'},
			{name: 'FECHA_FIN_CONTRATO'},
			{name: 'PLAZO_CONTRATO'},
			{name: 'OBJETO_CONTRATO'},
			{name: 'COMENTARIOS'},
			{name: 'ESTATUS_SOLICITUD'},
			{name: 'CAUSAS_RECHAZO_EXT'},
			{name: 'CLAVE_SOLICITUD'},
			{name: 'CLAVECONTRATACION'},
			{name: 'FECHASOLICITUD'},
			{name: 'FIRMA_CONTRATO'},
			{name: 'MOTIVOS_CANCELACION'},
			{name: 'CLAVE_ESTATUS'},
			{name: 'AUTORIZACION_CONSENTIMIENTO'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				beforeLoad:	{fn: function(store, options){
						Ext.apply(options.params, { 						
						operacion: 'Generar'
						
						});
					}},
				load: procesarConsultaDataA,
				exception: {
					fn: function(proxy,type,action,optionRequest,response,args){
						NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
						procesarConsultaDataA(null,null,null);
					}
				}
			}
	});
	
	var gridA = {
		xtype: 'grid',
		store: consultaDataA,
		id: 'gridA',
		columns: [
			{
				header: 'Dependencia',
				tooltip: 'Dependencia',
				dataIndex: 'DEPENDENCIA',
				align: 'left',
				width: 130
			},			
			{
				header: 'Intermediario Financiero (Cesionario)',
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'NOMBRE_CECIONARIO',
				align: 'left',				
				width: 130
			},
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				align: 'center',
				width: 130
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto/Moneda',
				tooltip: 'Monto/Moneda',
				dataIndex: 'MONTO_MONEDA',
				align: 'left',
				width: 130
			},
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				align: 'left',
				width: 130
			},
			{
				header: 'Fecha Inicio Contrato',
				tooltip: 'Fecha Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				align: 'center',
				width: 130
			},
			{
				header: 'Fecha Final Contrato',
				tooltip: 'Fecha Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				align: 'center',
				width: 130
			},
			{
				header: 'Plazo del Contrato',
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				align: 'center',
				width: 130
			},
			{
				header: 'Objeto del Contrato',
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				align: 'left',
				width: 130
			},			
			{
				header: 'Comentarios',
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				align: 'left',			
				width: 130
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS_SOLICITUD',
				align: 'center',				
				width: 130
			},			
			{
				header: 'Causa del Rechazo',
				tooltip: 'Causa del Rechazo',
				dataIndex: 'CAUSAS_RECHAZO_EXT',
				align: 'left',				
				width: 130
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
				dataIndex: 'CLAVE_SOLICITUD',
				align: 'center',
				width: 130,
				items: [
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa'
						}
						,	handler: descargaArchivoContrato
					}
				]
			},
			
			{
				xtype: 'actioncolumn',
				header: 'Autorizaci�n Consentimiento',
				tooltip: 'Autorizaci�n Consentimiento',
				dataIndex: 'AUTORIZACION_CONSENTIMIENTO',
				hidden: true,
				align: 'center',
				width: 120,
				items: [
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							if(registro.get('CLAVE_SOLICITUD')!=''){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler: VisorAutoConsorcio
					}
				]
			},
			{
				header: 'Motivos de Cancelaci�n',
				tooltip: 'Motivos de Cancelaci�n',
				dataIndex: 'MOTIVOS_CANCELACION',
				hidden: true,
				align: 'center',
				width: 120				
			},					
			
			
			{
				xtype: 'actioncolumn',
				header: 'Consentimiento',
				tooltip: 'Consentimiento',
				dataIndex: 'CLAVE_SOLICITUD',
				align: 'center',
				width: 130,
				items: [
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa'
						}
						,	handler: verConsentimiento
					}
				]
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato de Cesi�n',
				tooltip: 'Contrato de Cesi�n',
				dataIndex: 'CLAVE_SOLICITUD',
				align: 'center',
				width: 130,
				items: [
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa'
						}
						,	handler: descargarContratoCesion
					}
				]
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 885,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaDataB,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "Registros"
		}
	}
		
	//************MONITOR *************************
		var procesarConsultaPrincipalData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridContratoCesion = Ext.getCmp('gridContratoCesion');
			var elSol = gridContratoCesion.getGridEl('gridContratoCesion');
			
			var gridNotificaciones = Ext.getCmp('gridNotificaciones');
			var elNot = gridNotificaciones.getGridEl('gridNotificaciones');
			
			var gridExtincionContrato = Ext.getCmp('gridExtincionContrato');
			var elExt = gridExtincionContrato.getGridEl('gridExtincionContrato');
			
			if(store.getTotalCount()>0){
			
				gridContratoCesion.getView().getRow(0).style.backgroundColor="#C0D9D9";
				for(i=6;i<=17;i++){
					gridContratoCesion.getView().getRow(i).style.display = 'none';
				}				
				gridNotificaciones.getView().getRow(6).style.backgroundColor="#C0D9D9";
				for (i=0;i<6;i++){
					gridNotificaciones.getView().getRow(i).style.display = 'none';
				}
				for(i=12;i<=17;i++){
					gridNotificaciones.getView().getRow(i).style.display = 'none';
				}
				
				gridExtincionContrato.getView().getRow(12).style.backgroundColor="#C0D9D9";
				for(i=0;i<=11;i++){
					gridExtincionContrato.getView().getRow(i).style.display = 'none';
				}							
				
			}else{
				elSol.mask('No se encontro ning�n registro','x-mask');
				elNot.mask('No se encontro ning�n registro','x-mask');
				elExt.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
	

	var consultaPrincipalData = new Ext.data.JsonStore({
		root: 'registros',
		url: '34MonitorIFext.data.jsp',
		baseParams: {
			informacion: 'ConsultaPrincipal'
		},
		fields: [
			{name: 'Descripcion'},
			{name: 'Total'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaPrincipalData,
			exception: {
				fn: function(proxy,type,action,optionRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
					procesarConsultaPrincipalData(null,null,null);
				}
			}
		}
	});
	

	var gridContratoCesion = new Ext.grid.GridPanel({
		id: 'gridContratoCesion',
		store: consultaPrincipalData,
		style: 'margin:0 auto;',
		hidden: false,
		hideHeaders: true,
		columns: [
			{
				xtype: 'actioncolumn',
				header: 'CONTRATOS DE CESION',
				dataIndex: 'Descripcion',
				tooltip: 'CONTRATOS DE CESION',
				sortable: false,
				resiazable: false,
				align: 'left',
				width: 30,
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(registro.get('Descripcion')!=''){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}						
						}
						,handler: mostrarGridA
					}
				]	
			},
			{
				header: 'CONTRATOS DE CESION',
				dataIndex: 'Descripcion',
				tooltip: 'CONTRATOS DE CESION',
				sortable: false,
				resiazable: false,
				align: 'left',
				width: 250,
				renderer: function(value,metadata,record,rowIndex,colIndex,store){
					if(record.get('Descripcion')=="TOTAL SOLICITUDES"){
						value = "<P align='center'>TOTAL SOLICITUDES";
						return value;
					}else{
						return (record.get('Descripcion'));
					}
				}
			},
			{
				header: 'TOTAL',
				dataIndex: 'Total',
				sortable: false,
				resiazable: false,
				align: 'center',
				width: 100
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 160,
		width: 397,
		title: '',
		frame: true
	});
	  
	var gridNotificaciones = new Ext.grid.GridPanel({
		id: 'gridNotificaciones',
		store: consultaPrincipalData,
		style: 'margin:0 auto;',
		hidden: false,
		hideHeaders: true,
		columns: [
			{
				xtype: 'actioncolumn',
				header: 'NOTIFICACIONES',
				dataIndex: 'Descripcion',
				tooltip: 'NOTIFICACIONES',
				sortable: false,
				resiazable: false,
				align: 'left',
				width: 30,
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
						if(rowIndex!=2){
							if(registro.get('Descripcion')!=''){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						}
					}
					,handler: mostrarGridA
					}
				]	
			},
			{
				header: 'NOTIFICACIONES',
				dataIndex: 'Descripcion',
				tooltip: 'NOTIFICACIONES',
				sortable: false,
				resiazable: false,
				align: 'left',
				width: 250,
				renderer: function(value,metadata,record,rowIndex,colIndex,store){
					if(record.get('Descripcion')=="TOTAL SOLICITUDES"){
						value = "<P align='center'>TOTAL SOLICITUDES";
						return value;
					}else{
						return (record.get('Descripcion'));
					}
				}
			},
			{
				header: 'TOTAL',
				dataIndex: 'Total',
				sortable: false,
				resiazable: false,
				align: 'center',
				width: 100
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 170,
		width: 397,
		title: '',
		frame: true
	});
	
	
		
	var gridExtincionContrato = new Ext.grid.GridPanel({
		id: 'gridExtincionContrato',
		store: consultaPrincipalData,
		style: 'margin:0 auto;',
		hidden: false,
		hideHeaders: true,
		columns: [
			{
				xtype: 'actioncolumn',
				header: 'CONTRATOS DE CESION',
				dataIndex: 'Descripcion',
				tooltip: 'CONTRATOS DE CESION',
				sortable: false,
				resiazable: false,
				align: 'left',
				width: 30,
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
						if(rowIndex!=2){
							if(registro.get('Descripcion')!=''){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						}
					}
					,handler: mostrarGridB
					}
				]	
			},
			{
				header: 'CONTRATOS DE CESION',
				dataIndex: 'Descripcion',
				tooltip: 'CONTRATOS DE CESION',
				sortable: false,
				resiazable: false,
				align: 'left',
				width: 250,
				renderer: function(value,metadata,record,rowIndex,colIndex,store){
					if(record.get('Descripcion')=="TOTAL SOLICITUDES"){
						value = "<P align='center'>TOTAL SOLICITUDES";
						return value;
					}else{
						return (record.get('Descripcion'));
					}
				}
			},
			{
				header: 'TOTAL',
				dataIndex: 'Total',
				sortable: false,
				resiazable: false,
				align: 'center',
				width: 100
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 161,
		width: 397,
		title: '',
		frame: true
	});
	
	
	var elementoTitulo = [
		{
			xtype: 'label',
			id: 'lblTitulo',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;width:400px;height:30px;',
			fieldLabel: '',
			text: 'RESUMEN DE MOVIMIENTOS'
		}
	];
	
	var fp = new Ext.FormPanel({
		id: 'forma',
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: false,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		labelWidth: 0,
		defaultType: 'textfield',
		height:	20,
		width: 400,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		items: elementoTitulo,
		monitorValid: false
	});
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			NE.util.getEspaciador(15),
			fp,
			NE.util.getEspaciador(15),
			gridContratoCesion,
			NE.util.getEspaciador(15),
			gridNotificaciones, 
			NE.util.getEspaciador(15),
			gridExtincionContrato,
			NE.util.getEspaciador(15)
		]
	});
	

	consultaPrincipalData.load();
	
	
});