<%@ page language="java"%>
<%@ page import=" 
	netropology.utilerias.*,	
	com.netro.cesion.*,			
	netropology.utilerias.ServiceLocator,	
	com.netro.model.catalogos.*,
	java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/34cesion/34secsession_ext.jspf"%>
<%  
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion	=	(request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";

String infoRegresar	=	"", consulta  ="";
JSONObject 	jsonObj	= new JSONObject();

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

if(informacion.equals("ConsultaPrincipal")) {

	List registros = cesionBean.monitorCesionDerechos(strTipoUsuario, iNoCliente);
	
	String  regestatus = "",  estatus = "",  numero = "";					
	int posicion = 0, longitud = 0;
	int solicAceptadas =0, aceptadasPyme =0,aceptadasIF =0, rechazadoIF=0;
	int noAcepVentanilla =0,  notiAceptada =0,	notiAceptadaVent=0, notRechazadas=0;
	int extSolicitada  =0, extAceptadaIF =0,	extNotificacionAcept =0,  extincionAceptadaNotificada =0, extincionRedirecAplic =0,  canceladaNafin =0; 
	int   rechazadaExpirada = 0;
	for (int i = 0; i < registros.size(); i++) {
		regestatus = ((registros.get(i))==null)?"":(registros.get(i).toString());
		longitud = regestatus.length();
		posicion =regestatus.indexOf("e");
		estatus = regestatus.substring(0, posicion);
		numero = regestatus.substring(posicion + 1, longitud);
	
		if (estatus.equals("3") ) {
			solicAceptadas = (Integer.parseInt(numero));								
		} else if (estatus.equals("7")) {
			aceptadasPyme = (Integer.parseInt(numero));
		}	else if (estatus.equals("9")) {
			aceptadasIF = (Integer.parseInt(numero));
		}	else if (estatus.equals("10")) {						
			rechazadoIF = (Integer.parseInt(numero));
		}	else if (estatus.equals("15")) {						
			noAcepVentanilla = (Integer.parseInt(numero));
		}	else if (estatus.equals("11")) {						
			notiAceptada = (Integer.parseInt(numero));		
		}	else if (estatus.equals("16")) {						
			notiAceptadaVent = (Integer.parseInt(numero));
		}	else if (estatus.equals("12")) {
			notRechazadas = (Integer.parseInt(numero));
		
		}	else if (estatus.equals("19")) {						
			extSolicitada = (Integer.parseInt(numero));
		}	else if (estatus.equals("20")) {
			extAceptadaIF = (Integer.parseInt(numero));
		} else if (estatus.equals("21")) {
			extincionAceptadaNotificada = (Integer.parseInt(numero));
		} else if (estatus.equals("22")) {
			extincionRedirecAplic = (Integer.parseInt(numero));
		} else if (estatus.equals("23")) {
			extNotificacionAcept = (Integer.parseInt(numero));
		}	else if (estatus.equals("24")) {	 //Cancelada por  Nafin  F023-2015
				canceladaNafin = (Integer.parseInt(numero));
		}	else if (estatus.equals("25")) {	 //Rechazada Expirada  F023-2015
			rechazadaExpirada = (Integer.parseInt(numero));			 
		}
	}
		
	int totalContratoCesion = solicAceptadas + aceptadasPyme +aceptadasIF + rechazadoIF;
	int totalNotificaciones = noAcepVentanilla + notiAceptada +  notiAceptadaVent + notRechazadas;
	int totalExtincion = extSolicitada  + extAceptadaIF + extNotificacionAcept + extincionAceptadaNotificada + extincionRedirecAplic;
			
	JSONObject jsonObj0 = new JSONObject();
		jsonObj0.put("Descripcion","CONTRATOS DE CESION");
		jsonObj0.put("Total", new Integer(totalContratoCesion));			
	JSONObject jsonObj1 = new JSONObject();
		jsonObj1.put("Descripcion","SOLICITUDES ACEPTADAS");
		jsonObj1.put("Total", new Integer(solicAceptadas));
	JSONObject jsonObj2 = new JSONObject();
		jsonObj2.put("Descripcion", "ACEPTADOS PYME");
		jsonObj2.put("Total", new Integer(aceptadasPyme)); 
	JSONObject jsonObj3 = new JSONObject();
		jsonObj3.put("Descripcion","ACEPTADOS IF");
		jsonObj3.put("Total", new Integer(aceptadasIF));
	JSONObject jsonObj4 = new JSONObject();
		jsonObj4.put("Descripcion","RECHADADOS  IF");
		jsonObj4.put("Total", new Integer(rechazadoIF));
	
	JSONObject jsonObj17 = new JSONObject(); //F023-2015
		jsonObj17.put("Descripcion", "CANCELADA POR NAFIN");
		jsonObj17.put("Total", new Integer(canceladaNafin));
		
			
	JSONObject jsonObj5 = new JSONObject();
		jsonObj5.put("Descripcion","NOTIFICACIONES");
		jsonObj5.put("Total", new Integer(totalNotificaciones));				
	JSONObject jsonObj6 = new JSONObject();
		jsonObj6.put("Descripcion","NO ACEPTADOS POR VENTANILLA"); 
		jsonObj6.put("Total", new Integer(noAcepVentanilla));
	JSONObject jsonObj7 = new JSONObject();
		jsonObj7.put("Descripcion","NOTIFICACIONES ACEPTADAS");
		jsonObj7.put("Total", new Integer(notiAceptada));
	JSONObject jsonObj8 = new JSONObject();
		jsonObj8.put("Descripcion","NOTIFICADO VENTANILLA");
		jsonObj8.put("Total", new Integer(notiAceptadaVent));
	JSONObject jsonObj9 = new JSONObject();
		jsonObj9.put("Descripcion","RECHAZADAS");
		jsonObj9.put("Total", new Integer(notRechazadas));
	JSONObject jsonObj16 = new JSONObject();   //F023-2015
		jsonObj16.put("Descripcion","RECHAZADA EXPIRADA");
		jsonObj16.put("Total", new Integer(rechazadaExpirada));
		
		
	JSONObject jsonObj10 = new JSONObject();		
		jsonObj10.put("Descripcion","EXTINCI&Oacute;N DE CONTRATO");
		jsonObj10.put("Total", new Integer(totalExtincion));
	JSONObject jsonObj11 = new JSONObject();
		jsonObj11.put("Descripcion","SOLICITADA");
		jsonObj11.put("Total", new Integer(extSolicitada));
	JSONObject jsonObj12 = new JSONObject();
		jsonObj12.put("Descripcion","ACEPTADA IF");
		jsonObj12.put("Total", new Integer(extAceptadaIF));
	JSONObject jsonObj13 = new JSONObject();
		jsonObj13.put("Descripcion","NOTIFICACIONES ACEPTADAS");
		jsonObj13.put("Total", new Integer(extNotificacionAcept));
	JSONObject jsonObj14 = new JSONObject();
		jsonObj14.put("Descripcion","NOTIFICADO VENTANILLA");
		jsonObj14.put("Total", new Integer(extincionAceptadaNotificada));
	JSONObject jsonObj15 = new JSONObject();
		jsonObj15.put("Descripcion","REDIRECCIONAMIENTO APLICADO");
		jsonObj15.put("Total", new Integer(extincionRedirecAplic));
		
	JSONArray jsonArray = new JSONArray();
	jsonArray.add(jsonObj0);
	jsonArray.add(jsonObj1);
	jsonArray.add(jsonObj2);
	jsonArray.add(jsonObj3);
	jsonArray.add(jsonObj4);
	jsonArray.add(jsonObj17); //F23-2015
	jsonArray.add(jsonObj5);
	jsonArray.add(jsonObj6);
	jsonArray.add(jsonObj7);
	jsonArray.add(jsonObj8);
	jsonArray.add(jsonObj9);
	jsonArray.add(jsonObj16); //F23-2015
	
	jsonArray.add(jsonObj10);
	jsonArray.add(jsonObj11);	
	jsonArray.add(jsonObj12);
	jsonArray.add(jsonObj13);
	jsonArray.add(jsonObj14);
	jsonArray.add(jsonObj15);
		
	infoRegresar =	"{\"success\": true, \"total\":" + jsonArray.size() + ", \"registros\": " + jsonArray.toString()+"}";

}else  if(informacion.equals("Consulta")) {

	int start=  0, limit  =0;
	String estatus	=	(request.getParameter("estatus")!=null)?request.getParameter("estatus"):"";
	
	ConsulSolicitudConsentimiento paginador = new com.netro.cesion.ConsulSolicitudConsentimiento();
	paginador.setClaveIfCesionario(iNoCliente);	
	paginador.setClaveEstatusSolicitud(estatus);	
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if (operacion.equals("Generar") )  {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));		
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
		try {
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		
			Registros reg = queryHelper.getPageResultSet(request,start,limit);
			while(reg.next()){
				
				//Se obtiene el Monto Moneda
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(reg.getString("clave_solicitud"));
				reg.setObject("MONTO_MONEDA",montosMonedaSol.toString());	
				String  cedente = reg.getString("NOMBRE_CEDENTE");
				reg.setObject("NOMBRE_CEDENTE",cedente.replaceAll(";", "<br>"));
				
			
			}//while(reg.next()){
			consulta = 	"{\"success\": true, \"total\": \""+ queryHelper.getIdsSize() +"\", \"registros\": " + reg.getJSONData()+"}";
		
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		jsonObj = new JSONObject();
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("estatus",estatus);		
		infoRegresar = jsonObj.toString();  
	}

}else  if(informacion.equals("CONTRATO")) {
	try {
		String 			nombreArchivo = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
					
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo de Contrato", e);
	}

}else  if(informacion.equals("CONTRATO_CESION_IF")) {
		   		
	HashMap parametros = new HashMap();
	parametros.put("clave_solicitud", clave_solicitud);	
	parametros.put("tipo_archivo", "CONTRATO_CESION_IF");
	parametros.put("strDirectorioTemp", strDirectorioTemp);
	parametros.put("pais", (String)session.getAttribute("strPais"));
	parametros.put("noCliente", "");
	parametros.put("nombre", (String)session.getAttribute("strNombre"));
	parametros.put("nombreUsr", (String)session.getAttribute("strNombreUsuario"));
	parametros.put("logo", (String)session.getAttribute("strLogo"));
	parametros.put("strDirectorioPublicacion", strDirectorioPublicacion);
	
   String nombreArchivo =cesionBean.archConCesionIF (parametros);
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();  
 
}
%>
<%=infoRegresar%>
