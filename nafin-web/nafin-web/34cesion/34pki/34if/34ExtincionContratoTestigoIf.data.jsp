<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.text.SimpleDateFormat,
		netropology.utilerias.*,				
		com.netro.cesion.*, 	
		netropology.utilerias.usuarios.*, 
		com.netro.model.catalogos.*,		
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/34cesion/34secsession_ext.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String clave_pyme = (request.getParameter("clave_pyme")!=null)?request.getParameter("clave_pyme"):"";
String clave_contratacion = (request.getParameter("clave_contratacion")!=null)?request.getParameter("clave_contratacion"):"";
String clave_moneda = (request.getParameter("clave_moneda")!=null)?request.getParameter("clave_moneda"):"";
String numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String tipoArchivo = (request.getParameter("tipoArchivo") != null) ? request.getParameter("tipoArchivo") : "";
String clave_solicitud = (request.getParameter("clave_solicitud") != null) ? request.getParameter("clave_solicitud") : "";
String fechaCarga = (request.getParameter("fechaCarga") != null) ? request.getParameter("fechaCarga") : "";
String horaCarga = (request.getParameter("horaCarga") != null) ? request.getParameter("horaCarga") : "";
String nuevoEstatus = (request.getParameter("nuevoEstatus")!=null)?request.getParameter("nuevoEstatus"):"";
String pantalla = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";
String acuse = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
String fecha_inicio = (request.getParameter("fecha_inicio")!=null)?request.getParameter("fecha_inicio"):"";
String fecha_final = (request.getParameter("fecha_final")!=null)?request.getParameter("fecha_final"):"";
String login_usuario = iNoUsuario;
String nombre_usuario = strNombreUsuario;	
int start= 0, limit =0, indiceCamposAdicionales =0;
String infoRegresar	=	"", consulta ="", clasificacionEpo ="",  nombreIfCesionario ="",
campo01 ="", campo02 ="", campo03 ="", campo04 ="", campo05="";


JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

if(!clave_epo.equals(""))  {
	clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
	Vector lovDatos = cesionBean.getCamposAdicionales(clave_epo, "5");	
	indiceCamposAdicionales = lovDatos.size();
	for(int i = 0; i < indiceCamposAdicionales; i++){
		List campos =(List)lovDatos.get(i);
		if(i==0) 	campo01=  campos.get(1).toString();
		if(i==1) 	campo02=  campos.get(1).toString();
		if(i==2) 	campo03=  campos.get(1).toString();
		if(i==3) 	campo04=  campos.get(1).toString();
		if(i==4) 	campo05=  campos.get(1).toString();			
	}	
}

CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();

 if(informacion.equals("Consultar") || informacion.equals("ConsultarPreAcuse")  || informacion.equals("ConsultarAcusePDF")  ){
	
	String existeRepIf2 = cesionBean.getIfRepresentante2(iNoCliente);

	ConsultaContratosExtincionTestigoIf  paginador  = new ConsultaContratosExtincionTestigoIf();
	paginador.setClavePyme(clave_pyme);
	paginador.setClaveEpo(clave_epo);
	paginador.setClaveTipoContratacion(clave_contratacion);
	paginador.setNumeroContrato(numero_contrato);
	paginador.setClaveMoneda(clave_moneda);
	paginador.setClaveIfCesionario(iNoCliente);
	paginador.setClave_solicitud(clave_solicitud);
	paginador.setAcuse(acuse);
	paginador.setFechaCarga(fechaCarga);
	paginador.setHoraCarga(horaCarga);
	paginador.setUsuario(nombre_usuario);
	paginador.setEstatus(nuevoEstatus); 
	paginador.setFechaInicio(fecha_inicio);
	paginador.setFechaFin(fecha_final);
	paginador.setRepIf2(existeRepIf2);
	
	queryHelper = new CQueryHelperRegExtJS(paginador);  		
}

if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setCs_cesion_derechos("S"); 
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoPYME")){

	CatalogoPymeExtincion cat = new CatalogoPymeExtincion();
	cat.setCampoClave("DISTINCT PYME.IC_PYME");
	cat.setCampoDescripcion("PYME.CG_RAZON_SOCIAL");		
	cat.setClaveIf(iNoCliente);  
	cat.setOrden("PYME.CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();  

}else if(informacion.equals("CatalogoContratacion")  && !clave_epo.equals("") ){
	
	List contratacion=  cesionBean.obtenerComboTipoContratacion(clave_epo);
	registros = new JSONArray();
	registros = registros.fromObject(contratacion);
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();


}else if(informacion.equals("CatalogoMoneda") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setOrden("ic_moneda");
	cat.setValoresCondicionIn("0,1,54,25", Integer.class);
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("Consultar") ){

	 HashMap parametrosConsulta = new HashMap();
    parametrosConsulta.put("loginUsuario", strLogin);
    parametrosConsulta.put("clavePyme", clave_pyme);
    parametrosConsulta.put("claveEpo", clave_epo);
    parametrosConsulta.put("claveIfCesionario", iNoCliente);
    parametrosConsulta.put("numeroContrato", numero_contrato);
    parametrosConsulta.put("claveMoneda", clave_moneda);
    parametrosConsulta.put("claveTipoContratacion", clave_contratacion);
    parametrosConsulta.put("estatusSolicitud", "19");
    parametrosConsulta.put("perfilUsuario", "ADMIN TEST");
    HashMap solicitudesAutorizadasPorUsuario = cesionBean.obtenerExtincionesPorUsuario(parametrosConsulta);
	 int numSolAut = Integer.parseInt((String)solicitudesAutorizadasPorUsuario.get("numeroRegistros"));	
	
	if (operacion.equals("Generar") || operacion.equals("ArchivoPDF")  ) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	}
	
	if (operacion.equals("Generar")) {
		try {
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		
			Registros reg = queryHelper.getPageResultSet(request,start,limit);
			while(reg.next()){
				//FODEA-024-2014 MOD()
				String nombreCedente = reg.getString("nombre_cedente");
				String cedente[] = nombreCedente.split(";");
				if(cedente.length == 1){
					nombreCedente = nombreCedente.replaceAll(";","");
					reg.setObject("NOMBRE_CEDENTE",nombreCedente.toString());				
				}else{
					nombreCedente = nombreCedente.replaceAll(";","<br>");
					reg.setObject("NOMBRE_CEDENTE",nombreCedente.toString());				
				}
				//
				//Se obtiene el Monto Moneda
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(reg.getString("clave_solicitud"));
				reg.setObject("MONTO_MONEDA",montosMonedaSol.toString());				
			
				//Se obtiene el representante legal de la Pyme
				String noProveedor = (reg.getString("clave_pyme")==null)?"":(reg.getString("clave_pyme"));
						
				StringBuffer nombreContacto = new StringBuffer();
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
				for(int i=0;i<usuariosPorPerfil.size();i++){
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true; 
					if(i>0){
						nombreContacto.append(" / ");
					}
					nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
					nombreContacto.append("\n");
				}	
				reg.setObject("REPRESENTATE_LEGAL",nombreContacto.toString());							
				
				// para  las condiciones de Columna Accion
				String claveEstatus = reg.getString("clave_estatus");
				String aceptada_if = reg.getString("aceptada_if");
				
				if (claveEstatus.equals("19") && aceptada_if.equals("S") ) {
					String recordFound = "N";
					for (int i = 0; i < numSolAut; i++) {
						if (reg.getString("clave_solicitud").equals(solicitudesAutorizadasPorUsuario.get("numeroSolicitud" + i).toString())) {
							recordFound = "S";							
                  }
               }	
					if (recordFound.equals("S")) {
						reg.setObject("COLUMNA_ACCION","N");	
					} else if (recordFound.equals("N")) {
						reg.setObject("COLUMNA_ACCION","S");
					}
				} else {
					reg.setObject("COLUMNA_ACCION","N");
				}
				
			}//while(reg.next()){
			consulta = 	"{\"success\": true, \"total\": \""+ queryHelper.getIdsSize() +"\", \"registros\": " + reg.getJSONData()+"}";
		
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("clasificacionEpo", clasificacionEpo);	
		jsonObj.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
		jsonObj.put("campo01",campo01);
		jsonObj.put("campo02",campo02);
		jsonObj.put("campo03",campo03);
		jsonObj.put("campo04",campo04);
		jsonObj.put("campo05",campo05);
		jsonObj.put("clave_epo",clave_epo);		
		
	} else if (operacion.equals("ArchivoPDF")  ) {	
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, tipoArchivo);			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		
	} else if (operacion.equals("ArchivoCSV")  ) {	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}				
	} 
	
	infoRegresar = jsonObj.toString();  

} else if (informacion.equals("CONTRATO")  ) {	
	try {
		String 			nombreArchivo = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);			
		String nombreArchivo2 = nombreArchivo.substring(nombreArchivo.lastIndexOf("on/")+3, nombreArchivo.length());	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo2);
		infoRegresar = jsonObj.toString();
					
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo de Contrato", e);
	}	


}else if(informacion.equals("CONTRATO_CESION_PYME")  ){
   		
	List parametros = new ArrayList();
	parametros.add((String)session.getAttribute("strPais"));
	parametros.add(iNoCliente);
	parametros.add((String)session.getAttribute("sesExterno"));
	parametros.add((String) session.getAttribute("strNombre"));
	parametros.add((String) session.getAttribute("strNombreUsuario"));
	parametros.add((String)session.getAttribute("strLogo"));
	parametros.add(strDirectorioTemp);
	parametros.add((String) application.getAttribute("strDirectorioPublicacion"));
	parametros.add(tipoArchivo);
	parametros.add(clave_solicitud);
	parametros.add(clave_epo);
	parametros.add(iNoCliente);
		
	String nombreArchivo = cesionBean.DescargaContratoCesionPyme(parametros);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();  

}else if(informacion.equals("ConsultarPreAcuse")  ){
	
	try {
		Registros reg	=	queryHelper.doSearch();
		
		while(reg.next()){
			//FODEA-024-2014 MOD()
				String nombreCedente = reg.getString("nombre_cedente");
				String cedente[] = nombreCedente.split(";");
				if(cedente.length == 1){
					nombreCedente = nombreCedente.replaceAll(";","");
					reg.setObject("NOMBRE_CEDENTE",nombreCedente.toString());				
				}else{
					nombreCedente = nombreCedente.replaceAll(";","<br>");
					reg.setObject("NOMBRE_CEDENTE",nombreCedente.toString());				
				}
				//
			//Se obtiene el Monto Moneda
			StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(reg.getString("clave_solicitud"));
			reg.setObject("MONTO_MONEDA",montosMonedaSol.toString());				
			
			//Se obtiene el representante legal de la Pyme
			String noProveedor = (reg.getString("clave_pyme")==null)?"":(reg.getString("clave_pyme"));
						
			StringBuffer nombreContacto = new StringBuffer();
			UtilUsr utilUsr = new UtilUsr();
			boolean usuarioEncontrado = false;
			boolean perfilIndicado = false;
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
			for(int i=0;i<usuariosPorPerfil.size();i++){
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
				Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
				perfilIndicado = true; 
				if(i>0){
					nombreContacto.append(" / ");
				}
				nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
				nombreContacto.append("\n");
			}	
			reg.setObject("REPRESENTATE_LEGAL",nombreContacto.toString());							
		}	
		
		consulta = 	"{\"success\": true, \"total\": \""+ reg.getNumeroRegistros() +"\", \"registros\": " + reg.getJSONData()+"}";
		
	}	catch(Exception e) {
		throw new AppException("Error en la paginación", e);
	}	
		
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("clasificacionEpo", clasificacionEpo);	
	jsonObj.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
	jsonObj.put("campo01",campo01);
	jsonObj.put("campo02",campo02);
	jsonObj.put("campo03",campo03);
	jsonObj.put("campo04",campo04);
	jsonObj.put("campo05",campo05);	
	jsonObj.put("pantalla",pantalla);
	jsonObj.put("clave_epo",clave_epo);	
	infoRegresar = jsonObj.toString();  
	
}else if(informacion.equals("GenerarAcuse")  ){
	%><%@ include file="../certificado.jspf" %><%
	String textoFirmar = (request.getParameter("textoFirmar")!=null)?request.getParameter("textoFirmar"):"";
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
	
	String folioCert =new SimpleDateFormat("ddMMyy").format(new java.util.Date());
	fechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
	horaCarga = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
 
	String causasRechazoExtincion="", 	mensajeAuto ="";
 
	
	HashMap parametrosFirma = new HashMap();
	parametrosFirma.put("_serial", _serial);
	parametrosFirma.put("pkcs7", pkcs7);
	parametrosFirma.put("textoPlano", textoFirmar);
	parametrosFirma.put("validCert", String.valueOf(false));
	parametrosFirma.put("folioCert", folioCert);
	parametrosFirma.put("loginUsuario", login_usuario);
	parametrosFirma.put("nombreUsuario", nombre_usuario);
	parametrosFirma.put("claveSolicitud", clave_solicitud);
	parametrosFirma.put("nuevoEstatus", nuevoEstatus);
	parametrosFirma.put("causasRechazoExtincion", causasRechazoExtincion);
			
	acuse = cesionBean.acuseExtincionContratoTestigo(parametrosFirma);

	if( acuse.equals("")){
		mensajeAuto="<b>La autentificación no se llevó a cabo.<br/>PROCESO CANCELADO</b>";
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("acuse", acuse);
	jsonObj.put("usuario", iNoUsuario+" "+nombre_usuario);
	jsonObj.put("clave_solicitud", clave_solicitud);
	jsonObj.put("operacion", operacion);
	jsonObj.put("nuevoEstatus", nuevoEstatus);
	jsonObj.put("fechaCarga", fechaCarga);
	jsonObj.put("horaCarga", horaCarga);
	jsonObj.put("mensajeAuto", 	mensajeAuto);
	jsonObj.put("clave_epo",clave_epo);	
	infoRegresar = jsonObj.toString();  

}else if (informacion.equals("ConsultarAcusePDF")  ) {	
	
	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo PDF", e);
	}		
	
	infoRegresar = jsonObj.toString(); 	
	
}	

%>
<%=infoRegresar%>