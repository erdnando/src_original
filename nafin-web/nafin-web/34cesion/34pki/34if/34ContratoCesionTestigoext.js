Ext.onReady(function(){

	// generacion de Archivo de Acuse 
	var procesarGenerarPDFAcuse =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFAcuse');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFAcuse');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Atestiguar Acuse 	
	function procesarSuccessAtestiguarAcuse(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData != null){	
				
				if(jsonData.recibo !='') {
				
					var acuseCifras = [
						['N�mero de Acuse', jsonData.acuse],
						['Fecha de Carga ', jsonData.fechaCarga],
						['Hora de Carga', jsonData.horaCarga],
						['Usuario de Captura ', jsonData.usuario]
					];
					
					storeCifrasData.loadData(acuseCifras);		
					Ext.getCmp('btnAtestiguar').hide();   	
					Ext.getCmp('btncancelar').hide();    	
					Ext.getCmp('btnsalir').show();	
					Ext.getCmp('btnGenerarPDFAcuse').show();			
			
					Ext.getCmp('lblSellos').update(jsonData.lblSellos);				
					Ext.getCmp('gridCifrasControl').show();					
					Ext.getCmp('recibo_carga').setValue(jsonData.recibo);
					Ext.getCmp('acuse_carga').setValue(jsonData.acuse);
					Ext.getCmp('fecha_carga').setValue(jsonData.fechaCarga);
					Ext.getCmp('hora_carga').setValue(jsonData.horaCarga);
					Ext.getCmp('clave_solicitud').setValue(jsonData.clave_solicitud);
												
					Ext.getCmp('lblTitulo7').hide();							
						
				}else if(jsonData.recibo =='') {
					Ext.getCmp('PanelFoma').hide();	
					Ext.getCmp('fpBotones').show();											
				}
				
				Ext.getCmp("mensajeAuto").setValue(jsonData.mensajeAuto);					
				Ext.getCmp('mensajeAutorizacion').show();	
				
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 150,
		style: 'margin:0 auto;',		
		frame: true
	});
	
	
	var mensajeAutorizacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutorizacion',							
		width:	'300',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeAuto', 	value: '' }			
		]
	});
		
		
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'150',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{
				xtype: 'button',
				text: 'Regresar',			
				id: 'btnSalirR',		 
				handler: function() {
					window.location = "34ContratoCesionTestigoext.jsp";
				}
			}	
		]
	});
	                                     
	var confirmarAcuse = function(pkcs7, texto_plano,  clave_epo, clave_solicitud , sello_digital  ){
			
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else  {
			Ext.Ajax.request({
				url : '34ContratoCesionTestigoext.data.jsp',
				params : {
					informacion: 'GenerarAcuse',
					clave_epo:clave_epo,
					operacion:"ACUSE",
					clave_solicitud:clave_solicitud,
					sello_digital:sello_digital,
					pkcs7:pkcs7,
					texto_plano:texto_plano				
				}
				,callback: procesarSuccessAtestiguarAcuse
			});
		}
	}
	
	
		//ATESTIGUAR
	var botonAtestiguar = function() {
			
		var clave_solicitud = Ext.getCmp('clave_solicitud').getValue();
		var clave_epo =  Ext.getCmp('clave_epoA').getValue();		
		var sello_digital  = Ext.getCmp('sello_digital').getValue(); 
		
		var  texto_plano = 'CONTRATO DE CESI�N DE DERECHOS \n'+
								 'INFORMACI�N DE LA PYME CEDENTE \n'+							 
								' NOMBRE:| '+ Ext.getCmp('lblNombrePyme').getValue() +' \n'+
								' RFC:| '+ Ext.getCmp('lblRFC').getValue() +' \n'+
								' REPRESENTANTE PYME:| '+ Ext.getCmp('lblRepresentante1').getValue() +' \n'+
								' REPRESENTANTE PYME:| '+ Ext.getCmp('lblRepresentante2').getValue() +' \n'+
								' NUM. PROVEEDOR:| '+ Ext.getCmp('lblNumProveedor').getValue() +' \n'+
								'INFORMACI�N DELCONTRATO \n'+
								' N�MERO DE CONTRATO:| '+ Ext.getCmp('lblNumContrato').getValue() +' \n'+
								' MONTO / MONEDA:| '+ Ext.getCmp('lblMonto_Moneda').getValue() +' \n'+
								' FECHA DE VIGENCIA:| '+ Ext.getCmp('lblFechaVigencia').getValue() +' \n'+
								' PLAZO DEL CONTRATO:| '+ Ext.getCmp('lblPlazoContrato').getValue() +' \n'+
								' TIPO DE CONTRATACI�N:| '+ Ext.getCmp('lblTipoContra').getValue() +' \n'+
								' OBJETO DEL CONTRATO:| '+ Ext.getCmp('lblObjetoContrato').getValue() +' \n'+
								'INFORMACI�N DE LA EMPRESA DE PRIMER ORDEN \n'+								
								' NOMBRE:| '+ Ext.getCmp('lblNombreEmpresa').getValue() +' \n'+
								'INFORMACI�N DEL CESIONARIO (IF) \n'+
								' NOMBRE:| '+ Ext.getCmp('lblCesionarioIF').getValue() +' \n'+
								' BANCO DE DEP�SITO:| '+ Ext.getCmp('lblBancoDeposito').getValue() +' \n'+
								' N�MERO DE CUENTA PARA DEP�SITO:| '+ Ext.getCmp('lblCuenta').getValue() +' \n'+
								' N�MERO DE CUENTA CLABE PARA DEP�SITO:| '+ Ext.getCmp('lblCuentaClabe').getValue() +' \n'+
								'INFORMACI�N DE LA CESI�N DE DERECHOS \n'+	
								' FECHA DE SOLICITUD DE CONSENTIMIENTO:| '+ Ext.getCmp('lblFechaSolic').getValue() +' \n'+
								' FECHA DE CONSENTIMIENTO DE LA EPO:| '+ Ext.getCmp('lblFechaConsent').getValue() +' \n'+
								' PERSONA QUE OTORG� EL CONSENTIMIENTO:| '+ Ext.getCmp('lblPersonaConsent').getValue() +' \n'+
								' FECHA DE FORMALIZACI�N DEL CONTRATO DE CESI�N:| '+ Ext.getCmp('lblFechaFormali').getValue() +' \n'+
								' NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):| '+ Ext.getCmp('lblFirmaPyme1').getValue() +' \n'+
								 'NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):| '+ Ext.getCmp('lblFirmaPyme2').getValue() +' \n'+
								' NOMBRE DEL FIRMANTE DE CESIONARIO (IF):| '+ Ext.getCmp('lblFirmaCesionarioIF').getValue() +' \n'+
								' NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:| '+ Ext.getCmp('lblFirmaTestigo1').getValue() +' \n'+
								' NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:| '+ Ext.getCmp('lblFirmaTestigo2').getValue() +' \n'+								
								' FECHA DE NOTIFICACI�N ACEPTADA POR LA EPO:| '+ Ext.getCmp('lblFechaNotiEPO').getValue() +' \n'+
								' PERSONA QUE ACEPT� LA NOTIFICACI�N:| '+ Ext.getCmp('lblPersonaEPO').getValue() +' \n'+
								' FECHA DE INFORMACI�N A VENTANILLA DE PAGOS:| '+ Ext.getCmp('lblFechaInfVenPag').getValue() +' \n'+
								' FECHA DE RECEPCI�N EN VENTANILLA:| '+ Ext.getCmp('lblFechaRecepVen').getValue() +' \n'+
								' FECHA DE LA APLICACI�N DEL REDIRECCIONAMIENTO:| '+ Ext.getCmp('lblFechaAplicaRedirec').getValue() +' \n'+
								' PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:| '+ Ext.getCmp('lblPerCuentaEPO').getValue() +' \n'+
								'SE SUJETA A LAS SIGUIENTES CL�USULAS \n'+
								 Ext.getCmp('clausulado_parametrizado').getValue() +' \n'+
								 Ext.getCmp('firma1').getValue() +' \n'+
								 Ext.getCmp('firma2').getValue() +' \n'+
								 Ext.getCmp('firma3').getValue() +' \n'+
								 Ext.getCmp('firma4').getValue() +' \n'+
								 Ext.getCmp('firma5').getValue() +' \n'+
								Ext.getCmp('sello_digital').getValue() +' \n';
									
	
		
		NE.util.obtenerPKCS7(confirmarAcuse, texto_plano, clave_epo , clave_solicitud , sello_digital   );	
		
	
	}
	

	function procesarSuccessAtestiguar(opts, success, response){
		
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValores = Ext.util.JSON.decode(response.responseText);
			if(jsonValores != null){	
			
			Ext.getCmp('forma').hide();
			Ext.getCmp('mensajeConsulta').hide();
			Ext.getCmp('gridConsulta').hide();
			Ext.getCmp('PanelFoma').show();
			var hayCamposAdicionales  =jsonValores.hayCamposAdicionales;
			var operacion  =jsonValores.operacion;
			
			if(operacion=='PREACUSE') {
				Ext.getCmp('btnAtestiguar').show();   	Ext.getCmp('btncancelar').show();    	Ext.getCmp('btnsalir').hide();			
			}
			
			Ext.getCmp('lblNombrePyme').update(jsonValores.lblNombrePyme);
			Ext.getCmp('lblRFC').update(jsonValores.lblRFC);
			Ext.getCmp('lblRepresentante1').update(jsonValores.lblRepresentante1);
			Ext.getCmp('lblRepresentante2').update(jsonValores.lblRepresentante2);
			Ext.getCmp('lblNumProveedor').update(jsonValores.lblNumProveedor);
			Ext.getCmp('lblNumContrato').update(jsonValores.lblNumContrato);
			Ext.getCmp('lblMonto_Moneda').update(jsonValores.lblMonto_Moneda);
			Ext.getCmp('lblFechaVigencia').update(jsonValores.lblFechaVigencia);
			Ext.getCmp('lblPlazoContrato').update(jsonValores.lblPlazoContrato);
			Ext.getCmp('lblTipoContra').update(jsonValores.lblTipoContra);
			Ext.getCmp('lblObjetoContrato').update(jsonValores.lblObjetoContrato);		
			Ext.getCmp('lblNombreEmpresa').update(jsonValores.lblNombreEmpresa);
			Ext.getCmp('lblCesionarioIF').update(jsonValores.lblCesionarioIF);
			Ext.getCmp('lblBancoDeposito').update(jsonValores.lblBancoDeposito);
			Ext.getCmp('lblCuenta').update(jsonValores.lblCuenta);
			Ext.getCmp('lblCuentaClabe').update(jsonValores.lblCuentaClabe);
			Ext.getCmp('lblFechaSolic').update(jsonValores.lblFechaSolic);
			Ext.getCmp('lblFechaConsent').update(jsonValores.lblFechaConsent);
			Ext.getCmp('lblPersonaConsent').update(jsonValores.lblPersonaConsent);
			Ext.getCmp('lblFechaFormali').update(jsonValores.lblFechaFormali);
			Ext.getCmp('lblFirmaPyme1').update(jsonValores.lblFirmaPyme1);
			Ext.getCmp('lblFirmaPyme2').update(jsonValores.lblFirmaPyme2);
			Ext.getCmp('lblFirmaCesionarioIF').update(jsonValores.lblFirmaCesionarioIF);
			Ext.getCmp('lblFirmaTestigo1').update(jsonValores.lblFirmaTestigo1);
			Ext.getCmp('lblFirmaTestigo2').update(jsonValores.lblFirmaTestigo2);			
			Ext.getCmp('lblFechaNotiEPO').update(jsonValores.lblFechaNotiEPO);
			Ext.getCmp('lblPersonaEPO').update(jsonValores.lblPersonaEPO);
			Ext.getCmp('lblFechaInfVenPag').update(jsonValores.lblFechaInfVenPag);
			Ext.getCmp('lblFechaRecepVen').update(jsonValores.lblFechaRecepVen);
			Ext.getCmp('lblFechaAplicaRedirec').update(jsonValores.lblFechaAplicaRedirec);
			Ext.getCmp('lblPerCuentaEPO').update(jsonValores.lblPerCuentaEPO);			
			Ext.getCmp('lblSellos').update(jsonValores.lblSellos);		
			Ext.getCmp("clave_solicitud").setValue(jsonValores.clave_solicitud);			
			Ext.getCmp("sello_digital").setValue(jsonValores.sello_digital);
			Ext.getCmp("clave_epoA").setValue(jsonValores.clave_epo);		
			Ext.getCmp("clausulado_parametrizado").setValue(jsonValores.clausulado_parametrizado);	
			Ext.getCmp("firma1").setValue(jsonValores.firma1);	
			Ext.getCmp("firma2").setValue(jsonValores.firma2);	
			Ext.getCmp("firma3").setValue(jsonValores.firma3);	
			Ext.getCmp("firma4").setValue(jsonValores.firma4);	
			Ext.getCmp("firma5").setValue(jsonValores.firma5);	
									
			var formaAcuse= Ext.getCmp('PanelFoma');
			var campo_01 =  Ext.decode(jsonValores.campo_01);					
			var campo_02 =  Ext.decode(jsonValores.campo_02);					
			var campo_03 =  Ext.decode(jsonValores.campo_03);	
			var campo_04 =  Ext.decode(jsonValores.campo_04);
			var campo_05 =  Ext.decode(jsonValores.campo_05);	
			
			for(var i=1; i<=hayCamposAdicionales&&false; i++) { //Se quitan de la forma del preacuse
				if(i==1) {
					var indice1 = formaAcuse.items.indexOfKey('lblObjetoContrato')+1;
					formaAcuse.insert(indice1,campo_01);			
				}
				if(i==2) {	
					var indice1 = formaAcuse.items.indexOfKey('lblCampo1')+1;
					formaAcuse.insert(indice1,campo_02);
				}
				if(i==3) {
					var indice1 = formaAcuse.items.indexOfKey('lblCampo2')+1;
					formaAcuse.insert(indice1,campo_03);
				}
				if(i==4) {
					var indice1 = formaAcuse.items.indexOfKey('lblCampo3')+1;
					formaAcuse.insert(indice1,campo_04);
				}
				if(i==5) {
					var indice1 = formaAcuse.items.indexOfKey('lblCampo4')+1;
					formaAcuse.insert(indice1,campo_05);
				}
			}			
			formaAcuse.doLayout();
					
						
			fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	var procesoAtestiguar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');
		
		Ext.Ajax.request({
			url : '34ContratoCesionTestigoext.data.jsp',
			params : {
				informacion: 'PreAcuse_Atestiguar',
				clave_solicitud:clave_solicitud,
				clave_epo:clave_epo,
				operacion:'PREACUSE'
			}
			,callback: procesarSuccessAtestiguar
		});
				
	}

	// *** Forma del PreAcuse y Acuse 
	var  elementosFormaPanel  =[					
	{
			xtype: 'label',
			id: 'lblSellos',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			text: ''
		},
		{
			xtype: 'label',
			id: 'lblTitulo0',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden:true,
			text: 'CONTRATO DE CESI�N DE DERECHOS'
		},			
		{
			xtype: 'label',
			id: 'lblTitulo1',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden:true,
			text: 'INFORMACI�N DE LA PYME CEDENTE'
		},		
		{
			xtype: 'displayfield',
			id: 'lblNombrePyme',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'NOMBRE',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblRFC',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'RFC',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblRepresentante1',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'REPRESENTANTE PYME',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblRepresentante2',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'REPRESENTANTE PYME',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblNumProveedor',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'NUM. PROVEEDOR',
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTitulo2',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden:true,
			text: 'INFORMACI�N DEL CONTRATO'
		},
		{
			xtype: 'displayfield',
			id: 'lblNumContrato',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'N�MERO DE CONTRATO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblMonto_Moneda',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'MONTO / MONEDA',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFechaVigencia',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'FECHA DE VIGENCIA',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblPlazoContrato',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'PLAZO DEL CONTRATO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblTipoContra',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'TIPO DE CONTRATACI�N',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblObjetoContrato',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'OBJETO DEL CONTRATO',
			text: '-'
		},			
		
		{
			xtype: 'label',
			id: 'lblTitulo3',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden:true,
			text: 'INFORMACI�N DE LA EMPRESA DE PRIMER ORDEN'
		},		
		{
			xtype: 'displayfield',
			id: 'lblNombreEmpresa',
			style: 'text-align:left;',
			fieldLabel: 'NOMBRE',
			hidden:true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTitulo4',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden:true,
			text: 'NFORMACI�N DEL CESIONARIO (IF)'
		},	
		{
			xtype: 'displayfield',
			id: 'lblCesionarioIF',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'NOMBRE',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblBancoDeposito',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'BANCO DE DEP�SITO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblCuenta',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'N�MERO DE CUENTA PARA DEP�SITO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblCuentaClabe',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'N�MERO DE CUENTA CLABE PARA DEP�SITO',
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTitulo5',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden:true,
			text: 'INFORMACI�N DE LA CESI�N DE DERECHOS'
		},	
		{
			xtype: 'displayfield',
			id: 'lblFechaSolic',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'FECHA DE SOLICITUD DE CONSENTIMIENTO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFechaConsent',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'FECHA DE CONSENTIMIENTO DE LA EPO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblPersonaConsent',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'PERSONA QUE OTORG� EL CONSENTIMIENTO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFechaFormali',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'FECHA DE FORMALIZACI�N DEL CONTRATO DE CESI�N',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFirmaPyme1',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME)',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFirmaPyme2',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME)�',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFirmaCesionarioIF',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'NOMBRE DEL FIRMANTE DE CESIONARIO (IF)',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFirmaTestigo1',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFirmaTestigo2',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFechaNotiEPO',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'FECHA DE NOTIFICACI�N ACEPTADA POR LA EPO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblPersonaEPO',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'PERSONA QUE ACEPT� LA NOTIFICACI�N',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFechaInfVenPag',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'FECHA DE INFORMACI�N A VENTANILLA DE PAGOS',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFechaRecepVen',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'FECHA DE RECEPCI�N EN VENTANILLA',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblFechaAplicaRedirec',
			style: 'text-align:left;',
			hidden:true,
			fieldLabel: 'FECHA DE LA APLICACI�N DEL REDIRECCIONAMIENTO',
			text: '-'
		},
		{
			xtype: 'displayfield',
			id: 'lblPerCuentaEPO',
			hidden:true,
			style: 'text-align:left;',
			fieldLabel: 'PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO',
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTitulo6',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden:true,
			text: 'SE SUJETA A LAS SIGUIENTES CL�USULAS'
		},						
		{
			xtype: 'label',
			id: 'lblTitulo7',
			hidden:true,
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			text: 'Por instrucciones de la Pyme, notificamos a PEMEX el Contrato de Cesi�n de Derechos.'
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'sello_digital', 	value: '' }	,		
		{ 	xtype: 'textfield',  hidden: true, id: 'clave_solicitud', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'clave_epoA', 	value: '' },		
		{ 	xtype: 'textfield',  hidden: true, id: 'recibo_carga', 	value: '' },		
		{ 	xtype: 'textfield',  hidden: true,  id: 'acuse_carga', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true,  id: 'fecha_carga', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'hora_carga', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'clausulado_parametrizado', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'firma1', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'firma2', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'firma3', 	value: '' },	
		{ 	xtype: 'textfield',  hidden: true, id: 'firma4', 	value: '' },		
		{ 	xtype: 'textfield',  hidden: true, id: 'firma5', 	value: '' }
		

	];
		

	var PanelFoma = new Ext.FormPanel({
		id: 'PanelFoma',
		width: 900,
		style: ' margin:0 auto;',	
		title: '',
		hidden: true,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 350,
		defaultType: 'textfield',
		items: elementosFormaPanel	,	
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		monitorValid: false,
		autoScroll: true,
		buttons: [
			{
				text: 'Atestiguar',
				iconCls: 'autorizar',
				formBind: true,
				hidden: true,
				id: 'btnAtestiguar',
				handler: botonAtestiguar
			},			
			{
				text: 'Cancelar',
				id: 'btncancelar',
				 hidden: true,
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34ContratoCesionTestigoext.jsp';
				}
			},			
			{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDFAcuse',
			   hidden: true,
				handler: function(boton, evento) {
					
					var recibo_carga =Ext.getCmp('recibo_carga').getValue();
					var fecha_carga =Ext.getCmp('fecha_carga').getValue();
					var hora_carga =Ext.getCmp('hora_carga').getValue();
					var clave_solicitud =Ext.getCmp('clave_solicitud').getValue();		
					
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '34ContratoCesionTestigoext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ARCHIVO_ACUSE',						
						clave_solicitud:clave_solicitud,
						recibo_carga:recibo_carga,
						fecha_carga:fecha_carga,
						hora_carga:hora_carga
						}),
						callback: procesarGenerarPDFAcuse
					});
					
				}			
			},	
			{
				xtype: 'button',
				text: 'Bajar PDF',
				id: 'btnBajarPDFAcuse',
				hidden: true
			}, 
			{
				text: 'Salir',
				id: 'btnsalir',
				 hidden: true,
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34ContratoCesionTestigoext.jsp';
				}
			}
		]
	});
		
		
//*********************CONTRATO DE CESION IF ********************************

//Muestra el archivo de la Columna Contrato de Cesion 
	var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '../34pyme/34ContratoCesionPDFCSV.jsp',
			params: Ext.apply({				
				informacion: 'CONTRATO_CESION_PYME', 
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}


	function procesarGenerarPDFContratoCecionIF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var descargaArchivoContratoCecionIF = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
			Ext.Ajax.request({
			url: '34ContratoCesionTestigoext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
			informacion: 'CONTRATO_CESION_IF',
			tipo_archivo:'CONTRATO_CESION_IF',
			clave_solicitud:clave_solicitud
			}),
			callback: procesarGenerarPDFContratoCecionIF
		});		
	}
	
//*********************para ver los poderes de la pyme ********************************

	var VerPoderesPyme = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLAVE_PYME');
		var rfcPyme = registro.get('RFC');	
		var grupo_cesion = registro.get('GRUPO_CESION');	
		var no_contrato = registro.get('NO_CONTRATO');	
		
		var parametros = "clavePyme="+clavePyme+"&rfcPyme="+rfcPyme+"&grupo_cesion="+grupo_cesion+"&no_contrato="+no_contrato;
		
		var ventana = Ext.getCmp('VerPoderesPyme');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				layout: 'fit',
				width: 500,
				height: 200,
				closeAction: 'hide',
				autoDestroy:true,
				closable:false,
				modal:true,
				id: 'VerPoderesPyme',
				closeAction: 'hide',
				items: [					
					PanelVerPoderesPyme
				],
				title: 'Ver Poderes',
				bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{
							xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrar', 
							handler: function(){
								Ext.getCmp('PanelVerPoderesPyme').hide();
								Ext.getCmp('VerPoderesPyme').destroy();
							} 
						}
					]
				}
			}).show();
		}	
		
		var pabelBody = Ext.getCmp('PanelVerPoderesPyme').body;
		var mgr = pabelBody.getUpdater();
		mgr.on('failure', 
			function(el, response) {
				pabelBody.update('');
				NE.util.mostrarErrorResponse(response);
			});		
		mgr.update({
			url: '/nafin/34cesion/34pki/34pyme/34SolicConsVerPoderesPymeExt.jsp?'+parametros,						
			indicatorText: 'Cargando Ver Poderes'
		});					
	}
	
	var PanelVerPoderesPyme = {
		xtype: 'panel',
		id: 'PanelVerPoderesPyme',
		width: 700,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	};
	

//*********************VER CONSENTIMIENTO ********************************
	var verConsentimiento = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_contratacion = registro.get('TIPO_CONTRATACION');
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		var nombre_epo = registro.get('DEPENDENCIA');
		var objeto_contrato = registro.get('OBJETO_CONTRATO');
		var montospormoneda = registro.get('MONTO_MONEDA');
		var plazocontrato = registro.get('PLAZO_CONTRATO');
		var fechasolicitud = registro.get('FECHASOLICITUD');
		var numero_contrato = registro.get('NUMERO_CONTRATO');
		var nombre_if = registro.get('CECIONARIO');
		
		var ventana = Ext.getCmp('verConsentimiento');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 940,
				height: 550,
				closeAction: 'hide',
				autoDestroy:true,
				closable:false,
				modal:true,
				id: 'verConsentimiento',
				items: [					
					Consentimiento
				],
				title: 'Consentimiento',
				bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{
							xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrar', 
							handler: function(){
								Ext.getCmp('Consentimiento').hide();
								Ext.getCmp('verConsentimiento').destroy();
							} 
						}
					]
				}
			}).show();
		}
		
		var bodyPanel = Ext.getCmp('Consentimiento').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure', 
			function(el, response) {
				mgr.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		mgr.update({
			url: '/nafin/34cesion/34pki/34pyme/34ContratoCesionVerConsentimientoext.jsp',	
			scripts: true,
			params: {
				clave_contratacion: clave_contratacion,
				clave_solicitud:clave_solicitud,
				nombre_epo:nombre_epo,
				objeto_contrato:objeto_contrato,
				montospormoneda:montospormoneda,
				plazocontrato:plazocontrato,
				fechasolicitud:fechasolicitud,
				numero_contrato:numero_contrato,
				nombre_if:nombre_if
			},
			indicatorText: 'Cargando Consentimiento'
		});	
	}

	var Consentimiento = {
		xtype: 'panel',
		id: 'Consentimiento',
		width: 650,
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true
	};
	
	//*********************DESCARGA ARCHIVO DE CONTRATO ********************************
		
	function procesarSuccessFailureContrato(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}  
		
	var descargaArchivoContrato = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
			url: '34ContratoCesionTestigoext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContrato
		});
	}
	

//***************************  Consulta ************************

	var procesarGenerarCSVCons =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarCSVCons');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarCSVCons');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDFCons =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFCons');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFCons');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;				
			var  clasificacionEpo = jsonData.clasificacionEpo;	
			
			if(clasificacionEpo !=''){				
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), false);	
			}
			
			Ext.getCmp("btnBajarCSVCons").hide();
			Ext.getCmp("btnBajarPDFCons").hide();						
			Ext.getCmp("mensaje").setValue(jsonData.mensaje);
									
			if(store.getTotalCount() > 0) {	
				//Ext.getCmp("mensajeConsulta").show();
				Ext.getCmp('btnGenerarCSVCons').enable();		
				Ext.getCmp('btnGenerarPDFCons').enable();		
				el.unmask();					
			} else {	
				Ext.getCmp("mensajeConsulta").hide();
				Ext.getCmp('btnGenerarCSVCons').disable();		
				Ext.getCmp('btnGenerarPDFCons').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '34ContratoCesionTestigoext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'DEPENDENCIA'},
			{name: 'PYME_CEDENTE'},
			{name: 'RFC'},
			{name: 'NO_PROVEEDOR'},
			{name: 'FECHA_SOLICITUD'},			
			{name: 'NO_CONTRATO'},			
			{name: 'MONTO_MONEDA'},
			{name: 'TIPO_CONTRATACION'},
			{name: 'FECHA_INI_CONTRA'},
			{name: 'FECHA_FIN_CONTRA'},
			{name: 'PLAZO_CONTRATO'},
			{name: 'CLASIFICACION_EPO'},
			{name: 'FECHA_LIMITE'},
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'CAMPO5'},
			{name: 'VENTANILLA_PAGO'},
			{name: 'SUPERVISOR'},
			{name: 'TELEFONO'},
			{name: 'OBJETO_CONTRATO'},
			{name: 'COMENTARIOS'},
			{name: 'MONTO_CREDITO'},			
			{name: 'REFERENCIA_CREDITO'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'BANCO_DEPOSITO'},
			{name: 'CUENTA'},
			{name: 'CUENTA_CLABE'},
			{name: 'ESTATUS'},
			{name: 'IC_ESTATUS'},
			{name: 'CLAVE_EPO'},
			{name: 'CLAVE_PYME'},
			{name: 'CLAVE_SOLICITUD'},
			{name: 'COLUMNA_ACCION'},
			{name: 'FECHASOLICITUD'},
			{name: 'CECIONARIO'},
			{name: 'FIRMA_CONTRATO'},
			{name: 'GRUPO_CESION'}			

			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: 'Contrato de Cesi�n',
		columns: [
			{
				header: 'Dependencia', 
				tooltip: 'Dependencia',
				dataIndex: 'DEPENDENCIA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Pyme (Cedente)', 
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'PYME_CEDENTE',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
			},
			{
				header: 'RFC', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: ' No Proveedor ', 
				tooltip: 'No Proveedor',
				dataIndex: 'NO_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},			
			{
				header: 'Fecha de Solicitud PyME', 
				tooltip: 'Fecha de Solicitud PyME',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'No. de Contrato', 
				tooltip: 'No. de Contrato',
				dataIndex: 'NO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto/Moneda', 
				tooltip: 'Monto/Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Tipo Contrataci�n', 
				tooltip: 'Tipo Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'F. Inicio Contrato', 
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'FECHA_INI_CONTRA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'F. Final Contrato', 
				tooltip: 'F. Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Plazo del Contrato', 
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'CLASIFICACION_EPO',  //  HAY QUE ACTUALIZAR EL TITULO
				tooltip: 'CLASIFICACION_EPO',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'F. L�mite para Notificaci�n',  //  HAY QUE ACTUALIZAR EL TITULO
				tooltip: 'F. L�mite para Notificaci�n',
				dataIndex: 'FECHA_LIMITE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},			
			{
				header: 'Ventanilla de Pago',  
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENTANILLA_PAGO',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'left'
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra',  
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUPERVISOR',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'left'
			},
			{
				header: 'Tel�fono',  
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'center'
			},
			{
				header: 'Objeto del Contrato',  
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				resizable: true,			
				width: 130,			
				align: 'left'
			},
			{
				header: 'Comentarios',  
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'left'
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	handler: descargaArchivoContrato
					}
				]				
			},	
			{
				header: 'Monto del Cr�dito',  
				tooltip: 'Monto del Cr�dito',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Referencia / No Cr�dito',  
				tooltip: 'Referencia / No Cr�dito',
				dataIndex: 'REFERENCIA_CREDITO',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'center'				
			},
			{
				header: 'Fecha Vencimiento Cr�dito',  
				tooltip: 'Fecha Vencimiento Cr�dito', 
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'center'				
			},
			{
				header: 'Banco de Dep�sito',  
				tooltip: 'Banco de Dep�sito',
				dataIndex: 'BANCO_DEPOSITO',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'center'				
			},
			{
				header: 'Cuenta',  
				tooltip: 'Cuenta',
				dataIndex: 'CUENTA',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'center'				
			},
			{
				header: 'Cuenta CLABE',  
				tooltip: 'Cuenta CLABE',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'center'				
			},
			{
				header: 'Estatus',  
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true,				
				width: 130,			
				align: 'center'				
			},
			{
				xtype: 'actioncolumn',
				header: 'Consentimiento',
				tooltip: 'Consentimiento',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						}
						,	handler: verConsentimiento
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato de Cesi�n',
				tooltip: 'Contrato de Cesi�n',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						}
						,handler: descargarContratoCesion
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Poderes',
				tooltip: 'Poderes',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						}
						,handler: VerPoderesPyme
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('COLUMNA_ACCION') =='N') {
						return 'N/A ';					
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('COLUMNA_ACCION') =='S') {
							this.items[0].tooltip = 'Atestiguar Contrato';
								return 'autorizar';	
							}
						}
						,	handler: procesoAtestiguar
					}					
				]				
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {		
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFCons',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34ContratoCesionTestigoext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF',
								tipoArchivo:'PDF'
							}),
							callback: procesarGenerarPDFCons
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFCons',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVCons',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34ContratoCesionTestigoext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV',
								tipoArchivo:'CSV'
							}),
							callback: procesarGenerarCSVCons
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVCons',
					hidden: true
				}					
			]	
		}
	});


	var mensajeConsulta = new Ext.Container({
		layout: 'table',		
		id: 'mensajeConsulta',							
		width:	'700',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
		{ 	xtype: 'displayfield',  id: 'mensaje', 	value: '' }				
		]
	});
	
//*********************FORMA ********************************

	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
	
	var procesarCatalogoEPOData= function(store, records, oprion){
		if(store.getTotalCount()==0){
		 var clave_epo = Ext.getCmp('clave_epo1');
		 if(clave_epo.getValue()==''){
		  clave_epo.setValue('No existen EPOS con el producto');
		 }
		}
	}
	var procesarCatalogoPymeData= function(store, records, oprion){
		if(store.getTotalCount()==0){
		 var clave_pyme = Ext.getCmp('clave_pyme1');
		 if(clave_pyme.getValue()==''){
		  clave_pyme.setValue('No hay PyMEs con solicitudes pendientes');
		 }
		}
	}
	var catalogoContratacionData = new Ext.data.JsonStore({
		id: 'catalogoContratacionData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionTestigoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoContratacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoTipoPlazoData = new Ext.data.JsonStore({
		id: 'catalogoTipoPlazoData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionTestigoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	

	var catalogoEstatusData = new Ext.data.JsonStore({
		id: 'catalogoEstatusData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionTestigoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionTestigoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionTestigoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatalogoPymeData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPOData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34ContratoCesionTestigoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load:procesarCatalogoEPOData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var  elementosForma =  [	
		{
			xtype: 'combo',
			name: 'clave_epo',
			id: 'clave_epo1',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_epo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var cmbContratacion = Ext.getCmp('clave_contratacion1');
						cmbContratacion.setValue('');
						cmbContratacion.setDisabled(false);
						cmbContratacion.store.load({
							params: {
								clave_epo:combo.getValue()
							}
						});
					}
				}
			}
		}, 	
		{
			xtype: 'combo',
			name: 'clave_pyme',
			id: 'clave_pyme1',
			fieldLabel: 'PYME',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_pyme',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoPYMEData,
			tpl : NE.util.templateMensajeCargaCombo					
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Contrato',
			id: 'campoCompuesto1',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					name: 'numero_contrato',
					id: 'numero_contrato1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda ',
			combineErrors: false,
			msgTarget: 'side',
			id: 'campoCompuesto2',
			items: [
				{
					xtype: 'combo',
					name: 'clave_moneda',
					id: 'clave_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'clave_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					width: 240,
					allowBlank: true,
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Estatus ',
			combineErrors: false,
			msgTarget: 'side',
			id: 'campoCompuesto3',
			items: [
				{
					xtype: 'combo',
					name: 'clave_estatus_sol',
					id: 'clave_estatus_sol1',
					fieldLabel: 'Estatus',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'clave_estatus_sol',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					width: 240,
					allowBlank: true,
					store : catalogoEstatusData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Contrataci�n',
			combineErrors: false,
			msgTarget: 'side',
			id: 'campoCompuesto4',
			items: [
				{
					xtype: 'combo',
					name: 'clave_contratacion',
					id: 'clave_contratacion1',
					fieldLabel: 'Tipo de Contrataci�n',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'clave_contratacion',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 240,
					minChars : 1,
					allowBlank: true,
					store : catalogoContratacionData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		}, 
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vigencia de Contrato',
			combineErrors: false,
			msgTarget: 'side',
			id: 'campoCompuesto5',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_vigencia_ini',
					id: 'fecha_vigencia_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_vigencia_fin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_vigencia_fin',
					id: 'fecha_vigencia_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_vigencia_ini',
					margins: '0 20 0 0'  
					}
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Plazo del Contrato',
				combineErrors: false,
				msgTarget: 'side',
				id: 'campoCompuesto6',
				items: [
					{
						xtype: 'numberfield',
						name: 'plazoContrato',
						id: 'plazoContrato1',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',					
						margins: '0 20 0 0'  
					},	
					{
						xtype: 'combo',
						name: 'claveTipoPlazo',
						id: 'claveTipoPlazo1',
						fieldLabel: '',
						mode: 'local', 
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'claveTipoPlazo',
						emptyText: 'Seleccione...',					
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						allowBlank: true,
						store : catalogoTipoPlazoData,
						tpl : NE.util.templateMensajeCargaCombo
					}
				]
			}			
		];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de Busqueda TEST',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
							
					var clave_epo =  Ext.getCmp("clave_epo1");
					if (Ext.isEmpty(clave_epo.getValue()) ){
						clave_epo.markInvalid('El valor de la EPO es requerido.');
						return;
					}	
					
					var fecha_vigencia_ini =  Ext.getCmp("fecha_vigencia_ini");
					var fecha_vigencia_fin =  Ext.getCmp("fecha_vigencia_fin");
					var plazoContrato =  Ext.getCmp("plazoContrato1");
					var claveTipoPlazo =  Ext.getCmp("claveTipoPlazo1");
									
					if ( ( !Ext.isEmpty(fecha_vigencia_ini.getValue())  &&   Ext.isEmpty(fecha_vigencia_fin.getValue()) )
						||  ( Ext.isEmpty(fecha_vigencia_ini.getValue())  &&   !Ext.isEmpty(fecha_vigencia_fin.getValue()) ) ){
						fecha_vigencia_ini.markInvalid('Debe capturar ambas Fecha de Vigencia de Contrato o dejarlas en blanco');
						fecha_vigencia_fin.markInvalid('Debe capturar ambas Fecha de Vigencia de Contrato o dejarlas en blanco');
						return;
					}
										
					if ( !Ext.isEmpty(fecha_vigencia_ini.getValue())  &&   !Ext.isEmpty(fecha_vigencia_fin.getValue())   &&  !Ext.isEmpty(claveTipoPlazo.getValue()) &&  !Ext.isEmpty(plazoContrato.getValue()) ){										
						plazoContrato.markInvalid('S�lo puede ingresar el valor de la Fecha Inicial y Final de la Vigencia del Contrato � el Plazo del Contrato, no ambos.');
						return;
					}	
									
									
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar'						
						})
					});						
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34ContratoCesionTestigoext.jsp';
				}
			}
		]
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [	
			mensajeAutorizacion,
			NE.util.getEspaciador(20),
			fp,
			gridCifrasControl,
			fpBotones,	
			NE.util.getEspaciador(20),		
			mensajeConsulta,
			PanelFoma,
			NE.util.getEspaciador(20),			
			gridConsulta,
			
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoEPOData.load();
	catalogoPYMEData.load();
	catalogoMonedaData.load();
	catalogoEstatusData.load();
	catalogoTipoPlazoData.load();	
	
	//Ext.getCmp("mensajeConsulta").show();
	
});