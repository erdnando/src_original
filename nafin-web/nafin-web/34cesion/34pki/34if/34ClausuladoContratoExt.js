Ext.onReady(function(){


var panelContratoCesion= new Ext.FormPanel({  
    hidden: true,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html: '<table><tr><td><table BORDER="0" CELLSPACING="1" CELLPADDING="5" WIDTH="90%" style="text-align:justify">'+
			'<tr><td><b>Contrato de Cesi�n de Derechos de Cobro</b><br/><br/></td></tr>'+
'<tr><td>CONTRATO DE CESI�N DE DERECHOS DE COBRO (EN LO SUCESIVO EL �CONTRATO DE CESI�N�) QUE CELEBRAN POR UNA PARTE (1) PL�STICOS BEGOVICH, S.A. DE C.V. (2) FIERROS Y LAMINAS DE TULANCINGO SA DE CV (3) MONTAJES ELECTROMEC�NICOS IZA S.A. DE C.V. MA�Z EDIFICACIONES S.A. DE C.V. EN SU CAR�CTER DE CEDENTE, REPRESENTADO EN ESTE ACTO POR SU (4) APODERADO, SE�OR(A) (5) ALVARO SOMMERZ ENRIQUE JESUS , SANCHEZ HUERTA JOSE Y POR LA OTRA PARTE (6) AF BANREGIO S.A. DE C.V. SOFOMER, EN SU CAR�CTER DE CESIONARIO, REPRESENTADO EN ESTE ACTO POR SU (7) REPRESENTANTE LEGAL SE�OR(A) (8) ALBERTO MENDOZA DORANTES, AL TENOR DE LOS SIGUIENTES ANTECEDENTES, DECLARACIONES Y CL�USULAS:<br/><br/></td></tr>'+
 '<tr><td><b>ANTECEDENTES</b><br/></td></tr>'+
'<tr><td><br/>I.- Con fecha (9) 25 de Septiembre de 2013, el Cedente en su car�cter de (10) Contratista y la Entidad como organismo contratante, celebraron un Contrato de (11) Adquisici�n de Bienes No. (12) 2212, mediante el cual el Cedente se oblig� a: (13) 12112. Como contraprestaci�n (14) PEMEX REFINACI�N se oblig� a pagar al Cedente, la cantidad de (15) $123.00-150, 789.00-800.00 MONEDA NACIONAL, 789.00-800.00 MONEDA EURO, m�s el Impuesto al Valor Agregado (IVA), en los t�rminos y condiciones que aparecen en dicho instrumento contractual. </td></tr>'+
'<tr><td>II.-Con fecha (16) 26 de Septiembre de 2013, (14) PEMEX REFINACI�N otorg� su consentimiento al Cedente para ceder a (6) AF BANREGIO S.A. DE C.V. SOFOMER, el 100% (cien por ciento)  de los derechos de cobro que lleguen a derivarse de la ejecuci�n y cumplimiento del Contrato de (11) Adquisici�n de Bienes No. (12) 2212, incluidos los incrementos al monto original del mismo que, en su caso, se pacten con posterioridad mediante la formalizaci�n de convenios modificatorios. <br/><br/></td></tr>'+
'<tr><td><center><b>D E C L A R A C I O N E S</b><center><br/><br/></td></tr>'+
'<tr><td></td></tr>'+
'<tr><td>I.	Declara la CESIONARIA, por conducto de su representante, bajo protesta de decir verdad que:</td></tr>'+
'<tr><td>I.1	Es una (17) Empresa de Comercio,  constituida de conformidad con las Leyes de la Rep�blica Mexicana, que se formaliz� por medio de la Escritura P�blica No. (18) 1154, de fecha (19) 27 de Septiembre de 2013, otorgada ante la fe del Licenciado  (20) Arturo Huerta Valencia, Notario P�blico No.(21) 27935 de la Ciudad de (22) M�xico, cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de (23) en el folio mercantil No. (24) 147896, con fecha (19) 27 de Septiembre de 2013.</td></tr>'+
'<tr><td><br/><br/><b>Datos Representante IF1</b><br/><br/></td></tr>'+
'<tr><td> </td></tr>'+
'<tr><td>I.2	Su representante cuenta con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita con la Escritura P�blica No. (25) 1154, de fecha (26) 28 de Septiembre de 2013, otorgada ante la fe del Licenciado (27) Arturo Huerta Valencia, Notario P�blico No. (28) 27935 de la Ciudad de (29) M�xico, que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna</td></tr>'+
'<tr><td>  </td></tr>'+
'<tr><td><br/><br/><b>Datos Representante IF2</b><br/><br/></td></tr>'+
'<tr><td>  </td></tr>'+
'<tr><td>I.2.1 Su representante cuenta con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita con la Escritura P�blica No. (30) 1154, de fecha (31) 28 de Septiembre de 2013, otorgada ante la fe del Licenciado (32) Arturo Huerta Valencia, Notario P�blico No. (33) 27935 de la Ciudad de (34) M�xico, que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna  </td></tr>'+
'<tr><td><br/><br/>I.3     Para efectos del presente contrato se�ala como domicilio legal  el siguiente: Calle Chapala #3249 Col. Monte Grande. Delegaci�n Venustiano Carranza C.P. 87354 (35)  </td></tr>'+

'<tr><td><br/><br/><b> II.	Declara la CEDENTE, por conducto de su representante, bajo protesta de decir verdad que: </td></tr>'+
'<tr><td>II.1	Es una (36) Empresa de Comercio constituida de conformidad con las Leyes de la Rep�blica Mexicana, que se formaliz� por medio de la Escritura P�blica No. (37) 145632, de fecha (38) 30 de Septiembre de 2013, otorgada ante la fe del Licenciado (39) Roberto Avenda�o Ruiz, Notario P�blico No. (40) 2148 de la Ciudad de (41) M�xico, cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de (42) en el folio mercantil No. (43) 78987, con fecha (38) 30 de Septiembre de 2013.  </td></tr>'+

'<tr><td><br/><br/>II.2	Su representante cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato, lo cual acredita con la Escritura P�blica No. (44) 145632, de fecha (45) 28 de Septiembre de 2013, otorgada ante la fe del Licenciado (46) Roberto Avenda�o Ruiz, Notario P�blico No. (47) 2148 de la Ciudad de (48) M�xico, que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.  </td></tr>'+
'<tr><td><br/><br/>II.3 La Entidad otorg� Consentimiento Condicionado para ceder a la CESIONARIA el 100% (cien por ciento) de los derechos de cobro que se deriven por la ejecuci�n y cumplimiento del Contrato No. (12) 2212.  </td></tr>'+
'<tr><td><br/><br/>II.4Para efectos del presente contrato se�ala como domicilio legal  el siguiente: Lago Cuitzeo #176 Col. Ampliaci�n. Delegaci�n �lvaro Obreg�n C.P. 57354 (49)  </td></tr>'+
'<tr><td><br/><br/><b>III.	Declaran las partes por conducto de sus representantes que:  </td></tr>'+
'<tr><td><br/><br/>III.1   La CESIONARIA notificar� a la Entidad mediante la Notificaci�n del Contrato de Cesi�n de Derechos de Cobro conforme al Sistema, el presente Contrato.  </td></tr>'+
'<tr><td><br/><br/>III.2   Se reconocen la personalidad con la que se ostentan sus representantes.Expuesto lo anterior, las partes otorgan y se someten a lo que se consigna en las siguientes:  </td></tr>'+
'<tr><td><br/><br/><center><b>C L � U S U L A S <center> </td></tr>'+
'<tr><td><br/><br/><b>PRIMERA.</b> La CEDENTE cede a favor de la CESIONARIA el 100% (cien por ciento) de los derechos de cobro que lleguen a lleguen a generarse conforme a lo estipulado en el Contrato No. (12) 2212, y sus convenios modificatorios al monto original del mismo que, en su caso, se formalicen con posterioridad a la fecha de firma del presente Contrato de Cesi�n, y el Cesionario acepta la cesi�n de derechos de cobro que adquiere bajo el presente Contrato de Cesi�n.  </td></tr>'+
'<tr><td><br/><br/>La CESIONARIA podr� exigir y/o recibir el pago de la CEDENTE o en su caso del Obligado Solidario.  </td></tr>'+
'<tr><td><br/><br/><b>SEGUNDA</b>.-  La CESIONARIA acepta y est� de acuerdo en que el pago est� sujeto a que, en su caso, la Entidad realice las deducciones y/o retenciones que deban hacerse al CEDENTE por adeudos con la Entidad; penas convencionales; pagos en exceso; cualquier requerimiento de pago derivado de laudo firme ordenado por las Juntas Locales o Federales de Conciliaci�n y Arbitraje, con motivo de los juicios laborales instaurados en contra de Petr�leos Mexicanos y sus Organismos Subsidiarios, en relaci�n con el CEDENTE; o cualquier otra deducci�n derivada del Contrato No. (12) 2212 o de la Ley de la materia en relaci�n con el mismo, en cuyos supuestos se har�n las deducciones y/o retenciones correspondientes. En tal virtud, la Entidad quedar� liberada de cualquier responsabilidad por las deducciones y/o retenciones a que haya lugar.   </td></tr>'+
'<tr><td><br/><br/>La cesi�n de derechos de cobro �nica y exclusivamente proceder� con respecto a facturas pendientes de cobro por trabajos ejecutados en t�rminos de lo pactado en el contrato y que hayan sido instruidos por la Entidad.  </td></tr>'+
'<tr><td><br/><br/><b>TERCERA.</b> Una vez que surta efectos la cesi�n de derechos, la Entidad deber� depositar los pagos que conforme a los t�rminos y condiciones del Contrato No. (12) 2212 deba realizar a favor del CEDENTE, directamente en la CUENTA �NICA (50)  XXX-XXX del CESIONARIO.  </td></tr>'+
'<tr><td><br/><br/><b>CUARTA.</b> Al recibir la CESIONARIA el pago por el 100% (cien por ciento) del importe del Contrato No. (12) 2212, menos las deducciones y/o retenciones que la Entidad efect�e a la CEDENTE por los conceptos antes se�alados, deber� deducir la cantidad correspondiente al saldo del cr�dito, conforme a su estado de cuenta (capital, intereses, gastos y accesorios), oblig�ndose a depositar a la CEDENTE el remanente del pago recibido de la Entidad derivado de la presente cesi�n, sin causa de intereses, en la cuenta bancaria que el CEDENTE se�al� a la CESIONARIA en el contrato de Cr�dito respectivo.  </td></tr>'+
'<tr><td><br/><br/>La entrega a la CEDENTE del remanente a que se hace menci�n en el p�rrafo anterior, queda condicionada a que no existan adeudos pendientes de pago. En caso de que �stos existan, la CESIONARIA deducir� la cantidad correspondiente hasta cubrir el saldo insoluto del Cr�dito.  </td></tr>'+
'<tr><td><br/><br/>El pago o los pagos que efect�e la Entidad a la CESIONARIA ser�n con motivo de la presente cesi�n.  </td></tr>'+
'<tr><td><br/><br/><b>QUINTA.</b> La CEDENTE no se reserva derecho ni acci�n alguna en contra de la Entidad que se pudiera derivar de esta cesi�n.  </td></tr>'+
'<tr><td><br/><br/><b>SEXTA.</b> Todas las notificaciones, avisos y en general, cualquier comunicaci�n que las partes deban hacerse en el cumplimiento de esta cesi�n, incluyendo el emplazamiento en caso de juicio, se har�n en los  domicilios se�alados en la  declaraci�n I.3 y II.4 seg�n corresponda.  </td></tr>'+
'<tr><td><br/><br/>Cualquier cambio de domicilio de las partes deber� ser notificado por correo certificado, dirigido al representante legal de la contraparte, con acuse de recibo.  </td></tr>'+
'<tr><td><br/><br/><b>S�PTIMA.</b>  El consentimiento otorgado al Cedente por  la Entidad  para ceder los derechos de cobro del Contrato No. (12) 2212, incluidos los incrementos al monto original del mismo, no constituye por su parte una garant�a de pago ni reconocimiento de cualquier derecho del Cesionario frente al Cedente, siendo la �nica obligaci�n de dicha Entidad, la de pagar las cantidades l�quidas que sean exigibles por el cumplimiento del citado instrumento contractual una vez hechas las deducciones y/o retenciones antes mencionadas.  </td></tr>'+
'<tr><td><br/><br/><b>OCTAVA.</b> En caso de controversia respecto de la interpretaci�n, ejecuci�n y cumplimiento del presente contrato, las partes se someten expresamente a la jurisdicci�n de los tribunales competentes en la Ciudad de M�xico, Distrito Federal, renunciando a cualquier otro fuero que, por raz�n de sus domicilios presentes o futuros, les pudiera corresponder.  </td></tr>'+
'<tr><td><br/><br/><b>NOVENA.</b> Las partes acuerdan que para dar por terminado el presente Contrato ser� necesario que celebren un convenio de extinci�n de la cesi�n de derechos de cobro objeto de este instrumento contractual; para ello sus respectivos representantes legales o apoderados deber�n contar expresamente con facultades para actos de dominio, y en su oportunidad cualquiera de ellas deber� notificarlo a la Entidad conforme a los t�rminos que se se�alan en el Consentimiento Condicionado, para que surta los efectos legales que le correspondan.  </td></tr>'+
'<tr><td><br/><br/>Enteradas del contenido y alcance del presente contrato, las partes lo firman de conformidad, en la Ciudad de M�xico, Distrito Federal, a los (51) 25 d�as del mes de Septiembre de 2013.  </td></tr>'+

'</table>'+
'</td><td>'+
'<table BORDER="1" CELLSPACING="1" CELLPADDING="5" WIDTH=250 align="left">'+
'<tr><td><b>Claves del Texto</b></tr></td>'+
'<tr><td><br/></tr></td>'+
'<tr><td>(1)	Nombre de la Pyme 1 </tr></td>'+
'<tr><td>(2)	Nombre de la Pyme 2 (Opcional)</tr></td>'+
'<tr><td>(3)   Nombre de las Empresas Representadas </tr></td>'+
'<tr><td>(4)   Tipo de Representante del Cedente   </tr></td>'+
'<tr><td>(5)   Nombre del o los Representantes Pyme�s </tr></td>'+
'<tr><td>(7) 	Tipo de Representante del IF</tr></td>'+
'<tr><td>(8)	Nombre del Representante o Apoderado del Cesionario (IF)  </tr></td>'+
'<tr><td>(9	Fecha firma contrato.      </tr></td>'+
'<tr><td>(10)	Tipo de Cedente</tr></td>'+
'<tr><td>(11)	Tipo de Contrataci�n</tr></td>'+
'<tr><td>(12)	N�mero de Contrato</tr></td>'+
'<tr><td>(13)	Objeto del Contrato</tr></td>'+
'<tr><td>(14)	Nombre de la EPO</tr></td>'+
'<tr><td>(15)	Tipo de Monto y Moneda</tr></td>'+
'<tr><td>(16)	Fecha cuando la EPO acepta la solicitud de Consentimiento. </tr></td>'+
'<tr><td>(17)	Nombre de la Figura Jur�dica del Cesionario</tr></td>'+
'<tr><td>(18)	N�mero de Escritura P�blica del Cesionario</tr></td>'+
'<tr><td>(19)	Fecha Escritura P�blica del Cesionario</tr></td>'+
'<tr><td>(20)	Nombre del Licenciado del Cesionario</tr></td>'+
'<tr><td>(21)	N�mero de Notario P�blico del Cesionario</tr></td>'+
'<tr><td>(22)	Ciudad Origen Notario del Cesionario</tr></td>'+
'<tr><td>(23)	Registro P�blico de Comercio del Cesionario</tr></td>'+
'<tr><td>(24)	N�mero de Folio Mercantil del Cesionario</tr></td>'+
'<tr><td>(25)	N�mero de Escritura P�blica Representante IF1</tr></td>'+
'<tr><td>(26)	Fecha Escritura P�blica Representante IF1</tr></td>'+
'<tr><td>(27)	Nombre del Licenciado Representante IF1</tr></td>'+
'<tr><td>(28)	N�mero del Notario P�blico Representante IF1</tr></td>'+
'<tr><td>(29)	Ciudad Origen del Notario Representante IF1 </tr></td>'+
'<tr><td>(30)	N�mero de Escritura P�blica Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(31)	Fecha Escritura P�blica Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(32)	Nombre del Licenciado Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(33)	N�mero del Notario P�blico Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(34)	Ciudad Origen del Notario Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(35)	Domicilio Legal del Cesionario</tr></td>'+
'<tr><td>(36)	Figura Jur�dica del Cedente</tr></td>'+
'<tr><td>(37)	N�mero de Escritura P�blica del Cedente</tr></td>'+
'<tr><td>(38)	Fecha Registro P�blico del Cedente</tr></td>'+
'<tr><td>(39)	Nombre del Licenciado del Cedente</tr></td>'+
'<tr><td>(40)	N�mero del Notario P�blico del Cedente</tr></td>'+
'<tr><td>(41)	Ciudad Origen del Notario del Cedente</tr></td>'+
'<tr><td>(42)	Registro P�blico de Comercio del Cedente</tr></td>'+
'<tr><td>(43)	N�mero de Folio Mercantil del Cedente</tr></td>'+
'<tr><td>(44)	N�mero de Escritura P�blica del Cedente</tr></td>'+
'<tr><td>(45)	Fecha Registro P�blico del Cedente</tr></td>'+
'<tr><td>(46)	Nombre del Licenciado del Cedente</tr></td>'+
'<tr><td>(47)	N�mero del Notario P�blico del Cedente</tr></td>'+
'<tr><td>(48)	Ciudad Origen del Notario del Cedente</tr></td>'+
'<tr><td>(49)	Domicilio Legal Empresa del Cedente</tr></td>'+
'<tr><td>(50)	Muestra la cuenta �nica para el dep�sito del Cesionario</tr></td>'+
'<tr><td>(51)	Fecha final de �ltima Firma</tr></td>'+
'</table>'+
'</td></tr></table>'
		
	 });
	 
var panelConvenioExtincion= new Ext.FormPanel({  
    hidden: true,
	 defaults:{xtype:'textfield'},   
    bodyStyle:'padding: 10px', 
    html:
			 '<table><tr><td><table BORDER="0" CELLSPACING="1" CELLPADDING="5" WIDTH="90%" style="text-align:justify">'+
       '<tr><td><br/><b>Convenio de Extinci�n de Cesi�n de Derechos de Cobro</b></tr></td>'+
		 
'<tr><td><br/>CONVENIO DE EXTINCI�N DE  CESI�N DE DERECHOS DE COBRO (EN LO SUCESIVO EL �CONVENIO DE EXTINCION�) QUE CELEBRAN POR UNA PARTE  (1) PL�STICOS BEGOVICH, S.A. DE C.V. (2) FIERROS Y LAMINAS DE TULANCINGO SA DE CV  (3) MONTAJES ELECTROMEC�NICOS IZA S.A. DE C.V.; MA�Z EDIFICACIONES S.A. DE C.V.  EN SU CAR�CTER DE CEDENTE, REPRESENTADO EN ESTE ACTO POR SU (4) APODERADO  SE�OR (5) ALVARO SOMMERZ ENRIQUE JESUS, SANCHEZ HUERTA JOSE Y POR LA OTRA PARTE (6) AF BANREGIO S.A. DE C.V. SOFOMER  EN SU CAR�CTER DE CESIONARIO, REPRESENTADO EN ESTE ACTO POR SU (7) REPRESENTANTE LEGAL SE�OR (8) ALBERTO MENDOZA DORANTES, AL TENOR DE LOS SIGUIENTES ANTECEDENTES, DECLARACIONES Y CL�USULAS: </tr></td>'+
'<tr><td><br/><b>ANTECEDENTES</b> </tr></td>'+
'<tr><td><br/>Con fecha (9) 25 de Septiembre de 2013, el Cedente en su car�cter de (10) Contratista y cesionario, celebraron un Contrato de Cesi�n de  derechos de Cobro respecto del Contrato (11) Adquisici�n de Bienes  No. (12) 2212. </tr></td>'+
'<tr><td><br/><center><b>D E C L A R A C I O N E S</tr></td>'+
'<tr><td><br/><b>I.	Declara la CESIONARIA, por conducto de su representante, bajo protesta de decir verdad que:</b></tr></td>'+
'<tr><td><br/>I.1	Es una Instituci�n de Banca M�ltiple, constituida de conformidad con las Leyes de la Rep�blica Mexicana, que se formaliz� por medio de la Escritura P�blica No. (18) 1154, de fecha (19) 27 de Septiembre de 2013, otorgada ante la fe del Licenciado (20) Arturo Huerta Valencia Notario P�blico No. (21) 27935 de la Ciudad de (22) M�xico, cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de (23) en el folio mercantil No. (24) 147896, con fecha (19) 27 de Septiembre de 2013.</tr></td>'+
'<tr><td><br/><b>Datos Representante IF 1</tr></td>'+
		 
'<tr><td><br/>I.2  Su representante cuenta con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita con la Escritura P�blica No. (25) 1154, de fecha (26) 28 de Septiembre de 2013, otorgada ante la fe del Licenciado (27) Arturo Huerta Valencia, Notario P�blico No. (28) 27935 de la Ciudad de (29) M�xico, que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.</tr></td>'+
'<tr><td><br/><b>Datos Representante IF 2</b></tr></td>'+
'<tr><td><br/>I.2  Su representante cuenta con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita con la Escritura P�blica No. (30) 1154, de fecha (31) 28 de Septiembre de 2013, otorgada ante la fe del Licenciado (32) Arturo Huerta Valencia, Notario P�blico No. (33) 27935 de la Ciudad de (34) M�xico, que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.</tr></td>'+
'<tr><td><br/><b>II.	Declara la CEDENTE, por conducto de su representante, bajo protesta de decir verdad que: </b></tr></td>'+
'<tr><td><br/>II.1  Es una Sociedad An�nima, constituida de conformidad con las Leyes de la Rep�blica Mexicana, que se formaliz� por medio de la Escritura P�blica No. (37)  145632, de fecha (38) 28 de Septiembre de 2013, otorgada ante la fe del Licenciado (39) Roberto Avenda�o Ruiz, Notario P�blico No. (40) 2148 de la Ciudad de (41) M�xico, cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de (42) en el folio mercantil No. (43) 78987, con fecha (38) 28 de Septiembre de 2013.</tr></td>'+
'<tr><td><br/>II.2 Su representante cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato, lo cual acredita con la Escritura P�blica No. (44) 145632, de fecha (45) 28 de Septiembre de 2013, otorgada ante la fe del Licenciado (46) Roberto Avenda�o Ruiz, Notario P�blico No. (47) 2148 de la Ciudad de (48) M�xico, que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.</tr></td>'+
'<tr><td><br/>II.3 Tiene celebrado con la CESIONARIA un Contrato de Cesi�n Electr�nica de Derechos de Cobro respecto del Contrato No. (12) 2212.</tr></td>'+
'<tr><td><br/><b>III.	Declaran las partes por conducto de sus representantes que:<b></tr></td>'+
'<tr><td><br/>III.1 De conformidad con lo antes se�alado, la CESIONARIA presentar� a la Entidad junto con la notificaci�n de la Extinci�n a la Cesi�n Electr�nica de Derechos de Cobro, el presente Convenio de Extinci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro respecto del Contrato No. (12) 2212.</tr></td>'+
'<tr><td><br/>III.2 Se reconocen la personalidad con la que se ostentan sus representantes.</tr></td>'+
'<tr><td><br/>Expuesto lo anterior, las partes otorgan y se someten a lo que se consigna en las siguientes:</tr></td>'+
'<tr><td><br/><center><b>C L � U S U L A S</b></center> </tr></td>'+
'<tr><td><br/><b>PRIMERA.</b> La CESIONARIA y el CEDENTE, con la firma de este convenio, expresamente acuerdan en llevar a cabo la extinci�n de la Cesi�n Electr�nica de los Derechos de Cobro que le fueron transmitidos a la CESIONARIA mediante la celebraci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro respecto del Contrato No. (12) 2212 relativo a (13) 12112, y que a partir de la fecha de notificaci�n a la Entidad quedar� sin efecto legal alguno.</tr></td>'+
'<tr><td><br/><b>SEGUNDA.</b> Por virtud de la extinci�n de la Cesi�n Electr�nica de los Derechos de Cobro respecto del Contrato No. (12) 2212 que por el presente convenio se formaliza, el CEDENTE  a trav�s del sistema NAFINET, se obliga a notific�rselo a la Entidad, en un plazo no mayor a 10 (diez) d�as naturales, contados a partir de la fecha de firma del presente convenio, con objeto de que a partir de la fecha de notificaci�n, se efect�en los pagos que se generen del Contrato No. (12) 2212 directamente al CEDENTE.</tr></td>'+
'<tr><td><br/><b>TERCERA.</b> Las partes reconocen que no existe dolo, error, o mala fe por parte de quienes celebran el presente convenio y que cada uno de ellos se ha obligado en la manera y t�rminos que aparecen en este documento, sin que la validez del mismo dependa de la observancia de formalidades o requisitos determinados.</tr></td>'+
'<tr><td><br/><b>CUARTA.</b> Para la interpretaci�n, ejecuci�n y cumplimiento de los pactos contenidos en este convenio, las partes se someten a las leyes y tribunales competentes de la Ciudad de M�xico, D.F., por lo que renuncian expresamente a cualquier otro fuero que pudiera corresponderles, en virtud de sus domicilios presentes o futuros.</tr></td>'+
'<tr><td><br/>Le�do que fue el presente convenio y enteradas las partes de su contenido, alcance y fuerza legal, se firma en la Ciudad de M�xico, Distrito Federal, a los (51) 02 d�as del mes de Octubre del a�o 2013.</tr></td>'+
'</table>'+
'</td><td>'+
'<table BORDER="1" CELLSPACING="1" CELLPADDING="5" WIDTH=250 align="left">'+  
 '<tr><td><b>Claves del Texto</b></tr></td>'+
'<tr><td><br/></tr></td>'+		
'<tr><td>(1)	Nombre de la Pyme 1 </tr></td>'+
'<tr><td>(2)	Nombre de la Pyme 2 (Opcional)</tr></td>'+
'<tr><td>(3)   Nombre de las Empresas Representadas </tr></td>'+
'<tr><td>(4)   Tipo de Representante del Cedente   </tr></td>'+
'<tr><td>(5)   Nombre del o los Representantes Pyme�s </tr></td>'+
'<tr><td>(7) 	Tipo de Representante del IF</tr></td>'+
'<tr><td>(8)	Nombre del Representante o Apoderado del Cesionario (IF)  </tr></td>'+
'<tr><td>(9	Fecha firma contrato.      </tr></td>'+
'<tr><td>(10)	Tipo de Cedente</tr></td>'+
'<tr><td>(11)	Tipo de Contrataci�n</tr></td>'+
'<tr><td>(12)	N�mero de Contrato</tr></td>'+
'<tr><td>(13)	Objeto del Contrato</tr></td>'+
'<tr><td>(14)	Nombre de la EPO</tr></td>'+
'<tr><td>(15)	Tipo de Monto y Moneda</tr></td>'+
'<tr><td>(16)	Fecha cuando la EPO acepta la solicitud de Consentimiento. </tr></td>'+
'<tr><td>(17)	Nombre de la Figura Jur�dica del Cesionario</tr></td>'+
'<tr><td>(18)	N�mero de Escritura P�blica del Cesionario</tr></td>'+
'<tr><td>(19)	Fecha Escritura P�blica del Cesionario</tr></td>'+
'<tr><td>(20)	Nombre del Licenciado del Cesionario</tr></td>'+
'<tr><td>(21)	N�mero de Notario P�blico del Cesionario</tr></td>'+
'<tr><td>(22)	Ciudad Origen Notario del Cesionario</tr></td>'+
'<tr><td>(23)	Registro P�blico de Comercio del Cesionario</tr></td>'+
'<tr><td>(24)	N�mero de Folio Mercantil del Cesionario</tr></td>'+
'<tr><td>(25)	N�mero de Escritura P�blica Representante IF1</tr></td>'+
'<tr><td>(26)	Fecha Escritura P�blica Representante IF1</tr></td>'+
'<tr><td>(27)	Nombre del Licenciado Representante IF1</tr></td>'+
'<tr><td>(28)	N�mero del Notario P�blico Representante IF1</tr></td>'+
'<tr><td>(29)	Ciudad Origen del Notario Representante IF1 </tr></td>'+
'<tr><td>(30)	N�mero de Escritura P�blica Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(31)	Fecha Escritura P�blica Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(32)	Nombre del Licenciado Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(33)	N�mero del Notario P�blico Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(34)	Ciudad Origen del Notario Representante IF2 (Opcional)</tr></td>'+
'<tr><td>(35)	Domicilio Legal del Cesionario</tr></td>'+
'<tr><td>(36)	Figura Jur�dica del Cedente</tr></td>'+
'<tr><td>(37)	N�mero de Escritura P�blica del Cedente</tr></td>'+
'<tr><td>(38)	Fecha Registro P�blico del Cedente</tr></td>'+
'<tr><td>(39)	Nombre del Licenciado del Cedente</tr></td>'+
'<tr><td>(40)	N�mero del Notario P�blico del Cedente</tr></td>'+
'<tr><td>(41)	Ciudad Origen del Notario del Cedente</tr></td>'+
'<tr><td>(42)	Registro P�blico de Comercio del Cedente</tr></td>'+
'<tr><td>(43)	N�mero de Folio Mercantil del Cedente</tr></td>'+
'<tr><td>(44)	N�mero de Escritura P�blica del Cedente</tr></td>'+
'<tr><td>(45)	Fecha Registro P�blico del Cedente</tr></td>'+
'<tr><td>(46)	Nombre del Licenciado del Cedente</tr></td>'+
'<tr><td>(47)	N�mero del Notario P�blico del Cedente</tr></td>'+
'<tr><td>(48)	Ciudad Origen del Notario del Cedente</tr></td>'+
'<tr><td>(49)	Domicilio Legal Empresa del Cedente</tr></td>'+
'<tr><td>(50)	Muestra la cuenta �nica para el dep�sito del Cesionario</tr></td>'+
'<tr><td>(51)	Fecha final de �ltima Firma</tr></td>'+
'</td></tr></table>'
	 });
	 
	 


	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 365,
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		//bodyStyle: 'padding: 6px',
		//labelWidth: 200,
		//defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		//items: elementosForma
		buttons: [
			{ 	xtype: 'button',  id: 'contratoCesion', 	text: 'Contrato Cesi�n de Derechos',handler: function(boton, evento) {panelConvenioExtincion.setVisible(false);panelContratoCesion.setVisible(true);}},
			{ 	xtype: 'button', id: 'convenioExtincion', text: 'Convenio Extinci�n de Contrato',handler: function(boton, evento) {panelContratoCesion.setVisible(false);panelConvenioExtincion.setVisible(true);}	}	
		]
		
		
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [			
			fp,NE.util.getEspaciador(10),panelContratoCesion,panelConvenioExtincion
		]
	});
	
});