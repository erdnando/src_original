Ext.onReady(function(){

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
									
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna		
			var jsonData = store.reader.jsonData;	
			var el = gridConsulta.getGridEl();
			
			if(jsonData.numeroRegistros =='0') {
				window.location = '34ExtincionContratoIfext.jsp?pantalla=SolicExtincion';
			}
					
			if(store.getTotalCount() > 0) {						
				el.unmask();					
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '34ExtincionContratoIf.data.jsp',
		baseParams: {
			informacion: 'ConsultarAvisos'
		},		
		fields: [			
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_PYME'},
			{name: 'RFC'},
			{name: 'FECHA_SOLICITUD'},
			{name: 'NO_CONTRATO'},
			{name: 'MONTO_MONEDA'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});

	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Solicitudes de Extinci�n por Autorizar',
		columns: [	
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},			
			{
				header: 'Pyme(Cedente)',
				tooltip: 'Pyme(Cedente)',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha de Solicitud Pyme',
				tooltip: 'Fecha de Solicitud Pyme',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No.Contrato',
				tooltip: 'No.Contrato',
				dataIndex: 'NO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto / Moneda',
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 800,		
		frame: true,
		bbar: {		
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Enterado',
					iconCls: 'icoAceptar',
					id: 'btnEnterado',
					handler: function(boton, evento) {
						window.location = '34ExtincionContratoIfext.jsp?pantalla=SolicExtincion';
					}
				}
			]
		}
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [		
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	

	var valIniciales = function(){
		consultaData.load();	
	}
	
	valIniciales();
	
	
	
});