<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.io.File,
		com.netro.exception.NafinException,		
		com.netro.pdf.*,
		net.sf.json.JSONArray,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%> 
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<% 
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

HashMap convenioExtincionCesionDerechos = cesionBean.consultaConvenioExtincionCesionDerechos(clave_solicitud);

String infoRegresar ="";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

CreaArchivo archivo 				= null;
archivo  = new CreaArchivo();
String clave_if="",nombreArchivo="";
JSONObject jsonObj = new JSONObject();	
HashMap camposAdicionales = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
int indiceCamposAdicionales = (camposAdicionales.get("indice")==null?0:Integer.parseInt((String)camposAdicionales.get("indice")));
int indiceC = (convenioExtincionCesionDerechos.get("indice")==null?0:Integer.parseInt((String)convenioExtincionCesionDerechos.get("indice")));

String tipoExtincion = ""; // Fodea 04-2016

System.out.println("clave_solicitud "+clave_solicitud);
System.out.println("clave_epo "+clave_epo);

System.out.println("indiceCamposAdicionales "+indiceCamposAdicionales);
System.out.println("indiceC "+indiceC);
if(informacion.equals("CONTRATO_EXTINCION")){

	boolean banderaArchivo1=false,banderaArchivo2=false;
		
	clave_epo = (String)convenioExtincionCesionDerechos.get("claveEpo");
	clave_if = (String)convenioExtincionCesionDerechos.get("claveIf");
				
	HashMap campos_adic = cesionBean.getCamposAdicionalesParametrizados(clave_epo, "9");
	//StringBuffer clausulado_parametrizado = cesionBean.consultaClausuladoParametrizado(clave_if);
	String clasificacionEpo = cesionBean.clasificacionEpo(clave_epo);
		
	int indice_camp_adic = Integer.parseInt((String)campos_adic.get("indice"));
	HashMap contratoCesion = cesionBean.consultaContratoCesionPyme(clave_solicitud);	
	nombreArchivo = archivo.nombreArchivo()+".pdf";
	String nombreArchivo2;
	String nombreArchivo3=archivo.nombreArchivo()+".pdf";
	ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, false);
				
	String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
	String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
	String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
	String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
	String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
			
	pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
	
		float anchoCelda4[] = {100f};
		pdfDoc.addText("\n","contrato",ComunesPDF.CENTER);
		//====================================================================>> TESTIGO 1 IF
	if (convenioExtincionCesionDerechos.get("acuseExtTest1") != null && !convenioExtincionCesionDerechos.get("acuseExtTest1").toString().equals("")) {
		pdfDoc.setTable(1, 70, anchoCelda4);
		//pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
		pdfDoc.setCell("NOTIFICACION DE EXTINCI�N  DE CESI�N DE DERECHO DE COBRO" , "contrato", ComunesPDF.CENTER, 1, 1, 0);		
		pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "contrato", ComunesPDF.LEFT, 1, 1, 0);	 //nombre de la EPo
		pdfDoc.setCell("Presente", "contrato", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Asunto: Notificaci�n de Extinci�n de Cesi�n Electr�nica de Derechos de Cobro", "contrato", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Con base en lo dispuesto en el C�digo de Comercio, por este conducto me permito notificar a ese Organismo, por conducto de esa Unidad Administrativa adscrita a la Direcci�n Jur�dica de Petr�leos Mexicanos, el Convenio de Extinci�n del Contrato de Cesi�n de Derechos de Cobro respecto del Contrato referido en el apartado siguiente que hemos formalizado con la Empresa Cedente.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);    
		pdfDoc.setCell("Por lo anterior, me permito solicitar a usted una vez que surta efectos la extinci�n de derechos de cobro, gire sus instrucciones a fin de que se cancele y se deje sin efectos la Cesi�n de Derechos de Cobro de referencia a efecto de que la Entidad realice el pago de las facturas derivadas de la ejecuci�n y cumplimiento del Contrato referido en el apartado inmediato anterior a la(s) Empresa(s) Cedente(s) en la cuenta se�alada originalmente en dicho Contrato.", "contrato", ComunesPDF.JUSTIFIED, 1, 1, 0);   
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
		
	//====================================================================>> TESTIGO 1 IF
	if (convenioExtincionCesionDerechos.get("acuseExtTest1") != null && !convenioExtincionCesionDerechos.get("acuseExtTest1").toString().equals("")) {
		pdfDoc.addText("\n","contrato",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}
	//====================================================================>> TESTIGO 2 IF
	if (convenioExtincionCesionDerechos.get("acuseExtTest2") != null && !convenioExtincionCesionDerechos.get("acuseExtTest2").toString().equals("")) {
		pdfDoc.addText("\n","contrato",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo1=true;
	}

	
	pdfDoc.endDocument();
	
	if (cesionBean.getBanderaContratoExtincion(clave_solicitud).equals("S")){
		nombreArchivo2 = cesionBean.descargaContratoExtincionNuevo(clave_solicitud,strDirectorioTemp);
	}else{
		nombreArchivo2 = cesionBean.generarContratoExtincion(clave_solicitud,strDirectorioTemp);
	}
	
	  pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo3, "", false, false, false);
	   if (convenioExtincionCesionDerechos.get("acuseExtIf") != null && !convenioExtincionCesionDerechos.get("acuseExtIf").toString().equals("")) {
		pdfDoc.setTable(1,70);
		pdfDoc.setCell("Le�do que fue el presente convenio y enteradas las partes de su contenido, alcance y fuerza legal, se firma en la Ciudad de M�xico, Distrito Federal, a los "+convenioExtincionCesionDerechos.get("dfExtIf"),"contrato",ComunesPDF.JUSTIFIED,1,1,0);
		pdfDoc.addTable();
		banderaArchivo2=true;
	 }
	//====================================================================>> REPRESENTANTES LEGALES PYME
	
	//if (convenioExtincionCesionDerechos.get("acuseExtRep1") != null && !convenioExtincionCesionDerechos.get("acuseExtRep1").toString().equals("")) {
	tipoExtincion = cesionBean.getTipoExtincion(clave_epo); // Fodea 04-2016
	if(!tipoExtincion.equals("C")){
	List firmasRepresentantes = cesionBean.getFirmasRepresentantes(clave_solicitud);
	HashMap mapaRepresentantes = new HashMap();
	for(int i = 0; i < firmasRepresentantes.size(); i++){
		mapaRepresentantes = new HashMap();
		mapaRepresentantes = (HashMap)firmasRepresentantes.get(i);
		String fechaExt = "" + mapaRepresentantes.get("DF_EXTINCION_REP");
		//System.out.println("" + mapaRepresentantes.get("DF_EXTINCION_REP"));
		//System.out.println("" + mapaRepresentantes.get("cg_nombre_usuario"));
		//System.out.println("" + contratoCesion.get("nombre_pyme"));
		//System.out.println("" + mapaRepresentantes.get("cg_firma_representante"));
		if(!fechaExt.equals("")){
			pdfDoc.addText("\n","contrato",ComunesPDF.CENTER);
			pdfDoc.addText("Sello Digital de " + mapaRepresentantes.get("cg_nombre_usuario") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
			pdfDoc.setCell((String)mapaRepresentantes.get("cg_firma_representante"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo2=true;
	}
		}
	}
	//}
	/*
	//====================================================================>> REPRESENTANTE LEGAL 2 PYME
	if (convenioExtincionCesionDerechos.get("acuseExtRep2") != null && !convenioExtincionCesionDerechos.get("acuseExtRep2").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep2") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaRep2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo2=true;
	}
	*/
	//====================================================================>> REPRESENTANTE LEGAL IF
	if (convenioExtincionCesionDerechos.get("acuseExtIf") != null && !convenioExtincionCesionDerechos.get("acuseExtIf").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo2=true;
	}
	
	
		//====================================================================>> REPRESENTANTE LEGAL IF
	if (convenioExtincionCesionDerechos.get("acuseExtIf2") != null && !convenioExtincionCesionDerechos.get("acuseExtIf2").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + convenioExtincionCesionDerechos.get("nombreUsrExtIf2") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo2=true;
	}
	

	//====================================================================>> REPRESENTANTE LEGAL IF 2
	if (convenioExtincionCesionDerechos.get("claveUsrIfRep2") != null && !convenioExtincionCesionDerechos.get("claveUsrIfRep2").toString().equals("")) {
		pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIfRep2") + " representante legal " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
		pdfDoc.setTable(1, 40, anchoCelda4);
		pdfDoc.setCell((String)contratoCesion.get("firmaIfRep2"), "formas", ComunesPDF.CENTER, 1);
		pdfDoc.addTable();
		banderaArchivo2=true;
	}
	pdfDoc.addText("\n","contrato",ComunesPDF.CENTER);
	

	pdfDoc.endDocument();
	
	ArrayList listaA = new ArrayList();
	
	if(banderaArchivo1){listaA.add(strDirectorioTemp+nombreArchivo);}
	listaA.add(strDirectorioTemp+nombreArchivo2);
	if(banderaArchivo2){listaA.add(strDirectorioTemp+nombreArchivo3);}
	String nombreArchivoNevo = archivo.nombreArchivo()+".pdf";
	listaA.add(strDirectorioTemp+nombreArchivoNevo);
	String args[]= new String[listaA.size()];
	for (int i=0; i<listaA.size(); i++)
		{
			args[i]=(String)listaA.get(i);
		}
	//String args[] =listaA.toArray();//  {strDirectorioTemp+nombreArchivo,strDirectorioTemp+nombreArchivo2,strDirectorioTemp+nombreArchivo3,strDirectorioTemp+nombreArchivoNevo};
	
	if(banderaArchivo1||banderaArchivo2){
		nombreArchivo=nombreArchivoNevo;
		ComunesPDF.doMerge(args);
		
	}else{
		nombreArchivo=nombreArchivo2;
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	infoRegresar = jsonObj.toString();	

}%>

<%=infoRegresar%>