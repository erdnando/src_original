Ext.onReady(function(){
	var solicituds = [];
	var strNombre  = Ext.getDom('strNombre').value;
	/*var  mensaje= '<table align="center" style="text-align: justify;" border="1" width="700" cellpadding="0" cellspacing="0">'+
			'<tr>'+
			' <td class="formas"  colspan="3" style="text-align: justify;">'+
			'	De conformidad con lo se�alado en el (los) Contrato(s) cuyos datos se se�alan en esta pantalla, '+
			'  el representante de la(s) empresa(s) CEDENTE, solicita(n) a '+strNombre+'  su consentimiento para ceder el 100% de los derechos de cobro que lleguen a derivarse por la ejecuci�n y cumplimiento del (los) referido(s) Contrato(s) y de los convenios que se lleguen a formalizar, a favor del Intermediario Financiero(s), en su car�cter de CESIONARIO(S). '+
			'<br width="80%">   </br>'+
			'	<p>'+
			'	En virtud de lo anterior una vez que cuente la CEDENTE  con el consentimiento solicitado a trav�s de �ste medio, �ste celebrar� el contrato de Cesi�n de Derechos de Cobro correspondiente a trav�s del sistema de Cadenas Productivas.'+
			'<br width="80%">   </br>'+
			'	<p>'+			
			'	Al mismo tiempo manifiesto (amos) que cuento (amos) con las facultades para actos de dominio necesarias para llevar a cabo la cesi�n de derechos de cobro conforme se acredita mediante los poderes anexos a la presente, mismos que no han sido revocados, modificados o limitados en forma alguna a la presente fecha.'+
			'<br width="80%">   </br>'+
			'	</td>'+
			' </tr>'+
			' </table>';
	*/
	var procesarSuccessFailureEnterado =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
				document.location.href = "34Consu_Solic_ConsentimientoExt.jsp?enterado=S"; 	
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarEnterado =  function(opts, success, response) {	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		solicituds = []; //limpia Variable
			store.each(function(record) {
				solicituds.push(record.data['CLAVE_SOLICITUD']);			
			});
		
		var enterado = Ext.getCmp('enterado1').getValue();			
			
		if(enterado ==false) {
			Ext.MessageBox.alert('Mensaje','Es necesario confirmar que est� enterado los t�rminos legales de las solicitudes de consentimiento');
			return false;	
		}		
		Ext.Ajax.request({
			url : '34Consu_Solic_ConsentimientoExt.data.jsp',
			params : {
				informacion: 'Enterado',
				solicituds:solicituds							
			},
			callback: procesarSuccessFailureEnterado
		});
	}
	
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
		
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				//Ext.getCmp("mensaje").update(store.reader.jsonData.mensaje);//mensajeN);//FODEA-024-2014 MOD()
				gridConsulta.show();
				
			}			
		
			var btnAceptar = Ext.getCmp('btnAceptar');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');	
			
			var el = gridConsulta.getGridEl();						
			if(store.getTotalCount() > 0) {			
				btnAceptar.enable();
				btnImprimirPDF.enable();					
				btnBajarPDF.hide();
					
				el.unmask();
			} else {	
				btnAceptar.disable();
				btnImprimirPDF.disable();
				btnBajarPDF.hide();							
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

var consultaData = new Ext.data.GroupingStore({
	root: 'registros',
	url: '34Consu_Solic_ConsentimientoExt.data.jsp',
	baseParams: {
		informacion: 'Consultar'
	},
	reader: new Ext.data.JsonReader({
	root : 'registros',	totalProperty: 'total',
	fields: [
				{name: 'CLAVE_SOLICITUD'},
				{name: 'CECIONARIO'},
				{name: 'PYME'},
				{name: 'RFC'},
				{name: 'CLAVE_PYME'},					
				{name: 'FSOLICITUDINI'},
				{name: 'NOCONTRATO'},
				{name: 'MONTO_MONEDA'},
				{name: 'FIRMA_CONTRATO'},	
				{name: 'SOLICITUD_PRORROGA'}
				
	]
	}),
	groupField: 'SOLICITUD_PRORROGA',	sortInfo:{field: 'SOLICITUD_PRORROGA', direction: "asc"},
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
	
	/*var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [			
			{name: 'CLAVE_SOLICITUD'},
			{name: 'CECIONARIO'},
			{name: 'PYME'},
			{name: 'RFC'},
			{name: 'CLAVE_PYME'},					
			{name: 'FSOLICITUDINI'},
			{name: 'NOCONTRATO'},
			{name: 'MONTO_MONEDA'},
			{name: 'FIRMA_CONTRATO'}		
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	*/
		

	var gridConsulta = new Ext.grid.EditorGridPanel({	 
		id: 'gridConsulta',
		store: consultaData,
		hidden: true,
		title: 'Solicitudes Pendientes',
		columns: [		
		{
				header: '',
				tooltip: '',
				hidden: true,
				dataIndex: 'SOLICITUD_PRORROGA',
				width: 150,	
				align: 'center', 
				hideable:false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
									if (value!='true'){
										return 'Pendientes por Aceptar';
									}else{
										return ' Que Solicitaron Prorroga';
									}

								}
			},
			{
				header: 'Intermediario Financiero (Cesionario)', 
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'CECIONARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Pyme (Cedente)', 
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'RFC ', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'No Proveedor ', 
				tooltip: 'No Proveedor',
				dataIndex: 'CLAVE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},		
			{
				header: 'Fecha de solicitud Pyme', 
				tooltip: 'Fecha de solicitud Pyme ',
				dataIndex: 'FSOLICITUDINI',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},	
			{
				header: 'No. Contrato', 
				tooltip: 'No. Contrato',
				dataIndex: 'NOCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda',  // este usa  metodo para obtenerlo
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			}		
		],
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{[values.rs[0].data["SOLICITUD_PRORROGA"] == "true" ? "Registros" : "Solicitudes"]} {text}'}),
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {				
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,				
			emptyMsg: "No hay registros.",
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Aceptar',
					id: 'btnAceptar',
					handler: procesarEnterado
				},					
				'-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnImprimirPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34Consu_Solic_ConsentimientoExt.data.jsp',
							params: {
								informacion: 'ArchivoResumen',
								tipo_archivo: 'PDF'
							}
							,callback: procesarSuccessFailurePDF
						});
					}
				},					
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
	});

/*
	var mensajeResumen = new Ext.Container({
		layout: 'table',		
		id: 'mensajeResumen',							
		width:	'700',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				id:	'mensaje',
				html:	''//mensaje	//FODEA-024-2014 MOD()
			}		
		]
	});
	*/
    
	var mensajeLegal = new Ext.Container({
		layout: 'table',		
		id: 'mensajeLegal',							
		width:	'700',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				id:	'mensaje',
				html:	'<div align="center">A continuaci�n se detallan las solicitudes de Consentimiento para ceder los derechos de cobro a los<br>Intermediarios Financieros aqui se�alados, gestionados por los sujetos de apoyo aqui relacionados, a fin de <br>emitir, en su caso, el consentimiento condicionado correspondiente y de acuerdo a la normatividad<br>correspondiente.</div>'
			}		
		]
	});
    
	var checkEnterado = new Ext.Container({
		layout: 'table',		
		id: 'checkEnterado',							
		width:	'80',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{
				xtype: 'checkbox',
				boxLabel: 'Enterado',
				name: 'enterado',
				id: 'enterado1',
				inputValue: 'S',
				msgTarget: 'side'
			}
		]
	});
	
	

	
//--------------------------------PRINCIPAL-------------------------------------
var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		items: [	
			NE.util.getEspaciador(20),
			//mensajeResumen,
			NE.util.getEspaciador(20),
			checkEnterado,
			NE.util.getEspaciador(20),
            mensajeLegal,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)		
		]
	});
	

	consultaData.load({
		params: { 	informacion: 'ConsultarResumen'	}
	});
		
});