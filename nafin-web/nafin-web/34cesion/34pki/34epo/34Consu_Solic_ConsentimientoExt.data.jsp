<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*, 
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.text.*, 
		java.io.OutputStream,
		java.io.FileOutputStream,
		netropology.utilerias.usuarios.UtilUsr,
		netropology.utilerias.usuarios.Usuario,
		com.netro.exception.NafinException,
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray,
		java.io.FileInputStream,
		com.netro.pdf.*,
		java.io.File,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
	
	String noIF = (request.getParameter("noIF")!=null)?request.getParameter("noIF"):"";
	String pyme = (request.getParameter("pyme")!=null)?request.getParameter("pyme"):"";
	String estatus = (request.getParameter("estatus")!=null)?request.getParameter("estatus"):"";
	String noContrato = (request.getParameter("noContrato")!=null)?request.getParameter("noContrato"):"";
	String fvigenciaIni = (request.getParameter("fvigenciaIni")!=null)?request.getParameter("fvigenciaIni"):"";
	String fvigenciaFin = (request.getParameter("fvigenciaFin")!=null)?request.getParameter("fvigenciaFin"):"";
	String plazoContrato = (request.getParameter("plazoContrato")!=null)?request.getParameter("plazoContrato"):"";
	String claveTipoPlazo = (request.getParameter("claveTipoPlazo")!=null)?request.getParameter("claveTipoPlazo"):"";
	String fsolicitudIni = (request.getParameter("fsolicitudIni")!=null)?request.getParameter("fsolicitudIni"):"";
	String fsolicitudFin = (request.getParameter("fsolicitudFin")!=null)?request.getParameter("fsolicitudFin"):"";	
	String clave_solicitud  = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";				
	
	String tipoContratacion  = (request.getParameter("tipoContratacion")!=null)?request.getParameter("tipoContratacion"):"";				
	String estatusAnterior  = (request.getParameter("estatusAnterior")!=null)?request.getParameter("estatusAnterior"):"";				
	String cambiaEstatus  = (request.getParameter("cambiaEstatus")!=null)?request.getParameter("cambiaEstatus"):"";	
	String estatusProrroga  = (request.getParameter("estatusProrroga")!=null)?request.getParameter("estatusProrroga"):"";	
	String  nombrePymes ="", noProveedor_An ="";
	String clave_epo = iNoCliente;
			
	String infoRegresar = "";
	int start = 0, limit = 0;
	JSONObject 	resultado	= new JSONObject(); 
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	
	StringBuffer nombreContactoAviso = new StringBuffer();//FODEA-024-2014 MOD()
	String cedente="",mensajeNuevo="",cedenteLista = "";//FODEA-024-2014 MOD()
	
	CesionEJB  cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);


	String clasificacionEpo = cesionBean.clasificacionEpo(iNoCliente);
	Vector lovDatos = cesionBean.getCamposAdicionales(iNoCliente, "5");
	int indiceCamposAdicionales = lovDatos.size();
	String campo01="", campo02 ="",campo03 ="",campo04 ="", campo05 ="";	
	if(indiceCamposAdicionales>0){
		for(int i = 0; i < indiceCamposAdicionales; i++){
			List campos =(List)lovDatos.get(i);
			if(i==0) 	campo01=  campos.get(1).toString();
			if(i==1) 	campo02=  campos.get(1).toString();
			if(i==2) 	campo03=  campos.get(1).toString();
			if(i==3) 	campo04=  campos.get(1).toString();
			if(i==4) 	campo05=  campos.get(1).toString();			
		}
	}
	
	if(informacion.equals("ValoresIniciales")   && !clave_epo.equals("")){

		Map mpParams = cesionBean.getParametrosEpo(clave_epo);
		resultado.put("success", new Boolean(true));
		resultado.put("CDER_CS_PRORROGA_CONTRATO",(String)mpParams.get("CDER_CS_PRORROGA_CONTRATO"));
		infoRegresar = resultado.toString();
	
	}else if(informacion.equals("CatalogoIF")   && !clave_epo.equals("")){

		CatalogoIF catalogo = new CatalogoIF();
		catalogo.setClave("ic_if");
		catalogo.setDescripcion("cg_razon_social");
		catalogo.setG_cs_cesion_derechos("S");		
		infoRegresar = catalogo.getJSONElementos();	
		
		
		
	}	else if(informacion.equals("CatalogoPyme")   && !clave_epo.equals("")){
		CatalogoPYME catalogo = new CatalogoPYME();
		catalogo.setCampoClave("IC_PYME");
		catalogo.setCampoDescripcion("CG_RAZON_SOCIAL");
		catalogo.setCesionDerechos("S");	
		infoRegresar = catalogo.getJSONElementos();	
		
	}	else if(informacion.equals("CatalogoEstatus")  ){

		List estatusSolic = new ArrayList();
		estatusSolic.add("2"); // En Proceso
		estatusSolic.add("3"); // Solicitud Aceptada
		estatusSolic.add("5"); // Solicitud Rechazada
		estatusSolic.add("6");	//Solicitud Reactivada
		estatusSolic.add("8");	//Solicitud Vencida		 
				
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setTabla("cdercat_estatus_solic");
		catalogo.setCampoClave("ic_estatus_solic");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setOrden("ic_estatus_solic");
		catalogo.setOrden("ic_estatus_solic");
		catalogo.setValoresCondicionIn(estatusSolic);
			
		infoRegresar = catalogo.getJSONElementos();	
	
	}	else if(informacion.equals("CatalogoTipoPlazo")   && !clave_epo.equals("")){

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setTabla("cdercat_tipo_plazo");
		catalogo.setCampoClave("ic_tipo_plazo");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setOrden("ic_tipo_plazo");
		infoRegresar = catalogo.getJSONElementos();	

} else if(informacion.equals("ConsultarResumen")  ){

	List registrosR = cesionBean.consultaSolicitudes(clave_epo);
				String pymeAviso = "";
	if(registrosR.size()>0){	 
		for(int e = 0; e< registrosR.size(); e++){	 
			List registros1 = (List)registrosR.get(e);	
			String noSsolicitud = (String)registros1.get(0); 
			String nombre=(String)registros1.get(1); 
			String pyme2  =(String)registros1.get(2);
			String rfc =(String)registros1.get(3); 
			String noProveedor =(String)registros1.get(4); 
			 String fecha =(String)registros1.get(5);
			String noContrato2 =(String)registros1.get(6); 
			String monto =(String)registros1.get(7);
			String firmaContrato =(String)registros1.get(8);
			String solicitudProrroga =(String)registros1.get(9);
			
			String empresas =(String)registros1.get(10);
			//INFO DEBUG: VERIFICAR QUE EMPRESAS REPRESENTADAS SE DEBEN DE MOSTRAR???????????????????????????????
			cedenteLista = pyme2+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):"");//-------PARA EL GRID
			pymeAviso = pyme2;
			if(!noProveedor.equals(noProveedor_An))  {
				nombrePymes += pyme2+",";
			}
			
			noProveedor_An = noProveedor;
			
			datos = new HashMap();
			datos.put("CLAVE_SOLICITUD", noSsolicitud);
		
			datos.put("CECIONARIO", nombre);
			//datos.put("PYME", pyme2.replaceAll(";","<BR>"));
			datos.put("PYME", cedenteLista);
			datos.put("CLAVE_PYME", noProveedor);			
			datos.put("RFC", rfc);
			datos.put("FSOLICITUDINI", fecha);
			datos.put("NOCONTRATO", noContrato2);
			datos.put("MONTO_MONEDA", monto);	
			datos.put("FIRMA_CONTRATO", firmaContrato);	
			datos.put("SOLICITUD_PRORROGA",solicitudProrroga);
			
			StringBuffer nombreContacto = new StringBuffer();
				
				UtilUsr utilUsr = new UtilUsr();
				boolean perfilIndicado = false;
				nombreContactoAviso = new StringBuffer();//FODEA-024-2014 MOD()
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
				for(int i=0;i<usuariosPorPerfil.size();i++){
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true; 
					if(i>0){
						nombreContactoAviso.append(" / ");//FODEA-024-2014 MOD()
					}
					//FODEA-024-2014 MOD()
					nombreContactoAviso.append(usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getNombre()+" ");
					nombreContactoAviso.append("\n");
				}	
				datos.put("REPRESENTATE_LEGAL",nombreContactoAviso.toString());	
			
			
	//datos.put("mensajeN",mensajeNuevo);
			
			registros.add(datos);	
		}
		if(registrosR.size()>0){
			cedente = (String) session.getAttribute("strNombre")+ " y su(s) empresa(s) representada(s)";
		}else if (registrosR.size() == 1) {
			cedente = cedenteLista.replaceAll("<br>,","");
		}
		
	}
	
	
	mensajeNuevo="<table align='center' style='text-align: justify;' border='1' width='700' cellpadding='0' cellspacing='0'>"+
								"<tr>"+
								"<td class='formas' align='left'>"+
								" De conformidad con lo señalado en el(los) Contrato(s) cuyos datos se señalan en esta pantalla, "+nombreContactoAviso+", como representante(s) legal(es) de la Empresa "+pymeAviso+
								", solicito(tamos) a "+strNombre+" su consentimiento para ceder a favor del INTERMEDIARIO FINANCIERO 	señalado para estos efectos, los derechos de cobro sobre la(s) factura(s) por trabajos ejecutados, "+
								"	por el 100% (cien por ciento) del monto de dicho Contrato."+
								"	\n\n	En virtud de lo anterior una vez que cuente con el consentimiento que por este medio le es solicitado, "+
								" celebraremos el contrato de cesión de derechos de cobro con el INTERMEDIARIO FINANCIERO, quien procederá a notificar mediante el "+
								" Sistema NAFIN, en términos de Código de Comercio.	"+								
								"</td>"+
								"</tr>"+
								"</table>";		
						
			
					datos = new HashMap();
					
	
				

	String consulta2 =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consulta2);
	resultado.put("mensaje", mensajeNuevo);	
	
	infoRegresar = resultado.toString();		


} else if(informacion.equals("Aviso")){
	resultado.put("mensaje", mensajeNuevo);	
	infoRegresar = resultado.toString();	
}else if(informacion.equals("ArchivoResumen")  ){

	List registrosR = cesionBean.consultaSolicitudes(clave_epo);
	String pymeAviso = "";
	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = archivo.nombreArchivo()+".pdf";
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String usuario = iNoUsuario;
	String nombreUsuario =strNombreUsuario;
	SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:SS a");	//HH es formato 24 horas.
	String hora = sdf.format(new java.util.Date());
	String dia	= new SimpleDateFormat("dd/mm/yyyy").format(new java.util.Date());
	String UsuarioNom = usuario +" "+nombreUsuario;
		
	pdfDoc.encabezadoConImagenesPersonalizado(pdfDoc,(String)session.getAttribute("strPais"),
															((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
															(String)session.getAttribute("sesExterno"),								 
															(String) session.getAttribute("strNombre"),
															(String) session.getAttribute("strNombreUsuario"),
														"00archivos/15cadenas/15archcadenas/logos/" + "nafin.gif","/00utils/gif/ba_cadpro.jpg",strDirectorioPublicacion);
		//FODEA-024-2014 MOD()
				UtilUsr utilUsr = new UtilUsr();
				boolean perfilIndicado = false;
				nombreContactoAviso = new StringBuffer();//FODEA-024-2014 MOD()
				if(registrosR.size()>0){	 
					for(int e = 0; e< registrosR.size(); e++){	 
						List registros1 = (List)registrosR.get(e);
						String pyme2  =(String)registros1.get(2);
						String noProveedor =(String)registros1.get(4); 
						pymeAviso = pyme2;
						if(!noProveedor.equals(noProveedor_An) )  {
							nombrePymes += pyme2+",";
						}
						noProveedor_An=noProveedor;
						
						
						
						String empresas =(String)registros1.get(10);
						//INFO DEBUG: VERIFICAR QUE EMPRESAS REPRESENTADAS SE DEBEN DE MOSTRAR???????????????????????????????
						cedenteLista = pyme2+((empresas!="")?", "+empresas.replaceAll(";",", "):"");//-------PARA EL GRID
						
					}	
					
					if(registrosR.size()>0){
						cedente = (String) session.getAttribute("strNombre")+ " y su(s) empresa(s) representada(s)";
					}else if (registrosR.size() == 1) {
						cedente = cedenteLista.replaceAll("<br>,","");
					}
					
				}
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor_An, "P");
				//List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("6", "P");
				for(int i=0;i<usuariosPorPerfil.size();i++){
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true; 
					if(i>0){
						nombreContactoAviso.append(" / ");
					}
					nombreContactoAviso.append(usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getNombre()+" ");					
				}	
						
		/*	 mensajeNuevo="De conformidad con lo señalado en el(los) Contrato(s) cuyos datos se señalan en esta pantalla, "+nombreContactoAviso+", como representante(s) legal(es) de la Empresa "+pymeAviso+
			", solicito(tamos) a "+strNombre+" su consentimiento para ceder a favor del INTERMEDIARIO FINANCIERO 	señalado para estos efectos, los derechos de cobro sobre la(s) factura(s) por trabajos ejecutados, "+
			"	por el 100% (cien por ciento) del monto de dicho Contrato."+
			"	\n\n	En virtud de lo anterior una vez que cuente con el consentimiento que por este medio le es solicitado, "+
			" celebraremos el contrato de cesión de derechos de cobro con el INTERMEDIARIO FINANCIERO, quien procederá a notificar mediante el "+
			" Sistema NAFIN, en términos de Código de Comercio.	";	
			*/
			
		pdfDoc.setTable(1,70);
	//pdfDoc.setCell(mensajeNuevo,"formas",ComunesPDF.JUSTIFIED);//FODEA-024-2014 MOD()
	pdfDoc.addTable();
	
	pdfDoc.addText(" ");	
	
	pdfDoc.setTable(8,80);
	pdfDoc.setCell("Intermediario Financiero (Cesionario)","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setCell("Pyme(Cedente)","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setCell("RFC","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setCell("No Proveedor ","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de Solicitud Pyme","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setCell("No. Contrato","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setCell("Firma Contrato","celda01rep",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto / Moneda","celda01rep",ComunesPDF.LEFT);
	int contadorSolicitudProrroga=0;
	String solicitudProrrogaTmp="";
	if(registrosR.size()>0){	 
		for(int e = 0; e< registrosR.size(); e++){	 
			List registros1 = (List)registrosR.get(e);	
			String noSsolicitud = (String)registros1.get(0); 
			String nombre=(String)registros1.get(1); 
			String pyme2  =(String)registros1.get(2);
			
			String empresas  =(String)registros1.get(10);
			String cedenteListaPDF = pyme2+((empresas!="")?"\n"+empresas.replaceAll(";","\n"):"");//-----------PARA EL PDF
			//FODEA-024-2014 MOD()
			String []pyme2Arr=pyme2.split(";");
			if(pyme2Arr.length==1){
				pyme2= pyme2.replaceAll(";","");
			}else{
				pyme2= pyme2.replaceAll(";","\n");
			}
			//
			String rfc =(String)registros1.get(3); 
			String noProveedor =(String)registros1.get(4); 
			String fecha =(String)registros1.get(5);
			String noContrato2 =(String)registros1.get(6); 
			String monto =(String)registros1.get(7);
			String firmaContrato =(String)registros1.get(8);
			String solicitudProrroga =(String)registros1.get(9);
			if(!solicitudProrroga.equals(solicitudProrrogaTmp)){
					if(solicitudProrroga.equals("true")){
					pdfDoc.setCell("Registros que Solicitaron Prorroga","celda01rep",ComunesPDF.LEFT,8);
				}else{
						pdfDoc.setCell("Solicitudes Pendientes por Aceptar","celda01rep",ComunesPDF.LEFT,8);
				}
				
			}
		
			solicitudProrrogaTmp=solicitudProrroga;
				
			
			pdfDoc.setCell(nombre,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(cedenteListaPDF,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(rfc, "formas",ComunesPDF.CENTER);
			pdfDoc.setCell(noProveedor,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecha,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(noContrato2,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(firmaContrato,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(monto.replaceAll("<br/>", "\n"),"formas",ComunesPDF.CENTER);
			
			
			
		}
		pdfDoc.addTable();	
	}
	pdfDoc.endDocument();
	
	JSONObject jsonObj = new JSONObject();		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Enterado")  ){

	String	solicituds[]	=	request.getParameterValues("solicituds");
	List idParams = new ArrayList();
	for(int i=0;i<solicituds.length;i++) {	
			idParams.add(solicituds[i]);		
	}		
	
	String aceptaEnterado = cesionBean.aceptaEnterado(idParams);
	resultado.put("success", new Boolean(true));
	resultado.put("aceptaEnterado", aceptaEnterado);	
	infoRegresar = resultado.toString();


} else if(informacion.equals("Consultar")  || informacion.equals("ArchivoPDF")  	|| informacion.equals("ArchivoCSV")  ){
	
	
		//esto es para saber que logo corresponde a la EPO
		File logo = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/");
		File fdGif = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + clave_epo + ".gif");
		File fdGifHome = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + clave_epo + "_home.gif");
		String logoEpo =""; 
		if(fdGif.exists() || fdGifHome.exists()) {
			strLogo = clave_epo + ".gif"; 
			logoEpo= logo +"/"+ clave_epo + ".gif";;
			if(fdGifHome.exists()) {
				session.setAttribute("sesLogoAlterno", clave_epo + "_home.gif");
			}
		} else {
			strLogo = "nafin.gif";
			logoEpo= logo +"/"+"nafin.gif";
		}
		
		String consulta ="";
		ConsultaSolicitudConsentimiento paginador = new com.netro.cesion.ConsultaSolicitudConsentimiento();
			
		//pasar parametros al Paginador 	
		paginador.setNoIF(noIF);
		paginador.setEpo(clave_epo);
		paginador.setPyme(pyme);
		paginador.setEstatus(estatus);
		paginador.setNoContrato(noContrato);
		paginador.setFvigenciaIni(fvigenciaIni);
		paginador.setFvigenciaFin(fvigenciaFin);
		paginador.setPlazoContrato(plazoContrato);
		paginador.setClaveTipoPlazo(claveTipoPlazo);
		paginador.setfsolicitudIni(fsolicitudIni);
		paginador.setfsolicitudFin(fsolicitudFin);		
		paginador.setLoginUsuarioEpo(strLogin);		
		paginador.setEstatusProrroga(estatusProrroga);
		 
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		Registros registrosC =null;
		
		if(informacion.equals("Consultar")) {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta  = queryHelper.getJSONPageResultSet(request,start,limit);	
				registrosC 	= queryHelper.getPageResultSet(request,start,limit);
				
				
			
				while (registrosC.next()) {
				
					String estatus2 = registrosC.getString("NoEstatus");
					String fAceptacion = registrosC.getString("FAceptacion");
					String tipoContratacion2 = registrosC.getString("tipoContratacion");
					String pyme2 = (registrosC.getString("Pyme")==null)?"":registrosC.getString("Pyme").trim();
                    //System.out.println("pyme2:"+pyme2);
					String NoProveedor = (registrosC.getString("NoProveedor")==null)?"":registrosC.getString("NoProveedor").trim();
					String clave_solicitud2 = registrosC.getString("clave_solicitud");
					String cesionario = registrosC.getString("Cecionario");					
					String rfc = registrosC.getString("Rfc");
					String  noProveedor = 	registrosC.getString("NoProveedor");
					String fechaSolict = registrosC.getString("FSolicitudIni");
					String noContrato2 = registrosC.getString("NoContrato");
					StringBuffer moneda =  cesionBean.getMontoMoneda(clave_solicitud2);
					String contratacion= registrosC.getString("Contratacion");
					String fechaInicon = registrosC.getString("fecha_inicio_contrato");
					String  fechaFinCon = registrosC.getString("fecha_fin_contrato");
					String  plazoContra = 	registrosC.getString("plazo_contrato");
					String clasificacion = 	registrosC.getString("Clasificacion");
					String campo1 = (registrosC.getString("Campo1")==null)?"":registrosC.getString("Campo1");
					String campo2 = (registrosC.getString("Campo2")==null)?"":registrosC.getString("Campo2");	
					String campo3 = (registrosC.getString("Campo3")==null)?"":registrosC.getString("Campo3");		
					String campo4 = (registrosC.getString("Campo4")==null)?"":registrosC.getString("Campo4");		
					String campo5 = (registrosC.getString("Campo5")==null)?"":registrosC.getString("Campo5");	
					String ventanilla = registrosC.getString("venanilla_pago");
					String sup_adm = 	registrosC.getString("sup_adm_resob");
					String telefono = 	registrosC.getString("numero_telefono");
					String objeto= registrosC.getString("ObContrato");
					String comentarios = registrosC.getString("comentarios");
					String fechaAceptacion = registrosC.getString("FAceptacion");
					String desCestatus = registrosC.getString("Estatus");					
					String noEpo = registrosC.getString("noEpo"); 
					String nombreEpo = registrosC.getString("dependencia"); 
					String fechaSolicitud = registrosC.getString("fechasolicitud"); 
					String firmaContrato = registrosC.getString("firma_contrato"); 
					estatusProrroga = registrosC.getString("estatus_prorroga"); 
					String grupoCesion = registrosC.getString("grupo_cesion"); 
					//Se obtiene el representante Legal de la Pyme
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
                    
                    List lEmpresasGrupo =  cesionBean.getEmpresasXGrupoCder(grupoCesion);
                    //System.out.println("lEmpresasGrupo:"+lEmpresasGrupo);
                    for(int i=0;i<lEmpresasGrupo.size();i++) {
                        HashMap hmEmpresa = (HashMap)lEmpresasGrupo.get(i);
                        String str_representantes = (String)hmEmpresa.get("representantes");
                        str_representantes = str_representantes.replace(", ", ", <br>");
                        if (i > 0) {
							nombreContacto.append(" / <br>");
						}
                        nombreContacto.append(str_representantes);
                    }
                    /*
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(NoProveedor, "P");					
					for (int i = 0; i < usuariosPorPerfil.size(); i++) {
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if (i > 0) {
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
					}
                    */
                    
					String imagenlogo = "/nafin/00archivos/15cadenas/15archcadenas/logos/"+strLogo;
					datos = new HashMap();
					datos.put("CECIONARIO", cesionario);
					//FODEA-024-2014 MOD()
					String []pyme2Ar=pyme2.split(";");
					if(pyme2Ar.length==1){
						pyme2 = pyme2.replaceAll(";","");
					}else{
						pyme2 = pyme2.replaceAll(";","<br>");
					}
					datos.put("PYME", pyme2);
					datos.put("RFC", rfc);
					datos.put("CLAVE_PYME", noProveedor);
					datos.put("REPRESENTANTE", nombreContacto.toString());
					datos.put("FSOLICITUDINI", fechaSolict);
					datos.put("NOCONTRATO", noContrato2);
					datos.put("MONTO_MONEDA", moneda.toString());
					datos.put("CONTRATACION", contratacion);
					datos.put("FECHA_INICIO_CONTRATO", fechaInicon);
					datos.put("FECHA_FIN_CONTRATO", fechaFinCon);
					datos.put("PLAZO_CONTRATO", plazoContra);
					datos.put("CLASIFICACION", clasificacion);
					datos.put("CAMPO1", campo1);
					datos.put("CAMPO2", campo2);
					datos.put("CAMPO3", campo3);
					datos.put("CAMPO4", campo4);
					datos.put("CAMPO5", campo5);
					datos.put("VENANILLA_PAGO", ventanilla);
					datos.put("SUP_ADM_RESOB", sup_adm);
					datos.put("NUMERO_TELEFONO", telefono);
					datos.put("OBCONTRATO", objeto);
					datos.put("COMENTARIOS", comentarios);
					datos.put("FACEPTACION", fechaAceptacion);
					datos.put("ESTATUS", desCestatus);
					datos.put("CLAVE_SOLICITUD", clave_solicitud2);
					datos.put("TIPOCONTRATACION", tipoContratacion2);
					datos.put("NOESTATUS", estatus2);
					datos.put("NOEPO", noEpo);
					datos.put("LOGO", imagenlogo);
					datos.put("NOMBREEPO",nombreEpo);
					datos.put("FECHASOLICITUD",fechaSolicitud);
					datos.put("FIRMA_CONTRATO",firmaContrato);
					datos.put("ESTATUS_PRORROGA",estatusProrroga);
					datos.put("GRUPO_CESION",grupoCesion);					
					
					registros.add(datos);		
			
				}	
							
				String consulta2 =  "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.toString()+"}";
				resultado = JSONObject.fromObject(consulta2);
				resultado.put("clasificacionEpo", clasificacionEpo);	
				resultado.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));	 //numero de campos Adicionales
				resultado.put("campo01",campo01);
				resultado.put("campo02",campo02);
				resultado.put("campo03",campo03);
				resultado.put("campo04",campo04);
				resultado.put("campo05",campo05);	
	
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
		}
		
		infoRegresar = resultado.toString(); 	
	
		} else 	if (informacion.equals("ArchivoPDF")) {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}	
			
		} else 	if (informacion.equals("ArchivoCSV")) {	 
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}	
		}
	
	} else if (informacion.equals("CONTRATO")) {
		
		JSONObject jsonObj = new JSONObject();			
		ImagenDocumentoContrato imagen = new ImagenDocumentoContrato();
		imagen.setSolicitud(clave_solicitud);
		String nombreArchivo = 	imagen.consultarImagen(strDirectorioTemp);
		
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();

	} else if (informacion.equals("ConsultarCorreo")) {		

		//esto es para saber que logo corresponde a la EPO
		File logo = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/");
		File fdGif = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + clave_epo + ".gif");
		File fdGifHome = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + clave_epo + "_home.gif");
		String logoEpo =""; 
		if(fdGif.exists() || fdGifHome.exists()) {
			strLogo = clave_epo + ".gif";
			logoEpo= logo +"/"+ clave_epo + ".gif";;
			if(fdGifHome.exists()) {
				session.setAttribute("sesLogoAlterno", clave_epo + "_home.gif");
			}
		} else {
			strLogo = "nafin.gif";
			logoEpo= logo +"/"+"nafin.gif";
		}
		
		//Metodo para mostrar el archivo, para enviarlo como adjunto	
		ImagenDocumentoContrato imagen = new ImagenDocumentoContrato();
		imagen.setSolicitud(clave_solicitud);		
		String 	nombreArchivo = 	imagen.consultarImagen(strDirectorioTemp);
		String archivo = strDirectorioTemp+nombreArchivo;
		
		
		List Registro = new ArrayList();
		Registro= cesionBean.inicializarAvisoNotificacionCesion(clave_solicitud);//line 6142
	
		if(Registro!=null ){
		 for(int i=0;i<Registro.size();i++){
			List laux = (List)Registro.get(i);				
			String cesionario     =((laux.get(0))==null)?"":(laux.get(0).toString());
			String nompyme     =((laux.get(1))==null)?"":(laux.get(1).toString());
			String rfc =  ((laux.get(2))==null)?"":(laux.get(2).toString());
			String Proveedor =  ((laux.get(3))==null)?"":(laux.get(3).toString());
			String FSolicitud =  ((laux.get(4))==null)?"":(laux.get(4).toString());
			String contrato       = ((laux.get(5))==null)?"":(laux.get(5).toString());
			String contratacion   = ((laux.get(8))==null)?"":(laux.get(8).toString());
			String fechaCont_Ini  = ((laux.get(9))==null)?"":(laux.get(9).toString());
			String fechaCont_Fin  = ((laux.get(10))==null)?"":(laux.get(10).toString());
			String clasificionEpo = ((laux.get(11))==null)?"":(laux.get(11).toString());
			String campo1 			= ((laux.get(12))==null)?"":(laux.get(12).toString());
			String campo2         = ((laux.get(13))==null)?"":(laux.get(13).toString());
			String campo3         = ((laux.get(14))==null)?"":(laux.get(14).toString());
			String campo4         = ((laux.get(15))==null)?"":(laux.get(15).toString());
			String campo5         = ((laux.get(16))==null)?"":(laux.get(16).toString());
			String objeto         = ((laux.get(17))==null)?"":(laux.get(17).toString());
			String fechaLimite    = ((laux.get(18))==null)?"":(laux.get(18).toString());			
			String estatus2      = ((laux.get(19))==null)?"":(laux.get(19).toString());
			String solicitud      = ((laux.get(20))==null)?"":(laux.get(20).toString());
			String fechaAceptacion      = ((laux.get(23))==null)?"":(laux.get(23).toString());
			String noestatus =  ((laux.get(21))==null)?"":(laux.get(21).toString());
			String plazoContrato2 = ((laux.get(25))==null)?"":(laux.get(25).toString());
			String venanillaPago = ((laux.get(26))==null)?"":(laux.get(26).toString());
			String supAdmResob = ((laux.get(27))==null)?"":(laux.get(27).toString());
			String numeroTelefono = ((laux.get(28))==null)?"":(laux.get(28).toString());
			String firmaContrato = ((laux.get(29))==null)?"":(laux.get(29).toString());
			
			String empresas = ((laux.get(30))==null)?"":(laux.get(30).toString());
			
			StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(solicitud);
			
			datos = new HashMap();
			datos.put("CECIONARIO", cesionario);
			cedenteLista = nompyme+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):"");
			String []nomPyneAux=nompyme.split(";");
			if(nomPyneAux.length==1){
				nompyme = nompyme.replaceAll(";","");
			}else{
				nompyme = nompyme.replaceAll(";","<br>");
			}
			
			//datos.put("PYME", nompyme);
			datos.put("PYME", cedenteLista);
			datos.put("RFC", rfc);
			datos.put("CLAVE_PYME", Proveedor);			
			datos.put("FSOLICITUDINI", FSolicitud);
			datos.put("NOCONTRATO", contrato);
			datos.put("MONTO_MONEDA", montosPorMoneda.toString());
			datos.put("CONTRATACION", contratacion);
			datos.put("FECHA_INICIO_CONTRATO", fechaCont_Ini);
			datos.put("FECHA_FIN_CONTRATO", fechaCont_Fin);
			datos.put("PLAZO_CONTRATO", plazoContrato2);
			datos.put("CLASIFICACION", clasificionEpo);
			datos.put("CAMPO1", campo1);
			datos.put("CAMPO2", campo2);
			datos.put("CAMPO3", campo3);
			datos.put("CAMPO4", campo4);
			datos.put("CAMPO5", campo5);
			datos.put("VENANILLA_PAGO", venanillaPago);
			datos.put("SUP_ADM_RESOB", supAdmResob);
			datos.put("NUMERO_TELEFONO", numeroTelefono);		
			datos.put("OBCONTRATO", objeto);		
			datos.put("FACEPTACION", fechaAceptacion);
			datos.put("CLAVE_SOLICITUD", clave_solicitud);
			datos.put("ESTATUS", estatus2);
			datos.put("LOGO", logoEpo);
			datos.put("ARCHIVO", archivo);	
			datos.put("FIRMA_CONTRATO",firmaContrato);
			registros.add(datos);			
			}
		}
			
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta);		
		resultado.put("clasificacionEpo", clasificacionEpo);	
		resultado.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
		resultado.put("strLogo",strLogo);		
		resultado.put("archivo",archivo);
		resultado.put("nombreArchivo",nombreArchivo);
		resultado.put("campo01",campo01);
		resultado.put("campo02",campo02);
		resultado.put("campo03",campo03);
		resultado.put("campo04",campo04);
		resultado.put("campo05",campo05);	
		infoRegresar = resultado.toString();
		
	} else if (informacion.equals("ArchivoAdjunto")) {		
	
		//Metodo para mostrar el archivo, para enviarlo como adjunto	
		ImagenDocumentoContrato imagen = new ImagenDocumentoContrato();
		imagen.setSolicitud(clave_solicitud);		
		String 	nombreArchivo = 	imagen.consultarImagen(strDirectorioTemp);
		String archivo = strDirectorioTemp+nombreArchivo;
		
		JSONObject jsonObj = new JSONObject();		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		infoRegresar = jsonObj.toString();
				
	} else if (informacion.equals("ValidandoCorreo")) {		
		String mensaje  =""; 
		//Validar que se haya enviado correo de notificacion al proveedor si la accion a ejecutar es Aceptar ( cambiaEstatus == 2 )
		if ("2".equals(cambiaEstatus) && !cesionBean.proveedorNotificado(clave_solicitud)) {
			mensaje = "Es necesario enviar el correo de aviso para poder aceptar la solicitud.";			
		}
		// Fodea 041 - 2010 By JSHD
		// Si la solicitud se encontraba vencida, y se ha ejecutado la opcion reactivar  (cambiaEstatus == 8 ),
		// resetear el campo CDER_SOLICITUD.CS_NOTIF_PROVEEDOR para solicitar
		// que se vuelva a enviar nuevamente el correo de notificacion al proveedor
		if ("8".equals(cambiaEstatus)) {
			cesionBean.resetNotificacionProveedor(clave_solicitud);
		}
		if(mensaje.equals("")) {
			//cambia el estatus Reactivada o Aceptada			
			cesionBean.solicitudEnviActivarReactivar(clave_solicitud, cambiaEstatus, "");
		}
		resultado.put("success", new Boolean(true));	
		resultado.put("mensaje",mensaje);	
		resultado.put("clave_solicitud",clave_solicitud);	
		resultado.put("tipoContratacion",tipoContratacion);	
		resultado.put("estatusAnterior",estatusAnterior);	
		infoRegresar = resultado.toString();
		
	} else if (informacion.equals("ConsultarRechazo")   || informacion.equals("ConsultarPreAcuse") ) {	
	
		StringBuffer texto = new StringBuffer();
		//muestra el Preacuse 
		List	Registro = cesionBean.inicializarPreAcuseActivaReactiva(clave_solicitud);
		String nombrePymeCedente ="";
		for(int i=0;i<Registro.size();i++){
			List laux = (List)Registro.get(i);		
			String cesionario     =((laux.get(0))==null)?"":(laux.get(0).toString());
			String nompyme     =((laux.get(1))==null)?"":(laux.get(1).toString());
//			nombrePymeCedente = ((laux.get(1))==null)?"":(laux.get(1).toString());
			String rfc =  ((laux.get(2))==null)?"":(laux.get(2).toString());
			String Proveedor =  ((laux.get(3))==null)?"":(laux.get(3).toString());
			String FSolicitud =  ((laux.get(4))==null)?"":(laux.get(4).toString());
			String contrato       = ((laux.get(5))==null)?"":(laux.get(5).toString());
			String contratacion   = ((laux.get(8))==null)?"":(laux.get(8).toString());
			String fechaCont_Ini  = ((laux.get(9))==null)?"":(laux.get(9).toString());
			String fechaCont_Fin  = ((laux.get(10))==null)?"":(laux.get(10).toString());
			String clasificionEpo = ((laux.get(11))==null)?"":(laux.get(11).toString());
			String campo1 			= ((laux.get(12))==null)?"":(laux.get(12).toString());
			String campo2         = ((laux.get(13))==null)?"":(laux.get(13).toString());
			String campo3         = ((laux.get(14))==null)?"":(laux.get(14).toString());
			String campo4         = ((laux.get(15))==null)?"":(laux.get(15).toString());
			String campo5         = ((laux.get(16))==null)?"":(laux.get(16).toString());
			String objeto         = ((laux.get(17))==null)?"":(laux.get(17).toString());
			String fechaLimite    = ((laux.get(18))==null)?"":(laux.get(18).toString());			
			String estatus2      = ((laux.get(19))==null)?"":(laux.get(19).toString());
			String solicitud      = ((laux.get(20))==null)?"":(laux.get(20).toString());
			String fechaAceptacion      = ((laux.get(23))==null)?"":(laux.get(23).toString());
			String noestatus =  ((laux.get(21))==null)?"":(laux.get(21).toString());
			String comentarios  = ((laux.get(24))==null)?"":(laux.get(24).toString());
			String plazoContrato2 = ((laux.get(25))==null)?"":(laux.get(25).toString());
			String venanillaPago = ((laux.get(26))==null)?"":(laux.get(26).toString());
			String supAdmResob = ((laux.get(27))==null)?"":(laux.get(27).toString());
			String numeroTelefono = ((laux.get(28))==null)?"":(laux.get(28).toString());
			String fechaSolicitud = ((laux.get(29))==null)?"":(laux.get(29).toString());
			String nombreEpo = ((laux.get(30))==null)?"":(laux.get(30).toString());
			String firmaContrato = ((laux.get(31))==null)?"":(laux.get(31).toString());
			
			String empresas = ((laux.get(32))==null)?"":(laux.get(32).toString());
			nombrePymeCedente = nompyme+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):"");
			
			String claveGrupo = ((laux.get(33))==null)?"":(laux.get(33).toString());
			
			if(!"".equals(claveGrupo)){
				nompyme=""; 
				List lstEmpresas = cesionBean.getEmpresasXGrupoCder(claveGrupo);
				for(int x=0; x<lstEmpresas.size(); x++){
					nompyme += (String)((HashMap)lstEmpresas.get(x)).get("cg_razon_social")+" , "; 	
				}
			} 
				
				
			StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(solicitud);
			String estatusAcuse ="", estatuscodigo ="";  
			
			if(estatusAnterior.equals("2") && informacion.equals("ConsultarRechazo")) {
				estatuscodigo ="En Proceso";
					estatusAcuse ="5";			
			}
		
			if(estatusAnterior.equals("2") && !informacion.equals("ConsultarRechazo")){
				estatuscodigo ="Solicitud Aceptada";
				estatusAcuse ="3";	
			}else if(estatusAnterior.equals("8")){
				estatuscodigo ="Solicitud Reactivada";
				estatusAcuse ="6";
			}
			if(estatusAnterior.equals("6") ){
				estatusAcuse ="3";			
			}
		
			//Se obtiene el representante Legal de la Pyme
			StringBuffer nombreContacto = new StringBuffer();
			UtilUsr utilUsr = new UtilUsr();
			boolean usuarioEncontrado = false;
			boolean perfilIndicado = false;
            
            List lEmpresasGrupo =  cesionBean.getEmpresasXGrupoCder(claveGrupo);
            //System.out.println("lEmpresasGrupo:"+lEmpresasGrupo);
            for(int j=0;j<lEmpresasGrupo.size();j++) {
                HashMap hmEmpresa = (HashMap)lEmpresasGrupo.get(j);
                String str_representantes = (String)hmEmpresa.get("representantes");
                str_representantes = str_representantes.replace(", ", ", <br>");
                if (j > 0) {
                    nombreContacto.append(" / <br>");
                }
                nombreContacto.append(str_representantes);
            }
            /*
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(Proveedor, "P");			
			for (int y = 0; y < usuariosPorPerfil.size(); y++) {
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);					
				Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
				perfilIndicado = true;
				if(i>0){
					nombreContacto.append(" / ");
				}
				nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno());
				nombreContacto.append("\n");
			}
            */
			
			texto.append("Nombre del Cesionario::  "+cesionario.replace(',',' ')+","+"\n");
			texto.append("Pyme Cedente::   "+nompyme.replace(',',' ')+","+"\n");
			texto.append("RFC::  "+rfc.replace(',',' ')+","+"\n");
			texto.append("No Proveedor:: "+Proveedor.replace(',',' ')+","+"\n");
			texto.append("No de Contrato::  "+contrato.replace(',',' ')+","+"\n");
			texto.append("Monto / Moneda::  "+montosPorMoneda.toString().replaceAll("<br/>", "|")+","+"\\n");
			texto.append("Tipo de Contratacion:: "+contratacion.replace(',',' ')+","+"\n");
			texto.append("F.Inicio Contrato:: "+fechaCont_Ini.replace(',',' ')+","+"\n");
			texto.append("F. Final Contrato::  "+fechaCont_Fin.replace(',',' ')+","+"\n");
			texto.append("Plazo del Contrato::  "+plazoContrato.replace(',',' ')+","+"\n");
			texto.append("Ciudad de Firma del Contrato:  "+clasificionEpo.replace(',',' ')+","+"\n");
			
			datos = new HashMap();
			datos.put("CECIONARIO", cesionario);
			datos.put("PYME", nompyme+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):""));
			datos.put("RFC", rfc);
			datos.put("CLAVE_PYME", Proveedor);			
			datos.put("FSOLICITUDINI", FSolicitud);
			datos.put("NOCONTRATO", contrato);
			datos.put("MONTO_MONEDA", montosPorMoneda.toString());
			datos.put("CONTRATACION", contratacion);
			datos.put("FECHA_INICIO_CONTRATO", fechaCont_Ini);
			datos.put("FECHA_FIN_CONTRATO", fechaCont_Fin);
			datos.put("PLAZO_CONTRATO", plazoContrato2);
			datos.put("CLASIFICACION", clasificionEpo);
			datos.put("CAMPO1", campo1);
			datos.put("CAMPO2", campo2);
			datos.put("CAMPO3", campo3);
			datos.put("CAMPO4", campo4);
			datos.put("CAMPO5", campo5);
			datos.put("VENANILLA_PAGO", venanillaPago);
			datos.put("SUP_ADM_RESOB", supAdmResob);
			datos.put("NUMERO_TELEFONO", numeroTelefono);		
			datos.put("OBCONTRATO", objeto);		
			datos.put("FACEPTACION", fechaAceptacion);
			datos.put("ESTATUS", estatuscodigo);
			datos.put("REPRESENTANTE", nombreContacto.toString());
			datos.put("COMENTARIOS", comentarios);
			datos.put("CLAVE_SOLICITUD", clave_solicitud);
			datos.put("TIPOCONTRATACION", tipoContratacion );
			datos.put("NOESTATUS", noestatus);
			datos.put("CAUSA_RECHAZO", "");
			datos.put("TEXTO_FIRMAR", texto.toString());
			datos.put("ESTATUS_ACUSE", estatusAcuse);
			datos.put("FECHASOLICITUD",fechaSolicitud);
			datos.put("NOMBREEPO",nombreEpo);
			datos.put("FIRMA_CONTRATO",firmaContrato);
			registros.add(datos);		
		}
		
		String NombreClasificacionEpo ="";
		if(!clasificacionEpo.equals("")) {
			NombreClasificacionEpo = clasificacionEpo;
		}else {
			NombreClasificacionEpo = "Ciudad de Firma del Contrato";
		}
		
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta);		
		resultado.put("clasificacionEpo", NombreClasificacionEpo);	
		resultado.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
		resultado.put("campo01",campo01);
		resultado.put("campo02",campo02);
		resultado.put("campo03",campo03);
		resultado.put("campo04",campo04);
		resultado.put("campo05",campo05);	
		resultado.put("nombrePymeCedente",nombrePymeCedente);	
		resultado.put("nombrePymeCedenteLista",nombrePymeCedente);	
		resultado.put("nombrePymeCedenteFila",nombrePymeCedente.replaceAll("<br>",", "));	
		resultado.put("strNombreUsuario",strNombreUsuario);
		infoRegresar = resultado.toString();

	} else if (informacion.equals("AutorizaRechazo") || informacion.equals("Autorizar")   ) {	
		%><%@ include file="../certificado.jspf" %><%
		String serial  = _serial;	
		String textoFirmado  = (request.getParameter("textoFirmado")!=null)?request.getParameter("textoFirmado"):"";	
		String estatusAcuse  = (request.getParameter("estatusAcuse")!=null)?request.getParameter("estatusAcuse"):"";
		String cauza_rechazo  = (request.getParameter("cauza_rechazo")!=null)?request.getParameter("cauza_rechazo"):"";
		
		String pkcs7  = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";	
		String folio  =  "MFJR"+new SimpleDateFormat("ddMMyy").format(new java.util.Date());	
		String usuario  = strLogin;	
		String nombreUsuario = 	strNombreUsuario;
		String validCert =application.getInitParameter("paramValidaSerialCrt");
		boolean validacion = true;			
		if (validCert.equals("N") ){
			validacion = true;
		}else {
			validacion = false;
		}
		
		String acuse  = cesionBean.transmitesolicitud(clave_solicitud,serial,textoFirmado,pkcs7,folio,validacion,usuario,nombreUsuario,estatusAcuse,cauza_rechazo,strDirectorioTemp );			
		boolean banderaRechazo=false;
		if(informacion.equals("AutorizaRechazo")){
			banderaRechazo=true;
		}
		resultado.put("banderaRechazo",new Boolean(banderaRechazo));
		resultado.put("success", new Boolean(true));
		resultado.put("acuse",acuse);					
		infoRegresar = resultado.toString();
			
	} else if (informacion.equals("CancelarRechazo")  || informacion.equals("CancelarPreAcuse") ) {	

		String cancelaPreacuse = cesionBean.cancelaPreacuse(clave_solicitud,estatusAnterior);
	
		resultado.put("success", new Boolean(true));	
		resultado.put("cancelaPreacuse",cancelaPreacuse);					
		infoRegresar = resultado.toString();

	} else if (informacion.equals("ConsultarAcuse")) {	
		
		//muestra el Preacuse 
		List	Registro = cesionBean.inicializarPreAcuseActivaReactiva(clave_solicitud);
		String nombrePymeCedente ="", acuse ="";
		for(int i=0;i<Registro.size();i++){
			List laux = (List)Registro.get(i);		
			
			String cesionario     =((laux.get(0))==null)?"":(laux.get(0).toString());
			String nompyme     =((laux.get(1))==null)?"":(laux.get(1).toString());
			 nombrePymeCedente = ((laux.get(1))==null)?"":(laux.get(1).toString());
			String rfc =  ((laux.get(2))==null)?"":(laux.get(2).toString());
			String Proveedor =  ((laux.get(3))==null)?"":(laux.get(3).toString());
			String FSolicitud =  ((laux.get(4))==null)?"":(laux.get(4).toString());
			String contrato       = ((laux.get(5))==null)?"":(laux.get(5).toString());
			String contratacion   = ((laux.get(8))==null)?"":(laux.get(8).toString());
			String fechaCont_Ini  = ((laux.get(9))==null)?"":(laux.get(9).toString());
			String fechaCont_Fin  = ((laux.get(10))==null)?"":(laux.get(10).toString());
			String clasificionEpo = ((laux.get(11))==null)?"":(laux.get(11).toString());
			String campo1 			= ((laux.get(12))==null)?"":(laux.get(12).toString());
			String campo2         = ((laux.get(13))==null)?"":(laux.get(13).toString());
			String campo3         = ((laux.get(14))==null)?"":(laux.get(14).toString());
			String campo4         = ((laux.get(15))==null)?"":(laux.get(15).toString());
			String campo5         = ((laux.get(16))==null)?"":(laux.get(16).toString());
			String objeto         = ((laux.get(17))==null)?"":(laux.get(17).toString());
			String fechaLimite    = ((laux.get(18))==null)?"":(laux.get(18).toString());			
			String estatus2      = ((laux.get(19))==null)?"":(laux.get(19).toString());
			String solicitud      = ((laux.get(20))==null)?"":(laux.get(20).toString());
			String fechaAceptacion      = ((laux.get(23))==null)?"":(laux.get(23).toString());
			String noestatus =  ((laux.get(21))==null)?"":(laux.get(21).toString());
			String comentarios  = ((laux.get(24))==null)?"":(laux.get(24).toString());
			String plazoContrato2 = ((laux.get(25))==null)?"":(laux.get(25).toString());
			String venanillaPago = ((laux.get(26))==null)?"":(laux.get(26).toString());
			String supAdmResob = ((laux.get(27))==null)?"":(laux.get(27).toString());
			String numeroTelefono = ((laux.get(28))==null)?"":(laux.get(28).toString());
			String fechaSolicitud = ((laux.get(29))==null)?"":(laux.get(29).toString());
			String nombreEpo = ((laux.get(30))==null)?"":(laux.get(30).toString());
			String firmaContrato = ((laux.get(31))==null)?"":(laux.get(31).toString());
			
			String empresas = ((laux.get(32))==null)?"":(laux.get(32).toString());
			String claveGrupo = ((laux.get(33))==null)?"":(laux.get(33).toString());
			
			StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(solicitud);
			String estatusAcuse ="", estatuscodigo ="";  
			acuse	= ((laux.get(22))==null)?"":(laux.get(22).toString());
			//Se obtiene el representante Legal de la Pyme
			StringBuffer nombreContacto = new StringBuffer();
            UtilUsr utilUsr = new UtilUsr();
			boolean usuarioEncontrado = false;
			boolean perfilIndicado = false;
            
            List lEmpresasGrupo =  cesionBean.getEmpresasXGrupoCder(claveGrupo);
            //System.out.println("lEmpresasGrupo:"+lEmpresasGrupo);
            for(int j=0;j<lEmpresasGrupo.size();j++) {
                HashMap hmEmpresa = (HashMap)lEmpresasGrupo.get(j);
                String str_representantes = (String)hmEmpresa.get("representantes");
                str_representantes = str_representantes.replace(", ", ", <br>");
                if (j > 0) {
                    nombreContacto.append(" / <br>");
                }
                nombreContacto.append(str_representantes);
            }
            
            /*
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(Proveedor, "P");			
			for (int y = 0; y < usuariosPorPerfil.size(); y++) {
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);					
				Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
				perfilIndicado = true;
				if(i>0){
					nombreContacto.append(" / ");
				}
				nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno());
				nombreContacto.append("\n");
			}
            */
			datos = new HashMap();
			datos.put("CECIONARIO", cesionario);
			
			//FODEA-024-2014 MOD()
			cedenteLista = nompyme+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):"");//-------PARA EL GRID
			//
			datos.put("PYME", nompyme+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):""));
			datos.put("RFC", rfc);
			datos.put("CLAVE_PYME", Proveedor);			
			datos.put("FSOLICITUDINI", FSolicitud);
			datos.put("NOCONTRATO", contrato);
			datos.put("MONTO_MONEDA", montosPorMoneda.toString());
			datos.put("CONTRATACION", contratacion);
			datos.put("FECHA_INICIO_CONTRATO", fechaCont_Ini);
			datos.put("FECHA_FIN_CONTRATO", fechaCont_Fin);
			datos.put("PLAZO_CONTRATO", plazoContrato2);
			datos.put("CLASIFICACION", clasificionEpo);
			datos.put("CAMPO1", campo1);
			datos.put("CAMPO2", campo2);
			datos.put("CAMPO3", campo3);
			datos.put("CAMPO4", campo4);
			datos.put("CAMPO5", campo5);
			datos.put("VENANILLA_PAGO", venanillaPago);
			datos.put("SUP_ADM_RESOB", supAdmResob);
			datos.put("NUMERO_TELEFONO", numeroTelefono);		
			datos.put("OBCONTRATO", objeto);		
			datos.put("FACEPTACION", fechaAceptacion);
			datos.put("ESTATUS", estatus2);
			datos.put("REPRESENTANTE", nombreContacto.toString());
			datos.put("COMENTARIOS", comentarios);
			datos.put("CLAVE_SOLICITUD", clave_solicitud);
			datos.put("TIPOCONTRATACION", tipoContratacion );
			datos.put("NOESTATUS", noestatus);
			datos.put("CAUSA_RECHAZO", "");		
			datos.put("ESTATUS_ACUSE", estatusAcuse);
			datos.put("FECHASOLICITUD",fechaSolicitud);
			datos.put("NOMBREEPO",nombreEpo);
			datos.put("FIRMA_CONTRATO",firmaContrato);
			registros.add(datos);		
		}
	
		String NombreClasificacionEpo ="";
		if(!clasificacionEpo.equals("")) {
			NombreClasificacionEpo = clasificacionEpo;
		}else {
			NombreClasificacionEpo = "Ciudad de Firma del Contrato";
		}

		String nombreUsuario =iNoUsuario+"  "+strNombreUsuario;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:SS a");	//HH es formato 24 horas.
		String hora = sdf.format(new java.util.Date());
		String fecha	= new SimpleDateFormat("dd/mm/yyyy").format(new java.util.Date());
		
		String banderaConsentimiento=cesionBean.getBanderaConsentimiento(clave_solicitud);
		String archivoConsentimiento=null;
		if(banderaConsentimiento==null||!banderaConsentimiento.equals("S")){
			archivoConsentimiento=cesionBean.obtenerConsentimiento(clave_solicitud,strNombreUsuario,strDirectorioTemp);
		}else{
			archivoConsentimiento=strDirecVirtualTemp+cesionBean.descargaConsentimiento(clave_solicitud,strDirectorioTemp);
		}
		
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta);	
		resultado.put("clasificacionEpo", NombreClasificacionEpo);	
		resultado.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
		resultado.put("archivoConsentimiento",archivoConsentimiento);
		resultado.put("campo01",campo01);
		resultado.put("campo02",campo02);
		resultado.put("campo03",campo03);
		resultado.put("campo04",campo04);
		resultado.put("campo05",campo05);	
		resultado.put("nombreUsuario",nombreUsuario);
		resultado.put("hora",hora);
		resultado.put("fecha",fecha);
		resultado.put("acuse",acuse);
		resultado.put("nombrePymeCedente",nombrePymeCedente);	
		resultado.put("strNombreUsuario",strNombreUsuario);	
		infoRegresar = resultado.toString();
		
	} else if (informacion.equals("ArchivoAcuse")) {	
		String banderaRechazo  = (request.getParameter("banderaRechazo")!=null)?request.getParameter("banderaRechazo"):"";

		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = archivo.nombreArchivo()+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String usuario = iNoUsuario;
		String nombreUsuario =strNombreUsuario;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:SS a");	//HH es formato 24 horas.
		String hora = sdf.format(new java.util.Date());
		String dia	= new SimpleDateFormat("dd/mm/yyyy").format(new java.util.Date());
		String UsuarioNom = usuario +" "+nombreUsuario;

		String NombreClasificacionEpo ="";
		if(!clasificacionEpo.equals("")) {
			NombreClasificacionEpo = clasificacionEpo;
		}else {
			NombreClasificacionEpo = "Ciudad de Firma del Contrato";
		}
		
		pdfDoc.encabezadoConImagenesPersonalizado(pdfDoc,(String)session.getAttribute("strPais"),
															((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
															(String)session.getAttribute("sesExterno"),								 
															(String) session.getAttribute("strNombre"),
															(String) session.getAttribute("strNombreUsuario"),
														"00archivos/15cadenas/15archcadenas/logos/" + "nafin.gif","/00utils/gif/ba_cadpro.jpg",strDirectorioPublicacion);
		pdfDoc.addText(" ");
		pdfDoc.addText(" ");
		
		int contador = 21 + lovDatos.size();
		pdfDoc.setTable(contador,100);
		pdfDoc.setCell("Intermediario Financiero (Cesionario)","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Pyme(Cedente)","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("RFC","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("No Proveedor ","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Representante legal","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Fecha de Solicitud Pyme","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("No Contrato","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Firma Contrato","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Monto / Moneda","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Tipo de Contratacion","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("F. Inicio Contrato","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("F. Final Contrato","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Plazo del Contrato","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell(NombreClasificacionEpo,"celda01rep",ComunesPDF.CENTER,1,1,1);
			
		for (int i=0; i<lovDatos.size(); i++) { 
			Vector lovRegistro = (Vector) lovDatos.get(i);
			String lsNombreCampo 	= (String) lovRegistro.get(1);
			pdfDoc.setCell(lsNombreCampo,"celda01rep",ComunesPDF.CENTER,1,1,1);				
		}
		pdfDoc.setCell("Ventanilla de Pago","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Supervisor/Administrador/Residente de Obra","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Teléfono","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Objeto de Contrato","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Comentarios","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Fecha de Aceptacion o Rechazo","celda01rep",ComunesPDF.CENTER,1,1,1);
		pdfDoc.setCell("Estatus ","celda01rep",ComunesPDF.CENTER,1,1,1);
		
		List registro = cesionBean.inicializarPreAcuseActivaReactiva(clave_solicitud);	
		
		for(int i=0;i<registro.size();i++){
			List laux = (List)registro.get(i);
			
			String cesionario     =((laux.get(0))==null)?"":(laux.get(0).toString());
			String nompyme     =((laux.get(1))==null)?"":(laux.get(1).toString());
			String nombrePymeCedente = ((laux.get(1))==null)?"":(laux.get(1).toString());
			String rfc =  ((laux.get(2))==null)?"":(laux.get(2).toString());
			String Proveedor =  ((laux.get(3))==null)?"":(laux.get(3).toString());
			String FSolicitud =  ((laux.get(4))==null)?"":(laux.get(4).toString());
			String contrato       = ((laux.get(5))==null)?"":(laux.get(5).toString());
			String contratacion   = ((laux.get(8))==null)?"":(laux.get(8).toString());
			String fechaCont_Ini  = ((laux.get(9))==null)?"":(laux.get(9).toString());
			String fechaCont_Fin  = ((laux.get(10))==null)?"":(laux.get(10).toString());
			String clasificionEpo = ((laux.get(11))==null)?"":(laux.get(11).toString());
			String campo1 			= ((laux.get(12))==null)?"":(laux.get(12).toString());
			String campo2         = ((laux.get(13))==null)?"":(laux.get(13).toString());
			String campo3         = ((laux.get(14))==null)?"":(laux.get(14).toString());
			String campo4         = ((laux.get(15))==null)?"":(laux.get(15).toString());
			String campo5         = ((laux.get(16))==null)?"":(laux.get(16).toString());
			String objeto         = ((laux.get(17))==null)?"":(laux.get(17).toString());
			String fechaLimite    = ((laux.get(18))==null)?"":(laux.get(18).toString());			
			String estatus2        = ((laux.get(19))==null)?"":(laux.get(19).toString());
			String solicitud      = ((laux.get(20))==null)?"":(laux.get(20).toString());
			String Acuse				= ((laux.get(22))==null)?"":(laux.get(22).toString());						
			String fechaAceptacion      = ((laux.get(23))==null)?"":(laux.get(23).toString());
			String comentarios      = ((laux.get(24))==null)?"":(laux.get(24).toString());
			String plazoContrato2 = ((laux.get(25))==null)?"":(laux.get(25).toString());
			String venanillaPago = ((laux.get(26))==null)?"":(laux.get(26).toString());
			String supAdmResob = ((laux.get(27))==null)?"":(laux.get(27).toString());
			String numeroTelefono = ((laux.get(28))==null)?"":(laux.get(28).toString());
			String fechaSolicitud = ((laux.get(29))==null)?"":(laux.get(29).toString());
			String nombreEpo = ((laux.get(30))==null)?"":(laux.get(30).toString());
			String firmaContrato= ((laux.get(31))==null)?"":(laux.get(31).toString());
			
			String empresas= ((laux.get(32))==null)?"":(laux.get(32).toString());
            String claveGrupo = ((laux.get(33))==null)?"":(laux.get(33).toString());

			StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(clave_solicitud);

            //Se obtiene el representante Legal de la Pyme
            StringBuffer nombreContacto = new StringBuffer();
            UtilUsr utilUsr = new UtilUsr();
            boolean usuarioEncontrado = false;
            boolean perfilIndicado = false;
            
            List lEmpresasGrupo =  cesionBean.getEmpresasXGrupoCder(claveGrupo);
            //System.out.println("lEmpresasGrupo:"+lEmpresasGrupo);
            for(int j=0;j<lEmpresasGrupo.size();j++) {
                HashMap hmEmpresa = (HashMap)lEmpresasGrupo.get(j);
                String str_representantes = (String)hmEmpresa.get("representantes");
                str_representantes = str_representantes.replace(",", "; ");
                if (j > 0) {
                    nombreContacto.append(" / ");
                }
                nombreContacto.append(str_representantes);
            }
            /*
            List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(Proveedor, "P");			
			for (int y = 0; y < usuariosPorPerfil.size(); y++) {
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);					
				Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
				perfilIndicado = true;
				nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+"  ");						
				nombreContacto.append("\n");
			}
            */
		
			pdfDoc.setCell(" "+cesionario,"formasrep",ComunesPDF.CENTER,1,1,1);
			//FODEA-024-2014 MOD()
			String []nomPyneAux=nompyme.split(";");
			if(nomPyneAux.length==1){
			}else{
			}
			//
			pdfDoc.setCell(" "+nompyme+((empresas!="")?"\n"+empresas.replaceAll(";","\n"):""),"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+rfc,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+Proveedor,"formasrep",ComunesPDF.CENTER,1,1,1);	
			pdfDoc.setCell(" "+nombreContacto.toString(),"formasrep",ComunesPDF.CENTER,1,1,1);			
			pdfDoc.setCell(" "+FSolicitud,"formasrep",ComunesPDF.CENTER,1,1,1);	
			pdfDoc.setCell(" "+contrato,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+firmaContrato,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+montosPorMoneda.toString().replaceAll("<br/>", "\n"),"formasrep",ComunesPDF.LEFT,1,1,1);
			pdfDoc.setCell(" "+contratacion,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+fechaCont_Ini,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+fechaCont_Fin,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+plazoContrato2,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+clasificionEpo,"formasrep",ComunesPDF.CENTER,1,1,1);		
			
			for (int v=0; v<lovDatos.size(); v++) { 	
				Vector lovRegistro = (Vector) lovDatos.get(v);
				int liNumCampo 	= Integer.parseInt((String)lovRegistro.get(0));		
				if (liNumCampo== 1) {		
					pdfDoc.setCell(" "+campo1,"formasrep",ComunesPDF.CENTER,1,1,1);
				} if (liNumCampo== 2) {
					pdfDoc.setCell(" "+campo2,"formasrep",ComunesPDF.CENTER,1,1,1);
				} if (liNumCampo== 3) {
					pdfDoc.setCell(" "+campo3,"formasrep",ComunesPDF.CENTER,1,1,1);
				} if (liNumCampo== 4) {
					pdfDoc.setCell(" "+campo4,"formasrep",ComunesPDF.CENTER,1,1,1);
				} if (liNumCampo== 5) {
					pdfDoc.setCell(" "+campo5,"formasrep",ComunesPDF.CENTER,1,1,1);
				}
			}
			pdfDoc.setCell(" "+venanillaPago,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+supAdmResob,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+numeroTelefono,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+objeto,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(" "+comentarios,"formasrep",ComunesPDF.CENTER,1,1,1);		
			pdfDoc.setCell(" "+fechaAceptacion,"formasrep",ComunesPDF.CENTER,1,1,1);		
			pdfDoc.setCell(" "+estatus2,"formasrep",ComunesPDF.CENTER,1,1,1);	
			pdfDoc.addTable();
			
			pdfDoc.addText(" ");
				
			pdfDoc.setTable(2,35);
			pdfDoc.setCell("Cifras de Control","celda01rep",ComunesPDF.CENTER,2);
			pdfDoc.setCell("Numero de Acuse","celda01rep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(Acuse,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell("Fecha de Carga","celda01rep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(dia,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell("Hora de Carga","celda01rep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(hora,"formasrep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell("Usuario de Captura","celda01rep",ComunesPDF.CENTER,1,1,1);
			pdfDoc.setCell(UsuarioNom,"formasrep",ComunesPDF.CENTER,1,1,1);		
		  pdfDoc.addTable();

			pdfDoc.addText(" ");
			
			
			/*if(!banderaRechazo.equals("true")){//Si es rechazo no muestra el consentimiento
			pdfDoc.newPage();			
			
				pdfDoc.setTable(1,70);
				pdfDoc.setCell(nombrePymeCedente,"formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Presente","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Consentimiento Condicionado para Ceder los Derechos de Cobro respecto del Contrato No. "+ contrato,"subrayado",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(nombrePymeCedente,"formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Objeto: "+objeto,"formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Monto: "+montosPorMoneda.toString().replaceAll("<br/>", "|"),"formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Plazo: "+plazoContrato2,"formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Fecha de Solicitud: "+fechaSolicitud,"formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				
			  pdfDoc.setCell(" En atención a su solicitud, "+nombreEpo+" (EPO), le informa que con base en la información recabada al interior del organismo, se otorga el consentimiento que solicita, con fundamento en:  El Artículo 46 de la Ley de Adquisiciones, Arrendamientos y Servicios del Sector Público.  El Artículo 47 de la Ley de Obras Públicas y Servicios Relacionados con las Mismas.  Los Artículos 53, fracción XVII,de la Ley de Petróleos Mexicanos, 48 de su Reglamento, y 54 de las Disposiciones administrativas de contratación en materia de adquisiciones, arrendamientos, obras y servicios de las actividades sustantivas de carácter productivo de Petróleos Mexicanos y Organismos Subsidiarios , según corresponda, así como en la cláusula denominada Cesión del Contrato. ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("No obstante lo anterior, el Consentimiento estará Condicionado a que: ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("1.- "+nombrePymeCedente+", en lo sucesivo la CEDENTE, deberá celebrar con el "+ cesionario+" un Contrato de Cesión Electrónica de Derechos de Cobro, mismo que deberá ser suscrito por el o los representantes legales o apoderados de la CEDENTE que cuente con facultades generales o especiales para actos de dominio y que dicho instrumento contractual sea notificado a la Entidad a través de este sistema NAFIN para que surta efectos legales, en términos de lo establecido en el artículo 390 del Código de Comercio, en el entendido que de no efectuarse la mencionada notificación dentro de los 15 (quince) días hábiles siguientes a la fecha en que sea notificada en el sistema NAFIN el presente Consentimiento Condicionado, NAFIN deberá reactivar el trámite por otro periodo igual para proseguir con el trámite correspondiente, sin ninguna responsabilidad para la Entidad.","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("2.- La Cesión de Derechos de Cobro incluirá los incrementos al monto del contrato arriba señalado que, en su caso, se pacten con posterioridad mediante convenios modificatorios; y que la Cesión de Derechos de Cobro única y exclusivamente procederá con respecto a facturas pendientes de cobro por trabajos ejecutados/servicios prestados/bienes entregados/ejecutados en términos de lo pactado en el contrato y que hayan sido instruidos por la Entidad.","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("3.- La Cesión de Derechos de Cobro será por el 100% (cien por ciento) de los que se deriven por el cumplimiento en términos del mencionado contrato, a partir de la fecha en que surta efectos la notificación del correspondiente Contrato de Cesión Electrónica de Derechos de Cobro.  ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("4.-El INTERMEDIARIO FINANCIERO acepte y esté de acuerdo en que el pago de los derechos de cobro estará sujeto a que, en su caso, la Entidad realice las deducciones y/o retenciones que deban hacerse a la CEDENTE por adeudos con el organismo; penas convencionales; pagos en exceso; cualquier requerimiento de pago derivado de laudo firme ordenado por las Juntas Locales o Federales de Conciliación y Arbitraje, con motivo de los juicios laborales instaurados en contra de Petróleos Mexicanos y sus Organismos Subsidiarios, en relación con la CEDENTE; o cualquier otra deducción derivada del contrato mencionado o de la Ley de la materia en relación con el  contrato celebrado  con la Entidad y que es objeto de la presente cesión de derechos de cobro, en cuyos supuestos se harán las deducciones y/o retenciones correspondientes. En tal virtud, la Entidad quedará liberada de cualquier responsabilidad por las deducciones y/o retenciones a que haya lugar.  ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("5.- El Consentimiento Condicionado otorgado por la Entidad, no constituye una garantía de pago ni reconocimiento de cualquier derecho del INTERMEDIARIO FINANCIERO frente a la CEDENTE, siendo la única obligación de la Entidad, la de pagar cantidades líquidas que sean exigibles por el cumplimiento del citado contrato una vez hechas las deducciones y/o retenciones antes mencionadas. ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("6.- Se deberán proporcionar los datos de la cuenta ÚNICA bancaria del INTERMEDIARIO FINANCIERO a la cual se realizarán los pagos. ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("7.- A la notificación que se practique a la Entidad mediante este sistema NAFIN, NAFIN deberá subir al mismo: i) un ejemplar del Contrato de Cesión Electrónica de Derechos de Cobro con las firmas electrónicas de los representantes de la CEDENTE y del INTERMEDIARIO FINANCIERO; ii) copia certificada por fedatario público de la documentación con la que se acredite la personalidad del o de los representantes legales o apoderados de la CEDENTE con las facultades generales o especiales para actos de dominio, y iii) copia certificada por fedatario público de la documentación con la que se acredite la personalidad del o de los representantes legales o apoderados del INTERMEDIARIO FINANCIERO con las facultades suficientes para la celebración del Contrato de Cesión Electrónica de Derechos de Cobro en cuestión.  ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("8.- El presente Consentimiento Condicionado, quedará sin efecto en caso de que la Entidad antes de la fecha de notificación a que se refiere el punto número “1” del presente Consentimiento Condicionado, reciba alguna notificación de autoridad judicial o administrativa que incida sobre el contrato cuyos derechos de cobro serán materia de cesión. Bajo este supuesto, la Entidad lo informará inmediatamente mediante este sistema NAFIN a la CEDENTE y al INTERMEDIARIO FINANCIERO, una vez que haya recibido formalmente la notificación de que se trate ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("9.- El Contrato de Cesión Electrónica de Derechos de Cobro quedará sin efectos si y sólo si la CEDENTE y el INTERMEDIARIO FINANCIERO celebran un Convenio de Extinción de la Cesión Electrónica de Derechos de Cobro, en la inteligencia de que sus representantes legales o apoderados deberán contar con las facultades suficientes para ello, y en su oportunidad notificarlo a la Entidad en los mismos términos previstos para la notificación del Contrato de Cesión Electrónica de Derechos de Cobro para que surta los efectos legales que le correspondan. ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","formasrep",ComunesPDF.JUSTIFIED,1,1,0);				
				pdfDoc.setCell(strNombreUsuario,"formasrep",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.addTable();
			}*/
		} //termina For 
		
		pdfDoc.endDocument();
		
		if(!banderaRechazo.equals("true")){
			String ruta=cesionBean.descargaConsentimiento(clave_solicitud,strDirectorioTemp);
			String nombreArchivoNevo = archivo.nombreArchivo()+".pdf";
			String args[] =  {strDirectorioTemp+nombreArchivo,strDirectorioTemp+ruta,strDirectorioTemp+nombreArchivoNevo};
			nombreArchivo=nombreArchivoNevo;
			ComunesPDF.doMerge(args);
		}
		
		JSONObject jsonObj = new JSONObject();		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
		infoRegresar = jsonObj.toString();

	} else if (informacion.equals("EnvioCorreo")) {	
		
	
		HashMap archivoAdjunto 		 = new HashMap();
		ArrayList 		listaDeArchivos		 = new ArrayList();
		ArrayList 		listaDeImagenes	= new ArrayList();
		HashMap 			imagenNafin 		= new HashMap();
		List Registro = new ArrayList();
		String seEnvioCorreo ="", cesionario =" ", contrato =" ", monto =" ", moneda =" ", fechaLimite = "",
				 contratacion =" ", fechaCont_Ini =" ", fechaCont_Fin =" ", clasificionEpo =" ",
				 campo1 =" ", campo2 =" ", campo3 =" ", campo4 =" ", campo5 =" ", objeto = "", noestatus = "",
				 estatus2=" ", fechaAceptacion = "", FSolicitud = "", Proveedor ="", nompyme="", rfc = "", 
				 estatuscodigo = "", estatusAcuse = "",  plazoContrato2 = "", venanillaPago = "", 
				 supAdmResob = "", numeroTelefono = "", solicitud ="",firmaContrato="", empresas = "";
				 
		String directorioTemporal = strDirectorioTemp;
		String asunto  = (request.getParameter("asunto")!=null)?request.getParameter("asunto"):"";	
		String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
		String logo = (request.getParameter("logo")!=null)?request.getParameter("logo"):"";
		String archivo = (request.getParameter("archivo")!=null)?request.getParameter("archivo"):"";
		String destinatarios = (request.getParameter("destinatarios")!=null)?request.getParameter("destinatarios"):"";
		String remitente 		= (String) session.getAttribute("strCorreoUsuario");
		String ccDestinatarios =""; 											 
						
		Registro= cesionBean.inicializarAvisoNotificacionCesion(clave_solicitud);
		List laux = null;
		StringBuffer montosPorMoneda = new StringBuffer();
		for(int i=0;i<Registro.size();i++){
			laux = (List)Registro.get(i);		
			cesionario     =((laux.get(0))==null)?"":(laux.get(0).toString());
			nompyme     =((laux.get(1))==null)?"":(laux.get(1).toString());
			rfc =  ((laux.get(2))==null)?"":(laux.get(2).toString());
			Proveedor =  ((laux.get(3))==null)?"":(laux.get(3).toString());
			FSolicitud =  ((laux.get(4))==null)?"":(laux.get(4).toString());
			contrato       = ((laux.get(5))==null)?"":(laux.get(5).toString());
			monto          =  ((laux.get(6))==null)?"":(laux.get(6).toString());
			moneda         = ((laux.get(7))==null)?"":(laux.get(7).toString());
			contratacion   = ((laux.get(8))==null)?"":(laux.get(8).toString());
			fechaCont_Ini  = ((laux.get(9))==null)?"":(laux.get(9).toString());
			fechaCont_Fin  = ((laux.get(10))==null)?"":(laux.get(10).toString());
			clasificionEpo = ((laux.get(11))==null)?"":(laux.get(11).toString());
			campo1 			= ((laux.get(12))==null)?"":(laux.get(12).toString());
			campo2         = ((laux.get(13))==null)?"":(laux.get(13).toString());
			campo3         = ((laux.get(14))==null)?"":(laux.get(14).toString());
			campo4         = ((laux.get(15))==null)?"":(laux.get(15).toString());
			campo5         = ((laux.get(16))==null)?"":(laux.get(16).toString());
			objeto         = ((laux.get(17))==null)?"":(laux.get(17).toString());
			fechaLimite    = ((laux.get(18))==null)?"":(laux.get(18).toString());			
			estatus2      = ((laux.get(19))==null)?"":(laux.get(19).toString());
			solicitud      = ((laux.get(20))==null)?"":(laux.get(20).toString());
			fechaAceptacion      = ((laux.get(23))==null)?"":(laux.get(23).toString());
			noestatus =  ((laux.get(21))==null)?"":(laux.get(21).toString());
			plazoContrato2 = ((laux.get(25))==null)?"":(laux.get(25).toString());
			venanillaPago = ((laux.get(26))==null)?"":(laux.get(26).toString());
			supAdmResob = ((laux.get(27))==null)?"":(laux.get(27).toString());
			numeroTelefono = ((laux.get(28))==null)?"":(laux.get(28).toString());
			firmaContrato = ((laux.get(29))==null)?"":(laux.get(29).toString());
			
			empresas = ((laux.get(30))==null)?"":(laux.get(30).toString());
			
			montosPorMoneda = cesionBean.getMontoMoneda(solicitud);
			
		}
		//para mostrar los campos adicionales
		Vector lovRegistro 	=  new Vector();
		String lsNombreCampo = "";
		int liNumCampo 	= 0;
		String campos = "5";
		
		lovDatos = cesionBean.getCamposAdicionales(clave_epo,campos); 
		
		String NombreClasificacionEpo ="";
		if(!clasificacionEpo.equals("")) {
			NombreClasificacionEpo = clasificacionEpo;
		}else {
			NombreClasificacionEpo = "Clasificación EPO";
		}					
		
		String mensajeCorreo 	= 	"<html>		 		"  +
              			"       <head>													"  +
										"               <title>"+asunto+"</title>	"  +
										"       </head>												"  +
										"       <body>													"	+
										" <table border=\"0\" align=\"center\" height=\"400\" width=\"200\">"+
										"	<tr>"+
										" 		<td>"+
										" 			<img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\">"+
										" 		<td>"+
										" 	</tr>"+
										"</table>"+
										"<br>"+	
										
									"<table border=\"0\" align=\"center\" >"+
									"<tr>"+
									" 	<td align=\"center\" style=\"text-align:justify\"  >"+mensaje+"<td>"+
									"</tr>	"+
									"</table>"+
									"<br>"+
									"<br>"+
										"<table border=\"1\" align=\"center\" >"+
										"	<tr> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\" >Intermediario Financiero (Cesionario)</td> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\">Pyme (Cedente) </td> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\" >RFC </td> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\">No Proveedor</td> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\">Fecha de solcitud Pyme </td>"+
										"	<td bgcolor=\"#cccccc\" align=\"center\">No. Contrato</td>	"+
										"	<td bgcolor=\"#cccccc\" align=\"center\">Firma Contrato</td> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\">Monto / Moneda</td> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\">Tipo de Contratacion</td>"+
										"	<td bgcolor=\"#cccccc\" align=\"center\">F. Inicio Contrato</td> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\">F. Final Contrato</td> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\">Plazo del <br/>Contrato</td> "+
										"	<td bgcolor=\"#cccccc\" align=\"center\" >"+NombreClasificacionEpo+"</td>";
		for (int i=0; i<lovDatos.size(); i++) {  
			lovRegistro = (Vector) lovDatos.get(i);	 
			lsNombreCampo 	= (String) lovRegistro.get(1);
			mensajeCorreo += "	<td bgcolor=\"#cccccc\">"+lsNombreCampo+"</td>";
		}
		mensajeCorreo += " <td bgcolor=\"#cccccc\" align=\"center\">Ventanilla de Pago</td>";
		mensajeCorreo += " <td bgcolor=\"#cccccc\" align=\"center\">Supervisor/<br/>Administrador/<br/>Residente de Obra</td>";
		mensajeCorreo += " <td bgcolor=\"#cccccc\" align=\"center\">Teléfono</td>";
		mensajeCorreo += " <td bgcolor=\"#cccccc\" align=\"center\">Objeto del Contrato</td>"+
										" <td bgcolor=\"#cccccc\" align=\"center\">Fecha de Aceptacion o Rechazo</td>"+
										" <td bgcolor=\"#cccccc\" align=\"center\">Estatus Actual</td>";
		mensajeCorreo += 	" 	</tr>"+
										" 	<tr>"+										
										"	<td align=\"center\">"+cesionario+"</td>"+
										" <td align=\"center\">"+nompyme+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):"")+"</td>"+ //FODEA-024-2014 MOD()
										" <td align=\"center\">"+rfc+"</td>"+
										" <td align=\"center\">"+Proveedor+"</td>"+
										" <td align=\"center\">"+FSolicitud+"</td>"+
										" <td align=\"center\">"+contrato+"</td>"+
										" <td align=\"center\">"+firmaContrato+"</td>"+
										" <td align=\"center\">"+montosPorMoneda+"</td>"+
										" <td align=\"center\">"+contratacion+"</td>"+
										" <td align=\"center\">"+fechaCont_Ini+"</td>"+
										" <td align=\"center\">"+fechaCont_Fin+"</td>"+
										" <td align=\"center\">"+plazoContrato2+"</td>"+
										" <td align=\"center\">"+clasificionEpo+"</td>";
										
		for (int n=0; n<lovDatos.size(); n++) {  
			lovRegistro = (Vector) lovDatos.get(n);
			liNumCampo 	= Integer.parseInt((String)lovRegistro.get(0));
			if (liNumCampo== 1) { 
				mensajeCorreo += "<td align=\"center\">"+campo1+"</td>";
			} if (liNumCampo== 2) { 
				mensajeCorreo += 	"<td align=\"center\">"+campo2+"</td>";
			} if (liNumCampo== 3) { 
				mensajeCorreo += "<td align=\"center\">"+campo3+"</td>";
			} if (liNumCampo== 4) { 
				mensajeCorreo +=	"<td align=\"center\">"+campo4+"</td>";
			} if (liNumCampo== 5) {
				mensajeCorreo +="<td align=\"center\">"+campo5+"</td>";
			} 
		}// temrina for				
		
		mensajeCorreo +=	"<td align=\"center\">"+venanillaPago+"</td>";
		mensajeCorreo +=	"<td align=\"center\">"+supAdmResob+"</td>";
		mensajeCorreo +=	"<td align=\"center\">"+numeroTelefono+"</td>";
		mensajeCorreo +=	"<td align=\"center\">"+objeto+"</td>"+
										"<td align=\"center\">"+fechaAceptacion+"</td>"+
										"<td align=\"center\">"+estatus2+"</td>"+
										"</tr>"+
										"</table>"+
										"<br>"+		
										"<table border=\"0\" align=\"center\" height=\"400\" width=\"400\">"+
										" 	<tr>"+
										
										" 	</tr>"+
										"</table>"+
										"	</body>" 	+
										"</html>	";
										
		imagenNafin.put("FILE_FULL_PATH",  logo);
		imagenNafin.put("FILE_ID","nafin_id");
		listaDeImagenes.add(imagenNafin);
	
		archivoAdjunto.put("FILE_FULL_PATH", archivo );
		listaDeArchivos.add(archivoAdjunto);			
						
		Correo correo = new Correo();
        correo.setCodificacion("charset=UTF-8");
		correo.enviaCorreoConDatosAdjuntos(remitente.toUpperCase(),// Debug info: borrar esta linea y descomentar la siguiente
														  destinatarios,
														  ccDestinatarios,
														  asunto,
														  mensajeCorreo,
														  listaDeImagenes,
														  listaDeArchivos);
		seEnvioCorreo = "S";
 		// Si la solicitud se encuentra en los estatus: 2 (En Proceso) o 6 (Solicitud Reactiva)
		// Registrar en el campo CDER_SOLICITUD.CS_NOTIF_PROVEEDOR que el correo se envio exitosamente.
		if("2".equals(noestatus) || "6".equals(noestatus)){
			cesionBean.setProveedorNotificado(clave_solicitud); // F041 - 2010 By JSHD
		}
			String mensaje2 = "";
			if(seEnvioCorreo.equals("S")) {
				mensaje2 = "El correo se envio con Exito";
			}else {
				mensaje2= 	"El correo NO fue enviado";
			}

			String	respuestaCorreo = "		{ "+
					"	xtype: 'label',"+
					"	name: 'archivo',"+
					"	id: 'archivo1',"+
					"	html: '"+mensaje2+"',"+									
					" cls:		'x-form-item',  "+
					"	style: { "+
					"	width:   		'100%', "+
					"textAlign: 		'center'"+
					" } "+
					" }	";
					
		JSONObject jsonObj = new JSONObject();		
		jsonObj.put("seEnvioCorreo", seEnvioCorreo);
		jsonObj.put("respuestaCorreo", respuestaCorreo);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();

	}else if (informacion.equals("poderesIF")) {		
	
		JSONObject jsonObj = new JSONObject();		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo","/nafin/00archivos/34cesion/poderesIF.pdf");	
		infoRegresar = jsonObj.toString();
				
	}else if(informacion.equals("autorizarRechazarProrroga") ){

		 clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
		String estatusNuevo = (request.getParameter("estatusNuevo")!=null)?request.getParameter("estatusNuevo"):"";
		cesionBean.aceptarRechazarProrroga(clave_solicitud,estatusNuevo);
		JSONObject 	jsonObj	= new JSONObject();
		jsonObj.put("success", new Boolean(true));
		
		infoRegresar = jsonObj.toString();
	
}

%>
<%=infoRegresar%>
