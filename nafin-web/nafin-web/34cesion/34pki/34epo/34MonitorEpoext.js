Ext.onReady(function(){

	var VisorAutoConsorcio = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var ic_solicitud = registro.get('CLAVE_SOLICITUD');	
		var ic_estatus = registro.get('CLAVE_ESTATUS');	
		var titulo;
		var tipoVisor;		
		if(ic_estatus==24) {   titulo = 'Solicitud de Consentimiento' ; tipoVisor = 'Autorizacion_Consorcio'; } 
		if(ic_estatus==25) {   titulo = 'Firma Consorcio' ;    tipoVisor = 'Contrato_cesion';  } 
					
		var win = new NE.AutoConsorcio.VisorConsorcio(ic_solicitud, tipoVisor, titulo );
		
	}
//-----------------------------------HANDLERS-----------------------------------
	var jsonValoresIniciales = null;
	function procesaValoresIniciales(opts,success,response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarDescargaArchivos(opts, success, response) {
		var ventanaA = Ext.getCmp('ventanaA');
		if(ventanaA){
			ventanaA.el.unmask();
		}
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fpTitulo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpTitulo.getForm().getEl().dom.submit();

		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var mostrarContrato = function(grid,rowIndex,colIndex,item,event){
		var ventanaA = Ext.getCmp('ventanaA');
		if(ventanaA){
			ventanaA.el.mask('Descargando Archivo...','x-mask');
		}
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		document.forms[0].action = "/nafin/34cesion/34pki/34pyme/34CapturaSolConsDescargaAcusePdf.do?tipoArchivo=CONTRATO_PDF&numeroSolicitud="+clave_solicitud;
		document.forms[0].submit();
	}
	
		//Muestra el archivo de la Columna Contrato de Cesion 
	var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '../34pyme/34ContratoCesionPDFCSV.jsp',
			params: Ext.apply({
				informacion: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}
	
	var mostrarConsentimiento = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var claveTipoContratacion = registro.get('CLAVE_CONTRATACION');
		
		var nombre_epo = registro.get('DEPENDENCIA');
		var objeto_contrato = registro.get('OBJETO_CONTRATO');
		var montospormoneda = registro.get('MONTO_MONEDA');
		var plazocontrato = registro.get('PLAZO_CONTRATO');
		var fechasolicitud = registro.get('FECHASOLICITUD');
		var numero_contrato = registro.get('NUMERO_CONTRATO');
		var nombre_if = registro.get('NOMBRE_CESIONARIO');
		
		var ventana = Ext.getCmp('VerConsentimiento');
		if(ventana){
			ventana.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				width: 940,
				height: 550,
				id: 'VerConsentimiento',
				closeAction: 'hide',
				items: [
					panelConsentimiento
				],
				title: 'Consentimiento'
			}).show();
		}
		var bodyPanel = Ext.getCmp('panelConsentimiento').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure',
					function(el, response){
						x.update('');
						NE.util.mostrarErrorResponse(response);
					});
		mgr.update({
			url: '/nafin/34cesion/34pki/34pyme/34ContratoCesionVerConsentimientoext.jsp',
			scripts: false,
			params: {
				clave_solicitud: clave_solicitud,
				clave_contratacion: claveTipoContratacion,
				nombre_epo:nombre_epo,
				objeto_contrato:objeto_contrato,
				montospormoneda:montospormoneda,
				plazocontrato:plazocontrato,
				fechasolicitud:fechasolicitud,
				numero_contrato:numero_contrato,
				nombre_if:nombre_if
			},
			indicadorText: 'Cargando Consentimiento'
		});
	}
	var mostrarContratoCesion = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var numeroSolicitud = registro.get('CLAVE_SOLICITUD');
		var strDirectorioPublicacion = jsonValoresIniciales.strDirectorioPublicacion;
		document.forms[0].action = "/nafin/34cesion/34pki/34if/34ContratoCesionIfAcusePDF.do?tipo_archivo=CONTRATO_CESION_IF&clave_solicitud="+numeroSolicitud;
		document.forms[0].submit();
	}
	var mostrarGridA = function(grid,rowIndex,colIndex,item,event){
		var titulo = "";		
		if(rowIndex!=0&&rowIndex!=7){
			var tipo = "";
			if(rowIndex==1){
				tipo="2";
				titulo="En proceso";
			}else if(rowIndex==3){
				tipo="3";
				titulo="Solicitud Aceptada";
			}else if(rowIndex==4){
				tipo="5";
				titulo = "Solicitud Rechazada";
			}else if(rowIndex==5){
				tipo="8";
				titulo = "Solicitud Vencida";
			}else if(rowIndex==6){
				tipo="24";
				titulo = "Cancelada por Nafin";				
			}else if(rowIndex==8){
				tipo="9";
				titulo = "Notificaci�n Aceptada IF";
			}else if(rowIndex==9){
				tipo="15";
				titulo = "Notificaci�n no aceptada por ventanilla";
			}else if(rowIndex==10){
				tipo="11";
				titulo = "Notificaci�n aceptada";
			}else if(rowIndex==11){
				tipo="16";
				titulo = "Notificado ventanilla";
			}else if(rowIndex==12){
				tipo="12";
				titulo="Notificaci�n Rechazada";
			}else if(rowIndex==13){
				tipo="17";
				titulo="Notificaci�n con redireccionamiento aplicado.";
			}else if(rowIndex==14){
				tipo="25";
				titulo="Rechazada Expirada";
				
			}else if(rowIndex==15){
				tipo="19";
				titulo="Extinci�n Solicitada";
			}else if(rowIndex==16){
				tipo="20";
				titulo="Extinci�n Aceptada IF";
			}else if(rowIndex==17){
				tipo="23";
				titulo="Extinci�n Notificaci�n Aceptada EPO";
			}else if(rowIndex==18){
				tipo="21";
				titulo="Extinci�n Notificado Ventanilla";
			}else if(rowIndex==19){
				tipo="22";
				titulo="Extinci�n con redireccionamiento aplicado";
			}
			consultaDataA.load({
				params:{
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15,
					tipo: tipo
				}
			});
			var ventanaB = Ext.getCmp('ventanaB');
				if(ventanaB){ventanaB.hide();}
			var ventanaA = Ext.getCmp('ventanaA');
				if(ventanaA){
					ventanaA.setTitle(titulo);
					ventanaA.show();
				}else{
					new Ext.Window({
						layout: 'fit',
						modal: true,
						width: 800,
						height: 400,
						id: 'ventanaA',
						closeAction: 'hide',
						items: [
									gridA
						]
					}).show().setTitle(titulo);
				}
		}else if(rowIndex==0){
			location.href="/nafin/34cesion/34pki/34epo/34Consu_Solic_ConsentimientoExt.jsp";
		}else if(rowIndex==7){
			location.href="/nafin/34cesion/34pki/34epo/34Notificacionesext.jsp";
		}
	}
	var mostrarGridB = function(grid, rowIndex, colIndex, item, event){
		var titulo = "";
		if(rowIndex!=0&&rowIndex!=6){
			var tipo = "";
			if(rowIndex==16){
				tipo="19";
				titulo="Extinci�n Solicitada";
			}else if(rowIndex==17){
				tipo="20";
				titulo="Extinci�n Aceptada IF";
			}else if(rowIndex==18){
				tipo="23";
				titulo="Extinci�n notificaci�n aceptada";
			}else if(rowIndex==19){
				tipo="21";
				titulo="Extinci�n notificado ventanilla";
			}else if(rowIndex==20){
				tipo="22";
				titulo="Extinci�n con redireccionamiento aplicado";
			}
			consultaDataB.load({
				params:{
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15,
					tipo: tipo
				}
			});
			var ventanaA = Ext.getCmp('ventanaA');
			if(ventanaA){
				ventanaA.hide();
			}
			var ventanaB = Ext.getCmp('ventanaB');
			if(ventanaB){
				ventanaB.setTitle(titulo);
				ventanaB.show();
			}else{
				new Ext.Window({
					layout: 'fit',
					modal: true,
					width: 800,
					height: 400,
					id: 'ventanaB',
					closeAction: 'hide',
					items: [
								gridB
					],
					title: ''
				}).show().setTitle(titulo);
			}
		}else if(rowIndex==15){
			//location.href="/nafin/34cesion/34pki/34epo/34conExtinContratoExt.jsp";
		}
	}
	var procesarConsultaDataA = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridA = Ext.getCmp('gridA');
			var el = gridA.getGridEl();
			var store = consultaDataA;
			
			var cm = gridA.getColumnModel();
			var jsonData = store.reader.jsonData;
			
			if(store.getTotalCount()>0){
			 
				if(jsonData.tipo=='24') {
					gridA.getColumnModel().setHidden(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'), false);	
					gridA.getColumnModel().setColumnHeader(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'),'Autorizaci�n Consentimiento');
					gridA.getColumnModel().setHidden(cm.findColumnIndex('MOTIVOS_CANCELACION'), false);	
				}else  if(jsonData.tipo=='25') {
					gridA.getColumnModel().setHidden(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'), false);	
					gridA.getColumnModel().setColumnHeader(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'),'Firma Contrato');		
					gridA.getColumnModel().setHidden(cm.findColumnIndex('MOTIVOS_CANCELACION'), true);	
				}else {
					gridA.getColumnModel().setHidden(cm.findColumnIndex('AUTORIZACION_CONSENTIMIENTO'), true);
					gridA.getColumnModel().setHidden(cm.findColumnIndex('MOTIVOS_CANCELACION'), true);	
				}
				
				el.unmask();
			}else {
				el.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
	var procesarConsultaDataB = function(store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridB = Ext.getCmp('gridB');
			var el = gridB.getGridEl();
			if(store.getTotalCount()>0){
				if(jsonValoresIniciales.clasificacionEpo == "")
				{
					gridB.getColumnModel().setHidden(8, true); 
				}else{
					gridB.getColumnModel().setColumnHeader(8,jsonValoresIniciales.clasificacionEpo);
				}
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	var procesarConsultaPrincipalData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var gridSolicitudes = Ext.getCmp('gridSolicitudes');
			var elSol = gridSolicitudes.getGridEl('gridSolicitudes');
			
			var gridNotificaciones = Ext.getCmp('gridNotificaciones');
			var elNot = gridNotificaciones.getGridEl('gridNotificaciones');
			
			var gridExtincionContrato = Ext.getCmp('gridExtincionContrato');
			var elExt = gridExtincionContrato.getGridEl('gridExtincionContrato');
			
			if(store.getTotalCount()>0){
			
				gridSolicitudes.getView().getRow(0).style.backgroundColor="#C0D9D9";
				for(i=7;i<=20;i++){
					gridSolicitudes.getView().getRow(i).style.display = 'none';
				}
				
				for (i=0;i<7;i++){
					gridNotificaciones.getView().getRow(i).style.display = 'none';
				}
				for(i=15;i<=20;i++){
					gridNotificaciones.getView().getRow(i).style.display = 'none';
				}
				gridNotificaciones.getView().getRow(7).style.backgroundColor = "#C0D9D9";
				
				for(i=0;i<=14;i++){
					gridExtincionContrato.getView().getRow(i).style.display = 'none';
				}
				gridExtincionContrato.getView().getRow(15).style.backgroundColor = "#C0D9D9";
			
			
			}else{
				elSol.mask('No se encontro ning�n registro','x-mask');
				elNot.mask('No se encontro ning�n registro','x-mask');
				elExt.mask('No se encontro ning�n registro','x-mask');
			}
		}
	}
//------------------------------------STORES------------------------------------
	var consultaDataA = new Ext.data.JsonStore({
		root: 'registros',
		url: '34MonitorEpoext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'DEPENDENCIA'},
					{name: 'NOMBRE_CESIONARIO'},
					{name: 'NUMERO_CONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'TIPO_CONTRATACION'},
					{name: 'FECHA_INICIO_CONTRATO'},
					{name: 'FECHA_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'OBJETO_CONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'ESTATUS_SOLICITUD'},
					{name: 'CAUSAS_RECHAZO'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'CLAVE_CONTRATACION'},
					{name: 'FECHASOLICITUD'},
					{name: 'FIRMA_CONTRATO'},
					{name: 'MOTIVOS_CANCELACION'},
					{name: 'CLAVE_ESTATUS'}		
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataA,
			exception: {
								fn: function(proxy,type,action,optionRequest,response,args){
									NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
									procesarConsultaDataA(null,null,null);
								}
			}
		}
	});
	var consultaDataB = new Ext.data.JsonStore({
			root: 'registros',
			url: '34MonitorEpoext.data.jsp',
			baseParams: {
				informacion: 'Consulta'
			},
			fields: [
						{name: 'NOMBRE_CESIONARIO'},
						{name: 'NOMBRE_CEDENTE'},
						{name: 'NUMERO_CONTRATO'},
						{name: 'MONTO_MONEDA'},
						{name: 'TIPO_CONTRATACION'},
						{name: 'FECHA_INICIO_CONTRATO'},
						{name: 'FECHA_FIN_CONTRATO'},
						{name: 'PLAZO_CONTRATO'},
						{name: 'CLASIFICACION_EPO'},
						{name: 'OBJETO_CONTRATO'},
						{name: 'CLAVE_SOLICITUD'},
						{name: 'FECHA_EXTINCION'},
						{name: 'NUMERO_CUENTA'},
						{name: 'CUENTA_CLABE'},
						{name: 'BANCO_DEPOSITO'},
						{name: 'CAUSAS_RECHAZO_EXT'},
						{name: 'ESTATUS_SOLICITUD'},
						{name: 'CLASIFICACIONEPO'},
						{name: 'FECHASOLICITUD'},
						{name: 'FIRMA_CONTRATO'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaDataB,
				exception: {
									fn: function(proxy,type,action,optionRequest,response,args){
										NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
										procesarConsultaDataB(null,null,null);
									}
				}
			}
	});
	var consultaPrincipalData = new Ext.data.JsonStore({
			root: 'registros',
			url: '34MonitorEpoext.data.jsp',
			baseParams: {
				informacion: 'ConsultaPrincipal'
			},
			fields: [
				{name: 'Descripcion'},
				{name: 'Total'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaPrincipalData,
				exception: {
								fn: function(proxy,type,action,optionRequest,response,args){
									NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
									procesarConsultaPrincipalData(null,null,null);
								}
				}
			}
	});
//---------------------------------COMPONENTES----------------------------------
	var elementoTitulo = [
									{
										xtype: 'label',
										id: 'lblTitulo',
										style: 'font-weight:bold;color:#006699;text-align:center;display:block;width:400px;height:30px;',
										fieldLabel: '',
										text: 'RESUMEN DE MOVIMIENTOS'
									}
	];
	var gridA = {
			xtype: 'grid',
			store: consultaDataA,
			id: 'gridA',
			columns: [
							{
								header: 'Dependencia',
								tooltip: 'Dependencia',
								dataIndex: 'DEPENDENCIA',
								align: 'center',
								width: 250
							},
							{
								header: 'Intermediario Financiero (Cesionario)',
								tooltip: 'Intermediario Financiero (Cesionario)',
								dataIndex: 'NOMBRE_CESIONARIO',
								align: 'center',
								width: 250
							},
							{
								header: 'No. Contrato',
								tooltip: 'No. Contrato',
								dataIndex:'NUMERO_CONTRATO',
								align: 'center',
								width: 100
							},
							{
								header: 'Firma Contrato',
								tooltip: 'Firma Contrato',
								dataIndex: 'FIRMA_CONTRATO',
								sortable: true,
								resizable: true,
								width: 130,				
								align: 'center'
							},
							{
								header: 'Monto/Moneda',
								tooltip: 'Monto/Moneda',
								dataIndex: 'MONTO_MONEDA',
								align: 'left',
								width: 200
							},
							{
								header: 'Tipo de Contrataci�n',
								tooltip: 'Tipo de Contrataci�n',
								dataIndex: 'TIPO_CONTRATACION',
								align: 'center',
								width:120
							},
							{
								header: 'Fecha Inicio Contrato',
								tooltip: 'Fecha Inicio Contrato',
								dataIndex: 'FECHA_INICIO_CONTRATO',
								align: 'center',
								width: 120
							},
							{
								header: 'Fecha Final Contrato',
								tooltip: 'Fecha Final Contrato',
								dataIndex: 'FECHA_FIN_CONTRATO',
								align: 'center',
								width: 120
							},
							{
								header: 'Plazo del Contrato',
								tooltip: 'Plazo del Contrato',
								dataIndex: 'PLAZO_CONTRATO',
								align: 'center',
								width: 120
							},
							{
								header: 'Objeto del Contrato',
								tooltip: 'Objeto del Contrato',
								dataIndex: 'OBJETO_CONTRATO',
								align: 'center',
								width: 200,
								resiazable: true
							},
							{
								header: 'Comentarios',
								tooltip: 'Comentarios',
								dataIndex: 'COMENTARIOS',
								align: 'center',
								width: 120
							},
							{
								header: 'Estatus',
								tooltip: 'Estatus',
								dataIndex: 'ESTATUS_SOLICITUD',
								align: 'center',
								width: 120
							},
							{
								xtype: 'actioncolumn',
								header: 'Causa del Rechazo',
								tooltip: 'Causa del Rechazo',
								dataIndex: 'CAUSAS_RECHAZO',
								align: 'center',
								width: 120,
								renderer: function(value, metadata, registro, rowindex, colindex, store) {
									if(registro.get('CAUSAS_RECHAZO')!='N/A'&&registro.get('CAUSAS_RECHAZO').length>15){
										 return registro.get('CAUSAS_RECHAZO').substring(0,14) ;
									}else{
										return registro.get('CAUSAS_RECHAZO') ;
									}
								},
								items: [
											{
												getClass: function(value,metadata,registro,rowIndex,colIndex,store){
												
													if(registro.get('CAUSAS_RECHAZO')!='N/A'&&registro.get('CAUSAS_RECHAZO').length>15){
														this.items[0].tooltip = 'm�s';
														
														return 'mas';
													}else{
														return '';
													}
												},
												handler: function(grid,rowIndex,colIndex,item,event){
													
													var registro = grid.getStore().getAt(rowIndex);	
													Ext.Msg.alert('Causas de Rechazo',registro.get('CAUSAS_RECHAZO'));
												}
											}
								]
							},
							{
								xtype: 'actioncolumn',
								header: 'Contrato',
								tooltip: 'Contrato',
								dataIndex: 'CLAVE_SOLICITUD',
								align: 'center',
								width: 120,
								items: [
											{
												getClass: function(value,metadata,registro,rowIndex,colIndex,store){
													if(registro.get('CLAVE_SOLICITUD')!=''){
														this.items[0].tooltip = 'Ver';
														return 'iconoLupa';
													}
												},
												handler: mostrarContrato
											}
								]
							},
							{
								xtype: 'actioncolumn',
								header: 'Autorizaci�n Consentimiento',
								tooltip: 'Autorizaci�n Consentimiento',
								dataIndex: 'AUTORIZACION_CONSENTIMIENTO',
								hidden: true,
								align: 'center',
								width: 120,
								items: [
									{
										getClass: function(value,metadata,registro,rowIndex,colIndex,store){
											if(registro.get('CLAVE_SOLICITUD')!=''){
												this.items[0].tooltip = 'Ver';
												return 'iconoLupa';
											}
										},
										handler: VisorAutoConsorcio
									}
								]
							},
							
							{
								header: 'Motivos de Cancelaci�n',
								tooltip: 'Motivos de Cancelaci�n',
								dataIndex: 'MOTIVOS_CANCELACION',
								hidden: true,
								align: 'center',
								width: 120				
							},									
			
							{
								xtype: 'actioncolumn',
								header: 'Consentimiento',
								tooltip: 'Consentimiento',
								dataIndex: 'CLAVE_CONTRATACION',
								align: 'center',
								width: 150,
								items: [
											{
												getClass: function(value,metadata,registro,rowIndex,colIndex,store){
													this.items[0].tooltip = 'Ver';
													return 'iconoLupa'
												},
												handler: mostrarConsentimiento
											}
								]
							},
							{
								xtype: 'actioncolumn',
								header: 'Contrato de Cesi�n',
								tooltip: 'Contrato de Cesi�n',
								dataIndex: 'ESTATUS_SOLICITUD',
								align:  'center',
								width: 120,
								items: [
											{
												getClass: function(value,metadata,registro,rowIndex,colIndex,store){
													if(registro.get('CLAVE_ESTATUS')=='2'||
														registro.get('CLAVE_ESTATUS')=='5'||
														registro.get('CLAVE_ESTATUS')=='24'||
														registro.get('CLAVE_ESTATUS')=='8'){
														value = "N/A";
														return value
													}else{
														this.items[0].tooltip = 'Ver';
														return 'iconoLupa'
													}
												},
												handler: descargarContratoCesion
											}
								],
								renderer: function(value, metadata, record, rowIndex, colIndex, store){
												if(record.get('CLAVE_ESTATUS')=='2'||
													record.get('CLAVE_ESTATUS')=='5'||
													record.get('CLAVE_ESTATUS')=='24'||
													record.get('CLAVE_ESTATUS')=='8'){
														value = "N/A";
														return value
													}
								}
							}
			],
			stripeRows: true,
			loadMask: true,
			height: 400,
			width: 885,
			title: '',
			frame: true,
			bbar: {
						xtype: 'paging',
						pageSize: 15,
						buttonAlign: 'left',
						id: 'barraPaginacion1',
						displayInfo: true,
						store: consultaDataA,
						displayMsg: '{0} - {1} de {2}',
						emptyMsg: "No hay registros"
			}
	}
	var gridB = {
		xtype: 'grid',
		store: consultaDataB,
		id: 'gridB',
		columns: [
						{
							header: 'Intermediario Financiero (Cesionario)',
							tooltip: 'Intermediario Financiero (Cesionario)',
							dataIndex: 'NOMBRE_CESIONARIO',
							align: 'left',
							width: 250
						},
						{
							header: 'Pyme (Cedente)',
							tooltip: 'Pyme (Cedente)',
							dataIndex: 'NOMBRE_CEDENTE',
							align: 'left',
							width: 250
						},
						{
							header: 'No. Contrato',
							tooltip: 'No.Contrato',
							dataIndex: 'NUMERO_CONTRATO',
							align: 'center',
							width:100
						},
						{
							header: 'Firma Contrato',
							tooltip: 'Firma Contrato',
							dataIndex: 'FIRMA_CONTRATO',
							sortable: true,
							resizable: true,
							width: 130,				
							align: 'center'
						},
						{
							header: 'Monto/Moneda',
							tooltip: 'Monto/Moneda',
							dataIndex: 'MONTO_MONEDA',
							align: 'left',
							width: 200
						},
						{
							header: 'Tipo de Contrataci�n',
							tooltip: 'Tipo de Contrataci�n',
							dataIndex: 'TIPO_CONTRATACION',
							align: 'center',
							width: 170
						},
						{
							header: 'Fecha Inicio Contrato',
							tooltip: 'Fecha Inicio del Contrato',
							dataIndex: 'FECHA_INICIO_CONTRATO',
							align: 'center',
							width: 120
						},
						{
							header: 'Fecha Final Contrato',
							tooltip: 'Fecha Final del Contrato',
							dataIndex: 'FECHA_FIN_CONTRATO',
							align: 'center',
							width: 120
						},
						{
							header: 'Plazo del Contrato',
							tooltip: 'Plazo del Contrato',
							dataIndex: 'PLAZO_CONTRATO',
							align: 'center',
							width: 100
						},
						{
							header: 'Clasificaci�n EPO',
							tooltip: 'Clasificaci�n EPO',
							dataIndex: 'CLASIFICACION_EPO',
							align: 'center',
							width: 200
						},
						{
							header: 'Objeto del Contrato',
							tooltip: 'Objeto del Contrato',
							dataIndex: 'OBJETO_CONTRATO',
							align: 'center',
							width: 120
						},
						{
							xtype: 'actioncolumn',
							header: 'Contrato',
							tooltip: 'Contrato',
							dataIndex: 'CLAVE_SOLICITUD',
							width: 100,
							align: 'center',
							items: [
										{
											getClass: function(value,metadata,registro,rowIndex,colIndex,store){
												if(registro.get('CLAVE_SOLICITUD')!=''){
													this.items[0].tooltip = 'Ver';
													return 'iconoLupa'
												}
											},
											handler: mostrarContrato
										}
							]
						},
						{
							header: 'Fecha de Extinci�n del Contrato',
							tooltip: 'Fecha de Extinci�n del Contrato',
							dataIndex: 'FECHA_EXTINCION',
							align: 'center',
							width: 120
						},	
						{
							header: 'Cuenta',
							tooltip: 'Cuenta',
							dataIndex: 'NUMERO_CUENTA',
							align: 'center',
							width: 120
						},
						{
							header: 'Cuenta CLABE',
							tooltip: 'Cuenta CLABE',
							dataIndex: 'CUENTA_CLABE',
							align: 'center',
							width: 120
						},
						{
							header: 'Banco de D�posito',
							tooltip: 'Banco de D�posito',
							dataIndex: 'BANCO_DEPOSITO',
							align: 'center',
							width: 120
						},
						{
							header: 'Causa del Rechazo',
							tooltip: 'Causa del Rechazo',
							dataIndex: 'CAUSAS_RECHAZO_EXT',
							align: 'center',
							width: 120
						},
						{
							header: 'Estatus',
							tooltip: 'Estatus',
							dataIndex: 'ESTATUS_SOLICITUD',
							align: 'center',
							width: 150
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 885,
		title: '',
		frame: true,
		bbar: {
					xtype: 'paging',
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion2',
					displayInfo: true,
					store: consultaDataB,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros."
		}
	};
	var gridSolicitudes = new Ext.grid.GridPanel({
		id: 'gridSolicitudes',
		store: consultaPrincipalData,
		style: 'margin:0 auto;',
		hidden: false,
		hideHeaders: true,
		columns: [
						{
							xtype: 'actioncolumn',
							header: 'SOLICITUDES DE CONSENTIMIENTO',
							dataIndex: 'Descripcion',
							tooltip: 'Solicitudes de consentimiento',
							sortable: false,
							resiazable: false,
							align: 'left',
							width: 30,
							items: [
										{
											getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
												if(rowIndex!=2){
													if(registro.get('Descripcion')!=''){
														this.items[0].tooltip = 'Ver';
														return 'iconoLupa';
													}
												}
											},
											handler: mostrarGridA
										}
							]	
						},
						{
							header: 'SOLICITUDES DE CONSENTIMIENTO',
							dataIndex: 'Descripcion',
							tooltip: 'Solicitudes de consentimiento',
							sortable: false,
							resiazable: false,
							align: 'left',
							width: 250,
							renderer: function(value,metadata,record,rowIndex,colIndex,store){
									if(record.get('Descripcion')=="TOTAL SOLICITUDES"){
										value = "<P align='center'>TOTAL SOLICITUDES";
										return value;
									}else{
										return (record.get('Descripcion'));
									}
							}
						},
						{
							header: 'TOTAL',
							dataIndex: 'Total',
							sortable: false,
							resiazable: false,
							align: 'center',
							width: 100
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 250,
		width: 397,
		title: '',
		frame: true
	});
	var gridNotificaciones = new Ext.grid.GridPanel({
		id: 'gridNotificaciones',
		store: consultaPrincipalData,
		style: 'margin:0 auto;',
		hidden: false,
		hideHeaders: true,
		columns: [
						{
							xtype: 'actioncolumn',
							header: 'NOTIFICACIONES',
							dataIndex: 'Descripcion',
							tooltip: 'Notificaciones',
							sortable: false,
							resiazable: false,
							align: 'center',
							width: 30,
							items: [
										{
											getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
													if(registro.get('Descripcion')!=''){
														this.items[0].tooltip = 'Ver';
														return 'iconoLupa';
													}
											},
											handler: mostrarGridA
										}
							]
						},
						{
							header: 'NOTIFICACIONES',
							dataIndex: 'Descripcion',
							tooltip: 'Notificaciones',
							sortable: false,
							resiazable: false,
							align: 'left',
							width: 250
						},
						{
							header: 'TOTAL',
							dataIndex: 'Total',
							sortable: false,
							resiazable: false,
							align: 'center',
							width: 100
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 250,
		width: 397,
		title: '',
		frame: true
	});
	var gridExtincionContrato = new Ext.grid.GridPanel({
		id: 'gridExtincionContrato',
		store: consultaPrincipalData,
		style: 'margin: 0 auto;',
		hidden: false,
		hideHeaders: true,
		columns: [
						{
							xtype: 'actioncolumn',
							header: 'EXTINCI�N DE CONTRATO',
							dataIndex: 'Descripcion',
							tooltip: 'Extinci�n de contrato',
							sortable: false,
							resiazable: false,
							align: 'center',
							width: 30,
							items: [
										{
											getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
													if(registro.get('Descripcion')!=''){
														this.items[0].tooltip = 'Ver';
														return 'iconoLupa';
													}
											},
											handler: mostrarGridB
										}
							]
						},
						{
							header: 'EXTINCI�N DE CONTRATO',
							dataIndex: 'Descripcion',
							tooltip: 'Extinci�n de Contrato',
							sortable: false,
							resiazable: false,
							align: 'left',
							width: 250
						},
						{
							header: 'TOTAL',
							dataIndex: 'Total',
							sortable: false,
							resiazable: false,
							align: 'center',
							width: 100
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 161,
		width: 397,
		title: '',
		frame: true
	});
	var panelConsentimiento = new Ext.Panel({
		id: 'panelConsentimiento',
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true
	});
	var fpTitulo = new Ext.FormPanel({
		id: 'formaTitulo',
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: false,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		labelWidth: 0,
		defaultType: 'textfield',
		height:	20,
		width: 400,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		items: elementoTitulo,
		monitorValid: false
	});
//-----------------------------------PRINCIPAL----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fpTitulo,
			NE.util.getEspaciador(15),
			gridSolicitudes,
			NE.util.getEspaciador(15),
			gridNotificaciones,
			NE.util.getEspaciador(15),
			gridExtincionContrato,
			NE.util.getEspaciador(15)
		]
	});
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '34MonitorEpoext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
	consultaPrincipalData.load();
});