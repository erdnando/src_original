<%@ page 
	import="java.util.*,
	com.netro.cesion.*,
	netropology.utilerias.*"
	errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<%@ page contentType="text/html;charset=windows-1252"%>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="<%=strDirecVirtualCSS%><%=strClase%>">
<%
String tipoContratacion  = request.getParameter("tipoContratacion")==null?"":(String)request.getParameter("tipoContratacion");
StringBuffer textoConsentimiento = new StringBuffer();
%>
<p>
<table align='center' border='0' cellpadding='1' cellspacing='1' width='650'>
	<%textoConsentimiento.append("Consentimiento Condicionado para Ceder los Derechos de Cobro respecto del Contrato\n\n");%>
  <%textoConsentimiento.append(" En atenci�n a su solicitud, esta Entidad, le informa que con base en la informaci�n recabada al interior del organismo, se otorga el consentimiento que solicita, con fundamento en: El Art�culo 46 de la Ley de Adquisiciones, Arrendamientos y Servicios del Sector P�blico.  El Art�culo 47 de la Ley de Obras P�blicas y Servicios Relacionados con las Mismas.  Los Art�culos 53, fracci�n XVII, de la Ley de Petr�leos Mexicanos, 48 de su Reglamento, y 54 de las Disposiciones administrativas de contrataci�n en materia de adquisiciones, arrendamientos, obras y servicios de las actividades sustantivas de car�cter productivo de Petr�leos Mexicanos y  Organismos Subsidiarios) \n\n");%>
	<%textoConsentimiento.append("No obstante lo anterior, el Consentimiento estar� Condicionado a que: \n\n");%>
   <%textoConsentimiento.append("1.- NOMBRE DE LA CEDENTE, en lo sucesivo la CEDENTE, deber� celebrar con el INTERMEDIARIO FINANCIERO un Contrato de Cesi�n Electr�nica de Derechos de Cobro, mismo que deber� ser suscrito por el o los representantes legales o apoderados de la CEDENTE que cuente con facultades generales o especiales para actos de dominio y que dicho instrumento contractual sea notificado a la Entidad a trav�s de este sistema NAFIN para que surta efectos  legales, en t�rminos de lo establecido en el art�culo 390 del C�digo de Comercio, en el entendido que de no efectuarse la mencionada notificaci�n dentro de los 15 (quince) d�as h�biles siguientes a la fecha en que sea notificada en el sistema NAFIN el presente Consentimiento Condicionado, NAFIN deber� reactivar el tr�mite por otro periodo igual para proseguir con el tr�mite correspondiente, sin ninguna responsabilidad para la Entidad. \n\n");%>
	<%textoConsentimiento.append("2.- La Cesi�n de Derechos de Cobro incluir� los incrementos al monto del contrato arriba se�alado que, en su caso, se pacten con posterioridad mediante convenios modificatorios; y que la Cesi�n de Derechos de Cobro �nica y exclusivamente proceder� con respecto a facturas pendientes de cobro por trabajos ejecutados/servicios prestados/bienes entregados/ejecutados en t�rminos de lo pactado en el contrato y que hayan sido instruidos por la Entidad.\n\n");%>
	<%textoConsentimiento.append("3.- La Cesi�n de Derechos de Cobro ser� por el 100% (cien por ciento) de los que se deriven por el cumplimiento en t�rminos del mencionado contrato, a partir de la fecha en que surta efectos la notificaci�n del correspondiente Contrato de Cesi�n Electr�nica de Derechos de Cobro. \n\n");%>
	<%textoConsentimiento.append("4.- El INTERMEDIARIO FINANCIERO acepte y est� de acuerdo en que el pago de los derechos de cobro estar� sujeto a que, en su caso, la Entidad realice las deducciones y/o retenciones que deban hacerse a la CEDENTE por adeudos con el organismo; penas convencionales; pagos en exceso; cualquier requerimiento de pago derivado de laudo firme ordenado por las Juntas Locales o Federales de Conciliaci�n y Arbitraje, con motivo de los juicios laborales instaurados en contra de Petr�leos Mexicanos y sus Organismos Subsidiarios, en relaci�n con la CEDENTE; o cualquier otra deducci�n derivada del contrato mencionado o de la Ley de la materia en relaci�n con el  contrato celebrado  con la Entidad y que es objeto de la presente cesi�n de derechos de cobro, en cuyos supuestos se har�n las deducciones y/o retenciones correspondientes. En tal virtud, la Entidad quedar� liberada de cualquier responsabilidad por las deducciones y/o retenciones a que haya lugar. \n\n");%>
	<%textoConsentimiento.append("5.- El Consentimiento Condicionado otorgado por la Entidad, no constituye una garant�a de pago ni reconocimiento de cualquier derecho del INTERMEDIARIO FINANCIERO frente a la CEDENTE, siendo la �nica obligaci�n de la Entidad, la de pagar cantidades l�quidas que sean exigibles por el cumplimiento del citado contrato una vez hechas las deducciones y/o retenciones antes mencionadas.\n\n");%>
	<%textoConsentimiento.append("6.- Se deber�n proporcionar los datos de la cuenta �NICA bancaria del INTERMEDIARIO FINANCIERO a la cual se realizar�n los pagos. \n\n");%>
	<%textoConsentimiento.append("7.- A la notificaci�n que se practique a la Entidad mediante este sistema NAFIN, NAFIN deber� subir al mismo: i) un ejemplar del Contrato de Cesi�n Electr�nica de Derechos de Cobro con las firmas electr�nicas de los representantes de la CEDENTE y del INTERMEDIARIO FINANCIERO; ii) copia certificada por fedatario p�blico de la documentaci�n con la que se acredite la personalidad del o de los representantes legales o apoderados de la CEDENTE con las facultades generales o especiales para actos de dominio, y iii) copia certificada por fedatario p�blico de la documentaci�n con la que se acredite la personalidad del o de los representantes legales o apoderados del INTERMEDIARIO FINANCIERO con las facultades suficientes para la celebraci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro en cuesti�n. \n\n");%>
	<%textoConsentimiento.append("8.- El presente Consentimiento Condicionado, quedar� sin efecto en caso de que la Entidad antes de la fecha de notificaci�n a que se refiere el punto n�mero �1� del presente Consentimiento Condicionado, reciba alguna notificaci�n de autoridad judicial o administrativa que incida sobre el contrato cuyos derechos de cobro ser�n materia de cesi�n. Bajo este supuesto, la Entidad lo informar� inmediatamente mediante este sistema NAFIN a la CEDENTE y al INTERMEDIARIO FINANCIERO, una vez que haya recibido formalmente la notificaci�n de que se trate. \n\n");%>
	<%textoConsentimiento.append("9.- El Contrato de Cesi�n Electr�nica de Derechos de Cobro quedar� sin efectos si y s�lo si la CEDENTE y el INTERMEDIARIO FINANCIERO celebran un Convenio de Extinci�n de la Cesi�n Electr�nica de Derechos de Cobro, en la inteligencia de que sus representantes legales o apoderados deber�n contar con las facultades suficientes para ello, y en su oportunidad notificarlo a la Entidad en los mismos t�rminos previstos para la notificaci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro para que surta los efectos legales que le correspondan. \n\n");%>
	
	<tr>
	<td align="left" class="formas" colspan="3">
	<textarea cols=140 rows=20 name="consentimiento"><%=textoConsentimiento.toString()%></textarea>
	</td>
	</tr>	
</table>
<p>
<p>
<p>
