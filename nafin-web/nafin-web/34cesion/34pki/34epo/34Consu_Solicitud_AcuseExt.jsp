<!DOCTYPE html>
<%@ page import="java.util.*,
		com.netro.cesion.*,
		netropology.utilerias.*" 
		contentType="text/html;charset=windows-1252"
		errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<% 
String clave_solicitud  = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";	
String acuse  = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";		
String tipoContratacion  = (request.getParameter("tipoContratacion")!=null)?request.getParameter("tipoContratacion"):"";		
String banderaRechazo  = (request.getParameter("banderaRechazo")!=null)?request.getParameter("banderaRechazo"):"";		

%>
	
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="34Consu_Solicitud_AcuseExt.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
	
	<form id='formParametros' name="formParametros">	
		<input type="hidden" id="clave_solicitud" name="clave_solicitud" value="<%=clave_solicitud%>"/>	
		<input type="hidden" id="acuse" name="acuse" value="<%=acuse%>"/>	
		<input type="hidden" id="tipoContratacion" name="tipoContratacion" value="<%=tipoContratacion%>"/>	
		<input type="hidden" id="banderaRechazo" name="banderaRechazo" value="<%=banderaRechazo%>"/>	
	</form>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
