Ext.onReady(function() {

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

	var procesarConsultaContratos = function(store, registros, opts){
 
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		
		if (registros != null) {

			var grid  = Ext.getCmp('gridContratos');

			if (!grid.isVisible()) {
				grid.show();
			}

			Ext.getCmp('btnBajarArchivo').hide();
			Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				Ext.getCmp('botonBbarGenerarReporte').enable();
				el.unmask();
			} else {
				Ext.getCmp('botonBbarGenerarReporte').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
		}
	}

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

	var catalogoIfData = new Ext.data.JsonStore({
		id:				'catalogoIfDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'34contratosCedidos.data.jsp',
		baseParams:		{	informacion: 'CatalogoIf'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoNombrePymeData = new Ext.data.JsonStore({
		id:				'catalogoNombrePymeDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'34contratosCedidos.data.jsp',
		baseParams:		{	informacion:	'CatalogoNombrePyme'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var contratosConsultadosData = new Ext.data.JsonStore({
		root: 		'registros',
		id:			'contratosConsultadosDataStore',
		url: 			'34contratosCedidos.data.jsp',
		baseParams: {	informacion:	'ConsultaContratos'	},
		fields: [
			{ name: 'CONTRATO', 						type: 'string' },
			{ name: 'IC_EPO', 						type: 'string' },
			{ name: 'IC_IF', 							type: 'string' },
			{ name: 'IC_PYME', 						type: 'string' },
			{ name: 'NOMBRE_CESIONARIO',			type: 'string' },
			{ name: 'NOMBRE_PYME',					type: 'string' },
			{ name: 'ELIMINAR_CONTRATO',			type: 'string' },
			{ name: 'FECHA_CAPTURA', 			   convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload:	{
				fn: function(store, options){
					/*
					console.log("options.params(beforeload)");
					console.dir(options.params);
					*/
				}
			},
			load: 	procesarConsultaContratos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaContratos(null, null, null);
				}
			}
		}
	});

//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - 

	var procesaConsultaContrato = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							consultaContrato(resp.estadoSiguiente,resp);
						}
					);
				} else {
					consultaContrato(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var consultaContrato = function(estadoSiguiente, respuesta){

		if(			estadoSiguiente == "GUARDAR_CONTRATO" 											){

			var forma = Ext.getCmp("forma");
			forma.el.mask('Guardando...','x-mask-loading');

 			Ext.Ajax.request({
				url:			'34contratosCedidos.data.jsp',
				params:		Ext.apply(Ext.getCmp('forma').getForm().getValues(),{	informacion:'CapturaSaveContrato'}),
				callback:	procesaConsultaContrato
			});
			
		} else if(  estadoSiguiente == "RESPUESTA_GUARDAR_CONTRATO" 							){

			Ext.Msg.alert('Mensaje','El contrato se guard� con �xito',
				function(){
					consultaContrato("FIN",null);
				}
			);

		} else if(  estadoSiguiente == "CONSULTA_CONTRATOS" 										){

			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
			
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridContratos").hide();
			Ext.StoreMgr.key('contratosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consultar',
								operacion: 'Generar',
								start: 0,
								limit: 15
							}
				)
			});

		} else if(  estadoSiguiente == "GENERAR_REPORTE" 											){
 
 			Ext.Ajax.request({
				url:			'34contratosCedidos.data.jsp',
				params:		Ext.apply(Ext.getCmp('forma').getForm().getValues(),{	informacion: 'ArchivoCSV'	}),
				callback:	procesaConsultaContrato
			});

		} else if(  estadoSiguiente == "DESCARGAR_REPORTE"											){

			var btnGenerarArchivo = Ext.getCmp('botonBbarGenerarReporte');
			btnGenerarArchivo.setIconClass('icoXls');
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var archivo = respuesta.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			});

		}else if(	estadoSiguiente == "ELIMINAR_CONTRATO"							){

			Ext.MessageBox.confirm(
				'Aviso',
				'�Seguro que desea eliminar el contrato?',
				function(boton) {

					if (boton == 'yes') {

						// Cargar los valores iniciales
						Ext.Ajax.request({
							url: 		'34contratosCedidos.data.jsp',
							params: 	{
								informacion:	'ConsultaContrato.eliminarContrato',
								claveContrato:	respuesta.claveContrato,
								claveEpo:		respuesta.claveEpo
							},
							callback:procesaConsultaContrato
						});

					} else if (boton == 'no') {
						return;
					}
				}
			);

		} else if(  estadoSiguiente == "ACTUALIZAR_CONSULTA"                          	){
   
			Ext.Msg.alert('Aviso','El registro fue eliminado');
			Ext.getCmp("barraPaginacion").doRefresh();

		} else if(	estadoSiguiente == "BUSCAR_CONTRATO_CEDIDO"									){

			Ext.getCmp('forma').el.mask('Buscando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '34contratosCedidos.data.jsp',
				params: {
					informacion:	'BuscaContratoCedido',
					claveContrato:	respuesta.claveContrato,
					clavePyme:		respuesta.clavePyme
				},
				callback: procesaConsultaContrato
			});

		} else if(	estadoSiguiente == "BUSCAR_CONTRATO_SOLICITUD"								){

			Ext.getCmp('forma').el.mask('Buscando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '34contratosCedidos.data.jsp',
				params: {
					informacion:	'BuscaContratoSolicitud',
					claveContrato:	respuesta.claveContrato,
					clavePyme:		respuesta.clavePyme
				},
				callback: procesaConsultaContrato
			});

		} else if(	estadoSiguiente == "CONSULTA_CONTRATO_SOLICITUD"							){

			Ext.getCmp('forma').el.mask('Buscando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '34contratosCedidos.data.jsp',
				params: {
					informacion:	'BuscaContratoSolicitudConsulta',
					claveContrato:	respuesta.claveContrato,
					clavePyme:		respuesta.clavePyme
				},
				callback: procesaConsultaContrato
			});

		} else if(	estadoSiguiente == "RESPUESTA_BUSCAR_CONTRATO_CEDIDO"						){

			if (respuesta.existe){
				var noContrato = Ext.getCmp('id_contrato');
				Ext.Msg.alert('Mensaje','El contrato capturado ya fue cedido',
					function(){
						noContrato.setValue('');
						noContrato.fireEvent('focus');
					}
				);
				return;
			}else{

				consultaContrato("BUSCAR_CONTRATO_SOLICITUD",respuesta);
			}

		} else if(	estadoSiguiente == "RESPUESTA_BUSCAR_CONTRATO_SOLICITUD"					){
			
			if (respuesta.existe){
				var noContrato = Ext.getCmp('id_contrato');
				Ext.Msg.alert('Mensaje','El contrato est� siendo cedido electr�nicamente',
					function(){
						noContrato.setValue('');
						noContrato.fireEvent('focus');
					}
				);
				return;
			}else{

				consultaContrato("GUARDAR_CONTRATO",null);
			}

		} else if(	estadoSiguiente == "RESPUESTA_CONSULTA_CONTRATO_SOLICITUD"	 			){
			
			if (respuesta.existe){

				var grid  = Ext.getCmp('gridContratos');
				grid.store.removeAll();
				if (!grid.isVisible()) {
					grid.show();
				}
				Ext.getCmp('barraPaginacion').hide();
				grid.getGridEl().mask(respuesta.mensaje, 'x-mask');

			}else{
			
				consultaContrato("CONSULTA_CONTRATOS",null);

			}

		} else if(	estadoSiguiente == "LIMPIAR"														){

			Ext.getCmp('forma').getForm().reset();
			grid.hide();

		} else if(	estadoSiguiente == "FIN"															){
						
			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "34contratosCedidosExt.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -
	// Grid con las cuentas registradas
	var grid = new Ext.grid.GridPanel({
		store: 	contratosConsultadosData,
		id:		'gridContratos',
		style: 	'margin: 0 auto; padding-top:10px;',
		hidden: 	true,
		margins: '20 0 0 0',
		title: 	undefined,
		view: new Ext.grid.GridView({forceFit:	true}),
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		width: 		850,
		frame: 		true,
		columns: [
			//.toUpperCase();
			{
				header: 		'Contrato<br>&nbsp;',
				tooltip: 	'Contrato',
				dataIndex: 	'CONTRATO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		180,
				hidden: 		false,
				hideable:	false
			},{
				header: 		'Nombre Cesionario<br>&nbsp;',
				tooltip: 	'Nombre Cesionario',
				dataIndex: 	'NOMBRE_CESIONARIO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		250,
				hideable:	false,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'Pyme(Cedente)<br>&nbsp;',
				tooltip: 	'Pyme(Cedente)',
				dataIndex: 	'NOMBRE_PYME',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		250,
				hidden: 		false,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
            xtype: 		'actioncolumn',
            header: 		'Eliminar<br>&nbsp;',
				tooltip: 	'Eliminar',
            width: 		100,
            align:		'center',
            hidden: 		false,
            items: [
                // ACCION ELIMINAR CONTRATO
            	 {
                    getClass: function(value, metadata, record) { 

                        if (record.json['ELIMINAR_CONTRATO'] === "true" ) {
                            this.items[0].tooltip = 'Eliminar Contrato';
                            return 'borrar';
                        } else {
                            this.items[0].tooltip = '';
									 return null;
                        }
                    },
                    handler: function(grid, rowIndex, colIndex) {

                        var record = Ext.StoreMgr.key('contratosConsultadosDataStore').getAt(rowIndex);
                        // Eliminar Contrato
                        if (record.json['ELIMINAR_CONTRATO'] == "true" ) {

                        	var respuesta = new Object();
                        	respuesta['claveContrato']	= record.json['CONTRATO'];
									respuesta['claveEpo']		= record.json['IC_EPO'];
                        	consultaContrato("ELIMINAR_CONTRATO",respuesta);
                        }
                    }
                }
            ]
         }
		],
		bbar: {
			xtype: 			'paging',
			pageSize: 		15,
			buttonAlign: 	'left',
			id: 				'barraPaginacion',
			opts:				null,
			displayInfo: 	true,
			store: 			contratosConsultadosData,
			displayMsg: 	'{0} - {1} de {2}',
			emptyMsg: 		"No hay registros.",
			items: [
					'->',
					'-',
					{
						xtype:	'button',
						iconCls: 'icoXls',
						text: 	'Generar Archivo',
						id: 		'botonBbarGenerarReporte',
						disabled:true,
						handler: function(boton, evento) {
										boton.disable();
										boton.setIconClass('loading-indicator');
										consultaContrato("GENERAR_REPORTE",null);
						}
					},{
						xtype:	'button',
						text:		'Bajar Archivo',
						id: 		'btnBajarArchivo',
						hidden:	true
					}
			]
		}
	});

	var elementosForma = [
		{
			xtype:			'combo',
			id:				'cboNombreCesionario',
			name:				'cesionario',
			hiddenName:		'cesionario',
			fieldLabel:		'Nombre de Cesionario (IF)',
			emptyText:		'Seleccionar. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:false,
			typeAhead:		true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoIfData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'combo',
			id:				'cboPyme',
			name:				'ic_pyme',
			hiddenName:		'ic_pyme',
			fieldLabel:		'Pyme',
			emptyText:		'Seleccionar. . .',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		true,
			mode:				'local',
			minChars			:1,
			store:			catalogoNombrePymeData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'textfield',
			name:				'noContrato',
			id:				'id_contrato',
			style:			{textTransform: "uppercase"},
			allowBlank:		true,
			vtype:			'alphanum',
			fieldLabel:		'N�mero de contrato',
			anchor:			'50%',
			maxLength:		20,
			listeners:	{
				change:	function(field, newValue, oldValue){
								field.setValue(newValue.toUpperCase());
							}/*,
				blur:		function(field){
								if (Ext.getCmp('btnCaptura').getText() === 'Guardar'){

									if(	!Ext.isEmpty(field.getValue())	){
	
										var respuesta = new Object();
										respuesta['claveContrato']	= field.getValue();
										consultaContrato("BUSCAR_CONTRATO",respuesta);
	
									}

								}
							}*/
			}
		}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		title:			'Criterios de b�squeda',
		width:			610,
		style:			'margin:0 auto;',
		collapsible:	true,
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		monitorValid:	true,
		buttons: [
			{
				text:		'Capturar contrato',
				id:		'btnCaptura',
				iconCls:	'icon-register-add',
				formBind:true,
				handler: function(boton, evento) {

								if (boton.iconCls === 'icon-register-add'){

									Ext.getCmp('gridContratos').hide();
									Ext.getCmp('btnBuscar').hide();
									Ext.getCmp('btnCancelar').show();
									Ext.getCmp('forma').getForm().reset();
									Ext.getCmp('forma').setTitle('Captura contratos cedidos');
									boton.setText('Guardar');
									boton.setIconClass('icoGuardar');
									Ext.getCmp('cboNombreCesionario').allowBlank	= false;
									Ext.getCmp('cboPyme').allowBlank					= false;
									Ext.getCmp('id_contrato').allowBlank			= false;
									Ext.getCmp('cboNombreCesionario').focus();

								}else if(boton.iconCls === 'icoGuardar'){

									var field = Ext.getCmp('id_contrato').getValue();
	
									var respuesta = new Object();
									respuesta['claveContrato']	= field;
									respuesta['clavePyme']	= Ext.getCmp('cboPyme').getValue();
									consultaContrato("BUSCAR_CONTRATO_CEDIDO",respuesta);

								}
							}
			},{
				text:		'Buscar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:true,
				handler: function(boton, evento) {

								if(!Ext.getCmp('forma').getForm().isValid()){
									return;
								}

								if(	!Ext.isEmpty(Ext.getCmp('id_contrato').getValue())	&&	!Ext.isEmpty(Ext.getCmp('cboPyme').getValue())	){

									var respuesta = new Object();
									respuesta['claveContrato']	= Ext.getCmp('id_contrato').getValue();
									respuesta['clavePyme']		= Ext.getCmp('cboPyme').getValue();
									consultaContrato("CONSULTA_CONTRATO_SOLICITUD",respuesta);

								}else{
									consultaContrato("CONSULTA_CONTRATOS",null);
								}
					
				} //fin handler
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								consultaContrato("LIMPIAR",null);
							}
			},{
				text:		'Cancelar',
				id:		'btnCancelar',
				iconCls:	'borrar',
				hidden:	true,
				handler:	function(){
								consultaContrato("FIN",null);
							}
			}
		]
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(5),
			grid
		],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Ocultar mascara del panel de resumen de carga de plantillas
			element = Ext.getCmp('gridContratos').getGridEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Derefenrencia ultimo elemento...
			element = null;

		}

	});
	
	Ext.StoreMgr.key('catalogoIfDataStore').load();
	Ext.StoreMgr.key('catalogoNombrePymeDataStore').load();
	consultaContrato("CONSULTA_CONTRATOS",null);

});
