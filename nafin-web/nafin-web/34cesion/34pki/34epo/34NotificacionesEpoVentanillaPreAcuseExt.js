Ext.onReady(function(){
	
	var clave_solicitud =  Ext.getDom('clave_solicitud').value;
	var pantalla =  Ext.getDom('pantalla').value;
	var tipoOperacion =  Ext.getDom('tipoOperacion').value;
	var numeroContrato;
	var nombreArchivo;
	var observaciones;
	
	var procesarSuccessFailureAcuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){											
			
			var parametros = "pantalla=Acuse"+
				      			"&acuse="+jsondeAcuse.acuse+
									"&fecha="+jsondeAcuse.fecha+
									"&hora="+jsondeAcuse.hora+
									"&nombreUsuario="+jsondeAcuse.nombreUsuario+
									"&clave_solicitud="+jsondeAcuse.clave_solicitud+
									"&tipoOperacion="+jsondeAcuse.tipoOperacion;
				document.location.href = "34NotificacionesEpoVentanillaExt.jsp?"+parametros;	
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	
	var procesoContinuar =  function(opts, success, response) {	
	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var textoFirmar = "Intermediario Financiero(Cesionario)| Pyme (Cedente)|RFC|No. Proveedor|Fecha de Solicitud PyME|No. Contrato| "+
						"Monto / Moneda|Tipo de Contrataci�n|Fecha Inicio Contrato|Fecha Final Contrato|Plazo del Contrato|"+
						"// campos  y clasificacion EPO "+
						"Ventanilla de Pago|Supervisor/Administrador/Residente de Obra|Tel�fono|Objeto del Contrato|"+
						"Objeto del Contrato| Comentarios| Monto del Cr�dito| Referencia / No. Cr�dito|Fecha Vencimiento Cr�dito|"+
						"Banco de Dep�sito| Cuenta|Cuenta CLABE|Estatus| ";
						if(tipoOperacion =='RECHAZO')  {
						textoFirmar += "Observaciones";
						}
						
						textoFirmar += "\n ";
									
		var store = gridConsulta.getStore();
			store.each(function(record) {
			observaciones = record.data['OBSERVACIONES'];	
			 textoFirmar += record.data['NOMBRE_CESINARIO'] +": "+record.data['NOMBRE_PYME']+ ": "+record.data['RFC']+ ": "+record.data['NO_PROVEEDOR']+
								 record.data['FECHA_SOLICITUD'] +": "+record.data['NUMERO_CONTRATO']+ ": "+record.data['MONTO_MONEDA']+ ": "+record.data['TIPO_CONTRATACION']+
								 record.data['F_INI_CONTRATO'] +": "+record.data['F_FIN_CONTRATO']+ ": "+record.data['PLAZO_CONTRATO']+ ": "+record.data['CLASIFICACION_EPO']+
								 record.data['CAMPO1'] +": "+record.data['CAMPO2']+ ": "+record.data['CAMPO3']+ ": "+record.data['CAMPO4']+ ": "+record.data['CAMPO5']+
								 record.data['VENTANILLA_DE_PAGO'] +": "+record.data['SUPERVISOR']+ ": "+record.data['TELEFONO']+ ": "+record.data['OBJETO_CONTRATO']+
								 record.data['COMENTARIOS'] +": "+record.data['MONTO_CREDITO']+ ": "+record.data['REFERENCIA']+ ": "+record.data['F_VENCIMIENTO_CRE']+
								 record.data['BANCO_DEPOSITO'] +": "+record.data['CUENTA']+ ": "+record.data['CUENTA_CLABE']+ ": "+record.data['ESTATUS'];
								 if(tipoOperacion =='RECHAZO')  {
								 textoFirmar += record.data['OBSERVACIONES'];	
								 }
								 textoFirmar += "\n ";
								
			
		});
	
		if(tipoOperacion =='RECHAZO')  {
			if(observaciones =='') {
				Ext.MessageBox.alert("Mensaje","Es obligatorio escribir las observaciones para poder rechazar la notificaci�n");
				return true;
			}
		}
		
		var txtEmail = Ext.getCmp('txtEmail');					
		if(Ext.isEmpty(txtEmail.getValue())){
			txtEmail.markInvalid('Por favor especifique el(los) destinatario(s) del correo.');			
			txtEmail.focus();
			return;
		}
					
		NE.util.obtenerPKCS7(confirmar, textoFirmar);
	}
	
	
	var confirmar = function(pkcs7, textoFirmar) {
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}
			
				
		Ext.Ajax.request({
			url: '34NotificacionesEpoVentanilla.data.jsp',
			params: Ext.apply(fpEnvio.getForm().getValues(),{
				informacion: 'ConfirmaAcuse',
				pkcs7: pkcs7,
				textoFirmar: textoFirmar,
				clave_solicitud:clave_solicitud,
				observaciones:observaciones,
				tipoOperacion:tipoOperacion,
				nombreArchivo:nombreArchivo,
				numeroContrato:numeroContrato
			}),
			callback: procesarSuccessFailureAcuse
		});
	}
	//*******************Correo Electronico ***************************************

	
	var procesarSuccessFailureIniCorreo =  function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeMensaje= Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('lblSellos').update(jsondeMensaje.sellos);
			nombreArchivo=jsondeMensaje.nombreArchivo;
			numeroContrato=jsondeMensaje.lblnumContrato;
			if (jsondeMensaje != null){
				var fp = Ext.getCmp('formaEnvio');
				Ext.getCmp('lblNombrePyme').getEl().update(jsondeMensaje.lblNombrePyme)
				Ext.getCmp('lblRFC').getEl().update(jsondeMensaje.lblRFC);
				Ext.getCmp('lblRep1').update(jsondeMensaje.lblRep1);	
				Ext.getCmp('lblRep2').update(jsondeMensaje.lblRep2);
				Ext.getCmp('lblnumProveedor').update(jsondeMensaje.lblnumProveedor);
				Ext.getCmp('lblFechaSolic').update(jsondeMensaje.lblFechaSolic);
				Ext.getCmp('lblnumContrato').update(jsondeMensaje.lblnumContrato);
				Ext.getCmp('lblmontoMoneda').update(jsondeMensaje.lblmontoMoneda);
				Ext.getCmp('lblFeVigencia').update(jsondeMensaje.lblFeVigencia);				
				Ext.getCmp('lblPlazoCon').update(jsondeMensaje.lblPlazoCon);
				Ext.getCmp('lblTipoContra').update(jsondeMensaje.lblTipoContra);
				Ext.getCmp('lblObContrato').update(jsondeMensaje.lblObContrato);
				Ext.getCmp('lblCampo1').update(jsondeMensaje.lblCampo1);
				Ext.getCmp('lblCampo2').update(jsondeMensaje.lblCampo2);
				Ext.getCmp('lblCampo3').update(jsondeMensaje.lblCampo3);
				Ext.getCmp('lblCampo4').update(jsondeMensaje.lblCampo4);
				Ext.getCmp('lblCampo5').update(jsondeMensaje.lblCampo5);
				Ext.getCmp('lblnombre1').update(jsondeMensaje.lblnombre1);
				Ext.getCmp('lblPersAutorizo').update(jsondeMensaje.lblPersAutorizo);
				Ext.getCmp('lblnombre2').update(jsondeMensaje.lblnombre2);
				Ext.getCmp('lblBancoDep').update(jsondeMensaje.lblBancoDep);
				Ext.getCmp('lblCuentaDep').update(jsondeMensaje.lblCuentaDep);
				Ext.getCmp('lblCuentaCLABEDep').update(jsondeMensaje.lblCuentaCLABEDep);
				Ext.getCmp('lblFSolicConse').update(jsondeMensaje.lblFSolicConse);
				Ext.getCmp('lblFConseEPO').update(jsondeMensaje.lblFConseEPO);
				Ext.getCmp('lblOtoConsen').update(jsondeMensaje.lblOtoConsen);
				Ext.getCmp('lblFormConsen').update(jsondeMensaje.lblFormConsen);
				Ext.getCmp('lblNomFirmantePyme').update(jsondeMensaje.lblNomFirmantePyme);
				Ext.getCmp('lblNomFirmanteIF').update(jsondeMensaje.lblNomFirmanteIF);
				Ext.getCmp('lblTestigo1').update(jsondeMensaje.lblTestigo1);
				Ext.getCmp('lblTestigo2').update(jsondeMensaje.lblTestigo2);
				Ext.getCmp('lblFnotiAcepEpo').update(jsondeMensaje.lblFnotiAcepEpo);
				Ext.getCmp('lblPerAcepNoti').update(jsondeMensaje.lblPerAcepNoti);
				Ext.getCmp('lblFInfoVenta').update(jsondeMensaje.lblFInfoVenta);
				Ext.getCmp('lblFRepcVenta').update(jsondeMensaje.lblFRepcVenta);
				Ext.getCmp('lblFApliRedirec').update(jsondeMensaje.lblFApliRedirec);
				Ext.getCmp('lblFApliRedirec').update(jsondeMensaje.lblFApliRedirec);
				
				clave_solicitud =jsondeMensaje.clave_solicitud;
				
				fp.el.unmask();							
				
			}						
		} else {
			NE.util.mostrarConnError(response,opts);
			fp.el.mask('No se encontr� ning�n registro', 'x-mask');
		}
	}
	


	var elementosFormaEnvio = [
		{
		xtype: 'container',
		anchor: '100%',
		layout: {
						type: 'table',
						columns: 2
		},
		items: [
					{
						xtype: 'label',
						id: 'lblTitulo',
						style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
						fieldLabel: '',
						text: 'Enviar correo de Redireccionamiento Aplicado',
						colspan: 2
					},	
					{
						html:'<div style="height:30px"></div>',
						colspan: 2
					},
					{
						xtype: 'label',
						id: 'lblPara',
						style: 'font-size:smaller;font-weight:bold;text-align:right;display:block;',
						fieldLabel: '',
						text: 'Para:',
						width: 100,
						colspan: 1
					},
					{
						xtype: 'textfield',
						name: 'txtEmail',
						id: 'txtEmail',
						fieldLabel: '',
						//vtype: 'email',
						style: 'font-size:smaller;font-weight:bold;text-align:center;display:block;',
						msgTarget: 'side',
						width: 600,
						maxLength: 1000,	
						colspan: 1
					},
					{
						html:'<div style="height:10px"></div>',
						colspan: 2
					},
					{
						xtype: 'label',
						id: 'lblAviso',
						fieldLabel: '',
						text: '(Para varios destinatarios favor de ingresar las direcciones de correo separadas por comas(,). ejem. agomez@nafin.gob.mx,slopez@nafin.gob.mx)',
						width: 800,
						style: 'font-family:Arial;font-size:smaller;',
						colspan: 2
					},	
					{
						html:'<div style="height:10px"></div>',
						colspan: 2
					},
					{
						xtype: 'label',
						id: 'lblComentarios1',
						id: 'lblComentarios',
						style: 'font-size:smaller;font-weight:bold;text-align:right;display:block;',
						fieldLabel: '',
						text: 'Comentarios:',
						width: 100,
						colspan: 1
					},
					{
						xtype: 'textarea',
						name: 'txtComentarios',
						id: 'txtComentarios',
						fieldLabel: '',
						msgTarget: 'side',
						maxLength: 4000,
						maxLengthText: 'No puedes ingresar m�s de 4000 caracteres.',
						allowBlank: false,
						style: 'font-size:smaller;font-weight:bold;text-align:center;display:block;',
						width: 600,
						colspan: 2
					}
				]
	}];
	
	
		
	var elementosFormaEnvioFp = [
	{
			xtype: 'label',
			id: 'lblSellos',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			text: ''
		},
		{
			xtype: 'label',
			id: 'lblTitulo1',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden: true,
			text: 'NOTIFICACI�N '
		},
		{
			xtype: 'label',
			id: 'lblTitulo2',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden: true,
			text: 'INFORMACION DE LA PYME CEDENTE  '
		},
		{
			xtype: 'label',
			id: 'lblNombrePyme',
			fieldLabel: 'NOMBRE',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblRFC',
			fieldLabel: 'RFC',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblRep1',
			fieldLabel: 'REPRESENTANTE PyME 1',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblRep2',
			fieldLabel: 'REPRESENTANTE PyME 2',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblnumProveedor',
			fieldLabel: 'NUM PROVEEDOR',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblFechaSolic',
			fieldLabel: 'FECHA DE SOLICITUD DE CESION',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTitulo3',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden: true,
			text: 'INFORMACION DEL CONTRATO '
		},
		{
			xtype: 'label',
			id: 'lblnumContrato',
			fieldLabel: 'NUMERO DE CONTRATO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblmontoMoneda',
			fieldLabel: 'MONTO / MONEDA',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblFeVigencia',
			fieldLabel: 'FECHA DE VIGENCIA',
			hidden: true,
			text: '-'
		},		
		{
			xtype: 'label',
			id: 'lblPlazoCon',
			fieldLabel: 'PLAZO DEL CONTRATO',
			hidden: true,
			text: '-'
		}	,
		{
			xtype: 'label',
			id: 'lblTipoContra',
			fieldLabel: 'TIPO DE CONTRATACION',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblObContrato',
			fieldLabel: 'OBJETO DEL CONTRATO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblCampo1',
			fieldLabel: 'CAMPO ADICIONAL 1',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblCampo2',
			fieldLabel: 'CAMPO ADICIONAL 2',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblCampo3',
			fieldLabel: 'CAMPO ADICIONAL 3',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblCampo4',
			fieldLabel: 'CAMPO ADICIONAL 4',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblCampo5',
			fieldLabel: 'CAMPO ADICIONAL 5',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTitulo4',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden: true,
			text: 'INFORMACION DE LA EMPRESA DE PRIMER ORDEN'
		},
		{
			xtype: 'label',
			id: 'lblnombre1',
			fieldLabel: 'NOMBRE',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblPersAutorizo',
			hidden: true,
			fieldLabel: 'PERSONA QUE AUTORIZO LA CESION',
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTitulo5',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden: true,
			text: 'INFORMACION DE LA EMPRESA DE PRIMER ORDEN'
		},		
		{
			xtype: 'label',
			id: 'lblnombre2',
			fieldLabel: 'NOMBRE',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblBancoDep',
			fieldLabel: 'BANCO DE DEPOSITO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblCuentaDep',
			fieldLabel: 'NUMERO DE CUENTA PARA DEPOSITO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblCuentaCLABEDep',
			fieldLabel: 'NUMERO DE CUENTA CLABE PARA DEPOSITO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTitulo6',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldLabel: '',
			hidden: true,
			text: 'INFORMACION DEL CESIONARIO'
		},
		{
			xtype: 'label',
			id: 'lblFSolicConse',
			fieldLabel: 'FECHA DE SOLICITUD DE CONSENTIMIENTO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblFConseEPO',
			fieldLabel: 'FECHA DE CONSENTIMIENTO DE LA EPO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblOtoConsen',
			fieldLabel: 'PERSONA QUE OTORGO CONSENTIMIENTO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblFormConsen',
			fieldLabel: 'FECHA DE FORMALIZACION DEL CONTRATO DE CESION',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblNomFirmantePyme',
			fieldLabel: 'NOMBRE DEL FIRMANTE DE CEDENTE (PYME)',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblNomFirmanteIF',
			fieldLabel: 'NOMBRE DEL FIRMANTE DEL CESIONARIO (IF)',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTestigo1',
			fieldLabel: 'NOMBRE DEL TESTIGO 1 DEL CESIONARIO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblTestigo2',
			fieldLabel: 'NOMBRE DEL TESTIGO 2 DEL CESIONARIO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblFnotiAcepEpo',
			fieldLabel: 'FECHA DE LA NOTIFICACION ACEPTADA POR LA EPO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblPerAcepNoti',
			fieldLabel: 'PERSONA QUE ACEPTA LA NOTIFICACION',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblFInfoVenta',
			fieldLabel: 'FECHA DE INFORMACION A VENTANILLA DE PAGOS',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblFRepcVenta',
			fieldLabel: 'FECHA DE RECEPCION EN VENTANILLA',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblFApliRedirec',
			fieldLabel: 'FECHA DE APLICACION DEL REDIRECCIONAMIENTO',
			hidden: true,
			text: '-'
		},
		{
			xtype: 'label',
			id: 'lblPersoRedirec',
			fieldLabel: 'PERSONA QUE REDIRECCIONA LA CUENTA DE PAGO',
			hidden: true,
			text: '-'
		}
	];
	
	var fpEnvio = new Ext.FormPanel({
		id: 'formaEnvio',
		width: 800,		
		style: ' margin:0 auto;',
		title: '',
		hidden: true,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 250,
		defaultType: 'textfield',
		items: [elementosFormaEnvio, elementosFormaEnvioFp],		
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		monitorValid: false,
		autoScroll: true		
		/*buttons: [
			{
				text: 'Enviar',
				id: 'btnEnviar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton,evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					var txtEmail = Ext.getCmp('txtEmail');					
					if(Ext.isEmpty(txtEmail.getValue())){
						txtEmail.markInvalid('Por favor especifique el(los) destinatario(s) del correo.');
						boton.setIconClass('');
						boton.enable();
						txtEmail.focus();
						return;
					}
					fpEnvio.el.mask('Enviando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '34NotificacionesEpoVentanilla.data.jsp',
						params: Ext.apply(fpEnvio.getForm().getValues(),{
							informacion: 'EnviaCorreo',
							nombreArchivo:nombreArchivo,
							numeroContrato:numeroContrato,
							clave_solicitud: clave_solicitud							
						}),
						callback: procesaEnvio
					});
				}
			},
			{
				text: 'Cerrar',
				id: 'btnCerrar',
				iconCls: 'icoRechazar',
				formBind: true,
				handler:function(boton,evento){
					var ventana = Ext.getCmp('VerEnvioCorreo');
					if(ventana){
						ventana.hide();
					}
				}
			}	
		]
		*/
	});
	
	
	var fpBotones = new Ext.FormPanel({
		id: 'fpBotones',
		width: 450,
		style: ' margin:0 auto;',		
		hidden: false,
		frame: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 250,
		defaultType: 'textfield',			
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		monitorValid: false,
		autoScroll: true,
		buttons: [
			{
				text: 'Rechazar',
				id: 'btnRechazar',
				handler:procesoContinuar
			},			
			{
				text: 'Redireccionar',
				id: 'btnContinuar',
				handler:procesoContinuar
			},
			'-',
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				handler: function(boton, evento) {
					window.location = '34NotificacionesEpoVentanillaExt.jsp';
				}
			}		
		]
		
	});
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{			
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  clasificacionEpo = jsonData.clasificacionEpo;	
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			
			
			if(clasificacionEpo !=''){				
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), false);	
			}
			
			if(hayCamposAdicionales=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='2'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='3'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='4'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='5'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.campo05);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
			}	
			
			if(tipoOperacion =='ACEPTA')  {
				var btnRechazar = Ext.getCmp('btnRechazar');
				btnRechazar.hide();
				
				
				Ext.getCmp('formaEnvio').show();
				Ext.Ajax.request({
					url : '34NotificacionesEpoVentanilla.data.jsp',
					params : {
						informacion: 'IniciaCorreo',				
						clave_solicitud:clave_solicitud				
					},
					callback: procesarSuccessFailureIniCorreo
				});						
				
				
			}
			if(tipoOperacion =='RECHAZO')  {			
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('OBSERVACIONES'), false);			
				var btnContinuar = Ext.getCmp('btnContinuar');
				btnContinuar.hide();	
			}
	
			var el = gridConsulta.getGridEl();						
			if(store.getTotalCount() > 0) {		
				
				el.unmask();
			} else {	
					
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
		
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34NotificacionesEpoVentanilla.data.jsp',
		baseParams: {
			informacion: 'ConsultaPreAcuse'
		},
			fields: [
					{name: 'NOMBRE_CESINARIO'},
					{name: 'NOMBRE_PYME'},
					{name: 'RFC'},
					{name: 'NO_PROVEEDOR'},
					{name: 'FECHA_SOLICITUD'},
					{name: 'NUMERO_CONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'TIPO_CONTRATACION'},
					{name: 'F_INI_CONTRATO'},
					{name: 'F_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION_EPO'},
					{name: 'CAMPO1'},
					{name: 'CAMPO2'},
					{name: 'CAMPO3'},
					{name: 'CAMPO4'},
					{name: 'CAMPO5'},
					{name: 'VENTANILLA_DE_PAGO'},
					{name: 'SUPERVISOR'},
					{name: 'TELEFONO'},
					{name: 'OBJETO_CONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'MONTO_CREDITO'},
					{name: 'REFERENCIA'},
					{name: 'F_VENCIMIENTO_CRE'},
					{name: 'BANCO_DEPOSITO'},
					{name: 'CUENTA'},
					{name: 'CUENTA_CLABE'},
					{name: 'ESTATUS'},
					{name: 'IC_ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'CLAVE_PYME'},	
					{name: 'OBSERVACIONES'},
					{name: 'FIRMA_CONTRATO'}
					
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});			
	
 var causasRechazoNotificacion = new Ext.form.TextField({
		name: 'causasRechazo',
		id:'txtCausasRechazo',
		fieldLabel: '',
		allowBlank: true,
		maxLength: 100,
		width: 80,
		msgTarget: 'side',
		margins: '0 20 0 0'//Necesario para mostrar el icono de error
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	 
		id: 'gridConsulta',
		store: consultaData,		
		margins: '20 0 0 0',
		title: 'Consulta Notificaciones',
		columns: [	
			{
				header: 'Intermediario Financiero (Cesionario)', 
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'NOMBRE_CESINARIO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},			
			{
				header: 'Pyme(Cedente)', 
				tooltip: 'Pyme(Cedente)',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'RFC', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'No.Proveedor', 
				tooltip: 'No.Proveedor',
				dataIndex: 'NO_PROVEEDOR',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Fecha de Solicitud PyME', 
				tooltip: 'Fecha de Solicitud PyME',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'No.Contrato', 
				tooltip: 'No.Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda', 
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tipo de Contrataci�n', 
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'F. Inicio Contrato', 
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'F_INI_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,
				
				align: 'center'
			},
			{
				header: 'F. Final Contrato', 
				tooltip: 'F. Final Contrato',
				dataIndex: 'F_FIN_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Plazo del Contrato', 
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Clasificaci�n EPO', 
				tooltip: 'Clasificaci�n EPO',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO1', 
				tooltip: 'CAMPO1',
				dataIndex: 'CAMPO1',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO2', 
				tooltip: 'CAMPO2',
				dataIndex: 'CAMPO2',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO3', 
				tooltip: 'CAMPO3',
				dataIndex: 'CAMPO3',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO4', 
				tooltip: 'CAMPO4',
				dataIndex: 'CAMPO4',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO5', 
				tooltip: 'CAMPO5',
				dataIndex: 'CAMPO5',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'Ventanilla de Pago', 
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENTANILLA_DE_PAGO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra', 
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUPERVISOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tel�fono', 
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},				
			{
				header: 'Objeto del Contrato', 
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Comentarios', 
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},			
			
			{
				header: 'Monto del Cr�dito', 
				tooltip: 'Monto del Cr�dito',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Referencia /No. Cr�dito', 
				tooltip: 'Referencia /No. Cr�dito',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Fecha Vencimiento Cr�dito', 
				tooltip: 'Fecha Vencimiento Cr�dito',
				dataIndex: 'F_VENCIMIENTO_CRE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Banco de Dep�sito', 
				tooltip: 'Banco de Dep�sito',
				dataIndex: 'BANCO_DEPOSITO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Cuenta', 
				tooltip: 'Cuenta',
				dataIndex: 'CUENTA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Cuenta CLABE', 
				tooltip: 'Cuenta CLABE',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Estatus', 
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Observaciones', 
				tooltip: 'Observaciones',
				dataIndex: 'OBSERVACIONES',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left',
				hidden: true,
				editor: causasRechazoNotificacion,
				renderer: NE.util.colorCampoEdit
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 943,		
		frame: true
		/* bbar: {			
			items: [
			'-',
			'->',
				{
					xtype: 'button',
					text: 'Rechazar',
					id: 'btnRechazar',
					handler:procesoContinuar
				},			
				{
					xtype: 'button',
					text: 'Redireccionar',
					id: 'btnContinuar',
					handler:procesoContinuar
				},
				'-',
				{
					xtype: 'button',
					text: 'Cancelar',
					id: 'btnCancelar',
					handler: function(boton, evento) {
						window.location = '34NotificacionesEpoVentanillaExt.jsp';
					}
				}
			
			]
		}
		*/
	});
	
			
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		items: [
			
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),
			fpEnvio,
			NE.util.getEspaciador(20),
			fpBotones
		]
	});
	
	
	consultaData.load({  	
		params: {  
			informacion: 'ConsultaPreAcuse', 	
			clave_solicitud:clave_solicitud, 			
			pantalla:'PreAcuse',
			tipoOperacion:tipoOperacion	
		}  	
	});	
	

	
	
});