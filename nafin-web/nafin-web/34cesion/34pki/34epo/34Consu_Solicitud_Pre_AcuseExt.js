Ext.onReady(function(){
	 function replaceAll( text, busca, reemplaza ){ 

  while (text.toString().indexOf(busca) != -1) 

       text = text.toString().replace(busca,reemplaza); 
	
   return text; 

 } 
 
	var clave_solicitud  = Ext.getDom('clave_solicitud').value;
	var tipoContratacion  = Ext.getDom('tipoContratacion').value;
	var estatusAnterior  = Ext.getDom('estatusAnterior').value;
	var noEpo  = Ext.getDom('noEpo').value;
	var cambiaEstatus  = Ext.getDom('cambiaEstatus').value;
	
	var procesarSuccessFailureCancelar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
				if(jsondeAcuse.cancelaPreacuse=='S') {						
					document.location.href = "34Consu_Solic_ConsentimientoExt.jsp";		
				} 				
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var cancelar = function() 	{	
		Ext.Ajax.request({
			url : '34Consu_Solic_ConsentimientoExt.data.jsp',
			params : {
				informacion: 'CancelarPreAcuse',
				estatusAnterior: estatusAnterior,
				clave_solicitud:clave_solicitud
			},
			callback: procesarSuccessFailureCancelar
		});	
	}
	
	
	var procesarSuccessFailureAcuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);
			if (jsondeAcuse != null){
				if(jsondeAcuse.acuse!='') {									
					document.location.href = "34Consu_Solicitud_AcuseExt.jsp?clave_solicitud="+clave_solicitud+"&acuse="+jsondeAcuse.acuse+"&tipoContratacion="+tipoContratacion;		
				} 				
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarTransmitir = function() 	{
		
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		var cauza_rechazo ='';
		var textoFirmar  ='';
		var estatusAcuse ='';
		store.each(function(record) {			 
			textoFirmar = record.data['TEXTO_FIRMAR']; 	
			estatusAcuse = record.data['ESTATUS_ACUSE']; 	
		});
		NE.util.obtenerPKCS7(transmitir, textoFirmar, estatusAcuse);

		
	}
	
	var transmitir = function(pkcs7, textoFirmar, estatusAcuse) {
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnEmitir").enable();
			Ext.getCmp("btnReactivar").enable();
			return;	//Error en la firma. Termina...
		}else {
		
			Ext.getCmp("btnEmitir").disable();
			Ext.getCmp("btnReactivar").disable();
			
			Ext.Ajax.request({
				url : '34Consu_Solic_ConsentimientoExt.data.jsp',
				params : {
					pkcs7: pkcs7,
					textoFirmado: textoFirmar,
					informacion: 'Autorizar',
					estatusAcuse: estatusAcuse,
					textoFirmar:textoFirmar,
					clave_solicitud:clave_solicitud
				},
				callback: procesarSuccessFailureAcuse
			});
		}
	}
	
	//Contenedor para el PreAcuse
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',		
		layoutConfig: {
			columns: 20
		},
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [	
		{
			xtype: 'button',
			text: 'Emitir Consentimiento',
			id: 'btnEmitir',
			iconCls: 'icoAceptar',
			handler: procesarTransmitir		
		},	
		{
			xtype: 'button',
			text: 'Reactivar Solicitud',
			id: 'btnReactivar',
			iconCls: 'icoAceptar',
			handler: procesarTransmitir		
		},	
		{
			xtype: 'button',
			text: 'Cancelar',
			iconCls: 'icoLimpiar',
			handler: cancelar
		}			
	]
	});
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		//fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}			
			var registro=store.getAt(0);
			
			var numero_contrato = registro.get('NOCONTRATO');
			var montospormoneda = registro.get('MONTO_MONEDA');
			var objeto_contrato = registro.get('OBCONTRATO');
			var plazocontrato = registro.get('PLAZO_CONTRATO');
			var nombreEpo = registro.get('NOMBREEPO');
			var fechasolicitud = registro.get('FECHASOLICITUD');
			var nombre_if = registro.get('CECIONARIO');
			
			var jsonData = store.reader.jsonData;	
			
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  clasificacionEpo = jsonData.clasificacionEpo;
			var nombrePymeCedenteFila = jsonData.nombrePymeCedenteFila;	//FODEA-024-2014 MOD()
			var nombrePymeCedenteLista = jsonData.nombrePymeCedenteLista;	//FODEA-024-2014 MOD()
			var strNombreUsuario = jsonData.strNombreUsuario;	
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var table ="";
			 table = ['<table width="600" align="center">'+		
					'<tr>'+
					'<td class="formas" style="text-align: justify;">'+nombrePymeCedenteLista+
					'<br/>'+
					'<p>Presente</p>'+
					'<br/>'+
					'<u>Consentimiento para Ceder los Derechos de Cobro respecto del Contrato No. '+numero_contrato +'</u> '+
					'<br/>'+
					'<br/>'+
					'<br/>'+
					 nombrePymeCedenteFila+
					'<br/>'+
					'Objeto: '+objeto_contrato+
					'<br/>'+
					'Monto: '+ replaceAll(montospormoneda,'<br/>',' ') +
					'<br/>'+
					'Plazo: '+ plazocontrato +
					'<br/>'+
					'Fecha de Solicitud: '+fechasolicitud+
					'<br/>'+
					'<br width="80%">   </br>'+
//					'<p>Es importante se�alar que el Transitorio Octavo, Apartado A, fracci�n VII de la Ley de Petr�leos Mexicanos publicada en el Diario Oficial de la Federaci�n el 11 de agosto de 2014, establece que las nuevas Empresas Productivas Subsidiarias se subrogan en todos los derechos y obligaciones de los Organismos Subsidiarios, seg�n corresponda, anteriores y posteriores a la fecha de entrada en vigor de los Acuerdos de Creaci�n que al efecto se expidan, por lo que una vez que entraron en vigor dichos Acuerdos, las Empresas Productivas Subsidiarias se subrogaron en los derechos y obligaciones del Organismos Subsidiario que formaliz� el contrato, incluyendo los convenios modificatorios que de �l deriven.</br></br></p>'+
					'<p>En atenci�n a su solicitud, se le informa que con base en la informaci�n recabada al interior de �sta Empresa Productiva Subsidiaria '+ nombreEpo +', se otorga el consentimiento para ceder los derechos de cobro respecto del Contrato de antecedentes,con fundamento en:  </br>'+
					'<br width="80%">   </br>'+
					' <p> El Art�culo 46 de la Ley de Adquisiciones, Arrendamientos y Servicios del Sector P�blico. '+
					' El Art�culo 47 de la Ley de Obras P�blicas y Servicios Relacionados con las Mismas. '+
					' Los Art�culos 53, fracci�n XVII, de la Ley de Petr�leos Mexicanos, 48 de su Reglamento, y 54 de las Disposiciones'+
					'administrativas de contrataci�n en materia de adquisiciones, arrendamientos, obras y servicios de las actividades sustantivas de car�cter productivo de Petr�leos Mexicanos y Organismos Subsidiarios'+
					' El Art�culo 36 de las Disposiciones Generales de Contrataci�n para Petr�leos Mexicanos y sus Empresas Producctivas Subsidiarias, seg�n corresponda, as� como en la Cl�usulas relativas a la cesi�n de derechos Cobro del Contrato. </br>'+
					
					'<p><br>Es importante se�alar que el Transitorio Octavo, Apartado A, fracci�n VII de la Ley de Petr�leos Mexicanos publicada en el Diario Oficial de la Federaci�n el 11 de agosto de 2014, establece que las nuevas Empresas Productivas Subsidiarias se subrogan en todos los derechos y obligaciones de los Organismos Subsidiarios, seg�n corresponda, anteriores y posteriores a la fecha de entrada en vigor de los Acuerdos de Creaci�n que al efecto se expidan, por lo que una vez que entraron en vigor dichos Acuerdos, las Empresas Productivas Subsidiarias se subrogaron en los derechos y obligaciones del Organismos Subsidiario que formaliz� el contrato, incluyendo los convenios modificatorios que de �l deriven.</br></br></p>'+
							
					'<br width="80%">   </br>'+
					'<p>  <br>No obstante lo anterior, el Consentimiento estar� Condicionado a que: '+
					'<br width="80%">   </br>'+
					
					'1.- '+nombrePymeCedenteFila+', en lo sucesivo la CEDENTE, deber� celebrar con el INTERMEDIARIO FINANCIERO '+nombre_if+'  en su car�cter de Cesionario un  Contrato de Cesi�n Electr�nica de Derechos de Cobro, mismo que deber� ser firmado electr�nicamente por el o los representantes legales o apoderados de la CEDENTE que cuente(n) con facultades generales o especiales para actos de dominio y que dicho instrumento '+
					' contractual sea notificado a la Empresa a trav�s del sistema NAFIN para que surta efectos  legales,'+
					' en t�rminos de lo establecido en el art�culo 390 del C�digo de Comercio, en el entendido que de no efectuarse la mencionada notificaci�n dentro de los 30 (treinta) d�as h�biles siguientes a la fecha en que sea notificada en el sistema NAFIN el presente Consentimiento,'+
					' en el entendido que de no efectuarse la mencionada notificaci�n dentro del plazo se�alado, el consentimiento de que se trata quedar� sin efectos sin necesidad de que la Empresa le comunique dicha situaci�n, quedando bajo su responsabilidad que dicha notificaci�n se realice en los t�rminos indicados. '+
					'<br width="80%">   </br>'+
					
					'2.- La Cesi�n de Derechos de Cobro ser� por el 100% (cien por ciento) de los derechos de cobro del Contrato se�alado y la cesi�n surtir� sus efectos apartir de la aceptaci�n de la notificaci�n del correspondiente Contrato de Cesi�n Electr�nica de Derechos de Cobro de la Empresa por medio del Sistema NAFIN. '+
					'<br width="80%">   </br>'+
					'3.- La Cesi�n de Derechos de Cobro incluir� los incrementos al monto del contrato arriba se�alado que, en su caso, se pacten con posterioridad mediante convenios modificatorios; y que la Cesi�n de Derechos de Cobro �nica y exclusivamente proceder� con respecto de las facturas pendientes de cobro por los trabajos/servicios ejecutados, aceptados e instruidos por la Empresa, '+
					' en t�rminos de los pactado en el Contrato.'+
					'<br width="80%">   </br>'+
					'4.- Que el Cedente, bajo protesta de decir verdad, garantiza que los derechos de cobro objeto de cesi�n, no han sido dados en garant�a, pignorados, embargados, ni gravados en forma alguna y que no existe controversia sobre los mismos. '+
					'<br width="80%">   </br>'+
					' 5.- Que el Cedente y el Cesionario reconocen que el Cedente es el �nico responsable de garantizar que los derechos de cobro est�n libres de gravamen o disputa.'+
					'<br width="80%">   </br>'+
					' 6.- En caso de embargo es obligaci�n exclusiva del Cedente, notificar de forma inmediata al Cesionario de cualquier acci�n de cobro, embargo o cualquier gravamen.'+
					'<br width="80%">   </br>'+
					' 7.- El Cedente queda obligado a sacar en paz y a salvo a la Empresa de cualquier controversia relacionada con los grav�menes o las disputas que se originen por los derechos de cobro objeto de cesi�n. Lo anterior, sin menoscabo de las acciones que correspondan al Cesionario en contra del Cedente.'+
					'<br width="80%">   </br>'+
					' 8.- Para el caso de que la Empresa, con posterioridad a la cesi�n de derechos de cobro, llegare a efectuar cualquier pago al Cedente en lugar del Cesionario respecto de dichos derechos de cobro, el Cedente quedar� constituido en depositario de dicho pago, para todos los efectos a que haya lugar y se obliga a restituirlo, en forma inmediata al Cesionario, sin necesidad de que medie aviso o requerimiento para ello.'+
					'<br width="80%">   </br>'+
					' 9.- Una vez cedidos los derechos de cobro, en caso de que la Empresa sea requerida por autoridad judicial o administrativa para retener o exhibir cualquier cantidad relacionada con los derechos de cobro objeto de la presente cesi�n, la Empresa, informar� a la autoridad emisora que los derechos de cobro derivados del Contrato se encuentran cedidos a favor del Cesionario; y en caso de que dicha autoridad confirme '+
					' a la Empresa que �sta retenga y/o exhiba cualquier cantidad relacionada con los derechos de cobro objeto de la presente cesi�n, la Empresa comunicar� dicha situaci�n al Cedente y al Cesionario, para que hagan valer lo que a su derecho convenga ante dicha autoridad.'+
					'<br width="80%">   </br>'+
					' El Cedente y Cesionario renuncian en este acto y en los t�rminos m�s amplios que en derecho proceda, al ejercicio de cualquier acci�n que les asista o que les pudiera corresponder en contra de la Empresa, derivado del cumplimiento que �ste realice a cualquier requerimiento formulado o instado por las autoridades.'+
					'<br width="80%">   </br>'+
					' 10.- El Cesionario acepta y est� de acuerdo en que el pago de los derechos de cobro estar� sujeto a que, en su caso, la Empresa realice las deducciones y/o retenciones que deban hacerse al cedente por adeudos con la misma, penas convencionales, pagos en exceso; cualquier requerimiento de pago derivado de laudo firme ordenado por las Juntas Locales o Federales de Conciliaci�n y Arbitraje, '+
					' con motivo de los juicios laborales instaurados en contra de la Empresa en relaci�n con el Cedente; o cualquier otra deducci�n derivada del Contrato o de la ley de la materia en relaci�n �nicamente con el mismo, en cuyos supuestos se har�n las deducciones y/o retenciones correspondientes. En tal virtud, la Empresa quedar� liberada de cualquier responsabilidad por las deducciones y/o retenciones a que haya lugar. '+
					'<br width="80%">   </br>'+
					' 11.- El Cedente y Cesionario reconocen y est�n de acuerdo en que el consentimiento otorgado por la Empresa para ceder los derechos de cobro del Contrato que nos ocupa, incluidos los incrementos al monto original del mismo no constituye por su parte una garant�a de pago ni reconocimiento de cualquier derecho del Cesionario frente al Cedente, siendo la �nica obligaci�n de la Empresa, la de pagar cantidades '+
					' l�quidas que sean exigibles conforme al Contrato, una vez hechas las deducciones y/o retenciones antes mencionadas.'+
					'<br width="80%">   </br>'+
					' 12.- Se deber� considerar la clave del Registro Federal de Contribuyentes (RFC) del Cesionario y los datos de la Cuenta �nica previamente registrada la Empresa en que se realizar�n los pagos correspondientes.'+
					'<br width="80%">   </br>'+
					' 13.- Que para dejar sin efectos la cesi�n de los derechos de cobro previo a la extinci�n natural de dicha cesi�n, ser� necesario que el Cesionario as� lo comunique a la Empresa en los mismos t�rminos previstos para la notificaci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro para que surta los efectos legales que le correspondan. '+
					'<br width="80%">   </br>'+
					' A la notificaci�n que se practique a la Empresa mediante el Sistema NAFIN, NAFIN deber� anexar la siguiente documentaci�n: <br/>'+
					'<br width="80%">   </br>'+
					' i) un ejemplar del Contrato de Cesi�n Electr�nica de Derechos de Cobro con las firmas electr�nicas de los representantes de la Cedente y del Cesionario.'+
					'<br width="80%">   </br>'+
					' ii) copia certificada por fedatario p�blico de la documentaci�n con la que se acredite la personalidad del o de los representantes legales o apoderados del Cedente con las facultades generales o especiales para actos de dominio, y '+
					'<br width="80%">   </br>'+
					' iii) copia certificada por fedatario p�blico de la documentaci�n con la que se acredite la personalidad del o de los representantes legales o apoderados del Cesionario con las facultades suficientes para la celebraci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro en cuesti�n.'+
					'<br width="80%">   </br>'+
					' En el caso de que no se anexe a la notificaci�n la documentaci�n se�alada en los numerales anteriores, la misma no se considerar� recibida por la Empresa y por lo tanto no surtir� efectos la cesi�n de derechos de cobro, considerando adem�s de que el t�rmino de 30 (treinta) d�as h�biles para realizar la notificaci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro no se interrumpir�.'+
					'<br width="80%">   </br>'+
					' El presente consentimiento, quedar� sin efecto en caso de que la Empresa antes de la fecha de notificaci�n a que se refiere el punto n�mero \'1\' del presente documento, reciba alguna notificaci�n de autoridad judicial o administrativa que incida sobre el Contrato cuyos derechos de cobro ser�n materia de cesi�n. Bajo este supuesto, la Empresa lo informar� inmediatamente  '+
					' a trav�s del Sistema  NAFIN al Cedente o al Cesionario, una vez que haya recibido formalmente la notificaci�n de que se trate. '+
					'<br width="80%">   </br>'+
					'<p>'+strNombreUsuario+
					'</td>'+
					'</tr>'+	
					'</table>'];			
	
			if(tipoContratacion!='') {
				//panel para mostrar  el texto 
				var fpMensaje = new Ext.form.FormPanel({		
					id: 'fpMensaje',
					width: 620,	
					align: 'center',					
					bodyStyle: 'padding: 6px',	
					style: 'margin:0 auto;',
					defaultType: 'textfield',		
					html: table.join('')		
				});	
				contenedorPrincipalCmp.add(fpMensaje);
			}
			
			contenedorPrincipalCmp.add(NE.util.getEspaciador(20));				
			contenedorPrincipalCmp.add(fpBotones);			
			contenedorPrincipalCmp.doLayout();
			
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			
			if(clasificacionEpo ==''){				
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION'),clasificacionEpo);
			}
			
			if(hayCamposAdicionales=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='2'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='3'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='4'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='5'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.campo05);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
			}
							
			var el = gridConsulta.getGridEl();						
			if(store.getTotalCount() > 0) {			
					
				el.unmask();
			} else {	
					
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'ConsultarRechazo'
		},
			fields: [
					{name: 'CECIONARIO'},
					{name: 'PYME'},
					{name: 'RFC'},
					{name: 'CLAVE_PYME'},
					{name: 'REPRESENTANTE'},
					{name: 'FSOLICITUDINI'},
					{name: 'NOCONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'CONTRATACION'},
					{name: 'FECHA_INICIO_CONTRATO'},
					{name: 'FECHA_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION'},
					{name: 'CAMPO1'},
					{name: 'CAMPO2'},
					{name: 'CAMPO3'},
					{name: 'CAMPO4'},
					{name: 'CAMPO5'},
					{name: 'VENANILLA_PAGO'},
					{name: 'SUP_ADM_RESOB'},
					{name: 'NUMERO_TELEFONO'},
					{name: 'OBCONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'FACEPTACION'},
					{name: 'ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'TIPOCONTRATACION'},
					{name: 'NOESTATUS'},
					{name: 'CAUSA_RECHAZO'},
					{name: 'TEXTO_FIRMAR'},
					{name: 'ESTATUS_ACUSE'},
					{name: 'NOMBREEPO'},
					{name: 'FECHASOLICITUD'},
					{name: 'FIRMA_CONTRATO'}
					
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
		

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		store: consultaData,			
		title: 'Pre Acuse  Solicitud de Consentimiento',
		columns: [		
			{
				header: 'Intermediario Financiero (Cesionario)', 
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'CECIONARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Pyme (Cedente)', 
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'RFC ', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'No Proveedor ', 
				tooltip: 'No Proveedor',
				dataIndex: 'CLAVE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Representante legal ',  // usa un metodo pra obtenerlo
				tooltip: 'Representante legal',
				dataIndex: 'REPRESENTANTE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Fecha de solicitud Pyme', 
				tooltip: 'Fecha de solicitud Pyme ',
				dataIndex: 'FSOLICITUDINI',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},	
			{
				header: 'No. Contrato', 
				tooltip: 'No. Contrato',
				dataIndex: 'NOCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda',  // este usa  metodo para obtenerlo
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tipo de Contrataci�n',  
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'CONTRATACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'F. Inicio Contrato',  
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'						
			},
			{
				header: 'F. Final Contrato',  
				tooltip: 'F. Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},			
			{
				header: 'Plazo del Contrato',  
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Ciudad de Firma del Contrato',  
				tooltip: 'Ciudad de Firma del Contrato',
				dataIndex: 'CLASIFICACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			//aqui van los campos adicionales
			{
				header: 'Campo 1',  
				tooltip: 'Campo 1',
				dataIndex: 'CAMPO1',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 2',  
				tooltip: 'Campo 2',
				dataIndex: 'CAMPO2',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 3',  
				tooltip: 'Campo 3',
				dataIndex: 'CAMPO3',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 4',  
				tooltip: 'Campo 4',
				dataIndex: 'CAMPO4',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 5',  
				tooltip: 'Campo 5',
				dataIndex: 'CAMPO5',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Ventanilla de Pago',  
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENANILLA_PAGO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra',  
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUP_ADM_RESOB',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tel�fono',  
				tooltip: 'Tel�fono',
				dataIndex: 'NUMERO_TELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Objeto del Contrato',  
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Comentarios',  
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},					
			{
				header: 'Fecha de Aceptacion o Rechazo',  
				tooltip: 'Fecha de Aceptacion o Rechazo',
				dataIndex: 'FACEPTACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Estatus Actual',  
				tooltip: 'Estatus Actual',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				 
			}		
			
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 943,		
		frame: true,
		margins: '20 0 0 0'
	});

	
	
	
//--------------------------------PRINCIPAL-------------------------------------
var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		items: [
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});

	//para ejecutar la consulta del Preacuse 
	if(clave_solicitud!='') {
		
		consultaData.load({
			params: {
				informacion: 'ConsultarPreAcuse',
				clave_solicitud: clave_solicitud,
				tipoContratacion :tipoContratacion,
				estatusAnterior:estatusAnterior,
				cambiaEstatus:cambiaEstatus,
				noEpo:noEpo
			}	
		});
		
		var btnEmitir = Ext.getCmp('btnEmitir');
		var btnReactivar = Ext.getCmp('btnReactivar');
					
		if (estatusAnterior ==8) {
			btnEmitir.hide();
			btnReactivar.show ();
		}else {
			btnEmitir.show();
			btnReactivar.hide ();
		}
		
	}
	
});