<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.sql.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.cesion.*,
		com.netro.cesion.ConsulSolicitudConsentimiento,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"    
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<% 
String informacion	= (request.getParameter("informacion"))!=null?request.getParameter("informacion"):"";
String infoRegresar	= "";
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
int start				= 0;
int limit 				= 0;
if(informacion.equals("valoresIniciales")){
		String clasificacionEpo = cesionBean.clasificacionEpo(iNoCliente);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("strDirectorioPublicacion",strDirectorioPublicacion);
		jsonObj.put("clasificacionEpo",clasificacionEpo);
		infoRegresar = jsonObj.toString();
}
else if(informacion.equals("ConsultaPrincipal")){
		List registros = cesionBean.monitorCesionDerechos(strTipoUsuario, iNoCliente);
		
		String  regestatus = "";
		String  estatus = "";
		String  numero = "";					
		int posicion = 0, longitud = 0;
		int solicitar = 0;
		int enproceso = 0;
		int aceptadas = 0; 
		int rechazadas= 0; 
		int vencida = 0;
		int reactivada = 0;  
		int aceptadopyme = 0; 
		int aceptadoIF = 0; 
		int rechazadoIF = 0;
		int notiAceptada = 0;
		int notiRechazada = 0;
		int totalSolicitudes = 0;
		int totalContrato = 0;
		int totalNotificaciones = 0;
		int solicitudes = 0;
		int pendNotifVent = 0;
		int acepNotificada= 0;
		int acepNotifVent = 0;
		int redirAplicado = 0;
		int extincionSolicitada = 0;
		int extincionAceptadaIf = 0;
		int extincionAceptada	= 0;
		int extincionAceptadaNotificada = 0;
		int extincionRedirecAplic = 0;
		int totalExtinciones = 0;
		int canceladaNafin = 0;
		int rechazadaExpirada = 0;
		
		for (int i = 0; i < registros.size(); i++) {
			regestatus = ((registros.get(i))==null)?"":(registros.get(i).toString());
			longitud = regestatus.length();
			posicion =regestatus.indexOf("e");
			estatus = regestatus.substring(0, posicion);
			numero = regestatus.substring(posicion + 1, longitud);

			if (estatus.equals("2") ) {
				enproceso = (Integer.parseInt(numero));								
			} else if (estatus.equals("3")) {
				aceptadas = (Integer.parseInt(numero));
			}	else if (estatus.equals("5")) {
				rechazadas = (Integer.parseInt(numero));
			}	else if (estatus.equals("8")) {
				vencida = (Integer.parseInt(numero));
			}	else if (estatus.equals("9")) {
				aceptadoIF = (Integer.parseInt(numero));
			}	else if (estatus.equals("10")) {
				rechazadoIF = (Integer.parseInt(numero));
			}	else if (estatus.equals("11")) {
				notiAceptada = (Integer.parseInt(numero));
			}	else if (estatus.equals("12")) {
				notiRechazada = (Integer.parseInt(numero));
			}	else if (estatus.equals("15")) {
				pendNotifVent = (Integer.parseInt(numero));
			}	else if (estatus.equals("16")) {
				acepNotifVent = (Integer.parseInt(numero));
			}	else if (estatus.equals("17")) {
				redirAplicado = (Integer.parseInt(numero));
			} else if (estatus.equals("19")) {
				extincionSolicitada = (Integer.parseInt(numero));
			} else if (estatus.equals("20")) {
				extincionAceptadaIf = (Integer.parseInt(numero));
			} else if (estatus.equals("21")) {
				extincionAceptadaNotificada = (Integer.parseInt(numero));
			} else if (estatus.equals("22")) {
				extincionRedirecAplic = (Integer.parseInt(numero));
			} else if (estatus.equals("23")){
				extincionAceptada = (Integer.parseInt(numero));
			}	else if (estatus.equals("24")) {	 //Cancelada por  Nafin  F023-2015
				canceladaNafin = (Integer.parseInt(numero));
			}	else if (estatus.equals("25")) {	 //Rechazada Expirada  F023-2015
				rechazadaExpirada = (Integer.parseInt(numero));			 
			}
		}
		totalSolicitudes = solicitar+ enproceso + aceptadas + rechazadas + vencida;
		totalContrato =  aceptadas + aceptadopyme + aceptadoIF +rechazadoIF;
		totalNotificaciones = aceptadoIF + notiAceptada + notiRechazada + pendNotifVent + acepNotifVent + redirAplicado;
		solicitudes = aceptadas + rechazadas + vencida;
		totalExtinciones = extincionSolicitada + extincionAceptadaIf + extincionAceptada + extincionAceptadaNotificada + extincionRedirecAplic;
		
		JSONObject jsonObj0 = new JSONObject();
			jsonObj0.put("Descripcion","SOLICITUDES DE CONSENTIMIENTO");
			jsonObj0.put("Total", new Integer(totalSolicitudes));
		JSONObject jsonObj1 = new JSONObject();
			jsonObj1.put("Descripcion", "EN PROCESO");
			jsonObj1.put("Total", new Integer(enproceso)); 
		JSONObject jsonObj2 = new JSONObject();
			jsonObj2.put("Descripcion","TOTAL SOLICITUDES");
			jsonObj2.put("Total", new Integer(solicitudes));
		JSONObject jsonObj3 = new JSONObject();
			jsonObj3.put("Descripcion", "ACEPTADA");
			jsonObj3.put("Total", new Integer(aceptadas));
		JSONObject jsonObj4 = new JSONObject();
			jsonObj4.put("Descripcion", "RECHAZADAS");
			jsonObj4.put("Total", new Integer(rechazadas));
		JSONObject jsonObj5 = new JSONObject();
			jsonObj5.put("Descripcion", "VENCIDAS");
			jsonObj5.put("Total", new Integer(vencida));
		
		JSONObject jsonObj19 = new JSONObject(); //F023-2015
		jsonObj19.put("Descripcion", "CANCELADA POR NAFIN");
		jsonObj19.put("Total", new Integer(canceladaNafin));
			
		JSONObject jsonObj6 = new JSONObject();
			jsonObj6.put("Descripcion","NOTIFICACIONES"); 
			jsonObj6.put("Total", new Integer(totalNotificaciones));
		JSONObject jsonObj7 = new JSONObject();
			jsonObj7.put("Descripcion","ACEPTADOS IF");
			jsonObj7.put("Total", new Integer(aceptadoIF));
		JSONObject jsonObj8 = new JSONObject();
			jsonObj8.put("Descripcion","NO ACEPTADO POR VENTANILLA");
			jsonObj8.put("Total", new Integer(pendNotifVent));
		JSONObject jsonObj9 = new JSONObject();
			jsonObj9.put("Descripcion","NOTIFICACIONES ACEPTADAS");
			jsonObj9.put("Total", new Integer(notiAceptada));
		JSONObject jsonObj10 = new JSONObject();
			jsonObj10.put("Descripcion","NOTIFICADO VENTANILLA");
			jsonObj10.put("Total", new Integer(acepNotifVent));
		JSONObject jsonObj11 = new JSONObject();
			jsonObj11.put("Descripcion","RECHAZADAS");
			jsonObj11.put("Total", new Integer(notiRechazada));
		JSONObject jsonObj12 = new JSONObject();
			jsonObj12.put("Descripcion","REDIRECCIONAMIENTO APLICADO");
			jsonObj12.put("Total", new Integer(redirAplicado));
		JSONObject jsonObj20 = new JSONObject();   //F023-2015
		jsonObj20.put("Descripcion","RECHAZADA EXPIRADA");
		jsonObj20.put("Total", new Integer(rechazadaExpirada));
		
			
		JSONObject jsonObj13 = new JSONObject();		
			jsonObj13.put("Descripcion","EXTINCIÓN DE CONTRATO");
			jsonObj13.put("Total", new Integer(totalExtinciones));
		JSONObject jsonObj14 = new JSONObject();
			jsonObj14.put("Descripcion","SOLICITADA");
			jsonObj14.put("Total", new Integer(extincionSolicitada));
		JSONObject jsonObj15 = new JSONObject();
			jsonObj15.put("Descripcion","ACEPTADA IF");
			jsonObj15.put("Total", new Integer(extincionAceptadaIf));
		JSONObject jsonObj16 = new JSONObject();
			jsonObj16.put("Descripcion","NOTIFICACIONES ACEPTADAS");
			jsonObj16.put("Total", new Integer(extincionAceptada));
		JSONObject jsonObj17 = new JSONObject();
			jsonObj17.put("Descripcion","NOTIFICADO VENTANILLA");
			jsonObj17.put("Total", new Integer(extincionAceptadaNotificada));
		JSONObject jsonObj18 = new JSONObject();
			jsonObj18.put("Descripcion","REDIRECCIONAMIENTO APLICADO");
			jsonObj18.put("Total", new Integer(extincionRedirecAplic));
		
		JSONArray jsonArray = new JSONArray();
		jsonArray.add(jsonObj0);
		jsonArray.add(jsonObj1);
		jsonArray.add(jsonObj2);
		jsonArray.add(jsonObj3);
		jsonArray.add(jsonObj4);
		jsonArray.add(jsonObj5);
		jsonArray.add(jsonObj19);// F023-2015
		jsonArray.add(jsonObj6);
		jsonArray.add(jsonObj7);
		jsonArray.add(jsonObj8);
		jsonArray.add(jsonObj9);
		jsonArray.add(jsonObj10);
		jsonArray.add(jsonObj11);
		jsonArray.add(jsonObj12);
		jsonArray.add(jsonObj20);// F023-2015	
		jsonArray.add(jsonObj13);
		jsonArray.add(jsonObj14);
		jsonArray.add(jsonObj15);
		jsonArray.add(jsonObj16);
		jsonArray.add(jsonObj17);
		jsonArray.add(jsonObj18);

		infoRegresar =	"{\"success\": true, \"total\":" + jsonArray.size() + ", \"registros\": " + jsonArray.toString()+"}";

	}else if(informacion.equals("Consulta")||informacion.equals("ArchivoTotalPDF")){
		
		JSONObject jsonObj = new JSONObject();
		String tipo = (request.getParameter("tipo")==null)?"":request.getParameter("tipo");
		ConsulSolicitudConsentimiento paginador = new ConsulSolicitudConsentimiento();
		paginador.setClaveEpo(iNoCliente);
		paginador.setClaveEstatusSolicitud(tipo);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if(informacion.equals("Consulta")){
			String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			}catch(Exception e){
				throw new AppException("Error en los parámetros recibidos", e);
			}
			try{
				if(operacion.equals("Generar")){
					queryHelper.executePKQuery(request);
				}
				Registros registros = queryHelper.getPageResultSet(request,start,limit);
				while(registros.next()){
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(registros.getString("CLAVE_SOLICITUD"));
					registros.setObject("MONTO_MONEDA",montosMonedaSol.toString());
					String  cedente = registros.getString("NOMBRE_CEDENTE");
					registros.setObject("NOMBRE_CEDENTE",cedente.replaceAll(";", "<br>"));
				}
				
			String consulta =	"{\"success\": true, \"total\": \""+ registros.getNumeroRegistros() +"\", \"registros\": " + registros.getJSONData()+"}";
			
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("tipo",tipo);		
			infoRegresar = jsonObj.toString();  
			}catch(Exception e){
				throw new AppException("Error en la paginación",e);
			}
		}
	}
%>
<%=infoRegresar%>
