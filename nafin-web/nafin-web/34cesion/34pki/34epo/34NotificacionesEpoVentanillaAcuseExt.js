Ext.onReady(function(){
	
	var clave_solicitud =  Ext.getDom('clave_solicitud').value;
	var pantalla =  Ext.getDom('pantalla').value;
	var acuse =  Ext.getDom('acuse').value;
	var fecha =  Ext.getDom('fecha').value;
	var hora =  Ext.getDom('hora').value;
	var nombreUsuario =  Ext.getDom('nombreUsuario').value;	
	var tipoOperacion =  Ext.getDom('tipoOperacion').value;
	var autentificacion;
	
	
	if(acuse !='') {
	 autentificacion = '<table width="400" align="center" >'+
		'<tr><td><H1>La autentificaci�n se llev� a cabo con �xito </H1></td></tr>'+
		'<tr><td><H1>Recibo:'+acuse+'</H1></td></tr>'+
		'</table>';	
	}
	
	if(acuse =='') {
	 autentificacion = '<table width="400" align="center" >'+
		'<tr><td><H1>La autentificaci�n no se llev� a cabo. </H1></td></tr>'+
		'<tr><td><H1>PROCESO CANCELADO</H1></td>'+
		'</tr>'+
		'</table>';	
	}
 var mensajeAcuse = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAcuse',							
		width:	'400',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		autentificacion				
			}					
		]
	});
	
	var regresar = new Ext.Container({
		layout: 'table',		
		id: 'regresar',							
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [	
			{
				xtype: 'button',
				text: 'Regresar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34NotificacionesEpoVentanillaExt.jsp';
			}
		}
			
		]
	});
	
	
			//GENERAR ARCHIVO  PDF
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  clasificacionEpo = jsonData.clasificacionEpo;	
						
			var acuseCifras = [
				['N�mero de Acuse', acuse],
				['Fecha de Carga ', fecha],
				['Hora de Carga', hora],
				['Usuario de Captura ', nombreUsuario]
			];
						
			storeCifrasData.loadData(acuseCifras);	
			gridCifrasControl.show();
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			
			
			if(clasificacionEpo !=''){				
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), false);	
			}
			
			if(hayCamposAdicionales=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='2'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='3'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='4'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='5'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.campo05);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
			}					
			if(tipoOperacion =='RECHAZO')  {			
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('OBSERVACIONES'), false);	
			}
			
			
			var el = gridConsulta.getGridEl();						
			if(store.getTotalCount() > 0) {		
				
				el.unmask();
			} else {	
					
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
			//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
 //Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 300,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 550,
		height: 300,
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34NotificacionesEpoVentanilla.data.jsp',
		baseParams: {
			informacion: 'ConsultaPreAcuse'
		},
			fields: [
					{name: 'NOMBRE_CESINARIO'},
					{name: 'NOMBRE_PYME'},
					{name: 'RFC'},
					{name: 'NO_PROVEEDOR'},
					{name: 'FECHA_SOLICITUD'},
					{name: 'NUMERO_CONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'TIPO_CONTRATACION'},
					{name: 'F_INI_CONTRATO'},
					{name: 'F_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION_EPO'},
					{name: 'CAMPO1'},
					{name: 'CAMPO2'},
					{name: 'CAMPO3'},
					{name: 'CAMPO4'},
					{name: 'CAMPO5'},
					{name: 'VENTANILLA_DE_PAGO'},
					{name: 'SUPERVISOR'},
					{name: 'TELEFONO'},
					{name: 'OBJETO_CONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'MONTO_CREDITO'},
					{name: 'REFERENCIA'},
					{name: 'F_VENCIMIENTO_CRE'},
					{name: 'BANCO_DEPOSITO'},
					{name: 'CUENTA'},
					{name: 'CUENTA_CLABE'},
					{name: 'ESTATUS'},
					{name: 'IC_ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'CLAVE_PYME'},
					{name: 'OBSERVACIONES'},
					{name: 'FIRMA_CONTRATO'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});			
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	 
		id: 'gridConsulta',
		store: consultaData,
		//hidden: true,
		style: 'margin:0 auto;',
		title: 'Consulta Notificaciones',
		columns: [			
			{
				header: 'Pyme(Cedente)', 
				tooltip: 'Pyme(Cedente)',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'RFC', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'No.Proveedor', 
				tooltip: 'No.Proveedor',
				dataIndex: 'NO_PROVEEDOR',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Fecha deSolicitudPyME', 
				tooltip: 'Fecha deSolicitudPyME',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'No.Contrato', 
				tooltip: 'No.Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda', 
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tipo de Contrataci�n', 
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'F. Inicio Contrato', 
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'F_INI_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,
				
				align: 'center'
			},
			{
				header: 'F. Final Contrato', 
				tooltip: 'F. Final Contrato',
				dataIndex: 'F_FIN_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Plazo del Contrato', 
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Clasificaci�n EPO', 
				tooltip: 'Clasificaci�n EPO',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO1', 
				tooltip: 'CAMPO1',
				dataIndex: 'CAMPO1',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO2', 
				tooltip: 'CAMPO2',
				dataIndex: 'CAMPO2',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO3', 
				tooltip: 'CAMPO3',
				dataIndex: 'CAMPO3',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO4', 
				tooltip: 'CAMPO4',
				dataIndex: 'CAMPO4',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO5', 
				tooltip: 'CAMPO5',
				dataIndex: 'CAMPO5',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'Ventanilla de Pago', 
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENTANILLA_DE_PAGO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra', 
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUPERVISOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tel�fono', 
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},				
			{
				header: 'Objeto del Contrato', 
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Comentarios', 
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},			
			
			{
				header: 'Monto del Cr�dito', 
				tooltip: 'Monto del Cr�dito',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Referencia /No. Cr�dito', 
				tooltip: 'Referencia /No. Cr�dito',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Fecha Vencimiento Cr�dito', 
				tooltip: 'Fecha Vencimiento Cr�dito',
				dataIndex: 'F_VENCIMIENTO_CRE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Banco de Dep�sito', 
				tooltip: 'Banco de Dep�sito',
				dataIndex: 'BANCO_DEPOSITO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Cuenta', 
				tooltip: 'Cuenta',
				dataIndex: 'CUENTA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Cuenta CLABE', 
				tooltip: 'Cuenta CLABE',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Estatus', 
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Observaciones', 
				tooltip: 'Observaciones',
				dataIndex: 'OBSERVACIONES',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 943,		
		frame: true	
	});
	
		//Contenedor para el PreAcuse
	var fpBotones = new Ext.Container({
		layout: 'table',
		align: 'center',
		style: 'margin:0 auto;',
		id: 'fpBotones',		
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
			{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDF',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
					url: '34NotificacionesEpoVentanilla.data.jsp',
					params: {
						informacion: 'ArchivoAcuse',
						tipoArchivo:'PDF',
						clave_solicitud:clave_solicitud,
						acuse:acuse,
						fecha:fecha,
						hora:hora, 
						nombreUsuario:nombreUsuario,
						tipoOperacion:tipoOperacion
					},
					callback: procesarSuccessFailurePDF
				});
			}
		},
		{
			xtype: 'button',
			text: 'Bajar PDF',
			id: 'btnBajarPDF',
			hidden: true
		},
		{
			xtype: 'button',
			text: 'Salir',
			iconCls: 'icoLimpiar',
			handler: function() {
				window.location = '34NotificacionesEpoVentanillaExt.jsp';
			}
		}			
	]
	});
	
			
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		items: [	
			NE.util.getEspaciador(20),
			mensajeAcuse, 			
			NE.util.getEspaciador(20),
			regresar,
			gridConsulta,
			NE.util.getEspaciador(20),
			gridCifrasControl,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20)
		]
	});

	if(acuse =='') {
		fpBotones.hide();
		gridConsulta.hide();
		fpBotones.hide();	
	} else  if(acuse !='') {	
		regresar.hide();
		consultaData.load({  	
			params: {  
				informacion: 'ConsultaPreAcuse', 	
				clave_solicitud:clave_solicitud, 			
				pantalla:'Acuse'   
			}  	
		});	
	}
	
		
	
	
});