Ext.onReady(function(){
	
	var cadenaCorreo =  'Solicitud de Informacion al Area que Administra y Supervisa la ejecucion del Contrato \n\n'+
	'Agradeceremos se sirva informar a esta Gerencia el estado que guarda el contrato, es decir, si se encuentra vigente y si existe algun '+
	'inconveniente o impedimento para que la Empresa otorgue el consentimiento para la cesion de derechos de cobro solicitado.\n'+
	
	'\nEs preciso resaltar la necesidad de que nos proporcione la mencionada informacion dentro de los (2) dias habiles siguientes, a aquel en que reciba'+
	' la presente solicitud, para dar respuesta al citado (Proveedor/Contratista).\n'+
	
	'\nAsimismo, le solicitamos se sirva indicarnos: \n'+
	'\nNos confirme o indique el nombre y cargo del servidor publico del Area de Tesorer�a en donde se vayan a tramitar y efectuar los '+
	' correspondientes pagos que se deriven de la ejecucion y cumplimiento del referido instrumento contractual.\n';
	 
	var consulta  = Ext.getDom('consulta').value;
	var mensaje  = Ext.getDom('mensaje').value;

	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
	
	var csProrrogaContrato = '';
	
	var procesarSuccessValoresIniciales =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			csProrrogaContrato = infoR.CDER_CS_PRORROGA_CONTRATO;
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarPoderesIF = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		//forma.action = '/nafin/00archivos/22cotizador/instrucciones.pdf';
					Ext.Ajax.request({
							url: '../../34DescargaPoderes.data.jsp',
							params: Ext.apply({informacion:'PoderesContratoCesion',clave_solicitud:clave_solicitud }),
							callback: procesarArchivoSuccess
						});
		}
	// respuesta de  Reactivacion
	var procesarSuccessFailureCorreo =  function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeMensaje= Ext.util.JSON.decode(response.responseText);			
			if (jsondeMensaje != null){
				var seEnvioCorreo =jsondeMensaje.seEnvioCorreo;
				//esto es para agregrar el mensaje de envio de correo
				var fpCorreo = Ext.getCmp('fpCorreo');
				var respuestaCorreo =  Ext.decode(jsondeMensaje.respuestaCorreo);	
				var indice1 = fpCorreo.items.indexOfKey('mensaje2')+1;
				fpCorreo.insert(indice1,respuestaCorreo);
				fpCorreo.doLayout();	
				//se oculta el boton de envio
				var btnEnviar = Ext.getCmp("btnEnviar");		
				btnEnviar.hide();
				
			}						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//validar datos  de envio de correo 
	var enviarCorreo = function() {
		var destinatarios = Ext.getCmp("destinatarios1");		
		var mensajeF = Ext.getCmp("mensaje1");
		var asunto = Ext.getCmp("asunto1");
		
		if (Ext.isEmpty(destinatarios.getValue()) ){
			destinatarios.markInvalid('El  destinatarios  es requerido.');
			return;
		}
		if (Ext.isEmpty(mensajeF.getValue()) ){
			mensajeF.markInvalid('El  Mensaje Predeterminado por Cesion de Derechos es requerido.');						
			return;
		}
		
		var logo;
		var archivo;
		var clave_solicitud;
		var gridCorreo = Ext.getCmp('gridCorreo');		
		var store = gridCorreo .getStore();
		store.each(function(record) {
			logo = record.data['LOGO']; 	
			archivo = record.data['ARCHIVO']; 
			clave_solicitud=  record.data['CLAVE_SOLICITUD']; 
		});
		
		Ext.Ajax.request({
			url : '34Consu_Solic_ConsentimientoExt.data.jsp',
			params : {
				informacion: 'EnvioCorreo',				
				clave_solicitud:clave_solicitud,
				destinatarios:destinatarios.getValue(),
				asunto:asunto.getValue(),
				mensaje:mensajeF.getValue(),
				logo:logo,
				archivo:archivo
			},
			callback: procesarSuccessFailureCorreo
		});
	}
	
	var cerrarCorreo = function() {
		var ventana = Ext.getCmp('enviaCorreo');	
		if (ventana) {	
			ventana.destroy();	
		}
	}
	
	var aceptarProrroga =function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud =registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
				url : '34Consu_Solic_ConsentimientoExt.data.jsp',
				params : {
					informacion: 'autorizarRechazarProrroga',
					clave_solicitud:clave_solicitud,
					estatusNuevo:'A'
				},
				callback: function(opts, success, response){
					if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
						json = Ext.util.JSON.decode(response.responseText);
						Ext.Msg.alert(
									'Mensaje',
									'La pr�rroga ha sido aceptada con �xito',
									function(){
										location.reload();
									}
								);
						}
						else{
							NE.util.mostrarConnError(response,opts);
						}
					}	
			});		
	}
	
	var rechazarProrroga =function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud =registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
				url : '34Consu_Solic_ConsentimientoExt.data.jsp',
				params : {
					informacion: 'autorizarRechazarProrroga',
					clave_solicitud:clave_solicitud,
					estatusNuevo:'R'
				},
				callback: function(opts, success, response){
					if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
						json = Ext.util.JSON.decode(response.responseText);
						Ext.Msg.alert(
									'Mensaje',
									'La pr�rroga ha sido rechazada con �xito',
									function(){
										location.reload();
									}
								);
						}
						else{
							NE.util.mostrarConnError(response,opts);
						}
					}	
			});		
	}
	//se muestra la pantalla de envio de correo
	var procesarEnvioCorreo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var logo =registro.get('LOGO');
		mensajes2 = ['<table width="300" align="center" cellpadding="0" cellspacing="0" border="0" bordercolor="#A5B8BF" class="formas" >'+			
			'<tr align="center" >	<td width="50" height="25" align="center"  ><img src='+logo+' align="center" ></td></tr>'+			
			'</table>'];			
			
		 var encabezado = {
			xtype: 'container',
			layout: 'table',
			id: 'encabezado',		
			width:	'500',
			align: 'center',
			heigth:	'auto',
			style: 'margin:0 auto;',
			items: [			
				{ 
					xtype:   'label',  
					html:		mensajes2.join(''), 
					cls:		'x-form-item',
					style: { 
						width:   		'200%', 
						textAlign: 		'center'
					} 
				}	
			]
		};
				
		consultaCorreoData.load({params:	{clave_solicitud:	clave_solicitud}});		
		
		var ventana = Ext.getCmp('enviaCorreo');
		
		 cerrarCorreo ();
		
		
		if (ventana) {
			ventana.show();
		}else {	
			new Ext.Window({			
				width: 940,
				height: 550,
				resizable: false,
				closable:false,
				closeAction: 'hide',
				id: 'enviaCorreo',
				align: 'center',
				items: [
					encabezado,
					gridCorreo,
					fpCorreo				
				],
				title: 'Enviar Correo'						
			}).show();
		}
	}
	
	var elementosFormaCorreo = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Para',
			id: 'destinatarios2',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [
				{
					xtype: 'textfield',
					name: 'destinatarios',
					id: 'destinatarios1',					
					fieldLabel: 'Para',
					allowBlank: true,
					hidden: false,
					maxLength: 1000,	
					width: 300,
					msgTarget: 'side',
					margins: '0 20 0 0'			
				},
				{
					xtype: 'displayfield',
					value: 'Separar correos por coma (,)',
					width: 200
				}
			]
		}, 
		{
			xtype: 'compositefield',
			fieldLabel: 'Asunto',
			id: 'asunto2',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [
				{
					xtype: 'textfield',
					name: 'asunto',
					id: 'asunto1',
					fieldLabel: 'Asunto:',
					allowBlank: true,
					hidden: false,
					maxLength: 100,	
					width: 300,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				}
			]
		},	
		{
			xtype: 'panel',
			fieldLabel: 'Archivo Adjunto',
			items: [
				{
					xtype: 'button',
					width: 80,
					height:10,
					text: 'Archivo',					
					id: 'btnArchivoAd',
					anchor: '',
					style: 'float:left',
					handler: function(boton, evento) {
						var clave_solicitud;
						var  gridCorreo = Ext.getCmp('gridCorreo');		
						var store = gridCorreo .getStore();
						store.each(function(record) {												
							clave_solicitud=  record.data['CLAVE_SOLICITUD']; 
						});					
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34Consu_Solic_ConsentimientoExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoAdjunto',
								clave_solicitud:clave_solicitud
							})
							,callback: procesarSuccessFailureAdjunto
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo Adjunto',
					id: 'btnBajarAd',
					hidden: true
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',			
			width: 600,
			id: 'mensaje2',
			items: [
				{
					xtype: 'textarea',
					name: 'mensaje',
					id: 'mensaje1',
					fieldLabel: '',
					value: cadenaCorreo,
					allowBlank: true,
					hidden: false,
					maxLength: 5000,	
					width: 400,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				}
			]
		}
	];
	
	var fpCorreo = {
		xtype: 'panel',
		id: 'fpCorreo',
		layout: 'form',
		width: 700,	
		title: '',
		align: 'center',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		style: 'margin:0 auto;',
		items:[
			elementosFormaCorreo
		],
		//monitorValid: true,
			buttons: [
			{
				xtype: 'button',
				text: 'Enviar',
				id: 'btnEnviar',
				handler: enviarCorreo
			}	,
			{
				xtype: 'button',
				text: 'Cerrar',
				id: 'btnCancelar',
				handler: cerrarCorreo
			}
		]
	};

	
	// respuesta de  Reactivacion
	var procesarSuccessFailureReactivar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeMensaje= Ext.util.JSON.decode(response.responseText);			
			if (jsondeMensaje != null){
				var clave_solicitud =jsondeMensaje.clave_solicitud;
				var tipoContratacion= jsondeMensaje.tipoContratacion;
				var estatusAnterior= jsondeMensaje.estatusAnterior;							
				var parametros='clave_solicitud='+clave_solicitud+'&cambiaEstatus='+'8'+'&estatusAnterior='+estatusAnterior;
				document.location.href  = "34Consu_Solicitud_Pre_AcuseExt.jsp?"+parametros;						
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//proceso de Reactivar
	var procesarReactivar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');		
		var estatusAnterior  = registro.data['NOESTATUS'];	
		var cambiaEstatus = 8;
		Ext.Ajax.request({
			url : '34Consu_Solic_ConsentimientoExt.data.jsp',
			params : {
				informacion: 'ValidandoCorreo',
				cambiaEstatus: cambiaEstatus,
				clave_solicitud:clave_solicitud,				
				estatusAnterior:estatusAnterior				
			},
			callback: procesarSuccessFailureReactivar
		});	
	}
	
	// respuesta proceso de Aceptar
	var procesarSuccessFailureAceptar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeMensaje= Ext.util.JSON.decode(response.responseText);			
			if (jsondeMensaje != null){
				var mensajeE  = jsondeMensaje.mensaje
				if(jsondeMensaje.mensaje!='') {									
					document.location.href  = "34Consu_Solic_ConsentimientoExt.jsp?consulta=S&mensaje="+mensajeE;					
				} else {				
					var clave_solicitud =jsondeMensaje.clave_solicitud;
					var tipoContratacion= jsondeMensaje.tipoContratacion;
					var estatusAnterior= jsondeMensaje.estatusAnterior;							
					var parametros='clave_solicitud='+clave_solicitud+'&cambiaEstatus='+'2'+'&estatusAnterior='+estatusAnterior+'&tipoContratacion='+tipoContratacion;
					document.location.href  = "34Consu_Solicitud_Pre_AcuseExt.jsp?"+parametros;		
				}
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//proceso de Aceptar
	var procesarAceptar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var tipoContratacion = registro.get('TIPOCONTRATACION');
		var estatusAnterior  = registro.data['NOESTATUS'];	
		var cambiaEstatus = 2;
		Ext.Ajax.request({
			url : '34Consu_Solic_ConsentimientoExt.data.jsp',
			params : {
				informacion: 'ValidandoCorreo',
				cambiaEstatus: cambiaEstatus,
				clave_solicitud:clave_solicitud,
				tipoContratacion:tipoContratacion,
				estatusAnterior:estatusAnterior				
			},
			callback: procesarSuccessFailureAceptar
		});	
	}
		
	// respuesta de rechazo  realizar la accion de Rechazo
	var procesarSuccessFailureRechazo =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeMensaje= Ext.util.JSON.decode(response.responseText);			
			if (jsondeMensaje != null){
				var clave_solicitud =jsondeMensaje.clave_solicitud;
				var tipoContratacion= jsondeMensaje.tipoContratacion;
				var estatusAnterior= jsondeMensaje.estatusAnterior;							
				var parametros='clave_solicitud='+clave_solicitud+'&cambiaEstatus='+'5'+'&estatusAnterior='+estatusAnterior+'&tipoContratacion='+tipoContratacion;
				document.location.href  = "34Consu_Solicitud_RechazarExt.jsp?"+parametros;						
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// para realizar la accion de Rechazo
	var procesarRechazo = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);	
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var tipoContratacion = registro.get('TIPOCONTRATACION');
		var estatusAnterior  = registro.data['NOESTATUS'];	
		var cambiaEstatus = 5;
		Ext.Ajax.request({
			url : '34Consu_Solic_ConsentimientoExt.data.jsp',
			params : {
				informacion: 'ValidandoCorreo',
				cambiaEstatus: cambiaEstatus,
				clave_solicitud:clave_solicitud,
				tipoContratacion:tipoContratacion,
				estatusAnterior:estatusAnterior				
			},
			callback: procesarSuccessFailureRechazo
		});	
	}
	
	//Ventana Para ver el Contrato de Consentimiento
	var verConsentimiento = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_contratacion = registro.get('TIPOCONTRATACION');		
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		var nombre_epo = registro.get('NOMBREEPO');
		var objeto_contrato = registro.get('OBCONTRATO');
		var montospormoneda = registro.get('MONTO_MONEDA');
		var plazocontrato = registro.get('PLAZO_CONTRATO');
		var fechasolicitud = registro.get('FECHASOLICITUD');
		var numero_contrato = registro.get('NOCONTRATO');
		var nombre_if = registro.get('CECIONARIO');
		
		var ventana = Ext.getCmp('verConsentimiento');
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 940,
				height: 550,		
				id: 'verConsentimiento',
				closeAction: 'hide',
				items: [					
					Consentimiento
				],
				title: 'Consentimiento'
			}).show();
		}
		var bodyPanel = Ext.getCmp('Consentimiento').body;
		bodyPanel.on('focus',function(){Ext.getCmp('Consentimiento').focus();});
			var mgr = bodyPanel.getUpdater();
			mgr.on('failure', 
				function(el, response) {
					x.update('');
					NE.util.mostrarErrorResponse(response);
				}
			);	
			mgr.update({
				url: '../34pyme/34ContratoCesionVerConsentimientoext.jsp',					
				params: {
					clave_contratacion: clave_contratacion,
					clave_solicitud:clave_solicitud,
					nombre_epo:nombre_epo,
					objeto_contrato:objeto_contrato,
					montospormoneda:montospormoneda,
					plazocontrato:plazocontrato,
					fechasolicitud:fechasolicitud,
					numero_contrato:numero_contrato,
					nombre_if:nombre_if
				},
				indicatorText: 'Cargando Consentimiento'
			});	
	}
	
	var Consentimiento = {
		xtype: 'panel',
		id: 'Consentimiento',
		width: 650,
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true
	};

	
	//para ver los poderes de la pyme
	var VerPoderesPyme = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLAVE_PYME');	
		var rfcPyme = registro.get('RFC');	
		var grupo_cesion = registro.get('GRUPO_CESION');	
		var no_contrato = registro.get('NOCONTRATO');	
			
		var parametros = "clavePyme="+clavePyme+"&rfcPyme="+rfcPyme+"&grupo_cesion="+grupo_cesion+"&no_contrato="+no_contrato;
		
		var ventana = Ext.getCmp('VerPoderesPyme2');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
					layout: 'fit',
					width: 500,
					height: 200,			
					id: 'VerPoderesPyme2',
					closeAction: 'hide',
					items: [					
						PanelVerPoderesPyme2
					],
					title: 'Ver Poderes Pyme  '					
			}).show();
		}	
		
		var pabelBody = Ext.getCmp('PanelVerPoderesPyme2').body;
			var mgr = pabelBody.getUpdater();
			mgr.on('failure', 
			function(el, response) {
				pabelBody.update('');
				NE.util.mostrarErrorResponse(response);
			});		
			mgr.update({
				url: '/nafin/34cesion/34pki/34pyme/34SolicConsVerPoderesPymeExt.jsp?'+parametros,						
				indicatorText: 'Cargando Ver Poderes Pyme '
			});					
	}
	
	var PanelVerPoderesPyme2 = {
		xtype: 'panel',
		id: 'PanelVerPoderesPyme2',
		width: 700,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	};
	
		
	
	
		//muestra el archivo de la Columna Contrato 
	var procesarSuccessFailureContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	//descarga archivo de contrato
	var descargaArchivoContrato = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
			url: '34Consu_Solic_ConsentimientoExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContrato
		});
	}
	
	
	// Baja archivo Adjunto  que se envia en el correo
	var procesarSuccessFailureAdjunto =  function(opts, success, response) {
		var btnArchivoAd = Ext.getCmp('btnArchivoAd');
		btnArchivoAd.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarAd = Ext.getCmp('btnBajarAd');
			btnBajarAd.show();
			btnBajarAd.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarAd.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnArchivoAd.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//GENERAR ARCHIVO TODO CSV
	var procesarSuccessFailureCSV =  function(opts, success, response) {
		var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');
		btnImprimirCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	var procesarConsultaCorreoData = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var gridCorreo = Ext.getCmp('gridCorreo');
			if (!gridCorreo.isVisible()) {
				gridCorreo.show();
			}			
				
			var jsonData = store.reader.jsonData;	
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var clasificacionEpo = jsonData.clasificacionEpo;	
			
			var el = gridCorreo.getGridEl();
			var rutaArchivo=  jsonData.archivo;	
			var nombreArchivo =  jsonData.nombreArchivo;
						
			if(store.getTotalCount() > 0) {		
			
				//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
				var cm = gridCorreo.getColumnModel();
				
				if(clasificacionEpo !=''){				
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION'),clasificacionEpo);
				}
				
				if(hayCamposAdicionales=='0'){
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);					
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);	
					gridCorreo.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.campo05);						
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCorreo.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}
				el.unmask();
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  clasificacionEpo = jsonData.clasificacionEpo;	
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			
			if(clasificacionEpo ==''){				
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION'),clasificacionEpo);
			}
			
			if(hayCamposAdicionales=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='2'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='3'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='4'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='5'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.campo05);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
			}
					
			
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');	
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			
			var el = gridConsulta.getGridEl();						
			if(store.getTotalCount() > 0) {			
				btnImprimirPDF.enable();
				btnImprimirCSV.enable();					
				btnBajarPDF.hide();
				btnBajarCSV.hide();		
				el.unmask();
			} else {	
				btnImprimirPDF.disable();
				btnImprimirCSV.disable();
				btnBajarPDF.hide();
				btnBajarCSV.hide();			
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPymeData = new Ext.data.JsonStore({
		id: 'catalogoPymeDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoPyme'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	}); 
	
	var catalogoProrroga = new Ext.data.ArrayStore({
        fields: [
            'clave',
            'descripcion'
        ],
        data: [['P', 'Solicitud Pr�rroga'], ['A', 'Pr�rroga Aceptada'],['R', 'Pr�rroga Rechazada']]
    });
	 
	var catalogoEstatusData = new Ext.data.JsonStore({
		id: 'catalogoEstatusDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoTipoPlazoData = new Ext.data.JsonStore({
		id: 'catalogoTipoPlazoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var consultaCorreoData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'ConsultarCorreo'
		},
			fields: [
					{name: 'CECIONARIO'},
					{name: 'PYME'},
					{name: 'RFC'},
					{name: 'CLAVE_PYME'},					
					{name: 'FSOLICITUDINI'},
					{name: 'NOCONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'CONTRATACION'},
					{name: 'FECHA_INICIO_CONTRATO'},
					{name: 'FECHA_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION'},
					{name: 'CAMPO1'},
					{name: 'CAMPO2'},
					{name: 'CAMPO3'},
					{name: 'CAMPO4'},
					{name: 'CAMPO5'},
					{name: 'VENANILLA_PAGO'},
					{name: 'SUP_ADM_RESOB'},
					{name: 'NUMERO_TELEFONO'},
					{name: 'OBCONTRATO'},					
					{name: 'FACEPTACION'},
					{name: 'ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'LOGO'},
					{name: 'ARCHIVO'},
					{name: 'FIRMA_CONTRATO'}
					
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaCorreoData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaCorreoData(null, null, null);						
					}
				}
			}
		});	
		
			
	var gridCorreo = {
		xtype: 'editorgrid',
		store: consultaCorreoData,
		id: 'gridCorreo',		
		hidden: false,
		columns: [		
		{
				header: 'Intermediario Financiero (Cesionario)', 
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'CECIONARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Pyme (Cedente)', 
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'RFC ', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'No Proveedor ', 
				tooltip: 'No Proveedor',
				dataIndex: 'CLAVE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},			
			{
				header: 'Fecha de solicitud Pyme', 
				tooltip: 'Fecha de solicitud Pyme ',
				dataIndex: 'FSOLICITUDINI',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},	
			{
				header: 'No. Contrato', 
				tooltip: 'No. Contrato',
				dataIndex: 'NOCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda',  // este usa  metodo para obtenerlo
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tipo de Contrataci�n',  
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'CONTRATACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'F. Inicio Contrato',  
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'						
			},
			{
				header: 'F. Final Contrato',  
				tooltip: 'F. Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},			
			{
				header: 'Plazo del Contrato',  
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Clasificaci�n EPO',  
				tooltip: 'Clasificaci�n EPO',
				dataIndex: 'CLASIFICACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			//aqui van los campos adicionales
			{
				header: 'Campo 1',  
				tooltip: 'Campo 1',
				dataIndex: 'CAMPO1',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 2',  
				tooltip: 'Campo 2',
				dataIndex: 'CAMPO2',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 3',  
				tooltip: 'Campo 3',
				dataIndex: 'CAMPO3',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 4',  
				tooltip: 'Campo 4',
				dataIndex: 'CAMPO4',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 5',  
				tooltip: 'Campo 5',
				dataIndex: 'CAMPO5',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Ventanilla de Pago',  
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENANILLA_PAGO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra',  
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUP_ADM_RESOB',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tel�fono',  
				tooltip: 'Tel�fono',
				dataIndex: 'NUMERO_TELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Objeto del Contrato',  
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Fecha de Aceptacion o Rechazo',  
				tooltip: 'Fecha de Aceptacion o Rechazo',
				dataIndex: 'FACEPTACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Estatus Actual',  
				tooltip: 'Estatus Actual',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'				 
			}		
		],
		stripeRows: true,
		loadMask: true,
		height: 150,
		width: 925,
		frame: true
	};
	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
			fields: [
					{name: 'CECIONARIO'},
					{name: 'PYME'},
					{name: 'RFC'},
					{name: 'CLAVE_PYME'},
					{name: 'REPRESENTANTE'},
					{name: 'FSOLICITUDINI'},
					{name: 'NOCONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'CONTRATACION'},
					{name: 'FECHA_INICIO_CONTRATO'},
					{name: 'FECHA_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION'},
					{name: 'CAMPO1'},
					{name: 'CAMPO2'},
					{name: 'CAMPO3'},
					{name: 'CAMPO4'},
					{name: 'CAMPO5'},
					{name: 'VENANILLA_PAGO'},
					{name: 'SUP_ADM_RESOB'},
					{name: 'NUMERO_TELEFONO'},
					{name: 'OBCONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'FACEPTACION'},
					{name: 'ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'TIPOCONTRATACION'},
					{name: 'NOESTATUS'},
					{name: 'NOEPO'},
					{name: 'LOGO'},
					{name: 'NOMBREEPO'},
					{name: 'FECHASOLICITUD'},
					{name: 'FIRMA_CONTRATO'},
					{name: 'ESTATUS_PRORROGA'},
					{name: 'GRUPO_CESION'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
		

	var gridConsulta = new Ext.grid.EditorGridPanel({	 
		id: 'gridConsulta',
		store: consultaData,
		hidden: true,
		//margins: '20 0 0 0',
		title: 'Consulta Solicitud de Consentimiento',
		columns: [			
		{
				header: 'NOESTATUS', 
				tooltip: 'NOESTATUS',
				dataIndex: 'NOESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'Intermediario Financiero (Cesionario)', 
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'CECIONARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Pyme (Cedente)', 
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'RFC ', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'No Proveedor ', 
				tooltip: 'No Proveedor',
				dataIndex: 'CLAVE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Representante legal ',  // usa un metodo pra obtenerlo
				tooltip: 'Representante legal',
				dataIndex: 'REPRESENTANTE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Fecha de solicitud Pyme', 
				tooltip: 'Fecha de solicitud Pyme ',
				dataIndex: 'FSOLICITUDINI',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},	
			{
				header: 'No. Contrato', 
				tooltip: 'No. Contrato',
				dataIndex: 'NOCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda',  // este usa  metodo para obtenerlo
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tipo de Contrataci�n',  
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'CONTRATACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'F. Inicio Contrato',  
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'						
			},
			{
				header: 'F. Final Contrato',  
				tooltip: 'F. Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},			
			{
				header: 'Plazo del Contrato',  
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Ciudad de Firma del Contrato',  
				tooltip: 'Ciudad de Firma del Contrato',
				dataIndex: 'CLASIFICACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			//aqui van los campos adicionales
			{
				header: 'Campo 1',  
				tooltip: 'Campo 1',
				dataIndex: 'CAMPO1',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 2',  
				tooltip: 'Campo 2',
				dataIndex: 'CAMPO2',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 3',  
				tooltip: 'Campo 3',
				dataIndex: 'CAMPO3',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 4',  
				tooltip: 'Campo 4',
				dataIndex: 'CAMPO4',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 5',  
				tooltip: 'Campo 5',
				dataIndex: 'CAMPO5',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Ventanilla de Pago',  
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENANILLA_PAGO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra',  
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUP_ADM_RESOB',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tel�fono',  
				tooltip: 'Tel�fono',
				dataIndex: 'NUMERO_TELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Objeto del Contrato',  
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Comentarios',  
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
	      xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},	
						handler: descargaArchivoContrato
					}
				]				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Poderes',
				tooltip: 'Poderes',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						}
						,	handler: VerPoderesPyme
					}
				]				
			},{
	      xtype: 'actioncolumn',
				header: 'Poderes IF',
				tooltip: 'Poderes IF',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						}
						,	handler: procesarPoderesIF
					}
				]				
			},	
			{
				header: 'Fecha de Aceptacion o Rechazo',  
				tooltip: 'Fecha de Aceptacion o Rechazo',
				dataIndex: 'FACEPTACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Estatus Actual',  
				tooltip: 'Estatus Actual',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'				 
			},
			{
	      xtype: 'actioncolumn',
				header: 'Consentimiento',
				tooltip: 'Consentimiento',
        width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('TIPOCONTRATACION') ==1 || registro.get('TIPOCONTRATACION') ==2 || registro.get('TIPOCONTRATACION') ==3 || registro.get('TIPOCONTRATACION') ==4 || registro.get('TIPOCONTRATACION') ==5 || registro.get('TIPOCONTRATACION') ==6 ){
						return '';
					}else {
						return 'N/A';	
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							if(registro.get('TIPOCONTRATACION') ==1 || registro.get('TIPOCONTRATACION') ==2 || registro.get('TIPOCONTRATACION') ==3 || registro.get('TIPOCONTRATACION') ==4 || registro.get('TIPOCONTRATACION') ==5 || registro.get('TIPOCONTRATACION') ==6 ){
								return 'iconoLupa';		
							}
						}
						,	handler: verConsentimiento
					}
				]				
			},
			{
	      xtype: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',
        width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					var  mostrarNA;
					if(registro.get('NOESTATUS') ==5 || registro.get('NOESTATUS') ==3 ){
						 return 'N/A';
					}else { return ''; }
				},
				items: [
					{ //para Aceptar - Aceptar Consentimiento
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							if(registro.get('NOESTATUS') ==2 || registro.get('NOESTATUS') ==6 ){
									this.items[0].tooltip = 'Aceptar Solicitud';
									return 'icoAceptar';											
							}		
						}
						,handler: procesarAceptar					
					},
					{ //para Rechazar - Rechazar Consentimiento
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							if(registro.get('NOESTATUS') ==2 || registro.get('NOESTATUS') ==6){
									this.items[1].tooltip = 'Rechazar Solicitud';
									return 'borrar';											
							}		
						}
						,handler: procesarRechazo					
					},
					{ //para Aviso 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							if(registro.get('NOESTATUS') ==2 || registro.get('NOESTATUS') ==6  || registro.get('NOESTATUS') ==8){
									this.items[2].tooltip = 'Env�o Correo';
									return 'correo';											
							}		
						}
						,handler: procesarEnvioCorreo					
					},
					{ //para Reactivar Solicitud 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							if(registro.get('NOESTATUS') ==8  ){
									this.items[3].tooltip = 'Reactivar Solicitud';
									return 'autorizar';											
							}		
						}
						,handler: procesarReactivar					
					}
				]
			},
			/*{
				xtype: 'actioncolumn',
				header: 'Pr�rroga',
				tooltip: 'Pr�rroga',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					 return 'N/A';
				
				}
			},
			{
				header: 'Estatus Pr�rroga',  
				tooltip: 'Estatus Pr�rroga',
				dataIndex: 'ESTATUS_PRORROGA',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					  return 'N/A';
				
				}
			}*/
			{
	      xtype: 'actioncolumn',
				header: 'Pr�rroga',
				tooltip: 'Pr�rroga',
        width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					var  mostrarNA;
					if(csProrrogaContrato!='S' || registro.get('ESTATUS_PRORROGA') ==''||registro.get('ESTATUS_PRORROGA') =='A'||registro.get('ESTATUS_PRORROGA') =='R'){
						 return 'N/A';
					}else { return ''; }
				},
				items: [
					{ //para Aceptar 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							if(registro.get('ESTATUS_PRORROGA') =='P' && csProrrogaContrato=='S'){
									this.items[0].tooltip = 'Aceptar Pr�rroga';
									return 'icoAceptar';											
							}		
						}
						,handler: aceptarProrroga					
					},
					{ //para Rechazar 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
							if(registro.get('ESTATUS_PRORROGA') =='P' && csProrrogaContrato=='S'){
									this.items[1].tooltip = 'Rechazar Pr�rroga';
									return 'borrar';											
							}		
						}
						,handler: rechazarProrroga					
					}
				]
			},{
				header: 'Estatus Pr�rroga',  
				tooltip: 'Estatus Pr�rroga',
				dataIndex: 'ESTATUS_PRORROGA',
				sortable: true,
				resizable: true	,
				width: 130,
				align: 'left',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					
					if(csProrrogaContrato!='S'){
						return 'N/A';
					}else if(value=='P' ){
						 return 'Solicitud Pr�rroga';
					}else if(value=='A'){
						return 'Pr�rroga Aceptada';
					} else if(value=='R'){
						return 'Pr�rroga Rechazada';
					}else{
						return 'N/A';
					}
				}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {		
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,				
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnImprimirPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34Consu_Solic_ConsentimientoExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								tipo_archivo: 'PDF'
							})
							,callback: procesarSuccessFailurePDF
						});
					}
				},					
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnImprimirCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34Consu_Solic_ConsentimientoExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								tipo_archivo: 'CSV'
							})
							,callback: procesarSuccessFailureCSV
						});
					}
				},					
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}
			]
		}
	});
	
	var elementosForma = [
	
		{
			xtype: 'compositefield',
			fieldLabel: 'Nombre del Cesionario (IF)',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [			
				{
					xtype: 'combo',
					name: 'noIF',
					id: 'noIF1',
					fieldLabel: 'Nombre del Cesionario (IF)',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'noIF',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 400,
					minChars : 1,
					allowBlank: true,
					store : catalogoIFData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Pyme',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [
			{
				xtype: 'combo',
				name: 'pyme',
				id: 'pyme1',
				fieldLabel: 'Pyme',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'pyme',
				emptyText: 'Seleccione...',					
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				width: 400,
				minChars : 1,
				allowBlank: true,
				store : catalogoPymeData,
				tpl : NE.util.templateMensajeCargaCombo
			}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Estatus',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [				
				{
					xtype: 'combo',
					name: 'estatus',
					id: 'estatus1',
					fieldLabel: 'Estatus',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'estatus',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 200,
					minChars : 1,
					allowBlank: true,
					store : catalogoEstatusData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},	
		{
			xtype: 'compositefield',
			fieldLabel: 'Estatus Pr�rroga',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [				
				{
					xtype: 'combo',
					name: 'estatusProrroga',
					id: 'estatusP',
					fieldLabel: 'Estatus Pr�rroga',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'estatusProrroga',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 200,
					minChars : 1,
					allowBlank: true,
					store : catalogoProrroga
					
				}
			]
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Contrato:',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [
				{
				xtype: 'textfield',
				name: 'noContrato',
				id: 'noContrato1',
				fieldLabel: 'N�mero de Contrato',
				allowBlank: true,
				hidden: false,
				maxLength: 25,	
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0'  
				}
			]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Contrato',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'fvigenciaIni',
					id: 'fvigenciaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fvigenciaFin',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){
							Ext.getCmp("plazoContrato1").setValue('');
							Ext.getCmp("claveTipoPlazo1").setValue('');
						}
					}
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fvigenciaFin',
					id: 'fvigenciaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fvigenciaIni',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){
							Ext.getCmp("plazoContrato1").setValue('');
							Ext.getCmp("claveTipoPlazo1").setValue('');
						}
					}
				}
			]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo del Contrato',
			combineErrors: false,
			msgTarget: 'side',
			width: 600,
			items: [
				{
					xtype: 'textfield',
					name: 'plazoContrato',
					id: 'plazoContrato1',
					fieldLabel: 'Plazo del Contrato',
					allowBlank: true,
					hidden: false,
					maxLength: 25,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){
							Ext.getCmp("fvigenciaIni").setValue('');
							Ext.getCmp("fvigenciaFin").setValue('');
						}
					}
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 20
				},
				{
					xtype: 'combo',
					name: 'claveTipoPlazo',
					id: 'claveTipoPlazo1',
					fieldLabel: 'Pyme',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'claveTipoPlazo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 200,
					minChars : 1,
					allowBlank: true,
					store : catalogoTipoPlazoData,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners: {
						select: {
							fn: function(combo) {
								Ext.getCmp("fvigenciaIni").setValue('');
								Ext.getCmp("fvigenciaFin").setValue('');							
							}
						}
					}
				}
			]
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud PyMe',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'fsolicitudIni',
					id: 'fsolicitudIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fsolicitudFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fsolicitudFin',
					id: 'fsolicitudFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fsolicitudIni',
					margins: '0 20 0 0'  
				}
			]
		}	
	];
		

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
				
					var ventana = Ext.getCmp('verConsentimiento');	
					if (ventana) {	
						ventana.destroy();	
					}
					var ventana2 = Ext.getCmp('VerPoderesPyme2');
					if (ventana2) {	
						ventana2.destroy();	
					}
					var ventana3 = Ext.getCmp('enviaCorreo');
					if (ventana3) {	
						ventana3.destroy();	
					}
				
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar', 								
							start: 0,
							limit: 15
						})
					});				
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34Consu_Solic_ConsentimientoExt.jsp';
				}
			}
		]
	});
	
//--------------------------------PRINCIPAL-------------------------------------
var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)		
		]
	});
	
	
	catalogoIFData.load();
	catalogoPymeData.load();
	catalogoEstatusData.load();
	catalogoTipoPlazoData.load();
	
	Ext.Ajax.request({
		url: '34Consu_Solic_ConsentimientoExt.data.jsp',
		params: {
			informacion: 'ValoresIniciales'
		}
		,callback: procesarSuccessValoresIniciales
	});
	
	
	if(consulta=='S'){
		consultaData.load({
			params: {
				operacion: 'Generar', 								
				start: 0,
				limit: 15
			}
		});
		Ext.MessageBox.alert("Mensaje",mensaje);
	}
});