<!DOCTYPE html>
<%@ page import="java.util.*,
		com.netro.cesion.*,
		netropology.utilerias.*" 
		contentType="text/html;charset=windows-1252"
		errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<% 
String pantalla  = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";				
String clave_solicitud  = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";		
String acuse  = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";		
String fecha  = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):"";		
String hora  = (request.getParameter("hora")!=null)?request.getParameter("hora"):"";		
String nombreUsuario  = (request.getParameter("nombreUsuario")!=null)?request.getParameter("nombreUsuario"):"";		
String tipoOperacion  = (request.getParameter("tipoOperacion")!=null)?request.getParameter("tipoOperacion"):"";	
							
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../certificado.jspf" %>

<%if(pantalla.equals("")){ %>
<script type="text/javascript" src="34NotificacionesEpoVentanillaExt.js?<%=session.getId()%>"></script>
<%}  else  if(pantalla.equals("PreAcuse")){%>
<script type="text/javascript" src="34NotificacionesEpoVentanillaPreAcuseExt.js?<%=session.getId()%>"></script>
<%}  else  if(pantalla.equals("Acuse")){%>
	<script type="text/javascript" src="34NotificacionesEpoVentanillaAcuseExt.js?<%=session.getId()%>"></script>
<%}%>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
				<%@ include file="/01principal/01epo/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
						<div id="areaContenido"></div>						
					</div>
				</div>
				<%@ include file="/01principal/01epo/pie.jspf"%>
				
	<form id='formParametros' name="formParametros">	
		<input type="hidden" id="clave_solicitud" name="clave_solicitud" value="<%=clave_solicitud%>"/>	
		<input type="hidden" id="pantalla" name="pantalla" value="<%=pantalla%>"/>		
		<input type="hidden" id="acuse" name="acuse" value="<%=acuse%>"/>		
		<input type="hidden" id="fecha" name="fecha" value="<%=fecha%>"/>		
		<input type="hidden" id="hora" name="hora" value="<%=hora%>"/>		
		<input type="hidden" id="nombreUsuario" name="nombreUsuario" value="<%=nombreUsuario%>"/>
		<input type="hidden" id="tipoOperacion" name="tipoOperacion" value="<%=tipoOperacion%>"/>		
	</form>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
