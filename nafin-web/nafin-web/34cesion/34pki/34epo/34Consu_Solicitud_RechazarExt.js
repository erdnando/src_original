Ext.onReady(function(){

	var clave_solicitud  = Ext.getDom('clave_solicitud').value;
	var tipoContratacion  = Ext.getDom('tipoContratacion').value;
	var estatusAnterior  = Ext.getDom('estatusAnterior').value;
	var noEpo  = Ext.getDom('noEpo').value;
	var cambiaEstatus  = Ext.getDom('cambiaEstatus').value;
	
	var procesarSuccessFailureCancelar =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
				if(jsondeAcuse.cancelaPreacuse=='S') {						
					document.location.href = "34Consu_Solic_ConsentimientoExt.jsp";		
				} 				
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var cancelarRechazo = function() 	{	
		Ext.Ajax.request({
			url : '34Consu_Solic_ConsentimientoExt.data.jsp',
			params : {
				informacion: 'CancelarRechazo',
				estatusAnterior: estatusAnterior,
				clave_solicitud:clave_solicitud
			},
			callback: procesarSuccessFailureCancelar
		});	

	}
	
	var procesarSuccessFailureAcuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
				if(jsondeAcuse.acuse!='') {	
					var banderaRechazo
					if(jsondeAcuse.banderaRechazo !=true){
						banderaRechazo=false;
					}else{
						banderaRechazo=true;
					}
					document.location.href = "34Consu_Solicitud_AcuseExt.jsp?clave_solicitud="+clave_solicitud+"&acuse="+jsondeAcuse.acuse+"&tipoContratacion="+tipoContratacion+"&banderaRechazo="+banderaRechazo;		
				} 				
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarRechazo = function() 	{	
		
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		var cauza_rechazo ='';
		var textoFirmar  ='';
		var estatusAcuse ='';
		store.each(function(record) {
			cauza_rechazo = record.data['CAUSA_RECHAZO']; 
			textoFirmar = record.data['TEXTO_FIRMAR']; 	
			estatusAcuse = record.data['ESTATUS_ACUSE']; 
	
		});
		if(cauza_rechazo==''){
			Ext.MessageBox.alert("Mensaje","Es necesario escribir la causa de rechazo de la solicitud");
			return;
		}else {
			NE.util.obtenerPKCS7(rechazar, textoFirmar, estatusAcuse, cauza_rechazo);
		}
	}
	
	var rechazar = function(pkcs7, textoFirmar, estatusAcuse, cauza_rechazo) {
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else {
			Ext.Ajax.request({
				url : '34Consu_Solic_ConsentimientoExt.data.jsp',
				params : {
					pkcs7: pkcs7,
					textoFirmado: textoFirmar,
					informacion: 'AutorizaRechazo',
					estatusAcuse: estatusAcuse,
					textoFirmar:textoFirmar,
					clave_solicitud:clave_solicitud,
					cauza_rechazo:cauza_rechazo
				},
				callback: procesarSuccessFailureAcuse
			});
		}
	}
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		//fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  clasificacionEpo = jsonData.clasificacionEpo;	
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			
			if(clasificacionEpo ==''){				
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION'),clasificacionEpo);
			}
			
			if(hayCamposAdicionales=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='2'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='3'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='4'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='5'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.campo05);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
			}
							
			var el = gridConsulta.getGridEl();						
			if(store.getTotalCount() > 0) {			
					
				el.unmask();
			} else {	
					
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'ConsultarRechazo'
		},
			fields: [
					{name: 'CECIONARIO'},
					{name: 'PYME'},
					{name: 'RFC'},
					{name: 'CLAVE_PYME'},
					{name: 'REPRESENTANTE'},
					{name: 'FSOLICITUDINI'},
					{name: 'NOCONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'CONTRATACION'},
					{name: 'FECHA_INICIO_CONTRATO'},
					{name: 'FECHA_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION'},
					{name: 'CAMPO1'},
					{name: 'CAMPO2'},
					{name: 'CAMPO3'},
					{name: 'CAMPO4'},
					{name: 'CAMPO5'},
					{name: 'VENANILLA_PAGO'},
					{name: 'SUP_ADM_RESOB'},
					{name: 'NUMERO_TELEFONO'},
					{name: 'OBCONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'FACEPTACION'},
					{name: 'ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'TIPOCONTRATACION'},
					{name: 'NOESTATUS'},
					{name: 'CAUSA_RECHAZO'},
					{name: 'TEXTO_FIRMAR'},
					{name: 'ESTATUS_ACUSE'}					
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
		

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		store: consultaData,			
		title: 'Rechazo Solicitud de Consentimiento',
		columns: [
			{
				header: 'Intermediario Financiero (Cesionario)', 
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'CECIONARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Pyme (Cedente)', 
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'RFC ', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'No Proveedor ', 
				tooltip: 'No Proveedor',
				dataIndex: 'CLAVE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Representante legal ',  // usa un metodo pra obtenerlo
				tooltip: 'Representante legal',
				dataIndex: 'REPRESENTANTE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Fecha de solicitud Pyme', 
				tooltip: 'Fecha de solicitud Pyme ',
				dataIndex: 'FSOLICITUDINI',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},	
			{
				header: 'No. Contrato', 
				tooltip: 'No. Contrato',
				dataIndex: 'NOCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Monto / Moneda',  // este usa  metodo para obtenerlo
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tipo de Contrataci�n',  
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'CONTRATACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'F. Inicio Contrato',  
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'						
			},
			{
				header: 'F. Final Contrato',  
				tooltip: 'F. Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},			
			{
				header: 'Plazo del Contrato',  
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Ciudad de Firma del Contrato',  
				tooltip: 'Ciudad de Firma del Contrato',
				dataIndex: 'CLASIFICACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			//aqui van los campos adicionales
			{
				header: 'Campo 1',  
				tooltip: 'Campo 1',
				dataIndex: 'CAMPO1',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 2',  
				tooltip: 'Campo 2',
				dataIndex: 'CAMPO2',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 3',  
				tooltip: 'Campo 3',
				dataIndex: 'CAMPO3',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 4',  
				tooltip: 'Campo 4',
				dataIndex: 'CAMPO4',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 5',  
				tooltip: 'Campo 5',
				dataIndex: 'CAMPO5',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Ventanilla de Pago',  
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENANILLA_PAGO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra',  
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUP_ADM_RESOB',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tel�fono',  
				tooltip: 'Tel�fono',
				dataIndex: 'NUMERO_TELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Objeto del Contrato',  
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Comentarios',  
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Fecha de Aceptacion o Rechazo',  
				tooltip: 'Fecha de Aceptacion o Rechazo',
				dataIndex: 'FACEPTACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Causa del Rechazo',  
				tooltip: 'Causa del Rechazo',
				dataIndex: 'CAUSA_RECHAZO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left',
				editor: { 
					 xtype : 'textarea', 
					 allowBlank: false,  
						maxLength: 150
					}						
			},			
			{
				header: 'Estatus Actual',  
				tooltip: 'Estatus Actual',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				 
			}		
			
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 943,		
		frame: true,
		margins: '20 0 0 0'
	});

	//Contenedor para el PreAcuse
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',		
		layoutConfig: {
			columns: 20
		},
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [	
		{
			xtype: 'button',
			text: 'Rechazar',
			id: 'btnRechazar',
			iconCls: 'icoAceptar',
			handler: procesarRechazo		
		},			
		{
			xtype: 'button',
			text: 'Cancelar',
			iconCls: 'icoLimpiar',
			handler: cancelarRechazo
		}			
	]
	});
	
	
//--------------------------------PRINCIPAL-------------------------------------
var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		items: [
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20)
		]
	});

	//esto es para la consulta de Rechazada
	if(clave_solicitud!='') {
		
		consultaData.load({
			params: {
				informacion: 'ConsultarRechazo',
				clave_solicitud: clave_solicitud,
				tipoContratacion :tipoContratacion,
				estatusAnterior:estatusAnterior,
				noEpo:noEpo
				
			}	
		});			
	}
	
});