Ext.onReady(function(){
	var clave_solicitud;
	var nombreArchivo;
	var numeroContrato;
	
	
	var procesarRechazar = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);	
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');			
		document.location.href = "34NotificacionesEpoVentanillaExt.jsp?clave_solicitud="+clave_solicitud+"&tipoOperacion=RECHAZO&pantalla=PreAcuse"; 		
	}
	
//Muestra el archivo de la Columna Contrato de Cesion 
	var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '../34pyme/34ContratoCesionPDFCSV.jsp',
			params: Ext.apply({
				informacion: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}
	
	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarPoderesIF = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		//forma.action = '/nafin/00archivos/22cotizador/instrucciones.pdf';
					Ext.Ajax.request({
							url: '../../34DescargaPoderes.data.jsp',
							params: Ext.apply({informacion:'PoderesContratoCesion',clave_solicitud:clave_solicitud }),
							callback: procesarArchivoSuccess
						});
		}
	
	var procesarRedireccionar = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);	
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');	
	
		document.location.href = "34NotificacionesEpoVentanillaExt.jsp?clave_solicitud="+clave_solicitud+"&pantalla=PreAcuse&tipoOperacion=ACEPTA"; 
	}
	
//para ver los poderes de la pyme
	var VerPoderesPyme = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLAVE_PYME');			
		var rfcPyme = registro.get('RFC');	
		var grupo_cesion = registro.get('GRUPO_CESION');	
		var no_contrato = registro.get('NUMERO_CONTRATO');	
		
		var parametros = "clavePyme="+clavePyme+"&rfcPyme="+rfcPyme+"&grupo_cesion="+grupo_cesion+"&no_contrato="+no_contrato;
		
		var ventana = Ext.getCmp('VerPoderesPyme2');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
					layout: 'fit',
					width: 500,
					height: 200,			
					id: 'VerPoderesPyme2',
					closeAction: 'hide',
					items: [					
						PanelVerPoderesPyme2
					],
					title: 'Ver Poderes Pyme  '					
			}).show();
		}	
		
		var pabelBody = Ext.getCmp('PanelVerPoderesPyme2').body;
			var mgr = pabelBody.getUpdater();
			mgr.on('failure', 
			function(el, response) {
				pabelBody.update('');
				NE.util.mostrarErrorResponse(response);
			});		
			mgr.update({
				url: '/nafin/34cesion/34pki/34pyme/34SolicConsVerPoderesPymeExt.jsp?'+parametros,						
				indicatorText: 'Cargando Ver Poderes Pyme '
			});					
	}
	
	var PanelVerPoderesPyme2 = {
		xtype: 'panel',
		id: 'PanelVerPoderesPyme2',
		width: 700,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	};
	
	var procesarSuccessFailureContrato=  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				//var forma = Ext.getDom('formAux');
				//forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				//forma.submit();
				var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
//Muestra el archivo de la Columna Contrato de Cesion		
	/*
	var mostrarContratoCesion = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		document.forms[0].action = "/nafin/34cesion/34pki/34if/34ContratoCesionIfAcusePDF.do?tipo_archivo=CONTRATO_CESION_IF&clave_solicitud="+clave_solicitud;
		document.forms[0].submit();
	}
	*/
	

	//Muestra el archivo de la Columna Contrato 		
	var mostrarContrato = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '34NotificacionesEpoVentanilla.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContrato
		});			
	}
	
	

	
	//GENERAR ARCHIVO TODO PDF
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		//GENERAR ARCHIVO TODO CSV
	var procesarSuccessFailureCSV =  function(opts, success, response) {
		var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');
		btnImprimirCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnImprimirCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  clasificacionEpo = jsonData.clasificacionEpo;	
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			
			
			if(clasificacionEpo !=''){				
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), false);	
			}
			
			if(hayCamposAdicionales=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='2'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='3'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='4'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='5'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.campo05);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
			}
					
			
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnImprimirCSV = Ext.getCmp('btnImprimirCSV');	
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			
			var el = gridConsulta.getGridEl();						
			if(store.getTotalCount() > 0) {			
				btnImprimirPDF.enable();
				btnImprimirCSV.enable();					
				btnBajarPDF.hide();
				btnBajarCSV.hide();		
				el.unmask();
			} else {	
				btnImprimirPDF.disable();
				btnImprimirCSV.disable();
				btnBajarPDF.hide();
				btnBajarCSV.hide();			
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var catalogoEstatusData = new Ext.data.JsonStore({
		id: 'catalogoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34NotificacionesEpoVentanilla.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '34NotificacionesEpoVentanilla.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoPymeData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '34NotificacionesEpoVentanilla.data.jsp',
		baseParams: {
			informacion: 'CatalogoPyme'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var catalogoPlazoContratoData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave', 'descripcion','loadMsg'],
		url: '34NotificacionesEpoVentanilla.data.jsp',
		baseParams: {
			informacion: 'CatalogoPlazo'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34NotificacionesEpoVentanilla.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
			fields: [
					{name: 'NOMBRE_CESINARIO'},
					{name: 'NOMBRE_PYME'},
					{name: 'RFC'},
					{name: 'NO_PROVEEDOR'},
					{name: 'FECHA_SOLICITUD'},
					{name: 'NUMERO_CONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'TIPO_CONTRATACION'},
					{name: 'F_INI_CONTRATO'},
					{name: 'F_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION_EPO'},
					{name: 'CAMPO1'},
					{name: 'CAMPO2'},
					{name: 'CAMPO3'},
					{name: 'CAMPO4'},
					{name: 'CAMPO5'},
					{name: 'VENTANILLA_DE_PAGO'},
					{name: 'SUPERVISOR'},
					{name: 'TELEFONO'},
					{name: 'OBJETO_CONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'MONTO_CREDITO'},
					{name: 'REFERENCIA'},
					{name: 'F_VENCIMIENTO_CRE'},
					{name: 'BANCO_DEPOSITO'},
					{name: 'CUENTA'},
					{name: 'CUENTA_CLABE'},
					{name: 'ESTATUS'},
					{name: 'IC_ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'CLAVE_PYME'},
					{name: 'FIRMA_CONTRATO'},
					{name: 'GRUPO_CESION'}					
					
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
		
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	 
		id: 'gridConsulta',
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		title: 'Consulta Notificaciones',
		columns: [			
			{
				header: 'Pyme(Cedente)', 
				tooltip: 'Pyme(Cedente)',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'RFC', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'No.Proveedor', 
				tooltip: 'No.Proveedor',
				dataIndex: 'NO_PROVEEDOR',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Fecha de Solicitud PyME', 
				tooltip: 'Fecha de Solicitud PyME',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'No.Contrato', 
				tooltip: 'No.Contrato',
				dataIndex: 'NUMERO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda', 
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tipo de Contrataci�n', 
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'F. Inicio Contrato', 
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'F_INI_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,
				
				align: 'center'
			},
			{
				header: 'F. Final Contrato', 
				tooltip: 'F. Final Contrato',
				dataIndex: 'F_FIN_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Plazo del Contrato', 
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Clasificaci�n EPO', 
				tooltip: 'Clasificaci�n EPO',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO1', 
				tooltip: 'CAMPO1',
				dataIndex: 'CAMPO1',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO2', 
				tooltip: 'CAMPO2',
				dataIndex: 'CAMPO2',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO3', 
				tooltip: 'CAMPO3',
				dataIndex: 'CAMPO3',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO4', 
				tooltip: 'CAMPO4',
				dataIndex: 'CAMPO4',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'CAMPO5', 
				tooltip: 'CAMPO5',
				dataIndex: 'CAMPO5',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: 'Ventanilla de Pago', 
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENTANILLA_DE_PAGO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra', 
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUPERVISOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Tel�fono', 
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},				
			{
				header: 'Objeto del Contrato', 
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Comentarios', 
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
						xtype: 'actioncolumn',
							header: 'Poderes IF',
							tooltip: 'Poderes IF',
							width: 130,
							align: 'center',					
							items: [
								{
									getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';										
									}
									,	handler: procesarPoderesIF
								}
							]				
						},	
			{
				xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
				dataIndex: 'CONTRATO',
				align: 'center',
				resiazable: true,
				width: 120,
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(registro.get('CLAVE_SOLICITUD')!=''){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler: mostrarContrato
					}
				]
			},	
				{
				xtype: 'actioncolumn',
				header: 'Contrato de Cesi�n',
				tooltip: 'Contrato de Cesi�n',
				dataIndex: 'CONTRATO_CESION',
				align: 'center',
				resiazable: true,
				width: 120,
				items: [
					{
						getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
							if(registro.get('CLAVE_SOLICITUD')!=''){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler: descargarContratoCesion
					}
				]
			},		
			{
				xtype: 'actioncolumn',
				header: 'Poderes',
				tooltip: 'Poderes',
		      width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						}
						,	handler: VerPoderesPyme
					}
				]				
			},
			{
				header: 'Monto del Cr�dito', 
				tooltip: 'Monto del Cr�dito',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Referencia /No. Cr�dito', 
				tooltip: 'Referencia /No. Cr�dito',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Fecha Vencimiento Cr�dito', 
				tooltip: 'Fecha Vencimiento Cr�dito',
				dataIndex: 'F_VENCIMIENTO_CRE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Banco de Dep�sito', 
				tooltip: 'Banco de Dep�sito',
				dataIndex: 'BANCO_DEPOSITO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Cuenta', 
				tooltip: 'Cuenta',
				dataIndex: 'CUENTA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Cuenta CLABE', 
				tooltip: 'Cuenta CLABE',
				dataIndex: 'CUENTA_CLABE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Estatus', 
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				xtype: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					var  mostrarNA;
					if(registro.get('IC_ESTATUS') !=16  ){
						 return 'N/A ';
					}
				},
				items: [
					{ //para Aceptar 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						if(registro.get('IC_ESTATUS') == 16 ){
							this.items[0].tooltip = 'Redireccionar';
							return 'icoAceptar';											
						}		
					}
					,handler: procesarRedireccionar				
				},
				{ //para Aceptar 
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {	
						if(registro.get('IC_ESTATUS') == 16 ){
							this.items[1].tooltip = 'Rechazar Contrato';
							return 'icoRechazar';											
						}		
					}
					,handler: procesarRechazar					
				}
			]
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {		
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,				
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnImprimirPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '34NotificacionesEpoVentanilla.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								tipo_archivo: 'PDF',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize
							})
							,callback: procesarSuccessFailurePDF
						});
					}
				},					
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnImprimirCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34NotificacionesEpoVentanilla.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								tipo_archivo: 'CSV'
							})
							,callback: procesarSuccessFailureCSV
						});
					}
				},					
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}
			]
		}
	});
	
			
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'claveIF',
			id: 'claveIF1',
			fieldLabel: 'Nombre del Cesionario',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'claveIF',
			emptyText: 'Seleccione un Cesionario...',
			forceSelection: false,
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			allowBlank: true,
			store: catalogoIFData,
			tpl: NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'cmbPyme',
			id: 'cmbPyme1',
			fieldLabel: 'PYME',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'cmbPyme',
			emptyText: 'Seleccionar una PYME...',
			forceSelection: false,
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			allowBlank: true,
			store: catalogoPymeData,
			tpl: NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'clave_estatus_sol',
			id: 'clave_estatus_sol2',
			fieldLabel: 'Estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_estatus_sol',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEstatusData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vigencia de Contrato',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'fechaVigenciaIni',
					id: 'fechaVigenciaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaVigenciaFin',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){
							Ext.getCmp("plazoCont1").setValue('');
							Ext.getCmp("cmbPlazoCont1").setValue('');
						}
					}
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaVigenciaFin',
					id: 'fechaVigenciaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fechaVigenciaIni',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){
							Ext.getCmp("plazoCont1").setValue('');
							Ext.getCmp("cmbPlazoCont1").setValue('');
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo del Contrato',
			id: 'plazoContrato',
			items : [
				{
					xtype: 'numberfield',
					name: 'plazoCont',
					id: 'plazoCont1',
					hiddenName : 'plazoCont',
					maxLength:10,
					listeners:{
						blur: function(field){
							Ext.getCmp("fechaVigenciaFin").setValue('');
							Ext.getCmp("fechaVigenciaIni").setValue('');
						}
					}
				},
				{
					xtype: 'combo',
					name: 'cmbPlazoCont',
					id: 'cmbPlazoCont1',
					mode: 'local',
					emptyText: 'Seleccionar...',
					valueField: 'clave',
					displayField: 'descripcion',
					hiddenName : 'cmbPlazoCont',
					fieldLabel: 'Plazo del Contrato',
					disabled: false,
					forceSelection : false,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,		
					store: catalogoPlazoContratoData,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners: {
						select: {
							fn: function(combo) {
								Ext.getCmp("fechaVigenciaFin").setValue('');
								Ext.getCmp("fechaVigenciaIni").setValue('');					
							}
						}
					}
				}				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud PyME',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'fechaSolicitudIni',
					id: 'fechaSolicitudIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaSolicitudFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaSolicitudFin',
					id: 'fechaSolicitudFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fechaSolicitudIni',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Contrato',
			id: 'numContrato2',
			items : [
				{
					xtype: 'textfield',
					name: 'numContrato',
					id: 'numContrato1',
					hiddenName : 'numContrato',
					maxLength:10
				}
			]
		}	
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {					
				
				
				var fechaVigenciaIni = Ext.getCmp('fechaVigenciaIni');
				var fechaVigenciaFin = Ext.getCmp('fechaVigenciaFin');
				var fechaSolicitudIni = Ext.getCmp('fechaSolicitudIni');
				var fechaSolicitudFin = Ext.getCmp('fechaSolicitudFin');
				var plazoCont1 = Ext.getCmp('plazoCont1');
				var cmbPlazoCont1 = Ext.getCmp('cmbPlazoCont1');
				
				
				if(  (!Ext.isEmpty(fechaVigenciaIni.getValue()) &&  Ext.isEmpty(fechaVigenciaFin.getValue()) )  
						|| ( Ext.isEmpty(fechaVigenciaIni.getValue()) &&  !Ext.isEmpty(fechaVigenciaFin.getValue())  )  ){
					fechaVigenciaIni.markInvalid('Debe capturar ambas Fecha de Vigencia de Contrato o dejarlas en blanco');					
					fechaVigenciaFin.markInvalid('Debe capturar ambas Fecha de Vigencia de Contrato o dejarlas en blanco');
					return;
				}
				
				if(  (!Ext.isEmpty(fechaSolicitudIni.getValue()) &&  Ext.isEmpty(fechaSolicitudFin.getValue()) )  
						|| ( Ext.isEmpty(fechaSolicitudIni.getValue()) &&  !Ext.isEmpty(fechaSolicitudFin.getValue())  )  ){
					fechaSolicitudIni.markInvalid('Debe capturar ambas Fecha de Solicitud PyME o dejarlas en blanco');					
					fechaSolicitudFin.markInvalid('Debe capturar ambas Fecha de Solicitud PyME o dejarlas en blanco');
					return;
				}
				if(  (!Ext.isEmpty(plazoCont1.getValue())   &&  Ext.isEmpty(cmbPlazoCont1.getValue())   ) ) { 
					cmbPlazoCont1.markInvalid('El valor de el Tipo de Plazo del Contrato es requerido.');			
					return;
				}
				
					var ventana2 = Ext.getCmp('VerPoderesPyme2');
					if (ventana2) {	
						ventana2.destroy();	
					}						
					var ventana = Ext.getCmp('VerEnvioCorreo');
					if (ventana) {	
						ventana.destroy();	
					}	
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar', 								
							start: 0,
							limit: 15
						})
					});
							
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34NotificacionesEpoVentanillaExt.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
	catalogoIFData.load();
	catalogoPymeData.load();
	catalogoPlazoContratoData.load();
	catalogoEstatusData.load();
});