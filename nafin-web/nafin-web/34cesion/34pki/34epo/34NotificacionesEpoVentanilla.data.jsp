<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,	
		com.netro.cesion.*,
		java.text.*, 		
		com.netro.exception.NafinException,
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray,		
		netropology.utilerias.usuarios.*,		
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
	String claveIF = (request.getParameter("claveIF")!=null)?request.getParameter("claveIF"):"";
	String cmbPyme = (request.getParameter("cmbPyme")!=null)?request.getParameter("cmbPyme"):"";
	String fechaVigenciaIni = (request.getParameter("fechaVigenciaIni")!=null)?request.getParameter("fechaVigenciaIni"):"";
	String fechaVigenciaFin = (request.getParameter("fechaVigenciaFin")!=null)?request.getParameter("fechaVigenciaFin"):"";
	String fechaSolicitudIni = (request.getParameter("fechaSolicitudIni")!=null)?request.getParameter("fechaSolicitudIni"):"";
	String fechaSolicitudFin = (request.getParameter("fechaSolicitudFin")!=null)?request.getParameter("fechaSolicitudFin"):"";
	String cmbPlazoCont = (request.getParameter("cmbPlazoCont")!=null)?request.getParameter("cmbPlazoCont"):"";
	String plazoCont = (request.getParameter("plazoCont")!=null)?request.getParameter("plazoCont"):"";
	String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
	String pantalla = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";
	String tipoOperacion  = (request.getParameter("tipoOperacion")!=null)?request.getParameter("tipoOperacion"):"";
	String observaciones  = (request.getParameter("observaciones")!=null)?request.getParameter("observaciones"):"";
	String numContrato  = (request.getParameter("numContrato")!=null)?request.getParameter("numContrato"):"";
	String claveEstatus  = (request.getParameter("clave_estatus_sol")!=null)?request.getParameter("clave_estatus_sol"):"";
	
	StringBuffer montosPorMoneda = new StringBuffer();	
	String nombre_cesinario =  "", nombre_pyme =  "", rfc =  "", no_proveedor =  "",  fecha_solicitud ="", numero_contrato =  "", tipo_contratacion ="",
	f_ini_contrato =  "", f_fin_contrato =  "", plazo_contrato = "", clasificacion_epo = "", campo1  = "", campo2 = "", 	campo3 = "", 	campo4  = "", 	
   campo5 = "", ventanilla_de_pago = "",  supervisor = "", 	telefono = "",  objeto_contrato = "", comentarios =  "",  monto_credito = "",  referencia = "", 	
	f_vencimiento_cre= "", 	banco_deposito = "", cuenta = "", cuenta_clabe = "", 	estatus = "", 	 ic_estatus = "", clave_Pyme ="",firmaContrato="",empresas = "";
	
					
	int start =0;
	int limit =0;
	JSONObject 	resultado	= new JSONObject(); 
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	String infoRegresar = "";
	String campo01="", campo02 ="",campo03 ="",campo04 ="", campo05 ="";	
	
	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

	String clasificacionEpo = cesionBean.clasificacionEpo(iNoCliente);
	Vector lovDatos = cesionBean.getCamposAdicionales(iNoCliente, "5");	
	int indiceCamposAdicionales = lovDatos.size();
	for(int i = 0; i < indiceCamposAdicionales; i++){
		List campos =(List)lovDatos.get(i);
		if(i==0) 	campo01=  campos.get(1).toString();
		if(i==1) 	campo02=  campos.get(1).toString();
		if(i==2) 	campo03=  campos.get(1).toString();
		if(i==3) 	campo04=  campos.get(1).toString();
		if(i==4) 	campo05=  campos.get(1).toString();			
	}				
									
	
	if(informacion.equals("CatalogoIF")){
		CatalogoIF catalogo = new CatalogoIF();
		catalogo.setClave("ic_if");
		catalogo.setDescripcion("cg_razon_social");
		catalogo.setG_cs_cesion_derechos("S");
		infoRegresar = catalogo.getJSONElementos();
		
	}else if(informacion.equals("CatalogoPyme")){
	
		CatalogoPymeCesion cat = new CatalogoPymeCesion();
		cat.setCampoClave("pyme.ic_pyme");
		cat.setCampoDescripcion("pyme.cg_razon_social");		
		cat.setClaveEpo(iNoCliente);
		cat.setOrden("pyme.cg_razon_social");
		infoRegresar = cat.getJSONElementos();
		
	}	else if (informacion.equals("CatalogoPlazo")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_tipo_plazo");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("cdercat_tipo_plazo");
		catalogo.setOrden("ic_tipo_plazo");	
		infoRegresar = catalogo.getJSONElementos();
	
	}else if(informacion.equals("CatalogoEstatus")){

		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus_solic");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("cdercat_estatus_solic");
		catalogo.setOrden("ic_estatus_solic");
		catalogo.setValoresCondicionIn("16,17", Integer.class);		
		infoRegresar = catalogo.getJSONElementos();	
	
	System.out.println("infoRegresar***** "+infoRegresar);


}	else if (informacion.equals("Consultar") || informacion.equals("ArchivoPDF") || informacion.equals("ArchivoCSV")){
	
		ConsultaNotificacionesEPOVentanilla paginador  = new  ConsultaNotificacionesEPOVentanilla();		
		
		paginador.setLoginUsuario("");
		paginador.setClaveEpo(iNoCliente);
		paginador.setClaveIf(claveIF);
		paginador.setClavePyme(cmbPyme);
		paginador.setClaveEstatusSol("16");
		paginador.setNumeroContrato(numContrato);
		paginador.setFechaVigenciaIni(fechaVigenciaIni);
		paginador.setFechaVigenciaFin(fechaVigenciaFin);
		paginador.setFechaSolicitudIni(fechaSolicitudIni);
		paginador.setFechaSolicitudFin(fechaSolicitudFin);
		paginador.setPlazoContrato(plazoCont);
		paginador.setClaveTipoPlazo(cmbPlazoCont);
		paginador.setClaveEstatusSol(claveEstatus);
			
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		Registros registrosC =null;
		
		if(informacion.equals("Consultar")   ) {
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				
				
				String  consulta  = queryHelper.getJSONPageResultSet(request,start,limit);	
				registrosC 	= queryHelper.getPageResultSet(request,start,limit);
				
				
				while (registrosC.next()) {
				
				
					montosPorMoneda = cesionBean.getMontoMoneda(registrosC.getString("CLAVE_SOLICITUD"));		
					
					nombre_cesinario =  (registrosC.getString("NOMBRE_CESINARIO")==null)?"":registrosC.getString("NOMBRE_CESINARIO");
					nombre_pyme =  (registrosC.getString("NOMBRE_PYME")==null)?"":registrosC.getString("NOMBRE_PYME");
					empresas =  (registrosC.getString("EMPRESAS")==null)?"":registrosC.getString("EMPRESAS");
					//FODEA-024-2014 MOD()
					String cedenteAux[] = nombre_pyme.split(";");
					String cedente = nombre_pyme+((empresas!="")?";"+empresas:"");
					cedente = cedente.replaceAll(";","<br>");
					registrosC.setObject("NOMBRE_PYME",cedente.replaceAll(";","<br>"));	
					/*if(cedenteAux.length == 1){
						nombre_pyme = nombre_pyme.replaceAll(";","");
						registrosC.setObject("NOMBRE_PYME",nombre_pyme);	
					}else{
						nombre_pyme = nombre_pyme.replaceAll(";","<br>");
						registrosC.setObject("NOMBRE_PYME",nombre_pyme);	
					}*/
					//					
					rfc =  (registrosC.getString("RFC")==null)?"":registrosC.getString("RFC");
					no_proveedor =  (registrosC.getString("NO_PROVEEDOR")==null)?"":registrosC.getString("NO_PROVEEDOR");
					fecha_solicitud =  (registrosC.getString("FECHA_SOLICITUD")==null)?"":registrosC.getString("FECHA_SOLICITUD");
					numero_contrato =  (registrosC.getString("NUMERO_CONTRATO")==null)?"":registrosC.getString("NUMERO_CONTRATO");
					tipo_contratacion =  (registrosC.getString("TIPO_CONTRATACION")==null)?"":registrosC.getString("TIPO_CONTRATACION");
					f_ini_contrato =  (registrosC.getString("F_INI_CONTRATO")==null)?"":registrosC.getString("F_INI_CONTRATO");
					f_fin_contrato =  (registrosC.getString("F_FIN_CONTRATO")==null)?"":registrosC.getString("F_FIN_CONTRATO");
					plazo_contrato =  (registrosC.getString("PLAZO_CONTRATO")==null)?"":registrosC.getString("PLAZO_CONTRATO");				
					clasificacion_epo =  (registrosC.getString("CLASFIFICACION_EPO")==null)?"":registrosC.getString("CLASFIFICACION_EPO");
					campo1 =  (registrosC.getString("CAMPO1")==null)?"":registrosC.getString("CAMPO1");
					campo2 =  (registrosC.getString("CAMPO2")==null)?"":registrosC.getString("CAMPO2");
					campo3 =  (registrosC.getString("CAMPO3")==null)?"":registrosC.getString("CAMPO3");
					campo4 =  (registrosC.getString("CAMPO4")==null)?"":registrosC.getString("CAMPO4");
					campo5 =  (registrosC.getString("CAMPO5")==null)?"":registrosC.getString("CAMPO5");			
					ventanilla_de_pago =  (registrosC.getString("VENTANILLA_DE_PAGO")==null)?"":registrosC.getString("VENTANILLA_DE_PAGO");
					supervisor =  (registrosC.getString("SUPERVISOR")==null)?"":registrosC.getString("SUPERVISOR");
					telefono =  (registrosC.getString("TELEFONO")==null)?"":registrosC.getString("TELEFONO");
					objeto_contrato =  (registrosC.getString("OBJETO_CONTRATO")==null)?"":registrosC.getString("OBJETO_CONTRATO");
					comentarios =  (registrosC.getString("COMENTARIOS")==null)?"":registrosC.getString("COMENTARIOS");
					monto_credito =  (registrosC.getString("MONTO_CREDITO")==null)?"":registrosC.getString("MONTO_CREDITO");
					referencia =  (registrosC.getString("REFERENCIA")==null)?"":registrosC.getString("REFERENCIA");
					f_vencimiento_cre =  (registrosC.getString("F_VENCIMIENTO_CRE")==null)?"":registrosC.getString("F_VENCIMIENTO_CRE");
					banco_deposito =  (registrosC.getString("BANCO_DEPOSITO")==null)?"":registrosC.getString("BANCO_DEPOSITO");
					cuenta =  (registrosC.getString("CUENTA")==null)?"":registrosC.getString("CUENTA");
					cuenta_clabe =  (registrosC.getString("CUENTA_CLABE")==null)?"":registrosC.getString("CUENTA_CLABE");
					estatus =  (registrosC.getString("ESTATUS")==null)?"":registrosC.getString("ESTATUS");
					clave_solicitud =  (registrosC.getString("CLAVE_SOLICITUD")==null)?"":registrosC.getString("CLAVE_SOLICITUD");
					ic_estatus =  (registrosC.getString("IC_ESTATUS")==null)?"":registrosC.getString("IC_ESTATUS");						
					clave_Pyme =  (registrosC.getString("CLAVE_PYME")==null)?"":registrosC.getString("CLAVE_PYME");
					firmaContrato=  (registrosC.getString("FIRMA_CONTRATO")==null)?"":registrosC.getString("FIRMA_CONTRATO");
					String grupoCesion  =  (registrosC.getString("GRUPO_CESION")==null)?"":registrosC.getString("GRUPO_CESION");
					 
					datos = new HashMap();
					datos.put("NOMBRE_CESINARIO", nombre_cesinario);
					datos.put("NOMBRE_PYME", cedente);
					datos.put("RFC", rfc);
					datos.put("NO_PROVEEDOR", no_proveedor);
					datos.put("FECHA_SOLICITUD", fecha_solicitud);
					datos.put("NUMERO_CONTRATO", numero_contrato);
					datos.put("MONTO_MONEDA", montosPorMoneda.toString());
					datos.put("TIPO_CONTRATACION", tipo_contratacion);
					datos.put("F_INI_CONTRATO", f_ini_contrato);
					datos.put("F_FIN_CONTRATO", f_fin_contrato);					
					datos.put("PLAZO_CONTRATO", plazo_contrato);					
					datos.put("CLASIFICACION_EPO", clasificacion_epo);				
					datos.put("CAMPO1", campo1);
					datos.put("CAMPO2", campo2);
					datos.put("CAMPO3",campo3);
					datos.put("CAMPO4",campo4);
					datos.put("CAMPO5", campo5);
					datos.put("VENTANILLA_DE_PAGO", ventanilla_de_pago);
					datos.put("SUPERVISOR", supervisor);
					datos.put("TELEFONO", telefono);
					datos.put("OBJETO_CONTRATO", objeto_contrato);
					datos.put("COMENTARIOS", comentarios);
					datos.put("MONTO_CREDITO", monto_credito);
					datos.put("REFERENCIA", referencia);
					datos.put("F_VENCIMIENTO_CRE", f_vencimiento_cre);
					datos.put("BANCO_DEPOSITO", banco_deposito);
					datos.put("CUENTA", cuenta);
					datos.put("CUENTA_CLABE", cuenta_clabe);
					datos.put("ESTATUS", estatus);	
					datos.put("CLAVE_SOLICITUD", clave_solicitud);	
					datos.put("IC_ESTATUS", ic_estatus);	
					datos.put("CLAVE_PYME", clave_Pyme);
					datos.put("FIRMA_CONTRATO", firmaContrato);
					datos.put("GRUPO_CESION", grupoCesion);
					
					registros.add(datos);									
				}				
				
				String consulta2 =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
				resultado = JSONObject.fromObject(consulta2);
				resultado.put("clasificacionEpo", clasificacionEpo);	
				resultado.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
				resultado.put("campo01",campo01);
				resultado.put("campo02",campo02);
				resultado.put("campo03",campo03);
				resultado.put("campo04",campo04);
				resultado.put("campo05",campo05);			
				infoRegresar = resultado.toString();
				
			} catch(Exception e) {
					throw new AppException("Error en la paginacion", e);
			}
		
		} else 	if (informacion.equals("ArchivoPDF")) {		
		
			try {
				try {
					start = Integer.parseInt(request.getParameter("start"));
					limit = Integer.parseInt(request.getParameter("limit"));
				} catch(Exception e) {
					throw new AppException("Error en los parametros recibidos", e);
				}
			
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
			
				System.out.println("nombreArchivo    "+nombreArchivo);
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
				
				System.out.println("infoRegresar    "+infoRegresar); 
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}	
			
		} else 	if (informacion.equals("ArchivoCSV")) {	
		
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}	
		}
	
	} else 	if (informacion.equals("CONTRATO")) {	
		try {
			String 			nombreArchivo = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);			
			//String nombreArchivo2 = nombreArchivo.substring(nombreArchivo.lastIndexOf("on/")+3, nombreArchivo.length());	
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			//jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo2);
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}	
		
	
	} else 	if (informacion.equals("IniciaCorreo")) {	
	
		List correo = cesionBean.IniEnvioCorreo(iNoCliente, clave_solicitud);
		String nombreArchivo=null;
			if (cesionBean.getBanderaContrato(clave_solicitud).equals("S")){
				nombreArchivo = cesionBean.descargaContratoCesionNuevo(clave_solicitud,strDirectorioTemp);
			}else{
				nombreArchivo = cesionBean.generarContrato(clave_solicitud,strDirectorioTemp);
			}
		StringBuffer sellos = new StringBuffer();
		sellos.append("<table align='center' width='750' border='0' cellspacing='1' cellpadding='1'>"+
					"<embed src='"+strDirecVirtualTemp+nombreArchivo+"' width='770' height='500'></embed>"+
					"</table>");
		for(int i = 0; i< correo.size(); i++){	
			List info = (List)correo.get(i);	
			
			resultado.put("success", new Boolean(true));
			resultado.put("lblNombrePyme", (String)info.get(0));	
			resultado.put("lblRFC",(String)info.get(1));
			resultado.put("lblRep1",(String)info.get(2));
			resultado.put("lblRep2",(String)info.get(3));
			resultado.put("lblnumProveedor",(String)info.get(4));
			resultado.put("lblFechaSolic",(String)info.get(5));
			resultado.put("lblnumContrato",(String)info.get(6));
			resultado.put("lblmontoMoneda",(String)info.get(7));
			resultado.put("lblFeVigencia",info.get(8).toString().equals("N/A")?"N/A":(info.get(8) + " a "+ info.get(9)));			
			resultado.put("lblPlazoCon",(String)info.get(10));
			resultado.put("lblTipoContra",(String)info.get(11));
			resultado.put("lblObContrato",(String)info.get(12));
			resultado.put("lblCampo1",(String)info.get(13));
			resultado.put("lblCampo2",(String)info.get(14));
			resultado.put("lblCampo3",(String)info.get(15));
			resultado.put("lblCampo4",(String)info.get(16));
			resultado.put("lblCampo5",(String)info.get(17));
			resultado.put("lblnombre1",(String)info.get(18));
			resultado.put("lblPersAutorizo",(String)info.get(19));
			resultado.put("lblnombre2",(String)info.get(20));
			resultado.put("lblBancoDep",(String)info.get(21));
			resultado.put("lblCuentaDep",(String)info.get(22));
			resultado.put("lblCuentaCLABEDep",(String)info.get(23));
			resultado.put("lblFSolicConse",(String)info.get(24));
			resultado.put("lblFConseEPO",(String)info.get(25));
			resultado.put("lblOtoConsen",(String)info.get(26));
			resultado.put("lblFormConsen",(String)info.get(27));
			resultado.put("lblNomFirmantePyme",(String)info.get(28));
			resultado.put("lblNomFirmanteIF",(String)info.get(29));
			resultado.put("lblTestigo1",(String)info.get(30));
			resultado.put("lblTestigo2",(String)info.get(31));
			resultado.put("lblFnotiAcepEpo",(String)info.get(32));
			resultado.put("lblPerAcepNoti",(String)info.get(33));
			resultado.put("lblFInfoVenta",(String)info.get(34));
			resultado.put("lblFRepcVenta",(String)info.get(35));
			resultado.put("lblFApliRedirec",(String)info.get(36));
			resultado.put("lblPersoRedirec",(String)info.get(37));
			resultado.put("clave_solicitud",clave_solicitud);
			resultado.put("nombreArchivo",nombreArchivo);
			resultado.put("sellos",sellos.toString());
			
		}
		infoRegresar = resultado.toString();		
		
	}  else 	if (informacion.equals("ConsultaPreAcuse")) {	
	
		HashMap consultaNotificaciones = cesionBean.consultaNotificacionEpo(clave_solicitud);
		
		int num_registros = Integer.parseInt((String)consultaNotificaciones.get("indice"));
		for(int i = 0; i < num_registros; i++){
			String MontoMoneda ="";
			nombre_cesinario = 	consultaNotificaciones.get("nombreIf")==null?"":consultaNotificaciones.get("nombreIf").toString();
			nombre_pyme = 	consultaNotificaciones.get("nombrePyme")==null?"":consultaNotificaciones.get("nombrePyme").toString();;
			empresas = 	consultaNotificaciones.get("empresas")==null?"":consultaNotificaciones.get("empresas").toString();;
				//FODEA-024-2014 MOD()
				String cedenteAux[] = nombre_pyme.split(";");
				String cedente = nombre_pyme+((empresas != null)?";"+empresas:"");
				
				//		
			rfc = consultaNotificaciones.get("rfc")==null?"":consultaNotificaciones.get("rfc").toString();
			no_proveedor = consultaNotificaciones.get("numeroProveedor")==null?"":consultaNotificaciones.get("numeroProveedor").toString();
			fecha_solicitud = consultaNotificaciones.get("fechaSolicitudPyme")==null?"":consultaNotificaciones.get("fechaSolicitudPyme").toString();
			numero_contrato= consultaNotificaciones.get("numeroContrato")==null?"":consultaNotificaciones.get("numeroContrato").toString();
			MontoMoneda= consultaNotificaciones.get("montosPorMoneda")==null?"":consultaNotificaciones.get("montosPorMoneda").toString();
			tipo_contratacion = consultaNotificaciones.get("tipoContratacion")==null?"":consultaNotificaciones.get("tipoContratacion").toString();
			f_ini_contrato = consultaNotificaciones.get("fechaInicioContrato")==null?"":consultaNotificaciones.get("fechaInicioContrato").toString();
			f_fin_contrato= consultaNotificaciones.get("fechaFinContrato")==null?"":consultaNotificaciones.get("fechaFinContrato").toString();
			plazo_contrato= consultaNotificaciones.get("plazoContrato")==null?"":consultaNotificaciones.get("plazoContrato").toString();
			clasificacion_epo	= consultaNotificaciones.get("clasificacionEpo")==null?"":consultaNotificaciones.get("clasificacionEpo").toString();
			campo1  =consultaNotificaciones.get("campoAdicional1")==null?"":consultaNotificaciones.get("campoAdicional1").toString();
			campo2  =consultaNotificaciones.get("campoAdicional2")==null?"":consultaNotificaciones.get("campoAdicional2").toString();
			campo3  =consultaNotificaciones.get("campoAdicional3")==null?"":consultaNotificaciones.get("campoAdicional3").toString();
			campo4  =consultaNotificaciones.get("campoAdicional4")==null?"":consultaNotificaciones.get("campoAdicional4").toString();
			campo5  =consultaNotificaciones.get("campoAdicional5")==null?"":consultaNotificaciones.get("campoAdicional5").toString();
			ventanilla_de_pago  =consultaNotificaciones.get("venanillaPago")==null?"":consultaNotificaciones.get("venanillaPago").toString();
			supervisor  =consultaNotificaciones.get("supAdmResob")==null?"":consultaNotificaciones.get("supAdmResob").toString();
			telefono  =consultaNotificaciones.get("numeroTelefono")==null?"":consultaNotificaciones.get("numeroTelefono").toString();
			objeto_contrato  =consultaNotificaciones.get("objetoContrato")==null?"":consultaNotificaciones.get("objetoContrato").toString();
			comentarios  =consultaNotificaciones.get("comentarios")==null?"":consultaNotificaciones.get("comentarios").toString();
			monto_credito  =consultaNotificaciones.get("montoCredito")==null?"":consultaNotificaciones.get("montoCredito").toString();
			referencia  =consultaNotificaciones.get("numeroReferencia")==null?"":consultaNotificaciones.get("numeroReferencia").toString();
			f_vencimiento_cre  =consultaNotificaciones.get("fechaVencimiento")==null?"":consultaNotificaciones.get("fechaVencimiento").toString();
			banco_deposito  =consultaNotificaciones.get("bancoDeposito")==null?"":consultaNotificaciones.get("bancoDeposito").toString();
			cuenta  =consultaNotificaciones.get("numeroCuenta")==null?"":consultaNotificaciones.get("numeroCuenta").toString();
			cuenta_clabe  =consultaNotificaciones.get("clabe")==null?"":consultaNotificaciones.get("clabe").toString();
			observaciones  =consultaNotificaciones.get("observacionesRechazo")==null?"":consultaNotificaciones.get("observacionesRechazo").toString();
			firmaContrato=consultaNotificaciones.get("firma_contrato")==null?"":consultaNotificaciones.get("firma_contrato").toString();
			if(!pantalla.equals("Acuse") ) {
				if(tipoOperacion.equals("ACEPTA")){
				estatus  ="Redireccionamiento Aplicado";
				}  else  if(tipoOperacion.equals("RECHAZO")){ 
					estatus  ="No Aceptado Ventanilla";
				}
			}else {
				estatus  =consultaNotificaciones.get("estatusSolicitud")==null?"":consultaNotificaciones.get("estatusSolicitud").toString();
			}
	
			datos = new HashMap();
			datos.put("NOMBRE_CESINARIO", nombre_cesinario);
			//datos.put("NOMBRE_PYME", nombre_pyme);
			datos.put("NOMBRE_PYME", cedente.replaceAll(";","<br>"));
			datos.put("RFC", rfc);
			datos.put("NO_PROVEEDOR", no_proveedor);
			datos.put("FECHA_SOLICITUD", fecha_solicitud);
			datos.put("NUMERO_CONTRATO", numero_contrato);
			datos.put("MONTO_MONEDA", MontoMoneda);
			datos.put("TIPO_CONTRATACION", tipo_contratacion);
			datos.put("F_INI_CONTRATO", f_ini_contrato);
			datos.put("F_FIN_CONTRATO", f_fin_contrato);					
			datos.put("PLAZO_CONTRATO", plazo_contrato);					
			datos.put("CLASIFICACION_EPO", clasificacion_epo);				
			datos.put("CAMPO1", campo1);
			datos.put("CAMPO2", campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5", campo5);
			datos.put("VENTANILLA_DE_PAGO", ventanilla_de_pago);
			datos.put("SUPERVISOR", supervisor);
			datos.put("TELEFONO", telefono);
			datos.put("OBJETO_CONTRATO", objeto_contrato);
			datos.put("COMENTARIOS", comentarios);
			datos.put("MONTO_CREDITO", monto_credito);
			datos.put("REFERENCIA", referencia);
			datos.put("F_VENCIMIENTO_CRE", f_vencimiento_cre);
			datos.put("BANCO_DEPOSITO", banco_deposito);
			datos.put("CUENTA", cuenta);
			datos.put("CUENTA_CLABE", cuenta_clabe);
			datos.put("ESTATUS", estatus);	
			datos.put("CLAVE_SOLICITUD", clave_solicitud);				
			datos.put("CLAVE_PYME", clave_Pyme);
			datos.put("OBSERVACIONES", observaciones);			
			datos.put("FIRMA_CONTRATO", firmaContrato);			
			registros.add(datos);	
		}
	
		String consulta2 =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		resultado = JSONObject.fromObject(consulta2);
		resultado.put("clasificacionEpo", clasificacionEpo);	
		resultado.put("hayCamposAdicionales",String.valueOf(indiceCamposAdicionales));
		resultado.put("campo01",campo01);
		resultado.put("campo02",campo02);
		resultado.put("campo03",campo03);
		resultado.put("campo04",campo04);
		resultado.put("campo05",campo05);			
		infoRegresar = resultado.toString();
			
	} else 	if (informacion.equals("ConfirmaAcuse")) {		
		%><%@ include file="../certificado.jspf" %><%
		String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
		String textoPlano = (request.getParameter("textoFirmar")!=null)?request.getParameter("textoFirmar"):"";
			
		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formatoCert = new SimpleDateFormat("ddMMyyyyHHmmss");
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat formatoHora = new SimpleDateFormat("hh:mm aa");
		String folioCert = formatoCert.format(calendario.getTime());
		String fechaCarga = formatoFecha.format(calendario.getTime());
		String horaCarga = formatoHora.format(calendario.getTime());		
		String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");
		String nombreUsuario = (String)request.getSession().getAttribute("strNombreUsuario");
		String _acuse ="";	
		HashMap parametros_firma = new HashMap();
		parametros_firma.put("_serial", _serial);
		parametros_firma.put("pkcs7", pkcs7);
		parametros_firma.put("textoPlano", textoPlano);
		parametros_firma.put("validCert", String.valueOf(false));
		parametros_firma.put("folioCert", folioCert);
		parametros_firma.put("claveSolicitud", clave_solicitud);
		parametros_firma.put("fechaCarga", fechaCarga);
		parametros_firma.put("horaCarga", horaCarga);
		parametros_firma.put("loginUsuario", loginUsuario);
		parametros_firma.put("nombreUsuario", nombreUsuario);
		parametros_firma.put("tipoOperacion", tipoOperacion);
		
		System.out.println("_serial    "+_serial);
		System.out.println("pkcs7    "+pkcs7);
		System.out.println("textoPlano    "+textoPlano);
		System.out.println("folioCert    "+folioCert);
			
		if(tipoOperacion.equals("ACEPTA")){
			_acuse = cesionBean.acuseNotifEpoVentRedireccionamiento(parametros_firma);	
			
			if(!_acuse.equals("")){	
			
				String claveEpo = iNoCliente;
				String nombreRemitente = (String)request.getSession().getAttribute("strNombreUsuario");			
				String txtEmail = (request.getParameter("txtEmail")!=null)?request.getParameter("txtEmail"):"";
				comentarios = (request.getParameter("lblComentarios1")!=null)?request.getParameter("lblComentarios1"):"";
				String numeroContrato = (request.getParameter("numeroContrato")!=null)?request.getParameter("numeroContrato"):"";
				String nombreArchivo = (request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";
				
				String correoDe =  "cadenas@nafin.gob.mx";
				String correosEnviados = "N";			
				txtEmail = txtEmail.replaceAll("\n", "");
				txtEmail = txtEmail.replaceAll("\t", "");
				txtEmail = txtEmail.replaceAll(" ", "");
	
				UtilUsr utilUsr = new UtilUsr();
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(claveEpo, "E");
				
				StringTokenizer strTokenizerCorreos = new StringTokenizer(txtEmail, ";");
				
				while (strTokenizerCorreos.hasMoreTokens()) {
					String correoDestinatario = strTokenizerCorreos.nextToken();
					String nombreDestinatario = "";
	
					for (int i = 0; i < usuariosPorPerfil.size(); i++) {
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						if (correoDestinatario.equals(usuarioEpo.getEmail())) {
							nombreDestinatario = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
						}
					}
					
					HashMap parametrosEnvioCorreo = new HashMap();
					parametrosEnvioCorreo.put("numeroContrato",numeroContrato);
					parametrosEnvioCorreo.put("claveEpo", claveEpo);
					parametrosEnvioCorreo.put("claveSolicitud", clave_solicitud);
					parametrosEnvioCorreo.put("nombreRemitente", nombreRemitente);
					parametrosEnvioCorreo.put("nombreDestinatario", nombreDestinatario);
					parametrosEnvioCorreo.put("correoDestinatario", correoDestinatario);
					parametrosEnvioCorreo.put("comentarios", comentarios);
					parametrosEnvioCorreo.put("nombreArchivo",strDirectorioTemp+nombreArchivo);
					cesionBean.enviarCorreoRedireccionamiento(parametrosEnvioCorreo);
					correosEnviados = "S";
		
				}
				if(correosEnviados.equals("S")){
					System.out.println(" El Correo de Notificación a Ventanilla fué enviado exitosamente.")  ;
				}
		
			}	
			
			
			
			
		}else if(tipoOperacion.equals("RECHAZO")){ 
			_acuse = cesionBean.acuseNotifEpoVentRechazo(parametros_firma,observaciones);
		}
		
		resultado.put("success", new Boolean(true));
		resultado.put("acuse", _acuse);		
		resultado.put("fecha", fechaCarga);		
		resultado.put("hora", horaCarga);		
		resultado.put("nombreUsuario", nombreUsuario);	
		resultado.put("clave_solicitud", clave_solicitud);	
		resultado.put("tipoOperacion", tipoOperacion);	
		infoRegresar = resultado.toString();				
	
	
	} else 	if (informacion.equals("ArchivoAcuse")) {	
	
	String acuse  = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";		
	String fecha  = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):"";		
	String hora  = (request.getParameter("hora")!=null)?request.getParameter("hora"):"";		
	String nombreUsuario  = (request.getParameter("nombreUsuario")!=null)?request.getParameter("nombreUsuario"):"";		
	String claveIf  = (request.getParameter("claveIf")!=null)?request.getParameter("claveIf"):"";	
	String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");
	String pais = (String)request.getSession().getAttribute("strPais");
	String noCliente = (String)request.getSession().getAttribute("iNoCliente");
	String nombre = (String)request.getSession().getAttribute("strNombre");
	String nombreUsr = (String)request.getSession().getAttribute("strNombreUsuario");
	String logo = (String)request.getSession().getAttribute("strLogo");
	
	HashMap parametros = new HashMap();
	parametros.put("clave_solicitud", clave_solicitud);
	parametros.put("claveEpo", iNoCliente);
	parametros.put("acuse", acuse);
	parametros.put("hora", hora);
	parametros.put("fecha", fecha);
	parametros.put("nombreUsuario", nombreUsuario);	
	parametros.put("loginUsuario", loginUsuario);	
	parametros.put("claveIf", claveIf);	
	parametros.put("strDirectorioTemp", strDirectorioTemp);	
	parametros.put("tipoOperacion", tipoOperacion);	
	parametros.put("loginUsuario", loginUsuario);	
	parametros.put("pais", pais);	
	parametros.put("noCliente", noCliente);	
	parametros.put("nombre", nombre);	
	parametros.put("nombreUsr", nombreUsr);	
	parametros.put("logo", logo);	
	parametros.put("strDirectorioPublicacion",strDirectorioPublicacion);	
			
	String  nombreArchivo =  cesionBean.archNotifiEPO ( parametros);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
	
}	
	
	
%>
<%=infoRegresar%>

<% System.out.println("infoRegresar    "+infoRegresar); %>