Ext.onReady(function(){
	Ext.QuickTips.init();
//----------------------------------HANDLERS------------------------------------
	var procesarActualizacion = function(opts, success, response){
		var btnAceptar = Ext.getCmp('btnActualizar');
		btnAceptar.setIconClass('aceptar');
		btnAceptar.enable();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){  
					window.location = '34MensajeNotificacionext.jsp';	 
			});
		}else{
			Ext.Msg.alert('Mensaje de Error',"�Error al actualizar!",function(btn){  
					window.location = '34MensajeNotificacionext.jsp';	 
			});
		}
	}
//----------------------------------ELEMENTOS---------------------------------	
	var elementosForma = [{
									xtype: 'textarea',
									name: 'mensaje',
									id: 'txtMensaje',
									allowBlank: true,
									maxLength: 500,
									maxLengthText: 'El tama�o del Mensaje Predeterminado por Cesion de Derechos supera los caracteres permitidos (500)',
									minLength: 1,
									msgTarget: 'side',
									width: 200
	}];
	var fp = new Ext.FormPanel({
		id: 'forma',
		style: 'margin:0px auto;',
		title: 'Mensaje Predeterminado por Cesion de Derechos',
		hidden: false,
		frame: true,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		labelWidth: 15,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		width: 700,
		items: elementosForma,
		monitorValid: false,
		buttons: [
				{
					text: 'Actualizar',
					id: 'btnActualizar',
					iconCls: 'aceptar',
					formBind: true,
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');						
						var txtMensaje = Ext.getCmp('txtMensaje');
						var mensaje = txtMensaje.getValue();
						if(Ext.isEmpty(txtMensaje.getValue())){
							boton.setIconClass('aceptar');
							boton.enable();
							txtMensaje.markInvalid('El  Mensaje Predeterminado por Cesion de Derechos es requerido.');
							txtMensaje.focus();
							return;
						}
						if(mensaje.length > 500){
							boton.setIconClass('aceptar');
							boton.enable();
							txtMensaje.markInvalid('El tama�o del Mensaje Predeterminado por Cesion de Derechos supera los caracteres permitidos (500).');
							txtMensaje.focus();
							return;
						}
						Ext.Ajax.request({
							url: '34MensajeNotificacionext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Actualiza'
							}),
							callback: procesarActualizacion
						});
					}
				},
				{
					text: 'Limpiar',
					id: 'btnLimpiar',
					iconCls: 'icoLimpiar',
					formBind: true,
					handler: function(boton, evento){
						window.location = '34MensajeNotificacionext.jsp'
					}
				}
		]
	});
//--------------------------------PRINCIPAL-------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
					fp
		]
	});
});