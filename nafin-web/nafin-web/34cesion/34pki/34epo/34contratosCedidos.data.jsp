<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.cesion.*,
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoPYME,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/34cesion/34secsession_ext.jspf"%>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

log.debug("informacion = <"+informacion+">");

if (informacion.equals("Parametrizacion")) {	//	*-*-*-*-*-*--*-*-*-*-*-*-*-*	Parametrizacion	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


}else if (informacion.equals("CatalogoIf")) {

	CatalogoIF cat = new CatalogoIF();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setCesionDerechos("S");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoNombrePyme")){

	CatalogoPYME cat = new CatalogoPYME();
	cat.setCampoClave("ic_pyme");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setCesionDerechos("S");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consultar") || informacion.equals("ArchivoCSV")){

	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

	String cesionario	= (request.getParameter("cesionario")!=null)?request.getParameter("cesionario"):"";
	String noContrato	= (request.getParameter("noContrato")!=null)?request.getParameter("noContrato"):"";
	String ic_pyme		= (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";

	int start = 0;
	int limit = 0;

	com.netro.cesion.ContratosCedidosEPO pag =  new com.netro.cesion.ContratosCedidosEPO();

	pag.setClaveCesionario(	cesionario	);
	pag.setClaveContrato(	noContrato	);
	pag.setClaveEpo(			iNoCliente	);
	pag.setClavePyme(			ic_pyme		);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pag);

	if (informacion.equals("Consultar") ) {

		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}

			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

	}else if (informacion.equals("ArchivoCSV")	){
		try {
			String nombreArchivo	= queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj	= new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			jsonObj.put("estadoSiguiente", "DESCARGAR_REPORTE");
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
}else if (informacion.equals("CapturaSaveContrato")) {

	String claveIF		= (request.getParameter("cesionario")!=null)?request.getParameter("cesionario"):"";
	String noContrato	= (request.getParameter("noContrato")!=null)?request.getParameter("noContrato"):"";
	String clavePyme	= (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
	String estadoSiguiente	= "RESPUESTA_GUARDAR_CONTRATO";
	
	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject jsonObj	= new JSONObject();

	cesionBean.capturaContratosCedidos(iNoCliente, claveIF, clavePyme, noContrato);

	// Especificar el estado siguiente
	jsonObj.put("estadoSiguiente", estadoSiguiente );
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

} else if ( informacion.equals("ConsultaContrato.eliminarContrato") 					) {

	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject	resultado			= new JSONObject();
	String		estadoSiguiente	= "ACTUALIZAR_CONSULTA";

	String claveContrato	=	(request.getParameter("claveContrato")	== null)?"":request.getParameter("claveContrato");

	cesionBean.eliminaContratoCedido(iNoCliente, claveContrato);

	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", estadoSiguiente );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(true)		);

	infoRegresar = resultado.toString();

}else if(informacion.equals("BuscaContratoCedido") ){

	String claveContrato		= (request.getParameter("claveContrato")!=null)?request.getParameter("claveContrato"):"";
	String clavePyme		= (request.getParameter("clavePyme")!=null)?request.getParameter("clavePyme"):"";
	String estadoSiguiente	= "RESPUESTA_BUSCAR_CONTRATO_CEDIDO";

	CesionEJB cesionBean		= ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject 	resultado	= new JSONObject();

	boolean existe = cesionBean.existeNumeroContratoCedido(claveContrato, iNoCliente);
	resultado.put("existe", new Boolean(existe));

	resultado.put("claveContrato",	claveContrato	);
	resultado.put("clavePyme",			clavePyme		);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", estadoSiguiente );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(true)		);

	infoRegresar = resultado.toString();

}else if(informacion.equals("BuscaContratoSolicitud") ){

	String claveContrato	= (request.getParameter("claveContrato")!=null)?request.getParameter("claveContrato"):"";
	String clavePyme		= (request.getParameter("clavePyme")!=null)?request.getParameter("clavePyme"):"";
	String estadoSiguiente	= "RESPUESTA_BUSCAR_CONTRATO_SOLICITUD";

	CesionEJB cesionBean		= ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject 	resultado	= new JSONObject();

	boolean existe = cesionBean.existeNumeroContrato(clavePyme, claveContrato, iNoCliente);
	resultado.put("existe", new Boolean(existe));

	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", estadoSiguiente );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(true)		);

	infoRegresar = resultado.toString();

}else if(informacion.equals("BuscaContratoSolicitudConsulta") ){

	String claveContrato	= (request.getParameter("claveContrato")!=null)?request.getParameter("claveContrato"):"";
	String clavePyme		= (request.getParameter("clavePyme")!=null)?request.getParameter("clavePyme"):"";
	String estadoSiguiente	= "RESPUESTA_CONSULTA_CONTRATO_SOLICITUD";
	String mensaje = "";
	CesionEJB cesionBean		= ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject 	resultado	= new JSONObject();
	boolean existe = false;

	try{
		existe = cesionBean.existeNumeroContrato(clavePyme, claveContrato, iNoCliente);
		resultado.put("existe", new Boolean(existe));
		log.debug("cesionBean.existeNumeroContrato = <" + existe + ">"); 
	}catch(Exception e){
		log.error("BuscaContratoSolicitudConsulta(Exception): Error al buscar informacion");
		e.printStackTrace();
	}

	if(existe){
		mensaje = "El contrato está siendo cedido electrónicamente";
	}
	resultado.put("mensaje", mensaje);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", estadoSiguiente );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(true)		);

	infoRegresar = resultado.toString();


}
//String		estadoSiguiente	= "ACTUALIZAR_CONSULTA";
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>