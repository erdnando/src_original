<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.io.File,
		java.text.*,
		java.sql.*, 
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.pdf.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.cesion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String claveSolicitud = (request.getParameter("claveSolicitud")!=null)?request.getParameter("claveSolicitud"):"";
String infoRegresar = "";
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	
	if(informacion.equals("valoresIniciales")  ){
			String clasificacionEpo = cesionBean.clasificacionEpo(iNoCliente);
		String campoAdicional_1 = "";
		String campoAdicional_2 = "";
		String campoAdicional_3 = "";
		String campoAdicional_4 = ""; 
		String campoAdicional_5 = "";
		HashMap camposAdicionales = cesionBean.getCamposAdicionalesParametrizados(iNoCliente, "9");
		int indice = Integer.parseInt((String)camposAdicionales.get("indice"));
		for(int i = 0; i < indice; i++){
			if(i==0)
				campoAdicional_1 = (String)camposAdicionales.get("nombre_campo_"+i);
			if(i==1)
				campoAdicional_2 = (String)camposAdicionales.get("nombre_campo_"+i);
			if(i==2)
				campoAdicional_3 = (String)camposAdicionales.get("nombre_campo_"+i);
			if(i==3)
				campoAdicional_4 = (String)camposAdicionales.get("nombre_campo_"+i);
			if(i==4)
				campoAdicional_5 = (String)camposAdicionales.get("nombre_campo_"+i);
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("clasificacionEpo",clasificacionEpo);
		jsonObj.put("campoAdicional_1",campoAdicional_1);
		jsonObj.put("campoAdicional_2",campoAdicional_2);
		jsonObj.put("campoAdicional_3",campoAdicional_3);
		jsonObj.put("campoAdicional_4",campoAdicional_4);
		jsonObj.put("campoAdicional_5",campoAdicional_5);
		infoRegresar = jsonObj.toString();
	}
	else if(informacion.equals("CatalogoIF")){
		CatalogoIF catalogo = new CatalogoIF();
		catalogo.setClave("ic_if");
		catalogo.setDescripcion("cg_razon_social");
		catalogo.setG_cs_cesion_derechos("S");
		infoRegresar = catalogo.getJSONElementos();
	}
	else if(informacion.equals("CatalogoPyme")){
	
		CatalogoPymeCesion cat = new CatalogoPymeCesion();
		cat.setCampoClave("pyme.ic_pyme");
		cat.setCampoDescripcion("pyme.cg_razon_social");		
		cat.setClaveEpo(iNoCliente);
		cat.setOrden("pyme.cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	}
	else if(informacion.equals("CatalogoCambioEstatus")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus_solic");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("cdercat_estatus_solic");
		catalogo.setValoresCondicionIn("9,11,12,15,16,17",Integer.class);
		catalogo.setOrden("ic_estatus_solic");
		infoRegresar = catalogo.getJSONElementos();
	}
	else if (informacion.equals("CatalogoPlazo")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_tipo_plazo");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("cdercat_tipo_plazo");
		catalogo.setOrden("ic_tipo_plazo");	
		infoRegresar = catalogo.getJSONElementos();
	}
	else if (informacion.equals("VentanillaDestino")){
		CatalogoVentanilla catalogo = new CatalogoVentanilla();
		catalogo.setClave("ic_ventanilla");
		catalogo.setDescripcion("cg_descripcion");
		catalogo.setSeleccion(iNoCliente);
		List elementos = catalogo.getListaElementos();
		JSONArray jsonArray1 = JSONArray.fromObject(elementos);
		infoRegresar =  "{\"success\": true, \"total\": 1, \"registros\": " + jsonArray1.toString() + "}";
	}
	else if (informacion.equals("cambiaEstatus_notifica_a_ventanilla")){
		
	}
	else if (informacion.equals("Consulta")||informacion.equals("ArchivoPDF")||informacion.equals("ArchivoCSV")){
		String ic_if 						= (request.getParameter("ic_if"))!=null?request.getParameter("ic_if"):"";
		String ic_pyme						= (request.getParameter("ic_pyme"))!=null?request.getParameter("ic_pyme"):"";
		String clave_estatus				= (request.getParameter("clave_estatus"))!=null?request.getParameter("clave_estatus"):"";
		String df_fecha_vigencia_de	= (request.getParameter("df_fecha_vigencia_de"))!=null?request.getParameter("df_fecha_vigencia_de"):"";
		String df_fecha_vigencia_a		= (request.getParameter("df_fecha_vigencia_a"))!=null?request.getParameter("df_fecha_vigencia_a"):"";
		String plazo_Contrato			= (request.getParameter("plazo_Contrato"))!=null?request.getParameter("plazo_Contrato"):"";
		String clave_plazo_contrato	= (request.getParameter("clave_plazo_contrato"))!=null?request.getParameter("clave_plazo_contrato"):"";
		String df_fecha_solicitud_de	= (request.getParameter("df_fecha_solicitud_de"))!=null?request.getParameter("df_fecha_solicitud_de"):"";
		String df_fecha_solicitud_a	= (request.getParameter("df_fecha_solicitud_a"))!=null?request.getParameter("df_fecha_solicitud_a"):"";
		String numeroContrato			= (request.getParameter("numeroContrato"))!=null?request.getParameter("numeroContrato"):"";

		com.netro.cesion.ConsultaNotificacionesCesionDerechosEPO paginador = new com.netro.cesion.ConsultaNotificacionesCesionDerechosEPO();
		paginador.setLoginUsuario(strLogin);
		paginador.setClaveEpo(iNoCliente);
		paginador.setClaveIf(ic_if);
		paginador.setClavePyme(ic_pyme);
		paginador.setClaveEstatusSol(clave_estatus);
		paginador.setFechaVigenciaIni(df_fecha_vigencia_de);
		paginador.setFechaVigenciaFin(df_fecha_vigencia_a);
		paginador.setPlazoContrato(plazo_Contrato);
		paginador.setClaveTipoPlazo(clave_plazo_contrato);
		paginador.setFechaSolicitudIni(df_fecha_solicitud_de);
		paginador.setFechaSolicitudFin(df_fecha_solicitud_a);
		paginador.setNumeroContrato(numeroContrato);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if (informacion.equals("Consulta")){
			try{
				Registros reg = queryHelper.doSearch();
				while(reg.next()){
					//FODEA-024-2014 MOD(aplicando formato)
					String cedente =(reg.getString("NOMBRE_PYME")==null)?"":(reg.getString("NOMBRE_PYME"));
					String []cedenteAux=cedente.split(";");
					if(cedenteAux.length == 1){
						reg.setObject("NOMBRE_PYME",cedente.replaceAll(";",""));
					}else{
						reg.setObject("NOMBRE_PYME",cedente.replaceAll(";","<BR>"));
					}
					
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(reg.getString("CLAVE_SOLICITUD"));
					reg.setObject("MONTO_MONEDA",montosMonedaSol.toString());
				
					//Se obtiene el representante legal de la Pyme
					String noProveedor = (reg.getString("CLAVE_PYME")==null)?"":(reg.getString("CLAVE_PYME"));
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for(int i=0;i<usuariosPorPerfil.size();i++){
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(i>0){
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
						nombreContacto.append("\n");
					}	
					reg.setObject("REPRESENTANTE_LEGAL",nombreContacto.toString());
				}
				infoRegresar = 
							"{\"success\": true, \"total\": \""+ reg.getNumeroRegistros() +"\", \"registros\": " + reg.getJSONData()+"}";
			}catch(Exception e){
				throw new AppException("Error en la obtención de los datos");
			}
		}
		else if(informacion.equals("ArchivoPDF")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
		else if (informacion.equals("ArchivoCSV")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo CSV",e);
			}
		}
	}
	else if(informacion.equals("ConsultaEnvio")||informacion.equals("Enviar")){
		
		String destinatario = (request.getParameter("txtEmail")!=null)?request.getParameter("txtEmail"):"";
		String comentarios = (request.getParameter("txtComentarios")!=null)?request.getParameter("txtComentarios"):"";
		String catVentanilla = (request.getParameter("catVentanilla")!=null)?request.getParameter("catVentanilla"):"";
		String fechaHoy		= "";
		try{
			fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		}catch(Exception e){
			fechaHoy = "";
		}		
		if(informacion.equals("ConsultaEnvio")){
			JSONArray jsonArray = new JSONArray();
			try{
				List datos = new ArrayList();
				datos = cesionBean.IniEnvioCorreo(iNoCliente, claveSolicitud);
				String nombreArchivo=null;
					if (cesionBean.getBanderaContrato(claveSolicitud).equals("S")){
						nombreArchivo = cesionBean.descargaContratoCesionNuevo(claveSolicitud,strDirectorioTemp);
					}else{
						nombreArchivo = cesionBean.generarContrato(claveSolicitud,strDirectorioTemp);
					}
				StringBuffer sellos = new StringBuffer();
				sellos.append("<table align='center' width='750' border='0' cellspacing='1' cellpadding='1'>"+
							"<embed src='"+strDirecVirtualTemp+nombreArchivo+"' width='770' height='500'></embed>"+
							"</table>");
				if(datos.size()>0){
					for(int i=0; i<datos.size();i++){
						List registros = (List)datos.get(i);
						
						JSONObject jsonObj0 = new JSONObject();
		
						jsonObj0.put("PYME",(String)registros.get(0));
						jsonObj0.put("RFC",(String)registros.get(1));
						jsonObj0.put("REPRESENTANTE_PYME_1",registros.get(2));
						jsonObj0.put("REPRESENTANTE_PYME_2",registros.get(3));
						jsonObj0.put("NUM_PROVEEDOR",registros.get(4));
						jsonObj0.put("SOLICITUD_CESION",registros.get(5));
						jsonObj0.put("NUM_CONTRATO",registros.get(6));
						jsonObj0.put("MONTO_MONEDA",registros.get(7));
						jsonObj0.put("FECHA_VIGENCIA",registros.get(8).toString().equals("N/A")?"N/A":(registros.get(8) + " a "+ registros.get(9)));
						jsonObj0.put("PLAZO_CONTRATO",registros.get(10));
						jsonObj0.put("TIPO_CONTRATACION",registros.get(11));
						jsonObj0.put("OBJETO_CONTRATO",registros.get(12));
						jsonObj0.put("CAMPO_ADICIONAL_1",registros.get(13));
						jsonObj0.put("CAMPO_ADICIONAL_2",registros.get(14));
						jsonObj0.put("CAMPO_ADICIONAL_3",registros.get(15));
						jsonObj0.put("CAMPO_ADICIONAL_4",registros.get(16));
						jsonObj0.put("CAMPO_ADICIONAL_5",registros.get(17));
						jsonObj0.put("NOMBRE_INFO_1",registros.get(18));
						jsonObj0.put("PERSONA_AUTORIZA_CESION",registros.get(19));
						jsonObj0.put("NOMBRE_INFO_2",registros.get(20));
						jsonObj0.put("BANCO_DEPOSITO",registros.get(21));
						jsonObj0.put("CUENTA",registros.get(22));
						jsonObj0.put("CLABE",registros.get(23));
						jsonObj0.put("FECHA_SOLICITUD_CONSENTIMIENTO",registros.get(24));
						jsonObj0.put("FECHA_CONSENTIMIENTO_EPO",registros.get(25));
						jsonObj0.put("PERSONA_OTORGO_CONSENTIMIENTO",registros.get(26));
						jsonObj0.put("FECHA_FORMALIZACION",registros.get(27));
						jsonObj0.put("NOMBRE_FIRMANTE_CEDENTE",registros.get(28));
						jsonObj0.put("NOMBRE_FIRMANTE_CESIONARIO",registros.get(29));
						jsonObj0.put("NOMBRE_TESTIGO_1",registros.get(30));
						jsonObj0.put("NOMBRE_TESTIGO_2",registros.get(31));
						jsonObj0.put("FIRMA_CONTRATO",registros.get(32));
						
						jsonObj0.put("NOMBRE_ARCHIVO",nombreArchivo);
						jsonObj0.put("SELLOS",sellos.toString());
						
						String fecha = (String)registros.get(32);
						if(fecha.equals(" ")){
							fecha = fechaHoy;
						}
						jsonObj0.put("FECHA_NOTIFICACION",fecha);
						String persona = (String)registros.get(33);
						if(persona.equals(" ")){
							persona = strNombreUsuario;
						}
						jsonObj0.put("PERSONA_NOTIFICACION",persona);
						String fechapagos = (String)registros.get(34);
						if(fechapagos.equals("")){
							fechapagos = fechaHoy;
						}
						jsonObj0.put("FECHA_INF_VENTANILLA",fechapagos);
						String fechaVentanilla = (String)registros.get(35);
						if(fechaVentanilla.equals("")){
							fechaVentanilla = fechaHoy;
						}
						jsonObj0.put("FECHA_REC_VENTANILLA",fechaVentanilla);
						jsonObj0.put("FECHA_APLIC_REDIREC",registros.get(36));
						jsonObj0.put("PERSONA_REDIREC_CUENTA",registros.get(37));
							
						jsonArray.add(jsonObj0);
					}
				}
				infoRegresar = 
									"{\"success\": true, \"total\": \""+datos.size()+"\", \"registros\": " + jsonArray.toString()+"}";					
			}catch(Exception e){
				throw new AppException("Error en la obtención de los datos");
			}
		}
		else if(informacion.equals("Enviar")){
		String nombreArchivo = (request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";
			try{
				String respuesta = cesionBean.EnviodeCorreo(iNoCliente, claveSolicitud, strNombreUsuario, destinatario, comentarios, catVentanilla,strDirectorioTemp+nombreArchivo);
				String banderaCorreo = cesionBean.banderaCorreo(claveSolicitud);
				if("E".equals(respuesta)){
					infoRegresar = "{\"success\": true, \"mensaje\": \"El Correo de Notificación a Ventanilla fué enviado exitosamente.\",\"banderaCorreo\": \"" + banderaCorreo + "\"}";
				}
			}catch(Throwable e){
				throw new AppException("Error en el envío de los datos", e);
			}			
		}
	}
	else if(informacion.equals("Acuse")){
		%><%@ include file="../certificado.jspf" %><%
		try{
			Calendar calendario = Calendar.getInstance();
			SimpleDateFormat formatoCert = new SimpleDateFormat("ddMMyyyyHHmmss");
			SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatoHora = new SimpleDateFormat("hh:mm aa");	
			
			String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
			String textoPlano = (request.getParameter("textoPlano")!=null)?request.getParameter("textoPlano"):"";
			String folioCert = formatoCert.format(calendario.getTime());
			String fechaCarga = formatoFecha.format(calendario.getTime());
			String horaCarga = formatoHora.format(calendario.getTime());
			String loginUsuario = (String)request.getSession().getAttribute("Clave_usuario");
			String nombreUsuario = (String)request.getSession().getAttribute("strNombreUsuario");
			String tipoOperacion = (request.getParameter("tipoOperacion")!=null)?request.getParameter("tipoOperacion"):"";

			String catVentanilla = (request.getParameter("catVentanilla")!=null)?request.getParameter("catVentanilla"):"";
			String causasRechazoNotificacion = (request.getParameter("causasRechazoNotificacion")!=null)?request.getParameter("causasRechazoNotificacion"):"";//FODEA 016 - 2011 ACF
			
			HashMap parametrosFirma = new HashMap();
			parametrosFirma.put("_serial", _serial);
			parametrosFirma.put("pkcs7", pkcs7);
			parametrosFirma.put("textoPlano", textoPlano);
			parametrosFirma.put("validCert", String.valueOf(false));
			parametrosFirma.put("folioCert", folioCert);
			parametrosFirma.put("claveSolicitud", claveSolicitud);
			parametrosFirma.put("fechaCarga", fechaCarga);
			parametrosFirma.put("horaCarga", horaCarga);
			parametrosFirma.put("loginUsuario", loginUsuario);
			parametrosFirma.put("nombreUsuario", nombreUsuario);
			parametrosFirma.put("tipoOperacion", tipoOperacion);
			parametrosFirma.put("catVentanilla", catVentanilla);
			parametrosFirma.put("causasRechazoNotificacion", causasRechazoNotificacion);//FODEA 016 - 2011 ACF

			String acuse = cesionBean.acuseFirmaNotificacionEpo(parametrosFirma);
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("acuse",acuse);
			jsonObj.put("folioCert",folioCert);
			jsonObj.put("fechaCarga",fechaCarga);
			jsonObj.put("horaCarga",horaCarga);
			jsonObj.put("claveSolicitud",claveSolicitud);
			jsonObj.put("tipoOperacion",tipoOperacion);
			jsonObj.put("strLogin",strLogin);
			jsonObj.put("strNombreUsuario",strNombreUsuario);
			infoRegresar = jsonObj.toString();
		}catch(Throwable e){
			throw new AppException("Error al intentar actualizar los datos", e);
		}
	}else if(informacion.equals("AcuseNotifica")){
		%><%@ include file="../certificado.jspf" %><%
		try{
			Calendar calendario = Calendar.getInstance();
			SimpleDateFormat formatoCert	= new SimpleDateFormat("ddMMyyyyHHmmss");
			SimpleDateFormat formatoFecha	= new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatoHora	= new SimpleDateFormat("hh:mm aa");

			String pkcs7			= (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
			String textoPlano		= (request.getParameter("textoPlano")!=null)?request.getParameter("textoPlano"):"";
			String folioCert		= formatoCert.format(calendario.getTime());
			String fechaCarga		= formatoFecha.format(calendario.getTime());
			String horaCarga		= formatoHora.format(calendario.getTime());
			String loginUsuario	= (String)request.getSession().getAttribute("Clave_usuario");
			String nombreUsuario	= (String)request.getSession().getAttribute("strNombreUsuario");
			String tipoOperacion	= (request.getParameter("tipoOperacion")!=null)?request.getParameter("tipoOperacion"):"";

			//String catVentanilla = (request.getParameter("catVentanilla")!=null)?request.getParameter("catVentanilla"):"";
			//String causasRechazoNotificacion = (request.getParameter("causasRechazo")!=null)?request.getParameter("causasRechazo"):"";//FODEA 016 - 2011 ACF

			HashMap parametrosFirma = new HashMap();
			parametrosFirma.put("_serial", _serial);
			parametrosFirma.put("pkcs7", pkcs7);
			parametrosFirma.put("textoPlano", textoPlano);
			parametrosFirma.put("validCert", String.valueOf(false));
			parametrosFirma.put("folioCert", folioCert);
			parametrosFirma.put("claveSolicitud", claveSolicitud);
			parametrosFirma.put("fechaCarga", fechaCarga);
			parametrosFirma.put("horaCarga", horaCarga);
			parametrosFirma.put("loginUsuario", loginUsuario);
			parametrosFirma.put("nombreUsuario", nombreUsuario);
			parametrosFirma.put("tipoOperacion", tipoOperacion);
			//parametrosFirma.put("catVentanilla", catVentanilla);
			//parametrosFirma.put("causasRechazoNotificacion", causasRechazoNotificacion);//FODEA 016 - 2011 ACF

			String acuse = cesionBean.acuseFirmaNotificacionEpo(parametrosFirma);
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("acuse",acuse);
			jsonObj.put("folioCert",folioCert);
			jsonObj.put("fechaCarga",fechaCarga);
			jsonObj.put("horaCarga",horaCarga);
			jsonObj.put("claveSolicitud",claveSolicitud);
			jsonObj.put("tipoOperacion",tipoOperacion);
			jsonObj.put("strLogin",strLogin);
			jsonObj.put("strNombreUsuario",strNombreUsuario);
			infoRegresar = jsonObj.toString();
		}catch(Throwable e){
			throw new AppException("Error al intentar actualizar los datos", e);
		}
	}else if(informacion.equals("ImprimirNotificaPreAcuse")){
		
		String acuse			= (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
		String fechaCarga		= (request.getParameter("fechaCarga")!=null)?request.getParameter("fechaCarga"):"";
		String horaCarga		= (request.getParameter("horaCarga")!=null)?request.getParameter("horaCarga"):"";
		String _strLogin		= (request.getParameter("strLogin")!=null)?request.getParameter("strLogin"):"";
		String _strNombreUsuario	= (request.getParameter("strNombreUsuario")!=null)?request.getParameter("strNombreUsuario"):"";
		String regs				= (request.getParameter("regs")!=null)?request.getParameter("regs"):"";

		JSONObject jsonObj = new JSONObject();
		
		JSONArray arrayJson = new JSONArray();
		arrayJson.add(JSONObject.fromObject(regs));

		try{
			String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual_a  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual_a.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual_a.substring(3,5))-1];
			String anioActual   = fechaActual_a.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

			HashMap camposAdicionales = cesionBean.getCamposAdicionalesParametrizados(iNoCliente, "9");
			int indice = Integer.parseInt((String)camposAdicionales.get("indice"));
			int cont = 1;
			float anchoCeldaA[] = {5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f};
			pdfDoc.setTable(18,100,anchoCeldaA);
			
			pdfDoc.setCell("A","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Intermediario Financiero (Cesionario)","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Pyme (Cedente)","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("No.Proveedor","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Representante legal","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de solicitud PYME","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("No.Contrato","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Firma Contrato","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto/Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Contratación","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("F. Inicio Contrato","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("F. Final Contrato","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo del Contrato","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Clasificación EPO","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Ventanilla de Pago","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Supervisor/Administrador/Residente de Obra","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Teléfono","celda01",ComunesPDF.CENTER);

			pdfDoc.setCell("B", "celda02", ComunesPDF.CENTER);
			for(int i = 0; i<indice; i++){
				pdfDoc.setCell(camposAdicionales.get("nombre_campo_"+i) + "", "celda02", ComunesPDF.CENTER);
				cont+=1;
			}
			pdfDoc.setCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Comentarios","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del Crédito","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Referencia/No. Crédito","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Vencimiento Crédito","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco de Déposito","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta CLABE","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Observaciones","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Causas de Rechazo de Notificación","celda02",ComunesPDF.CENTER);
			for(int i = 0; i<=(5-cont); i++){
				pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);
			}
			pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);

			for (int i = 0; i < arrayJson.size(); ++i) {
				JSONObject rec = arrayJson.getJSONObject(i);
				pdfDoc.setCell("A","formas", ComunesPDF.CENTER);

				pdfDoc.setCell(rec.getString("NOMBRE_IF"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("NOMBRE_PYME").replaceAll("<BR>","\n"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(rec.getString("RFC"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("NUMERO_PROVEEDOR"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("REPRESENTANTE_LEGAL"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("FECHA_SOL_PYME"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("NUMERO_CONTRATO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("FIRMA_CONTRATO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("MONTO_MONEDA").replaceAll("<br/>","\n"),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("TIPO_CONTRATACION"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("FECHA_INICIO_CONTRATO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("FECHA_FIN_CONTRATO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("PLAZO_CONTRATO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("CLASIFICACION_EPO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("VENTANILLA_PAGO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("SUP_ADM_RESOB"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("NUMERO_TELEFONO"), "formas", ComunesPDF.CENTER);
				
				pdfDoc.setCell("B", "formas", ComunesPDF.CENTER);
				for(int j = 0; j<indice;j++){
					pdfDoc.setCell(rec.getString("CAMPO_ADICIONAL_"+(j + 1))==null?"":rec.getString("CAMPO_ADICIONAL_"+(j + 1)) + "", "formas", ComunesPDF.LEFT);
				}
				pdfDoc.setCell(rec.getString("OBJETO_CONTRATO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("COMENTARIOS"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(rec.getString("MONTO_CREDITO"),2), "formasrep", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("NUMERO_REFERENCIA"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("FECHA_VENCIMIENTO"),"formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("BANCO_DEPOSITO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("NUMERO_CUENTA"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("CLABE"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("CAUSASRECHAZO"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("ESTATUS_SOLICITUD"), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rec.getString("CAUSAS_RECHAZO_NOTIF"), "formas", ComunesPDF.CENTER);
				for(int ii = 0; ii<=(5-cont); ii++){
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
				}
				pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			}
			pdfDoc.addTable();
			
			pdfDoc.setTable(2, 50);
			pdfDoc.setCell("Datos de cifras de control","celda01",ComunesPDF.CENTER,2);
			pdfDoc.setCell("Número de Acuse","formas",ComunesPDF.LEFT);
			pdfDoc.setCell(acuse,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell("Fecha","formas",ComunesPDF.LEFT);
			pdfDoc.setCell(fechaCarga,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell("Hora","formas",ComunesPDF.LEFT);
			pdfDoc.setCell(horaCarga,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell("Nombre y número de usuario","formas",ComunesPDF.LEFT);
			pdfDoc.setCell(_strLogin + " " + _strNombreUsuario,"formas",ComunesPDF.LEFT);
			pdfDoc.addTable();

			pdfDoc.endDocument();

			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

		} catch (Exception e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			//Termina proceso de imprimir
		}
		infoRegresar = jsonObj.toString();

	}else if(informacion.equals("CONTRATO_PDF")){
		JSONObject jsonObj = new JSONObject();
		String clave_solicitud = request.getParameter("clave_solicitud")==null?"":request.getParameter("clave_solicitud");
		String nombreArchivo = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("ACUSE_PDF")){
		JSONObject jsonObj = new JSONObject();
		CreaArchivo archivo = new CreaArchivo();
		String  acuse = "";
		StringBuffer clausuladoParametrizado = new StringBuffer();
		String claveEpo = iNoCliente;
		
		//claveSolicitud = request.getParameter("claveSolicitud")==null?"":request.getParameter("claveSolicitud");
			
		String acuseCarga = request.getParameter("acuseCarga")==null?"":request.getParameter("acuseCarga");
		String fechaCarga = request.getParameter("fechaCarga")==null?"":request.getParameter("fechaCarga");
		String horaCarga = request.getParameter("horaCarga")==null?"":request.getParameter("horaCarga");
		String tipoOperacion = request.getParameter("tipoOperacion")==null?"":request.getParameter("tipoOperacion");
		String loginUsuario = request.getParameter("Clave_usuario")==null?"":request.getParameter("Clave_usuario");
		String nombreUsuario = strNombreUsuario;
		
		HashMap camposAdicionalesEpo = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
		int indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesEpo.get("indice"));
		
		HashMap consultaNotificaciones = cesionBean.consultaNotificacionEpo(claveSolicitud);
		int num_registros = Integer.parseInt((String)consultaNotificaciones.get("indice"));
		String claveIf = consultaNotificaciones.get("claveIf")==null?"":(String)consultaNotificaciones.get("claveIf");
		
		if(!claveIf.equals("")){
			clausuladoParametrizado = cesionBean.consultaClausuladoParametrizado(claveIf);
		}
		
		String clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
		String nombreArchivo = archivo.nombreArchivo()+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
		
		String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
		String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
		String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
		String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
		String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
		
		pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String)application.getAttribute("strDirectorioPublicacion"), "");
		
		//pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		pdfDoc.addText("La autentificación se llevó a cabo con éxito \n Recibo: "+acuseCarga, "titulo", ComunesPDF.CENTER);
		//pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

		float anchoCeldaA[] = {5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f};
		pdfDoc.setTable(17, 100, anchoCeldaA);
		
		pdfDoc.setCell("A", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Intermediario Financiero (Cesionario)", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Pyme (Cedente)", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("RFC", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("No. Proveedor", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Representante legal", "celda02", ComunesPDF.CENTER);				
		pdfDoc.setCell("Fecha de Solicitud PyME", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("No. Contrato", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Firma Contrato", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto / Moneda", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo de Contratación", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Inicio Contrato", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Final Contrato", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo del Contrato", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell(clasificacionEpo, "celda02", ComunesPDF.CENTER);
		
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		
		pdfDoc.setCell("B", "celda02", ComunesPDF.CENTER);
		for(int i = 0; i < indiceCamposAdicionales; i++){pdfDoc.setCell(camposAdicionalesEpo.get("nombre_campo_"+i) + "", "celda02", ComunesPDF.CENTER);}
		pdfDoc.setCell("Ventanilla de Pago", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Supervisor/Administrador/Residente de Obra", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Teléfono", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Objeto del Contrato", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Comentarios", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto del Crédito", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Referencia / No. Crédito", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Vencimiento Crédito", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Banco de Depósito", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Cuenta", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Cuenta CLABE", "celda02", ComunesPDF.CENTER);
		for(int i = 0; i < 5 - indiceCamposAdicionales; i++){pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);}
		
		pdfDoc.setCell("C", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("Observaciones", "celda02", ComunesPDF.CENTER);
		if (!tipoOperacion.equals("ACEPTAR")) {
			pdfDoc.setCell("Causas de Rechazo Notificación", "celda02", ComunesPDF.CENTER);
		} else {
			pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		}
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
		pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);


		pdfDoc.setHeaders();
		
		for(int i = 0; i < num_registros; i++){
		
			//Se obtiene el representante Legal de la Pyme
				String Proveedor =  consultaNotificaciones.get("clavePyme")==null?"":(String)consultaNotificaciones.get("clavePyme");
				
			StringBuffer nombreContacto = new StringBuffer();
			UtilUsr utilUsr = new UtilUsr();
			boolean usuarioEncontrado = false;
			boolean perfilIndicado = false;
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(Proveedor, "P");
			
			for (int y = 0; y < usuariosPorPerfil.size(); y++) {
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);					
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+"  ");														
			}
			String pyme =consultaNotificaciones.get("nombrePyme")==null?"":consultaNotificaciones.get("nombrePyme").toString().replaceAll(",", "");
			String empresas =consultaNotificaciones.get("empresas")==null?"":consultaNotificaciones.get("empresas").toString().replaceAll(",", "");
			String cedente = pyme+""+((empresas != null)?";"+empresas:"");
			pdfDoc.setCell("A", "formas", ComunesPDF.CENTER);
			pdfDoc.setCell(consultaNotificaciones.get("nombreIf")==null?"":consultaNotificaciones.get("nombreIf").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(cedente.replaceAll(";","\n") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("rfc")==null?"":consultaNotificaciones.get("rfc").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("numeroProveedor")==null?"":consultaNotificaciones.get("numeroProveedor").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(nombreContacto.toString()+ "", "formas", ComunesPDF.LEFT);					
			pdfDoc.setCell(consultaNotificaciones.get("fechaSolicitudPyme")==null?"":consultaNotificaciones.get("fechaSolicitudPyme").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("numeroContrato")==null?"":consultaNotificaciones.get("numeroContrato").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("firma_contrato")==null?"":consultaNotificaciones.get("firma_contrato").toString(), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("montosPorMoneda")==null?"":consultaNotificaciones.get("montosPorMoneda").toString().replaceAll("<br/>", "\n") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("tipoContratacion")==null?"":consultaNotificaciones.get("tipoContratacion") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("fechaInicioContrato")==null?"":consultaNotificaciones.get("fechaInicioContrato") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("fechaFinContrato")==null?"":consultaNotificaciones.get("fechaFinContrato") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("plazoContrato")==null?"":consultaNotificaciones.get("plazoContrato") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("clasificacionEpo")==null?"":consultaNotificaciones.get("clasificacionEpo") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			
			pdfDoc.setCell("B", "formas", ComunesPDF.CENTER);
			for(int j = 0; j < indiceCamposAdicionales; j++){pdfDoc.setCell(consultaNotificaciones.get("campoAdicional"+(j + 1))==null?"":consultaNotificaciones.get("campoAdicional"+(j + 1)) + "", "formas", ComunesPDF.LEFT);}
			pdfDoc.setCell(consultaNotificaciones.get("venanillaPago")==null?"":consultaNotificaciones.get("venanillaPago").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("supAdmResob")==null?"":consultaNotificaciones.get("supAdmResob").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("numeroTelefono")==null?"":consultaNotificaciones.get("numeroTelefono").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("objetoContrato")==null?"":consultaNotificaciones.get("objetoContrato").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("cg_comentarios")==null?"":consultaNotificaciones.get("cg_comentarios").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("$" + consultaNotificaciones.get("montoCredito")==null?"0":consultaNotificaciones.get("montoCredito").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("numeroReferencia")==null?"":consultaNotificaciones.get("numeroReferencia").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("fechaVencimiento")==null?"":consultaNotificaciones.get("fechaVencimiento").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("bancoDeposito")==null?"":consultaNotificaciones.get("bancoDeposito").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("numeroCuenta")==null?"":consultaNotificaciones.get("numeroCuenta").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("clabe")==null?"":consultaNotificaciones.get("clabe").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			for(int j = 0; j < 5 - indiceCamposAdicionales; j++){pdfDoc.setCell("", "formas", ComunesPDF.CENTER);}
		
			pdfDoc.setCell("C", "formas", ComunesPDF.CENTER);
			pdfDoc.setCell(consultaNotificaciones.get("estatusSolicitud")==null?"":consultaNotificaciones.get("estatusSolicitud") + "", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell(consultaNotificaciones.get("causasRechazo")==null?"":consultaNotificaciones.get("causasRechazo").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			if (!tipoOperacion.equals("ACEPTAR")) {
				pdfDoc.setCell(consultaNotificaciones.get("causasRechazoNotificacion")==null?"":consultaNotificaciones.get("causasRechazoNotificacion").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
			} else {
				pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			}
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
			
			acuse = consultaNotificaciones.get("acuse5")==null?"":consultaNotificaciones.get("acuse5").toString();
		}
		pdfDoc.addTable();

		float anchoCelda1[] = {50f, 50f};
		pdfDoc.setTable(2, 50, anchoCelda1);
		
		pdfDoc.setCell("Cifras de Control", "celda02", ComunesPDF.CENTER, 2);
		
		pdfDoc.setCell("Número de Acuse", "formas", ComunesPDF.RIGHT);
		pdfDoc.setCell(acuse, "formas",ComunesPDF.LEFT);
		
		pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.RIGHT);
		pdfDoc.setCell(fechaCarga, "formas", ComunesPDF.LEFT);

		pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.RIGHT);
		pdfDoc.setCell(horaCarga, "formas", ComunesPDF.LEFT);

		pdfDoc.setCell("Usuario", "formas", ComunesPDF.RIGHT);
		pdfDoc.setCell(loginUsuario + " - " + nombreUsuario, "formas",ComunesPDF.LEFT);

		pdfDoc.addTable();
		
		float anchoCelda3[] = {100f};
		pdfDoc.setTable(1, 60, anchoCelda3);

		//pdfDoc.setCell(clausuladoParametrizado.toString(), "formas", ComunesPDF.RIGHT);

		pdfDoc.addTable();
		float anchoCelda4[] = {100f};	
		pdfDoc.newPage();
		pdfDoc.setTable(1, 70, anchoCelda4);
		pdfDoc.setCell("\n","formas",ComunesPDF.JUSTIFIED, 1, 1, 0);
		pdfDoc.setCell("Con base a lo dispuesto en el Código de Comercio por este conducto reciba la notificación sobre el contrato de Cesión de Derechos de Cobro que hemos formalizado con la empresa CEDENTE respecto al contrato que aquí mismo se detalla.", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("\n","formas",ComunesPDF.JUSTIFIED, 1, 1, 0);
		pdfDoc.setCell("Por lo anterior, se solicita a esa Entidad, se registre la Cesión de Derechos de Cobro de referencia a efecto de que realice el(los) pago(s) derivados del Contrato aquí señalado a favor del INTERMEDIARIO FINANCIERO (CESIONARIO), en la inteligencia de que éste, está de acuerdo con lo establecido en el Convenio Marco de Cesión Electrónica de Derechos  de Cobro.", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("\n","formas",ComunesPDF.JUSTIFIED, 1, 1, 0);
		pdfDoc.setCell("Los datos de la cuenta bancaria del CESIONARIO(s) en la que se deben depositar los pagos derivados de esta operación son los que aquí se detallan.","formas",ComunesPDF.CENTER);
		pdfDoc.setCell("\n","formas",ComunesPDF.JUSTIFIED, 1, 1, 0);
		pdfDoc.setCell("Sobre el particular y de conformidad con lo señalado en la emisión de la conformidad para ceder los derechos de cobro emitida por esa Entidad, por este medio les notifico, la Cesión de Derechos de Cobro del Contrato antes mencionado, para todos los efectos legales a que haya lugar, adjunto, en forma electrónica.","formas",ComunesPDF.CENTER);
		pdfDoc.setCell("\n","formas",ComunesPDF.JUSTIFIED, 1, 1, 0);

		pdfDoc.endDocument();
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	
	}
%>
<%=infoRegresar%>
