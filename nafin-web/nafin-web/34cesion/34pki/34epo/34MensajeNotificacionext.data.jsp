<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		com.netro.exception.*,
		com.netro.afiliacion.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.cesion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar = "";
	if(informacion.equals("Actualiza")){
		try{
			String actualiza = "";
			String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
		//Instanciacion para el uso del EJB
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);	
		//Metodo para actualizar el mensaje de Cesion de Derechos
			actualiza = cesionBean.MensajeNotiCesionDerechos(iNoCliente,mensaje);			
		//Metodo para mostrar el mensaje parametrizado 
			mensaje = cesionBean.MostrarMensajeNotiCesionDerechos(iNoCliente);
			infoRegresar = "{\"success\":true,\"mensaje\":\"El Mensaje Predeterminado por Cesion de Derechos fue Actualizado.\"}";
		}catch(Exception e){
			throw new AppException("Error al intentar actualizar el mensaje de Cesion de Derechos",e);
		}
	}	
%>
<%=infoRegresar%>