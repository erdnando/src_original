Ext.onReady(function(){
	var nombreArchivo;
	
	
	Ext.QuickTips.init();

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//-----------------------------------VARIABLES----------------------------------
	var jsonValoresIniciales = null;
	var elementosAcuse = null;
	var claveSolicitud = null;
	var catVentanilla = null;
	var acuse5 = null;
	var tipoOperacion = null;
	var textoFirmar = null;
	var pkcs7 = null;
	var banderaCorreo = null; 
	var indice = null;
	
//-----------------------------------FUNCIONES----------------------------------
// Poderes de la Pyme 
	var VerPoderesPyme = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme= registro.get('CLAVE_PYME');
		var rfcPyme = registro.get('RFC');	
		var grupo_cesion = registro.get('GRUPO_CESION');	
		var no_contrato = registro.get('NUMERO_CONTRATO');	
		
		var parametros = "clavePyme="+clavePyme+"&rfcPyme="+rfcPyme+"&grupo_cesion="+grupo_cesion+"&no_contrato="+no_contrato;
		
		var ventana = Ext.getCmp('VerPoderesPyme');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
					layout: 'fit',
					width: 400,
					height: 200,		
					style: 'margin:0 auto;',
					id: 'VerPoderesPyme',
					closeAction: 'hide',
					items: [					
						PanelPoderesPyme
					],
					title: 'Ver Poderes Pyme  '					
			}).show();
		}	
		
	var pabelBody = Ext.getCmp('PanelPoderesPyme').body;
		var mgr = pabelBody.getUpdater();
		mgr.on('failure', 
		function(el, response) {
			pabelBody.update('');
			NE.util.mostrarErrorResponse(response);
		});		
		mgr.update({
			url: '../34pyme/34SolicConsVerPoderesPymeExt.jsp?'+parametros,			
			scripts: true,			
			indicatorText: 'Cargando Ver Poderes Pyme '
		});					
	}
	
	var PanelPoderesPyme = new Ext.Panel({
		id: 'PanelPoderesPyme',
		width: 400,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	});
	
	//Fin Poderes.

function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarPoderesIF = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		//forma.action = '/nafin/00archivos/22cotizador/instrucciones.pdf';
					Ext.Ajax.request({
							url: '../../34DescargaPoderes.data.jsp',
							params: Ext.apply({informacion:'PoderesContratoCesion',clave_solicitud:clave_solicitud }),
							callback: procesarArchivoSuccess
						});
		}

	Ext.util.Format.comboRenderer = function(combo){
			return function(value,metadata,registro,rowIndex,colIndex,store){
				if(registro.get('CLAVE_ESTATUS')==11||registro.get('CLAVE_ESTATUS')==15){	//Modificaci�n por Hugo.
					if(value!=null){
						var record = combo.findRecord(combo.valueField, value);
						return record ? record.get(combo.displayField) : combo.valueNotFoundText;
					}else{
						return "Seleccionar..."
					}
				}else{
					return "N/A"
				}
			}
	}
	function firmar(){
		pkcs7 = NE.util.firmar(textoFirmar);
		if (Ext.isEmpty(pkcs7) || pkcs7 == 'error:noMatchingCert') {
			return false;//Error en la firma. Termina...
		}
		return true;
	}
//------------------------------------HANDLERS----------------------------------
//Muestra el archivo de la Columna Contrato de Cesion 
	var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '../34pyme/34ContratoCesionPDFCSV.jsp',
			params: Ext.apply({
				informacion: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});
	}
	//---------------------------------------------------------------------------


	function procesaValoresIniciales(opts,success,response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
		}
	}

	var procesaImprimirPreAcuse = function(opts, success, response){
		var btnImprime = Ext.getCmp('btnImprimirNotifica');
		btnImprime.setIconClass('icoPdf');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	//Pendiente
	var procesarAcuse =  function(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			elementosAcuse = Ext.util.JSON.decode(response.responseText);
			var lblAcuse = Ext.getCmp('lblAcuse');
			var lblFechaCarga = Ext.getCmp('lblFechaCarga');
			var lblHoraCarga = Ext.getCmp('lblHoraCarga');
			var lblUsuarioCaptura = Ext.getCmp('lblUsuarioCaptura');
			var lblRecibo = Ext.getCmp('lblRecibo');
			var formaAcuse = Ext.getCmp('formaAcuse');
			var formaTituloAceptacion = Ext.getCmp('formaTituloAceptacion');
			var forma = Ext.getCmp('forma');
			var grid = Ext.getCmp('grid');
			var gridPreacuse = Ext.getCmp('gridPreacuse');
			//gridPreacuse.getView().scroller.scrollTo('right', 6500, { easing: 'bounceOut' });
			if(elementosAcuse.acuse!=null&&elementosAcuse.acuse!=""){
				formaTituloAceptacion.show();
				formaAcuse.show();
				if(opts.params.informacion === 'Acuse'){
					Ext.getCmp('btnImprimir').show();
				}else{
					Ext.getCmp('btnImprimirNotifica').show();
				}
				lblRecibo.getEl().update(elementosAcuse.acuse);
				lblAcuse.getEl().update(elementosAcuse.acuse);
				lblFechaCarga.getEl().update(elementosAcuse.fechaCarga);
				lblHoraCarga.getEl().update(elementosAcuse.horaCarga);
				lblUsuarioCaptura.getEl().update(elementosAcuse.strLogin + " - " + elementosAcuse.strNombreUsuario);	
				
				var btnCancelar = Ext.getCmp('btnCancelar');	
				var btnNotificacionAcuse = Ext.getCmp('btnNotificacionAcuse');	
				btnNotificacionAcuse.hide();
				btnCancelar.hide();
			}else{
				Ext.Msg.alert('Mensaje informativo','La autentificaci�n no se llev� a cabo.PROCESO CANCELADO',function(btn){
					return;
				});
				forma.show();
				formaAcuse.hide();				
				grid.hide();
				gridPreacuse.hide();
			}
		}
	}
	var procesaEnvio = function(opts,success,response){
		var formaEnvio = Ext.getCmp('formaEnvio');
		var btnEnviar = Ext.getCmp('btnEnviar');
		var grid = Ext.getCmp('grid');
		btnEnviar.setIconClass('');
		btnEnviar.enable();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){
				formaEnvio.el.unmask();
				var ventana = Ext.getCmp('VerEnvioCorreo');
				banderaCorreo = Ext.util.JSON.decode(response.responseText).banderaCorreo;
				if(banderaCorreo=='S'){			
					if(ventana){
						ventana.hide();
					}
					preacuse(grid,indice);
				}else{
					Ext.Msg.alert('Mensaje informativo','El Correo no ha sido enviado',function(btn){
						if(ventana){
							ventana.show();
						}
					});
				}
				return;
			});
		}else{
			Ext.Msg.alert('Mensaje de Error',"�Error al intentar enviar los datos!",function(btn){
				formaEnvio.el.unmask();
				return;	 
			});
		}
	}
	var procesarConsultaDataEnvio = function(store,arrRegistros,opts){
		var lblNombrePyme = Ext.getCmp('lblNombrePyme');
		var lblRFC = Ext.getCmp('lblRFC');
		var lblRepresentantePyme1 = Ext.getCmp('lblRepresentantePyme1');
		var lblRepresentantePyme2 = Ext.getCmp('lblRepresentantePyme2');
		var lblNumProveedor = Ext.getCmp('lblNumProveedor');
		var lblFechaSolicitud = Ext.getCmp('lblFechaSolicitud');
		var lblNumContrato = Ext.getCmp('lblNumContrato');
		var lblMontoMoneda = Ext.getCmp('lblMontoMoneda');
		var lblFechaVigencia = Ext.getCmp('lblFechaVigencia');
		var lblPlazoContrato = Ext.getCmp('lblPlazoContrato');
		var lblTipoContratacion = Ext.getCmp('lblTipoContratacion');
		var lblObjetoContrato = Ext.getCmp('lblObjetoContrato');
		var lblCampoAdicional1 = Ext.getCmp('lblCampoAdicional1');
		var lblCampoAdicional2 = Ext.getCmp('lblCampoAdicional2');
		var lblCampoAdicional3 = Ext.getCmp('lblCampoAdicional3');
		var lblCampoAdicional4 = Ext.getCmp('lblCampoAdicional4');
		var lblCampoAdicional5 = Ext.getCmp('lblCampoAdicional5');
		var lblNombreInfo1 = Ext.getCmp('lblNombreInfo1');
		var lblPersonaAutCesion = Ext.getCmp('lblPersonaAutCesion');
		var lblNombreInfo2 = Ext.getCmp('lblNombreInfo2');
		var lblBancoDeposito = Ext.getCmp('lblBancoDeposito');
		var lblNumCuentaDeposito = Ext.getCmp('lblNumCuentaDeposito');
		var lblNumCuentaClabeDeposito = Ext.getCmp('lblNumCuentaClabeDeposito');
		var lblFechaSolicitudConsentimiento = Ext.getCmp('lblFechaSolicitudConsentimiento');
		var lblFechaConsentimiento = Ext.getCmp('lblFechaConsentimiento');
		var lblPersonaConsentimiento = Ext.getCmp('lblPersonaConsentimiento');
		var lblFechaFormalizacion = Ext.getCmp('lblFechaFormalizacion');
		var lblNombreFirmanteCedente = Ext.getCmp('lblNombreFirmanteCedente');
		var lblNombreFirmanteCesionario = Ext.getCmp('lblNombreFirmanteCesionario');
		var lblNombreTestigo1 = Ext.getCmp('lblNombreTestigo1');
		var lblNombreTestigo2 = Ext.getCmp('lblNombreTestigo2');
		var lblFechaAceptacion = Ext.getCmp('lblFechaAceptacion');
		var lblPersonaAceptaNotificacion = Ext.getCmp('lblPersonaAceptaNotificacion');
		var lblInfoVentanilla = Ext.getCmp('lblInfoVentanilla');
		var lblRecepcionVentanilla = Ext.getCmp('lblRecepcionVentanilla');
		var lblFechaAplicacionRedireccionamiento = Ext.getCmp('lblFechaAplicacionRedireccionamiento');
		var lblRedireccionaCuentaPago = Ext.getCmp('lblRedireccionaCuentaPago');
		var fp = Ext.getCmp('formaEnvio');
		fp.el.unmask();
		var btnCsv = Ext.getCmp('btnGenerarArchivo');
		var btnPdf = Ext.getCmp('btnGenerarPDF');
		if(arrRegistros!=null){
			if(store.getTotalCount() > 0){
				var row = store.getAt(0);
				
				lblNombrePyme.getEl().update(row.get('PYME'));
				lblRFC.getEl().update(row.get('RFC'));
				lblRepresentantePyme1.getEl().update(row.get('REPRESENTANTE_PYME_1'));
				lblRepresentantePyme2.getEl().update(row.get('REPRESENTANTE_PYME_2'));
				lblNumProveedor.getEl().update(row.get('NUM_PROVEEDOR'));
				lblFechaSolicitud.getEl().update(row.get('SOLICITUD_CESION'));
				
				lblNumContrato.getEl().update(row.get('NUM_CONTRATO'));
				lblMontoMoneda.getEl().update(row.get('MONTO_MONEDA'));
				lblFechaVigencia.getEl().update(row.get('FECHA_VIGENCIA'));
				lblPlazoContrato.getEl().update(row.get('PLAZO_CONTRATO'));
				lblTipoContratacion.getEl().update(row.get('TIPO_CONTRATACION'));
				lblObjetoContrato.getEl().update(row.get('OBJETO_CONTRATO'));
				lblCampoAdicional1.getEl().update(row.get('CAMPO_ADICIONAL_1'));
				lblCampoAdicional2.getEl().update(row.get('CAMPO_ADICIONAL_2'));
				lblCampoAdicional3.getEl().update(row.get('CAMPO_ADICIONAL_3'));
				lblCampoAdicional4.getEl().update(row.get('CAMPO_ADICIONAL_4'));
				lblCampoAdicional5.getEl().update(row.get('CAMPO_ADICIONAL_5'));
				
				lblNombreInfo1.getEl().update(row.get('NOMBRE_INFO_1'));
				lblPersonaAutCesion.getEl().update(row.get('PERSONA_AUTORIZA_CESION'));
				lblNombreInfo2.getEl().update(row.get('NOMBRE_INFO_2'));
				lblBancoDeposito.getEl().update(row.get('BANCO_DEPOSITO'));
				lblNumCuentaDeposito.getEl().update(row.get('CUENTA'));
				lblNumCuentaClabeDeposito.getEl().update(row.get('CLABE'));
				
				lblFechaSolicitudConsentimiento.getEl().update(row.get('FECHA_SOLICITUD_CONSENTIMIENTO'));
				lblFechaConsentimiento.getEl().update(row.get('FECHA_CONSENTIMIENTO_EPO'));
				lblPersonaConsentimiento.getEl().update(row.get('PERSONA_OTORGO_CONSENTIMIENTO'));
				lblFechaFormalizacion.getEl().update(row.get('FECHA_FORMALIZACION'));
				lblNombreFirmanteCedente.getEl().update(row.get('NOMBRE_FIRMANTE_CEDENTE'));
				lblNombreFirmanteCesionario.getEl().update(row.get('NOMBRE_FIRMANTE_CESIONARIO'));
				lblNombreTestigo1.getEl().update(row.get('NOMBRE_TESTIGO_1'));
				lblNombreTestigo2.getEl().update(row.get('NOMBRE_TESTIGO_2'));
				lblFechaAceptacion.getEl().update(row.get('FECHA_NOTIFICACION'));
				lblPersonaAceptaNotificacion.getEl().update(row.get('PERSONA_NOTIFICACION'));
				lblInfoVentanilla.getEl().update(row.get('FECHA_INF_VENTANILLA'));
				lblRecepcionVentanilla.getEl().update(row.get('FECHA_REC_VENTANILLA'));
				lblFechaAplicacionRedireccionamiento.getEl().update(row.get('FECHA_APLIC_REDIREC'));
				lblRedireccionaCuentaPago.getEl().update(row.get('PERSONA_REDIREC_CUENTA'));
				
				Ext.getCmp('lblSellos').update(row.get('SELLOS'));
				nombreArchivo= row.get('NOMBRE_ARCHIVO');
				
				
				btnCsv.enable();
				btnPdf.enable();
			}else{
				btnCsv.disable();
				btnPdf.disable();
				fp.el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response){
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			//btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler(function(boton, evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPDF = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			//btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5,easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton, evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var preacuse = function(grid,rowIndex){
		var forma = Ext.getCmp('forma');
		grid.hide();
		forma.hide();
		var btnNotificacionAcuse = Ext.getCmp('btnNotificacionAcuse');
		var registro = grid.getStore().getAt(rowIndex);
		claveSolicitud = registro.get('CLAVE_SOLICITUD');
		var cmbVentanilla = Ext.getCmp('cmbVentanilla');
		catVentanilla = cmbVentanilla.getValue();
		if(tipoOperacion == "ACEPTAR"){
			btnNotificacionAcuse.setText('Aceptar');
			registro.data.ESTATUS_SOLICITUD = 'Notificado a Ventanilla';
		}else{
			btnNotificacionAcuse.setText('Rechazar Notificaci�n');
			registro.data.ESTATUS_SOLICITUD = 'Notificaci�n Rechazada';
		}
		consultaDataPreacuse.removeAll();
		var dataPreacuse = [];//para agregar registros al preacuse
		dataPreacuse.push(registro);
		consultaDataPreacuse.add(dataPreacuse);
		
		var gridPreacuse = Ext.getCmp('gridPreacuse');
		gridPreacuse.show();
		var cm = gridPreacuse.getColumnModel();
		var campo1 = cm.findColumnIndex('CAMPO_ADICIONAL_1');
		var campo2 = cm.findColumnIndex('CAMPO_ADICIONAL_2');
		var campo3 = cm.findColumnIndex('CAMPO_ADICIONAL_3');
		var campo4 = cm.findColumnIndex('CAMPO_ADICIONAL_4');
		var campo5 = cm.findColumnIndex('CAMPO_ADICIONAL_5');
		cm.setHidden(campo1,true); //Campo Adicional 1
		cm.setHidden(campo2,true); //Campo Adicional 2 
		cm.setHidden(campo3,true); //Campo Adicional 3
		cm.setHidden(campo4,true); //Campo Adicional 4
		cm.setHidden(campo5,true); //Campo Adicional 5
		var clasificacionEpo = jsonValoresIniciales.clasificacionEpo;
		//Firma
		var camposAdicionales = "";
		textoFirmar = "Intermediario Financiero (Cesionario)|Pyme (Cedente)|RFC|No. Proveedor|Fecha de Solicitud PyME|"+
								"No. Contrato|Monto / Moneda|Tipo de Contrataci�n|Fecha Inicio Contrato|"+
								"Fecha Final Contrato|Plazo del Contrato|";		
		if(clasificacionEpo ==''){		
			gridPreacuse.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), true);					
		}else{
			textoFirmar += clasificacionEpo + "|";
			gridPreacuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
		}		
		//if(registro.get('CAMPO_ADICIONAL_1')!=""){
		if(jsonValoresIniciales.campoAdicional_1!=""){
			cm.setHidden(campo1, false);//Campo Adicional 1
			gridPreacuse.getColumnModel().setColumnHeader(campo1,jsonValoresIniciales.campoAdicional_1);
			textoFirmar += jsonValoresIniciales.campoAdicional_1 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_1') + "|";
		}
		//if(registro.get('CAMPO_ADICIONAL_2')!=""){
		if(jsonValoresIniciales.campoAdicional_2!=""){
			cm.setHidden(campo2, false);//Campo Adicional 2
			gridPreacuse.getColumnModel().setColumnHeader(campo2,jsonValoresIniciales.campoAdicional_2);
			textoFirmar += jsonValoresIniciales.campoAdicional_2 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_2') + "|";
		}
		//if(registro.get('CAMPO_ADICIONAL_3')!=""){
		if(jsonValoresIniciales.campoAdicional_3!=""){
			cm.setHidden(campo3, false);//Campo Adicional 3
			gridPreacuse.getColumnModel().setColumnHeader(campo3,jsonValoresIniciales.campoAdicional_3);
			textoFirmar += jsonValoresIniciales.campoAdicional_3 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_3') + "|";
		}
		//if(registro.get('CAMPO_ADICIONAL_4')!=""){
		if(jsonValoresIniciales.campoAdicional_4!=""){
			cm.setHidden(campo4, false);//Campo Adicional 4
			gridPreacuse.getColumnModel().setColumnHeader(campo4,jsonValoresIniciales.campoAdicional_4);
			textoFirmar += jsonValoresIniciales.campoAdicional_4 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_4') + "|";
		}
		//if(registro.get('CAMPO_ADICIONAL_5')!=""){
		if(jsonValoresIniciales.campoAdicional_5!=""){
			cm.setHidden(campo5, false);//Campo Adicional 5
			gridPreacuse.getColumnModel().setColumnHeader(campo5,jsonValoresIniciales.campoAdicional_5);
			textoFirmar += jsonValoresIniciales.campoAdicional_5 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_5') + "|";
		}
		textoFirmar += "Ventanilla de Pago|Supervisor/Administrador/Residente de Obra|Tel�fono|" +
							"Objeto del Contrato|Comentarios|Monto del Cr�dito|Referencia / No. Cr�dito|" +
							"Fecha Vencimiento Cr�dito|Banco de Dep�sito|Cuenta|Cuenta CLABE|Estatus|\n";
		textoFirmar += registro.get('NOMBRE_IF')+"|"+registro.get('NOMBRE_PYME')+"|"+
							registro.get('RFC')+"|"+registro.get('NUMERO_PROVEEDOR')+"|"+
							registro.get('FECHA_SOL_PYME')+"|"+registro.get('NUMERO_CONTRATO')+"|"+
							registro.get('MONTO_MONEDA')+"|"+registro.get('TIPO_CONTRATACION')+"|"+registro.get('FECHA_INICIO_CONTRATO')+"|"+
							registro.get('FECHA_FIN_CONTRATO')+"|"+registro.get('PLAZO_CONTRATO')+"|"+registro.get('CLASIFICACION_EPO')+"|";
		if(camposAdicionales!=""){
			textoFirmar += camposAdicionales;
		}
		textoFirmar += registro.get('VENTANILLA_PAGO')+"|"+registro.get('SUP_ADM_RESOB')+"|"+registro.get('NUMERO_TELEFONO')+"|"+
							 registro.get('OBJETO_CONTRATO')+"|"+registro.get('COMENTARIOS')+"|"+
							 registro.get('MONTO_CREDITO')+"|"+registro.get('NUMERO_REFERENCIA')+"|"+
							 registro.get('FECHA_VENCIMIENTO')+"|"+registro.get('BANCO_DEPOSITO')+"|"+
							 registro.get('NUMERO_CUENTA')+"|"+registro.get('CLABE')+"|";
		if(tipoOperacion == "ACEPTAR"){
			cm.setHidden(cm.findColumnIndex('CAUSAS_RECHAZO_NOTIF'), true);
			textoFirmar += "Notificado a Ventanilla|"+"\n";
		}else{
			cm.setHidden(cm.findColumnIndex('CAUSAS_RECHAZO_NOTIF'), false);
			textoFirmar += "No Aceptado Ventanilla|"+"\n";
		}	
	}
	var autorizarNotificacion = function(grid,rowIndex,colIndex,item,event){
		var cmbVentanilla = Ext.getCmp('cmbVentanilla');
		if(Ext.isEmpty(cmbVentanilla.getValue())){
			Ext.Msg.alert('Mensaje informativo','Es necesario seleccionar la ventanilla destino',function(btn){
				return;
			});
			return;
		}
		tipoOperacion = "ACEPTAR";
		indice = rowIndex;
		var registro = grid.getStore().getAt(rowIndex);
		claveSolicitud = registro.get('CLAVE_SOLICITUD');
		catVentanilla = cmbVentanilla.getValue();
		acuse5 = registro.get('ACUSE5');
		consultaDataEnvio.load({
			params: Ext.apply(fp.getForm().getValues(),{
				claveSolicitud: claveSolicitud,
				catVentanilla: catVentanilla
			})
		});
		var ventana = Ext.getCmp('VerEnvioCorreo');
		if(ventana){
			ventana.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				width: 850,
				height: 605,
				id: 'VerEnvioCorreo',
				closeAction: 'hide',
				items: [
					fpEnvio
				],
				title: 'Enviar correo de Notificaci�n a Ventanilla'
			}).show();
		}
	}
	var rechazarNotificacion = function(grid,rowIndex,colIndex,item,event){	
		tipoOperacion = "RECHAZAR";
		indice = rowIndex;
		preacuse(grid,rowIndex);
	}

/////////////////////////////////////////////////////////////////////////////////////////// Hugo V.C.


	var muestraAcuseNoficacion = function(grid,rowIndex,accion){

		var registro = grid.getStore().getAt(rowIndex);
		claveSolicitud = registro.get('CLAVE_SOLICITUD');

		if(accion === 'Aceptar Notificaci�n'){//'Autorizar Contrato'
			registro.data.ESTATUS_SOLICITUD = 'Notificaci�n Aceptada';
			accion = 'Aceptar';
		}else if (accion === 'Rechazar Notificaci�n'){//'Rechazar Contrato'
			registro.data.ESTATUS_SOLICITUD = 'Notificaci�n Rechazada';
		}else{
			Ext.Msg.alert('Mensaje','Error en el proceso');
		}
		registro.commit();
		consultaDataPreacuse.removeAll();
		var dataPreacuse = [];//para agregar registros al preacuse
		dataPreacuse.push(registro);
		consultaDataPreacuse.add(dataPreacuse);
		
		var gridPreacuse = Ext.getCmp('gridPreacuse');
		var cm = gridPreacuse.getColumnModel();
		var campo1 = cm.findColumnIndex('CAMPO_ADICIONAL_1');
		var campo2 = cm.findColumnIndex('CAMPO_ADICIONAL_2');
		var campo3 = cm.findColumnIndex('CAMPO_ADICIONAL_3');
		var campo4 = cm.findColumnIndex('CAMPO_ADICIONAL_4');
		var campo5 = cm.findColumnIndex('CAMPO_ADICIONAL_5');
		cm.setHidden(campo1,true); //Campo Adicional 1
		cm.setHidden(campo2,true); //Campo Adicional 2 
		cm.setHidden(campo3,true); //Campo Adicional 3
		cm.setHidden(campo4,true); //Campo Adicional 4
		cm.setHidden(campo5,true); //Campo Adicional 5
		cm.setHidden(cm.findColumnIndex('CAUSAS_RECHAZO_NOTIF'), true);
		var clasificacionEpo = jsonValoresIniciales.clasificacionEpo;
		//Firma
		var camposAdicionales = "";
		textoFirmar = "Intermediario Financiero (Cesionario)|Pyme (Cedente)|RFC|No. Proveedor|Fecha de Solicitud PyME|"+
								"No. Contrato|Monto / Moneda|Tipo de Contrataci�n|Fecha Inicio Contrato|"+
								"Fecha Final Contrato|Plazo del Contrato|";		
		if(clasificacionEpo ==''){
			gridPreacuse.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), true);					
		}else{
			textoFirmar += clasificacionEpo + "|";
			gridPreacuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
		}
		//if(registro.get('CAMPO_ADICIONAL_1')!=""){
		if(jsonValoresIniciales.campoAdicional_1!=""){
			cm.setHidden(campo1, false);//Campo Adicional 1
			gridPreacuse.getColumnModel().setColumnHeader(campo1,jsonValoresIniciales.campoAdicional_1);
			textoFirmar += jsonValoresIniciales.campoAdicional_1 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_1') + "|";
		}
		//if(registro.get('CAMPO_ADICIONAL_2')!=""){
		if(jsonValoresIniciales.campoAdicional_2!=""){
			cm.setHidden(campo2, false);//Campo Adicional 2
			gridPreacuse.getColumnModel().setColumnHeader(campo2,jsonValoresIniciales.campoAdicional_2);
			textoFirmar += jsonValoresIniciales.campoAdicional_2 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_2') + "|";
		}
		//if(registro.get('CAMPO_ADICIONAL_3')!=""){
		if(jsonValoresIniciales.campoAdicional_3!=""){
			cm.setHidden(campo3, false);//Campo Adicional 3
			gridPreacuse.getColumnModel().setColumnHeader(campo3,jsonValoresIniciales.campoAdicional_3);
			textoFirmar += jsonValoresIniciales.campoAdicional_3 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_3') + "|";
		}
		//if(registro.get('CAMPO_ADICIONAL_4')!=""){
		if(jsonValoresIniciales.campoAdicional_4!=""){
			cm.setHidden(campo4, false);//Campo Adicional 4
			gridPreacuse.getColumnModel().setColumnHeader(campo4,jsonValoresIniciales.campoAdicional_4);
			textoFirmar += jsonValoresIniciales.campoAdicional_4 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_4') + "|";
		}
		//if(registro.get('CAMPO_ADICIONAL_5')!=""){
		if(jsonValoresIniciales.campoAdicional_5!=""){
			cm.setHidden(campo5, false);//Campo Adicional 5
			gridPreacuse.getColumnModel().setColumnHeader(campo5,jsonValoresIniciales.campoAdicional_5);
			textoFirmar += jsonValoresIniciales.campoAdicional_5 + "|";
			camposAdicionales += registro.get('CAMPO_ADICIONAL_5') + "|";
		}
		textoFirmar += "Ventanilla de Pago|Supervisor/Administrador/Residente de Obra|Tel�fono|" +
							"Objeto del Contrato|Comentarios|Monto del Cr�dito|Referencia / No. Cr�dito|" +
							"Fecha Vencimiento Cr�dito|Banco de Dep�sito|Cuenta|Cuenta CLABE|Estatus|\n";
		textoFirmar += registro.get('NOMBRE_IF')+"|"+registro.get('NOMBRE_PYME')+"|"+
							registro.get('RFC')+"|"+registro.get('NUMERO_PROVEEDOR')+"|"+
							registro.get('FECHA_SOL_PYME')+"|"+registro.get('NUMERO_CONTRATO')+"|"+
							registro.get('MONTO_MONEDA')+"|"+registro.get('TIPO_CONTRATACION')+"|"+registro.get('FECHA_INICIO_CONTRATO')+"|"+
							registro.get('FECHA_FIN_CONTRATO')+"|"+registro.get('PLAZO_CONTRATO')+"|"+registro.get('CLASIFICACION_EPO')+"|";
		if(camposAdicionales!=""){
			textoFirmar += camposAdicionales;
		}
		textoFirmar += registro.get('VENTANILLA_PAGO')+"|"+registro.get('SUP_ADM_RESOB')+"|"+registro.get('NUMERO_TELEFONO')+"|"+
							 registro.get('OBJETO_CONTRATO')+"|"+registro.get('COMENTARIOS')+"|"+
							 registro.get('MONTO_CREDITO')+"|"+registro.get('NUMERO_REFERENCIA')+"|"+
							 registro.get('FECHA_VENCIMIENTO')+"|"+registro.get('BANCO_DEPOSITO')+"|"+
							 registro.get('NUMERO_CUENTA')+"|"+registro.get('CLABE')+"|"+
							 registro.get('ESTATUS_SOLICITUD')+"|";

		NE.util.obtenerPKCS7(autorizarRechazarContrato, textoFirmar, grid, accion, claveSolicitud);

	}

	var autorizarAceptadoIf = function(grid,rowIndex,colIndex,item,event){
		Ext.Msg.confirm('Mensaje','Se cambiara el estatus a Notificaci�n Aceptada <br> �Desea continuar?',
			function(boton){
				if(boton === 'yes'){
					muestraAcuseNoficacion(grid,rowIndex,item.tooltip)
				}
			}
		);
	}

	var rechazarAceptadoIf = function(grid,rowIndex,colIndex,item,event){
		Ext.Msg.confirm('Mensaje','Se cambiara el estatus a Notificaci�n Rechazada <br> �Desea continuar?',
			function(boton){
				if(boton === 'yes'){
					muestraAcuseNoficacion(grid,rowIndex,item.tooltip)
				}
			}
		);
	}
////////////////////////////////////////////////////////////////////////////////////////// End-Hugo V.C.

	var procesarConsultaData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('forma');
		var record = store.getAt(0);
		fp.el.unmask();
		if(arrRegistros != null){
			if(!grid.isVisible()){
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var formaMensaje = Ext.getCmp('formaMensaje');
			var el = grid.getGridEl();
			
			var cm = grid.getColumnModel();
			var campo1 = cm.findColumnIndex('CAMPO_ADICIONAL_1');
			var campo2 = cm.findColumnIndex('CAMPO_ADICIONAL_2');
			var campo3 = cm.findColumnIndex('CAMPO_ADICIONAL_3');
			var campo4 = cm.findColumnIndex('CAMPO_ADICIONAL_4');
			var campo5 = cm.findColumnIndex('CAMPO_ADICIONAL_5');
			var clasEpo = cm.findColumnIndex('CLASIFICACION_EPO');
			cm.setHidden(campo1,true); //Campo Adicional 1
			cm.setHidden(campo2,true); //Campo Adicional 2 
			cm.setHidden(campo3,true); //Campo Adicional 3
			cm.setHidden(campo4,true); //Campo Adicional 4
			cm.setHidden(campo5,true); //Campo Adicional 5
			if(store.getTotalCount()>0){
				//formaMensaje.show(); Se oculta el mensaje
				btnGenerarPDF.enable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				if(!btnBajarArchivo.isVisible()){
					btnGenerarArchivo.enable();
				} else{
					btnGenerarArchivo.disable();
				}
				if(jsonValoresIniciales.clasificacionEpo == ""){
					grid.getColumnModel().setHidden(clasEpo, true);
				}else{
					grid.getColumnModel().setColumnHeader(clasEpo,jsonValoresIniciales.clasificacionEpo);
				}
				store.each(function(registro){
					if(jsonValoresIniciales.campoAdicional_1!=""){
						cm.setHidden(campo1, false);//Campo Adicional 1
						grid.getColumnModel().setColumnHeader(campo1,jsonValoresIniciales.campoAdicional_1);
					}
					if(jsonValoresIniciales.campoAdicional_2!=""){
						cm.setHidden(campo2, false);//Campo Adicional 2
						grid.getColumnModel().setColumnHeader(campo2,jsonValoresIniciales.campoAdicional_2);
					}
					if(jsonValoresIniciales.campoAdicional_3!=""){
						cm.setHidden(campo3, false);//Campo Adicional 3
						grid.getColumnModel().setColumnHeader(campo3,jsonValoresIniciales.campoAdicional_3);
					}
					if(jsonValoresIniciales.campoAdicional_4!=""){
						cm.setHidden(campo4, false);//Campo Adicional 4
						grid.getColumnModel().setColumnHeader(campo4,jsonValoresIniciales.campoAdicional_4);
					}
					if(jsonValoresIniciales.campoAdicional_5!=""){
						cm.setHidden(campo5, false);//Campo Adicional 5
						grid.getColumnModel().setColumnHeader(campo5,jsonValoresIniciales.campoAdicional_5);
					}
				});
				catalogoVentanilla.load();
				el.unmask();
				//grid.getView().scroller.scrollTo('right', 6500, { easing: 'bounceOut' });	//Linea temporal ... por Hugo Vargas C.
			}else{
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	
	var procesarDescargaArchivos = function(opts, success, response) {
		pnl.el.unmask();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();

		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var mostrarContrato = function (grid,rowIndex,colIndex,item,event){
		pnl.el.mask('Descargando Archivo...','x-mask');
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		//document.forms[0].action = "/nafin/34cesion/34pki/34epo/34NotificacionesEpoArchivoContrato.do?tipoArchivo=CONTRATO&clave_solicitud="+clave_solicitud;
		//document.forms[0].submit();
		
		Ext.Ajax.request({
			url: '34Notificacionesext.data.jsp',
			params: {
						informacion: 'CONTRATO_PDF',
						clave_solicitud: clave_solicitud
			},
			callback:procesarDescargaArchivos
		});
	}
	
	/*
	var mostrarContratoCesion = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		document.forms[0].action = "/nafin/34cesion/34pki/34pyme/34ContratoCesionPymeAcusePDF.do?tipo_archivo=CONTRATO_CESION_PYME&clave_solicitud="+clave_solicitud+"&acuse_carga=";
		document.forms[0].submit();
	}
	*/
//-------------------------------------STORES-----------------------------------
	var catalogoVentanilla = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '34Notificacionesext.data.jsp',
		baseParams: {
			informacion: 'VentanillaDestino'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoIFData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '34Notificacionesext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoPymeData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '34Notificacionesext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPyme'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoEstatusData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '34Notificacionesext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCambioEstatus'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoPlazoContratoData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave', 'descripcion','loadMsg'],
		url: '34Notificacionesext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPlazo'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaDataPreacuse = new Ext.data.ArrayStore({
			fields: [
						{name: 'BANCO_DEPOSITO'},
						{name: 'CAMPO_ADICIONAL_1'},
						{name: 'CAMPO_ADICIONAL_2'},
						{name: 'CAMPO_ADICIONAL_3'},
						{name: 'CAMPO_ADICIONAL_4'},
						{name: 'CAMPO_ADICIONAL_5'},
						{name: 'CAUSASRECHAZO'},
						{name: 'CAUSAS_RECHAZO_NOTIF'},
						{name: 'CLABE'},
						{name: 'CLASIFICACION_EPO'},
						{name: 'CLAVE_ESTATUS'},
						{name: 'CLAVE_SOLICITUD'},
						{name: 'COMENTARIOS'},
						{name: 'ESTATUS_SOLICITUD'},
						{name: 'FECHA_FIN_CONTRATO'},
						{name: 'FECHA_INICIO_CONTRATO'},
						{name: 'FECHA_SOL_PYME'},
						{name: 'FECHA_VENCIMIENTO'},
						{name: 'MONTO_CREDITO'},
						{name: 'MONTO_MONEDA'},
						{name: 'NOMBRE_IF'},
						{name: 'NOMBRE_PYME'},
						{name: 'NUMERO_CONTRATO'},
						{name: 'NUMERO_CUENTA'},
						{name: 'NUMERO_PROVEEDOR'},
						{name: 'NUMERO_REFERENCIA'},
						{name: 'NUMERO_TELEFONO'},
						{name: 'OBJETO_CONTRATO'},
						{name: 'PLAZO_CONTRATO'},
						{name: 'REPRESENTANTE_LEGAL'},
						{name: 'RFC'},
						{name: 'SUP_ADM_RESOB'},
						{name: 'TIPO_CONTRATACION'},
						{name: 'VENTANILLA_PAGO'},
						{name: 'ACUSE5'},
						{name: 'FIRMA_CONTRATO'}
					]
    });
	var consultaDataEnvio = new Ext.data.JsonStore({
		root: 'registros',
		url: '34Notificacionesext.data.jsp',
		baseParams: {
			informacion: 'ConsultaEnvio'
		},
		fields: [
					{name: 'PYME'},
					{name: 'RFC'},
					{name: 'REPRESENTANTE_PYME_1'},
					{name: 'REPRESENTANTE_PYME_2'},
					{name: 'NUM_PROVEEDOR'},
					{name: 'SOLICITUD_CESION'},
					{name: 'NUM_CONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'FECHA_VIGENCIA'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'TIPO_CONTRATACION'},
					{name: 'OBJETO_CONTRATO'},
					{name: 'CAMPO_ADICIONAL_1'},
					{name: 'CAMPO_ADICIONAL_2'},
					{name: 'CAMPO_ADICIONAL_3'},
					{name: 'CAMPO_ADICIONAL_4'},
					{name: 'CAMPO_ADICIONAL_5'},
					{name: 'NOMBRE_INFO_1'},
					{name: 'PERSONA_AUTORIZA_CESION'},
					{name: 'NOMBRE_INFO_2'},
					{name: 'BANCO_DEPOSITO'},
					{name: 'CUENTA'},
					{name: 'CLABE'},
					{name: 'FECHA_SOLICITUD_CONSENTIMIENTO'},
					{name: 'FECHA_CONSENTIMIENTO_EPO'},
					{name: 'PERSONA_OTORGO_CONSENTIMIENTO'},
					{name: 'FECHA_FORMALIZACION'},
					{name: 'NOMBRE_FIRMANTE_CEDENTE'},
					{name: 'NOMBRE_FIRMANTE_CESIONARIO'},
					{name: 'NOMBRE_TESTIGO_1'},
					{name: 'NOMBRE_TESTIGO_2'},
					{name: 'FECHA_NOTIFICACION'},
					{name: 'PERSONA_NOTIFICACION'},
					{name: 'FECHA_INF_VENTANILLA'},
					{name: 'FECHA_REC_VENTANILLA'},
					{name: 'FECHA_APLIC_REDIREC'},
					{name: 'PERSONA_REDIREC_CUENTA'},
					{name: 'NOMBRE_ARCHIVO'},
					{name: 'SELLOS'},
					{name: 'FIRMA_CONTRATO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataEnvio,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaDataEnvio(null,null,null);
				}
			}
		}
	});
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '34Notificacionesext.data.jsp',
		baseParams: {
							informacion: 'Consulta'
		},
		fields: [
					{name: 'NOMBRE_IF'},
					{name: 'NOMBRE_PYME'},
					{name: 'RFC'},
					{name: 'NUMERO_PROVEEDOR'},
					{name: 'FECHA_SOL_PYME'},
					{name: 'NUMERO_CONTRATO'},
					{name: 'TIPO_CONTRATACION'},
					{name: 'FECHA_INICIO_CONTRATO'},
					{name: 'FECHA_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION_EPO'},
					{name: 'VENTANILLA_PAGO'},
					{name: 'SUP_ADM_RESOB'},
					{name: 'NUMERO_TELEFONO'},
					{name: 'OBJETO_CONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'MONTO_CREDITO'},
					{name: 'NUMERO_REFERENCIA'},
					{name: 'FECHA_VENCIMIENTO'},
					{name: 'BANCO_DEPOSITO'},
					{name: 'NUMERO_CUENTA'},
					{name: 'CLABE'},
					{name: 'CAUSASRECHAZO'},
					{name: 'CLAVE_ESTATUS'},
					{name: 'ESTATUS_SOLICITUD'},
					{name: 'MONTO_MONEDA'},
					{name: 'CAUSAS_RECHAZO_NOTIF'},
					{name: 'REPRESENTANTE_LEGAL'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'CAMPO_ADICIONAL_1'},
					{name: 'CAMPO_ADICIONAL_2'},
					{name: 'CAMPO_ADICIONAL_3'},
					{name: 'CAMPO_ADICIONAL_4'},
					{name: 'CAMPO_ADICIONAL_5'},
					{name: 'FIRMA_CONTRATO'},
					{name:  'CLAVE_PYME'},
					{name:  'GRUPO_CESION'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
						load: procesarConsultaData,
						exception: {
							fn: function(proxy,type,action,optionRequest,response,args){
								NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
								procesarConsultaData(null,null,null);//Llama procesar consulta, para que desbloquee los componentes.
							}
						}
		}
	});
//----------------------------------COMPONENTES---------------------------------
	var causasRechazoNotificacion = new Ext.form.TextField({
		name: 'causasRechazo',
		id:'txtCausasRechazo',
		fieldLabel: '',
		allowBlank: true,
		maxLength: 100,
		width: 80,
		msgTarget: 'side',
		margins: '0 20 0 0'//Necesario para mostrar el icono de error
	});
	var gridPreacuse = new Ext.grid.EditorGridPanel({
		store: consultaDataPreacuse,
		clicksToEdit: 1,
		id: 'gridPreacuse',
		columns: [
						{
							header: 'Intermediario Financiero (Cesionario)',
							tooltip: 'Intermediario Financiero (Cesionario)',
							dataIndex: 'NOMBRE_IF',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Pyme (Cedente)',
							tooltip: 'Pyme (Cedente)',
							dataIndex: 'NOMBRE_PYME',
							align: 'center',
							sortable: true,
							renderer: function(value){
								return '<div align= left >'+value+'</div>';
							},
							width: 250
						},
						{
							header: 'RFC',
							tooltip: 'RFC',
							dataIndex: 'RFC',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'No. Proveedor',
							tooltip: 'No. Proveedor',
							dataIndex: 'NUMERO_PROVEEDOR',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Representante legal',
							tooltip: 'Representante legal',
							dataIndex: 'REPRESENTANTE_LEGAL',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Fecha de Solicitud PyME',
							tooltip: 'Fecha de Solicitud PyME',
							dataIndex: 'FECHA_SOL_PYME',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'No. Contrato',
							tooltip: 'No. Contrato',
							dataIndex: 'NUMERO_CONTRATO',
							align: 'center',
							sortable: true,
							width: 250
						},
							{
							header: 'Firma Contrato',
							tooltip: 'Firma Contrato',
							dataIndex: 'FIRMA_CONTRATO',
							sortable: true,
							resizable: true,
							width: 130,				
							align: 'center'
						},
						{
							header: 'Monto/Moneda',
							tooltip: 'Monto/Moneda',
							dataIndex: 'MONTO_MONEDA',
							align: 'left',
							sortable: true,
							width: 250
						},
						{
							header: 'Tipo de Contrataci�n',
							tooltip: 'Tipo de Contrataci�n',
							dataIndex: 'TIPO_CONTRATACION',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'F. Inicio Contrato',
							tooltip: 'F. Inicio Contrato',
							dataIndex: 'FECHA_INICIO_CONTRATO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'F. Final Contrato',
							tooltip: 'F. Final Contrato',
							dataIndex: 'FECHA_FIN_CONTRATO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Plazo del Contrato',
							tooltip: 'Plazo del Contrato',
							dataIndex: 'PLAZO_CONTRATO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Clasificaci�n EPO',
							tooltip: 'Clasificaci�n EPO',
							dataIndex: 'CLASIFICACION_EPO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Campo Adicional 1',
							tooltip: 'Campo Adicional 1',
							dataIndex: 'CAMPO_ADICIONAL_1',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Campo Adicional 2',
							tooltip: 'Campo Adicional 2',
							dataIndex: 'CAMPO_ADICIONAL_2',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Campo Adicional 3',
							tooltip: 'Campo Adicional 3',
							dataIndex: 'CAMPO_ADICIONAL_3',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Campo Adicional 4',
							tooltip: 'Campo Adicional 4',
							dataIndex: 'CAMPO_ADICIONAL_4',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Campo Adicional 5',
							tooltip: 'Campo Adicional 5',
							dataIndex: 'CAMPO_ADICIONAL_5',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Ventanilla de Pago',
							tooltip: 'Ventanilla de Pago',
							dataIndex: 'VENTANILLA_PAGO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Supervisor/Administrador/<br/>Residente de Obra',
							tooltip: 'Supervisor/Administrador/<br/>Residente de Obra',
							dataIndex: 'SUP_ADM_RESOB',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Tel�fono',
							tooltip: 'Tel�fono',
							dataIndex: 'NUMERO_TELEFONO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Objeto del Contrato',
							tooltip: 'Objeto del Contrato',
							dataIndex: 'OBJETO_CONTRATO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Comentarios',
							tooltip: 'Comentarios',
							dataIndex: 'COMENTARIOS',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Monto del Cr�dito',
							tooltip: 'Monto del Cr�dito',
							dataIndex: 'MONTO_CREDITO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Referencia/ No. Cr�dito',
							tooltip: 'Referencia/ No. Cr�dito',
							dataIndex: 'NUMERO_REFERENCIA',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Fecha/ Vencimiento<br>Cr�dito',
							tooltip: 'Fecha/ Vencimiento/ Cr�dito',
							dataIndex: 'FECHA_VENCIMIENTO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Banco de Dep�sito',
							tooltip: 'Banco de Dep�sito',
							dataIndex: 'BANCO_DEPOSITO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Cuenta',
							tooltip: 'Cuenta',
							dataIndex: 'NUMERO_CUENTA',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Cuenta CLABE',
							tooltip: 'Cuenta CLABE',
							dataIndex: 'CLABE',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Observaciones',
							tooltip: 'Observaciones',
							dataIndex: 'CAUSASRECHAZO',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header: 'Estatus',
							tooltip: 'Estatus',
							dataIndex: 'ESTATUS_SOLICITUD',
							align: 'center',
							sortable: true,
							width: 250
						},
						{
							header:'Causas de Rechazo Notificaci�n',	tooltip:'Causas de Rechazo Notificaci�n',
							dataIndex:'CAUSAS_RECHAZO_NOTIF',
							align: 'center',sortable: true,width: 250, hideable:false,
							editor: causasRechazoNotificacion,
							renderer: NE.util.colorCampoEdit
						}
		],
		stripeRows:	true,
		hidden:		true,
		loadMask:	true,
		height:		300,
		width:		940,
		style:		'margin: 0 auto;',
		//title: '',
		frame:		true,
		bbar:	{
					buttonAlign: 'left',
					displayInfo: true,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					items: [
								'-',
								{
									xtype: 'button',
									text: 'Notificaci�n',
									id: 'btnNotificacionAcuse',
									handler: function(boton,evento){
										var txtCausasRechazo = Ext.getCmp('txtCausasRechazo');
										if(tipoOperacion=="RECHAZAR"){
											if(Ext.isEmpty(txtCausasRechazo.getValue())){
												Ext.Msg.alert('Mensaje informativo','Es obligatorio escribir las causas del rechazo,\nen el campo Causas de Rechazo Notificaci�n.',function(btn){
													return;
												});
												return;
											}
										}
										NE.util.obtenerPKCS7(notificar, textoFirmar, tipoOperacion, catVentanilla, claveSolicitud, txtCausasRechazo);
									}
								},
								'-',
								{
									xtype: 'button',
									text: 'Cancelar',
									id: 'btnCancelar',
									handler: function(boton,evento){
										var grid = Ext.getCmp('grid');
										var forma = Ext.getCmp('forma');
										var gridPreacuse = Ext.getCmp('gridPreacuse');
										grid.show();
										forma.show();
										gridPreacuse.hide();
									}
								}
					]			
		}
	});
	
	var autorizarRechazarContrato = function(vPkcs7, vTextoFirmar, vGrid, vAccion, vClaveSolicitud) {
		if(Ext.isEmpty(vPkcs7)){
			Ext.Msg.alert('Mensaje','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;
		}
		
		var vGridPreacuse = Ext.getCmp('gridPreacuse');
		
		pnl.el.mask('Procesando...', 'x-mask-loading');
		Ext.getCmp('forma').hide();
		vGrid.hide();
		vGridPreacuse.show();
		Ext.getCmp('btnNotificacionAcuse').hide();
		Ext.Ajax.request({
			url: '34Notificacionesext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'AcuseNotifica',
				pkcs7: vPkcs7,
				textoPlano: vTextoFirmar,
				tipoOperacion: vAccion,
				claveSolicitud: vClaveSolicitud
			}),
			callback: procesarAcuse
		});
	}


	var notificar = function(vPkcs7, vTextoFirmar, vTipoOperacion, vCatVentanilla, vClaveSolicitud, vTxtCausasRechazo) {
		if(Ext.isEmpty(vPkcs7)){
			Ext.Msg.alert('Mensaje informativo','"Error en el proceso de Firmado.\nNo se puede continuar."',function(btn){
				return;
			});
			return;
		}
		pnl.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '34Notificacionesext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Acuse',
				pkcs7: vPkcs7,
				textoPlano: vTextoFirmar,
				tipoOperacion: vTipoOperacion,
				catVentanilla: vCatVentanilla,
				claveSolicitud: vClaveSolicitud,
				causasRechazoNotificacion: vTxtCausasRechazo.getValue()	
			}),
			callback: procesarAcuse
		});
	}


	
	var combo = new Ext.form.ComboBox({
		name: 'ventanilla',
		id: 'cmbVentanilla',
		emptyText: 'Seleccionar...',
		mode: 'local',
		displayField: 'descripcion',
		valueField: 'clave',
		hiddenName: 'ventanilla',
		forceSelection: false,
		triggerAction: 'all',
		typeAhead: true,
		lazyRender:true,
		minChars: 1,
		allowBlank: true,
		store: catalogoVentanilla,
		tpl: NE.util.templateMensajeCargaCombo 
	});
	var grid = new Ext.grid.EditorGridPanel({
		id: 'grid',	store: consultaData,	stripeRows: true,	clicksToEdit: 1,	hidden: true,
		loadMask: true,	height: 300,	width: 940,	style: 'margin: 0 auto;',	frame: true,
		columns: [
						{
							header: 'Intermediario Financiero (Cesionario)',
							tooltip: 'Intermediario Financiero (Cesionario)',
							dataIndex: 'NOMBRE_IF',
							align: 'center',
							resiazable: true,
							width: 250
						},
						{
							header: 'Pyme (Cedente)',
							tooltip: 'Pyme (Cedente)',
							dataIndex: 'NOMBRE_PYME',
							align: 'center',
							resiazable: true,
							renderer: function(value){
								return '<div align= left >'+value+'</div>';
							},
							width: 250
						},
						{
							header: 'RFC',
							tooltip: 'RFC',
							dataIndex: 'RFC',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'No. Proveedor',
							tooltip: 'No. Proveedor',
							dataIndex: 'NUMERO_PROVEEDOR',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Representante legal',
							tooltip: 'Representante legal',
							dataIndex: 'REPRESENTANTE_LEGAL',
							align: 'center',
							resiazable: true,
							width: 250
						},
						{
							header: 'Fecha de solicitud PyME',
							tooltip: 'Fecha de solicitud PyME',
							dataIndex: 'FECHA_SOL_PYME',
							align: 'center',
							resiazable: true,
							width: 150
						},
						{
							header: 'No. Contrato',
							tooltip: 'No. Contrato',
							dataIndex: 'NUMERO_CONTRATO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Firma Contrato',
							tooltip: 'Firma Contrato',
							dataIndex: 'FIRMA_CONTRATO',
							sortable: true,
							resizable: true,
							width: 130,				
							align: 'center'
						},
						{
							header: 'Monto/Moneda',
							tooltip: 'Monto/Moneda',
							dataIndex: 'MONTO_MONEDA',
							align: 'left',
							resiazable: true,
							width: 250
						},
						{
							header: 'Tipo de Contrataci�n',
							tooltip: 'Tipo de Contrataci�n',
							dataIndex: 'TIPO_CONTRATACION',
							align: 'center',
							resiazable: true,
							width: 150
						},
						{
							header: 'F. Inicio Contrato',
							tooltip: 'F. Inicio Contrato',
							dataIndex: 'FECHA_INICIO_CONTRATO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'F. Final Contrato',
							tooltip: 'F. Final Contrato',
							dataIndex: 'FECHA_FIN_CONTRATO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Plazo del Contrato',
							tooltip: 'Plazo del Contrato',
							dataIndex: 'PLAZO_CONTRATO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Campo Adicional 1',
							tooltip: 'Campo Adicional 1',
							dataIndex: 'CAMPO_ADICIONAL_1',
							align: 'center',
							resiazable: true,
							width: 150,
							hidden: false
						},
						{
							header: 'Campo Adicional 2',
							tooltip: 'Campo Adicional 2',
							dataIndex: 'CAMPO_ADICIONAL_2',
							align: 'center',
							resiazable: true,
							width: 150,
							hidden: false
						},
						{
							header: 'Campo Adicional 3',
							tooltip: 'Campo Adicional 3',
							dataIndex: 'CAMPO_ADICIONAL_3',
							align: 'center',
							resiazable: true,
							width: 150,
							hidden: false
						},
						{
							header: 'Campo Adicional 4',
							tooltip: 'Campo Adicional 4',
							dataIndex: 'CAMPO_ADICIONAL_4',
							align: 'center',
							resiazable: true,
							width: 150,
							hidden: false
						},
						{
							header: 'Campo Adicional 5',
							tooltip: 'Campo Adicional 5',
							dataIndex: 'CAMPO_ADICIONAL_5',
							align: 'center',
							resiazable: true,
							width: 150,
							hidden: false
						},
						{
							header: 'Clasificaci�n EPO',
							tooltip: 'Clasificaci�n EPO',
							dataIndex: 'CLASIFICACION_EPO',
							align: 'center',
							resiazable: true,
							width: 190
						},
						{
							header: 'Ventanilla de pago',
							tooltip: 'Ventanilla de pago',
							dataIndex: 'VENTANILLA_PAGO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Supervisor/Administrador/<br>Residente de Obra',
							tooltip: 'Supervisor/Administrador/Residente de Obra',
							dataIndex: 'SUP_ADM_RESOB',
							align: 'center',
							resiazable: true,
							width: 150
						},
						{
							header: 'Tel�fono',
							tooltip: 'Tel�fono',
							dataIndex: 'NUMERO_TELEFONO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Objeto del Contrato',
							tooltip: 'Objeto del Contrato',
							dataIndex: 'OBJETO_CONTRATO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Comentarios',
							tooltip: 'Comentarios',
							dataIndex: 'COMENTARIOS',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							xtype: 'actioncolumn',
							header: 'Contrato',
							tooltip: 'Contrato',
							dataIndex: 'CLAVE_SOLICITUD',
							align: 'center',
							resiazable: true,
							width: 120,
							items: [
										{
											getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
												if(registro.get('CLAVE_SOLICITUD')!=''){
													this.items[0].tooltip = 'Ver';
													return 'iconoLupa';
												}
											},
											handler: mostrarContrato
										}
							]
						},
						{
							xtype: 'actioncolumn',
							header: 'Contrato de Cesi�n',
							tooltip: 'Contrato de Cesi�n',
							dataIndex: 'CLAVE_SOLICITUD',
							align: 'center',
							resiazable: true,
							width: 120,
							items: [
										{
											getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
												if(registro.get('CLAVE_SOLICITUD')!=''){
													this.items[0].tooltip = 'Ver';
													
													return 'iconoLupa';
												}
											},
											handler: descargarContratoCesion
										}
							]
						},
						{
						xtype: 'actioncolumn',
							header: 'Poderes',
							tooltip: 'Poderes',
					  width: 130,
							align: 'center',					
							items: [
								{
									getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';										
									},	
									handler: VerPoderesPyme
								}
							]				
						},
						{
							header: 'Monto del Cr�dito',
							tooltip: 'Monto del Cr�dito',
							dataIndex: 'MONTO_CREDITO',
							align: 'center',
							resiazable: true,
							width: 120,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Referencia/ No. Cr�dito',
							tooltip: 'Referencia/ No. Cr�dito',
							dataIndex: 'NUMERO_REFERENCIA',
							align: 'center',
							resiazable: true,
							width: 150
						},
						{
							header: 'Fecha Vencimiento<br>Cr�dito',
							tooltip: 'Fecha Vencimiento Cr�dito',
							dataIndex: 'FECHA_VENCIMIENTO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Banco de D�posito',
							tooltip: 'Banco de D�posito',
							dataIndex: 'BANCO_DEPOSITO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Cuenta',
							tooltip: 'Cuenta',
							dataIndex: 'NUMERO_CUENTA',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Cuenta CLABE',
							tooltip: 'Cuenta CLABE',
							dataIndex: 'CLABE',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Observaciones',
							tooltip: 'Observaciones',
							dataIndex: 'CAUSASRECHAZO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Estatus',
							tooltip: 'Estatus',
							dataIndex: 'ESTATUS_SOLICITUD',
							align: 'center',
							resiazable: true,
							width: 180
						},
						
						{
							header: 'Causas de Rechazo Notificaci�n',
							tooltip: 'Causas de Rechazo Notificaci�n',
							dataIndex: 'CAUSAS_RECHAZO_NOTIF',
							align: 'center',
							resiazable: true,
							width: 250,
							renderer: function(value,metaData,registro,rowIndex,colIndex,store){
											if(registro.get('CAUSAS_RECHAZO_NOTIF') == ""){
												value = "N/A";
											}
											return value;
							}
						},
						{
							header: 'Ventanilla destino',
							tooltip: 'Ventanilla destino',
							dataIndex: 'CLAVE',
							align: 'center',
							resiazable: true,
							width: 120,
							editor: combo,
							renderer: Ext.util.Format.comboRenderer(combo)
						},	
						{
						xtype: 'actioncolumn',
							header: 'Poderes IF',
							tooltip: 'Poderes IF',
							width: 130,
							align: 'center',					
							items: [
								{
									getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
										this.items[0].tooltip = 'Ver';
										return 'iconoLupa';										
									}
									,	handler: procesarPoderesIF
								}
							]				
						},	
						{
							xtype: 'actioncolumn',
							header: 'Acci�n',
							tooltip: 'Acci�n',
							dataIndex: 'CLAVE_SOLICITUD',
							align: 'center',
							resiazable: true,
							width: 120,
							renderer: function(value,metaData,registro,rowIndex,colIndex,store){
											if(registro.get('CLAVE_ESTATUS')!=9&&registro.get('CLAVE_ESTATUS')!=11&&registro.get('CLAVE_ESTATUS')!=15){
												value = "N/A";
												return value;
											}
							},
							items: [
										{
											getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
												if(registro.get('CLAVE_SOLICITUD')!=''&&(registro.get('CLAVE_ESTATUS')==11||registro.get('CLAVE_ESTATUS')==15)){
													this.items[0].tooltip = 'Autorizar: Enviar Correo';
													return 'icoAceptar';
												}else{
													return null;
												}
											},
											handler: autorizarNotificacion
										},{
											getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
												if(registro.get('CLAVE_SOLICITUD')!=''&&(registro.get('CLAVE_ESTATUS')==11||registro.get('CLAVE_ESTATUS')==15)){
													this.items[1].tooltip = 'Rechazar Notificaci�n';//'Rechazar Contrato';
													return 'icoRechazar';
												}else{
													return null;
												}
											},
											handler: rechazarNotificacion
										},{
											getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
												if(registro.get('CLAVE_SOLICITUD')!=''&&registro.get('CLAVE_ESTATUS')==9){	//Modificado por Hugo V.C
													this.items[2].tooltip = 'Aceptar Notificaci�n';//'Autorizar Contrato';
													return 'icoAceptar';
												}else{
													return null;
												}
											},
											handler: autorizarAceptadoIf
										},{
											getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
												if(registro.get('CLAVE_SOLICITUD')!=''&&registro.get('CLAVE_ESTATUS')==9){	//Modificado por Hugo V.C
													this.items[3].tooltip = 'Rechazar Notificaci�n';//'Rechazar Contrato';
													return 'icoRechazar';
												}else{
													return null;
												}
											},
											handler: rechazarAceptadoIf
										}
							]
						}
		],
		listeners: {
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var grid = e.grid; 
				var campo= e.field;
				if(campo == 'CLAVE'){
					//if(record.get('CLAVE_ESTATUS')==9||record.get('CLAVE_ESTATUS')==15){
					if(record.get('CLAVE_ESTATUS')==11||record.get('CLAVE_ESTATUS')==15){	//Modifica Hugo V.C.
						return true; // si es editable 	
					}else{
						return false; // no es editable 	
					}
				}
			}
		},
		bbar: {
					buttonAlign: 'left',
					displayInfo: true,
					hidden:	true,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					items: [
								'->',
								'-',
								{
									xtype:	'button',
									text:		'Generar Archivo',
									iconCls:	'icoXls',
									id:		'btnGenerarArchivo',
									handler: function(boton,evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmbCesionario = Ext.getCmp('cmbCesionario');
										var cmbPyme = Ext.getCmp('cmbPyme');
										var cmbEstatus = Ext.getCmp('cmbEstatus');
										var dc_fecha_vigenciaMin = Ext.getCmp('dc_fecha_vigenciaMin');
										var dc_fecha_vigenciaMax = Ext.getCmp('dc_fecha_vigenciaMax');
										var plazoCont = Ext.getCmp('plazoCont');
										var cmbPlazoCont = Ext.getCmp('cmbPlazoCont');
										var dc_fecha_solicitudMin = Ext.getCmp('dc_fecha_solicitudMin');
										var dc_fecha_solicitudMax = Ext.getCmp('dc_fecha_solicitudMax');
										var numContrato = Ext.getCmp('numContrato');
										
										Ext.Ajax.request({
											url: '34Notificacionesext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV',
												ic_if: cmbCesionario.getValue(),
												ic_pyme: cmbPyme.getValue(),
												clave_estatus: cmbEstatus.getValue(),
												df_fecha_vigencia_de: dc_fecha_vigenciaMin.getValue(),
												df_fecha_vigencia_a: dc_fecha_vigenciaMax.getValue(),
												plazo_Contrato: plazoCont.getValue(),
												clave_plazo_contrato: cmbPlazoCont.getValue(),
												df_fecha_solicitud_de: dc_fecha_solicitudMin.getValue(),
												df_fecha_solicitud_a: dc_fecha_solicitudMax.getValue(),
												numeroContrato: numContrato.getValue()
											}),
											callback: procesarSuccessFailureGenerarArchivo
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivo',
									hidden: true
								},
								'-',
								{
									xtype:	'button',
									text:		'Generar PDF',
									iconCls:	'icoPdf',
									id:		'btnGenerarPDF',
									hidden:	true,
									handler: function(boton,evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmbCesionario = Ext.getCmp('cmbCesionario');
										var cmbPyme = Ext.getCmp('cmbPyme');
										var cmbEstatus = Ext.getCmp('cmbEstatus');
										var dc_fecha_vigenciaMin = Ext.getCmp('dc_fecha_vigenciaMin');
										var dc_fecha_vigenciaMax = Ext.getCmp('dc_fecha_vigenciaMax');
										var plazoCont = Ext.getCmp('plazoCont');
										var cmbPlazoCont = Ext.getCmp('cmbPlazoCont');
										var dc_fecha_solicitudMin = Ext.getCmp('dc_fecha_solicitudMin');
										var dc_fecha_solicitudMax = Ext.getCmp('dc_fecha_solicitudMax');
										var numContrato = Ext.getCmp('numContrato');
										Ext.Ajax.request({
											url: '34Notificacionesext.data.jsp',
											params: {
												informacion: 'ArchivoPDF',
												ic_if: cmbCesionario.getValue(),
												ic_pyme: cmbPyme.getValue(),
												clave_estatus: cmbEstatus.getValue(),
												df_fecha_vigencia_de: dc_fecha_vigenciaMin.getValue(),
												df_fecha_vigencia_a: dc_fecha_vigenciaMax.getValue(),
												plazo_Contrato: plazoCont.getValue(),
												clave_plazo_contrato: cmbPlazoCont.getValue(),
												df_fecha_solicitud_de: dc_fecha_solicitudMin.getValue(),
												df_fecha_solicitud_a: dc_fecha_solicitudMax.getValue(),
												numeroContrato: numContrato.getValue()
											},
											callback: procesarSuccessFailureGenerarPDF
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDF',
									hidden: true
								},
								'-'		
					]
		}
	});
	var elementosFormaEnvio = [{
		xtype: 'container',
		anchor: '100%',
		layout: {
						type: 'table',
						columns: 2
		},
		items: [
					{
						xtype: 'label',
						id: 'lblTitulo',
						style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
						fieldLabel: '',
						text: 'Enviar correo de Notificaci�n a Ventanilla',
						colspan: 2
					},	
					{
						html:'<div style="height:30px"></div>',
						colspan: 2
					},
					{
						xtype: 'label',
						id: 'lblPara',
						style: 'font-size:smaller;font-weight:bold;text-align:right;display:block;',
						fieldLabel: '',
						text: 'Para:',
						width: 100,
						colspan: 1
					},
					{
						xtype: 'textfield',
						name: 'txtEmail',
						id: 'txtEmail',
						fieldLabel: '',
						//vtype: 'email',
						style: 'font-size:smaller;font-weight:bold;text-align:center;display:block;',
						msgTarget: 'side',
						width: 600,
						maxLength: 1000,
						colspan: 1
					},
					{
						html:'<div style="height:10px"></div>',
						colspan: 2
					},
					{
						xtype: 'label',
						id: 'lblAviso',
						fieldLabel: '',
						text: '(Para varios destinatarios favor de ingresar las direcciones de correo separadasa por comas(,). ejem. agomez@nafin.gob.mx,slopez@nafin.gob.mx)',
						width: 800,
						style: 'font-family:Arial;font-size:smaller;',
						colspan: 2
					},	
					{
						html:'<div style="height:10px"></div>',
						colspan: 2
					},
					{
						xtype: 'label',
						id: 'lblComentarios',
						style: 'font-size:smaller;font-weight:bold;text-align:right;display:block;',
						fieldLabel: '',
						text: 'Comentarios:',
						width: 100,
						colspan: 1
					},
					{
						xtype: 'textarea',
						name: 'txtComentarios',
						id: 'txtComentarios',
						fieldLabel: '',
						msgTarget: 'side',
						maxLength: 4000,
						maxLengthText: 'No puedes ingresar m�s de 4000 caracteres.',
						allowBlank: false,
						style: 'font-size:smaller;font-weight:bold;text-align:center;display:block;',
						width: 600,
						colspan: 2
					}
				]
	}];
	//Elemento T�tulo aceptaci�n
	var elementoTituloAceptacion1 = [
				{
					xtype: 'label',
					id: 'lblTituloAceptacion',
					style: 'font-weight:bold;text-align:center;display:block;',
					fieldLabel: '',
					text: 'La autentificaci�n se llev� a cabo con �xito.',
					width: 295
				}
	];	
	var elementoTituloAceptacion2 = [
				{
					xtype: 'label',
					id: 'lblRecibo',
					style: 'font-weight:bold;text-align:center;display:block;',
					fieldLabel: '',
					text: 'Recibo:',
					width: 295
				}
	];	
	//Elementos fpAcuse
	var elementosFormaAcuse = [
									{
										xtype: 'label',
										id: 'lblAcuse',
										fieldLabel: 'N�mero de Acuse',
										text: '--'	
									},
									{
										xtype: 'label',
										id: 'lblFechaCarga',
										fieldLabel: 'Fecha de Carga',
										text: '--'
									},
									{
										xtype: 'label',
										id: 'lblHoraCarga',
										fieldLabel: 'Hora de Carga',
										text: '--'
									},
									{
										xtype: 'label',
										id: 'lblUsuarioCaptura',
										fieldLabel: 'Usuario de Captura',
										text: '--'
									}
	];
	var fpAcuse = new Ext.FormPanel({
		id: 'formaAcuse',
		width: 615,
		style: ' margin:0 auto;',
		title: '<div align="center">Cifras de Control</div>',
		hidden: true,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		items: elementosFormaAcuse,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		monitorValid: false,
		autoScroll: true,
		buttonAlign:'center',
		buttons: [
						{
							text:		'Imprimir PDF',
							id:		'btnImprimir',
							iconCls:	'icoPdf',
							hidden:	true,
							formBind:true,
							handler:	function (boton,evento){
								var acuse = elementosAcuse.acuse;
								var fechaCarga = elementosAcuse.fechaCarga;
								var horaCarga = elementosAcuse.horaCarga;
								var strLogin = elementosAcuse.strLogin;
								var strNombreUsuario = elementosAcuse.strNombreUsuario;
								/*
								document.forms[0].action = '/nafin/34cesion/34pki/34epo/34NotificacionesEpoAcusePDF.do?tipoOperacion='+tipoOperacion+
																	'&tipoArchivo=ACUSE'+
																	'&claveSolicitud='+claveSolicitud+
																	'&acuseCarga='+acuse+
																	'&fechaCarga='+fechaCarga+
																	'&horaCarga='+horaCarga+
																	'&Clave_usuario='+strLogin+
																	'&strNombreUsuario='+strNombreUsuario;
								document.forms[0].submit();
								*/
								Ext.Ajax.request({
									url: '34Notificacionesext.data.jsp',
									params:{
										informacion:		'ACUSE_PDF',
										tipoOperacion:		tipoOperacion,
										tipoArchivo:		'ACUSE_PDF',
										claveSolicitud:		claveSolicitud,
										acuseCarga:			acuse,
										fechaCarga:			fechaCarga,
										horaCarga:			horaCarga,
										Clave_usuario: 		strLogin,
										strNombreUsuario: 	strNombreUsuario
									},
									callback: procesarDescargaArchivos
								});
								
						
							}
						},{
							text:		'Imprimir PDF',
							id:		'btnImprimirNotifica',
							iconCls:	'icoPdf',
							hidden:	true,
							formBind:true,
							handler:	function (boton,evento){
								boton.setIconClass('loading-indicator');
								var record = gridPreacuse.getStore().getRange();
								Ext.Ajax.request({
									url: '34Notificacionesext.data.jsp',
									params:{
										informacion:		'ImprimirNotificaPreAcuse',
										regs:					Ext.encode(record[0].data),
										acuse:				elementosAcuse.acuse,
										fechaCarga:			elementosAcuse.fechaCarga,
										horaCarga:			elementosAcuse.horaCarga,
										strLogin:			elementosAcuse.strLogin,
										strNombreUsuario:	elementosAcuse.strNombreUsuario
									},
									callback: procesaImprimirPreAcuse
								});
							}
						},{
							text: 'Salir',
							id: 'btnSalir',
							iconCls: 'cancelar',
							formBind: true,
							handler: function (boton,evento){
								window.location = '34Notificacionesext.jsp'									
							}							
						}
					]
	});
	var elementosFormaEnvioFp = [
									NE.util.getEspaciador(35),
									{
										xtype: 'label',
										id: 'lblSellos',
										style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
										fieldLabel: '',
										text: ''
									},
									{
										xtype: 'label',
										id: 'lblTitulo1',
										style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
										fieldLabel: '',
										hidden: true,
										text: 'Informaci�n de la PYME cedente'
									},	
									{
										xtype: 'label',
										id: 'lblNombrePyme',
										fieldLabel: 'Nombre',
										hidden: true,
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblRFC',
										fieldLabel: 'RFC',
										hidden: true,
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblRepresentantePyme1',
										hidden: true,
										fieldLabel: 'Representante Pyme 1',
										text: '-'									
									},
									{
										xtype: 'label',
										id: 'lblRepresentantePyme2',
										hidden: true,
										fieldLabel: 'Representante Pyme 2',
										text: '-'									
									},
									{
										xtype: 'label',
										id: 'lblNumProveedor',
										hidden: true,
										fieldLabel: 'Num. Proveedor',
										text: '-'									
									},
									{
										xtype: 'label',
										id: 'lblFechaSolicitud',
										hidden: true,
										fieldLabel: 'Fecha de Solicitud de Cesi�n',
										text: '-'									
									},
									//NE.util.getEspaciador(20),
									{
										xtype: 'label',
										id: 'lblTitulo3',
										hidden: true,
										style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
										fieldLabel: '',
										text: 'Informaci�n de Contrato'
									},	
									{
										xtype: 'label',
										id: 'lblNumContrato',
										hidden: true,
										fieldLabel: 'N�mero de Contrato',
										text: '-'									
									},
									{
										xtype: 'label',
										id: 'lblMontoMoneda',
										hidden: true,
										fieldLabel: 'Monto/Moneda',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblFechaVigencia',
										hidden: true,
										fieldLabel: 'Fecha de Vigencia',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblPlazoContrato',
										hidden: true,
										fieldLabel: 'Plazo del Contrato',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblTipoContratacion',
										hidden: true,
										fieldLabel: 'Tipo de Contrataci�n',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblObjetoContrato',
										hidden: true,
										fieldLabel: 'Objeto del Contrato',
										text: '-'
									},
									{
										xtype: 'label',
										hidden: true,
										id: 'lblCampoAdicional1',
										fieldLabel: 'Campo Adicional 1',
										text: '-'
									},
									{
										xtype: 'label',
										hidden: true,
										id: 'lblCampoAdicional2',
										fieldLabel: 'Campo Adicional 2',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblCampoAdicional3',
										hidden: true,
										fieldLabel: 'Campo Adicional 3',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblCampoAdicional4',
										hidden: true,
										fieldLabel: 'Campo Adicional 4',
										text: '-'
									},	
									{
										xtype: 'label',
										id: 'lblCampoAdicional5',
										hidden: true,
										fieldLabel: 'Campo Adicional 5',
										text: '-'
									},
									//NE.util.getEspaciador(20),
									{
										xtype: 'label',
										id: 'lblTitulo4',
										hidden: true,
										style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
										fieldLabel: '',
										text: 'Informaci�n de la empresa de primer orden'
									},	
									{
										xtype: 'label',
										id: 'lblNombreInfo1',
										hidden: true,
										fieldLabel: 'Nombre',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblPersonaAutCesion',
										hidden: true,
										fieldLabel: 'Persona que autoriz� la cesi�n',
										text: '-'
									},
									//NE.util.getEspaciador(20),
									{
										xtype: 'label',
										id: 'lblTitulo5',
										hidden: true,
										style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
										fieldLabel: '',
										text: 'Informaci�n de la empresa de primer orden'
									},	
									{
										xtype: 'label',
										id: 'lblNombreInfo2',
										hidden: true,
										fieldLabel: 'Nombre',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblBancoDeposito',
										hidden: true,
										fieldLabel: 'Banco de Dep�sito',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblNumCuentaDeposito',
										hidden: true,
										fieldLabel: 'N�mero de cuenta para dep�sito',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblNumCuentaClabeDeposito',
										hidden: true,
										fieldLabel: 'N�mero de cuenta CLABE para dep�sito',
										text: '-'
									},
									//NE.util.getEspaciador(20),
									{
										xtype: 'label',
										id: 'lblTitulo6',
										hidden: true,
										style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
										fieldLabel: '',
										text: 'Informaci�n del cesionario'
									},	
									{
										xtype: 'label',
										id: 'lblFechaSolicitudConsentimiento',
										hidden: true,
										fieldLabel: 'Fecha de solicitud de consentimiento',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblFechaConsentimiento',
										hidden: true,
										fieldLabel: 'Fecha de consentimiento de la EPO',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblPersonaConsentimiento',
										hidden: true,
										fieldLabel: 'Persona que otorg� el consentimiento',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblFechaFormalizacion',
										hidden: true,
										fieldLabel: 'Fecha de formalizaci�n del contrato de cesi�n',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblNombreFirmanteCedente',
										hidden: true,
										fieldLabel: 'Nombre del firmante de cedente (PYME)',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblNombreFirmanteCesionario',
										hidden: true,
										fieldLabel: 'Nombre del firmante del cesionario (IF)',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblNombreTestigo1',
										hidden: true,
										fieldLabel: 'Nombre del testigo 1 del cesionario',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblNombreTestigo2',
										hidden: true,
										fieldLabel: 'Nombre del testigo 2 del cesionario',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblFechaAceptacion',
										hidden: true,
										fieldLabel: 'Fecha de la notificaci�n aceptada por la EPO',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblPersonaAceptaNotificacion',
										hidden: true,
										fieldLabel: 'Persona que acepta la notificaci�n',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblInfoVentanilla',
										hidden: true,
										fieldLabel: 'Fecha de informaci�n a ventanilla de pagos',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblRecepcionVentanilla',
										hidden: true,
										fieldLabel: 'Fecha de recepci�n en ventanilla',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblFechaAplicacionRedireccionamiento',
										hidden: true,
										fieldLabel: 'Fecha de aplicaci�n del redireccionamiento',
										text: '-'
									},
									{
										xtype: 'label',
										id: 'lblRedireccionaCuentaPago',
										hidden: true,
										fieldLabel: 'Persona que redirecciona la cuenta de pago',
										text: '-'
									}
	];	
	var fpEnvio = new Ext.FormPanel({
		id: 'formaEnvio',
		width: 800,
		style: ' margin:0 auto;',
		title: '',
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 250,
		defaultType: 'textfield',
		items: [elementosFormaEnvio, elementosFormaEnvioFp],
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		monitorValid: false,
		autoScroll: true,
		buttons: [
						{
							text: 'Enviar',
							id: 'btnEnviar',
							iconCls: 'icoAceptar',
							formBind: true,
							handler: function(boton,evento){
								boton.disable();
								boton.setIconClass('loading-indicator');
								var txtEmail = Ext.getCmp('txtEmail');
								/*if(!Ext.form.VTypes.email(txtEmail.getValue())){
									txtEmail.markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato "usuario@dominio.com"');
									boton.setIconClass('');
									boton.enable();
									txtEmail.focus();
									return;
								}
								*/
								if(Ext.isEmpty(txtEmail.getValue())){
									txtEmail.markInvalid('Por favor especifique el(los) destinatario(s) del correo.');
									boton.setIconClass('');
									boton.enable();
									txtEmail.focus();
									return;
								}
								fpEnvio.el.mask('Enviando...', 'x-mask-loading');
								Ext.Ajax.request({
									url: '34Notificacionesext.data.jsp',
									params: Ext.apply(fpEnvio.getForm().getValues(),{
													informacion: 'Enviar',
													claveSolicitud: claveSolicitud,
													nombreArchivo:nombreArchivo,
													catVentanilla: catVentanilla
									}),
									callback: procesaEnvio
								});
							}
						},
						{
							text: 'Cerrar',
							id: 'btnCerrar',
							iconCls: 'icoRechazar',
							formBind: true,
							handler:function(boton,evento){
								var ventana = Ext.getCmp('VerEnvioCorreo');
								if(ventana){
									ventana.hide();
								}
							}
						}
		]
	});
	var elementosForma = [
									{
										xtype: 'combo',
										name: 'ic_if',
										id: 'cmbCesionario',
										fieldLabel: 'Nombre del Cesionario',
										mode: 'local',
										displayField: 'descripcion',
										valueField: 'clave',
										hiddenName: 'ic_if',
										emptyText: 'Seleccione un Cesionario...',
										forceSelection: false,
										triggerAction: 'all',
										typeAhead: true,
										minChars: 1,
										allowBlank: true,
										store: catalogoIFData,
										tpl: NE.util.templateMensajeCargaCombo
									},
									{
										xtype: 'combo',
										name: 'ic_pyme',
										id: 'cmbPyme',
										fieldLabel: 'PYME',
										mode: 'local',
										displayField: 'descripcion',
										valueField: 'clave',
										hiddenName: 'ic_pyme',
										emptyText: 'Seleccionar una PYME...',
										forceSelection: false,
										triggerAction: 'all',
										typeAhead: true,
										minChars: 1,
										allowBlank: true,
										store: catalogoPymeData,
										tpl: NE.util.templateMensajeCargaCombo
									},
									{
										xtype: 'combo',
										name: 'clave_estatus',
										id: 'cmbEstatus',
										fieldLabel: 'Estatus',
										emptyText: 'Seleccionar estatus...',
										mode: 'local',
										displayField: 'descripcion',
										valueField: 'clave',
										hiddenName: 'clave_estatus',
										forceSelection: false,
										triggerAction: 'all',
										typeAhead: true,
										minChars: 1,
										allowBlank: true,
										store: catalogoEstatusData,
										tpl: NE.util.templateMensajeCargaCombo
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Fecha vigencia del Contrato',
										combineErrors: false,
										msgTarget: 'side',
										anchor: '90%',
										items: [
													{
														xtype: 'datefield',
														name: 'df_fecha_vigencia_de',
														id: 'dc_fecha_vigenciaMin',
														allowBlank: true,
														startDay: 0,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoFinFecha: 'dc_fecha_vigenciaMax',
														margins: '0 20 0 0',//necesario para mostrar el icono de error
														listeners:{
															blur: function(field){
																Ext.getCmp("plazoCont").setValue('');
																Ext.getCmp("cmbPlazoCont").setValue('');
															}
														}
													},
													{
														xtype: 'displayfield',
														value: 'al',
														width: 24
													},
													{
														xtype: 'datefield',
														name: 'df_fecha_vigencia_a',
														id: 'dc_fecha_vigenciaMax',
														allowBlank: true,
														startDay: 1,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoInicioFecha: 'dc_fecha_vigenciaMin',
														margins: '0 20 0 0', //necesario paara mostrar el icono de error
														listeners:{
															blur: function(field){
																Ext.getCmp("plazoCont").setValue('');
																Ext.getCmp("cmbPlazoCont").setValue('');
															}
														}
													}
										]
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Plazo del Contrato',
										id: 'plazoContrato',
										items : [
											{
												xtype: 'numberfield',
												name: 'plazo_Contrato',
												id: 'plazoCont',
												hiddenName : 'plazo_Contrato',
												maxLength:10,
												listeners:{
													blur: function(field){
														Ext.getCmp("dc_fecha_vigenciaMin").setValue('');
														Ext.getCmp("dc_fecha_vigenciaMax").setValue('');
													}
												}
											},
											{
												xtype: 'combo',
												name: 'clave_plazo_contrato',
												id: 'cmbPlazoCont',
												mode: 'local',
												emptyText: 'Seleccionar...',
												valueField: 'clave',
												displayField: 'descripcion',
												hiddenName : 'clave_plazo_contrato',
												fieldLabel: 'Plazo del Contrato',
												disabled: false,
												forceSelection : false,
												triggerAction : 'all',
												typeAhead: true,
												minChars : 1,
												store: catalogoPlazoContratoData,
												tpl : NE.util.templateMensajeCargaCombo,
												listeners: {
													select: {
														fn: function(combo) {
															Ext.getCmp("dc_fecha_vigenciaMin").setValue('');
															Ext.getCmp("dc_fecha_vigenciaMax").setValue('');						
														}
													}
												}
											}				
										]
									},							
									{
										xtype: 'compositefield',
										fieldLabel: 'Fecha de Solicitud PyME',
										combineErrors: false,
										msgTarget: 'side',
										anchor: '90%',
										items: [
													{
														xtype: 'datefield',
														name: 'df_fecha_solicitud_de',
														id: 'dc_fecha_solicitudMin',
														allowBlank: true,
														startDay: 0,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoFinFecha: 'dc_fecha_solicitudMax',
														margins: '0 20 0 0' //necesario para mostrar el icono de error
													},
													{
														xtype: 'displayfield',
														value: 'al',
														width: 24
													},
													{
														xtype: 'datefield',
														name: 'df_fecha_solicitud_a',
														id: 'dc_fecha_solicitudMax',
														allowBlank: true,
														startDay: 1,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoInicioFecha: 'dc_fecha_solicitudMin',
														margins: '0 20 0 0'//necesario para mostrar el icono de error
													}
										]
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'N�mero de Contrato',
										items : [
													{
														xtype: 'textfield',
														name: 'numeroContrato',
														id: 'numContrato',
														hiddenName: 'numContrato',
														fieldLabel: '',
														width: 130,
														maxLength: 15
													}				
										]
									}
	];
	var elementoMensaje = [
										{
											xtype: 'box',
											html: '<p>Con base a lo dispuesto en el C�digo de Comercio por este conducto reciba la notificaci�n sobre el contrato de Cesi�n de Derechos de Cobro que hemos formalizado con la empresa CEDENTE respecto al contrato que aqu� mismo se detalla.' +
													'<p><br>Por lo anterior, se solicita a esa Entidad, se registre la Cesi�n de Derechos de Cobro de referencia a efecto de que realice el(los) pago(s) derivados del Contrato aqu� se�alado a favor del INTERMEDIARIO FINANCIERO (CESIONARIO), en la inteligencia de que �ste, est� de acuerdo con lo establecido en el Convenio Marco de Cesi�n Electr�nica de Derechos  de Cobro.' + 
													'<p><br>Los datos de la cuenta bancaria del CESIONARIO(s) en la que se deben depositar los pagos derivados de esta operaci�n son los que aqu� se detallan.' +
													'<p><br>Sobre el particular y de conformidad con lo se�alado en la emisi�n de la conformidad para ceder los derechos de cobro emitida por esa Entidad, por este medio les notifico, la Cesi�n de Derechos de Cobro del Contrato antes mencionado, para todos los efectos legales a que haya lugar, adjunto, en forma electr�nica.',
											style: 'font-family:arial;font-size:11;text-align:justify;display:block;height:120px;',
											width: 590
										}
	];
	var fpMensaje = new Ext.form.FormPanel({
		id: 'formaMensaje',
		width: 615,
		height: 245,
		title: '',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		hidden: true,
		defaultType: {
			msgTarget: 'side',
			anchor: '-25'
		},
		items: elementoMensaje,
		monitorValid: false
	});
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 515,
		title: 'Notificaciones',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-25'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
						{
							text: 'Consultar',
							id: 'btnConsultar',
							iconCls: 'icoBuscar',
							formBind: true,
							handler: function(boton,evento){
								grid.hide();
								var formaMensaje = Ext.getCmp('formaMensaje');
								formaMensaje.hide();
								var dc_fecha_vigenciaMin = Ext.getCmp('dc_fecha_vigenciaMin');
								var dc_fecha_vigenciaMax = Ext.getCmp('dc_fecha_vigenciaMax');
								var dc_fecha_solicitudMin = Ext.getCmp('dc_fecha_solicitudMin');
								var dc_fecha_solicitudMax = Ext.getCmp('dc_fecha_solicitudMax');
								if(!Ext.isEmpty(dc_fecha_vigenciaMin.getValue())||!Ext.isEmpty(dc_fecha_vigenciaMax.getValue())){
									if(Ext.isEmpty(dc_fecha_vigenciaMin.getValue())){
										dc_fecha_vigenciaMin.markInvalid('Debe capturar ambas fechas de vigencia o dejarlas en blanco');
										dc_fecha_vigenciaMin.focus();
										return;
									}
									if(Ext.isEmpty(dc_fecha_vigenciaMax.getValue())){
										dc_fecha_vigenciaMax.markInvalid('Debe capturar ambas fechas de vigencia o dejarlas en blanco');
										dc_fecha_vigenciaMax.focus();
										return;
									}
								}
								if(!Ext.isEmpty(dc_fecha_solicitudMin.getValue())||!Ext.isEmpty(dc_fecha_solicitudMax.getValue())){
									if(Ext.isEmpty(dc_fecha_solicitudMin.getValue())){
										dc_fecha_solicitudMin.markInvalid('Debe capturar ambas fechas de vigencia o dejarlas en blanco');
										dc_fecha_solicitudMin.focus();
										return;
									}
									if(Ext.isEmpty(dc_fecha_solicitudMax.getValue())){
										dc_fecha_solicitudMax.markInvalid('Debe capturar ambas fechas de vigencia o dejarlas en blanco');
										dc_fecha_solicitudMax.focus();
										return;
									}
								}
								fp.el.mask('Enviando...','x-mask-loading');
								consultaData.load({
										params: Ext.apply(fp.getForm().getValues(),{
										})
								});
							}
						},
						{
							text: 'Limpiar',
							iconCls: 'icoLimpiar',
							handler: function(){
								var formaMensaje = Ext.getCmp('formaMensaje');
								formaMensaje.hide();
								fp.getForm().reset();
								grid.hide();
							}
						}
		]
	});
	var fpTitulo = new Ext.FormPanel({
		id: 'formaTituloAceptacion',
		style: 'margin:0 auto;',
		title: '',
		hidden: true,
		frame: false,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		labelWidth: 0,
		defaultType: 'textfield',
		height:	40,
		width: 300,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		items: [
					elementoTituloAceptacion1,
					NE.util.getEspaciador(5),
					elementoTituloAceptacion2
		],
		monitorValid: false
	});
//-----------------------------------PRINCIPAL----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		heigth: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,
			fpTitulo,
			NE.util.getEspaciador(10),
			gridPreacuse,
			NE.util.getEspaciador(5),
			fpAcuse,
			NE.util.getEspaciador(5),
			fpMensaje
		]
	});
	//Petici�n para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '34Notificacionesext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
	
	catalogoIFData.load();
	catalogoPymeData.load();
	catalogoEstatusData.load();
	catalogoPlazoContratoData.load();
});