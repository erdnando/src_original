Ext.onReady(function(){

	var clave_solicitud  = Ext.getDom('clave_solicitud').value;
	var acuse  = Ext.getDom('acuse').value;
	var tipoContratacion = Ext.getDom('tipoContratacion').value;
	var banderaRechazo = false;
	if(Ext.getDom('banderaRechazo')!=undefined)
		banderaRechazo = Ext.getDom('banderaRechazo').value;
	var nombrePymeCedente; 
	
	
	
		//GENERAR ARCHIVO  PDF
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	//Contenedor para el PreAcuse
	var fpBotones = new Ext.Container({
		layout: 'table',
		align: 'center',
		style: 'margin:0 auto;',
		id: 'fpBotones',		
		layoutConfig: {
			columns: 20
		},
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
			{
				xtype: 'button',
				text: 'Generar PDF',
				id: 'btnGenerarPDF',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
					url: '34Consu_Solic_ConsentimientoExt.data.jsp',
					params: {
						informacion: 'ArchivoAcuse',
						tipoArchivo:'PDF',
						clave_solicitud:clave_solicitud,
						tipoContratacion:tipoContratacion,
						banderaRechazo:banderaRechazo
					},
					callback: procesarSuccessFailurePDF
				});
			}
		},
		{
			xtype: 'button',
			text: 'Bajar PDF',
			id: 'btnBajarPDF',
			hidden: true
		},
		{
			xtype: 'button',
			text: 'Salir',
			iconCls: 'icoLimpiar',
			handler: function() {
				window.location = '34Consu_Solic_ConsentimientoExt.jsp';
			}
		}			
	]
	});
	
	
	//muestra el acuse  
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		//fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}			
			
			var registro=store.getAt(0);
			
			var numero_contrato = registro.get('NOCONTRATO');
			var montospormoneda = registro.get('MONTO_MONEDA');
			var objeto_contrato = registro.get('OBCONTRATO');
			var plazocontrato = registro.get('PLAZO_CONTRATO');
			var nombreEpo = registro.get('NOMBREEPO');
			var fechasolicitud = registro.get('FECHASOLICITUD');
			var nombre_if = registro.get('CECIONARIO');
			
			var jsonData = store.reader.jsonData;	
			var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			var  clasificacionEpo = jsonData.clasificacionEpo;	
			var nombrePymeCedente = jsonData.nombrePymeCedente;	
			var strNombreUsuario = jsonData.strNombreUsuario;	
			
			var acuseCifras = [
				['N�mero de Acuse', jsonData.acuse],
				['Fecha de Carga ', jsonData.fecha],
				['Hora de Carga', jsonData.hora],
				['Usuario de Captura ', jsonData.nombreUsuario]
			];
		
			storeCifrasData.loadData(acuseCifras);	
			gridCifrasControl.show();
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var table ="";
			table ='<embed src="'+jsonData.archivoConsentimiento+'" width="930" height="450"></embed>';
			
			/* ['<table width="600" align="center">'+		
					'<tr>'+
					'<td class="formas" style="text-align: justify;">'+nombrePymeCedente+
					'<br/>'+
					'<p>Presente</p>'+
					'<br/>'+
					'<u>Consentimiento Condicionado para Ceder los Derechos de Cobro respecto del Contrato No. '+numero_contrato +'</u> '+
					'<br/>'+
					'<br/>'+
					'<br/>'+
					nombrePymeCedente+
					'<br/>'+
					'Objeto: '+objeto_contrato+
					'<br/>'+
					'Monto: '+ replaceAll(montospormoneda,'<br/>',' ') +
					'<br/>'+
					'Plazo: '+ plazocontrato +
					'<br/>'+
					'Fecha de Solicitud: '+fechasolicitud+
					'<br/>'+
					'<br width="80%">   </br>'+
					'<p> En atenci�n a su solicitud, '+ nombreEpo +', le informa que con base en la informaci�n recabada al interior del organismo, se otorga el consentimiento que solicita, con fundamento en: El Art�culo 46 de la Ley de Adquisiciones, Arrendamientos y Servicios del Sector P�blico.  El Art�culo 47 de la Ley de Obras P�blicas y Servicios Relacionados con las Mismas.  Los Art�culos 53, fracci�n XVII, de la Ley de Petr�leos Mexicanos, 48 de su Reglamento, y 54 de las Disposiciones'+
					'administrativas de contrataci�n en materia de adquisiciones, arrendamientos, obras y servicios de las actividades sustantivas de car�cter productivo de Petr�leos Mexicanos y Organismos Subsidiarios , seg�n corresponda, as� como en la cl�usula relativa a la Cesi�n de derechos de Cobro del Contrato. '+
					'<br width="80%">   </br>'+
					'<p> No obstante lo anterior, el Consentimiento estar� Condicionado a que: '+
					'<br width="80%">   </br>'+
					'<p>1.- '+nombrePymeCedente+', en lo sucesivo la CEDENTE, deber� celebrar con el '+nombre_if+' un Contrato de Cesi�n Electr�nica de Derechos de Cobro, mismo que deber� ser suscrito por el o los representantes legales o apoderados de la CEDENTE que cuente(n) con facultades generales o especiales para actos de dominio y que dicho instrumento contractual sea notificado a la Entidad a trav�s de este sistema NAFIN para que surta efectos legales, en t�rminos de lo establecido'+
					'en el art�culo 390 del C�digo de Comercio, en el entendido que de no efectuarse la mencionada notificaci�n dentro de los 15 (quince) d�as h�biles siguientes a la fecha en que sea notificada en el sistema NAFIN el presente Consentimiento Condicionado, �ste quedar� sin efecto alguno, sin ninguna responsabilidad para la Entidad. '+
					'<br width="80%">   </br>'+
					'<p>2.-La Cesi�n de Derechos de Cobro incluir� los incrementos al monto del contrato arriba se�alado que, en su caso, se pacten con posterioridad mediante convenios modificatorios; y que la Cesi�n de Derechos de Cobro �nica y exclusivamente proceder� con respecto a facturas pendientes de cobro por trabajos ejecutados/servicios prestados/bienes entregados/ejecutados en t�rminos de lo pactado en el contrato y que hayan sido instruidos por la Entidad.  '+
					'<br width="80%">   </br>'+
					'<p>3.- La Cesi�n de Derechos de Cobro ser� por el 100% (cien por ciento) de los derechos de cobro que se deriven por el cumplimiento en t�rminos del mencionado contrato, a partir de la fecha en que surta efectos la notificaci�n del correspondiente Contrato de Cesi�n Electr�nica de Derechos de Cobro.  '+
					'<br width="80%">   </br>'+
					'<p>4.- El INTERMEDIARIO FINANCIERO acepte y est� de acuerdo en que el pago de los derechos de cobro estar� sujeto a que, en su caso, la Entidad realice las deducciones y/o retenciones que deban hacerse a la CEDENTE por adeudos con la Entidad; penas convencionales; pagos en exceso; cualquier requerimiento de pago derivado de laudo firme ordenado por las Juntas Locales o Federales de Conciliaci�n y Arbitraje, con motivo de los juicios laborales instaurados en contra de Petr�leos Mexicanos y sus Organismos Subsidiarios, en relaci�n con la CEDENTE; o cualquier otra deducci�n derivada del contrato mencionado o de la Ley de la materia en relaci�n con el  contrato celebrado  con la Entidad y que es objeto de la presente cesi�n de derechos de cobro, en cuyos supuestos se har�n las deducciones y/o retenciones correspondientes. En tal virtud, la Entidad quedar� liberada de cualquier responsabilidad por las deducciones y/o retenciones a que haya lugar. '+
					'<br width="80%">   </br>'+
					'<p>5.- El Consentimiento Condicionado otorgado por la Entidad, no constituye una garant�a de pago ni reconocimiento de cualquier derecho del INTERMEDIARIO FINANCIERO frente a la CEDENTE, siendo la �nica obligaci�n de la Entidad, la de pagar cantidades l�quidas que sean exigibles por el cumplimiento del citado contrato una vez hechas las deducciones y/o retenciones antes mencionadas. '+
					'<br width="80%">   </br>'+
					'<p>6.- Se deber�n proporcionar los siguientes datos:  Nombre del INTERMEDIARIO FINANCIERO, Banco de Dep�sito, N�mero de Cuenta para Dep�sito y N�mero de Cuenta CLABE, a la cual se realizar�n los pagos.  '+
					'<br width="80%">   </br>'+
					'<p>7.-A la notificaci�n que se practique a la Entidad mediante este sistema NAFIN, NAFIN deber� subir al mismo: i) un ejemplar del Contrato de Cesi�n Electr�nica de Derechos de Cobro con las firmas electr�nicas de los representantes de la CEDENTE y del INTERMEDIARIO FINANCIERO; ii) copia certificada por fedatario p�blico de la documentaci�n con la que se acredite la personalidad del o de los representantes legales o apoderados de la CEDENTE con las facultades generales o especiales para actos de dominio, y iii) copia certificada por fedatario p�blico de la documentaci�n con la que se acredite la personalidad del o de los representantes legales o apoderados del INTERMEDIARIO FINANCIERO con las facultades suficientes para la celebraci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro en cuesti�n. '+
					'<br width="80%">   </br>'+
					'<p>8.- El presente Consentimiento Condicionado, quedar� sin efecto en caso de que la Entidad antes de la fecha de notificaci�n a que se refiere el punto n�mero �1� del presente Consentimiento Condicionado, reciba alguna notificaci�n de autoridad judicial o administrativa que incida sobre el contrato cuyos derechos de cobro ser�n materia de cesi�n. Bajo este supuesto, la Entidad lo informar� inmediatamente mediante este sistema NAFIN a la CEDENTE y al INTERMEDIARIO FINANCIERO, una vez que haya recibido formalmente la notificaci�n de que se trate '+
					'<br width="80%">   </br>'+
					'<p>9.- El Contrato de Cesi�n Electr�nica de Derechos de Cobro quedar� sin efectos si y s�lo si la CEDENTE y el INTERMEDIARIO FINANCIERO celebran un Convenio de Extinci�n de la Cesi�n Electr�nica de Derechos de Cobro, en la inteligencia de que sus representantes legales o apoderados deber�n contar con las facultades suficientes para ello, y en su oportunidad notificarlo a la Entidad en los mismos t�rminos previstos para la notificaci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro para que surta los efectos legales que le correspondan. '+
					'<br width="80%">   </br>'+					
					'<p>'+strNombreUsuario+
					'</td>'+
					'</tr>'+	
					'</table>'];*/

			if(tipoContratacion!=''){
				//panel para mostrar  el texto 
				var fpMensaje = new Ext.form.FormPanel({		
					id: 'fpMensaje',
					width: 940,	
					align: 'center',					
					bodyStyle: 'padding: 6px',
					style: 'margin:0 auto;',
					defaultType: 'textfield',		
					html: table
				});
			if(banderaRechazo!='true')
				contenedorPrincipalCmp.add(fpMensaje);
		}
			contenedorPrincipalCmp.add(NE.util.getEspaciador(20));
			contenedorPrincipalCmp.add(fpBotones);			
			contenedorPrincipalCmp.doLayout();
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			
			if(clasificacionEpo ==''){				
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION'),clasificacionEpo);
			}
			
			if(hayCamposAdicionales=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='1'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);					
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='2'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='3'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='4'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='5'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.campo01);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.campo02);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.campo03);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.campo04);	
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.campo05);				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
			}
							
			var el = gridConsulta.getGridEl();						
			if(store.getTotalCount() > 0) {			
					
				el.unmask();
			} else {	
					
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	
		//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
 //Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 300,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 500,
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});
	
	

	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34Consu_Solic_ConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'ConsultarAcuse'
		},
			fields: [
					{name: 'CECIONARIO'},
					{name: 'PYME'},
					{name: 'RFC'},
					{name: 'CLAVE_PYME'},
					{name: 'REPRESENTANTE'},
					{name: 'FSOLICITUDINI'},
					{name: 'NOCONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'CONTRATACION'},
					{name: 'FECHA_INICIO_CONTRATO'},
					{name: 'FECHA_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION'},
					{name: 'CAMPO1'},
					{name: 'CAMPO2'},
					{name: 'CAMPO3'},
					{name: 'CAMPO4'},
					{name: 'CAMPO5'},
					{name: 'VENANILLA_PAGO'},
					{name: 'SUP_ADM_RESOB'},
					{name: 'NUMERO_TELEFONO'},
					{name: 'OBCONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'FACEPTACION'},
					{name: 'ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'TIPOCONTRATACION'},
					{name: 'NOESTATUS'},
					{name: 'CAUSA_RECHAZO'},
					{name: 'TEXTO_FIRMAR'},
					{name: 'NOMBREEPO'},
					{name: 'FECHASOLICITUD'},
					{name: 'FIRMA_CONTRATO'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
		

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		store: consultaData,
		align: 'center',
		//margins: '20 0 0 0',
		title: 'Acuse Solicitud de Consentimiento',
		columns: [
			{
				header: 'Intermediario Financiero (Cesionario)', 
				tooltip: 'Intermediario Financiero (Cesionario)',
				dataIndex: 'CECIONARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Pyme (Cedente)', 
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'RFC ', 
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'No Proveedor ', 
				tooltip: 'No Proveedor',
				dataIndex: 'CLAVE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Representante legal ',  // usa un metodo pra obtenerlo
				tooltip: 'Representante legal',
				dataIndex: 'REPRESENTANTE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Fecha de solicitud Pyme', 
				tooltip: 'Fecha de solicitud Pyme ',
				dataIndex: 'FSOLICITUDINI',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},	
			{
				header: 'No. Contrato', 
				tooltip: 'No. Contrato',
				dataIndex: 'NOCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto / Moneda',  // este usa  metodo para obtenerlo
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tipo de Contrataci�n',  
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'CONTRATACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'F. Inicio Contrato',  
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'						
			},
			{
				header: 'F. Final Contrato',  
				tooltip: 'F. Final Contrato',
				dataIndex: 'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				
			},			
			{
				header: 'Plazo del Contrato',  
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Ciudad de Firma del Contrato',  
				tooltip: 'Ciudad de Firma del Contrato',
				dataIndex: 'CLASIFICACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			//aqui van los campos adicionales
			{
				header: 'Campo 1',  
				tooltip: 'Campo 1',
				dataIndex: 'CAMPO1',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 2',  
				tooltip: 'Campo 2',
				dataIndex: 'CAMPO2',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 3',  
				tooltip: 'Campo 3',
				dataIndex: 'CAMPO3',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 4',  
				tooltip: 'Campo 4',
				dataIndex: 'CAMPO4',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 5',  
				tooltip: 'Campo 5',
				dataIndex: 'CAMPO5',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Ventanilla de Pago',  
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENANILLA_PAGO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra',  
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUP_ADM_RESOB',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Tel�fono',  
				tooltip: 'Tel�fono',
				dataIndex: 'NUMERO_TELEFONO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Objeto del Contrato',  
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBCONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Comentarios',  
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},			
			{
				header: 'Fecha de Aceptacion o Rechazo',  
				tooltip: 'Fecha de Aceptacion o Rechazo',
				dataIndex: 'FACEPTACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Estatus Actual',  
				tooltip: 'Estatus Actual',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'				 
			}	
			
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 943,		
		frame: true	
	});


	
	
//--------------------------------PRINCIPAL-------------------------------------
var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		items: [
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),
			gridCifrasControl,
			NE.util.getEspaciador(20)				
		]
	});


	if(acuse!='') {
		consultaData.load({
			params: {
			informacion: 'ConsultarAcuse',
				clave_solicitud: clave_solicitud,
				acuse:acuse
			}	
		});		
		
	}



	
	
});