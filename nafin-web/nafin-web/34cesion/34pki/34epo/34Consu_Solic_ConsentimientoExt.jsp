<!DOCTYPE html>
<%@ page import="java.util.*,
		com.netro.cesion.*,
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*" 
		contentType="text/html;charset=windows-1252"
		errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String consulta  = (request.getParameter("consulta")!=null)?request.getParameter("consulta"):"";	
String mensaje  = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";	
String enterado  = (request.getParameter("enterado")!=null)?request.getParameter("enterado"):"";	

CesionEJB  cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class); 

List registrosR = cesionBean.consultaSolicitudes(iNoCliente);
 
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<% if(registrosR.size()>0 && enterado.equals("")) { %>
<script type="text/javascript" src="34SolicConsentimientoExt.js?<%=session.getId()%>"></script> 
<%}else {%>
<script type="text/javascript" src="34Consu_Solic_ConsentimientoExt.js?<%=session.getId()%>"></script>
<%}%>
<%@ include file="/00utils/componente_firma.jspf" %>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>	
	<form id='formAux' name="formAux" target='_new'></form>
	<form id='formParametros' name="formParametros">	
		<input type="hidden" id="consulta" name="consulta" value="<%=consulta%>"/>	
		<input type="hidden" id="mensaje" name="mensaje" value="<%=mensaje%>"/>
		<input type="hidden" id="strNombre" name="strNombre" value="<%=strNombre%>"/>
	</form>
	
</body>
</html>
