Ext.onReady(function() {

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

    Ext.apply(Ext.form.VTypes, {
         // This function validates input text with address mail�s
        variosMails:  function(v) {
				var cad = v.split(',');
				if (cad.length > 0){
					for(x=0;x<cad.length;x++){
						if (	!(/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/.test(cad[x])) ){
							return false;
						}
					}
				}
				return true;
        },
		  variosMailsText:	'No hay una direcci�n de correo v�lida.'
    });


	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarPoderesIF = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		
		//forma.action = '/nafin/00archivos/22cotizador/instrucciones.pdf';
					Ext.Ajax.request({
							url: '../../34DescargaPoderes.data.jsp',
							params: Ext.apply({informacion:'PoderesExtincionContrato',clave_solicitud:clave_solicitud }),
							callback: procesarArchivoSuccess
						});
		}

	var form = {vtanilla:null, clave_solicitud:null, notificacionAceptada:false};

	function procesaValoresIniciales(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.registros != undefined && infoR.registros.length > 0){

				var consultaIniData = new Ext.data.JsonStore({
					id:'consultaAccionData',
					fields: [ {name: 'IC_SOLICITUD'},{name: 'CECIONARIO'},{name: 'PYME'},{name: 'RFC'},{name: 'FSOLICITUDINI'},{name: 'NOCONTRATO'},{name: 'MONTO_MONEDA'} ],
					data:	[{'IC_SOLICITUD':'','CECIONARIO':'','PYME':'','RFC':'','FSOLICITUDINI':'','NOCONTRATO':'','MONTO_MONEDA':''}],
					autoLoad: true,
					listeners: {exception: NE.util.mostrarDataProxyError}
				});

				consultaIniData.loadData(infoR.registros);

				var gridIni = new Ext.grid.GridPanel({
					id:'gridIni',store: consultaIniData,columLines:true,stripeRows:true,	loadMask:true,	
					height:400,	width:800, style:'margin:0 auto;',	frame:false,viewConfig: {forceFit: true},
					columns: [
						{
							header: 'Intermediario Financiero (Cesionario)',	tooltip: 'Intermediario Financiero (Cesionario)',	dataIndex:'CECIONARIO',	sortable:true,	resizable:true,	width:200,	align:'center',
							renderer:function(value, metadata, record, rowindex, colindex, store) {
											metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
											return value;
										}
						},{
							header:'PYME (Cedente)',	tooltip:'PYME (Cedente)',	dataIndex:'PYME',	sortable:true,	resizable:true,	width:200,	align:'center',
							renderer:function(value, metadata, record, rowindex, colindex, store) {
											metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
											return '<div align=left >'+value+'</div>';
										}
						},{
							header:'RFC',	tooltip:'RFC',	dataIndex:'RFC',	sortable:true,	resizable:true,	width:130,	align:'center'
						},{
							header:'Fecha de Solicitud Extinci�n',	tooltip:'Fecha de Solicitud Extinci�n',dataIndex:'FSOLICITUDINI',	sortable:true,	resizable:true,	width:120,	align:'center'
						},{
							header:'No. de Contrato',	tooltip:'No. de Contrato',	dataIndex:'NOCONTRATO',	sortable:true,	resizable:true,	width:130,	align:'center'
						},{
							header:'Monto / Moneda',	tooltip:'Monto / Moneda',	dataIndex:'MONTO_MONEDA',	sortable:true,	resizable:true,	width:130,	align:'left',
							renderer:function(value, metadata, record, rowindex, colindex, store) {
											metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
											return value;
										}
						}
					]
				});

				var winIni = new Ext.Window ({
					id:'winIni',
					x: 200,
					y: 200,
					width: 820,
					layout:'fit',
					modal: true,
					height:400,
					resizable:false,
					closable:false,
					bodyStyle: 'padding: 6px',
					style: 'margin:0 auto;',
					title: '<div align="center">Notificaciones de Extinci�n pendientes de Autorizar</div>',
					items:[gridIni],
					buttonAlign:'center',
					buttons:[
						{text:'Enterado', iconCls:'icoAceptar',
							handler:function(){
								catalogoIfData.load();
								catalogoNombrePymeData.load();
								catalogoMonedaData.load();
								catalogoTipoContratacionData.load();
								Ext.getCmp('winIni').hide(); 
								fp.show();
							}	
						}
					]
				}).show();
			}else{
				catalogoIfData.load();
				catalogoNombrePymeData.load();
				catalogoMonedaData.load();
				catalogoTipoContratacionData.load();
				fp.show();				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '../34pyme/34ContratoCesionPDFCSV.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}
	
	var Convenio = new Ext.Panel({
		id: 'Convenio',
		width: 600,
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true
	});
	
		var verConvenioExtincion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');		
		var ventana = Ext.getCmp('verConvenio');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 940,
				height: 550,			
				id: 'verConvenio',
				closeAction: 'hide',
				items: [					
					Convenio
				],
				title: 'Convenio de Extinci�n'	
			}).show();
		}
		var bodyPanel = Ext.getCmp('Convenio').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure',
			function(el, response) {
				bodyPanel.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		Ext.Ajax.request({
				url: '../34ConvenioExtincionCesionDerechosExt.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'CONTRATO_EXTINCION',
					clave_solicitud: clave_solicitud,
					clave_epo: clave_epo		
				}),
				callback: procesarContratoExtincionPDF
			});
		}
	
		var procesarContratoExtincionPDF =  function(opts, success, response) {
		//var btnGenerarPDFAcuse = Ext.getCmp('btnGenerarPDFAcuse');
		//btnGenerarPDFAcuse.setIconClass('');
		//AcuseFirma.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFAcuse = Ext.getCmp('btnBajarPDFAcuse');
			 Ext.getCmp('Convenio').update('<table align="center" width="750" border="0" cellspacing="1" cellpadding="1"><embed src="'+ Ext.util.JSON.decode(response.responseText).urlArchivo+'" width="770" height="500"></embed></table>');
			//btnBajarPDFAcuse.show();
			//btnBajarPDFAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			
		} else {
			//btnGenerarPDFAcuse.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureFirmaAccion =  function(opts, success, response) {
		Ext.getCmp('idAccion').hide();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var infoR = Ext.util.JSON.decode(response.responseText);
				if (infoR._acuse != undefined){
					var elementosAcuse = [
						{
							xtype: 'panel',	layout:'table',	width:450,	border:true,	layoutConfig:{ columns: 2 },
							defaults: {frame:false, border: true,width:120, height: 35,bodyStyle:'padding:5px'},
							items:[
								{	frame:true,	border:false,	width:450, colspan:2,	html:'<div align="center">Cifras de Control</div>'	},
								{	frame:true,	border:false,	html:'<div align="left">N�mero de Acuse</div>'	},
								{	id:'disAcuse',					width:330,	html:'<div align="center">'+infoR._acuse+'</div>'	},
								{	frame:true,	border:false,	html:'<div align="left">Fecha de carga</div>'	},
								{	id:'disFechaCarga',			width:330,	html:'<div align="center">'+infoR.fechaCarga+'</div>'	},
								{	frame:true,	border:false,	html:'<div align="left">Hora de carga</div>'	},
								{	id:'disHoraCarga',			width:330,	html:'<div align="center">'+infoR.horaCarga+'</div>'},
								{	frame:true,						html:'<div align="left">Usuario</div>'	},
								{	id:'disUsuario',				width:330,	html: '<div align="center">'+infoR.usuario+'</div>'	}
							]
						}
					];

					var fpAcuse=new Ext.FormPanel({	style: ' margin:0 auto;',	height:'auto',	width:452,	border:true,	frame:false,	items:elementosAcuse	});

					if(Ext.getCmp('idAccion').text === 'Autorizar'){
						//pnl.insert(1,NE.util.getEspaciador(10));
						//pnl.insert(2,fpAcuse);
						//FODEA-024-2014 MOD()
						pnl.insert(3,NE.util.getEspaciador(10));
						pnl.insert(4,fpAcuse);
						pnl.doLayout();
					}else{
						pnl.insert(3,NE.util.getEspaciador(10));
						pnl.insert(4,fpAcuse);
						pnl.doLayout();
					}

					Ext.getCmp('idCancelAccion').setText('Salir');
					Ext.Ajax.request({
						url:		'34conExtinContratoExt.data.jsp',
						params:	{
							informacion:		'getDataPrintAcuse',
							acuse:				infoR._acuse,
							fechaCarga:			infoR.fechaCarga,
							horaCarga:			infoR.horaCarga,
							_usuario:			infoR.usuario,
							clave_solicitud:	opts.params.clave_solicitud,
							tipoOperacion:		opts.params.tipoOperacion,
							catVentanillaC:	opts.params.catVentanillaC
						},
						callback:procesarSuccessFailureDataPrintAcuse
					});

				}else{
					pnl.el.unmask();
					Ext.getCmp('idCancelAccion').setText('Regresar');
					panelMsg.body.update('<div align="center"><b>La autentificaci�n no se llev� a cabo.<br>Proceso CANCELADO</b></div>');
					gridAccion.hide();
				}
		} else {
			pnl.el.unmask();
			Ext.getCmp('idCancelAccion').setText('Regresar');
			panelMsg.body.update('<div align="center"><b>La autentificaci�n no se llev� a cabo.<br>Proceso CANCELADO</b></div>');
			gridAccion.hide();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var confirmarAcuse  = function(pkcs7, textoFirmar, clave_solicitud, tipoOperacion, causasRechazo , vtanilla ){
	
		if (Ext.isEmpty(pkcs7) ) {
			Ext.Msg.alert('Mensaje','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		}else  {
			
			pnl.el.mask('Enviando...', 'x-mask-loading');
			Ext.Ajax.request({
				url : '34conExtinContratoExt.data.jsp',
				params : {
					informacion:"AcuseExtContrato",
					clave_solicitud:clave_solicitud,
					tipoOperacion:tipoOperacion,
					causasRechazo:causasRechazo,
					catVentanillaC:vtanilla,
					TextoFirmado: textoFirmar,
					Pkcs7: pkcs7
				},
				callback: procesarSuccessFailureFirmaAccion
			});				
		}
	}
	

	function firmarAccion(){
			var tipoOperacion="";
			var descVentanilla="";
			var causasRechazo="";
			/*//var textoFirmar =	"Nombre Cesionario|PYME (Cedente)|RFC|Representante Legal|No. de Contrato|Monto / Moneda|Tipo de Contrataci�n|Fecha Inicio Contrato|"+
									"Fecha Final Contrato|Area Autorizada|Adicional 1|Adicional 2|Objeto del Contrato|Fecha de Extinci�n del contrato|Cuenta|Cuenta CLABE|"+
									"Banco de Deposito|";//*/
			var textoFirmar = "Intermediario Financiero (Cesionario)|PYME (Cedente)|RFC|Representante Legal|No. de Contrato|Monto / Moneda|Tipo de Contrataci�n|Fecha Inicio Contrato|"+
									"Fecha Final Contrato|Area Autorizada|Objeto del Contrato|Fecha de Extinci�n del contrato|Cuenta|Cuenta CLABE|"+
									"Banco de Deposito|";

			if( Ext.getCmp('idAccion').text === "Autorizar" ){
				tipoOperacion = "A";
				textoFirmar += "Ventanilla Destino|";
			}else{
				tipoOperacion = "R";
			}
			textoFirmar += "\n";

			var modificados = Ext.getCmp('gridAccion').getStore().getModifiedRecords();
			if (modificados.length > 0) {
				Ext.each(modificados, function(record) {
					if(tipoOperacion==="A"){
						var dato = catalogoVentanillaData.findExact("clave", record.data['VENTANILLA']);
						if (dato != -1){
							var reg = catalogoVentanillaData.getAt(dato);
							descVentanilla = reg.get('descripcion');
						}
					}
					textoFirmar += record.data['NOMBRE_IF'].replace(',', '') + "|" +
							record.data['NOMBRE_PYME'].replace(',', '') + "|" +
							record.data['RFC'].replace(',', '') + "|" +
							record.data['REPRESENTA_LEGAL'].replace(',', '') + "|" +
							record.data['NUMERO_CONTRATO'].replace(',', '') + "|" +
							record.data['MONTO_MONEDA'].replace(',', '') + "|" +
							record.data['TIPO_CONTRATACION'].replace(',', '') + "|" +
							record.data['FECHA_INICIO_CONTRATO'].replace(',', '') + "|" +
							record.data['FECHA_FIN_CONTRATO'].replace(',', '') + "|" +
							record.data['PLAZO_CONTRATO'].replace(',', '') + "|" +
							record.data['OBJETO_CONTRATO'].replace(',', '') + "|" +
							record.data['FEXTIONCONTRATO'].replace(',', '') + "|" +
							record.data['CLABE'].replace(',', '') + "|" +
							record.data['BANCO_DEPOSITO'].replace(',', '') + "|" +
							record.data['ESTATUS_SOLICITUD'].replace(',', '') + "|";
							if(tipoOperacion==="A"){
								textoFirmar += descVentanilla.replace(',', '') + "|";
							}
							textoFirmar +=	"\n";
				});
				
				causasRechazo = Ext.getCmp('causasRechazo').getValue();
					
				NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar, form.clave_solicitud, tipoOperacion, causasRechazo , form.vtanilla   );
				
				
			}
		return;
	}
	
	
	var confirmarNotificacion = function(pkcs7, textoFirmar, clave_solicitud, vaccion   ){
		
		if (Ext.isEmpty(pkcs7) ) {
			Ext.Msg.alert('Mensaje','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;
		}

		Ext.getCmp('idCancelAccion').show();
		pnl.el.mask('Enviando...', 'x-mask-loading');
		Ext.Ajax.request({
			url : '34conExtinContratoExt.data.jsp',
			params : {
				informacion:		'AcuseExtNotifica',
				clave_solicitud:	clave_solicitud,
				tipoOperacion:		vaccion,
				causasRechazo:		Ext.getCmp('causasRechazo').getValue(),
				catVentanillaC:	'',
				TextoFirmado:		textoFirmar,
				Pkcs7:				pkcs7
			},
			callback: procesarSuccessFailureFirmaAccion
		});
		

	}

	function firmarAccionNotificacion(accion){
		var registro = gridAccion.getStore().getAt(0);
		var textoFirmar =	"Intermediario Financiero (Cesionario)|PYME (Cedente)|RFC|Representante Legal|No. de Contrato|Monto / Moneda|Tipo de Contrataci�n|Fecha Inicio Contrato|"+
								"Fecha Final Contrato|Area Autorizada|Objeto del Contrato|Fecha de Extinci�n del contrato|Cuenta|Cuenta CLABE|"+
								"Banco de Deposito|\n";
		var tmpEmpresas = replaceAll(registro.get('NOMBRE_PYME'),'<br>',';');
		textoFirmar += registro.get('NOMBRE_IF').replace(',', '') + "|" +
				registro.get('NOMBRE_PYME').replace(',', '') + "|" +
				registro.get('RFC').replace(',', '') + "|" +
				registro.get('REPRESENTA_LEGAL').replace(',', '') + "|" +
				registro.get('NUMERO_CONTRATO').replace(',', '') + "|" +
				registro.get('MONTO_MONEDA').replace(',', '') + "|" +
				registro.get('TIPO_CONTRATACION').replace(',', '') + "|" +
				registro.get('FECHA_INICIO_CONTRATO').replace(',', '') + "|" +
				registro.get('FECHA_FIN_CONTRATO').replace(',', '') + "|" +
				registro.get('PLAZO_CONTRATO').replace(',', '') + "|" +
				registro.get('OBJETO_CONTRATO').replace(',', '') + "|" +
				registro.get('FEXTIONCONTRATO').replace(',', '') + "|" +
				registro.get('CLABE').replace(',', '') + "|" +
				registro.get('BANCO_DEPOSITO').replace(',', '') + "|" +
				registro.get('ESTATUS_SOLICITUD').replace(',', '') + "|";
				textoFirmar +=	"\n";

		
		NE.util.obtenerPKCS7(confirmarNotificacion, textoFirmar, form.clave_solicitud , accion    );
		

		return;
	}

	function processResult(){
			var col = Ext.getCmp('gridAccion').getColumnModel().findColumnIndex('CAUSASRECHAZO_B')
			Ext.getCmp('gridAccion').getView().scroller.scrollTo('right', 7000, { easing: 'bounceOut' });
			Ext.getCmp('gridAccion').startEditing(0,col);
			return;
	}

	var procesaConsultaEnvioCorreo =  function(opts, success, response) {
		Ext.getCmp('winAccion').el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var infoR = Ext.util.JSON.decode(response.responseText);
				if (infoR.respuesta != undefined && infoR.respuesta != 'S'){
					Ext.Msg.show({title:'Autorizar',	msg:'Es necesario enviar el correo.',buttons:	Ext.Msg.OK,	icon: Ext.MessageBox.WARNING});
				}else{
					firmarAccion();
				}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var processSendMail =  function(opts, success, response) {
		Ext.getCmp('winAccion').el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var infoR = Ext.util.JSON.decode(response.responseText);
				if (infoR != undefined){
					Ext.getCmp('panelEnvio').show();
					if(infoR.respuesta === 'EXITO'){
						Ext.getCmp('btnCloseMail').show();
						Ext.getCmp('btnCancelMail').hide();
						Ext.getCmp('idEnviar').hide();
						Ext.getCmp('panelEnvio').body.update('<table align="center"><tr><td class="celda02" align="center"><b>El correo se envi� con �xito</b></td></tr></table>');
					}else if (infoR.respuesta === 'ERROR' || Ext.isEmpty(infoR.respuesta) ){
						Ext.getCmp('panelEnvio').body.update('<table align="center"><tr><td class="celda02" align="center"><b>El correo NO se envi�</b></td></tr></table>');
					}
				}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarAccion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		form.clave_solicitud =registro.get('CLAVE_SOLICITUD');
		form.vtanilla =registro.get('VENTANILLA');
		if (item.tooltip==='Autorizar'){
			if (Ext.isEmpty(form.vtanilla)){
				Ext.Msg.alert('Acci�n','Es necesario seleccionar la ventanilla destino');
				return;
			}
		}

		consultaAccionData.removeAll();
		var rowReg = registro;
		consultaAccionData.add(rowReg);

		var store = grid.getStore();
		var jsonData = store.reader.jsonData;
		var  clasificacionEpo = jsonData.clasificacionEpo;
		var camposAdd=null;
		if (jsonData.camposAdicionalesParametrizados != undefined && jsonData.camposAdicionalesParametrizados.length > 0){
			camposAdd = jsonData.camposAdicionalesParametrizados;
		}
		if (!gridAccion.isVisible()) {
			gridAccion.show();
		}
		var cmA = gridAccion.getColumnModel();
		cmA.setHidden(cmA.findColumnIndex('VENTANILLA'), true);
		cmA.setHidden(cmA.findColumnIndex('CAUSASRECHAZO'), true);
		cmA.setHidden(cmA.findColumnIndex('CAUSASRECHAZO_B'), true);
		if(Ext.isEmpty(clasificacionEpo)){
			cmA.setHidden(cmA.findColumnIndex('CLASIFICACION_EPO'), true);
		}else{
			cmA.setColumnHeader(cmA.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
			cmA.setColumnTooltip(cmA.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
		}
		var x=1;
		for(x=1;x<=5;x++){
			cmA.setHidden(cmA.findColumnIndex('CAMPO_ADICIONAL_'+x), true);
		}
		Ext.each(camposAdd, function(item, index, arrItems){
			cmA.setHidden(cmA.findColumnIndex('CAMPO_ADICIONAL_'+(index+1)), false);
			cmA.setColumnHeader(cmA.findColumnIndex('CAMPO_ADICIONAL_'+(index+1)),item.campoAdd);
			cmA.setColumnTooltip(cmA.findColumnIndex('CAMPO_ADICIONAL_'+(index+1)),item.campoAdd);
		});
		
		var winVen = Ext.getCmp('winAccion');
		
		if (item.tooltip==="Autorizar"){

			var fpCorreo = new Ext.form.FormPanel({
				id:'fpCorreo',	width: 610,	style: ' margin:0 auto;',	frame: true, bodyStyle: 'padding: 15px',labelWidth: 50,
				defaultType: 'textfield',	defaults: {msgTarget: 'side',	anchor: '-20'},
				items: [
					{
						xtype:'panel',
						html:'<div align="right">Separar correos por comas (,) Ejem. usuario1@nafin.gob.mx, usuario2@nafin.gob.mx</div><br>'
					},{
						id:'correoPara',	name:'correoPara',	fieldLabel:'Para', maxLength:500, allowBlank:false, enableKeyEvents:true,	vtype: 'variosMails',
						listeners:{
							'keydown':function(field){
											if( !Ext.isEmpty(field.getValue()) && field.getValue().indexOf(" ") != -1 ){
												var txt = field.getValue().replace(' ','');
												field.setValue(txt);
											}
										}
						}
					},{
						id:'asunto',	name:'asunto',	fieldLabel:'Asunto',	allowBlank:false,	maxLength:500,value:'Extinci�n de Contrato'
					},{
						xtype:'panel',	layout:'form',	labelAlign:'top',	defaults: {msgTarget: 'side',	anchor: '-20'	},
						items:[{xtype:'textarea',id:'comentarios',	name:'comentarios',	fieldLabel:'Comentarios', maxLength:150,	anchor:'100%',enableKeyEvents: true,
								listeners:{
									'keyup':	function(txtA){
													if (	!Ext.isEmpty(txtA.getValue())	){
														var numero = (txtA.getValue().length);
														if (numero > 150){
															var cadena = (txtA.getValue()).substring(0,150);
															txtA.setValue(cadena);
															Ext.Msg.alert('Comentarios','No puedes ingresar m�s de 150 caracteres.');
															return false;
														}
													}
												}
								}
						}]
					},{
						xtype:'panel',	id:'panelEnvio', hidden:true,html:'&nbsp;'
					}
				],
				monitorValid: true,
				buttonAlign:'center',
				buttons: [
					{
						text: 'Enviar',
						id: 'idEnviar',
						iconCls: 'icoContinuar',
						formBind: true,
						handler: function(boton, evento) {
										Ext.getCmp('winAccion').el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url:'34conExtinContratoExt.data.jsp',
											params:Ext.apply(fpCorreo.getForm().getValues(),{
														informacion:'EnviarMail',
														clave_solicitud:form.clave_solicitud,
														catVentanillaC: form.vtanilla
											}),
											callback: processSendMail
										});
						} //fin handler
					},{
						text: 'Cancelar',
						id:	'btnCancelMail',
						iconCls: 'icoRechazar',
						handler: function() {
							fp.hide();
							grid.hide();
							Ext.getCmp('winAccion').hide();
							Ext.getCmp('idAccion').show();
							Ext.getCmp('idCancelAccion').show();
							pnl.insert(0,gridAccion);
							pnl.doLayout();
							if(!gridAccion.isVisible()){
								gridAccion.show();
							}
							var cmm = gridAccion.getColumnModel();
							cmm.setHidden(cmm.findColumnIndex('CAUSASRECHAZO'), true);
						}
					},{
						text: 'Cerrar Ventana',
						id:	'btnCloseMail',
						hidden:true,
						iconCls: 'icoLimpiar',
						handler: function() {
							fp.hide();
							grid.hide();
							Ext.getCmp('winAccion').hide();
							Ext.getCmp('panelMsg').show();
							Ext.getCmp('idAccion').show();
							Ext.getCmp('idCancelAccion').show();
							pnl.insert(0,gridAccion);
							pnl.doLayout();
							if(!gridAccion.isVisible()){
								gridAccion.show();
							}
						}
					}
				]
			});

			Ext.getCmp('idAccion').setText('Autorizar');
			cmA.setHidden(cmA.findColumnIndex('VENTANILLA'), false);
			cmA.setHidden(cmA.findColumnIndex('CAUSASRECHAZO'), false);
			if (winVen){
				winVen.show();
			}else{
				var winAccion = new Ext.Window ({
					id:'winAccion',
					x: 20,
					y: 100,
					width: 960,
					modal: true,
					autoHeight: true,
					resizable:false,
					closeAction: 'hide',
					title: item.tooltip,
					items:[NE.util.getEspaciador(10),gridAccion,NE.util.getEspaciador(10),fpCorreo,NE.util.getEspaciador(10)]
				}).show();
			}
		} else if (item.tooltip==='Rechazar'){	//si presiono rechazar...

			fp.hide();
			grid.hide();
			if (winVen){
				winVen.hide();
			}
			Ext.getCmp('idAccion').show();
			Ext.getCmp('idCancelAccion').show();
			Ext.getCmp('panelMsg').show();
			Ext.getCmp('idAccion').setText('Rechazar');
			pnl.insert(0,gridAccion);
			pnl.doLayout();
			if(!gridAccion.isVisible()){
				gridAccion.show();
			}
			cmA.setHidden(cmA.findColumnIndex('CAUSASRECHAZO_B'), false);
		}
	}

	var muestraAcuseNoficacion = function(grid,rowIndex,accion){

		var registro = grid.getStore().getAt(rowIndex);
		form.clave_solicitud =registro.get('CLAVE_SOLICITUD');

		if(accion === 'Autorizar'){
			registro.data.ESTATUS_SOLICITUD = 'Extinci�n Notificaci�n Aceptada';
			Ext.getCmp('idAccion').setText('Autorizar');
		}else if (accion === 'Rechazar'){
			registro.data.ESTATUS_SOLICITUD = 'Extinci�n Notificaci�n Rechazada';
			Ext.getCmp('idAccion').setText('Rechazar');
		}else{
			Ext.Msg.alert('Mensaje','Error en el proceso');
		}
		registro.commit();
		var rowReg = registro;
		consultaAccionData.removeAll();
		consultaAccionData.add(rowReg);

		var store = grid.getStore();
		var jsonData = store.reader.jsonData;
		var  clasificacionEpo = jsonData.clasificacionEpo;
		var camposAdd=null;
		if (jsonData.camposAdicionalesParametrizados != undefined && jsonData.camposAdicionalesParametrizados.length > 0){
			camposAdd = jsonData.camposAdicionalesParametrizados;
		}

		var cmA = gridAccion.getColumnModel();
		cmA.setHidden(cmA.findColumnIndex('VENTANILLA'), true);
		cmA.setHidden(cmA.findColumnIndex('CAUSASRECHAZO'), true);
		cmA.setHidden(cmA.findColumnIndex('CAUSASRECHAZO_B'), true);
		if(Ext.isEmpty(clasificacionEpo)){
			cmA.setHidden(cmA.findColumnIndex('CLASIFICACION_EPO'), true);
		}else{
			cmA.setColumnHeader(cmA.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
			cmA.setColumnTooltip(cmA.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
		}
		var x=1;
		for(x=1;x<=5;x++){
			cmA.setHidden(cmA.findColumnIndex('CAMPO_ADICIONAL_'+x), true);
		}
		Ext.each(camposAdd, function(item, index, arrItems){
			cmA.setHidden(cmA.findColumnIndex('CAMPO_ADICIONAL_'+(index+1)), false);
			cmA.setColumnHeader(cmA.findColumnIndex('CAMPO_ADICIONAL_'+(index+1)),item.campoAdd);
			cmA.setColumnTooltip(cmA.findColumnIndex('CAMPO_ADICIONAL_'+(index+1)),item.campoAdd);
		});

		if (!gridAccion.isVisible()) {
			gridAccion.show();
		}
		Ext.getCmp('idAccion').show();
		Ext.getCmp('idCancelAccion').show();
		if (accion === 'Rechazar'){
			cmA.setHidden(cmA.findColumnIndex('CAUSASRECHAZO_B'), false);
		}
		panelMsg.show();
		fp.hide();
		grid.hide();

	}

	var procesarAccionNotificacion = function(grid,rowIndex,colIndex,item,event){
		form.notificacionAceptada = true;
		muestraAcuseNoficacion(grid,rowIndex,item.tooltip);
	}

///*-/*-/*-/-*/*-/-*/*-/*-Inicia Contrato de Cesion
	var procesarDescargaContratoCesion =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var descargaContratoCesion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');
		var clave_if = registro.get('CLAVE_IF');
		Ext.Ajax.request({
			url: '34conExtinContratoExt.data.jsp',
			params:{
						informacion: 'ContratoCesionPyme',
						tipo_archivo: 'CONTRATO_CESION_PYME',
						clave_solicitud: clave_solicitud,
						clave_epo: clave_epo,
						clave_if: clave_if		},
			callback: procesarDescargaContratoCesion
		});
	}
///*-/*-/*-/-*/*-/-*/*-/*-Termina Contrato de Cesion

///*-/*-/*-/*-/*-/*-/- Inicia ver Convenio de Extincion
	/*var verConvenioExtincion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var clave_epo = registro.get('CLAVE_EPO');		
		var ventana = Ext.getCmp('verConvenio');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				title: 'Convenio de Extinci�n',	width: 661,	height: 750,	id: 'verConvenio',	closeAction: 'hide',	autoScroll: true,	resizable:false,	modal:true,
				items: [	{	xtype:'panel',	id: 'Convenio',	width: 630,	autoHeight: true}	],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	buttons: ['-',{xtype: 'button',	text: 'Cerrar',		id: 'btnCerrar', handler: function(){Ext.getCmp('verConvenio').hide();} },'-']
				}
			}).show();
		}
		var bodyPanel = Ext.getCmp('Convenio').body;
		var mgr = bodyPanel.getUpdater();
		mgr.on('failure',
			function(el, response) {
				bodyPanel.update('');
				NE.util.mostrarErrorResponse(response);
			}
		);		
		mgr.update({
			url: '/nafin/34cesion/34pki/34ConvenioExtincionCesionDerechosExt.jsp',
			scripts: true,
			params: {
				clave_solicitud: clave_solicitud,
				clave_epo: clave_epo
			},
			indicatorText: 'Convenio de Extinci�n'
		});	
	}*/
///*-/*-/*-/*-/*-/*-/- Termina ver Convenio de Extincion

///*-/*-/*-/*-/*-/*-/- Inicia Descarga el Archivo de Contrato
	var procesarSuccessFailureContrato =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	var descargarArchivoContrato = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '34conExtinContratoExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion:		'Contrato',			
				clave_solicitud:	clave_solicitud
			}),
			callback: procesarSuccessFailureContrato
		});
	}
///*-/*-/*-/*-/*-/*-/- Termina Descarga el Archivo de Contrato

///*-/*-/*-/*-/*-/*-/- Inicia Poderes
	var procesarPoderesPyme =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var ventana = Ext.getCmp('VerPoderesPyme');
			if (ventana) {
				ventana.show();
			} else {
				new Ext.Window({
						title: 'Poderes Pyme',	width:300,	height:115,	id:'VerPoderesPyme',	closeAction:'hide',	modal:true, layout: 'fit', resizable:false,
						items: [
							{
								xtype:'panel', layout:'table', layoutConfig:{columns:2}, id:'panelPoderesPyme', style:'margin:0 auto;',
								defaults:{bodyStyle:'padding:20px', frame:false, border:false},
								items:[	{html:'&nbsp;', id:'pnlMsg', width:180} , {xtype:'button',	id:'btnPdf',	iconCls:'icoPdf',	width:20,	hidden:true}	]
							}
						],
						bbar: {
							xtype:'toolbar',	buttonAlign:'center',	buttons: ['-',{xtype: 'button',	text: 'Cerrar',	id: 'btnCerrarP', handler: function(){Ext.getCmp('VerPoderesPyme').hide();} },'-']
						}
				}).show();
			}
			if (infoR.urlArchivo != undefined){
				Ext.getCmp('pnlMsg').body.update('<div align="right">Documentos Pyme </div>');
				Ext.getCmp('btnPdf').setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = infoR.urlArchivo;
					forma.submit();
				});
				Ext.getCmp('btnPdf').show();
			}else{
				Ext.getCmp('pnlMsg').body.update('<div align="center">No se encontraron registros.</div>');
				Ext.getCmp('btnPdf').hide();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		//para ver los poderes de la pyme
	var VerPoderesPyme = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLAVE_PYME');	
		var rfcPyme = registro.get('RFC');	
		var grupo_cesion = registro.get('GRUPO_CESION');	
		var no_contrato = registro.get('NUMERO_CONTRATO');	
			
		var parametros = "clavePyme="+clavePyme+"&rfcPyme="+rfcPyme+"&grupo_cesion="+grupo_cesion+"&no_contrato="+no_contrato;
		
		var ventana = Ext.getCmp('VerPoderesPyme2');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
					layout: 'fit',
					width: 500,
					height: 200,			
					id: 'VerPoderesPyme2',
					closeAction: 'hide',
					items: [					
						PanelVerPoderesPyme2
					],
					title: 'Ver Poderes Pyme'					
			}).show();
		}	
		
		var pabelBody = Ext.getCmp('PanelVerPoderesPyme2').body;
			var mgr = pabelBody.getUpdater();
			mgr.on('failure', 
			function(el, response) {
				pabelBody.update('');
				NE.util.mostrarErrorResponse(response);
			});		
			mgr.update({
				url: '/nafin/34cesion/34pki/34pyme/34SolicConsVerPoderesPymeExt.jsp?'+parametros,						
				indicatorText: 'Cargando Ver Poderes Pyme '
			});					
	}
	
	var PanelVerPoderesPyme2 = {
		xtype: 'panel',
		id: 'PanelVerPoderesPyme2',
		width: 700,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	};
///*-/*-/*-/*-/*-/*-/- Termina Poderes

	var procesarSuccessFailureGenerarArchivo = function (opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler ( function (boton,evento) {
				var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDF = function (opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDF.setHandler(function (boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureDataPrintAcuse = function (opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);

			Ext.getCmp('idPdfAccion').setHandler ( function (boton,evento) {
				if (infoR.urlArchivo != undefined){
					var archivo = infoR.urlArchivo;
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};
					fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					fp.getForm().getEl().dom.submit();
				}
			});
			Ext.getCmp('idPdfAccion').show();
			if(infoR.registros != undefined){
				consultaAccionData.loadData(infoR.registros);
				Ext.getCmp('causasRechazo').setReadOnly(true);
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) 	{

		var jsonData = store.reader.jsonData;
		var  clasificacionEpo = jsonData.clasificacionEpo;
		var camposAdd=null;
		if (jsonData.camposAdicionalesParametrizados != undefined && jsonData.camposAdicionalesParametrizados.length > 0){
			camposAdd = jsonData.camposAdicionalesParametrizados;
		}
		pnl.el.unmask();

		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}	
			var cm = grid.getColumnModel();
			if(Ext.isEmpty(clasificacionEpo)){
				cm.setHidden(cm.findColumnIndex('CLASIFICACION_EPO'), true);					
			}else{
				cm.setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
				cm.setColumnTooltip(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
			}
			var x=1;
			for(x=1;x<=5;x++){
				cm.setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_'+x), true);
			}
			Ext.each(camposAdd, function(item, index, arrItems){
				cm.setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_'+(index+1)), false);
				cm.setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_'+(index+1)),item.campoAdd);
				cm.setColumnTooltip(cm.findColumnIndex('CAMPO_ADICIONAL_'+(index+1)),item.campoAdd);
			});

			var el = grid.getGridEl();
			var btnImprimirPDF = Ext.getCmp('btnGenerarPDF');
			var btnImprimirCSV = Ext.getCmp('btnGenerarArchivo');
			Ext.getCmp('btnBajarPDF').hide();
			Ext.getCmp('btnBajarArchivo').hide();
				
			if(store.getTotalCount() > 0) {
				Ext.getCmp('panelMsg').update(jsonData.MENSAJE);
				//panelMsg.show();
				btnImprimirPDF.enable();
				btnImprimirCSV.enable();
				el.unmask();
				grid.getView().scroller.scrollTo('right', 6500, { easing: 'bounceOut' });	//linea temporal por Hugo V.C.
			} else {
				panelMsg.hide();
				btnImprimirPDF.disable();
				btnImprimirCSV.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var catalogoIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34conExtinContratoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoIf'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoNombrePymeData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34conExtinContratoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoNombrePyme'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34conExtinContratoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoTipoContratacionData = new Ext.data.JsonStore({
		id: 'catalogoTipoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34conExtinContratoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoContratacion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoVentanillaData = new Ext.data.JsonStore({
		id: 'catalogoVentanillaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34conExtinContratoExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoVentanilla'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaAccionData = new Ext.data.JsonStore({
		id:'consultaAccionData',
		fields: [
			{name: 'CLAVE_SOLICITUD',type: 'float'},{name: 'CLAVE_EPO',type: 'float'},{name: 'CLAVE_IF',type: 'float'},{name: 'CLAVE_PYME',type: 'float'},{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_PYME'},{name: 'RFC'},{name: 'MONTO_MONEDA'},{name: 'NUMERO_PROVEEDOR'},{name: 'REPRESENTA_LEGAL'},{name: 'FECHA_SOL_PYME'},{name: 'NOMBRE_CONTRATO'},
			{name: 'NUMERO_CONTRATO'},{name: 'TIPO_CONTRATACION'},{name: 'FECHA_INICIO_CONTRATO'},{name: 'FECHA_FIN_CONTRATO'},{name: 'PLAZO_CONTRATO'},
			{name: 'CLASIFICACION_EPO'},{name: 'CAMPO_ADICIONAL_1'},{name: 'CAMPO_ADICIONAL_2'},{name: 'CAMPO_ADICIONAL_3'},{name: 'CAMPO_ADICIONAL_4'},{name: 'CAMPO_ADICIONAL_5'},
			{name: 'OBJETO_CONTRATO'},{name: 'COMENTARIOS'},{name: 'MONTO_CREDITO'},{name: 'NUMERO_REFERENCIA'},{name: 'FECHA_VENCIMIENTO'},{name: 'BANCO_DEPOSITO'},{name: 'NUMERO_CUENTA'},
			{name: 'CLABE'},{name: 'CLAVE_ESTATUS'},{name: 'ESTATUS_SOLICITUD'},{name: 'CAUSAS_RECHAZO_NOTIF'},{name: 'CAUSASRECHAZO'},{name: 'FEXTIONCONTRATO'},{name: 'CORREO'},{name: 'VENTANILLA'},{name: 'CAUSASRECHAZO_B'}
		],
		data:	[{'CLAVE_SOLICITUD':0,'CLAVE_EPO':0,'CLAVE_IF':0,'CLAVE_PYME':0,'NOMBRE_IF':'','NOMBRE_PYME':'','RFC':'','MONTO_MONEDA':'','NUMERO_PROVEEDOR':'',
					'REPRESENTA_LEGAL':'','FECHA_SOL_PYME':'','NOMBRE_CONTRATO':'','NUMERO_CONTRATO':'','TIPO_CONTRATACION':'','FECHA_INICIO_CONTRATO':'','FECHA_FIN_CONTRATO':'',
					'PLAZO_CONTRATO':'','CLASIFICACION_EPO':'','CAMPO_ADICIONAL_1':'','CAMPO_ADICIONAL_2':'','CAMPO_ADICIONAL_3':'','CAMPO_ADICIONAL_4':'','CAMPO_ADICIONAL_5':'',
					'OBJETO_CONTRATO':'','COMENTARIOS':'','MONTO_CREDITO':'','NUMERO_REFERENCIA':'','FECHA_VENCIMIENTO':'','BANCO_DEPOSITO':'','NUMERO_CUENTA':'','CLABE':'',
					'CLAVE_ESTATUS':'','ESTATUS_SOLICITUD':'','CAUSAS_RECHAZO_NOTIF':'','CAUSASRECHAZO':'','FEXTIONCONTRATO':'','CORREO':'','VENTANILLA':'','CAUSASRECHAZO_B':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34conExtinContratoExt.data.jsp',
		baseParams: {
			informacion: 'Consultar',
			operacion: 'Generar'
		},
		fields: [
			{name: 'CLAVE_SOLICITUD',type: 'float'},
			{name: 'CLAVE_EPO',type: 'float'},
			{name: 'CLAVE_IF',type: 'float'},
			{name: 'CLAVE_PYME',type: 'float'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_PYME'},
			{name: 'RFC'},
			{name: 'MONTO_MONEDA'},
			{name: 'NUMERO_PROVEEDOR'},
			{name: 'REPRESENTA_LEGAL'},
			{name: 'FECHA_SOL_PYME' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMBRE_CONTRATO'},
			{name: 'NUMERO_CONTRATO'},
			{name: 'TIPO_CONTRATACION'},
			{name: 'FECHA_INICIO_CONTRATO'},
			{name: 'FECHA_FIN_CONTRATO'},
			{name: 'PLAZO_CONTRATO'},
			{name: 'CLASIFICACION_EPO'},
			{name: 'CAMPO_ADICIONAL_1'},
			{name: 'CAMPO_ADICIONAL_2'},
			{name: 'CAMPO_ADICIONAL_3'},
			{name: 'CAMPO_ADICIONAL_4'},
			{name: 'CAMPO_ADICIONAL_5'},
			{name: 'OBJETO_CONTRATO'},
			{name: 'COMENTARIOS'},
			{name: 'MONTO_CREDITO'},
			{name: 'NUMERO_REFERENCIA'},
			{name: 'FECHA_VENCIMIENTO' ,type: 'date', dateFormat: 'd/m/Y'},
			{name: 'BANCO_DEPOSITO'},
			{name: 'NUMERO_CUENTA'},
			{name: 'CLABE'},
			{name: 'CLAVE_ESTATUS'},
			{name: 'ESTATUS_SOLICITUD'},
			{name: 'CAUSAS_RECHAZO_NOTIF'},
			{name: 'CAUSASRECHAZO'},
			{name: 'FEXTIONCONTRATO'},	//type: 'date', dateFormat: 'd/m/Y'
			{name: 'CORREO'},
			{name: 'VENTANILLA'},
			{name: 'GRUPO_CESION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var gridAccion = new Ext.grid.EditorGridPanel({
		id:'gridAccion',store: consultaAccionData,	columLines:true,	clicksToEdit:1,
		stripeRows:true,	loadMask:true,	height:110,	width:940,	style:'margin:0 auto;',	frame:false, hidden:true,
		columns: [
			{
				header: 'Intermediario Financiero (Cesionario)',	tooltip: 'Intermediario Financiero (Cesionario)',	dataIndex:'NOMBRE_IF',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header:'PYME (Cedente)',	tooltip:'PYME (Cedente)',	dataIndex:'NOMBRE_PYME',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return '<div align=left>'+value+'</div>';
				}
			},{
				header:'RFC',	tooltip:'RFC',	dataIndex:'RFC',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Representante Legal',	tooltip:'Representante Legal',	dataIndex:'REPRESENTA_LEGAL',	sortable:true,	resizable:true,	width:200,	align:'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header:'No. de Contrato',	tooltip:'No. de Contrato',	dataIndex:'NUMERO_CONTRATO',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Monto / Moneda',	tooltip:'Monto / Moneda',	dataIndex:'MONTO_MONEDA',	sortable:true,	resizable:true,	width:130,	align:'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header:'Tipo de Contrataci�n',	tooltip:'Tipo de Contrataci�n',	dataIndex:'TIPO_CONTRATACION',	sortable:true,	resizable:true,	width:150,	align:'center'
			},{
				header: 'Fecha Inicio Contrato',	tooltip:'Fecha Inicio Contrato',dataIndex:'FECHA_INICIO_CONTRATO',sortable:true,	resizable:true,	width:120,	align:'center'
			},{
				header:'Fecha Final Contrato',	tooltip:'Fecha Final Contrato',dataIndex:'FECHA_FIN_CONTRATO',	sortable:true,	resizable:true,	width:120,	align:'center'
			},{
				header: 'Plazo del Contrato',	tooltip: 'Plazo del Contrato',	dataIndex: 'PLAZO_CONTRATO',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'clasificacionEpo',	tooltip: 'Clasificaci�n Epo',	dataIndex: 'CLASIFICACION_EPO',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 1',	tooltip: 'campo Adicional 1',	dataIndex: 'CAMPO_ADICIONAL_1',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 2',	tooltip: 'campo Adicional 2',	dataIndex: 'CAMPO_ADICIONAL_2',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 3',	tooltip: 'campo Adicional 3',	dataIndex: 'CAMPO_ADICIONAL_3',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 4',	tooltip: 'campo Adicional 4',	dataIndex: 'CAMPO_ADICIONAL_4',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 5',	tooltip: 'campo Adicional 5',	dataIndex: 'CAMPO_ADICIONAL_5',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'Objeto del Contrato',	tooltip: 'Objeto del Contrato',	dataIndex: 'OBJETO_CONTRATO',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header : 'Fecha de Extinci�n del Contrato',	tooltip: 'Fecha de Extinci�n del Contrato',	dataIndex : 'FEXTIONCONTRATO',
				width : 150,	sortable:false,	hidden:false,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {	if(Ext.isEmpty(value)){	value = "N/A"	}	return value;	}
			},{
				header : 'Cuenta',	tooltip: 'Cuenta',	dataIndex : 'NUMERO_CUENTA',	width : 150,	sortable : false,	hidden: false,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {	if(Ext.isEmpty(value)){	value = "N/A"	}	return value;	}
			},{
				header : 'Cuenta CLABE',	tooltip: 'Cuenta CLABE',	dataIndex : 'CLABE',	width : 150,	sortable : false,	hidden: false,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {	if(Ext.isEmpty(value)){	value = "N/A"	}	return value;	}
			},{
				header : 'Banco de Dep�sito',	tooltip: 'Banco de Dep�sito',	dataIndex : 'BANCO_DEPOSITO',	width : 150,	sortable : false,	hidden: false,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {	if(Ext.isEmpty(value)){	value = "N/A"	}	return value;	}
			},{
				header: 'Causas de Rechazo',	tooltip: 'Causas de Rechazo',	dataIndex: 'CAUSASRECHAZO',	sortable: true,	resizable: true,	width: 130,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {	if(Ext.isEmpty(value)){	value = "N/A"	}	return value;	}
			},{
				header: 'Estatus',	tooltip: 'Estatus',	dataIndex: 'ESTATUS_SOLICITUD',	sortable: true,	resizable: true,	width: 180,	align: 'center'
			},{
				header: 'Ventanilla Destino',	tooltip: 'Ventanilla Destino',	dataIndex: 'VENTANILLA',
				hidden:true,	sortable: true,	resizable: true,	width: 130,	align: 'center',	hideable:false,
				renderer:function(value,metadata,registro){
								var dato = catalogoVentanillaData.findExact("clave", form.vtanilla);
								if (dato != -1){
									var reg = catalogoVentanillaData.getAt(dato);
									value = reg.get('descripcion');
								}
								return value;
							}
			},{
				header: 'Causas de Rechazo',	tooltip: 'Causas de Rechazo',	dataIndex: 'CAUSASRECHAZO_B',
				sortable:true, hidden:true,	resizable:true,	width:230,	align:'center',	hideable:false,
				editor: {xtype:'textarea', id:'causasRechazo',enableKeyEvents: true,
							listeners:{
								keyup:	function(txtA){
												if (	!Ext.isEmpty(txtA.getValue())	){
													var numero = (txtA.getValue().length);
													if (numero > 150){
														var cadena = (txtA.getValue()).substring(0,150);
														txtA.setValue(cadena);
														Ext.Msg.alert('Comentarios','No puedes ingresar m�s de 150 caracteres.');
														txtA.focus();
														return;
													}
												}
											}
							}
				},
				renderer:function(value,metadata,registro){
								return NE.util.colorCampoEdit(value,metadata,registro);
							}
			}
		]
	});

	var grid = new Ext.grid.EditorGridPanel({
		store: consultaData,	hidden: true,	columLines:	true,	clicksToEdit: 1,	columnLines:true,	stripeRows:true,
		loadMask: true,	height: 400,	width: 940,	style: 'margin:0 auto;',	frame: false,
		columns: [
			{
				header: 'Intermediario Financiero (Cesionario)',	tooltip: 'Intermediario Financiero (Cesionario)',	dataIndex:'NOMBRE_IF',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header:'PYME (Cedente)',	tooltip:'PYME (Cedente)',	dataIndex:'NOMBRE_PYME',	sortable:true,	resizable:true,	width:200,	align:'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return '<div align=left>'+value+'</div>';
				}
			},{
				header:'RFC',	tooltip:'RFC',	dataIndex:'RFC',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Representante Legal',	tooltip:'Representante Legal',	dataIndex:'REPRESENTA_LEGAL',	sortable:true,	resizable:true,	width:200,	align:'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header:'No. de Contrato',	tooltip:'No. de Contrato',	dataIndex:'NUMERO_CONTRATO',	sortable:true,	resizable:true,	width:130,	align:'center'
			},{
				header:'Monto / Moneda',	tooltip:'Monto / Moneda',	dataIndex:'MONTO_MONEDA',	sortable:true,	resizable:true,	width:130,	align:'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header:'Tipo de Contrataci�n',	tooltip:'Tipo de Contrataci�n',	dataIndex:'TIPO_CONTRATACION',	sortable:true,	resizable:true,	width:150,	align:'center'
			},{
				header: 'Fecha Inicio Contrato',	tooltip:'Fecha Inicio Contrato',dataIndex:'FECHA_INICIO_CONTRATO',sortable:true,	resizable:true,	width:120,	align:'center'
			},{
				header:'Fecha Final Contrato',	tooltip:'Fecha Final Contrato',dataIndex:'FECHA_FIN_CONTRATO',	sortable:true,	resizable:true,	width:120,	align:'center'
			},{
				header: 'Plazo del Contrato',	tooltip: 'Plazo del Contrato',	dataIndex: 'PLAZO_CONTRATO',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'clasificacionEpo',	tooltip: 'Clasificaci�n Epo',	dataIndex: 'CLASIFICACION_EPO',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 1',	tooltip: 'campo Adicional 1',	dataIndex: 'CAMPO_ADICIONAL_1',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 2',	tooltip: 'campo Adicional 2',	dataIndex: 'CAMPO_ADICIONAL_2',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 3',	tooltip: 'campo Adicional 3',	dataIndex: 'CAMPO_ADICIONAL_3',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 4',	tooltip: 'campo Adicional 4',	dataIndex: 'CAMPO_ADICIONAL_4',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'campo Adicional 5',	tooltip: 'campo Adicional 5',	dataIndex: 'CAMPO_ADICIONAL_5',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				header: 'Objeto del Contrato',	tooltip: 'Objeto del Contrato',	dataIndex: 'OBJETO_CONTRATO',	sortable: true,	resizable: true,	width: 130,	align: 'center'
			},{
				xtype: 'actioncolumn',	header: 'Contrato de Cesi�n',	tooltip: 'Contrato de Cesi�n',	width: 130,	align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						},
						handler: descargarContratoCesion
					}
				]
			},{
				xtype: 'actioncolumn',	header: 'Convenio de Extinci�n',	tooltip: 'Convenio de Extinci�n',	width: 130,	align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						},
						handler: verConvenioExtincion
					}
				]
			},{
				xtype: 'actioncolumn',	header: 'Contrato',	tooltip: 'Contrato',	width: 130,	align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						},
						handler: descargarArchivoContrato
					}
				]
			},{
				xtype: 'actioncolumn',	header: 'Poderes',	tooltip: 'Poderes',	width: 130,	align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						},
						handler: VerPoderesPyme
					}
				]
			},{
				xtype: 'actioncolumn',	header: 'Poderes IF Extinci�n',	tooltip: 'Poderes IF Extinci�n',	width: 130,	align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						},
						handler: procesarPoderesIF
					}
				]
			},{
				header : 'Fecha de Extinci�n del Contrato',	tooltip: 'Fecha de Extinci�n del Contrato',	dataIndex : 'FEXTIONCONTRATO',
				width : 150,	sortable:false,	hidden:false,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if(Ext.isEmpty(value)){
									value = "N/A"
								}
								return value;
				}
			},{
				header : 'Cuenta',	tooltip: 'Cuenta',	dataIndex : 'NUMERO_CUENTA',	width : 150,	sortable : false,	hidden: false,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if(Ext.isEmpty(value)){
									value = "N/A"
								}
								return value;
				}				
			},{
				header : 'Cuenta CLABE',	tooltip: 'Cuenta CLABE',	dataIndex : 'CLABE',	width : 150,	sortable : false,	hidden: false,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if(Ext.isEmpty(value)){
									value = "N/A"
								}
								return value;
				}
			},{
				header : 'Banco de Dep�sito',	tooltip: 'Banco de Dep�sito',	dataIndex : 'BANCO_DEPOSITO',	width : 150,	sortable : false,	hidden: false,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if(Ext.isEmpty(value)){
									value = "N/A"
								}
								return value;
				}
			},{
				header: 'Causa de Rechazo',	tooltip: 'Causa de Rechazo',	dataIndex: 'CAUSASRECHAZO',	sortable: true,	resizable: true,	width: 130,	align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if(Ext.isEmpty(value)){
									value = "N/A"
								}
								return value;
				}
			},{
				header: 'Estatus',	tooltip: 'Estatus',	dataIndex: 'ESTATUS_SOLICITUD',	sortable: true,	resizable: true,	width: 150,	align: 'center'
			},{
				header: 'Ventanilla Destino',	tooltip: 'Ventanilla Destino',	dataIndex: 'VENTANILLA',	sortable: true,	resizable: true,	width: 130,	align: 'center',
				editor: {xtype : 'combo', id: 0,	mode: 'local',	displayField: 'descripcion',	valueField: 'clave',	forceSelection : true,	triggerAction : 'all',	
							typeAhead: true,	minChars : 1,store:catalogoVentanillaData,lazyInit: false, lazyRender: true, emptyText:'Seleccione...',
							listeners:{ focus:{ fn: function (comboField) { comboField.doQuery(comboField.allQuery, true); comboField.expand(); }},
											select:{ fn: function (comboField, record, index) { comboField.fireEvent('blur'); grid.getView().refresh(); }}
							}},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								if(registro.get('CLAVE_ESTATUS')	==	'23'){
									if(!Ext.isEmpty(value) && value != null){
										var dato = catalogoVentanillaData.findExact("clave", value);
										if (dato != -1 ) {
											var reg = catalogoVentanillaData.getAt(dato);
											value = reg.get('descripcion');
											return NE.util.colorCampoEdit(value,metadata,registro);
										}
									}else{
										return 'Seleccionar...';
									}
								}else{
									return 'N/A';
								}
							}
			},{
				xtype: 'actioncolumn',	header: 'Acci�n',	tooltip: 'Acci�n',	width: 130,	align: 'center',
				renderer: function(value,metaData,registro,rowIndex,colIndex,store){
								if(	registro.get('CLAVE_ESTATUS')	==	'21'	){
									value = "N/A";
									return value;
								}
				},
				items: [
					{ //Autorizar ventanilla
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
										if(	!Ext.isEmpty(registro.get('CLAVE_SOLICITUD'))	&&	registro.get('CLAVE_ESTATUS')	== '23'	){
											this.items[0].tooltip = 'Autorizar';
											return 'icoAceptar';
										}else{
											return null;
										}
						},
						handler: procesarAccion
					},{
						getClass:function(valor, metadata, registro, rowIndex, colIndex, store) {
										if(	!Ext.isEmpty(registro.get('CLAVE_SOLICITUD'))	&&	registro.get('CLAVE_ESTATUS')	==	'20'	){
											this.items[1].tooltip = 'Autorizar';
											return 'icoAceptar';
										}else{
											return null;
										}
						},
						handler: procesarAccionNotificacion
					},{//Autorizar notificaci�n
						getClass:function(valor, metadata, registro, rowIndex, colIndex, store) {
										if(	registro.get('CLAVE_ESTATUS')	!=	'21'	){
											this.items[2].tooltip = '';
											return 'vacio';
										}else{
											return null;
										}
						}
					},{//Rechazar
						getClass:function(valor, metadata, registro, rowIndex, colIndex, store) {
										if(	!Ext.isEmpty(registro.get('CLAVE_SOLICITUD'))	&&	registro.get('CLAVE_ESTATUS')	==	'23'	){
											this.items[3].tooltip = 'Rechazar';
											return 'icoRechazar';
										}else{
											return null;
										}
						},
						handler: procesarAccion
					},{//Rechazar Notificacion
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
										if(	!Ext.isEmpty(registro.get('CLAVE_SOLICITUD'))	&&	registro.get('CLAVE_ESTATUS')	==	'20'	){
											this.items[4].tooltip = 'Rechazar';
											return 'icoRechazar';
										}else{
											return null;
										}
						},
						handler: procesarAccionNotificacion
					}
				]
			}
		],
		listeners: {
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables
				if(e.field === 'VENTANILLA'){
					if(	e.record.get('CLAVE_ESTATUS')	==	'23'	){	//Modifica Hugo V.C.
						return true; // si es editable
					}else{
						return false; // no es editable
					}
				}
			}
		},
		bbar:{
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype:	'button',
					text:		'Generar PDF',
					iconCls:	'icoPdf',
					id:		'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '34conExtinContratoExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},{
					xtype:	'button',
					text:		'Bajar PDF',
					id:		'btnBajarPDF',
					hidden:	true
				},'-',{
					xtype:	'button',
					text:		'Generar Archivo',
					iconCls:	'icoXls',
					id:		'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34conExtinContratoExt.data.jsp',
							params:	Ext.apply(fp.getForm().getValues(),{
										informacion: 'ArchivoCSV'}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},'-',{
					xtype:	'button',
					text:		'Bajar Archivo',
					id:		'btnBajarArchivo',
					hidden:	true
				}
			]
		}
	});

	var panelMsg = new Ext.Panel({
		id:'panelMsg',
		width:610,
		hidden:true,
		autoHeigth:true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 5px 5px 5px 5px',
		hidden: true,
		border: true,
		frame: false,
		html:	'<table align="center" width="600" cellpadding="0" cellspacing="0" border="0" align="center">'+
					'<tr>'+
						'<td class="formas" style="text-align: justify;">'+
							'<p>Con base a lo dispuesto en el C�digo de Comercio por este conducto me permito notificar a esa Entidad la Extinci�n del Contrato de Cesi�n de Derechos de Cobro que fue formalizado con la empresa(s) CEDENTE respecto al contrato que aqu� mismo se detalla.'+
							'<p>Por lo anterior, me permito solicitar a usted, se registre la Extinci�n del Contrato de Cesi�n de Derechos de Cobro de referencia a efecto de que esa Empresa, realice el(los) pago(s) derivados del Contrato aqu� se�alado a favor del CEDENTE.'+
							'<p>Los datos de la Cuenta bancaria del CEDENTE en la que se deber�n depositar los pagos derivados de esta operaci�n son los que aqu� se detallan.'+
							'<p>Sobre el particular, por este medio les notifico, la Extinci�n del Contrato de Cesi�n de Derechos de Cobro antes mencionado, para todos los efectos legales a que haya lugar�.'+
						'</td>'+
					'</tr>'+
				'</table>',
		buttonAlign:'center',
		buttons: [
			{
				text: '',
				id: 'idAccion',
				iconCls: 'icoAceptar',
				//hidden:true,
				//formBind: true,
				handler: function(boton, evento) {
								if(boton.text === 'Autorizar'){
									if (!form.notificacionAceptada){
										Ext.getCmp('winAccion').el.mask('Enviando...', 'x-mask-loading');
										Ext.Ajax.request({
											url:'34conExtinContratoExt.data.jsp',
											params:{	informacion:'consultaEnvioCorreo',
														clave_solicitud:form.clave_solicitud	},
											callback: procesaConsultaEnvioCorreo
										});
									}else{
										firmarAccionNotificacion('Autorizar');
									}
								}else{
									if(	Ext.isEmpty(Ext.getCmp('causasRechazo').getValue())	){
										Ext.Msg.show({
											title:	boton.text,
											msg:		'Es obligatorio escribir las causas del rechazo.',
											buttons:	Ext.Msg.OK,
											fn: processResult,
											closable:false,
											icon: Ext.MessageBox.WARNING
										});
									}else{
										if (!form.notificacionAceptada){
											firmarAccion();
										}else{
											firmarAccionNotificacion('Rechazar');
										}
									}
								}
				} //fin handler
			},{
				text:		'Imprimir PDF',
				id:		'idPdfAccion',
				iconCls:	'icoPdf',
				hidden:	true
			},{
				text:		'Cancelar',
				id:		'idCancelAccion',
				iconCls:	'icoRechazar',
				hidden:	true,
				handler: function() {
					window.location = '34conExtinContratoExt.jsp';
				}
			}
		]
	});

	var elementosForma = [{
			xtype: 'combo',
			id:	'catIntermediario_id',
			name: 'catIntermediario',
			hiddenName : 'catIntermediario',
			fieldLabel: 'Nombre de Cesionario (IF)',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIfData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'textfield',
			name: 'noContrato',
			id: 'noContrato_id',
			allowBlank: true,
			vtype:'alphanum',
			fieldLabel:'N�mero de contrato',
			anchor:'50%',
			maxLength: 15
		},{
			xtype: 'combo',
			id:	'catPyme_id',
			name: 'catPyme',
			hiddenName : 'catPyme',
			fieldLabel: 'Pyme',
			emptyText: 'Seleccionar. . .',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			forceSelection:true,
			typeAhead: true,
			mode: 'local',
			minChars : 1,
			store: catalogoNombrePymeData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'combo',
			id:	'catMoneda_id',
			name: 'catMoneda',
			hiddenName : 'catMoneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'combo',
			id:	'catContratacion_id',
			name: 'catContratacion',
			hiddenName : 'catContratacion',
			fieldLabel: 'Tipo Contrataci�n',
			emptyText: 'Seleccionar. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoTipoContratacionData,
			tpl : NE.util.templateMensajeCargaCombo
	},{
		xtype:              'compositefield',
		fieldLabel:         'Fecha Firma de Contrato',
		msgTarget:          'side',
		combineErrors:      false,
		items: [{
			width:           110,
			xtype:           'datefield',
			id:              'fecha_inicio',
			name:            'fecha_inicio',
			msgTarget:       'side',
			vtype:           'rangofecha',
			minValue:        '01/01/1901',
			campoFinFecha:   'fecha_final',
			margins:         '0 20 0 0',
			allowBlank:      true,
			startDay:        0
		},{
			xtype:           'displayfield',
			value:           'a',
			width:           20
		},{
			width:           110,
			xtype:           'datefield',
			id:              'fecha_final',
			name:            'fecha_final',
			msgTarget:       'side',
			vtype:           'rangofecha',
			minValue:        '01/01/1901',
			campoInicioFecha:'fecha_inicio',
			margins:         '0 20 0 0',
			allowBlank:      true,
			startDay:        1
		},{
			xtype:           'displayfield',
			value:           'dd/mm/aaaa',
			width:           30
		}]
	}];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		title:'Extinci�n de Contrato',
		width: 610,
		style: ' margin:0 auto;',
		collapsible: true,
		titleCollapse: true,
		hidden:true,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {

					// Estos if son para validar las fechas
					if(!Ext.getCmp('forma').getForm().isValid()){
						return;
					}
					if(Ext.getCmp('fecha_inicio').getValue() == '' && !Ext.getCmp('fecha_final').getValue() == ''){
						Ext.getCmp('fecha_inicio').markInvalid('El campo es obligatorio.');
						return;
					}
					if(Ext.getCmp('fecha_final').getValue() == '' && !Ext.getCmp('fecha_inicio').getValue() == ''){
						Ext.getCmp('fecha_final').markInvalid('El campo es obligatorio.');
						return;
					}

					catalogoVentanillaData.load();
					pnl.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							operacion: 'Generar', 								
									start: 0,
									limit: 15
						})
					});
				} //fin handler
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fp.getForm().reset();
					panelMsg.hide();
					grid.hide();
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		items: [
			fp,	NE.util.getEspaciador(10),	gridAccion,	NE.util.getEspaciador(10),	panelMsg,	NE.util.getEspaciador(10),	grid
		]
	});

	pnl.el.mask('Procesando...', 'x-mask-loading');
	Ext.Ajax.request({
		url : '34conExtinContratoExt.data.jsp',
		params: {informacion: 'Parametrizacion'},
		callback: procesaValoresIniciales
	});

});
