<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.SimpleDateFormat,
		netropology.utilerias.*,
		com.netro.pdf.*,
		netropology.utilerias.usuarios.UtilUsr,
		netropology.utilerias.usuarios.Usuario,
		com.netro.cesion.*, java.math.*,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoPYME,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoVentanilla,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/34cesion/34secsession_ext.jspf"%>

<%
String informacion  = (request.getParameter("informacion")  !=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

if (informacion.equals("Parametrizacion")) {	//	*-*-*-*-*-*--*-*-*-*-*-*-*-*	Parametrizacion	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject jsonObj = new JSONObject();

	List regs = new ArrayList();
	List registros = cesionBean.consSoliExtincion(iNoCliente);

	if(registros.size()>0){
		for(int e = 0; e< registros.size(); e++){
			HashMap hash = new HashMap();
			List registros1 = (List)registros.get(e);
			String noSsolicitud = (String)registros1.get(0);
			String nombre=(String)registros1.get(1);
			String pyme  =(String)registros1.get(2);
			String rfc =(String)registros1.get(3);
			String fecha =(String)registros1.get(7);
			String noContrato =(String)registros1.get(5);
			String monto =(String)registros1.get(6);
			//FODEA-024-2014 MOD()
			String empresas =(String)registros1.get(8);
				pyme = pyme+((empresas!="")?";"+empresas:"");
			//
			hash.put("IC_SOLICITUD",noSsolicitud);
			hash.put("CECIONARIO",nombre);
			hash.put("PYME",pyme.replaceAll(";","<br>"));
			hash.put("RFC",rfc);
			hash.put("FSOLICITUDINI",fecha);
			hash.put("NOCONTRATO",noContrato);
			hash.put("MONTO_MONEDA",monto);
			regs.add(hash);
		}
		jsonObj.put("registros", regs);
	}

	jsonObj.put("success", new Boolean(true));

	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoIf")) {

	CatalogoIF cat = new CatalogoIF();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setCesionDerechos("S");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoNombrePyme")){

	CatalogoPYME cat = new CatalogoPYME();
	cat.setCampoClave("ic_pyme");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setCesionDerechos("S");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoMoneda")) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setOrden("ic_moneda");
	cat.setValoresCondicionIn("1,54,25", Integer.class);
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoTipoContratacion")) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("cdercat_tipo_contratacion");
	cat.setCampoClave("ic_tipo_contratacion");
	cat.setCampoDescripcion("cg_nombre");
	cat.setOrden("ic_tipo_contratacion");
	infoRegresar = cat.getJSONElementos();
 
}else if (informacion.equals("CatalogoVentanilla")) {

	CatalogoVentanilla cat = new CatalogoVentanilla();
	cat.setClave("ic_ventanilla");
	cat.setDescripcion("cg_descripcion");
	cat.setSeleccion(iNoCliente);
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consultar") || informacion.equals("ArchivoPDF") || informacion.equals("ArchivoCSV")){

	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

	String catIntermediario = (request.getParameter("catIntermediario")!=null)?request.getParameter("catIntermediario"):"";
	String noContrato       = (request.getParameter("noContrato")      !=null)?request.getParameter("noContrato"):"";
	String catPyme          = (request.getParameter("catPyme")         !=null)?request.getParameter("catPyme"):"";
	String catMoneda        = (request.getParameter("catMoneda")       !=null)?request.getParameter("catMoneda"):"";
	String catContratacion  = (request.getParameter("catContratacion") !=null)?request.getParameter("catContratacion"):"";
	String fecha_inicio     = (request.getParameter("fecha_inicio")    !=null)?request.getParameter("fecha_inicio"):"";
	String fecha_final      = (request.getParameter("fecha_final")     !=null)?request.getParameter("fecha_final"):"";
	int start = 0;
	int limit = 0;

	com.netro.cesion.ConsExtincionContratoEPO pag =  new com.netro.cesion.ConsExtincionContratoEPO();
	
	pag.setCatIntermediario(catIntermediario);
	pag.setCatContratacion(catContratacion);
	pag.setNoContrato(noContrato);
	pag.setCatPyme(catPyme);
	pag.setCatMoneda(catMoneda);
	pag.setClaveEpo(iNoCliente);
	pag.setFechaInicio(fecha_inicio);
	pag.setFechaFin(fecha_final);
	

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(pag);

	if (informacion.equals("Consultar") ) {
		String consulta ="";
		JSONObject 	resultado	= new JSONObject();
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				String mensaje="";
				queryHelper.executePKQuery(request);
				Registros	registros 	= queryHelper.getPageResultSet(request,start,limit);

				List adicionales;
				while (registros.next()) {
					String claveSol = registros.getString("clave_solicitud");
					String montosPorMoneda = cesionBean.getMontoMoneda(claveSol).toString().replaceAll(",", "");
					String noProveedor = registros.getString("clave_pyme");
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for (int y = 0; y < usuariosPorPerfil.size(); y++) {
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(y>0){
							 nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+"  ");
						nombreContacto.append(" ");
					}
					registros.setObject("MONTO_MONEDA", montosPorMoneda.toString());
					registros.setObject("REPRESENTA_LEGAL",nombreContacto.toString());
					//FODEA-024-2014 MOD()
					mensaje="<table align=\"center\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">"+
								"<tr>"+
								"<td class=\"formas\" style=\"text-align: justify;\">"+
								"<p>Con base en lo dispuesto en el Código de Comercio, por este conducto me permito notificar a esa Empresa, por conducto de esa Gerencia Jurídica, el Convenio de Extinción del Contrato de Cesión Electrónica de Derechos de Cobro respecto del Contrato referido en el apartado inmediato anterior que hemos formalizado con la Empresa Cedente.<p>"+
								"<p>Por lo anterior, me permito solicitar a usted una vez que surta efectos la extinción de derechos de cobro, gire sus instrucciones a fin de que se deje sin efectos la Cesión Electrónica de Derechos de Cobro de referencia y la Empresa realice el pago de las facturas derivadas de la ejecución y cumplimiento del Contrato referido a la Empresa Cedente en la cuenta señalada originalmente en dicho Contrato.</p>"+
								"</td></tr><table>";
					String nombrePyme =registros.getString("nombre_pyme");//FODEA-024-2014 MOD()
					String empresas =registros.getString("empresas");//FODEA-024-2014 MOD()
					nombrePyme = nombrePyme+((empresas!="")?";"+empresas:"");
					String []nombrePymeAux=nombrePyme.split(";");
						nombrePyme = nombrePyme.replaceAll(";","<br>");
						registros.setObject("NOMBRE_PYME", nombrePyme);	
					//
				}
				
				consulta	=	"{\"success\": true, \"total\": \"" + registros.getNumeroRegistros() + "\", \"registros\": " + registros.getJSONData()+"}";
				resultado = JSONObject.fromObject(consulta);

				//obtencion la Clasificacion EPO
				String clasificacionEpo = cesionBean.clasificacionEpo(iNoCliente);
				if(clasificacionEpo ==null) { clasificacionEpo=""; }
				resultado.put("clasificacionEpo", clasificacionEpo);
			
				//obtencion de campos Adicionales
				HashMap camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(iNoCliente, "9");
				int indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));
				adicionales = new ArrayList();
				for(int y = 0; y < indiceCamposAdicionales; y++){
					HashMap camposAdd = new HashMap();
					camposAdd.put("campoAdd",camposAdicionalesParametrizados.get("nombre_campo_"+y));
					adicionales.add(camposAdd);
				}
				resultado.put("camposAdicionalesParametrizados",adicionales);
				
				resultado.put("MENSAJE",mensaje);//FODEA-024-2014 MOD()
				infoRegresar = resultado.toString();
			}
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	
	}else if (informacion.equals("ArchivoCSV")	){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDF")	){
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}

}else if (	informacion.equals("ContratoCesionPyme")	){

	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject jsonObj = new JSONObject();
	
	String clave_if			=	(request.getParameter("clave_if") == null)?"":request.getParameter("clave_if");
	String clave_epo			=	(request.getParameter("clave_epo") == null)?"":request.getParameter("clave_epo");
	String tipo_archivo		=	(request.getParameter("tipo_archivo") == null)?"":request.getParameter("tipo_archivo");
	String clave_solicitud	=	(request.getParameter("clave_solicitud") == null)?"":request.getParameter("clave_solicitud");

	String pais					=	(String)session.getAttribute("strPais");
	String logo					=	(String)session.getAttribute("strLogo");
	String noExterno			=	(String)session.getAttribute("sesExterno");
	String nombre				=	(String) session.getAttribute("strNombre");
	String nombreUsua			=	(String) session.getAttribute("strNombreUsuario");
	String directorioPubli	=	(String) application.getAttribute("strDirectorioPublicacion");
	String nafinElectronico	=	((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString());

	List datos = new ArrayList();
	datos.add(pais);
	datos.add(nafinElectronico);
	datos.add(noExterno);
	datos.add(nombre);
	datos.add(nombreUsua);
	datos.add(logo);
	datos.add(strDirectorioTemp);
	datos.add(directorioPubli);
	datos.add(tipo_archivo);
	datos.add(clave_solicitud);
	datos.add(clave_epo);
	datos.add(clave_if);

	String nombreArchivo = cesionBean.DescargaContratoCesionPyme(datos);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Contrato")	){

	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject jsonObj = new JSONObject();
	
	String clave_solicitud	=	(request.getParameter("clave_solicitud") == null)?"":request.getParameter("clave_solicitud");

	String arvo = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);
	java.io.File file = new java.io.File(arvo);
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+file.getName());
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("PoderesPyme")	){
	
	JSONObject jsonObj = new JSONObject();
	try {
		String clavePyme		=	(request.getParameter("clavePyme") == null)?"":request.getParameter("clavePyme");
		String solicitud    = (request.getParameter("solicitud") == null)?"":request.getParameter("solicitud");
		String loginUsuario	=	(String)request.getSession().getAttribute("Clave_usuario");
		String grupoCesion  = "";
		String contrato     = "";
		String rfcPyme      = "";

		/***** Obtengo los datos necesarios para ejecutarl el procedimiento almacenado *****/
		CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
		HashMap datosPyme = cesionBean.getDatosPoderesPyme(solicitud, clavePyme);

		grupoCesion = "" + datosPyme.get("GRUPO_CESION");
		contrato    = "" + datosPyme.get("NO_CONTRATO");
		rfcPyme     = "" + datosPyme.get("RFC");
		
		System.out.println("clavePyme: " + clavePyme);
		System.out.println("grupoCesion: " + grupoCesion);
		System.out.println("contrato: " + contrato);
		System.out.println("rfc: " + rfcPyme);
		System.out.println("loginUsuario: " + loginUsuario);

		ImagenDocumentoContrato imagenDocumentoContrato = new ImagenDocumentoContrato();
		imagenDocumentoContrato.setClavePyme(clavePyme);
		imagenDocumentoContrato.setLoginUsuario(loginUsuario);
		imagenDocumentoContrato.setRfcPyme(rfcPyme);
		imagenDocumentoContrato.setGrupo_cesion(grupoCesion);
		imagenDocumentoContrato.setNo_contrato(contrato);
		imagenDocumentoContrato.setTipoExpediente("11");

		Map documentacionPyme = imagenDocumentoContrato.consultarImagenesEFile(strDirectorioTemp);

		if (documentacionPyme != null && documentacionPyme.size() > 0){
			jsonObj.put("urlArchivo", strDirecVirtualTemp+documentacionPyme.get("6"));
		}
		jsonObj.put("success", new Boolean(true));

	} catch (Exception e) {
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("msg", "No se encuentra ningun registro. " + e);
	}

	infoRegresar = jsonObj.toString();

}else if (informacion.equals("EnviarMail")	){

	CesionEJB cesionBean	= ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject jsonObj	=	new JSONObject();
	String correoDe		=	"cadenas@nafin.gob.mx";
	String correoPara		=	(request.getParameter("correoPara") == null)?"":request.getParameter("correoPara");
	String asunto			=	(request.getParameter("asunto") == null)?"":request.getParameter("asunto");
	String comentarios	=	(request.getParameter("comentarios") == null)?"":request.getParameter("comentarios");
	String claveSolicitud=	(request.getParameter("clave_solicitud") == null)?"":request.getParameter("clave_solicitud");
	String catVentanillaC=	(request.getParameter("catVentanillaC") == null)?"0":request.getParameter("catVentanillaC");
	String respuesta ="";

	try {
		// metodo que genera el formato de envio del Correo
		respuesta = cesionBean.EnviodeCorreoEXT( iNoCliente, claveSolicitud,  correoPara, comentarios, catVentanillaC, asunto  );

	} catch (Exception e) {
		jsonObj.put("msg", "Error al enviar el correo. " + e);
	}
	jsonObj.put("respuesta", respuesta);
	jsonObj.put("success", new Boolean(true));

	infoRegresar = jsonObj.toString();

}else if (informacion.equals("consultaEnvioCorreo")	){

	CesionEJB cesionBean	= ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject jsonObj	=	new JSONObject();
	String claveSolicitud = (request.getParameter("clave_solicitud") == null)?"":request.getParameter("clave_solicitud");

	String resp = cesionBean.consultaEnvioCorreo(claveSolicitud);
	jsonObj.put("respuesta", resp);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("AcuseExtContrato")	){
	%><%@ include file="../certificado.jspf" %><%
	CesionEJB cesionBean	= ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject jsonObj	=	new JSONObject();
	List parametrosFirma = new ArrayList();

	Calendar calendario = Calendar.getInstance();
	SimpleDateFormat formatoCert	= new SimpleDateFormat("ddMMyyyyHHmmss");
	SimpleDateFormat formatoFecha	= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat formatoHora	= new SimpleDateFormat("hh:mm aa");

	String clave_solicitud	=	(request.getParameter("clave_solicitud") == null)?"":request.getParameter("clave_solicitud");
	String tipoOperacion		=	(request.getParameter("tipoOperacion") == null)?"":request.getParameter("tipoOperacion");
	String catVentanillaC	=	(request.getParameter("catVentanillaC") == null)?"":request.getParameter("catVentanillaC");
	String causasRechazo		=	(request.getParameter("causasRechazo") == null)?"":request.getParameter("causasRechazo");
	String pkcs7				= request.getParameter("Pkcs7");
	String externContent		= request.getParameter("TextoFirmado");
	String folioCert			= formatoCert.format(calendario.getTime());
	String fechaCarga			= formatoFecha.format(calendario.getTime());
	String horaCarga			= formatoHora.format(calendario.getTime());

	parametrosFirma.add(	_serial						);
	parametrosFirma.add(	externContent				);
	parametrosFirma.add(	pkcs7							);
	parametrosFirma.add(	String.valueOf(false)	);
	parametrosFirma.add(	folioCert					);
	parametrosFirma.add(	clave_solicitud			);
	parametrosFirma.add(	strLogin						);
	parametrosFirma.add(	strNombreUsuario			);
	parametrosFirma.add(	tipoOperacion				);
	parametrosFirma.add(	catVentanillaC				);
	parametrosFirma.add(	causasRechazo				);

	String acuse = cesionBean.acuseExtinContratoEPO(parametrosFirma);

	if (acuse != null && !acuse.equals("")){
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("_acuse", acuse);
		jsonObj.put("fechaCarga", fechaCarga);
		jsonObj.put("horaCarga", horaCarga);
		String _usuario = strLogin + " - " + strNombreUsuario;
		jsonObj.put("usuario", _usuario);	
	}else{
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "La autentificación no se llevó a cabo.<br>Proceso CANCELADO");
	}

	infoRegresar = jsonObj.toString();

}else if (informacion.equals("AcuseExtNotifica")	){
	%><%@ include file="../certificado.jspf" %><%
	CesionEJB cesionBean	= ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject jsonObj	=	new JSONObject();
	List parametrosFirma = new ArrayList();

	Calendar calendario = Calendar.getInstance();
	SimpleDateFormat formatoCert	= new SimpleDateFormat("ddMMyyyyHHmmss");
	SimpleDateFormat formatoFecha	= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat formatoHora	= new SimpleDateFormat("hh:mm aa");

	String clave_solicitud	=	(request.getParameter("clave_solicitud") == null)?"":request.getParameter("clave_solicitud");
	String tipoOperacion		=	(request.getParameter("tipoOperacion") == null)?"":request.getParameter("tipoOperacion");
	String catVentanillaC	=	(request.getParameter("catVentanillaC") == null)?"":request.getParameter("catVentanillaC");
	String causasRechazo		=	(request.getParameter("causasRechazo") == null)?"":request.getParameter("causasRechazo");
	String pkcs7				= request.getParameter("Pkcs7");
	String externContent		= request.getParameter("TextoFirmado");
	String folioCert			= formatoCert.format(calendario.getTime());
	String fechaCarga			= formatoFecha.format(calendario.getTime());
	String horaCarga			= formatoHora.format(calendario.getTime());

	parametrosFirma.add(	_serial						);
	parametrosFirma.add(	externContent				);
	parametrosFirma.add(	pkcs7							);
	parametrosFirma.add(	String.valueOf(false)	);
	parametrosFirma.add(	folioCert					);
	parametrosFirma.add(	clave_solicitud			);
	parametrosFirma.add(	strLogin						);
	parametrosFirma.add(	strNombreUsuario			);
	parametrosFirma.add(	tipoOperacion				);
	parametrosFirma.add(	catVentanillaC				);
	parametrosFirma.add(	causasRechazo				);

	String acuse = cesionBean.acuseExtinContratoEpoNotificacion(parametrosFirma);

	if (acuse != null && !acuse.equals("")){
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("_acuse", acuse);
		jsonObj.put("fechaCarga", fechaCarga);
		jsonObj.put("horaCarga", horaCarga);
		String _usuario = strLogin + " - " + strNombreUsuario;
		jsonObj.put("usuario", _usuario);	
	}else{
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "La autentificación no se llevó a cabo.<br>Proceso CANCELADO");
	}

	infoRegresar = jsonObj.toString();

}else if (informacion.equals("getDataPrintAcuse")	){

	CesionEJB cesionBean		= ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject jsonObj		=	new JSONObject();

	String clave_solicitud	=	(request.getParameter("clave_solicitud") == null)?"":request.getParameter("clave_solicitud");
	String tipoOperacion		=	(request.getParameter("tipoOperacion") == null)?"":request.getParameter("tipoOperacion");
	String catVentanillaC	=	(request.getParameter("catVentanillaC") == null)?"":request.getParameter("catVentanillaC");
	String acuse				=	(request.getParameter("acuse") == null)?"":request.getParameter("acuse");
	String fechaCarga			=	(request.getParameter("fechaCarga") == null)?"":request.getParameter("fechaCarga");
	String horaCarga			=	(request.getParameter("horaCarga") == null)?"":request.getParameter("horaCarga");
	String _usuario			=	(request.getParameter("_usuario") == null)?"":request.getParameter("_usuario");
	
	try{
		ComunesPDF 	docPdf	= null;
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		List resultado = new ArrayList();
	
		//Campos Adicionales por EPO
		HashMap campos_adicionales = cesionBean.getCamposAdicionalesParametrizados(iNoCliente, "9");
		int indice = Integer.parseInt((String)campos_adicionales.get("indice"));
		if(indice > 0){request.setAttribute("campos_adicionales", campos_adicionales);}
		
		//titulo de la clasificacion EPO
		String clasificacionEpo = cesionBean.clasificacionEpo(iNoCliente);
		if(clasificacionEpo ==null){   clasificacionEpo=""; }
	
		ComunesPDF pdfDoc	= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String pais       = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
		String noCliente  = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
		String strnombre  = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
		String nombreUsr  = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
		String logo      	= (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
		
		String meses[]		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual	= fechaActual.substring(0,2);
		String mesActual	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual	= fechaActual.substring(6,10);
		String horaActual	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
		pdfDoc.encabezadoConImagenes(pdfDoc,pais,noCliente,strnombre,nombreUsr, "", logo,strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
		int renglones = 16;
		if(!clasificacionEpo.equals("")){ renglones+=1; }
		if(indice > 0){  renglones +=indice; }
		if(tipoOperacion.equals("A")){renglones+=1;}
		pdfDoc.setTable(renglones, 100);					
		pdfDoc.setCell("Intermediario Financiero (Cesionario)","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("PYME (Cedente)","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Representante Legal","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("No. de Contrato","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto / Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo de Contratación","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Inicio Contrato","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Final Contrato","celda01",ComunesPDF.CENTER);			
		pdfDoc.setCell("Plazo del Contrato","celda01",ComunesPDF.CENTER);		
		if(!clasificacionEpo.equals("")){
			pdfDoc.setCell(clasificacionEpo,"celda01",ComunesPDF.CENTER);
		}
		if(indice > 0){
			indice = Integer.parseInt((String)campos_adicionales.get("indice"));
			for(int i = 0; i < indice; i++){				
				pdfDoc.setCell(campos_adicionales.get("nombre_campo_"+i) + "", "celda01", ComunesPDF.CENTER);
			}		
		}
		pdfDoc.setCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Extinción del contrato","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Cuenta","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Cuenta CLABE","celda01",ComunesPDF.CENTER);			
		pdfDoc.setCell("Banco de Deposito","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);			
		if(tipoOperacion.equals("A")){
			pdfDoc.setCell("Ventanilla Destino","celda01",ComunesPDF.CENTER);
		}

		resultado = cesionBean.consExtincionPreacuse(clave_solicitud, iNoCliente, catVentanillaC.equals("")?"0":catVentanillaC	);
		//FODEA-024-2014 MOD(variable para en nuevo mensaje)
		String mensaje="";

		if(resultado.size()>0) {
			List reg = new ArrayList();
			for(int x=0; x<resultado.size(); x++) {	 
				List datos = (ArrayList)resultado.get(x);	
				HashMap	datos_b = new HashMap();
				
				//Se obtiene el representante Legal de la Pyme
				String noProveedor = (String)datos.get(26);
				StringBuffer nombreContacto = new StringBuffer();
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
				for (int y = 0; y < usuariosPorPerfil.size(); y++) {
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true;
					if(y>0){
						nombreContacto.append(" / ");
					}
					nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+"  ");	
					nombreContacto.append("\n");
				}
				//FODEA-024-2014 MOD()
				String nombrePyme = (String)datos.get(2)+(((String)datos.get(27)!="")?";"+(String)datos.get(27):"");
				//
				datos_b.put("CLAVE_SOLICITUD",(String)datos.get(0));
				datos_b.put("NOMBRE_IF",(String)datos.get(1));
				datos_b.put("NOMBRE_PYME",nombrePyme.replaceAll(";","<br>"));//FODEA-024-2014 MOD()
				datos_b.put("RFC",(String)datos.get(3));
				datos_b.put("REPRESENTA_LEGAL",nombreContacto.toString());
				datos_b.put("NUMERO_CONTRATO",(String)datos.get(5));
				datos_b.put("MONTO_MONEDA",(String)datos.get(6));
				datos_b.put("TIPO_CONTRATACION",(String)datos.get(7));
				datos_b.put("FECHA_INICIO_CONTRATO",(String)datos.get(8));
				datos_b.put("FECHA_FIN_CONTRATO",(String)datos.get(9));
				datos_b.put("PLAZO_CONTRATO",(String)datos.get(10));
				datos_b.put("OBJETO_CONTRATO",(String)datos.get(17));
				datos_b.put("FEXTIONCONTRATO",(String)datos.get(18));
				datos_b.put("NUMERO_CUENTA",(String)datos.get(19));
				datos_b.put("CLABE",(String)datos.get(20));
				datos_b.put("BANCO_DEPOSITO",(String)datos.get(21));
				datos_b.put("CAUSASRECHAZO",(String)datos.get(22));
				datos_b.put("ESTATUS_SOLICITUD",(String)datos.get(23));
				if(tipoOperacion.equals("A")){
					datos_b.put("VENTANILLA",(String)datos.get(24));
				}else if(tipoOperacion.equals("R")	||	tipoOperacion.equals("Rechazar")){
					datos_b.put("CAUSASRECHAZO_B",(String)datos.get(22));
				}
				datos_b.put("CORREO",(String)datos.get(25));
				datos_b.put("CLAVE_PYME",(String)datos.get(26));
				String cedente = (String)datos.get(2)+(((String)datos.get(27)!="")?";"+(String)datos.get(27):"");
				pdfDoc.setCell((String)datos.get(1),"formas",ComunesPDF.CENTER);
				//pdfDoc.setCell((String)datos.get(2),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cedente.replaceAll(";","\n"),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell((String)datos.get(3),"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell(nombreContacto.toString(),"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell((String)datos.get(5),"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell(((String)datos.get(6)).replaceAll("<br/>","\n"),"formas",ComunesPDF.LEFT);	
				pdfDoc.setCell((String)datos.get(7),"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell((String)datos.get(8),"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell((String)datos.get(9),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell((String)datos.get(10),"formas",ComunesPDF.CENTER);	
				if(!clasificacionEpo.equals("")){
					pdfDoc.setCell((String)datos.get(11),"formas",ComunesPDF.CENTER);
					datos_b.put("CLASIFICACION_EPO",(String)datos.get(11));
				}
				if(indice > 0){
					for(int j = 1; j <= indice; j++){
						pdfDoc.setCell(	(String)datos.get(+(11 + j)),"formas",ComunesPDF.CENTER);
						String campoAdd = "CAMPO_ADICIONAL_"+j;
						datos_b.put(campoAdd,(String)datos.get(+(11 + j)));
					}
				}
				pdfDoc.setCell((String)datos.get(17),"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell((String)datos.get(18),"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell((String)datos.get(19),"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell((String)datos.get(20),"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell((String)datos.get(21),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell((String)datos.get(23),"formas",ComunesPDF.CENTER);
				if(tipoOperacion.equals("A")){
					pdfDoc.setCell((String)datos.get(24),"formas",ComunesPDF.CENTER);	
				}
				reg.add(datos_b);
				//FODEA-024-2014 MOD (nuevo mensaje)
                mensaje =
                    "Con base en lo dispuesto en el Código de Comercio, por este conducto me permito notificar a esa Empresa, por conducto de esa Gerencia Jurídica, el Convenio de Extinción del Contrato de Cesión Electrónica de Derechos de Cobro respecto del Contrato referido en el apartado inmediato anterior que hemos formalizado con la Empresa Cedente.\n\n"+
                    "Por lo anterior, me permito solicitar a usted una vez que surta efectos la extinción de derechos de cobro, gire sus instrucciones a fin de que se deje sin efectos la Cesión Electrónica de Derechos de Cobro de referencia y la Empresa realice el pago de las facturas derivadas de la ejecución y cumplimiento del Contrato referido a la Empresa Cedente en la cuenta señalada originalmente en dicho Contrato.\n\n";
						
			}
			jsonObj.put("registros", reg);
		}
		pdfDoc.addTable();

		float anchoCelda1[] = {10f, 10f};
		pdfDoc.setTable(2, 50, anchoCelda1);			
		pdfDoc.setCell("Cifras de Control", "celda02", ComunesPDF.CENTER, 2);
		pdfDoc.setCell("Número de Acuse", "formas", ComunesPDF.LEFT);
		pdfDoc.setCell(acuse, "formas",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.LEFT);
		pdfDoc.setCell(fechaCarga, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.LEFT);
		pdfDoc.setCell(horaCarga, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("Usuario", "formas", ComunesPDF.LEFT);
		pdfDoc.setCell(_usuario, "formas",ComunesPDF.CENTER);
		pdfDoc.addTable();
		
		float anchoCelda4[] = {100f};
		pdfDoc.setTable(1, 70, anchoCelda4);
		
		pdfDoc.setCell(mensaje, "formas", ComunesPDF.LEFT, 1, 1, 1);//FODEA-024-2014 MOD (nuevo mensaje)
		/*pdfDoc.setCell("Con base a lo dispuesto en el Código de Comercio por este conducto me permito notificar a esa Entidad la Extinción del Contrato de Cesión de Derechos de Cobro que fue formalizado con la empresa CEDENTE respecto al contrato que aquí mismo se detalla. \n"	+
						  "Por lo anterior, me permito solicitar a usted, se registre la Extinción del Contrato de Cesión de Derechos de Cobro de referencia a efecto de que esa Entidad, realice el(los) pago(s) derivados del Contrato aquí señalado a favor del PROVEEDOR. \n"+
									 " Los datos de la Cuenta bancaria de la Pyme en la que se deberán depositar los pagos derivados de esta operación son los que aquí se detallan. \n" +
									"Sobre el particular, por este medio les notifico, la Extinción del Contrato de Cesión de Derechos de Cobro antes mencionado, para todos los efectos legales a que haya lugar”. \n ", "formas", ComunesPDF.LEFT, 1, 1, 1);	
		*/							
		pdfDoc.addTable();
		pdfDoc.endDocument();
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
	
	} catch(Exception e) {
		jsonObj.put("msg", "Error en el proceso. "+e+" <br>Proceso CANCELADO");
		jsonObj.put("success", new Boolean(false));
	} finally {
	}
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>