<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,				
		java.io.File,
		com.netro.cesion.*, 
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/34cesion/34secsession_ext.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
String clave_pyme = (request.getParameter("clave_pyme")!=null)?request.getParameter("clave_pyme"):"";
JSONObject jsonObj = new JSONObject();
String infoRegresar="";
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

 if(informacion.equals("PoderesContratoCesion") ){
	
	String 			bandera = cesionBean.getBanderaPoderes(clave_solicitud, "bi_poderes_cesion");
	if (bandera.equals("0")){
		throw new AppException("El Cesionario no ha subido el archivo de Poderes IF ");
	}
	try {
		
		String 			nombreArchivo = cesionBean.descargaPoderesContratoCesion(clave_solicitud, strDirectorioTemp);			
		//String nombreArchivo2 = nombreArchivo.substring(nombreArchivo.lastIndexOf("on/")+3, nombreArchivo.length());	
		jsonObj.put("success", new Boolean(true));
		//jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo2);02261709
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
					
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo de Contrato", e);
	}

 }else if(informacion.equals("PoderesExtincionContrato")){
 
	String 			bandera = cesionBean.getBanderaPoderes(clave_solicitud, "bi_poderes_extincion");
	if (bandera.equals("0")){
		throw new AppException("El Cesionario no ha subido el archivo de Poderes de Extinción IF ");
	}
 try {
		String 			nombreArchivo = cesionBean.descargaPoderesContratoExtincion(clave_solicitud, strDirectorioTemp);			
		//String nombreArchivo2 = nombreArchivo.substring(nombreArchivo.lastIndexOf("on/")+3, nombreArchivo.length());	
		jsonObj.put("success", new Boolean(true));
		//jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo2);
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
					
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo de Contrato", e);
	}
 
 }


%>
<%=infoRegresar%>

