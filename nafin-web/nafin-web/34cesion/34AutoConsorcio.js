Ext.namespace('NE.AutoConsorcio');



	var objGral={
		ic_solicitud: null,
		tipoVisor: null,
		titulo: null,
		csIF: null,
		id_grupo: null
	}
	
	NE.AutoConsorcio.VisorConsorcio = function (ic_solicitud, tipoVisor, titulo, csIF, id_grupo ){
		
		objGral.ic_solicitud = ic_solicitud;
		objGral.tipoVisor = tipoVisor;
		objGral.titulo = titulo;
		objGral.csIF = csIF;
		objGral.id_grupo = id_grupo
		
		if(tipoVisor=='Empresas'){
			consultaData.load({
				params: {
					informacion: 'Consultar_2',
					ic_solicitud:ic_solicitud,
					tipoVisor:tipoVisor,
					id_grupo: objGral.id_grupo
				}
			});
		} else if(tipoVisor == 'Firma_Convenio_Extincion'){
			consultaDataExtincion.load({
				params: {
					informacion: 'Consultar_Extincion',
					ic_solicitud: ic_solicitud,
					tipoVisor:    tipoVisor
				}
			});
		} else{
			consultaData.load({
				params: {
					informacion: 'Consultar',
					ic_solicitud:ic_solicitud,
					tipoVisor:tipoVisor
				}
			});
		}
	
		new Ext.Window({
			modal: true,
			width: 850,
			autoHeight: true,
			resizable: false,
			closable:true,
			id: 'VentanaCorreo',
			autoDestroy:false, 			
			closeAction: 'destroy',  			
			items: [
				NE.AutoConsorcio.gridConsulta,
				NE.AutoConsorcio.gridConsulta_1,
				NE.AutoConsorcio.gridConsultaExtincion
			]
		}).show().setTitle(titulo);
	}
		
	Ext.reg('VisorConsorcio', NE.AutoConsorcio.VisorConsorcio);
	
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			
			Ext.getCmp('btnGenerarPDF').enable();
			Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
			
			Ext.getCmp('btnGenerarPDF_1').enable();
			Ext.getCmp('btnGenerarPDF_1').setIconClass('icoPdf');
			
			Ext.getCmp('btnGenerarPDF_3').enable();
			Ext.getCmp('btnGenerarPDF_3').setIconClass('icoPdf');

			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			var forma    		= Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 		
			forma.submit();		
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
			
 var procesarConsulta_1Data = function(store,arrRegistros,opts){
	
		if (arrRegistros != null) {
			var jsonData = store.reader.jsonData;
			if(jsonData.perfil=='ADMIN NAFIN'){
			var gridConsulta_1 = Ext.getCmp('NE.AutoConsorcio.gridConsulta_1');
			var el = gridConsulta_1.getGridEl(); 
			
			gridConsulta_1.show(); 
			
			if(store.getTotalCount() > 0) {			
					
					var jsonData = store.reader.jsonData;	
				var cm = gridConsulta_1.getColumnModel();
				
				Ext.getCmp('btnGenerarPDF_1').enable();
				el.unmask();
			} else {	
			
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnGenerarPDF_1').disable();
			}
		}
	}
	}
	
		
	//-------------------------------- STORES -----------------------------------
	var consultaData_1 = new Ext.data.JsonStore({ 
		root : 'registros',
		url : NE.appWebContextRoot+'/34cesion/34AutoConsorcio.data.jsp',
		baseParams: {
			informacion: 'Consultar_1'
		},
		fields: [
			{	name: 'RAZON_SOCIAL'},
			{	name: 'PERFIL_AUTORIZADOR'},
			{	name: 'REP_LEGAL'},
			{	name: 'EMAIL'},
			{	name: 'FIRMA_CONTRATO'},				
			{	name: 'FECHA_FIRMA'}
			
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsulta_1Data,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsulta_1Data(null, null, null);						
					}
				}
			}
		});
		
		
	//NE.AutoConsorcio.gridConsulta_1 = new Ext.grid.GridPanel({
	NE.AutoConsorcio.gridConsulta_1 = {
		xtype: 'editorgrid',		
		store: consultaData_1,
		id: 'NE.AutoConsorcio.gridConsulta_1',		
		frame: true,
		loadMask: true,
		height: 400,
		width: 800,
		hidden: true,
		align: 'center',
		style: 'margin:0 auto;',
		title: 'Firmas Contrato de Cesi�n',
		columns: [
			{
				header:'Raz�n Social',
				tooltip:'Raz�n Social',
				dataIndex:'RAZON_SOCIAL',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 300,
				align: 'left',
				renderer:function(value,metadata,registro){  
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return  value;
				}
			},
			{
				header:'Perfil Autorizador',
				tooltip:'Perfil Autorizador',
				dataIndex:'PERFIL_AUTORIZADOR',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 200,
				align: 'left',
				renderer:function(value,metadata,registro){  
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return  value;
				}
			},				
			{
				header:'Nombre Representante Legal',
				tooltip:'Nombre Representante Legal',
				dataIndex:'REP_LEGAL',
				sortable: true,
				resizable: true,
				width: 200,
				align: 'left'
			},
			{
				header:'Correo Electr�nico',
				tooltip:'Correo electr�nico',
				dataIndex:'EMAIL',
				sortable: true,
				resizable: true,
				width: 170,
				align: 'left'
			},
			{
				header:'Firma Contrato',
				tooltip:'Firma Contrato',
				dataIndex:'FIRMA_CONTRATO',
				sortable: true,
				resizable: true	,			
				width: 120,
				align: 'center'				
			},
			{
				header:'Fecha de Firma',
				tooltip:'Fecha de Firma',
				dataIndex:'FECHA_FIRMA',
				sortable: true,
				resizable: true,				
				width: 150,
				align: 'center'
			}
		],
		bbar: {
			items: [
				'->',
				{
					xtype: 'button',
					text: 'Imprimir',
					iconCls:	'icoPdf',
					id: 'btnGenerarPDF_1',
					handler: function(boton, evento) {
						boton.disable();
											
						Ext.Ajax.request({
							url: NE.appWebContextRoot+'/34cesion/34AutoConsorcio.data.jsp',
							params:{
								informacion: 'ArchivoPDF_1',
								ic_solicitud: objGral.ic_solicitud,
								tipoVisor: objGral.tipoVisor,
								titulo: objGral.titulo
							},
							callback: procesarDescargaArchivos
						});
					}
				}				
			]
		}				
	}
	//);
/*--------------------------------- HANDLERS -------------------------------*/
	var procesarConsultaData = function(store,arrRegistros,opts){
	
		if (arrRegistros != null) {
		
			var gridConsulta = Ext.getCmp('NE.AutoConsorcio.gridConsulta');
			
			var el = gridConsulta.getGridEl(); 			
			gridConsulta.show();
		
			if(store.getTotalCount() > 0) {			
					
				var jsonData = store.reader.jsonData;	
				var cm = gridConsulta.getColumnModel();
				
			if(jsonData.tipoVisor=='Autorizacion_Consorcio'){					
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('AUTORIZACION_SOLIC'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_AUTORIZACION'), false);					
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FIRMA_CONTRATO'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_FIRMA'), true);	
			
			}else  if(jsonData.tipoVisor=='Contrato_cesion'){	
			
					gridConsulta.setTitle('Estatus de Firmas'); 		 		
			
				if(objGral.csIF!='N'){
					consultaData_1.load({
						params: {
							informacion: 'Consultar_1',
							ic_solicitud:objGral.ic_solicitud,
							tipoVisor:objGral.tipoVisor
						}
					});	
				}
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('AUTORIZACION_SOLIC'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_AUTORIZACION'), true);					
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FIRMA_CONTRATO'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_FIRMA'), false);	
			
			
			}else if(jsonData.tipoVisor=='Empresas'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('AUTORIZACION_SOLIC'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_AUTORIZACION'), true);					
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FIRMA_CONTRATO'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_FIRMA'), true);	
			
			}
				Ext.getCmp('btnGenerarPDF').enable();
				el.unmask();
			} else {	
			
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnGenerarPDF').disable();
			}
		}
	}
	

	//-------------------------------- STORES -----------------------------------
	var consultaData = new Ext.data.JsonStore({ 
		root : 'registros',
		url : NE.appWebContextRoot+'/34cesion/34AutoConsorcio.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{	name: 'NOMBRE_PYME'},
			{	name: 'NOMBRE_REPRESENTANTE_LEGAL'},
			{	name: 'CORREO_ELECTRONICO'},	
			
			{	name: 'AUTORIZACION_SOLIC'},
			{	name: 'FECHA_AUTORIZACION'},
			
			{	name: 'FIRMA_CONTRATO'},
			{	name: 'FECHA_FIRMA'}
			
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	

	
	//NE.AutoConsorcio.gridConsulta = new Ext.grid.GridPanel({
	 NE.AutoConsorcio.gridConsulta = {
		xtype: 'editorgrid',				
		store: consultaData,
		id: 'NE.AutoConsorcio.gridConsulta',		
		title: 'Estatus de Firmas',
		frame: true,
		loadMask: true,
		hidden: true,
		height: 400,
		width: 800,
		align: 'center',
		style: 'margin:0 auto;',
		title: '',	 	
		columns: [
			{
				header:'Nombre PYME',
				tooltip:'Nombre PYME',
				dataIndex:'NOMBRE_PYME',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 200,
				align: 'left',
				renderer:function(value,metadata,registro){  
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return  value;
				}
			},
			{
				header:'Nombre Representante Legal',
				tooltip:'Nombre Representante Legal',
				dataIndex:'NOMBRE_REPRESENTANTE_LEGAL',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 200,
				align: 'left',
				renderer:function(value,metadata,registro){  
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return  value;
				}
			},
			{
				header:'Correo Electr�nico',
				tooltip:'Correo Electr�nico',
				dataIndex:'CORREO_ELECTRONICO',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 150,
				align: 'left'
			},
			//Visor Autorizaci�n Consorcio
			{
				header:'Autorizaci�n Solicitud',
				tooltip:'Autorizaci�n Solicitud',
				dataIndex:'AUTORIZACION_SOLIC',
				sortable: true,
				resizable: true	,
				hidden: true,
				width: 100,
				align: 'left',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('FECHA_AUTORIZACION') !='' ) {
						return 'AUTORIZADO';
					}				
				}		
			},
			{
				header:'Fecha de Autorizaci�n',
				tooltip:'Fecha de Autorizaci�n',
				dataIndex:'FECHA_AUTORIZACION',
				sortable: true,
				resizable: true,
				hidden: true,
				width: 150,
				align: 'center'
			},
			//Visor Firma Consorcio
			{
				header:'Firma Contrato',
				tooltip:'Firma Contrato',
				dataIndex:'FIRMA_CONTRATO',
				sortable: true,
				resizable: true	,
				hidden: true,
				width: 150,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('FECHA_FIRMA') !='' ) {
						return 'FIRMADO';
					}				
				}	
			},
			{
				header:'Fecha de Firma',
				tooltip:'Fecha de Firma',
				dataIndex:'FECHA_FIRMA',
				sortable: true,
				resizable: true	,
				hidden: true,
				width: 150,
				align: 'center'
			}
		],
		bbar: {
			items: [
				'->',
				{
					xtype: 'button',
					text: 'Imprimir',
					iconCls:	'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
											
						Ext.Ajax.request({
							url: NE.appWebContextRoot+'/34cesion/34AutoConsorcio.data.jsp',
							params:{
								informacion: (objGral.tipoVisor=='Empresas')?'ArchivoPDF_2':'ArchivoPDF',
								ic_solicitud: objGral.ic_solicitud,
								tipoVisor: objGral.tipoVisor,
								titulo: objGral.titulo,
								id_grupo: objGral.id_grupo
							},
							callback: procesarDescargaArchivos
						});
					}
				}				
			]
		}				
	}
	//); 
	
	var procesarConsultaDataExtincion = function(store,arrRegistros,opts){
	
		if (arrRegistros != null){
			var gridConsulta = Ext.getCmp('NE.AutoConsorcio.gridConsultaExtincion');
			var el = gridConsulta.getGridEl(); 
			gridConsulta.show(); 

			if(store.getTotalCount() > 0){
				var jsonData = store.reader.jsonData;
				var cm = gridConsulta.getColumnModel();
				Ext.getCmp('btnGenerarPDF_3').enable();
				el.unmask();
			} else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
				Ext.getCmp('btnGenerarPDF_3').disable();
			}
		}

	}

	//-------------------------------- STORES -----------------------------------
	var consultaDataExtincion = new Ext.data.JsonStore({ 
		root: 'registros',
		url: NE.appWebContextRoot+'/34cesion/34AutoConsorcio.data.jsp',
		baseParams:{
			informacion: 'Consultar_Extincion'
		},
		fields: [
			{name: 'NOMBRE_PYME'               },
			{name: 'NOMBRE_REPRESENTANTE_LEGAL'},
			{name: 'CORREO_ELECTRONICO'        },
			{name: 'FIRMA_CONVENIO_EXTINCION'  },
			{name: 'FECHA_FIRMA'               }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: procesarConsultaDataExtincion,
			exception:{
				fn: function(proxy, type, action, optionsRequest, response, args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataExtincion(null, null, null);
				}
			}
		}
	});

	NE.AutoConsorcio.gridConsultaExtincion = {
		width:        '100%',
		height:       400,
		store:        consultaDataExtincion,
		xtype:        'editorgrid',
		id:           'NE.AutoConsorcio.gridConsultaExtincion',
		align:        'center',
		style:        'margin:0 auto;',
		frame:        false,
		loadMask:     true,
		hidden:       true,
		columns:[{
			width:     200,
			header:    'Nombre PYME',
			tooltip:   'Nombre PYME',
			dataIndex: 'NOMBRE_PYME',
			align:     'left',
			sortable:  true,
			resizable: true
		},{
			width:     200,
			header:    'Nombre Representante Legal',
			tooltip:   'Nombre Representante Legal',
			dataIndex: 'NOMBRE_REPRESENTANTE_LEGAL',
			align:     'left',
			sortable:  true,
			resizable: true
		},{
			width:     200,
			header:    'Correo Electr�nico',
			tooltip:   'Correo Electr�nico',
			dataIndex: 'CORREO_ELECTRONICO',
			align:     'left',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Firma Convenio Extinci�n',
			tooltip:   'Firma Convenio Extinci�n',
			dataIndex: 'FIRMA_CONVENIO_EXTINCION',
			align:     'center',
			sortable:  true,
			resizable: true,
			renderer:  function(value, metadata, registro, rowindex, colindex, store){
				if(registro.get('FECHA_FIRMA') != ''){
					return 'FIRMADO';
				} else{
					return '';
				}
			}
		},{
			width:     150,
			header:    'Fecha de Firma',
			tooltip:   'Fecha de Firma',
			dataIndex: 'FECHA_FIRMA',
			align:     'center',
			sortable:  true,
			resizable: true
		}],
		bbar: {
			items: ['->',{
				xtype:   'button',
				text:    'Imprimir',
				iconCls: 'icoPdf',
				id:      'btnGenerarPDF_3',
				handler: function(boton, evento) {
					boton.disable();
					var informacion = '';
					if(objGral.tipoVisor=='Firma_Convenio_Extincion'){
						informacion = 'ArchivoPDF_3';
					} else if(objGral.tipoVisor=='Empresas'){
						informacion = 'ArchivoPDF_2';
					} else{
						informacion = 'ArchivoPDF';
					}
					Ext.Ajax.request({
						url: NE.appWebContextRoot+'/34cesion/34AutoConsorcio.data.jsp',
						params:{
							informacion:  informacion,
							ic_solicitud: objGral.ic_solicitud,
							tipoVisor:    objGral.tipoVisor,
							titulo:       objGral.titulo
						},
						callback: procesarDescargaArchivos
					});
				}
			}]
		}
	}