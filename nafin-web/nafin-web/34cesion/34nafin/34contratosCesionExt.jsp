<!DOCTYPE html>
<%@ page import="java.util.*" 
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession.jspf" %>
<html> 
	<head>
		<title>Nafinet</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<%@ include file="/extjs.jspf" %>
		<%@ include file="/01principal/menu.jspf"%>
		<script type="text/javascript" src="34contratosCesionExt.js?<%=session.getId()%>"></script>
		<script type="text/javascript" src="../34AutoConsorcio.js"></script> 
	</head>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%if(esEsquemaExtJS) {%>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
		<%}else  {%>
			<div id="Contcentral">
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
			</div>
			<form id='formAux' name="formAux" target='_new'></form>	
		<%}%>
	</body>
</html>