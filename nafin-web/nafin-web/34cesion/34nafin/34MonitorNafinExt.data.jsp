<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.text.*,
	java.util.*,
	java.io.File,
	org.apache.commons.logging.Log,
	netropology.utilerias.usuarios.UtilUsr,
	netropology.utilerias.usuarios.Usuario,
	com.netro.pdf.*,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	com.netro.cesion.*,
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/34cesion/34secsession_ext.jspf" %>
	<%!
		private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	%>
<% 
	String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
	String tipo_archivo = request.getParameter("tipo") == null?"":(String)request.getParameter("tipo");
	String operacion = request.getParameter("operacion") == null?"": request.getParameter("operacion");
	CesionEJB BeanCesionEJB = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	String infoRegresar ="";
	int start				= 0;
	int limit 				= 0;
	HashMap datos = new HashMap();
	JSONArray registros1 = new JSONArray();
	JSONObject 	jsonObj	= new JSONObject();
	JSONObject 	jsonObj1	= new JSONObject();
	if(informacion.equals("ConsultaPrincipal")){
	
		List registros =BeanCesionEJB.monitorCesionDerechos(strTipoUsuario, iNoCliente);
		System.out.println("registros  "+registros);
		
		
		String  regestatus = "";
		String  estatus = "";
		String  numero = "";					
		int posicion = 0, longitud = 0;
		int solicitar = 0;
		int enproceso = 0;
		int aceptadas = 0; 
		int rechazadas= 0; 
		int vencida = 0;
		int reactivada = 0;  
		int aceptadopyme = 0; 
		int aceptadoIF = 0; 
		int rechazadoIF = 0;
		int notiAceptada = 0;
		int notiRechazada = 0;
		int totalSolicitudes = 0;
		int totalContrato = 0;
		int totalNotificaciones = 0;
		int solicitudes = 0;
		int pendNotifVent = 0;
		int preSolicitada = 0;
		int solicitada = 0;
		int acepNotificada= 0;
		int acepNotifVent = 0;
		int redirAplicado = 0;
		int redireccionamiento = 0;
		int extAceptadaIF = 0;
		int extAcepNotiVent = 0;
		int extincionSolicitada = 0;
		int extincionAceptadaIf = 0;
		int extincionAceptada	= 0;
		int extincionAceptadaNotificada = 0;
		int extincionRedirecAplic = 0;
		int extNotiAcep = 0;
		int totalExtinciones = 0;
		int canceladaNafin = 0;
		int rechazadaExpirada = 0;
		
		for (int i = 0; i < registros.size(); i++) {
			regestatus =((registros.get(i))==null)?"":(registros.get(i).toString());
			longitud = regestatus.length();
			posicion =regestatus.indexOf("e");
			estatus= regestatus.substring(0,posicion);
			numero= regestatus.substring(posicion+1, longitud);
			if (estatus.equals("1") ) {
				solicitar = (Integer.parseInt(numero));
			} else if (estatus.equals("2") ) {
				enproceso = (Integer.parseInt(numero));								
			} else if (estatus.equals("3")) {
				aceptadas = (Integer.parseInt(numero));
			}	else if (estatus.equals("5")) {
				rechazadas = (Integer.parseInt(numero));
			}	else if (estatus.equals("6")) {
				reactivada = (Integer.parseInt(numero));
			}	else if (estatus.equals("7")) {
				aceptadopyme = (Integer.parseInt(numero));
			}	else if (estatus.equals("8")) {
				vencida = (Integer.parseInt(numero));
			}	else if (estatus.equals("9")) {						
				aceptadoIF = (Integer.parseInt(numero));
			}	else if (estatus.equals("10")) {						
				rechazadoIF = (Integer.parseInt(numero));
			}	else if (estatus.equals("11")) {						
				notiAceptada = (Integer.parseInt(numero));
			}	else if (estatus.equals("12")) {						
				notiRechazada = (Integer.parseInt(numero));
			}	else if (estatus.equals("15")) {
				pendNotifVent = (Integer.parseInt(numero));
			}	else if (estatus.equals("16")) {						
				acepNotifVent = (Integer.parseInt(numero));
			}	else if (estatus.equals("18")) {	 //Pre-Solicitada					
				preSolicitada = (Integer.parseInt(numero));
			}	else if (estatus.equals("19")) {	 //Extincion Solicitada					
				solicitada = (Integer.parseInt(numero));  
			}	else if (estatus.equals("20")) {	 //Extincion Aceptada IF					
				extAceptadaIF = (Integer.parseInt(numero));
			}	else if (estatus.equals("21")) {	 //Extincion  Notificado Ventanilla
				extAcepNotiVent = (Integer.parseInt(numero));
			}	else if (estatus.equals("22")) {	 //Re-direrccionamiento
				redireccionamiento = (Integer.parseInt(numero));
			}	else if (estatus.equals("23")) {	 //Extinción Notificación Aceptada
				extNotiAcep = (Integer.parseInt(numero));
			}	else if (estatus.equals("24")) {	 //Cancelada por  Nafin  F023-2015
				canceladaNafin = (Integer.parseInt(numero));
			}	else if (estatus.equals("25")) {	 //Rechazada Expirada  F023-2015
				rechazadaExpirada = (Integer.parseInt(numero));		
			}else
				totalSolicitudes = solicitar+ enproceso + aceptadas + rechazadas + vencida;
				totalContrato =  aceptadas + aceptadopyme + aceptadoIF +rechazadoIF;
				totalNotificaciones =aceptadoIF + notiAceptada + notiRechazada + pendNotifVent + acepNotifVent;//aceptadoIF + notiAceptada + notiRechazada + pendNotifVent + acepNotifVent;
				solicitudes = aceptadas + rechazadas + vencida;
				totalExtinciones = preSolicitada + solicitada + extAceptadaIF + extAcepNotiVent + redireccionamiento + extNotiAcep;
		
			
		}  
		JSONObject jsonObj0 = new JSONObject();
			jsonObj0.put("Descripcion","<a href='/nafin/34cesion/34nafin/34solicitudConsentimientoExt.jsp'>SOLICITUDES DE CONSENTIMIENTO");
			jsonObj0.put("Total", new Integer(totalSolicitudes));
		JSONObject jsonObj23 = new JSONObject();
			jsonObj23.put("Descripcion", "POR SOLICITAR");
			jsonObj23.put("Total", new Integer(solicitar));
		JSONObject jsonObj2 = new JSONObject();
			jsonObj2.put("Descripcion","EN PROCESO");
			jsonObj2.put("Total", new Integer(enproceso));
		JSONObject jsonObj3 = new JSONObject();
			jsonObj3.put("Descripcion","TOTAL SOLICITUDES");
			jsonObj3.put("Total", new Integer(solicitudes));
		JSONObject jsonObj4 = new JSONObject();
			jsonObj4.put("Descripcion", "ACEPTADA");
			jsonObj4.put("Total", new Integer(aceptadas));
		JSONObject jsonObj5 = new JSONObject();
			jsonObj5.put("Descripcion", "RECHAZADAS");
			jsonObj5.put("Total", new Integer(rechazadas));
		JSONObject jsonObj6 = new JSONObject();
			jsonObj6.put("Descripcion", "VENCIDAS");
			jsonObj6.put("Total", new Integer(vencida));
		JSONObject jsonObj7 = new JSONObject();
			jsonObj7.put("Descripcion","<a href='/nafin/34cesion/34nafin/34contratosCesionExt.jsp' >CONTRATOS DE CESION"); 
			jsonObj7.put("Total", new Integer(totalContrato));
		JSONObject jsonObj8 = new JSONObject();
			jsonObj8.put("Descripcion","ACEPTADOS PYME");
			jsonObj8.put("Total", new Integer(aceptadopyme));
		JSONObject jsonObj9 = new JSONObject();
			jsonObj9.put("Descripcion","ACEPTADOS IF");
			jsonObj9.put("Total", new Integer(aceptadoIF));
		JSONObject jsonObj10 = new JSONObject();
			jsonObj10.put("Descripcion","RECHAZADOS IF");
			jsonObj10.put("Total", new Integer(rechazadoIF));
		JSONObject jsonObj11 = new JSONObject();
			jsonObj11.put("Descripcion","<a href='/nafin/34cesion/34nafin/34notificacionesNafin01Ext.jsp' >NOTIFICACIONES"); 
			jsonObj11.put("Total", new Integer(totalNotificaciones));
		JSONObject jsonObj12 = new JSONObject();
			jsonObj12.put("Descripcion","NO ACEPTADOS POR VENTANILLA");
			jsonObj12.put("Total", new Integer(pendNotifVent));
		JSONObject jsonObj13 = new JSONObject();
			jsonObj13.put("Descripcion","NOTIFICACIONES ACEPTADAS");
			jsonObj13.put("Total", new Integer(notiAceptada));
		JSONObject jsonObj14 = new JSONObject();
			jsonObj14.put("Descripcion","NOTIFICADO VENTANILLA");
			jsonObj14.put("Total", new Integer(acepNotifVent));
		JSONObject jsonObj15 = new JSONObject();
			jsonObj15.put("Descripcion","RECHAZADAS");
			jsonObj15.put("Total", new Integer(notiRechazada));
		JSONObject jsonObj16 = new JSONObject();		
			jsonObj16.put("Descripcion","EXTINCIÓN DE CONTRATO");
			jsonObj16.put("Total", new Integer(totalExtinciones));
		JSONObject jsonObj17 = new JSONObject();
			jsonObj17.put("Descripcion","PRE-SOLICITADA");
			jsonObj17.put("Total", new Integer(preSolicitada));
		JSONObject jsonObj18 = new JSONObject();
			jsonObj18.put("Descripcion","SOLICITADA");
			jsonObj18.put("Total", new Integer(solicitada));
		JSONObject jsonObj19 = new JSONObject();
			jsonObj19.put("Descripcion","ACEPTADA IF");
			jsonObj19.put("Total", new Integer(extAceptadaIF));
		JSONObject jsonObj20 = new JSONObject();
			jsonObj20.put("Descripcion","NOTIFICACIONES ACEPTADAS");
			jsonObj20.put("Total", new Integer(extNotiAcep));
		JSONObject jsonObj21 = new JSONObject();
			jsonObj21.put("Descripcion","NOTIFICADO VENTANILLA");
			jsonObj21.put("Total", new Integer(extAcepNotiVent));
		JSONObject jsonObj22 = new JSONObject();
			jsonObj22.put("Descripcion","RE-DIRECCIONAMIENTO");
			jsonObj22.put("Total", new Integer(redireccionamiento));
		
		JSONObject jsonObj24 = new JSONObject(); //F023-2015
		jsonObj24.put("Descripcion", "CANCELADA POR NAFIN");
		jsonObj24.put("Total", new Integer(canceladaNafin));
		
		JSONObject jsonObj25 = new JSONObject();   //F023-2015
		jsonObj25.put("Descripcion","RECHAZADA EXPIRADA");
		jsonObj25.put("Total", new Integer(rechazadaExpirada));
		
		
		JSONArray jsonArray = new JSONArray();		
		
		jsonArray.add(jsonObj0); //0 SOLICITUDES DE CONSENTIMIENTO
		jsonArray.add(jsonObj23); //1 POR SOLICITAR
		jsonArray.add(jsonObj2); //2  EN PROCESO
		jsonArray.add(jsonObj3); //3  TOTAL SOLICITUDES
		jsonArray.add(jsonObj4); //4 ACEPTADA
		jsonArray.add(jsonObj5); //5  RECHAZADAS
		jsonArray.add(jsonObj6); //6 VENCIDAS
		
		jsonArray.add(jsonObj24); //7 F23-2015   CANCELADA POR NAFIN
		
		jsonArray.add(jsonObj7); // 8 Contrato Cesion  
		jsonArray.add(jsonObj8); // 9 ACEPTADOS PYME
		jsonArray.add(jsonObj9); // 10   ACEPTADOS IF 
		jsonArray.add(jsonObj10); // 11 RECHAZADOS IF
		
		jsonArray.add(jsonObj25);//  12 F23-2015     Rechazada Expirada
			
		jsonArray.add(jsonObj11); // 13 Notificaciones 
		
		jsonArray.add(jsonObj12); //14 NO ACEPTADOS POR VENTANILLA
		jsonArray.add(jsonObj13); //15 NOTIFICACIONES ACEPTADAS
		jsonArray.add(jsonObj14); //16 NOTIFICADO VENTANILLA
		jsonArray.add(jsonObj15); //17  RECHAZADAS
		
		jsonArray.add(jsonObj16);  //18  Extinsion de contrato 
		jsonArray.add(jsonObj17); //19  PRE-SOLICITADA
		jsonArray.add(jsonObj18); //20 SOLICITADA
		jsonArray.add(jsonObj19); //21 ACEPTADA IF
		jsonArray.add(jsonObj20); //22  NOTIFICACIONES ACEPTADAS
		jsonArray.add(jsonObj21); //23  NOTIFICADO VENTANILLA
		jsonArray.add(jsonObj22);	//24 RE-DIRECCIONAMIENTO
		
		infoRegresar =	"{\"success\": true, \"total\":" + jsonArray.size() + ", \"registros\": " + jsonArray.toString()+"}";
		
	}
	else if(informacion.equals("valoresIniciales")){
		
		String clasificacionEpo = BeanCesionEJB.clasificacionEpo(iNoCliente);
		JSONObject jsonObj20 = new JSONObject();
		jsonObj20.put("success",new Boolean(true));
		jsonObj20.put("strDirectorioPublicacion",strDirectorioPublicacion);
		jsonObj20.put("clasificacionEpo",clasificacionEpo);
		infoRegresar = jsonObj20.toString();
	}else if(informacion.equals("Consulta")){
	
		String tipo = (request.getParameter("tipo")==null)?"":request.getParameter("tipo");
		ConsulSolicitudConsentimiento paginador = new ConsulSolicitudConsentimiento();
		paginador.setClaveEstatusSolicitud(tipo);
		JSONObject 	resultado	= new JSONObject();
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		if(informacion.equals("Consulta")){
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			}catch(Exception e){
				throw new AppException("Error en los parámetros recibidos", e);
			}
			try{
				if(operacion.equals("Generar")){   
					queryHelper.executePKQuery(request);
				}
				Registros registros = queryHelper.getPageResultSet(request,start,limit);
				while(registros.next()){
					StringBuffer montosMonedaSol = BeanCesionEJB.getMontoMoneda(registros.getString("CLAVE_SOLICITUD"));
					String  cedente = registros.getString("NOMBRE_CEDENTE");
					registros.setObject("MONTO_MONEDA",montosMonedaSol.toString());
					registros.setObject("NOMBRE_CEDENTE",cedente.replaceAll(";", "<br>"));
						
				}
			String consulta ="{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
			
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("tipo",tipo);		
			infoRegresar = jsonObj.toString();  
		
			}catch(Exception e){
				throw new AppException("Error en la paginación",e);
			}
		}
	}else if(informacion.equals("DESCARGA_DOCUMENTO")){
	
		CreaArchivo creaArchivo = new CreaArchivo();
		File file = null;
		String nombreArchivo = "";
		String urlArchivo  = "";
		
		String tipoArchivo = request.getParameter("tipoArchivo") == null?"":(String)request.getParameter("tipoArchivo");
		String numeroSolicitud = request.getParameter("clave_solicitud") == null?"":(String)request.getParameter("clave_solicitud");
		
		try{
		
			if (tipoArchivo.equals("CONTRATO_PDF")) {
				nombreArchivo = BeanCesionEJB.descargaContratoCesion(numeroSolicitud, strDirectorioTemp);
			}

			if (tipoArchivo.equals("CONTRATO_CESION_IF")) {
				
				String clave_solicitud = numeroSolicitud;
				CreaArchivo archivo = new CreaArchivo();
				String clave_if ="";
				String clave_epo = "";
				HashMap contratoCesion = BeanCesionEJB.consultaContratoCesionPyme(clave_solicitud);
				int indice_camp_adic = 0;
				String clasificacionEpo ="", claveEstatus ="";
				HashMap campos_adic =  new HashMap();
				StringBuffer clausulado_parametrizado =  new  StringBuffer();
				
				nombreArchivo = archivo.nombreArchivo()+".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
				pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
				
				
				if(contratoCesion.size()>1){
				clave_epo = (String)contratoCesion.get("clave_epo");
				clave_if = (String)contratoCesion.get("clave_if");
				claveEstatus = (String)contratoCesion.get("clave_estatus");
				
				campos_adic = BeanCesionEJB.getCamposAdicionalesParametrizados(clave_epo, "9");
				clausulado_parametrizado = BeanCesionEJB.consultaClausuladoParametrizado(clave_if);
				clasificacionEpo = BeanCesionEJB.clasificacionEpo(clave_epo);
				
				 indice_camp_adic = Integer.parseInt((String)campos_adic.get("indice"));
				
			
				
				
				
				String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
				String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
				String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
				String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
				String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
				
				pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, (String)session.getServletContext().getAttribute("strDirectorioPublicacion"), "");
				float anchoCelda4[] = {100f};
				
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
					pdfDoc.setTable(1, 70, anchoCelda4);
					pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
					pdfDoc.setCell("NOTIFICACION " , "formas", ComunesPDF.CENTER, 1, 1, 0);		
					pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "formas", ComunesPDF.LEFT, 1, 1, 0);	 //nombre de la EPo
					
					pdfDoc.setCell("Asunto: "+"Notificación de Cesión de Derechos de Cobro" , "formas", ComunesPDF.LEFT, 1, 1, 0);		
					pdfDoc.setCell("Con base en lo dispuesto  en el Código de Comercio, por este conducto me permito notificar a la Entidad, por conducto de esa Gerencia Jurídica de Convenios y Contratos, el Contrato de Cesión Electrónica de Derechos de Cobro, que mediante el uso de firmas electrónicas hemos formalizado con la Empresa Cedente como INTERMEDIARIO FINANCIERO y con el carácter de Cesionario, respecto del Contrato referido en el apartado siguiente.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);    
					pdfDoc.setCell("Por lo anterior, me permito solicitar a usted gire sus instrucciones a fin de que una vez que surta efectos, se registre la Cesión Electrónica de Derechos de Cobro de referencia a efecto de que "+(String)contratoCesion.get("nombre_epo")+" (Entidad), realice el pago de las facturas derivadas del cumplimiento y ejecución del Contrato referido en el apartado siguiente a la cuenta del suscrito INTERMEDIARIO FINANCIERO.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.setCell("Los datos de la cuenta en la que se deben depositar los pagos derivados de esta operación se detallan en el apartado siguiente como Nombre del Intermediario Financiero, Banco de Depósito,  Número de Cuenta “PEMEX” para Depósito y Número de Cuenta CLABE. ", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.addTable();
				}
			
				//====================================================================>> REPRESENTANTE LEGAL IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				
				//====================================================================>> TESTIGO 1 IF
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaTest1"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> TESTIGO 2 IF
				if (contratoCesion.get("claveUsrTest2") != null && !contratoCesion.get("claveUsrTest2").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}	
				
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("CONTRATO DE CESIÓN DE DERECHOS", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				float anchoCelda2[] = {50f, 50f};
				pdfDoc.setTable(2, 80, anchoCelda2);

				pdfDoc.setCell("INFORMACIÓN DE LA PYME CEDENTE", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("RFC:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("rfcPyme"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("REPRESENTANTE PYME:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("REPRESENTANTE PYME:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NUM. PROVEEDOR:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroProveedor"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACIÓN DEL CONTRATO", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NÚMERO DE CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numero_contrato"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("MONTO / MONEDA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(contratoCesion.get("montosPorMoneda").toString().replaceAll(",", "").replaceAll("<br/>", ""), "formas", ComunesPDF.LEFT);
				/*pdfDoc.setCell("MONTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(Comunes.formatoDecimal(contratoCesion.get("monto_contrato"), 2), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("MONEDA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("moneda"), "formas", ComunesPDF.LEFT);*/
				pdfDoc.setCell("FECHA DE VIGENCIA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N/A".equals(contratoCesion.get("fecha_inicio_contrato"))?"N/A":(contratoCesion.get("fecha_inicio_contrato") + " a " + contratoCesion.get("fecha_fin_contrato")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PLAZO DEL CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("plazoContrato"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("TIPO DE CONTRATACIÓN:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("tipo_contratacion").toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("OBJETO DEL CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("objeto_contrato").toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				for (int i = 0; i < indice_camp_adic; i++) {
					pdfDoc.setCell(campos_adic.get("nombre_campo_"+i).toString().toUpperCase() + ":", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(contratoCesion.get("campo_adicional_"+(i+1)).toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				}
				
				pdfDoc.setCell("INFORMACIÓN DE LA EMPRESA DE PRIMER ORDEN", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACIÓN DEL CESIONARIO (IF)", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_if"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("BANCO DE DEPÓSITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NÚMERO DE CUENTA PARA DEPÓSITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NÚMERO DE CUENTA CLABE PARA DEPÓSITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroCuentaClabe"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACIÓN DE LA CESIÓN DE DERECHOS", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("FECHA DE SOLICITUD DE CONSENTIMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("fechaSolicitudConsentimiento"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE CONSENTIMIENTO DE LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("fechaAceptacionEpo"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE OTORGÓ EL CONSENTIMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrEpoAutCesion")==null?"":contratoCesion.get("nombreUsrEpoAutCesion")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE FORMALIZACIÓN DEL CONTRATO DE CESIÓN:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaFormalContrato")==null?"":contratoCesion.get("fechaFormalContrato")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE DE CESIONARIO (IF):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrIf")==null?"":(!claveEstatus.equals("10")?contratoCesion.get("nombreUsrIf"):"")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrTest1")==null?"":contratoCesion.get("nombreUsrTest1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrTest2")==null?"":contratoCesion.get("nombreUsrTest2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE NOTIFICACIÓN ACEPTADA POR LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifEpo")==null?"":contratoCesion.get("fechaNotifEpo")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE ACEPTÓ LA NOTIFICACIÓN:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrNotifEpo")==null?"":contratoCesion.get("nombreUsrNotifEpo")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE INFORMACIÓN A VENTANILLA DE PAGOS:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE RECEPCIÓN EN VENTANILLA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE LA APLICACIÓN DEL REDIRECCIONAMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaRedirecccion")==null?"":contratoCesion.get("fechaRedirecccion")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrRedirec")==null?"":contratoCesion.get("nombreUsrRedirec")), "formas", ComunesPDF.LEFT);

				pdfDoc.addTable();

				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("SE SUJETA A LAS SIGUIENTES CLAUSULAS", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				float anchoCelda3[] = {100f};
				pdfDoc.setTable(1, 70, anchoCelda3);
				
				pdfDoc.setCell(clausulado_parametrizado.toString(), "formas", ComunesPDF.LEFT, 1);

				pdfDoc.addTable();
				
				
				//====================================================================>> REPRESENTANTE LEGAL 1 PYME
				if (contratoCesion.get("claveUsrAutRep1") != null && !contratoCesion.get("claveUsrAutRep1").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep1") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
	
					pdfDoc.setCell((String)contratoCesion.get("firmaRep1"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> REPRESENTANTE LEGAL 2 PYME
				if (contratoCesion.get("claveUsrAutRep2") != null && !contratoCesion.get("claveUsrAutRep2").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep2") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaRep2"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> REPRESENTANTE LEGAL IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				
				
				}
				pdfDoc.endDocument();
			}
			if (tipoArchivo.equals("CONTRATO_PDF")) {
				urlArchivo = strDirecVirtualTemp+nombreArchivo;
				log.debug("..:: strDirectorioTemp + nombreArchivo: "+nombreArchivo);
			}else{
				urlArchivo = strDirecVirtualTemp+nombreArchivo;
				log.debug("..:: strDirectorioTemp + nombreArchivo: "+strDirectorioTemp + nombreArchivo);
			}
			jsonObj	= new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", urlArchivo);
			infoRegresar = jsonObj.toString();
			System.out.println("infoRegresar -- >> "+infoRegresar);
		}catch(Exception e){
				throw new AppException("Error al crear archivo",e);
			}
		
	}
%>
<%=infoRegresar%>

