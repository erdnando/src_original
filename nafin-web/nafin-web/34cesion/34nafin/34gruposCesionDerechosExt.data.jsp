<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.text.*,
	java.util.*,
	java.io.File,
	org.apache.commons.logging.Log,
	com.netro.pdf.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.CatalogoEPO_CesionDerechos,
	com.netro.model.catalogos.CatalogoPymePorGrupoCesion,
	com.netro.cesion.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion   = request.getParameter("informacion")   == null?"":(String)request.getParameter("informacion");
String operacion     = request.getParameter("operacion")     == null?"":(String)request.getParameter("operacion");
String icGrupoCesion = request.getParameter("icGrupoCesion") == null?"":(String)request.getParameter("icGrupoCesion");
String icPyme        = request.getParameter("icPyme")        == null?"":(String)request.getParameter("icPyme");
String icEpo         = request.getParameter("icEpo")         == null?"":(String)request.getParameter("icEpo");
String nombreGrupo   = request.getParameter("nombreGrupo")   == null?"":(String)request.getParameter("nombreGrupo");
String noContrato    = request.getParameter("noContrato")    == null?"":(String)request.getParameter("noContrato");
String datosGrid     = request.getParameter("datosGrid")     == null?"":(String)request.getParameter("datosGrid");

// Variables del Form Búsqueda Empresa
String razonSocial      = request.getParameter("razonSocial")      == null?"":(String)request.getParameter("razonSocial");
String rfc              = request.getParameter("rfc")              == null?"":(String)request.getParameter("rfc");
String nafinElectronico = request.getParameter("nafinElectronico") == null?"":(String)request.getParameter("nafinElectronico");

String consulta = "";
String mensaje = "";
String infoRegresar = "";
JSONObject resultado = new JSONObject();
boolean success = true;

int start = 0;
int limit = 0;

log.debug("<<<informacion: " + informacion + ">>>>"); 

if(informacion.equals("Catalogo_EPO")){

	/*
	Estoy asumiendo que este catálogo solo va a mostrar las EPOS con las siguientes características:
	CS_CESION_DERECHOS = 'S' AND CS_HABILITADO = 'S'
	*/
	CatalogoEPO_CesionDerechos catalogo = new CatalogoEPO_CesionDerechos();
	catalogo.setCampoClave("IC_EPO");
	catalogo.setCampoDescripcion("CG_RAZON_SOCIAL");
	catalogo.setOrden("CG_RAZON_SOCIAL");
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("Catalogo_Empresa")){

	razonSocial = razonSocial.replaceAll("\'","");
	rfc = rfc.replaceAll("\'","");
	nafinElectronico = nafinElectronico.replaceAll("\'","");

	CatalogoPymePorGrupoCesion catalogo = new CatalogoPymePorGrupoCesion();
	catalogo.setCampoClave("C.IC_PYME");
	catalogo.setCampoDescripcion("P.CG_RAZON_SOCIAL");
	catalogo.setIcEpo(icEpo);
	catalogo.setRazonSocial(razonSocial);
	catalogo.setRfc(rfc);
	catalogo.setNafinElectronico(nafinElectronico);
	catalogo.setOrden(" P.CG_RAZON_SOCIAL");
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("Valida_Grupo")){

	boolean estatusGpo = true;
	GruposCesionDerechos grupos = new GruposCesionDerechos();
	estatusGpo = grupos.validaGrupo(Integer.parseInt(icGrupoCesion));
	if(estatusGpo == false){
		mensaje = "No se puede editar el grupo debido a que ya existe una solicitud de consentimiento.";
	}
	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

} else if(informacion.equals("Valida_Contrato")){

	boolean hayDatos = false;
	JSONArray registros = new JSONArray();
	try{

		GruposCesionDerechos grupos = new GruposCesionDerechos();
		grupos.setIcEpo(icEpo);
		grupos.setNoContrato(noContrato);
		grupos.setIcGrupoCesion(icGrupoCesion);
		hayDatos = grupos.validaContratosRepetidos();
		if(hayDatos == true){
			mensaje = "El No. de Contrato fue asignado a otro grupo y &eacute;ste no debe repetirse";
		} else{
			mensaje = "";
		}
	} catch(Exception e) {
		success = false;
		throw new AppException("Error al obtener la validación del contrato", e);
	}
	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consulta_Datos_Pyme")){

	boolean hayDatos = false;
	JSONArray registros = new JSONArray();
	try{

		GruposCesionDerechosEmpresas empresa = new GruposCesionDerechosEmpresas();
		empresa.setIcPyme(icPyme);
		hayDatos = empresa.getDatosPyme();
		if(hayDatos == true){
			resultado.put("IC_PYME",             empresa.getIcPyme()            );
			resultado.put("NOMBRE_PYME",         empresa.getNombrePyme()        );
			resultado.put("RFC",                 empresa.getRfc()               );
			resultado.put("REPRESENTANTE_LEGAL", empresa.getRepresentanteLegal());
			resultado.put("CONTACTO",            empresa.getContacto()          );
			resultado.put("NAFIN_ELECTRONICO",   empresa.getNafinElectronico()  );
		} else{
			mensaje = "La consulta no arrojó datos.";
		}
	} catch(Exception e) {
		success = false;
		throw new AppException("Error al obtener los dtos del representante legal y el contacto.", e);
	}
	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consulta_Grid_Grupos")){

	GruposCesionDerechos paginador = new GruposCesionDerechos();
	/***** Aqui deben ir los setters para filtrar la consulta, pero aun no se que variables son *****/
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos (star y limit)", e);
	}

	try {
		if (operacion.equals("generar")){
			queryHelper.executePKQuery(request);
		}
		consulta = queryHelper.getJSONPageResultSet(request,start,limit);
		log.debug("consulta: " + consulta);
		resultado = JSONObject.fromObject(consulta);
	} catch(Exception e) {
		throw new AppException(" Error en la paginacion", e);
	}
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consulta_Grid_Empresas")){

	GruposCesionDerechosEmpresas paginador = new GruposCesionDerechosEmpresas();
	paginador.setIcGrupoCesion(icGrupoCesion);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros listaRegistros = new Registros();

	try{
		listaRegistros = queryHelper.doSearch();
		consulta = listaRegistros.getJSONData();
		resultado.put("total", "" + listaRegistros.getNumeroRegistros());
		resultado.put("registros", consulta);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error en la paginacion", e);
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Eliminar_Grupo")){

	try{
		GruposCesionDerechos grupos = new GruposCesionDerechos();
		grupos.setIcGrupoCesion(icGrupoCesion);
		mensaje = grupos.eliminaGrupo();
	} catch(Throwable e) {
		success = false;
		mensaje = "Error al eliminar el registro" + e;
		throw new AppException("Error al eliminar el registro", e);
	}
	log.debug(mensaje);
	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

} else if(informacion.equals("Actualizar_Grupo")){

	try{

		JSONArray arrayEmpresas = JSONArray.fromObject(datosGrid);
		List empresas = new ArrayList();
		HashMap mapaEmpresas = new HashMap();
		for(int i=0; i<arrayEmpresas.size(); i++){
			JSONObject auxiliar = arrayEmpresas.getJSONObject(i);
			mapaEmpresas = new HashMap();
			mapaEmpresas.put("IC_PYME", auxiliar.getString("IC_PYME"));
			mapaEmpresas.put("NOMBRE_PYME", auxiliar.getString("NOMBRE_PYME"));
			mapaEmpresas.put("RFC", auxiliar.getString("RFC"));
			if((""+ auxiliar.getString("PYME_PRINCIPAL")).equals("true")){
				mapaEmpresas.put("PYME_PRINCIPAL", "S");
			} else{
				mapaEmpresas.put("PYME_PRINCIPAL", "N");
			}
			empresas.add(mapaEmpresas);
		}

		GruposCesionDerechos grupos = new GruposCesionDerechos();
		grupos.setIcEpo(icEpo);
		grupos.setNombreGrupo(nombreGrupo);
		grupos.setNoContrato(noContrato);
		grupos.setUsuario(iNoUsuario);
		grupos.setEmpresas(empresas);
		if(!icGrupoCesion.equals("")){
			grupos.setIcGrupoCesion(icGrupoCesion);
			grupos.setAccion("UPDATE");
		} else{
			grupos.setIcGrupoCesion("");
			grupos.setAccion("INSERT");
		}
		mensaje = grupos.actualizaGrupo();

	} catch(Throwable e) {
		success = false;
		mensaje = "Error al actualizar el grupo" + e;
		throw new AppException(e);
	}
	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();
}
%>
<%=infoRegresar%>