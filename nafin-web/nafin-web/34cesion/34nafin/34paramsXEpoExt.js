Ext.onReady(function() {
//--------------------------------- HANDLERS -----------------------------------0
	var fnMuestraAyuda = function(titulo, numOpc){
		var winVerAyuda = Ext.ComponentQuery.query('#winVerAyuda')[0];
		var textoAyuda = '';
		if(numOpc===1){
			textoAyuda = 'Establece si la EPO cuenta con el Flujo de Extinci�n Corto o Convencional, el flujo convencional lo comienza la Pyme, el flujo corto lo comienza el IF';
		}else if(numOpc===2){
			textoAyuda = 'Establece si la EPO est� habilitada para solicitar Pr�rroga.';
		}else if(numOpc===3){
			textoAyuda = 'D�as h�biles que tiene el IF para Firmar el Contrato, esto incluye los siguientes perfiles: IF_OPERACION, ADMIN IF y ADMIN TEST';
		}
		if(winVerAyuda){
			winVerAyuda.show();
			winVerAyuda.setTitle(titulo);
			winVerAyuda.update(textoAyuda);
		}else{
			Ext.create('Ext.window.Window', {
				title: titulo,
				itemId: 'winVerAyuda',
				width: 450,
				height: 100,
				layout: 'fit',
				html: textoAyuda,
				resize: false,
				modal: true,
				closeAction: 'hide'
			}).show();
		}
	}
	
	var bthFnGuardar = function(){
		Ext.Ajax.request({
			url : '34paramsXEpoExt.data.jsp',
			params : {
				formData: Ext.encode(formaParams.getValues()),
				informacion: 'GUARDA_PARAMETROS'
			},
			callback: procesarSuccessGuardaParams
		});
	};
	
	var procesarSuccessGuardaParams = function(options, success, response) {
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			Ext.Msg.alert('Mensaje', 'Los Par�metros se actualizaron satisfactoriamente');
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	};
	
	var procesarSuccessObtieneParams = function(options, success, response) {
		formaParams.getForm().reset();
		if (success === true && Ext.JSON.decode(response.responseText).success === true) {
			var resp = Ext.JSON.decode(response.responseText);
			if(resp.formData.CDER_DIAS_FIRMAR_CONTRATO ==''){
				resp.formData.CDER_DIAS_FIRMAR_CONTRATO = '30';
			}
			formaParams.getForm().setValues(resp.formData);
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	};
//------------------------------------------------------------------------------

	Ext.define('Catalogo', {
		extend: 'Ext.data.Model',
		fields: [
				{ name: 'clave', type: 'string' },
				{ name: 'descripcion', type: 'string' }
		]
	});
	
	var storeCatEposCesion = Ext.create('Ext.data.Store', {
		model: 'Catalogo',
		autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '34paramsXEpoExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CONS_CAT_EPO_CESION'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		listeners: {
			load:function(myStore, records, successful, eOpts ){
				if(successful && myStore.getTotalCount() > 0){
				
				}
			}
		}
	});
	
	var formaParams = Ext.create('Ext.form.Panel',{
		width: 650,
		labelWidth: 100,
		title:           'Par�metros por EPO',
		layout:          'form',
		frame:           true,
		defaults: {
			xtype:       'panel',
			frame:       false,
			bodyPadding: 5,
			layout: {
				type: 'table',
				columns: 3
			}
		},
		items: [{
			items:[{
				width:          580,
				colspan:        3,
				xtype:          'combobox',
				itemId:         'cmbEpoCesion',
				name:           'cmbEpoCesion',
				fieldLabel:     'EPO',
				valueField:     'clave',
				displayField:   'descripcion',
				emptyText:      'Debe seleccionar alguna EPO',
				queryMode:      'local',
				typeAhead:      true,
				allowBlank:     false,
				forceSelection: true,
				store:          storeCatEposCesion,
				listeners: {
					select: function(combo, record, e){
						Ext.Ajax.request({
							url: '34paramsXEpoExt.data.jsp',
							params: Ext.apply(formaParams.getForm().getValues(),{
								informacion: 'OBTIENE_PARAMETROS'
							}),
							callback: procesarSuccessObtieneParams
						});
					}
				}
			}]
		},{
			items:[
				{xtype: 'displayfield', value:'<CENTER><b>Nombre del Par�metro</b></CENTER>', width:300, align:'center'},
				{xtype: 'displayfield', value:'<CENTER><b>Tipo de Dato</b></CENTER>',         width:170                },
				{xtype: 'displayfield', value:'<CENTER><b>Valores</b></CENTER>',              width:180                }
			]
		},{
			items:[{
				width:          300,
				xtype:          'fieldcontainer',
				layout:         'hbox',
				items:[{
					xtype:      'button',
					iconCls:    'icoAyuda',
					text:       '',
					handler:    function(){
						fnMuestraAyuda('Extinci�n de Contrato', 1);
					}
				},{
					width:      250,
					xtype:      'displayfield',
					cls:        'text-wrapper',
					value:      'Extinci�n de Contrato'
				}]
			},{
				width:          170,
				xtype:          'displayfield',
				value:          '<CENTER>C</CENTER>'
			},{
				width:          180,
				columns:        1,
				xtype:          'radiogroup',
				itemId:         'extincionContratoRg',
				vertical:       true,
				items: [
					{ boxLabel: 'Convencional', name: 'CDER_TIPO_EXT_CONTRATO', inputValue: 'L',  checked: true },
					{ boxLabel: 'Corta',        name: 'CDER_TIPO_EXT_CONTRATO', inputValue: 'C'                 }
				]
			}]
		},{
			items:[{
				width:          300,
				xtype:          'fieldcontainer',
				layout:         'hbox',
				items:[{
					xtype:      'button',
					iconCls:    'icoAyuda',
					text:       '',
					handler:    function(){
						fnMuestraAyuda('Se habilita la Pr�rroga del Contrato de Cesi�n', 2);
					}
				},{
					width:      250,
					xtype:      'displayfield',
					cls:        'text-wrapper',
					value:      'Se habilita la Pr�rroga del Contrato de Cesi�n'
				}]
			},{
				width:          170,
				xtype:          'displayfield',
				value:          '<CENTER>C</CENTER>'
			},{
				width:          180,
				columns:        1,
				xtype:          'radiogroup',
				itemId:         'prorrogaContratoRg',
				vertical:       true,
				items: [
					{ boxLabel: 'Si', name: 'CDER_CS_PRORROGA_CONTRATO', inputValue: 'S' },
					{ boxLabel: 'No', name: 'CDER_CS_PRORROGA_CONTRATO', inputValue: 'N', checked: true}
				]
			}]
		},{
			items:[{
				width:          300,
				xtype:          'fieldcontainer',
				layout:         'hbox',
				items: [{
					xtype:      'button',
					iconCls:    'icoAyuda',
					text:       '',
					handler:    function(){
						fnMuestraAyuda('D�as h�biles que se cuentan para firmar el Contrato de Cesi�n de Derechos', 3);
				}
				},{
					width:      250,
					xtype:      'displayfield',
					cls:        'text-wrapper',
					value:      'D�as h�biles que se cuentan para firmar el Contrato de Cesi�n de Derechos'
				}]
			},{
				width:          170,
				xtype:          'displayfield',
				value:          '<CENTER>N</CENTER>'
			},{
				xtype:          'numberfield',
				itemId:         'diasFirmaContrato',
				name:           'CDER_DIAS_FIRMAR_CONTRATO', 
				step:           1, 
				width:          100,
				minValue:       0,
				value:          30,
				maxLength:      3
			}]
		}],
		buttons:[{
			text:               'Guardar',
			iconCls:            'icoGuardar',
				formBind:       true,
				handler:        bthFnGuardar
		}]
	});

//-------------------------------- PRINCIPAL -----------------------------------
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 890,
		height: 'auto',
		defaults:{
			style: {margin: '0 auto'}
		},
		items: [
			formaParams,
			NE.util.getEspaciador(20)
		]
	});
//-------------------------------- INICIALIZACION -----------------------------------

});