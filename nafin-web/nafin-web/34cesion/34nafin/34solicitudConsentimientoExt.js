Ext.onReady(function() {



	//F023-2015 
	var procesarCancelacionNafin = function (grid,rowIndex,colIndex,item,event){
		
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');	
		
		var motivosCancelacion=registro.get('MOTIVOS_CANCELACION');
		if(motivosCancelacion==''){
			Ext.MessageBox.alert("Mensaje",'Para Cancelar la solicitud es necesario primero capturar los Motivos de Cancelaci�n');
			return;
		}
		
		Ext.Ajax.request({
			url : '34solicitudConsentimientoExt.data.jsp',
			params : {
				informacion: 'CancelacionNafin',
				clave_solicitud:clave_solicitud,
				motivosCancelacion:motivosCancelacion
			},
			callback: function(opts, success, response){
				if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
					json = Ext.util.JSON.decode(response.responseText);
					Ext.Msg.alert('Mensaje', 'La operaci�n se realiz� exitosamente',function(){
						location.reload();
								
					});
				}else{
					NE.util.mostrarConnError(response,opts);
				}
			}
		});
			
	}
	
	
	//F023-2015 
	var VisorAutoConsorcio = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var ic_solicitud = registro.get('CLAVE_SOLICITUD');	
		
		var win = new NE.AutoConsorcio.VisorConsorcio(ic_solicitud, 'Autorizacion_Consorcio','Solicitud de Consentimiento' );
		
	}
	
	
	
/*--------------------------------- HANDLERS -------------------------------*/
	var procesarConsultaData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			var grid = Ext.getCmp('gridConsulta');
			if (!grid.isVisible()) {
				grid.show();
			}
			var jsonData = store.reader.jsonData;
			var  indiceCamposAdicionales = jsonData.indiceCamposAdicionales;
			var  clasificacionEpo = jsonData.clasificacionEpos;
			var cm = grid.getColumnModel();
			if(clasificacionEpo ==''){				
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CLASIFICACION_EPO'),clasificacionEpo);
			}
			if(indiceCamposAdicionales=='0'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(indiceCamposAdicionales=='1'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(indiceCamposAdicionales=='2'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.CAMPO_ADICIONAL_1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.CAMPO_ADICIONAL_2);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(indiceCamposAdicionales=='3'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.CAMPO_ADICIONAL_1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.CAMPO_ADICIONAL_2);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.CAMPO_ADICIONAL_3);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(indiceCamposAdicionales=='4'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.CAMPO_ADICIONAL_1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.CAMPO_ADICIONAL_2);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.CAMPO_ADICIONAL_3);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.CAMPO_ADICIONAL_4);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), true);
			}
			if(indiceCamposAdicionales=='5'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_1'),jsonData.CAMPO_ADICIONAL_1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_2'),jsonData.CAMPO_ADICIONAL_2);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_3'),jsonData.CAMPO_ADICIONAL_3);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_4'),jsonData.CAMPO_ADICIONAL_4);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_ADICIONAL_5'),jsonData.CAMPO_ADICIONAL_5);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_3'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_4'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_ADICIONAL_5'), false);
			}
			
			var el = grid.getGridEl();	
			if(store.getTotalCount() > 0) {			
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnGenerarArchivoCSV').enable();					
				el.unmask();
			} else {	
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnGenerarArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	function mostrarArchivoCSV(opts, success, response) {
		Ext.getCmp('btnGenerarArchivoCSV').enable();
		var btnImprimirCSV = Ext.getCmp('btnGenerarArchivoCSV');
		btnImprimirCSV.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoPDF(opts, success, response) {
		Ext.getCmp('btnGenerarPDF').enable();
		var btnImprimirPDF = Ext.getCmp('btnGenerarPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarDescargaArchivosContrato(opts, success, response) {
		Ext.getCmp('btnGenerarPDF').enable();
		var el = grid.getGridEl();
		el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var contratoArchivo = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud;
		var el = grid.getGridEl();
		var gridCorreo = Ext.getCmp('grid');		
		var store = grid .getStore();
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		el.mask('Descargando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '34solicitudConsentimientoExt.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'contratoPdf',
				 tipo:'PDF'	,		
				 clave_solicitud: clave_solicitud
			}),
			callback: procesarDescargaArchivosContrato
		});		
	}
		// Poderes de la Pyme 
	var VerPoderesPyme = function(grid, rowIndex, colIndex, item, event) {
	var registro = grid.getStore().getAt(rowIndex);
		var clavePyme = registro.get('CLAVE_PYME');	
		var rfcPyme = registro.get('RFC');	
		var grupo_cesion = registro.get('GRUPO_CESION');	
		var no_contrato = registro.get('NUMERO_CONTRATO');	
		
		var parametros = "clavePyme="+clavePyme+"&rfcPyme="+rfcPyme+"&grupo_cesion="+grupo_cesion+"&no_contrato="+no_contrato;
		
		var ventana = Ext.getCmp('VerPoderesPyme2');
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({
				layout: 'fit',
				width: 500,
				height: 200,			
				id: 'VerPoderesPyme2',
				closeAction: 'hide',
				items: [					
					PanelVerPoderesPyme2
				],
				title: 'Ver Poderes Pyme  '					
			}).show();
		}	
		var pabelBody = Ext.getCmp('PanelVerPoderesPyme2').body;
		var mgr = pabelBody.getUpdater();
		mgr.on('failure', 
		function(el, response) {
			pabelBody.update('');
			NE.util.mostrarErrorResponse(response);
		});		
		mgr.update({
			url: '/nafin/34cesion/34pki/34pyme/34SolicConsVerPoderesPymeExt.jsp?'+parametros,						
			indicatorText: 'Cargando Ver Poderes Pyme '
		});					
	}
	
	var PanelVerPoderesPyme2 = {
		xtype: 'panel',
		id: 'PanelVerPoderesPyme2',
		width: 700,
		height: 'auto',
		hidden: false,
		align: 'center',	
		autoScroll: true	
	};
	//-------------------------------- STORES -----------------------------------
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '34solicitudConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'Generar'
		},
		fields: [
					{name: 'DEPENDENCIA'},
					{name: 'CESIONARIO'},
					{name: 'PYME'},
					{name: 'RFC'},
					{name: 'NUMERO_PROVEEDOR'},
					{name: 'REPRESENTANTE'},
					{name: 'FECHA_SOLICITUD'},
					{name: 'NUMERO_CONTRATO'},
					{name: 'MONE'},
					{name: 'TIPO_CONTRATACION'},
					{name: 'FECHA_INICIO_CONTRATO'},
					{name: 'FECHA_FIN_CONTRATO'},
					{name: 'PLAZO_CONTRATO'},
					{name: 'CLASIFICACION_EPO'},
					{name: 'FECHA_LIMITE_FIRMA'},
					{name: 'CAMPO_ADICIONAL_1'},
					{name: 'CAMPO_ADICIONAL_2'},
					{name: 'CAMPO_ADICIONAL_3'},
					{name: 'CAMPO_ADICIONAL_4'},
					{name: 'CAMPO_ADICIONAL_5'},
					{name: 'VENTANILLA_PAGO'},
					{name: 'SUP_ADM_RESOB'},
					{name: 'NUMERO_TELEFONO'},
					{name: 'OBJETO_CONTRATO'},
					{name: 'COMENTARIOS'},//causas_rechazo
					{name: 'MONTO_CREDITO'},
					{name: 'NUMERO_REFERENCIA'},
					{name: 'FECHA_VENCIMIENTO'},
					{name: 'BANCO_DEPOSITO'},
					{name: 'NUMERO_CUENTA'},
					{name: 'CLABE'},
					{name: 'FECHA_AUTRECH_PYME'}, 
					{name: 'FECHA_AUTRECH_EPO'},
					{name: 'ESTATUS'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'CLAVE_PYME'},
					{name: 'OBSERV_RECHAZO'},
					{name: 'CAUSAS_RECHAZO'},
					{name: 'CLAVE_ESTATUS'},
					{name: 'MOTIVOS_CANCELACION'},
					{name: 'GRUPO_CESION'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				 beforeLoad:	{fn: function(store, options){
						Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues()),
						claveEPO: Ext.getCmp('cmbClaveEPO').getValue()
						
						});
					}},
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	

	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34solicitudConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'catEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	

	var catalogoCesionario = new Ext.data.JsonStore({
		id: 'catalogoCosecionario',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34solicitudConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'catCesionario'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoPyme = new Ext.data.JsonStore({
		id: 'catalogoPyme',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34solicitudConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'catPyme'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34solicitudConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'Moneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34solicitudConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'Estatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoContratacion = new Ext.data.JsonStore({
		id: 'catalogoContratacion',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34solicitudConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'TipoContratacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var catalogoPlazo = new Ext.data.JsonStore({
		id: 'catalogoPlazo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34solicitudConsentimientoExt.data.jsp',
		baseParams: {
			informacion: 'Plazo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
var grid = new Ext.grid.EditorGridPanel({
		store: consultaData,
		id: 'gridConsulta',
		hidden: true,
		frame: true,
		loadMask: true,
		height: 400,
		width: 900,
		align: 'center',
		style: 'margin:0 auto;',
		columns: [
			{
				header:'Dependencia',
				tooltip:'Dependencia',
				dataIndex:'DEPENDENCIA',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 100,
				align: 'left'
			},
			{
			
				header:'Intermediario Financiero (Cesionario)',
				tooltip:'Intermediario Financiero (Cesionario)',
				dataIndex:'CESIONARIO',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 150,
				align: 'left'
			},
			{
				header:'Pyme(Cedente)',
				tooltip:'Pyme(Cedente)',
				dataIndex:'PYME',
				sortable: true,
				resizable: true	,
				hidden: false,
				width: 150,
				align: 'left'
			},
			{
				header: '<center>R.F.C</center>', 
				tooltip: 'R.F.C',
				dataIndex: 'RFC',
				hidden: false,
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
				
			},			
			{
				header: 'No. Proveedor', 
				tooltip: 'No. Proveedor',
				dataIndex: 'NUMERO_PROVEEDOR',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'center'
				
			},			
			{
				header: '<center>Fecha de Solicitud PyME</center>', 
				tooltip: 'Fecha de Solicitud PyME',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'center'
				
			},			
			{
				header:'<center>No. Contrato</center>',
				tooltip:'No. Contrato',
				dataIndex:'NUMERO_CONTRATO',
				sortable: true,
				resizable: true,
				hidden: false,
				width: 150,
				align: 'center'
			},			
			{
				header: 'Monto / Moneda', 
				tooltip: 'Monto / Moneda',
				dataIndex: 'MONE',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'right'
				
			},			
			{
				header: 'Tipo de Contrataci&oacute;n', 
				tooltip: 'Tipo de Contrataci&oacute;n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
			},			
			{
				header: '<center>F. Inicio Contrato</center>', 
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'FECHA_INICIO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'center'
			
			},						
			{
				header:'<center>F. Final Contrato</center>',
				tooltip:'F. Final Contrato',
				dataIndex:'FECHA_FIN_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'	
			},			
			{
				header: 'Plazo del Contrato', 
				tooltip: 'Plazo del Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'center'
			},
			{
				header: 'CIUDAD UNICA', 
				tooltip: 'CIUDAD UNICA',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Campo 1',  
				tooltip: 'Campo 1',
				dataIndex: 'CAMPO_ADICIONAL_1',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Campo 2',  
				tooltip: 'Campo 2',
				dataIndex: 'CAMPO_ADICIONAL_2',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'			
			},
			{
				header: 'Campo 3',  
				tooltip: 'Campo 3',
				dataIndex: 'CAMPO_ADICIONAL_3',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Campo 4',  
				tooltip: 'Campo 4',
				dataIndex: 'CAMPO_ADICIONAL_4',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Campo 5',  
				tooltip: 'Campo 5',
				dataIndex: 'CAMPO_ADICIONAL_5',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Ventanilla de Pago', 
				tooltip: 'Ventanilla de Pago',
				dataIndex: 'VENTANILLA_PAGO',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Supervisor/Administrador/Residente de Obra', 
				tooltip: 'Supervisor/Administrador/Residente de Obra',
				dataIndex: 'SUP_ADM_RESOB',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
			},
			{
				header: 'T�lefono', 
				tooltip: 'T�lefono',
				dataIndex: 'NUMERO_TELEFONO',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Objeto del Contrato', 
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Comentarios', 
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato',
				tooltip: 'Contrato',
				dataIndex: 'icSolicitud',
				width: 100,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('Contrato') ==0 ) {
						return 'N/A';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						if(registro.get('Contrato') !=0 ) {
							this.items[0].tooltip = 'Buscar';
							return 'icoBuscar';		
							}								
						},handler: contratoArchivo
						
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Poderes',
				tooltip: 'Poderes',
				dataIndex: 'icSolicitud',
				width: 100,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('Contrato de Cesi�n') ==0 ) {
						return 'N/A';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						if(registro.get('Contrato de Cesi�n') !=0 ) {
							this.items[0].tooltip = 'Buscar';
							return 'icoBuscar';		
							}								
						},handler: VerPoderesPyme
						
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Autorizaci�n Consentimiento',
				tooltip: 'Autorizaci�n Consentimiento',
				dataIndex: 'icSolicitud',
				width: 100,
				align: 'center',				
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {						
							this.items[0].tooltip = 'Buscar';
							return 'icoBuscar';																
						},handler: VisorAutoConsorcio
						
					}
				]				
			},					
			{
				header: 'F.L�mite de Firma', 
				tooltip: 'F. L�mite de Firma',
				dataIndex: 'FECHA_LIMITE_FIRMA',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'right'
				
			},
			{
				header: 'Estatus', 
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				resizable: true,
				width: 130,			
				align: 'left'
				
			},
			{
				xtype: 'actioncolumn',
				header: 'Acciones',
				tooltip: 'Acciones',
				dataIndex: 'icSolicitud',
				width: 100,
				align: 'center',	
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('CLAVE_ESTATUS') ==5  ||  registro.get('CLAVE_ESTATUS')==6   ||  registro.get('CLAVE_ESTATUS')==8  ||  registro.get('CLAVE_ESTATUS')==24 ) {
						return 'N/A';
					}				
				},				
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						if(registro.get('CLAVE_ESTATUS')==1  ||  registro.get('CLAVE_ESTATUS')==2   ||  registro.get('CLAVE_ESTATUS')==3  ||  registro.get('CLAVE_ESTATUS')==13 ) {
							this.items[0].tooltip = 'Cancelada por NAFIN';
							return 'icoRechazar';		
							}								
						},handler: procesarCancelacionNafin
						
					}
				]				
			},
			{
				header: 'Motivos de Cancelaci�n',  
				tooltip: 'Motivos de Cancelaci�n',
				dataIndex: 	'MOTIVOS_CANCELACION', 
				fixed:		true,
				sortable: 	false,	
				width: 		174,
				hiddeable: 	false,
				editor:{
					xtype:	'textarea',					
					maxLength: 500,
					enableKeyEvents: true,
					listeners:	{
						keyup: function(field, e){
							if ( (field.getValue()).length > 500){
								field.setValue((field.getValue()).substring(0,500));
								Ext.Msg.alert('Observaciones','Los Motivos de Cancelaci�n no debe sobrepasar los 500 caracteres.');								
								field.focus();
							}
						}
					}
				},
				renderer:function( value, metadata, record, rowIndex, colIndex, store){
					
					if( record.data['CLAVE_ESTATUS']!=24  ){
						metadata.attr = 'style="border: thin solid #3399CC;"'; 
						return value;
					}else {
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						return value;
					}
				}
			}
		],
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var grid = e.grid; 
				var campo= e.field;							
				if(campo == 'MOTIVOS_CANCELACION'){
					if(record.data['CLAVE_ESTATUS']==24 ){ 
						return false;
					}else {
						return true; 
					}
				}	
			}				
		},		
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No se encontro ning�n registro",
			items: [
				'->',
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnGenerarArchivoCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url:'34solicitudConsentimientoExt.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{					
								informacion:'GenerarArchivoCSV',
								tipo:'CSV'
								
							}),
							callback: mostrarArchivoCSV
						});		
							
					}
				},				
				{
					xtype: 'button',
					text: 'Generar PDF',
					iconCls:	'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34solicitudConsentimientoExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
								
							}),
							callback: mostrarArchivoPDF
						});
					}
				}			
			]
		}	
	});
var elementosForma = [
		{
			xtype: 'combo',	
			name: 'claveEPO',	
			id: 'cmbClaveEPO',
			hiddenName : 'claveEPO',
			fieldLabel:'Nombre de la EPO',
			 mode: 'local', 
			autoLoad: false,	
			displayField : 'descripcion',	
			valueField : 'clave',
			emptyText: 'Seleccionar...',
			forceSelection : true,	
			triggerAction : 'all',	
			typeAhead: true,
			allowBlank: true,	
			minChars : 1,	
			store : catalogoEPO,	
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {
					
						catalogoContratacion.load({
							params: {
								claveEPO:combo.getValue()
							}				
						});					
					}
				}
			}
		},
		{
			xtype: 'combo',
			name: 'claveIf',
			id: 'cmbClaveIf',
			hiddenName : 'claveIf',
			fieldLabel: 'Nombre de Cesionario(IF)',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccionar...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoCesionario,
			tpl : NE.util.templateMensajeCargaCombo
						
		},
		{
			xtype: 'combo',
			name: 'clavePyme',
			id: 'cmbClavePyme',
			hiddenName : 'clavePyme',
			fieldLabel: 'PYME',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccionar...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,		
			store : catalogoPyme,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Contrato',
			combineErrors: true,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			combineErrors: false,
			minChars : 1,
			width: 500,
			items: [				
				{
					xtype: 'textfield',
					name: 'numeroContrato',
					id: 'tfNumeroContrato',
					allowBlank: true,
					maxLength: 25,
					width: 100,
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',			
					valueField: 'clave',
					hiddenName : 'numeroContrato',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1					
					
				}
			]

		},
		{
			xtype: 'combo',
			name: 'claveMoneda',
			id: 'cmbClaveMoneda',
			hiddenName : 'claveMoneda',
			fieldLabel: 'Moneda',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccionar...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,		
			tpl : NE.util.templateMensajeCargaCombo,
			store : catalogoMoneda
		},
		{
			xtype: 'combo',
			name: 'claveEstatusSol',
			id: 'cmbClaveEstatusSol',
			hiddenName : 'claveEstatusSol',
			fieldLabel: 'Estatus',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccionar...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,		
			store : catalogoEstatus,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'claveContrat',
			id: 'cmbClaveContrat',
			hiddenName : 'claveContrat',
			fieldLabel: 'Tipo de Contrataci�n',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccionar...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,		
			store : catalogoContratacion,
			tpl : NE.util.templateMensajeCargaCombo
		},	
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vigencia del Contrato',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaVigenciaIni',
					id: 'dfFechaVigenciaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'dfFechaVigenciaFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaVigenciaFin',
					id: 'dfFechaVigenciaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'dfFechaVigenciaIni',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 20
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo del Contrato',
			combineErrors: true,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			combineErrors: false,
			minChars : 1,
			width: 500,
			items: [
				{
					xtype: 'textfield',
					fieldLabel: 'Plazo del Contrato',
					name: 'plazoContrato',
					id: 'tfPlazoContrato',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',			
					valueField: 'clave',
					maxLength: 4,
					hiddenName : 'plazoContrato',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					maskRe:/[0-9]/
				},				
				{
					xtype: 'combo',	
					name: 'claveTipoPlazo',	
					id: 'cmbClaveTipoPlazo',
					hiddenName : 'claveTipoPlazo',
					fieldLabel:'Nombre de la EPO',
					 mode: 'local', 
					autoLoad: false,	
					displayField : 'descripcion',	
					valueField : 'clave',
					emptyText: 'Seleccionar...',
					forceSelection : true,	
					triggerAction : 'all',	
					typeAhead: true,
					allowBlank: true,	
					minChars : 1,	
					store : catalogoPlazo,	
					tpl : NE.util.templateMensajeCargaCombo
			
				}
			]	
		}
	];

 var fp = new Ext.form.FormPanel({
		id: 'forma',
		frame			: true,
		width: 600,		
		autoHeight	: true,
		title: 'Solicitudes de Consentimiento',				
		layout		: 'form',
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
      defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
					var df_fecha_solicitudIni=Ext.getCmp('dfFechaVigenciaIni');					
					var df_fecha_solicitudFin=Ext.getCmp('dfFechaVigenciaFin');
					if(!Ext.isEmpty(df_fecha_solicitudIni.getValue()) || !Ext.isEmpty(df_fecha_solicitudFin.getValue())){
						if(Ext.isEmpty(df_fecha_solicitudIni.getValue())){
							df_fecha_solicitudIni.markInvalid('El valor de la fecha de vigencia inicial es requerido');
							df_fecha_solicitudIni.focus();
							return;
						}
						if(Ext.isEmpty(df_fecha_solicitudFin.getValue())){
							df_fecha_solicitudFin.markInvalid('El valor de la fecha Final de Vencimiento es requerido');
							df_fecha_solicitudFin.focus();
							return;
						}
					}
					var numeroPlazo=Ext.getCmp('tfPlazoContrato');					
					var clavePlazo=Ext.getCmp('cmbClaveTipoPlazo');
					if((!Ext.isEmpty(df_fecha_solicitudIni.getValue()) && !Ext.isEmpty(df_fecha_solicitudFin.getValue()))){
						if((!Ext.isEmpty(numeroPlazo.getValue()) && !Ext.isEmpty(clavePlazo.getValue()))){
							alert("S�lo puede ingresar el valor de la Fecha Inicial y Final de la Vigencia del Contrato � el Plazo del Contrato, no ambos.");	
							return "";
						}
					}
					
					var epo = Ext.getCmp('cmbClaveEPO');
					if(Ext.isEmpty(epo.getValue())){
						epo.markInvalid('Seleccione una EPO por favor');
						return;
					}
					fp.el.mask('Consultando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Generar',
							start: 0,
							limit: 15,
							operacion:'Generar'
						})
					});						
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				formBind: true	,
				handler: function() {
					fp.el.mask('Limpiando...', 'x-mask-loading');
					window.location = '34solicitudConsentimientoExt.jsp';
				}
			}
		]
	});
	//-------------------------------- PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		applyTo: 'areaContenido',
		width: 949,	
		align: 'center',
		style: 'margin:0 auto;',
		items: [
			
			fp,
			NE.util.getEspaciador(20),
			grid,
			NE.util.getEspaciador(20)			
		]
	});
	catalogoEPO.load();
	catalogoCesionario.load();
	catalogoPyme.load();
	catalogoMoneda.load();
	catalogoEstatus.load();
	catalogoPlazo.load();
	
});//fin onReady