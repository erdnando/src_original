<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.text.*,
	java.util.*,
	java.io.File,
	org.apache.commons.logging.Log,
	com.netro.pdf.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.CatalogoEPO_CesionDerechos,
	com.netro.model.catalogos.CatalogoPymePorGrupoCesion,
	com.netro.cesion.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion   = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String cveEpo   = request.getParameter("cmbEpoCesion") == null?"":(String)request.getParameter("cmbEpoCesion");
String infoRegresar = "";
JSONObject jsonObj = new JSONObject();
boolean success = true;

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

if(informacion.equals("CONS_CAT_EPO_CESION")){
	/*
	Estoy asumiendo que este catálogo solo va a mostrar las EPOS con las siguientes características:
	CS_CESION_DERECHOS = 'S' AND CS_HABILITADO = 'S'
	*/
	CatalogoEPO_CesionDerechos catalogo = new CatalogoEPO_CesionDerechos();
	catalogo.setCampoClave("IC_EPO");
	catalogo.setCampoDescripcion("CG_RAZON_SOCIAL");
	catalogo.setOrden("CG_RAZON_SOCIAL");
	infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("GUARDA_PARAMETROS")){
	Map map = new HashMap();
	JSONObject jsonData = JSONObject.fromObject(request.getParameter("formData"));
	Iterator keysItr = jsonData.keys();
	while(keysItr.hasNext()) {
		String key = (String)keysItr.next();
		String value = jsonData.getString(key);
		System.out.println("key = "+key);
		System.out.println("value = "+value);
		if(key.indexOf("CDER_")!=-1 )
			map.put(key,value);
	}
	
	cesionBean.setParametrosEpo(jsonData.getString("cmbEpoCesion"), map);
	
	jsonObj.put("success",new Boolean(true));
	infoRegresar = jsonObj.toString();
}else if(informacion.equals("OBTIENE_PARAMETROS")){
	Map mpParams = cesionBean.getParametrosEpo(cveEpo);
	//mpParams.put("CDER_DIAS_FIRMAR_CONTRATO",mpParams.get("CDER_DIAS_FIRMAR_CONTRATO")==null?"30":mpParams.get("CDER_DIAS_FIRMAR_CONTRATO"));
	mpParams.put("cmbEpoCesion",cveEpo );
	jsonObj.put("formData", JSONObject.fromObject(mpParams));
	jsonObj.put("success",new Boolean(true));
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>