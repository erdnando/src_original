Ext.onReady(function(){
	var win;
	var winHelp;
	var winConsulta;
	var descripcion = ['<br><div align="center"> Es el n�mero que identifica el contrato formalizado con la Entidad que ser� financiada. </div><br>&nbsp;'];

	var getIconoAyuda = function(){
		var iconoAyuda =
		"<img class=\"x-btn x-btn-icon\" src=\"/nafin/00utils/gif/ayudaLayout.png\" border=\"0\" width=\"25\" style=\"padding-right:2px;height: 16px;width: 16px;\" onclick=\"javascript:mostrarAyuda();\" >";
		//"<img class=\"x-btn x-btn-icon\" src=\"/nafin/00utils/gif/ayudaLayout.png\" border=\"0\" width=\"25\" style=\"padding-right:10px;height: 16px;width: 16px; outline: 0;\" onclick=\"javascript:mostrarAyuda();\" >";
		return iconoAyuda;
	}
//------------------------------ Handlers ------------------------------//

	// Realiza la validacion para editar el grupo seleccionado en el grid 
	function editarGrupo(){

		// Verifico que se hay seleccionado un registro del grid
		var forma = Ext.ComponentQuery.query('#formaAgregarGrupos')[0];
		var fila = forma.query('#textfieldFila')[0].getValue();
		if(fila == '' || fila == null){
			Ext.MessageBox.alert('Mensaje', 'Debe seleccionar un registro.');
			return;
		}

		// Obtengo los datos del gris y valido si es editable
		var rec      = consultaGruposData.getAt(fila);
		var ic_grupo = rec.get('IC_GRUPO_CESION');
		Ext.Ajax.request({
			url: '34gruposCesionDerechosExt.data.jsp',
			params: {
				informacion: 'Valida_Grupo',
				icGrupoCesion: ic_grupo
			},
			callback: procesarEditaGrupo
		});
	}

	// Muestra el form para agregar un grupo nuevo 
	function agregarGrupo(){
		// Realizo la consulta del combo 'Cadena Productiva'
		catalogoEPO.load();
		// Muestro y oculto los componentes correspondientes
		Ext.ComponentQuery.query('#gridGrupos')[0].hide();
		var forma = Ext.ComponentQuery.query('#formaAgregarGrupos')[0];
		forma.show();
		forma.query('#cmb_epo_id')[0].show();
		forma.query('#txt_epo_id')[0].hide();
		forma.query('#textfieldIcGrupo')[0].setValue(''); // Este campo es para indicar que el grupo es nuevo
		forma.query('#textfieldIcEpo')[0].setValue(''); // Este campo es para indicar que el grupo es nuevo
		forma.setTitle('Agregar Grupo');
	}

	// Elimina el grupo seleccionado en el grid 'Grupos de Cesi�n de Derechos'
	function eliminarGrupo(){
		// Verifico que se hay seleccionado un registro del grid
		var forma = Ext.ComponentQuery.query('#formaAgregarGrupos')[0];
		var fila = forma.query('#textfieldFila')[0].getValue();
		if(fila == '' || fila == null){
			Ext.MessageBox.alert('Mensaje', 'Debe seleccionar un registro.');
			return;
		}
		forma.query('#textfieldFila')[0].setValue(''); // Limpio el campo para el proximo evento
		// Comfirmar eliminaci�n
		Ext.Msg.confirm('Confirmaci�n', '�Desea eliminar el registro seleccionado?',function(botonConf){
			if (botonConf == 'ok' || botonConf == 'yes'){
				var rec = consultaGruposData.getAt(fila);
				var ic_grupo = rec.get('IC_GRUPO_CESION');
				Ext.ComponentQuery.query('#gridGrupos')[0].getView().getEl().mask('Procesando...', 'x-mask-loading');
				Ext.Ajax.request({
					url: '34gruposCesionDerechosExt.data.jsp',
					params: {
						informacion: 'Eliminar_Grupo',
						icGrupoCesion: ic_grupo
					},
					callback: procesarEliminarGrupo
				});
			}
		});

	}

	// Guarda el los datos del grupo agregado o editado en el form 'formaAgregarGrupos'
	function actualizaGrupo(){

		var ic_epo;
		var forma        = Ext.ComponentQuery.query('#formaAgregarGrupos')[0];
		var ic_grupo     = forma.query('#textfieldIcGrupo')[0].getValue();
		var no_contrato  = forma.query('#no_contrato_id')[0].getValue();
		if(ic_grupo == ''){ // El grupo es nuevo
			ic_epo = forma.query('#cmb_epo_id')[0].getValue();
		} else{
			ic_epo = forma.query('#textfieldIcEpo')[0].getValue();
		}

		//Se valida el form
		if(!Ext.ComponentQuery.query('#formaAgregarGrupos')[0].getForm().isValid()){
			return;
		}
		if(ic_grupo == '' && ic_epo == ''){// Este if valida el combo de la cadena productiva solo si el grupo es nuevo
			forma.query('#cmb_epo_id')[0].markInvalid('Este campo es obligatorio');
			return;
		}
		if(no_contrato == '' || no_contrato == null){// Este if valida que el campo numero de contrato no est� vacio
			forma.query('#no_contrato_id')[0].markInvalid('Este campo es obligatorio');
			return;
		}
		// Valido que solo se haya seleccionado una empresa principal
		var grid = Ext.ComponentQuery.query('#gridEmpresas')[0];
		var store = grid.getStore();
		var principal = 0;
		var contador = 0;
		store.each(function(record){
			if(record.data['PYME_PRINCIPAL'] == true){
				principal++;
			}
			contador++;
		});
		if(contador < 2){
			Ext.MessageBox.alert('Mensaje', 'Debe asignar al grupo al menos dos empresas.');
			return;
		}
		if(principal == 0){
			Ext.MessageBox.alert('Mensaje', 'Por favor elija a la empresa principal.');
			return;
		}
		if(principal > 1){
			Ext.MessageBox.alert('Mensaje', 'Debe seleccionar solo una empresa principal.');
			return;
		}
	
		// Se env�an los datos a actualizar
		var jsonDataEncode = '';
		var records = store.getRange();
		var datar = new Array();
		for (var i = 0; i < records.length; i++) {
			datar.push(records[i].data);
		}
		jsonDataEncode = Ext.encode(datar);
		Ext.Ajax.request({
			url: '34gruposCesionDerechosExt.data.jsp',
			params: {
				informacion:   'Actualizar_Grupo',
				icGrupoCesion: ic_grupo,
				icEpo:         ic_epo,
				nombreGrupo:   '',
				noContrato:    no_contrato,
				datosGrid:     jsonDataEncode
			},
			callback: procesarActualizarGrupo
		});
	}

	// Se cancela el proceso de agregar o editar grupo, se oculta el form y se muestra el grid 'Grupos Cesi�n de Derechos' 
	function cancelaEdicionGrupo(){
		window.location.href = '34gruposCesionDerechosExt.jsp';
	}

	// Se cierra la ventana que muestra las empresas seleccionadas en el grid 'Grupos Cesi�n de Derechos' 
	function cierraFormConsultaEmpresas(){
		Ext.ComponentQuery.query('#ventanaMostrar')[0].hide();
		Ext.ComponentQuery.query('#gridGrupos')[0].show();
	}

	// Renderer del grid gridConsultarEmpresas
	function pymePrincipal(value, metaData, record, rowIndex, colIndex, store){
		if (record.data.PYME_PRINCIPAL == true)
			return '<b>Empresa Principal </b>' + value;
		else 
			return value;
	}

	// Valida si la epo y el numero de contrato ingresados ya existe en otro grupo. Ya que no deben repetirse.
	function validaContratosRepetidos(noContrato){
		if(noContrato == '' || noContrato == null){
			return;
		}
		//Si el campo 'textfieldIcGrupo' est� vac�o, entonces la empresa pertenece a un grupo nuevo, el ic_epo se obtiene del combo.
		//En caso contrario, el grupo ya existe y la epo se trae del grid 'Grupo de Cesi�n de Derechos'
		var forma = Ext.ComponentQuery.query('#formaAgregarGrupos')[0];
		var ic_grupo = forma.query('#textfieldIcGrupo')[0].getValue();
		var icEpo = '';
		if(ic_grupo == ''){
			if(forma.query('#cmb_epo_id')[0].getValue() == '' || forma.query('#cmb_epo_id')[0].getValue() == null){
				Ext.MessageBox.alert('Mensaje', 'Seleccione una Cadena Productiva');
				forma.query('#no_contrato_id')[0].setValue('');
				return;
			} else{
				icEpo = forma.query('#cmb_epo_id')[0].getValue();
			}
		} else{
			icEpo = forma.query('#textfieldIcEpo')[0].getValue();
		}
		Ext.Ajax.request({
			url: '34gruposCesionDerechosExt.data.jsp',
			params: {
				informacion:   'Valida_Contrato',
				icGrupoCesion: ic_grupo,
				icEpo:         icEpo,
				noContrato:    noContrato
			},
			callback: procesarValidaContrato
		});
		
		
	}

	// Despues de realizar la validaci�n del grupo, se muestra el form o se indica que no se puede editar
	var procesarEditaGrupo = function(options, success, response){
		if (success == true && Ext.JSON.decode(response.responseText).success == true){
			var jsonData = Ext.JSON.decode(response.responseText);
			if(jsonData.mensaje != ''){
				Ext.MessageBox.alert('Mensaje', jsonData.mensaje);
			} else{

				var forma = Ext.ComponentQuery.query('#formaAgregarGrupos')[0];
				var fila = forma.query('#textfieldFila')[0].getValue();
				// Obtengo los datos del gris y valido si es editable
				var rec         = consultaGruposData.getAt(fila);
				var ic_grupo    = rec.get('IC_GRUPO_CESION');
				var ic_epo      = rec.get('IC_EPO');
				var no_contrato = rec.get('CG_NO_CONTRATO');
				var nombre_epo  = rec.get('NOMBRE_EPO');

				// Muestro y oculto los componentes correspondientes
				forma.show();
				forma.query('#cmb_epo_id')[0].hide();
				forma.query('#txt_epo_id')[0].show();
				forma.query('#textfieldFila')[0].setValue(''); // Limpio el campo para el proximo evento
				Ext.ComponentQuery.query('#gridGrupos')[0].hide();

				// Lleno los campos del form con los datos del grid
				forma.query('#no_contrato_id')[0].setValue(no_contrato);
				forma.query('#txt_epo_id')[0].setValue(nombre_epo);
				forma.query('#textfieldIcGrupo')[0].setValue(ic_grupo); // Este campo es para saber que grupo es el que se editar�
				forma.query('#textfieldIcEpo')[0].setValue(ic_epo); // Este campo es para saber que grupo es el que se editar�
				forma.setTitle('Editar Grupo');

				// Realizo la consulta del 'Grid Empresas'
				consultaEmpresasData.load({
					params:{
						icGrupoCesion: ic_grupo
					}
				});

			}
		}
	}

	// Se ejecuta una vez que se va a actualizar o insertar un grupo
	var procesarActualizarGrupo = function(options, success, response){
		if (success == true && Ext.JSON.decode(response.responseText).success == true){
			var jsonData = Ext.JSON.decode(response.responseText);
			if(jsonData.mensaje == 'OK'){
				Ext.MessageBox.alert('Mensaje', 'El grupo se actualiz� correctamente.',cancelaEdicionGrupo);
			} else{
				Ext.MessageBox.alert('Mensaje', jsonData.mensaje,cancelaEdicionGrupo);
			}
		} else{
			Ext.MessageBox.alert('Error', 'Ocurri� un error al Actualizar el grupo.',cancelaEdicionGrupo);
		}
	}

	// Se ejecuta una vez que se manda a eliminar un grupo del grid 'Grupos de Cesi�n de Derechos'
	var procesarEliminarGrupo = function(options, success, response){
		if (success == true && Ext.JSON.decode(response.responseText).success == true){
			var jsonData = Ext.JSON.decode(response.responseText);
			if(jsonData.mensaje == 'OK'){
				// Consulta nuevamente los valores iniciales para actualizar el grid
				Ext.MessageBox.alert('Mensaje', 'El registro se elimin� correctamente.');
				consultaGruposData.load({
					params: {
						informacion: 'Consulta_Grid_Grupos',
						operacion:   'generar',
						start:       0,
						limit:       15
					}
				});
			} else{
				Ext.MessageBox.alert('Mensaje', jsonData.mensaje);
			}
		} else{
			Ext.MessageBox.alert('Error', 'Ocurri� un error al intentar eliminar el registro.');
		}
		Ext.ComponentQuery.query('#gridGrupos')[0].getView().getEl().unmask();
	}

	// Muestra un alert si el contrato ya existe en otro grupo
	var procesarValidaContrato = function(options, success, response){
		if (success == true && Ext.JSON.decode(response.responseText).success == true){
			if(Ext.JSON.decode(response.responseText).mensaje != ''){
				Ext.ComponentQuery.query('#formaAgregarGrupos')[0].query('#no_contrato_id')[0].setValue('');
				Ext.MessageBox.alert('Mensaje', Ext.JSON.decode(response.responseText).mensaje);
			}
		}
	}

	// Procesa los datos de la pyme seleccionada en el combo del form Agregar Empresa
	var procesarConsultaDatosPyme = function(options, success, response){
		if (success == true && Ext.JSON.decode(response.responseText).success == true){
			var jsonData = Ext.JSON.decode(response.responseText);
			if(jsonData.mensaje == ''){
				var forma = Ext.ComponentQuery.query('#formaAgregarEmpresa')[0];
				forma.query('#representante_legal_id')[0].setValue(jsonData.REPRESENTANTE_LEGAL);
				forma.query('#contacto_id')[0].setValue(jsonData.CONTACTO);
				forma.query('#hid_ic_pyme_id')[0].setValue(jsonData.IC_PYME);
				forma.query('#hid_nombre_pyme_id')[0].setValue(jsonData.NOMBRE_PYME);
				forma.query('#hid_rfc_pyme_id')[0].setValue(jsonData.RFC);
				forma.query('#hid_ne_pyme_id')[0].setValue(jsonData.NAFIN_ELECTRONICO);
			}else{
				Ext.MessageBox.alert('Error', jsonData.mensaje);
			}
		}
	}

	// Se ejecuta una vez que se realiza la consulta del grid 'Grupos de Cesi�n de Derechos'
	var procesarConsultaGruposData = function(store,  arrRegistros, success, opts){
		if (arrRegistros != null){
			var gridGrupos =  Ext.ComponentQuery.query('#gridGrupos')[0];
			var json =  store.proxy.reader.rawData;

			Ext.ComponentQuery.query('#gridGrupos')[0].show();

			var btnEditar = Ext.ComponentQuery.query('#btnEditarGrupo')[0];
			var btnAgregar = Ext.ComponentQuery.query('#btnAgregarGrupo')[0];
			var btnEliminar = Ext.ComponentQuery.query('#btnEliminarGrupo')[0];

			if(store.getTotalCount() > 0) {
				btnEditar.enable();
				btnAgregar.enable();
				btnEliminar.enable();
			} else{
				btnEditar.disable();
				btnAgregar.enable();
				btnEliminar.disable();
			}
		}
	};

	// Habilita o deshabilita el combo Nombre del form 'B�squeda Empresa'
	var procesarCatalogoEmpresa = function( store, records, successful, options ){
		var formaAgregarEmpresa = Ext.ComponentQuery.query('#formaAgregarEmpresa')[0];
		// Si la carga no fue exitosa, no hacer nada
		if( successful == false ){
			formaAgregarEmpresa.query('#pyme_id')[0].disable();
			formaAgregarEmpresa.query('#btn_aceptar_win')[0].disable();
			return;
		}
		if(store.getTotalCount() > 0) {
			formaAgregarEmpresa.query('#pyme_id')[0].enable();
			formaAgregarEmpresa.query('#btn_aceptar_win')[0].enable();
		} else{
			formaAgregarEmpresa.query('#pyme_id')[0].disable();
			formaAgregarEmpresa.query('#btn_aceptar_win')[0].disable();
			Ext.MessageBox.alert('Mensaje', 'No existe informaci�n con los criterios determinados.');
		}
	};

//------------------------------ Stores ------------------------------//

	// Se define el modelo para el store del grid 'Grupos de Cesi�n de Derechos'
	Ext.define('ListaGrupos',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'IC_GRUPO_CESION'     },
			{name: 'IC_EPO'              },
			{name: 'NOMBRE_EPO'          },
			{name: 'CG_NOMBRE_GRUPO'     },
			{name: 'CG_NO_CONTRATO'      },
			{name: 'CG_EXPEDIENTE_EFILE' },
			{name: 'FECHA_REGISTRO'      }
		]
	});

	// Se define el modelo para el store del grid 'Empresas'
	Ext.define('ListaEmpresas',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'IC_GRUPO_CESION'  },
			{name: 'IC_PYME'          },
			{name: 'NOMBRE_PYME'      },
			{name: 'NAFIN_ELECTRONICO'},
			{name: 'RFC'              },
			{name: 'PYME_PRINCIPAL',type: 'bool'}
		]
	});

	// Se define el modelo para el store del grid para consultar empresas, �ste se muestra dentro del form 'formaConsultarEmpresas'
	Ext.define('ListaEmpresas_show',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'IC_GRUPO_CESION'  },
			{name: 'IC_PYME'          },
			{name: 'NOMBRE_PYME'      },
			{name: 'NAFIN_ELECTRONICO'},
			{name: 'RFC'              },
			{name: 'PYME_PRINCIPAL',type: 'bool'}
		]
	});

	// Se crea el MODEL para el cat�logo 'Cadena Productiva'
	Ext.define('ModelCatologoEPO',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);

	// Se crea el MODEL para el cat�logo 'Nombre de la Empresa' del form B�squeda de Empresa
	Ext.define('ModelCatologoEmpresa',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);



	// Se crea el STORE del grid 'Grupos de Cesi�n de Derechos'
	var consultaGruposData = Ext.create('Ext.data.Store',{
		model: 'ListaGrupos',
		proxy: {
			type: 'ajax',
			url:  '34gruposCesionDerechosExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Grid_Grupos'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultaGruposData
		}
	});

	// Se crea el STORE del grid 'Empresas'
	var consultaEmpresasData = Ext.create('Ext.data.Store',{
		model: 'ListaEmpresas',
		storeId:'consultaEmpresasData',
		proxy: {
			type: 'ajax',
			url:  '34gruposCesionDerechosExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Grid_Empresas'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false
	});

	// Se crea el STORE del grid para consultar empresas, �ste se muestra dentro del form 'formaConsultarEmpresas'
	var consultaEmpresasData_show = Ext.create('Ext.data.Store',{
		model: 'ListaEmpresas_show',
		proxy: {
			type: 'ajax',
			url:  '34gruposCesionDerechosExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Consulta_Grid_Empresas'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false
	});

	// Se crea el STORE para el cat�logo 'Cadena Productiva'
	var catalogoEPO = Ext.create('Ext.data.Store',{
		model: 'ModelCatologoEPO',
		proxy: {
			type: 'ajax',
			url: '34gruposCesionDerechosExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Catalogo_EPO'
			},
			autoLoad: false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false
	});

	// Se crea el STORE para el cat�logo 'Nombre Empresa'
	var catalogoEmpresa = Ext.create('Ext.data.Store',{
		model: 'ModelCatologoEmpresa',
		proxy: {
			type: 'ajax',
			url: '34gruposCesionDerechosExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'Catalogo_Empresa'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarCatalogoEmpresa
		}
	});

//------------------------------ Componentes ------------------------------//

	// Se muestra la ventana cuando se selecciona la opci�n "Consultar Empresas" del grid "Grupos de Cesi�n de Derechos"
	function mostrarConsultaEmpresas(){
		if (!winConsulta){
			winConsulta = new Ext.widget('window',{
				width:       550,
				itemId:      'ventanaMostrar',
				closeAction: 'hide',
				layout:      'fit',
				modal:       true,
				autoHeight:  true,
				resizable:   false,
				constrain:   true,
				closable:    false,
				autoScroll:  true,
				items:       formaConsultarEmpresas
			})
		}
		winConsulta.show();
	}

	// Se activa con el bot�n de Ayuda del form AgregarGrupos
	mostrarAyuda = function(){
		if (!winHelp){
			winHelp = new Ext.widget('window',{
				width:       550,
				id:          'ventanaAyuda',
				title:       'No. Contrato',
				closeAction: 'hide',
				layout:      'fit',
				modal:       true,
				autoHeight:  true,
				resizable:   false,
				constrain:   true,
				closable:    true,
				autoScroll:  true,
				html:        descripcion.join('')
			})
		}
		winHelp.show();
	}

	// Muestra la ventana de B�squeda de Empresa
	function muestraFormEmpresa(){
		if (!win){
			win = new Ext.widget('window',{
				width:       500,
				height:      400,
				minHeight:   400,
				itemId:      'win_Empresa',
				title:       'B�squeda de Empresa',
				closeAction: 'hide',
				layout:      'fit',
				modal:       true,
				autoHeight:  true,
				resizable:   false,
				constrain:   true,
				closable:    false,
				autoScroll:  true,
				items:       formaAgregarEmpresa
			})
		}
		win.show();
	}

	// Se crea el grid 'Grupos de Cesi�n de Derechos'
	var gridGrupos = Ext.create('Ext.grid.Panel',{
		width:            800,
		height:           450,
		store:            consultaGruposData,
		itemId:           'gridGrupos',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'Grupos de Cesi�n de Derechos',
		frame:            false,
		stripeRows:       true,
		autoScroll:       true,
		hidden:           true,
		enableColumnMove: false,
		enableColumnHide: false,
		listeners: {
			cellclick: function(gridView, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
				// Este l�stener es para saber cual registro del grid estoy seleccionando
				var forma = Ext.ComponentQuery.query('#formaAgregarGrupos')[0];
				forma.query('#textfieldFila')[0].setValue(rowIndex);
			}
		},
		columns: [{
			width:       200,
			header:      'No. Contrato',
			dataIndex:   'CG_NO_CONTRATO',
			align:       'center',
			sortable:    true,
			resizable:   true
		},{
			width:       200,
			header:      'Expediente E-File',
			dataIndex:   'CG_EXPEDIENTE_EFILE',
			align:       'center',
			sortable:    true,
			resizable:   true
		},{
			width:       200,
			header:      'Fecha de Registro',
			dataIndex:   'FECHA_REGISTRO',
			align:       'center',
			sortable:    true,
			resizable:   true
		},{
			width:       195,
			xtype:       'actioncolumn',
			text:        'Consultar Empresas',
			align:       'center',
			sortable:    false,
			resizable:   false,
			items: [{
				iconCls:  'icoBuscar',
				handler: function(grid, rowIndex, colIndex) {
					var forma = Ext.ComponentQuery.query('#formaConsultarEmpresas')[0];
					var rec          = consultaGruposData.getAt(rowIndex);
					var ic_grupo     = rec.get('IC_GRUPO_CESION');
					var no_contrato  = rec.get('CG_NO_CONTRATO');
					var nombre_epo   = rec.get('NOMBRE_EPO');
					forma.query('#contrato_id')[0].setValue('<b>No. Contrato: ' + no_contrato + '</b>');
					forma.query('#epo_id')[0].setValue('<b>Epo: ' + nombre_epo + '</b>');
					consultaEmpresasData_show.load({
						params: {
							icGrupoCesion: ic_grupo,
							operacion:     'generar',
							start:         0,
							limit:         15
						}
					});
					mostrarConsultaEmpresas();
				}
			}]
		}],
		bbar: Ext.create('Ext.PagingToolbar',{
			store:       consultaGruposData,
			displayMsg:  '{0} - {1} de {2}',
			emptyMsg:    'No se encontraron registros',
			displayInfo: true
		}),
		tbar: [{ 
			xtype:       'button',
			text:        'Editar Grupo',
			itemId:      'btnEditarGrupo',
			iconCls:     'icon-register-edit',
			tooltip:     'Edita el grupo seleccionado.',
			handler:      editarGrupo
		},'-',{ 
			xtype:       'button',
			text:        'Agregar Grupo',
			itemId:      'btnAgregarGrupo',
			iconCls:     'icon-register-add',
			tooltip:     'Agrega un nuevo grupo.',
			handler:     agregarGrupo
		},'-',{ 
			xtype:       'button',
			text:        'Eliminar Grupo',
			itemId:      'btnEliminarGrupo',
			iconCls:     'borrar',
			tooltip:     'Elimina el grupo seleccionado.',
			handler:     eliminarGrupo
		},'-']
	});

	// Se crea el grid 'Empresas'
	var gridEmpresas = Ext.create('Ext.grid.Panel',{
		width:          650,
		height:         300,
		store:           Ext.data.StoreManager.lookup('consultaEmpresasData'),
		itemId:         'gridEmpresas',
		style:          'margin:0 auto;',
		bodyStyle:      'padding: 6px',
		frame:          false,
		autoScroll:     true,
		hidden:         false,
		columns: [{
			width:       250,
			header:      '<div align="center"> Nombre Empresa </div>',
			dataIndex:   'NOMBRE_PYME',
			sortable:    true,
			resizable:   true
		},{
			width:       120,
			header:      '<div align="center"> Nafin Electr�nico </div>',
			dataIndex:   'NAFIN_ELECTRONICO',
			align:       'center',
			sortable:    true,
			resizable:   true
		},{
			width:       120,
			header:      '<div align="center"> RFC </div>',
			dataIndex:   'RFC',
			align:       'center',
			sortable:    true,
			resizable:   true
		},{
			width:       75,
			xtype:       'actioncolumn',
			text:        'Eliminar',
			align:       'center',
			sortable:    false,
			resizable:   false,
			items: [{
				iconCls:  'borrar',
				tooltip:  'Elimina la empresa seleccionada.',
				handler: function(grid, rowIndex, colIndex) {

				//Por seguridad confirmo que siempres existan por lo menos dos empresas.
					var grid = Ext.ComponentQuery.query('#gridEmpresas')[0];
					var store = grid.getStore();
					var principal = 0;
					var contador = 0;
					//El foreach es opcional, si no necesito validar la pyme principal solo debo saber cuantos registros son.
					store.each(function(record){
						if(record.data['PYME_PRINCIPAL'] == true){
							principal++;
						}
						contador++;
					});
					if(contador <= 2){
						Ext.MessageBox.alert('Mensaje', 'No deben existir menos de dos empresas en el grid.');
						return;
					}
					// Comfirmar eliminaci�n
					Ext.Msg.confirm('Confirmaci�n', '�Desea eliminar el registro seleccionado?',function(botonConf){
						if (botonConf == 'ok' || botonConf == 'yes'){
							consultaEmpresasData.removeAt(rowIndex);
						}
					});
				}
			}]
		},{
			width:       80,
			xtype:       'checkcolumn',
			text:        '<div align="center"> Principal </div>',
			dataIndex:   'PYME_PRINCIPAL',
			align:       'center',
			sortable:    false,
			resizable:   false,
			listeners:   {
				'checkchange': function(grid, rowindex, isChecked){
					// Valido que solo se haya seleccionado una empresa principal
					var grid = Ext.ComponentQuery.query('#gridEmpresas')[0];
					var store = grid.getStore();
					var principal = 0;
					store.each(function(record){
						if(record.data['PYME_PRINCIPAL'] == true){
							principal++;
						}
					});
					if(principal == 0){
						Ext.MessageBox.alert('Mensaje', 'Por favor elija a la empresa principal.');
					}
					if(principal > 1){
						Ext.MessageBox.alert('Mensaje', 'Debe seleccionar solo una empresa principal.');
					}
				}
			}
		}],
		tbar: [{ 
			xtype:       'button',
			text:        'Agregar Empresa',
			itemId:      'btnAgregarEmpresa',
			iconCls:     'icon-register-add',
			tooltip:     'Agrega una empresa nueva.',
			handler:     function(boton, evento){
				var forma = Ext.ComponentQuery.query('#formaAgregarGrupos')[0];
				var ic_grupo = forma.query('#textfieldIcGrupo')[0].getValue();
				var ic_epo   = forma.query('#cmb_epo_id')[0].getValue();
				if(ic_grupo == '' && (ic_epo == '' || ic_epo == null || ic_epo == 'null')){
					forma.query('#cmb_epo_id')[0].markInvalid('Este campo es obligatorio.');
					return;
				}
				muestraFormEmpresa();
			}
		}]
	});

	// Se crea el grid para consultar empresas, �ste se muestra dentro del form 'formaConsultarEmpresas'
	var gridConsultarEmpresas = Ext.create('Ext.grid.Panel',{
		width:          '95%',
		height:         300,
		store:          consultaEmpresasData_show,
		itemId:         'gridConsultarEmpresas',
		style:          'margin:0 auto;',
		bodyStyle:      'padding: 6px',
		frame:          false,
		autoScroll:     true,
		hidden:         false,
		columns: [
			{width: 250, header: '<div align="center"> Nombre Empresa </div>',    dataIndex: 'NOMBRE_PYME',       renderer: pymePrincipal},
			{width: 115, header: '<div align="center"> Nafin Electr�nico </div>', dataIndex: 'NAFIN_ELECTRONICO', align: 'center'        },
			{width: 115, header: '<div align="center"> RFC </div>',               dataIndex:   'RFC',             align: 'center'        }
		],
		bbar: Ext.create('Ext.PagingToolbar',{
			store:       consultaEmpresasData_show,
			displayMsg:  '{0} - {1} de {2}',
			emptyMsg:    'No se encontraron registros',
			displayInfo: true
		})
	});

	// Se crea el form para agregar o editar grupos
	var formaAgregarGrupos = Ext.create( 'Ext.form.Panel',{
		width:             800,
		itemId:            'formaAgregarGrupos',
		title:             'Agregar Grupo',
		bodyPadding:       '12 6 12 6',
		style:             'margin: 0px auto 0px auto;',
		frame:             true,
		border:            false,
		hidden:            true,
		fieldDefaults: {
			msgTarget:      'side',
			labelWidth:     110
		},
		items:[{
			anchor:         '95%',
			xtype:          'combobox', 
			itemId:         'cmb_epo_id',
			name:           'cmb_epo',
			fieldLabel:     'Cadena Productiva',
			emptyText:      'Seleccione ...',
			displayField:   'descripcion',
			valueField:     'clave',
			queryMode:      'local',
			triggerAction:  'all',
			listClass:      'x-combo-list-small',
			typeAhead:      true,
			selectOnTab:    true,
			lazyRender:     true,
			forceSelection: true,
			hidden:         false,
			editable:       true,
			store:          catalogoEPO,
			listeners: {
				select: function(combo, record, index){
					// Se limpian los datos cada vez que se selecciona una epo diferente
					Ext.ComponentQuery.query('#formaAgregarGrupos')[0].query('#no_contrato_id')[0].setValue('');
					var grid = Ext.ComponentQuery.query('#gridEmpresas')[0];
					grid.getStore().removeAll();
				}
			}
		},{
			anchor:         '95%',
			xtype:          'textfield', 
			itemId:         'txt_epo_id',
			name:           'txt_epo',
			fieldLabel:     'Cadena Productiva',
			disabled:       true,
			hidden:         false
		},{
			anchor:         '95%',
			xtype:          'textfield', 
			itemId:         'no_contrato_id',
			id:             '_contrato_id',
			name:           'no_contrato',
			fieldLabel:     getIconoAyuda()+'No. Cotrato',
			maxLength:      20,
			allowBlank:     true,
			listeners: {
				focus: function(d){
					Ext.ComponentQuery.query('#formaAgregarGrupos')[0].query('#textfieldNoContrato')[0].setValue(d.getValue());					
				},
				blur: function(d){
					if(Ext.ComponentQuery.query('#formaAgregarGrupos')[0].query('#textfieldNoContrato')[0].getValue() != d.getValue()){
						validaContratosRepetidos(d.getValue());
					}
				}
			}
		},{
			anchor:         '95%',
			xtype:          'textfield',
			itemId:         'textfieldNoContrato',
			hidden:         true
		},{
			anchor:         '95%',
			xtype:          'textfield',
			itemId:         'textfieldIcGrupo',
			hidden:         true
		},{
			anchor:         '95%',
			xtype:          'textfield',
			itemId:         'textfieldIcEpo',
			hidden:         true
		},{
			anchor:         '95%',
			xtype:          'textfield',
			itemId:	       'textfieldFila',
			hidden:         true
		},{
			anchor:         '95%',
			xtype:          'displayfield',
			value:	       '&nbsp;'
		},gridEmpresas],
		buttons: [{
			text:           'Guardar',
			itemId:         'btnGuardar',
			iconCls:        'icoGuardar',
			handler:        actualizaGrupo
		},{
			text:           'Cancelar',
			itemId:         'btnCancelar',
			iconCls:        'icoCancelar',
			handler:        cancelaEdicionGrupo
		}]
	});

	// Se crea el form para consultar empresas
	var formaConsultarEmpresas = Ext.create( 'Ext.form.Panel',{
		width:             '100%',
		itemId:            'formaConsultarEmpresas',
		bodyPadding:       '12 6 12 6',
		style:             'margin: 0px auto 0px auto;',
		frame:             true,
		border:            false,
		hidden:            false,
		items:[
			{anchor: '95%', xtype: 'displayfield', itemId: 'contrato_id'},
			{anchor: '95%', xtype: 'displayfield', itemId: 'epo_id'		},
			gridConsultarEmpresas
		],
		buttons: [{
			text:           'Salir',
			iconCls:        'icoLimpiar',
			handler:        cierraFormConsultaEmpresas
		}]
	});

	// Se crea el form Agregar Empresa
	var formaAgregarEmpresa = Ext.create( 'Ext.form.Panel',{
		width:             '100%',
		itemId:            'formaAgregarEmpresa',
		bodyPadding:       '12 6 12 6',
		style:             'margin: 0px auto 0px auto;',
		frame:             true,
		border:            false,
		hidden:            false,
		items:[{
			anchor:         '95%',
			xtype:          'textfield',
			fieldLabel:     'Nombre',
			itemId:         'razon_social_id',
			msgTarget:      'side',
			maxLength:      200
		},{
			anchor:         '95%',
			xtype:          'textfield',
			fieldLabel:     'RFC',
			itemId:         'rfc_id',
			msgTarget:      'side',
			maxLength:      20
		},{
			anchor:         '95%',
			xtype:          'textfield',
			fieldLabel:     'N�mero de Nafin Electr�nico',
			itemId:         'nafin_electronico_id',
			msgTarget:      'side',
			regex:          /^[0-9]*$/,
			maxLength:      30
		},{
			xtype:          'component',
			html:           '<div align="center"> Utilice el * para realizar una b�squeda gen�rica. </div>',
			style:          'margin-bottom: 10px;'
		},{
			xtype:          'container',
			style:          'text-align:center',
			items: [{
				xtype:       'button',
				text:        'Buscar',
				iconCls:     'icoBuscar',
				handler: function(boton, evento){
				
					var formaEmpresa = this.up('form').getForm();
					if(!formaEmpresa.isValid()){
						return;
					}
					//Si el campo 'textfieldIcGrupo' est� vac�o, entonces la empresa pertenece a un grupo nuevo, el ic_epo se obtiene del combo.
					//En caso contrario, el grupo ya existe y la epo se trae del grid 'Grupo de Cesi�n de Derechos'
					var forma = Ext.ComponentQuery.query('#formaAgregarGrupos')[0];
					var ic_grupo = forma.query('#textfieldIcGrupo')[0].getValue();
					var icEpo = '';
					if(ic_grupo == ''){
						icEpo = forma.query('#cmb_epo_id')[0].getValue();
					} else{
						icEpo = forma.query('#textfieldIcEpo')[0].getValue();
					}

					var formaAgregarEmpresa = Ext.ComponentQuery.query('#formaAgregarEmpresa')[0];

					catalogoEmpresa.load({
						params: Ext.apply({
							informacion:      'Catalogo_Empresa',
							icEpo:            icEpo,
							razonSocial:      formaAgregarEmpresa.query('#razon_social_id')[0].getValue(),
							rfc:              formaAgregarEmpresa.query('#rfc_id')[0].getValue(),
							nafinElectronico: formaAgregarEmpresa.query('#nafin_electronico_id')[0].getValue()
						})
					});
				}
			}]
		},{
			xtype:          'displayfield',
			value:          '&nbsp;'
		},{
			anchor:         '95%',
			xtype:          'combobox', 
			itemId:         'pyme_id',
			fieldLabel:     'Nombre',
			emptyText:      'Seleccione ...',
			displayField:   'descripcion',
			valueField:     'clave',
			queryMode:      'local',
			msgTarget:      'side',
			triggerAction:  'all',
			listClass:      'x-combo-list-small',
			typeAhead:      true,
			selectOnTab:    true,
			lazyRender:     true,
			forceSelection: true,
			editable:       true,
			allowBlank:     true,
			disabled:       true,
			store:          catalogoEmpresa,
			listeners: {
				select: function(combo, records, eOpts){
					if(combo.getValue() != ''){
						Ext.Ajax.request({
							url: '34gruposCesionDerechosExt.data.jsp',
							params: {
								informacion: 'Consulta_Datos_Pyme',
								icPyme: combo.getValue()
							},
							callback: procesarConsultaDatosPyme
						});
					}
				}
			}
		},{
			xtype:          'fieldset',
			title:          'Representante Legal',
			defaultType:    'displayfield',
			layout:         'anchor',
			defaults: {
				anchor:      '100%'
			},
			items :[{
				itemId:      'representante_legal_id',
				value:       '&nbsp;'
			}]
		},{
			xtype:          'fieldset',
			title:          'Contacto',
			defaultType:    'displayfield',
			layout:         'anchor',
			defaults: {
				anchor:      '100%'
			},
			items :[{
				itemId:      'contacto_id',
				value:       '&nbsp;'
			}]
		},{
			xtype:          'textfield',
			itemId:         'hid_ic_pyme_id',
			hidden:         true
		},{
			xtype:          'textfield',
			itemId:         'hid_nombre_pyme_id',
			hidden:         true
		},{
			xtype:          'textfield',
			itemId:         'hid_rfc_pyme_id',
			hidden:         true
		},{
			xtype:          'textfield',
			itemId:         'hid_ne_pyme_id',
			hidden:         true
		}],
		buttons: [{
			text:           'Aceptar',
			iconCls:        'icoAceptar',
			itemId:         'btn_aceptar_win',
			disabled:       true,
			handler: function(){
				//Se valida que el representante legal y el contacto sean los mismos
				var forma = Ext.ComponentQuery.query('#formaAgregarEmpresa')[0];
				var rep_legal = forma.query('#representante_legal_id')[0].getValue();
				var contacto = forma.query('#contacto_id')[0].getValue();
				if(forma.query('#pyme_id')[0].getValue() == '' || forma.query('#pyme_id')[0].getValue() == null){
					forma.query('#pyme_id')[0].markInvalid('El campo es obligatorio');
					return;
				}
				if(rep_legal != contacto){
					Ext.MessageBox.alert('Error', 'El Representante Legal y el Contacto no son los mismos, favor de revisar.');
					return;
				}
				//Verifico que la pyme no se repita en el grid empresas
				var grid = Ext.ComponentQuery.query('#gridEmpresas')[0];
				var store = grid.getStore();
				var existeDuplicado = 0;
				store.each(function(record){
					if(record.data['IC_PYME'] == forma.query('#hid_ic_pyme_id')[0].getValue()){
						existeDuplicado++;
					}
				});
				if(existeDuplicado > 0){
					Ext.MessageBox.alert('Mensaje', 'La empresa seleccionada ya se encuentra agregada al grupo.');
					return;
				}
				//Se crea el nuevo regitro del grid Empresas
				var rec = Ext.create('ListaEmpresas',{
					IC_GRUPO_CESION:  '',
					IC_PYME:          forma.query('#hid_ic_pyme_id')[0].getValue(),
					NOMBRE_PYME:      forma.query('#hid_nombre_pyme_id')[0].getValue(),
					NAFIN_ELECTRONICO:forma.query('#hid_ne_pyme_id')[0].getValue(),
					RFC:              forma.query('#hid_rfc_pyme_id')[0].getValue(),
					PYME_PRINCIPAL:   false
				});
				Ext.data.StoreManager.lookup('consultaEmpresasData').insert(0, rec);

				var formaAgregarEmpresa = Ext.ComponentQuery.query('#formaAgregarEmpresa')[0];
				formaAgregarEmpresa.query('#pyme_id')[0].disable();
				formaAgregarEmpresa.query('#btn_aceptar_win')[0].disable();
				this.up('form').getForm().reset();
				this.up('window').hide();
			}
		},{
			text:           'Cancelar',
			iconCls:        'icoCancelar',
			handler: function(){
				var formaAgregarEmpresa = Ext.ComponentQuery.query('#formaAgregarEmpresa')[0];
				formaAgregarEmpresa.query('#pyme_id')[0].disable();
				formaAgregarEmpresa.query('#btn_aceptar_win')[0].disable();
				this.up('form').getForm().reset();
				this.up('window').hide();
			}
		}]
	});

	// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:    949,
		id:       'contenedorPrincipal',
		renderTo: 'areaContenido',
		style:    'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			gridGrupos,
			formaAgregarGrupos
		]
	});

	// Valores iniciales
	consultaGruposData.load({
		params: {
			informacion: 'Consulta_Grid_Grupos',
			operacion:   'generar',
			start:       0,
			limit:       15
		}
	});

});