<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.text.*,
	java.util.*,
	java.util.ArrayList,
	java.io.File,
	org.apache.commons.logging.Log,
	org.apache.commons.beanutils.PropertyUtils,
	netropology.utilerias.usuarios.UtilUsr,
	netropology.utilerias.usuarios.Usuario,
	com.netro.pdf.*,
	com.netro.exception.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,
	com.netro.distribuidores.*,
	com.netro.cesion.*,
	com.netro.anticipos.*,
	net.sf.json.JSONArray,
	com.netro.exception.NafinException,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/34cesion/34secsession_ext.jspf" %>
	<%!
		private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	%>
<% 
	String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
	String tipo_archivo = request.getParameter("tipo") == null?"":(String)request.getParameter("tipo");
	String clave_solicitud = request.getParameter("clave_solicitud") == null?"":(String)request.getParameter("clave_solicitud");
	String claveEPO = request.getParameter("claveEPO") == null?"":(String)request.getParameter("claveEPO");
	String claveIf = request.getParameter("claveIf") == null?"":(String)request.getParameter("claveIf");
	String clavePyme = request.getParameter("clavePyme") == null?"":(String)request.getParameter("clavePyme");
	String noContrato = request.getParameter("numeroContrato") == null?"":(String)request.getParameter("numeroContrato");
	String claveMoneda = request.getParameter("claveMoneda") == null?"":(String)request.getParameter("claveMoneda");
	String claveEstatus = request.getParameter("claveEstatusSol") == null?"":(String)request.getParameter("claveEstatusSol");
	String claveContratacion = request.getParameter("claveContrat") == null?"":(String)request.getParameter("claveContrat");
	String vigenciaContrato = request.getParameter("fechaVigenciaIni") == null?"": (String)request.getParameter("fechaVigenciaIni");
	String vigenciaContrato_a = request.getParameter("fechaVigenciaFin") == null?"": request.getParameter("fechaVigenciaFin");
	String plazo = request.getParameter("plazoContrato") == null?"":(String)request.getParameter("plazoContrato");
	String clavePlazo = request.getParameter("claveTipoPlazo") == null ?"":(String) request.getParameter("claveTipoPlazo");
	String operacion = request.getParameter("operacion") == null?"": request.getParameter("operacion");
	String motivosCancelacion = request.getParameter("motivosCancelacion") == null?"":(String)request.getParameter("motivosCancelacion");

	String infoRegresar ="", consulta="", usuario="";
	CesionEJB BeanCesionEJB = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	HashMap datos = new HashMap();
	JSONArray registros1 = new JSONArray();
	JSONObject 	jsonObj	= new JSONObject();
	JSONObject 	jsonObj1	= new JSONObject();
	String clasificacionEpos = BeanCesionEJB.clasificacionEpo(claveEPO);
	Vector lovDatosA = BeanCesionEJB.getCamposAdicionales(claveEPO, "5");
	int indiceCamposAdicionales = lovDatosA.size();
	String campo_adicional_1="", campo_adicional_2 ="",campo_adicional_3 ="",campo_adicional_4 ="", campo_adicional_5 ="";	
	if(indiceCamposAdicionales>0){
		for(int i = 0; i < indiceCamposAdicionales; i++){
			List campos =(List)lovDatosA.get(i);
			if(i==0) 	campo_adicional_1= campos.get(1).toString();
			if(i==1) 	campo_adicional_2= campos.get(1).toString();
			if(i==2) 	campo_adicional_3= campos.get(1).toString();
			if(i==3) 	campo_adicional_4= campos.get(1).toString();
			if(i==4) 	campo_adicional_5= campos.get(1).toString();			
		}
	}
	
	if (informacion.equals("catEPO") ) {
		
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setCesionDerechos("S");
		infoRegresar = catalogo.getJSONElementos();
	}else  if (informacion.equals("catCesionario") ) {
		
		CatalogoIF catalogo = new CatalogoIF();
		catalogo.setClave("ic_if");
		catalogo.setDescripcion("cg_razon_social");
		catalogo.setG_cs_cesion_derechos("S");
		infoRegresar = catalogo.getJSONElementos();
	}else  if (informacion.equals("catPyme") ) {
	
		CatalogoPYME catalogo = new CatalogoPYME();
		catalogo.setCampoClave("cp.ic_pyme");
		catalogo.setCampoDescripcion("cp.cg_razon_social");
		catalogo.setCampoLlave("cs.ic_estatus_solic");
		catalogo.setValoresCondicionIn("11,12,15,16,17", Integer.class); 
		catalogo.setOrden("cp.cg_razon_social");
		catalogo.setCesionDerechos("S");
		infoRegresar = catalogo.getJSONElementos();
	}else  if (informacion.equals("Moneda") ) {
		
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("0,1,54,25", Integer.class);
		infoRegresar = catalogo.getJSONElementos();
	}else  if (informacion.equals("Estatus") ) {
		
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus_solic");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("cdercat_estatus_solic");
		catalogo.setOrden("cg_descripcion");
		catalogo.setValoresCondicionIn("13,1,2,3,5,6,8,24", Integer.class);
		infoRegresar = catalogo.getJSONElementos();
	}else  if (informacion.equals("TipoContratacion") ) {
		String clave_producto = "9";
		HashMap hmCampos = new HashMap();
		if(!claveEPO.equals("")){
			List catalogoTipoContratacion = BeanCesionEJB.obtenerComboTipoContratacion(claveEPO);
			for(int i=0;i<catalogoTipoContratacion.size();i++){
			registros1.add(catalogoTipoContratacion.get(i));	
			}
			consulta =  "{\"success\": true, \"total\": \"" + registros1.size() + "\", \"registros\": " + registros1.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);
			infoRegresar = jsonObj.toString(); 
		}
		
	}else  if (informacion.equals("Generar") ||informacion.equals("GenerarArchivoCSV")||informacion.equals("ArchivoPDF") ) {
		
		CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
		String limite_pyme  	= (request.getParameter("limite")==null)?"":request.getParameter("limite");
		String tipoConsulta 	= (request.getParameter("limites")==null)?"":request.getParameter("limites");
		SoliConsenti paginador = new SoliConsenti();
		int start = 0;
		int limit = 0;
		Registros registrosC =null;
		paginador.setClaveEpo(claveEPO);
		paginador.setClaveIf(claveIf);
		paginador.setClavePyme(clavePyme);
		paginador.setNumeroContrato(noContrato);
		paginador.setClaveMoneda(claveMoneda);
		paginador.setClaveEstatusSol(claveEstatus);
		paginador.setClaveContrat(claveContratacion);
		paginador.setFechaVigenciaIni(vigenciaContrato);
		paginador.setFechaVigenciaFin(vigenciaContrato_a);
		paginador.setPlazoContrato(plazo);
		paginador.setClaveTipoPlazo(clavePlazo);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		if(informacion.equals("Generar")){
			JSONObject 	resultado	= new JSONObject();
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			try{
				if(operacion.equals("Generar")){
					queryHelper.executePKQuery(request);
				}
				Registros registros = queryHelper.getPageResultSet(request,start,limit);
				String nombreClasificacionEpo = cesionBean.clasificacionEpo(claveEPO);
				Vector lovDatos = cesionBean.getCamposAdicionales(claveEPO, "5");
				while(registros.next()){
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(registros.getString("clave_solicitud"));
					registros.setObject("MONE",montosMonedaSol.toString());
					String noProveedor = registros.getString("clave_pyme");
					String pyme=registros.getString("PYME");
					String empresas=registros.getString("EMPRESAS");
						registros.setObject("PYME",pyme+((empresas!="")?"<br>"+empresas.replaceAll(";","<br>"):""));	
					
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for (int y = 0; y < usuariosPorPerfil.size(); y++) {
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(y>0){
							nombreContacto.append(" / ");
						}
						registros.setObject("representante",usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno());	
					}
					int j = 1;
				}
				String consulta2	=	"{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";
				resultado = JSONObject.fromObject(consulta2);
				resultado.put("clasificacionEpos", clasificacionEpos);	
				resultado.put("indiceCamposAdicionales",String.valueOf(indiceCamposAdicionales));	 //numero de campos Adicionales
				resultado.put("CAMPO_ADICIONAL_1",campo_adicional_1);
				resultado.put("CAMPO_ADICIONAL_2",campo_adicional_2);
				resultado.put("CAMPO_ADICIONAL_3",campo_adicional_3);
				resultado.put("CAMPO_ADICIONAL_4",campo_adicional_4);
				resultado.put("CAMPO_ADICIONAL_5",campo_adicional_5);
				infoRegresar = resultado.toString();
			}catch(Exception e){
				throw new AppException("Error en la paginación",e);
			}
		}else if(informacion.equals("GenerarArchivoCSV")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}else if (informacion.equals("ArchivoPDF")	){
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			try {
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp, "PDF");
				JSONObject jsonObj3 = new JSONObject();
				jsonObj3.put("success", new Boolean(true));
				jsonObj3.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj3.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
	}else  if (informacion.equals("Plazo") ) {
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_tipo_plazo");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("cdercat_tipo_plazo");
		catalogo.setOrden("ic_tipo_plazo");	
		infoRegresar = catalogo.getJSONElementos();
	}else if(informacion.equals("contratoPdf")){
		log.info("Descarga Archivo (E)");
		String contentType = "application/pdf";
		JSONObject jsonObj2 = new JSONObject();
		try{
			if(tipo_archivo.equals("PDF")){
				String file = BeanCesionEJB.descargaContratoCesion(clave_solicitud, strDirectorioTemp);
				jsonObj2.put("success", new Boolean(true));
				jsonObj2.put("urlArchivo",strDirecVirtualTemp+file);
			}
			infoRegresar = jsonObj2.toString();
		} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
		}
	
	}else if(informacion.equals("CancelacionNafin")){
	
		jsonObj = new JSONObject();
		
		boolean  cancela =   BeanCesionEJB.setCancelaNafin(clave_solicitud, motivosCancelacion); 
		
		jsonObj.put("success", new Boolean(cancela));
		infoRegresar = jsonObj.toString();
	
	}
%>
<%=infoRegresar%>

