<%@ page import="java.sql.*, java.text.*, netropology.utilerias.*"%>
<%
//El siguiente c�digo debe ser lo primero en ejecutarse, el cual determina si la sesi�n est� activa.
//De lo contrario en lugar de mostrar "Su sesi�n ha expirado..." lanzar� un NullPointer Exception
String gsCveUsuario = (String) session.getAttribute("sesCveUsuario");
if(gsCveUsuario == null){
%>
		<script language="JavaScript">
			alert("Su sesión ha expirado, por favor vuelva a entrar.");
			top.location="/nafin/15cadenas/15salir.jsp"
		</script>
<%
	return;
}


try {
	String strTipoUsuario = (String)session.getAttribute("strTipoUsuario"); // POSIBLES VALORES NAFIN-EPO-PYME-IF
	String strLogin = (String)session.getAttribute("Clave_usuario");//LOGIN DEL USUARIO QUE SE LOGUEA
	String strSerial = (String)session.getAttribute("strSerial"); //VARIABLE QUE CONTIENE EL NUMERO DE SERIE DEL CERTIFICADO
	
	String gsParSistema = request.getParameter("parSistema");


	//String  gsIdioma = "";
	//gsIdioma= (String)session.getAttribute("sesIdiomaUsuario");

	if ("EPO".equals(strTipoUsuario)) {
		if(strSerial.equals("")) {
			%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
			<%
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
		}
	} else if ("PYME".equals(strTipoUsuario)) {

		if(strSerial.equals("")) {
			%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
			<%
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
		}
	} else if ("IF".equals(strTipoUsuario)) {
		if(strSerial.equals("")) {
			%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
			<%
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
		}
	} else if("NAFIN".equals(strTipoUsuario)) {
		if(strSerial.equals("")) {
			%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
			<%
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
		}
	}
} finally {

}
%>