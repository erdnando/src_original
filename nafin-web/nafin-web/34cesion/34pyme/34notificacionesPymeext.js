Ext.onReady(function(){
//---------------------------------FUNCIONES------------------------------------
	var jsonValoresIniciales = null;
	function procesaValoresIniciales(opts, success, response) {
		fp.el.mask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
				if(jsonValoresIniciales != null){
					fp.el.unmask();
				}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
//----------------------------------HANDLERS------------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();   
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var el = grid.getGridEl();
			
			if(store.getTotalCount()>0){
				btnGenerarPDF.enable();
				btnBajarPDF.hide();
				btnBajarArchivo.hide();
				if(!btnBajarArchivo.isVisible()){
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}
				el.unmask();
			}else{
				el.mask('No se encontró ningún registro','x-mask');
			}
		}
	}
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
			//Muestra el archivo de la Columna Contrato de Cesion 
	var procesarSuccessFailureContratoCesion =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '../34pki/34pyme/34ContratoCesionPDFCSV.jsp',
			params: Ext.apply({
				informacion: 'CONTRATO_CESION_PYME',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}
	
	var mostrarContrato = function (grid,rowIndex,colIndex,item){
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		var strDirectorioTemp = jsonValoresIniciales.strDirectorioTemp;
		document.forms[0].action = "34NotificacionesPymeDescargaArchivo.do?tipo_archivo=DESCARGAR_PDF&strDirectorioTemp="+strDirectorioTemp+"&clave_solicitud="+clave_solicitud;
		document.forms[0].submit();
	}
	var mostrarContratoCesion = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		document.forms[0].action = "/nafin/34cesion/34pki/34pyme/34ContratoCesionPymeAcusePDF.do?tipo_archivo=CONTRATO_CESION_PYME&clave_solicitud="+clave_solicitud+"&acuse_carga=";
		document.forms[0].submit();
	}
//-----------------------------------STORE--------------------------------------
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '34notificacionesPymeext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'DEPENDENCIA'},
					{name: 'CESIONARIO'},
					{name: 'NUMERO_CONTRATO'},
					{name: 'MONTO_MONEDA'},
					{name: 'TIPO_CONTRATACION'},
					{name: 'INICIO_CONTRATO'},
					{name: 'FIN_CONTRATO'},
					{name: 'PLAZO'},
					{name: 'DESC_PLAZO'},
					{name: 'CLASIFICACION_EPO'},
					{name: 'OBJETO_CONTRATO'},
					{name: 'COMENTARIOS'},
					{name: 'CUENTA'},
					{name: 'CUENTA_CLABE'},
					{name: 'CLAVE_SOLICITUD'},
					{name: 'ESTATUS'},
					{name: 'OBSERV_RECHAZO'},
					{name: 'FIRMA_CONTRATO'}
					
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
				load: procesarConsultaData,
				exception: {
							fn: function(proxy,type,action,optionRequest,response,args){
									NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
									procesarConsultaData(null,null,null);//Llama procesar consulta, para que desbloquee los componentes.
							}
				}
		}
	});

//-------------------------------- STORES -----------------------------------

	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCambioEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoTipoContratacionData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoCotratacion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoPlazoContratoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesPymeext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
//-------------------------------------------------------------------

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'cmbEpo',
			fieldLabel: 'Nombre de la Epo',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Seleccione una EPO',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars: 1,
			allowBlank: false,
			store: catalogoEPOData,
			tpl: NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo){
						var cmbSolic = Ext.getCmp('cmbSolic');
						cmbSolic.setValue('');
						cmbSolic.setDisabled(false);
						cmbSolic.store.load({
							params: {
								ic_epo: combo.getValue()
							}
						});
					}
				}
			}
		},
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'cmbClaveIF',
			mode: 'local',
			displayField: 'descripcion',
			emptyText: 'Seleccione un IF',
			valueField: 'clave',
			hiddenName : 'ic_if',
			fieldLabel: 'Nombre de Cesionario (IF)',
			disabled: false,
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIFData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'textfield',
			name: 'numeroContrato',
			id: 'numContrato',
			hiddenName : 'numContrato',
			fieldLabel: 'Numero de Contrato',
			maxLength: 15
		},
		{
			xtype: 'combo',
			name: 'ic_moneda',
			id: 'cmbMoneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccione una moneda...',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_moneda',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'clave_estatus',
			id: 'cmbEstatus',
			fieldLabel: 'Estatus',
			emptyText: 'Todos los estatus...',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'clave_estatus',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEstatusData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'clave_contrat',
			id: 'cmbSolic',
			mode: 'local',
			emptyText: 'Todos los tipos de solicitudes...',
			valueField: 'clave',
			displayField: 'descripcion',
			hiddenName : 'clave_contrat',
			fieldLabel: 'Tipo de Contratación',
			disabled: false,
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoTipoContratacionData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype:'compositefield',
			fieldLabel: 'Fecha vigencia del Contrato',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '90%',
			items: [
						{
							xtype: 'datefield',
							name: 'df_fecha_vigencia_de',
							id: 'dc_fecha_vigenciaMin',
							allowBlank: true,
							startDay: 0,
							width: 100,
							msgTarget: 'side',
							vtype: 'rangofecha',
							campoFinFecha:'dc_fecha_vigenciaMax',
							margins: '0 20 0 0' //necesario para mostrar el icono de error
						},
						{
							xtype: 'displayfield',
							value: 'al',
							width: 24
						},
						{
							xtype: 'datefield',
							name: 'df_fecha_vigencia_a',
							id: 'dc_fecha_vigenciaMax',
							allowBlank: true,
							startDay: 1,
							width: 100,
							msgTarget: 'side',
							vtype: 'rangofecha',
							campoInicioFecha: 'dc_fecha_vigenciaMin',
							margins: '0 20 0 0'//necesario para mostrar el icono de error
						}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo del Contrato',
			id: 'plazoContrato',
			items : [
				{
					xtype: 'textfield',
					name: 'plazo_Contrato',
					id: 'plazoCont',
					hiddenName : 'plazo_Contrato',
					maxLength:10
				},
				{
					xtype: 'combo',
					name: 'clave_plazo_contrato',
					id: 'cmbPlazoCont',
					mode: 'local',
					emptyText: 'Seleccionar...',
					valueField: 'clave',
					displayField: 'descripcion',
					hiddenName : 'clave_plazo_contrato',
					fieldLabel: 'Plazo del Contrato',
					disabled: false,
					forceSelection : false,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoPlazoContratoData,
					tpl : NE.util.templateMensajeCargaCombo
				}				
			]
		}
	];

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'grid',
		columns: [
						{
							header: 'Dependencia',
							tooltip: 'Dependencia',
							dataIndex: 'DEPENDENCIA',
							align: 'center',
							resiazable: true,
							width: 250
						},
						{
							header: 'Intermediario Financiero (Cesionario)',
							tooltip: 'Intermediario Financiero (Cesionario)',
							dataIndex: 'CESIONARIO',
							align: 'center',
							resiazable: true,
							width: 250
						},
						{
							header: 'No. Contrato',
							tooltip: 'No.Contrato',
							dataIndex: 'NUMERO_CONTRATO',
							align: 'center',
							resiazable: true,
							width:100
						},
						{
							header: 'Firma Contrato',
							tooltip: 'Firma Contrato',
							dataIndex: 'FIRMA_CONTRATO',
							sortable: true,
							resizable: true,
							width: 130,				
							align: 'center'
						},
						{
							header: 'Monto/Moneda',
							tooltip: 'Monto/Moneda',
							dataIndex: 'MONTO_MONEDA',
							align: 'left',
							resiazable: true,
							width: 150
						},
						{
							header: 'Tipo de Contratación',
							tooltip: 'Tipo de Contratación',
							dataIndex: 'TIPO_CONTRATACION',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'F. Inicio Contrato',
							tooltip: 'Fecha Inicio del Contrato',
							dataIndex: 'INICIO_CONTRATO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'F. Final Contrato',
							tooltip: 'Fecha Final del Contrato',
							dataIndex: 'FIN_CONTRATO',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Plazo del Contrato',
							tooltip: 'Plazo del Contrato',
							dataIndex: 'PLAZO',
							align: 'center',
							resiazable: true,
							width: 100,
							renderer: function (value,metaData,registro,rowIndex,colIndex,store){
											if(registro.get('PLAZO')!=''&&registro.get('DESC_PLAZO')){
												value = registro.get('PLAZO')+' '+registro.get('DESC_PLAZO');
											}
										return value;
							}
						},
						{
							header: 'Clasificación EPO',
							tooltip: 'Clasificación EPO',
							dataIndex: 'CLASIFICACION_EPO',
							align: 'center',
							resiazable: true,
							width: 250
						},
						{
							header: 'Objeto del Contrato',
							tooltip: 'Objeto del Contrato',
							dataIndex: 'OBJETO_CONTRATO',
							align: 'center',
							resiazable: true,
							width: 180
						},						
						{
							header: 'Comentarios',
							tooltip: 'Comentarios',
							dataIndex: 'COMENTARIOS',
							align: 'center',
							resiazable: true,
							width: 100
						},	
						{
							header: 'Cuenta',
							tooltip: 'Cuenta',
							dataIndex: 'CUENTA',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							header: 'Cuenta CLABE',
							tooltip: 'Cuenta CLABE',
							dataIndex: 'CUENTA_CLABE',
							align: 'center',
							resiazable: true,
							width: 120
						},
						{
							xtype: 'actioncolumn',
							header: 'Contrato',
							tooltip: 'Contrato',
							dataIndex: 'CLAVE_SOLICITUD',
							align: 'center',
							resiazable: true,
							width: 150,
							items: [
										{
											getClass: function (valor,metadata,registro,rowIndex,colIndex,store){
												if(registro.get('CLAVE_SOLICITUD')!=''){
												this.items[0].tooltip = 'Ver';
												return 'iconoLupa';
												}
											},
											handler: mostrarContrato
										}
							]
						},
						{
							xtype: 'actioncolumn',
							header: 'Contrato de Cesión',
							tooltip: 'Contrato de Cesión',
							dataIndex: 'CLAVE_SOLICITUD',
							align: 'center',
							resiazable: true,
							width: 120,
							items: [
										{
											getClass: function (valor,metadata,registro,rowIndex,colIndex,store){
												if(registro.get('CLAVE_SOLICITUD')!=''){
												this.items[0].tooltip = 'Ver';
												return 'iconoLupa';
												}
											},
											handler: descargarContratoCesion
										}
							]
						},
						{
							header: 'Estatus',
							tooltip: 'Estatus',
							dataIndex: 'ESTATUS',
							align: 'center',
							resiazable: true,
							width: 200
						},
						{
								xtype: 'actioncolumn',
								header: 'Causas de Rechazo Notificación',
								tooltip: 'Causas de Rechazo Notificación',
								dataIndex: 'OBSERV_RECHAZO',
								align: 'center',
								width: 120,
								renderer: function(value, metadata, registro, rowindex, colindex, store) {
									if(registro.get('OBSERV_RECHAZO')!='N/A'&&registro.get('OBSERV_RECHAZO').length>15){
									
										 return registro.get('OBSERV_RECHAZO').substring(0,14) ;
									}else{
										return registro.get('OBSERV_RECHAZO') ;
									}
								},
								items: [
											{
												getClass: function(value,metadata,registro,rowIndex,colIndex,store){
												
													if(registro.get('OBSERV_RECHAZO')!='N/A'&&registro.get('OBSERV_RECHAZO').length>15){
														this.items[0].tooltip = 'más';
														
														return 'mas';
													}else{
														return '';
													}
												},
												handler: function(grid,rowIndex,colIndex,item,event){
													
													var registro = grid.getStore().getAt(rowIndex);	
													Ext.Msg.alert('Causas de Rechazo',registro.get('OBSERV_RECHAZO'));
												}
											}
								]
							}
		],
		stripeRows: true,
		hidden: true,
		loadMask: true,
		height: 400,
		width: 940,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		bbar: {
				buttonAlign: 'left',
				displayInfo: true,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros.",
				items: [
							'-',
							{
								xtype:	'button',
								text:		'Generar Archivo',
								iconCls:	'icoXls',
								id: 'btnGenerarArchivo',
								handler: function(boton, evento) {
									boton.disable();
									boton.setIconClass('loading-indicator');
									var cmbEpo = Ext.getCmp('cmbEpo');
									var cmbClaveIF = Ext.getCmp('cmbClaveIF');
									var numContrato = Ext.getCmp('numContrato');
									var cmbMoneda = Ext.getCmp('cmbMoneda');
									var cmbEstatus = Ext.getCmp('cmbEstatus');
									var cmbSolic = Ext.getCmp('cmbSolic');
									var dc_fecha_vigenciaMin = Ext.getCmp('dc_fecha_vigenciaMin');
									var dc_fecha_vigenciaMax = Ext.getCmp('dc_fecha_vigenciaMax');
									var plazoCont = Ext.getCmp('plazoCont');
									var cmbPlazoCont = Ext.getCmp('cmbPlazoCont');
									Ext.Ajax.request({
										url: '34notificacionesPymeext.data.jsp',
										params:	Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV',
												ic_epo: cmbEpo.getValue(),
												ic_if: cmbClaveIF.getValue(),
												numeroContrato: numContrato.getValue(),
												ic_moneda: cmbMoneda.getValue(),
												clave_estatus: cmbEstatus.getValue(),
												clave_contrat: cmbSolic.getValue(),
												df_fecha_vigencia_de: dc_fecha_vigenciaMin.getValue(),
												df_fecha_vigencia_a: dc_fecha_vigenciaMax.getValue(),
												plazo_Contrato: plazoCont.getValue(),
												clave_plazo_contrato: cmbPlazoCont.getValue()
										}),
										callback: procesarSuccessFailureGenerarArchivo});
								}
							},
							{
								xtype: 'button',
								text: 'Bajar Archivo',
								id: 'btnBajarArchivo',
								hidden: true
							},
							'-',
							{
								xtype:	'button',
								text:		'Generar PDF',
								iconCls:	'icoPdf',
								id:		'btnGenerarPDF',
								handler: function(boton, evento) {
									boton.disable();
									boton.setIconClass('loading-indicator');
									var cmbEpo = Ext.getCmp('cmbEpo');
									var cmbClaveIF = Ext.getCmp('cmbClaveIF');
									var numContrato = Ext.getCmp('numContrato');
									var cmbMoneda = Ext.getCmp('cmbMoneda');
									var cmbEstatus = Ext.getCmp('cmbEstatus');
									var cmbSolic = Ext.getCmp('cmbSolic');
									var dc_fecha_vigenciaMin = Ext.getCmp('dc_fecha_vigenciaMin');
									var dc_fecha_vigenciaMax = Ext.getCmp('dc_fecha_vigenciaMax');
									var plazoCont = Ext.getCmp('plazoCont');
									var cmbPlazoCont = Ext.getCmp('cmbPlazoCont');
									Ext.Ajax.request({
											url: '34notificacionesPymeext.data.jsp',
											params:{
											informacion: 'ArchivoPDF',
											ic_epo: cmbEpo.getValue(),
											ic_if: cmbClaveIF.getValue(),
											numeroContrato: numContrato.getValue(),
											ic_moneda: cmbMoneda.getValue(),
											clave_estatus: cmbEstatus.getValue(),
											clave_contrat: cmbSolic.getValue(),
											df_fecha_vigencia_de: dc_fecha_vigenciaMin.getValue(),
											df_fecha_vigencia_a: dc_fecha_vigenciaMax.getValue(),
											plazo_Contrato: plazoCont.getValue(),
											clave_plazo_contrato: cmbPlazoCont.getValue()
											},
									callback: procesarSuccessFailureGenerarPDF
									});
								}
							},
							{
								xtype: 'button',
								text: 'Bajar PDF',
								id: 'btnBajarPDF',
								hidden: true
							},
							'-'
					]
			}	
	});

//-------------------------------- PRINCIPAL -----------------------------------

	var fp = new Ext.form.FormPanel({
		id :	'forma',
		width :	890,
		title :	'Notificaciones',
		frame :	true,
		collapsible : false,
		titleCollapse: false,
		style: 'margin:0 auto',
		bodyStyle : 'padding: 6px',
		labelWidth : 150,
		defaultType : 'textfield',
		defaults : {
			msgTarget :	'side',
			anchor : '-25'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					var dc_fecha_vigenciaMin = Ext.getCmp('dc_fecha_vigenciaMin');
					var dc_fecha_vigenciaMax = Ext.getCmp('dc_fecha_vigenciaMax');
					var cmbEpo = Ext.getCmp('cmbEpo');
					
					if(!Ext.isEmpty(dc_fecha_vigenciaMin.getValue())||!Ext.isEmpty(dc_fecha_vigenciaMax.getValue())){
						if(Ext.isEmpty(dc_fecha_vigenciaMin.getValue())){
							dc_fecha_vigenciaMin.markInvalid('Debe capturar ambas fechas de vigencia o dejarlas en blanco');
							dc_fecha_vigenciaMin.focus();
							return;
						}else if(Ext.isEmpty(dc_fecha_vigenciaMax.getValue())){
							dc_fecha_vigenciaMax.markInvalid('Debe capturar ambas fechas de vigencia o dejarlas en blanco');
							dc_fecha_vigenciaMax.focus();
							return;
						}
					}
					if(Ext.isEmpty(cmbEpo.getValue())){
						cmbEpo.markInvalid('El valor de la EPO es requerido');
						cmbEpo.focus();
						return;
					}
					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
						})
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fp.getForm().reset();
					grid.hide();
				}
			}
		]
	});

//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id :	'contenedorPrincipal',
		applyTo :'areaContenido',
		width : 949,
		height : 'auto',
		items :	[
		fp,
		NE.util.getEspaciador(20),
		grid
		]		
	});
	
	//Peticion para obtener valores iniciales y la parametrización
	Ext.Ajax.request({
		url: '34notificacionesPymeext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
//-------------------------------- ----------------- -----------------------------------
	catalogoEPOData.load();
	catalogoIFData.load();
	catalogoMonedaData.load();	
	catalogoEstatusData.load();	
	catalogoPlazoContratoData.load();
});