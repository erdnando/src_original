<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.model.catalogos.*,
		com.netro.cesion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%
CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "";
	if(informacion.equals("CatalogoEPO")){
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setClave("ic_epo");
		catalogo.setDescripcion("cg_razon_social");
		catalogo.setCs_cesion_derechos("S");
		infoRegresar = catalogo.getJSONElementos();
	}
	else if (informacion.equals("CatalogoIF")){
		CatalogoIF catalogo = new CatalogoIF();
		catalogo.setClave("ic_if");
		catalogo.setDescripcion("cg_razon_social");
		catalogo.setG_cs_cesion_derechos("S");
		infoRegresar = catalogo.getJSONElementos();
	}
	else if (informacion.equals("CatalogoMoneda")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("0,1,25,54", Integer.class);
		infoRegresar = catalogo.getJSONElementos();
	}
	else if (informacion.equals("CatalogoCambioEstatus")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_estatus_solic");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("cdercat_estatus_solic");
		catalogo.setOrden("cg_descripcion");
		catalogo.setValoresCondicionIn("11,12,15,16,17", Integer.class);
		infoRegresar = catalogo.getJSONElementos();
	}
	else if(informacion.equals("CatalogoTipoCotratacion")){
		String clave_epo = (request.getParameter("ic_epo"))!=null?request.getParameter("ic_epo"):"";
		List catalogoTipoContratacion = cesionBean.obtenerComboTipoContratacion(clave_epo);
		JSONArray jsonArray = JSONArray.fromObject(catalogoTipoContratacion);
		infoRegresar = 						
							"{\"success\": true, \"total\": \""+ catalogoTipoContratacion.size() +"\", \"registros\": " + jsonArray.toString()+"}";
	}
	else if (informacion.equals("CatalogoPlazo")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_tipo_plazo");
		catalogo.setCampoDescripcion("cg_descripcion");
		catalogo.setTabla("cdercat_tipo_plazo");
		catalogo.setOrden("ic_tipo_plazo");	
		infoRegresar = catalogo.getJSONElementos();
	}
	else if (informacion.equals("valoresIniciales")){
		JSONObject jsonObj= new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("strDirectorioTemp", strDirectorioTemp);
		infoRegresar = jsonObj.toString();
	}
	else if (informacion.equals("Consulta")||informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")){
		String clave_epo 		= (request.getParameter("ic_epo"))!=null?request.getParameter("ic_epo"):"";
		String clave_if		= (request.getParameter("ic_if"))!=null?request.getParameter("ic_if"):"";
		String num_contrato	= (request.getParameter("numeroContrato"))!=null?request.getParameter("numeroContrato"):"";
		String clave_moneda	= (request.getParameter("ic_moneda"))!=null?request.getParameter("ic_moneda"):"";
		String clave_estatus = (request.getParameter("clave_estatus"))!=null?request.getParameter("clave_estatus"):"";
		String clave_contrat	= (request.getParameter("clave_contrat"))!=null?request.getParameter("clave_contrat"):"";
		String desde			= (request.getParameter("df_fecha_vigencia_de"))!=null?request.getParameter("df_fecha_vigencia_de"):"";
		String hasta			= (request.getParameter("df_fecha_vigencia_a"))!=null?request.getParameter("df_fecha_vigencia_a"):"";
		String plazo_contrato = (request.getParameter("plazo_Contrato"))!=null?request.getParameter("plazo_Contrato"):"";//FODEA 016 - 2011 ACF
		String clave_plazo_contrato = (request.getParameter("clave_plazo_contrato"))!=null?request.getParameter("clave_plazo_contrato"):"";//FODEA 016 - 2011 ACF

		com.netro.cesion.ConsultaNotificacionesCesionDerechos paginador = new com.netro.cesion.ConsultaNotificacionesCesionDerechos();

		paginador.setClavePyme(iNoCliente);
		paginador.setClaveEPO(clave_epo);
		paginador.setClaveIF(clave_if);
		paginador.setClaveEstatus(clave_estatus);
		paginador.setClaveContrat(clave_contrat);
		paginador.setClaveMoneda(clave_moneda);
		paginador.setClavePlazoContrato(clave_plazo_contrato);
		paginador.setPlazoContrato(plazo_contrato);
		paginador.setNumeroContrato(num_contrato);
		paginador.setFechaVigenciaDesde(desde);
		paginador.setFechaVigenciaHasta(hasta);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if(informacion.equals("Consulta")){
			try{
				Registros reg = queryHelper.doSearch();
				
				while(reg.next()){
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(reg.getString("CLAVE_SOLICITUD"));
					reg.setObject("MONTO_MONEDA",montosMonedaSol.toString());
				}
				infoRegresar = 						
							"{\"success\": true, \"total\": \""+ reg.getNumeroRegistros() +"\", \"registros\": " + reg.getJSONData()+"}";
			}catch(Exception e){
				throw new AppException("Error en la obtención de los datos");
			}
		}	
		else if (informacion.equals("ArchivoCSV")) {
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
					throw new AppException("Error al generar el archivo CSV", e);
			}
		}
		else if (informacion.equals("ArchivoPDF")){
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF",e);
			}
		}
	}else if (informacion.equals("DESCARGAR_PDF")){
		JSONObject jsonObj= new JSONObject();
		String clave_solicitud = request.getParameter("clave_solicitud")==null?"":request.getParameter("clave_solicitud");
		String nombreArchivo = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
		
	}
%>
<%=infoRegresar%>