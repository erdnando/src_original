<%@ page language="java"%>
<%@ page import=" 
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,	
	com.netro.cesion.*,
	java.text.*, 
	netropology.utilerias.usuarios.UtilUsr,
	netropology.utilerias.usuarios.Usuario,
	com.netro.exception.NafinException,
netropology.utilerias.ServiceLocator, 
com.netro.exception.*,  
com.netro.model.catalogos.*, 
net.sf.json.JSONArray,
net.sf.json.JSONObject"
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="../34secsession_ext.jspf"%>
<%  
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String testigo	=	(request.getParameter("testigo")!=null)?request.getParameter("testigo"):"";


String infoRegresar 	= "", consulta ="", claveAfiliado  =iNoCliente , mensaje="";

JSONArray registros = new JSONArray();
HashMap info = new HashMap();
JSONObject 	jsonObj	= new JSONObject();

CesionEJB BeanCesionEJB = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

try {
	
	
if(informacion.equals("CatalogoTestigo") ){
	String tipoAfiliado 	= "I";	
	String perfil = "ADMIN TEST";
	List lUsuarios = BeanCesionEJB.getComboTestigos(tipoAfiliado, claveAfiliado, perfil, "");
	registros = registros.fromObject(lUsuarios);
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";

	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();

}else  if(informacion.equals("Consultar") ){    

	HashMap hmTestigos = BeanCesionEJB.getTestigos(claveAfiliado);
	int index = Integer.parseInt((String)hmTestigos.get("index"));  
	for (int i = 0; i <index; i++) {		
		HashMap hmRegistros = (HashMap)hmTestigos.get("hmRegistrosTestigos"+i+""); 
		testigo = hmRegistros.get("testigo")==null?"":hmRegistros.get("testigo").toString();
		info = new HashMap();
		info.put("TESTIGO", testigo);	
		registros.add(info);		
	}
	consulta =  "{\"success\": true, \"total\": \"" + index + "\", \"registros\": " + registros.toString()+"}";
	
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();

}else  if(informacion.equals("Guardar") ){

	List lguardaTestigos = BeanCesionEJB.guardaTestigos(claveAfiliado, testigo);
	
	if(lguardaTestigos.get(0).equals("Failure")){
		mensaje ="No se puede guardar el mismo testigo";
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensaje", mensaje);	
	infoRegresar=jsonObj.toString();			
	
}else  if(informacion.equals("Eliminar") ){

	List lborraTestigos = BeanCesionEJB.borraTestigos(claveAfiliado, testigo);
	jsonObj.put("success", new Boolean(true));
	infoRegresar=jsonObj.toString();	

}
}catch(Exception e){
			System.out.println("getNotasDeCredito(Exception)" +e);		
			e.printStackTrace();
			throw new Exception("Error al consultar datos de las Notas de Credito");
		}finally{			
			System.out.println("getNotasDeCredito(S)");
		}
			
%>
<%= infoRegresar%>
<%
System.out.println("infoRegresar >>>>>>>"+informacion  +">>>>>>>>>>"+	infoRegresar);  
%>