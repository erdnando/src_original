<%@ page language="java"%>
<%@ page import=" netropology.utilerias.ServiceLocator,  java.util.*, com.netro.cesion.*, com.netro.exception.*,  com.netro.model.catalogos.*,  net.sf.json.JSONArray,net.sf.json.JSONObject"
contentType="application/json;charset=UTF-8" errorPage="/00utils/error.jsp"%>
<%@ include file="../34secsession_ext.jspf"%>
<%  
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clausulado		=	(request.getParameter("clausulado")!=null)?request.getParameter("clausulado"):"";
String infoRegresar 	= "";

if (!clausulado.equals("")){
	CesionEJB BeanCesionEJB = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			
	List leyenda = BeanCesionEJB.guardaClausulado(clausulado, iNoCliente);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", leyenda);
	infoRegresar=jsonObj.toString();
}
%>
<%= infoRegresar%>
