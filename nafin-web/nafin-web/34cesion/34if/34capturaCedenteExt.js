Ext.onReady(function(){

	var fm = Ext.form;

//------------------------------ HANDLERS ------------------------------
	// Realiza la consulta para llenar el gridConsulta
	function buscar(){
		if(!Ext.getCmp('formaPrincipal').getForm().isValid()){
			return;
		}
		var fp = Ext.getCmp('formaPrincipal');
		fp.el.mask('Procesando...', 'x-mask-loading');
		consultaData.load({
			params: Ext.apply({
				informacion: 'Consultar',
				ic_epo:      Ext.getCmp('ic_epo_id').getValue(),
				no_contrato: Ext.getCmp('no_contrato_id').getValue()
			})
		});
	}

	// Limpia el form principal
	function cancelar(){
		Ext.getCmp('formaPrincipal').getForm().reset();
		Ext.getCmp('gridConsulta').hide();
	}

	// Realiza la consulta para llenar el form 'formaDatosConstitutivo'
	function consultaDatosConstitutivos(rec){
		pnl.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '34capturaCedenteExt.data.jsp',
			params: Ext.apply({
				informacion:  'Consulta_Datos_Constitutivo', 
				ic_pyme:      rec.get('IC_PYME'),
				ic_solicitud: rec.get('IC_SOLICITUD')
			}),
			callback: procesarConsultaDatosConstitutivo
		});
	}

	// Guarda los datos del form 'formaDatosConstitutivo'
	function guardarDatosConstitutivo(){

		if(!Ext.getCmp('formaDatosConstitutivo').getForm().isValid()){
			return;
		}
		pnl.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url: '34capturaCedenteExt.data.jsp',
			params: Ext.apply({
				informacion:               'Guarda_Datos_Constitutivo', 
				tipo_cedente:              Ext.getCmp('tipo_cedente_id').getValue(),
				figura_juridica:           Ext.getCmp('figura_juridica_id').getValue(),
				escritura_publica:         Ext.getCmp('escritura_publica_id').getValue(),
				fecha_escritura:           new Date(Ext.getCmp('fecha_escritura_id').getValue()).format('d/m/Y'),
				nombre_licenciado:         Ext.getCmp('nombre_licenciado_id').getValue(),
				no_notario_publico:        Ext.getCmp('no_notario_publico_id').getValue(),
				ciudad_origen:             Ext.getCmp('ciudad_origen_id').getValue(),
				registro_publico_comercio: Ext.getCmp('registro_publico_comercio_id').getValue(),
				folio_mercantil:           Ext.getCmp('folio_mercantil_id').getValue(),
				domicilio_empresa:         Ext.getCmp('domicilio_empresa_id').getValue(),
				ic_solicitud:              Ext.getCmp('ic_solicitud_id').getValue(),
				ic_pyme:                   Ext.getCmp('ic_pyme_id').getValue(),
				estatus:                   Ext.getCmp('estatus_id').getValue()
			}),
			callback: procesarDatosConstitutivo
		});
	}

	// Refresca el gridConsulta para mostrar cuando se ha editado una ventana
	function refrescaGrid(identificador){
		if(identificador == 1){
			cancelarDatosConstitutivo();
		} else if(identificador == 2){
			Ext.getCmp('winRepLeg').hide();
		}
		buscar();
	}

	//Limpia el form 'formaDatosConstitutivo' y oculta el panel de dicho form
	function cancelarDatosConstitutivo(){
		Ext.getCmp('formaDatosConstitutivo').getForm().reset();
		Ext.getCmp('winConst').hide();
	}

	// Realiza la consulta para llenar el grid de representantes
	function consultaRepresentantes(rec){
		pnl.el.mask('Procesando...', 'x-mask-loading');
		consultaRepresentantesData.load({
			params: Ext.apply({
				ic_pyme:      rec.get('IC_PYME'),
				ic_solicitud: rec.get('IC_SOLICITUD')
			})
		});
	}

	// Valida el gridRepresentantes y de ser correctos los datos, los manda a guardar.
	function guardaRepresentantes(){

		var grid = Ext.getCmp('gridRepresentantes');
		var store = grid.getStore();
		var columnModelGrid = grid.getColumnModel();
		var numRegistro = -1;
		var errorValidacion = false;
		store.each(function(record){
			numRegistro = store.indexOf(record);
			if(record.data['CG_TIPO_REPRESENTANTE'] == ''){
				errorValidacion = true;
				grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('CG_TIPO_REPRESENTANTE'));
				return false;
			}
			if(record.data['CG_NOMBRE_LICENCIADO'] == ''){
				errorValidacion = true;
				grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('CG_NOMBRE_LICENCIADO'));
				return false;
			}
			if(record.data['ESCRITURA_PUBLICA'] == ''){
				errorValidacion = true;
				grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('ESCRITURA_PUBLICA'));
				return false;
			}
			if(record.data['FECHA_ESCRITURA_PUBLICA'] == ''){
				errorValidacion = true;
				grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('FECHA_ESCRITURA_PUBLICA'));
				return false;
			}
			if(record.data['NUM_NOTARIO_PUBLICO'] == ''){
				errorValidacion = true;
				grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('NUM_NOTARIO_PUBLICO'));
				return false;
			}
			if(record.data['CG_ORIGEN_NOTARIO'] == ''){
				errorValidacion = true;
				grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('CG_ORIGEN_NOTARIO'));
				return false;
			}
		});
		if(errorValidacion == true){
			return;
		}

		// Se env�an los datos a actualizar
		var jsonDataEncode = '';
		var records = consultaRepresentantesData.getRange();
		var datar = new Array();
		for (var i = 0; i < records.length; i++) {
			datar.push(records[i].data);
		}
		jsonDataEncode = Ext.encode(datar);
		Ext.Ajax.request({
			url: '34capturaCedenteExt.data.jsp',
			params: {
				informacion: 'Guarda_Datos_Representante',
				datosGrid:   jsonDataEncode
			},
			callback: procesarGuardarRepresentantes
		});

	}

	// Proceso para generar el Grid de Representantes
	var procesarConsultaRepresentantesData = function(store, arrRegistros, opts){
		pnl.el.unmask();
		if (arrRegistros != null){
			var jsonData = store.reader.jsonData;
			// Se crea la ventana
			var winRep = Ext.getCmp('winRepLeg');
			if(winRep){
				winRep.show();
			} else{
				var winRepLeg = new Ext.Window ({
					width:       1059,
					id:          'winRepLeg',
					title:       'Representantes - CEDENTE',
					closeAction: 'hide',
					modal:       true,
					closable:    true,
					resizable:   false,
					items:       gridRepresentantes
				}).show();
			}
		}
	};

	// Proceso para generar el Grid de consuta
	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('formaPrincipal');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();
		gridConsulta.show();
		if (arrRegistros != null){
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0){
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

	// Lleno el form 'formaDatosConstitutivo' con los datos de la consulta
	var procesarConsultaDatosConstitutivo = function(opts, success, response){
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			// Se crea la ventana
			var winVen = Ext.getCmp('winConst');
			if(winVen){
				winVen.show();
			} else{
				var winConst = new Ext.Window ({
					width:     500,
					id:        'winConst',
					title:     'Datos Constitutivos - CEDENTE',
					modal:     true,
					closable:  false,
					resizable: false,
					items:     formaDatosConstitutivo
				}).show();
			}
			// Asigno los datos al form
			if(Ext.util.JSON.decode(response.responseText).estatus == 'MODIFICADO'){
				valorFormulario = Ext.util.JSON.decode(response.responseText).registros;
				formaDatosConstitutivo.getForm().setValues(valorFormulario);
			} else{
				Ext.getCmp('ic_pyme_id').setValue(Ext.util.JSON.decode(response.responseText).pyme);
				Ext.getCmp('ic_solicitud_id').setValue(Ext.util.JSON.decode(response.responseText).solicitud);
			}
			Ext.getCmp('estatus_id').setValue(Ext.util.JSON.decode(response.responseText).estatus);

		} else{
			Ext.getCmp('winConst').hide();
			Ext.Msg.alert('Error desconocido', 'Ocurri� un error inesperado al obtener los datos.');
		}
	}

	//Proceso que se ejecuta despues de Guardar los datos del form formaDatosConstitutivo
	var procesarDatosConstitutivo = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje', 'Los datos se actualizaron correctamente.', refrescaGrid(1));
		}else{
			Ext.Msg.alert('Error', Ext.util.JSON.decode(response.responseText).mensaje);
		}
		pnl.el.unmask();
	}

	//Proceso que se ejecuta despues de Guardar los datos de los representantes legales
	var procesarGuardarRepresentantes = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje', 'Los datos se actualizaron correctamente.', refrescaGrid(2));
		}else{
			Ext.Msg.alert('Error', Ext.util.JSON.decode(response.responseText).mensaje);
		}
		pnl.el.unmask();
	}

//------------------------------ STORES ------------------------------
	// Se crea el Store del combo EPO
   var catalogoEpo = new Ext.data.JsonStore({
	   id:         'catalogoEpo',
		root:       'registros',
		url:        '34capturaCedenteExt.data.jsp',
		baseParams: { informacion: 'Catalogo_Epo'},
		fields:     ['clave', 'descripcion', 'loadMsg'],
		autoLoad:   true,
		listeners:  {
			exception:  NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	// Se crea el Store del combo No. Contrato
   var catalogoNoContrato = new Ext.data.JsonStore({
	   id:         'catalogoNoContrato',
		root:       'registros',
		url:        '34capturaCedenteExt.data.jsp',
		baseParams: { informacion: 'Catalogo_No_Contrato'},
		fields:     ['clave', 'descripcion', 'loadMsg'],
		autoLoad:   false,
		listeners:  {
			exception:  NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	// Se crea el Store del combo Tipo de Cedente
   var catalogoTipoCedente = new Ext.data.SimpleStore({
		fields:['clave', 'descripcion'],
		data:[['C','Contratista'],['P','Proveedor']]
	});

	// Se crea el Store del combo Tipo de Representante
	var catalogoTipoRepresentante = new Ext.data.SimpleStore({
		fields:['clave', 'descripcion'],
		data:[['Apoderado','Apoderado'],['Representante Legal','Representante Legal']]
	});

	// Se crea el Store del grid Representantes
	var consultaRepresentantesData = new Ext.data.JsonStore({
		root: 'registros',
		url: '34capturaCedenteExt.data.jsp',
		baseParams: {
			informacion: 'Consultar_Representantes'
		},
		fields: [
			{ name: 'IC_SOLICITUD'           },
			{ name: 'IC_PYME'                },
			{ name: 'IC_USUARIO'             },
			{ name: 'NOMBRE_REPRESENTANTE'   },
			{ name: 'CG_TIPO_REPRESENTANTE'  },
			{ name: 'CG_NOMBRE_LICENCIADO'   },
			{ name: 'ESCRITURA_PUBLICA'      },
			{ name: 'FECHA_ESCRITURA_PUBLICA', type: 'date', convert: function(value,record){ return (Ext.isEmpty(value)?null:Date.parseDate(value, 'd/m/Y')); } },
			{ name: 'NUM_NOTARIO_PUBLICO'    },
			{ name: 'CG_ORIGEN_NOTARIO'      }
			
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaRepresentantesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRepresentantesData(null, null, null);
				}
			}
		}
	});

	// Se crea el Store del grid Consulta
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '34capturaCedenteExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{ name: 'IC_SOLICITUD'},
			{ name: 'IC_PYME'     },
			{ name: 'NOMBRE_PYME' },
			{ name: 'RFC'         },
			{ name: 'NOTARIO'     },
			{ name: 'REP_LEGAL'   },
			{ name: 'EDITABLE'    }
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

//------------------------------ COMPONENTES ------------------------------

	// Se crea el grid de Representantes
	var gridRepresentantes = new Ext.grid.EditorGridPanel({
		width:                '100%',
		height:               200,
		clicksToEdit:         1,
		store:                consultaRepresentantesData,
		id:                   'gridRepresentantes',
		align:                'center',
		margins:              '20 0 0 0',
		style:                'margin:0 auto;',
		emptyMsg:             'No hay registros.',
		hidden:               false,
		autoHeight:           false,
		displayInfo:          true,
		loadMask:             true,
		frame:                false,
		columns: [{
			width:             200,
			header:            '<div align="center"> Nombre del Representante </div>',
			dataIndex:         'NOMBRE_REPRESENTANTE',
			align:             'left'
		},{
			width:             130,
			header:            '<div align="center"> Tipo de Representante </div>',
			dataIndex:         'CG_TIPO_REPRESENTANTE',
			align:             'center',
			editor:            new fm.ComboBox({
				mode:           'local',
				displayField:   'descripcion',
				valueField:     'clave',
				triggerAction:  'all',
				forceSelection: true,
				typeAhead:      true,
				allowBlank:     false,
				store:          catalogoTipoRepresentante
			})
		},{
			width:             200,
			header:            '<div align="center"> Nombre Licenciado </div>',
			dataIndex:         'CG_NOMBRE_LICENCIADO',
			align:             'left',
			editor:            new fm.TextField({
				maxLength:      60,
				allowBlank:     false
			})
		},{
			width:             100,
			header:            '<div align="center"> Escritura P�blica </div>',
			dataIndex:         'ESCRITURA_PUBLICA',
			align:             'center',
			editor:            new fm.TextField({
				maxLength:      20,
				regex:          /^[0-9]*$/,
				allowBlank:     false
			})
		},{
			width:             140,
			header:            '<div align="center"> Fecha Escritura P�blica </div>',
			dataIndex:         'FECHA_ESCRITURA_PUBLICA',
			align:             'center',
			xtype:             'datecolumn',
			format:            'd/m/Y',
			editor:            new fm.DateField({
				allowBlank:     false
			})
		},{
			width:             120,
			header:            '<div align="center"> No. Notario P�blico </div>',
			dataIndex:         'NUM_NOTARIO_PUBLICO',
			align:             'center',
			editor:            new fm.NumberField({
				maxLength:      15,
				allowBlank:     false
			})
		},{
			width:             150,
			header:            '<div align="center"> Ciudad origen del Notario </div>',
			dataIndex:         'CG_ORIGEN_NOTARIO',
			align:             'center',
			editor:            new fm.TextField({
				maxLength:      50,
				allowBlank:     false
			})
		}],
		bbar: ['->','-',{
				text:           'Guardar',
				iconCls:        'icoGuardar',
				id:             'btnGuardaRepresentantes',
				handler:        guardaRepresentantes
        }]
	});

	// Se crea el grid Captura CEDENTE
	var gridConsulta = new Ext.grid.EditorGridPanel({
		width:          650,
		height:         300,
		clicksToEdit:   1,
		store:          consultaData,
		id:             'gridConsulta',
		title:          'Captura CEDENTE',
		align:          'center',
		style:          'margin:0 auto;',
		bodyStyle:      'padding: 6px',
		emptyMsg:       'No hay registros.',
		hidden:         true,
		displayInfo:    true,
		loadMask:       true,
		frame:          false,
		columns: [{
			width:       285,
			header:      '<div align="center"> PYME (Cedente) </div>',
			dataIndex:   'NOMBRE_PYME',
			align:       'left'
		},{
			width:       120,
			header:      '<div align="center"> RFC </div>',
			dataIndex:   'RFC',
			align:       'center'
		},{
			width:       120,
			xtype:       'actioncolumn',
			header:      '<div align="center"> Datos Constitutivos </div>',
			align:       'center',
			sortable:    false,
			resizable:   false,
			items: [{
				iconCls:  'icoEditarForma',
				tooltip:  'Capturar / Modificar',
				handler:  function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if(rec.get('EDITABLE') == 'N'){
						Ext.Msg.alert('Mensaje', 'Los Datos constitutivos ya no pueden ser modificados porque el contrato de Cesi�n de Derechos se encuentra en proceso de autorizaci�n');
					} else{
						consultaDatosConstitutivos(rec);
					}
				}
			},{
				getClass: function(v, meta, rec){
					if(rec.get('NOTARIO') != ''){
						this.items[1].tooltip = 'Captura completa';
						return 'icoAutorizar';
					} else{
						return '';
					}
				}
			}]
		},{
			width:       120,
			xtype:       'actioncolumn',
			header:      '<div align="center"> Representantes </div>',
			align:       'center',
			sortable:    false,
			resizable:   false,
			items: [{
				iconCls:  'icoUserGary',
				tooltip:  'Capturar Representantes',
				handler:  function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					if(rec.get('EDITABLE') == 'N'){
						Ext.Msg.alert('Mensaje', 'Los datos de los Representantes Legales ya no pueden ser modificados por que el contrato de Cesi�n de Derechos se encuentra en proceso de autorizaci�n');
					} else{
						consultaRepresentantes(rec);
					}
				}
			},{
				getClass: function(v, meta, rec){
					if(rec.get('REP_LEGAL') == '0'){
						this.items[1].tooltip = 'Captura completa';
						return 'icoAutorizar';
					} else{
						return '';
					}
				}
			}]
		}]
	});

	// Form Panel Datos Constitutivos
	var formaDatosConstitutivo = new Ext.form.FormPanel({
		width:             '100%',
		labelWidth:        170,
		id:                'formaDatosConstitutivo',
		layout:            'form',
		bodyStyle:         'padding: 6px',
		style:             'margin:0 auto;',
		frame:             true,
		hidden:            false,
		autoHeight:        true,
		items: [{
			anchor:         '90%',
			xtype:          'combo',
			id:             'tipo_cedente_id',
			name:           'tipo_cedente',
			fieldLabel:     '&nbsp; Tipo de Cedente',
			emptyText:      'Seleccionar...',
			mode:           'local',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			msgTarget:      'side',
			forceSelection: true,
			typeAhead:      true,
			allowBlank:     false,
			store:         catalogoTipoCedente
		},{
			anchor:         '90%',
			xtype:          'textfield',
			id:             'figura_juridica_id',
			name:           'figura_juridica',
			fieldLabel:     '&nbsp; Figura Jur�dica',
			msgTarget:      'side',
			maxLength:      80,
			allowBlank:     false
		},{
			anchor:         '90%',
			xtype:          'textfield',
			id:             'escritura_publica_id',
			name:           'escritura_publica',
			fieldLabel:     '&nbsp; Escritura P�blica',
			msgTarget:      'side',
			regex:          /^[0-9]*$/,
			maxLength:      20,
			allowBlank:     false
		},{
			anchor:         '90%',
			xtype:          'datefield',
			id:             'fecha_escritura_id',
			name:           'fecha_escritura',
			fieldLabel:     '&nbsp; Fecha Escritura P�blica',
			minValue:       '01/01/1901',
			msgTarget:      'side',
			allowBlank:     false
		},{
			anchor:         '90%',
			xtype:          'textfield',
			id:             'nombre_licenciado_id',
			name:           'nombre_licenciado',
			fieldLabel:     '&nbsp; Nombre Licenciado',
			msgTarget:      'side',
			maxLength:      60,
			allowBlank:     false
		},{
			anchor:         '90%',
			xtype:          'textfield',
			id:             'no_notario_publico_id',
			name:           'no_notario_publico',
			fieldLabel:     '&nbsp; N�mero de Notario P�blico',
			msgTarget:      'side',
			regex:          /^[0-9]*$/,
			maxLength:      15,
			allowBlank:     false
		},{
			anchor:         '90%',
			xtype:          'textfield',
			id:             'ciudad_origen_id',
			name:           'ciudad_origen',
			fieldLabel:     '&nbsp; Ciudad origen del Notario',
			msgTarget:      'side',
			maxLength:      50,
			allowBlank:     false
		},{
			anchor:         '90%',
			xtype:          'textfield',
			id:             'registro_publico_comercio_id',
			name:           'registro_publico_comercio',
			fieldLabel:     '&nbsp; Registro P�blico de Comercio',
			msgTarget:      'side',
			maxLength:      30,
			allowBlank:     false
		},{
			anchor:         '90%',
			xtype:          'textfield',
			id:             'folio_mercantil_id',
			name:           'folio_mercantil',
			fieldLabel:     '&nbsp; Numero de Folio Mercantil',
			msgTarget:      'side',
			regex:          /^[0-9]*$/,
			maxLength:      20,
			allowBlank:     false
		},{
			anchor:         '90%',
			height:         40,
			xtype:          'textarea',
			id:             'domicilio_empresa_id',
			name:           'domicilio_empresa',
			fieldLabel:     '&nbsp; Domicilio Legal Empresa',
			msgTarget:      'side',
			maxLength:      350,
			autoScroll:     true,
			allowBlank:     false
		},{
			xtype:          'displayfield',
			id:             'ic_solicitud_id',
			name:           'ic_solicitud',
			hidden:         true
		},{
			xtype:          'displayfield',
			id:             'ic_pyme_id',
			name:           'ic_pyme',
			hidden:         true
		},{
			xtype:          'displayfield', // Este campo indica si los datos son nuevos o se modifican (NUEVO, MODIFICADO)
			id:             'estatus_id',
			name:           'estatus',
			hidden:         true
		}],
		buttons: [{
			text:           'Guardar',
			id:             'btnGuardar',
			iconCls:        'icoGuardar',
			handler:        guardarDatosConstitutivo
		},{
			text:           'Cancelar',
			id:             'btnCancelar',
			iconCls:        'icoCancelar',
			handler:        cancelarDatosConstitutivo
		}]
	});

	// Form Panel principal
	var formaPrincipal = new Ext.form.FormPanel({
		width:             650,
		labelWidth:        120,
		id:                'formaPrincipal',
		title:             'Criterios de B�squeda',
		layout:            'form',
		bodyStyle:         'padding: 6px',
		style:             'margin:0 auto;',
		frame:             true,
		autoHeight:        true,
		items: [{
			width:          455,
			xtype:          'combo',
			id:             'ic_epo_id',
			name:           'ic_epo',
			fieldLabel:     '&nbsp; Cadena Productiva',
			emptyText:      'Seleccionar...',
			mode:           'local',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			msgTarget:      'side',
			forceSelection: true,
			typeAhead:      true,
			allowBlank:     false,
			store:          catalogoEpo,
			listeners:{
				select:{
					fn: function(combo){
						if(combo.getValue() != '' || combo.getValue() != null){
							catalogoNoContrato.load({
								params: {
									ic_epo: combo.getValue()
								}
							});
						}
					}
				}
			}
		},{
			width:          455,
			xtype:          'combo',
			id:             'no_contrato_id',
			name:           'no_contrato',
			fieldLabel:     '&nbsp; No. Contrato',
			emptyText:      'Seleccionar...',
			mode:           'local',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			msgTarget:      'side',
			forceSelection: true,
			typeAhead:      true,
			allowBlank:     false,
			store:          catalogoNoContrato
		}],
		buttons: [{
			text:           'Buscar',
			id:             'btnBuscar',
			iconCls:        'icoBuscar',
			handler:        buscar
		},{
			text:           'Limpiar',
			id:             'btnLimpiar',
			iconCls:        'icoLimpiar',
			handler:        cancelar
		}]
	});

	//Contenedor Principal
	var pnl = new Ext.Container({
		width:     949,
		id:        'contenedorPrincipal',
		applyTo:   'areaContenido',
		style:     'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal,
			NE.util.getEspaciador(10),
			gridConsulta
		]
	});

});