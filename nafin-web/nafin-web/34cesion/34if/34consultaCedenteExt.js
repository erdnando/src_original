Ext.onReady(function(){

//---------------------------------- Handlers --------------------------------//
	// Resetea el form principal y oculta el grid principal
	function limpiar(){
		Ext.getCmp('forma').getForm().reset();
		Ext.getCmp('gridConsulta').hide();
	}

	// Realiza la consulta para llenar el grid
	function consultaDatos(){
		if(!Ext.getCmp('forma').getForm().isValid()){
			return;
		}
		if(Ext.getCmp('fecha_inicio').getValue() == '' && Ext.getCmp('fecha_final').getValue() != ''){
			Ext.getCmp('fecha_inicio').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			Ext.getCmp('fecha_inicio').focus();
			return;
		}
		if(Ext.getCmp('fecha_inicio').getValue() != '' && Ext.getCmp('fecha_final').getValue() == ''){
			Ext.getCmp('fecha_final').markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			Ext.getCmp('fecha_final').focus();
			return;
		}
		pnl.el.mask('Procesando...', 'x-mask-loading');
		consultaData.load({
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Consultar_Cedente'
			})
		});
	}

	// Realiza la consulta para llenar el grid representantes
	var verRepresentantes =  function(gridConsulta, rowIndex, colIndex){
		pnl.el.mask('Procesando...', 'x-mask-loading');
		var rec = consultaData.getAt(rowIndex);
		Ext.getCmp('pyme_id').setValue(rec.get('IC_PYME'));
		Ext.getCmp('solicitud_id').setValue(rec.get('IC_SOLICITUD'));
		Ext.getCmp('nombre_pyme_id').setValue(rec.get('NOMBRE_PYME'));
		consultaRepresentantesData.load({
			params: Ext.apply({
				ic_pyme:      rec.get('IC_PYME'),
				ic_solicitud: rec.get('IC_SOLICITUD')
			})
		});
	}

//---------------------------------- Callback --------------------------------

	// Descarga los archivos generados en los grids
	function procesarGenerarCSV(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarCSV').enable();
		Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
		Ext.getCmp('btnGenerarCSV_Rep').enable();
		Ext.getCmp('btnGenerarCSV_Rep').setIconClass('icoXls');
	}

	// Proceso para generar el Grid de Representantes
	var procesarConsultaRepresentantesData = function(store, arrRegistros, opts){
		pnl.el.unmask();
		if (arrRegistros != null){
			//Agrego el t�tulo al grid
			Ext.getCmp('gridRepresentantes').setTitle(Ext.getCmp('nombre_pyme_id').getValue());
			// Se crea la ventana
			var winRep = Ext.getCmp('winRepLeg');
			if(winRep){
				winRep.show();
			} else{
				var winRepLeg = new Ext.Window ({
					width:       900,
					id:          'winRepLeg',
					title:       'Representantes - CEDENTE',
					closeAction: 'hide',
					modal:       true,
					closable:    true,
					resizable:   false,
					items:       gridRepresentantes
				}).show();
			}
		}
	};

	// Proceso para generar el Grid de consuta
	var procesarConsultaData = function(store, arrRegistros, opts){
		pnl.el.unmask();
		var grid = Ext.getCmp('gridConsulta');
		var el = grid.getGridEl();
		grid.show();
		if (arrRegistros != null){
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0){
				Ext.getCmp('btnGenerarCSV').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnGenerarCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

//---------------------------------- Stores --------------------------------

	//Este store es para los datos que seran cargados en el grid de representantes
	var consultaRepresentantesData = new Ext.data.JsonStore({
		root: 'registros',
		id:   'consultaRepresentantesData',
		url:  '34consultaCedenteExt.data.jsp',
		baseParams: {
			informacion: 'Consultar_Representante'
		},
		fields: [
			{ name: 'IC_SOLICITUD'           },
			{ name: 'IC_PYME'                },
			{ name: 'IC_USUARIO'             },
			{ name: 'NOMBRE_REPRESENTANTE'   },
			{ name: 'CG_TIPO_REPRESENTANTE'  },
			{ name: 'CG_NOMBRE_LICENCIADO'   },
			{ name: 'ESCRITURA_PUBLICA'      },
			{ name: 'FECHA_ESCRITURA_PUBLICA'},
			{ name: 'NUM_NOTARIO_PUBLICO'    },
			{ name: 'CG_ORIGEN_NOTARIO'      }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: procesarConsultaRepresentantesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRepresentantesData(null, null,null);
				}
			}
		}
	});

	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		id:   'consultaData',
		url:  '34consultaCedenteExt.data.jsp',
		baseParams: {
			informacion: 'Consultar_Cedente'
		},
		fields: [
			{ name: 'IC_SOLICITUD'           },
			{ name: 'IC_EPO'                 },
			{ name: 'IC_PYME'                },
			{ name: 'NOMBRE_EPO'             },
			{ name: 'NOMBRE_PYME'            },
			{ name: 'NO_CONTRATO'            },
			{ name: 'TIPO_CEDENTE'           },
			{ name: 'FIGURA_JURIDICA'        },
			{ name: 'ESCRITURA_PUBLICA'      },
			{ name: 'FECHA_ESCRITURA_PUBLICA'},
			{ name: 'NOMBRE_LICENCIADO'      },
			{ name: 'NOTARIO_PUBLICO'        },
			{ name: 'ORIGEN_NOTARIO'         },
			{ name: 'REG_PUB_COMERCIO'       },
			{ name: 'NUM_FOLIO_MERCANTIL'    },
			{ name: 'DOMICILIO_LEGAL'        }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null,null);
				}
			}
		}
	});

	//Catalogo para el combo de seleccion de Epo
	var catalogoEPO = new Ext.data.JsonStore({
		id:     'catalogoEPO',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '34consultaCedenteExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty: 'total',
		autoLoad:      false,
		listeners: {
			exception:  NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	//Catalogo para el combo de seleccion de Pyme
	var catalogoPYME = new Ext.data.JsonStore({
		id:     'catalogoPYME',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '34consultaCedenteExt.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty: 'total',
		autoLoad:      false,
		listeners: {
			exception:  NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//------------------------------- COMPONENTES ---------------------------------

	//Elementos del grid representantes
	var gridRepresentantes = new Ext.grid.EditorGridPanel({
		height:         250,
		store:          consultaRepresentantesData,
		title:          'Representante',
		width:          '100%',
		id:             'gridRepresentantes',
		align:          'center',
		style:          'margin:0 auto;',
		bodyStyle:      'padding: 6px',
		emptyMsg:       'No hay registros.',
		hidden:         false,
		displayInfo:    true,
		loadMask:       true,
		frame:          false,
		columns: [
			{width:200, align:'left',   header:'<div align="center"> Nombre del Representante </div>',   dataIndex:'NOMBRE_REPRESENTANTE'   },
			{width:130, align:'left',   header:'<div align="center"> Tipo de Representante </div>',      dataIndex:'CG_TIPO_REPRESENTANTE'  },
			{width:170, align:'left',   header:'<div align="center"> Nombre Licenciado </div>',          dataIndex:'CG_NOMBRE_LICENCIADO'   },
			{width:120, align:'center', header:'<div align="center"> Escritura P�blica </div>',          dataIndex:'ESCRITURA_PUBLICA'      },
			{width:145, align:'center', header:'<div align="center"> Fecha Escritura P�blica </div>',    dataIndex:'FECHA_ESCRITURA_PUBLICA'},
			{width:120, align:'center', header:'<div align="center"> No. Notario P�blico </div>',        dataIndex:'NUM_NOTARIO_PUBLICO'    },
			{width:170, align:'left',   header:'<div align="center"> Ciudad. Origen del Notario </div>', dataIndex:'CG_ORIGEN_NOTARIO'      }
		],
		bbar:{
			items: ['->','-',{
				xtype:   'button',
				text:    'Generar Archivo',
				tooltip: 'Generar Archivo en formato CSV',
				id:      'btnGenerarCSV_Rep',
				iconCls: 'icoXls',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '34consultaCedenteExt.data.jsp',
						params: Ext.apply({
							informacion:  'ArchivoCSV_Rep',
							ic_pyme:      Ext.getCmp('pyme_id').getValue(),
							ic_solicitud: Ext.getCmp('solicitud_id').getValue(),
							nombre_pyme:  Ext.getCmp('nombre_pyme_id').getValue()
						}),
						callback: procesarGenerarCSV
					});
				}
			}]
		}
	});

	//Elementos del grid de la consulta
	var gridConsulta = new Ext.grid.EditorGridPanel({
		width:          800,
		height:         300,
		store:          consultaData,
		id:             'gridConsulta',
		title:          'Datos Constitutivos CEDENTE',
		align:          'center',
		style:          'margin:0 auto;',
		bodyStyle:      'padding: 6px',
		emptyMsg:       'No hay registros.',
		hidden:         true,
		displayInfo:    true,
		loadMask:       true,
		frame:          false,
		columns: [
		{width:200, align:'left',   header:'<div align="center"> EPO </div>',                      dataIndex:'NOMBRE_EPO'             },
		{width:220, align:'left',   header:'<div align="center"> PYME </div>',                     dataIndex:'NOMBRE_PYME'            },
		{width:100, align:'center', header:'<div align="center"> No. Contrato </div>',             dataIndex:'NO_CONTRATO'            },
		{width:120, align:'left',   header:'<div align="center"> Tipo de CEDENTE </div>',          dataIndex:'TIPO_CEDENTE'           },
		{width:120, align:'left',   header:'<div align="center"> Figura Jur�dica </div>',          dataIndex:'FIGURA_JURIDICA'        },
		{width:120, align:'center', header:'<div align="center"> Escritura P�blica </div>',        dataIndex:'ESCRITURA_PUBLICA'      },
		{width:140, align:'center', header:'<div align="center"> Fecha Escritura P�blica </div>',  dataIndex:'FECHA_ESCRITURA_PUBLICA'},
		{width:150, align:'left',   header:'<div align="center"> Nombre del Licenciado </div>',    dataIndex:'NOMBRE_LICENCIADO'      },
		{width:125, align:'center', header:'<div align="center"> N�m. Notario P�blico </div>',     dataIndex:'NOTARIO_PUBLICO'        },
		{width:150, align:'left',   header:'<div align="center"> Cd. Origen del Notario </div>',   dataIndex:'ORIGEN_NOTARIO'         },
		{width:150, align:'center', header:'<div align="center"> Reg. P�blico de Comercio </div>', dataIndex:'REG_PUB_COMERCIO'       },
		{width:110, align:'center', header:'<div align="center"> Folio Mercantil </div>',          dataIndex:'NUM_FOLIO_MERCANTIL'    },
		{width:200, align:'left',   header:'<div align="center"> Domicilio Legal Empresa </div>',  dataIndex:'DOMICILIO_LEGAL'        },
		{
			width:       120,
			xtype:       'actioncolumn',
			header:      '<div align="center"> Ver Representantes </div>',
			align:       'center',
			sortable:    false,
			resizable:   false,
			items: [{
				iconCls:  'icoUserGary',
				tooltip:  'Representantes CEDENTE',
				handler:  verRepresentantes
			}]
		}],
		bbar:{
			items: ['->','-',{
				xtype:   'button',
				text:    'Generar Archivo',
				tooltip: 'Generar Archivo con formato CSV',
				id:      'btnGenerarCSV',
				iconCls: 'icoXls',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '34consultaCedenteExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV'
						}),
						callback: procesarGenerarCSV
					});
				}
			}]
		}
	});

	//Elementos que van dentro del formulario de datos para realizar la consulta
	var elementosFormaConsulta = [{
		anchor:              '95%',
		xtype:               'combo',
		id:                  'ic_epo_id',
		hiddenName:          'ic_epo',
		fieldLabel:          'EPO',
		emptyText:           'Seleccione...',
		mode:                'local',
		triggerAction:       'all',
		valueField:          'clave',
		displayField:        'descripcion',
		forceSelection:      true,
		editable:            true,
		typeAhead:           true,
		allowBlank:          true,
		minChars:            1,
		store:               catalogoEPO,
		tpl:                 NE.util.templateMensajeCargaCombo,
		listeners:{
			select:{
				fn: function(combo){
					if(combo.getValue() != '' || combo.getValue() != null){
						catalogoPYME.load({
							params: {
								ic_epo: combo.getValue()
							}
						});
					}
				}
			}
		}
	},{
		anchor:              '95%',
		xtype:               'combo',
		id:                  'ic_pyme_id',
		hiddenName:          'ic_pyme',
		fieldLabel:          'PYME',
		emptyText:           'Seleccione...',
		mode:                'local',
		triggerAction:       'all',
		valueField:          'clave',
		displayField:        'descripcion',
		forceSelection:      true,
		editable:            true,
		typeAhead:           true,
		allowBlank:          true,
		minChars:            1,
		store:               catalogoPYME,
		tpl:                 NE.util.templateMensajeCargaCombo
	},{
		anchor:              '95%',
		xtype:               'textfield',
		id:                  'no_contrato_id',
		hiddenName:          'no_contrato',
		fieldLabel:          'No. contrato',
		maxLength:           20
	},{
		xtype:               'compositefield',
		fieldLabel:          'Fecha Escritura P�blica',
		combineErrors:       false,
		items: [{
			width:            150,
			xtype:            'datefield',
			id:               'fecha_inicio',
			name:             'fecha_inicio',
			hiddenName:       'fecha_ini',
			msgTarget:        'side',
			vtype:            'rangofecha',
			minValue:         '01/01/1901',
			campoFinFecha:    'fecha_final',
			margins:          '0 20 0 0',
			allowBlank:       true,
			startDay:         0
		},{
			xtype:            'displayfield',
			value:            'a',
			width:            20
		},{
			width:            150,
			xtype:            'datefield',
			id:               'fecha_final',
			name:             'fecha_final',
			hiddenName:       'fecha_fin',
			msgTarget:        'side',
			vtype:            'rangofecha',
			minValue:         '01/01/1901',
			campoInicioFecha: 'fecha_inicio',
			margins:          '0 20 0 0',
			allowBlank:       true,
			startDay:         1
		}]
	},{
		xtype:               'displayfield',
		id:                  'pyme_id',
		hiddenName:          'pyme',
		hidden:              true
	},{
		xtype:               'displayfield',
		id:                  'solicitud_id',
		hiddenName:          'solicitud',
		hidden:              true
	},{
		xtype:               'displayfield',
		id:                  'nombre_pyme_id',
		hiddenName:          'nombre_pyme',
		hidden:              true
	}];

	var fp = new Ext.form.FormPanel({
		width:         700,
		id:            'forma',
		title:         'Criterios de B�squeda',
		style:         'margin:0 auto;',
		bodyStyle:     'padding: 6px',
		collapsible:   true,
		titleCollapse: true,
		frame:         true,
		labelWidth:    168,
		defaults:      {
			msgTarget:  'side',
			anchor:     '-20'
		},
		items:         elementosFormaConsulta,
		buttons: [{
			text:       'Buscar',
			id:         'btnBuscar',
			iconCls:    'icoBuscar',
			handler:    consultaDatos
		},{
			text:       'Limpiar',
			iconCls:    'icoLimpiar',
			handler:    limpiar
		}]
	});

//----------------------------------- CONTENEDOR -------------------------------
	var pnl = new Ext.Container({
		width:   940,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		height:  'auto',
		items: [
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta
		]
	});

//------------------------------- INICIALIZACI�N -------------------------------
	catalogoEPO.load();
	catalogoPYME.load();

});