Ext.onReady(function(){
	var pyme_ant="";

	var procesarConsultaRegistros = function(store, registros, opts){
		var jsonData = store.reader.jsonData;
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		if (registros != null) {
			var grid  = Ext.getCmp('gridConsulta');
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnXls').enable();
			} else {
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnXls').disable();
				el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
	
	function procesarGenerarPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnGenerarPDF').enable();
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			boton = Ext.getCmp('btnGenerarPDF');
			boton.setIconClass('icoPdf');
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarSuccessFailureArchivoCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			boton = Ext.getCmp('btnXls');
			boton.enable();
			boton.setIconClass('icoXls');
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	var modificarCedente  = function (grid,rowIndex,colIndex,item){
		
		var registro = grid.getStore().getAt(rowIndex);
		fp.hide();
		grid.hide();
		Ext.getCmp('_cmb_pyme').setValue(registro.get('IC_PYME'));
		Ext.getCmp('_cmb_rep').setValue(registro.get('TIPO_REP'));
		Ext.getCmp('_cmb_cedente').setValue(registro.get('TIPO_CED'));
		Ext.getCmp('_txt_fig_jur').setValue(registro.get('FIG_JUR'));
		Ext.getCmp('_txt_escritura').setValue(registro.get('ESC_PUB'));
		Ext.getCmp('_fecha_esc_pub').setValue(registro.get('FEC_ESC_PUB'));
		Ext.getCmp('_txt_lic').setValue(registro.get('NOM_LIC'));
		Ext.getCmp('_txt_not_pub').setValue(registro.get('NOT_PUB'));
		Ext.getCmp('_txt_ori_not').setValue(registro.get('CD_NOT'));
		Ext.getCmp('_txt_pub_com').setValue(registro.get('REG_PUB'));
		Ext.getCmp('_txt_folio').setValue(registro.get('FOLIO'));
		
		if(registro.get('CS_BANDERA_REP')=='S'){
			disabledCamposREP(true);//Deshabilitar componentes del Representante de inicio
			Ext.getCmp('id_empSI').setValue(true);
		}else{
			Ext.getCmp('id_empNO').setValue(true);
		}
		Ext.getCmp('_txt_escritura_rep').setValue(registro.get('ESC_PUB_REP'));
		Ext.getCmp('_fecha_esc_pub_rep').setValue(registro.get('FEC_ESC_PUB_REP'));
		Ext.getCmp('_txt_lic_rep').setValue(registro.get('NOM_LIC_REP'));
		Ext.getCmp('_txt_not_pub_rep').setValue(registro.get('NOT_PUB_REP'));
		Ext.getCmp('_txt_ori_not_rep').setValue(registro.get('CD_NOT_REP'));
		Ext.getCmp('_txt_domicilio_legal').setValue(registro.get('CG_DOMICILIO_LEGAL'));
		
		
		
		
		pyme_ant = registro.data['IC_PYME'];
		fpMod.show();
		
	}
	
	var eliminarCedente  = function (grid,rowIndex,colIndex,item){
		var registro = grid.getStore().getAt(rowIndex);
		Ext.Msg.confirm('', 'Esta seguro de querer eliminar el registro...? ',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							Ext.Ajax.request({
								url: '34capturaCed.data.jsp',
								params:	Ext.apply(fp.getForm().getValues(),
										{
											informacion:'eliminarCedente',
											ic_pyme: registro.data['IC_PYME']
										}
								),
								callback: procesarSuccessFailureEliminar
							});
						}
		});
	}
	
	function procesarSuccessFailureEliminar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			accionConsulta("BUSCAR", null);
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarModificarCedente(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fpMod.hide();
			fp.show();
			accionConsulta("BUSCAR", null);
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	

	function actualizaInformacionREP(){
		if(Ext.getCmp('id_empSI').getValue()){
			Ext.getCmp('_txt_escritura_rep').setValue(Ext.getCmp('_txt_escritura').getValue());
			Ext.getCmp('_fecha_esc_pub_rep').setValue(Ext.getCmp('_fecha_esc_pub').getValue());
			Ext.getCmp('_txt_lic_rep').setValue(Ext.getCmp('_txt_lic').getValue());
			Ext.getCmp('_txt_not_pub_rep').setValue(Ext.getCmp('_txt_not_pub').getValue());
			Ext.getCmp('_txt_ori_not_rep').setValue(Ext.getCmp('_txt_ori_not').getValue());
		}
	}
	
	function disabledCamposREP(bandera){
		Ext.getCmp('_txt_escritura_rep').setDisabled(bandera);
		Ext.getCmp('_fecha_esc_pub_rep').setDisabled(bandera);
		Ext.getCmp('_txt_lic_rep').setDisabled(bandera);
		Ext.getCmp('_txt_not_pub_rep').setDisabled(bandera);
		Ext.getCmp('_txt_ori_not_rep').setDisabled(bandera);
	}
	
	
/*----------------------------- Maquina de Edos. ----------------------------*/
		var accionConsulta = function(estadoSiguiente, respuesta){

			if(  estadoSiguiente == "BUSCAR" ){
				var forma = Ext.getCmp("forma");
				forma.el.mask('Buscando...','x-mask-loading');
	
				// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
				Ext.getCmp('gridConsulta').hide(); 
				Ext.StoreMgr.key('registrosConsultadosDataStore').load({
										params:	Ext.apply(fp.getForm().getValues(),
												{
													informacion: 'ConsultarCedente'
												}
											)
				});
				
			}else if(	estadoSiguiente == "LIMPIAR"){
				Ext.getCmp('forma').getForm().reset();
				grid.hide();
				
			}else if( estadoSiguiente == "MODIFICAR" ){
				if(Ext.getCmp('_cmb_pyme').getValue()=='' || Ext.getCmp('_txt_fig_jur').getValue()=='' 
					|| Ext.getCmp('_txt_escritura').getValue()=='' || Ext.getCmp('_fecha_esc_pub').getValue()=='' || Ext.getCmp('_txt_lic').getValue()=='' 
					|| Ext.getCmp('_txt_not_pub').getValue()=='' || Ext.getCmp('_txt_ori_not').getValue()=='' || Ext.getCmp('_txt_pub_com').getValue()==''
					|| Ext.getCmp('_txt_folio').getValue()==''
					|| Ext.getCmp('_txt_escritura_rep').getValue()=='' || Ext.getCmp('_fecha_esc_pub_rep').getValue()=='' 
					|| Ext.getCmp('_txt_lic_rep').getValue()=='' || Ext.getCmp('_txt_not_pub_rep').getValue()=='' || Ext.getCmp('_txt_ori_not_rep').getValue()==''
					|| Ext.getCmp('_txt_domicilio_legal').getValue()==''){
					
						if(Ext.getCmp('_cmb_pyme').getValue()=='')
							Ext.getCmp('_cmb_pyme').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_fig_jur').getValue()=='' )
							Ext.getCmp('_txt_fig_jur').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_txt_escritura').getValue()=='')
							Ext.getCmp('_txt_escritura').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_fecha_esc_pub').getValue()=='')
							Ext.getCmp('_fecha_esc_pub').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_lic').getValue()=='')
							Ext.getCmp('_txt_lic').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_not_pub').getValue()=='')
							Ext.getCmp('_txt_not_pub').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_txt_ori_not').getValue()=='')
							Ext.getCmp('_txt_ori_not').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_txt_pub_com').getValue()=='')
							Ext.getCmp('_txt_pub_com').markInvalid("Campo obligatorio");		
						else if(Ext.getCmp('_txt_folio').getValue()=='')
							Ext.getCmp('_txt_folio').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_domicilio_legal').getValue()=='')
							Ext.getCmp('_txt_domicilio_legal').markInvalid("Campo obligatorio");
						
					
				}else{
					bandera = true;
					var fechaEP = Ext.getCmp('_fecha_esc_pub');
					if(fechaEP.getValue()!=""){
						var fechaDe = Ext.util.Format.date(fechaEP.getValue(),'d/m/Y');
						if(!isdate(fechaDe)){
							fechaEP.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							bandera = false;
						}
					}if(bandera){
						Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
							if (botonConf == 'ok' || botonConf == 'yes') {
								Ext.Ajax.request({
									url: '34capturaCed.data.jsp',
									params: Ext.apply(fpMod.getForm().getValues(),{
										informacion: 'ModificarCedente',
										pyme_ant: pyme_ant
									}),
									callback: procesarModificarCedente
								});
							}
						});
					}
				}
				
			}
			
		}
		
/*---------------------------------- Store's --------------------------------*/

	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'34capturaCed.data.jsp',
			baseParams: {	informacion:	'ConsultarCedente'	},
			fields: [
				{ name: 'IC_PYME'},
				{ name: 'PYME'},
				{ name: 'TIPO_REP'},
				{ name: 'TIPO_CED'},
				{ name: 'FIG_JUR'},
				{ name: 'ESC_PUB'},
				{ name: 'FEC_ESC_PUB'},
				{ name: 'NOM_LIC'},
				{ name: 'NOT_PUB'},
				{ name: 'CD_NOT'},
				{ name: 'REG_PUB'},
				{ name: 'FOLIO'},
				{ name: 'ESC_PUB_REP'},
				{ name: 'FEC_ESC_PUB_REP'},
				{ name: 'NOM_LIC_REP'},
				{ name: 'NOT_PUB_REP'},
				{ name: 'CD_NOT_REP'},
				{ name: 'CS_BANDERA_REP'},
				{ name: 'CG_DOMICILIO_LEGAL'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null,null);
					}
				}
			}
	});
	
	//Catalogo para el combo de seleccion de Pyme
	var catalogoPymeData = new Ext.data.JsonStore({
		id:				'catalogoPymeDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'34capturaCed.data.jsp',
		baseParams:		{	informacion: 'CatalogoPYME'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeRepresentante = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
    data : [ ['L','Representante Legal']
				 ,['A','Apoderado']
			  ]
	});
	
	var storeTipoCedente = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
    data : [ ['C','Contratista']
				 ,['P','Proveedor']
			  ]
	});
	
	
//------------------------------- COMPONENTES ---------------------------------

//Elementos del grid de la consulta
		var grid = new Ext.grid.GridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			title:		'Contrato de Cesi�n',
			stripeRows: true,
			loadMask: 	true,
			height: 		380,
			width: 		845,
			frame: 		false,
			columns: [
				{
					header: 		'PYME',
					tooltip: 	'Nombre de la Pyme',
					dataIndex: 	'PYME',
					sortable: 	true,
					align:		'left',
					resizable: 	true,
					width: 		180
				},{
					header: 		'Tipo de Representante',
					tooltip: 	'Tipo de Representante',
					dataIndex: 	'TIPO_REP',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		160,
					renderer: function (value, metadata, registro, rowIndex, colIndex){
						if(registro.data['TIPO_REP']=='L')
							return 'Representante Legal';
						else if(registro.data['TIPO_REP']=='A')
							return 'Apoderado';
					}
				},{
					header: 		'Tipo de CEDENTE',
					tooltip: 	'Tipo de CEDENTE',
					dataIndex: 	'TIPO_CED',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150,
					renderer: function (value, metadata, registro, rowIndex, colIndex){
						if(registro.data['TIPO_CED']=='C')
							return 'Contratista';
						else if(registro.data['TIPO_CED']=='P')
							return 'Proveedor';
					}
				},{
					header: 		'Figura Juridica',
					tooltip: 	'Figura Juridica',
					dataIndex: 	'FIG_JUR',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Escritura P�blica',
					tooltip: 	'Escritura P�blica',
					dataIndex: 	'ESC_PUB',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Fecha Escritura P�blica',
					tooltip: 	'Fecha Escritura P�blica',
					dataIndex: 	'FEC_ESC_PUB',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Nombre del Licenciado',
					tooltip: 	'Nombre del Licenciado',
					dataIndex: 	'NOM_LIC',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'N�m. Notario P�blico',
					tooltip: 	'N�m. Notario P�blico',
					dataIndex: 	'NOT_PUB',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Cd. Origen del Notario',
					tooltip: 	'Cd. Origen del Notario',
					dataIndex: 	'CD_NOT',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Reg. P�blico de Comercio',
					tooltip: 	'Reg. P�blico de Comercio',
					dataIndex: 	'REG_PUB',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Folio Mercantil',
					tooltip: 	'Folio Mercantil',
					dataIndex: 	'FOLIO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120
					
				},{
					header: 		'Domicilio Legal Empresa',
					tooltip: 	'Domicilio Legal Empresa',
					dataIndex: 	'CG_DOMICILIO_LEGAL',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
				},{
					header: 		'Escritura P�blica Representante',
					tooltip: 	'Escritura P�blica Representante',
					dataIndex: 	'ESC_PUB_REP',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Fecha Escritura P�blica Representante',
					tooltip: 	'Fecha Escritura P�blica Representante',
					dataIndex: 	'FEC_ESC_PUB_REP',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Nombre del Licenciado Representante',
					tooltip: 	'Nombre del Licenciado Representante',
					dataIndex: 	'NOM_LIC_REP',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'N�m. Notario P�blico Representante',
					tooltip: 	'N�m. Notario P�blico Representante',
					dataIndex: 	'NOT_PUB_REP',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Cd. Origen del Notario Representante',
					tooltip: 	'Cd. Origen del Notario Representante',
					dataIndex: 	'CD_NOT_REP',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					xtype: 'actioncolumn',
					header: 'Acciones',
					tooltip: 'Modificar o eliminar los datos',
					width: 90,
					align: 'center',
					items: [
						{
							getClass: function(valor, metadata, record, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Modificar';
									return 'modificar';
							},
							handler: modificarCedente
						},{
							getClass: function(valor, metadata, record, rowIndex, colIndex, store) {
									this.items[1].tooltip = 'Eliminar';
									return 'rechazar';
							},
							handler: eliminarCedente
						}
					]
				}
			],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						Ext.Ajax.request({
							url: '34capturaCed.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoPDF'							
							})					
							,callback: procesarGenerarPDF
						});
					}
				},'-',{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnXls',
					iconCls: 'icoXls',
					handler: function(boton, evento){
									boton.disable();
									boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url : '34capturaCed.data.jsp',
											params:	Ext.apply(fp.getForm().getValues(),
														{
															informacion:'ArchivoCSV'
														}
											),
											callback: procesarSuccessFailureArchivoCSV
										});
					}
				},'-'
			]
		}
	});

	//Elementos que van dentro del formulario de datos para realizar la consulta
	var elementosFormaConsulta = [
			{
				xtype: 'combo',
				id: '_cmb_pyme_con',
				fieldLabel: 'PYME',
				mode: 'local',
				hiddenName : 'cmb_pyme',
				emptyText: 'Seleccione...',
				forceSelection : true,
				triggerAction : 'all',
				editable: true,
				typeAhead: true,
				allowBlank: false,
				minChars : 1,
				valueField		: 'clave',
				displayField	:'descripcion',
				store: catalogoPymeData,
				tpl: NE.util.templateMensajeCargaCombo,
				anchor:	'95%'
			}	
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			700,
		style:			'margin:0 auto;',
		collapsible:	true,
		title: 'Criterios de Busqueda',
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		168,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosFormaConsulta,
		monitorValid:	true,
		buttons: [
			{
				text:		'Buscar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:false,
				handler: function(boton, evento) {
								accionConsulta("BUSCAR", null);
							}
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR", null);
							}
			}
		]
	});
	
	var elementosFormaModificar = [
			{
				xtype: 'displayfield',
				value: 'DATOS CEDENTE',
				width: 35
			},{
				xtype: 'combo',
				id: '_cmb_pyme',
				fieldLabel: 'Pyme',
				mode: 'local',
				hiddenName : 'cmb_pyme',
				emptyText: 'Seleccione...',
				forceSelection : true,
				triggerAction : 'all',
				editable: true,
				typeAhead: true,
				allowBlank: false,
				minChars : 1,
				valueField		: 'clave',
				displayField	:'descripcion',
				store: catalogoPymeData,
				anchor:	'95%'
			},{
				xtype: 'combo',
				id:	'_cmb_cedente',
				fieldLabel: 'Tipo de Cedente',
				editable: true,
				emptyText: 'Seleccione...',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName : 'cmb_cedente',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: storeTipoCedente,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_fig_jur',
				name : 'txt_fig_jur',
				fieldLabel:	'Figura Juridica',
				allowBlank: false,
				maxLength	: 80,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_escritura',
				name : 'txt_escritura',
				fieldLabel:	'Escritura P�blica',
				allowBlank: false,
				maskRe:		/[0-9]/,
				maxLength	: 20,
				anchor:	'95%',
				listeners:{
					change:	function(){
									actualizaInformacionREP();
								}
				}
			},{
				xtype: 'datefield',
				name: 'fecha_esc_pub',
				id: '_fecha_esc_pub',
				allowBlank: false,
				fieldLabel:	'Fecha Escritura P�blica',
				startDay: 0,
				anchor:	'40%',
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
				,
				listeners:{
					change:	function(){
									actualizaInformacionREP();
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_lic',
				name : 'txt_lic',
				fieldLabel:	'Nombre del Licenciado',
				allowBlank: false,
				maxLength	: 60,
				anchor:	'95%',
				listeners:{
					change:	function(){
									actualizaInformacionREP();
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_not_pub',
				name : 'txt_not_pub',
				fieldLabel:	'N�mero del Notario P�blico',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 15,
				anchor:	'95%',
				listeners:{
					change:	function(){
									actualizaInformacionREP();
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_ori_not',
				name : 'txt_ori_not',
				fieldLabel:	'Ciudad Origen del Notario',
				allowBlank: false,
				maxLength	: 50,
				anchor:	'95%',
				listeners:{
					change:	function(){
									actualizaInformacionREP();
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_pub_com',
				name : 'txt_pub_com',
				fieldLabel:	'Registro P�blico de Comercio',
				allowBlank: false,
				maxLength	: 30,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_folio',
				name : 'txt_folio',
				fieldLabel:	'N�mero de Folio Mercantil',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 20,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_domicilio_legal',
				name : 'txt_domicilio_legal',
				fieldLabel:	'Domicilio Legal Empresa',
				allowBlank: false,
				maxLength	: 350,
				anchor:	'95%'
			},{
				xtype: 'panel',
				title: 'Captura CEDENTE (Representante)'
			},{
				xtype:'radiogroup',
				id:	'radioGp',
				columns: 2,
				allowBlank	: false,
				width			: 240,
				fieldLabel  : '�Son los mismos datos de la Empresa?',
				hidden: false,
				items:[
					{boxLabel: 'Si',id:'id_empSI', name: 'isEmpresa',inputValue:'S',  checked: true, 
						listeners:{
							check:	function(radio){
											if(radio.checked){
												
												disabledCamposREP(true);										
												actualizaInformacionREP(false);
											}
										}
						}
					},
					{boxLabel: 'No',id:'id_empNO', name: 'isEmpresa',inputValue:'N',checked: false,
						listeners:{
							check:	function(radio){
											if (radio.checked){
												disabledCamposREP(false);
												Ext.getCmp('_txt_escritura_rep').setValue('');
												Ext.getCmp('_fecha_esc_pub_rep').setValue('');
												Ext.getCmp('_txt_lic_rep').setValue('');
												Ext.getCmp('_txt_not_pub_rep').setValue('');
												Ext.getCmp('_txt_ori_not_rep').setValue('');
											}
										}
						}
						
					}
				]
			},{
				xtype: 'combo',
				id:		'_cmb_rep',
				hiddenName:		'cmb_rep',
				fieldLabel: 'Tipo de Representante',
				emptyText: 'Seleccione...',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				forceSelection : false,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				editable: true,
				store: storeRepresentante,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_escritura_rep',
				name : 'txt_escritura_rep',
				fieldLabel:	'Escritura P�blica',
				allowBlank: false,
				maskRe:		/[0-9]/,
				maxLength	: 20,
				anchor:	'95%'
			},{
				xtype: 'datefield',
				name: 'fecha_esc_pub_rep',
				id: '_fecha_esc_pub_rep',
				allowBlank: false,
				fieldLabel:	'Fecha Escritura P�blica',
				startDay: 0,
				anchor:	'40%',
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},{
				xtype:	'textfield',
				id:		'_txt_lic_rep',
				name : 'txt_lic_rep',
				fieldLabel:	'Nombre del Licenciado',
				allowBlank: false,
				maxLength	: 60,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_not_pub_rep',
				name : 'txt_not_pub_rep',
				fieldLabel:	'N�mero del Notario P�blico',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 15,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_ori_not_rep',
				name : 'txt_ori_not_rep',
				fieldLabel:	'Ciudad Origen del Notario',
				allowBlank: false,
				maxLength	: 50,
				anchor:	'95%'
			}
			
	];
	
	var fpMod = new Ext.form.FormPanel({
		id:				'formaModificar',
		width:			710,
		style:			'margin:0 auto;',
		collapsible:	true,
		title: 'Modificar CEDENTE (Empresa)',
		titleCollapse:	true,
		frame:			true,
		hidden: true,
		bodyStyle:		'padding: 6px',
		labelWidth:		168,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosFormaModificar,
		monitorValid:	true,
		buttons: [
			{
				text:		'Guardar',
				id:		'btnGuardar',
				iconCls:	'icoGuardar',
				formBind:false,
				handler: function(boton, evento) {
								accionConsulta("MODIFICAR", null);
							}
			},{
				text:		'Cancelar',
				iconCls:	'icoCancelar',
				handler:	function() {
								window.location = "/nafin/34cesion/34if/34consultaCed.jsp"; 
							}
			}
		]
	});
	
//----------------------------------- CONTENEDOR -------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			fpMod,
			NE.util.getEspaciador(20),
			grid
		]
	});
	
	Ext.StoreMgr.key('catalogoPymeDataStore').load();
	
});