<%@ page language="java"%>
<%@ page import=" 
	netropology.utilerias.*,	
	com.netro.cesion.*,			
	netropology.utilerias.ServiceLocator,	
	com.netro.model.catalogos.*,
	java.util.HashMap,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="../34secsession_ext.jspf"%>
<%  
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String clave_pyme = (request.getParameter("clave_pyme")!=null)?request.getParameter("clave_pyme"):"";
String clave_epo = (request.getParameter("clave_epo")!=null)?request.getParameter("clave_epo"):"";
String numero_contrato = (request.getParameter("numero_contrato")!=null)?request.getParameter("numero_contrato"):"";
String clave_moneda = (request.getParameter("clave_moneda")!=null)?request.getParameter("clave_moneda"):"";
String clave_estatus_sol = (request.getParameter("clave_estatus_sol")!=null)?request.getParameter("clave_estatus_sol"):"";
String clave_contratacion = (request.getParameter("clave_contratacion")!=null)?request.getParameter("clave_contratacion"):"";
String fecha_vigencia_ini = (request.getParameter("fecha_vigencia_ini")!=null)?request.getParameter("fecha_vigencia_ini"):"";
String fecha_vigencia_fin = (request.getParameter("fecha_vigencia_fin")!=null)?request.getParameter("fecha_vigencia_fin"):"";
String plazoContrato = (request.getParameter("plazoContrato")!=null)?request.getParameter("plazoContrato"):"";
String claveTipoPlazo = (request.getParameter("claveTipoPlazo")!=null)?request.getParameter("claveTipoPlazo"):"";
String clave_solicitud = (request.getParameter("clave_solicitud")!=null)?request.getParameter("clave_solicitud"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";

String infoRegresar	=	"", consulta  ="";
JSONObject 	jsonObj	= new JSONObject();

CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);


if(informacion.equals("CatalogoEPO")){

	CatalogoEPO cat = new CatalogoEPO();
	cat.setClave("ic_epo");
	cat.setDescripcion("cg_razon_social");
	cat.setCs_cesion_derechos("S"); 
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoPYME")){

	CatalogoPymeCesion cat = new CatalogoPymeCesion();
	cat.setCampoClave("pyme.ic_pyme");
	cat.setCampoDescripcion("pyme.cg_razon_social");		
	cat.setClaveIF(iNoCliente);  
	cat.setOrden("pyme.cg_razon_social");
	infoRegresar = cat.getJSONElementos();  

}else if(informacion.equals("CatalogoMoneda") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setTabla("comcat_moneda");
	cat.setOrden("ic_moneda");
	cat.setValoresCondicionIn("0,1,54,25", Integer.class);
	infoRegresar = cat.getJSONElementos();	
	
} else  if(informacion.equals("CatalogoEstatus")) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_solic");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setTabla("cdercat_estatus_solic");
	cat.setOrden("ic_estatus_solic");
	cat.setValoresCondicionIn("11,12,15,16,17", Integer.class);
	cat.setOrden("cg_descripcion");
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoTipoPlazo") ){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("cdercat_tipo_plazo");
	cat.setCampoClave("ic_tipo_plazo");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("ic_tipo_plazo");
	infoRegresar = cat.getJSONElementos();	

}else if(informacion.equals("CatalogoContratacion")  ){
	
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_tipo_contratacion");
	cat.setCampoDescripcion("cg_nombre");
	cat.setTabla("cdercat_tipo_contratacion");
	cat.setOrden("cg_nombre");
	infoRegresar = cat.getJSONElementos();	
	

}else if(informacion.equals("Consultar")  ){

	ConsultaNotificacionesIF paginador = new ConsultaNotificacionesIF();
	paginador.setClave_pyme(clave_pyme);
	paginador.setClave_epo(clave_epo);
	paginador.setNum_contrato(numero_contrato);
	paginador.setClave_moneda(clave_moneda) ;
	paginador.setClave_estatus(clave_estatus_sol);
	paginador.setClave_contrat(clave_contratacion);
	paginador.setFecha_vigencia_ini(fecha_vigencia_ini);
	paginador.setFecha_vigencia_fin(fecha_vigencia_fin);  
	paginador.setPlazo_contrato(plazoContrato);
	paginador.setClave_plazo_contrato(claveTipoPlazo);
	paginador.setClave_if(iNoCliente);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if(operacion.equals("Generar")) {
		try{
			Registros reg = queryHelper.doSearch();
			while(reg.next()){
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(reg.getString("CLAVE_SOLICITUD"));
				reg.setObject("MONTO_MONEDA",montosMonedaSol.toString());
				//FODEA-024-2014 MOD()
				String pyme_cedente = (reg.getString("PYME_CEDENTE")==null)?"":(reg.getString("PYME_CEDENTE"));	
				String cedente[] = pyme_cedente.split(";");
					if(cedente.length == 1){
						pyme_cedente = pyme_cedente.replaceAll(";","");
						reg.setObject("PYME_CEDENTE",pyme_cedente.toString());					
					}else{
						pyme_cedente = pyme_cedente.replaceAll(";","<br>");
						reg.setObject("PYME_CEDENTE",pyme_cedente.toString());					
					}
					//
			}//while(reg.next()){ 
			consulta = 	"{\"success\": true, \"total\": \""+ reg.getNumeroRegistros() +"\", \"registros\": " + reg.getJSONData()+"}";
			jsonObj = JSONObject.fromObject(consulta);
		}catch(Exception e){
			throw new AppException("Error en la obtenci�n de los datos");
		}
		
	}else if (operacion.equals("ArchivoPDF") ||  operacion.equals("ArchivoCSV") ) {
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArchivo);			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}				
	}
	
	infoRegresar=jsonObj.toString();	
	
}else if(informacion.equals("CONTRATO") ){

	try {
		String 			nombreArchivo = cesionBean.descargaContratoCesion(clave_solicitud, strDirectorioTemp);			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
		System.out.println("infoRegresar -->> "+infoRegresar);			
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo de Contrato", e);
	}		
	
 }else if(informacion.equals("CONTRATO_CESION_IF")  ){
   		
	HashMap parametros = new HashMap();
	parametros.put("clave_solicitud", clave_solicitud);	
	parametros.put("tipo_archivo", tipoArchivo);
	parametros.put("strDirectorioTemp", strDirectorioTemp);
	parametros.put("pais", (String)session.getAttribute("strPais"));
	parametros.put("noCliente", "");
	parametros.put("nombre", (String)session.getAttribute("strNombre"));
	parametros.put("nombreUsr", (String)session.getAttribute("strNombreUsuario"));
	parametros.put("logo", (String)session.getAttribute("strLogo"));
	parametros.put("strDirectorioPublicacion", strDirectorioPublicacion);
	
  String nombreArchivo =cesionBean.archNotifiConCesionIF(parametros);
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString(); 
		
}
	
%>
<%= infoRegresar%>
