Ext.onReady(function(){
	
	var banderaRep2;
	var procesarConsultaRegistros = function(store, registros, opts){
		var jsonData = store.reader.jsonData;
		if (registros != null) {
			if(store.getTotalCount() > 0) {
				var registro = store.getAt(0);
				Ext.getCmp('_cmb_rep').setValue(registro.get('TIPO_REP'));
				Ext.getCmp('_txt_nom_apo').setValue(registro.get('NOM_REP'));
				Ext.getCmp('_txt_fig_jur').setValue(registro.get('FIG_JUR'));
				Ext.getCmp('_txt_escritura').setValue(registro.get('ESC_PUB'));
				Ext.getCmp('_fecha_esc_pub').setValue(registro.get('FEC_ESC_PUB'));
				Ext.getCmp('_txt_lic').setValue(registro.get('NOM_LIC'));
				Ext.getCmp('_txt_not_pub').setValue(registro.get('NOT_PUB'));
				Ext.getCmp('_txt_ori_not').setValue(registro.get('CD_NOT'));
				Ext.getCmp('_txt_pub_com').setValue(registro.get('REG_PUB'));
				Ext.getCmp('_txt_folio').setValue(registro.get('FOLIO'));
				Ext.getCmp('_txt_clabe').setValue(registro.get('CLABE'));
				
				
				if(registro.get('CS_BANDERA_REP')=='S'){
					disabledCamposREP(true);//Deshabilitar componentes del Representante de inicio
					Ext.getCmp('id_empSI').setValue(true);
				}else{
					Ext.getCmp('id_empNO').setValue(true);
				}
				Ext.getCmp('_txt_escritura_rep').setValue(registro.get('ESC_PUB_REP'));
				Ext.getCmp('_fecha_esc_pub_rep').setValue(registro.get('FEC_ESC_PUB_REP'));
				Ext.getCmp('_txt_lic_rep').setValue(registro.get('NOM_LIC_REP'));
				Ext.getCmp('_txt_not_pub_rep').setValue(registro.get('NOT_PUB_REP'));
				Ext.getCmp('_txt_ori_not_rep').setValue(registro.get('CD_NOT_REP'));
				Ext.getCmp('_txt_domicilio_legal').setValue(registro.get('CG_DOMICILIO_LEGAL'));
				
				if(jsonData.registros[0].PODERES_CESION>0){
					Ext.getCmp('poderesIF').setValue('PoderesCesion.pdf');
				}
				if(jsonData.registros[0].PODERES_EXTINCION>0){
					Ext.getCmp('poderesExtincion').setValue('PoderesExtincion.pdf');
				}
				Ext.getCmp('_cmb_rep_2').setValue(jsonData.registros[0].TIPO_REP2);
				Ext.getCmp('_txt_nom_apo_2').setValue(jsonData.registros[0].NOM_REP2);
				Ext.getCmp('_txt_escritura_rep_2').setValue(jsonData.registros[0].ESCRITURA_PUBLICA_REP2);
				Ext.getCmp('_fecha_esc_pub_rep_2').setValue(jsonData.registros[0].FECHA_ESCRITURA_REP2);
				Ext.getCmp('_txt_lic_rep_2').setValue(jsonData.registros[0].NOM_LIC_REP2);
				Ext.getCmp('_txt_not_pub_rep_2').setValue(jsonData.registros[0].NUM_NOTARIO_REP2);
				Ext.getCmp('_txt_ori_not_rep_2').setValue(jsonData.registros[0].CIUDAD_NOTARIO_REP2);				
				CatalogoUsuarios.load();	
				
				if(jsonData.strLogin!=jsonData.registros[0].IF_REP2)  {
					Ext.getCmp('hidClaveIf2').setValue(jsonData.registros[0].IF_REP2);
					Ext.getCmp('clave_if_2').enable();	
				}else  {
					Ext.getCmp('clave_if_2').disable();	
					Ext.getCmp('hidClaveIf2').setValue(jsonData.registros[0].IF_REP2);
				}
				banderaRep2=jsonData.registros[0].BANDERA_REP2;
				
			} else {
				Ext.Msg.alert('Mensaje','No existe Cesionario capturado');
				Ext.getCmp('btnGuardar').disable();
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
	
	function procesarSuccessFailureGuardarRep2(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje','Se guardo con exito el Representante IF 2',function(){location.reload();});
									

		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	var modificarCesionario  = function (grid,rowIndex,colIndex,item){
		
		var registro = grid.getStore().getAt(rowIndex);
		Ext.getCmp('_cmb_rep').setValue(registro.get('TIPO_REP'));
		
	}
	
	function procesarModificarCesionario(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			window.location = "/nafin/34cesion/34if/34consultaCes.jsp"; 
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	
	
	function actualizaInformacionREP(){
		if(Ext.getCmp('id_empSI').getValue()){
			Ext.getCmp('_txt_escritura_rep').setValue(Ext.getCmp('_txt_escritura').getValue());
			Ext.getCmp('_fecha_esc_pub_rep').setValue(Ext.getCmp('_fecha_esc_pub').getValue());
			Ext.getCmp('_txt_lic_rep').setValue(Ext.getCmp('_txt_lic').getValue());
			Ext.getCmp('_txt_not_pub_rep').setValue(Ext.getCmp('_txt_not_pub').getValue());
			Ext.getCmp('_txt_ori_not_rep').setValue(Ext.getCmp('_txt_ori_not').getValue());
		}
	}
	
	function disabledCamposREP(bandera){
		Ext.getCmp('_txt_escritura_rep').setDisabled(bandera);
		Ext.getCmp('_fecha_esc_pub_rep').setDisabled(bandera);
		Ext.getCmp('_txt_lic_rep').setDisabled(bandera);
		Ext.getCmp('_txt_not_pub_rep').setDisabled(bandera);
		Ext.getCmp('_txt_ori_not_rep').setDisabled(bandera);
	}
	

/*----------------------------- Maquina de Edos. ----------------------------*/
		var accionConsulta = function(estadoSiguiente, respuesta){

			if( estadoSiguiente == "MODIFICAR" ){
				if(Ext.getCmp('_cmb_rep').getValue()=='' || Ext.getCmp('_txt_fig_jur').getValue()=='' || Ext.getCmp('_txt_nom_apo').getValue()==''
					|| Ext.getCmp('_txt_escritura').getValue()=='' || Ext.getCmp('_fecha_esc_pub').getValue()=='' || Ext.getCmp('_txt_lic').getValue()=='' 
					|| Ext.getCmp('_txt_not_pub').getValue()=='' || Ext.getCmp('_txt_ori_not').getValue()=='' || Ext.getCmp('_txt_pub_com').getValue()==''
					|| Ext.getCmp('_txt_folio').getValue()=='' || Ext.getCmp('_txt_clabe').getValue()==''
					|| Ext.getCmp('_txt_escritura_rep').getValue()=='' || Ext.getCmp('_fecha_esc_pub_rep').getValue()=='' 
					|| Ext.getCmp('_txt_lic_rep').getValue()=='' || Ext.getCmp('_txt_not_pub_rep').getValue()=='' || Ext.getCmp('_txt_ori_not_rep').getValue()==''
					|| Ext.getCmp('_txt_domicilio_legal').getValue()==''){
					
						if(Ext.getCmp('_cmb_rep').getValue()=='' )
							Ext.getCmp('_cmb_rep').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_nom_apo').getValue()=='' )
							Ext.getCmp('_txt_nom_apo').markInvalid("Campo obligatorio");
						if(Ext.getCmp('_txt_fig_jur').getValue()=='' )
							Ext.getCmp('_txt_fig_jur').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_txt_escritura').getValue()=='')
							Ext.getCmp('_txt_escritura').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_fecha_esc_pub').getValue()=='')
							Ext.getCmp('_fecha_esc_pub').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_lic').getValue()=='')
							Ext.getCmp('_txt_lic').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_not_pub').getValue()=='')
							Ext.getCmp('_txt_not_pub').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_txt_ori_not').getValue()=='')
							Ext.getCmp('_txt_ori_not').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_txt_pub_com').getValue()=='')
							Ext.getCmp('_txt_pub_com').markInvalid("Campo obligatorio");		
						else if(Ext.getCmp('_txt_folio').getValue()=='')
							Ext.getCmp('_txt_folio').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_clabe').getValue()=='')
							Ext.getCmp('_txt_clabe').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_domicilio_legal').getValue()=='')
							Ext.getCmp('_txt_domicilio_legal').markInvalid("Campo obligatorio");
					
				}else{
					bandera = true;
					var fechaEP = Ext.getCmp('_fecha_esc_pub');
					if(fechaEP.getValue()!=""){
						var fechaDe = Ext.util.Format.date(fechaEP.getValue(),'d/m/Y');
						if(!isdate(fechaDe)){
							fechaEP.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							bandera = false;
						}
					}if(bandera){
					
						if(!fpMod.getForm().isValid()){
							return;
						}
						Ext.Msg.confirm('', '�Est� seguro de querer enviar su informaci�n?',	function(botonConf) {
							if (botonConf == 'ok' || botonConf == 'yes'){
								fpMod.getForm().submit({
									clientValidation: 	true,
									url: 						'34capturaCes.data.jsp?informacion=ModificarCesionario',
									waitMsg:   				'Guardando informaci�n.',
									waitTitle: 				'Por favor espere',
									success: function(form, action) {
										Ext.Msg.alert('Mensaje','El Cesionaro se guard� con �xito.',function(){
											location.reload();
										});
										
									},
									failure:NE.util.mostrarSubmitError
								});
							}
						});
					}
				}
				
			}else if(estadoSiguiente == "GUARDAR_REP2"){
				
				if(!fpIF2.getForm().isValid()){
					return;
				}
				Ext.Ajax.request({
									url: '34capturaCes.data.jsp',
									params:	Ext.apply(fpIF2.getForm().getValues(),
											{
												informacion:'representanteIF2'
											}
									),
									callback: procesarSuccessFailureGuardarRep2
								});
			}else if(	estadoSiguiente == "LIMPIAR"){
				fpIF2.getForm().reset();
				fpMod.getForm().reset();
			}
			
		}
		
/*---------------------------------- Store's --------------------------------*/


	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34capturaCes.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var procesarCatalogoUsuariosData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var clave_if_2 = Ext.getCmp('clave_if_2');
			var hidClaveIf2 = Ext.getCmp('hidClaveIf2');				
			if(hidClaveIf2.getValue()!=''){
				clave_if_2.setValue(hidClaveIf2.getValue());
			}
		}
  }
  
	var CatalogoUsuarios = new Ext.data.JsonStore({
		id: 'CatalogoUsuarios',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34capturaCes.data.jsp',
		baseParams: {
			informacion: 'CatalogoUsuarios'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatalogoUsuariosData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'34capturaCes.data.jsp',
			baseParams: {	informacion:	'ConsultarCesionario'	},
			fields: [
				{ name: 'TIPO_REP'},
				{ name: 'NOM_REP'},
				{ name: 'FIG_JUR'},
				{ name: 'ESC_PUB'},
				{ name: 'FEC_ESC_PUB'},
				{ name: 'NOM_LIC'},
				{ name: 'NOT_PUB'},
				{ name: 'CD_NOT'},
				{ name: 'REG_PUB'},
				{ name: 'FOLIO'},
				{ name: 'CLABE'},
				{ name: 'ESC_PUB_REP'},
				{ name: 'FEC_ESC_PUB_REP'},
				{ name: 'NOM_LIC_REP'},
				{ name: 'NOT_PUB_REP'},
				{ name: 'CD_NOT_REP'},
				{ name: 'CS_BANDERA_REP'},
				{ name: 'CG_DOMICILIO_LEGAL'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null,null);
					}
				}
			}
	});
	
	var storeRepresentante = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
    data : [ ['L','Representante Legal']
				 ,['A','Apoderado']
			  ]
	});
	
	
//------------------------------- COMPONENTES ---------------------------------

var elementosFormaRep2 = [
			{
				xtype:'radiogroup',
				id:	'radioGpRep_2',
				columns: 2,
				allowBlank	: false,
				width			: 240,
				fieldLabel  : '',
				hidden: false,
				items:[
					{boxLabel: 'Representante IF 1',id:'id_rep1_2', name: 'isRep',inputValue:'S',  checked: false, 
						listeners:{
							check:	function(radio){
											if(radio.checked){
												radio.setValue('N');
												fpIF2.setVisible(false);
												fpMod.setVisible(true);
												
											}
										}
						}
					},
					{boxLabel: 'Representante IF 2',id:'id_rep_2', name: 'isRep',inputValue:'N',checked: true,
						listeners:{
							check:	function(radio){
											if (radio.checked){
												
											}
										}
						}
						
					}
				]
			},{
				xtype: 'combo',
				id:		'_cmb_rep_2',
				hiddenName:		'cmb_rep',
				fieldLabel: 'Tipo de Representante',
				emptyText: 'Seleccione...',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				forceSelection : false,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				allowBlank: false,
				editable: true,
				store: storeRepresentante,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_nom_apo_2',
				name : 'txt_nom_apo',
				fieldLabel:	'Nombre del Representante o Apoderado',
				allowBlank: false,
				maxLength	: 80,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_escritura_rep_2',
				name : 'txt_escritura_rep',
				fieldLabel:	'Escritura P�blica',
				allowBlank: false,
				maskRe:		/[0-9]/,
				maxLength	: 20,
				anchor:	'95%'
			},{
				xtype: 'datefield',
				name: 'fecha_esc_pub_rep',
				id: '_fecha_esc_pub_rep_2',
				allowBlank: false,
				fieldLabel:	'Fecha Escritura P�blica',
				startDay: 0,
				anchor:	'40%',
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},{
				xtype:	'textfield',
				id:		'_txt_lic_rep_2',
				name : 'txt_lic_rep',
				fieldLabel:	'Nombre del Licenciado',
				allowBlank: false,
				maxLength	: 60,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_not_pub_rep_2',
				name : 'txt_not_pub_rep',
				fieldLabel:	'N�mero del Notario P�blico',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 15,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_ori_not_rep_2',
				name : 'txt_ori_not_rep',
				fieldLabel:	'Ciudad Origen del Notario',
				allowBlank: false,
				maxLength	: 50,
				anchor:	'95%'
			},
			{
				xtype     : 'label',
				text: 'Nota: El Representante IF2 que seleccione, tendr� la facultad para Autorizar y Firmar el Contrato.',
				style:  'text-align:left;margin:0px;color:black;',
				width     : 200
			},
			{
			xtype: 'combo',
			name: 'clave_if',
			id: 'clave_if_2',
			fieldLabel: 'Representante IF2',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_if',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : CatalogoUsuarios,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'hidClaveIf2', 	value: '' }	
	];

	
	var elementosFormaModificar = [
			{
						xtype: 'displayfield',
						value: 'DATOS CESIONARIO',
						width: 35
			},
			{
				xtype:	'textfield',
				id:		'_txt_fig_jur',
				name : 'txt_fig_jur',
				fieldLabel:	'Figura Juridica',
				allowBlank: false,
				maxLength	: 80,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_escritura',
				name : 'txt_escritura',
				fieldLabel:	'Escritura P�blica',
				allowBlank: false,
				maskRe:		/[0-9]/,
				maxLength	: 20,
				anchor:	'95%',  
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}
			},{
				xtype: 'datefield',
				name: 'fecha_esc_pub',
				id: '_fecha_esc_pub',
				allowBlank: false,
				fieldLabel:	'Fecha Escritura P�blica',
				startDay: 0,
				anchor:	'40%',
				msgTarget: 'side',
				margins: '0 20 0 0',  
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_lic',
				name : 'txt_lic',
				fieldLabel:	'Nombre del Licenciado',
				allowBlank: false,
				maxLength	: 60,
				anchor:	'95%',  
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_not_pub',
				name : 'txt_not_pub',
				fieldLabel:	'N�mero del Notario P�blico',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 15,
				anchor:	'95%',  
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_ori_not',
				name : 'txt_ori_not',
				fieldLabel:	'Ciudad Origen del Notario',
				allowBlank: false,
				maxLength	: 50,
				anchor:	'95%',  
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_pub_com',
				name : 'txt_pub_com',
				fieldLabel:	'Registro P�blico de Comercio',
				allowBlank: false,
				maxLength	: 30,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_folio',
				name : 'txt_folio',
				fieldLabel:	'N�mero de Folio Mercantil',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 20,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_domicilio_legal',
				name : 'txt_domicilio_legal',
				fieldLabel:	'Domicilio Legal IF',
				allowBlank: false,
				maxLength	: 350,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_clabe',
				name : 'txt_clabe',
				fieldLabel:	'Cuenta Clabe',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 20,
				anchor:	'95%'
			},{
				xtype: 'panel',
				title: 'Captura CESIONARIO (Representante IF)'
			},
			{
				xtype:'radiogroup',
				id:	'radioGpRep',
				columns: 2,
				allowBlank	: false,
				width			: 240,
				fieldLabel  : '',
				hidden: false,
				items:[
					{boxLabel: 'Representante IF 1',id:'id_rep1', name: 'isRep',inputValue:'S',  checked: true, 
						listeners:{
							check:	function(radio){
											if(radio.checked){
												
												
											}
										}
						}
					},
					{boxLabel: 'Representante IF 2',id:'id_rep2', name: 'isRep',inputValue:'N',checked: false,
						listeners:{
							check:	function(radio){
											if (radio.checked){
												radio.setValue('S');
												fpIF2.setVisible(true);
												fpMod.setVisible(false);
												if(banderaRep2!='S'){
													Ext.Msg.alert('Mensaje','Debe  de Capturar el Representante IF 2 para modificarlo.');
													Ext.getCmp('btnGuardar_2').setDisabled(true);
												}
												
											}
										}
						}
						
					}
				]
			},
			
			{
				xtype:'radiogroup',
				id:	'radioGp',
				columns: 2,
				allowBlank	: false,
				width			: 240,
				fieldLabel  : '�Son los mismos datos del IF?',
				hidden: false,
				items:[
					{boxLabel: 'Si',id:'id_empSI', name: 'isEmpresa',inputValue:'S',  checked: true, 
						listeners:{
							check:	function(radio){
											if(radio.checked){
												
												disabledCamposREP(true);										
												actualizaInformacionREP(false);
											}
										}
						}
					},
					{boxLabel: 'No',id:'id_empNO', name: 'isEmpresa',inputValue:'N',checked: false,
						listeners:{
							check:	function(radio){
											if (radio.checked){
												disabledCamposREP(false);
												Ext.getCmp('_txt_escritura_rep').setValue('');
												Ext.getCmp('_fecha_esc_pub_rep').setValue('');
												Ext.getCmp('_txt_lic_rep').setValue('');
												Ext.getCmp('_txt_not_pub_rep').setValue('');
												Ext.getCmp('_txt_ori_not_rep').setValue('');
											}
										}
						}
						
					}
				]
			},{
				xtype: 'combo',
				id:		'_cmb_rep',
				hiddenName:		'cmb_rep',
				fieldLabel: 'Tipo de Representante',
				emptyText: 'Seleccione...',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				forceSelection : false,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				editable: true,
				store: storeRepresentante,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_nom_apo',
				name : 'txt_nom_apo',
				fieldLabel:	'Nombre del Representante o Apoderado',
				allowBlank: false,
				maxLength	: 80,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_escritura_rep',
				name : 'txt_escritura_rep',
				fieldLabel:	'Escritura P�blica',
				allowBlank: false,
				maskRe:		/[0-9]/,
				maxLength	: 20,
				anchor:	'95%'
			},{
				xtype: 'datefield',
				name: 'fecha_esc_pub_rep',
				id: '_fecha_esc_pub_rep',
				allowBlank: false,
				fieldLabel:	'Fecha Escritura P�blica',
				startDay: 0,
				anchor:	'40%',
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},{
				xtype:	'textfield',
				id:		'_txt_lic_rep',
				name : 'txt_lic_rep',
				fieldLabel:	'Nombre del Licenciado',
				allowBlank: false,
				maxLength	: 60,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_not_pub_rep',
				name : 'txt_not_pub_rep',
				fieldLabel:	'N�mero del Notario P�blico',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 15,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_ori_not_rep',
				name : 'txt_ori_not_rep',
				fieldLabel:	'Ciudad Origen del Notario',
				allowBlank: false,
				maxLength	: 50,
				anchor:	'95%'
			},
			{
				xtype: 'panel',
				title: 'Captura CESIONARIO (Representante IF)'
			},
			{
				xtype: 'fileuploadfield',
	  			id: 'poderesIF',
			   width: 150,	  
				emptyText: 'Ruta del archivo',
				fieldLabel: 'Carga de Poderes Cesi�n',
				name: 'archivoCesion',
				buttonText: 'Examinar...',
				regex:	/^.+\.([pP][dD][fF])$/,
				regexText:'El archivo debe tener extensi�n .pdf',
				buttonCfg: {
					  //iconCls: 'upload-icon',
					  width: 90
				}
			},
			{
				xtype: 'fileuploadfield',
	  			id: 'poderesExtincion',
			   width: 150,	  
				emptyText: 'Ruta del Contrato',
				fieldLabel: 'Carga de Poderes Extinci�n',
				name: 'archivoExtincion',
				regex:	/^.+\.([pP][dD][fF])$/,
				regexText:'El archivo debe tener extensi�n .pdf',
				buttonText: 'Examinar...',
				buttonCfg: {
					  //iconCls: 'upload-icon'
					  width: 90
				}
			},
			{
				xtype     : 'label',
				text: 'Nota: La carga de poderes IF no ser� Obligatoria y solo se mostrar� en el Cesionario 1',
				style:  'text-align:left;margin:0px;color:black;',
				width     : 200
			}
			
	];
	
	var fpMod = new Ext.form.FormPanel({
		id:				'formaModificar',
		width:			710,
		style:			'margin:0 auto;',
		collapsible:	true,
		title: 'Modificar CESIONARIO (IF)',
		titleCollapse:	true,
		frame:			true,
		fileUpload: 	true,
		bodyStyle:		'padding: 6px',
		labelWidth:		168,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosFormaModificar,
		monitorValid:	true,
		buttons: [
			{
				text:		'Guardar',
				id:		'btnGuardar',
				iconCls:	'icoGuardar',
				formBind:false,
				handler: function(boton, evento) {
								accionConsulta("MODIFICAR", null);
							}
			},{
				text:		'Cancelar',
				iconCls:	'icoCancelar',
				handler:	function() {
								window.location = "/nafin/01principal/01if/index.jsp";
							}
			}
		]
	});
	
	var fpIF2 = new Ext.form.FormPanel({
		id:				'forma_2',
		width:			710,
		style:			'margin:0 auto;',
		collapsible:	true,
		title: 'Captura Representante IF 2',
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		168,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosFormaRep2,
		hidden: true,
		monitorValid:	true,
		buttons: [
			{
				text:		'Guardar',
				id:		'btnGuardar_2',
				iconCls:	'icoGuardar',
				formBind:false,
				handler: function(boton, evento) {
								if(!fpIF2.getForm().isValid()){
									return;
								}
								accionConsulta("GUARDAR_REP2", null);
							}
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR", null);
							}
			}
		]
	});
//----------------------------------- CONTENEDOR -------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fpMod,fpIF2
		]
	});
	
	
	Ext.StoreMgr.key('registrosConsultadosDataStore').load();
	
	
});