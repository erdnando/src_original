<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.text.*,
	java.util.*,
	java.io.File,
	org.apache.commons.logging.Log,
	com.netro.pdf.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.CatalogoPymeCuenBan,
	com.netro.model.catalogos.CatalogoPymeEpoCesion,
	com.netro.model.catalogos.CatalogoEPO_CesionDerechos,
	com.netro.cesion.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion  = request.getParameter("informacion")    == null?"":(String)request.getParameter("informacion");
String ic_epo       = request.getParameter("ic_epo")         == null?"":(String)request.getParameter("ic_epo");
String ic_pyme      = request.getParameter("ic_pyme")        == null?"":(String)request.getParameter("ic_pyme");
String no_contrato  = request.getParameter("no_contrato_id") == null?"":(String)request.getParameter("no_contrato_id");
String fecha_inicio = request.getParameter("fecha_inicio")   == null?"":(String)request.getParameter("fecha_inicio");
String fecha_final  = request.getParameter("fecha_final")    == null?"":(String)request.getParameter("fecha_final");
String ic_solicitud = request.getParameter("ic_solicitud")   == null?"":(String)request.getParameter("ic_solicitud");
String nombre_pyme  = request.getParameter("nombre_pyme")    == null?"":(String)request.getParameter("nombre_pyme");

String consulta      = "";
String mensaje       = "";
String nombreArchivo = "";
String infoRegresar  = "";
JSONObject resultado = new JSONObject();
boolean success      = true;

log.debug("<<<informacion: " + informacion + ">>>>"); 

if(informacion.equals("CatalogoEPO")){

	CatalogoEPO_CesionDerechos cat = new CatalogoEPO_CesionDerechos();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CatalogoPYME")){

/* Este es el catalogo que utilizaba la pantalla original
	CatalogoPymeCuenBan cat = new CatalogoPymeCuenBan();
	cat.setCampoClave("pyme.ic_pyme");
	cat.setCampoDescripcion("pyme.cg_razon_social");
	cat.setClaveIF(iNoCliente);
	cat.setCesion("S");
	cat.setOrden("pyme.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
*/

	// Este catálogo solo obtiene las PYMES afiliadas a una EPO y que son suceptibles a cesión de derechos
	CatalogoPymeEpoCesion cat = new CatalogoPymeEpoCesion();
	cat.setCampoClave("C.IC_PYME");
	cat.setCampoDescripcion("P.CG_RAZON_SOCIAL");
	if(!ic_epo.equals("")){
		cat.setIcEpo(ic_epo);
	}
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Consultar_Cedente")){

	ConsultaCedente paginador = new ConsultaCedente();
	paginador.setIcEpo(ic_epo);
	paginador.setIcPyme(ic_pyme);
	paginador.setNoContrato(no_contrato);
	paginador.setFechaInicio(fecha_inicio);
	paginador.setFechaFin(fecha_final);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros listaRegistros = new Registros();

	try{
		listaRegistros = queryHelper.doSearch();
		consulta = listaRegistros.getJSONData();
		resultado.put("total", "" + listaRegistros.getNumeroRegistros());
		resultado.put("registros", consulta);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error en la paginacion", e);
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consultar_Representante")){

	String nombresRepresentante = "";
	String usuarioTmp = "";
	netropology.utilerias.usuarios.Usuario usuario = null;
	netropology.utilerias.usuarios.UtilUsr utilUsr = new netropology.utilerias.usuarios.UtilUsr();
	
	CapturaCedenteRepLegal paginador = new CapturaCedenteRepLegal();
	paginador.setIcPyme(ic_pyme);
	paginador.setIcSolicitud(ic_solicitud);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros listaRegistros = new Registros();
	try{
		listaRegistros = queryHelper.doSearch();
		while(listaRegistros.next()){ //Agrego las columnas faltantes
			// Obtengo el nombre del representante legal
			usuarioTmp = listaRegistros.getString("IC_USUARIO");
			if(!usuarioTmp.equals("")){
				usuario = utilUsr.getUsuario(usuarioTmp);
				nombresRepresentante = usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
				listaRegistros.setObject("NOMBRE_REPRESENTANTE", nombresRepresentante);
			} else{
				listaRegistros.setObject("NOMBRE_REPRESENTANTE", "");
			}
			//Edito el campo tipo de representante:  L = Representante Legal, A = Apoderado
			if((listaRegistros.getString("CG_TIPO_REPRESENTANTE")).equals("A")){
				listaRegistros.setObject("CG_TIPO_REPRESENTANTE", "Apoderado");
			} else if((listaRegistros.getString("CG_TIPO_REPRESENTANTE")).equals("L")){
				listaRegistros.setObject("CG_TIPO_REPRESENTANTE", "Representante Legal");
			}
		}
		consulta = listaRegistros.getJSONData();
		resultado.put("total", "" + listaRegistros.getNumeroRegistros());
		resultado.put("registros", consulta);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error en la consulta", e);
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("ArchivoCSV")){

	ConsultaCedente paginador = new ConsultaCedente();
	paginador.setIcEpo(ic_epo);
	paginador.setIcPyme(ic_pyme);
	paginador.setNoContrato(no_contrato);
	paginador.setFechaInicio(fecha_inicio);
	paginador.setFechaFin(fecha_final);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try{
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e){
		success = false;
		throw new AppException("Error al generar el archivo CSV", e);
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("ArchivoCSV_Rep")){

	CapturaCedenteRepLegal paginador = new CapturaCedenteRepLegal();
	paginador.setIcPyme(ic_pyme);
	paginador.setIcSolicitud(ic_solicitud);
	paginador.setNombrePyme(nombre_pyme);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try{
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e){
		success = false;
		throw new AppException("Error al generar el archivo CSV", e);
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

}

%>
<%=infoRegresar%>