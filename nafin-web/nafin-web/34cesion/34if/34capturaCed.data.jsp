<%@ page language="java"%>
<%@ page import=" netropology.utilerias.ServiceLocator,  
java.util.*, com.netro.cesion.*, 
com.netro.exception.*,  
com.netro.model.catalogos.*,
netropology.utilerias.*,
net.sf.json.JSONArray,net.sf.json.JSONObject"
contentType="application/json;charset=UTF-8"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="../34secsession_ext.jspf"%>
<%  
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar 	= "";

if (informacion.equals( "CatalogoPYME" )){
	CatalogoPymeCuenBan cat = new CatalogoPymeCuenBan();
	cat.setCampoClave("pyme.ic_pyme");
	cat.setCampoDescripcion("pyme.cg_razon_social");
	cat.setClaveIF(iNoCliente);
	cat.setCesion("S");
	cat.setOrden("pyme.cg_razon_social");
	
	infoRegresar = cat.getJSONElementos(); 
	
}else if(informacion.equals( "GuardarCedente" ) || informacion.equals( "ModificarCedente" ) || informacion.equals("eliminarCedente")){
	
	CesionEJB BeanCesionEJB = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	
	JSONObject resultado = new JSONObject();
	
	String num_pyme	=	(request.getParameter("cmb_pyme")!=null)?request.getParameter("cmb_pyme"):"",
			 tipo_rep	=	(request.getParameter("cmb_rep")!=null)?request.getParameter("cmb_rep"):"",
			 tip_ced	=	(request.getParameter("cmb_cedente")!=null)?request.getParameter("cmb_cedente"):"",
			 fig_jud	=	(request.getParameter("txt_fig_jur")!=null)?request.getParameter("txt_fig_jur"):"",
			 esc_pub	=	(request.getParameter("txt_escritura")!=null)?request.getParameter("txt_escritura"):"",
			 fec_esc	=	(request.getParameter("fecha_esc_pub")!=null)?request.getParameter("fecha_esc_pub"):"",
			 nom_lic	=	(request.getParameter("txt_lic")!=null)?request.getParameter("txt_lic"):"",
			 num_not	=	(request.getParameter("txt_not_pub")!=null)?request.getParameter("txt_not_pub"):"",
			 cd_not	=	(request.getParameter("txt_ori_not")!=null)?request.getParameter("txt_ori_not"):"",
			 reg_pub	=	(request.getParameter("txt_pub_com")!=null)?request.getParameter("txt_pub_com"):"",
			 folio	=	(request.getParameter("txt_folio")!=null)?request.getParameter("txt_folio"):"",
			 
			 esc_pub_rep	=	(request.getParameter("txt_escritura_rep")!=null)?request.getParameter("txt_escritura_rep"):"",
			 fec_esc_rep	=	(request.getParameter("fecha_esc_pub_rep")!=null)?request.getParameter("fecha_esc_pub_rep"):"",
			 nom_lic_rep	=	(request.getParameter("txt_lic_rep")!=null)?request.getParameter("txt_lic_rep"):"",
			 num_not_rep	=	(request.getParameter("txt_not_pub_rep")!=null)?request.getParameter("txt_not_pub_rep"):"",
			 cd_not_rep	=	(request.getParameter("txt_ori_not_rep")!=null)?request.getParameter("txt_ori_not_rep"):"",
			 banderaRep	=	(request.getParameter("isEmpresa")!=null)?request.getParameter("isEmpresa"):"",
			 domicilioLegal=	(request.getParameter("txt_domicilio_legal")!=null)?request.getParameter("txt_domicilio_legal"):"";
			 
			 
			 if(banderaRep.equals("S")){
				esc_pub_rep =esc_pub;
				fec_esc_rep = fec_esc;
				nom_lic_rep = nom_lic;
				num_not_rep = num_not;
				cd_not_rep = cd_not;
			 }
	
	if(informacion.equals( "GuardarCedente" ) ){
		try{
			BeanCesionEJB.guardaCedente(iNoCliente, num_pyme, tipo_rep, tip_ced, fig_jud, esc_pub, fec_esc, nom_lic, num_not, cd_not, reg_pub, folio,
			esc_pub_rep,fec_esc_rep,nom_lic_rep,num_not_rep,cd_not_rep,banderaRep,domicilioLegal);
		
			resultado.put("success", new Boolean(true));
		}catch(Exception e) {
			resultado.put("success", new Boolean(false));
			throw new AppException("Error al guardar el Cedente", e);
		}
		
		infoRegresar = resultado.toString();
		
	}else if(informacion.equals( "ModificarCedente" )){
	
		String num_pyme_ant	=	(request.getParameter("pyme_ant")!=null)?request.getParameter("pyme_ant"):"";
		
		BeanCesionEJB.modificarCedente(iNoCliente, num_pyme, num_pyme_ant, tipo_rep, tip_ced, fig_jud, esc_pub, fec_esc, nom_lic, num_not, cd_not, reg_pub, folio,
		esc_pub_rep,fec_esc_rep,nom_lic_rep,num_not_rep,cd_not_rep,banderaRep,domicilioLegal);
		
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();
	
	}else if(informacion.equals("eliminarCedente")){
		String ic_pyme	=	(request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
		
		BeanCesionEJB.eliminaCedente(iNoCliente, ic_pyme);
		
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();
	}
	

}else if(informacion.equals( "ConsultarCedente" ) || informacion.equals("ArchivoPDF") || informacion.equals("ArchivoCSV") ){

	String num_pyme	=	(request.getParameter("cmb_pyme")!=null)?request.getParameter("cmb_pyme"):"";
	String consulta = "";
	JSONObject resultado = new JSONObject();
	
	ConsCedCesion paginador = new ConsCedCesion();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
	
	paginador.setCve_pyme(num_pyme);
	
	if(informacion.equals( "ConsultarCedente" )){
		try {
			Registros reg	=	queryHelper.doSearch();
			//FODEA-024-2014 MOD()
			while(reg.next()){
				String pyme = reg.getString("PYME");
				String []pymeAux = pyme.split(";");
				if(pymeAux.length == 1){
					pyme = pyme.replaceAll(";","");
					reg.setObject("PYME",pyme);
				}else{
					pyme = pyme.replaceAll(";","<br>");
					reg.setObject("PYME",pyme);
				}
			}
			//
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			 
		}	catch(Exception e) {
				throw new AppException("Error en la paginaci�n", e);
		}
	
		resultado = JSONObject.fromObject(consulta);
		infoRegresar = resultado.toString();
		
	}else if(informacion.equals("ArchivoPDF")){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = resultado.toString();
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		
	}else if(informacion.equals("ArchivoCSV")){
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	

			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = resultado.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
		
	}
	
}else if(informacion.equals("ExisteCedente")){

	CesionEJB BeanCesionEJB = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	JSONObject resultado = new JSONObject();
	boolean resp;
	
	String ic_pyme	=	(request.getParameter("cve_pyme")!=null)?request.getParameter("cve_pyme"):"";
		
	resp = BeanCesionEJB.existeCedente(iNoCliente, ic_pyme);
	
	resultado.put("success", new Boolean(resp));
	infoRegresar = resultado.toString();
	
}
%>
<%= infoRegresar%>
