<%@ page language="java"%>
<%@ page import=" netropology.utilerias.ServiceLocator,  
java.util.*, com.netro.cesion.*, 
com.netro.exception.*,  
com.netro.model.catalogos.*,
netropology.utilerias.*,

		java.io.File,
		org.apache.commons.logging.Log,
		org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileUploadException,

net.sf.json.JSONArray,net.sf.json.JSONObject"
errorPage="/00utils/error.jsp"%>
<%@ include file="../34secsession_ext.jspf"%>
<%!

	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	private final int MAX_PLANTILLA_FILE_SIZE = 52428800; // 50 MB   

%>
<%  
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar 	= "";

CesionEJB BeanCesionEJB = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
	
if(informacion.equals( "GuardarCesionario") || informacion.equals("ModificarCesionario")){

	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType);
	request.setAttribute("myContentType", myContentType);

	// Variables adicionales
	String directorioTemporal = strDirectorioTemp ;
	String login = iNoUsuario;
	
	// Variables de la Respuesta
	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;
 
	// Create a factory for disk-based file items
	DiskFileItemFactory 	factory 			= new DiskFileItemFactory(DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD, new File(directorioTemporal) );
	// Create a new file upload handler
	ServletFileUpload 	upload 			= new ServletFileUpload(factory);
	List						fileItemList	= null; // List of FileItem
	try {
		
		// Definir el tamaño máximo que pueden tener los archivos
		upload.setSizeMax(MAX_PLANTILLA_FILE_SIZE);
		// Parsear request
		fileItemList = upload.parseRequest(request);
		
	} catch(Throwable t) {
			
		success		= false;
		log.error("Exception: Cargar en memoria o disco el contenido del archivo");
		t.printStackTrace();
		if( t instanceof SizeLimitExceededException  ) {
			throw new AppException("El Archivo es muy Grande, excede el límite que es de " + ( MAX_PLANTILLA_FILE_SIZE / 1048576 ) + " MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}

String tipo_rep = "";
String nom_rep = "";
String fig_jud = "";
String esc_pub = "";
String fec_esc = "";
String nom_lic = "";
String num_not = "";
String cd_not = "";
String reg_pub = "";
String folio = "";
String clabe = "";
String esc_pub_rep = "";
String fec_esc_rep = "";
String nom_lic_rep = "";
String num_not_rep = "";
String cd_not_rep = "";
String banderaRep = "";
String domicilioLegal= "";

	// Leer parametros
	for(int i=0;i<fileItemList.size();i++){
		FileItem fileItem = (FileItem) fileItemList.get(i);
		 if(fileItem.isFormField() && "cmb_rep".equals(fileItem.getFieldName()) ){
			tipo_rep = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_nom_apo".equals(fileItem.getFieldName()) ){
			nom_rep = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_fig_jur".equals(fileItem.getFieldName()) ){
			fig_jud = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_escritura".equals(fileItem.getFieldName()) ){
			esc_pub = fileItem.getString();
		} else if(fileItem.isFormField() && "fecha_esc_pub".equals(fileItem.getFieldName()) ){
			fec_esc = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_lic".equals(fileItem.getFieldName()) ){
			nom_lic = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_not_pub".equals(fileItem.getFieldName()) ){
			num_not = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_ori_not".equals(fileItem.getFieldName()) ){
			cd_not = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_pub_com".equals(fileItem.getFieldName()) ){
			reg_pub = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_folio".equals(fileItem.getFieldName()) ){
			folio = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_clabe".equals(fileItem.getFieldName()) ){
			clabe = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_escritura_rep".equals(fileItem.getFieldName()) ){
			esc_pub_rep = fileItem.getString();
		} else if(fileItem.isFormField() && "fecha_esc_pub_rep".equals(fileItem.getFieldName()) ){
			fec_esc_rep = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_lic_rep".equals(fileItem.getFieldName()) ){
			nom_lic_rep = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_not_pub_rep".equals(fileItem.getFieldName()) ){
			num_not_rep = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_ori_not_rep".equals(fileItem.getFieldName()) ){
			cd_not_rep = fileItem.getString();
		} else if(fileItem.isFormField() && "isEmpresa".equals(fileItem.getFieldName()) ){
			banderaRep = fileItem.getString();
		} else if(fileItem.isFormField() && "txt_domicilio_legal".equals(fileItem.getFieldName()) ){
			domicilioLegal = fileItem.getString();
		} else if(!fileItem.isFormField() && "archivoCesion".equals(fileItem.getFieldName())){
			// Guardar Archivo en Disco
			try {

				// Generar Nombre del Archivo
				String fileName = Comunes.cadenaAleatoria(16)+".pdf";
				
				// Guardar Archivo en Disco
				File uploadedFile = new File( directorioTemporal + login + "." + fileName );
				fileItem.write(uploadedFile);
				resultado.put("fileName", fileName );
			} catch(Throwable e) {
				success = false;
				log.error("(Exception): Guardar Archivo en Disco");
				e.printStackTrace();
				throw new AppException("Ocurrió un error al guardar el archivo");
			}
		} else if(!fileItem.isFormField() && "archivoExtincion".equals(fileItem.getFieldName())){
			// Guardar Archivo en Disco
			try {

				// Generar Nombre del Archivo
				String fileName = Comunes.cadenaAleatoria(16)+".pdf";
				
				// Guardar Archivo en Disco
				File uploadedFile = new File( directorioTemporal + login + "." + fileName );
				fileItem.write(uploadedFile);
				resultado.put("fileName", fileName );
			} catch(Throwable e) {
				success = false;
				log.error("(Exception): Guardar Archivo en Disco");
				e.printStackTrace();
				throw new AppException("Ocurrió un error al guardar el archivo");
			}
		}
	}

	if(banderaRep.equals("S")){
		esc_pub_rep =esc_pub;
		fec_esc_rep = fec_esc;
		nom_lic_rep = nom_lic;
		num_not_rep = num_not;
		cd_not_rep = cd_not;
	}
	int numeroArchivos=0;
	String nombrePoderesIF="";
	String nombreExtincionIF="";

	if(informacion.equals( "GuardarCesionario")){
		try{

			BeanCesionEJB.guardaCesionario(iNoCliente, tipo_rep, nom_rep, fig_jud, esc_pub, fec_esc, nom_lic, num_not, cd_not, reg_pub, folio, clabe,
			esc_pub_rep,fec_esc_rep,nom_lic_rep,num_not_rep,cd_not_rep,banderaRep,domicilioLegal,strDirectorioTemp,nombrePoderesIF,nombreExtincionIF);

			resultado.put("success", new Boolean(true));
		}catch(Exception e){
			resultado.put("success", new Boolean(false));
			throw new AppException("Error al guardar el Cedente", e);
		}

		infoRegresar = resultado.toString();

	}else if(informacion.equals( "ModificarCesionario" )){

		BeanCesionEJB.modificarCesionario(iNoCliente, tipo_rep, nom_rep, fig_jud, esc_pub, fec_esc, nom_lic, num_not, cd_not, reg_pub, folio, clabe,
		esc_pub_rep,fec_esc_rep,nom_lic_rep,num_not_rep,cd_not_rep,banderaRep,domicilioLegal,strDirectorioTemp,nombrePoderesIF,nombreExtincionIF);

		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();

	}


} else if(informacion.equals( "GuardarCesionario_" ) || informacion.equals( "ModificarCesionario_" ) ){
	
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
	
	boolean success;
	
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(10000000);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		//strSerialrror("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 10000000) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 10 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
	}
			
	
	
	JSONObject resultado = new JSONObject();
	
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	
	String tipo_rep	=	(myRequest.getParameter("cmb_rep")!=null)?myRequest.getParameter("cmb_rep"):"",
			 nom_rep	=	(myRequest.getParameter("txt_nom_apo")!=null)?myRequest.getParameter("txt_nom_apo"):"",
			 fig_jud	=	(myRequest.getParameter("txt_fig_jur")!=null)?myRequest.getParameter("txt_fig_jur"):"",
			 esc_pub	=	(myRequest.getParameter("txt_escritura")!=null)?myRequest.getParameter("txt_escritura"):"",
			 fec_esc	=	(myRequest.getParameter("fecha_esc_pub")!=null)?myRequest.getParameter("fecha_esc_pub"):"",
			 nom_lic	=	(myRequest.getParameter("txt_lic")!=null)?myRequest.getParameter("txt_lic"):"",
			 num_not	=	(myRequest.getParameter("txt_not_pub")!=null)?myRequest.getParameter("txt_not_pub"):"",
			 cd_not	=	(myRequest.getParameter("txt_ori_not")!=null)?myRequest.getParameter("txt_ori_not"):"",
			 reg_pub	=	(myRequest.getParameter("txt_pub_com")!=null)?myRequest.getParameter("txt_pub_com"):"",
			 folio	=	(myRequest.getParameter("txt_folio")!=null)?myRequest.getParameter("txt_folio"):"",
			 clabe	=	(myRequest.getParameter("txt_clabe")!=null)?myRequest.getParameter("txt_clabe"):"",
			 
			 esc_pub_rep	=	(myRequest.getParameter("txt_escritura_rep")!=null)?myRequest.getParameter("txt_escritura_rep"):"",
			 fec_esc_rep	=	(myRequest.getParameter("fecha_esc_pub_rep")!=null)?myRequest.getParameter("fecha_esc_pub_rep"):"",
			 nom_lic_rep	=	(myRequest.getParameter("txt_lic_rep")!=null)?myRequest.getParameter("txt_lic_rep"):"",
			 num_not_rep	=	(myRequest.getParameter("txt_not_pub_rep")!=null)?myRequest.getParameter("txt_not_pub_rep"):"",
			 cd_not_rep	=	(myRequest.getParameter("txt_ori_not_rep")!=null)?myRequest.getParameter("txt_ori_not_rep"):"",
			 banderaRep	=	(myRequest.getParameter("isEmpresa")!=null)?myRequest.getParameter("isEmpresa"):"",
			 domicilioLegal=	(myRequest.getParameter("txt_domicilio_legal")!=null)?myRequest.getParameter("txt_domicilio_legal"):"";
			 if(banderaRep.equals("S")){
				esc_pub_rep =esc_pub;
				fec_esc_rep = fec_esc;
				nom_lic_rep = nom_lic;
				num_not_rep = num_not;
				cd_not_rep = cd_not;
			 }
		int numeroArchivos=0;
		String nombrePoderesIF="",nombreExtincionIF="";
		try {
		
		numeroArchivos=myUpload.getFiles().getCount();
		if(numeroArchivos>0){
			com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
			if(!myFile.isMissing()){
				nombrePoderesIF = Comunes.cadenaAleatoria(16)+"."+myFile.getFileExt();
				myFile.saveAs(strDirectorioTemp   + nombrePoderesIF);
				if(myFile.getFieldName().equals("archivoExtincion")){
					nombreExtincionIF = nombrePoderesIF;
					nombrePoderesIF="";
				}
			}
			
		}
		if(numeroArchivos>1){
			com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(1);
			if(!myFile.isMissing()){
				nombreExtincionIF = Comunes.cadenaAleatoria(16)+"."+myFile.getFileExt();
				myFile.saveAs(strDirectorioTemp + nombreExtincionIF	 );
			}
		}
		
	}catch(Exception e){
		success		= false;
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
			 
	
	if(informacion.equals( "GuardarCesionario" ) ){
		try{
			
			BeanCesionEJB.guardaCesionario(iNoCliente, tipo_rep, nom_rep, fig_jud, esc_pub, fec_esc, nom_lic, num_not, cd_not, reg_pub, folio, clabe,
			esc_pub_rep,fec_esc_rep,nom_lic_rep,num_not_rep,cd_not_rep,banderaRep,domicilioLegal,strDirectorioTemp,nombrePoderesIF,nombreExtincionIF);
		
			resultado.put("success", new Boolean(true));
		}catch(Exception e) {
			resultado.put("success", new Boolean(false));
			throw new AppException("Error al guardar el Cedente", e);
		}
		
		infoRegresar = resultado.toString();
		
	}else if(informacion.equals( "ModificarCesionario" )){
		
		BeanCesionEJB.modificarCesionario(iNoCliente, tipo_rep, nom_rep, fig_jud, esc_pub, fec_esc, nom_lic, num_not, cd_not, reg_pub, folio, clabe,
		esc_pub_rep,fec_esc_rep,nom_lic_rep,num_not_rep,cd_not_rep,banderaRep,domicilioLegal,strDirectorioTemp,nombrePoderesIF,nombreExtincionIF);
		
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();
	
	}

}else if(informacion.equals("representanteIF2")){

		JSONObject resultado = new JSONObject();
		String tipo_rep	=	(request.getParameter("cmb_rep")!=null)?request.getParameter("cmb_rep"):"";
		String representante	=	(request.getParameter("txt_nom_apo")!=null)?request.getParameter("txt_nom_apo"):"";
		String escritura_pub	=	(request.getParameter("txt_escritura_rep")!=null)?request.getParameter("txt_escritura_rep"):"";
		String fecha_escritura	=	(request.getParameter("fecha_esc_pub_rep")!=null)?request.getParameter("fecha_esc_pub_rep"):"";
		String txt_lic_rep	=	(request.getParameter("txt_lic_rep")!=null)?request.getParameter("txt_lic_rep"):"";
		String num_notario	=	(request.getParameter("txt_not_pub_rep")!=null)?request.getParameter("txt_not_pub_rep"):"";
		String ciudad_origen	=	(request.getParameter("txt_ori_not_rep")!=null)?request.getParameter("txt_ori_not_rep"):"";
		String claveIf2	=	(request.getParameter("clave_if")!=null)?request.getParameter("clave_if"):"";

		BeanCesionEJB.modificarRepresentanteIF2(iNoCliente,tipo_rep,representante,escritura_pub,fecha_escritura,txt_lic_rep,num_notario,ciudad_origen,claveIf2);
		resultado.put("success", new Boolean(true));
		resultado.put("strLogin",strLogin );
		infoRegresar = resultado.toString();

}else if(informacion.equals( "ConsultarCesionario" ) ){

	String consulta = "";
	JSONObject resultado = new JSONObject();
	
	ConsCesCesion paginador = new ConsCesCesion();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
	
	paginador.setCve_if(iNoCliente);
	
	try {
		Registros reg	=	queryHelper.doSearch();
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		 
	}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
	}
	
	resultado = JSONObject.fromObject(consulta);
	resultado.put("strLogin",strLogin );
	infoRegresar = resultado.toString();
	
}else if(informacion.equals("CatalogoIF")   ){

	CatalogoIF cat = new CatalogoIF();
	cat.setClave("ic_if");
	cat.setDescripcion("cg_razon_social");
	cat.setG_cs_cesion_derechos("S");
	infoRegresar = cat.getJSONElementos();	
	
}else if(informacion.equals("CatalogoUsuarios") ){

	String tipoAfiliado 	= "I";	
	String perfil = "ADMIN IF";
	JSONObject 	jsonObj	= new JSONObject();
	String consulta="";
	JSONArray registros = new JSONArray();	
	List lUsuarios = BeanCesionEJB.getComboTestigos(tipoAfiliado, iNoCliente, perfil, strLogin );
	registros = registros.fromObject(lUsuarios);
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();
		
}else if(informacion.equals( "ExisteCesionario" )){
	
	JSONObject resultado = new JSONObject();
	boolean resp;
	
	resp = BeanCesionEJB.existeCesionario(iNoCliente);
	resultado.put("rep2",new Boolean(BeanCesionEJB.existeRepresentante2(iNoCliente)));
	resultado.put("success", new Boolean(resp));
	infoRegresar = resultado.toString();
}
%>
<%= infoRegresar%>
