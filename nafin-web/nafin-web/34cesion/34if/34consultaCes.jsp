<% String version = (String)session.getAttribute("version"); %>
<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../34secsession.jspf" %>
<html>
<head>
<title>Nafi@net </title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="34consultaCes.js?<%=session.getId()%>"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
                <div id="_menuApp"></div>
                <div id="Contcentral">
                <%if(version!=null){%>
                <%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
                <%}%>
                <div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
                </div>
                </div>
                <%if(version!=null){%>
                <%@ include file="/01principal/01nafin/pie.jspf"%>
                <%}%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
