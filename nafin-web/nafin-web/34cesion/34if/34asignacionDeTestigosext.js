Ext.onReady(function() {
	
	//************Eliminacion de registro****************
	
	var procesoModificar = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);	
		var testigo = registro.get('TESTIGO');	
		Ext.getCmp("testigo1").setValue(testigo);
		Ext.getCmp("modificar").setValue("S");
	}
	
	//************Eliminacion de registro****************
	var resultadoEliminacion = function (opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			
			if(jsonValoresIniciales != null){	
					
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'						
					})
				});					
		
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}
	
	var procesoEliminar = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);	
		var testigo = registro.get('TESTIGO');
		
		Ext.getCmp("modificar").setValue("");
		if(!confirm("�Seguro que desea eliminar el registro permanentemente?")) {
			return;
		}else {			
			Ext.Ajax.request({
				url: '34asignacionDeTestigos.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Eliminar',
					testigo:testigo
				}),
				callback: resultadoEliminacion
			});
		}	
	}
	
//************Guardar de registro****************
	var procesaGuardar = function (opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if(jsonValoresIniciales != null){	
					
				var mensaje = jsonValoresIniciales.mensaje;	
				if(mensaje !='') {
					Ext.MessageBox.alert('Mensaje',mensaje);
				}else  {		
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'						
						})
					});	
				}		
				Ext.getCmp("modificar").setValue("");	
				Ext.getCmp("testigo1").setValue("");
				
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}
	
	//************Consulta de registros****************
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();					
									
			if(store.getTotalCount() > 0) {					
				el.unmask();							
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '34asignacionDeTestigos.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'TESTIGO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	
	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Asignaci�n de Testigos',
		columns: [	
			{
				header: 'Testigo',
				tooltip: 'Testigo',
				dataIndex: 'TESTIGO',
				sortable: true,
				width: 350,			
				resizable: true,				
				align: 'left'	
			},
			{
				xtype: 'actioncolumn',
				header: 'Acci�n',
				tooltip: 'Acci�n',
				width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'modificar';												
						},
						handler: procesoModificar
					},
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[1].tooltip = 'Borrar';
							return 'borrar';												
						},
						handler: procesoEliminar
					}
				]				
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 300,
		width: 510,	
		frame: true		
	});
	
		
	// **** Forma ************
	
	var catalogoTestigoData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '34asignacionDeTestigos.data.jsp',
		baseParams: {
			informacion: 'CatalogoTestigo'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {	
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var  elementosForma = [
		{ 	xtype: 'textfield',hidden: true,  id: 'modificar', 	value: '' },
		{
			xtype: 'combo',
			name: 'testigo',
			id: 'testigo1',
			fieldLabel: 'Testigo',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'testigo',
			emptyText: 'Seleccionar...',
			forceSelection: false,
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			allowBlank: true,
			store: catalogoTestigoData,
			tpl: NE.util.templateMensajeCargaCombo
		}	
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Criterios de Captura',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 120,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Guardar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {	
					
					var testigo =  Ext.getCmp("testigo1");
					var modificar =  Ext.getCmp("modificar");
					
					if (Ext.isEmpty(testigo.getValue()) ){
						testigo.markInvalid('Por favor selecciona el testigo');
						return;
					}					
					
					if (Ext.isEmpty(modificar.getValue()) ){		
					
						fp.el.mask('Enviando...', 'x-mask-loading');	
					
						Ext.Ajax.request({
							url: '34asignacionDeTestigos.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Guardar'				
							}),
							callback: procesaGuardar
						});
					}
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function(){
					window.location = '34asignacionDeTestigosext.jsp';
				}
			}
		]
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		heigth: 'auto',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		items: [
			fp,
			NE.util.getEspaciador(20),	
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});
	
	
	var valIniciales = function(){
		fp.el.mask('Cargando ...', 'x-mask-loading');	
		catalogoTestigoData.load();
		consultaData.load({
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Consultar'						
			})
		});	
	}
	
	valIniciales();
	
});