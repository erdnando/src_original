<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.text.*,
	java.util.*,
	java.io.File,
	org.apache.commons.logging.Log,
	com.netro.pdf.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.CatalogoEPOCedente,
	com.netro.model.catalogos.CatalogoNumContrato,
	com.netro.cesion.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion      = request.getParameter("informacion")               == null?"":(String)request.getParameter("informacion");
String operacion        = request.getParameter("operacion")                 == null?"":(String)request.getParameter("operacion");
String icEpo            = request.getParameter("ic_epo")                    == null?"":(String)request.getParameter("ic_epo");
String icSolicitud      = request.getParameter("ic_solicitud")               == null?"":(String)request.getParameter("ic_solicitud");
String icPyme           = request.getParameter("ic_pyme")                   == null?"":(String)request.getParameter("ic_pyme");
String noContrato       = request.getParameter("no_contrato")               == null?"":(String)request.getParameter("no_contrato");
String tipoCedente      = request.getParameter("tipo_cedente")              == null?"":(String)request.getParameter("tipo_cedente");
String figuraJuridica   = request.getParameter("figura_juridica")           == null?"":(String)request.getParameter("figura_juridica");
String escrituraPublica = request.getParameter("escritura_publica")         == null?"":(String)request.getParameter("escritura_publica");
String fechaEscritura   = request.getParameter("fecha_escritura")           == null?"":(String)request.getParameter("fecha_escritura");
String nombreLicenciado = request.getParameter("nombre_licenciado")         == null?"":(String)request.getParameter("nombre_licenciado");
String notarioPublico   = request.getParameter("no_notario_publico")        == null?"":(String)request.getParameter("no_notario_publico");
String ciudadOrigen     = request.getParameter("ciudad_origen")             == null?"":(String)request.getParameter("ciudad_origen");
String registroPublico  = request.getParameter("registro_publico_comercio") == null?"":(String)request.getParameter("registro_publico_comercio");
String folioMercantil   = request.getParameter("folio_mercantil")           == null?"":(String)request.getParameter("folio_mercantil");
String domicilioEmpresa = request.getParameter("domicilio_empresa")         == null?"":(String)request.getParameter("domicilio_empresa");
String estatus          = request.getParameter("estatus")                   == null?"":(String)request.getParameter("estatus");
String datosGrid        = request.getParameter("datosGrid")                 == null?"":(String)request.getParameter("datosGrid");

String consulta      = "";
String mensaje       = "";
String infoRegresar  = "";
JSONObject resultado = new JSONObject();
boolean success      = true;

log.debug("<<<informacion: " + informacion + ">>>>"); 

if(informacion.equals("Catalogo_Epo")){

	CatalogoEPOCedente cat = new CatalogoEPOCedente();
	cat.setCampoClave("DISTINCT C.IC_EPO");
	cat.setCampoDescripcion("E.CG_RAZON_SOCIAL");
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Catalogo_No_Contrato")){

	CatalogoNumContrato cat = new CatalogoNumContrato();
	cat.setIcEpo(icEpo);
	cat.setCampoClave("CG_NO_CONTRATO");
	cat.setCampoDescripcion("CG_NO_CONTRATO");
	cat.setEstatus("1,2,3,13,24"); // TABLA: cdercat_estatus_solic CAMPO: 1 (Por solicitar), 2 (En proceso) o 3 (Solicitud Aceptada)
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("Consultar")){

	CapturaCedente paginador = new CapturaCedente();
	paginador.setIcEpo(icEpo);
	paginador.setNoContrato(noContrato);//"ABC_01"
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros listaRegistros = new Registros();

	try{
		listaRegistros = queryHelper.doSearch();
		consulta = listaRegistros.getJSONData();
		resultado.put("total", "" + listaRegistros.getNumeroRegistros());
		resultado.put("registros", consulta);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error en la paginacion", e);
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consulta_Datos_Constitutivo")){

	HashMap datos = new HashMap();
	CapturaCedente captura = new CapturaCedente();
	try{
		captura.setIcPyme(icPyme);
		captura.setIcSolicitud(icSolicitud);
		datos = captura.getCderSolicPymeNotariales();
		if(datos == null){
			resultado.put("pyme", icPyme);
			resultado.put("solicitud", icSolicitud);
			resultado.put("estatus", "NUEVO");
		} else{
			resultado.put("estatus", "MODIFICADO");
			resultado.put("registros", datos);
		}
	} catch(Exception e) {
		throw new AppException("Error al realizar la consulta Datos Constitutivo", e);
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consultar_Representantes")){

	String nombresRepresentante = "";
	String usuarioTmp = "";
	netropology.utilerias.usuarios.Usuario usuario = null;
	netropology.utilerias.usuarios.UtilUsr utilUsr = new netropology.utilerias.usuarios.UtilUsr();
	
	CapturaCedenteRepLegal paginador = new CapturaCedenteRepLegal();
	paginador.setIcPyme(icPyme);
	paginador.setIcSolicitud(icSolicitud);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros listaRegistros = new Registros();
	try{
		listaRegistros = queryHelper.doSearch();
		while(listaRegistros.next()){ //Agrego las columnas faltantes
			// Obtengo el nombre del representante legal
			usuarioTmp = listaRegistros.getString("IC_USUARIO");
			if(!usuarioTmp.equals("")){
				usuario = utilUsr.getUsuario(usuarioTmp);
				nombresRepresentante = usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
				listaRegistros.setObject("NOMBRE_REPRESENTANTE", nombresRepresentante);
			} else{
				listaRegistros.setObject("NOMBRE_REPRESENTANTE", "");
			}
			//Edito el campo tipo de representante:  L = Representante Legal, A = Apoderado
			if((listaRegistros.getString("CG_TIPO_REPRESENTANTE")).equals("A")){
				listaRegistros.setObject("CG_TIPO_REPRESENTANTE", "Apoderado");
			} else if((listaRegistros.getString("CG_TIPO_REPRESENTANTE")).equals("L")){
				listaRegistros.setObject("CG_TIPO_REPRESENTANTE", "Representante Legal");
			}
		}
		consulta = listaRegistros.getJSONData();
		resultado.put("total", "" + listaRegistros.getNumeroRegistros());
		resultado.put("registros", consulta);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error en la consulta", e);
	}
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("Guarda_Datos_Constitutivo")){

	try{
		CapturaCedente captura = new CapturaCedente();
		captura.setIcSolicitud(icSolicitud);
		captura.setIcPyme(icPyme);
		captura.setTipoCedente(tipoCedente);
		captura.setFiguraJuridica(figuraJuridica);
		captura.setEscrituraPublica(escrituraPublica);
		captura.setDfEscrituraPublica(fechaEscritura);
		captura.setNombreLicenciado(nombreLicenciado);
		captura.setNumNotarioPublico(notarioPublico);
		captura.setOrigenNotario(ciudadOrigen);
		captura.setNumFolioMercantil(folioMercantil);
		captura.setRegPubComercio(registroPublico);
		captura.setDomicilioLegal(domicilioEmpresa);
		captura.setEstatus(estatus);

		if(estatus.equals("NUEVO")){
			success = captura.insertCderSolicPymeNotariales();
		} else if(estatus.equals("MODIFICADO")){
			success = captura.updateCderSolicPymeNotariales();
		}
		if(success == false){
			mensaje = captura.getMensaje();
		}
	}catch(Exception e){
		success = false;
		mensaje = "Error al actualizar los datos." + e;
		throw new AppException("Error al guardar el Cedente", e);
	}

	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

} else if(informacion.equals("Guarda_Datos_Representante")){

	try{

		JSONArray arrayRepresentantes = JSONArray.fromObject(datosGrid);
		List representantes = new ArrayList();
		HashMap mapaRep = new HashMap();
		for(int i=0; i<arrayRepresentantes.size(); i++){
			JSONObject auxiliar = arrayRepresentantes.getJSONObject(i);
			mapaRep = new HashMap();
			mapaRep.put("IC_SOLICITUD",            auxiliar.getString("IC_SOLICITUD")           );
			mapaRep.put("IC_PYME",                 auxiliar.getString("IC_PYME")                );
			mapaRep.put("IC_USUARIO",              auxiliar.getString("IC_USUARIO")             );
			mapaRep.put("NOMBRE_REPRESENTANTE",    auxiliar.getString("NOMBRE_REPRESENTANTE")   );
			mapaRep.put("CG_TIPO_REPRESENTANTE",   auxiliar.getString("CG_TIPO_REPRESENTANTE")  );
			mapaRep.put("CG_NOMBRE_LICENCIADO",    auxiliar.getString("CG_NOMBRE_LICENCIADO")   );
			mapaRep.put("ESCRITURA_PUBLICA",       auxiliar.getString("ESCRITURA_PUBLICA")      );
			mapaRep.put("FECHA_ESCRITURA_PUBLICA", auxiliar.getString("FECHA_ESCRITURA_PUBLICA"));
			mapaRep.put("NUM_NOTARIO_PUBLICO",     auxiliar.getString("NUM_NOTARIO_PUBLICO")    );
			mapaRep.put("CG_ORIGEN_NOTARIO",       auxiliar.getString("CG_ORIGEN_NOTARIO")      );
			representantes.add(mapaRep);
		}

		CapturaCedenteRepLegal captura = new CapturaCedenteRepLegal();
		captura.setRepresentantes(representantes);
		success = captura.updateRepresentanteLegal();
		if(success == false){
			mensaje = captura.getMensaje();
		}

	} catch(Throwable e) {
		success = false;
		mensaje = "Error al actualizar los representantes legales. " + e;
		throw new AppException(e);
	}
	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

}
%>
<%=infoRegresar%>