Ext.onReady(function() {
var paramSubmit;
TaskLocation = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
]);
//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT

//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT

	var consultaData =  new Ext.data.GroupingStore({
		root : 'registros',
		url : '34parametrizacionClausuladoext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
	fields: [
			{name: 'CD_NOMBRE'},
			{name: 'IC_TASA'},
			{name: 'FN_VALOR'},
			{name: 'FECHA'}
		]
		}),
		groupField: 'FECHA',
		sortInfo:{field: 'FECHA', direction: "ASC"},
			
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			/*beforeLoad:	{
				fn: function(store, options){
					var noEpo = Ext.getCmp('cmbEpo');
					Ext.apply(options.params, {no_epo: noEpo.value});
				}
			},*/
			//load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
		
	});
	var elementosForma = [
		{
			xtype: 'textarea',
			maxLength: 10000,
			name: 'clausulado',
			id: 'idClausulado',
			hiddenName : 'clausulado',
			anchor: '100%',
			height: 270,
			msgTarget: 'under',
			enableKeyEvents: true,
			allowBlank:false
		}
	];

	var fp = {
		xtype: 'form',
		id: 'forma',
		width: 600,
		collapsible: true,
		frame:true,
		titleCollapse: false,
		title: 'Clausulado del Contrato',
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		labelWidth: 1,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosForma],
		monitorValid: true,
		buttons: [
			{
				text: 'Guardar',
				id: 'btnBuscar',
				iconCls: 'icoGuardar',
				formBind: true,
				handler: function(boton, evento) {
					var cmpForma = Ext.getCmp('forma');
					//paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					
					paramSubmit = cmpForma.getForm().submit({  
						url : '34parametrizacionClausuladoext.data.jsp',  
		            waitMsg : 'Guardando Clausulado...',  
						waitTitle: 'Espere',
		            failure: function (form, action) {  
	                Ext.MessageBox.show({  
	                    title: 'Error',  
	                    msg: 'Error al guardar el Clausulado.',  
	                    buttons: Ext.MessageBox.OK,  
	                    icon: Ext.MessageBox.ERROR  
	                });  
						},  
						success: function (form, request) {  
							Ext.MessageBox.show({  
								title: 'Clausulado guardado correctamente',  
								msg: 'Clausulado guardado correctamente',  
								buttons: Ext.MessageBox.OK,  
								icon: Ext.MessageBox.INFO  
							});  
							responseData = Ext.util.JSON.decode(request.response.responseText);  
							consultaData.load({
								params: Ext.apply(paramSubmit,{
									operacion: 'Consultar'							
								}),  
								waitMsg : 'Espere por favor...'  
							});
						}  
					});  
				} //fin handler
			},{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34parametrizacionClausuladoext.jsp';
				}
			}
		]
	};//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(5)
		]
	});
});