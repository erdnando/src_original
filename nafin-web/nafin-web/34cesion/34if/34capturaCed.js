Ext.onReady(function(){

var bandera = true;

	function procesarSuccessFailureGuardar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje','Se guardo con exito el Cedente...');
			accionConsulta("LIMPIAR", null);
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}
	
	function procesarExisteCedente(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje','Ya existe Cedente capturado para esta PYME...');
			Ext.getCmp('_cmb_pyme').reset();
		}	
	}
	
	function actualizaInformacionREP(){
		if(Ext.getCmp('id_empSI').getValue()){
			Ext.getCmp('_txt_escritura_rep').setValue(Ext.getCmp('_txt_escritura').getValue());
			Ext.getCmp('_fecha_esc_pub_rep').setValue(Ext.getCmp('_fecha_esc_pub').getValue());
			Ext.getCmp('_txt_lic_rep').setValue(Ext.getCmp('_txt_lic').getValue());
			Ext.getCmp('_txt_not_pub_rep').setValue(Ext.getCmp('_txt_not_pub').getValue());
			Ext.getCmp('_txt_ori_not_rep').setValue(Ext.getCmp('_txt_ori_not').getValue());
		}
	}
	
	function disabledCamposREP(bandera){
		Ext.getCmp('_txt_escritura_rep').setDisabled(bandera);
		Ext.getCmp('_fecha_esc_pub_rep').setDisabled(bandera);
		Ext.getCmp('_txt_lic_rep').setDisabled(bandera);
		Ext.getCmp('_txt_not_pub_rep').setDisabled(bandera);
		Ext.getCmp('_txt_ori_not_rep').setDisabled(bandera);
	}

/*----------------------------- Maquina de Edos. ----------------------------*/
		var accionConsulta = function(estadoSiguiente, respuesta){

			if(  estadoSiguiente == "GUARDAR" ){
				if(Ext.getCmp('_cmb_pyme').getValue()=='' || Ext.getCmp('_txt_fig_jur').getValue()=='' || Ext.getCmp('_cmb_rep').getValue()==''
					|| Ext.getCmp('_cmb_cedente').getValue()=='' || Ext.getCmp('_txt_escritura').getValue()=='' || Ext.getCmp('_fecha_esc_pub').getValue()=='' 
					|| Ext.getCmp('_txt_lic').getValue()=='' || Ext.getCmp('_txt_not_pub').getValue()=='' || Ext.getCmp('_txt_ori_not').getValue()=='' 
					|| Ext.getCmp('_txt_pub_com').getValue()=='' || Ext.getCmp('_txt_folio').getValue()==''
					|| Ext.getCmp('_txt_escritura_rep').getValue()=='' || Ext.getCmp('_fecha_esc_pub_rep').getValue()=='' 
					|| Ext.getCmp('_txt_lic_rep').getValue()=='' || Ext.getCmp('_txt_not_pub_rep').getValue()=='' || Ext.getCmp('_txt_ori_not_rep').getValue()==''
					|| Ext.getCmp('_txt_domicilio_legal').getValue()==''){
					
						if(Ext.getCmp('_cmb_pyme').getValue()=='')
							Ext.getCmp('_cmb_pyme').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_cmb_rep').getValue()=='')
							Ext.getCmp('_cmb_rep').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_cmb_cedente').getValue()=='')
							Ext.getCmp('_cmb_cedente').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_fig_jur').getValue()=='' )
							Ext.getCmp('_txt_fig_jur').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_txt_escritura').getValue()=='')
							Ext.getCmp('_txt_escritura').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_fecha_esc_pub').getValue()=='')
							Ext.getCmp('_fecha_esc_pub').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_lic').getValue()=='')
							Ext.getCmp('_txt_lic').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_not_pub').getValue()=='')
							Ext.getCmp('_txt_not_pub').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_txt_ori_not').getValue()=='')
							Ext.getCmp('_txt_ori_not').markInvalid("Campo obligatorio");	
						else if(Ext.getCmp('_txt_pub_com').getValue()=='')
							Ext.getCmp('_txt_pub_com').markInvalid("Campo obligatorio");		
						else if(Ext.getCmp('_txt_folio').getValue()=='')
							Ext.getCmp('_txt_folio').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_txt_domicilio_legal').getValue()=='')
							Ext.getCmp('_txt_domicilio_legal').markInvalid("Campo obligatorio");
					
				}else{
					bandera = true;
					var fechaEP = Ext.getCmp('_fecha_esc_pub');
					if(fechaEP.getValue()!=""){
						var fechaDe = Ext.util.Format.date(fechaEP.getValue(),'d/m/Y');
						if(!isdate(fechaDe)){
							fechaEP.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							bandera = false;
						}
					}
					if(bandera){
						Ext.Msg.confirm('', 'Esta seguro de querer enviar su informaci�n...? ',	function(botonConf) {
							if (botonConf == 'ok' || botonConf == 'yes') {
								Ext.Ajax.request({
									url: '34capturaCed.data.jsp',
									params:	Ext.apply(fp.getForm().getValues(),
											{
												informacion:'GuardarCedente'
											}
									),
									callback: procesarSuccessFailureGuardar
								});
							}
						});
					}
				}
				
			}else if(	estadoSiguiente == "LIMPIAR"){
				Ext.getCmp('forma').getForm().reset();
			}
			
		}
		
/*---------------------------------- Store's --------------------------------*/
	
	var storeRepresentante = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
    data : [ ['L','Representante Legal']
				 ,['A','Apoderado']
			  ]
	});
	
	var storeTipoCedente = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
    data : [ ['C','Contratista']
				 ,['P','Proveedor']
			  ]
	});
	
	//Catalogo para el combo de seleccion de Pyme
	var catalogoPymeData = new Ext.data.JsonStore({
		id:				'catalogoPymeDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'34capturaCed.data.jsp',
		baseParams:		{	informacion: 'CatalogoPYME'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
//------------------------------- COMPONENTES ---------------------------------

	//Elementos que van dentro del formulario de datos para realizar la consulta
		var elementosFormaConsulta = [
			{
						xtype: 'displayfield',
						value: 'DATOS CEDENTE',
						width: 35
			},{
				xtype: 'combo',
				id: '_cmb_pyme',
				fieldLabel: 'Pyme',
				mode: 'local',
				hiddenName : 'cmb_pyme',
				emptyText: 'Seleccione...',
				forceSelection : true,
				triggerAction : 'all',
				editable: true,
				typeAhead: true,
				allowBlank: false,
				minChars : 1,
				valueField		: 'clave',
				displayField	:'descripcion',
				store: catalogoPymeData,
				anchor:	'95%',
				tpl: NE.util.templateMensajeCargaCombo,
				listeners :{select: function(combo, record, index) {
									Ext.Ajax.request({
														url: '34capturaCed.data.jsp',
														params: {
																informacion: 'ExisteCedente',
																cve_pyme: combo.getValue()
														},
														callback: procesarExisteCedente
									});
								}
				}
			},{
				xtype: 'combo',
				id:	'_cmb_cedente',
				fieldLabel: 'Tipo de Cedente',
				editable: true,
				emptyText: 'Seleccione...',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				hiddenName : 'cmb_cedente',
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store: storeTipoCedente,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_fig_jur',
				name : 'txt_fig_jur',
				fieldLabel:	'Figura Juridica',
				allowBlank: false,
				maxLength	: 80,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_escritura',
				name : 'txt_escritura',
				fieldLabel:	'Escritura P�blica',
				allowBlank: false,
				maskRe:		/[0-9]/,
				maxLength	: 20,
				anchor:	'95%',  
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}
			},{
				xtype: 'datefield',
				name: 'fecha_esc_pub',
				id: '_fecha_esc_pub',
				allowBlank: false,
				fieldLabel:	'Fecha Escritura P�blica',
				startDay: 0,
				anchor:	'40%',
				msgTarget: 'side',
				margins: '0 20 0 0',  //necesario para mostrar el icono de error
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_lic',
				name : 'txt_lic',
				fieldLabel:	'Nombre del Licenciado',
				allowBlank: false,
				maxLength	: 60,
				anchor:	'95%',
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}

			},{
				xtype:	'textfield',
				id:		'_txt_not_pub',
				name : 'txt_not_pub',
				fieldLabel:	'N�mero del Notario P�blico',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 15,
				anchor:	'95%',
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_ori_not',
				name : 'txt_ori_not',
				fieldLabel:	'Ciudad Origen del Notario',
				allowBlank: false,
				maxLength	: 50,
				anchor:	'95%',
				listeners:{
					change:	function(){
									actualizaInformacionREP(false);
								}
				}
			},{
				xtype:	'textfield',
				id:		'_txt_pub_com',
				name : 'txt_pub_com',
				fieldLabel:	'Registro P�blico de Comercio',
				allowBlank: false,
				maxLength	: 30,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_folio',
				name : 'txt_folio',
				fieldLabel:	'N�mero de Folio Mercantil',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 20,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_domicilio_legal',
				name : 'txt_domicilio_legal',
				fieldLabel:	'Domicilio Legal Empresa',
				allowBlank: false,
				maxLength	: 350,
				anchor:	'95%'
			},{
				xtype: 'panel',
				title: 'Captura CEDENTE (Representante)'
			},{
				xtype:'radiogroup',
				id:	'radioGp',
				columns: 2,
				allowBlank	: false,
				width			: 240,
				fieldLabel  : '�Son los mismos datos de la Empresa?',
				hidden: false,
				items:[
					{boxLabel: 'Si',id:'id_empSI', name: 'isEmpresa',inputValue:'S',  checked: true, 
						listeners:{
							check:	function(radio){
											
											if(radio.checked){
												
												disabledCamposREP(true);										
												actualizaInformacionREP(false);
											}
										}
						}
					},
					{boxLabel: 'No',id:'id_EmpNO', name: 'isEmpresa',inputValue:'N',checked: false,
						listeners:{
							check:	function(radio){
											
											if (radio.checked){
												disabledCamposREP(false);
												Ext.getCmp('_txt_escritura_rep').setValue('');
												Ext.getCmp('_fecha_esc_pub_rep').setValue('');
												Ext.getCmp('_txt_lic_rep').setValue('');
												Ext.getCmp('_txt_not_pub_rep').setValue('');
												Ext.getCmp('_txt_ori_not_rep').setValue('');
											}
										}
						}
						
					}
				]
			},{
				xtype: 'combo',
				id:		'_cmb_rep',
				hiddenName:		'cmb_rep',
				fieldLabel: 'Tipo de Representante',
				emptyText: 'Seleccione...',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				forceSelection : false,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				editable: true,
				store: storeRepresentante,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_escritura_rep',
				name : 'txt_escritura_rep',
				fieldLabel:	'Escritura P�blica',
				allowBlank: false,
				maskRe:		/[0-9]/,
				maxLength	: 20,
				anchor:	'95%'
			},{
				xtype: 'datefield',
				name: 'fecha_esc_pub_rep',
				id: '_fecha_esc_pub_rep',
				allowBlank: false,
				fieldLabel:	'Fecha Escritura P�blica',
				startDay: 0,
				anchor:	'40%',
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},{
				xtype:	'textfield',
				id:		'_txt_lic_rep',
				name : 'txt_lic_rep',
				fieldLabel:	'Nombre del Licenciado',
				allowBlank: false,
				maxLength	: 60,
				anchor:	'95%'
			},{
				xtype:	'textfield',
				id:		'_txt_not_pub_rep',
				name : 'txt_not_pub_rep',
				fieldLabel:	'N�mero del Notario P�blico',
				maskRe:		/[0-9]/,
				allowBlank: false,
				maxLength	: 15,
				anchor:	'95%'
			},
			{
				xtype:	'textfield',
				id:		'_txt_ori_not_rep',
				name : 'txt_ori_not_rep',
				fieldLabel:	'Ciudad Origen del Notario',
				allowBlank: false,
				maxLength	: 50,
				anchor:	'95%'
			}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			710,
		style:			'margin:0 auto;',
		collapsible:	true,
		title: 'Captura CEDENTE (Empresa)',
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		168,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosFormaConsulta,
		monitorValid:	true,
		buttons: [
			{
				text:		'Guardar',
				id:		'btnGuardar',
				iconCls:	'icoGuardar',
				formBind:false,
				handler: function(boton, evento) {
								accionConsulta("GUARDAR", null);
							}
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR", null);
							}
			}
		]
	});
	
//----------------------------------- CONTENEDOR -------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp
		]
	});
	
	Ext.StoreMgr.key('catalogoPymeDataStore').load();
	disabledCamposREP(true);//Deshabilitar componentes del Representante de inicio
});