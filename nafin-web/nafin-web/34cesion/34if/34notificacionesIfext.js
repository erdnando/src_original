Ext.onReady(function() {
	
	//*********************DESCARGA ARCHIVO DE CONTRATO CESION ********************************
		
	function procesarSuccessFailureContratoCesion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}  
			
	var descargaArchivoContratoCesion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
			url: '34notificacionesIfext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO_CESION_IF',
				tipoArchivo: 'CONTRATO_CESION_IF',				
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});
	}
	
	//*********************DESCARGA ARCHIVO DE CONTRATO   ********************************
	
	function procesarSuccessFailureContrato(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}  
				
	
		var descargarContratoCesion = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
		Ext.Ajax.request({
			url: '../34pki/34pyme/34ContratoCesionPDFCSV.jsp',
			params: Ext.apply({
				informacion: 'CONTRATO_CESION_IF',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContratoCesion
		});			
	}
	
	var descargaArchivoContrato = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var clave_solicitud = registro.get('CLAVE_SOLICITUD');
	
		Ext.Ajax.request({
			url: '34notificacionesIfext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'CONTRATO',
				clave_solicitud: clave_solicitud
			}),
			callback: procesarSuccessFailureContrato
		});
	}
		
	//*********************GENERACION DE ARCHIVO csv  ********************************
	var procesarGenerarCSVCons =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarCSVCons');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarCSVCons');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//*********************GENERACION DE ARCHIVO PDF  ********************************
	var procesarGenerarPDFCons =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFCons');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFCons');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//*********************GRID DE CONSULTA  ********************************
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;	
													
			if(store.getTotalCount() > 0) {	
				Ext.getCmp('mensajeConsulta').show();	
				Ext.getCmp('btnGenerarCSVCons').enable();		
				Ext.getCmp('btnGenerarPDFCons').enable();				  
				el.unmask();					
			} else {	
				Ext.getCmp('btnGenerarCSVCons').disable();		
				Ext.getCmp('btnGenerarPDFCons').disable();
				
				Ext.getCmp('mensajeConsulta').hide();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '34notificacionesIfext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{name: 'CLAVE_SOLICITUD'},
			{name: 'CLAVE_EPO'},
			{name: 'CLAVE_PYME'},		
			{name: 'DEPENDENCIA'},
			{name: 'PYME_CEDENTE'},
			{name: 'RFC'},
			{name: 'NO_PROVEEDOR'},
			{name: 'FECHA_SOLICITUD'},
			{name: 'NO_CONTRATO'},
			{name: 'MONTO_MONEDA'},
			{name: 'TIPO_CONTRATACION'},
			{name: 'F_INICIO_CONTRATO'},
			{name: 'F_FINAL_CONTRATO'},
			{name: 'PLAZO_CONTRATO'},
			{name: 'CLASIFICACION_EPO'},		
			{name: 'OBJETO_CONTRATO'},
			{name: 'COMENTARIOS'},
			{name: 'MONTO_CREDITO'},
			{name: 'REFERENCIA_CREDITO'},
			{name: 'FECHA_VENCIMIENTO_CREDITO'},
			{name: 'BANCO_DEPOSITO'},
			{name: 'CUENTA'},
			{name: 'CUENTA_CABLE'},
			{name: 'ESTATUS'},
			{name: 'CAUSAS_RECHAZO'},
			{name: 'FIRMA_CONTRATO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	
	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Notificaciones',
		columns: [	
			{
				header: 'Dependencia',
				tooltip: 'Dependencia',
				dataIndex: 'DEPENDENCIA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Pyme (Cedente)',
				tooltip: 'Pyme (Cedente)',
				dataIndex: 'PYME_CEDENTE',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. Proveedor',
				tooltip: 'No. Proveedor',
				dataIndex: 'NO_PROVEEDOR',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Solicitud PyME',
				tooltip: 'Fecha de Solicitud PyME',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. Contrato',
				tooltip: 'No. Contrato',
				dataIndex: 'NO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Firma Contrato',
				tooltip: 'Firma Contrato',
				dataIndex: 'FIRMA_CONTRATO',
				sortable: true,
				resizable: true,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Monto/Moneda',
				tooltip: 'Monto/Moneda',
				dataIndex: 'MONTO_MONEDA',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Tipo de Contrataci�n',
				tooltip: 'Tipo de Contrataci�n',
				dataIndex: 'TIPO_CONTRATACION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'F. Inicio Contrato',
				tooltip: 'F. Inicio Contrato',
				dataIndex: 'F_INICIO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'F. Final Contrato',
				tooltip: 'F. Final Contrato',
				dataIndex: 'F_FINAL_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Plazo del  Contrato',
				tooltip: 'Plazo del  Contrato',
				dataIndex: 'PLAZO_CONTRATO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Clasificaci�n EPO',
				tooltip: 'Clasificaci�n EPO',
				dataIndex: 'CLASIFICACION_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},		
			{
				header: 'Objeto del Contrato',
				tooltip: 'Objeto del Contrato',
				dataIndex: 'OBJETO_CONTRATO',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Comentarios',
				tooltip: 'Comentarios',
				dataIndex: 'COMENTARIOS',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'center'	
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato ',
				tooltip: 'Contrato',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						} ,
						handler: descargaArchivoContrato
					}
				]				
			},
			{
				header: 'Monto del Cr�dito',
				tooltip: 'Monto del Cr�dito',
				dataIndex: 'MONTO_CREDITO',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Referencia / No. Cr�dito',
				tooltip: 'Referencia / No. Cr�dito',
				dataIndex: 'REFERENCIA_CREDITO',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Fecha de Vencimiento Cr�dito',
				tooltip: 'Fecha de Vencimiento Cr�dito',
				dataIndex: 'FECHA_VENCIMIENTO_CREDITO',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Banco de Deposito',
				tooltip: 'Banco de Deposito',
				dataIndex: 'BANCO_DEPOSITO',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Cuenta',
				tooltip: 'Cuenta',
				dataIndex: 'CUENTA',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Cuenta CABLE',
				tooltip: 'Cuenta CABLE',
				dataIndex: 'CUENTA_CABLE',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Causas de Rechazo Notificaci�n',
				tooltip: 'Causas de Rechazo Notificaci�n',
				dataIndex: 'CAUSAS_RECHAZO',
				sortable: true,
				width: 130,	
				resizable: true,				
				align: 'left'				
			},
			{
				xtype: 'actioncolumn',
				header: 'Contrato de  Cesi�n ',
				tooltip: 'Contrato  de  Cesi�n',
				 width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';										
						} 
						,handler: descargarContratoCesion
					}
				]				
			}			
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {		
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFCons',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34notificacionesIfext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF',
								tipoArchivo:'PDF'
							}),
							callback: procesarGenerarPDFCons
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFCons',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVCons',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '34notificacionesIfext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV',
								tipoArchivo:'CSV'
							}),
							callback: procesarGenerarCSVCons
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVCons',
					hidden: true
				}					
			]	
		}
	});
	
	var mensaje2 = '<table width="620" cellpadding="1" cellspacing="2" border="0"> '+
						'<tr>'+
						'<td class="formas" align="left">'+
						'<DIV align="left">'+
						'A continuaci�n usted ha recibido las siguientes notificaciones a las cesiones de derecho de cobro realizadas:'+
						'</DIV>'+
						'</td>'+
						'</tr>'+
						'</table>';
						
	var mensajeConsulta = new Ext.Container({
		layout: 'table',		
		id: 'mensajeConsulta',							
		width:	'620',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [		{  	xtype:'label',  	html:	mensaje2  	}			]
	});

	//*********************FORMA ********************************
	var  mensaje = '<table width="620" cellpadding="1" cellspacing="2" border="0"> '+
						'<tr>'+
						'<td class="formas" align="left">'+
						'<DIV align="left">'+
							'Por este conducto me permito notificar a usted el Contrato de Cesi�n de Derechos de Cobro que hemos formalizado '+
							'con la empresa antes se�alada en esta pantalla respecto del contrato que se detalla en esta misma pantalla.<br><br>'+
						�	'Por lo anterior, me permito solicitar a usted, se registre la Cesi�n de Derechos de Cobro de referencia a efecto de que esa '+
							'Entidad, realice el pago derivado del Contrato antes mencionado al <%=strNombre%>, en la inteligencia de que �ste '+
							'est� de acuerdo con lo establecido en el Contrato para el Otorgamiento de Cr�ditos.<br><br>'+
				�			'Los datos de la cuenta bancaria en los que se deben depositar los pagos derivados de esta operaci�n son los que se muestran '+
							'en esta pantalla.<br><br>'+
				�			'Sobre el particular y de conformidad con lo se�alado en la emisi�n de la conformidad para ceder los derechos de cobro emitida'+
							'por esa Entidad, por este medio les notifico, la Cesi�n de Derechos de Cobro del Contrato antes mencionado, para todos los '+
							'efectos legales a que haya lugar, adjunto, en forma electr�nica.<br><br>'+
							'</DIV>'+
							'</td>'+
							'</tr>'+
							'</table>';
							
	var mensajeInico = new Ext.Container({
		layout: 'table',		
		id: 'mensajeInico',							
		width:	'620',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [				{ 	xtype:   'label',   html:		 mensaje 	}		 	]
	});
	
	
	var procesarCatalogoEPOData= function(store, records, oprion){
		if(store.getTotalCount()==0){
		 var clave_epo = Ext.getCmp('clave_epo1');
		 if(clave_epo.getValue()==''){
		  clave_epo.setValue('No existen EPOS con el producto');
		 }
		}
	}
	var procesarCatalogoPymeData= function(store, records, oprion){
		if(store.getTotalCount()==0){
		 var clave_pyme = Ext.getCmp('clave_pyme1');
		 if(clave_pyme.getValue()==''){
		  clave_pyme.setValue('No hay PyMEs con solicitudes pendientes');
		 }
		}
	}
	
	var catalogoContratacionData = new Ext.data.JsonStore({
		id: 'catalogoContratacionData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesIfext.data.jsp',
		baseParams: {
			informacion: 'CatalogoContratacion'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoTipoPlazoData = new Ext.data.JsonStore({
		id: 'catalogoTipoPlazoData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesIfext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	

	var catalogoEstatusData = new Ext.data.JsonStore({
		id: 'catalogoEstatusData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesIfext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesIfext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 'catalogoPYMEData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesIfext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatalogoPymeData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPOData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '34notificacionesIfext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load:procesarCatalogoEPOData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var  elementosForma =  [	
		{
			xtype: 'combo',
			name: 'clave_epo',
			id: 'clave_epo1',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_epo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo
		}, 	
		{
			xtype: 'combo',
			name: 'clave_pyme',
			id: 'clave_pyme1',
			fieldLabel: 'PYME',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'clave_pyme',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoPYMEData,
			tpl : NE.util.templateMensajeCargaCombo					
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Contrato',
			id: 'campoCompuesto1',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'numero_contrato',
					id: 'numero_contrato1',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',					
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda ',
			combineErrors: false,
			msgTarget: 'side',
			id: 'campoCompuesto2',
			items: [
				{
					xtype: 'combo',
					name: 'clave_moneda',
					id: 'clave_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'clave_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					width: 240,
					allowBlank: true,
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Estatus ',
			combineErrors: false,
			msgTarget: 'side',
			id: 'campoCompuesto3',
			items: [
				{
					xtype: 'combo',
					name: 'clave_estatus_sol',
					id: 'clave_estatus_sol1',
					fieldLabel: 'Estatus',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'clave_estatus_sol',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					width: 240,
					allowBlank: true,
					store : catalogoEstatusData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Contrataci�n',
			combineErrors: false,
			msgTarget: 'side',
			id: 'campoCompuesto4',
			items: [
				{
					xtype: 'combo',
					name: 'clave_contratacion',
					id: 'clave_contratacion1',
					fieldLabel: 'Tipo de Contrataci�n',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'clave_contratacion',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 240,
					minChars : 1,
					allowBlank: true,
					store : catalogoContratacionData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		}, 
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vigencia de Contrato',
			combineErrors: false,
			msgTarget: 'side',
			id: 'campoCompuesto5',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_vigencia_ini',
					id: 'fecha_vigencia_ini',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_vigencia_fin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_vigencia_fin',
					id: 'fecha_vigencia_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fecha_vigencia_ini',
					margins: '0 20 0 0'  
					}
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Plazo del Contrato',
				combineErrors: false,
				msgTarget: 'side',
				id: 'campoCompuesto6',
				items: [
					{
						xtype: 'numberfield',
						name: 'plazoContrato',
						id: 'plazoContrato1',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',					
						margins: '0 20 0 0'  
					},	
					{
						xtype: 'combo',
						name: 'claveTipoPlazo',
						id: 'claveTipoPlazo1',
						fieldLabel: '',
						mode: 'local', 
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'claveTipoPlazo',
						emptyText: 'Seleccione...',					
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						allowBlank: true,
						store : catalogoTipoPlazoData,
						tpl : NE.util.templateMensajeCargaCombo
					}
				]
			}			
		];
	
		var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,			
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
							
					var clave_epo =  Ext.getCmp("clave_epo1");
					if (Ext.isEmpty(clave_epo.getValue()) ){
						clave_epo.markInvalid('El valor de la EPO es requerido.');
						return;
					}	
					
					var fecha_vigencia_ini =  Ext.getCmp("fecha_vigencia_ini");
					var fecha_vigencia_fin =  Ext.getCmp("fecha_vigencia_fin");
					var plazoContrato =  Ext.getCmp("plazoContrato1");
					var claveTipoPlazo =  Ext.getCmp("claveTipoPlazo1");
									
					if ( ( !Ext.isEmpty(fecha_vigencia_ini.getValue())  &&   Ext.isEmpty(fecha_vigencia_fin.getValue()) )
						||  ( Ext.isEmpty(fecha_vigencia_ini.getValue())  &&   !Ext.isEmpty(fecha_vigencia_fin.getValue()) ) ){
						fecha_vigencia_ini.markInvalid('Debe capturar ambas Fecha de Vigencia de Contrato o dejarlas en blanco');
						fecha_vigencia_fin.markInvalid('Debe capturar ambas Fecha de Vigencia de Contrato o dejarlas en blanco');
						return;
					}
										
					if ( !Ext.isEmpty(fecha_vigencia_ini.getValue())  &&   !Ext.isEmpty(fecha_vigencia_fin.getValue())   &&  !Ext.isEmpty(claveTipoPlazo.getValue()) &&  !Ext.isEmpty(plazoContrato.getValue()) ){										
						plazoContrato.markInvalid('S�lo puede ingresar el valor de la Fecha Inicial y Final de la Vigencia del Contrato � el Plazo del Contrato, no ambos.');
						return;
					}	
										
					Ext.getCmp('btnBajarCSVCons').hide();						
					Ext.getCmp('btnBajarPDFCons').hide();
					
					fp.el.mask('Enviando...', 'x-mask-loading');	
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar'						
						})
					});	
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '34notificacionesIfext.jsp';
				}
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,		
		items: [
			fp,
			NE.util.getEspaciador(20),	
			mensajeConsulta,
			NE.util.getEspaciador(20),	
			gridConsulta,
			//NE.util.getEspaciador(20),	
			//mensajeInico,
			NE.util.getEspaciador(20)			
		]
	});
		
	catalogoEPOData.load();
	catalogoPYMEData.load();
	catalogoMonedaData.load();
	catalogoEstatusData.load();
	catalogoTipoPlazoData.load();	
	catalogoContratacionData.load();
		
});