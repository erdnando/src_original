<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.text.*,
	java.util.*,
	org.apache.commons.logging.Log,	
	com.netro.exception.*,
	com.netro.cesion.*,
	netropology.utilerias.*,	
	netropology.utilerias.usuarios.*,	
	net.sf.json.JSONArray,
	com.netro.exception.NafinException,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/34cesion/34secsession_ext.jspf" %>
<%!
		private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	%>
<% 
	String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
	String ic_solicitud = request.getParameter("ic_solicitud") == null?"":(String)request.getParameter("ic_solicitud");
	String tipoVisor = request.getParameter("tipoVisor") == null?"":(String)request.getParameter("tipoVisor");
	String titulo = request.getParameter("titulo") == null?"":(String)request.getParameter("titulo");
	String id_grupo = request.getParameter("id_grupo") == null?"":(String)request.getParameter("id_grupo");
	
	String infoRegresar ="", consulta ="", nombreArchivo ="", tipoConsulta =  "Consultar";
	
	JSONObject jsonObj = new JSONObject();
	
	
	CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);	
	
	AutoCorsorcio paginador = new AutoCorsorcio();
	
	if (informacion.equals("ArchivoPDF")  ||  informacion.equals("Consultar")  ){	
		tipoConsulta = "Consultar";
	}else if (informacion.equals("ArchivoPDF_1")  ||  informacion.equals("Consultar_1")  ){	
		tipoConsulta = "Consultar_1";
	}else if (informacion.equals("ArchivoPDF_2")  ||  informacion.equals("Consultar_2")  ){	
		tipoConsulta = "Consultar_2";
	} else if(informacion.equals("ArchivoPDF_3")  ||  informacion.equals("Consultar_Extincion")  ){	
		tipoConsulta = "Consultar_3";
	}
	
	paginador.setIc_solicitud(ic_solicitud); 
	paginador.setTipoVisor(tipoVisor); 
	paginador.setTitulo(titulo); 
	paginador.setTipoConsulta(tipoConsulta);
	paginador.setId_grupo(id_grupo);
			
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);


	if (informacion.equals("Consultar") ){
		
		paginador.setTipoConsulta(tipoConsulta);      
		
		Registros reg	=	queryHelper.doSearch();		
		
		while(reg.next()){
		
			String  nombresRepresentantesPyme ="", correos ="";		
			String  ic_usuario  = (reg.getString("IC_USUARIO") == null) ? "" : reg.getString("IC_USUARIO");
			String  ic_pyme  = (reg.getString("IC_PYME") == null) ? "" : reg.getString("IC_PYME");
					
			HashMap repre =  cesionBean.getRepresentantes(ic_usuario, ""); 		
			  
			reg.setObject("NOMBRE_REPRESENTANTE_LEGAL",repre.get("NOMBRE_REPRESENTANTE_LEGAL").toString());		
			reg.setObject("CORREO_ELECTRONICO",repre.get("CORREO_ELECTRONICO").toString());		
			
		}
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("tipoVisor",tipoVisor);		
		infoRegresar = jsonObj.toString();  


	}else if ( informacion.equals("Consultar_1") ){
		if("ADMIN NAFIN".equals(strPerfil)){
		Registros reg	=	queryHelper.doSearch();		
		HashMap representante = new HashMap();
		HashMap datos = new HashMap();
		JSONArray registros = new JSONArray();
		
		String NOMBRE_IF                = "";
		String NOMBRE_EPO               = "";
		String DF_FIRMA_IF              = "";
		String DF_FIRMA_IF2             = "";
		String DF_FIRMA_TESTIGO1        = "";
		String DF_FIRMA_TESTIGO2        = "";
		String DF_AUTORIZO_IF_OPERATIVO = "";
		String DF_NOTIF_VENT            = "";
		String DF_REDIRECCION           = "";
		String CC_ACUSE4                = "";
		String CC_ACUSE5                = "";
		String CC_ACUSE6                = "";
		String CC_ACUSE_TESTIGO1        = "";
		String CC_ACUSE_TESTIGO2        = "";
		String CC_ACUSE_REDIREC         = "";
		String CG_USUARIO_AUT_IF_OPER   = "";
		if(reg.next()){					
			 NOMBRE_IF  = (reg.getString("NOMBRE_IF") == null) ? "" : reg.getString("NOMBRE_IF");
			 NOMBRE_EPO  = (reg.getString("NOMBRE_EPO") == null) ? "" : reg.getString("NOMBRE_EPO");
			DF_FIRMA_IF  = (reg.getString("DF_FIRMA_IF") == null) ? "" : reg.getString("DF_FIRMA_IF");
			DF_FIRMA_IF2  = (reg.getString("DF_FIRMA_IF2") == null) ? "" : reg.getString("DF_FIRMA_IF2");
			DF_FIRMA_TESTIGO1  = (reg.getString("DF_FIRMA_TESTIGO1") == null) ? "" : reg.getString("DF_FIRMA_TESTIGO1");
			DF_FIRMA_TESTIGO2  = (reg.getString("DF_FIRMA_TESTIGO2") == null) ? "" : reg.getString("DF_FIRMA_TESTIGO2");
			DF_AUTORIZO_IF_OPERATIVO  = (reg.getString("DF_AUTORIZO_IF_OPERATIVO") == null) ? "" : reg.getString("DF_AUTORIZO_IF_OPERATIVO");
			DF_NOTIF_VENT  = (reg.getString("DF_NOTIF_VENT") == null) ? "" : reg.getString("DF_NOTIF_VENT");
			DF_REDIRECCION  = (reg.getString("DF_REDIRECCION") == null) ? "" : reg.getString("DF_REDIRECCION");			
			CC_ACUSE4                = (reg.getString("CC_ACUSE4")                == null) ? "" : reg.getString("CC_ACUSE4");
			CC_ACUSE5                = (reg.getString("CC_ACUSE5")                == null) ? "" : reg.getString("CC_ACUSE5");
			CC_ACUSE6                = (reg.getString("CC_ACUSE6")                == null) ? "" : reg.getString("CC_ACUSE6");
			CC_ACUSE_TESTIGO1        = (reg.getString("CC_ACUSE_TESTIGO1")        == null) ? "" : reg.getString("CC_ACUSE_TESTIGO1");
			CC_ACUSE_TESTIGO2        = (reg.getString("CC_ACUSE_TESTIGO2")        == null) ? "" : reg.getString("CC_ACUSE_TESTIGO2");
			CC_ACUSE_REDIREC         = (reg.getString("CC_ACUSE_REDIREC")         == null) ? "" : reg.getString("CC_ACUSE_REDIREC");
			CG_USUARIO_AUT_IF_OPER   = (reg.getString("CG_USUARIO_AUT_IF_OPER")   == null) ? "" : reg.getString("CG_USUARIO_AUT_IF_OPER");
		} 
		
		// CG_USUARIO_AUT_IF_OPER
		datos = new HashMap();
		datos.put("RAZON_SOCIAL",NOMBRE_IF);
		datos.put("PERFIL_AUTORIZADOR","IF OPERACION	");
		datos.put("FIRMA_CONTRATO",!"".equals(DF_AUTORIZO_IF_OPERATIVO)?"AUTORIZADO": "" );
		datos.put("FECHA_FIRMA",DF_AUTORIZO_IF_OPERATIVO);
		if(!CG_USUARIO_AUT_IF_OPER.equals("")){
			representante = new HashMap();
			representante = cesionBean.getRepresentantes(CG_USUARIO_AUT_IF_OPER, "");
			datos.put("REP_LEGAL", "" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"));
			datos.put("EMAIL",     "" + representante.get("CORREO_ELECTRONICO"));
		} else{
			datos.put("REP_LEGAL", "");
			datos.put("EMAIL",     "");
		}
		registros.add(datos);	
		
		//CC_ACUSE4
		datos = new HashMap();
		datos.put("RAZON_SOCIAL",NOMBRE_IF);
		datos.put("PERFIL_AUTORIZADOR"," REPRESENTANTE IF 1");
		datos.put("FIRMA_CONTRATO",!"".equals(DF_FIRMA_IF)?"FIRMADO": "" );
		datos.put("FECHA_FIRMA",DF_FIRMA_IF	);	
		if(!CC_ACUSE4.equals("")){
			representante = new HashMap();
			representante = cesionBean.getRepresentantes(CC_ACUSE4);
			datos.put("REP_LEGAL", "" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"));
			datos.put("EMAIL",     "" + representante.get("CORREO_ELECTRONICO"));
		} else{
			datos.put("REP_LEGAL", "");
			datos.put("EMAIL",     "");
		}
		registros.add(datos);	
		
		//CC_ACUSE6
		datos = new HashMap();
		datos.put("RAZON_SOCIAL",NOMBRE_IF);
		datos.put("PERFIL_AUTORIZADOR"," REPRESENTANTE IF 2");
		datos.put("FIRMA_CONTRATO",!"".equals(DF_FIRMA_IF2)?"FIRMADO": "N/A" );
		datos.put("FECHA_FIRMA",!"".equals(DF_FIRMA_IF2)?DF_FIRMA_IF2: "N/A");

		if(!DF_FIRMA_IF2.equals("")){
			if(!CC_ACUSE6.equals("")){
				representante = new HashMap();
				representante = cesionBean.getRepresentantes(CC_ACUSE6);
				datos.put("REP_LEGAL", "" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"));
				datos.put("EMAIL",     "" + representante.get("CORREO_ELECTRONICO"));
			} else{
				datos.put("REP_LEGAL", "");
				datos.put("EMAIL",     "");
			}
		} else{
			datos.put("REP_LEGAL", "N/A");
			datos.put("EMAIL",     "N/A");
		}
		registros.add(datos);	
		
		//CC_ACUSE_TESTIGO1
		datos = new HashMap();
		datos.put("RAZON_SOCIAL",NOMBRE_IF);
		datos.put("PERFIL_AUTORIZADOR"," TESTIGO 1");
		datos.put("FIRMA_CONTRATO",!"".equals(DF_FIRMA_TESTIGO1)?"FIRMADO": "");
		datos.put("FECHA_FIRMA",DF_FIRMA_TESTIGO1	);	
		if(!CC_ACUSE_TESTIGO1.equals("")){
			representante = new HashMap();
			representante = cesionBean.getRepresentantes(CC_ACUSE_TESTIGO1);
			datos.put("REP_LEGAL", "" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"));
			datos.put("EMAIL",     "" + representante.get("CORREO_ELECTRONICO"));
		} else{
			datos.put("REP_LEGAL", "");
			datos.put("EMAIL",     "");
		}
		registros.add(datos);	
		
		//CC_ACUSE_TESTIGO2
		datos = new HashMap();
		datos.put("RAZON_SOCIAL",NOMBRE_IF);
		datos.put("PERFIL_AUTORIZADOR"," TESTIGO 2");
		datos.put("FIRMA_CONTRATO",!"".equals(DF_FIRMA_TESTIGO2)?"FIRMADO": "" );
		datos.put("FECHA_FIRMA",DF_FIRMA_TESTIGO2	);	
		if(!CC_ACUSE_TESTIGO2.equals("")){
			representante = new HashMap();
			representante = cesionBean.getRepresentantes(CC_ACUSE_TESTIGO2);
			datos.put("REP_LEGAL", "" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"));
			datos.put("EMAIL",     "" + representante.get("CORREO_ELECTRONICO"));
		} else{
			datos.put("REP_LEGAL", "");
			datos.put("EMAIL",     "");
		}
		registros.add(datos);	
		
		//CC_ACUSE5
		datos = new HashMap();
		datos.put("RAZON_SOCIAL",NOMBRE_EPO);
		datos.put("PERFIL_AUTORIZADOR"," ADMIN EPO");
		datos.put("FIRMA_CONTRATO",!"".equals(DF_NOTIF_VENT)?"NOTIFICADO": "" );
		datos.put("FECHA_FIRMA",DF_NOTIF_VENT	);	
		if(!CC_ACUSE5.equals("")){
			representante = new HashMap();
			representante = cesionBean.getRepresentantes(CC_ACUSE5);
			datos.put("REP_LEGAL", "" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"));
			datos.put("EMAIL",     "" + representante.get("CORREO_ELECTRONICO"));
		} else{
			datos.put("REP_LEGAL", "");
			datos.put("EMAIL",     "");
		}
		registros.add(datos);	
		
		//CC_ACUSE_REDIREC
		datos = new HashMap();
		datos.put("RAZON_SOCIAL",NOMBRE_EPO);
		datos.put("PERFIL_AUTORIZADOR"," VENTANILLA");
		datos.put("FIRMA_CONTRATO",!"".equals(DF_REDIRECCION)?"REDIRECCIONADO": "" ); 
		datos.put("FECHA_FIRMA",DF_REDIRECCION	);	
		if(!CC_ACUSE_REDIREC.equals("")){
			representante = new HashMap();
			representante = cesionBean.getRepresentantes(CC_ACUSE_REDIREC);
			datos.put("REP_LEGAL", "" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"));
			datos.put("EMAIL",     "" + representante.get("CORREO_ELECTRONICO"));
		} else{
			datos.put("REP_LEGAL", "");
			datos.put("EMAIL",     "");
		}
		registros.add(datos);	
		
		consulta	= "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);

		}else{
			jsonObj.put("registros","");
		}
		jsonObj.put("perfil",strPerfil);
		jsonObj.put("tipoVisor",tipoVisor);		
		infoRegresar = jsonObj.toString();  
	
	}else if ( informacion.equals("Consultar_2") ){
		paginador.setTipoConsulta(tipoConsulta);
		paginador.setId_grupo(id_grupo);
		
		List lstEmpresas = new ArrayList();
		HashMap hmEmpresa = null;
		Registros reg	=	queryHelper.doSearch();		
		UtilUsr utilUsr = new UtilUsr();
		while(reg.next()){
		
			String  nombresRepresentantesPyme ="", correos ="";		
			String  nombrePyme  = (reg.getString("NOMBRE_PYME") == null) ? "" : reg.getString("NOMBRE_PYME");
			String  ic_pyme  = (reg.getString("IC_PYME") == null) ? "" : reg.getString("IC_PYME");
			
			
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(ic_pyme, "P");
			String nombreRepPyme = "";
			for (int i = 0; i < usuariosPorPerfil.size(); i++) {
				String loginUsrPyme = (String)usuariosPorPerfil.get(i);
				Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
				String mail = usuario.getEmail();
				nombreRepPyme = usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
				
				hmEmpresa = new HashMap();
				hmEmpresa.put("IC_PYME", ic_pyme);
				hmEmpresa.put("NOMBRE_PYME", nombrePyme);
				hmEmpresa.put("NOMBRE_REPRESENTANTE_LEGAL", nombreRepPyme);
				hmEmpresa.put("CORREO_ELECTRONICO", mail);
				lstEmpresas.add(hmEmpresa);
			}
			
			
			
		}
		
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(lstEmpresas);
		consulta	=	"{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("tipoVisor",tipoVisor);		
		infoRegresar = jsonObj.toString();  

	} else if(informacion.equals("Consultar_Extincion")){

		Registros reg = queryHelper.doSearch();

		while(reg.next()){

			String nombresRepresentantesPyme ="", correos ="";
			String ic_usuario = (reg.getString("IC_USUARIO") == null) ? "" : reg.getString("IC_USUARIO");
			String ic_pyme = (reg.getString("IC_PYME") == null) ? "" : reg.getString("IC_PYME");

			HashMap repre = cesionBean.getRepresentantes(ic_usuario, "");

			reg.setObject("NOMBRE_REPRESENTANTE_LEGAL",repre.get("NOMBRE_REPRESENTANTE_LEGAL").toString());
			reg.setObject("CORREO_ELECTRONICO",repre.get("CORREO_ELECTRONICO").toString());

		}

		consulta = "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("tipoVisor",tipoVisor);
		infoRegresar = jsonObj.toString();  
	} else if( informacion.equals("ArchivoPDF") || informacion.equals("ArchivoPDF_1") || informacion.equals("ArchivoPDF_2") || informacion.equals("ArchivoPDF_3") ){
	
	try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString(); 
	
	}
%>
<%=infoRegresar%>
