<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,		
		java.util.Vector,
		java.io.*,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.distribuidores.*,
		com.netro.servicios.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String sesClaveAfiliado = (String)session.getAttribute("iNoCliente");
String infoRegresar = "", cboCvePrograma ="";

if (informacion.equals("Consulta")) {
	
	Registros regResumen = this.getPrograma(sesClaveAfiliado, cboCvePrograma);	
	infoRegresar = "{\"success\": true, \"total\": " + regResumen.getNumeroRegistros() + ", \"registros\": " + regResumen.getJSONData() + "}";

} else if (informacion.equals("SeleccionPrograma")) {
	//**************************** Elimina variables de session que son establecida en carga_variables  *************************
	 session.removeAttribute("inicializar.FINALIZADO");	//Elimina el identificador para que se marque la sesión de trabajo como no finalizada y pueda pasar por las demas pantallas intermedias de incialización (por ejemplo encuestas, noticias, etc...)
	 //Elementos que se muestran en el menu lateral
	 session.removeAttribute("sesMenu");	//Menu principal
	 session.removeAttribute("sesMenuDirecto");	//Menu de Acceso Directo
	 session.removeAttribute("sesMensajesIniciales");	//Mensajes Iniciales
	 session.removeAttribute("sesEncuestas");	//Encuestas

	//***************************************************************************************************************************

	String cveProgramaFondoJR = request.getParameter("cveProgramaFondoJR");
	String descProgramaFondoJR = request.getParameter("descProgramaFondoJR");
	JSONObject jsonObj = new JSONObject();
		
	try {
		if (cveProgramaFondoJR == null || cveProgramaFondoJR.equals("")) {
			throw new Exception("La clave del Programa  debe ser especificada ");
		}
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	session.setAttribute("cveProgramaFondoJR", cveProgramaFondoJR );
	session.setAttribute("descProgramaFondoJR",descProgramaFondoJR );
		
	Navegacion nav = (Navegacion)session.getAttribute("navegacion");
	
	List pantallasNavegacionInicial = (List)session.getAttribute("inicializar.PantallasBasicas");
	if (pantallasNavegacionInicial == null || pantallasNavegacionInicial.isEmpty()) {
		//Puede ya haber entrado al sistema... y esta cambiando de EPO
		//por lo que pantallasNavegacionInicial estaria vacio
		//Vuelve a carga la ruta de navegacion a partir de esta pantalla
		pantallasNavegacionInicial = nav.getPantallasBasicas();
		Iterator it = pantallasNavegacionInicial.iterator();
		while(it.hasNext()) {
			String url = (String)it.next();
			//Tiene que eliminar las paginas de la navegación previas a elige_programaExt.jsp
			if ("/20secure/elige_programaExt.jsp".equals(url)) {
				break;
			} else {
				it.remove();
			} 
		}
	}
	
	pantallasNavegacionInicial.remove("/20secure/elige_programaExt.jsp");  //El nombre debe coincidir con el especificado en Navegacion

	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlPagina", appWebContextRoot + pantallasNavegacionInicial.get(0));
	infoRegresar = jsonObj.toString();
}
%>

<%=infoRegresar%>

<%!
public Registros getPrograma(String esIF,  String cboCvePrograma ) {
		System.out.println("getPrograma(E)");

		AccesoDB con = new AccesoDB();		
		List params  = null;
		try {
			con.conexionDB();

			String strSQL =
				" SELECT 	 "  + 
				" F.IC_PROGRAMA_FONDOJR											AS CLAVE,      "  +
				" I.CG_NOMBRE_COMERCIAL || ' - ' || F.CG_DESCRIPCION    AS DESCRIPCION "  +
				" FROM                                                                   "  + 
				"	COMCAT_PROGRAMA_FONDOJR F,                                           "  +
				"	COMCAT_IF               I                                            "  +
				"WHERE                                                                  "  +
				"	F.IC_IF = I.IC_IF                                                    "  ;
								
				//if(!esIF.equals("0"))  strSQL +=" AND I.IC_IF = ? ";				
				if(!cboCvePrograma.equals("")) strSQL +=" AND F.IC_PROGRAMA_FONDOJR = ?";				
				strSQL +=" ORDER BY DESCRIPCION    ";

			params = new ArrayList();
			//if(!esIF.equals("0"))	params.add(esIF);						 
			if(!cboCvePrograma.equals("")) params.add(cboCvePrograma); 
			
			System.out.println(strSQL);
			System.out.println(params);
			Registros reg = con.consultarDB(strSQL, params);
			return reg;
			
		} catch (Exception e) {
			throw new AppException("Error al general el resumen.", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("getPrograma(S)");
		}
	}
	
%>