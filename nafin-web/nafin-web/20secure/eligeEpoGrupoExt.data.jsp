<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,				
		java.io.*,
		java.sql.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		org.apache.commons.logging.Log,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String tipoCliente = (request.getParameter("tipoCliente")!=null)?request.getParameter("tipoCliente"):"";

String infoRegresar = "";
JSONObject jsonObj = new JSONObject();

if(informacion.equals("Consulta")) {

	List  datos = getConsultaGrupoEPO(iNoCliente);
	JSONArray registros = new JSONArray();
	registros = registros.fromObject(datos);
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";


}  else if (informacion.equals("SeleccionEpo")) {

	//**************************** Elimina variables de session que son establecida en carga_variables  *************************
	 session.removeAttribute("inicializar.FINALIZADO");	//Elimina el identificador para que se marque la sesión de trabajo como no finalizada y pueda pasar por las demas pantallas intermedias de incialización (por ejemplo encuestas, noticias, etc...)
	 //Elementos que se muestran en el menu lateral
	 session.removeAttribute("sesMenu");	//Menu principal
	 session.removeAttribute("sesMenuDirecto");	//Menu de Acceso Directo
	 session.removeAttribute("sesMensajesIniciales");	//Mensajes Iniciales
	 session.removeAttribute("sesEncuestas");	//Encuestas
	session.setAttribute("iNoEPO", ic_epo);
	session.setAttribute("iNoCliente", ic_epo);
	
	Navegacion nav = (Navegacion)session.getAttribute("navegacion");


	List pantallasNavegacionInicial = (List)session.getAttribute("inicializar.PantallasBasicas");
	if (pantallasNavegacionInicial == null || pantallasNavegacionInicial.isEmpty()) {
		//Puede ya haber entrado al sistema... y esta cambiando de EPO
		//por lo que pantallasNavegacionInicial estaria vacio
		//Vuelve a carga la ruta de navegacion a partir de esta pantalla
		pantallasNavegacionInicial = nav.getPantallasBasicas();		
		Iterator it = pantallasNavegacionInicial.iterator();
		while(it.hasNext()) {
			String url = (String)it.next();
			//Tiene que eliminar las paginas de la navegación previas a eligeEpoExt.jsp
			if ("/20secure/eligeEpoGrupoExt.jsp".equals(url)) {
				break;
			} else {
				it.remove();
			}
		}
	}

	pantallasNavegacionInicial.remove("/20secure/eligeEpoGrupoExt.jsp");  //El nombre debe coincidir con el especificado en Navegacion

	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlPagina", appWebContextRoot + pantallasNavegacionInicial.get(0));
	infoRegresar = jsonObj.toString();	
	
} 

%>

<%=infoRegresar%>


<%!

	public List  getConsultaGrupoEPO(String  ic_epo ){
		log.info("getConsultaGrupoEPO (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		PreparedStatement	ps			= null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		StringBuffer qrySentencia = new StringBuffer("");
		StringBuffer grupos = new StringBuffer("");
		
		try{
			con.conexionDB();
			
			qrySentencia = new StringBuffer("");
			qrySentencia.append( " SELECT IC_GRUPO_EPO  from COMREL_GRUPO_X_EPO_OPERA where ic_epo = "+ic_epo);
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
		
			while(rs.next()) {			
				grupos.append( (rs.getString("IC_GRUPO_EPO")==null)?"":rs.getString("IC_GRUPO_EPO")+",");				
			}
			rs.close();
			
			grupos.deleteCharAt(grupos.length()-1);
						
			qrySentencia = new StringBuffer("");
			qrySentencia.append( " SELECT g.CG_DESCRIPCION  AS GRUPO   "+
									   " ,e.CG_RAZON_SOCIAL AS CADENA  "+
										" ,e.ic_epo AS IC_EPO "+										
										" FROM "+
										" COMREL_GRUPO_X_EPO_OPERA ge, "+
										" COMCAT_GRUPO_EPO_OPERA g,  "+
										" comcat_epo e  "+
										" where ge.IC_GRUPO_EPO = g.IC_GRUPO_EPO  "+
										" and ge.ic_epo = e.ic_epo  "+
										" and g.CS_ACTIVO = 'S'  "+
										" and g.IC_GRUPO_EPO  in ( "+grupos +" ) ");
			
			log.debug("qrySentencia::::::::::::::::  "+qrySentencia);
			
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
		
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("GRUPO", (rs.getString("GRUPO")==null)?"":rs.getString("GRUPO") );
				datos.put("CADENA", (rs.getString("CADENA")==null)?"":rs.getString("CADENA") );
				datos.put("IC_EPO", (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO") );				
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("getConsultaGrupoEPO Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getConsultaGrupoEPO (S) ");
		return registros;
	}
	
	%>
