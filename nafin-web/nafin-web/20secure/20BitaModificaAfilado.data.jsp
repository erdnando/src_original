<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,		
		java.util.Vector,
		java.io.*,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
                java.text.SimpleDateFormat,
		com.netro.cadenas.*"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String fechaDescuento	=	(request.getParameter("fechaDescuento")!=null)?request.getParameter("fechaDescuento"):"";
String infoRegresar = "";
JSONObject jsonObj = new JSONObject();

ConsBitModificaAfiliado    paginador = new ConsBitModificaAfiliado();
paginador.setUsuarioIF(iNoUsuario); 
paginador.setClaveIf(iNoCliente);
CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

Registros listaRegistros = new Registros();

if ("Consulta".equals(informacion)  ) {

  Registros reg = queryHelper.doSearch();
  
    while(reg.next()){
        String ictramites = reg.getString("TRAMITE");
        String tramite =  paginador.datosTramite(ictramites, "Grid");
        reg.setObject("TRAMITE",tramite);
    }
   
  infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
  
}else  if ("GenerarArchivo".equals(informacion)  ) {

    try {
        String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
    } catch(Throwable e) {
        throw new AppException("Error al generar el archivo CSV", e);
    }
    infoRegresar = jsonObj.toString(); 

}else  if ("Enterado".equals(informacion)  ) {

    String fechaEnterado = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
    String  horaEntrerado = (new SimpleDateFormat ("HH:mm:ss")).format(new java.util.Date());   
    String idBitAfiliados[]= request.getParameterValues("idBitAfiliados");
        
    boolean enterado =  paginador.actualizaEnterado ( idBitAfiliados,  fechaEnterado ,  horaEntrerado, iNoCliente, iNoUsuario  );
       
    if(enterado==true) { 
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("enterado", "S");        
        
    }else  if(enterado==false) { 
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("enterado", "N");        
    }
    infoRegresar = jsonObj.toString(); 
    
}else  if ("Omitir".equals(informacion)  ) {
 
    
    List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
    pantallasNavegacionComplementaria.remove("/20secure/20BitaModificaAfilado.jsp");  //El nombre debe coincidir con el especificado en Navegacion

    jsonObj.put("success", new Boolean(true));
    jsonObj.put("urlPagina", appWebContextRoot + pantallasNavegacionComplementaria.get(0));
    infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>
