<%@page import="
    com.nafin.filter.UtilXSSSanatizer,
    com.chermansolutions.oracle.sso.partnerapp.beans.SiteInfoBean"%>
<jsp:useBean id="ssoObj" scope="application" class="com.chermansolutions.oracle.sso.partnerapp.beans.SSOEnablerJspBean"/>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setHeader("Expires", "Thu, 29 Oct 2000 17:04:19 GMT");

//ssoObj.setDatasource("ssoApps");
HttpServletResponse response2 = 	
	new HttpServletResponseWrapper(response) {
		public void sendRedirect(String location) throws java.io.IOException {}
	};

String ssousername = (request.getParameter("ic_usuario")!=null)?UtilXSSSanatizer.HTMLFullEncode(request.getParameter("ic_usuario")):"";
String password = (request.getParameter("password")!=null)?UtilXSSSanatizer.HTMLFullEncode(request.getParameter("password")):"";

//getSSOUserInfo hace un sendRedirect de manera automática si no se obtienen
//los datos del usuario. En este caso le enviamos un reponse2 modificado
//que inhabilita el sendRedirect() con la finalidad de enviar a un
//login personalizado de esta aplicación.
//Si ocurre un problema de autenticación, ya será controlado por el SSO y
//su configuración, no por esta pantalla.
String usrInfo = ssoObj.getSSOUserInfo(request, response2);

if (usrInfo != null) { //Si ya hay usuario redirige al establecimiento del ambiente de usuario
	response.sendRedirect("index.jsp");
	return;
}

SiteInfoBean siteInfo = ssoObj.getSiteInfo(request);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
<form name="frmLogin" method="post" AutoComplete="off" action="<%=siteInfo.getLoginURL()%>">
  <input type="hidden" name="site2pstoretoken" value="<%=siteInfo.getSiteToken()%>" /> 
  <input type="hidden" name="p_submit_url" value="<%=siteInfo.getLoginURL()%>" />
  <input type="hidden" name="p_requested_url" value="<%=siteInfo.getRequestedURL()%>" />
  <input type="hidden" name="p_cancel_url" value="<%=siteInfo.getCancelURL()%>" />
  <input type="hidden" name="ssousername" value="<%=ssousername%>"/>
  <input type="hidden" name="password" value="<%=password%>"/>
</form>
<script>	
	document.frmLogin.submit();
</script>
</body>
</html>