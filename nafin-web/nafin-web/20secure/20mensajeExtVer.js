Ext.onReady(function() {

//--------------------------------- HANDLERS -------------------------------
	var jsonMensajes = null;
	var procesarSuccessFailureObtieneMensajeXClave = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var bContinuar = false;
			var tipoMensaje = "";
			jsonMensajes = Ext.util.JSON.decode(response.responseText);

			if(jsonMensajes.CS_ADICIONAL && jsonMensajes.CS_ADICIONAL=='S'){
				panel.show();
				panel.setTitle(jsonMensajes.CG_TITULO);
				
				var nuevoContenido = "";
				if (jsonMensajes.CG_CONTENIDO != "") {
					nuevoContenido += '<br><br><br>' +
							jsonMensajes.CG_CONTENIDO +
							'<br><br><br>';
				}
				tipoMensaje = "M";
				bContinuar = true;
			
			}else{
				var registros = jsonMensajes.registros;
				if (registros.length > 0) {
					var nuevoContenido = "";
					panel.show();
					mensaje = registros[0];
					panel.setTitle(mensaje.CG_TITULO);

					if (mensaje.CG_IMAGEN != "") {
						nuevoContenido = 
								'<img src="' + NE.appWebContextRoot + '/00archivos/15cadenas/15archcadenas/mensIni' + mensaje.IC_MENSAJE_INI + '/imagenes/' + mensaje.CG_IMAGEN + '" />';
		
					}
					if (mensaje.CG_CONTENIDO != "") {
						nuevoContenido += '<br><br><br>' +
								mensaje.CG_CONTENIDO +
								'<br><br><br>';
					}
					tipoMensaje = mensaje.CG_TIPO_MENSAJE;
					bContinuar = true;
				}
			}
				
			if(bContinuar){
				
				panel.update(nuevoContenido);
				
				var btnInteresado = Ext.getCmp('btnInteresado');
				var tbsInteresado = Ext.getCmp('tbsInteresado');
				var btnNoInteresado = Ext.getCmp('btnNoInteresado');
				var tbsNoInteresado = Ext.getCmp('tbsNoInteresado');
				if (tipoMensaje == "M") {
					btnInteresado.hide();
					tbsInteresado.hide();
					btnNoInteresado.hide();
					tbsNoInteresado.hide();
				} else {
					btnInteresado.show();
					tbsInteresado.show();
					btnNoInteresado.show();
					tbsNoInteresado.show();
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	
	var obtenerMensaje = function() {
			Ext.Ajax.request({
			url: '20mensajeExt.data.jsp',
			params: {
				informacion: "ObtieneMensajeXClave",
				msgini: Ext.getDom('msgini').value,
				msgtitulo: Ext.getDom('msgtitulo').value,
				csAdicional: Ext.getDom('csAdicional').value
			},
			callback: procesarSuccessFailureObtieneMensajeXClave
		});
	}

		


	var procesarSuccessFailureGuardarInteresMensaje = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonObj = Ext.util.JSON.decode(response.responseText);
			//window.location = jsonObj.urlPagina;
			Ext.MessageBox.alert(' ', jsonObj.mensaje, 
					function(boton, evento) {
						
					}
			);
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGuardarInteresMensaje = function(interes) {
		Ext.Ajax.request({
			url: '20mensajeExt.data.jsp',
			params: {
				informacion: "GuardarInteresMensaje",
				msgini: mensaje.IC_MENSAJE_INI,
				estatus: interes
			},
			callback: procesarSuccessFailureGuardarInteresMensaje
		});
	}

//-------------------------------- STORES -----------------------------------

//-------------------------------------------------------------------


	var panel = new Ext.Panel({
		hidden: true,
		height: 'auto',
		boxMinHeight: 300,
		width: 700,
		title: 'Mensajes',
		frame: false,
		bodyStyle: 'text-align:center',
		bbar: [
			'->',
			{
				xtype: 'tbseparator',
				id: 'tbsInteresado',
				hidden: true
			},
			{
				xtype: 'button',
				id: 'btnInteresado',
				text: 'Si me interesa',
				hidden: true,
				handler: function(boton, evento) {
					procesarGuardarInteresMensaje('S');
				}
			},
			{
				xtype: 'tbseparator',
				id: 'tbsNoInteresado',
				hidden: true
			},
			{
				xtype: 'button',
				id: 'btnNoInteresado',
				text: 'No me interesa',
				tooltip: 'No volver a mostrar esta información',
				hidden: true,
				handler: function(boton, evento) {
					procesarGuardarInteresMensaje('N');
				}
			}
		]
	});

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			panel
		]
	});

//-------------------------------- ----------------- -----------------------------------

obtenerMensaje();


});