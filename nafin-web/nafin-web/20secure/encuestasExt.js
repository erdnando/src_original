Ext.onReady(function() {

//--------------------------------- HANDLERS -------------------------------
	var procesarObtieneEncuestas = function(store, arrRegistros, opts) {
		var totalCount = store.getTotalCount();
		var btnContinuarCmp = Ext.getCmp("btnContinuar");
		var tbsContinuarCmp = Ext.getCmp("tbsContinuar");
		var cargaVariablesProcesada = (Ext.getDom("cargaVariablesProcesada").value == "S")?true:false;
		if (cargaVariablesProcesada) {
			//Si se llega a esta pantalla por menu, no tiene sentido mostrar el 
			//boton de "Recordar mas tarde" o "Continuar"
			tbsContinuarCmp.hide();
			btnContinuarCmp.hide();
		}
		
		if (totalCount > 0) {
			var s = (totalCount > 1)?"s":"";
			panelInfo.update(
				'Usted tiene ' + store.getTotalCount() + ' encuesta' + s + ' por contestar. ' +
				'<br><br><hr>* Le recordamos que es necesario contestar la' + s + ' encuesta' + s +
				' requerida' + s + ' antes de su fecha l�mite de vencimiento para brindarle un mejor servicio'
			);
			panelInfo.show();
			if (!cargaVariablesProcesada) {
				store.each(function(reg) {
					if (reg.get("CS_OBLIGATORIO") == "S" && reg.get("NUM_DIA_HABILES") <= 1) {
						//Deshabilita el boton de recordar mas tarde para obligar a contestar la encuesta
						tbsContinuarCmp.hide();
						btnContinuarCmp.hide();
						return false;
					}
				});
			}
		} else {
			grid.getGridEl().mask('No hay m�s encuestas por contestar', 'x-mask');
			if(!cargaVariablesProcesada) {
				//Si no hay mas encuestas por contestar y es necesario terminar de inicializar la sesi�n, se  muestra el boton "Continuar"
				tbsContinuarCmp.show();
				btnContinuarCmp.setText("Continuar");
				btnContinuarCmp.show();
			}
		}
	}

	function procesarSuccessFailureContinuar(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonObj = Ext.util.JSON.decode(response.responseText);


			if (encuestasData.getCount() > 0) {
				var diasFaltantesEncuesta1 = encuestasData.getAt(0).get('NUM_DIA_HABILES');
				Ext.MessageBox.alert('Informaci�n',
					"Estimado Usuario <br> Le restan "+diasFaltantesEncuesta1+" d�as h�biles para responder <br> la(s) encuestas(s) pendientes(s).",
						function(){
							window.location = jsonObj.urlPagina;
						}
				);
			} else {
				window.location = jsonObj.urlPagina;
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarContinuar = function(boton, evnt) {
		boton.disable();
		
		Ext.Ajax.request({
			url: 'encuestasExt.data.jsp',
			params: {
				informacion: "Continuar"
			},
			callback: procesarSuccessFailureContinuar
		});
	}


//-------------------------------- STORES -----------------------------------
	var encuestasData = new Ext.data.JsonStore({
		root : 'registros',
		fields : [
			{name: 'IC_ENCUESTA_AFIL'},
			{name: 'CG_TITULO'},
			{name: 'TOTAL'},
			{name: 'FECHA_LIMITE', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'TT_INSTRUCCIONES'},
			{name: 'CS_OBLIGATORIO'},
			{name: 'NUM_DIA_HABILES', type: 'integer'}
		],
		url : 'encuestasExt.data.jsp',
		baseParams: {
			informacion: 'ObtieneEncuestas'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarObtieneEncuestas
		}
	});

//-------------------------------------------------------------------

	var panelInfo = new Ext.Panel({
		hidden: true,
		height: 'auto',
		width: 700,
		title: 'Encuestas Electr�nicas',
		frame: true/*,
		bodyStyle: 'text-align:center'*/
	});

	var grid = new Ext.grid.GridPanel({
		store: encuestasData,
		hidden: false,
		columns: [
			{
				header: 'T�tulo', tooltip: 'T�tulo',
				dataIndex: 'CG_TITULO',
				sortable: true,
				width: 223,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Preguntas', tooltip: 'N�mero de Preguntas',
				dataIndex : 'TOTAL',
				width : 90,
				sortable : true,
				hidden: false
			},
			{
				header: 'Instrucciones',
				tooltip: 'Instrucciones para contestar la encuesta',
				dataIndex: 'TT_INSTRUCCIONES',
				sortable: true,
				width: 230,	
				resizable: true, 
				hideable : true,
				hidden: false,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					if (valor!=null && valor.length < 50) {
						return valor;
					} else {
						return valor.substring(0,50) + "...";
					}
				}
			},
			{
				header : 'Fecha L�mite', tooltip: 'Fecha L�mite para Contestar',
				dataIndex : 'FECHA_LIMITE',
				sortable : true,
				width : 80,
				hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				css: 'cursor: pointer'
			},
			{
				//xtype: 'actioncolumn',
				header: 'Contestar',
				tooltip: 'Contestar Encuesta',
				width: 60,
				align: 'center',
				renderer: function() {
					return "<img class='icoContinuar' style='cursor:pointer' src='" + Ext.BLANK_IMAGE_URL + "'>";
				}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 700,
		title: '',
		frame: false,
		listeners: {
			rowclick: function(grid, rowIndex, evento) {
				window.location = "contesta_encuestaExt.jsp?ic_encuesta_afil=" + 
						grid.getStore().getAt(rowIndex).get('IC_ENCUESTA_AFIL');
			}
		},
		bbar: [
			'->',
			{
				xtype: 'tbseparator',
				id: 'tbsContinuar',
				hidden: false
			},
			{
				text: 'Recordar m�s tarde',
				id: 'btnContinuar',
				hidden: false,
				handler: procesarContinuar
			}
		]
	});


//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			panelInfo,
			grid
		]
	});

//-------------------------------- ----------------- -----------------------------------

encuestasData.load();


});