Ext.onReady(function() {
	var appWebContextRoot =  Ext.getDom('appWebContextRoot').value;	
	var obtenerMensaje = function() {
		panel.setTitle(' Mensaje ');	
		panel.update('<br>'+'El cliente con el que intenta ingresar a NAFINET se encuentra Inactivo favor de comunicarse  con C.P.,<br> PEDRO MACEDO CASTILLO Subdirector de Operaciones de Cr�dito al tel�fono (55) 5325-6872.<br><br> ');
	}
//-------------------------------------------------------------------

	var panel = new Ext.Panel({
		hidden: false,
		height: 'auto',
		boxMinHeight: 300,
		width: 700,
		title: 'Mensajes',
		frame: false,
		bodyStyle: 'text-align:center',
		bbar: [
			'->',
			{
				xtype: 'button',
				text: 'Salir',
				hidden: false,
				handler: function(boton, evento) {
					window.location = appWebContextRoot+'/ssologout.jsp';
				}
			}
		]
	});

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			panel
		]
	});

//-------------------------------- ----------------- -----------------------------------

	obtenerMensaje();

});