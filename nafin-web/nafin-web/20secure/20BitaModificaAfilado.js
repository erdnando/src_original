Ext.onReady(function() {

    function procesarAccionEnterado(opts, success, response) {
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
            var jsonObj = Ext.util.JSON.decode(response.responseText);
            
            if(jsonObj.enterado==='S') {
                Ext.Ajax.request({
                    url: '20BitaModificaAfilado.data.jsp',
                    params: {
                        informacion: "Omitir"			
                    },
                    callback: procesarAccionOmitir
                });	
                    
            }else   if(jsonObj.enterado==='N') {
                 Ext.MessageBox.alert("Mensaje","Hubo un error.");
            }
            
	} else {
            NE.util.mostrarConnError(response,opts);
            grid.el.unmask();			
	}
    }; 
    

    function procesarAccionOmitir(opts, success, response) {
        if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
           var  jsonObj = Ext.util.JSON.decode(response.responseText);
            window.location = jsonObj.urlPagina;
	} else {
            NE.util.mostrarConnError(response,opts);
            grid.el.unmask();			
	}
    };   

    function procesarDescargaArchivos(opts, success, response) {
        if (success ===  true && Ext.util.JSON.decode(response.responseText).success === true) {
            var infoR = Ext.util.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
            fp.getForm().getEl().dom.submit();
	}else {
            NE.util.mostrarConnError(response,opts);
	}
    };
        
    var procesarConsultaData = function(store, arrRegistros, opts) 	{
        var gridConsulta = Ext.getCmp('gridConsulta');	
        var el = gridConsulta.getGridEl();	
	if (arrRegistros !== null) {
            if (!gridConsulta.isVisible()) {
                gridConsulta.show();
            }	
		
            if(store.getTotalCount() > 0) {			
                el.unmask();
            } else {	
		el.mask('No existe notificaciones de actualizaciones', 'x-mask');				
            }
	}
    };
        
    var consultaData = new Ext.data.JsonStore({
        root: 'registros',
	url: '20BitaModificaAfilado.data.jsp',
	baseParams: {
            informacion: 'Consulta'
	},
	fields: [
            {name: 'IC_BIT'},
            {name: 'IC_PYME'},            
            {name: 'NA_ELECTRONICO'},
            {name: 'RAZON_SOCIAL'},
            {name: 'RFC'},
            {name: 'TRAMITE'},
            {name: 'DF_MODIFICACION'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
            load: procesarConsultaData,
            exception: {
                fn: function(proxy,type,action,optionsRequest,response,args){
                    NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
                    procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
		}
            }
	}
    });

   	
    var cambiosRenderer = function( value, metadata, record, rowIndex, colIndex, store){
        metadata.attr = 'style="white-space: normal; word-wrap:break-word;';
	metadata.attr += '" ';
	return value;
    };
    
    var gridConsulta = new Ext.grid.GridPanel({
        id: 'gridConsulta',
        hidden: false,
	header:true,
	title: 'NOTIFICACI�N DE ACTUALIZACIONES ',
	store: consultaData,
	style: 'margin:0 auto;',
	columns:[
            {
                header:'<center>NE </center>',
		tooltip: 'Nafin Electr�nico ',
		sortable: true,
		dataIndex: 'NA_ELECTRONICO',
		width: 80,
		align: 'center'
            },
            {
                header:'<center>Raz�n Social</center>',
		tooltip: 'Raz�n Social',
		sortable: true,
		dataIndex: 'RAZON_SOCIAL',
		width: 260,
		align: 'left'
            },
            {
                header:'<center>RFC</center>',
		tooltip: 'RFC',
		sortable: true,
		dataIndex: 'RFC',
		width: 110,
		align: 'center'
            },
            {
                header:'<center>Tr�mite</center>',
		tooltip: 'Tr�mite',
		sortable: true,
		dataIndex: 'TRAMITE',
		width: 250,
		align: 'left',
                renderer:cambiosRenderer
            },
            {
                header:'<center>Fecha de Modificaci�n</center>',
		tooltip: 'Fecha de Modificaci�n',
		sortable: true,
		dataIndex: 'DF_MODIFICACION',
		width: 120,
		align: 'center'
            }
        ],
        stripeRows: true,
        loadMask: true,
	height: 300,
	width: 680,	
	frame: true,
        bbar: {
            items: [
                '-','->',
                {
                    xtype: 'button',
                    text: 'Descargar ',
                    id: 'btnDescargar',                    
                    iconCls: 'icoXls',
                    handler: function(boton, evento) {
                        Ext.Ajax.request({
                            url: '20BitaModificaAfilado.data.jsp',
                            params: Ext.apply({
                                informacion: 'GenerarArchivo'
                            }),
                            callback: procesarDescargaArchivos
			});
                    }
		},
                {
                    xtype: 'button',
                    text: 'Enterado ',
                    id: 'btnEnterado',                   
                    iconCls: 'icoAceptar',
                    handler: function(boton, evento) {
                        var idBitAfiliados =[];                        
                        gridConsulta = Ext.getCmp('gridConsulta');
                        var store = gridConsulta.getStore();	
                        store.each(function(record) {
                            idBitAfiliados.push(record.data['IC_BIT']);                           
                        });
                                                
			Ext.Ajax.request({
                            url: '20BitaModificaAfilado.data.jsp',
                            params: {
                                informacion: "Enterado",
                                idBitAfiliados:idBitAfiliados
                            },
                            callback: procesarAccionEnterado
                        });                        
                    }
		},
                {
                    xtype: 'button',
                    text: 'Omitir ',
                    id: 'btnOmitir',                   
                    iconCls: 'icoLimpiar',
                    handler: function(boton, evento) {
                     
                        Ext.Ajax.request({
                            url: '20BitaModificaAfilado.data.jsp',
                            params: {
                                informacion: "Omitir"			
			},
                            callback: procesarAccionOmitir
                        });	
                    }
		}
            ]
        }
    });

    var fp = new Ext.form.FormPanel({
        id: 'fpArchivo1',
	width: 600,
	title: '',
	frame: true,
	hidden: true,
	bodyStyle: 'padding: 6px',
	style: 'margin:0 auto;'
    });
        
    new Ext.Container({
        id: 'contenedorPrincipal',
	applyTo: 'areaContenido',
	width: 700,		
	style: 'margin:0 auto;',
	items: [ 
            fp,
            NE.util.getEspaciador(20),
            gridConsulta 
        ]
    });  
        
    
    consultaData.load({  });	   

                                        
        
});