<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,		
		java.util.Vector,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.servicios.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%

String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String sesClaveAfiliado = (String)session.getAttribute("iNoCliente");
String sesTipoAfiliado = (String)session.getAttribute("strTipoUsuario");
String sesClaveEpoRelacionado = (String)session.getAttribute("iNoEPO");

String infoRegresar = "";

Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB", Servicios.class);

if (informacion.equals("ObtieneEncuestas")) {
	//Las encuestas solo se muestran a los 3 principales perfiles ADMIN PYME, EPO e IF
	Registros encuestas = servicios.obtenerEncuestas(strTipoUsuario, sesClaveAfiliado, sesClaveEpoRelacionado);
	Map mEncuestas = new LinkedHashMap();
	while(encuestas.next()) {
		mEncuestas.put(encuestas.getString("IC_ENCUESTA_AFIL"), encuestas.getString("CG_TITULO"));
	}
	//Las claves de las encuestas se cargan en sesión dado que se van a estar presentando en el menu lateral.
	session.setAttribute("sesEncuestas", mEncuestas);
	infoRegresar = "{\"success\": true, \"total\": " + encuestas.getNumeroRegistros() + ", \"registros\": " + encuestas.getJSONData() + "}";
}else if (informacion.equals("Continuar")) {
	List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
	pantallasNavegacionComplementaria.remove("/20secure/encuestasExt.jsp");  //El nombre debe coincidir con el especificado en Navegacion

	infoRegresar = "{\"success\": true, \"urlPagina\": \"" + appWebContextRoot + pantallasNavegacionComplementaria.get(0) + "\"}";
}
%>
<%=infoRegresar%>
