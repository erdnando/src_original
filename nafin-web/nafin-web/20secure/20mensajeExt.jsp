<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252"	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>

<script type="text/javascript" src="20mensajeExt.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if ("PYME".equals(strTipoUsuario)) {%>
<%@ include file="/01principal/01pyme/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01pyme/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01pyme/pie.jspf"%>
<%
} else if ("EPO".equals(strTipoUsuario)) {
%>
<%@ include file="/01principal/01epo/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01epo/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01epo/pie.jspf"%>
<%
} else if ("IF".equals(strTipoUsuario)) {
%>
<%@ include file="/01principal/01if/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01if/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01if/pie.jspf"%>
<%
}
%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>



<%--
<%@ include file="../15cadenas/015secsession2.jsp" %>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />

<%response.setHeader("Pragma","no-cache");

AccesoDB con = new AccesoDB();
int i = 0;
String destino = "";
try{

String 				qrySentencia	= "";
PreparedStatement	ps 				= null;
ResultSet			rs 				= null;
Enumeration			icMensajesIni	= (Enumeration)session.getAttribute("icMensajesIni");
con.conexionDB();

String tmensaje = "";
String msgini = "";

boolean esPymeInternacionalIngles = (strTipoUsuario.equals("PYME") && 
		session.getAttribute("sesPymeInternacional").equals("S") &&
		session.getAttribute("sesIdiomaUsuario").equals("EN"))?true:false;
%>

<html>
<head>
	<title>Mensaje</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="<%=strDirecVirtualCSS%>css/nafin.css">
</head>
<script>
	function fnBoton(opc,msg,ruta){
		var f = document.forma;
		if(opc==1){
			f.estatus.value = "S";
		}else if(opc==2){
			f.estatus.value = "N";
		}
		f.msgini.value = msg;
		f.ruta.value = ruta;
		f.ic_epo.value = "<%=iNoEPO%>";
		f.ic_pyme.value = "<%=iNoCliente%>";
		f.action = "20mensajeinsert.jsp";
		f.submit();
	}
</script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="forma" method="post">
<input type="Hidden" name="estatus" value="">
<input type="Hidden" name="msgini" value="">
<input type="Hidden" name="ic_epo" value="">
<input type="Hidden" name="ic_pyme" value="">
<input type="Hidden" name="ruta" value="">
<style>
	.formas {font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding-right: 5px; padding-left: 5px}
	.a {font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; text-decoration: none}
	.titulos01 {color: #034663; font-size: 21px; font-weight: bold; font-family: arial, helvetica, sanserif; font-style: normal; line-height: 110%;}
	.titulos02 {color: #034663; font-size: 18px; font-weight: bold; font-family: arial, helvetica, sanserif; font-style: normal; line-height: 110%}
	.titulos03 {color: #034663; font-size: 15px; font-weight: bold; font-family: arial, helvetica, sanserif; font-style: normal; line-height: 110%}
	.subtitulos01 {  color: #034663; font-size: 13px; font-weight: bold; font-family: arial, helvetica, sanserif; font-style: italic; line-height: 110% }
</style>
<table width="500" align="center" cellspacing="0" cellpadding="0" border="0">
<tr>    		
<% 	if(icMensajesIni==null){
		Vector vecIcMensajes = new Vector();
		if("EPO".equals(strTipoUsuario)){
			qrySentencia	=
				"  SELECT mi.ic_mensaje_ini"   +
				"  FROM com_mensaje_ini mi"   +
				"  ,comrel_epo_mensaje_ini emi"   +
				"  WHERE mi.ic_mensaje_ini = emi.ic_mensaje_ini"   +
				"  AND emi.ic_epo = ?"   +
				"  AND mi.cg_tipo_afiliado = 'E'"   +
				"  AND TRUNC(SYSDATE) BETWEEN TRUNC(mi.df_ini_mensaje) AND TRUNC(mi.df_fin_mensaje)"  ;
		}else if("IF".equals(strTipoUsuario)){
			qrySentencia	=
				"  SELECT mi.ic_mensaje_ini"   +
				"  FROM com_mensaje_ini mi"   +
				"  ,comrel_if_mensaje_ini emi"   +
				"  WHERE mi.ic_mensaje_ini = emi.ic_mensaje_ini"   +
				"  AND emi.ic_if = ?"   +
				"  AND mi.cg_tipo_afiliado = 'I'"   +
				"  AND TRUNC(SYSDATE) BETWEEN TRUNC(mi.df_ini_mensaje) AND TRUNC(mi.df_fin_mensaje)"  ;
		}else if("PYME".equals(strTipoUsuario)){
			qrySentencia	=
				"  SELECT distinct mi.ic_mensaje_ini"   +
				"  FROM com_mensaje_ini mi"   +
				"  ,comrel_epo_mensaje_ini emi"   +
				"  ,comrel_pyme_epo pe"+
				"  WHERE mi.ic_mensaje_ini = emi.ic_mensaje_ini"   +
				"  AND emi.ic_epo = pe.ic_epo"   +
				"  AND pe.ic_pyme = ?"+
				"  AND pe.cs_habilitado = 'S'"+
				"  AND mi.cg_tipo_afiliado = 'P'"   +
				"  AND TRUNC(SYSDATE) BETWEEN TRUNC(mi.df_ini_mensaje) AND TRUNC(mi.df_fin_mensaje)" +
				"  AND emi.ic_epo = "+iNoEPO ;		
		}
		if (qrySentencia != null && !qrySentencia.equals("")) {
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,iNoCliente);
			rs = ps.executeQuery();
			while(rs.next()){	
				vecIcMensajes.add(rs.getString(1));
			}
			rs.close();
			ps.close();
		}
		icMensajesIni = vecIcMensajes.elements();
	}

	if(icMensajesIni.hasMoreElements()){
		qrySentencia = 
			"  SELECT mi.ic_mensaje_ini"   +
			"  ,mi.cg_titulo"   +
			"  ,mi.cg_contenido"   +
			"  ,mi.cg_imagen"   +
			" ,NVL(mi.cg_tipo_mensaje,'M') cg_tipo_mensaje"  +
			"  FROM com_mensaje_ini mi"   +
			"  WHERE mi.ic_mensaje_ini = ?"  +
			"  AND NOT EXISTS (Select ic_mensaje_ini from com_estadistica_mensaje_ini where ic_mensaje_ini = ?" +
			"   and ic_pyme = "+ iNoCliente +")";
			
		String valor = icMensajesIni.nextElement().toString();
		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,valor);
		ps.setString(2,valor);
		rs = ps.executeQuery();
		while(rs.next()){	
			String	strTitulo 		= (rs.getString("cg_titulo")==null)?"":rs.getString("cg_titulo");
			String	nombreImagen	= (rs.getString("cg_imagen")==null)?"":rs.getString("cg_imagen");
			String	icMensajeIni	= (rs.getString("ic_mensaje_ini")==null)?"":rs.getString("ic_mensaje_ini");
			String	strContenido	= (rs.getString("cg_contenido")==null)?"":rs.getString("cg_contenido");
			String	strTipoMensaje	= (rs.getString("cg_tipo_mensaje")==null)?"":rs.getString("cg_tipo_mensaje");
			String 	urlArchivo 		= strDirecVirtualTrabajo+"15archcadenas/mensIni"+icMensajeIni+"/imagenes/";		
			tmensaje = strTipoMensaje;
			msgini = icMensajeIni;
%>
		<td align="center" class="formas">
			<%if(!"M".equals(tmensaje)){%>
			<img src="<%=urlArchivo + nombreImagen%>" alt="" border="0">
			<%} else {%>

			<table align="center" cellspacing="0" cellpadding="0" border="0" >
			<tr>
				<td class="formas" align="center">
					<%if(tmensaje.equals("M")){%>
						<%@ include file="../15cadenas/cabeza2.jsp" %>
						<br><br>
					<%}%>
				</td>
			</tr>
<%				if(!"".equals(nombreImagen)){%>
				<tr>
					<td class="formas" align="center">
						<img src="<%=urlArchivo + nombreImagen%>" alt="" border="0">
					</td>
				</tr>
<%				}%>
			<tr>
			    <td class="formas" align="center">
					<%if(tmensaje.equals("M")){%>
						<br><br><%=strContenido%><br><br>
					<%}%>
				</td>
			</tr>
			</table>
			<%} %>
		</td>
<%		i++;
		}//while
		ps.close();
		session.setAttribute("icMensajesIni",icMensajesIni);
	}else{ // if hasMoreElements
		session.removeAttribute("icMensajesIni");	
	}
%>
</tr>
<tr>
    <td align="center">
		<table border="0" cellspacing="0" cellpadding="0"  border="0">
			<tr>
				<td>
					<%if(tmensaje.equals("M")){%>
					<table align="center" cellspacing="2" cellpadding="2" border="0">
						<tr>
<%						destino = icMensajesIni.hasMoreElements()?"20mensaje.jsp":"../15cadenas/frame.jsp";%>
						    <td class="celda02" width="50" height="24" align="center">
							 	<a href="<%=destino%>"><%=(esPymeInternacionalIngles)?"OK":mensaje_param.getMensaje("20mensaje.Enterado",sesIdiomaUsuario)%></a>
							</td>
						 </tr>
					</table>
					<%}else{%>
						<table align="center" cellspacing="0" cellpadding="0" border="0">
						<tr>
<%						destino = icMensajesIni.hasMoreElements()?"20mensaje.jsp":"../15cadenas/frame.jsp";%>
							<td class="formas" align="center">
								<a href="javascript:fnBoton(1,<%=msgini%>,'<%=destino%>');"><img src="../00utils/gif/btn_siinteresa_<%=sesIdiomaUsuario%>.gif" border="0" alt="Si me interesa"></a>
							</td>
							<td class="formas" align="center">
								<a href="javascript:fnBoton(2,<%=msgini%>,'<%=destino%>');"><img src="../00utils/gif/btn_nomostrar_<%=sesIdiomaUsuario%>.gif" border="0" alt="No volver a mostrar esta información"></a>&nbsp;	
							</td>
							<td class="formas" align="center">
								<a href="<%=destino%>"><img src="../00utils/gif/btn_continuar_<%=sesIdiomaUsuario%>.gif" border="0" alt="Continuar"></a>	
							</td>
						 </tr>
					</table>
					<%}%>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
<%
}catch(Exception e){
	System.out.println("20mensaje.jsp Exception "+e);
	response.sendRedirect("../15cadenas/frame.jsp");
}finally{
	if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	}
}
if(i==0){
	response.sendRedirect(destino);
}
%>
</form>
</body>
</html>


--%>