<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,		
		java.util.Vector,
		net.sf.json.JSONObject,
		net.sf.json.JSONArray,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.servicios.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%

String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String sesClaveAfiliado = (String)session.getAttribute("iNoCliente");
String sesTipoAfiliado = (String)session.getAttribute("strTipoUsuario");
String sesClaveEpoRelacionado = (String)session.getAttribute("iNoEPO");

String infoRegresar = "";

Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB", Servicios.class);

if (informacion.equals("ObtieneMensajes")) {
	final String TODOS_LOS_MENSAJES = null;
	Registros mensajes = servicios.obtenerMensajesEdoCta(sesClaveAfiliado);
	Map mMensajesIniciales = new LinkedHashMap();
	Map mMensajesAdicIniciales = new LinkedHashMap();
	while(mensajes.next()) {
		mMensajesIniciales.put("IC_NOTIFICA", mensajes.getString("IC_NOTIFICA"));
                mMensajesIniciales.put("DF_FIN_MES", mensajes.getString("DF_FIN_MES"));
                mMensajesIniciales.put("DF_FIN_MES", mensajes.getString("DF_FIN_MES"));
                mMensajesIniciales.put("CG_NOMBRE_COMERCIAL", mensajes.getString("CG_NOMBRE_COMERCIAL"));
                mMensajesIniciales.put("IC_PROC_EDO_CUENTA", mensajes.getString("IC_PROC_EDO_CUENTA"));
	}

	JSONArray jsonArr = JSONArray.fromObject(mensajes.getJSONData());
	
	//Las claves de los mensajes se cargan en sesión dado que se van a estar presentando en el menu lateral.
	//Los mensajes Iniciales adicionales agregados no son considerados.
	session.setAttribute("sesMensajesIniciales", mMensajesIniciales);
	session.setAttribute("sesMensajesAdicIniciales", mMensajesAdicIniciales);
	infoRegresar = "{\"success\": true, \"total\": " + mensajes.getNumeroRegistros() + ", \"registros\": " + jsonArr + "}";
} else if (informacion.equals("GuardarInteresMensaje")){
	String estatus = (request.getParameter("estatus")==null)?"":request.getParameter("estatus");
	String msgini = (request.getParameter("msgini")==null)?"":request.getParameter("msgini");

	String mensaje = servicios.guardaVistoEdoCta(sesClaveAfiliado, gsCveUsuario);
	Map mMensajesIniciales = (Map)session.getAttribute("sesMensajesIniciales");	//Sirve para mostrar los mensajes iniciales en el menu lateral
	mMensajesIniciales.remove(msgini);
	
	infoRegresar = "{\"success\": true, \"mensaje\": \"" + mensaje + "\"}";
}  else if (informacion.equals("ObtieneMensajeXClave")){	//Obtiene un mensaje en particular...
	String msgini = (request.getParameter("msgini")==null)?"":request.getParameter("msgini");
	String msgtitulo = (request.getParameter("msgtitulo")==null)?"":request.getParameter("msgtitulo");
	String csAdicional = (request.getParameter("csAdicional")==null)?"":request.getParameter("csAdicional");
	if (msgini == null || msgini.equals("")) {
		throw new AppException("Error en los parametros recibidos.");
	}
	
	if("S".equals(csAdicional)){
		System.out.println("msgini================================================="+msgini);
		java.util.Map _mMensajesAdicIniciales = (java.util.Map)session.getAttribute("sesMensajesAdicIniciales");
		String contenido = (String)_mMensajesAdicIniciales.get(msgini);
		System.out.println("contenido================================================="+contenido);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("CG_TITULO", msgtitulo);
		jsonObj.put("CG_CONTENIDO", contenido);
		jsonObj.put("CS_ADICIONAL", "S");
		
		infoRegresar = jsonObj.toString();
	}else{
		Registros mensajes = servicios.obtenerMensajesIniciales(sesClaveAfiliado, sesTipoAfiliado, sesClaveEpoRelacionado, msgini);
		infoRegresar = "{\"success\": true, \"total\": " + mensajes.getNumeroRegistros() + ", \"registros\": " + mensajes.getJSONData() + "}";
	}
}else if (informacion.equals("Continuar")) {
        String msgini = (request.getParameter("msgini")==null)?"":request.getParameter("msgini");

	String mensaje = servicios.guardaVistoEdoCta(sesClaveAfiliado, gsCveUsuario);
        	Map mMensajesIniciales = (Map)session.getAttribute("sesMensajesIniciales");	//Sirve para mostrar los mensajes iniciales en el menu lateral
	mMensajesIniciales.remove(msgini);
        
	List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
	pantallasNavegacionComplementaria.remove("/20secure/20mensajeEdoCtaExt.jsp");  //El nombre debe coincidir con el especificado en Navegacion

	infoRegresar = "{\"success\": true, \"urlPagina\": \"" + appWebContextRoot + pantallasNavegacionComplementaria.get(0) + "\"}";
}
%>
<%=infoRegresar%>
