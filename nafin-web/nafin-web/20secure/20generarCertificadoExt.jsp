<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252"	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%@ include file="/01principal/menu.jspf" %>

<script type="text/javascript" src="20generarCertificadoExt.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if ("PYME".equals(strTipoUsuario)) {%>
<%@ include file="/01principal/01pyme/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01pyme/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01pyme/pie.jspf"%>
				
<%}else if ("EPO".equals(strTipoUsuario)) {%>

<%@ include file="/01principal/01epo/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01epo/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01epo/pie.jspf"%>
<%}else if ("IF".equals(strTipoUsuario)) {%>

<%@ include file="/01principal/01if/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01if/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01if/pie.jspf"%>
				
<% }else if ("UNIV".equals(strTipoUsuario)) {%>

<%@ include file="/01principal/01uni/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01uni/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01uni/pie.jspf"%>
				
<%}else if ("CLIENTE".equals(strTipoUsuario)) {%>

<%@ include file="/01principal/01cliente/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01cliente/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01cliente/pie.jspf"%>

<%}else if ("NAFIN".equals(strTipoUsuario)) {%>

<%@ include file="/01principal/01nafin/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01nafin/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01nafin/pie.jspf"%>
				
<%}else if ("IFNB CEDI".equals(strTipoUsuario)) {%>

<%@ include file="/01principal/01cedi/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01cedi/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01cedi/pie.jspf"%>
				
<% }%>

<%
String _ua = request.getHeader("User-Agent");
boolean _isChrome = false;
boolean _isIE10 = false;
boolean _isIE11 = false;
String _urlGenerarCertificado = "";
if (_ua != null) {
	_isChrome = ( _ua.indexOf("Chrome/") != -1 );
	_isIE10 = ( _ua.indexOf("MSIE 10") != -1 );
	_isIE11 = ( _ua.indexOf("Trident") != -1 && _ua.indexOf("rv:11") != -1 );
}
if (_isChrome || _isIE10 || _isIE11) {
	_urlGenerarCertificado = "/cgi-bin/certaut/cgi_certifica.exe/get_page";
} else {
	_urlGenerarCertificado = "/cgi-bin/cgi_certifica/get_page";
}
%>

<form id='formAux' name="formAux" target='_new' action="<%=_urlGenerarCertificado%>">
	<input type="hidden" name="USERNAME" value="<%=strLogin%>">
	<input type="hidden" name="PASSWORD" value="">
</form>

<div id="mensaje1" style="display: none" >
	Para ingresar al m�dulo solicitado, es necesario generar previamente
	el certificado digital que le permitir� firmar su informaci�n.
	<br><br>
	<hr>
	<span style="font-weight: bold">IMPORTANTE:</span><br>
	Una vez generado e instalado el certificado, es necesario
	cerrar el navegador y volver a ingresar al sistema.
</div>
<div id="mensaje2" style="display: none" >
	Para ingresar al m�dulo solicitado, es necesario cerrar el navegador
	y volver a ingresar al sistema.<br>
	Posteriormente seleccione el certificado digital que corresponda, cuando
	le sea requerido.
	<br>
</div>

</body>
</html>

