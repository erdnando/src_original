<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,		
		java.util.Vector,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.servicios.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%

String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String sesClaveAfiliado = (String)session.getAttribute("iNoCliente");
String sesTipoAfiliado = (String)session.getAttribute("strTipoUsuario");
String sesClaveEpoRelacionado = (String)session.getAttribute("iNoEPO");

String infoRegresar = "";

Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB", Servicios.class);



String claveEncuestaAfil = request.getParameter("ic_encuesta_afil");

if (informacion.equals("ObtieneEncuesta")) {

	try {
		if (claveEncuestaAfil == null || claveEncuestaAfil.equals("")) {
			throw new Exception("Los parametros son requeridos.");
		}
		Integer.parseInt(claveEncuestaAfil);
	} catch (Exception e ){
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	Map datosEncuesta = servicios.obtenerEncuesta(claveEncuestaAfil);
	Registros regP = (Registros)datosEncuesta.get("PREGUNTAS");
	Registros regO = (Registros)datosEncuesta.get("OPCIONES"); 
	
	int totalPreg = regP.getNumeroRegistros();
	//Armado de los items para el Extjs. Esto es solo armado de la vista, por eso puede ir en el .data.
	//Cualquier cambio de logica de negocio no debe ir aqui...
	int i = -1;
	int numPregunta = 1;
	StringBuffer sbEncuesta = new StringBuffer(512);
	StringBuffer funcionesEnc = new StringBuffer(512);
	sbEncuesta.append("[");
	int p = 0;
	while(regP.next()) {
		i++;
		String tipoRespuesta = regP.getString("ic_tipo_respuesta");
		if (i == 0 || "4".equals(tipoRespuesta)) {
			
			if (i>0) {
				//si es elprimer rubro no necesita cerrar el anterior.
				sbEncuesta.deleteCharAt(sbEncuesta.length()-1);
				sbEncuesta.append("]},");
			}
			//Se genera un nuevo RUBRO
			String tituloRubro = "";
			if ("4".equals(tipoRespuesta)) {	//Si es 4 hay titulo del rubro
				tituloRubro =  regP.getString("cg_pregunta").replaceAll("\r\n|\n", "<br>");
			}

			sbEncuesta.append(
					"{xtype:'fieldset', autoHeight: true, title:'" + tituloRubro.toUpperCase() + "'," +
					" items: [");
		}
		
		if (!"4".equals(tipoRespuesta)) {
			p++;
			if ("1".equals(tipoRespuesta)) {
			sbEncuesta.append(
					"{xtype:'fieldset',autoHeight: true, " +
					" id:'PRG" +(p)+"'," +
					" tipoPreg:'"+tipoRespuesta+"'," +
					"defaults:{msgTarget: 'side', anchor: '-20' }," +
					"title:'" + numPregunta +") "+ regP.getString("cg_pregunta").replaceAll("\r\n|\n", "<br>") + "'," +
					"items: [");
			}else  {
			sbEncuesta.append(
					"{xtype:'fieldset',autoHeight: true, " +
					" id:'PRG" +(p)+"'," +
					" tipoPreg:'"+tipoRespuesta+"'," +
					"defaults:{msgTarget: 'side' }," +
					"title:'" + numPregunta +") "+ regP.getString("cg_pregunta").replaceAll("\r\n|\n", "<br>") + "'," +
					"items: [");
				}
			if ("1".equals(tipoRespuesta)) {	//Libre
				sbEncuesta.append(
						"{xtype:'textarea', allowBlank: true, maxLength: 255, " +
						" tipoResp: 'S', "+
						"name: '" + claveEncuestaAfil + "_" + tipoRespuesta + "_");
				if(regO.getNumeroRegistros() > 0) {
					regO.setNumRegistro(0);	//Se va al primer registro
					if (regO.getString("ic_pregunta").equals(regP.getString("ic_pregunta"))) {
						//La considera y la elimina del objeto de registros
						sbEncuesta.append(
								regO.getString("ic_respuesta") + "'}");
						regO.remove();
					}
				}
				sbEncuesta.append(		
						"]},");
			} else if ("2".equals(tipoRespuesta)) {	//Multiple
				sbEncuesta.append(
						"{xtype:'checkboxgroup', columns: 2, " +
						" tipoResp: 'S', "+
						" numOpc: '"+regO.getNumeroRegistros()+"', "+
						" items: [");
				while(regO.getNumeroRegistros() > 0) {
					regO.setNumRegistro(0);	//Se va al primer registro
					if (regO.getString("ic_pregunta").equals(regP.getString("ic_pregunta"))) {
						//La considera y la elimina del objeto de registros
						sbEncuesta.append(
								"{boxLabel: '" + regO.getString("cg_respuesta") + "'," +
								"name: '" + claveEncuestaAfil + "_" + tipoRespuesta + "_" + regO.getString("ic_respuesta") + "'," +
								"inputValue: 'S'},");
						regO.remove();
						if(regO.getNumeroRegistros() == 0) {
							sbEncuesta.deleteCharAt(sbEncuesta.length()-1);
						}
					} else {
						sbEncuesta.deleteCharAt(sbEncuesta.length()-1);
						break;
					}
				}
				
				sbEncuesta.append(		
						"]}]},");
			}  else if ("3".equals(tipoRespuesta)) {	//Excluyente
				sbEncuesta.append(
						"{xtype:'radiogroup', allowBlank: false, columns: 2, " +
						" tipoResp: 'S', "+
						"items: [");
				while(regO.getNumeroRegistros() > 0) {
					regO.setNumRegistro(0);	//Se va al primer registro
					if (regO.getString("ic_pregunta").equals(regP.getString("ic_pregunta"))) {
						String valExcluyente = regO.getString("cg_excluyentes");
						System.out.println("valExcluyente======="+valExcluyente);
						String listener = "";
						
						if(!"".equals(valExcluyente)){
						
							
							listener = "for(var w=1; w<="+totalPreg+"; w++){ "+
										" var respPr = Ext.getCmp('PRG'+w); " +
										" if(respPr.tipoPreg == '1' || respPr.tipoPreg == '3'){ var objTxt =  respPr.find('tipoResp','S');  " +
										" for(var y=0; y<objTxt.length; y++){  objTxt[y].allowBlank=false;  } " +
										"  } " +
										" respPr.enable(); } ";
																		
							int index = (valExcluyente).indexOf("-");
							int rang1=0;
							int rang2=0;
							
							if(index != -1){
								rang1 = Integer.parseInt((valExcluyente).substring(0,index));
								rang2 = Integer.parseInt((valExcluyente).substring(index+1));
								for(int w=rang1; w<=rang2; w++){
									listener += " var respPrev = Ext.getCmp('PRG"+w+"'); "+
													" if(respPrev.tipoPreg != ''){ var objTxt =  respPrev.find('tipoResp','S');  " +
													" for(var y=0; y<objTxt.length; y++){  " +
													"   if(respPrev.tipoPreg == '1' || respPrev.tipoPreg == '3'){objTxt[y].allowBlank=true}  objTxt[y].setValue('');  " +
													"   if(respPrev.tipoPreg == '2'){ var arrayChk = [];  for(var z=0; z<Number(objTxt[0].numOpc); z++){arrayChk.push(false);}  " +
													"     objTxt[y].setValue(arrayChk); " +
													"   } " +
													"  }} " +
													" respPrev.disable(); ";
								}
								
							}else{
								listener += " var respPrev = Ext.getCmp('PRG"+valExcluyente+"');  " +
												" if(respPrev.tipoPreg != '' ){ var objTxt =  respPrev.find('tipoResp','S');  " +
												" for(var y=0; y<objTxt.length; y++){ " +
												"   if(respPrev.tipoPreg == '1' || respPrev.tipoPreg == '3'){objTxt[y].allowBlank=true;} objTxt[y].setValue(''); " +
												"   if(respPrev.tipoPreg == '2'){ var arrayChk = [];  for(var z=0; z<Number(objTxt[0].numOpc); z++){arrayChk.push(false);}  " +
												"     objTxt[y].setValue(arrayChk); " +
												"   } " +
												" }} " +
												" respPrev.disable(); ";
							}
						}else{
							listener = "for(var w=1; w<="+totalPreg+"; w++){ "+
										" var respPr = Ext.getCmp('PRG'+w); " +
										" if(respPr.tipoPreg == '1' || respPr.tipoPreg == '3'){ var objTxt =  respPr.find('tipoResp','S');  " +
										" for(var y=0; y<objTxt.length; y++){  objTxt[y].allowBlank=false;  } " +
										"  } " +
										" respPr.enable(); } ";
						}
						
						
						//La considera y la elimina del objeto de registros
						sbEncuesta.append(
								"{boxLabel: '" + regO.getString("cg_respuesta") + "'," +
								"name: '" + claveEncuestaAfil + "_" + tipoRespuesta + "_" + regO.getString("ic_pregunta") + "'," +
								"listeners:{check:function(obj, checked){ if(checked){"+listener+"}}}," +
								"inputValue: '" + regO.getString("ic_respuesta") + "'},");
						regO.remove();
						if(regO.getNumeroRegistros() == 0) {
							sbEncuesta.deleteCharAt(sbEncuesta.length()-1);
						}
					} else {
						sbEncuesta.deleteCharAt(sbEncuesta.length()-1);
						break;
					}
				}
				
				sbEncuesta.append(		
						"]}]},");

			} else if ("5".equals(tipoRespuesta)) {	//Silder
				//sbEncuesta.append(
				int itera = 0;
				String pondera = "";
				while(regO.getNumeroRegistros() > 0) {
					regO.setNumRegistro(0);	//Se va al primer registro
					
					if (regO.getString("ic_pregunta").equals(regP.getString("ic_pregunta"))) {
						if(itera==0){
							pondera = regO.getString("ig_ponderacion");
							//System.out.println("pondera==="+pondera);
							//funcionesEnc.append(claveEncuestaAfil + "1_" + tipoRespuesta + "_" + regO.getString("ic_respuesta"));
						}
						itera++;
						//
						funcionesEnc.append("Ext.getCmp('"+claveEncuestaAfil + "2_" + tipoRespuesta + "_" + regO.getString("ic_respuesta")+"').setMaxValue("+pondera+"); ");
						//La considera y la elimina del objeto de registros
						sbEncuesta.append(
								"{ xtype:'panel', " +
								" 	frame:false, " +
								"	autoHeight:true, " +
								"	title:'', " +
								"	layout: 'table', " +
								"	layoutConfig: {columns:2}, "+
								"	labelWidth: 110, " +
								"	items: " +
								"			[ " +
								//"			  { " +
								"{xtype: 'panel', layout:'form', items:[ {xtype:'slider',  " +
								//"name: '" + claveEncuestaAfil + "_" + tipoRespuesta + "_" + regO.getString("ic_respuesta") + "'," +
								" tipoResp: 'S', "+
								"id: '" + claveEncuestaAfil + "2_" + tipoRespuesta + "_" + regO.getString("ic_respuesta") + "'," +
								"idHid: '" + claveEncuestaAfil + "1_" + tipoRespuesta + "_" + regO.getString("ic_respuesta") + "'," +
								"idTxt: 'txt" + claveEncuestaAfil + "1_" + tipoRespuesta + "_" + regO.getString("ic_respuesta") + "'," +
								"fieldLabel: '" +regO.getString("cg_respuesta")+"',"+
								"value: 0, "+
								" plugins: new Ext.slider.Tip({ " +
								"		getText: function(thumb){ " +
								"			 return String.format('{0}', thumb.value); "+
								"		} "+
								"  }), " +
								"listeners:{ " +
								"	change:function(obj, newVal, oldVal){ " +
								"		Ext.getCmp(obj.idHid).setValue(newVal); "+
								"		Ext.getCmp(obj.idTxt).setText(newVal); "+
								"	} " +
								"}, " +
								"width:300 }, {xtype:'hidden', " +
								"name: '" + claveEncuestaAfil + "_" + tipoRespuesta + "_" + regO.getString("ic_respuesta") + "'," +
								"id: '" + claveEncuestaAfil + "1_" + tipoRespuesta + "_" + regO.getString("ic_respuesta") + "'," +
								" value:'0'}]}," +
								"  {xtype: 'panel', layout:'form', frame:true, bodyStyle:'backgroundColor:white;', width:50, items:[ {" +
													"		xtype:'label', " +
													"		id: 'txt" + claveEncuestaAfil + "1_" + tipoRespuesta + "_" + regO.getString("ic_respuesta") + "'," +
													"		html: '0', " +
													"	   style: 'margin:3px 5px 0px 5px;' " +
													"  } ]}" +
								"]" +
								"},"
								);
						regO.remove();
						if(regO.getNumeroRegistros() == 0) {
							sbEncuesta.deleteCharAt(sbEncuesta.length()-1);
						}
					} else {
						sbEncuesta.deleteCharAt(sbEncuesta.length()-1);
						break;
					}
				}
				sbEncuesta.append(		
						"]},");
			}
			numPregunta++;
		}
	
	} //fin while
		
	//cierra Ultimo Rubro;
	if (sbEncuesta.length()>0) {
		//quita coma sobrante y realiza cierre faltante
		sbEncuesta.deleteCharAt(sbEncuesta.length()-1);
		sbEncuesta.append("]}]");
	}

	System.out.println("sbEncuesta>>>>>>>>>>>>>> "+sbEncuesta);
	infoRegresar = "{\"success\": true, \"itemsEncuesta\": " + sbEncuesta + ", \"funcionENc\": \""+funcionesEnc+"\"}";
} else if (informacion.equals("GuardaRespuestasAfiliado")) {
	System.out.println("erase un aves de ayer==="); 
	Map mRespuestas = new HashMap();
	Enumeration en = request.getParameterNames();
	while (en.hasMoreElements()) {
		String claveRespuesta = "";
		String textoRespuesta = "";
		
		String nombreParametro = (String)en.nextElement();
		List lParam = Comunes.explode("_", nombreParametro);
		//Las respuestas vienen en parametros cuyo nombre comienza con el numero de Clave de la Encuesta del Afiliado (ic_encuesta_afil)
		if (lParam.size() > 1 && lParam.get(0).equals(claveEncuestaAfil)) {	//Identifica si es un parametro relacionado con las respuestas
			String tipoRespuesta = (String)lParam.get(1);
			if (tipoRespuesta.equals("1")) {
				//Libre. La clave de la respuesta a almacenar viene en el NOMBRE del Parametro 
				claveRespuesta = (String)lParam.get(2);
				textoRespuesta = request.getParameter(nombreParametro);
			} else if (tipoRespuesta.equals("2")) {
				//Multiple. La clave de la respuesta a almacenar viene en el NOMBRE del Parametro
				claveRespuesta = (String)lParam.get(2);
				textoRespuesta = null; //no aplica
			} else if (tipoRespuesta.equals("3")) {
				//Excluyente. La clave de la respuesta a almacenar viene en el VALOR del Parametro
				claveRespuesta = request.getParameter(nombreParametro);
				textoRespuesta = null; //no aplica
			}else if (tipoRespuesta.equals("5")) {
				//Libre. La clave de la respuesta a almacenar viene en el NOMBRE del Parametro 
				claveRespuesta = (String)lParam.get(2);
				textoRespuesta = request.getParameter(nombreParametro);
			}
			mRespuestas.put(claveRespuesta, textoRespuesta);
		}
	}// fin while
	servicios.guardaRespuestaAfiliado(claveEncuestaAfil, strLogin, mRespuestas);
	Map mEncuestas = (Map)session.getAttribute("sesEncuestas");
	//Si la encuesta se guardo exitosamente, ya no debe aparecer en la seccion
	//de "Encuestas" del menu lateral
	mEncuestas.remove(claveEncuestaAfil);
	
	infoRegresar = "{\"success\": true}";
}
%>

<%=infoRegresar%>
