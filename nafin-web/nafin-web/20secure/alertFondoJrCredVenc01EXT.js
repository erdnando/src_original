Ext.onReady(function(){

//-----------------------------procesarConsultaData-----------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
   var jsonData = store.reader.jsonData;
   var fp = Ext.getCmp('forma');
		fp.el.unmask();							
		var grid1 = Ext.getCmp('griduno');	
    var grid2 = Ext.getCmp('griddos');
		var el = grid1.getGridEl();
    var ee = grid2.getGridEl();
    
		if (arrRegistros != null) {
			if (!grid1.isVisible()) {
				grid1.show();
				}				
			if(store.getTotalCount() > 0) {
        Ext.getCmp('dp').setValue(jsonData.registros[0].CG_DESCRIPCION);
        Ext.getCmp('df').setValue(jsonData.registros[0].DF_EJECUCION_PROC);
				el.unmask();
        ee.unmask();
			} else {		
        Ext.getCmp('programa').hide();	
        Ext.getCmp('fecha').hide();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
        ee.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
//--------------------------Fin procesarConsultaData----------------------------

//------------------------------ ------------------------------------
 var consultaData = new Ext.data.JsonStore({
		root 				: 'registros',
		url 				: 'alertFondoJrCredVenc01EXT.data.jsp',
		baseParams	: {
                  informacion		: 'consulta'  
                  },
    fields      : [	
                  {	name: 'IC_REPORTE'},
                  {	name: 'DF_FECHA_VMTO_INI'},
                  {	name: 'DF_FECHA_VMTO_FIN'},
                  {	name: 'IG_DIAS_VENCIMIENTO'},
                  {	name: 'IG_TOTAL_REG'},
                  {	name: 'FG_TOTALVENCIMIENTO'},
                  {	name: 'IG_TOTAL_REG_X_VENC'},
                  {	name: 'FG_TOTALVENC_X_VENC'},
                  {	name: 'DF_EJECUCION_PROC'},
                  {	name: 'CG_DESCRIPCION'}
                ],		
		totalProperty 	: 'total',
    messageProperty: 'msg',
		autoLoad			  : false,
    listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}	
	})
//----------------------------Fin ----------------------------------

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		id: 'grupoHeaders',
		rows: [
			[
				{header: '', colspan: 1, align: 'center'},
				{header: 'Fecha de Vencimiento', colspan: 2, align: 'center'},
				{header: '', colspan: 1, align: 'center'},
				{header: '', colspan: 1, align: 'center'}
			]
		]
	});

var griduno = new Ext.grid.GridPanel({
		id: 'griduno',
		store: consultaData,
    style: 'margin: 0 auto;',
		plugins:	grupos,
		height: 100,
		width: 600,
		columns: [
				{
				header: 'N�mero de Cr�ditos',
				tooltip: 'N�mero de Cr�ditos',
				dataIndex: 'IG_TOTAL_REG',
				sortable: true,
				resizable: true,
				align: 'center',
        width: 140
			},{
				header: 'Desde',
				tooltip: 'Fecha Vencimiento Desde',
				dataIndex: 'DF_FECHA_VMTO_INI',
				sortable: true,
				resizable: true,
				align: 'center'
			},{
				header: 'Hasta',
				tooltip: 'Fecha Vencimiento Hasta',
				dataIndex: 'DF_FECHA_VMTO_FIN',
				sortable: true,
				resizable: true,
				align: 'center'
			},{
				header: 'D�as L�mite de Vencimiento',
				tooltip: 'D�as L�mite de Vencimiento',
				dataIndex: 'IG_DIAS_VENCIMIENTO',
				sortable: true,
				resizable: true,
				align: 'center',
        width: 140
			},{
				header: 'Monto Total',
				tooltip: 'Monto Total',
				dataIndex: 'FG_TOTALVENCIMIENTO',
				sortable: true,
				resizable: true,
				align: 'center',
        renderer:function(value){
          return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
          }
			}
		]
});

var griddos = new Ext.grid.GridPanel({
		id: 'griddos',
		store: consultaData,
    style: 'margin: 0 auto;',
		plugins:	grupos,
		height: 100,
		width: 600,
		columns: [
				{
				header: 'N�mero de Cr�ditos',
				tooltip: 'N�mero de Cr�ditos',
				dataIndex: 'IG_TOTAL_REG_X_VENC',
				sortable: true,
				resizable: true,
				align: 'center',
        width: 140
			},{
				header: 'Desde',
				tooltip: 'Fecha Vencimiento Desde',
				dataIndex: 'DF_FECHA_VMTO_INI',
				sortable: true,
				resizable: true,
				align: 'center'
			},{
				header: 'Hasta',
				tooltip: 'Fecha Vencimiento Hasta',
				dataIndex: 'DF_FECHA_VMTO_FIN',
				sortable: true,
				resizable: true,
				align: 'center'
			},{
				header: 'D�as L�mite de Vencimiento',
				tooltip: 'D�as L�mite de Vencimiento',
				dataIndex: 'IG_DIAS_VENCIMIENTO',
				sortable: true,
				resizable: true,
				align: 'center',
        width: 140
			},{
				header: 'Monto Total',
				tooltip: 'Monto Total',
				dataIndex: 'FG_TOTALVENC_X_VENC',
				sortable: true,
				resizable: true,
				align: 'center',
        renderer:function(value){
          return "<div align='right'>"+Ext.util.Format.usMoney(value)+"</div>";
          }
			}
		]
});
//-------------------------------Elementos Forma--------------------------------
var  elementosForma =  [
  {
  xtype: 'compositefield',
  id: 'programa',
  combineErrors: false,
  msgTarget: 'side',
  items: [
    {
    xtype: 'displayfield',
    value:'Programa:',
    width : 250
    },{
    xtype: 'displayfield',
    id: 'dp',
    width : 300
    }]
	},{
  xtype: 'compositefield',
  id :'fecha',
  combineErrors: false,
  msgTarget: 'side',
  items: [
    {
    xtype: 'displayfield',
    value:'Fecha de �ltima actualizaci�n:',
    width : 250
    },{
    xtype: 'displayfield',
    id: 'df',
    width : 300
    }]
	},{
  xtype: 'compositefield',
  combineErrors: false,
  msgTarget: 'side',
  items: [
    {
    xtype: 'displayfield',
    value:'Estimado Usuario:',
    width : 150
    }]
	},{
  xtype: 'compositefield',
  combineErrors: false,
  msgTarget: 'side',
  items: [
    {
    xtype: 'displayfield',
    value:'Se le notifica que existen Cr�ditos Vencidos.',
    width : 250
    }]
	},{
    xtype: 'compositefield',
		items: [{
      xtype: 'displayfield',
			width : 1 
      },
    griduno
    ]
  },{
  xtype: 'compositefield',
  combineErrors: false,
  msgTarget: 'side',
  items: [
    {
    xtype: 'displayfield',
    value:'Se le notifica que existen Cr�ditos a vencer en los siguientes 20 d�as naturales.',
    width : 600
    }]
	},{
    xtype: 'compositefield',
		items: [{
      xtype: 'displayfield',
			width : 1 
      },
    griddos
    ]
  }
]
//-----------------------------Fin Elementos Forma------------------------------


//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Garant�as Vencidas y por Vencer',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 50,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma
})
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------
	var pnl = new Ext.Container({
		id			:'contenedorPrincipal',
		applyTo	:'areaContenido',
		width		: 950,
		height	: 600,
		style		:'margin:0 auto;',
		items		:[
					NE.util.getEspaciador(20),
					fp
					]
})//-----------------------------Fin Contenedor Principal-------------------------
consultaData.load();
})//-----------------------------------------------Fin Ext.onReady(function(){}