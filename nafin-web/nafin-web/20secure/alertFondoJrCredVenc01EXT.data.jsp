<%@ page contentType="application/json;charset=UTF-8" import="java.sql.*, java.io.*,  java.util.* ,   com.netro.cadenas.FondoJr ,netropology.utilerias.*,   org.apache.commons.logging.Log, net.sf.json.JSONObject " errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/01principal/01secsession.jspf"%>
<%!  private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName()); %>
<% 
  String infoRegresar   = "";
	String informacion 	  = (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String tipoUsuario    =(String)request.getSession().getAttribute("strTipoUsuario");
  String perfilUsuario  =(String)request.getSession().getAttribute("sesPerfil");
  String gsIdioma       =(String)request.getSession().getAttribute("sesIdiomaUsuario");
  String cboCvePrograma =(String)request.getSession().getAttribute("cveProgramaFondoJR");
     
if(informacion.equals("consulta")) {
  JSONObject jsonObj  = new JSONObject();
	
  Registros reg=null;
  FondoJr f = new FondoJr();
  
  f.setCboCvePrograma(cboCvePrograma);
  reg = f.busqueda();
  
	jsonObj.put("success",  new Boolean(true)); 
  jsonObj.put("total",new Integer( reg.getNumeroRegistros()) );
  jsonObj.put("registros", reg.getJSONData());
  infoRegresar = jsonObj.toString();
  log.debug("infoRegresar: "+infoRegresar);
  }
%>
<%= infoRegresar%>
