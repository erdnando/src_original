Ext.onReady(function() {
Ext.override(Ext.dd.DragTracker, {
    onMouseMove: function (e, target) {
        var isIE9 = Ext.isIE && (/msie 9/.test(navigator.userAgent.toLowerCase())) && document.documentMode != 6;
        if (this.active && Ext.isIE && !isIE9 && !e.browserEvent.button) {
            e.preventDefault();
            this.onMouseUp(e);
            return;
        }
        e.preventDefault();
        var xy = e.getXY(), s = this.startXY;
        this.lastXY = xy;
        if (!this.active) {
            if (Math.abs(s[0] - xy[0]) > this.tolerance || Math.abs(s[1] - xy[1]) > this.tolerance) {
                this.triggerStart(e);
            } else {
                return;
            }
        }
        this.fireEvent('mousemove', this, e);
        this.onDrag(e);
        this.fireEvent('drag', this, e);
    }
});

//--------------------------------- HANDLERS -------------------------------
	var procesarSuccessFailureObtieneEncuesta = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			
			formaEncuesta.add(jsonObj.itemsEncuesta);
			formaEncuesta.el.unmask();
			formaEncuesta.doLayout();
			eval(jsonObj.funcionENc);
	
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGuardaRespuestasAfiliado =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.MessageBox.alert('Encuesta Registrada','Las respuestas a la encuestas se registraron con �xito',
				function(boton, evento) {
					window.location = "encuestasExt.jsp";
				});
		} else {
			var btnAceptarCmp = Ext.getCmp('Aceptar');
			btnAceptarCmp.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGuardaRespuestasAfiliado = function() {
		var formaCmp = Ext.getCmp('forma');
		Ext.Ajax.request({
			url: 'contesta_encuestaExt.data.jsp',
			params: Ext.apply(formaCmp.getForm().getValues(),{
				informacion: "GuardaRespuestasAfiliado",
				ic_encuesta_afil: Ext.getDom("ic_encuesta_afil").value
			}),
			callback: procesarSuccessFailureGuardaRespuestasAfiliado
		});
	}



	var obtenerEncuesta = function() {
			formaEncuesta.el.mask('Cargando...', 'x-mask-loading');
			Ext.Ajax.request({
			url: 'contesta_encuestaExt.data.jsp',
			params: {
				informacion: "ObtieneEncuesta",
				ic_encuesta_afil: Ext.getDom("ic_encuesta_afil").value
			},
			callback: procesarSuccessFailureObtieneEncuesta
		});
	}

//-------------------------------- STORES -----------------------------------


//-------------------------------------------------------------------

	var formaEncuesta = new Ext.form.FormPanel({
		id: 'forma',
		hidden: false,
		height: 'auto',
		width: 700,
		title: 'Encuestas Electr�nicas',
		frame: true,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				formBind: true,
				handler: function(boton, evento) {
					boton.disable();
					procesarGuardaRespuestasAfiliado();
				}
			}
		]
	});
	
	



//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			formaEncuesta
		]
	});

//-------------------------------- ----------------- -----------------------------------

obtenerEncuesta();


});