Ext.onReady(function() {


	function procesarSuccessFailureSeleccionEpo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonObj = Ext.util.JSON.decode(response.responseText);
			window.location = jsonObj.urlPagina;
		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}


	var procesarContinuar = function(grid, rowIndex, colIndex, tipoCliente) {
		grid.el.mask('Procesando...', 'x-mask-loading');
		var ic_epo = grid.getStore().getAt(rowIndex).get('IC_EPO');
		Ext.Ajax.request({
			url: 'eligeEpoGrupoExt.data.jsp',
			params: {
				informacion: "SeleccionEpo",
				ic_epo:ic_epo				
			},
			callback: procesarSuccessFailureSeleccionEpo
		});
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			
			if(store.getTotalCount() > 0) {			
				el.unmask();					
			} else {		
				
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : 'eligeEpoGrupoExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'GRUPO'},
			{name: 'CADENA'},
			{name: 'IC_EPO', type: 'integer'},
			{name: 'IC_GRUPO_EPO', type: 'integer'}	
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var gridConsulta= new Ext.grid.GridPanel({
		store: consultaData,
		id:'gridConsulta',
		hidden: true,		
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 700,
		title: 'Grupos de Epo�s',
		frame: false,	
		listeners: {
			rowclick: function(grid, rowindex, evento) {
				procesarContinuar(grid, rowindex, null, 2);
			}
		},
		columns: [
			{
				header: 'Grupo',
				tooltip: 'Grupo',
				dataIndex: 'GRUPO',
				sortable: true,
				width: 300,
				resizable: true,
				hidden: false,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},		
			{
				header: 'Cadena',
				tooltip: 'Nombre de la Cadena Productiva',
				dataIndex: 'CADENA',
				sortable: true,
				width: 300,
				resizable: true,
				hidden: false,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},		
			{
				header: 'Elegir',
				tooltip: 'Elegir Cadena Productiva',
				width: 40,
				align: 'center',
				renderer: function() {
					return "<img class='icoContinuar' style='cursor:pointer' src='" + Ext.BLANK_IMAGE_URL + "'>";
				}
			}
		]	
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			gridConsulta
		]
	});

	consultaData.load();

});