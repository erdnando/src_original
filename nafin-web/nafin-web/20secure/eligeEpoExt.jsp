<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252"	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%-- Se remplaza el estilo predeterminado del renglon, para que parezca que todo el renglon es una "liga"  --%>
<style type="text/css">
.x-grid3-row {
	cursor: pointer;
}
</style>
<%@ include file="/01principal/01secsession.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%
//System.out.println(strLogin);
%>
<script type="text/javascript" src="eligeEpoExt.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01pyme/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01pyme/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01pyme/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
