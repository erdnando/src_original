Ext.onReady(function() {

//--------------------------------- HANDLERS -------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var el = grid.getGridEl();			
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('El IF no se encuentra afiliado a ning�n Programa', 'x-mask');
				grid.hide();
			}
		}
	}

	function procesarSuccessFailureSeleccionPrograma(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonObj = Ext.util.JSON.decode(response.responseText);
			window.location = jsonObj.urlPagina;
		} else {
			NE.util.mostrarConnError(response,opts);
			grid.el.unmask();			
		}
	}

	var procesarContinuar = function(grid, rowIndex, colIndex, tipoCliente) {
		grid.el.mask('Procesando...', 'x-mask-loading');
		var cveProgramaFondoJR = grid.getStore().getAt(rowIndex).get('CLAVE');
		var descProgramaFondoJR = grid.getStore().getAt(rowIndex).get('DESCRIPCION');
		Ext.Ajax.request({
			url: 'elige_programa.data.jsp',
			params: {
				informacion: "SeleccionPrograma",
				cveProgramaFondoJR:  cveProgramaFondoJR,
				descProgramaFondoJR: descProgramaFondoJR
			},
			callback: procesarSuccessFailureSeleccionPrograma
		});
	}

//-------------------------------- STORES -----------------------------------

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : 'elige_programa.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'CLAVE'},
			{name: 'DESCRIPCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}

	});
//-------------------------------------------------------------------

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: false,		
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 700,
		title: 'Elige Programa Fondo JR',
		frame: false,
		listeners: {
			rowclick: function(grid, rowindex, evento) {
				procesarContinuar(grid, rowindex, null, 1);
			}
		},
		columns: [
			{
				header: 'Nombre del Programa',
				tooltip: 'Nombre del Programa',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 500,
				resizable: true,
				hidden: false,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},		
			{				
				header: 'Elegir',
				tooltip: 'Elegir Cadena Productiva',
				width: 40,
				align: 'center',
				renderer: function() {
					return "<img class='icoContinuar' style='cursor:pointer' src='" + Ext.BLANK_IMAGE_URL + "'>";
				}
			}
		]
	});



//-------------------------------- PRINCIPAL -----------------------------------

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			grid
		]
	});

//-------------------------------- ----------------- -----------------------------------
	consultaData.load();
	


});