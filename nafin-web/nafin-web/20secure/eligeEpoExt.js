Ext.onReady(function() {

//--------------------------------- HANDLERS -------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) {

		if (arrRegistros != null) {

			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {

				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				grid.hide();
			}
		}
	}

	var procesarConsultaDistribuidoresData = function(store, arrRegistros, opts) {

		if (arrRegistros != null) {
			if(store.getTotalCount() > 0) {
				gridDistribuidores.show();
			}
		}
	}


	var procesarConsultaFactRecursoData = function(store, arrRegistros, opts) {

		if (arrRegistros != null) {
			if(store.getTotalCount() > 0) {
				gridFactRecurso.show();
			}
		}
	}




	function procesarSuccessFailureSeleccionEpo(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonObj = Ext.util.JSON.decode(response.responseText);
			window.location = jsonObj.urlPagina;
		} else {
			NE.util.mostrarConnError(response,opts);
			grid.el.unmask();
			gridDistribuidores.el.unmask();
		}
	}

	var procesarContinuar = function(grid, rowIndex, colIndex, tipoCliente) {
		grid.el.mask('Procesando...', 'x-mask-loading');
		var claveEpo = grid.getStore().getAt(rowIndex).get('IC_EPO');
		Ext.Ajax.request({
			url: 'eligeEpoExt.data.jsp',
			params: {
				informacion: "SeleccionEpo",
				ic_epo:  claveEpo,
				ic_tipo_cliente: tipoCliente
			},
			callback: procesarSuccessFailureSeleccionEpo
		});
	}

//-------------------------------- STORES -----------------------------------

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : 'eligeEpoExt.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'IC_EPO'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'NUMDOCTOSMN', type: 'integer'},
			{name: 'MONTOMN', type: 'float'},
			{name: 'NUMDOCTOSUSD', type: 'integer'},
			{name: 'MONTOUSD', type: 'float'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}

	});

	var consultaDistribuidoresData = new Ext.data.JsonStore({
		root : 'registros',
		url : 'eligeEpoExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaDistribuidores'
		},
		fields: [
			{name: 'IC_EPO'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'NUMDOCTOSMN', type: 'integer'},
			{name: 'MONTOMN', type: 'float'},
			{name: 'NUMDOCTOSUSD', type: 'integer'},
			{name: 'MONTOUSD', type: 'float'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDistribuidoresData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}

	});
	
	
	var consultaFactRecursoData = new Ext.data.JsonStore({
		root : 'registros',
		url : 'eligeEpoExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaFactRecurso'
		},
		fields: [
			{name: 'IC_EPO'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'NUMDOCTOSMN', type: 'integer'},
			{name: 'MONTOMN', type: 'float'},
			{name: 'NUMDOCTOSUSD', type: 'integer'},
			{name: 'MONTOUSD', type: 'float'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaFactRecursoData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	
//-------------------------------------------------------------------

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 1, align: 'center'},
				{header: 'Moneda Nacional', colspan: 2, align: 'center'},
				{header: 'D�lares', colspan: 2, align: 'center'},
				{header: '', colspan: 1, align: 'center'}
			]
		]
	});

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: false,
		plugins: grupos,
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 700,
		title: 'Resumen Descuento Electr�nico',
		frame: false,
		listeners: {
			rowclick: function(grid, rowindex, evento) {
				procesarContinuar(grid, rowindex, null, 1);
			}
		},
		columns: [
			{
				header: 'Cadena',
				tooltip: 'Nombre de la Cadena Productiva',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				width: 210,
				resizable: true,
				hidden: false,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header: 'Num. Doctos.',
				tooltip: 'Cantidad de Documentos publicados',
				dataIndex: 'NUMDOCTOSMN',
				sortable: true,
				width: 80,
				resizable: true,
				align: 'right',
				hidden: false
			},
			{
				header: 'Monto',
				tooltip: 'Monto de Documentos',
				dataIndex: 'MONTOMN',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Num. Doctos.',
				tooltip: 'Cantidad de Documentos publicados',
				dataIndex: 'NUMDOCTOSUSD',
				sortable: true,
				width: 80,
				resizable: true,
				align: 'right',
				hidden: false
			},
			{
				header: 'Monto',
				tooltip: 'Monto de Documentos',
				dataIndex: 'MONTOUSD',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				//xtype: 'actioncolumn',
				header: 'Elegir',
				tooltip: 'Elegir Cadena Productiva',
				width: 40,
				align: 'center',
				renderer: function() {
					return "<img class='icoContinuar' style='cursor:pointer' src='" + Ext.BLANK_IMAGE_URL + "'>";
				}
			}
		]
	});

	var gridDistribuidores = new Ext.grid.GridPanel({
		store: consultaDistribuidoresData,
		hidden: true,
		plugins: grupos,
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 700,
		title: 'Resumen Distribuidores',
		frame: false,
		listeners: {
			rowclick: function(grid, rowindex, evento) {
				procesarContinuar(grid, rowindex, null, 2);
			}
		},
		columns: [
			{
				header: 'Cadena',
				tooltip: 'Nombre de la Cadena Productiva',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				width: 210,
				resizable: true,
				hidden: false,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header: 'Num. Doctos.',
				tooltip: 'Cantidad de Documentos publicados',
				dataIndex: 'NUMDOCTOSMN',
				sortable: true,
				width: 80,
				resizable: true,
				align: 'right',
				hidden: false
			},
			{
				header: 'Monto',
				tooltip: 'Monto de Documentos',
				dataIndex: 'MONTOMN',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Num. Doctos.',
				tooltip: 'Cantidad de Documentos publicados',
				dataIndex: 'NUMDOCTOSUSD',
				sortable: true,
				width: 80,
				resizable: true,
				align: 'right',
				hidden: false
			},
			{
				header: 'Monto',
				tooltip: 'Monto de Documentos',
				dataIndex: 'MONTOUSD',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				//xtype: 'actioncolumn',
				header: 'Elegir',
				tooltip: 'Elegir Cadena Productiva',
				width: 40,
				align: 'center',
				renderer: function() {
					return "<img class='icoContinuar' style='cursor:pointer' src='" + Ext.BLANK_IMAGE_URL + "'>";
				}
			}
		]
	});
	
	var gridFactRecurso= new Ext.grid.GridPanel({
		store: consultaFactRecursoData,
		hidden: true,
		plugins: grupos,
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 700,
		title: 'Resumen Factoraje con Recurso',
		frame: false,
		listeners: {
			rowclick: function(grid, rowindex, evento) {
				procesarContinuar(grid, rowindex, null, 2);
			}
		},
		columns: [
			{
				header: 'Cadena',
				tooltip: 'Nombre de la Cadena Productiva',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				width: 210,
				resizable: true,
				hidden: false,
				renderer:  function (valor, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
					return valor;
				}
			},
			{
				header: 'Num. Doctos.',
				tooltip: 'Cantidad de Documentos publicados',
				dataIndex: 'NUMDOCTOSMN',
				sortable: true,
				width: 80,
				resizable: true,
				align: 'right',
				hidden: false
			},
			{
				header: 'Monto',
				tooltip: 'Monto de Documentos',
				dataIndex: 'MONTOMN',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Num. Doctos.',
				tooltip: 'Cantidad de Documentos publicados',
				dataIndex: 'NUMDOCTOSUSD',
				sortable: true,
				width: 80,
				resizable: true,
				align: 'right',
				hidden: false
			},
			{
				header: 'Monto',
				tooltip: 'Monto de Documentos',
				dataIndex: 'MONTOUSD',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				//xtype: 'actioncolumn',
				header: 'Elegir',
				tooltip: 'Elegir Cadena Productiva',
				width: 40,
				align: 'center',
				renderer: function() {
					return "<img class='icoContinuar' style='cursor:pointer' src='" + Ext.BLANK_IMAGE_URL + "'>";
				}
			}
		]
	});
//-------------------------------- PRINCIPAL -----------------------------------

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			grid,
			gridDistribuidores,
			gridFactRecurso
		]
	});

//-------------------------------- ----------------- -----------------------------------
	consultaData.load();
	consultaDistribuidoresData.load();
	consultaFactRecursoData.load();


});