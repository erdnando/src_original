<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,		
		java.util.Vector,
		java.io.*,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.distribuidores.*,
		com.netro.servicios.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%

String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String sesClaveAfiliado = (String)session.getAttribute("iNoCliente");
StringBuffer condicion = new StringBuffer();

String infoRegresar = "";

if (informacion.equals("Consulta")) {
	ISeleccionDocumento ejb = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	Registros regResumen = ejb.getResumenPublicacionXEpo(sesClaveAfiliado);
	infoRegresar = "{\"success\": true, \"total\": " + regResumen.getNumeroRegistros() + ", \"registros\": " + regResumen.getJSONData() + "}";
} else if (informacion.equals("SeleccionEpo")) {
	//**************************** Elimina variables de session que son establecida en carga_variables  *************************
	 session.removeAttribute("inicializar.FINALIZADO");	//Elimina el identificador para que se marque la sesión de trabajo como no finalizada y pueda pasar por las demas pantallas intermedias de incialización (por ejemplo encuestas, noticias, etc...)
	 //Elementos que se muestran en el menu lateral
	 session.removeAttribute("sesMenu");	//Menu principal
	 session.removeAttribute("sesMenuDirecto");	//Menu de Acceso Directo
	 session.removeAttribute("sesMensajesIniciales");	//Mensajes Iniciales
	 session.removeAttribute("sesEncuestas");	//Encuestas


	 
	//***************************************************************************************************************************

	String claveEpo = request.getParameter("ic_epo");
	String tipoCliente = request.getParameter("ic_tipo_cliente");
	tipoCliente = (tipoCliente == null || tipoCliente.equals(""))?"1":tipoCliente;	//1.- Proveedor 2.- Distribuidor
	JSONObject jsonObj = new JSONObject();
	
	try {
		if (claveEpo == null || claveEpo.equals("")) {
			throw new Exception("La clave de EPO debe ser especificada ");
		}
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	session.setAttribute("iNoEPO", claveEpo);
	session.setAttribute("strEpo-Pyme", "epoRegistrada");

	Navegacion nav = (Navegacion)session.getAttribute("navegacion");
	nav.setTipoCliente(tipoCliente);	//Este es para más adelante ( en carga_variables) determinar la pantalla principal que se le mostrará a la Pyme


	List pantallasNavegacionInicial = (List)session.getAttribute("inicializar.PantallasBasicas");
	if (pantallasNavegacionInicial == null || pantallasNavegacionInicial.isEmpty()) {
		//Puede ya haber entrado al sistema... y esta cambiando de EPO
		//por lo que pantallasNavegacionInicial estaria vacio
		//Vuelve a carga la ruta de navegacion a partir de esta pantalla
		pantallasNavegacionInicial = nav.getPantallasBasicas();
		Iterator it = pantallasNavegacionInicial.iterator();
		while(it.hasNext()) {
			String url = (String)it.next();
			//Tiene que eliminar las paginas de la navegación previas a eligeEpoExt.jsp
			if ("/20secure/eligeEpoExt.jsp".equals(url)) {
				break;
			} else {
				it.remove();
			}
		}
	}
	
	pantallasNavegacionInicial.remove("/20secure/eligeEpoExt.jsp");  //El nombre debe coincidir con el especificado en Navegacion

	/*ServiciosHome serviciosHome = (ServiciosHome)ServiceLocator.getInstance().getEJBHome("ServiciosEJB",ServiciosHome.class);	
	Servicios servicios = serviciosHome.create();
	List encuesta = servicios.hayEncuestas( "ADMIN PYME",  sesClaveAfiliado,  claveEpo);
	String urlPaginaSiguiente = "";
	if (encuesta != null && encuesta.size() > 0) { //Hay encuestas por contestar
		urlPaginaSiguiente = "encuestasExt.jsp";
	} else {
		urlPaginaSiguiente = "carga_variables.jsp";
	}*/
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlPagina", appWebContextRoot + pantallasNavegacionInicial.get(0));
	infoRegresar = jsonObj.toString();
} else if (informacion.equals("ConsultaDistribuidores")) {
	AceptacionPyme ejb = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
	Registros regResumen = ejb.getResumenPublicacionXEpo(sesClaveAfiliado);
	infoRegresar = "{\"success\": true, \"total\": " + regResumen.getNumeroRegistros() + ", \"registros\": " + regResumen.getJSONData() + "}";

} else if (informacion.equals("ConsultaFactRecurso")) {
	AceptacionPyme ejb = ServiceLocator.getInstance().lookup("AceptacionPymeEJB", AceptacionPyme.class);
	Registros regResumen = ejb.getResumenPublicacionXEpoFacRec(sesClaveAfiliado);
	infoRegresar = "{\"success\": true, \"total\": " + regResumen.getNumeroRegistros() + ", \"registros\": " + regResumen.getJSONData() + "}";
}
%>
<%=infoRegresar%>
