Ext.onReady(function() {

//--------------------------------- HANDLERS -------------------------------
	var procesarSuccessFailureVerificaExistenciaCertificado = function(opts, success, response) {
				//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			if (jsonObj.generado == false) {
				panel.update(Ext.getDom("mensaje1").innerHTML);
				Ext.getCmp('btnContinuar').enable();
			} else {
				panel.update(Ext.getDom("mensaje2").innerHTML);
				Ext.getCmp('btnContinuar').disable();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var verificarExistenciaCertificado = function() {
			Ext.Ajax.request({
			url: '20generarCertificadoExt.data.jsp',
			params: {
				informacion: "VerificaExistenciaCertificado"
			},
			callback: procesarSuccessFailureVerificaExistenciaCertificado
		});
	}
//-------------------------------- STORES -----------------------------------

//-------------------------------------------------------------------


	var panel = new Ext.Panel({
		hidden: false,
		height: 'auto',
		width: 700,
		title: 'Certificado Digital',
		frame: true,
		bodyStyle: 'text-align:center',
		buttons: [
			{
				id: 'btnContinuar',
				disabled: true,
				text: 'Generar Certificado',
				handler: function(boton, evento) {
					boton.disable();
					var forma = Ext.getDom('formAux');
					forma.submit();
				}
			}
		]
	});

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			panel
		]
	});

//-------------------------------- ----------------- -----------------------------------

verificarExistenciaCertificado();

});