<%--
En este archivo deber�a de ir unicamente el establecimiento de variables
que dependen de una selecci�n por parte del usuario.
Por ejemplo, que una pyme haya seleccionado una epo con la cual desea
trabajar.

Por lo que hay cosas que no deberia de ir en este archivo...
Se dejan con motivos de compatibilidad... pero deben moverse
en cuanto se migren los dem�s tipos de usuario al ExtJS y ya no se use
la version anterior de N@E
--%>

<%@ page import="java.util.*, java.text.*, java.io.*, java.sql.*,
		netropology.utilerias.*, netropology.utilerias.usuarios.*, com.netro.afiliacion.AfiliadoInfo"
errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf"%>
<%
// Set the Expires and Cache Control Headers
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setHeader("Expires", "Thu, 29 Oct 1969 17:04:19 GMT");
  
// Set request and response type
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");

String gsCveUsuario = "", gsIP = "", gsHost = "", gsTpUsuario = "",
		strTipoUsuario = "", iNoCliente = "", strNombre = "", strLogo = "", strClase = "", iNoUsuario = "",
		iNoIF = "", iNoEPO = "", iNoDistribuidor = "", iNoPYME = "", strNombreUsuario = "", 
		strSerial = "",
		strDirectorioPublicacion = (String)application.getAttribute("strDirectorioPublicacion");
boolean esEsquemaExtJS = (session.getAttribute("version") != null && session.getAttribute("version").equals("2011"))?true:false;
List params = null;
AccesoDB con = new AccesoDB();
boolean bExito = true;
List pantallasNavegacionComplementaria = new ArrayList();

try {
	gsCveUsuario      = (String) session.getAttribute("sesCveUsuario");
	
	iNoUsuario        = (String) session.getAttribute("iNoUsuario");
	strNombreUsuario  = (String) session.getAttribute("strNombreUsuario");
	
	strTipoUsuario    = (String) session.getAttribute("strTipoUsuario");
	iNoCliente        = (String) session.getAttribute("iNoCliente");
	
	strLogo           = (String) session.getAttribute("strLogo");
	strClase          = (String) session.getAttribute("strClase");
	iNoEPO            = (String) session.getAttribute("iNoEPO");
	String strPerfil = (String) session.getAttribute("sesPerfil");
	
	//--------------------  -----------------------
	//Indica que el m�dulo no ha sido inicializado para el afiliado
	session.setAttribute("inicializar.13DESCUENTO", new Boolean(false));
	session.setAttribute("inicializar.24DISTRIBUIDORES", new Boolean(false));
	session.setAttribute("inicializar.34CESION", new Boolean(false));
	
	
	
	//_---------------------------------------------------------------------------------------------------------------------------------
	
/*	out.print("strLogo: " + strLogo + "<br>");
	out.print("strClase: " + strClase + "<br>");
	out.print("iNoUsuario: " + iNoUsuario + "<br>");
	out.print("strTipoUsuario: " + strTipoUsuario + "<br>");
	out.print("iNoUsuario: " + iNoUsuario + "<br>");
	out.print("iNoCliente: " + iNoCliente + "<br>");
	out.print("iNoEPO: " + iNoEPO + "<br>");
*/
	gsIP = (String) request.getRemoteAddr();
	gsHost = (String) request.getRemoteHost();
	
	con.conexionDB();
	PreparedStatement pstmt2 = null;
	//********* NOS TRAEMOS EL NUMERO DE SERIE DEL CERTIFICADO DE LA BD
	String qryTraeSerial = 
			" SELECT dn_user, '20secure/carga_variables.jsp' " +
			" FROM users_seguridata "+
			" WHERE table_uid like UPPER(?) ";
	params = new ArrayList();
	
	params.add(iNoUsuario + "%");
	Registros regSerial = con.consultarDB(qryTraeSerial, params);
	con.terminaTransaccion(true);	//Debido a que users_seguridata es remota
	
	if(regSerial.next()) {
		strSerial = regSerial.getString("dn_user").trim();
	} else {
		strSerial = "";
	}

//------------------------------------------------------------------------------		
	String sQuery = 
			" SELECT sc_tipo_usuario " +
			" FROM seg_perfil " +
			" WHERE cc_perfil = ? ";
	
	params = new ArrayList();
	params.add((String)session.getAttribute("sesPerfil"));
	Registros regTipoUsr = con.consultarDB(sQuery, params);

	int iTipoAfiliado = 0;
	
	if (regTipoUsr.next()) {
		iTipoAfiliado = Integer.parseInt(regTipoUsr.getString("SC_TIPO_USUARIO"));   //comcat_tipo_afiliado.ic_tipo_afiliado
	}
	
	session.setAttribute("iTipoPerfil", Integer.toString(iTipoAfiliado));
//------------------------------------------------------------------------------

	String strNombreEmpresa = "";
	String qryNombreEmp = "";
	String strPais = "MEXICO";
	if(strTipoUsuario.equals("NAFIN")) {
		if (esEsquemaExtJS) {
			session.setAttribute("sesMenu", MenuUsuario.getMenuNafin(strPerfil, strSerial, appWebContextRoot));
			session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));

			Navegacion nav = new Navegacion();
			nav.setTipoAfiliado("N");
			//nav.setPerfil(strPerfil);
			pantallasNavegacionComplementaria = nav.getPantallasComplementarias();
			session.setAttribute("inicializar.PantallasComplementarias", pantallasNavegacionComplementaria);
		}

		if("ADMIN BANCOMEXT".equals(session.getAttribute("sesPerfil"))) {
			strNombreEmpresa = "Administrador Bancomext";
		} else {
				strNombreEmpresa = "Administrador Nafin";
		}
		
		
	} else if(strTipoUsuario.equals("EPO")) {
		
		session.setAttribute("sesMenu", MenuUsuario.getMenuEpo(strPerfil, iNoCliente, strSerial, appWebContextRoot));
		session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
		
		Navegacion nav = new Navegacion();
		nav.setClaveAfiliado(iNoCliente);
		nav.setTipoAfiliado("E");
		nav.setPerfil(strPerfil);
		nav.setMenuUsuario((String)session.getAttribute("sesMenu"));	//Las opciones de menu que tenga habilitado el usuario podran afectar navegacion
		pantallasNavegacionComplementaria = nav.getPantallasComplementarias();
		session.setAttribute("inicializar.PantallasComplementarias", pantallasNavegacionComplementaria);
				
		
		//Fodea 010-2014  Para actualizar el logo
		File fdGif = new File(strDirectorioPublicacion + "/00archivos/15cadenas/15archcadenas/logos/" + iNoEPO + ".gif");
		File fdGifHome = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + iNoEPO + "_home.gif");
	
		if(fdGif.exists() || fdGifHome.exists()) {
			strLogo = iNoEPO + ".gif";
			if(fdGifHome.exists()) {
				session.setAttribute("sesLogoAlterno", iNoEPO + "_home.gif");
			}
		} else {
			strLogo = "nafin.gif";
		}
		
		//Determina si existe dise�o para la version de Extjs.
		File dirNuevoDiseno = new File(strDirectorioPublicacion + "/00utils/css/"+ iNoEPO);
		if (dirNuevoDiseno.exists()) {
			session.setAttribute("sesDiseno", iNoEPO);
			File dirNuevoDisenoBanner = new File(strDirectorioPublicacion + "/00utils/css/"+ iNoEPO + "/banners.jsp");
			if (dirNuevoDisenoBanner.exists()) {
				session.setAttribute("sesDisenoBanner", iNoEPO);
			} else {
				session.setAttribute("sesDisenoBanner", "nafin");
			}
		} else {
			session.setAttribute("sesDiseno", "nafin");
			session.setAttribute("sesDisenoBanner", "nafin");
		}
				
		
	} else if(strTipoUsuario.equals("IF")) {
		session.setAttribute("sesMenu", MenuUsuario.getMenuIF(strPerfil, iNoCliente, strSerial, appWebContextRoot));
		session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
		
		Navegacion nav = new Navegacion();
		nav.setClaveAfiliado(iNoCliente);
		nav.setTipoAfiliado("I");
		nav.setPerfil(strPerfil);
                nav.setINoUsuario(iNoUsuario);
		nav.setMenuUsuario((String)session.getAttribute("sesMenu"));	//Las opciones de menu que tenga habilitado el usuario podran afectar navegacion
		pantallasNavegacionComplementaria = nav.getPantallasComplementarias();
		session.setAttribute("inicializar.PantallasComplementarias", pantallasNavegacionComplementaria);

	} else if(strTipoUsuario.equals("PYME")){
		session.setAttribute("sesMenu", MenuUsuario.getMenuPyme(strPerfil, iNoCliente, iNoEPO, strSerial, appWebContextRoot));
		session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuPymeDirecto((String)session.getAttribute("sesMenu"), strPerfil));
		
		if (esEsquemaExtJS) {
			Navegacion nav = (Navegacion)session.getAttribute("navegacion");		
			nav.setClaveEpoRelacionada(iNoEPO);
			nav.setMenuUsuario((String)session.getAttribute("sesMenu"));	//Las opciones de menu que tenga habilitado el usuario podran afectar navegacion
			pantallasNavegacionComplementaria = nav.getPantallasComplementarias();
			session.setAttribute("inicializar.PantallasComplementarias", pantallasNavegacionComplementaria);
		}
		
		File fdGif = new File(strDirectorioPublicacion + "/00archivos/15cadenas/15archcadenas/logos/" + iNoEPO + ".gif");
		File fdGifHome = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + iNoEPO + "_home.gif");
	
		if(fdGif.exists() || fdGifHome.exists()) {
			strLogo = iNoEPO + ".gif";
			if(fdGifHome.exists()) {
				session.setAttribute("sesLogoAlterno", iNoEPO + "_home.gif");
			}
		} else {
			strLogo = "nafin.gif";
		}
		
		//Determina si existe dise�o para la version de Extjs.
		File dirNuevoDiseno = new File(strDirectorioPublicacion + "/00utils/css/"+ iNoEPO);
		if (dirNuevoDiseno.exists()) {
			session.setAttribute("sesDiseno", iNoEPO);
			File dirNuevoDisenoBanner = new File(strDirectorioPublicacion + "/00utils/css/"+ iNoEPO + "/banners.jsp");
			if (dirNuevoDisenoBanner.exists()) {
				session.setAttribute("sesDisenoBanner", iNoEPO);
			} else {
				session.setAttribute("sesDisenoBanner", "nafin");
			}
		} else {
			session.setAttribute("sesDiseno", "nafin");
			session.setAttribute("sesDisenoBanner", "nafin");
		}
		
	}  else if(strTipoUsuario.equals("EXPORTADOR")) {

	} else if(strTipoUsuario.equals("CLIENTE")){

	} else if(strTipoUsuario.equals("MANDANTE")) {

	} 	else if(strTipoUsuario.equals("AFIANZADORA")){

	} 	else if(strTipoUsuario.equals("FIADO")){

	}
		
	//*******NOS TRAEMOS EN NOMBRE DE LA EMPRESA SI ES PYME - EPO - IF
	if(!strTipoUsuario.equals("NAFIN") && !strTipoUsuario.equals("DIST") &&
			!strTipoUsuario.equals("EXEMPLEADO")){

		strNombreEmpresa = AfiliadoInfo.getNombreAfiliado(Integer.parseInt(iNoCliente), String.valueOf(iTipoAfiliado));
	}
		
	strNombre = strNombreEmpresa;
	if(strNombre==null || strNombre.equals("null"))strNombre="";
	session.setAttribute("strNombre",strNombre);
	session.setAttribute("strPais",strPais);
	session.setAttribute("Clave_usuario",gsCveUsuario);
	session.setAttribute("strSerial",strSerial);
	
		
	//---------------------FIN CARGA INICIAL DE VARIABLES---------------------------------------------------
	
	//--------------------------------------------------------------------------------------------
	if((String) session.getAttribute("strEpo-Pyme") == null){
		if (iNoEPO == null) {
			gsTpUsuario = (String)session.getAttribute("strTipoUsuario");
		} else {
			gsTpUsuario = iNoEPO;
		}
	} else {
		gsTpUsuario = (String)session.getAttribute("iNoEPO");
		session.setAttribute("sesTpUsuario", (String)session.getAttribute("iNoEPO"));
		session.setAttribute("strEpo-Pyme", "");
	}
	
	// POR SI NO SE ENCUENTRA LOS ARCHIVOS (XML, CSS Y JS).
	gsTpUsuario = gsTpUsuario.toLowerCase();

	if(iTipoAfiliado == 8) {
			gsTpUsuario = "bancomext";
			strLogo = "bancomext.gif";
			strClase = "bancomext.css";
	} else if("CLIENTE EXT".equals(session.getAttribute("sesPerfil"))) {
			gsTpUsuario = "nafin";
			strLogo = "nafin.gif";
			strClase = "nafin.css";
	} else {
		File fCC  = new File(strDirectorioPublicacion + "14seguridad/Seguridad/css/"+gsTpUsuario+".css");
		File fJs  = new File(strDirectorioPublicacion + "14seguridad/Seguridad/css/"+gsTpUsuario+".js");
		File fXML = new File(strDirectorioPublicacion + "14seguridad/Seguridad/xml/"+gsTpUsuario+".xml");
		
		if (!esEsquemaExtJS && (!fCC.exists() || !fJs.exists() || !fXML.exists())) {
			//Esto solo aplica si no es ExtJS
			gsTpUsuario = "nafin";
			strLogo = "nafin.gif";
			strClase = "nafin.css";
		}
	}
	
	session.setAttribute("strLogo", strLogo);
	session.setAttribute("strClase", strClase);
	session.setAttribute("sesTpUsuario", gsTpUsuario);
	

/**************************** BITACORA *************************************/

	String qryIBitDiaria = 
			" INSERT INTO bit_diaria (f_consulta,c_sistema,c_facultad,ic_usuario,t_ip,c_host) "+
			" VALUES(SYSDATE,'ADMCA','LOGIN',?,?,?)";
	
	PreparedStatement psBit = con.queryPrecompilado(qryIBitDiaria);
	psBit.setString(1, iNoUsuario);
	psBit.setString(2, gsIP);
	psBit.setString(3, gsHost);
	psBit.executeUpdate();
	
	bExito = true;
	
	if(esEsquemaExtJS) {
		List pantallasNavegacionInicial = (List)session.getAttribute("inicializar.PantallasBasicas");
		pantallasNavegacionInicial.remove("/20secure/carga_variables.jsp");  //El nombre debe coincidir con el especificado en Navegacion
		if (!pantallasNavegacionInicial.isEmpty()) {
			//carga_variables debe ser siempre la ultima pantalla de la navegacion inicial.
			//Si hay registros en "pantallasNavegacionInicial" significa que se salt� alguna
			//pantalla
			response.sendRedirect(appWebContextRoot + "/15cadenas/15salir.jsp");
		} else {
			//Pantallas complementarias, despu�s que se establecieron variables
			//Por ejemplo para la pyme, pueden ser avisos, encuestas, etc., que dependen de la elecci�n de la EPO relacionada
			//Pero que deben mostrarse antes de ingresar a otras opciones de menu
			response.sendRedirect(appWebContextRoot + pantallasNavegacionComplementaria.get(0));
		}	
	} else {
		String lsExterno = (session.getAttribute("sesExterno")==null)?"":session.getAttribute("sesExterno").toString();
	%>
		<Script Language="JavaScript">
<%
		 if((String) session.getAttribute("strEpo-Pyme") == null){%>
				if ("<%=lsExterno%>" == "")	self.location.replace("ne.jsp");
				else self.location.replace("ne.jsp");
<%
		}

		if(!strTipoUsuario.equals("NAFIN") && !strTipoUsuario.equals("EXEMPLEADO") && !strTipoUsuario.equals("EXPORTADOR") && !strTipoUsuario.equals("CLIENTE")) {
%>
			
			var winSisNafin = window.open('20mensaje.jsp','winNafin', 			'ToolBar=No,Status=No,Location=No,MenuBar=No,ScrollBars=Yes,Resizable=Yes,Directories=No,Fullscreen=No,ChannelMode=No,top=0,left=0,Width=790,Height=550');
<%
		} else {
%>
			var winSisNafin = window.open('../15cadenas/frame.jsp','winNafin', 	'ToolBar=No,Status=Yes,Location=No,MenuBar=No,ScrollBars=Yes,Resizable=Yes,Directories=No,Fullscreen=No,ChannelMode=No,top=0,left=0,Width=790,Height=550');
<%
		}
%>
	   </script>	
<%
	}
%>
	</html>
<%
} catch(Exception e) {
	bExito = false;
	throw new AppException("Error al establecer el ambiente del usuario", e);
} finally {
	if (con.hayConexionAbierta()) {
		con.terminaTransaccion(bExito);
		con.cierraConexionDB();
	}
}
/**Verifica el cierre de servicio NO BORRAR***/
	//fnChkCierrePyme(1);
	fnChkCierrePyme(2);
	fnChkCierrePyme(4);
	fnChkCierrePyme(5);
/**Termina */
%>




<%!
/******************************************************************************************************
*	CIERRA LA BANDERA "cs_cierre_servicio" DE "comcat_producto_nafin"
******************************************************************************************************/
public void fnChkCierrePyme(int liProducto)	{
	AccesoDB con = new AccesoDB();
	try	{
		con.conexionDB();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String lsQry = "select cs_cierre_servicio, '20secure/carga_variables.jsp' "+
						" from comcat_producto_nafin WHERE ic_producto_nafin = ? ";
		//System.out.println(lsQry);
		pstmt = con.queryPrecompilado(lsQry);
		pstmt.setInt(1, liProducto);
		rs = pstmt.executeQuery();
		pstmt.clearParameters();
		if (rs.next()) {	//********* SI EL CIERRE ES IGUAL A "N" ES PORQUE ESTA ABIERTO 
			if (rs.getString("cs_cierre_servicio").equals("N")) {
				lsQry = "SELECT count(cg_horario_inicio), '20secure/carga_variables.jsp' " +
						"FROM(select /*+ index(he CP_COMREL_HORARIO_X_EPO_PK )*/ cg_horario_inicio, cg_horario_fin " +
							" from comrel_horario_x_epo he " +
							" where TO_DATE(cg_horario_fin, 'hh24:mi') > TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
							" and ic_producto_nafin = ? " + 
							" UNION ALL " +
							" select cg_horario_inicio, cg_horario_fin FROM comcat_producto_nafin " +
							" where TO_DATE(cg_horario_fin, 'hh24:mi') > TO_DATE(TO_CHAR(SYSDATE, 'hh24:mi'), 'hh24:mi') " +
							" and ic_producto_nafin = ? )";
				//System.out.println(lsQry);
				pstmt = con.queryPrecompilado(lsQry);
				pstmt.setInt(1, liProducto);
				pstmt.setInt(2, liProducto);
				rs = pstmt.executeQuery();
				pstmt.clearParameters();
				if(rs.next()) {	//****** SI YA NO HAY HORARIOS ABIERTOS DE PYME => SE CIERRA LA BANDERA
					if(rs.getInt(1)==0)	{
						lsQry = "UPDATE comcat_producto_nafin SET cs_cierre_servicio = 'S' " +
								"WHERE ic_producto_nafin = " + liProducto;
						try {
							con.ejecutaSQL(lsQry);
							con.terminaTransaccion(true);
						} catch(SQLException sqle) {
							con.terminaTransaccion(false);
							System.out.print("\nError: " + sqle.getMessage());
						}
					}
				}
				rs.close();
			}
		}
		rs.close();
	} catch (Exception e)	{
		System.out.print("\nError: " + e.getMessage());
	} finally {
		if (con.hayConexionAbierta()) con.cierraConexionDB();
	}
}	// Fin del M�todo.
%>
