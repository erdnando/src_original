<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252"	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%@ include file="/01principal/menu.jspf" %>

<script type="text/javascript" src="20mensajeExtVer.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if ("PYME".equals(strTipoUsuario)) {%>
<%@ include file="/01principal/01pyme/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01pyme/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01pyme/pie.jspf"%>
<%
} else if ("EPO".equals(strTipoUsuario)) {%>
<%@ include file="/01principal/01epo/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01epo/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01epo/pie.jspf"%>
<%
} else if ("IF".equals(strTipoUsuario)) {%>
<%@ include file="/01principal/01if/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01if/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01if/pie.jspf"%>
<%
}
%>
<form id='formAux' name="formAux" target='_new'></form>

<%-- Paraetros iniciales de la forma... se reciben y se colocan en un hidden --%>
<input type="hidden" id="msgini" value="<%=request.getParameter("msgini")%>">
<input type="hidden" id="msgtitulo" value="<%=request.getParameter("msgtitulo")%>">
<input type="hidden" id="csAdicional" value="<%=request.getParameter("csAdicional")%>">

</body>
</html>

