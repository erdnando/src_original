Ext.onReady(function() {

//--------------------------------- HANDLERS -------------------------------
	var jsonMensajes = null;
	var procesarSuccessFailureObtieneMensajes = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonMensajes = Ext.util.JSON.decode(response.responseText);
			
			mostrarSiguienteMensaje();			

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var mensaje = null;	//Mensaje en turno

	var mostrarSiguienteMensaje = function() {
		var registros = jsonMensajes.registros;
		if (registros.length > 0) {
			panel.show();
			mensaje = registros[0];
			panel.setTitle('Fecha corte fin de mes: '+mensaje.DF_FIN_MES+' '+mensaje.CG_NOMBRE_COMERCIAL);
			var nuevoContenido = "";
			if (mensaje.DF_FIN_MES != "") {
				nuevoContenido += '<br><b>Estimado Cliente: En la opci�n denominada M�dulo de C�dula de Conciliaci�n, se encuentra a su disposici�n la informaci�n referente al saldo de cr�dito a cargo de esa Instituci�n con cifras al cierre del mes inmediato anterior para su aceptaci�n o comentarios, en este �ltimo caso puede utilizar el campo de Observaciones para emitirlos.' +
                                                              ' Es importante mencionar, que s� en los siguientes 10 d�as naturales posteriores a la fecha de la recepci�n de este mensaje no se recibe la C�dula firmada de aceptaci�n o comentarios, NAFIN dar� por aceptadas para todos los efectos que corresponda las cifras de cartera crediticia a cargo de esa Instituci�n</b>' +
						'<br><br><br>';
			}
			
			panel.update(nuevoContenido);
			
			var btnEnterado = Ext.getCmp('btnEnterado');
			var btnInteresado = Ext.getCmp('btnInteresado');
			var tbsInteresado = Ext.getCmp('tbsInteresado');
			var btnNoInteresado = Ext.getCmp('btnNoInteresado');
			var tbsNoInteresado = Ext.getCmp('tbsNoInteresado');
			if (true) {
				btnEnterado.setText('Enterado');
				btnInteresado.hide();
				tbsInteresado.hide();
				btnNoInteresado.hide();
				tbsNoInteresado.hide();
			} else {
				btnEnterado.setText('Enterado');
				btnInteresado.show();
				tbsInteresado.show();
				btnNoInteresado.show();
				tbsNoInteresado.show();
			}
			btnEnterado.show();			
			registros.splice(0,1); //Elimina el mensaje mostrado
			
		} else {
			procesarContinuar(Ext.getCmp("btnEnterado"));
		}
	}
	
	var obtenerMensajes = function() {
			Ext.Ajax.request({
			url: '20mensajeEdoCtaExt.data.jsp',
			params: {
				informacion: "ObtieneMensajes"
			},
			callback: procesarSuccessFailureObtieneMensajes
		});
	}

		


	var procesarSuccessFailureGuardarInteresMensaje = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonObj = Ext.util.JSON.decode(response.responseText);
			//window.location = jsonObj.urlPagina;
			Ext.MessageBox.alert(' ', jsonObj.mensaje, 
					function(boton, evento) {
						mostrarSiguienteMensaje();
					}
			);
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGuardarInteresMensaje = function(interes) {
		Ext.Ajax.request({
			url: '20mensajeEdoCtaExt.data.jsp',
			params: {
				informacion: "GuardarInteresMensaje",
				msgini: mensaje.IC_NOTIFICA,
				estatus: interes
			},
			callback: procesarSuccessFailureGuardarInteresMensaje
		});
	}

	function procesarSuccessFailureContinuar(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonObj = Ext.util.JSON.decode(response.responseText);
				window.location = jsonObj.urlPagina;
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarContinuar = function(boton) {
		boton.disable();
		Ext.Ajax.request({
			url: '20mensajeEdoCtaExt.data.jsp',
			params: {
				informacion: "Continuar",
                                msgini: mensaje.IC_NOTIFICA
			},
			callback: procesarSuccessFailureContinuar
		});
	}

//-------------------------------- STORES -----------------------------------

//-------------------------------------------------------------------


	var panel = new Ext.Panel({
		hidden: true,
		height: 'auto',
		boxMinHeight: 300,
		width: 700,
		title: 'Mensajes',
		frame: false,
		bodyStyle: 'text-align:center',
		bbar: [
			'->',
			{
				xtype: 'tbseparator',
				id: 'tbsInteresado',
				hidden: true
			},
			{
				xtype: 'button',
				id: 'btnInteresado',
				text: 'Si me interesa',
				hidden: true,
				handler: function(boton, evento) {
					procesarGuardarInteresMensaje('S');
				}
			},
			{
				xtype: 'tbseparator',
				id: 'tbsNoInteresado',
				hidden: true
			},
			{
				xtype: 'button',
				id: 'btnNoInteresado',
				text: 'No me interesa',
				tooltip: 'No volver a mostrar esta informaci�n',
				hidden: true,
				handler: function(boton, evento) {
					procesarGuardarInteresMensaje('N');
				}
			},
			'-',
			{
				xtype: 'button',
				id: 'btnEnterado',
				text: 'Enterado',
				hidden: true,
				handler: function(boton, evento) {
					mostrarSiguienteMensaje();
				}
			}
		]
	});

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			panel
		]
	});

//-------------------------------- ----------------- -----------------------------------

obtenerMensajes();


});