<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,		
		java.util.Vector,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.seguridadbean.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%

String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "";
JSONObject jsonObj = new JSONObject();
if (informacion.equals("VerificaExistenciaCertificado")) {
	com.netro.seguridadbean.Seguridad seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("generado", new Boolean(seguridad.certificadoGenerado(strLogin)));
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>
