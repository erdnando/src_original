<%@ page import="
		java.sql.*,
		java.util.*,
		java.io.*,
		com.netro.descuento.*,
		com.netro.cadenas.*,
		javax.naming.*,
		com.netro.afiliacion.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.Seguridad,
		com.netro.seguridadbean.*,
		netropology.utilerias.*" 
errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%
// Set the Expires and Cache Control Headers
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setHeader("Expires", "Thu, 29 Oct 1969 17:04:19 GMT");

// Set request and response type
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");

List pantallasNavegacionInicial = new ArrayList();

String mecanismoLogin = ParametrosGlobalesApp.getInstance().getParametro("MECANISMO_LOGIN");
String loginUsuario = null;
if ("DUMMY".equals(mecanismoLogin)) {
	loginUsuario = request.getParameter("ic_usuario");
} else {
%>
	<%@ include file="/00utils/sso/ssoinclude.jsp" %>	


<%
	//usrInfo contiene el siguiente formato: 00000012/Nombre Compañia
	//El primer elemento es el login del usuario.
	
	loginUsuario = (String) (Comunes.explode("/", usrInfo)).get(0);
	
	if (application.getAttribute("urlLogout") == null) {
		application.setAttribute("urlLogout", logoutURL+"?p_done_url=" + requestURL);
	}
}

session.setAttribute("sesDiseno", "nafin");  //Establece el diseño predeterminado. Este valor puede modificarse en carga_variables segun el afiliado.
session.setAttribute("sesDisenoBanner", "nafin"); //Establece los banners predeterminados. Este valor puede modificarse en carga_variables segun el afiliado.

String sCveUsuario = "",gsRespuesta = "";
String iNoUsuario = null,iNoIF = null,iNoEPO = null,iNoPYME = null, iNoDistribuidor = null, iNoU = null, iAfianzadora = null, ifiado =null,iIFNB =null;
String strNombreUsuario = "",strTipoUsuario = "",iNoExportador = null, iNoClient = null, iNoMandante = null;
String strTipoAfiliacion = "", qryTraeNumNafinElect = "", strTipo = "";
String strLogo = "",	strClase = "";
String strCorreoUsuario = "";
String strDirectorioPublicacion = (String) application.getAttribute("strDirectorioPublicacion");
String tipo_usuario = "0";
PreparedStatement pstmt = null;
String  iNoAfianza = null, iNoFiado = null;//F037-2011 FVR

AccesoDB con = new AccesoDB();
sCveUsuario  = loginUsuario;

UtilUsr utilUsr = new UtilUsr();

Usuario usuario = utilUsr.getUsuario(sCveUsuario);

/***
Usuario usuario = new Usuario();
usuario.setClaveAfiliado("007");
usuario.setTipoAfiliado("N");
usuario.setApellidoPaterno("Guzman");
usuario.setApellidoMaterno("Hurtado");
usuario.setNombre("Victor Hugo");
usuario.setEmail("vguzmanh@nafin.gob.mx");
usuario.setPerfil("ADMIN GESTGAR");

//ADMIN NAFIN 

//usuario.setPerfil("ADMIN NAFIN");

*/
usuario.setLogin(loginUsuario);

boolean bOk = false;
ResultSet rs1 = null;

session.setAttribute("sesCveUsuario",sCveUsuario);
session.setAttribute("sesContrasenia","");//Se deja por motivos de compatibilidad?
session.setAttribute("sesIdentificacion", "");
session.setAttribute("sesTipoBrowser", request.getParameter("hddTipoBrowser"));

String bPymeRelacionadaVariasEpos = "N";

iNoUsuario = usuario.getLogin();

String claveAfiliado = usuario.getClaveAfiliado();
String tipoAfiliado = usuario.getTipoAfiliado();
String perfil = usuario.getPerfil();

session.setAttribute("sesPerfil", perfil);
session.setAttribute("sesTipoAfiliado", tipoAfiliado);


if (tipoAfiliado.equals("P")) {	//Pyme
	iNoPYME = claveAfiliado;
} else if (tipoAfiliado.equals("E")) {	//EPO
	iNoEPO = claveAfiliado;
	session.removeAttribute("generaInfoGraficasThread");
} else if (tipoAfiliado.equals("I")) {	//IF
	iNoIF = claveAfiliado;
} else if (tipoAfiliado.equals("U")) {	//IF
	iNoU = claveAfiliado;
} else if (tipoAfiliado.equals("N")) {	//Nafin
	//TO DO
} else if (tipoAfiliado.equals("A")) {	//Afianzadora
	iAfianzadora = claveAfiliado;
} else if (tipoAfiliado.equals("F")) {	//Fiado
	ifiado = claveAfiliado;
} else if (tipoAfiliado.equals("C")) {	//Cliente Externo
	iNoClient = claveAfiliado;
	
}else if (tipoAfiliado.equals("CE")) {	//Cliente Externo
	iIFNB = claveAfiliado;
	
}
 else if (tipoAfiliado.equals("BXT")) {	//Bancomext
	//TO DO
}

else {
	throw new AppException("Error en el usuario " + iNoUsuario + ". El tipo de usuario no esta especificado.");
}

strNombreUsuario = usuario.getApellidoPaterno() + " " +
		usuario.getApellidoMaterno() + " " + usuario.getNombre();
strCorreoUsuario	= usuario.getEmail();


//El tipo de afiliación sólo aplica para pyme y es A cuando es de alto riesgo y B de lo contrario
strTipoAfiliacion = (perfil.equals("ADMIN PYME"))?"A":"B";


session.setAttribute("iNoUsuario",        iNoUsuario.trim());
session.setAttribute("strNombreUsuario",  strNombreUsuario.trim());
session.setAttribute("strCorreoUsuario",  strCorreoUsuario.trim());
session.setAttribute("strTipoAfiliacion", strTipoAfiliacion.trim());

session.setAttribute("version", "2011"); //Indicador de la version de la Aplicacion. 2011.- Es la version con ExtJS

try {
	con.conexionDB();

	if(iNoPYME != null) {
		/********************************
		*              PYME
		********************************/
		Navegacion nav = new Navegacion();
		session.setAttribute("navegacion", nav);
		
		nav.setClaveAfiliado(iNoPYME);
		nav.setTipoAfiliado("P");
		pantallasNavegacionInicial = nav.getPantallasBasicas();
		
		session.setAttribute("strTipoUsuario", "PYME");
		session.setAttribute("sesiNoUsuario" , "3");

		String strCveVersionConvenio = "";
		String strIcTipoCliente		= "";
		String pymeUnicamenteEnEpoBloqueada = "N";
		if(Integer.parseInt(iNoPYME) > 0){  ///   PYME INTERNO.

			session.setAttribute("iNoCliente"    , iNoPYME);
			strTipo = "P";
			/*//CHECAMOS CON CUANTAS EPOS ESTA RELACIONADA LA PYME
			String qryRelPyemEpo =
					" SELECT COUNT(*) AS numRelacionesPymeEpo " +
					" FROM comrel_pyme_epo pe, comcat_epo e  " +
					" WHERE pe.ic_epo = e.ic_epo  " +
					" 	AND e.cs_habilitado = 'S' " +
					" 	AND pe.ic_pyme = ? ";
			pstmt = con.queryPrecompilado(qryRelPyemEpo);
			long numero1=Long.parseLong(iNoPYME);
			pstmt.setLong(1,numero1);
			ResultSet rsTraeRelPYMEEPO = pstmt.executeQuery();
			pstmt.clearParameters();

			if(rsTraeRelPYMEEPO.next()) {

				pymeUnicamenteEnEpoBloqueada = (rsTraeRelPYMEEPO.getInt("numRelacionesPymeEpo") == 0)?"S":"N";
				bPymeRelacionadaVariasEpos = (rsTraeRelPYMEEPO.getInt("numRelacionesPymeEpo") > 1)?"S":"N";
			}
			rsTraeRelPYMEEPO.close();
			pstmt.close();

			//---------Determina si una pyme es Internacional o no------------
			String qryPymeInternacional =
					" SELECT cs_internacional, '20secure/index.jsp' " +
					" FROM comcat_pyme WHERE ic_pyme = ? ";
			pstmt = con.queryPrecompilado(qryPymeInternacional);
			pstmt.setInt(1, Integer.parseInt(iNoPYME));
			rs1 = pstmt.executeQuery();
			if(rs1.next()){
				session.setAttribute("sesPymeInternacional" ,
						rs1.getString("cs_internacional"));
			}
			rs1.close();
			pstmt.close();*/



			//VERIFICA SI LA PYME TIENE FIANZA ELECTRONICA Y OBTIENE CLAVE DEL FIADO QUE LE CORRESPONDE
			String cveFiado = "";
			String qryPymeFiado = " SELECT ff.ic_fiado FROM comcat_pyme cp, fe_fiado ff "+
										" WHERE cp.cg_rfc = ff.cg_rfc "+
										" AND cp.cs_fianza_electronica = ? "+
										" AND cp.ic_pyme = ? ";

			pstmt = con.queryPrecompilado(qryPymeFiado);
			pstmt.setString(1,"S");
			pstmt.setInt(2, Integer.parseInt(iNoPYME));
			rs1 = pstmt.executeQuery();
			if(rs1.next()){
				cveFiado = rs1.getString("ic_fiado");
			}
			session.setAttribute("sesClaveFiado", cveFiado);
			rs1.close();
			pstmt.close();

			//-----------------------------------------------------------------

			//Si la pyme solo está afiliada a una EPO inhabilitada, entonces se le muestra un mensaje y
			//no le permite entrar
			/*if (pymeUnicamenteEnEpoBloqueada.equals("S")) {
				response.sendRedirect("20epo_bloqueada.jsp");
				return;
			}

			if (session.getAttribute("sesPymeInternacional").equals("S")) {
				response.sendRedirect("20seleccion_idioma.jsp?bPymeRelacionada=" +  bPymeRelacionadaVariasEpos);
			}

			if(!session.getAttribute("sesPymeInternacional").equals("S")) {
				response.sendRedirect("eligeEpoExt.jsp");
			} 
			*/
			/*else if(bPymeRelacionadaVariasEpos.equals("N")) {
				//QUERY PARA TRAERNOS EL NUMERO DE LA EPO A LA QUE PERTENECE LA PYME
				String qryNoEPO =
					"SELECT pe.ic_epo, e.ic_banco_fondeo, '20secure/index.jsp' " +
					" FROM comrel_pyme_epo pe, comcat_epo e " +
					" WHERE pe.ic_epo = e.ic_epo " +
					" 	AND e.cs_habilitado = ? AND pe.ic_pyme = ? ";
				pstmt = con.queryPrecompilado(qryNoEPO);
				pstmt.setString(1, "S");
				pstmt.setLong(2, Long.parseLong(iNoPYME));
				ResultSet rsNoEPO = pstmt.executeQuery();

				if(rsNoEPO.next()) {
					session.setAttribute("iNoEPO" , rsNoEPO.getString(1));
					session.setAttribute("sesBancoFondeo" , rsNoEPO.getString(2));
				} else {
					throw new AppException("El usuario no tiene privilegios para entrar al Sistema.");
				}

				rsNoEPO.close();
				pstmt.clearParameters();

				if(pstmt!=null) pstmt.close();

				File fdCSS = new File(strDirectorioPublicacion + "14seguridad/Seguridad/css/" + session.getAttribute("iNoEPO") + ".css");
				if(fdCSS.exists()) {
					strClase = session.getAttribute("iNoEPO") + ".css";
				} else {
					strClase = "nafin.css";
				}

				//Inicio F070-2005
				File fdGif = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + session.getAttribute("iNoEPO") + ".gif");
				File fdGifHome = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + session.getAttribute("iNoEPO") + "_home.gif");

				if(fdGif.exists() || fdGifHome.exists()) {
					strLogo = session.getAttribute("iNoEPO") + ".gif";
					if (fdGifHome.exists()) {
						session.setAttribute("sesLogoAlterno", session.getAttribute("iNoEPO") + "_home.gif");
					}
				} else {
					strLogo = "nafin.gif";
				}
				//Fin F070-2005

				session.setAttribute("strLogo"  , strLogo);
				session.setAttribute("strClase"  , strClase);
			}
*/


		//esto es para las Estadisticas Fodea 017-2011
		ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
		
		boolean xmlOk =false;
		xmlOk = BeanSeleccionDocumento.existeResumenOperacion(session.getAttribute("iNoCliente").toString());		
		System.out.println("xmlOk "+xmlOk);
		System.out.println("*****iNoCliente ***************"+session.getAttribute("iNoCliente").toString());	
		
		if(xmlOk ==false){
			BeanSeleccionDocumento.generarResumenOperacion(session.getAttribute("iNoCliente").toString());		
		}
		
		List xml = new ArrayList();			
		xml= BeanSeleccionDocumento.generarXmlEstadisticas(session.getAttribute("iNoCliente").toString());
		session.setAttribute("xmlsesEstadisticas" , xml);	
		
		}
	} else if(iNoEPO != null) {
		/********************************
		*              EPO
		**********/
		
		GeneraInfoGraficasThread zipThread = new GeneraInfoGraficasThread();
		zipThread.setCveEpo(iNoEPO);
		zipThread.setRunning(true);
		new Thread(zipThread).start();
		session.setAttribute("generaInfoGraficasThread", zipThread);
		
		System.out.println(" ENTRANDO USUARIO EPO.");
		Navegacion nav = new Navegacion();
		session.setAttribute("navegacion", nav);
		nav.setClaveAfiliado(iNoEPO);
		nav.setTipoAfiliado("E");
		nav.setPerfil(perfil);
		
		pantallasNavegacionInicial = nav.getPantallasBasicas();

		session.setAttribute("iNoEPO"        , iNoEPO);
		session.setAttribute("strTipoUsuario", "EPO");
		session.setAttribute("sesiNoUsuario" , "1");
		session.setAttribute("iNoCliente"    , iNoEPO);

		strTipo = "E";

		File fdGif = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + iNoEPO + ".gif");
		File fdGifHome = new File(strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/" + iNoEPO + "_home.gif");
		if(fdGif.exists() || fdGifHome.exists()) {
			session.setAttribute("strLogo", iNoEPO + ".gif");
			if (fdGifHome.exists()) {
				session.setAttribute("sesLogoAlterno", iNoEPO + "_home.gif");
			}
		} else {
			session.setAttribute("strLogo", "nafin.gif");
		}
		//Determina si existe diseño para la version de Extjs.
		File dirNuevoDiseno = new File(strDirectorioPublicacion + "/00utils/css/"+ iNoEPO);
		if (dirNuevoDiseno.exists()) {
			session.setAttribute("sesDiseno", iNoEPO);
			File dirNuevoDisenoBanner = new File(strDirectorioPublicacion + "/00utils/css/"+ iNoEPO + "/banners.jsp");
			if (dirNuevoDisenoBanner.exists()) {
				session.setAttribute("sesDisenoBanner", iNoEPO);
			} else {
				session.setAttribute("sesDisenoBanner", "nafin");
			}
		} else {
			session.setAttribute("sesDiseno", "nafin");
			session.setAttribute("sesDisenoBanner", "nafin");
		}
		
	} else if(iNoIF != null) {
		/********************************
		 *              IF
		 ********************************/
		
		System.out.println(" ENTRANDO USUARIO IF.");
		Navegacion nav = new Navegacion();
		nav.setClaveAfiliado(iNoIF);
		nav.setTipoAfiliado("I");
		nav.setPerfil(perfil);
		pantallasNavegacionInicial = nav.getPantallasBasicas();
		
		strTipo = "I";

		session.setAttribute("strTipoUsuario", "IF");
		session.setAttribute("sesiNoUsuario" , "2");
		session.setAttribute("iNoCliente"    , iNoIF);
		
		
		File fdGif = new File(strDirectorioPublicacion + "00archivos/if/logos/" + iNoIF + ".gif");
		if(fdGif.exists()) {
			//Se agrega un prefijo "IF_" para identificar que la imagen es de IF... 
			//aunque el archivo fisicamente no tiene ese prefijo. Es para corregir un detalle en PDFS que siempre buscan la ruta de logos de EPOS
			//El prefijo se debe remover en donde se ocupe esta variable de sesión
			session.setAttribute("strLogo", "IF_" + iNoIF + ".gif");	
		} else {
			session.setAttribute("strLogo", "if.gif");
		}
		
		//System.out.println(" CHECANDO RELACION DE IF.");
		String qryTraeRelIFEPO = 
				"SELECT count(1) as numRelacionesIfEpo, '20secure/index.jsp' " +
				" FROM comrel_if_epo " +
				" WHERE ic_if = ? ";

		pstmt = con.queryPrecompilado(qryTraeRelIFEPO);
		pstmt.setInt(1, Integer.parseInt(iNoIF));
		ResultSet rs = pstmt.executeQuery();
		if(rs.next()) {
			session.setAttribute("bIfRelacionada",  (rs.getInt(1) > 0)?"S":"N");
		}
		rs.close();
		pstmt.close();

		pstmt = con.queryPrecompilado(
				" SELECT cs_tipo " +
				" FROM comcat_if " +
				" WHERE ic_if = ? ");
		pstmt.setString(1, iNoIF);
		rs = pstmt.executeQuery();
		if(rs.next()) {
			session.setAttribute("strTipoBanco", rs.getString(1).trim());
		}
		rs.close();
		pstmt.close();

		bOk = true;
		try {
			//Esta inserción deberia meterse en otro componente... se deja aqui por tener menor impacto en lo que esta a punto de liberarse :-)
			String sqlAcceso = " INSERT INTO bit_acceso (ic_usuario,ic_afiliado,cg_tipo_afiliado,cg_version) " +
					" VALUES(?,?,?,?) ";

			pstmt = con.queryPrecompilado(sqlAcceso);
			pstmt.setString(1,iNoUsuario);
			pstmt.setInt(2, Integer.parseInt(claveAfiliado));
			pstmt.setString(3, tipoAfiliado);
			pstmt.setInt(4, 2);	//2=Nueva version (extjs)
			pstmt.executeUpdate();
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			System.out.println("Error al insertar en bit_acceso... ignorando: " + t.getMessage());
		} finally {
			try {
				if (pstmt != null ) {
					pstmt.close();
				}
				if(con.hayConexionAbierta()) {
					con.terminaTransaccion(bOk);
				}
			} catch(Throwable t1) {}
		}
	} else if(iNoU != null) {
	
		/********************************
		 *             UNIVERSIDAD
		********************************/
		
		String strNombreEmpresa = "", gsCveUsuario ="", qryNombreEmp = "", strPais = "MEXICO", strNombre="",
				 strSerial ="";	
		
		System.out.println(" ENTRANDO USUARIO UNIVERSIDAD.");
		
		Navegacion nav = new Navegacion();
		nav.setClaveAfiliado(iNoU);
		nav.setTipoAfiliado("U");
		nav.setPerfil(perfil);
		pantallasNavegacionInicial = nav.getPantallasComplementarias();
		
		strTipo = "U";
		session.setAttribute("strTipoUsuario", "UNIV");
		//session.setAttribute("sesiNoUsuario" , "2");
		session.setAttribute("iNoCliente"    , iNoU);
		session.setAttribute("strLogo", "if.gif");
		bOk = true;
			
		List pantallasNavegacionComplementaria = new ArrayList();
		
		try {
		
			PreparedStatement pstmt2 = null;
			//********* NOS TRAEMOS EL NUMERO DE SERIE DEL CERTIFICADO DE LA BD
			String qryTraeSerial = 
					" SELECT dn_user, '20secure/carga_variables.jsp' " +
					" FROM users_seguridata "+
					" WHERE table_uid like UPPER(?) ";
					
			List params = new ArrayList();			
			params.add(iNoUsuario + "%");
			Registros regSerial = con.consultarDB(qryTraeSerial, params);
			con.terminaTransaccion(true);	//Debido a que users_seguridata es remota
			
			if(regSerial.next()) {
				strSerial = regSerial.getString("dn_user").trim();
			} else {
				strSerial = "";
			}
		
			//Esta inserción deberia meterse en otro componente... se deja aqui por tener menor impacto en lo que esta a punto de liberarse :-)
			String sqlAcceso = " INSERT INTO bit_acceso (ic_usuario,ic_afiliado,cg_tipo_afiliado,cg_version) " +
					" VALUES(?,?,?,?) ";

			pstmt = con.queryPrecompilado(sqlAcceso);
			pstmt.setString(1,iNoUsuario);
			pstmt.setInt(2, Integer.parseInt(claveAfiliado));
			pstmt.setString(3, tipoAfiliado);
			pstmt.setInt(4, 2);	//2=Nueva version (extjs)
			pstmt.executeUpdate();
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			System.out.println("Error al insertar en bit_acceso... ignorando: " + t.getMessage());
		} finally {
			try {
				if (pstmt != null ) {
					pstmt.close();
				}
				if(con.hayConexionAbierta()) {
					con.terminaTransaccion(bOk);
				}
			} catch(Throwable t1) {}
		}
	
		String strPerfil = (String) session.getAttribute("sesPerfil");
		String iNoCliente        = (String) session.getAttribute("iNoCliente");
		session.setAttribute("sesMenu", MenuUsuario.getMenuUNI(strPerfil, iNoCliente, strSerial, appWebContextRoot));
		session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
		
		nav = new Navegacion();
		nav.setClaveAfiliado(iNoCliente);
		nav.setTipoAfiliado("U");
		nav.setPerfil(strPerfil);
		nav.setMenuUsuario((String)session.getAttribute("sesMenu"));	//Las opciones de menu que tenga habilitado el usuario podran afectar navegacion
		pantallasNavegacionComplementaria = nav.getPantallasComplementarias();
		session.setAttribute("inicializar.PantallasComplementarias", pantallasNavegacionComplementaria);

		
		strNombreEmpresa = AfiliadoInfo.getNombreAfiliado(Integer.parseInt(iNoCliente), "14"); //comcat_tipo_afiliado.ic_tipo_afiliado = 14 .- UNIVERSIDAD

		
		strNombre = strNombreEmpresa;
		if(strNombre==null || strNombre.equals("null"))strNombre="";
		session.setAttribute("strNombre",strNombre);
		session.setAttribute("strPais",strPais);
		session.setAttribute("Clave_usuario",gsCveUsuario);
		session.setAttribute("strSerial",strSerial);
		
	
	} else if(iAfianzadora != null) {
	
		/******  AFIANZADORA **************/
		
		String strNombreEmpresa = "", gsCveUsuario ="", qryNombreEmp = "", strPais = "MEXICO", strNombre="",	 strSerial ="";	
		
		System.out.println(" ENTRANDO USUARIO AFIANZADORA.");
		
		Navegacion nav = new Navegacion();
		nav.setClaveAfiliado(iAfianzadora);
		nav.setTipoAfiliado("A");
		nav.setPerfil(perfil);
		pantallasNavegacionInicial = nav.getPantallasComplementarias();
		
		strTipo = "A";
		session.setAttribute("strTipoUsuario", "AFIANZADORA");		
		session.setAttribute("iNoCliente"    , iAfianzadora);
		session.setAttribute("strLogo", "if.gif");
		bOk = true;
			
		List pantallasNavegacionComplementaria = new ArrayList();
		
		try {
		
			PreparedStatement pstmt2 = null;
			//********* NOS TRAEMOS EL NUMERO DE SERIE DEL CERTIFICADO DE LA BD
			String qryTraeSerial = 
					" SELECT dn_user, '20secure/carga_variables.jsp' " +
					" FROM users_seguridata "+
					" WHERE table_uid like UPPER(?) ";
					
			List params = new ArrayList();			
			params.add(iNoUsuario + "%");
			Registros regSerial = con.consultarDB(qryTraeSerial, params);
			con.terminaTransaccion(true);	//Debido a que users_seguridata es remota
			
			if(regSerial.next()) {
				strSerial = regSerial.getString("dn_user").trim();
			} else {
				strSerial = "";
			}
		
			//Esta inserción deberia meterse en otro componente... se deja aqui por tener menor impacto en lo que esta a punto de liberarse :-)
			String sqlAcceso = " INSERT INTO bit_acceso (ic_usuario,ic_afiliado,cg_tipo_afiliado,cg_version) " +
					" VALUES(?,?,?,?) ";

			pstmt = con.queryPrecompilado(sqlAcceso);
			pstmt.setString(1,iNoUsuario);
			pstmt.setInt(2, Integer.parseInt(claveAfiliado));
			pstmt.setString(3, tipoAfiliado);
			pstmt.setInt(4, 2);	//2=Nueva version (extjs)
			pstmt.executeUpdate();
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			System.out.println("Error al insertar en bit_acceso... ignorando: " + t.getMessage());
		} finally {
			try {
				if (pstmt != null ) { 
					pstmt.close();
				}
				if(con.hayConexionAbierta()) {
					con.terminaTransaccion(bOk);
				}
			} catch(Throwable t1) {}
		}
	
		String strPerfil = (String) session.getAttribute("sesPerfil");
		String iNoCliente        = (String) session.getAttribute("iNoCliente");
		session.setAttribute("sesMenu", MenuUsuario.getMenuAfianzadora(strPerfil, iNoCliente, strSerial, appWebContextRoot));
		session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
		
		nav = new Navegacion(); 
		nav.setClaveAfiliado(iNoCliente);
		nav.setTipoAfiliado("A");
		nav.setPerfil(strPerfil);
		nav.setMenuUsuario((String)session.getAttribute("sesMenu"));	//Las opciones de menu que tenga habilitado el usuario podran afectar navegacion
		pantallasNavegacionComplementaria = nav.getPantallasComplementarias();
		session.setAttribute("inicializar.PantallasComplementarias", pantallasNavegacionComplementaria);

		
		strNombreEmpresa = AfiliadoInfo.getNombreAfiliado(Integer.parseInt(iNoCliente), "12"); //AFIANZADORA 

		 
		strNombre = strNombreEmpresa;
		if(strNombre==null || strNombre.equals("null"))strNombre="";
		session.setAttribute("strNombre",strNombre);
		session.setAttribute("strPais",strPais);
		session.setAttribute("Clave_usuario",gsCveUsuario);
		session.setAttribute("strSerial",strSerial);
			
	
		
	} else if(ifiado != null) {
	
		/******  FIADO **************/
		
		String strNombreEmpresa = "", gsCveUsuario ="", qryNombreEmp = "", strPais = "MEXICO", strNombre="",	 strSerial ="";	
		
		System.out.println(" ENTRANDO USUARIO FIADO." +ifiado );
		
		Navegacion nav = new Navegacion();
		nav.setClaveAfiliado(ifiado);
		nav.setTipoAfiliado("F");
		nav.setPerfil(perfil);
		pantallasNavegacionInicial = nav.getPantallasComplementarias(); 
		
		strTipo = "F";
		session.setAttribute("strTipoUsuario", "FIADO");		
		session.setAttribute("iNoCliente"    , ifiado);
		session.setAttribute("strLogo", "if.gif");
		bOk = true;
			
		List pantallasNavegacionComplementaria = new ArrayList();
		
		try {
		
			PreparedStatement pstmt2 = null;
			//********* NOS TRAEMOS EL NUMERO DE SERIE DEL CERTIFICADO DE LA BD
			String qryTraeSerial = 
					" SELECT dn_user, '20secure/carga_variables.jsp' " +
					" FROM users_seguridata "+
					" WHERE table_uid like UPPER(?) ";
					
			List params = new ArrayList();			
			params.add(iNoUsuario + "%");
			Registros regSerial = con.consultarDB(qryTraeSerial, params);
			con.terminaTransaccion(true);	//Debido a que users_seguridata es remota
			
			if(regSerial.next()) {
				strSerial = regSerial.getString("dn_user").trim();
			} else {
				strSerial = "";
			}
		
			//Esta inserción deberia meterse en otro componente... se deja aqui por tener menor impacto en lo que esta a punto de liberarse :-)
			String sqlAcceso = " INSERT INTO bit_acceso (ic_usuario,ic_afiliado,cg_tipo_afiliado,cg_version) " +
					" VALUES(?,?,?,?) ";

			pstmt = con.queryPrecompilado(sqlAcceso);
			pstmt.setString(1,iNoUsuario);
			pstmt.setInt(2, Integer.parseInt(claveAfiliado));
			pstmt.setString(3, tipoAfiliado);
			pstmt.setInt(4, 2);	//2=Nueva version (extjs)
			pstmt.executeUpdate();
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			System.out.println("Error al insertar en bit_acceso... ignorando: " + t.getMessage());
		} finally {
			try {
				if (pstmt != null ) { 
					pstmt.close();
				}
				if(con.hayConexionAbierta()) {
					con.terminaTransaccion(bOk);
				}
			} catch(Throwable t1) {}
		}
	
		String strPerfil = (String) session.getAttribute("sesPerfil");
		String iNoCliente        = (String) session.getAttribute("iNoCliente");
		session.setAttribute("sesMenu", MenuUsuario.getMenuFIADO(strPerfil, iNoCliente, strSerial, appWebContextRoot));
		session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
		
		nav = new Navegacion(); 
		nav.setClaveAfiliado(iNoCliente);
		nav.setTipoAfiliado("F");
		nav.setPerfil(strPerfil);
		nav.setMenuUsuario((String)session.getAttribute("sesMenu"));	//Las opciones de menu que tenga habilitado el usuario podran afectar navegacion
		pantallasNavegacionComplementaria = nav.getPantallasComplementarias();
		session.setAttribute("inicializar.PantallasComplementarias", pantallasNavegacionComplementaria);

		strNombreEmpresa = AfiliadoInfo.getNombreAfiliado(Integer.parseInt(iNoCliente), "13"); //FIADO 

		 
		strNombre = strNombreEmpresa;
		if(strNombre==null || strNombre.equals("null"))strNombre="";
		session.setAttribute("strNombre",strNombre);
		session.setAttribute("strPais",strPais);
		session.setAttribute("Clave_usuario",gsCveUsuario);
		session.setAttribute("strSerial",strSerial);
	
			
	} else if(iNoClient != null) {
	
		/******  CLIENTE EXTERNO **************/
		
		String strNombreEmpresa = "", gsCveUsuario ="", qryNombreEmp = "", strPais = "MEXICO", strNombre="",	 strSerial ="";	
		
		System.out.println(" ENTRANDO USUARIO CLIENTE EXTENO." +iNoClient );
		gsCveUsuario = sCveUsuario;
		
		Navegacion nav = new Navegacion();
		nav.setClaveAfiliado(iNoClient);
		nav.setTipoAfiliado("C");
		nav.setPerfil(perfil);
		pantallasNavegacionInicial = nav.getPantallasComplementarias(); 
		
		strTipo = "C";
		session.setAttribute("strTipoUsuario", "CLIENTE");		
		session.setAttribute("iNoCliente"    , iNoClient);
		session.setAttribute("Clave_usuario",gsCveUsuario);
		session.setAttribute("strLogo", "if.gif");
		bOk = true;
			
		List pantallasNavegacionComplementaria = new ArrayList();
		
		try {
		
			PreparedStatement pstmt2 = null;
			//********* NOS TRAEMOS EL NUMERO DE SERIE DEL CERTIFICADO DE LA BD
			String qryTraeSerial = 
					" SELECT dn_user, '20secure/carga_variables.jsp' " +
					" FROM users_seguridata "+
					" WHERE table_uid like UPPER(?) ";
					
			List params = new ArrayList();			
			params.add(iNoUsuario + "%");
			Registros regSerial = con.consultarDB(qryTraeSerial, params);
			con.terminaTransaccion(true);	//Debido a que users_seguridata es remota
			
			if(regSerial.next()) {
				strSerial = regSerial.getString("dn_user").trim();
			} else {
				strSerial = "";
			}
		
			//Esta inserción deberia meterse en otro componente... se deja aqui por tener menor impacto en lo que esta a punto de liberarse :-)
			String sqlAcceso = " INSERT INTO bit_acceso (ic_usuario,ic_afiliado,cg_tipo_afiliado,cg_version) " +
					" VALUES(?,?,?,?) ";

			pstmt = con.queryPrecompilado(sqlAcceso);
			pstmt.setString(1,iNoUsuario);
			pstmt.setInt(2, Integer.parseInt(claveAfiliado));
			pstmt.setString(3, tipoAfiliado);
			pstmt.setInt(4, 2);	//2=Nueva version (extjs)
			pstmt.executeUpdate();
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			System.out.println("Error al insertar en bit_acceso... ignorando: " + t.getMessage());
		} finally {
			try {
				if (pstmt != null ) { 
					pstmt.close();
				}
				if(con.hayConexionAbierta()) {
					con.terminaTransaccion(bOk);
				}
			} catch(Throwable t1) {}
		}
	
		String strPerfil = (String) session.getAttribute("sesPerfil");
		String iNoCliente        = (String) session.getAttribute("iNoCliente");
		session.setAttribute("sesMenu", MenuUsuario.getMenuClienteExt(strPerfil, iNoCliente, strSerial, appWebContextRoot));
		session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
		
		nav = new Navegacion(); 
		nav.setClaveAfiliado(iNoCliente);
		nav.setTipoAfiliado("C");
		nav.setPerfil(strPerfil);
		nav.setMenuUsuario((String)session.getAttribute("sesMenu"));	//Las opciones de menu que tenga habilitado el usuario podran afectar navegacion
		pantallasNavegacionComplementaria = nav.getPantallasComplementarias();
		session.setAttribute("inicializar.PantallasComplementarias", pantallasNavegacionComplementaria);

		strNombreEmpresa = AfiliadoInfo.getNombreAfiliado(Integer.parseInt(iNoCliente), "10"); //CLIENTE 

		 
		strNombre = strNombreEmpresa;
		if(strNombre==null || strNombre.equals("null"))strNombre="";
		session.setAttribute("strNombre",strNombre);
		session.setAttribute("strPais",strPais);
		session.setAttribute("Clave_usuario",gsCveUsuario);
		session.setAttribute("strSerial",strSerial);
		
		 
	 } else if(iIFNB != null) {
	
		/********************************
		 *             IFNB CEDI
		********************************/
		String strNombreEmpresa = "", gsCveUsuario ="", qryNombreEmp = "", strPais = "MEXICO", strNombre="",
				 strSerial ="";	
		gsCveUsuario = sCveUsuario;
		
		Navegacion nav = new Navegacion();
		nav.setClaveAfiliado(iIFNB);
		nav.setTipoAfiliado("CE");
		nav.setPerfil(perfil);
		pantallasNavegacionInicial = nav.getPantallasComplementarias();
		strTipo = "IFNB CEDI";
		session.setAttribute("strTipoUsuario", "IFNB CEDI");
		session.setAttribute("iNoCliente"    , iIFNB);
		session.setAttribute("strLogo", "if.gif");
		bOk = true;
			
		List pantallasNavegacionComplementaria = new ArrayList();
		
		try {
		
			PreparedStatement pstmt2 = null;
			
		
			//Esta inserción deberia meterse en otro componente... se deja aqui por tener menor impacto en lo que esta a punto de liberarse :-)
			String sqlAcceso = " INSERT INTO bit_acceso (ic_usuario,ic_afiliado,cg_tipo_afiliado,cg_version) " +
					" VALUES(?,?,?,?) ";

			pstmt = con.queryPrecompilado(sqlAcceso);
			pstmt.setString(1,iNoUsuario);
			pstmt.setInt(2, 0);
			pstmt.setString(3, tipoAfiliado);
			pstmt.setInt(4, 2);	//2=Nueva version (extjs)
			pstmt.executeUpdate();
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			System.out.println("Error al insertar en bit_acceso... ignorando: " + t.getMessage());
		} finally {
			try {
				if (pstmt != null ) {
					pstmt.close();
				}
				if(con.hayConexionAbierta()) {
					con.terminaTransaccion(bOk);
				}
			} catch(Throwable t1) {}
		}
	
		String strPerfil = (String) session.getAttribute("sesPerfil");
		String iNoCliente        = (String) session.getAttribute("iNoCliente");
		session.setAttribute("sesMenu", MenuUsuario.getMenuIFNBCedi(strPerfil, iNoCliente, strSerial, appWebContextRoot));
		session.setAttribute("sesMenuDirecto", MenuUsuario.getMenuDirecto((String)session.getAttribute("sesMenu"), strPerfil));
		
		nav = new Navegacion();
		nav.setClaveAfiliado(iNoCliente);
		nav.setTipoAfiliado("CE");
		nav.setPerfil(strPerfil);
		nav.setMenuUsuario((String)session.getAttribute("sesMenu"));	//Las opciones de menu que tenga habilitado el usuario podran afectar navegacion
		pantallasNavegacionComplementaria = nav.getPantallasComplementarias();
		session.setAttribute("inicializar.PantallasComplementarias", pantallasNavegacionComplementaria);
		
		
		session.setAttribute("strNombre",usuario.getNombreOrganizacionUsuario());
		session.setAttribute("strPais",strPais);
		session.setAttribute("Clave_usuario",gsCveUsuario);
		session.setAttribute("strSerial",strSerial);
		session.setAttribute("sesObjUsuario",usuario);
		
	
	} else {
		/********************************
		*              NAFIN (4) ó BANCOMEXT (8)
		********************************/
		Navegacion nav = new Navegacion();
		nav.setTipoAfiliado("N");
		nav.setPerfil(perfil);
                System.out.print("perfil: "+perfil);
		pantallasNavegacionInicial = nav.getPantallasBasicas();


		//System.out.println("ENTRANDO USUARIO NAFIN.");
		session.setAttribute("strTipoUsuario", "NAFIN");
		session.setAttribute("sesiNoUsuario" , "4");
		session.setAttribute("iNoCliente"    , "0"); 
		session.setAttribute("strLogo"       , "nafin.gif");

		Seguridad objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", Seguridad.class);

		if (objSeguridad.getTipoAfiliadoXPerfil(perfil).equals("8")) { //Si es un perfil asociado a bancomext
			session.setAttribute("sesiNoUsuario" , "8"); //Se sobreescribe el 4 (Nafin) por 8 (Bancomext)
			//Aqui también deberian sobreescribirse los logos y el css, en lugar de en el carga_variables.jsp
			//pero por no disponer del tiempo para analizar los efectos y probar los ajustes, se deja tal como esta,
			//tratando de no afectar la funcionalidad actual.
		}
	}
	 
	 
	 	 	// PARA CUALQUIER USUARIO QUE NO SEA NAFIN 	NI EXEMPLEADO.
			
	if( !"NAFIN".equals(session.getAttribute("strTipoUsuario")) &&
			!"EXEMPLEADO".equals(session.getAttribute("strTipoUsuario"))&&!"IFNB CEDI".equals(session.getAttribute("strTipoUsuario")) ){
		qryTraeNumNafinElect = 
				" SELECT ic_nafin_electronico " +
				" FROM comrel_nafin " +
				" WHERE ic_epo_pyme_if = ? "+
				" AND cg_tipo = ? ";

		pstmt = con.queryPrecompilado(qryTraeNumNafinElect);
		pstmt.setLong(1,Long.parseLong(session.getAttribute("iNoCliente").toString()));
		pstmt.setString(2, strTipo);
		ResultSet rsTraeNafinElectronico = pstmt.executeQuery();
		int iNoNafinElectronico = 0;

		if(rsTraeNafinElectronico.next()) {
			iNoNafinElectronico = rsTraeNafinElectronico.getInt(1);
		}
		rsTraeNafinElectronico.close();
		pstmt.clearParameters();
		if(pstmt!=null) pstmt.close();

		session.setAttribute("iNoNafinElectronico", new Long(iNoNafinElectronico));

		if(iNoNafinElectronico == 0){
			throw new AppException("El afiliado no cuenta con un numero de Nafin Electronico");
		}
	}

	session.setAttribute("inicializar.PantallasBasicas", pantallasNavegacionInicial);

	response.sendRedirect(appWebContextRoot + pantallasNavegacionInicial.get(0));

} finally {
	if (con.hayConexionAbierta()) {
		con.cierraConexionDB();
	}
}
%>
