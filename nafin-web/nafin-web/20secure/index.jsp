<%@ page import="
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*
		" 
session = "false"
errorPage="/00utils/error.jsp"%>
<%
HttpSession httpSession = request.getSession(false);
if (httpSession != null) {
	httpSession.invalidate();// invalida la session
} else {
	request.getSession(true);
}

// Set the Expires and Cache Control Headers
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setHeader("Expires", "Thu, 29 Oct 1969 17:04:19 GMT");

// Set request and response type
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");


String mecanismoLogin = ParametrosGlobalesApp.getInstance().getParametro("MECANISMO_LOGIN");
String loginUsuario = null;
if ("DUMMY".equals(mecanismoLogin)) {
	loginUsuario = request.getParameter("ic_usuario");
} else {
%>
	<%@ include file="/00utils/sso/ssoinclude.jsp" %>	

<%
	//usrInfo contiene el siguiente formato: 00000012/Nombre Compañia
	//El primer elemento es el login del usuario.
	
	loginUsuario = (String) (Comunes.explode("/", usrInfo)).get(0);
	
	if (application.getAttribute("urlLogout") == null) {
		application.setAttribute("urlLogout", logoutURL+"?p_done_url=" + requestURL);
	}

}

%>
	<jsp:forward page="indexA.jsp"/>
