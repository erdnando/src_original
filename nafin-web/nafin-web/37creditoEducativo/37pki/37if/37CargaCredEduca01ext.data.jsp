<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		java.io.OutputStream,
		java.io.PrintStream,
		java.io.File,
		java.io.BufferedInputStream, 
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.educativo.*,
		com.netro.model.catalogos.CatalogoIF_Universidad"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/37creditoEducativo/37secsession_extjs.jspf" %>
<%@ include file="/37creditoEducativo/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CargaArchivo.inicializacion") )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;

	// 1. Obtener instancia de EJB de Seguridad
	com.netro.seguridadbean.Seguridad seguridad  = null;
	try {
		
		seguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);

	}catch(Exception e){

		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");

	}

	// 2. Validar Numero Fiso
	if ( !hayAviso && !seguridad.validaNumeroFISO(claveIF) ) {
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene definido un Fideicomiso para realizar sus operaciones.<br/><br/>Por favor comuníquese al Centro de Atención a clientes al teléfono 50-89-61-07 <br/>o del Interior al 01-800-NAFINSA (01-800-6234672).";
		hayAviso				= true;
	}

	// 3. Remover variables de sesion
	if ( !hayAviso ){
		session.removeAttribute("CLAVES_FINAN");
		session.removeAttribute("rgprint");
	}

	// Obtener instancia del EJB de CreditoEducativo
	CreditoEducativo creditoEducativo = null;
	try {
		
		creditoEducativo = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);
			
	} catch(Exception e) {

		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de CreditoEducativo");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de CreditoEducativo.");

	}

	if(!hayAviso){

		//String longitudClaveFinanciamiento = creditoEducativo.operaProgramaSHFCreditoHipotecario(claveIF)?"30":"20";
		String saldosLayout = 
		"<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" aling =\"center\" style=\"margin-left:30px;width:600px;\"> "  +
      "			<tr> "  +
      "				<td class=\"celda01\" align=\"center\" colspan=\"4\" style=\"font-weight:bold;\">Layout Alta de Garant&iacute;as</td> "  +
      "			</tr> "  +
      "			<tr> "  +
      "				<td class=\"celda01\" align=\"center\" width=\"25px\" height=\"45px\">No.</td> "  +
      "				<td class=\"celda01\" align=\"center\">Descripci&oacute;n</td> "  +
      "				<td class=\"celda01\" align=\"center\">Tipo</td> "  +
      "				<td class=\"celda01\" align=\"center\">Longitud<br>M&aacute;xima</td> "  +
      "			</tr> "  +
      "			<tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">1</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Nombre o Raz&oacute;n Social del Acreditado "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">80</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">2</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Registro Federal de Causante del Acreditado "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">14</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">3</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Calle N&uacute;mero de Acreditado "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">80</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">4</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Colonia del Acreditado "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">80</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">5</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              C&oacute;digo Postal "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">5</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">6</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Ciudad "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">75</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">7</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Clave del Municipio o Delegaci&oacute;n del Acreditado "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">8</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Clave del Estado o Entidad Federativa del Acreditado "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">2</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">9</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Giro de la Actividad a la que se dedica el Acreditado "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">200</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">10</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Clave del Financiamiento "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">20</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">11</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Sector de Actividad Econ&oacute;mica "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">2</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">12</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Solicita Fondeo de NAFIN "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">1</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">13</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Solicita la Participaci&oacute;n en el Riesgo de NAFIN "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">1</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">14</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Porcentaje de Participaci&oacute;n en el Registro Solicitado "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">6</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">15</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Personal Ocupado (n&uacute;mero) "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">16</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Promedio de Ventas Anual (en Pesos) "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">14</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">17</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Prop&oacute;sito del Proyecto "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">2</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">18</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Clave del Tipo de  Financiamiento "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">2</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\" rowspan=\"2\" valign=\"top\">19</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              19.a Clave de la Garant&iacute;a "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">2</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              19.b Monto de la Garant&iacute;a "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">12,2</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\" rowspan=\"4\" valign=\"top\">20</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              20.a Clave de concepto de recurso "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">2</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              20.b Porcentaje Parcial "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              20.c Porcentaje Nacional "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              20.d Porcentaje Importaci&oacute;n "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">21</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Porcentaje de la Producci&oacute;n destinada al Mercado Interno "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">22</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Porcentaje de la Producci&oacute;n  Destinada al Mercado Externo "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">23</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Forma de Amortizaci&oacute;n "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">1</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">24</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Monto del Financiamiento "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">11</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">25</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Fecha de Disposici&oacute;n y de Participaci&oacute;n en el Riesgo "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Fecha</td> "  +
      "           <td class=\"formas\" align=\"center\">8</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">26</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Importe de Disposici&oacute;n "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">11</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">27</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Moneda "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">7</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">28</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              N&uacute;mero de Meses de Plazo "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">29</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              N&uacute;mero de Meses de la Gracia del Financiamiento "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">30</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Sobretasa "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">6</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">31</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Clave del Funcionario Facultado del Intermediario Financiero "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">2</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">32</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Clave del Intermediario Financiero. "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">5</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">33</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Tipo de Autorizaci&oacute;n "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">1</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">34</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Tipo Programa "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">5</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">35</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Tipo Tasa "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">1</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">36</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Plazo en N&uacute;mero de D&iacute;as "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">37</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Calificaci&oacute;n Inicial "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">2</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">38</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Antig&uuml;edad como Cliente en Meses "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">39</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Nombre del Contacto "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">80</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">40</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Tel&eacute;fono con Clave Lada "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">15</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">41</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Correo Electr&oacute;nico "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">60</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">42</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Dependencia "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">4</td> "  +
      "        </tr> "  +
		"        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">43</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Tipo de Persona "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">1</td> "  +
      "        </tr> "  +
		"        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">44</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Género del Acreditado "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">1</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">45</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Id Matricula "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">20</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">46</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Descripción Estudios "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">200</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">47</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Fecha Ingreso "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">8</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">48</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Año colocaci&oacute;n "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">4</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">49</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Id periodo educativo "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">10</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">50</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Modalidad programa "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">200</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">51</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Fecha &uacute;ltima disposici&oacute;n "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Texto</td> "  +
      "           <td class=\"formas\" align=\"center\">3</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">52</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              N&uacute;mero disposici&oacute;n "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">200</td> "  +
      "        </tr> "  +
      "        <tr> "  +
      "           <td class=\"formas\" align=\"center\" height=\"45px\">53</td> "  +
      "           <td class=\"formas\" align=\"left\"> "  +
      "              Saldo en l&iacute;neas "  +
      "           </td> "  +
      "           <td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
      "           <td class=\"formas\" align=\"center\">22,2</td> "  +
      "        </tr> "  +
      "		</table> ";
		resultado.put("saldosLayout", 	saldosLayout			);

		estadoSiguiente 	= "ESPERAR_CAPTURA_SOLICITUD_DE_CARGA";

		Registros reg = creditoEducativo.getBasesOperaIf(iNoCliente);

		resultado.put("basesOpera", reg.getJSONData());

	}

	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (          informacion.equals("CatalogoUniv") )	{

	CatalogoIF_Universidad cat = new CatalogoIF_Universidad();
	cat.setCampoClave("u.ic_universidad");
	cat.setCampoDescripcion("u.cg_razon_social ||' - '|| u.cg_nombre_campus");
	cat.setIntermediario(iNoCliente);
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

} else if (          informacion.equals("CargaArchivo.enviarSolicitudDeCarga") )	{

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;

	String claveUniversidad			= (request.getParameter("claveUniversidad")			== null)?"":request.getParameter("claveUniversidad");
	String numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String sumatoriaSaldosFinMes 	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?"":request.getParameter("sumatoriaSaldosFinMes");

	resultado.put("claveUniversidad",		claveUniversidad			);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("sumatoriaSaldosFinMes",	sumatoriaSaldosFinMes	);

	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	"ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES" 		);
	infoRegresar = resultado.toString();

} else if (        	informacion.equals("CargaArchivo.subirArchivo") 				)	{

	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);

	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;

	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {

		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(8097152);
		myUpload.upload();

	} catch(Exception e) {

		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 8097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}

	}

	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	String claveUniversidad			= (myRequest.getParameter("claveUniversidad1")			== null)?"":myRequest.getParameter("claveUniversidad1");
	String numeroRegistros 			= (myRequest.getParameter("numeroRegistros1")			== null)?"":myRequest.getParameter("numeroRegistros1");
	String sumatoriaSaldosFinMes	= (myRequest.getParameter("sumatoriaSaldosFinMes1")	== null)?"":myRequest.getParameter("sumatoriaSaldosFinMes1");

	// Guardar Archivo en Disco
	int numFiles = 0;
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," ") );
		resultado.put("fileName", 	myFile.getFileName().replaceAll("\\s+"," ") );

	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	
	// Enviar parametros adicionales
	resultado.put("claveUniversidad",		claveUniversidad			);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("sumatoriaSaldosFinMes",	sumatoriaSaldosFinMes	);
 
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 		"REALIZAR_VALIDACION" 	);
	// Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacion") 				)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String		claveUniversidad			= (request.getParameter("claveUniversidad")			== null)?"":request.getParameter("claveUniversidad");
	String 		numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldosFinMes 	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?"":request.getParameter("sumatoriaSaldosFinMes");
	String 		fileName 					= (request.getParameter("fileName")						== null)?"":request.getParameter("fileName");

	String 		rutaArchivo 				= strDirectorioTemp + iNoUsuario + "." + fileName	;
	String 		claveIF 						= iNoCliente;
	
	// Obtener instancia del EJB de CreditoEducativo
	CreditoEducativo creditoEducativo = null;
	try {
		
		creditoEducativo = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);
			
	} catch(Exception e) {

		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de CreditoEducativo");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de CreditoEducativo.");

	}

	// Realizar validacion
	ResultadosGarEducativo rg = creditoEducativo.procesarAltaGarantiaEducativo(
									strDirectorioTemp,
									rutaArchivo,
									numeroRegistros,
									sumatoriaSaldosFinMes,
									claveIF
								);
	
	// Guardar en sesión las claves de financiamiento
	List		  clavesFinanciamiento = rg.getClavesFinan();
	session.setAttribute("CLAVES_FINAN",clavesFinanciamiento);
 
	// Leer Resultado de la Validacion
	String claveProceso 		= String.valueOf(rg.getIcProceso());
	String totalRegistros	= String.valueOf(rg.getNumRegOk());
	String montoTotal 		= String.valueOf(rg.getSumRegOk());
	String montoTotalPagar 	= String.valueOf(rg.getSumTotPag());

	//  Leer Resultado de la Validacion ( Registros sin Errores )
	String 		 registrosSinErrores 				  = rg.getCorrectos();
	
		StringBuffer 	buffer 	= new StringBuffer();
		JSONArray		registrosSinErroresDataArray = new JSONArray();
		int				ctaRegistros = 0;
		
		for(int i=0;i<registrosSinErrores.length();i++){
			char lastChar = registrosSinErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosSinErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosSinErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		// Si el primer registro dice: Claves de Financiamiento, borrarlo
		if( 	registrosSinErroresDataArray.size() > 0 ){
				
			JSONArray registro = (JSONArray) registrosSinErroresDataArray.get(0);
			if("Claves de Financiamiento:".equals( registro.getString(1) )){
				registrosSinErroresDataArray.remove(0);
			}
			
		}
 
	String numeroRegistrosSinErrores 		= String.valueOf(rg.getNumRegOk());
	String sumatoriaSaldosFinMesSinErrores	= "$ "+Comunes.formatoDecimal(rg.getSumRegOk(),2);
 
	//  Leer Resultado de la Validacion ( Registros con Errores )
	String registrosConErrores 				= rg.getErrores();

		buffer 			= new StringBuffer();
		JSONArray		registrosConErroresDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<registrosConErrores.length();i++){
			char lastChar = registrosConErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosConErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosConErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		
	String numeroRegistrosConErrores 		= String.valueOf(rg.getNumRegErr());
	String sumatoriaSaldosFinMesConErrores	= "$ " + Comunes.formatoDecimal(rg.getSumRegErr(),2); 
 
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	String erroresVsCifrasControl = rg.getCifras();
	
		buffer 			= new StringBuffer();
		JSONArray		erroresVsCifrasControlDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<erroresVsCifrasControl.length();i++){
			char lastChar = erroresVsCifrasControl.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				erroresVsCifrasControlDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			erroresVsCifrasControlDataArray.add(registro);
			buffer.setLength(0);
		}

	// Determinar si se mostrará el boton continuar carga	
	Boolean continuarCarga 								= ("".equals(rg.getErrores())&&"".equals(rg.getCifras()))
																					?new Boolean(true):new Boolean(false);

	// Enviar parametros adicionales
	resultado.put("claveUniversidad",									claveUniversidad									);
	resultado.put("numeroRegistros",										numeroRegistros									);
	resultado.put("sumatoriaSaldosFinMes",								sumatoriaSaldosFinMes							);

	// Enviar resultado general de la validacion
	resultado.put("claveProceso",											claveProceso										);
	resultado.put("totalRegistros", 										totalRegistros										);
	resultado.put("montoTotal",											montoTotal											);
	resultado.put("montoTotalPagar",										montoTotalPagar									);
	
	resultado.put("registrosSinErroresDataArray",					registrosSinErroresDataArray					);
	resultado.put("numeroRegistrosSinErrores",						numeroRegistrosSinErrores						);
	resultado.put("sumatoriaSaldosFinMesSinErrores",				sumatoriaSaldosFinMesSinErrores				);
	
	resultado.put("cadenaRegistrosConErrores",						registrosConErrores								);
	resultado.put("registrosConErroresDataArray",					registrosConErroresDataArray					);
	resultado.put("numeroRegistrosConErrores",						numeroRegistrosConErrores						);
	resultado.put("sumatoriaSaldosFinMesConErrores",				sumatoriaSaldosFinMesConErrores				);
	
	resultado.put("erroresVsCifrasControlDataArray",				erroresVsCifrasControlDataArray				);
	resultado.put("continuarCarga",										continuarCarga										);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);

	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.descargarErrores") 			)	{

	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String		registrosConErrores	=	(request.getParameter("registrosConErrores")== null)?"":request.getParameter("registrosConErrores");

	CreaArchivo archivos = new CreaArchivo();
	String nombreArchivo = null;

	if(!archivos.make(registrosConErrores, strDirectorioTemp, ".csv")) {
		success = false;
		resultado.put("msg", "Error al generar el archivo Errores ");
	} else {
		nombreArchivo = archivos.nombre;
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}

	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.presentarPreacuse") 			)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		claveProceso 							= (request.getParameter("claveProceso")					== null)?"":request.getParameter("claveProceso");
	String 		totalRegistros 						= (request.getParameter("totalRegistros")					== null)?"":request.getParameter("totalRegistros");
	String 		montoTotal 								= (request.getParameter("montoTotal")						== null)?"":request.getParameter("montoTotal");
	String 		montoTotalPagar 						= (request.getParameter("montoTotalPagar")				== null)?"":request.getParameter("montoTotalPagar");
	

	String 		claveUniversidad						= (request.getParameter("claveUniversidad")				== null)?"":request.getParameter("claveUniversidad");
	String 		nombreUniversidad						= (request.getParameter("nombreUniversidad")				== null)?"":request.getParameter("nombreUniversidad");
	String 		numeroRegistros 						= (request.getParameter("numeroRegistros")				== null)?"":request.getParameter("numeroRegistros");
	String 		sumatoriaSaldosFinMes 				= (request.getParameter("sumatoriaSaldosFinMes")		== null)?"":request.getParameter("sumatoriaSaldosFinMes");

	// Construir Array con los datos del preacuse
	JSONArray 	registrosPorAgregarDataArray 		= new JSONArray();
	JSONArray	registro									= null;

	// Determinar el mes de carga
	String 	meses[]		={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

	registro = new JSONArray();
	registro.add("Tipo de Operación");
	registro.add("Crédito Educativo");
	registrosPorAgregarDataArray.add(registro);
/*
	registro = new JSONArray();
	registro.add("Universidad - Campus");
	registro.add( nombreUniversidad );
	registrosPorAgregarDataArray.add(registro);
*/
	registro = new JSONArray();
	registro.add("No. total de registros transmitidos");
	registro.add(totalRegistros);
	registrosPorAgregarDataArray.add(registro);
	
	registro = new JSONArray();
	registro.add("Monto total de los registros transmitidos");
	registro.add(Comunes.formatoDecimal(montoTotal,2));
	registrosPorAgregarDataArray.add(registro);
	
	// Hacer eco de los parametros de la carga
	resultado.put("claveProceso",								claveProceso);
	resultado.put("totalRegistros",							totalRegistros);
	resultado.put("montoTotal",								montoTotal);
	resultado.put("montoTotalPagar",							montoTotalPagar);

	resultado.put("claveUniversidad",						claveUniversidad);
	resultado.put("nombreUniversidad",						nombreUniversidad);
	resultado.put("numeroRegistros",							numeroRegistros);
	resultado.put("sumatoriaSaldosFinMes",					sumatoriaSaldosFinMes);

	// Enviar descripcion de los registros por agregar
	resultado.put("registrosPorAgregarDataArray", 		registrosPorAgregarDataArray);
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_PREACUSE" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.transmitirRegistros")		)  {

	JSONObject		resultado							= new JSONObject();
	String 			estadoSiguiente					= null;
	boolean			success								= true;
	boolean			hayError								= false;
	ResultadosGarEducativo 	rg 						= null;
	String 			msg 									= "";
	String 			acuseFirmaDigital 				= "";
	String 			loginUsuario						= iNoUsuario;

	// Leer campos del preacuse
	String claveProceso 				= (request.getParameter("claveProceso")				== null)?"0"	:request.getParameter("claveProceso");
	String totalRegistros 			= (request.getParameter("totalRegistros")				== null)?"0"	:request.getParameter("totalRegistros");
	String montoTotal 				= (request.getParameter("montoTotal")					== null)?"0.00":request.getParameter("montoTotal");
	String montoTotalPagar 			= (request.getParameter("montoTotalPagar")			== null)?"0.00":request.getParameter("montoTotalPagar");

	String claveUniversidad			= (request.getParameter("claveUniversidad")			== null)?""		:request.getParameter("claveUniversidad");
	String nombreUniversidad		= (request.getParameter("nombreUniversidad")			== null)?"":	request.getParameter("nombreUniversidad");
	String numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?""		:request.getParameter("numeroRegistros");
	String sumatoriaSaldosFinMes	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?""		:request.getParameter("sumatoriaSaldosFinMes");

	// Leer campos de la firma digital
	String isEmptyPkcs7 				= (request.getParameter("isEmptyPkcs7")	 			== null)?"false"	:request.getParameter("isEmptyPkcs7");
	String textoFirmado				= (request.getParameter("textoFirmado")	 			== null)?""			:request.getParameter("textoFirmado");
	String pkcs7						= (request.getParameter("pkcs7")			 	 			== null)?""			:request.getParameter("pkcs7");

	if( session.getAttribute("rgprint")!=null ){
		msg 					= "Para realizar otra operacion por favor seleccione la opcion Cargar Archivo en el menu Credito Educativo";
		estadoSiguiente 	= "ESPERAR_DECISION";
		hayError				= true;
	} else {
		session.setAttribute("rgprint", new ResultadosGarEducativo() ); //Se establece la variable de sesión para controlar envíos duplicados
	} 
	
	if( !hayError ){
		
		// Obtener instancia del EJB de CreditoEducativo
		CreditoEducativo creditoEducativo = null;
	
		try {
					
			creditoEducativo = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);

		} catch(Exception e) {

			log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de CreditoEducativo");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de CreditoEducativo.");
	
		}
		
		// Se declaran variables adicionales
		String 									folioCert 		= "";
		String 									externContent 	= textoFirmado;
		char 										getReceipt 		= 'Y';
		netropology.utilerias.Seguridad 	s 					= null;
		String 									claveIF 			= iNoCliente;
 
		/* Nota: Para otros navegadores diferentes de IE y Mozilla
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		*/
			
		// Autenticar certificado
		if ( !_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")){
			
			folioCert 	= "02SPC"+claveIF+new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			s 				= new netropology.utilerias.Seguridad();
			
			List clavesFinanciamiento = (List)session.getAttribute("CLAVES_FINAN");
			if ( true){	
			//if ( s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
				rg = creditoEducativo.procesarAltaCreditoEducativo(
							claveProceso,
							claveIF,
							totalRegistros,
							montoTotal,
							claveUniversidad,
							loginUsuario,
							strDirectorioTemp
						);

				estadoSiguiente 	= "MOSTRAR_ACUSE_TRANSMISION_REGISTROS";
			} else{
				msg 					= "La autentificacion no se llevo a cabo: "+s.mostrarError();
				estadoSiguiente 	= "ESPERAR_DECISION";
				hayError				= true;
			}
			
		} else{
			
			try {
				throw new NafinException("GRAL0021");
			}catch(Exception e){
				estadoSiguiente 	= "ESPERAR_DECISION";
				msg					= e.getMessage();
				hayError				= true;
			}
		}
	}
	
	// Poner en sesion el resultado de la transmicion de solicitudes
	if( "MOSTRAR_ACUSE_TRANSMISION_REGISTROS".equals(estadoSiguiente) ){
		
		session.setAttribute("rgprint",	rg);
		resultado.put("folioSolicitud", 	"Su solicitud fue recibida con el número de folio: " + rg.getFolio() );
		resultado.put("folioCarga",	rg.getFolio() );
	
		// Determinar el mes de carga
		String 	meses[]		={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

		// Construir Array con los datos del acuse
		JSONArray 	registrosAgregadosDataArray 	= new JSONArray();
		JSONArray	registro								= null;
		
		registro = new JSONArray();
		registro.add("Tipo de Operación");
		registro.add("Crédito Educativo");
		registrosAgregadosDataArray.add(registro);
/*
		registro = new JSONArray();
		registro.add("Universidad - Campus");
		registro.add( nombreUniversidad );
		registrosAgregadosDataArray.add(registro);
*/
		registro = new JSONArray();
		registro.add("No. total de registros transmitidos");
		registro.add(totalRegistros);
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Monto total de los registros transmitidos");
		registro.add(Comunes.formatoDecimal(montoTotal,2));
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Fecha de carga");
		registro.add(rg.getFecha());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Hora de carga");
		registro.add(rg.getHora());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Usuario");
		registro.add(iNoUsuario+" - "+strNombreUsuario);
		registrosAgregadosDataArray.add(registro);
 
		resultado.put("registrosAgregadosDataArray",	registrosAgregadosDataArray);
		
		// Hacer eco de los parametros de la carga
		resultado.put("claveProceso",						claveProceso				);
		resultado.put("totalRegistros",					totalRegistros				);
		resultado.put("montoTotal",						montoTotal					);
		resultado.put("montoTotalPagar",					montoTotalPagar			);
		
		resultado.put("claveUniversidad",				claveUniversidad			);
		resultado.put("nombreUniversidad",				nombreUniversidad			);
		resultado.put("numeroRegistros",					numeroRegistros			);
		resultado.put("sumatoriaSaldosFinMes",			sumatoriaSaldosFinMes	);

	}
	
	resultado.put("msg",								msg									);	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
 
} else if (    informacion.equals("CargaArchivo.cancelarRegistros") 				)	{
	CreditoEducativo creditoEducativo = null;

	try {
	
		creditoEducativo = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);

	} catch(Exception e) {

		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de CreditoEducativo");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de CreditoEducativo.");

	}

	JSONObject		resultado							= new JSONObject();
	String 			estadoSiguiente					= "FIN_PREACUSE";
	boolean			success								= true;
	String			msg									= "";
	String 			claveProceso		= (request.getParameter("claveProceso")== null)?"0"	:request.getParameter("claveProceso");
	try {
		boolean ok = creditoEducativo.eliminaTemporalesCarga(claveProceso);
	}catch(Exception e){
		success = false;
		estadoSiguiente 	= "";
		msg					= e.getMessage();
	}

	resultado.put("estadoSiguiente", 			estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.descargarCorrectos") 			)	{
	CreditoEducativo creditoEducativo = null;

	try {	
		
		creditoEducativo = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);

	} catch(Exception e) {

		log.error("CargaArchivo.inicializacion(Exception): Obtener instancia del EJB de CreditoEducativo");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de CreditoEducativo.");

	}

	JSONObject		resultado			= new JSONObject();
	boolean			success				= true;
	String 			msg 					= "";
	StringBuffer	contenidoArchivo	= new StringBuffer(2000);
	String 			folioCarga			= (request.getParameter("folioCarga")== null)?"0"	:request.getParameter("folioCarga");

	String nombre="",ombre="",rfc="",calle="",col="",cp="",ciudad="",claveMun="",entidad="",giro="",cve_fin="",sector="",
			fondeo="",riesgo="",porParticipa="",perOcupa="",promVentas="",proposito="",tipoFin="",claveMontoFin="",recurso="",
			merInter="",merExter="",forma="",montoFin="",fechaDispo="",montoDispo="",moneda="",mesesPlazo="",mesesGracia="",
			sobretasa="",claveFacultado="",claveIfSiag="",tipoAmor="",tipoProgram="",tipoTasa="",Plazo="",calif_Inicial="",
			antigueda="",nombreContac="",telefono="",email="",dependencia="",
			id_matricula="",desc_estudios="",fechaIngreso="",anio="",id_periodo="",modalidad="",fechaUltimaDispo="",
			noDispo="",saldoLineas="",fechaCarga="";

	try{
		Registros reg = new Registros();
		CreaArchivo archivos = new CreaArchivo();
		String nombreArchivo = null;

		reg = creditoEducativo.getDatosCarga(folioCarga);

		/*******************
		Cabecera del archivo
		********************/
		contenidoArchivo.append(
			"Número de folio carga: "+folioCarga+"\n\n"+
			"\nNombre del Acreditado,Registro Federal de Causante,Calle Número de Acreditado,Colonia,CP,Ciudad,Clave Municipio o Delegación,"+
			"Clave Estado/Entidad Federativa,Giro de la Actividad del Acreditado,Clave Financiamiento,Sector de Actividad Económica,Solicita Fondeo de NAFIN,"+
			"Solicita la Participación en el Riesgo de NAFIN,Porcentaje de Participación en el Registro Solicitado,Personal Ocupado (número),"+
			"Promedio Ventas Anual (en Pesos),Propósito del Proyecto,Clave Tipo de Financiamiento,Clave-Monto de la Garantía,Recurso,Porcentaje Producción Mercado Interno,"+
			"Porcentaje Producción Mercado Externo,Forma de Amortización,Monto del Financiamiento,Fecha Disposición,Importe Disposición,Moneda,"+
			"Número de Meses de Plazo,No. Meses de Gracia del Financiamiento,Sobretasa,Clave Funcionario Facultado del IF,"+
			"Clave IF,Tipo de Autorización,Tipo Programa,Tipo Tasa,Plazo en Días,Calificación Inicial,Antigüedad como Cliente en Meses,"+
			"Nombre Contacto,Teléfono,Correo Electrónico,Dependencia,"+
			"Tipo Persona , Género del Acreditado,  "+
			"Id Matricula,Descripción Estudios,Fecha Ingreso,Año colocación,Id periodo Educativo,Modalidad Programa,Fecha última Disposición,"+
			"Número Disposición,Saldo en Líneas,Fecha carga"
		);
		int numreg = 0;
		while(reg.next()){
			nombre			=(reg.getString("CG_RAZON_SOCIAL")==null)?"":reg.getString("CG_RAZON_SOCIAL").replace(',',' ');
			rfc				=(reg.getString("CG_REGISTRO")==null)?"":reg.getString("CG_REGISTRO").replace(',',' ');
			calle				=(reg.getString("CG_CALLE")==null)?"":reg.getString("CG_CALLE").replace(',',' ');
			col				=(reg.getString("CG_COLONIA")==null)?"":reg.getString("CG_COLONIA").replace(',',' ');
			cp					=(reg.getString("CG_CODIGOPOSTAL")==null)?"":reg.getString("CG_CODIGOPOSTAL").replace(',',' ');
			ciudad			=(reg.getString("CG_CIUDAD")==null)?"":reg.getString("CG_CIUDAD").replace(',',' ');
			claveMun			=(reg.getString("IC_MUNICIPIO")==null)?"":reg.getString("IC_MUNICIPIO");
			entidad			=(reg.getString("IC_ESTADO")==null)?"":reg.getString("IC_ESTADO").replace(',',' ');
			giro				=(reg.getString("CG_GIRO")==null)?"":reg.getString("CG_GIRO").replace(',',' ');
			cve_fin			=(reg.getString("CG_FINANCIAMIENTO")==null)?"":reg.getString("CG_FINANCIAMIENTO");
			sector			=(reg.getString("CG_SECTOR")==null)?"":reg.getString("CG_SECTOR").replace(',',' ');
			fondeo			=(reg.getString("CG_FONDEO")==null)?"":reg.getString("CG_FONDEO").replace(',',' ');
			riesgo			=(reg.getString("CG_RIESGO")==null)?"":reg.getString("CG_RIESGO").replace(',',' ');
			porParticipa	=(reg.getString("CG_PORCENTAJE_REG")==null)?"":reg.getString("CG_PORCENTAJE_REG").replace(',',' ');
			perOcupa			=(reg.getString("IC_PERSONAL")==null)?"":reg.getString("IC_PERSONAL");
			promVentas		=(reg.getString("FN_VENTAS_ANUAL")==null)?"":reg.getString("FN_VENTAS_ANUAL");
			proposito		=(reg.getString("CG_PROPOSITO")==null)?"":reg.getString("CG_PROPOSITO").replace(',',' ');
			tipoFin			=(reg.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":reg.getString("IC_TIPO_FINANCIAMIENTO");
			claveMontoFin	=(reg.getString("CC_CLAVE_MONTO_GARANTIA")==null)?"":reg.getString("CC_CLAVE_MONTO_GARANTIA").replace(',',' ');
			recurso			=(reg.getString("CC_RECURSOS")==null)?"":reg.getString("CC_RECURSOS").replace(',',' ');
			merInter			=(reg.getString("FN_PORC_MER_INTERNO")==null)?"":reg.getString("FN_PORC_MER_INTERNO");
			merExter			=(reg.getString("FN_PORC_MER_EXTERNO")==null)?"":reg.getString("FN_PORC_MER_EXTERNO");
			forma				=(reg.getString("IC_AMORTIZACION")==null)?"":reg.getString("IC_AMORTIZACION");
			montoFin			=(reg.getString("FN_FINANCIAMIENTO")==null)?"":reg.getString("FN_FINANCIAMIENTO");
			fechaDispo		=(reg.getString("DF_DISPOSICION_RIES")==null)?"":reg.getString("DF_DISPOSICION_RIES");
			montoDispo		=(reg.getString("FN_IMP_DISPOSICION")==null)?"":reg.getString("FN_IMP_DISPOSICION");
			moneda			=(reg.getString("CG_MONEDA")==null)?"":reg.getString("CG_MONEDA").replace(',',' ');
			mesesPlazo		=(reg.getString("CG_MESES_PLAZO")==null)?"":reg.getString("CG_MESES_PLAZO").replace(',',' ');
			mesesGracia		=(reg.getString("CG_MESES_FINANCIAMIENTO")==null)?"":reg.getString("CG_MESES_FINANCIAMIENTO").replace(',',' ');
			sobretasa		=(reg.getString("CG_SOBRETASA")==null)?"":reg.getString("CG_SOBRETASA").replace(',',' ');
			claveFacultado	=(reg.getString("IC_FUNCIONARIO_IF")==null)?"":reg.getString("IC_FUNCIONARIO_IF");
			claveIfSiag		=(reg.getString("IC_IF_SIAG")==null)?"":reg.getString("IC_IF_SIAG");
			tipoAmor			=(reg.getString("IC_AUTORIZACION")==null)?"":reg.getString("IC_AUTORIZACION");
			tipoProgram		=(reg.getString("IC_PROGRAMA")==null)?"":reg.getString("IC_PROGRAMA").replace(',',' ');
			tipoTasa			=(reg.getString("IC_TASA")==null)?"":reg.getString("IC_TASA");
			Plazo				=(reg.getString("CG_PLAZO_DIAS")==null)?"":reg.getString("CG_PLAZO_DIAS").replace(',',' ');
			calif_Inicial	=(reg.getString("CG_CALIFICACION")==null)?"":reg.getString("CG_CALIFICACION").replace(',',' ');
			antigueda		=(reg.getString("CG_ANTI_CLI")==null)?"":reg.getString("CG_ANTI_CLI").replace(',',' ');
			nombreContac	=(reg.getString("CG_NOMCONTRATO")==null)?"":reg.getString("CG_NOMCONTRATO").replace(',',' ');
			telefono			=(reg.getString("CG_TELEFONO")==null)?"":reg.getString("CG_TELEFONO").replace(',',' ');
			email				=(reg.getString("CG_CORREO")==null)?"":reg.getString("CG_CORREO");
			dependencia		=(reg.getString("CG_DEPENDENCIA")==null)?"":reg.getString("CG_DEPENDENCIA").replace(',',' ');
			id_matricula	=(reg.getString("CG_MATRICULA")==null)?"":reg.getString("CG_MATRICULA");
			desc_estudios	=(reg.getString("CG_DESC_ESTUDIOS")==null)?"":reg.getString("CG_DESC_ESTUDIOS").replace(',',' ');
			fechaIngreso	=(reg.getString("DF_INGRESO")==null)?"":reg.getString("DF_INGRESO");
			anio				=(reg.getString("CG_ANIO")==null)?"":reg.getString("CG_ANIO");
			id_periodo		=(reg.getString("IC_PERIODO")==null)?"":reg.getString("IC_PERIODO");
			modalidad		=(reg.getString("CG_MODALIDAD")==null)?"":reg.getString("CG_MODALIDAD").replace(',',' ');
		  fechaUltimaDispo=(reg.getString("DF_DISPOSICION")==null)?"":reg.getString("DF_DISPOSICION");
			noDispo			=(reg.getString("IC_NUM_DISPOSICION")==null)?"":reg.getString("IC_NUM_DISPOSICION");
			saldoLineas		=(reg.getString("FN_SALDOLINEAS")==null)?"":reg.getString("FN_SALDOLINEAS");
			fechaCarga		=(reg.getString("FN_FECHA_CARGA")==null)?"":reg.getString("FN_FECHA_CARGA");
			String tipoPersona		=(reg.getString("CG_TIPOPERSONA")==null)?"":reg.getString("CG_TIPOPERSONA");
			String sexoAcreditado		=(reg.getString("CG_SEXOACREDITADO")==null)?"":reg.getString("CG_SEXOACREDITADO");
			
			
			contenidoArchivo.append("\n"+ nombre + "," + rfc + "," + calle + "," + col + "," + cp + "," + ciudad + "," + claveMun +
				"," + entidad + "," + giro + "," + cve_fin + "," + sector + "," + fondeo + "," + riesgo + "," + porParticipa + "," + perOcupa +
				"," + promVentas + "," + proposito + "," + tipoFin + "," + claveMontoFin + "," + recurso + "," + merInter + "," + merExter +
				"," + forma + "," + montoFin + "," + fechaDispo + "," + montoDispo + "," + moneda + "," + mesesPlazo + "," + mesesGracia +
				"," + sobretasa + "," + claveFacultado + "," + claveIfSiag + "," + tipoAmor + "," + tipoProgram + "," + tipoTasa +
				"," + Plazo + "," + 	calif_Inicial + "," + antigueda + "," + nombreContac + "," + telefono + "," + email +
				"," + dependencia + "," + tipoPersona +", "+sexoAcreditado+","+id_matricula +
				"," + desc_estudios + "," + fechaIngreso + "," + anio + "," + id_periodo + "," + modalidad + "," + fechaUltimaDispo +
				"," + noDispo + "," + saldoLineas + "," + fechaCarga
			);

			numreg++;
		}
		if (numreg == 0)	{
			contenidoArchivo.append("\nNo se Encontró Ningún Registro");
		}

		if(!archivos.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			success = false;
			resultado.put("msg", "Error al generar el archivo Errores ");
		} else {
			nombreArchivo = archivos.nombre;
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}

	}catch(Throwable e){
		log.error("CargaArchivo.descargarCorrectos");
		e.printStackTrace();
		log.error("folioCarga	       		= <" + folioCarga                              + ">");
		log.error("iNoUsuario               = <" + iNoUsuario                                     + ">");
		log.error("strNombreUsuario         = <" + strNombreUsuario                               + ">");

		msg		= e.getMessage();
		success	= false;
	}

	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg);	
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("37CargaCredEduca01ext.jsp"); 
	
} else if (    informacion.equals("GeneraArchivoPDF")	)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String 		msg 			= "";
	
	// Leer parametros
	String totalRegistros 	= (request.getParameter("totalRegistros")		== null)?"":request.getParameter("totalRegistros");
	String montoTotal 		= (request.getParameter("montoTotal")			== null)?"":request.getParameter("montoTotal");
	String claveUniversidad	= (request.getParameter("claveUniversidad")	== null)?""	:request.getParameter("claveUniversidad");
	String nombreUniversidad= (request.getParameter("nombreUniversidad")	== null)?""	:request.getParameter("nombreUniversidad");
	
	String 	urlArchivo 		= "";
	HashMap 	cabecera 		= null;
	try {
		
		// Crear map con la informacion del cabcera del PDF
		cabecera = new HashMap();
		cabecera.put("strPais", 				(String) session.getAttribute("strPais"));
		cabecera.put("iNoNafinElectronico", (String) ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()));
		cabecera.put("sesExterno", 			(String) session.getAttribute("sesExterno"));
		cabecera.put("strNombre", 				(String) session.getAttribute("strNombre"));
		cabecera.put("strNombreUsuario", 	(String) session.getAttribute("strNombreUsuario"));
		cabecera.put("strLogo", 				(String) session.getAttribute("strLogo"));
		
		String nombreArchivo = CargaArchivoEducativo.generaAcuseArchivoPDF(
			(ResultadosGarEducativo)session.getAttribute("rgprint"),
			strDirectorioTemp,
			cabecera,
			strDirectorioPublicacion,
			totalRegistros,
			montoTotal,
			nombreUniversidad,
			iNoUsuario,
			strNombreUsuario
		);

		urlArchivo = strDirecVirtualTemp+nombreArchivo;
		 
	}catch(Exception e){
			
		log.error("GeneraArchivoPDF(Exception)");
		e.printStackTrace();
		log.error("rg                       = <" + (ResultadosGarEducativo)session.getAttribute("rgprint") + ">");
		log.error("strDirectorioTemp        = <" + strDirectorioTemp                              + ">");
		log.error("cabecera                 = <" + cabecera                                       + ">");
		log.error("strDirectorioPublicacion = <" + strDirectorioPublicacion                       + ">");
		log.error("totalRegistros           = <" + totalRegistros                                 + ">");
		log.error("montoTotal               = <" + montoTotal                                     + ">");
		log.error("claveUniversidad         = <" + claveUniversidad                               + ">");
		log.error("iNoUsuario               = <" + iNoUsuario                                     + ">");
		log.error("strNombreUsuario         = <" + strNombreUsuario                               + ">");

		msg		= e.getMessage();
		success	= false;
			
	}

	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo ); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)						);

	infoRegresar = resultado.toString();

} else {

	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");

}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>