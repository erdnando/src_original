
Ext.onReady(function() {

	//------------Consulta del Monitor ----------------------------
	
	var mostrarConsulta = function(grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var ic_if = registro.get('IC_IF');	
		var modalidad = registro.get('MODALIDAD');	
		if(modalidad == 'Primer Perdidas- 1ra Disposici�n y/o Pari Passu') no_disposicion ='1'; 
		if(modalidad != 'Primer Perdidas- 1ra Disposici�n y/o Pari Passu') no_disposicion = '0'; 		
		document.location.href  = "validaCargaReg.jsp?ic_if="+ic_if+'&informacion=Consulta'+'&intermediario='+ic_if+'&no_disposicion='+no_disposicion;
	}
	
	
	var procesarConsultaMonitorData = function(store, arrRegistros, opts) 	{
							
		var gridMonitor = Ext.getCmp('gridMonitor');	
		var el = gridMonitor.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridMonitor.isVisible()) {
				gridMonitor.show();
			}								
			if(store.getTotalCount() > 0) {			
				el.unmask();					
			} else {								
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaMonitorData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : 'validaCargaReg.data.jsp',
		baseParams: {
			informacion: 'ConsultarMonitor'
		},		
		fields: [
			{	name: 'IC_IF'},	
			{	name: 'NOMBRE_IF'},			
			{	name: 'NO_REGISTROS'},
			{	name: 'MONTO_ENVIADO'},
			{	name: 'MODALIDAD'}					
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaMonitorData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaMonitorData(null, null, null);					
				}
			}
		}		
	});
	
		
	var gridMonitor = new Ext.grid.EditorGridPanel({	
		store: consultaMonitorData,
		id: 'gridMonitor',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Monitor',		
		hidden: true,
		columns: [	
			{
				header: 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'No. de Registros',
				tooltip: 'No. de Registros',
				dataIndex: 'NO_REGISTROS',
				sortable: true,
				width: 100,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Modalidad',
				tooltip: 'Modalidad',
				dataIndex: 'MODALIDAD',
				sortable: true,
				width: 270,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Monto Enviado',
				tooltip: 'Monto Enviado',
				dataIndex: 'MONTO_ENVIADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',				
				align: 'center',
				width: 120,
				items: [
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa'
						},
						handler: mostrarConsulta
					}
				]
			}
		],	
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 820,		
		frame: true		
	});
	
	
// -------------------Criterios de Busqueda--------------------


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			gridMonitor, 
			NE.util.getEspaciador(20)			
		]
	});
	consultaMonitorData.load();	
});	
