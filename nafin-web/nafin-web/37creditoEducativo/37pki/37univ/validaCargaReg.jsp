<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/37creditoEducativo/37secsession.jspf" %>
<%@ include file="/37creditoEducativo/certificado.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_if = (request.getParameter("intermediario")!=null)?request.getParameter("intermediario"):"";
String no_disposicion = (request.getParameter("no_disposicion")!=null)?request.getParameter("no_disposicion"):"";

%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>


<%if( informacion.equals("")  || informacion.equals("Monitor") ) { %>
<script type="text/javascript" src="monitorValidaCargaReg.js?<%=session.getId()%>"></script>
<%} else if(informacion.equals("Consulta") ) { %>
<script type="text/javascript" src="validaCargaReg.js?<%=session.getId()%>"></script>
<% } %>

<%@ include file="/00utils/componente_firma.jspf" %>


</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%@ include file="/01principal/01uni/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01uni/menuLateralFlotante.jspf"%>
	<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01uni/pie.jspf"%>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="ic_if" name="ic_if" value="<%=ic_if%>"/>	
	<input type="hidden" id="no_disposicion" name="no_disposicion" value="<%=no_disposicion%>"/>		
</form>

<form id='formAux' name="formAux" target='_new'></form>

</body>
</html>