<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,
	java.util.*,
	java.math.*, 
	java.text.*, 
	java.sql.*,
	netropology.utilerias.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.exception.*,
	com.netro.model.catalogos.*,	
	com.netro.educativo.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/37creditoEducativo/37secsession_extjs.jspf"%>
<%@ include file="../../certificado.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String intermediario = (request.getParameter("intermediario")!=null)?request.getParameter("intermediario"):"";
String fechaCargaIni = (request.getParameter("fechaCargaIni")!=null)?request.getParameter("fechaCargaIni"):"";
String fechaCargaFin = (request.getParameter("fechaCargaFin")!=null)?request.getParameter("fechaCargaFin"):"";
String credito = (request.getParameter("credito")!=null)?request.getParameter("credito"):"";
String matricula = (request.getParameter("matricula")!=null)?request.getParameter("matricula"):"";
String estudios = (request.getParameter("estudios")!=null)?request.getParameter("estudios"):"";
String fechaAcreditadoIni = (request.getParameter("fechaAcreditadoIni")!=null)?request.getParameter("fechaAcreditadoIni"):"";
String fechaAcreditadoFin = (request.getParameter("fechaAcreditadoFin")!=null)?request.getParameter("fechaAcreditadoFin"):"";
String anio = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
String periodo = (request.getParameter("periodo")!=null)?request.getParameter("periodo"):"";
String modalidad = (request.getParameter("modalidad")!=null)?request.getParameter("modalidad"):"";
String fechaDisposicionIni = (request.getParameter("fechaDisposicionIni")!=null)?request.getParameter("fechaDisposicionIni"):"";
String fechaDisposicionFin = (request.getParameter("fechaDisposicionFin")!=null)?request.getParameter("fechaDisposicionFin"):"";
String no_disposicion = (request.getParameter("no_disposicion")!=null)?request.getParameter("no_disposicion"):"";
String impDisposicion = (request.getParameter("impDisposicion")!=null)?request.getParameter("impDisposicion"):"";
String montoTotal = (request.getParameter("montoTotal")!=null)?request.getParameter("montoTotal"):"";
String saldoLinea = (request.getParameter("saldoLinea")!=null)?request.getParameter("saldoLinea"):"";
String estatus = (request.getParameter("estatus")!=null)?request.getParameter("estatus"):"";
String  nombreIntermediario = (request.getParameter("nombreIntermediario")!=null)?request.getParameter("nombreIntermediario"):"";
String  noRegAcep = (request.getParameter("noRegAcep")!=null)?request.getParameter("noRegAcep"):"0";
String  noRegRechazados = (request.getParameter("noRegRechazados")!=null)?request.getParameter("noRegRechazados"):"0";
String  noRegistros = (request.getParameter("noRegistros")!=null)?request.getParameter("noRegistros"):"0";
String  acuseT = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
String  aceptados = (request.getParameter("aceptados")!=null)?request.getParameter("aceptados"):"";
String  rechazados = (request.getParameter("rechazados")!=null)?request.getParameter("rechazados"):"";
String  operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String  monitor = (request.getParameter("monitor")!=null)?request.getParameter("monitor"):"";
String  modalidadP = (request.getParameter("modalidadP")!=null)?request.getParameter("modalidadP"):"";

String fechaHora ="", fecha ="",  hora=""; 
int  start= 0, limit =0;

try{
	fechaHora = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(new java.util.Date());
	fecha = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	hora = new java.text.SimpleDateFormat("HH:mm").format(new java.util.Date());
}catch(Exception e){
	fechaHora ="";
	fecha =""; 
	hora=""; 
}

String ic_reg_aceptados[] = request.getParameterValues("ic_reg_aceptados");
String ic_reg_rechazados[] = request.getParameterValues("ic_reg_rechazados");
String causaRechazo[] = request.getParameterValues("causaRechazo");
String montosAceptados[] = request.getParameterValues("montosAceptados");

StringBuffer regRechazados = new StringBuffer();
StringBuffer regAceptados = new StringBuffer();

if(ic_reg_aceptados !=null){
	regAceptados = new StringBuffer();
	for(int i=0;i<ic_reg_aceptados.length;i++){		
		regAceptados.append(ic_reg_aceptados[i]+',');	
	}
}

if(ic_reg_rechazados !=null){
	regRechazados = new StringBuffer();
	for(int i=0;i<ic_reg_rechazados.length;i++){
		regRechazados.append(ic_reg_rechazados[i]+',');
	}
}
if(regAceptados.length()>0){
	regAceptados.deleteCharAt(regAceptados.length()-1);
	aceptados = regAceptados.toString();
}
if(regRechazados.length()>0){
	regRechazados.deleteCharAt(regRechazados.length()-1);
	rechazados = regRechazados.toString();
}

String infoRegresar ="", consulta ="",  universisad="",campus = "", nombreIF="";
JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

CreditoEducativo educativoBean = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);	
if (informacion.equals("CatalogoIF")  ) {
	
	CatalogoIF_Universidad catalogo = new CatalogoIF_Universidad();
	catalogo.setCampoClave("i.ic_if");
	catalogo.setCampoDescripcion("i.cg_razon_social");			
	catalogo.setUniversidad(iNoCliente);
	catalogo.setOrden("i.ic_if");
	
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(catalogo.getJSONElementos());
	jsonObj.put("intermediario",intermediario);	
	infoRegresar = jsonObj.toString();		
	

}else if ( informacion.equals("ConsultarMonitor") 	||  informacion.equals("Consultar")   ||  informacion.equals("ConsultarTotales")  

||  informacion.equals("ConsultarValidos")  ||  informacion.equals("ArchivoCSVCons")  ||  informacion.equals("ArchivoCSValidados") 

||  informacion.equals("ArchivoAcuse")    ) {

	ConsValidaRegistrosV paginador = new ConsValidaRegistrosV();
	paginador.setIntermediario(intermediario);
	paginador.setFechaCargaIni(fechaCargaIni);
	paginador.setFechaCargaFin(fechaCargaFin);
	paginador.setCredito(credito);
	paginador.setMatricula(matricula);
	paginador.setEstudios(estudios);
	paginador.setFechaAcreditadoIni(fechaAcreditadoIni);
	paginador.setFechaAcreditadoFin(fechaAcreditadoFin);
	paginador.setAnio(anio);
	paginador.setPeriodo(periodo);
	paginador.setModalidad(modalidad);
	paginador.setFechaDisposicionIni(fechaDisposicionIni);
	paginador.setFechaDisposicionFin(fechaDisposicionFin);
	paginador.setNo_disposicion(no_disposicion);
	paginador.setImpDisposicion(impDisposicion);
	paginador.setMontoTotal(montoTotal);
	paginador.setSaldoLinea(saldoLinea);	
	paginador.setUniversidad(iNoCliente);
	paginador.setTipoConsulta(informacion);
	paginador.setEstatus(estatus);
	paginador.setRegRechazados(aceptados);
	paginador.setRegAceptados(rechazados);
	paginador.setAcuse(acuseT);
	paginador.setMonitor(monitor);
	paginador.setModalidaPrim(modalidadP);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	queryHelper.setMultiplesPaginadoresXPagina(true);
	
	consulta="";
	jsonObj = new JSONObject();
	
	if (informacion.equals("ConsultarMonitor") ){
		try {
			Registros reg	=	queryHelper.doSearch();
			
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
					
	} else if (informacion.equals("Consultar") ){
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			
			if (reg.next()) {
				universisad = (reg.getString("UNIVERSIDAD") == null) ? "" : reg.getString("UNIVERSIDAD");
				campus = (reg.getString("CAMPUS") == null) ? "" : reg.getString("CAMPUS");
				nombreIF = (reg.getString("INTERMEDIARIO") == null) ? "" : reg.getString("INTERMEDIARIO");
			}
			
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
	
		jsonObj.put("universisad","<b>Universidad:</b> "+universisad);		
		jsonObj.put("campus","<b>Campus:</b> "+campus);		
		jsonObj.put("nombreIF","<b>Nombre Intermediario:</b> "+nombreIF);
		jsonObj.put("monitor",monitor );  
		if(no_disposicion.equals("1") ) {
			jsonObj.put("no_disposicion",no_disposicion);
		}else if(no_disposicion.equals("0") ) {
			jsonObj.put("no_disposicion","");
		}
		
			
		
	}else if (informacion.equals("ConsultarTotales") ){
	
		double totalReg =0, impDisp = 0, montoTotalT =0, saldoLineaT =0;
		int regTotales=0;
		Registros reg	= queryHelper.doSearch();		
		while (reg.next()) {
			regTotales++;
			String importe = (reg.getString("IMPORTE_DISPOSICON") == null) ? "" : reg.getString("IMPORTE_DISPOSICON");
			String monto = (reg.getString("MONTO_TOTAL") == null) ? "" : reg.getString("MONTO_TOTAL");
			String linea = (reg.getString("SALDO_LINEA") == null) ? "" : reg.getString("SALDO_LINEA");
			
			impDisp+=Double.parseDouble((String)importe);
			montoTotalT+=Double.parseDouble((String)monto);
			saldoLineaT+=Double.parseDouble((String)linea);
				
		}
		datos = new HashMap();
		datos.put("NO_REGISTROS",String.valueOf(regTotales));		
		datos.put("IMPORTE_DISPOSICION",Double.toString (impDisp) );					
		datos.put("MONTO_TOTAL", Double.toString (montoTotalT) );
		datos.put("SALDO_LINEA", Double.toString (saldoLineaT) );
		registros.add(datos);		
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";					
		jsonObj.put("success", new Boolean(true));
		jsonObj = JSONObject.fromObject(consulta);
		
	}else  if (informacion.equals("ArchivoCSVCons") ) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}		
	}else if( informacion.equals("ArchivoAcuse") ) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}	
		
	}else	if (informacion.equals("ConsultarValidos")  ) {	
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
				
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		 
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);										
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		
		jsonObj = JSONObject.fromObject(consulta);	
		
	}else  if ( informacion.equals("ArchivoCSValidados") ) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
		
	}
	
	jsonObj.put("intermediario",intermediario);		
	infoRegresar = jsonObj.toString();

}else if ( informacion.equals("ConsultarRechazados") 	||  informacion.equals("ArchivoCSRechazados")  ) {

	ConsValidaRegistros paginador = new ConsValidaRegistros();
	paginador.setIntermediario(intermediario);
	paginador.setFechaCargaIni(fechaCargaIni);
	paginador.setFechaCargaFin(fechaCargaFin);
	paginador.setCredito(credito);
	paginador.setMatricula(matricula);
	paginador.setEstudios(estudios);
	paginador.setFechaAcreditadoIni(fechaAcreditadoIni);
	paginador.setFechaAcreditadoFin(fechaAcreditadoFin);
	paginador.setAnio(anio);
	paginador.setPeriodo(periodo);
	paginador.setModalidad(modalidad);
	paginador.setFechaDisposicionIni(fechaDisposicionIni);
	paginador.setFechaDisposicionFin(fechaDisposicionFin);
	paginador.setNo_disposicion(no_disposicion);
	paginador.setImpDisposicion(impDisposicion);
	paginador.setMontoTotal(montoTotal);
	paginador.setSaldoLinea(saldoLinea);	
	paginador.setUniversidad(iNoCliente);
	paginador.setTipoConsulta(informacion);
	paginador.setEstatus(estatus);
	paginador.setRegRechazados(aceptados);
	paginador.setRegAceptados(rechazados);
	paginador.setAcuse(acuseT);
	paginador.setMonitor(monitor);
	paginador.setIc_reg_rechazados(ic_reg_rechazados);
	paginador.setCausaRechazo(causaRechazo);
	
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	queryHelper.setMultiplesPaginadoresXPagina(true);
	Registros registrosC =null;

	if (informacion.equals("ConsultarRechazados")  ) {	
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
				
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			//consulta = queryHelper.getJSONPageResultSet(request,start,limit);		
			registrosC 	= queryHelper.getPageResultSet(request,start,limit);
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		
	
		
		while (registrosC.next()) {
		   
			String ic_registro = registrosC.getString("IC_REGISTRO");
			String ic_if = registrosC.getString("IC_IF");
			String intermediario2 = registrosC.getString("INTERMEDIARIO");
			String fecha_carga= registrosC.getString("FECHA_CARGA");
			String credito2= registrosC.getString("CREDITO");
			String nombre_acreditado = registrosC.getString("NOMBRE_ACREDITADO");
			String rfc= registrosC.getString("RFC");
			String matricula2 = registrosC.getString("MATRICULA");
			String estudios2 = registrosC.getString("ESTUDIOS");	
			String fecha_ingreso = registrosC.getString("FECHA_INGRESO");
			String anio_colocacion = registrosC.getString("ANIO_COLOCACION");
			String periodo_educativo = registrosC.getString("PERIODO_EDUCATIVO");
			String modalidad_programa = registrosC.getString("MODALIDAD_PROGRAMA");	
			String fecha_ul_disposicion = registrosC.getString("FECHA_UL_DISPOSICION");
			String no_disposicion2 = registrosC.getString("NO_DISPOSICION");
			String importe_disposicion =  registrosC.getString("IMPORTE_DISPOSICON");
			String monto_total = registrosC.getString("MONTO_TOTAL");
			String saldo_linea = registrosC.getString("SALDO_LINEA");			
			String aceptados2 = registrosC.getString("ACEPTADOS");
			String rechazados2 = registrosC.getString("RECHAZADOS");
			String fn_financiamiento = registrosC.getString("FN_FINANCIAMIENTO");
			String  modalidad2 = registrosC.getString("MODALIDAD");
			String  causa_rechazo = registrosC.getString("CAUSA_RECHAZO");
						
			if(causa_rechazo.equals("")) {
				for(int i=0;i<ic_reg_rechazados.length;i++){
					if(ic_reg_rechazados[i].equals(ic_registro)) {
						causa_rechazo = causaRechazo[i];
					}
				}
			}
			datos = new HashMap();
			datos.put("IC_REGISTRO", ic_registro);
			datos.put("IC_IF",ic_if );
			datos.put("INTERMEDIARIO", intermediario2);
			datos.put("FECHA_CARGA",fecha_carga);	
			datos.put("CREDITO",credito2);
			datos.put("NOMBRE_ACREDITADO",nombre_acreditado); 	
			datos.put("RFC", rfc);
			datos.put("MATRICULA",matricula2);	
			datos.put("ESTUDIOS", estudios2);
			datos.put("FECHA_INGRESO", fecha_ingreso);	
			datos.put("ANIO_COLOCACION",anio_colocacion);
			datos.put("PERIODO_EDUCATIVO",periodo_educativo);
			datos.put("MODALIDAD_PROGRAMA", modalidad_programa);
			datos.put("FECHA_UL_DISPOSICION",fecha_ul_disposicion);
			datos.put("NO_DISPOSICION", no_disposicion2);
			datos.put("IMPORTE_DISPOSICON", importe_disposicion);
			datos.put("MONTO_TOTAL", monto_total);
			datos.put("SALDO_LINEA",saldo_linea);			
			datos.put("ACEPTADOS", aceptados2);
			datos.put("RECHAZADOS",rechazados2);
			datos.put("CAUSA_RECHAZO",causa_rechazo);
			datos.put("FN_FINANCIAMIENTO",fn_financiamiento);
			datos.put("MODALIDAD", modalidad2);		
			registros.add(datos);	
								
		}
			
		String consulta2 =  "{\"success\": true, \"total\": \"" + queryHelper.getIdsSize() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta2);
				
			
	}else  if ( informacion.equals("ArchivoCSRechazados") ) {
	try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
		
	}
	
	jsonObj.put("intermediario",intermediario);		
	infoRegresar = jsonObj.toString();

}else if ( informacion.equals("PreAcuse") )  {

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("TipoOperacion", "Crédito Educativo");
	jsonObj.put("intermediario", nombreIntermediario);		
	jsonObj.put("fechaHora",fechaHora );		
	jsonObj.put("Usuario",iNoUsuario+" - "+strNombreUsuario);		
	jsonObj.put("noReg", noRegistros);	
	jsonObj.put("noRegAcep", noRegAcep);
	jsonObj.put("noRegRechazados", noRegRechazados);
	jsonObj.put("aceptados",aceptados);	
	jsonObj.put("rechazados",rechazados);	
	infoRegresar = jsonObj.toString();

}else if ( informacion.equals("transEducativoaGarantia") )  {

	StringBuffer regAcep = new StringBuffer();
	double impte_total = 0;
	//Registros Aceptados
	
	if(!noRegAcep.equals("0")) {
		if(ic_reg_aceptados !=null){
			for(int i=0;i<ic_reg_aceptados.length;i++){		
				regAcep.append(ic_reg_aceptados[i]+',');
			}
			regAcep.deleteCharAt(regAcep.length()-1);
		
			//MOntos Aceptados 
			
			if(montosAceptados !=null ){
				for(int i=0;i<montosAceptados.length;i++){		
					impte_total += Double.parseDouble((String)montosAceptados[i]);
				}
			}
		}
	}

	List parametros  =  new ArrayList();
	parametros.add(intermediario);
	parametros.add(nombreIntermediario);	
	parametros.add(noRegAcep);
	parametros.add(noRegRechazados);
	parametros.add(noRegistros);
	parametros.add(iNoUsuario);
	parametros.add(fechaHora);
	parametros.add(fecha);
	parametros.add(hora);	
	parametros.add(Double.toString (impte_total) );
	parametros.add(regAcep.toString());  // consulta
	parametros.add(strNombreUsuario);  
	parametros.add(iNoCliente);  
		
	String externContent = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
	String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
	char getReceipt = 'Y';
	String folioCert  ="", _acuse  ="", mensajeError ="", respuesta ="";
	Acuse acuse = new Acuse(iNoUsuario);
	
	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		folioCert = acuse.toString();			
		Seguridad s = new Seguridad();
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			_acuse = s.getAcuse();
			try {
				respuesta = educativoBean.transEducativoaGarantia(parametros, ic_reg_aceptados, ic_reg_rechazados, causaRechazo, _acuse, no_disposicion);
			} catch (AppException ne){
				mensajeError = "La autentificación no se llevo acabo con éxito"+s.mostrarError();
			}
		}else  {
			mensajeError = "La autentificación no se llevo acabo con éxito"+s.mostrarError();
		}
	}else  {
		mensajeError = "La autentificación no se llevo acabo con éxito";
	}	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);
	jsonObj.put("fechaHora",fechaHora );	
	jsonObj.put("noRegAcep",noRegAcep );
	jsonObj.put("noRegRechazados",noRegRechazados );
	jsonObj.put("mensajeError",mensajeError );	
	jsonObj.put("acuse",_acuse );	
	jsonObj.put("TipoOperacion", "Crédito Educativo");
	jsonObj.put("intermediario", nombreIntermediario);		
	jsonObj.put("fechaHora",fechaHora );		
	jsonObj.put("Usuario",iNoUsuario+" - "+strNombreUsuario);		
	jsonObj.put("noReg", noRegistros);			
	infoRegresar  = jsonObj.toString();

}

%>

<%=infoRegresar%>

