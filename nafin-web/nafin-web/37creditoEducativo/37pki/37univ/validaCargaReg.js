
	
function selecDeselec(check){
	var  gridConsulta = Ext.getCmp('gridConsulta');
	var store = gridConsulta.getStore();
	
	//alert(check.id);

	if(check.id == 'todoAcepta'){	
		if(check.checked==true)  {
			store.each(function(rec){ rec.set('ACEPTADOS', true); rec.set('RECHAZADOS',false);  rec.set('CAUSA_RECHAZO',''); })
			var el_otro_check = document.getElementById('todoRechaza');
			el_otro_check.checked = false;
		}else{
			store.each(function(rec){ rec.set('ACEPTADOS', false);})
		}
	}else if (check.id == 'todoRechaza'){
		if(check.checked==true)  {
			store.each(function(rec){ rec.set('ACEPTADOS', false); rec.set('RECHAZADOS',true); })
			var el_otro_check = document.getElementById('todoAcepta');
			el_otro_check.checked = false;
		}else{
			store.each(function(rec){ rec.set('RECHAZADOS', false);  rec.set('CAUSA_RECHAZO','');  })
		}
	}
	store.commitChanges();
}


Ext.onReady(function() {

	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
	
	var ic_if =  Ext.getDom('ic_if').value;
	var no_disposicion =  Ext.getDom('no_disposicion').value;
	
	var ic_reg_aceptados = [];
	var ic_reg_rechazados = [];
	var montosAceptados = [];
	var causaRechazo   = [];
	var causaRechazoP = [];
	var cancelar =  function() {  
		ic_reg_aceptados = [];
		ic_reg_rechazados = [];
		montosAceptados =[];	
		causaRechazo   = [];
		causaRechazoP   = [];
	}
		
	// boton para Transmitir  de Credito Electronico a  Garantias
	
	function transmiteAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			if(info.respuesta !='Fallo') {
						
				var preAcuseCifras = [
					['Tipo de Operaci�n', info.TipoOperacion],
					['Intermediario Financiero', info.intermediario],
					['Fecha y Hora de Validaci�n', info.fechaHora],
					['Usuario', info.Usuario],
					['N�mero de Registros Validados', info.noReg]
				];
				
				storeCifrasData.loadData(preAcuseCifras);	
							
				var  gridValidados = Ext.getCmp('gridValidados');
				gridValidados.setTitle('<div><div style="float:left">REGISTRO VALIDO-UNIVERSIDAD'+info.respuesta+'</div>');
				
				Ext.getCmp('acuse').setValue(info.acuse);
				Ext.getCmp("btnTransmitir").hide();	
				Ext.getCmp("btnCancelar").hide();
				Ext.getCmp('btnImprimir').show();
				Ext.getCmp('btnSalir').show();
				Ext.getCmp('gridControlPre').hide();
				
				Ext.getCmp("mensajeAuto").setValue('<table width="400" align="center" ><tr><td><H1>Acuse Validaci�n de registros de Cr�dito Educativo</H1></td></tr></table>');				
				Ext.getCmp('mensajeAutorizacion').show();
				
				var dataControl = [	
					[info.noRegAcep, 'REGISTRO VALIDO-UNIVERSIDAD', info.respuesta],
					[info.noRegRechazados, 'REGISTRO RECHAZADO-UNIVERSIDAD', 'N/A']
				];
				storeControlData.loadData(dataControl);
				Ext.getCmp('gridControl').show();
				
				
				consultaValidadosData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					operacion: 'Generar',
					informacion: 'ConsultarValidos',
					start:0,
					limit:15,
					acuse:info.acuse,
					estatus:Ext.getCmp("estatusAcep").getValue()
				})
			});
			
			consultaRechazadosData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					operacion: 'Generar',
					informacion: 'ConsultarRechazados',
					start:0,
					limit:15,
					acuse:info.acuse,
					ic_reg_rechazados:ic_reg_rechazados,
					causaRechazo :causaRechazo,
					estatus:Ext.getCmp("estatusReg").getValue()
				})
			});  
			
									
			}

		} else {
				NE.util.mostrarConnError(response,opts);
				Ext.getCmp("mensajeAuto").setValue('<table width="400" align="center" ><tr><td><H1>La autentificaci�n no se llevo acabo con �xito </H1></td></tr></table>');					
				Ext.getCmp('btnSalir').show();
				Ext.getCmp('fpBotones').show();
				Ext.getCmp('gridCifrasControl').hide();
				Ext.getCmp('gridValidados').hide();
				Ext.getCmp('gridRechazados').hide();				
				Ext.getCmp('btnCancelar').hide();
				Ext.getCmp('btnTransmitir').hide();
				Ext.getCmp('btnImprimir').hide();
				Ext.getCmp('gridControlPre').hide();	
				Ext.getCmp('mensajeAutorizacion').show();				
		}
	}
	
	
	var confirmarTransision = function(pkcs7, textoFirmado, noRegAcep, noRegRechazados, noRegistros  ){
			
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnTransmitir").enable();	
			Ext.getCmp("btnCancelar").enable();	
			return;	//Error en la firma. Termina...
		
		}else  {			
			
			Ext.getCmp("btnTransmitir").disable();	
			Ext.getCmp("btnCancelar").disable();
			
			Ext.getCmp("estatusAcep").setValue("2");
			Ext.getCmp("estatusReg").setValue("3");
					
			Ext.Ajax.request({
				url: 'validaCargaReg.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'transEducativoaGarantia',
					ic_reg_aceptados:ic_reg_aceptados,
					ic_reg_rechazados:ic_reg_rechazados,					
					montosAceptados:montosAceptados,
					causaRechazo:causaRechazo,
					intermediario:intermediario,
					nombreIntermediario:nombreIntermediario,
					noRegAcep:noRegAcep,
					noRegRechazados:noRegRechazados,
					noRegistros:noRegistros,
					textoFirmado:textoFirmado,
					pkcs7:pkcs7,
					no_disposicion:no_disposicion
				}),
				callback: transmiteAcuse 
			});
		}
	
	}
	
	
	
	var procesarTransmitir = function() {
		
		var  gridSeleccionados = Ext.getCmp('gridSeleccionados');
		var store = gridSeleccionados.getStore();		
		
		var textoFirmar;
		var noRegistros=0, noRegAcep=0, noRegRechazados=0;
		cancelar(); //limpia las variables 
		
		store.each(function(record) {
			intermediario =record.data['IC_IF'];
			nombreIntermediario =record.data['INTERMEDIARIO'];
			
			if(record.data['ACEPTADOS']==true){			
				ic_reg_aceptados.push(record.data['IC_REGISTRO']);
				montosAceptados.push(record.data['FN_FINANCIAMIENTO']);
				noRegAcep++;			
			}
			if(record.data['RECHAZADOS']==true){			
				ic_reg_rechazados.push(record.data['IC_REGISTRO']);			
				causaRechazo.push(record.data['CAUSA_RECHAZO']);
				noRegRechazados++;
			}
		});
		
		noRegistros = noRegAcep + noRegRechazados;		
		var textoFirmado ="Tipo de Operaci�n: Cr�dito Educativo \n"+
								" Intermediario Financiero: "+nombreIntermediario+"\n"+
								" No de Registros Validados: "+noRegistros+"\n"+
								" Registros Validados: "+noRegAcep+"\n"+
								" Registros Rechazado: "+noRegRechazados+"\n";
		
			
		
		NE.util.obtenerPKCS7(confirmarTransision, textoFirmado, noRegAcep, noRegRechazados, noRegistros   );
				
	}
	
	
	//------------Leyenda Acuse y PreAcuse ----------------------------------------

	
	var mensajeAutorizacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutorizacion',							
		width:	'350',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeAuto', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: 'aceptados', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'rechazados', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: 'estatusReg', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'estatusAcep', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'acuse', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: 'claveInter', 	value: '' }	
		]
	});
		
	
	function procesarArchivoAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var fpBotones = new Ext.Container({
		layout: 'table',		
		style: 'margin:0 auto;',
		id: 'fpBotones',	
		hidden: true,
	   width: '200',
		heigth:'auto',		
		items: [
			{
				xtype: 'button',
				text: 'Cancelar',
				tooltip:	'Cancelar',					
				iconCls: 'cancelar',							
				id: 'btnCancelar',
				hidden: true,
				handler: function(boton, evento) {				
					window.location = 'validaCargaReg.jsp';
				}
			},			
			{
				xtype: 'button',
				text: 'Transmitir Registros',
				tooltip:	'Transmitir Registros',					
				iconCls: 'icoContinuar',							
				id: 'btnTransmitir',
				hidden: true,
				handler: procesarTransmitir
			},
			{
				xtype: 'button',
				text: 'Imprimir',
				tooltip:	'Imprimir',					
				iconCls: 'icoImprimir',							
				id: 'btnImprimir',
				hidden: true,
				handler: function(boton, evento) {
					Ext.Ajax.request({
						url: 'validaCargaReg.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoAcuse',
							acuse:Ext.getCmp('acuse').getValue()
						}),
						callback: procesarArchivoAcuse
					});
				}			
			},
			{
				xtype: 'button',
				text: 'Aceptar',
				tooltip:	'Aceptar',					
				iconCls: 'icoAceptar',							
				id: 'btnSalir',
				hidden: true,
				handler: function(boton, evento) {				
					window.location = 'validaCargaReg.jsp';
				}
			}
		]
	});
	
//------------Procesar Continuarl ----------------------------------------

	function continuaPreacuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			var preAcuseCifras = [
				['Tipo de Operaci�n', info.TipoOperacion],
				['Intermediario Financiero', info.intermediario],
				['Fecha y Hora de Validaci�n', info.fechaHora],
				['Usuario', info.Usuario],
				['N�mero de Registros Validados', info.noReg]
			];			
			storeCifrasData.loadData(preAcuseCifras);	
			
			
			var dataControlPre = [	
				[info.noRegAcep, 'REGISTRO VALIDO-UNIVERSIDAD'],
				[info.noRegRechazados, 'REGISTRO RECHAZADO-UNIVERSIDAD']
			];
			storeControlPreData.loadData(dataControlPre);
			Ext.getCmp('gridControlPre').show();
		
			Ext.getCmp('rechazados').setValue(info.rechazados);
			Ext.getCmp('aceptados').setValue(info.aceptados);
			
		
			Ext.getCmp("mensajeAuto").setValue('<table width="400" align="center" ><tr><td><H1>Preacuse Validaci�n de registros de Cr�dito Educativo</H1></td></tr></table>');
			Ext.getCmp('mensajeAutorizacion').show();		
		
			if(info.aceptados !=''){
				consultaValidadosData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'ConsultarValidos',
							start:0,
							limit:15,						
							aceptados:info.aceptados
						})
					});
			}
				if(info.rechazados !=''){
					consultaRechazadosData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'ConsultarRechazados',
							start:0,
							limit:15,
							ic_reg_rechazados:ic_reg_rechazados,
							causaRechazo :causaRechazoP,
							rechazados:info.rechazados
						})
					});
				}
			
			Ext.getCmp('forma').hide();
			Ext.getCmp('datosUni').hide();
			Ext.getCmp('gridConsulta').hide();
			Ext.getCmp('gridTotales').hide();
				
			Ext.getCmp('gridCifrasControl').show();
			Ext.getCmp('gridValidados').show();
			Ext.getCmp('gridRechazados').show();
			Ext.getCmp('fpBotones').show();	
			Ext.getCmp('btnCancelar').show();
			Ext.getCmp('btnTransmitir').show();
			
			Ext.getCmp('rechazados').setValue(info.rechazados);
			Ext.getCmp('aceptados').setValue(info.aceptados);
			
			if(info.noRegAcep=="0") {
				var el =  Ext.getCmp('gridValidados').getGridEl();	
				el.mask('No hay registros seleccionados', 'x-mask');
				Ext.getCmp('gridValidados').disable();
			}
			if(info.noRegRechazados=="0") {
				var el =  Ext.getCmp('gridRechazados').getGridEl();	
				el.mask('No hay registros seleccionados', 'x-mask');
				Ext.getCmp('gridRechazados').disable();
			}

		} else {
			NE.util.mostrarConnError(response,opts);			
		}
	}
	
	var procesarContinuar = function() {
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();		
		var modificados = store.getModifiedRecords();
		var columnModelGrid = gridConsulta.getColumnModel();
		
		var regSeleccioandos = [];		
		var noRegAcep=0,noRegRechazados =0;
		cancelar(); //limpia las variables 	
		
		var MontoDispA =0, MontoDispR =0;
		var MontoTotalA =0, MontoTotalR =0;
		var SaldoLineaA =0, SaldoLineaR =0;
		var numRegistro = -1;
		var errorValidacion = false;
		
		store.each(function(record) {	
			
			nombreIntermediario =record.data['INTERMEDIARIO'];
			numRegistro = store.indexOf(record);			
			regSeleccioandos.push(record); 
			
			//Registros Validos			
			if(record.data['ACEPTADOS']==true ){				
				ic_reg_aceptados.push(record.data['IC_REGISTRO']);						
				noRegAcep++;
				MontoDispA +=record.data['IMPORTE_DISPOSICON'];
				MontoTotalA +=record.data['MONTO_TOTAL'];
				SaldoLineaA +=record.data['SALDO_LINEA'];
				
			}
			
			//Registros Rechazados
			if(record.data['RECHAZADOS']==true){
				if(record.data['CAUSA_RECHAZO'] ==''){
					errorValidacion = true;					
					Ext.MessageBox.alert('Error de validaci�n','Debe de capturar la causa de rechazo',
						function(){
							gridConsulta.startEditing(numRegistro, columnModelGrid.findColumnIndex('CAUSA_RECHAZO'));
						}
					);
					return false;				
				}else {					
					ic_reg_rechazados.push(record.data['IC_REGISTRO']);					
					noRegRechazados++;
					MontoDispR +=record.data['IMPORTE_DISPOSICON'];
					MontoTotalR +=record.data['MONTO_TOTAL'];
					SaldoLineaR +=record.data['SALDO_LINEA'];
					causaRechazoP.push(record.data['CAUSA_RECHAZO']);
				}
			}	
			
		});
		
		if (errorValidacion) {
			return;
		}
		
		noRegistros = noRegAcep + noRegRechazados
		
		if(noRegistros >0){
			consultaSeleccData.add(regSeleccioandos);
		}
				
		if(noRegistros ==0){
			Ext.MessageBox.alert('Aviso','Debe seleccionar al menos un registro');
			return false;			
		}else  {	
		
			Ext.getCmp("estatusAcep").setValue("1");
			Ext.getCmp("estatusReg").setValue("1");
			
			Ext.Ajax.request({
				url: 'validaCargaReg.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'PreAcuse',
					noRegistros:noRegistros,
					nombreIntermediario:nombreIntermediario,
					noRegAcep:noRegAcep,
					noRegRechazados:noRegRechazados,
					ic_reg_aceptados:ic_reg_aceptados,
					ic_reg_rechazados:ic_reg_rechazados,
					monitor:Ext.getCmp('monitor').getValue()
					
				}),
				callback: continuaPreacuse
			});
		}
	}
	
	
	//------------Cifras de Control  para PreAcuse ----------------------------------------

	
	var storeControlPreData = new Ext.data.ArrayStore({
		fields: [			
			{name: 'NO_REGISTROS' },
			{name: 'ESTATUS'}							
		]	
	});
	
	
	var gridControlPre = new Ext.grid.EditorGridPanel({	
		id: 'gridControlPre',
		store: storeControlPreData,	
		title: '',			
		columns: [	
			{
				header: 'No_Registos',
				dataIndex: 'NO_REGISTROS',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Estatus',
				dataIndex: 'ESTATUS',
				width: 250,
				align: 'left'				
			}			
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 100,
		width: 420,		
		frame: true	,
	   style: 'margin:0 auto;',
		hidden: true
	});
	
	
//------------Cifras de Control  para Acuse ----------------------------------------
	
	var storeControlData = new Ext.data.ArrayStore({
		fields: [			
			{name: 'NO_REGISTROS' },
			{name: 'ESTATUS'},
			{name: 'FOLIO_OPERACION' }						
		]	
	});
	
	
	var gridControl = new Ext.grid.EditorGridPanel({	
		id: 'gridControl',
		store: storeControlData,	
		title: '',			
		columns: [	
			{
				header: 'No_Registos',
				dataIndex: 'NO_REGISTROS',
				width: 150,
				align: 'center'				
			},
			{
				header: 'Estatus',
				dataIndex: 'ESTATUS',
				width: 250,
				align: 'left'				
			},
			{
				header: 'Folio Operaci�n',
				dataIndex: 'FOLIO_OPERACION',
				width: 150,
				align: 'center'				
			}		
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 100,
		width: 570,		
		frame: true	,
	   style: 'margin:0 auto;',
		hidden: true
	});
	
	//------------Cifras de Control ----------------------------------------
	
	var storeCifrasData = new Ext.data.ArrayStore({
		fields: [
			{name: 'etiqueta'},
			{name: 'informacion'}
		]
	 });
	
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 180,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 150,
		style: 'margin:0 auto;',				
		title:'<div><div style="text-align: center;">Cifras de Control</div>',
		frame: true
	});
	
	//------------esta lo uso  para realizar el acuse Acuse  ----------------------------------------

	var consultaSeleccData = new Ext.data.ArrayStore({
		fields: [	
			{	name: 'IC_REGISTRO'},
			{	name: 'IC_IF'},
			{	name: 'INTERMEDIARIO'},
			{	name: 'CAUSA_RECHAZO'},
			{ name: 'ACEPTADOS',	convert: NE.util.string2boolean },
			{ name: 'RECHAZADOS', convert: NE.util.string2boolean }
		]
	});
	
	var gridSeleccionados = new Ext.grid.EditorGridPanel({	
		store: consultaSeleccData,
		id: 'gridSeleccionados'		
	});
	
//------------Consulta de Registros Rechazados ----------------------------------------

	function procesarArRechazados(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaRechazadosData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridRechazados = Ext.getCmp('gridRechazados');	
		var el = gridRechazados.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridRechazados.isVisible()) {
				gridRechazados.show();
			}								
			
			var jsonData = store.reader.jsonData;
			//Ext.getCmp('rechazados').setValue(jsonData.rechazados);
			//Ext.getCmp('aceptados').setValue(jsonData.aceptados);
			
			if(store.getTotalCount() > 0) {					
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var consultaRechazadosData = new Ext.data.JsonStore({
		root : 'registros',
		url : 'validaCargaReg.data.jsp',
		baseParams: {
			informacion: 'ConsultarRechazados'		
		},
		hidden: true,		
		fields: [
			{	name: 'IC_REGISTRO'},
			{	name: 'IC_IF'},
			{	name: 'INTERMEDIARIO'},
			{	name: 'FECHA_CARGA'},	
			{	name: 'CREDITO'},	
			{	name: 'NOMBRE_ACREDITADO'},	
			{	name: 'RFC'},	
			{	name: 'MATRICULA'},	
			{	name: 'ESTUDIOS'},	
			{	name: 'FECHA_INGRESO'},	
			{	name: 'ANIO_COLOCACION'},	
			{	name: 'PERIODO_EDUCATIVO'},	
			{	name: 'MODALIDAD_PROGRAMA'},	
			{	name: 'FECHA_UL_DISPOSICION'},	
			{	name: 'NO_DISPOSICION'},	
			{	name: 'IMPORTE_DISPOSICON'},	
			{	name: 'MONTO_TOTAL'},	
			{	name: 'SALDO_LINEA'},			
			{ name: 'ACEPTADOS',	convert: NE.util.string2boolean },
			{ name: 'RECHAZADOS', convert: NE.util.string2boolean },
			{	name: 'CAUSA_RECHAZO'},
			{	name: 'FN_FINANCIAMIENTO'},
			{ name:'MODALIDAD' }
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaRechazadosData,
			beforeLoad:	{
				fn: function(store, options){
					Ext.apply(options.params, {
						rechazados:Ext.getCmp('rechazados').getValue(),
						ic_reg_rechazados:ic_reg_rechazados,
						causaRechazo :causaRechazoP
						//aceptados:Ext.getCmp('aceptados').getValue()
					 });
				}
			},
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConsultaRechazadosData(null, null, null);						
				}
			}
		}
	});
	
	var gridRechazados = new Ext.grid.EditorGridPanel({	
		store: consultaRechazadosData,
		id: 'gridRechazados',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'REGISTRO RECHAZADO-UNIVERSIDAD',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Fecha y Hora de Carga',
				tooltip: 'Fecha y Hora de Carga',
				dataIndex: 'FECHA_CARGA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Cr�dito',
				tooltip: 'Cr�dito',
				dataIndex: 'CREDITO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Nombre del acreditado',
				tooltip: 'Nombre del acreditado',
				dataIndex: 'NOMBRE_ACREDITADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Matricula',
				tooltip: 'Matricula',
				dataIndex: 'MATRICULA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Estudios',
				tooltip: 'Estudios',
				dataIndex: 'ESTUDIOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Ingreso',
				tooltip: 'Fecha de Ingreso',
				dataIndex: 'FECHA_INGRESO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'A�o de colocaci�n',
				tooltip: 'A�o de colocaci�n',
				dataIndex: 'ANIO_COLOCACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Periodo Educativo',
				tooltip: 'Periodo Educativo',
				dataIndex: 'PERIODO_EDUCATIVO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Modalidad Programa',
				tooltip: 'Modalidad Programa',
				dataIndex: 'MODALIDAD_PROGRAMA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha �ltima disposici�n',
				tooltip: 'Fecha �ltima disposici�n',
				dataIndex: 'FECHA_UL_DISPOSICION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. de disposici�n',
				tooltip: 'No. de disposici�n',
				dataIndex: 'NO_DISPOSICION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Importe Disposici�n',
				tooltip: 'Importe Disposici�n',
				dataIndex: 'IMPORTE_DISPOSICON',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: 'Monto Total',
				tooltip: 'Monto Total',
				dataIndex: 'MONTO_TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Saldo de L�nea',
				tooltip: 'Saldo de L�nea',
				dataIndex: 'SALDO_LINEA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{						
				header:'Modalidad',
				tooltip: 'MODALIDAD',
				dataIndex : 'MODALIDAD',
				width : 150,
				align: 'center',
				sortable : false			
			},
			{
				header: 'Causas de rechazo',
				tooltip: 'Causas de rechazo',
				dataIndex: 'CAUSA_RECHAZO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			}
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacionR',
			displayInfo: true,
			store: consultaRechazadosData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArcRechazados',
					handler: function(boton, evento) {
						var barraPaginacionR = Ext.getCmp("barraPaginacionR");
						
						Ext.Ajax.request({
							url: 'validaCargaReg.data.jsp',
							params: {
								informacion: 'ArchivoCSRechazados',
								estatus:Ext.getCmp("estatusReg").getValue(),
								rechazados:Ext.getCmp('rechazados').getValue(),
								ic_reg_rechazados:ic_reg_rechazados,
								causaRechazo :causaRechazoP,
								start: barraPaginacionR.cursor,
								limit: barraPaginacionR.pageSize,
								acuse:Ext.getCmp("acuse").getValue()	
							},
							callback: procesarArRechazados
						});
					
					}
				}				
			]
		}
	});
	
	

//------------Consulta de Registros Validados ----------------------------------------

	function procesarArValidados(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	var procesarConsultaValidosData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridValidados = Ext.getCmp('gridValidados');	
		var el = gridValidados.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridValidados.isVisible()) {
				gridValidados.show();
			}	
		
			var jsonData = store.reader.jsonData;
			
			if(store.getTotalCount() > 0) {					
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaValidadosData = new Ext.data.JsonStore({
		root : 'registros',
		url : 'validaCargaReg.data.jsp',
		baseParams: {
			informacion: 'ConsultarValidos'			
		},
		hidden: true,
		fields: [	
			{	name: 'IC_REGISTRO'},
			{	name: 'IC_IF'},
			{	name: 'INTERMEDIARIO'},
			{	name: 'FECHA_CARGA'},	
			{	name: 'CREDITO'},	
			{	name: 'NOMBRE_ACREDITADO'},	
			{	name: 'RFC'},	
			{	name: 'MATRICULA'},	
			{	name: 'ESTUDIOS'},	
			{	name: 'FECHA_INGRESO'},	
			{	name: 'ANIO_COLOCACION'},	
			{	name: 'PERIODO_EDUCATIVO'},	
			{	name: 'MODALIDAD_PROGRAMA'},	
			{	name: 'FECHA_UL_DISPOSICION'},	
			{	name: 'NO_DISPOSICION'},	
			{	name: 'IMPORTE_DISPOSICON'},	
			{	name: 'MONTO_TOTAL'},	
			{	name: 'SALDO_LINEA'},			
			{ name: 'ACEPTADOS',	convert: NE.util.string2boolean },
			{ name: 'RECHAZADOS', convert: NE.util.string2boolean },
			{	name: 'FN_FINANCIAMIENTO'},
			{ name:'MODALIDAD' }
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaValidosData,	
				beforeLoad:	{
				fn: function(store, options){
					Ext.apply(options.params, {						
						aceptados:Ext.getCmp('aceptados').getValue()
					 });
				}
			},
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConsultaValidosData(null, null, null);						
				}
			}
		}			
	});
	
	var gridValidados = new Ext.grid.EditorGridPanel({	
		store: consultaValidadosData,
		id: 'gridValidados',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'REGISTRO VALIDO-UNIVERSIDAD',	
		clicksToEdit: 1,
		hidden: true,
		columns: [	
			{
				header: 'Fecha y Hora de Carga',
				tooltip: 'Fecha y Hora de Carga',
				dataIndex: 'FECHA_CARGA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Cr�dito',
				tooltip: 'Cr�dito',
				dataIndex: 'CREDITO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Nombre del acreditado',
				tooltip: 'Nombre del acreditado',
				dataIndex: 'NOMBRE_ACREDITADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Matricula',
				tooltip: 'Matricula',
				dataIndex: 'MATRICULA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Estudios',
				tooltip: 'Estudios',
				dataIndex: 'ESTUDIOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Ingreso',
				tooltip: 'Fecha de Ingreso',
				dataIndex: 'FECHA_INGRESO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'A�o de colocaci�n',
				tooltip: 'A�o de colocaci�n',
				dataIndex: 'ANIO_COLOCACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Periodo Educativo',
				tooltip: 'Periodo Educativo',
				dataIndex: 'PERIODO_EDUCATIVO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Modalidad Programa',
				tooltip: 'Modalidad Programa',
				dataIndex: 'MODALIDAD_PROGRAMA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha �ltima disposici�n',
				tooltip: 'Fecha �ltima disposici�n',
				dataIndex: 'FECHA_UL_DISPOSICION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. de disposici�n',
				tooltip: 'No. de disposici�n',
				dataIndex: 'NO_DISPOSICION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Importe Disposici�n',
				tooltip: 'Importe Disposici�n',
				dataIndex: 'IMPORTE_DISPOSICON',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: 'Monto Total',
				tooltip: 'Monto Total',
				dataIndex: 'MONTO_TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Saldo de L�nea',
				tooltip: 'Saldo de L�nea',
				dataIndex: 'SALDO_LINEA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{						
				header:'Modalidad',
				tooltip: 'MODALIDAD',
				dataIndex : 'MODALIDAD',
				width : 150,
				align: 'center',
				sortable : false			
			}
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacionA',
			displayInfo: true,
			store: consultaValidadosData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArcValidados',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacionA");
						
							Ext.Ajax.request({
							url: 'validaCargaReg.data.jsp',
							params: {
								informacion: 'ArchivoCSValidados',
								estatus:Ext.getCmp("estatusAcep").getValue(),
								aceptados:Ext.getCmp('aceptados').getValue(),
								start: barraPaginacionA.cursor,
								limit: barraPaginacionA.pageSize,
								acuse:Ext.getCmp("acuse").getValue()							
							},
							callback: procesarArValidados
						});						
					}
				}				
			]
		}
	});
	

//------------Consulta de totales ----------------------------------------


	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {	
				
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : 'validaCargaReg.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [		
			{	name: 'NO_REGISTROS'},	
			{	name: 'IMPORTE_DISPOSICION'},	
			{	name: 'MONTO_TOTAL'},	
			{	name: 'SALDO_LINEA'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);					
				}
			}
		}		
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Totales',	
		align: 'left',
		hidden: true,
		columns: [	
			{
				header: 'N�mero de Registros',
				tooltip: 'N�mero de Registros',
				dataIndex: 'NO_REGISTROS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Importe Disposici�n',
				tooltip: 'Importe Disposici�n',
				dataIndex: 'IMPORTE_DISPOSICION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},	
			{
				header: 'Monto Total',
				tooltip: 'Monto Total',
				dataIndex: 'MONTO_TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: 'S�ldo L�nea',
				tooltip: 'S�ldo L�nea',
				dataIndex: 'SALDO_LINEA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			}		
		],	
		stripeRows: true,
		loadMask: true,
		height: 150,
		width: 620,		
		frame: true	
	});
	
	//------------Consulta Generarl ----------------------------------------
	
	function procesarArchivoCons(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var datosUni = new Ext.Container({
		layout: 'table',		
		id: 'datosUni',	
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{
				xtype: 'displayfield',
				id: 'universisad',
				style: 'text-align:left;',
				fieldLabel: 'NOMBRE',
				text: ''
			},	
			{
				xtype: 'displayfield',
				id: 'campus',
				style: 'text-align:left;',
				fieldLabel: 'NOMBRE',
				text: ''
			},		
			{
				xtype: 'displayfield',
				id: 'nombreIF',
				style: 'text-align:left;',
				fieldLabel: 'NOMBRE',
				text: ''
			}	
		]
	});
				
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;
			Ext.getCmp('universisad').setValue(jsonData.universisad);
			Ext.getCmp('campus').setValue(jsonData.campus);
			Ext.getCmp('nombreIF').setValue(jsonData.nombreIF);
			Ext.getCmp('monitor').setValue(jsonData.monitor);
			Ext.getCmp('no_disposicion1').setValue(jsonData.no_disposicion);
			
			consultaTotalesData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					estatus: '1',
					intermediario:jsonData.intermediario
				})
			});
			
			if(store.getTotalCount() > 0) {			
				el.unmask();	
				Ext.getCmp('btnArchivoCons').enable();
				Ext.getCmp('btnContinuar').enable();
			} else {		
				Ext.getCmp('btnArchivoCons').disable();
				Ext.getCmp('btnContinuar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : 'validaCargaReg.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'IC_REGISTRO'},
			{	name: 'IC_IF'},
			{	name: 'INTERMEDIARIO'},
			{	name: 'FECHA_CARGA'},	
			{	name: 'CREDITO'},	
			{	name: 'NOMBRE_ACREDITADO'},	
			{	name: 'RFC'},	
			{	name: 'MATRICULA'},	
			{	name: 'ESTUDIOS'},	
			{	name: 'FECHA_INGRESO'},	
			{	name: 'ANIO_COLOCACION'},	
			{	name: 'PERIODO_EDUCATIVO'},	
			{	name: 'MODALIDAD_PROGRAMA'},	
			{	name: 'FECHA_UL_DISPOSICION'},	
			{	name: 'NO_DISPOSICION'},	
			{	name: 'IMPORTE_DISPOSICON'},	
			{	name: 'MONTO_TOTAL'},	
			{	name: 'SALDO_LINEA'},
			{ name: 'ACEPTADOS',	convert: NE.util.string2boolean },
			{ name: 'RECHAZADOS', convert: NE.util.string2boolean },		
			{	name: 'CAUSA_RECHAZO'},
			{	name: 'FN_FINANCIAMIENTO'},
			{	name: 'MODALIDAD'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
		
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Fecha y Hora de Carga',
				tooltip: 'Fecha y Hora de Carga',
				dataIndex: 'FECHA_CARGA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Cr�dito',
				tooltip: 'Cr�dito',
				dataIndex: 'CREDITO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Nombre del acreditado',
				tooltip: 'Nombre del acreditado',
				dataIndex: 'NOMBRE_ACREDITADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Matricula',
				tooltip: 'Matricula',
				dataIndex: 'MATRICULA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Estudios',
				tooltip: 'Estudios',
				dataIndex: 'ESTUDIOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Ingreso',
				tooltip: 'Fecha de Ingreso',
				dataIndex: 'FECHA_INGRESO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'A�o de colocaci�n',
				tooltip: 'A�o de colocaci�n',
				dataIndex: 'ANIO_COLOCACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Periodo Educativo',
				tooltip: 'Periodo Educativo',
				dataIndex: 'PERIODO_EDUCATIVO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Modalidad Programa',
				tooltip: 'Modalidad Programa',
				dataIndex: 'MODALIDAD_PROGRAMA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha �ltima disposici�n',
				tooltip: 'Fecha �ltima disposici�n',
				dataIndex: 'FECHA_UL_DISPOSICION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. de disposici�n',
				tooltip: 'No. de disposici�n',
				dataIndex: 'NO_DISPOSICION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Importe Disposici�n',
				tooltip: 'Importe Disposici�n',
				dataIndex: 'IMPORTE_DISPOSICON',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total',
				tooltip: 'Monto Total',
				dataIndex: 'MONTO_TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: 'Saldo de L�nea',
				tooltip: 'Saldo de L�nea',
				dataIndex: 'SALDO_LINEA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{						
				header:'Modalidad',
				tooltip: 'MODALIDAD',
				dataIndex : 'MODALIDAD',
				width : 150,
				align: 'center',
				sortable : false			
			},			
			{
				xtype: 'checkcolumn',				
				header:'<input type="checkbox" name="chek"  id="todoAcepta"  checked=true;  onclick="selecDeselec(this);"/> Valido',
				tooltip: 'Valido',
				dataIndex : 'ACEPTADOS',
				width : 150,
				align: 'center',
				sortable : false			
			},			
			{
				xtype: 'checkcolumn',
				header : '<input type="checkbox" name="chek"  id="todoRechaza" onclick="selecDeselec(this);"/> Rechazado',
				tooltip: 'Rechazado',
				dataIndex : 'RECHAZADOS',
				width : 150,
				align: 'center',
				sortable : false				 
			},				
			{
				header : 'Ingresar causas de rechazo', 
				tooltip: 'Ingresar causas de rechazo',
				dataIndex : 'CAUSA_RECHAZO', 
				fixed:true,
				sortable : false,	
				width : 300,
				hiddeable: false,
				editor:{
					xtype:'textarea',
					id: 'txtArea',
					maxLength: 260,
					enableKeyEvents: true,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 1000){
								field.setValue((field.getValue()).substring(0,999));
								Ext.Msg.alert('Observaciones','La causas de rechazo no debe sobrepasar los 1000 caracteres.');								
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			}		
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
					
				if(campo == 'CAUSA_RECHAZO') {					
					if(record.data['RECHAZADOS']==true ) {					
						return true;		
					}				
					if(record.data['ACEPTADOS']==true  || record.data['RECHAZADOS'] ==false ){
						record.data['CAUSA_RECHAZO']='';
						record.commit();	
						return false;		
					}
				}								
			}					
		},
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCons',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: 'validaCargaReg.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSVCons',
							estatus:'1'
							}),
							callback: procesarArchivoCons
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Continuar',					
					tooltip:	'Continuaro ',
					iconCls: 'icoContinuar',
					id: 'btnContinuar',
					handler: procesarContinuar
				}		
			]
		}
	});

	gridConsulta.getStore().on('update', function(store, record, action){
		if (action == Ext.data.Record.EDIT) {
			var dato = record.getChanges();	
			
			if(dato.ACEPTADOS	&&	record.get('RECHAZADOS')){
				record.set('RECHAZADOS', false);
				record.set('CAUSA_RECHAZO', '');
			}else if(dato.RECHAZADOS 	&&	record.get('ACEPTADOS')){
				record.set('ACEPTADOS', false);
			}
			record.commit();
		}
	});

		
// -------------------Criterios de Busqueda--------------------

	
	 leyenda1 = '<table width="700" border="0">'+
		'<tr><td class="formas" style="text-align: justify;">'+
		'Al transmitir los registros electr�nicamente, usted est� confirmando la validez de la informaci�n y acepta los t�rminos relacionados con la operaci�n del Programa Nacional de Financiamiento a la Educaci�n Superior. '+
		'</td></tr></table>';
		
	var leyendaMostrar = new Ext.Container({
		layout: 'table',		
		id: 'leyendaMostrar',	
		style: 'margin:0 auto;',		
		layoutConfig: {
			columns: 1
		},
		width:	'700',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		leyenda1, 
				cls:		'x-form-item'
			}						
		]
	});
	
	var procesarCatalogoIFData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){			
			var jsonData = store.reader.jsonData;
			var ic_if = jsonData.intermediario;		
			var intermediario1 = Ext.getCmp('intermediario1');
			if(intermediario1.getValue()==''){
				intermediario1.setValue(ic_if);
			}			
		}
  }
  
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : 'validaCargaReg.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoIFData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	var catalogoModalidad = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			["1",'Primeras Perdidas - 1ra disposici�n y/o PariPasu'],
			["2",'Primeras Perdidas Disposiciones correlativas a la 1ra']			
		 ]
	});
	
	var  elementosForma  = [	
		{ 	xtype: 'textfield',  hidden: true, id: 'monitor', 	value: '' },	
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			name: 'intermediario',
			id: 'intermediario1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'intermediario',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIF,
			tpl : NE.util.templateMensajeCargaCombo
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Carga del Archivo',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'fechaCargaIni',
					id: 'fechaCargaIni',
					allowBlank: true,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaCargaFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaCargaFin',
					id: 'fechaCargaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 					
					campoInicioFecha: 'fechaCargaIni',
					margins: '0 20 0 0' 
				}
			]
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Cr�dito ',		
			name: 'credito',
			id: 'credito1',
			allowBlank: true,
			maxLength: 30,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Matricula ',		
			name: 'matricula',
			id: 'matricula1',
			allowBlank: true,
			maxLength: 20,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Estudios ',		
			name: 'estudios',
			id: 'estudios1',
			allowBlank: true,
			maxLength: 40,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha ingreso del Acreditado',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaAcreditadoIni',
					id: 'fechaAcreditadoIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaAcreditadoFin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaAcreditadoFin',
					id: 'fechaAcreditadoFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fechaAcreditadoIni',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'A�o de colocaci�n ',		
			name: 'anio',
			id: 'anio1',
			allowBlank: true,
			maxLength: 4,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'Periodo Educativo',		
			name: 'periodo',
			id: 'periodo1',
			allowBlank: true,
			maxLength: 10,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Modalidad del Programa',	
			name: 'modalidad',
			id: 'modalidad1',
			allowBlank: true,
			maxLength: 400,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha �ltima disposici�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaDisposicionIni',
					id: 'fechaDisposicionIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaDisposicionFin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaDisposicionFin',
					id: 'fechaDisposicionFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'fechaDisposicionIni',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'No. Disposici�n',
			name: 'no_disposicion',
			id: 'no_disposicion1',
			allowBlank: true,
			maxLength: 30,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'Importe Disposici�n',
			name: 'impDisposicion',
			id: 'impDisposicion1',
			allowBlank: true,
			maxLength: 22,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'		
		},		
		{
			xtype: 'numberfield',
			fieldLabel: 'Monto Total',
			name: 'montoTotal',
			id: 'montoTotal1',
			allowBlank: true,
			maxLength: 22,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		},		
		{
			xtype: 'numberfield',
			fieldLabel: 'Saldo L�nea',
			name: 'saldoLinea',
			id: 'saldoLinea1',
			allowBlank: true,
			maxLength: 22,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		},
		{
			xtype: 'combo',
			fieldLabel: 'Modalidad',
			name: 'modalidadP',
			id: 'modalidadP1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'modalidadP',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			forceSelection : true,
			store: catalogoModalidad			
		}
	];


	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Validaci�n registros Cr�dito Educativo',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {					
				
					var intermediario = Ext.getCmp("intermediario1");
					var fechaCargaIni = Ext.getCmp('fechaCargaIni');
					var fechaCargaFin = Ext.getCmp('fechaCargaFin');
					var fechaAcreditadoIni = Ext.getCmp('fechaAcreditadoIni');
					var fechaAcreditadoFin = Ext.getCmp('fechaAcreditadoFin');
					var fechaDisposicionIni = Ext.getCmp('fechaDisposicionIni');
					var fechaDisposicionFin = Ext.getCmp('fechaDisposicionFin');
					
					var modalidadP = Ext.getCmp('modalidadP1');
					
					
					if (Ext.isEmpty(intermediario.getValue()) ){
						intermediario.markInvalid('El campo es obigatorio');
						return;
					}
					if (Ext.isEmpty(modalidadP.getValue()) ){
						modalidadP.markInvalid('El campo es obigatorio');
						return;
					}
					
					if(!Ext.isEmpty(fechaCargaIni.getValue()) || !Ext.isEmpty(fechaCargaFin.getValue())){
						if(Ext.isEmpty(fechaCargaIni.getValue())){
							fechaCargaIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaCargaIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaCargaFin.getValue())){
							fechaCargaFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaCargaFin.focus();
							return;
						}
					}
					
					if(!Ext.isEmpty(fechaAcreditadoIni.getValue()) || !Ext.isEmpty(fechaAcreditadoFin.getValue())){
						if(Ext.isEmpty(fechaAcreditadoIni.getValue())){
							fechaAcreditadoIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaAcreditadoIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaAcreditadoFin.getValue())){
							fechaAcreditadoFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaAcreditadoFin.focus();
							return;
						}
					}
					
					if(!Ext.isEmpty(fechaDisposicionIni.getValue()) || !Ext.isEmpty(fechaDisposicionFin.getValue())){
						if(Ext.isEmpty(fechaDisposicionIni.getValue())){
							fechaDisposicionIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaDisposicionIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaDisposicionFin.getValue())){
							fechaDisposicionFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaDisposicionFin.focus();
							return;
						}
					}
					
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							estatus: '1',
							monitor:'N',
							intermediario:intermediario.getValue()
						})
					});
					
					consultaTotalesData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							estatus: '1',
							informacion: 'ConsultarTotales',
							intermediario:intermediario.getValue()
						})
					});
			
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = 'validaCargaReg.jsp';					
				}
			}
		]
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			mensajeAutorizacion,
			NE.util.getEspaciador(20),
			fp,
			gridCifrasControl,
			NE.util.getEspaciador(20),
			gridControl,
			gridControlPre,
			NE.util.getEspaciador(20),
			leyendaMostrar,
			NE.util.getEspaciador(20),
			fpBotones,			
			datosUni,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridValidados,						
			NE.util.getEspaciador(20),
			gridTotales,
			gridRechazados,
			NE.util.getEspaciador(20)			
		]
	});
	
	catalogoIF.load({
		params: {
			intermediario:ic_if			
		}	
	});
	
	consultaData.load({
		params: Ext.apply(fp.getForm().getValues(),{
			estatus: '1',
			intermediario:ic_if,
			no_disposicion:no_disposicion,
			monitor:'S'
		})
	});
	
	
});	
