Ext.onReady(function(){

	var campoFechas = new Ext.form.DateField({
		startDay: 0
	});

	var ic_if =  Ext.getDom('ic_if').value;
	var strPerfil = '';

	function inicializar(){
		Ext.Ajax.request({
			url: '37ConsultaCreditos.data.jsp',
			params: Ext.apply({
				informacion: 'Inicializar'
			}),
			callback: procesarInicializar
		});
	}

	var procesarInicializar = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){ 
			Ext.util.JSON.decode(response.responseText)
			strPerfil = Ext.util.JSON.decode(response.responseText).strPerfil;
			if(strPerfil == 'ADMIN GARANT'){
				Ext.getCmp('cmbUniversidad1').show();
				Ext.getCmp('intermediario1').show();
				catalogoIF.load({
					params: {
						intermediario:ic_if			
					}	
				});
			} else if(strPerfil == 'ADMIN IF GARANT'){
				Ext.getCmp('cmbUniversidad1').show();
				Ext.getCmp('intermediario1').hide();
				catalogoUniversidad.load({
					params: Ext.apply({
						intermediario: ''
					})
				});
			} else{
				Ext.getCmp('cmbUniversidad1').hide();
				Ext.getCmp('intermediario1').show();
				catalogoIF.load({
					params: {
						intermediario:ic_if			
					}	
				});
			}
		}
  }

	// boton para Transmitir  de Credito Electronico a  Garantias
	function verificaFechas(fec, fec2){
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue() != '' || fecha2.getValue() != ''){
			if(fecha1.getValue() == ''){
				fecha1.markInvalid('Ambas Fechas son necesarias');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue() == ''){
				fecha2.markInvalid('Ambas Fechas son necesarias');
				fecha2.focus();
				return false;
			}
		}
		return true;
	}	

	var datosUni = new Ext.Container({
		layout:        'table',
		id:            'datosUni',
		style:         'margin:0 auto;',
		layoutConfig: {
			columns:    1
		},
		width:         900,
		heigth:        'auto',
		style:         'margin:0 auto;',
		items: [{
			xtype:      'displayfield',
			id:         'universisad',
			style:      'text-align:left;',
			fieldLabel: 'NOMBRE',
			text:       ''
		},{
			xtype:      'displayfield',
			id:         'campus',
			style:      'text-align:left;',
			fieldLabel: 'NOMBRE',
			text:       ''
		},{
			xtype:      'displayfield',
			id:         'nombreIF',
			style:      'text-align:left;',
			fieldLabel: 'NOMBRE',
			text:       ''
		}]
	});

	function procesarArchivoSuccess(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin', '');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnArchivo');
			boton.setIconClass('icoXls');
			boton.enable();
		} else{
			NE.util.mostrarConnError(response, opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var boton=Ext.getCmp('btnArchivo');		
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null){
			if (!gridConsulta.isVisible()){
				gridConsulta.show();
			}

			var jsonData = store.reader.jsonData;
			Ext.getCmp('universisad').setValue(jsonData.universisad);
			Ext.getCmp('campus').setValue(jsonData.campus);
			Ext.getCmp('nombreIF').setValue(jsonData.nombreIF);

			boton.enable();
			if(store.getTotalCount() > 0){
				el.unmask();
			} else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
				boton.disable();
			}
		}
	}

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '37ConsultaCreditos.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{ name: 'IC_REGISTRO'},
			{ name: 'IC_IF'},
			{ name: 'INTERMEDIARIO'},
			{ name: 'FECHA_CARGA'},
			{ name: 'FOLIO',type: 'String'},
			{ name: 'CREDITO'},
			{ name: 'NOMBRE_ACREDITADO'},
			{ name: 'RFC'},
			{ name: 'MATRICULA'},
			{ name: 'ESTUDIOS'},
			{ name: 'FECHA_INGRESO'},
			{ name: 'ANIO_COLOCACION'},
			{ name: 'PERIODO_EDUCATIVO'},
			{ name: 'MODALIDAD_PROGRAMA'},
			{ name: 'FECHA_UL_DISPOSICION'},
			{ name: 'NO_DISPOSICION'},
			{ name: 'IMPORTE_DISPOSICON'},
			{ name: 'MONTO_TOTAL'},
			{ name: 'SALDO_LINEA'},
			{ name: 'MONTO_GARANTIA'},
			{ name: 'ESTATUS'},
			{ name: 'CAUSA_RECHAZO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var gridConsulta = new Ext.grid.GridPanel({
		width:        900,
		height:       400,
		id:           'gridConsulta',
		title:        'Consulta',
		align:        'center',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		emptyMsg:     'No hay registros.',
		loadMask:     true,
		stripeRows:   true,
		frame:        false,
		hidden:       true,
		displayInfo:  true,
		store:        consultaData,
		columns: [{
			width:     150,
			header:    'Fecha y Hora de Carga',
			tooltip:   'Fecha y Hora de Carga',
			dataIndex: 'FECHA_CARGA',
			align:     'left',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Folio de Operaci�n',
			tooltip:   'Folio de Operaci�n',
			dataIndex: 'FOLIO'
		},{
			width:     150,
			header:    'Cr�dito',
			tooltip:   'Cr�dito',
			dataIndex: 'CREDITO',
			align:     'left',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Nombre del acreditado',
			tooltip:   'Nombre del acreditado',
			dataIndex: 'NOMBRE_ACREDITADO',
			align:     'left',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'RFC',
			tooltip:   'RFC',
			dataIndex: 'RFC',
			align:     'center',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Matricula',
			tooltip:   'Matricula',
			dataIndex: 'MATRICULA',
			align:     'center',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Estudios',
			tooltip:   'Estudios',
			dataIndex: 'ESTUDIOS',
			align:     'center',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Fecha de Ingreso',
			tooltip:   'Fecha de Ingreso',
			dataIndex: 'FECHA_INGRESO',
			align:     'center',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'A�o de colocaci�n',
			tooltip:   'A�o de colocaci�n',
			dataIndex: 'ANIO_COLOCACION',
			align:     'center',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Periodo Educativo',
			tooltip:   'Periodo Educativo',
			dataIndex: 'PERIODO_EDUCATIVO',
			align:     'center',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Modalidad Programa',
			tooltip:   'Modalidad Programa',
			dataIndex: 'MODALIDAD_PROGRAMA',
			align:     'center',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Fecha �ltima disposici�n',
			tooltip:   'Fecha �ltima disposici�n',
			dataIndex: 'FECHA_UL_DISPOSICION',
			align:     'center',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'No. de disposici�n',
			tooltip:   'No. de disposici�n',
			dataIndex: 'NO_DISPOSICION',
			align:     'center',
			sortable:  true,
			resizable: true
		},{
			width:     150,
			header:    'Importe Disposici�n',
			tooltip:   'Importe Disposici�n',
			dataIndex: 'IMPORTE_DISPOSICON',
			align:     'right',
			sortable:  true,
			resizable: true,
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			width:     150,
			header:    'Monto Total',
			tooltip:   'Monto Total',
			dataIndex: 'MONTO_TOTAL',
			align:     'right',
			sortable:  true,
			resizable: true,
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			width:     150,
			header:    'Saldo de L�nea',
			tooltip:   'Saldo de L�nea',
			dataIndex: 'SALDO_LINEA',
			align:     'right',
			sortable:  true,
			resizable: true,
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			width:     150,
			header:    'Monto de Contragarant�a a Pagar',
			tooltip:   'Monto de Contragarant�a a Pagar',
			dataIndex: 'MONTO_GARANTIA',
			align:     'right',
			sortable:  true,
			resizable: true,
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			width:     130,
			header:    '<center>Estatus</center>',
			tooltip:   'Estatus',
			dataIndex: 'ESTATUS',
			align:     'left',
			sortable:  true,
			renderer:  function(value, metadata, record, rowindex, colindex, store){
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				return value;
			}
		},{
			width:     130,
			align:     'center',
			header:    'Causas del Rechazo',
			tooltip:   'Causas del Rechazo',
			dataIndex: 'CAUSA_RECHAZO',
			sortable:  true,
			renderer:  function(value, metadata, record, rowindex, colindex, store){
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				if(value=='')
					return 'N/A';
				else
					return value;
			}
		}],
		bbar: {
			pageSize:    15,
			xtype:       'paging',
			buttonAlign: 'left',
			id:          'barraPaginacion',
			displayMsg:  '{0} - {1} de {2}',
			emptyMsg:    'No hay registros.',
			displayInfo: true,
			store:       consultaData,
			items: [
				'->','-',
				{
					xtype:   'button',
					id:      'btnArchivo',
					text:    'Descargar Archivo',
					tooltip: 'Descargar Archivo ',
					iconCls: 'icoXls',
					handler: function(boton, evento){
						var barraPaginacionR = Ext.getCmp('barraPaginacion');
						var intermediario = Ext.getCmp('intermediario1');
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '37ConsultaCreditos.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							intermediario:intermediario.getValue(),
							informacion: 'ArchivoCSV'
						}),
							callback: procesarArchivoSuccess
						});
					}
				}
			]
		}
	});

// -------------------Criterios de Busqueda--------------------
	var procesarCatalogoIFData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = store.reader.jsonData;
			var ic_if = jsonData.intermediario;
			var intermediario1 = Ext.getCmp('intermediario1');
			if(intermediario1.getValue()==''){
				intermediario1.setValue(ic_if);
			}
		}
  }

	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave', 'descripcion','loadMsg'],
		url: '37ConsultaCreditos.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoIFData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoUniversidad = new Ext.data.JsonStore({
		id: 'catalogoUniversidad',
		root: 'registros',
		fields: ['clave', 'descripcion','loadMsg'],
		url: '37ConsultaCreditos.data.jsp',
		baseParams: {
			informacion: 'CatalogoUniversidad'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var elementosForma = [{
		width:               500,
		xtype:               'combo',
		fieldLabel:          'Intermediario Financiero',
		id:                  'intermediario1',
		name:                'intermediario',
		hiddenName:          'intermediario',
		mode:                'local',
		displayField:        'descripcion',
		valueField:          'clave',
		emptyText:           'Seleccionar ...',
		triggerAction:       'all',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		forceSelection:      true,
		autoLoad:            false,
		typeAhead:           true,
		hidden:              true,
		minChars:            1,
		store:               catalogoIF,
		tpl:                 NE.util.templateMensajeCargaCombo,
		listeners: {
			select: function(combo, record, index){
				if((strPerfil == 'ADMIN GARANT' || strPerfil == 'ADMIN IF GARANT') && combo.getValue() != ''){
					Ext.getCmp('cmbUniversidad1').setValue('');
					catalogoUniversidad.load({
						params: Ext.apply({
							intermediario: combo.getValue()
						})
					});
				}
			}
		}
	},{
		width:               500,
		xtype:               'combo',
		fieldLabel:          'Universidad',
		id:                  'cmbUniversidad1',
		name:                'cmbUniversidad',
		hiddenName:          'cmbUniversidad',
		emptyText:           'Seleccionar ...',
		displayField:        'descripcion',
		valueField:          'clave',
		mode:                'local',
		triggerAction:       'all',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		hidden:              false,
		forceSelection:      true,
		typeAhead:           true,
		minChars:            1,
		store:               catalogoUniversidad,
		tpl:                 NE.util.templateMensajeCargaCombo
	},{
		width:               500,
		xtype:               'textfield',
		id:                  'folio',
		name:                'folio',
		fieldLabel:          'Folio Operaci�n',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		maxLength:           30
	},{
		width:               500,
		xtype:               'textfield',
		fieldLabel:          'Cr�dito',
		id:                  'credito1',
		name:                'credito',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		allowBlank:          true,
		maxLength:           30
	},{
		width:               500,
		xtype:               'textfield',
		fieldLabel:          'Matricula',
		id:                  'matricula1',
		name:                'matricula',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		allowBlank:          true,
		maxLength:           20
	},{
		width:               500,
		xtype:               'textfield',
		fieldLabel:          'Estudios',
		id:                  'estudios1',
		name:                'estudios',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		allowBlank:          true,
		maxLength:           40
	},{
		xtype:               'compositefield',
		fieldLabel:          'Fecha ingreso del Acreditado',
		msgTarget:           'side',
		combineErrors:       false,
		items: [{
			width:            100,
			xtype:            'datefield',
			id:               'fechaAcreditadoIni',
			name:             'fechaAcreditadoIni',
			msgTarget:        'side',
			vtype:            'rangofecha',
			minValue:         '01/01/1901',
			campoFinFecha:    'fechaAcreditadoFin',
			margins:          '0 20 0 0', // necesario para mostrar el icono de error
			allowBlank:       true,
			startDay:         0
		},{
			width:            20,
			xtype:            'displayfield',
			value:            'al'
		},{
			width:            100,
			xtype:            'datefield',
			id:               'fechaAcreditadoFin',
			name:             'fechaAcreditadoFin',
			msgTarget:        'side',
			vtype:            'rangofecha',
			minValue:         '01/01/1901',
			campoInicioFecha: 'fechaAcreditadoIni',
			margins:          '0 20 0 0', // necesario para mostrar el icono de error
			startDay:         1,
			allowBlank:       true
		}]
	},{
		width:               500,
		xtype:               'bigdecimal',
		fieldLabel:          'A�o de colocaci�n ',
		id:                  'anio1',
		name:                'anio',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		maxValue:            '9999',
		blankText:           'El tama�o m�ximo del campos es de 4',
		allowDecimals:       false,
		allowNegative:       false,
		allowBlank:          true
	},{
		width:               500,
		xtype:               'bigdecimal',
		fieldLabel:          'Periodo Educativo',
		name:                'periodo',
		id:                  'periodo1',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		maxValue:            '9999999999',
		blankText:           'El tama�o m�ximo del campos es de 10',
		allowDecimals:       false,
		allowNegative:       false,
		allowBlank:          true
	},{
		width:               500,
		xtype:               'textfield',
		fieldLabel:          'Modalidad del Programa',
		name:                'modalidad',
		id:                  'modalidad1',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		maxLength:           400,
		allowBlank:          true
	},{
		xtype:               'compositefield',
		fieldLabel:          'Fecha �ltima disposici�n',
		msgTarget:           'side',
		combineErrors:       false,
		items: [{
			width:            100,
			xtype:            'datefield',
			name:             'fechaDisposicionIni',
			id:               'fechaDisposicionIni',
			msgTarget:        'side',
			vtype:            'rangofecha',
			minValue:         '01/01/1901',
			campoFinFecha:    'fechaDisposicionFin',
			margins:          '0 20 0 0', // necesario para mostrar el icono de error
			startDay:         0,
			allowBlank:       true
		},{
			width:            20,
			xtype:            'displayfield',
			value:            'al'
		},{
			width:            100,
			xtype:            'datefield',
			name:             'fechaDisposicionFin',
			id:               'fechaDisposicionFin',
			msgTarget:        'side',
			minValue:         '01/01/1901',
			vtype:            'rangofecha',
			campoInicioFecha: 'fechaDisposicionIni',
			margins:          '0 20 0 0', // necesario para mostrar el icono de error
			startDay:         1,
			allowBlank:       true
		}]
	},{
		width:               500,
		xtype:               'bigdecimal',
		fieldLabel:          'No. Disposici�n',
		name:                'no_disposicion',
		id:                  'no_disposicion1',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		maxValue:            '999999999999999999999999999999',
		blankText:           'El tama�o m�ximo del campos es de 30',
		allowDecimals:       false,
		allowNegative:       false,
		allowBlank:          true
	},{
		width:               500,
		xtype:               'bigdecimal',
		fieldLabel:          'Importe Disposici�n',
		name:                'impDisposicion',
		id:                  'impDisposicion1',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		maxValue:            '99999999999999999.99',
		blankText:           'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
		allowDecimals:       true,
		allowNegative:       false,
		allowBlank:          true
	},{
		width:               500,
		xtype:               'bigdecimal',
		fieldLabel:          'Monto Total',
		name:                'montoTotal',
		id:                  'montoTotal1',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		maxValue:            '99999999999999999.99',
		blankText:           'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
		allowDecimals:       true,
		allowNegative:       false,
		allowBlank:          true
	},{
		width:               500,
		xtype:               'bigdecimal',
		fieldLabel:          'Saldo L�nea',
		name:                'saldoLinea',
		id:                  'saldoLinea1',
		msgTarget:           'side',
		margins:             '0 20 0 0',
		maxValue:            '9999999999999999999999.99',
		blankText:           'El tama�o m�ximo del campos es de 22 enteros 2 decimales',
		allowDecimals:       true,
		allowNegative:       false,
		allowBlank:          true
	}];

	var fp = new Ext.form.FormPanel({
		width:        700,
		labelWidth:   150,	
		id:           'forma',
		title:        'Validaci�n registros Cr�dito Educativo',
		layout:       'form',
		style:        'margin:0 auto;',
		bodyStyle:    'padding: 6px',
		defaultType:  'textfield',
		defaults: {
			msgTarget: 'side',
			anchor:    '-20'
		},
		frame:        true,
		collapsible:  true,
		titleCollapse:false,
		items:        elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento){				

					var intermediario = Ext.getCmp("intermediario1");
					var fechaAcreditadoIni = Ext.getCmp('fechaAcreditadoIni');
					var fechaAcreditadoFin = Ext.getCmp('fechaAcreditadoFin');
					var fechaDisposicionIni = Ext.getCmp('fechaDisposicionIni');
					var fechaDisposicionFin = Ext.getCmp('fechaDisposicionFin');

					/***** Se validan los combos *****/
					if ((strPerfil == 'ADMIN UNIV' || strPerfil == 'ADMIN GARANT') && Ext.isEmpty(intermediario.getValue())){
						intermediario.markInvalid('El campo es obigatorio');
						return;
					}
					if ((strPerfil == 'ADMIN GARANT' || strPerfil == 'ADMIN IF GARANT') && Ext.getCmp('cmbUniversidad1').getValue() == ''){
						Ext.getCmp('cmbUniversidad1').markInvalid('El campo es obigatorio');
						return;
					}
					/***** Se validan las fechas *****/
					if((Ext.getCmp('fechaAcreditadoIni').getValue() == '' && Ext.getCmp('fechaAcreditadoFin').getValue() != '') || 
						(Ext.getCmp('fechaAcreditadoIni').getValue() != '' && Ext.getCmp('fechaAcreditadoFin').getValue() == '')){
						Ext.getCmp('fechaAcreditadoIni').markInvalid('Debe ingresar ambas fechas');
						Ext.getCmp('fechaAcreditadoFin').markInvalid('Debe ingresar ambas fechas');
						return;
					}
					if((Ext.getCmp('fechaDisposicionIni').getValue() == '' && Ext.getCmp('fechaDisposicionFin').getValue() != '') || 
						(Ext.getCmp('fechaDisposicionIni').getValue() != '' && Ext.getCmp('fechaDisposicionFin').getValue() == '')){
						Ext.getCmp('fechaDisposicionIni').markInvalid('Debe ingresar ambas fechas');
						Ext.getCmp('fechaDisposicionFin').markInvalid('Debe ingresar ambas fechas');
						return;
					}
					if( Ext.getCmp('forma').getForm().isValid()){
						fp.el.mask('Enviando...', 'x-mask-loading');
						consultaData.load({
							params: Ext.apply(fp.getForm().getValues(),{
								intermediario:intermediario.getValue(),
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15
							})
						});
					}
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '37ConsultaCreditos.jsp';					
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [

			NE.util.getEspaciador(20),
			fp,
			datosUni,
			NE.util.getEspaciador(20),
			gridConsulta

			
		]
	});

	inicializar();	

});	
