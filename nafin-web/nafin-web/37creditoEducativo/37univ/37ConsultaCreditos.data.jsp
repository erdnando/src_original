<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,
	java.math.*,
	java.text.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,
	com.netro.model.catalogos.*,
	com.netro.educativo.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/37creditoEducativo/37secsession_extjs.jspf"%>

<%
String informacion         = (request.getParameter("informacion")         !=null )?request.getParameter("informacion"):"";
String intermediario       = (request.getParameter("intermediario")       !=null )?request.getParameter("intermediario"):"";
String fechaCargaIni       = (request.getParameter("fechaCargaIni")       !=null )?request.getParameter("fechaCargaIni"):"";
String fechaCargaFin       = (request.getParameter("fechaCargaFin")       !=null )?request.getParameter("fechaCargaFin"):"";
String credito             = (request.getParameter("credito")             !=null )?request.getParameter("credito"):"";
String matricula           = (request.getParameter("matricula")           !=null )?request.getParameter("matricula"):"";
String estudios            = (request.getParameter("estudios")            !=null )?request.getParameter("estudios"):"";
String fechaAcreditadoIni  = (request.getParameter("fechaAcreditadoIni")  !=null )?request.getParameter("fechaAcreditadoIni"):"";
String fechaAcreditadoFin  = (request.getParameter("fechaAcreditadoFin")  !=null )?request.getParameter("fechaAcreditadoFin"):"";
String anio                = (request.getParameter("anio")                !=null )?request.getParameter("anio"):"";
String periodo             = (request.getParameter("periodo")             !=null )?request.getParameter("periodo"):"";
String modalidad           = (request.getParameter("modalidad")           !=null )?request.getParameter("modalidad"):"";
String fechaDisposicionIni = (request.getParameter("fechaDisposicionIni") !=null )?request.getParameter("fechaDisposicionIni"):"";
String fechaDisposicionFin = (request.getParameter("fechaDisposicionFin") !=null )?request.getParameter("fechaDisposicionFin"):"";
String no_disposicion      = (request.getParameter("no_disposicion")      !=null )?request.getParameter("no_disposicion"):"";
String impDisposicion      = (request.getParameter("impDisposicion")      !=null )?request.getParameter("impDisposicion"):"";
String montoTotal          = (request.getParameter("montoTotal")          !=null )?request.getParameter("montoTotal"):"";
String saldoLinea          = (request.getParameter("saldoLinea")          !=null )?request.getParameter("saldoLinea"):"";
String estatus             = (request.getParameter("estatus")             !=null )?request.getParameter("estatus"):"";
String nombreIntermediario = (request.getParameter("nombreIntermediario") !=null )?request.getParameter("nombreIntermediario"):"";
String noRegAcep           = (request.getParameter("noRegAcep")           !=null )?request.getParameter("noRegAcep"):"0";
String noRegRechazados     = (request.getParameter("noRegRechazados")     !=null )?request.getParameter("noRegRechazados"):"0";
String noRegistros         = (request.getParameter("noRegistros")         !=null )?request.getParameter("noRegistros"):"0";
String acuseT              = (request.getParameter("acuse")               !=null )?request.getParameter("acuse"):"";
String aceptados           = (request.getParameter("aceptados")           !=null )?request.getParameter("aceptados"):"";
String rechazados          = (request.getParameter("rechazados")          !=null )?request.getParameter("rechazados"):"";
String operacion           = (request.getParameter("operacion")           !=null )?request.getParameter("operacion"):"";
String folio               = (request.getParameter("folio")               !=null )?request.getParameter("folio"):"";
String universisad         = (request.getParameter("cmbUniversidad")      !=null )?request.getParameter("cmbUniversidad"):"";

int  start = 0, limit = 0;
String fechaHora = "", fecha = "",  hora = ""; 
String infoRegresar = "", consulta = "", campus = "", nombreIF = "";
HashMap datos = new HashMap();
JSONObject jsonObj = new JSONObject();
JSONArray registros = new JSONArray();

if(informacion.equals("Inicializar")){

	jsonObj.put("success",   new Boolean(true));
	jsonObj.put("strPerfil", strPerfil);
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("CatalogoIF")){

	CatalogoIF_Universidad catalogo = new CatalogoIF_Universidad();
	catalogo.setCampoClave("i.ic_if");
	catalogo.setCampoDescripcion("i.cg_razon_social");
	if(strPerfil.equals("ADMIN UNIV")){
		catalogo.setUniversidad(iNoCliente);
	}
	catalogo.setOrden("i.ic_if");

	jsonObj = JSONObject.fromObject(catalogo.getJSONElementos());
	jsonObj.put("intermediario", intermediario);
	jsonObj.put("success",       new Boolean(true));
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("CatalogoUniversidad")){

	CatalogoIF_Universidad catalogo = new CatalogoIF_Universidad();
	if(!intermediario.equals("")){
		catalogo.setIntermediario(intermediario);
	}
	catalogo.setCampoClave("U.IC_UNIVERSIDAD");
	catalogo.setCampoDescripcion("U.CG_RAZON_SOCIAL || ' - ' || U.CG_NOMBRE_CAMPUS");
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("Consultar") || informacion.equals("ArchivoCSV")){

	ConsultaCreditos paginador = new ConsultaCreditos();
	paginador.setIntermediario(intermediario);
	paginador.setFechaCargaIni(fechaCargaIni);
	paginador.setFechaCargaFin(fechaCargaFin);
	paginador.setCredito(credito);
	paginador.setMatricula(matricula);
	paginador.setEstudios(estudios);
	paginador.setFechaAcreditadoIni(fechaAcreditadoIni);
	paginador.setFechaAcreditadoFin(fechaAcreditadoFin);
	paginador.setAnio(anio);
	paginador.setPeriodo(periodo);
	paginador.setModalidad(modalidad);
	paginador.setFechaDisposicionIni(fechaDisposicionIni);
	paginador.setFechaDisposicionFin(fechaDisposicionFin);
	paginador.setNo_disposicion(no_disposicion);
	paginador.setImpDisposicion(impDisposicion);
	paginador.setMontoTotal(montoTotal);
	paginador.setSaldoLinea(saldoLinea);

	if(strPerfil.equals("ADMIN GARANT") || strPerfil.equals("ADMIN IF GARANT")){
		paginador.setUniversidad(universisad);
	} else{
		paginador.setUniversidad(iNoCliente);
	}

	paginador.setTipoConsulta(informacion);
	paginador.setEstatus(estatus);
	paginador.setRegRechazados(aceptados);
	paginador.setRegAceptados(rechazados);
	paginador.setAcuse(acuseT);
	paginador.setFolio(folio);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

	consulta = "";
	jsonObj = new JSONObject();

	if (informacion.equals("Consultar")){
		try{
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e){
			throw new AppException("Error en los parametros recibidos. ", e);
		}

		try{
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			Registros reg=queryHelper.getPageResultSet(request,start,limit);
			consulta	=	"{\"success\": true, \"total\": \""	+	queryHelper.getIdsSize() + "\", \"registros\": " + reg.getJSONData()+ "}";
			universisad = "";
			campus      = "";
			nombreIF    = "";
			if (reg.next()){
				universisad = (reg.getString("UNIVERSIDAD")   == null) ? "" : reg.getString("UNIVERSIDAD");
				campus      = (reg.getString("CAMPUS")        == null) ? "" : reg.getString("CAMPUS");
				nombreIF    = (reg.getString("INTERMEDIARIO") == null) ? "" : reg.getString("INTERMEDIARIO");
			}
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);	
			jsonObj.put("universisad","<b>Universidad:</b> "+universisad);		
			jsonObj.put("campus","<b>Campus:</b> "+campus);		
			jsonObj.put("nombreIF","<b>Nombre Intermediario:</b> "+nombreIF);
			//consulta = queryHelper.getJSONPageResultSet(request,start,limit);//getPageResultSet								
		} catch(Exception e){
			throw new AppException("Error en la paginacion. ", e);
		}
	} else if(informacion.equals("ArchivoCSV")){
		try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			throw new AppException("Error al generar el archivo CSV. ", e);
		}
	}
	jsonObj.put("intermediario",intermediario);
	infoRegresar = jsonObj.toString();
}
%>

<%=infoRegresar%>