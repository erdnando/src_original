<!DOCTYPE html>
<%@ page import="java.util.*,netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/37creditoEducativo/37secsession.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_if = (request.getParameter("intermediario")!=null)?request.getParameter("intermediario"):"";
%>
<html>

<head>
	<title>Nafinet</title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<%@ include file="/extjs.jspf" %>
	<%if(esEsquemaExtJS) {%>
		<%@ include file="/01principal/menu.jspf"%>
	<%}%>
	<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
	<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
	<script type="text/javascript" src="37ConsultaCreditos.js?<%=session.getId()%>"></script>
</head>

<% if(esEsquemaExtJS){%>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
				<div id="areaContenido"><div style="height:230px"></div></div>
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
		<form id='formParametros' name="formParametros">
			<input type="hidden" id="ic_if" name="ic_if" value="<%=ic_if%>"/>
		</form>
	</body>
	 
<%} else{%>
	<body>
	<div id='areaContenido'></div>
		<form id='formAux' name="formAux" target='_new'></form>
		<form id='formParametros' name="formParametros">
			<input type="hidden" id="ic_if" name="ic_if" value="<%=ic_if%>"/>
		</form>
	</body>
<%}%>

</html>