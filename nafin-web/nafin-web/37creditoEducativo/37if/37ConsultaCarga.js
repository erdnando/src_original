Ext.onReady(function(){

	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			Ext.getCmp('btnGenerarArchivo').enable();
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		var boton = Ext.getCmp('btnGenerarArchivo');
		var titulo = Ext.getCmp('grupo').getRawValue();
		Ext.getCmp('grid').setTitle(titulo);
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
				boton.enable();
				resumenTotalesData.load();
				var totalesCmp = Ext.getCmp('gridTotalesE');
				totalesCmp.show();
				if (!totalesCmp.isVisible()) {
					totalesCmp.show();
					//totalesCmp.getEl().dom.scrollIntoView();
					}		
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				boton.disable();
				
			}
		}
	}
function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	

	var consulta = new Ext.data.JsonStore({
		root: 'registros',
		url:  '37ConsultaCarga.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'GRUPO'        },
			{name: 'UNIVERSIDAD'  },
			{name: 'FECHA_CARGA'  },
			{name: 'FOLIO_CARGA'  },
			{name: 'FOLIO_OPER'   },
			{name: 'IC_CREDITO'   },
			{name: 'NOMBRE'       },
			{name: 'RFC'          },
			{name: 'MATRICULA'    },
			{name: 'ESTUDIOS'     },
			{name: 'FECHA_INGRESO'},
			{name: 'ANIO'         },
			{name: 'PERIODO'      },
			{name: 'MODALIDAD'    },
			{name: 'CAMPUS'       },
			{name: 'FECHA_DISPO'  },
			{name: 'NUM_DISPO'    },
			{name: 'IMPORTE_DISP' },
			{name: 'MONTO_TOTAL'  },
			{name: 'SALDO_LINEAS' },
			{name: 'ESTATUS'      },
			{name: 'CAUSA'        },
			{name: 'DES_USUARIO'  }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			beforeLoad: {fn: function(store, options){
				Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
			}},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy,type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					//procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
				}
			}
		}
	});

  var catalogoUni = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '37ConsultaCarga.data.jsp',
		baseParams: 
		{
		 informacion: 'CatologoUniversidad'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var catalogoEstatus = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '37ConsultaCarga.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

  var catalogoGrupo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '37ConsultaCarga.data.jsp',
		baseParams: 
		{
		 informacion: 'CatologoGrupo'
		},
		autoLoad: false,
		totalProperty : 'total',
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

	var resumenTotalesData = new Ext.data.JsonStore({
		root: 'registros',
		url: '37ConsultaCarga.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'ESTATUS',      mapping: 'ESTATUS'                    },
			{name: 'NUMERO',       mapping: 'NUMERO'                     },
			{name: 'DISP',         mapping: 'DISP',         type: 'float'},
			{name: 'MONTO',        mapping: 'MONTO',        type: 'float'},
			{name: 'SALDO_LINEAS', mapping: 'SALDO_LINEAS', type: 'float'}
		],
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

//-----------------------GRIDS------------------------
	var gridTotalesE =  new Ext.grid.GridPanel({
		width:  930,
		height: 130,
		id:     'gridTotalesE',
		title:  'Totales',
		hidden: true,
		frame:  false,
		view:   new Ext.grid.GridView({forceFit:true}),
		store:  resumenTotalesData,
		columns: [{
			width:        150,
			header:       'Estatus',
			dataIndex:    'ESTATUS',
			align:        'left',
			menuDisabled: true
		},{
			width:        200,
			header:       'N�mero de registros',
			dataIndex:    'NUMERO',
			align:        'center',
			menuDisabled: true
		},{
			width:        200,
			header:       'Importe Disposici�n',
			dataIndex:    'DISP',
			align:        'right',
			renderer:     Ext.util.Format.numberRenderer('$0,0.00'),
			menuDisabled: true
		},{
			width:        200,
			header:       'Monto Total',
			dataIndex:    'MONTO',
			align:        'right',
			renderer:     Ext.util.Format.numberRenderer('$0,0.00'),
			menuDisabled: true
		},{
			width:        200,
			header:       'Saldo de l�nea',
			dataIndex:    'SALDO_LINEAS',
			align:        'right',
			renderer:     Ext.util.Format.numberRenderer('$0,0.00'),
			menuDisabled: true
		}],
		tools: [{
			id:      'close',
			handler: function(evento, toolEl, panel, tc){
				panel.hide();
			}
		}]
	});

	var grid = new Ext.grid.GridPanel({
		width:      940,
		height:     400,
		id:         'grid',
		title:      '&nbsp;',
		style:      'margin:0 auto;',
		stripeRows: true,
		loadMask:   true,
		frame:      true,
		hidden:     true,
		header:     true,
		store:      consulta,
		columns:[{
			header: 'Fecha y hora de Carga',
			tooltip: 'Fecha y hora de Carga',
			sortable: true,
			dataIndex: 'FECHA_CARGA',
			width: 150,
			align: 'center'
		},{
			header: 'Folio de Carga',
			tooltip: 'Folio de Carga',
			sortable: true,
			dataIndex: 'FOLIO_CARGA',
			width: 150,
			align: 'center'
		},{
			header: 'Folio de Operaci�n',
			tooltip: 'Folio de Operaci�n',
			sortable: true,
			dataIndex: 'FOLIO_OPER',
			width: 130,
			align: 'center'
		},{
			align:'center',
			header: 'Cr�dito',
			tooltip: 'Cr�dito',
			sortable: true,
			dataIndex: 'IC_CREDITO',
			width: 130
		},{
			header:'Nombre del Acreditado',
			tooltip: 'Nombre del Acreditado',
			sortable: true,
			dataIndex: 'NOMBRE',
			width: 130
		},{
			header: 'RFC',
			tooltip: 'RFC',
			sortable: true,
			dataIndex: 'RFC',
			width: 150,
			align: 'center'
		},{
			header: 'Matricula',
			tooltip: 'Matricula',
			sortable: true,
			dataIndex: 'MATRICULA',
			width: 130,
			align: 'center'
		},{
			header: 'Estudios',
			tooltip: 'Estudios',
			sortable: true,
			dataIndex: 'ESTUDIOS',
			width: 130,
			align: 'center'
		},{
			header: '<center>Fecha de Ingreso</center>',
			tooltip: 'Fecha de Ingreso',
			sortable: true,
			dataIndex: 'FECHA_INGRESO',
			width: 150
		},{
			header: 'A�o de colocaci�n',
			tooltip: 'A�o de colocaci�n',
			sortable: true,
			dataIndex: 'ANIO',
			width: 120,
			align: 'center'
		},{
			header: 'Periodo Educativo',
			tooltip: 'Periodo Educativo',
			sortable: true,
			dataIndex: 'PERIODO',
			width: 120
		},{
			header: 'Modalidad Programa',
			tooltip: 'Modalidad Programa',
			sortable: true,
			dataIndex: 'MODALIDAD',
			width: 150,
			align: 'center'
		},{
			header: 'Grupo',
			tooltip: 'Grupo',
			sortable: true,
			dataIndex: 'GRUPO',
			width: 150
		},{
			header: 'Universidad',
			tooltip: 'Universidad',
			sortable: true,
			dataIndex: 'UNIVERSIDAD',
			width: 150
		},{
			header: 'Campus',
			tooltip: 'Campus',
			sortable: true,
			dataIndex: 'CAMPUS',
			width: 150
		},{
			header: 'Fecha �ltima disposici�n',
			tooltip: 'Fecha �ltima disposici�n',
			sortable: true,
			dataIndex: 'FECHA_DISPO',
			width: 150,
			align: 'center'
		},{
			header: 'No. Disposici�n',
			tooltip: 'No. Disposici�n',
			sortable: true,
			dataIndex: 'NUM_DISPO',
			width: 150,
			align: 'center'
		},{
			header: 'Importe Disposici�n',
			tooltip: 'Importe Disposici�n',
			sortable: true,
			dataIndex: 'IMPORTE_DISP',
			width: 150,
			align: 'center'
		},{
			header: 'Monto Total',
			tooltip: 'Monto Total',
			sortable: true,
			dataIndex: 'MONTO_TOTAL',
			width: 150,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header: 'Saldo de L�nea',
			tooltip: 'Saldo de L�nea',
			sortable: true,
			dataIndex: 'SALDO_LINEAS',
			width: 150,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header: '<center>Estatus</center>',
			tooltip: 'Estatus',
			sortable: true,
			dataIndex: 'ESTATUS',
			width: 130,
			align: 'left'
		},{
			align:'center',
			header: 'Observaciones',
			tooltip: 'Observaciones',
			sortable: true,
			dataIndex: 'CAUSA',
			width: 130,
			renderer:function(value, metadata, record, rowindex, colindex, store) {
				metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
				return value;
			}
		},{
			header: 'Usuario',
			tooltip: 'Usuario',
			sortable: true,
			dataIndex: 'DES_USUARIO',
			width: 150,
			align: 'center'
		}],
		bbar: {
			xtype: 'paging',
			autoScroll:true,
			height: 30,
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consulta,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: ['->','-',{
				xtype: 'button',
				//height: 40,
				text: 'Generar Archivo',
				id: 'btnGenerarArchivo',
				handler: function(boton, evento){
					boton.disable();
					//boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '37ConsultaCarga.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'
						}),
						callback: procesarArchivoSuccess
					});
				}
			},
			/*{
				xtype: 'button',
				text: 'Totales',
				id: 'btnTotales',
				hidden: false,
				handler: function(boton, evento){
					resumenTotalesData.load();
					var totalesCmp = Ext.getCmp('gridTotalesE');
					totalesCmp.show();
					if (!totalesCmp.isVisible()) {
						totalesCmp.show();
						//totalesCmp.getEl().dom.scrollIntoView();
					}
				}
			},*/
		'-']}
	});

//-------------------------------------------
	var elementosForma= [{
		width:          500,
		xtype:          'combo',
		id:             'grupo',
		name:           'HGrupo',
		hiddenName:     'HGrupo',
		fieldLabel:     'Grupo',
		emptyText:      'Seleccionar Grupo',
		displayField:   'descripcion',
		valueField:     'clave',
		triggerAction:  'all',
		mode:           'local',
		minChars:       1,
		typeAhead:      true,
		forceSelection: true,
		allowBlank:     false,
		store:          catalogoGrupo,
		tpl:            NE.util.templateMensajeCargaCombo,
		listeners: {
			select: function(combo, record, index){
				Ext.getCmp('universidad').reset();
				catalogoUni.load({
					params:{
						Hgrupo:Ext.getCmp('grupo').getValue()
					}
				});
			}
		}
	},{
		width:          230,
		xtype:          'combo',
		id:             'universidad',
		name:           'HUniversidad',
		hiddenName:     'HUniversidad',
		fieldLabel:     'Universidad - Campus',
		emptyText:      'Seleccionar una Universidad',
		displayField:   'descripcion',
		valueField:     'clave',
		mode:           'local',
		triggerAction:  'all',
		typeAhead:      true,
		forceSelection: true,
		minChars:       1,
		store:          catalogoUni,
		tpl:            NE.util.templateMensajeCargaCombo
	},{
		xtype:          'textfield',
		id:             'id_folio_carga',
		name:           'ic_folio_carga',
		fieldLabel:     'Folio de Carga'
	},{
		xtype:          'textfield',
		id:             'id_folio_operacion',
		name:           'ic_folio_operacion',
		fieldLabel:     'Folio de Operaci�n'
	},{
		xtype:          'textfield',
		id:             'id_credito',
		name:           'ic_credito',
		fieldLabel:     'Cr�dito'
	},{
		xtype:          'textfield',
		id:             'id_rfc_acreditado',
		name:           'ic_rfc_acreditado',
		fieldLabel:     'RFC Acreditado'
	},{
		xtype:          'textfield',
		id:             'id_matricula',
		name:           'ic_matricula',
		fieldLabel:     'Matricula'
	},{
		width:          230,
		xtype:          'combo',
		id:             'estatus',
		name:           'HEstatus',
		hiddenName:     'HEstatus',
		fieldLabel:     'Estatus',
		emptyText:      'Seleccionar Estatus',
		mode:           'local',
		displayField:   'descripcion',
		valueField:     'clave',
		triggerAction:  'all',
		typeAhead:      true,
		forceSelection: true,
		minChars:       1,
		store:          catalogoEstatus,
		tpl:            NE.util.templateMensajeCargaCombo
	},{
		xtype:               'compositefield',
		fieldLabel:          'Fecha de Carga',
		msgTarget:           'side',
		combineErrors:       false,
		items: [{
			width:            100,
			xtype:            'datefield',
			name:             'fechaCarga1',
			id:               'fechaCarga1',
			msgTarget:        'side',
			vtype:            'rangofecha', 
			campoFinFecha:    'fechaCarga2',
			margins:          '0 20 0 0',
			startDay:         0
		},{
			width:            24,
			xtype:            'displayfield',
			value:            'al'
		},{
			width:            100,
			xtype:            'datefield',
			id:               'fechaCarga2',
			name:             'fechaCarga2',
			msgTarget:        'side',
			vtype:            'rangofecha', 
			campoInicioFecha: 'fechaCarga1',
			margins:          '0 20 0 0',
			startDay:         1
		}]
	}];

	var fp = new Ext.form.FormPanel({
		width:         650,
		labelWidth:    125,
		id:            'forma',
		title:         'Consulta Carga',
		height:        'auto',
		style:         'margin:0 auto;',
		bodyStyle:     'padding: 6px',
		defaultType:   'textfield',
		frame:         true,
		hidden:        false,
		monitorValid:  true,
		collapsible:   false,
		titleCollapse: false,
		defaults: {
			msgTarget:  'side',
			anchor:     '-20'
		},
		items:         elementosForma,
		buttons:[{
			xtype:    'button',
			text:     'Consultar',
			name:     'btnConsultar',
			iconCls:  'icoBuscar',
			hidden:   false,
			formBind: true,
			handler:  function(boton, evento){
				if(verificaFechas('fechaCarga1','fechaCarga2')){
					gridTotalesE.hide();
					fp.el.mask('cargando','x-mask');
					grid.show();
					grid.setTitle(Ext.getCmp('universidad').getRawValue());
					consulta.load({ 
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start: 0,
							limit: 15
						})
					});
				}
			}
		},{
			text:    'Limpiar',
			iconCls: 'icoLimpiar',
			handler: function() {
				location.reload();
			}
		}]
	});

//----------------Principal------------------------------
	var pnl = new Ext.Container({
		width: 930,
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		height: 'auto',
		items: [  
			fp,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10),
			gridTotalesE 
		]
	});
		
		catalogoUni.load();
		catalogoEstatus.load()
		catalogoGrupo.load();
});