<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
netropology.utilerias.*,
com.netro.model.catalogos.*,
com.netro.educativo.ConsultaCarga,
net.sf.json.JSONArray,
net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/37creditoEducativo/37secsession_extjs.jspf" %>

<%
String informacion  = (request.getParameter("informacion") !=null)? request.getParameter("informacion"):"";
String icGrupo      = (request.getParameter("Hgrupo")      !=null)? request.getParameter("Hgrupo"):"";
String claveIF	     = iNoCliente;
String infoRegresar = "";
JSONObject jsonObj  = new JSONObject();

if (informacion.equals("CatologoGrupo")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_GRUPO");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("CECAT_GRUPO");
	catalogo.setOrden("IC_GRUPO");
	infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("catologoEstatus")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_ESTATUS_CRED_EDUCA");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("CECAT_ESTATUS_CRED_EDUCA");
	catalogo.setOrden("IC_ESTATUS_CRED_EDUCA");
	String estatus = "1,2,3";
	catalogo.setValoresCondicionIn(estatus, Integer.class);
	infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("CatologoUniversidad")){

	CatalogoSimple catalogo = new CatalogoSimple();

	catalogo.setCampoClave("IC_UNIVERSIDAD");
	String cad = "";
	if(!icGrupo.equals("")){
		cad = "ic_grupo";
		catalogo.setCampoLlave(cad);
		catalogo.setValoresCondicionIn(icGrupo,Integer.class);
	}

	catalogo.setCampoDescripcion("CG_RAZON_SOCIAL||' - '|| CG_NOMBRE_CAMPUS ");
	catalogo.setTabla("COMCAT_UNIVERSIDAD");
	catalogo.setOrden("IC_UNIVERSIDAD");

	infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("Consulta") || informacion.equals("ArchivoCSV")){

	int start = 0;
	int limit = 0;
	String grupo         = (request.getParameter("HGrupo")            != null)?request.getParameter("HGrupo"):"";
	String universidad   = (request.getParameter("HUniversidad")      != null)?request.getParameter("HUniversidad"):"";
	String folioCarga    = (request.getParameter("ic_folio_carga")    != null)?request.getParameter("ic_folio_carga"):"";
	String folioOper     = (request.getParameter("ic_folio_operacion")!= null)?request.getParameter("ic_folio_operacion"):"";
	String credito       = (request.getParameter("ic_credito")        != null)?request.getParameter("ic_credito"):"";
	String rfc           = (request.getParameter("ic_rfc_acreditado") != null)?request.getParameter("ic_rfc_acreditado"):"";
	String matricula     = (request.getParameter("ic_matricula")      != null)?request.getParameter("ic_matricula"):"";
	String estatus       = (request.getParameter("HEstatus")          != null)?request.getParameter("HEstatus"):"";
	String fechaCargaIni = (request.getParameter("fechaCarga1")       != null)?request.getParameter("fechaCarga1"):"";
	String fechaCargaFin = (request.getParameter("fechaCarga2")       != null)?request.getParameter("fechaCarga2"):"";

	ConsultaCarga paginador =new  ConsultaCarga();
	paginador.setGrupo(grupo);
	paginador.setUniversidad(universidad);
	paginador.setFolioCarga(folioCarga);
	paginador.setFolioOper(folioOper);
	paginador.setCredito(credito);
	paginador.setRfc(rfc);
	paginador.setMatricula(matricula);
	paginador.setEstatus(estatus);
	paginador.setFechaCargaIni(fechaCargaIni);
	paginador.setFechaCargaFin(fechaCargaFin);
	paginador.setIcIF(claveIF);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		if (informacion.equals("Consulta")){ // Datos para la Consulta con Paginacion
			String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e){
				throw new AppException("Error en los parametros recibidos. ", e);
			}
			try{
				if (operacion.equals("Generar")) { //Nueva consulta
					queryHelper.executePKQuery(request);
				}
				infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
			} catch(Exception e){
				throw new AppException("Error en la paginacion. ", e);
			}
		} else if(informacion.equals("ArchivoCSV")){
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
} else if (informacion.equals("ResumenTotales")){ //Datos para el Resumen de Totales

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion
}

%>

<%=infoRegresar%>