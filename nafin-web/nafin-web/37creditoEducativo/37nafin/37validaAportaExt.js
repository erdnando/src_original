var presionCheck = false;
function selecDeselec(check){
	presionCheck = true;
	var  gridFolios = Ext.getCmp('gridFolios');
	var store = gridFolios.getStore();

	if(check.id == 'todoAcepta'){
		if(check.checked==true)  {
			store.each(function(rec){ rec.set('ACEPTADOS', true); rec.set('RECHAZADOS',false); })
			var el_otro_check = document.getElementById('todoRechaza');
			el_otro_check.checked = false;
		}else{
			store.each(function(rec){ rec.set('ACEPTADOS', false);})
		}
	}else if (check.id == 'todoRechaza'){
		if(check.checked==true)  {
			store.each(function(rec){ rec.set('ACEPTADOS', false); rec.set('RECHAZADOS',true); })
			var el_otro_check = document.getElementById('todoAcepta');
			el_otro_check.checked = false;
		}else{
			store.each(function(rec){ rec.set('RECHAZADOS', false);})
		}
	}
	store.commitChanges();
	presionCheck = false;
}

Ext.onReady(function() {

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});
//- - - - - - - - - - - - VARIABLE GLOBAS

	var infoAcuse = {registrosRechaza:null,	registrosAcepta:null}
	var varGlobal = {estatusReg:""}
//- - - - - - - - - - - -  FUNCIONES - - - - - - - - -  - - - - - - - - -  

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

	var procesarConsultaFolios = function(store, registros, opts){

		var forma = Ext.getCmp('forma');
		forma.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('gridFolios');

			if (!grid.isVisible()) {
				grid.show();
			}

			Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				Ext.getCmp('botonContinuar').enable();
				el.unmask();
			} else {
				Ext.getCmp('botonContinuar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
		}
	}

	var procesarSuccessFailureGeneraArchivoPDF =  function(opts, success, response) {
 
		var botonImprimirPDF = Ext.getCmp('botonImprimirPDF');
		botonImprimirPDF.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			botonImprimirPDF.setIconClass('icoPdf');
			botonImprimirPDF.setText('Descargar PDF');
			
			botonImprimirPDF.urlArchivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			botonImprimirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						botonImprimirPDF.setIconClass('icoImprimir');
						botonImprimirPDF.setText('Imprimir PDF');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				botonImprimirPDF.setIconClass('icoImprimir');
				botonImprimirPDF.setText('Imprimir PDF');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
 
	}

	var procesarConsultaRegistrosAceptados = function(store, registros, opts){
		var gridRegistrosAceptados 			= Ext.getCmp('gridRegistrosAceptados');
		if (registros != null) {
			var el 							= gridRegistrosAceptados.getGridEl();	
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
			}
		}
	}

	var procesarConsultaRegistrosRechazados = function(store, registros, opts){
		var gridRegistrosRechazados			= Ext.getCmp('gridRegistrosRechazados');
		if (registros != null) {
			var el 							= gridRegistrosRechazados.getGridEl();	
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
			}
		}
	}

	var procesarConsultaRegistrosAgregados = function(store, registros, opts){
		var gridRegistrosAgregados 			= Ext.getCmp('gridRegistrosAgregados');
		if (registros != null) {
			var el 							= gridRegistrosAgregados.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
			}
		}
	}

	var procesarConsultaDataE = function(store, arrRegistros, opts) {
		//var botonT = Ext.getCmp('btnTotalesE');

		if (arrRegistros != null) {
			var gridE  = Ext.getCmp('gridE');

			if (!gridE.isVisible()) {
				gridE.show();
			}
			var el = gridE.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
				
				var cm = gridE.getColumnModel();

				if(varGlobal.estatusReg === "S"){
					
					cm.setHidden(cm.findColumnIndex("FG_PORCENTAJE_APORTACION"),false);
					cm.setHidden(cm.findColumnIndex("FN_MONTO_GARANTIA"),			false);
					
					resumenTotalesData.load();
					var totalesCmp = Ext.getCmp('gridTotalesE');

					if (!totalesCmp.isVisible()) {
						totalesCmp.show();
						totalesCmp.el.dom.scrollIntoView();
					}
				}else{
					cm.setHidden(cm.findColumnIndex("FG_PORCENTAJE_APORTACION"),true);
					cm.setHidden(cm.findColumnIndex("FN_MONTO_GARANTIA"),			true);
				}

			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				botonT.disable();
			}
		}
	}
	/*var renderContenidoGridAcuse = function(value, metaData, record, rowIndex, colIndex, store) {
		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: left !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;
	}*/

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

	var registrosAceptadosData = new Ext.data.ArrayStore({
		storeId: 'registrosAceptadosDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'NOMBRE_IF'},
			{ name: 'FOLIO_OPERACION'},
			{ name: 'NUM_ACEPTADAS'},
			{ name: 'MONTO_GARANTIA',			type: 'float' }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosAceptados,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosAceptados(null, null, null);						
				}
			}
		}
	});

	var registrosRechazadosData = new Ext.data.ArrayStore({
		storeId: 'registrosRechazadosDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'NOMBRE_IF'},
			{ name: 'FOLIO_OPERACION'},
			{ name: 'NUM_ACEPTADAS'},
			{ name: 'CAUSA_RECHAZO'}
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosRechazados,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosRechazados(null, null, null);						
				}
			}
		}
	});

	var registrosAgregadosData = new Ext.data.ArrayStore({
		storeId: 'registrosAgregadosDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosAgregados,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosAgregados(null, null, null);						
				}
			}
		}
	});

	var catalogoIfData = new Ext.data.JsonStore({
		id:				'catalogoIfDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'37validaAporta.data.jsp',
		baseParams:		{	informacion: 'CatalogoIf'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var foliosConsultadosData = new Ext.data.JsonStore({
		root: 		'registros',
		id:			'foliosConsultadosDataStore',
		url: 			'37validaAporta.data.jsp',
		baseParams: {	informacion:	'ConsultaFolios'	},
		fields: [
			{ name: 'FOLIO_OPERACION', 		type: 'auto' },
			{ name: 'NOMBRE_IF', 				type: 'string' },
			{ name: 'CLAVE_IF', 					type: 'string' },
			{ name: 'ACEPTADOS', 				convert: NE.util.string2boolean },
			{ name: 'RECHAZADOS', 				convert: NE.util.string2boolean },
			{ name: 'MONTO_VALIDO',				type: 'float' },
			{ name: 'MONTO_GARANTIA',			type: 'float' },
			{ name: 'NUM_ACEPTADAS',			type: 'string' },
			{ name: 'NUM_RECHAZADAS',			type: 'string' },
			{ name: 'TOTAL_OPERACIONES',		type: 'string' },
			{ name: 'FECHA_HORA',				type: 'string' }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload:	{
				fn: function(store, options){
					/*
					console.log("options.params(beforeload)");
					console.dir(options.params);
					*/
				}
			},
			load: 	procesarConsultaFolios,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaFolios(null, null, null);
				}
			}
		}
	});

	var consultaE = new Ext.data.JsonStore({
		root: 'registros',
		id:	'consultaEStore',
		url: '37validaAporta.data.jsp',
		baseParams: {
			informacion: 'ConsultaE'
		},
		fields: [
					{name: 'CREDI'},
					{name: 'NOMBRE'},
					{name: 'RFC'},
					{name: 'MATRICULA'},
					{name: 'ESTUDIOS'},
					{name: 'FECHA_INGRESO'},
					{name: 'ANIO'},
					{name: 'PERIODO'},
					{name: 'MODALIDAD'},
					{name: 'FECHA_DIS'},
					{name: 'NUM_DIS'},
					{name: 'IMPORTE'},
					{name: 'MONTO_TOTAL'},
					{name: 'SALDOLINEAS'},
					{name: 'FG_PORCENTAJE_APORTACION'},
					{name: 'FN_MONTO_GARANTIA'},
					{name: 'MODALIDAD_BASES_OPER'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load:		procesarConsultaDataE,
			exception: {
						fn: function(proxy,type,action,optionsRequest,response,args){
								NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
								procesarConsultaDataE(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
						}
			}
		}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '37validaAporta.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'REGISTROS',							mapping: 'REGISTROS'},
			{name: 'TOTAL_IMPORTE', type: 'float', mapping: 'TOTAL_IMPORTE'},
			{name: 'TOTAL_MONTO', 	type: 'float', mapping: 'TOTAL_MONTO'},
			{name: 'TOTAL_SALDO',	type: 'float', mapping: 'TOTAL_SALDO'},
			{name: 'TOTAL_CONTRA',	type: 'float', mapping: 'TOTAL_CONTRA'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});

//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - 

	var procesaConsultaFolios = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							consultaFolios(resp.estadoSiguiente,resp);
						}
					);
				} else {
					consultaFolios(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var consultaFolios = function(estadoSiguiente, respuesta){

		if(  estadoSiguiente == "CONSULTA_FOLIOS" 													){

			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');

			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridFolios").hide();
			Ext.StoreMgr.key('foliosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'ConsultarFolios'
							}
				)
			});

		} else if(  estadoSiguiente == "CONTINUAR_PROCESO" 											){	

			var forma = Ext.getCmp("forma");
			forma.el.mask('Enviando...','x-mask-loading');

			var gridRegistrosAgregados = Ext.getCmp('gridRegistrosAgregados');
			var el 							= gridRegistrosAgregados.getGridEl();
			el.mask('Enviando...', 'x-mask');

 			Ext.Ajax.request({
				url:			'37validaAporta.data.jsp',
				params:		{	informacion:		'ContinuaProceso',
									folios:				respuesta.folios,
									montoGaran:			respuesta.montoGaran,
									aceptados:			respuesta.aceptados,
									rechazados:			respuesta.rechazados,
									causaRechazo:		respuesta.causaRechazo
								},
				callback:	procesaConsultaFolios
			});

		} else if(	estadoSiguiente == "RESPUESTA_MOSTRAR_ACUSE"	 									){

			Ext.getCmp("forma").hide();
			Ext.getCmp('gridFolios').hide();
			Ext.getCmp("botonImprimirPDF").urlArchivo = "";
			Ext.getCmp("labelAcuseInformacion").setText(respuesta.msjInfo,false);
			Ext.getCmp('panelAcuse.fechaValidacion').setValue(respuesta.fechaVal);
			Ext.getCmp("panelAcuse").show();
			Ext.StoreMgr.key('registrosAgregadosDataStore').loadData(respuesta.registrosAgregadosDataArray);
			if(Ext.StoreMgr.key('registrosAceptadosDataStore').getCount() <= 0){
				Ext.getCmp('gridRegistrosAceptados').getGridEl().mask('No existen registros...', 'x-mask');;
			}
			if (Ext.StoreMgr.key('registrosRechazadosDataStore').getCount() <= 0){
				Ext.getCmp('gridRegistrosRechazados').getGridEl().mask('No existen registros...', 'x-mask');;
			}

		} else if(	estadoSiguiente == "NUM_OPERACIONES_ACEPTADAS"								){

			Ext.getCmp('verEstudiantes').setTitle('Operaciones Aceptadas');
			Ext.getCmp('labelFolio').setText('Folio Operaci�n:					'	+respuesta.folioOpera,	false);
			Ext.getCmp('labelIf').setText(	'Intermediario Financiero:		'	+respuesta.nombreIf,		false);
			Ext.getCmp('labelFecha').setText('Fecha y Hora de Operaci�n:	'	+respuesta.fechaHora,	false);
			Ext.getCmp('verEstudiantes').show();
			var totalesCmp = Ext.getCmp('gridTotalesE');
			if (totalesCmp.isVisible()) {
				totalesCmp.hide();
			}

			varGlobal.estatusReg = respuesta.estatusReg;

			consultaE.load({
				params: Ext.apply({
					estatusReg:			respuesta.estatusReg,
					folioOperacion:	respuesta.folioOpera,
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15
				})
			});

		} else if(	estadoSiguiente == "NUM_OPERACIONES_RECHAZADAS"								){

			Ext.getCmp('verEstudiantes').setTitle('Operaciones Rechazadas');
			Ext.getCmp('labelFolio').setText('Folio Operaci�n:					'				+respuesta.folioOpera,	false);
			Ext.getCmp('labelIf').setText(	'Intermediario Financiero:		'	+respuesta.nombreIf,		false);
			Ext.getCmp('labelFecha').setText('Fecha y Hora de Operaci�n:	'	+respuesta.fechaHora,	false);
			Ext.getCmp('verEstudiantes').show();
			var totalesCmp = Ext.getCmp('gridTotalesE');
			if (totalesCmp.isVisible()) {
				totalesCmp.hide();
			}

			varGlobal.estatusReg = respuesta.estatusReg;

			consultaE.load({
				params: Ext.apply({
					estatusReg:			respuesta.estatusReg,
					folioOperacion:	respuesta.folioOpera,
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15
				})
			});

		} else if(	estadoSiguiente == "LIMPIAR"														){

			Ext.getCmp('forma').getForm().reset();
			grid.hide();

		} else if(	estadoSiguiente == "FIN"															){
						
			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "37validaAportaExt.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

	var gridTotalesE =  new Ext.grid.GridPanel({
		store:resumenTotalesData,	id:'gridTotalesE',	view:new Ext.grid.GridView({forceFit:true}),	width:930,	height:100,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'Numero de Registros',	dataIndex: 'REGISTROS',	align: 'left',	width: 150, menuDisabled:true
			},{
				header: 'Importe Disposici�n',	dataIndex: 'TOTAL_IMPORTE',	width: 200,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Total',dataIndex: 'TOTAL_MONTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Saldo L�neas',	dataIndex: 'TOTAL_SALDO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto de Contragarant�a<br/> a pagar',	dataIndex: 'TOTAL_CONTRA',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		]/*,
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]*/
	});

	var gridE = new Ext.grid.GridPanel({
		store:	consultaE,
		id:		'gridE',
		style:	'margin:0 auto;',
		columns:[
			{
				header:		'Cr�dito',
				tooltip:		'Cr�dito',
				sortable:	true,
				dataIndex:	'CREDI',
				width:		150,
				align:		'center',
				fixed:		true,
				hideable:	false
			},
			{
				header:		'Nombre de Acreditado',
				tooltip:		'Nombre de Acreditado',
				sortable:	true,
				dataIndex:	'NOMBRE',
				width:		150,
				align:		'center'
			},
			{
				header:		'RFC',
				tooltip:		'RFC',
				sortable:	true,
				dataIndex:	'RFC',
				width:		150,
				align:		'center'
			},
			{
				header:		'Matric�la',
				tooltip:		'Matric�la',
				sortable:	true,
				dataIndex:	'MATRICULA',
				width:		130,
				align:		'center'
			},
			{
				align:'center',
				header: 'Estudios',
				tooltip: 'Estudios',
				sortable: true,
				dataIndex: 'ESTUDIOS',
				width: 130
			},{
				header:		'Fecha de Ingreso',
				tooltip:		'Fecha de Ingreso',
				sortable:	true,
				dataIndex:	'FECHA_INGRESO',
				width:		130,
				align:		'center'
			},
			{
				header: 'A�o de Colocaci�n',
				tooltip: 'A�o de Colocaci�n',
				sortable: true,
				dataIndex: 'ANIO',
				width: 150,
				align: 'center'
			},
			{
				header: 'Periodo Educativo',
				tooltip: 'Periodo Educativo',
				sortable: true,
				dataIndex: 'PERIODO',
				width: 130,
				align: 'center'
			},{
				header: 'Modalidad Programa',
				tooltip: 'Modalidad Programa',
				sortable: true,
				dataIndex: 'MODALIDAD',
				width: 130,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						return value;
					}
			},
			{
				header:		'Fecha �ltima Disposici�n',
				tooltip:		'Fecha �ltima Disposici�n',
				sortable:	true,
				dataIndex:	'FECHA_DIS',
				width:		150,
				align:		'center'
			},
			{
				header: 'No. Disposiciones',
				tooltip: 'No. Disposiciones',
				sortable: true,
				dataIndex: 'NUM_DIS',
				width: 130,
				align: 'left'
			},
			{
				align:'center',
				header: 'Importe Disposici�n',
				tooltip: 'Importe Disposici�n',
				sortable: true,
				dataIndex: 'IMPORTE',
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total',
				tooltip: 'Monto Total',
				sortable: true,
				dataIndex: 'MONTO_TOTAL',
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Saldo de L�nea',
				tooltip: 'Importe del Dep�sito',
				sortable: true,
				dataIndex: 'SALDOLINEAS',
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Modalidad',
				tooltip: 'Modalidad',
				sortable: true,
				dataIndex: 'MODALIDAD_BASES_OPER',
				width: 200,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						return value;
					}
			},
			{
				header: '% de aportaci�n',
				tooltip: '% de aportaci�n',
				sortable: true,
				hideable:	false,
				dataIndex: 'FG_PORCENTAJE_APORTACION',
				width: 150,
				align: 'center'
			},
			{
				header:		'Monto de Contragarant�a a pagar',
				tooltip:		'Monto de Contragarant�a a pagar',
				sortable:	true,
				dataIndex:	'FN_MONTO_GARANTIA',
				width:		150,
				align:		'right',
				hideable:	false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows:	true,
		loadMask:	true,
		height:		400,
		width:		930,
		style:		'margin:0 auto;',
		frame:		false,
		bbar: {
			xtype:		'paging',
			autoScroll:	true,
			height:		30,
			pageSize:	15,
			buttonAlign:'left',
			id:			'barraPaginacion1',
			displayInfo:true,
			store:		consultaE,
			displayMsg:	'{0} - {1} de {2}',
			emptyMsg:	"No hay registros."/*,
			items:		[
				'->','-',
				{
						xtype:	'button',
						text:		'Totales',
						id:		'btnTotalesE',
						hidden:	false,
						handler:	function(boton, evento) {
										resumenTotalesData.load();
										var totalesCmp = Ext.getCmp('gridTotalesE');
										totalesCmp.show();
										if (!totalesCmp.isVisible()) {
											totalesCmp.show();
											totalesCmp.el.dom.scrollIntoView();
										}
						}
				},'-'
			]*/
		}
	});

	// Grid con las cuentas registradas
	var grid = new Ext.grid.EditorGridPanel({
		store: 	foliosConsultadosData,
		id:		'gridFolios',
		style: 	'margin: 0 auto; padding-top:10px;',
		hidden: 	true,
		margins: '20 0 0 0',
		title: 	undefined,
		view: new Ext.grid.GridView({markDirty:	false}),
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		width: 		930,
		frame: 		true,
		clicksToEdit:1,
		columns: [
			{
				header: 		'<div align="center">Intermediario <br>Financiero</div>',
				tooltip: 	'Intermediario Financiero',
				dataIndex: 	'NOMBRE_IF',
				align:		'left',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		false,
				hideable:	false,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'Fecha y hora <br>de Operaci�n',
				tooltip: 	'Fecha y hora de operaci�n',
				dataIndex: 	'FECHA_HORA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hideable:	false,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'Folio <br>operaci�n',
				tooltip: 	'Folio operaci�n',
				dataIndex: 	'FOLIO_OPERACION',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				hidden: 		false,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'Monto validado',
				tooltip: 	'Monto validado',
				dataIndex: 	'MONTO_VALIDO',
				align:		'right',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				hidden: 		false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},{
            xtype: 		'actioncolumn',
            header: 		'No. Operaciones <br>aceptadas',
				tooltip: 	'No. Operaciones aceptadas',
            width: 		80,
            align:		'center',
            hidden: 		false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return (record.get('NUM_ACEPTADAS'));
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(	!Ext.isEmpty(registro.get('NUM_ACEPTADAS'))	&& registro.get('NUM_ACEPTADAS') != '0'	)	{
								this.items[0].tooltip = 'Ver detalle de operaciones aceptadas';
								return 'iconoLupa';
							}else	{
								return value;
							}
							//return 'iconoLupa';
						},
						handler: function(grid, rowIndex, colIndex) {

									var record = Ext.StoreMgr.key('foliosConsultadosDataStore').getAt(rowIndex);
                        	var respuesta = new Object();
                        	respuesta['folioOpera']	= record.json['FOLIO_OPERACION'];
									respuesta['claveIf']		= record.json['CLAVE_IF'];
									respuesta['nombreIf']	= record.json['NOMBRE_IF'];
									respuesta['fechaHora']	= record.json['FECHA_HORA'];
									respuesta['estatusReg']	= "S";
                        	consultaFolios("NUM_OPERACIONES_ACEPTADAS",respuesta);
                    }
					}
				]
			},{
            xtype: 		'actioncolumn',
            header: 		'No. Operaciones <br>rechazadas',
				tooltip: 	'No. Operaciones rechazadas',
            width: 		80,
            align:		'center',
            hidden: 		false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return (record.get('NUM_RECHAZADAS'));
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(	!Ext.isEmpty(registro.get('NUM_RECHAZADAS'))	&& registro.get('NUM_RECHAZADAS') != '0'	)	{
								this.items[0].tooltip = 'Ver detalle de operaciones rechazadas';
								return 'iconoLupa';
							}else	{
								return value;
							}
						},
						handler: function(grid, rowIndex, colIndex) {

									var record = Ext.StoreMgr.key('foliosConsultadosDataStore').getAt(rowIndex);
                        	var respuesta = new Object();
                        	respuesta['folioOpera']	= record.json['FOLIO_OPERACION'];
									respuesta['claveIf']		= record.json['CLAVE_IF'];
									respuesta['nombreIf']	= record.json['NOMBRE_IF'];
									respuesta['fechaHora']	= record.json['FECHA_HORA'];
									respuesta['estatusReg']	= "N";
                        	consultaFolios("NUM_OPERACIONES_RECHAZADAS",respuesta);

                    }
					}
				]
			},{
				header: 		'Total <br>operaciones',
				tooltip: 	'Total operaciones',
				dataIndex: 	'TOTAL_OPERACIONES',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false
			},{
				header: 		'<div align="center">Monto contragarantia <br>a pagar</div>',
				tooltip: 	'Monto contragarantia a pagar',
				dataIndex: 	'MONTO_GARANTIA',
				align:		'right',
				sortable: 	true,
				resizable: 	true,
				width: 		130,
				hidden: 		false,
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			},{
				xtype:		'checkcolumn',
				id:			'acepta',
				header:		'<div align="center">Confirmar</div><br><div align="center"><input type="checkbox"  id="todoAcepta" onClick="selecDeselec(this);"></div>',
				id:			'chkAcepta',
				dataIndex:	'ACEPTADOS',
				width:		70,
				hideable:	false,
				menuDisabled:true
			},{
				xtype:		'checkcolumn',
				id:			'rechaza',
				header:		'<div align="center">Rechazar</div><br><div align="center"><input type="checkbox" id="todoRechaza" onClick="selecDeselec(this);"></div>',
				id:			'chkRechaza',
				dataIndex:	'RECHAZADOS',
				width:		70,
				hideable:	false,
				menuDisabled:true
			},{
				header:		'Causas de rechazo',
				tooltip:		'Causas de rechazo',	
				dataIndex:	'CAUSA_RECHAZO', 
				fixed:		true,
				sortable:	false,
				width:		250,
				hiddeable:	false,
				editor:{
					xtype:				'textarea',
					id:					'causa_rechazo',
					maxLength:			254,
					enableKeyEvents:	true,
					listeners:	{
						keydown: function(field, e){
							if ( (field.getValue()).length >= 254){
								field.setValue((field.getValue()).substring(0,253));
								field.focus();
							}
						}
					}
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			}
		],
		listeners:{
			beforeedit:	function(obj){
								if(obj.field == 'CAUSA_RECHAZO'){
									if(	obj.record.data['ACEPTADOS']	){
										obj.record.set('CAUSA_RECHAZO',"");
										obj.record.commit();
										obj.cancel = true;
									}
								}
			}
		},
		bbar: {
			id:	'barraPaginacion',
			items: [
					'->',
					'-',
					{
						xtype:	'button',
						iconCls: 'icoContinuar',
						text: 	'Continuar',
						id: 		'botonContinuar',
						disabled:true,
						handler: function(boton, evento) {

										var  gridFolios = Ext.getCmp('gridFolios');
										var store = gridFolios.getStore();
										//store.commitChanges();
										var flag = false;
										var mensaje = "";
										Ext.StoreMgr.key('registrosAceptadosDataStore').removeAll();
										Ext.StoreMgr.key('registrosRechazadosDataStore').removeAll();
										
										var respuesta = new Object();
										respuesta['folios']				= [];
										respuesta['montoGaran']			= [];
										respuesta['aceptados']			= [];
										respuesta['rechazados']			= [];
										respuesta['causaRechazo']		= [];
										respuesta['registrosAcepta']	= [];
										respuesta['registrosRechaza']	= [];

										var listaAceptados	= store.query('ACEPTADOS', true);
										var listaRechazados	= store.query('RECHAZADOS', true);
										
										if(listaAceptados.length > 0){
										}else if(listaRechazados.length > 0){
										}else{
											Ext.Msg.alert(boton.text,'Es necesario seleccionar al menos un registro <br>para ejecutar la operaci�n.');
											return;
										}
										infoAcuse.registrosRechaza = [];
										infoAcuse.registrosAcepta	= [];
										var index = 0;
										store.each(function(rec){
											/*if(	rec.get('ACEPTADOS') == true	&&	rec.get('RECHAZADOS') == true	){
												Ext.Msg.alert(boton.text,'Es necesario seleccionar solo una opci�n <br>Confirmar o Rechazar');
												flag = true;
												return false;
											}*/
											if(	rec.get('RECHAZADOS')	){
												if(	Ext.isEmpty(rec.get('CAUSA_RECHAZO'))	){
													Ext.Msg.alert(boton.text,
															'Es necesario capturar las causas de rechazo.',
															function(){
																var col = gridFolios.getColumnModel().findColumnIndex('CAUSA_RECHAZO');
																gridFolios.startEditing(index, col);
															}
													);
													flag = true;
													return false;
												}
												infoAcuse.registrosRechaza.push(rec.data);
												respuesta.registrosRechaza.push(rec);
											}
											if(	rec.get('ACEPTADOS')	){
												infoAcuse.registrosAcepta.push(rec.data);
												respuesta.registrosAcepta.push(rec);
											}
											if(	rec.get('ACEPTADOS')	||	rec.get('RECHAZADOS')		){
												respuesta.folios.push(		rec.get('FOLIO_OPERACION')	);
												respuesta.montoGaran.push(	rec.get('MONTO_GARANTIA')	);
												respuesta.aceptados.push(	rec.get('ACEPTADOS')			);
												respuesta.rechazados.push(	rec.get('RECHAZADOS')		);
												respuesta.causaRechazo.push(rec.get('CAUSA_RECHAZO')	);
											}
											index++;
										})

										if(flag){return;}

										Ext.MessageBox.confirm(
											'Confirmaci�n', 
											"�Esta usted seguro de continuar con la operaci�n?", 
											function(confirmBoton){
												if( confirmBoton == "yes" ){
													Ext.StoreMgr.key('registrosAceptadosDataStore').add(respuesta.registrosAcepta);
													Ext.StoreMgr.key('registrosRechazadosDataStore').add(respuesta.registrosRechaza);
													consultaFolios("CONTINUAR_PROCESO",respuesta);
												}
											}
										);
						}
					},'-'
			]
		}
	});

//----------------------- PANEL ACUSE-----------------------------------------------------

	var elementosAcuse = [
		{
			xtype: 	'label',
			id:	 	'labelAcuseCifrasControl',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		80,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			registrosAgregadosData,
						xtype: 			'grid',
						id:				'gridRegistrosAgregados',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '90%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		185,
								hidden: 		false,
								hideable:	false,
								fixed:		true,
								align:		'left',
								renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
														return '<span style="font-weight:bold;">' + value+ '</span>';
												}
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false//,
								//align:		'left',
								//renderer: 	renderContenidoGridAcuse
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{//GRID ACEPTADOS
			store: 			registrosAceptadosData,
			title:			'Contragarant�a Confirmada - Universidad',
			xtype: 			'grid',
			id:				'gridRegistrosAceptados',
			stripeRows: 	true,
			loadMask: 		true,
			height:			200,
			region: 			'center',
			style: {
				borderWidth: 1,
				width: '100%'
			},
			viewConfig: {
				autoFill: 		true,
				forceFit:		true
			},
			columns: [
				{
					header: 		'Intermediario Financiero',
					tooltip: 	'Intermediario Financiero',
					dataIndex: 	'NOMBRE_IF',
					sortable: 	true,
					resizable: 	true,
					width: 		185,
					hidden: 		false,
					hideable:	false,
					fixed:		true,
					renderer:function(value, metadata, record, rowindex, colindex, store) {
									metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
									return value;
								}
				},
				{
					header: 		'Folio Operaci�n',
					tooltip: 	'Folio Operaci�n',
					dataIndex: 	'FOLIO_OPERACION',
					sortable: 	true,
					resizable: 	true,
					//width: 		285,
					hidden: 		false,
					hideable:	false,
					align:		'center'
				},
				{
					header: 		'No. Registros',
					tooltip: 	'No. Registros',
					dataIndex: 	'NUM_ACEPTADAS',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					hidden: 		false,
					hideable:	false,
					align:		'center'
				},
				{
					header: 		'Monto Contragarant�a a pagar',
					tooltip: 	'Monto de contragarant�a a pagar',
					dataIndex: 	'MONTO_GARANTIA',
					sortable: 	true,
					resizable: 	true,
					//width: 		285,
					hidden: 		false,
					hideable:	false,
					align:		'right',
					renderer:	Ext.util.Format.numberRenderer('$0,00.00')
				}
			]
		},
		{
			xtype: 	'box',
			height:	10
		},
		{//GRID PARA LOS RECHAZADOS
			store: 			registrosRechazadosData,
			title:			'Contragarant�a Rechazada - Universidad',
			xtype: 			'grid',
			id:				'gridRegistrosRechazados',
			stripeRows: 	true,
			loadMask: 		true,
			height:			200,
			region: 			'center',
			style: {
				borderWidth: 1,
				width: '100%'
			},
			viewConfig: {
				autoFill: 		true,
				forceFit:		true
			},
			columns: [
				{
					header: 		'Intermediario Financiero',
					tooltip: 	'Intermediario Financiero',
					dataIndex: 	'NOMBRE_IF',
					sortable: 	true,
					resizable: 	true,
					width: 		185,
					hidden: 		false,
					hideable:	false,
					fixed:		true,
					renderer:function(value, metadata, record, rowindex, colindex, store) {
									metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
									return value;
								}
				},
				{
					header: 		'Folio Operaci�n',
					tooltip: 	'Folio Operaci�n',
					dataIndex: 	'FOLIO_OPERACION',
					sortable: 	true,
					resizable: 	true,
					//width: 		285,
					hidden: 		false,
					hideable:	false,
					align:		'center'
				},
				{
					header: 		'No. Registros',
					tooltip: 	'No. Registros',
					dataIndex: 	'NUM_ACEPTADAS',
					sortable: 	true,
					resizable: 	true,
					width: 		80,
					hidden: 		false,
					hideable:	false,
					align:		'center'
				},
				{
					header: 		'Causas de rechazo',
					tooltip: 	'Causas de rechazo',
					dataIndex: 	'CAUSA_RECHAZO',
					sortable: 	true,
					resizable: 	true,
					width: 		200,
					hidden: 		false,
					hideable:	false,
					renderer:function(value, metadata, record, rowindex, colindex, store) {
									metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
									return value;
								}
				}
			]
		},
		{
			xtype: 	'box'
		},
		{
			xtype: 	'label',
			id:	 	'labelAcuseInformacion',
			cls:		'x-form-item',
			style: 	'text-align:center;margin:14px;',
			html:  	''
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					text: 		'Imprimir PDF',
					width: 		150,
					margins: 	' 3',
					iconCls: 	'icoImprimir',
					id: 			'botonImprimirPDF',
					urlArchivo: '',
					handler:    function(boton, evento) {
 
						if(Ext.isEmpty(boton.urlArchivo)){
							
							// Cambiar icono
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							// Genera Archivo PDF
							Ext.Ajax.request({
								url: 			'37validaAportaExt_pdf.jsp',
								params: 		{
									fechaValidacion:	Ext.getCmp('panelAcuse.fechaValidacion').getValue(),
									jsonAceptados:		Ext.encode(infoAcuse.registrosAcepta),
									jsonRechazados:	Ext.encode(infoAcuse.registrosRechaza)
								},
								callback: 	procesarSuccessFailureGeneraArchivoPDF
							});

						// Descargar el archivo generado	
						} else {

							var forma 		= Ext.getDom('formAux');
							forma.action 	= boton.urlArchivo;
							forma.submit();

						}
						
					},
					style: {
						width: 150
					}
				},
				{
					xtype: 		'button',
					text: 		'Aceptar',
					width: 		150,
					margins: 	' 3',
					iconCls: 	'icoAceptar',
					id: 			'botonSalir',
					handler:    function(boton, evento) {
						consultaFolios("FIN",null);
					},
					style: {
						width: 150
					}
				}
			]
		},
		// INPUT HIDDEN: CIFRAS DE CONTROL
		{
        xtype:	'hidden',  
        name:	'panelAcuse.fechaValidacion',
        id: 	'panelAcuse.fechaValidacion'
      },
		// INPUT HIDDEN: FOLIO CARGA
		{
        xtype:	'hidden',  
        name:	'panelAcuse.aceptados',
        id: 	'panelAcuse.aceptados'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{
        xtype:	'hidden',  
        name:	'panelAcuse.rechazados',
        id: 	'panelAcuse.rechazados'
      }
	];

	var panelAcuse = {
		title:			'Acuse',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelAcuse',
		width: 			650, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosAcuse
	}

	var verEstudiantes = new Ext.Window({
		id:				'verEstudiantes',
		width:			940,
		autoHeight:		true,
		maximizable:	false,
		modal:			true,
		closeAction:	'hide',
		resizable:		false,
		maximized:		false,
		constrain:		true,
		renderHidden:	true,
		buttonAlign:	'center',
		items: [
			{
				xtype: 	'label',
				id:	 	'labelFolio',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				html:  	'Folio Operaci�n:'
			},
			{
				xtype: 	'label',
				id:	 	'labelIf',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				html:  	'Intermediario Financiero:'
			},
			{
				xtype: 	'label',
				id:	 	'labelFecha',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				html:  	'Fecha y hora de operaci�n:'
			},
			gridE
			,gridTotalesE
		],
		buttons:[
			{
				xtype:	'button',
				text: 	'Cerrar',
				handler:	function(boton, evento) {
								Ext.getCmp('verEstudiantes').hide();
				}
			}
		]
	});

//-----------------------EVENTO PARA GRID, PARA CACHAR EL CHECKCOLUMN - - - - - - - - - - -
	grid.getStore().on('update', function(store, record, action){
		if(!presionCheck){
			if (action == Ext.data.Record.EDIT) {
				var dato = record.getChanges();
				if(dato.ACEPTADOS	&&	record.get('RECHAZADOS')){
					record.set('RECHAZADOS', false);
				}else if(dato.RECHAZADOS	&&	record.get('ACEPTADOS')){
					record.set('ACEPTADOS', false);
				}
				record.commit();
			}
		}
	});

	var elementosForma = [
		{
			xtype:			'combo',
			id:				'cboIf',
			name:				'claveIf',
			hiddenName:		'claveIf',
			fieldLabel:		'Intermediario Financiero',
			emptyText:		'Seleccionar. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			typeAhead:		true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoIfData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			//Fecha de operaci�n
			xtype:			'compositefield',	
			fieldLabel:		'Fecha de operaci�n',
			combineErrors:	false,
			msgTarget:		'side',
			items: [
				{
					xtype:			'datefield',
					name:				'fecha_ini',
					id:				'_fecha_ini',
					allowBlank:		true,
					startDay:		0,
					width:			100,
					msgTarget:		'side',
					vtype:			'rangofecha', 
					campoFinFecha:	'_fecha_fin',
					margins:	'0 20 0 0'
				},{
					xtype: 'displayfield',	value: 'al',	width: 24
				},{
					xtype:				'datefield',
					name:					'fecha_fin',
					id:					'_fecha_fin',
					allowBlank:			true,
					startDay:			1,
					width:				100,
					msgTarget:			'side',
					vtype:				'rangofecha', 
					campoInicioFecha:	'_fecha_ini',
					margins:				'0 20 0 0'
				}
			]
		},{
			//Folio de operaci�n
			xtype:			'textfield',
			name:				'folioOpera',
			id:				'_folioOpera',
			allowBlank:		true,
			//vtype:			'alphanum',
			fieldLabel:		'Folio de operaci�n',
			anchor:			'60%',
			maxLength:		30,
			maskRe:			/[0-9]/
		}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		title:			'Criterios de b�squeda',
		width:			610,
		style:			'margin:0 auto;',
		collapsible:	true,
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		monitorValid:	true,
		buttons: [
			{
				text:		'Consultar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:true,
				handler: function(boton, evento) {

								if(!Ext.getCmp('forma').getForm().isValid()){
									return;
								}

								var fechaOpMin = Ext.getCmp('_fecha_ini');
								var fechaOpMax = Ext.getCmp('_fecha_fin');
								if (!Ext.isEmpty(fechaOpMin.getValue()) || !Ext.isEmpty(fechaOpMax.getValue()) ) {
									if(Ext.isEmpty(fechaOpMin.getValue()))	{
										fechaOpMin.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
										fechaOpMin.focus();
										return;
									}else if (Ext.isEmpty(fechaOpMax.getValue())){
										fechaOpMax.markInvalid('Debe capturar ambas fechas de operaci�n o dejarlas en blanco');
										fechaOpMax.focus();
										return;
									}
								}

								consultaFolios("CONSULTA_FOLIOS",null);

				} //fin handler
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								consultaFolios("LIMPIAR",null);
							}
			},{
				text:		'Cancelar',
				id:		'btnCancelar',
				iconCls:	'borrar',
				hidden:	true,
				handler:	function(){
								consultaFolios("FIN",null);
							}
			}
		]
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,panelAcuse,
			NE.util.getEspaciador(5),
			grid
		],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Ocultar mascara del panel de resumen de carga de plantillas
			element = Ext.getCmp('gridFolios').getGridEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Derefenrencia ultimo elemento...
			element = null;

		}
	});

	Ext.StoreMgr.key('catalogoIfDataStore').load();
	consultaFolios("CONSULTA_FOLIOS",null);

});
