<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		javax.naming.Context,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		com.netro.educativo.*,
		com.netro.model.catalogos.CatalogoIF_Universidad,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.exception.*
		"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/37creditoEducativo/37secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

log.debug("informacion = <"+informacion+">");

if (informacion.equals("Parametrizacion")) {	//	*-*-*-*-*-*--*-*-*-*-*-*-*-*	Parametrizacion	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


}else if (	informacion.equals("CatalogoIf")) {

	CatalogoIF_Universidad cat = new CatalogoIF_Universidad();
	cat.setCampoClave("i.ic_if");
	cat.setCampoDescripcion("i.cg_razon_social");
	if("ADMIN UNIV".equals(strPerfil)){
		cat.setUniversidad(iNoCliente);
	}else if("ADMIN IF GARANT".equals(strPerfil)){
		cat.setIntermediario(iNoCliente);
	}
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

}else if (	informacion.equals("ConsultarFolios")){

	// Obtener instancia del EJB de CreditoEducativo
	CreditoEducativo creditoEducativo = null;
	try {

		creditoEducativo = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);
	

	} catch(Exception e) {

		log.error("Consulta(Exception): Obtener instancia del EJB de CreditoEducativo");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de CreditoEducativo.");

	}

	String claveUniversidad	= iNoCliente;
	String claveIf				= (request.getParameter("claveIf")!=null)?request.getParameter("claveIf"):"";
	String fecha_ini			= (request.getParameter("fecha_ini")!=null)?request.getParameter("fecha_ini"):"";
	String fecha_fin			= (request.getParameter("fecha_fin")!=null)?request.getParameter("fecha_fin"):"";
	String folioOpera			= (request.getParameter("folioOpera")!=null)?request.getParameter("folioOpera"):"";

	Registros registros = creditoEducativo.getFoliosConfirmar(claveUniversidad, fecha_ini, fecha_fin, folioOpera, claveIf);

	infoRegresar =	"{\"success\": true, \"total\": \"" + 
				registros.getNumeroRegistros() + "\", \"registros\": " + registros.getJSONData()+"}";

}else if (informacion.equals("ContinuaProceso")) {

	// Obtener instancia del EJB de CreditoEducativo
	CreditoEducativo creditoEducativo = null;
	try {
		creditoEducativo = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);
	} catch(Exception e) {
		log.error("Consulta(Exception): Obtener instancia del EJB de CreditoEducativo");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de CreditoEducativo.");
	}

	String folios[]			= request.getParameterValues("folios");
	String montoGaran[]		= request.getParameterValues("montoGaran");
	String aceptados[]		= request.getParameterValues("aceptados");
	String rechazados[]		= request.getParameterValues("rechazados");
	String causaRechazo[]	= request.getParameterValues("causaRechazo");
	String estadoSiguiente	= "RESPUESTA_MOSTRAR_ACUSE";
	boolean success			= true;
	String msg					= "";
	String msjInfo =  "Al transmitir los registros electr&oacute;nicamente, usted est&aacute; confirmando la "    +
							"validez de la informaci&oacute;n y acepta los t&eacute;rminos relacionados con la "        +
							"operaci&oacute;n del Programa Nacional de Financiamiento a la Educaci&oacute;n Superior, " +
							"estando de acuerdo con realizar la aportaci&oacute;n de recursos a Nacional Financiera, "  +
							"S.N.C. por concepto de aportaci&oacute;n de contragarant&iacute;a en los tiempos y "       +
							"t&eacute;rminos que se estipula en el Reglamento Operativo del Programa Nacional de "      +
							"Financiamiento a la Educaci&oacute;n Superior.";
	String fechaVal			= "";
	fechaVal = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new java.util.Date());

	JSONObject jsonObj	= new JSONObject();
	
	jsonObj.put("msjInfo", msjInfo);
	jsonObj.put("fechaVal", fechaVal);

	try {

		creditoEducativo.setValidaAportaciones(folios,aceptados,rechazados,montoGaran, causaRechazo, fechaVal);

	} catch(Exception e) {
		success	= false;
		msg		= e.getMessage();
		log.error("ContinuaProceso(Exception): Error al establecer la validacion de garantias");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al establecer la validacion de aportaciones.");
	}
	

	JSONArray 	registrosAgregadosDataArray 	= new JSONArray();
	JSONArray	registro								= null;

	registro = new JSONArray();
	registro.add("Tipo de Operación:");
	registro.add("Crédito Educativo");
	registrosAgregadosDataArray.add(registro);

	registro = new JSONArray();
	registro.add("Fecha y hora de validación:");
	registro.add(fechaVal);
	registrosAgregadosDataArray.add(registro);

	registro = new JSONArray();
	registro.add("Usuario:");
	registro.add(iNoUsuario+" - "+strNombreUsuario);
	registrosAgregadosDataArray.add(registro);

	jsonObj.put("registrosAgregadosDataArray",	registrosAgregadosDataArray);

	// Especificar el estado siguiente
	jsonObj.put("estadoSiguiente",	estadoSiguiente );

	// En caso de que haya alguno mensaje
	jsonObj.put("msg",					msg);	
	
	jsonObj.put("success", new Boolean(success));
	infoRegresar = jsonObj.toString();

}else if (	informacion.equals("ConsultaE") ) {

	int start = 0;
	int limit = 0;

	ConsultaRegistroIndividual paginador =new  ConsultaRegistroIndividual();
	String folioOperacion	=	(request.getParameter("folioOperacion")!=null)?request.getParameter("folioOperacion"):"";
	String estatusReg			=	(request.getParameter("estatusReg")!=null)?request.getParameter("estatusReg"):"";

	paginador.setEstatusReg(estatusReg);
	paginador.setFolio(folioOperacion);
	paginador.setCalculaMonto(true);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if (informacion.equals("ConsultaE")) {	//Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request);
		}
		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
		}
	}

} else if (informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>