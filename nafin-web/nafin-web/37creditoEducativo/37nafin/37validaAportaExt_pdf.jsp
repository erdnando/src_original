<%@ page import="java.text.*, java.util.*, 
				netropology.utilerias.*, 
				com.netro.exception.*,	
				javax.naming.*,
				org.apache.commons.logging.Log,
				net.sf.json.*, 
				java.io.*, 
				com.netro.educativo.*,
				com.netro.pdf.*"
				contentType="application/json;charset=UTF-8"
				errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/37creditoEducativo/37secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

JSONObject jsonObj = new JSONObject();
try {
	//	DECLARACION DE VARIABLES
	//	Parámetros provenienen de la página anterior
	String fechaValidacion	= (request.getParameter("fechaValidacion")!=null)?request.getParameter("fechaValidacion"):"";
	String jsonAceptados		= (request.getParameter("jsonAceptados")!=null)?request.getParameter("jsonAceptados"):"";
	String jsonRechazados	= (request.getParameter("jsonRechazados")!=null)?request.getParameter("jsonRechazados"):"";

	String nombreArchivo = "";

	List arrRegistrosAcepta		= JSONArray.fromObject(jsonAceptados);
	List arrRegistrosRechaza	= JSONArray.fromObject(jsonRechazados);

	Iterator itRegAcepta			= arrRegistrosAcepta.iterator();
	Iterator itRegRechaza		= arrRegistrosRechaza.iterator();

	CreaArchivo archivo = new CreaArchivo();
	nombreArchivo = archivo.nombreArchivo()+".pdf";
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
											session.getAttribute("iNoNafinElectronico").toString(),
											(String)session.getAttribute("sesExterno"),
											(String) session.getAttribute("strNombre"),
											(String) session.getAttribute("strNombreUsuario"),
											(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

	pdfDoc.setTable(2,70);
	pdfDoc.setCell("Cifras de control","celda01",ComunesPDF.CENTER,2,2);
	pdfDoc.setCell("Tipo de Operación:","formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Crédito Educativo","formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Fecha y hora de validación:","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(fechaValidacion,"formas",ComunesPDF.LEFT);
	pdfDoc.setCell("Usuario:","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(iNoUsuario+" "+strNombreUsuario,"formas",ComunesPDF.LEFT);

	pdfDoc.addTable();
	
	pdfDoc.setTable(4,100);
	pdfDoc.setCell("Contragarantía confirmada - Universidad","celda01",ComunesPDF.CENTER,4);
	pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Folio de Operación","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("No. Registros","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto de Contragarantía a pagar","celda01",ComunesPDF.CENTER);
	int count = 0;
	while (itRegAcepta.hasNext()) {
		JSONObject registro = (JSONObject)itRegAcepta.next();			
		pdfDoc.setCell(registro.getString("NOMBRE_IF"),"formas",ComunesPDF.LEFT);
		pdfDoc.setCell(registro.getString("FOLIO_OPERACION"),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(registro.getString("NUM_ACEPTADAS"),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(registro.getString("MONTO_GARANTIA"),"formas",ComunesPDF.RIGHT);
		count++;
	} //fin del while
	if (count == 0){
		pdfDoc.setCell("No existen registros","formas",ComunesPDF.CENTER,4);
	}
	pdfDoc.addTable();
	//pdfDoc.endDocument();

	pdfDoc.setTable(4,100);
	pdfDoc.setCell("Contragarantía Rechazada - Universidad","celda01",ComunesPDF.CENTER,4);
	pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Folio de Operación","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("No. Registros","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Causas de Rechazo","celda01",ComunesPDF.LEFT);
	
	count = 0;
	while (itRegRechaza.hasNext()) {
		JSONObject registro = (JSONObject)itRegRechaza.next();			
		pdfDoc.setCell(registro.getString("NOMBRE_IF"),"formas",ComunesPDF.LEFT);
		pdfDoc.setCell(registro.getString("FOLIO_OPERACION"),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(registro.getString("NUM_ACEPTADAS"),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(registro.getString("CAUSA_RECHAZO"),"formas",ComunesPDF.RIGHT);
		count++;
	} //fin del while
	if (count == 0){
		pdfDoc.setCell("No existen registros","formas",ComunesPDF.CENTER,4);
	}
	pdfDoc.addTable();
	//pdfDoc.endDocument();

	String msjInfo =  "Al transmitir los registros electrónicamente, usted está confirmando la validez de la información    \n" +
							"y acepta los términos relacionados con la operación del Programa Nacional de Financiamiento a la     \n" +
							"Educación Superior, estando de acuerdo con realizar la aportación de recursos a Nacional Financiera, \n" +
							"S.N.C. por concepto de aportación de contragarantía en los tiempos y términos que se estipula en el  \n" +
							"Reglamento Operativo del Programa Nacional de Financiamiento a la Educación Superior.";

	pdfDoc.setTable(1,70);
	pdfDoc.setCell(msjInfo,"formas",ComunesPDF.CENTER);
	pdfDoc.addTable();
	pdfDoc.endDocument();

	log.debug("strDirecVirtualTemp+nombreArchivo == "+strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	e.printStackTrace();
	jsonObj.put("success", new Boolean(false));
	jsonObj.put("msg", "Error al generar el archivo");
}
%>

<%=jsonObj%>