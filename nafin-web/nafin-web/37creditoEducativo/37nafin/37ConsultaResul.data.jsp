<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		com.netro.model.catalogos.*,	
		com.netro.educativo.ConsultaRegistros,
		net.sf.json.JSONArray,
		com.netro.educativo.*,
		com.netro.descuento.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
	<%@ include file="/37creditoEducativo/37secsession.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String icGrupo= (request.getParameter("HGrupo")!=null)?request.getParameter("HGrupo"):"";
String icIF= (request.getParameter("HIF")!=null)?request.getParameter("HIF"):"";
String cEstatus= (request.getParameter("HEstatus")!=null)?request.getParameter("HEstatus"):"";
String icUni= (request.getParameter("HUniversidad")!=null)?request.getParameter("HUniversidad"):"";
String folio= (request.getParameter("folio")!=null)?request.getParameter("folio"):"";
String fechaVIni= (request.getParameter("fechaVal1")!=null)?request.getParameter("fechaVal1"):"";
String fechaVFin= (request.getParameter("fechaVal2")!=null)?request.getParameter("fechaVal2"):"";
String fechaCIni= (request.getParameter("fechaConf1")!=null)?request.getParameter("fechaConf1"):"";
String fechaCFin= (request.getParameter("fechaConf2")!=null)?request.getParameter("fechaConf1"):"";

String infoRegresar ="";
JSONObject jsonObj = new JSONObject();

if (informacion.equals("CatologoIF")  ) {

	CatalogoIF_Universidad cat = new CatalogoIF_Universidad();
	cat.setCampoClave("i.ic_if");
	cat.setCampoDescripcion("i.cg_razon_social");
	if("ADMIN UNIV".equals(strPerfil)){
		cat.setUniversidad(iNoCliente);
	}else if("ADMIN IF GARANT".equals(strPerfil)){
		cat.setIntermediario(iNoCliente);
	}
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();	

}else if (informacion.equals("CatologoGrupo")  ) {
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_GRUPO");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("CECAT_GRUPO");		
	catalogo.setOrden("IC_GRUPO");
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("catologoEstatus")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_ESTATUS_CRED_EDUCA");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("CECAT_ESTATUS_CRED_EDUCA");		
	catalogo.setOrden("IC_ESTATUS_CRED_EDUCA");
	String estatus="4,6,7";
	catalogo.setValoresCondicionIn(estatus, Integer.class);
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("CatologoUniversidad")  ) {
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_UNIVERSIDAD");
	String cad="";
	if(!icGrupo.equals("")){
		cad="ic_grupo";
		catalogo.setCampoLlave(cad);
		catalogo.setValoresCondicionIn(icGrupo,Integer.class);
		}
	catalogo.setCampoDescripcion("CG_RAZON_SOCIAL||' - '|| CG_NOMBRE_CAMPUS ");
	catalogo.setTabla("COMCAT_UNIVERSIDAD");		
	catalogo.setOrden("IC_UNIVERSIDAD");
	
	infoRegresar = catalogo.getJSONElementos();	
} else if (informacion.equals("Agregar")  ) {



}else if (informacion.equals("Consulta")||informacion.equals("ArchivoCSV")  ) {
int start = 0;
int limit = 0;

	ConsultaRegistros paginador =new  ConsultaRegistros();
	paginador.setIntermediario(icIF);
	paginador.setUniversidad(icUni);
	paginador.setGrupo(icGrupo);
	paginador.setEstatus(cEstatus);
	paginador.setFolio(folio);
	paginador.setFechaValidaIni(fechaVIni);
	paginador.setFechaValidaFin(fechaVFin);
	paginador.setFechaOperaIni(fechaCIni);
	paginador.setFechaOperaFin(fechaCFin);
	if("ADMIN UNIV".equals(strPerfil)){
		paginador.setUniversidad(iNoCliente);
	}
	
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	queryHelper.setMultiplesPaginadoresXPagina(true);
		if (informacion.equals("Consulta")) {	//Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
			}
		}else if(informacion.equals("ArchivoCSV")){
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
}else if (informacion.equals("ConsultaE")||informacion.equals("ArchivoCSVE")  ) {
int start = 0;
int limit = 0;

	ConsultaRegistroIndividual paginador =new  ConsultaRegistroIndividual();
	String folioPag= (request.getParameter("folioPag")!=null)?request.getParameter("folioPag"):"";
	String estatusReg= (request.getParameter("estatusReg")!=null)?request.getParameter("estatusReg"):"";
	
	paginador.setEstatusReg(estatusReg);
	paginador.setFolio(folioPag);

	
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	queryHelper.setMultiplesPaginadoresXPagina(true);
		if (informacion.equals("ConsultaE")) {	//Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
			}
		}else if(informacion.equals("ArchivoCSVE")){
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
}else if(informacion.equals("Totales")){
		CQueryHelperRegExtJS queryHelperE = new CQueryHelperRegExtJS(new ConsultaRegistros()); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	queryHelperE.setMultiplesPaginadoresXPagina(true);
	infoRegresar  = queryHelperE.getJSONResultCount(request);	//los saca de sesion

	
}else if(informacion.equals("TotalesE")){
	
	CQueryHelperRegExtJS queryHelperE = new CQueryHelperRegExtJS(new ConsultaRegistroIndividual()); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	queryHelperE.setMultiplesPaginadoresXPagina(true);
	infoRegresar  = queryHelperE.getJSONResultCount(request);
	
}

%>

<%=infoRegresar%>
