<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/37creditoEducativo/37secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>

<%if(esEsquemaExtJS) {%>
<%@ include file="/01principal/menu.jspf"%>
<%} %>

<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigAmount.js"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>

<script type="text/javascript" src="37validaGarantExt.js?<%=session.getId()%>"></script>

</head>

<%if(esEsquemaExtJS &&  strTipoUsuario.equals("UNIV")  ) {%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%@ include file="/01principal/01uni/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01uni/menuLateralFlotante.jspf"%>
	<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01uni/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
<%}else  if(esEsquemaExtJS &&  strTipoUsuario.equals("NAFIN")  ) {%>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">	
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
				<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>			
	</body>
<%} else {%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<div id="_menuApp"></div>
	<div id="Contcentral">	
	<div id="areaContenido"></div>						
	</div>
	</div>	
	<form id='formAux' name="formAux" target='_new'></form>
</body>
<%} %>
</html>