Ext.onReady(function(){
var banderaUniv=false;
var folioAc;
var estatusAc;
if(Ext.get('_strPerfil').getValue()=='ADMIN UNIV')
	banderaUniv=true;
//-------------------------Handlers-----------------------------
var procesarConsultaDataE = function(store, arrRegistros, opts) {
		fp.el.unmask();
		var boton = Ext.getCmp('btnGenerarArchivoE');
		
		if (arrRegistros != null) {
			
			verEstudiantes.show();
			
			var el = gridE.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
				boton.enable();
				Ext.Ajax.request({
				url: '37ConsultaResul.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'TotalesE'})
				, callback: procesaTotalesE
				});

			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				boton.disable();
			}
		}
	}

var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		var boton = Ext.getCmp('btnGenerarArchivo');
		if (arrRegistros != null) {

			if (!grid.isVisible()) {
				grid.show();
				
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
				Ext.Ajax.request({
					url: '37ConsultaResul.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Totales'})
					, callback: procesaTotales
				});
				boton.enable();
			} else {
				gridTotales.setVisible(false);
				el.mask('No se encontr� ning�n registro', 'x-mask');
				boton.disable();
			}
		}
	}
function cambiaTitle(registro,titulo){
	verEstudiantes.setTitle(titulo);
	folioAc=registro.get('IC_FOLIO');
	

	Ext.getCmp('labelFolio').setText('Folio Operaci�n: '				+registro.get('IC_FOLIO'),	false);
			Ext.getCmp('labelIf').setText(	'Intermediario Financiero: '	+registro.get('CD_IF'),		false);
			Ext.getCmp('labelFecha').setText('Fecha y Hora de Operaci�n:'	+registro.get('FECHA_OPER'),	false);
}
var procesarEAceptado = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
		estatusAc='S';
		cambiaTitle(registro,'Operaciones Aceptadas');
		cm = gridE.getColumnModel();
		cm.setHidden(cm.findColumnIndex('FG_PORCENTAJE_APORTACION'), false);
		cm.setHidden(cm.findColumnIndex('FN_MONTO_GARANTIA'), false);
		
		consultaE.load({ params: Ext.apply({estatusReg: 'S',folioPag: registro.get('IC_FOLIO'),operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15 })});
		verEstudiantes.show();
		gridTotalesE.show();
		
	}
var procesarERechazado = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
		estatusAc='N';
		cambiaTitle(registro,'Operaciones Rechazadas');
		cm = gridE.getColumnModel();
		cm.setHidden(cm.findColumnIndex('FG_PORCENTAJE_APORTACION'), true);
		cm.setHidden(cm.findColumnIndex('FN_MONTO_GARANTIA'), true);
		
		consultaE.load({ params: Ext.apply({estatusReg: 'N',folioPag: registro.get('IC_FOLIO'),operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15 })});
		verEstudiantes.show();
		gridTotalesE.setVisible(false);

	}

function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnGenerarArchivo');
			boton.setIconClass('');
			boton.enable();
			
			var botonE=Ext.getCmp('btnGenerarArchivoE');
			botonE.setIconClass('');
			botonE.enable();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
function procesaTotales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridTotales.setVisible(true);
			totalesData.loadData(Ext.util.JSON.decode(response.responseText).registros);
		} else {
		gridTotales.setVisible(false);
			NE.util.mostrarConnError(response,opts);
		}
	}
function procesaTotalesE(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			totalesDataE.loadData(Ext.util.JSON.decode(response.responseText).registros);
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambas Fechas son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambas Fechas son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	
//------------------Stores-----------------------------------------

	var totalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'ESTATUS'},
			{name: 'REGISTROS'},
			{name: 'TOTAL_VALIDO'},
			{name: 'TOTAL_CONTRA'}
		],
		autoLoad: false,	listeners: {exception: NE.util.mostrarDataProxyError}
	});
	
	var totalesDataE = new Ext.data.JsonStore({
		fields: [
			{name: 'REGISTROS'},
			{name: 'TOTAL_IMPORTE'},
			{name: 'TOTAL_MONTO'},
			{name: 'TOTAL_SALDO'},
			{name: 'TOTAL_CONTRA'}
		],
		autoLoad: false,	listeners: {exception: NE.util.mostrarDataProxyError}
	});
var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '37ConsultaResul.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'CD_IF'},
				{name: 'UNIVERSIDAD'},
				{name: 'CAMPUS'},
				{name: 'FECHA_OPER'},
				{name: 'IC_FOLIO'},
				{name: 'FN_MONTO_CONTRAGAN'},
				{name: 'IN_REGISTROS_ACEP'},
				{name: 'IN_REGISTROS_RECH'},
				{name: 'IN_REGISTROS'},
				{name: 'FN_FINANCIAMIENTO'},
				{name: 'ESTATUS'},
				{name: 'CG_CAUSA_RECHAZO'},
				{name: 'DF_DEPOSITO'},
				{name: 'FN_IMPORTE'},
				{name: 'CG_NOMBRE_BANCO'},
				{name: 'CG_CLAVE_REFERENCIA'},
				{name: 'MODALIDAD_BASES_OPER'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
						}	},
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});


var consultaE = new Ext.data.JsonStore({
	root: 'registros',
	url: '37ConsultaResul.data.jsp',
	baseParams: {
		informacion: 'ConsultaE'
	},
	fields: [
				{name: 'CREDI'},
				{name: 'NOMBRE'},
				{name: 'RFC'},
				{name: 'MATRICULA'},
				{name: 'ESTUDIOS'},
				{name: 'FECHA_INGRESO'},
				{name: 'ANIO'},
				{name: 'PERIODO'},
				{name: 'MODALIDAD'},
				{name: 'FECHA_DIS'},
				{name: 'NUM_DIS'},
				{name: 'IMPORTE'},
				{name: 'MONTO_TOTAL'},
				{name: 'SALDOLINEAS'},
				{name: 'FG_PORCENTAJE_APORTACION'},
				{name: 'FN_MONTO_GARANTIA'},
				{name: 'MODALIDAD_BASES_OPER'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		beforeLoad:	{fn: function(store, options){
							Ext.apply(options.params, { params: Ext.apply(fp.getForm().getValues())});
						}	},
		load: procesarConsultaDataE,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaDataE(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var catalogoIF = new Ext.data.JsonStore
  ({
	   id: 'catologoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '37ConsultaResul.data.jsp',
		baseParams: 
		{
		 informacion: 'CatologoIF'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var catalogoUni = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '37ConsultaResul.data.jsp',
		baseParams: 
		{
		 informacion: 'CatologoUniversidad'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var catalogoEstatus = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '37ConsultaResul.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoEstatus'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

  var catalogoGrupo = new Ext.data.JsonStore
  ({
	   id: 'catologoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '37ConsultaResul.data.jsp',
		baseParams: 
		{
		 informacion: 'CatologoGrupo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

//-----------------------GRIDS------------------------

var gridE = new Ext.grid.GridPanel({
				id: 'gridEs',
				hidden: false,
				header:true,
				store: consultaE,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
					
						{
						header: 'Cr�dito',
						tooltip: 'Cr�dito',
						sortable: true,
						dataIndex: 'CREDI',
						width: 150,
						align: 'center'
						},
						{
						header: 'Nombre de Acreditado',
						tooltip: 'Nombre de Acreditado',
						sortable: true,
						dataIndex: 'NOMBRE',
						width: 150,
						align: 'center'
						},
						{
						header: 'RFC',
						tooltip: 'RFC',
						sortable: true,
						dataIndex: 'RFC',
						width: 150,
						align: 'center'
						},
						{
						header: 'Matric�la',
						tooltip: 'Matric�la',
						sortable: true,
						dataIndex: 'MATRICULA',
						width: 130,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'Estudios',
						tooltip: 'Estudios',
						sortable: true,
						dataIndex: 'ESTUDIOS',
						width: 130
						},{
						
						header:'Fecha de Ingreso',
						tooltip: 'Fecha de Ingreso',
						sortable: true,
						dataIndex: 'FECHA_INGRESO',
						width: 130,
						align: 'center'
						},
						{
						header: 'A�o de Colocaci�n',
						tooltip: 'A�o de Colocaci�n',
						sortable: true,
						dataIndex: 'ANIO',
						width: 150,
						align: 'center'
						},
						{
						header: 'Periodo Educativo',
						tooltip: 'Periodo Educativo',
						sortable: true,
						dataIndex: 'PERIODO',
						width: 130,
						align: 'center'
						},{
						header: 'Modalidad Programa',
						tooltip: 'Modalidad Programa',
						sortable: true,
						dataIndex: 'MODALIDAD',
						width: 130,
						align: 'center'
						},
						{
						header: 'Fecha �ltima Disposici�n',
						tooltip: 'Fecha �ltima Disposici�n',
						sortable: true,
						dataIndex: 'FECHA_DIS',
						width: 150,
						align: 'center'
						},
						{
						header: 'No. Disposiciones',
						tooltip: 'No. Disposiciones',
						sortable: true,
						dataIndex: 'NUM_DIS',
						width: 130,
						align: 'center'
						},
						{
						align:'right',
						header: 'Importe Disposici�n',
						tooltip: 'Importe Disposici�n',
						sortable: true,
						dataIndex: 'IMPORTE',
						width: 130,
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: 'Monto Total',
						tooltip: 'Monto Total',
						sortable: true,
						dataIndex: 'MONTO_TOTAL',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: 'Saldo de Linea',
						tooltip: 'Saldo de Linea',
						sortable: true,
						dataIndex: 'SALDOLINEAS',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},{
						header: 'Modalidad',
						tooltip: 'Modalidad',
						sortable: true,
						dataIndex: 'MODALIDAD_BASES_OPER',
						width: 130,
						align: 'center'
						},
						{
						header: '% de aportaci�n',
						tooltip: '% de aportaci�n',
						sortable: true,
						dataIndex: 'FG_PORCENTAJE_APORTACION',
						width: 150,
						align: 'center',
						renderer: Ext.util.Format.numberRenderer('00.00%')
						},
						{
						header: 'Monto de Contragarant�a a Pagar',
						tooltip: 'Monto de Contragarant�a a Pagar',
						sortable: true,
						dataIndex: 'FN_MONTO_GARANTIA',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						}
						
						
				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 930,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
					height: 30,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion1',
					displayInfo: true,
					store: consultaE,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					
					items: ['->','-',
								
								{
									xtype: 'button',
									//height: 40,
									text: 'Generar Archivo',
									id: 'btnGenerarArchivoE',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '37ConsultaResul.data.jsp',
											params: Ext.apply({
												folioPag:folioAc,
												estatusReg:estatusAc,
												informacion: 'ArchivoCSVE'})
											, callback: procesarArchivoSuccess
										});
									}
								},
								'-']
				}
		});

var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: true,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
					
						{
						header: 'Intermediario Financiero',
						tooltip: 'Intermediario Financiero',
						sortable: true,
						dataIndex: 'CD_IF',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Universidad',
						tooltip: 'Universidad',
						sortable: true,
						dataIndex: 'UNIVERSIDAD',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						header: 'Campus',
						tooltip: 'Campus',
						sortable: true,
						dataIndex: 'CAMPUS',
						width: 150,
						align: 'center'
						},
						{
						header: 'Fecha y Hora de la Operacion',
						tooltip: 'Fecha y Hora de la Operacion',
						sortable: true,
						dataIndex: 'FECHA_OPER',
						width: 130,
						align: 'center'
						
						},
						{
						align:'center',
						header: 'Folio de la Operaci�n',
						tooltip: 'Folio de la Operaci�n',
						sortable: true,
						dataIndex: 'IC_FOLIO',
						width: 130
						},{
						
						header:'<center>Monto Valido</center>',
						tooltip: 'Monto Valido',
						sortable: true,
						dataIndex: 'FN_FINANCIAMIENTO',
						width: 130,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						xtype: 'actioncolumn',
						header: 'No. Operaciones Aceptadas',
						tooltip: 'No. Operaciones Aceptadas',
						sortable: true,
						dataIndex: 'IN_REGISTROS_ACEP',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value!='')
									return value;
								else
									return '0';
							},
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       if(banderaUniv){
										 if(value!=''&&value!='0')
											return 'iconoLupa';	
										 else 
											return '';
									 }
									 else{
										return '';
									 }
								 }
								 ,handler: procesarEAceptado
					      }]
						},
						{
						xtype: 'actioncolumn',
						header: 'No. Operaciones Rechazadas',
						tooltip: 'No. Operaciones Rechazadas',
						sortable: true,
						dataIndex: 'IN_REGISTROS_RECH',
						width: 130,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value!='')
									return value;
								else
									return '0';
							},
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';		
									 if(banderaUniv){
										 if(value!=''&&value!='0')
											return 'iconoLupa';	
										 else 
											return '';
									 }
									 else{
										 return '';
									 }
								 }
								 ,handler: procesarERechazado
					      }]
						},{
						header: 'Total de Operaciones',
						tooltip: 'Total de Operaciones',
						sortable: true,
						dataIndex: 'IN_REGISTROS',
						width: 130,
						align: 'center'
						},
						{
						header: '<center>Monto de Contragarant�a a pagar</center>',
						tooltip: 'Monto de Contragarant�a a pagar',
						sortable: true,
						dataIndex: 'FN_MONTO_CONTRAGAN',
						width: 150,
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
						header: '<center>Estatus</center>',
						tooltip: 'Estatus',
						sortable: true,
						dataIndex: 'ESTATUS',
						width: 130,
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						align:'center',
						header: 'Causas del Rechazo',
						tooltip: 'Causas del Rechazo',
						sortable: true,
						dataIndex: 'CG_CAUSA_RECHAZO',
						width: 130,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								if(value=='')
								return 'N/A';
								else
								return value;
							}
						},
						{
						header: 'Fecha del Dep�sito',
						tooltip: 'Fecha del Dep�sito',
						sortable: true,
						dataIndex: 'DF_DEPOSITO',
						width: 130,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value=='')
								return 'N/A';
								else
								return value;
							}
						
						},
						{
						header: 'Importe del Dep�sito',
						tooltip: 'Importe del Dep�sito',
						sortable: true,
						dataIndex: 'FN_IMPORTE',
						width: 130,
						align: 'right',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value=='')
								return 'N/A';
								else
								return Ext.util.Format.number(value,'$0,0.00');
							}
						},
						{
						header: 'Banco de Dep�sito',
						tooltip: 'Banco de Dep�sito',
						sortable: true,
						dataIndex: 'CG_NOMBRE_BANCO',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value=='')
								return 'N/A';
								else
								return value;
							}
						},
						{
						header: 'Clave Referencia',
						tooltip: 'Clave Referencia',
						sortable: true,
						dataIndex: 'CG_CLAVE_REFERENCIA',
						width: 150,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value=='')
								return 'N/A';
								else
								return value;
							}
						}
						
						
				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
					height: 30,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					
					items: ['->','-',
							
								{
									xtype: 'button',
									//height: 40,
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '37ConsultaResul.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'ArchivoCSV'})
											, callback: procesarArchivoSuccess
										});
									}
								},
								'-']
				}
		});
	var gridTotalesE =  new Ext.grid.GridPanel({
		xtype:'grid',	store:totalesDataE,	id:'gridTotalesE',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:930,	height:100,	title:'Totales', hidden:false,	frame: false,
		columns: [
			{
				header: 'Numero de Registros',	dataIndex: 'REGISTROS',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'Importe Disposici�n',	dataIndex: 'TOTAL_IMPORTE',	width: 100,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,0.00'),menuDisabled:true
			},{
				header: 'Monto Total',dataIndex: 'TOTAL_MONTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Saldo Lineas',	dataIndex: 'TOTAL_SALDO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto de Contragarant�a<br/> a pagar',	dataIndex: 'TOTAL_CONTRA',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		]
		
		
	});
	var gridTotales =  new Ext.grid.GridPanel({
		xtype:'grid',	store:totalesData,	id:'gridTotales',	view:new Ext.grid.GridView({forceFit:true,markDirty: false}),	width:940,	height:150,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'Estatus',	dataIndex: 'ESTATUS',	align: 'left',	width: 200, menuDisabled:true
			},{
				header: 'N�mero de Registros',	dataIndex: 'REGISTROS',	width: 100,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),menuDisabled:true
			},{
				header: 'Monto Valido',dataIndex: 'TOTAL_VALIDO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto de Contragarant�a<br/>a pagar',	dataIndex: 'TOTAL_CONTRA',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		]
		
	});
//-------------------------------------------
var elementosForma= [
	{
						
						xtype: 'combo',
						width: 230,
						emptyText: 'Seleccionar',
						fieldLabel: 'Intermediario Financiero',
						displayField: 'descripcion',
						valueField: 'clave',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: catalogoIF,
						tpl: NE.util.templateMensajeCargaCombo,
						name:'HIF',
						id: 'IF',
						mode: 'local',
						hiddenName: 'HIF',
						//allowBlank: false,
						forceSelection: true
				},
				{
						xtype: 'combo',
						width: 230,
						emptyText: 'Seleccionar Grupo',
						fieldLabel: 'Grupo',
						displayField: 'descripcion',
						valueField: 'clave',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: catalogoGrupo,
						tpl: NE.util.templateMensajeCargaCombo,
						name:'HGrupo',
						id: 'grupo',
						mode: 'local',
						hiddenName: 'HGrupo',
						//allowBlank: false,
						forceSelection: true,
						listeners: {
							select: function(combo, record, index) {
								Ext.getCmp('universidad').reset();
								
								catalogoUni.load({
											params:{											
												HGrupo:Ext.getCmp('grupo').getValue()												
												}
											});
							}
						}
				},
				{
						
						xtype: 'combo',
						width: 230,
						emptyText: 'Seleccionar una Universidad',
						fieldLabel: 'Universidad - Campus',
						displayField: 'descripcion',
						valueField: 'clave',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: catalogoUni,
						tpl: NE.util.templateMensajeCargaCombo,
						name:'HUniversidad',
						id: 'universidad',
						mode: 'local',
						hiddenName: 'HUniversidad',
						//allowBlank: false,
						forceSelection: true
				},
				{
						
						xtype: 'combo',
						width: 230,
						emptyText: 'Seleccionar Estatus',
						fieldLabel: 'Estatus',
						displayField: 'descripcion',
						valueField: 'clave',
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						store: catalogoEstatus,
						tpl: NE.util.templateMensajeCargaCombo,
						name:'HEstatus',
						id: 'estatus',
						mode: 'local',
						hiddenName: 'HEstatus',
						//allowBlank: false,
						forceSelection: true
					},{
						name: 'folio',
						id: 'folio',
						fieldLabel: 'Folio Operaci�n',
						anchor:'93%',
						maxLength: 30
					},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha Validaci�n<br/>(Universidad)',
							combineErrors: false,
							id: 'fechav',
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaVal1',
									id: 'fechaVal1',
							
									allowBlank: true,
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaVal2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaVal2',
									id: 'fechaVal2',
									
									allowBlank: true,
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaVal1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Confirmaci�n<br/>(NAFIN)',
							combineErrors: false,
							msgTarget: 'side',
							id:'fechac',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaConf1',
									id: 'fechaConf1',
							
									allowBlank: true,
									startDay: 0,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaConf2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaConf2',
									id: 'fechaConf2',
									
									allowBlank: true,
									startDay: 1,
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoInicioFecha: 'fechaConf1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						}
];
var verEstudiantes = new Ext.Window({
			width: 940,
			height: 700,
			maximizable: false,
			modal: true,
			closeAction: 'hide',
			resizable: false,
			maximized: false,
			constrain: true,
			items: [{
				xtype: 	'label',
				id:	 	'labelFolio',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				html:  	'Folio Operaci�n:'
			},
			{
				xtype: 	'label',
				id:	 	'labelIf',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				html:  	'Intermediario Financiero:'
			},
			{
				xtype: 	'label',
				id:	 	'labelFecha',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				html:  	'Fecha y hora de operaci�n:'
			},gridE,gridTotalesE],
			buttons:[
				{
							xtype: 'button',
							text: 			'Regresar',
							handler:    function(boton, evento) {
								verEstudiantes.setVisible(false);
							}
						}
			]
});	
var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 500,
		collapsible: false,
		titleCollapse: false,
		monitorValid: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						

						if(verificaFechas('fechaVal1','fechaVal2')&&verificaFechas('fechaConf1','fechaConf2')){
						//gridTotales.hide();
						fp.el.mask('cargando','x-mask');
						grid.show();
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15})
								});
						}
						/*//Mi acci�n
						grid.show();
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{txt_ne: Ext.getCmp('txt_ne').getValue()})
						});				
*/
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});

//----------------Principal------------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  width: 940,
	  items: 
	    [ 
	  
		fp ,NE.util.getEspaciador(10),grid,NE.util.getEspaciador(10),gridTotales,verEstudiantes
		 ]
  });
		catalogoIF.load();
		catalogoUni.load();
		catalogoEstatus.load()
		catalogoGrupo.load();
		if(banderaUniv){
			Ext.getCmp('fechav').hide();
			Ext.getCmp('grupo').hide();
			Ext.getCmp('fechac').hide();
			Ext.getCmp('universidad').hide();
			cm = grid.getColumnModel();
			cm.setHidden(cm.findColumnIndex('UNIVERSIDAD'), true);
			cm.setHidden(cm.findColumnIndex('CAMPUS'), true);
		}
});