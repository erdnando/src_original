<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.sql.*,
		java.text.SimpleDateFormat,
		netropology.utilerias.*,			
		netropology.utilerias.usuarios.*, 
		com.netro.model.catalogos.*,	
		com.netro.educativo.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/37creditoEducativo/37secsession_extjs.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String radio = (request.getParameter("radio")!=null)?request.getParameter("radio"):"";
String ifs = (request.getParameter("ifs")!=null)?request.getParameter("ifs"):"";
String universidad = (request.getParameter("ic_universidad")!=null)?request.getParameter("ic_universidad"):"";
String infoRegresar ="";
JSONObject jsonObj = new JSONObject();
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();



CreditoEducativo educativoBean = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);


if (informacion.equals("CatalogoIF")  ) {
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setTabla("comcat_if");		
	catalogo.setOrden("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("CatalogoUniversidad")  ) {
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_UNIVERSIDAD");
	catalogo.setCampoDescripcion("CG_RAZON_SOCIAL ||' - '|| CG_NOMBRE_CAMPUS ");
	catalogo.setTabla("COMCAT_UNIVERSIDAD");		
	catalogo.setOrden("CG_RAZON_SOCIAL");
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("Agregar")  ) {

	String[] ifsSeleccionadas;
	String[] univSeleccionadas;
	String respuesta="";
	if(radio.equals("I")){		
		String delimiter = ",";
		univSeleccionadas = universidad.split(delimiter);
		respuesta= educativoBean.paramIF_Univ(ifs, univSeleccionadas);
	}
	if(radio.equals("U")){		
		String delimiter = ",";
		ifsSeleccionadas = ifs.split(delimiter);		
		respuesta=educativoBean.paramUniv_IF(ifsSeleccionadas,  universidad );		
	}
	

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);
	infoRegresar  = jsonObj.toString();
	
}else if (informacion.equals("Consultar")  ) {

	ConsRelUniversiad_If paginador = new ConsRelUniversiad_If();
	paginador.setIntermediario(ifs);
	paginador.setUniversidad(universidad);		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
		
	try {
		Registros reg	=	queryHelper.doSearch();
		
		while (reg.next())	{	
		
			String ic_if  = (reg.getString("IC_IF") == null) ? "" : reg.getString("IC_IF");
			String nombre_IF   = (reg.getString("NOMBRE_IF") == null) ? "" : reg.getString("NOMBRE_IF");
			String nombre_univ   = (reg.getString("NOMBRE_UNIV") == null) ? "" : reg.getString("NOMBRE_UNIV");
			String nombre_campus   = (reg.getString("NOMBRE_CAMPUS") == null) ? "" : reg.getString("NOMBRE_CAMPUS");
			String no_universidad   = (reg.getString("NO_UNIVERSIDAD") == null) ? "" : reg.getString("NO_UNIVERSIDAD");
			String ic_universidad   = (reg.getString("IC_UNIVERSIDAD") == null) ? "" : reg.getString("IC_UNIVERSIDAD");
			String estatus   = (reg.getString("ESTATUS") == null) ? "" : reg.getString("ESTATUS");
			String cg_activo   = (reg.getString("CG_ACTIVO") == null) ? "" : reg.getString("CG_ACTIVO");
			
			String base_operacion =  educativoBean.baseOperacionIF(ic_if)	;			
			datos = new HashMap();
			datos.put("NOMBRE_IF",nombre_IF);
			if(!base_operacion.equals(""))  {datos.put("BASE_OPERACION",base_operacion);  }
			if(base_operacion.equals(""))   {datos.put("BASE_OPERACION","Favor de Parametizar la B.O.");  }			
			datos.put("NOMBRE_UNIV",nombre_univ);
			datos.put("NOMBRE_CAMPUS",nombre_campus);
			datos.put("NO_UNIVERSIDAD",no_universidad);
			datos.put("IC_IF",ic_if);
			datos.put("IC_UNIVERSIDAD",ic_universidad);
			datos.put("ESTATUS",estatus);
			datos.put("CG_ACTIVO",cg_activo);
			
			
			registros.add(datos);	
		}		
		String consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar  = jsonObj.toString();
		
		}	catch(Exception e) {
		throw new AppException("Error en la paginación", e);
	}
}else if (informacion.equals("Inactivar")  ) {

	educativoBean.inacRelIF_Univ(ifs,  universidad, "N" );

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("ifs",ifs);
	jsonObj.put("universidad", universidad);
	infoRegresar  = jsonObj.toString();

}else if (informacion.equals("Activar")  ) {

	educativoBean.inacRelIF_Univ(ifs,  universidad , "S");

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("ifs",ifs);
	jsonObj.put("universidad", universidad);
	infoRegresar  = jsonObj.toString();

}


%>

<%=infoRegresar%>
