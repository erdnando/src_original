<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		java.sql.*,
		org.apache.commons.logging.Log,
		java.text.SimpleDateFormat,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.educativo.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/37creditoEducativo/37secsession_extjs.jspf"%>
<%!
private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = (request.getParameter("informacion")   !=null)?request.getParameter("informacion")   :"";
String intermediario = (request.getParameter("intermediario") !=null)?request.getParameter("intermediario") :"";
String universidad   = (request.getParameter("universidad") !=null)?request.getParameter("universidad")     :"";
String baseOperacion = (request.getParameter("baseOperacion") !=null)?request.getParameter("baseOperacion") :"";
String operacion     = (request.getParameter("operacion")     !=null)?request.getParameter("operacion")     :"";
String modalidad     = (request.getParameter("modalidad")     !=null)?request.getParameter("modalidad")     :"";
String estatus       = (request.getParameter("estatus")       !=null)?request.getParameter("estatus")       :"";

String clave         = (request.getParameter("clave")         !=null)?request.getParameter("clave")         :"";
String descripcion   = (request.getParameter("descripcion")   !=null)?request.getParameter("descripcion")   :"";
String porcentaje    = (request.getParameter("porcentaje")    !=null)?request.getParameter("porcentaje")    :"";

String modificar     = (request.getParameter("modificar")     !=null)?request.getParameter("modificar")     :"";
String ic_base       = (request.getParameter("ic_base")       !=null)?request.getParameter("ic_base")       :"";
String claveAnt      = (request.getParameter("claveAnt")      !=null)?request.getParameter("claveAnt")      :"";

String infoRegresar  = "";
JSONObject jsonObj   = new JSONObject();


CreditoEducativo educativoBean = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);
	

log.info("***** Informacion: " + informacion + "*****");

if(informacion.equals("CatalogoIF")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setTabla("comcat_if");
	catalogo.setOrden("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("CatalogoBaseOperacion")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_BASE_OPERACION");
	catalogo.setCampoDescripcion("IC_CLAVE ||' - '|| CG_DESCRIPCION ");
	catalogo.setTabla("CECAT_BASES_OPERA");
	catalogo.setOrden("IC_BASE_OPERACION");
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("CatalogoUniversidad")){

	CatalogoIF_Universidad catalogo = new CatalogoIF_Universidad();
	catalogo.setIntermediario(intermediario);
	catalogo.setCampoClave("U.IC_UNIVERSIDAD");
	catalogo.setCampoDescripcion("U.CG_RAZON_SOCIAL");
	catalogo.setOrden("U.CG_RAZON_SOCIAL");
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("Agregar")){

	ArrayList parametros = new ArrayList();
	parametros.add(intermediario);
	parametros.add(clave);
	parametros.add(descripcion);
	parametros.add(operacion);
	parametros.add(porcentaje);
	parametros.add(modalidad);
	parametros.add(iNoUsuario);
	parametros.add(claveAnt); // clave Anterior
	parametros.add(ic_base); // ic_base_operacion
	parametros.add(universidad); // Fodea 44-2014

	String respuesta = "";

	if(modificar.equals("")){
		respuesta=educativoBean.agregarBO(parametros);
	} else if(modificar.equals("S")){
		respuesta=educativoBean.ModificarBO(parametros);
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);
	infoRegresar  = jsonObj.toString();

} else if(informacion.equals("Consultar")){

	ConsParametrosBO paginador = new ConsParametrosBO();
	paginador.setIntermediario(intermediario);
	paginador.setBaseOperacion(baseOperacion);
	paginador.setOperacion(operacion);
	paginador.setModalidad(modalidad);
	paginador.setEstatus(estatus);
	paginador.setUniversidad(universidad);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

	try{
		Registros reg = queryHelper.doSearch();
		infoRegresar = "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	} catch(Exception e){
		throw new AppException("Error en la paginación", e);
	}

} else if(informacion.equals("ConsModificar")){

	List datos = educativoBean.consBaseOperacion(ic_base, universidad);

	intermediario = datos.get(0).toString();
	clave         = datos.get(1).toString();
	descripcion   = datos.get(2).toString();
	operacion     = datos.get(3).toString();
	porcentaje    = datos.get(4).toString();
	modalidad     = datos.get(5).toString();
	ic_base       = datos.get(6).toString();
	universidad   = datos.get(7).toString();

	intermediario = "<div><div style='float:left'>" + intermediario + "</div>";
	universidad = "<div><div style='float:left'>" + universidad + "</div>";

	jsonObj.put("success",      new Boolean(true));
	jsonObj.put("nombreIF",     intermediario);
	jsonObj.put("clave",        clave);
	jsonObj.put("descripcion",  descripcion);
	jsonObj.put("operaCredito", operacion);
	jsonObj.put("porcentaje",   porcentaje);
	jsonObj.put("modalidad",    modalidad);
	jsonObj.put("modificar",    "S");
	jsonObj.put("ic_base",      ic_base);
	jsonObj.put("claveAnt",     clave);
	jsonObj.put("universidad",  universidad);
	infoRegresar  = jsonObj.toString();

} else if(informacion.equals("CambioEstatus")){

	String ic_base2[] = request.getParameterValues("_ic_base");
	String estatus2[] = request.getParameterValues("_estatus");

	String respuesta= educativoBean.cambioEstatus(ic_base2, estatus2);

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);
	infoRegresar  = jsonObj.toString();

}

%>

<%=infoRegresar%>
