Ext.onReady(function(){

//---------ACCION DE CAMBIAR DE ESTATUS EL  REGISTRO-------------------------
	function procesarSuccessFailureCambioEstatus(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			jsonData = Ext.util.JSON.decode(response.responseText);
			if(jsonData.respuesta =='Correcto'){
				Ext.MessageBox.alert('Mensaje...','La operaci�n ha sido realizada con �xito.');
				fp.el.mask('Enviando...', 'x-mask-loading');
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'
					})
				});
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var cambioEstatus = function(){
		var grid = Ext.getCmp('gridConsulta'); 
		var seleccionados = grid.getSelectionModel().getSelections();
		if(seleccionados.length == 0){
			Ext.MessageBox.alert('Aviso','Debe seleccionar un registro.');
			return;
		}
		var ic_base = [];
		var estatus = [];
		for(var i=0;i< seleccionados.length;i++){
			ic_base.push(seleccionados[i].get('CONSECUTIVO'));
			estatus.push(seleccionados[i].get('CLAVE_ESTATUS'));
		}
		Ext.MessageBox.confirm('Confirmaci�n','Se cambiar� el estatus de las Bases de Operaci�n seleccionadas., �Desea usted continuar?', function(resp){
			if(resp =='yes'){
				Ext.Ajax.request({
					url : 'paramBO.data.jsp',
					params: {
						informacion: 'CambioEstatus',
						_ic_base:     ic_base,
						_estatus:     estatus
					},
					callback: procesarSuccessFailureCambioEstatus
				});
			}
		});
	}

//---------ACCION DE MODIFICAR REGISTRO-------------------------
	function procesarSuccessFailureConsModificar(opts, success, response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			jsonData = Ext.util.JSON.decode(response.responseText);

			Ext.getCmp('intermediario2').setValue(jsonData.nombreIF);
			Ext.getCmp('universidad2').setValue(jsonData.universidad);
			Ext.getCmp('clave1').setValue(jsonData.clave);
			Ext.getCmp('descripcion1').setValue(jsonData.descripcion);

			Ext.getCmp('operacion1').setValue(jsonData.operaCredito);
			Ext.getCmp('porcentaje1').setValue(jsonData.porcentaje);
			Ext.getCmp('modalidad1').setValue(jsonData.modalidad);

			Ext.getCmp('modificar').setValue(jsonData.modificar);
			Ext.getCmp('ic_base').setValue(jsonData.ic_base);
			Ext.getCmp('claveAnt').setValue(jsonData.claveAnt);

		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var editar = function(){

		var grid = Ext.getCmp("gridConsulta"); 
		var seleccionados = grid.getSelectionModel().getSelections();
		if( seleccionados.length == 0){
			Ext.MessageBox.alert('Mensaje...','Debe seleccionar un registro.');
			return;
		} else if(seleccionados.length > 1){
			Ext.MessageBox.alert('Mensaje...','S�lo se puede editar un registro a la vez.');
			return;
		}

	var ic_base= seleccionados[0].get('CONSECUTIVO');
	var universidad= seleccionados[0].get('IC_UNIV');

		Ext.Ajax.request({
			url: 'paramBO.data.jsp',
			params: {
				informacion: 'ConsModificar',
				ic_base:     ic_base,
				universidad: universidad
			},
			callback: procesarSuccessFailureConsModificar
		});

		Ext.getCmp("Guardar").show();
		Ext.getCmp("cancelar").show();
		Ext.getCmp("Buscar").hide();
		Ext.getCmp("limpiar").hide();

		Ext.getCmp("porcentaje2").show();
		Ext.getCmp("descripcion2").show();
		Ext.getCmp("clave2").show();
		Ext.getCmp("intermediario2").show();
		Ext.getCmp("universidad2").show();

		Ext.getCmp("baseOperacion1").hide();
		Ext.getCmp("estatus1").hide();
		Ext.getCmp("intermediario1").hide();
		Ext.getCmp("universidad1").hide();
		var fp = Ext.getCmp('forma');
		fp.setTitle('<div><div style="float:left">Editar de Registro </div>');

		Ext.getCmp('intermediario1').label.update('* Intermediario Financiero:');
		Ext.getCmp('clave2').label.update('* Clave:');
		Ext.getCmp('descripcion2').label.update('* Descripci�n:');
		Ext.getCmp('operacion1').label.update('* Opera Cred. Educativo:');
		Ext.getCmp('porcentaje2').label.update('* % de Aportaci�n:');
		Ext.getCmp('modalidad1').label.update('*  Modalidad:');

	}

//---------ACCION DE GUARDAR REGISTRO-------------------------

	function procesarSuccessFailureAgregar(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var fp = Ext.getCmp('forma');
			fp.el.unmask();

			jsonData = Ext.util.JSON.decode(response.responseText);

			if(jsonData.respuesta =='Correcto'){

				Ext.MessageBox.alert('Mensaje...','La operaci�n ha sido realizada con �xito.');

				Ext.getCmp('Guardar').hide();
				Ext.getCmp('cancelar').hide();
				Ext.getCmp('clave2').hide();
				Ext.getCmp('descripcion2').hide();
				Ext.getCmp('universidad2').hide();
				Ext.getCmp('porcentaje2').hide();
				Ext.getCmp('intermediario2').hide();
				Ext.getCmp('intermediario1').setValue('');
				Ext.getCmp('universidad1').setValue('');
				Ext.getCmp('baseOperacion1').setValue('');
				Ext.getCmp('clave1').setValue('');
				Ext.getCmp('descripcion1').setValue('');
				Ext.getCmp('operacion1').setValue('');
				Ext.getCmp('porcentaje1').setValue('');
				Ext.getCmp('modalidad1').setValue('');

				Ext.getCmp('intermediario1').show();
				Ext.getCmp('universidad1').show();
				Ext.getCmp('baseOperacion1').show();
				Ext.getCmp('operacion1').show();
				Ext.getCmp('modalidad1').show();
				Ext.getCmp('estatus1').show();
				Ext.getCmp('Buscar').show();
				Ext.getCmp('limpiar').show();

				Ext.getCmp('intermediario1').label.update(' Intermediario Financiero:');
				Ext.getCmp('universidad1').label.update(' Universidad:');
				Ext.getCmp('clave2').label.update(' Clave:');
				Ext.getCmp('descripcion2').label.update(' Descripci�n:');
				Ext.getCmp('operacion1').label.update(' Opera Cred. Educativo:');
				Ext.getCmp('porcentaje2').label.update('% de Aportaci�n:');
				Ext.getCmp('modalidad1').label.update(' Modalidad:');

				fp.el.mask('Enviando...', 'x-mask-loading');
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'
					})
				});

			} else if(jsonData.respuesta =='Existe'){
				Ext.MessageBox.alert('Mensaje...','La clave de la B.O ya existe, favor de verificar y no se permite continuar.');
			}
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var agregar = function(){

		Ext.getCmp('intermediario1').setValue('');
		Ext.getCmp('universidad1').setValue('');
		Ext.getCmp('baseOperacion1').setValue('');
		Ext.getCmp('clave1').setValue('');
		Ext.getCmp('descripcion1').setValue('');
		Ext.getCmp('operacion1').setValue('');
		Ext.getCmp('porcentaje1').setValue('');
		Ext.getCmp('modalidad1').setValue('');

		Ext.getCmp("Guardar").show();
		Ext.getCmp("cancelar").show();
		Ext.getCmp("Buscar").hide();
		Ext.getCmp("limpiar").hide();
		
		Ext.getCmp("porcentaje2").show();
		Ext.getCmp("descripcion2").show();
		Ext.getCmp("intermediario1").show();
		Ext.getCmp("universidad1").show();
		Ext.getCmp("clave2").show();

		Ext.getCmp('intermediario1').label.update('* Intermediario Financiero:');
		Ext.getCmp('universidad1').label.update('* Universidad:');
		Ext.getCmp('clave2').label.update('* Clave:');
		Ext.getCmp('descripcion2').label.update('* Descripci�n:');
		Ext.getCmp('operacion1').label.update('* Opera Cred. Educativo:');
		Ext.getCmp('porcentaje2').label.update('* % de Aportaci�n:');
		Ext.getCmp('modalidad1').label.update('*  Modalidad:');

		Ext.getCmp("baseOperacion1").hide();
		Ext.getCmp("estatus1").hide();
		Ext.getCmp("intermediario2").hide();
		Ext.getCmp("universidad2").hide();

		var fp = Ext.getCmp('forma');
		fp.setTitle('<div><div style="float:left">Alta de Registro </div>');

	}

	// ------------GENERACION DE LA CONSULTA ------------------

	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();

		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()){
				gridConsulta.show();
			}
			if(store.getTotalCount() > 0) {			
				el.unmask();					
			} else {								
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: 'paramBO.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{ name: 'CONSECUTIVO'   },
			{ name: 'CLAVE_ESTATUS' },
			{ name: 'NOMBRE_IF'     },
			{ name: 'CLAVE'         },
			{ name: 'UNIVERSIDAD'   },
			{ name: 'DESCRIPCION'   },
			{ name: 'OPERA_CREDITO' },
			{ name: 'POR_APORTACION'},
			{ name: 'MODALIDAD'     },
			{ name: 'ESTATUS'       },
			{ name: 'USUARIO_ALTA'  },
			{ name: 'FECHA_INGRESO' },
			{ name: 'USUARIO_MOD'   },
			{ name: 'FECHA_MOD'     },
			{ name: 'IC_UNIV'       }
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var gridConsulta = new Ext.grid.EditorGridPanel({
		width:      800,
		height:     400,
		id:         'gridConsulta',
		title:      'Base de Operaci�n de Cr�dito Educativo',
		margins:    '20 0 0 0',
		style:      'margin:0 auto;',
		stripeRows: true,
		loadMask:   true,
		frame:      true,
		sm:         new Ext.grid.RowSelectionModel({singleSelect:false}), // para seleccionar el registro
		store:      consultaData,
		tbar:{
			xtype:   'toolbar',
			style:   {borderWidth: 0},
			items: [{
				text:    'Alta de Registro',
				iconCls: 'icon-register-add',
				scope:   this,
				handler: agregar
			},'-',{
				text:    'Editar Registro',
				iconCls: 'icon-register-edit',
				scope:   this,
				handler: editar
			},'-',{
				text:    'Cambiar Estatus',
				iconCls: 'borrar',
				scope:   this,
				handler: cambioEstatus
			}]
		},
		columns: [{
			width:      250,
			header:     '<div><div align=center >Intermediario Financiero</div>',
			tooltip:    'Intermediario Financiero',
			dataIndex:  'NOMBRE_IF',
			align:      'left',
			sortable:   true,
			resizable:  true
		},{
			width:      80,
			header:     '<div><div align=center >Clave</div>',
			tooltip:    'Clave',
			dataIndex:  'CLAVE',
			align:      'center',
			sortable:   true,
			resizable:  true
		},{
			width:      250,
			header:     '<div><div align=center >Universidad</div>',
			tooltip:    'Universidad',
			dataIndex:  'UNIVERSIDAD',
			align:      'left',
			sortable:   true,
			resizable:  true
		},{
			width:      200,
			header:     '<div><div align=center >Descripci�n</div>',
			tooltip:    'Descripci�n',
			dataIndex:  'DESCRIPCION',
			align:      'left',
			sortable:   true,
			resizable:  true
		},{
			width:      120,
			header:     '<div><div align=center >Opera Cred. Educativo</div>',
			tooltip:    'Opera Cred. Educativo',
			dataIndex:  'OPERA_CREDITO',
			align:      'center',
			sortable:   true,
			resizable:  true
		},{
			width:      100,
			header:     '<div><div align=center >% de Aportaci�n</div>',
			tooltip:    '% de Aportaci�n',
			dataIndex:  'POR_APORTACION',
			align:      'center',
			sortable:   true,
			resizable:  true
		},{
			width:      150,
			header:     '<div><div align=center >Modalidad</div>',
			tooltip:    'Modalidad',
			dataIndex:  'MODALIDAD',
			align:      'left',
			sortable:   true,
			resizable:  true
		},{
			width:      100,
			header:     '<div><div align=center >Estatus</div>',
			tooltip:    'Estatus',
			dataIndex:  'ESTATUS',
			align:      'center',
			sortable:   true,
			resizable:  true
		},{
			width:      150,
			header:     '<div><div align=center >Usuario Alta</div>',
			tooltip:    'Usuario Alta',
			dataIndex:  'USUARIO_ALTA',
			align:      'center',
			sortable:   true,
			resizable:  true
		},{
			width:      150,
			header:     '<div><div align=center >Fecha Ingreso</div>',
			tooltip:    'Fecha Ingreso',
			dataIndex:  'FECHA_INGRESO',
			align:      'center',
			sortable:   true,
			resizable:  true
		},{
			width:      150,
			header:     '<div><div align=center >Usuario Mod.</div>',
			tooltip:    'Usuario Mod',
			dataIndex:  'USUARIO_MOD',
			align:      'center',
			sortable:   true,
			resizable:  true
		},{
			width:      150,
			header:     '<div><div align=center >Fecha Modificaci�n</div>',
			tooltip:    'Fecha Modificaci�n',
			dataIndex:  'FECHA_MOD',
			align:      'center',
			sortable:   true,
			resizable:  true
		}]
	});

  //---------------------   Criterios de Busqueda----------------------
  var catalogoEstatus = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			["A",'Activo'],
			["I",'Inactivo']
		 ]
	});

	var catalogoOperacion = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			["S",'SI'],
			["N",'NO']
		 ]
	});

	var catalogoModalidad = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			["1",'Pari Passu'],
			["2",'Primeras Perdidas']
		 ]
	});

	var catalogoBaseOperacion = new Ext.data.JsonStore({
		id: 'catalogoBaseOperacion',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : 'paramBO.data.jsp',
		baseParams: {
			informacion: 'CatalogoBaseOperacion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : 'paramBO.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoUniversidad = new Ext.data.JsonStore({
		id: 'catalogoUniversidad',
		root: 'registros',
		fields: ['clave', 'descripcion','loadMsg'],
		url: 'paramBO.data.jsp',
		baseParams: {
			informacion: 'CatalogoUniversidad'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var  elementosForma  = [
		{ xtype: 'textfield',  hidden: true,  id: 'modificar',  value: '' },
		{ xtype: 'textfield',  hidden: true,  id: 'ic_base',    value: '' },
		{ xtype: 'textfield',  hidden: true,  id: 'claveAnt',   value: '' },
		{
			xtype:          'combo',
			fieldLabel:     'Intermediario Financiero',
			id:             'intermediario1',
			name:           'intermediario',
			hiddenName:     'intermediario',
			emptyText:      'Seleccionar ...',
			displayField:   'descripcion',
			valueField:     'clave',
			mode:           'local',
			triggerAction:  'all',
			forceSelection: true,
			typeAhead:      true,
			minChars:       1,
			store:          catalogoIF,
			tpl:            NE.util.templateMensajeCargaCombo,
			listeners: {
				select: function(combo, record, index){
					if(combo.getValue() != ''){
						catalogoUniversidad.load({
							params: Ext.apply({
								intermediario: combo.getValue()
							})
						});
					}
				}
			}
		},{
			xtype:          'displayfield',
			fieldLabel:     'Intermediario Financiero',
			id:             'intermediario2',
			value:          '',
			align:          'left',
			hidden:         true
		},{
			xtype:          'combo',
			fieldLabel:     'Universidad',
			id:             'universidad1',
			name:           'universidad',
			hiddenName:     'universidad',
			emptyText:      'Seleccionar ...',
			displayField:   'descripcion',
			valueField:     'clave',
			mode:           'local',
			triggerAction:  'all',
			forceSelection: true,
			typeAhead:      true,
			minChars:       1,
			store:          catalogoUniversidad,
			tpl:            NE.util.templateMensajeCargaCombo
		},{
			xtype:          'displayfield',
			fieldLabel:     'Universidad',
			id:             'universidad2',
			value:          '',
			align:          'left',
			hidden:         true
		},{
			xtype:          'combo',
			fieldLabel:     'Clave - Descripci�n',
			id:             'baseOperacion1',
			name:           'baseOperacion',
			hiddenName:     'baseOperacion',
			emptyText:      'Seleccionar ...',
			displayField:   'descripcion',
			valueField:     'clave',
			triggerAction:  'all',
			autoLoad:       false,
			forceSelection: true,
			typeAhead:      true,
			minChars:       1,
			store:          catalogoBaseOperacion
		},{
			xtype:          'compositefield',
			fieldLabel:     'Clave',
			id:             'clave2',
			msgTarget:      'side',
			combineErrors:  false,
			hidden:         true,
			items: [{
				width:       90,
				xtype:       'numberfield',
				fieldLabel:  'Clave',
				id:          'clave1',
				name:        'clave',
				msgTarget:   'side',
				margins:     '0 20 0 0',
				maxLength:   5,
				allowBlank:  true
			}]
		},{
			xtype:          'compositefield',
			fieldLabel:     'Descripci�n',
			id:             'descripcion2',
			msgTarget:      'side',
			combineErrors:  false,
			hidden:         true,
			items: [{
				width:       500,
				xtype:       'textfield',
				fieldLabel:  'Descripci�n',
				id:          'descripcion1',
				name:        'descripcion',
				msgTarget:   'side',
				margins:     '0 20 0 0',
				maxLength:   100,
				allowBlank:  true
			}]
		},{
			xtype:          'combo',
			fieldLabel:     'Opera Cred. Educativo',
			id:             'operacion1',
			name:           'operacion',
			hiddenName:     'operacion',
			mode:           'local',
			displayField:   'descripcion',
			valueField:     'clave',
			emptyText:      'Seleccionar ...',
			triggerAction:  'all',
			autoLoad:       false,
			forceSelection: true,
			typeAhead:      true,
			minChars:       1,
			store:          catalogoOperacion
		},{
			xtype:          'compositefield',
			fieldLabel:     '% de Aportaci�n',
			id:             'porcentaje2',
			msgTarget:      'side',
			combineErrors:  false,
			hidden:         true,
			items: [{
				width:       90,
				xtype:       'numberfield',
				fieldLabel:  '% de Aportaci�n',
				id:          'porcentaje1',
				name:        'porcentaje',
				msgTarget:   'side',
				margins:     '0 20 0 0',
				maxLength:   3,
				allowBlank:  true
			}]
		},{
			xtype:          'combo',
			fieldLabel:     'Modalidad',
			id:             'modalidad1',
			name:           'modalidad',
			hiddenName:     'modalidad',
			mode:           'local',
			displayField:   'descripcion',
			emptyText:      'Seleccionar ...',
			valueField:     'clave',
			triggerAction:  'all',
			autoLoad:       false,
			forceSelection: true,
			typeAhead:      true,
			minChars:       1,
			store:          catalogoModalidad
		},{
			xtype:          'combo',
			fieldLabel:     'Estatus',
			id:             'estatus1',
			name:           'estatus',
			hiddenName:     'estatus',
			mode:           'local',
			displayField:   'descripcion',
			valueField:     'clave',
			emptyText:      'Seleccionar ...',
			triggerAction:  'all',
			autoLoad:       false,
			forceSelection: true,
			typeAhead:      true,
			minChars:       1,
			store:          catalogoEstatus
		}
	];

	var fp = new Ext.form.FormPanel({
		width:         700,
		labelWidth:    150,
		id:            'forma',
		title:         'Criterios de B�squeda',
		style:         'margin:0 auto;',
		bodyStyle:     'padding: 6px',
		defaultType:   'textfield',
		frame:         true,
		collapsible:   true,
		titleCollapse: false,
		defaults: {
			msgTarget:  'side',
			anchor:     '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text:     'Guardar',
				id:       'Guardar',
				iconCls:  'icoGuardar',
				hidden:   true,
				formBind: true,
				handler:  function(boton, evento){

					var modificar = Ext.getCmp('modificar').getValue();
					if(modificar==''){
						var intermediario = Ext.getCmp('intermediario1');
						if (Ext.isEmpty(intermediario.getValue())){
							intermediario.markInvalid('Este campo es obligatorio.');
							return;
						}
					}
					if(modificar==''){
						var universidad = Ext.getCmp('universidad1');
						if (Ext.isEmpty(universidad.getValue())){
							universidad.markInvalid('Este campo es obligatorio.');
							return;
						}
					}
					var clave = Ext.getCmp('clave1');
					if (Ext.isEmpty(clave.getValue())){
						clave.markInvalid('Este campo es obligatorio.');
						return;
					}
					var descripcion = Ext.getCmp('descripcion1');
					if (Ext.isEmpty(descripcion.getValue())){
						descripcion.markInvalid('Este campo es obligatorio.');
						return;
					}
					var operacion = Ext.getCmp('operacion1');
					if (Ext.isEmpty(operacion.getValue())){
						operacion.markInvalid('Este campo es obligatorio.');
						return;
					}
					var porcentaje = Ext.getCmp('porcentaje1');
					if (Ext.isEmpty(porcentaje.getValue())){
						porcentaje.markInvalid('Este campo es obligatorio.');
						return;
					}
					var modalidad = Ext.getCmp('modalidad1');
					if (Ext.isEmpty(modalidad.getValue())){
						modalidad.markInvalid('Este campo es obligatorio.');
						return;
					}

					Ext.Ajax.request({
						url: 'paramBO.data.jsp',
						params: {
							informacion: 'Agregar',
							intermediario: Ext.getCmp('intermediario1').getValue(),
							universidad:   Ext.getCmp('universidad1').getValue(),
							clave:         Ext.getCmp('clave1').getValue(),
							descripcion:   Ext.getCmp('descripcion1').getValue(),
							operacion:     Ext.getCmp('operacion1').getValue(),
							porcentaje:    Ext.getCmp('porcentaje1').getValue(),
							modalidad:     Ext.getCmp('modalidad1').getValue(),
							modificar:     Ext.getCmp('modificar').getValue(),
							ic_base:       Ext.getCmp('ic_base').getValue(),
							claveAnt:      Ext.getCmp('claveAnt').getValue()
							},
						callback: procesarSuccessFailureAgregar
					});
				}
			},
			{
				text:    'Cancelar',
				id:      'cancelar',
				iconCls: 'cancelar',
				hidden:  true,
				handler: function() {
					window.location = 'paramBO.jsp';
				}
			},
			{
				text:     'Buscar',
				id:       'Buscar',
				iconCls:  'icoBuscar',
				formBind: true,
				handler:  function(boton, evento) {
				
				fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:   'Consultar',
							intermediario: Ext.getCmp('intermediario1').getValue(),
							universidad:   Ext.getCmp('universidad1').getValue(),
							baseOperacion: Ext.getCmp('baseOperacion1').getValue(),
							operacion:     Ext.getCmp('operacion1').getValue(),
							modalidad:     Ext.getCmp('modalidad1').getValue(),
							estatus:       Ext.getCmp('estatus1').getValue()
						})
					});
				}
			},
			{
				text:    'Limpiar',
				id:      'limpiar',
				iconCls: 'icoLimpiar',
				handler: function(){
					window.location = 'paramBO.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		width:   890,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		height:  'auto',
		style:   'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});

	catalogoIF.load();

	consultaData.load({
		params: Ext.apply(fp.getForm().getValues(),{
			informacion: 'Consultar'
		})
	});

});