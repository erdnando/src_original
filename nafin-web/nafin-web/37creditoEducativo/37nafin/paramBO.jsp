<!DOCTYPE html>
<%@ page import="
		java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/37creditoEducativo/37secsession.jspf" %>
<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs.jspf" %>

<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="paramBO.js?<%=session.getId()%>"></script>
<%if(esEsquemaExtJS) {%>
<%@ include file="/01principal/menu.jspf"%>		
<%}%>
</head>

<%if(esEsquemaExtJS) {%>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
		<div id="_menuApp"></div>
			<div id="Contcentral">	
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
				<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
			</div>
		</div>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>		
	</body>
	 
<%}else  {%>
	<body>
	<div id='areaContenido' style="margin-left: 3px; margin-top: 3px; text-align: center"></div>
	<form id='formAux' name="formAux" target='_new'></form> 
	</body>
<%}%>
</html>
