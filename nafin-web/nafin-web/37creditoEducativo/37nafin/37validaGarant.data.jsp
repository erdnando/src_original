<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		javax.naming.Context,
		netropology.utilerias.*,
		org.apache.commons.logging.Log,
		com.netro.educativo.*,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoIF_Universidad,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		com.netro.exception.*
		"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/37creditoEducativo/37secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

log.debug("informacion = <"+informacion+">");

if (informacion.equals("Parametrizacion")) {	//	*-*-*-*-*-*--*-*-*-*-*-*-*-*	Parametrizacion	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

	//No hay parametrización para esta pantalla

}else if (				informacion.equals("CatalogoIf")) {

	CatalogoIF_Universidad cat = new CatalogoIF_Universidad();
	cat.setCampoClave("i.ic_if");
	cat.setCampoDescripcion("i.cg_razon_social");
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();

}else if (				informacion.equals("CatologoGrupo")  ) {
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_grupo");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("cecat_grupo");
	catalogo.setOrden("ic_grupo");
	infoRegresar = catalogo.getJSONElementos();	

} else if (          informacion.equals("CatalogoUniv") )	{

	String claveGrupo	= (request.getParameter("claveGrupo")!=null)?request.getParameter("claveGrupo"):"";

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_universidad");
	catalogo.setCampoDescripcion("cg_razon_social||' - '|| cg_nombre_campus");
	catalogo.setTabla("comcat_universidad");
	catalogo.setOrden("ic_universidad");
	String cad="";

	if(!claveGrupo.equals("")){
		cad="ic_grupo";
		catalogo.setCampoLlave(cad);
		catalogo.setValoresCondicionIn(claveGrupo,Integer.class);
	}
	infoRegresar = catalogo.getJSONElementos();

}else if (				informacion.equals("ConsultarFolios")){

	String claveIf				= (request.getParameter("claveIf")!=null)?request.getParameter("claveIf"):"";
	String ic_grupo			= (request.getParameter("claveGrupo")!=null)?request.getParameter("claveGrupo"):"";
	String ic_universidad	= (request.getParameter("claveUniversidad")!=null)?request.getParameter("claveUniversidad"):"";
	String folioOpera			= (request.getParameter("folioOpera")!=null)?request.getParameter("folioOpera"):"";

	int start = 0;
	int limit = 0;

	ConsultaValidaGarantia paginador =new  ConsultaValidaGarantia();
	paginador.setFolio(folioOpera);
	paginador.setIntermediario(claveIf);
	paginador.setGrupo(ic_grupo);
	paginador.setUniversidad(ic_universidad);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	queryHelper.setMultiplesPaginadoresXPagina(true);

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request);
		}
		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}

} else if (				informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(new  ConsultaValidaGarantia()); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	queryHelper.setMultiplesPaginadoresXPagina(true);
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

}else if (informacion.equals("validaGarantia.accion.aceptar")) {

	// Obtener instancia del EJB de CreditoEducativo
	CreditoEducativo creditoEducativo =null;
	try {
		creditoEducativo = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);
	} catch(Exception e) {
		log.error("validaGarantia.accion.aceptar(Exception): Obtener instancia del EJB de CreditoEducativo");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de CreditoEducativo.");
	}

	JSONObject jsonObj		= new JSONObject();
	String estadoSiguiente	= "RESPUESTA_PROCESO_ACEPTAR";
	String msg					= "";
	String folio				=	(request.getParameter("winConfirma.folioOpera")!=null)?request.getParameter("winConfirma.folioOpera"):"";
	String fechaDeposito		=	(request.getParameter("fechaDeposito")!=null)?request.getParameter("fechaDeposito"):"";
	String importe				=	(request.getParameter("importe")!=null)?request.getParameter("importe"):"";
	String Banco				=	(request.getParameter("Banco")!=null)?request.getParameter("Banco"):"";
	String referencia			=	(request.getParameter("referencia")!=null)?request.getParameter("referencia"):"";
	String estatus 			=	"6";	//	CONTRAGARANTIA ACEPTADA
	boolean success			= true;

	try {

		creditoEducativo.setValidaGarantias(folio,	estatus, "",fechaDeposito, importe, Banco,	referencia);

	} catch(Exception e) {
		success	= false;
		msg		= e.getMessage();
		log.error("validaGarantia.accion.aceptar(Exception): Error al establecer la validacion de garantias");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al establecer la validacion de garantias.");
	}

	// En caso de que haya alguno mensaje
	jsonObj.put("msg",					msg);

	// Establece el estado siguiente
	jsonObj.put("estadoSiguiente", estadoSiguiente );

	jsonObj.put("success", new Boolean(success));

	infoRegresar = jsonObj.toString();

}else if (informacion.equals("validaGarantia.accion.rechazo")) {

	// Obtener instancia del EJB de CreditoEducativo
	CreditoEducativo creditoEducativo = null;
	try {
		creditoEducativo = ServiceLocator.getInstance().lookup("CreditoEducativoEJB", CreditoEducativo.class);
	} catch(Exception e) {
		log.error("validaGarantia.accion.rechazo(Exception): Obtener instancia del EJB de CreditoEducativo");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de CreditoEducativo.");
	}

	JSONObject jsonObj		= new JSONObject();
	String estadoSiguiente	= "RESPUESTA_PROCESO_RECHAZO";
	String folio				=	(request.getParameter("folio")!=null)?request.getParameter("folio"):"";
	String causaRechazo		=	(request.getParameter("causaRechazo")!=null)?request.getParameter("causaRechazo"):"";
	String estatus 			=	"7";	//	CONTRAGARANTIA RECHAZADA
	boolean success			= true;
	String msg					= "";

	try {

		creditoEducativo.setValidaGarantias(folio,	estatus, causaRechazo,"SYSDATE", "", "",	"");

	} catch(Exception e) {
		success	= false;
		msg		= e.getMessage();
		log.error("validaGarantia.accion.rechazo(Exception): Error al establecer la validacion de garantias");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al establecer la validacion de garantias.");
	}

	// En caso de que haya alguno mensaje
	jsonObj.put("msg",					msg);	

	// Establece el estado siguiente
	jsonObj.put("estadoSiguiente", estadoSiguiente );

	jsonObj.put("success", new Boolean(success));

	infoRegresar = jsonObj.toString();

}else if (				informacion.equals("ConsultaE") ) {

	int start = 0;
	int limit = 0;

	ConsultaRegistroIndividual paginador =new  ConsultaRegistroIndividual();
	String folioOperacion	=	(request.getParameter("folioOperacion")!=null)?request.getParameter("folioOperacion"):"";
	String estatusReg			=	(request.getParameter("estatusReg")!=null)?request.getParameter("estatusReg"):"";

	paginador.setEstatusReg(estatusReg);
	paginador.setFolio(folioOperacion);
	paginador.setCalculaMonto(true);

	CQueryHelperRegExtJS queryHelperE = new CQueryHelperRegExtJS(paginador);

	queryHelperE.setMultiplesPaginadoresXPagina(true);
	
	if (informacion.equals("ConsultaE")) {	//Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelperE.executePKQuery(request);
		}
		infoRegresar = queryHelperE.getJSONPageResultSet(request,start,limit);

	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
		}
	}

} else if (informacion.equals("ResumenTotalesE")) {		//Datos para el Resumen de Totales

	CQueryHelperRegExtJS queryHelperE = new CQueryHelperRegExtJS(new ConsultaRegistroIndividual()); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	queryHelperE.setMultiplesPaginadoresXPagina(true);
	infoRegresar  = queryHelperE.getJSONResultCount(request);	//los saca de sesion

}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>