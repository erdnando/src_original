Ext.onReady(function() {


//----------Inactivar la relación IF - Universidad--------------------
	
	function procesarActivarRelacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var fp = Ext.getCmp('forma');
			fp.el.unmask();			
			jsonData = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Mensaje','La operación se realizo con éxito');
			
			var ic_if_1 = Ext.getCmp("ic_if_1");
			var ic_universidad_2 = Ext.getCmp("ic_universidad_2");
			var ifs = ic_if_1.getValue();
			var ic_universidad = ic_universidad_2.getValue();
					
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Consultar',
					ifs:ifs,
					ic_universidad:ic_universidad	
				})
			});
					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesoActivar = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var ic_universidad = registro.get('IC_UNIVERSIDAD');
		var ic_if = registro.get('IC_IF');
		
		Ext.Ajax.request({
			url: 'paramIFUni.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'Activar',
				ifs:ic_if,
				ic_universidad:ic_universidad
			}),
			callback: procesarActivarRelacion
		});		
	}
	

//----------Inactivar la relación IF - Universidad--------------------
	
	function procesarInactivarRelacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			
			jsonData = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Mensaje','La operación se realizo con éxito');
			
			var ic_if_1 = Ext.getCmp("ic_if_1");
			var ic_universidad_2 = Ext.getCmp("ic_universidad_2");
			var ifs = ic_if_1.getValue();
			var ic_universidad = ic_universidad_2.getValue();
					
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Consultar',
					ifs:ifs,
					ic_universidad:ic_universidad	
				})
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesoInactivar = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var ic_universidad = registro.get('IC_UNIVERSIDAD');
		var ic_if = registro.get('IC_IF');
		
			Ext.Ajax.request({
			url: 'paramIFUni.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
			informacion: 'Inactivar',
			ifs:ic_if,
			ic_universidad:ic_universidad
			}),
			callback: procesarInactivarRelacion
		});		
	}
	
	//----------se agregan registros--------------------
	
	function procesarSuccessFailureAgregar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			
			jsonData = Ext.util.JSON.decode(response.responseText);
			
			if(jsonData.respuesta =='Correcto'){
				Ext.MessageBox.alert('Mensaje','La operación se realizo con éxito');
				Ext.MessageBox.alert('Mensaje','Favor de parametrizar las Bases de Operación de él o los Intermediarios Financieros seleccionados');		
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function agregar(opts, success, response) {
		var radioT;
		var ifs;
		var ic_universidad;
		
		//  ------------ Parametrización IF - Universidad--------------------------	
		var radio1 = Ext.getCmp("radio1");		
		if(radio1.getValue()==true){ radioT='I'; }	
		var ic_if_1 = Ext.getCmp("ic_if_1");
		var ic_universidad_1 = Ext.getCmp("ic_universidad_1");
		if(radio1.getValue()==true) {	
			if (Ext.isEmpty(ic_if_1.getValue()) ){
				ic_if_1.markInvalid('Debe seleccionar el Intermediario Financiero');
				return;
			}
			if (Ext.isEmpty(ic_universidad_1.getValue()) ){
				ic_universidad_1.markInvalid('Debe seleccionar una Universidad');
				return;
			}
		}
		//  ------------ Parametrización Universidad - IF--------------------------
		
		var radio2 = Ext.getCmp("radio2");		
		if(radio2.getValue()==true){ radioT='U'; }
		var ic_universidad_2 = Ext.getCmp("ic_universidad_2");
		var ic_if_2 = Ext.getCmp("ic_if_2");				
		if(radio2.getValue()==true) {	
			if (Ext.isEmpty(ic_universidad_2.getValue()) ){
				ic_universidad_2.markInvalid('Debe seleccionar una Universidad');
				return;
			}			
			if (Ext.isEmpty(ic_if_2.getValue())){
				ic_if_2.markInvalid('Debe seleccionar el Intermediario Financiero');
				return;
			}
		}		
		
		//-------------CONSULTA-------------
		var radio3 = Ext.getCmp("radio3");		
		if(radio3.getValue()==true){ radioT='C'; }
		if(radioT=='I') {
			ifs = ic_if_1.getValue();
			ic_universidad = ic_universidad_1.getValue();
		}
		if(radioT=='U') {
			ifs = ic_if_2.getValue();
			ic_universidad = ic_universidad_2.getValue();
		}
		if(radioT=='C') {
			ifs = ic_if_1.getValue();
			ic_universidad = ic_universidad_2.getValue();
		}		
		
		Ext.Ajax.request({
			url : 'paramIFUni.data.jsp',
			params: {
				informacion: 'Agregar',
				radio:radioT,
				ifs:ifs,
				ic_universidad:ic_universidad
				},
			callback: procesarSuccessFailureAgregar
		});	
		
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}								
			if(store.getTotalCount() > 0) {			
				el.unmask();					
			} else {								
				el.mask('No se encontró ningún registro', 'x-mask');				
			}
		}
	}
	
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : 'paramIFUni.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{	name: 'NOMBRE_IF'},
			{	name: 'BASE_OPERACION'},
			{	name: 'NOMBRE_UNIV'},
			{	name: 'NOMBRE_CAMPUS'}	,
			{	name: 'NO_UNIVERSIDAD'},
			{	name: 'IC_IF'},
			{	name: 'IC_UNIVERSIDAD'},
			{	name: 'ESTATUS'},
			{	name: 'CG_ACTIVO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '',		
		columns: [	
			{
				header: 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Base de Operación IF',
				tooltip: 'Base de Operación IF',
				dataIndex: 'BASE_OPERACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Universidad',
				tooltip: 'Universidad',
				dataIndex: 'NOMBRE_UNIV',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Campus',
				tooltip: 'Campus',
				dataIndex: 'NOMBRE_CAMPUS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},		
			{
				header: 'No. N@E Universidad',
				tooltip: 'No. N@E Universidad',
				dataIndex: 'NO_UNIVERSIDAD',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				xtype: 'actioncolumn',
				header: 'Acciones',
				tooltip: 'Acciones',
				dataIndex: 'ACCIONES',
				align: 'center',
				width: 130,
				items: [
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							if(registro.get('CG_ACTIVO') =='S') {
								this.items[0].tooltip = 'Inactivar';
								return 'borrar'
							}
						}
						,handler: procesoInactivar
					},
					{
						getClass: function(value,metadata,registro,rowIndex,colIndex,store){
							if(registro.get('CG_ACTIVO') =='N') {
								this.items[1].tooltip = 'Activar';
								return 'correcto'
							}
						}
						,handler: procesoActivar
					}
				]
			}		
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 770,		
		frame: true		
	});
	
	
	
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : 'paramIFUni.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoUniversidad = new Ext.data.JsonStore({
		id: 'catalogoUniversidad',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : 'paramIFUni.data.jsp',
		baseParams: {
			informacion: 'CatalogoUniversidad'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var radioTipo = new  Ext.form.RadioGroup({
		hideLabel: true,
		name: 'radio',	
		style: 'margin:0 auto;',
		columns: 3,
		items: [
			{ 
				boxLabel:    'Parametrización IF - Universidad',
				name:  'radio',
				id: 	'radio1',
				inputValue: 'I',
				value: 'I',
				checked: true
			},
			{ 
				boxLabel:    'Parametrización Universidad - IF',
				name:   'radio',
				id: 	'radio2',
				inputValue: 'U',
				value: 'U'
			},
			{ 
				boxLabel:    'Consulta',
				name:        'radio',
				id: 	'radio3',
				inputValue: 'C',
				value: 'C'
			}
		],
		style: { paddingLeft: '10px' } ,
		listeners:{
			change: function(grupo, radio){
				var tipoOp = radio.inputValue;
				
					Ext.getCmp("ic_if_1").hide();
					Ext.getCmp("ic_universidad_1").hide();	
					
					Ext.getCmp("ic_universidad_2").hide();									
					Ext.getCmp("ic_if_2").hide();
					
					Ext.getCmp("consulta").hide();
					Ext.getCmp("aceptar").hide();
					Ext.getCmp('gridConsulta').hide();
					var fp = Ext.getCmp('forma');
					
					Ext.getCmp('ic_if_1').setValue('');
					Ext.getCmp('ic_universidad_1').setValue('');
					Ext.getCmp('ic_universidad_2').setValue('');
					Ext.getCmp('ic_if_2').setValue('');
					
				if(tipoOp=='I') {
					Ext.getCmp("ic_if_1").show();	
					Ext.getCmp("aceptar").show();					
					fp.setTitle('<div><div style="float:left">Parametrización IF - Universidad</div>');
				}else	if(tipoOp=='U') {
				   Ext.getCmp("ic_universidad_2").show();
					Ext.getCmp("aceptar").show();		
					fp.setTitle('<div><div style="float:left">Parametrización Universidad - IF</div>');
					
				}else if(tipoOp=='C') {
					catalogoUniversidad.load();
					Ext.getCmp("ic_if_1").show();
					Ext.getCmp("ic_universidad_2").show();
					Ext.getCmp("consulta").show();					
					fp.setTitle('<div><div style="float:left">Consultar Parametrización</div>');
				}
				
			}
		}
	});
	
	
	var  elementosForma  = [	
	 //  ------------ Parametrización IF - Universidad--------------------------
		{
			xtype: 'combo',
			fieldLabel: '* Intermediario Financiero',
			name: 'ic_if',
			id: 'ic_if_1',	
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',						
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIF,			
			listeners: {
				select:{ 
					fn:function (combo) {		
						var radio = Ext.getCmp("radio1");
						var radio1;
						if(radio.getValue()==true){ radio1 ='I'; }		
						if(radio1=='I'){							
							Ext.getCmp("ic_universidad_1").show();
							catalogoUniversidad.load();
						}						
					}
				}
			}
		},		
		{
			xtype: 'multiselect',
			fieldLabel: 'Universidad(es)-:',
			name: 'ic_universidad1',
			id: 'ic_universidad_1',
			hidden: true,
			width: 500,
			height: 200, 
			autoLoad: false,
			allowBlank:false,
			hidden: true,
			mode: 'local',
			store:catalogoUniversidad,
			displayField: 'descripcion',
			valueField: 'clave',		 
			tbar:[
				{
					text: 'Limpiar',
					handler: function(){
						fp.getForm().findField('ic_universidad_1').reset();
					}
				}
			],
			ddReorder: true					
		},
		 //  ------------ Parametrización Universidad - IF--------------------------
		 
		 {
			xtype: 'combo',
			fieldLabel: '* Universidad -Campus',
			name: 'ic_universidad2',
			id: 'ic_universidad_2',		
			mode: 'local',
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_universidad2',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,			
			store: catalogoUniversidad,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {		
						var radio = Ext.getCmp("radio2");
						var radio1;
						if(radio.getValue()==true){ radio1 ='U'; }		
						if(radio1=='U'){							
							Ext.getCmp("ic_if_2").show();
							catalogoIF.load();
						}						
					}
				}
			}
		},			
		{
			xtype: 'multiselect',
			fieldLabel: 'Intermediario(es):',
			name: 'ic_if',
			id: 'ic_if_2',
			mode: 'local',
			width: 500,
			height: 200, 
			autoLoad: false,
			allowBlank:false,
			hidden: true,
			store:catalogoIF,
			displayField: 'descripcion',
			valueField: 'clave',		 
			tbar:[
				{
					text: 'Limpiar',
					handler: function(){
						fp.getForm().findField('ic_if_2').reset();
					}
				}
			],
			ddReorder: true					
		}			
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: '<div><div style="float:left">Parametrización IF - Universidad</div>',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [
			{
				text: 'Aceptar',
				id: 'aceptar',
				iconCls: '',
				formBind: true,				
				handler: agregar
			},
			{
				text: 'Consultar',
				id: 'consulta',
				iconCls: '',
				formBind: true,
				hidden: true,
				handler: function(boton, evento) {	
					
					var ic_if_1 = Ext.getCmp("ic_if_1");
					var ic_universidad_2 = Ext.getCmp("ic_universidad_2");
					var ifs = ic_if_1.getValue();
					var ic_universidad = ic_universidad_2.getValue();
							
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							ifs:ifs,
							ic_universidad:ic_universidad						
						})
					});
				}
			}
		]
	});
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			radioTipo,
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
	catalogoIF.load();	
	Ext.getCmp('ic_universidad_2').hide();
});	
