Ext.onReady(function() {

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

	//----------------------------- NUEVOS COMPONENTES DE EXTJS --------------------------
	
	// Custom Action Column Component
	Ext.grid.CustomizedActionColumn = Ext.extend(Ext.grid.ActionColumn, {
		constructor: function(cfg) {
			  var me = this,
					items = cfg.items || (me.items = [me]),
					l = items.length,
					i,
					item;
	
			  Ext.grid.CustomizedActionColumn.superclass.constructor.call(me, cfg);
	
			  // Renderer closure iterates through items creating an <img> element for each and tagging with an identifying 
			  // class name x-action-col-{n}
			  me.renderer = function(v, meta) {
			  	   // Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
					v = Ext.isFunction(cfg.renderer) ? cfg.renderer.apply(this, arguments)||'' : '';
	
					meta.css += ' x-action-col-cell';
					for (i = 0; i < l; i++) {
						 item = items[i];
						 
						 // Check for icon position
						 var iconPosition =  !Ext.isEmpty(item.iconPosition) && "right" == item.iconPosition?"right":"left";
						
						 // Render icon to left side
						 if( "right" == iconPosition ){
						 	 
						 	 v += Ext.isEmpty(item.text)?'':'<a class="x-action-col-icon x-action-col-' + String(i) + '" style="color:#0000FF;text-decoration:underline;" >'+item.text+'</a>&nbsp;';
						 	 v += '<img alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
							  		'" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
							  		' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
							  		((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />'; 
							 
						 // Render icon to right side
						 } else if (  "left" == iconPosition  ){
						 	 
						 	 v += '<img alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
							  		'" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
							  		' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
							  		((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />'; 
							 v += Ext.isEmpty(item.text)?'':'&nbsp;<a class="x-action-col-icon x-action-col-' + String(i) + '" style="color:#0000FF;text-decoration:underline;" >'+item.text+'</a>';
							 //v += Ext.isEmpty(item.text)?"":"&nbsp;"+item.text;
						 }
						 
					}
					
					return v;
			  };
		 }
	});
	Ext.apply(Ext.grid.Column.types, { customizedactioncolumn: Ext.grid.CustomizedActionColumn }  );

//- - - - - - - - - - - - VARIABLE GLOBAS

//- - - - - - - - - - - -  FUNCIONES - - - - - - - - -  - - - - - - - - -  

	var replaceAll= function ( text, busca, reemplaza ){
	  while (text.toString().indexOf(busca) != -1)
			text = text.toString().replace(busca,reemplaza);
	  return text;
	}

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

	var procesarConsultaFolios = function(store, registros, opts){

		var forma = Ext.getCmp('forma');
		forma.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('gridFolios');

			if (!grid.isVisible()) {
				grid.show();
			}

			Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				el.unmask();
				
				resumenTotalesData.load();
				var totalesCmp = Ext.getCmp('gridTotales');
				if (!totalesCmp.isVisible()) {
					totalesCmp.show();
					totalesCmp.el.dom.scrollIntoView();
				}
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
		}
	}

	var procesarConsultaDataE = function(store, arrRegistros, opts) {
		//var botonT = Ext.getCmp('btnTotalesE');

		if (arrRegistros != null) {
			var gridE  = Ext.getCmp('gridE');

			if (!gridE.isVisible()) {
				gridE.show();
			}
			var el = gridE.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();

				resumenTotalesDataE.load();
				var totalesCmp = Ext.getCmp('gridTotalesE');
				if (!totalesCmp.isVisible()) {
					totalesCmp.show();
					totalesCmp.el.dom.scrollIntoView();
				}
				//botonT.enable();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				//botonT.disable();
			}
		}
	}

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

	var catalogoIfData = new Ext.data.JsonStore({
		id:				'catalogoIfDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'37validaGarant.data.jsp',
		baseParams:		{	informacion: 'CatalogoIf'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

  var catalogoGrupo = new Ext.data.JsonStore({
	   id:				'catologoGrupoStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'37validaGarant.data.jsp',
		baseParams:		{	informacion: 'CatologoGrupo'	},
		autoLoad:		false,
		listeners:		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

	var catalogoUnivData = new Ext.data.JsonStore({
		id: 			'catalogoUnivDataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'37validaGarant.data.jsp',
		baseParams: {
			informacion: 'CatalogoUniv'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			/*load: function(store,records,options){
 	
				var index = 0; //store.getCount()-1; 
				// El registro fue encontrado por lo que hay que seleccionarlo
				if ( store.getCount() > 0){
					
					var defaultValue 	= store.getAt(index).get('clave');
					
					var comboUniv		= Ext.getCmp("comboUniv");
					comboUniv.setValue(defaultValue);
					comboUniv.originalValue = defaultValue;
					// Debido a la validacion de validaciones extraidas, evita mostrar 
					// inmediatamente el error.
					comboUniv.clearInvalid();
					
				}
			
			},*/
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var foliosConsultadosData = new Ext.data.JsonStore({
		root: 		'registros',
		id:			'foliosConsultadosDataStore',
		url: 			'37validaGarant.data.jsp',
		baseParams: {	informacion:	'ConsultaFolios'	},
		fields: [
			{ name: 'FOLIO_OPERACION', 		type: 'auto' },
			{ name: 'NOMBRE_IF', 				type: 'string' },
			{ name: 'CLAVE_IF', 					type: 'string' },
			{ name: 'UNIVERSIDAD_CAMPUS',		type: 'string' },
			{ name: 'IC_UNIVERSIDAD',			type: 'string' },
			{ name: 'IC_GRUPO',					type: 'string' },
			{ name: 'MONTO_VALIDO',				type: 'float' },
			{ name: 'MONTO_GARANTIA',			type: 'float' },
			{ name: 'NUM_ACEPTADAS',			type: 'string' },
			{ name: 'NUM_RECHAZADAS',			type: 'string' },
			{ name: 'TOTAL_OPERACIONES',		type: 'string' },
			{ name: 'FECHA_HORA',				type: 'string' }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload:	{
				fn: function(store, options){
					/*
					console.log("options.params(beforeload)");
					console.dir(options.params);
					*/
				}
			},
			load: 	procesarConsultaFolios,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaFolios(null, null, null);
				}
			}
		}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '37validaGarant.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'REGISTROS',							mapping: 'REGISTROS'},
			{name: 'TOTAL_IMPORTE', type: 'float', mapping: 'TOTAL_IMPORTE'},
			{name: 'TOTAL_CONTRA',	type: 'float', mapping: 'TOTAL_CONTRA'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});

	var consultaE = new Ext.data.JsonStore({
		root: 'registros',
		id:	'consultaEStore',
		url: '37validaGarant.data.jsp',
		baseParams: {
			informacion: 'ConsultaE'
		},
		fields: [
					{name: 'CREDI'},
					{name: 'NOMBRE'},
					{name: 'RFC'},
					{name: 'MATRICULA'},
					{name: 'ESTUDIOS'},
					{name: 'FECHA_INGRESO'},
					{name: 'ANIO'},
					{name: 'PERIODO'},
					{name: 'MODALIDAD'},
					{name: 'FECHA_DIS'},
					{name: 'NUM_DIS'},
					{name: 'IMPORTE'},
					{name: 'MONTO_TOTAL'},
					{name: 'SALDOLINEAS'},
					{name: 'FG_PORCENTAJE_APORTACION'},
					{name: 'FN_MONTO_GARANTIA'},
					{name: 'MODALIDAD_BASES_OPER'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load:		procesarConsultaDataE,
			exception: {
						fn: function(proxy,type,action,optionsRequest,response,args){
								NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
								procesarConsultaDataE(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
						}
			}
		}
	});

	var resumenTotalesDataE = new Ext.data.JsonStore({
		root : 'registros',
		url : '37validaGarant.data.jsp',
		baseParams: {
			informacion: 'ResumenTotalesE'
		},
		fields: [
			{name: 'REGISTROS',							mapping: 'REGISTROS'},
			{name: 'TOTAL_IMPORTE', type: 'float', mapping: 'TOTAL_IMPORTE'},
			{name: 'TOTAL_MONTO', 	type: 'float', mapping: 'TOTAL_MONTO'},
			{name: 'TOTAL_SALDO',	type: 'float', mapping: 'TOTAL_SALDO'},
			{name: 'TOTAL_CONTRA',	type: 'float', mapping: 'TOTAL_CONTRA'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});

//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - 

	var procesaConsultaFolios = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							consultaFolios(resp.estadoSiguiente,resp);
						}
					);
				} else {
					consultaFolios(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var consultaFolios = function(estadoSiguiente, respuesta){

		if(  estadoSiguiente == "CONSULTA_FOLIOS" 													){

			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');

			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridFolios").hide();
			//Ocultar grid Totales
			var totales = Ext.getCmp('gridTotales');
			if (totales.isVisible()) {
				totales.hide();
			}
			
			Ext.StoreMgr.key('foliosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'ConsultarFolios',
								operacion: 'Generar',
								start: 0,
								limit: 15
							}
				)
			});

		} else if(  estadoSiguiente == "VALIDA_GARANTIA_ACEPTAR"										){

				Ext.getCmp('formaAceptar').getForm().reset();
				Ext.getCmp('winConfirma.folioOpera').setValue(respuesta.folioOpera);
				Ext.getCmp('winConfirma').show();

		} else if(  estadoSiguiente == "CONTINUAR_PROCESO_ACEPTAR" 									){

 			Ext.Ajax.request({
				url:			'37validaGarant.data.jsp',
				params:		Ext.apply(Ext.getCmp('formaAceptar').getForm().getValues(),
									{	
										informacion:		'validaGarantia.accion.aceptar'
									}
								),
				callback:	procesaConsultaFolios
			});

		} else if(  estadoSiguiente == "RESPUESTA_PROCESO_ACEPTAR" 									){

			Ext.Msg.alert("Mensaje de p�gina web",
								"La operaci�n se realiz� con �xito.",
								function(){
									//Ext.getCmp("barraPaginacion").doRefresh();
									Ext.getCmp('winConfirma').hide();
									consultaFolios("FIN",null);
								}
			);

		} else if(  estadoSiguiente == "VALIDA_GARANTIA_RECHAZAR"									){
				
				Ext.getCmp('winRechazo.folioOpera').setValue(respuesta.folioOpera);
				Ext.getCmp('winRechazo.causaRechazo').setValue("");
				Ext.getCmp('winRechazo.causaRechazo').reset();
				Ext.getCmp('winRechazo').show();

		} else if(  estadoSiguiente == "CONTINUAR_PROCESO_RECHAZO" 									){

			var folioOpera		= respuesta.folioOpera;
			var causaRechazo	= respuesta.causaRechazo;

 			Ext.Ajax.request({
				url:			'37validaGarant.data.jsp',
				params:		{	informacion:		'validaGarantia.accion.rechazo',
									folio:				folioOpera,
									causaRechazo:		causaRechazo
								},
				callback:	procesaConsultaFolios
			});

		} else if(  estadoSiguiente == "RESPUESTA_PROCESO_RECHAZO" 									){

			Ext.getCmp('btnRechazo').setIconClass('icoAceptar');

			Ext.Msg.alert("Mensaje de p�gina web",
								"El rechazo se registr� exitosamente.",
								function(){
									//Ext.getCmp("barraPaginacion").doRefresh();
									Ext.getCmp('winRechazo').hide();
									consultaFolios("FIN",null);
								}
			);

		} else if(	estadoSiguiente == "NUM_OPERACIONES_ACEPTADAS"								){

			Ext.getCmp('verEstudiantes').setTitle('Operaciones Aceptadas');
			Ext.getCmp('labelFolio').setText('Folio Operaci�n: '				+respuesta.folioOpera,	false);
			Ext.getCmp('labelIf').setText(	'Intermediario Financiero: '	+respuesta.nombreIf,		false);
			Ext.getCmp('labelFecha').setText('Fecha y Hora de Operaci�n: '	+respuesta.fechaHora,	false);
			Ext.getCmp('verEstudiantes').show();
			var totalesCmp = Ext.getCmp('gridTotalesE');
			if (totalesCmp.isVisible()) {
				totalesCmp.hide();
			}

			consultaE.load({
				params: Ext.apply({
					estatusReg:			respuesta.estatusReg,
					folioOperacion:	respuesta.folioOpera,
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15
				})
			});

		} else if(	estadoSiguiente == "LIMPIAR"														){

			Ext.getCmp('forma').getForm().reset();
			grid.hide();
			var totales = Ext.getCmp('gridTotales');
			if (totales.isVisible()) {
				totales.hide();
			}

		} else if(	estadoSiguiente == "FIN"															){
						
			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "37validaGarantExt.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

	var gridTotalesE =  new Ext.grid.GridPanel({
		store:resumenTotalesDataE,	id:'gridTotalesE',	view:new Ext.grid.GridView({forceFit:true}),	width:930,	height:100,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'Numero de Registros',	dataIndex: 'REGISTROS',	align: 'left',	width: 150, menuDisabled:true
			},{
				header: 'Importe Disposici�n',	dataIndex: 'TOTAL_IMPORTE',	width: 200,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),menuDisabled:true
			},{
				header: 'Monto Total',dataIndex: 'TOTAL_MONTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),menuDisabled:true
			},{
				header: 'Saldo Lineas',	dataIndex: 'TOTAL_SALDO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),menuDisabled:true
			},{
				header: 'Monto de Contragarant�a<br/> a pagar',	dataIndex: 'TOTAL_CONTRA',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),menuDisabled:true
			}
		]/*,
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]*/
	});

	var gridE = new Ext.grid.GridPanel({
		store:	consultaE,
		id:		'gridE',
		style:	'margin:0 auto;',
		columns:[
			{
				header:		'Cr�dito',
				tooltip:		'Cr�dito',
				sortable:	true,
				dataIndex:	'CREDI',
				width:		150,
				align:		'center'
			},
			{
				header:		'Nombre de Acreditado',
				tooltip:		'Nombre de Acreditado',
				sortable:	true,
				dataIndex:	'NOMBRE',
				width:		150,
				align:		'center'
			},
			{
				header:		'RFC',
				tooltip:		'RFC',
				sortable:	true,
				dataIndex:	'RFC',
				width:		150,
				align:		'center'
			},
			{
				header:		'Matric�la',
				tooltip:		'Matric�la',
				sortable:	true,
				dataIndex:	'MATRICULA',
				width:		130,
				align:		'center'
			},
			{
				align:'center',
				header: 'Estudios',
				tooltip: 'Estudios',
				sortable: true,
				dataIndex: 'ESTUDIOS',
				width: 130
			},{
				header:		'Fecha de Ingreso',
				tooltip:		'Fecha de Ingreso',
				sortable:	true,
				dataIndex:	'FECHA_INGRESO',
				width:		130,
				align:		'center'
			},
			{
				header: 'A�o de Colocaci�n',
				tooltip: 'A�o de Colocaci�n',
				sortable: true,
				dataIndex: 'ANIO',
				width: 150,
				align: 'center'
			},
			{
				header: 'Periodo Educativo',
				tooltip: 'Periodo Educativo',
				sortable: true,
				dataIndex: 'PERIODO',
				width: 130,
				align: 'center'
			},{
				header: 'Modalidad Programa',
				tooltip: 'Modalidad Programa',
				sortable: true,
				dataIndex: 'MODALIDAD',
				width: 130,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						return value;
					}
			},
			{
				header:		'Fecha �ltima Disposici�n',
				tooltip:		'Fecha �ltima Disposici�n',
				sortable:	true,
				dataIndex:	'FECHA_DIS',
				width:		150,
				align:		'center'
			},
			{
				header: 'No. Disposiciones',
				tooltip: 'No. Disposiciones',
				sortable: true,
				dataIndex: 'NUM_DIS',
				width: 130,
				align: 'left'
			},
			{
				align:'center',
				header: 'Importe Disposici�n',
				tooltip: 'Importe Disposici�n',
				sortable: true,
				dataIndex: 'IMPORTE',
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Total',
				tooltip: 'Monto Total',
				sortable: true,
				dataIndex: 'MONTO_TOTAL',
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Saldo de Linea',
				tooltip: 'Importe del Dep�sito',
				sortable: true,
				dataIndex: 'SALDOLINEAS',
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Modalidad',
				tooltip: 'Modalidad',
				sortable: true,
				dataIndex: 'MODALIDAD_BASES_OPER',
				width: 200,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						return value;
					}
			},
			{
				header: '% de aportaci�n',
				tooltip: '% de aportaci�n',
				sortable: true,
				dataIndex: 'FG_PORCENTAJE_APORTACION',
				width: 150,
				align: 'center'
			},
			{
				header:		'Monto de Contragarant�a a pagar',
				tooltip:		'Monto de Contragarant�a a pagar',
				sortable:	true,
				dataIndex:	'FN_MONTO_GARANTIA',
				width:		150,
				align:		'right',
				renderer:	Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows:	true,
		loadMask:	true,
		height:		400,
		width:		930,
		style:		'margin:0 auto;',
		frame:		false,
		bbar: {
			xtype:		'paging',
			pageSize:	15,
			buttonAlign:'left',
			id:			'barraPaginacion1',
			displayInfo:true,
			store:		consultaE,
			displayMsg:	'{0} - {1} de {2}',
			emptyMsg:	"No hay registros."/*,
			items:		[
				'->','-',
				{
						xtype:	'button',
						text:		'Totales',
						id:		'btnTotalesE',
						hidden:	false,
						handler:	function(boton, evento) {
										resumenTotalesDataE.load();
										var totalesCmp = Ext.getCmp('gridTotalesE');
										totalesCmp.show();
										if (!totalesCmp.isVisible()) {
											totalesCmp.show();
											totalesCmp.el.dom.scrollIntoView();
										}
						}
				},'-'
			]*/
		}
	});

	var gridTotales =  new Ext.grid.GridPanel({
		store:resumenTotalesData,	id:'gridTotales',	view:new Ext.grid.GridView({forceFit:true}),	width:930,	height:100,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'Numero de Registros',	dataIndex: 'REGISTROS',	align: 'right',	width: 200, menuDisabled:true
			},{
				header: 'Monto Valido',	dataIndex: 'TOTAL_IMPORTE',	width: 300,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,0.00'),menuDisabled:true
			},{
				header: 'Monto de Contragarant�a<br/> a pagar',	dataIndex: 'TOTAL_CONTRA',	width:300,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			}
		]/*,
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]*/
	});

	// Grid con las cuentas registradas
	var grid = new Ext.grid.GridPanel({
		store:		foliosConsultadosData,
		id:			'gridFolios',
		style:		'margin: 0 auto; padding-top:10px;',
		hidden:		true,
		margins:		'20 0 0 0',
		title:		undefined,
		view:			new Ext.grid.GridView({markDirty:	false}),
		stripeRows:	true,
		loadMask:	true,
		height: 		400,
		width: 		930,
		frame: 		false,
		columns: [
			{
				header: 		'<div align="center">Intermediario <br>Financiero</div>',
				tooltip: 	'Intermediario Financiero',
				dataIndex: 	'NOMBRE_IF',
				align:		'left',
				sortable: 	true,
				resizable: 	true,
				width: 		250,
				hidden: 		false,
				hideable:	false,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'<div align="center">Universidad - Campus</div>',
				tooltip: 	'Universidad - Campus',
				dataIndex: 	'UNIVERSIDAD_CAMPUS',
				align:		'left',
				sortable: 	true,
				resizable: 	true,
				width: 		250,
				hidden: 		false,
				hideable:	false,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'Fecha y hora <br>de Operaci�n',
				tooltip: 	'Fecha y hora de operaci�n',
				dataIndex: 	'FECHA_HORA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hideable:	false,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'Folio <br>operaci�n',
				tooltip: 	'Folio operaci�n',
				dataIndex: 	'FOLIO_OPERACION',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				hidden: 		false,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				header: 		'Monto validado',
				tooltip: 	'Monto validado',
				dataIndex: 	'MONTO_VALIDO',
				align:		'right',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				hidden: 		false,
				renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
            xtype: 		'actioncolumn',
            header: 		'No. Operaciones <br>aceptadas',
				tooltip: 	'No. Operaciones aceptadas',
            width: 		100,
            align:		'center',
            hidden: 		false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return (record.get('NUM_ACEPTADAS'));
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(	!Ext.isEmpty(registro.get('NUM_ACEPTADAS')) && registro.get('NUM_ACEPTADAS') != '0'	)	{
								this.items[0].tooltip = 'Ver detalle de operaciones aceptadas';
								return 'iconoLupa';
							}else	{
								return value;
							}
						},
						handler: function(grid, rowIndex, colIndex) {

									var record = Ext.StoreMgr.key('foliosConsultadosDataStore').getAt(rowIndex);
                        	var respuesta = new Object();
                        	respuesta['folioOpera']	= record.json['FOLIO_OPERACION'];
									respuesta['claveIf']		= record.json['CLAVE_IF'];
									respuesta['nombreIf']	= record.json['NOMBRE_IF'];
									respuesta['fechaHora']	= record.json['FECHA_HORA'];
									respuesta['estatusReg']	= "S";
                        	consultaFolios("NUM_OPERACIONES_ACEPTADAS",respuesta);
                    }
					}
				]
			},{
            header: 		'No. Operaciones <br>rechazadas',
				tooltip: 	'No. Operaciones rechazadas',
				dataIndex:	'NUM_RECHAZADAS',
            width: 		100,
            align:		'center',
            hidden: 		false
			},{
				header: 		'Total <br>operaciones',
				tooltip: 	'Total operaciones',
				dataIndex: 	'TOTAL_OPERACIONES',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				hidden: 		false
			},{
				header: 		'<div align="center">Monto contragarantia <br>a pagar</div>',
				tooltip: 	'Monto contragarantia a pagar',
				dataIndex: 	'MONTO_GARANTIA',
				align:		'right',
				sortable: 	true,
				resizable: 	true,
				width: 		150,
				hidden: 		false,
				renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				xtype:		'actioncolumn',
				header:		'Acciones',
				align:		'center',
				width:		100,
				resizable: 	false,
				sortable:	false,
				hidden:		false,
				hideable:	false,
				menuDisabled:true,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Aceptar Contragarant�a';
								return 'icoAceptar';
						},
						handler: function(grid, rowIndex, colIndex) {

									var record = Ext.StoreMgr.key('foliosConsultadosDataStore').getAt(rowIndex);
                        	var respuesta = new Object();
                        	respuesta['folioOpera']	= record.json['FOLIO_OPERACION'];
                        	consultaFolios("VALIDA_GARANTIA_ACEPTAR",respuesta);
                  }
					},{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
								this.items[1].tooltip = 'Rechazar Contragarant�a';
								return 'icoRechazar';
						},
						handler: function(grid, rowIndex, colIndex) {

									var record = Ext.StoreMgr.key('foliosConsultadosDataStore').getAt(rowIndex);
                        	var respuesta = new Object();
                        	respuesta['folioOpera']	= record.json['FOLIO_OPERACION'];
                        	consultaFolios("VALIDA_GARANTIA_RECHAZAR",respuesta);
                  }
					}
				]
			}
		],
		bbar: {
			xtype:		'paging',
			pageSize:	15,
			buttonAlign:'left',
			id:			'barraPaginacion',
			displayInfo:true,
			store:		foliosConsultadosData,
			displayMsg:	'{0} - {1} de {2}',
			emptyMsg:	"No hay registros."/*,
			items:		[
				'->','-',
				{
						xtype:	'button',
						text:		'Totales',
						id:		'btnTotales',
						hidden:	false,
						handler:	function(boton, evento) {
										resumenTotalesData.load();
										var totales = Ext.getCmp('gridTotales');
										if (!totales.isVisible()) {
											totales.show();
											totales.el.dom.scrollIntoView();
										}
						}
				},'-'
			]*/
		}
	});

	var verEstudiantes = new Ext.Window({
		id:				'verEstudiantes',
		width:			940,
		autoHeight:		true,
		maximizable:	false,
		modal:			true,
		closeAction:	'hide',
		resizable:		false,
		maximized:		false,
		constrain:		true,
		renderHidden:	true,
		buttonAlign:	'center',
		items: [
			{
				xtype: 	'label',
				id:	 	'labelFolio',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				html:  	'Folio Operaci�n:'
			},
			{
				xtype: 	'label',
				id:	 	'labelIf',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				html:  	'Intermediario Financiero:'
			},
			{
				xtype: 	'label',
				id:	 	'labelFecha',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				html:  	'Fecha y hora de operaci�n:'
			},
			gridE
			,gridTotalesE
		],
		buttons:[
			{
				xtype:	'button',
				text: 	'Cerrar',
				handler:	function(boton, evento) {
								Ext.getCmp('verEstudiantes').hide();
				}
			}
		]
	});

	var winRechazo = new Ext.Window({
		id:				"winRechazo",
		title:			"Rechazar",
		layout:			"form",
		width:			400,
		height:			300,
		maximizable:	false,
		modal:			true,
		closeAction:	'hide',
		closable:		false,
		resizable:		false,
		maximized:		false,
		constrain:		true,
		constrainHeader:true,
		renderHidden:	true,
		buttonAlign:	"center",
		labelAlign:		"top",
		bodyStyle:		'padding: 5px',
		items: [
			{
				xtype:		"textarea",
				fieldLabel:	"Favor de capturar las causas de rechazo",
				id:			"winRechazo.causaRechazo",
				name:			"winRechazo.causaRechazo",
				msTarget:	'side',
				width:		376,
				maxLength:	1000,
				allowBlank:	false,
				height:		208
			},{
				xtype:		"hidden",
				id:			"winRechazo.folioOpera",
				name:			"winRechazo.folioOpera",
				value:		""
			}
		],
		buttons:[
			{
				xtype:	'button',
				text:		'Aceptar',
				id:		'btnRechazo',
				iconCls:	'icoAceptar',
				handler:	function(boton, evento) {
								var causaRechazo = Ext.getCmp('winRechazo.causaRechazo').getValue();
								if(	Ext.isEmpty(causaRechazo)	){
									Ext.Msg.alert("Mensaje de p�gina web",
														"Las causas de rechazo son obligatorias.",
														function(){
															Ext.getCmp('winRechazo.causaRechazo').focus();
														}
									);
									return;
								}
								if(	causaRechazo.length > 1000	){
									Ext.Msg.alert("Mensaje de p�gina web",
														"Las causas de rechazo exceden la longitud <br>m�xima de 1000 caracteres.",
														function(){
															Ext.getCmp('winRechazo.causaRechazo').focus();
														}
									);
									return;
								}

								boton.disable();
								boton.setIconClass('loading-indicator');

								var respuesta					= new Object();
								respuesta['folioOpera']		= Ext.getCmp('winRechazo.folioOpera').getValue();
								respuesta['causaRechazo']	= causaRechazo;
								consultaFolios("CONTINUAR_PROCESO_RECHAZO",respuesta);

							}
			},{
				xtype:	'button',
				text: 	'Cancelar',
				iconCls:	'icoRechazar',
				handler:	function(boton, evento) {
								Ext.getCmp('winRechazo').hide();
							}
			}
		]
	});

	var winConfirma = new Ext.Window({
		id:				"winConfirma",
		title:			"Aceptar Contragarant�a",
		width:			600,
		height:			280,
		maximizable:	false,
		modal:			true,
		closeAction:	'hide',
		closable:		false,
		resizable:		false,
		maximized:		false,
		constrain:		true,
		constrainHeader:true,
		renderHidden:	true,
		layout:			'fit',
		items: [
			{
				xtype:			'form',
				id:				'formaAceptar',
				title:			'Favor de capturar los datos del dep�sito',
				style:			'margin:0 auto;',
				bodyStyle:		'padding: 10px',
				labelWidth:		150,
				defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
				monitorValid:	true,
				items:			[
					{
						xtype:			'datefield',
						name:				'fechaDeposito',
						id:				'_fechaDeposito',
						fieldLabel:		'Fecha de Dep�sito',
						allowBlank:		false,
						startDay:		0,
						anchor:			'50%',
						margins:			'0 20 0 0'
					},{
						xtype: 'bigamount',
						name: 			'importe',
						id: 				'_importe',
						fieldLabel:		'Importe',
						blankText:		'Favor de capturar el importe',
						minValue:		1
					},{
						xtype:			'textfield',
						name:				'Banco',
						id:				'_Banco',
						allowBlank:		false,
						fieldLabel:		'Banco de Dep�sito',
						maxLength:		100
					},{
						xtype:			'textfield',
						name:				'referencia',
						id:				'_referencia',
						allowBlank:		false,
						fieldLabel:		'Clave de Referencia',
						maxLength:		22
					},{
						xtype: 		'label',
						id:	 		'labelInformacion',
						cls:			'x-form-item',
						style: 		'text-align:center;margin:14px;',
						html:  		'Al realizar la confirmaci�n electr�nica, acepta que los recursos han sido depositados en tiempo y forma de acuerdo a t�rminos y condiciones del programa de Cr�dito Educativo.'
					},{
						xtype:		"hidden",
						id:			"winConfirma.folioOpera",
						name:			"winConfirma.folioOpera",
						value:		""
					}
				],
				buttons:[
					{
						xtype:	'button',
						id:		'btnAceptar',
						text:		'Continuar',
						iconCls:	'icoContinuar',
						formBind:true,
						handler:	function(boton, evento) {

										if(	!Ext.getCmp('formaAceptar').getForm().isValid()	){
											return;
										}

										var imp	=	replaceAll(Ext.getCmp('_importe').getValue(),',','');
										if(imp.length > 25){
											Ext.Msg.alert('Mensaje de p�gina web','El tama�o m�ximo para el importe es de 22 enteros y 2 decimales');
											return;
										}
										Ext.getCmp('formaAceptar').getEl().mask('Enviando...', 'x-mask-loading');
										consultaFolios("CONTINUAR_PROCESO_ACEPTAR",null);
		
									}
					},{
						xtype:	'button',
						text: 	'Cancelar',
						iconCls:	'icoRechazar',
						handler:	function(boton, evento) {
										Ext.getCmp('winConfirma').hide();
									}
					}
				]
			}
		]
	});

//-----------------------EVENTO PARA GRID, PARA CACHAR EL CHECKCOLUMN - - - - - - - - - - -
	grid.getStore().on('update', function(store, record, action){
		if(!presionCheck){
			if (action == Ext.data.Record.EDIT) {
				var dato = record.getChanges();
				if(dato.ACEPTADOS	&&	record.get('RECHAZADOS')){
					record.set('RECHAZADOS', false);
				}else if(dato.RECHAZADOS	&&	record.get('ACEPTADOS')){
					record.set('ACEPTADOS', false);
				}
				record.commit();
			}
		}
	});

	var elementosForma = [
		{
			xtype:			'combo',
			id:				'cboIf',
			name:				'claveIf',
			hiddenName:		'claveIf',
			fieldLabel:		'Intermediario Financiero',
			emptyText:		'Seleccionar. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			typeAhead:		true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoIfData,
			tpl:				NE.util.templateMensajeCargaCombo/*,
			listeners:		{
				'select':function(cbo){
								var comboUniv = Ext.getCmp('comboUniv');
								comboUniv.setValue('');
								comboUniv.store.removeAll();
								comboUniv.store.reload({	params: {claveIf:	cbo.getValue()}	});
							}
			}*/
		},{
			xtype:			'combo',
			id:				'comboGrupo',
			name:				'claveGrupo',
			hiddenName:		'claveGrupo',
			emptyText:		'Seleccionar Grupo',
			fieldLabel:		'Grupo',
			displayField: 'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			typeAhead:		true,
			mode:				'local',
			minChars:		1,
			forceSelection:true,
			store:			catalogoGrupo,
			tpl:				NE.util.templateMensajeCargaCombo,
			listeners:		{
				'select':function(cbo){
								var comboUniv = Ext.getCmp('comboUniv');
								comboUniv.setValue('');
								comboUniv.store.removeAll();
								comboUniv.store.reload({
									params: {
												claveGrupo:	cbo.getValue()
									}
								});
							}
			}
		},{
			//Universidad
			xtype: 				'combo',
			name: 				'claveUniversidad',
			id: 					'comboUniv',
			fieldLabel: 		'Universidad - Campus',
			allowBlank: 		true,
			boxMinWidth:		100,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveUniversidad',
			emptyText: 			'Seleccione...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoUnivData,
			tpl: 					NE.util.templateMensajeCargaCombo
		},{
			//Folio de operaci�n
			xtype:			'textfield',
			name:				'folioOpera',
			id:				'_folioOpera',
			allowBlank:		true,
			fieldLabel:		'Folio de operaci�n',
			anchor:			'60%',
			maxLength:		30,
			maskRe:			/[0-9]/
		}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		title:			'Criterios de b�squeda',
		width:			610,
		style:			'margin:0 auto;',
		collapsible:	true,
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		monitorValid:	true,
		buttons: [
			{
				text:		'Consultar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:true,
				handler: function(boton, evento) {

								if(!Ext.getCmp('forma').getForm().isValid()){
									return;
								}

								consultaFolios("CONSULTA_FOLIOS",null);

				} //fin handler
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								consultaFolios("LIMPIAR",null);
							}
			},{
				text:		'Cancelar',
				id:		'btnCancelar',
				iconCls:	'borrar',
				hidden:	true,
				handler:	function(){
								consultaFolios("FIN",null);
							}
			}
		]
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,verEstudiantes,winRechazo,winConfirma,
			NE.util.getEspaciador(5),
			grid,gridTotales
		],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Ocultar mascara del panel de resumen de carga de plantillas
			element = Ext.getCmp('gridFolios').getGridEl();
			if( element.isMasked()){
				element.unmask();
			}

         element = Ext.getCmp("formaAceptar").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			// Derefenrencia ultimo elemento...
			element = null;

		}
	});

	Ext.StoreMgr.key('catalogoIfDataStore').load();
	Ext.StoreMgr.key('catologoGrupoStore').load();
 	Ext.StoreMgr.key('catalogoUnivDataStore').load();
	consultaFolios("CONSULTA_FOLIOS",null);

});
