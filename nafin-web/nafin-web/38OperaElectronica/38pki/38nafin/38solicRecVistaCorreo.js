Ext.ns('NE.solic.recursos');


NE.solic.recursos.FormVistaPreviaCorreo = Ext.extend(Ext.Panel,{
	tipoVista: null,
	nombreIf: null,
	nombreUserIf: null,
	nombreMoneda: null,
	fechaActual: null,
	datosGerSub: null,
	observOP: null,
	causasRech: null,
	dataSolicCorreo: null,
	dataTotalesCorreo: null,
	observaciones: null,
	importeTotal: null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'fpVistaPreviaCorreo',
			width: 810,
			height: 850,
			title: 'Vista Previa del Correo',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			labelWidth: 150,
			style: 'margin:0 auto;',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(this),
			buttons: this.generarBotones(this)
		});
		
		NE.solic.recursos.FormVistaPreviaCorreo.superclass.initComponent.call(this);
	},
	generarCampos: function(fp){
		var tipoVista = fp.tipoVista;
		var importe = Ext.util.Format.number(fp.importeTotal,'$0,0.00');
		var titulo = '';
		var textoA = '';
		var textoB = '';
		var txtObs = '';
		var txtAtte = '';
		if(tipoVista=='A'){
			fp.observaciones = fp.observOP;
			var puesto = "";
			if(fp.datosGerCon.GERENTE_NOMBRE!=''){
				if(fp.datosGerCon.CONCENTRADOR=="1"){
					puesto ="Gerente de Operaciones de Cr�dito";
				}
				if(fp.datosGerCon.CONCENTRADOR=="2"){
					puesto ="Mesa de Control de Cr�dito";
				}
				txtAtte = fp.datosGerCon.GERENTE_NOMBRE+'<br>' +
				puesto+'<br>' +
				'<u>'+fp.datosGerCon.GERENTE_MAIL+'</u><br>' +
				'Tel. '+fp.datosGerCon.GERENTE_TELEFONO+'.</p><br><br>';
			}
			titulo = 'Confirmaci�n de Abono';
			textoA = 'Por este conducto, hago de su conocimiento que el d�a de hoy se abon� a la cuenta '+
						'de cheques que nos fue proporcionada, la cantidad de '+importe+' '+fp.nombreMoneda+
						' derivado de la realizaci�n de sus operaciones de cr�dito solicitadas ante esta instituci�n, de acuerdo con  el siguiente detalle:';
			txtObs = 'Observaciones Ejecutivo OP:';
		}else if(tipoVista=='R'){
			fp.observaciones = fp.causasRech;
			if(fp.datosGerCon.GERENTE_NOMBRE!=''){
				if(fp.datosGerCon.CONCENTRADOR=="1"){
					puesto ="Gerente de Operaciones de Cr�dito";
				}
				if(fp.datosGerCon.CONCENTRADOR=="2"){
					puesto ="Mesa de Control de Cr�dito";
				}
				txtAtte = fp.datosGerCon.GERENTE_NOMBRE+'<br>' +
				puesto+'<br>' +
				'<u>'+fp.datosGerCon.GERENTE_MAIL+'</u><br>' +
				'Tel. '+fp.datosGerCon.GERENTE_TELEFONO+'.</p><br><br>';
			}
			titulo = 'Solicitud de Recursos Rechazada';
			textoA = 'Por este medio, se hace de su conocimiento, que la operaci�n de cr�dito solicitada a Nacional Financiera, ' +
						'S.N.C. por un importe total de '+importe+' '+fp.nombreMoneda+', no se concluy� como tr�mite de disposici�n, en virtud de las causas se�aladas a continuaci�n:';
			textoB = 'Derivado de lo anterior y con el prop�sito de estar en posibilidades de atender su solitud de recursos, le agradecemos a la brevedad posible las correcciones y/o aclaraciones correspondientes.<br><br>';
			txtObs = 'Causas de Rechazo:';
		}
		return [
				{
					xtype:'panel',
					name:'p_titulo',
					id:'p_titulo1',
					anchor: '100%',
					html: '<p align="center"><b>'+titulo+'</b><br><br></p>'
				},
				{
					xtype:'panel',
					name:'p_encabezadoA',
					id:'p_encabezadoA1',
					layout:'hbox',
					anchor: '100%',
					items:[
						{
							xtype:'panel',
							width:'560',
							html: '<p align="left"><b>'+fp.nombreIf+'</b><br><b>'+fp.nombreUserIf+'</b><br><br><br></p>'
						},
						{
							xtype:'panel',
							width:'200',
							html: '<p align="right">'+fp.fechaActual+'</p>'
						}
					]
				},
				{
					xtype:'panel',
					name:'p_textoA',
					id:'p_textoA1',
					anchor: '100%',
					html: '<p align="left">'+textoA+'</p><br><br>'
				},
				NE.util.getEspaciador(10),
				{
					xtype: 'grid',
					name: 'gridSolicsCorreo',
					style:	'margin:0 auto;',
					frame: true,
					margins: '20 0 0 0',
					height: 200,
					width: (tipoVista=='A')?'600':'360',
					store: new Ext.data.ArrayStore({
						  fields: [
							  {name: 'PRESTAMO'},
							  {name: 'CVESOLIC'},
							  {name: 'NOMBREMONEDA'},
							  {name: 'FOLIO'},
							  {name: 'IMPORTE'},
							  {name: 'FECOP'},
							  //{name: 'HORAOP'},
							  {name: 'NUMCTA'},
							  {name: 'NOMBREBANCO'},
							  {name: 'NUMCLABE'}
						  ],
						  data: fp.dataSolicCorreo
					 }),
					columns: [
						{
							header: '<p align="center">Pr�stamo</p>',
							tooltip: 'Prestamo',
							dataIndex: 'PRESTAMO',
							sortable: true,
							hideable: false,
							width: 100,
							align: 'left',
							hidden: (tipoVista=='A')?false:true
						},
						{
							header: '<p align="center">N�mero de Slicitud</p>',
							tooltip: 'N�mero de Slicitud',
							dataIndex: 'CVESOLIC',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'left',
							hidden: (tipoVista=='R')?false:true
						},
						{
							header: '<p align="center">No. Folio</p>',
							tooltip: 'No. Folio',
							dataIndex: 'FOLIO',
							sortable: true,
							hideable: false,
							width: 100,
							align: 'left',
							hidden: (tipoVista=='R')?false:true
						},
						{
							header: '<p align="center">Importe</p>',
							tooltip: 'Importe',
							dataIndex: 'IMPORTE',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'right',
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: '<p align="center">Moneda</p>',
							tooltip: 'Moneda',
							dataIndex: 'NOMBREMONEDA',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'left',
							hidden: false
						},
						{
							header: '<p align="center">Fecha Operado</p>',
							tooltip: 'Fecha Operado',
							dataIndex: 'FECOP',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'left',
							hidden: (tipoVista=='A')?false:true
						},
						/*{
							header: '<p align="center">Hora Operado</p>',
							tooltip: 'Hora Operado',
							dataIndex: 'HORAOP',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'left',
							hidden: (tipoVista=='A')?false:true
						},*/
						{
							header: '<p align="center">N�mero de Cuenta</p>',
							tooltip: 'N�mero de Cuenta',
							dataIndex: 'NUMCTA',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'left',
							hidden: (tipoVista=='A')?false:true
						},
						{
							header: '<p align="center">Cuenta CLABE</p>',
							tooltip: 'Cuenta CLABE',
							dataIndex: 'NUMCLABE',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'center',
							hidden: (tipoVista=='A')?false:true
						},
						{
							header: '<p align="center">Nombre Banco</p>',
							tooltip: 'Nombre Banco',
							dataIndex: 'NOMBREBANCO',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'left',
							hidden: (tipoVista=='A')?false:true
						}
					]
				},
				NE.util.getEspaciador(10),
				{
					xtype: 'grid',
					name: 'gridSolicsTotales',
					title: 'Totales',
					style:	'margin:0 auto;',
					margins: '20 0 0 0',
					height: 100,
					width: '360',
					frame: true,
					store: new Ext.data.ArrayStore({
						  fields: [
							  {name: 'NUMPRESTAMOS'},
							  {name: 'MONEDA'},
							  {name: 'IMPORTETOTAL'}
						  ],
						  data: fp.dataTotalesCorreo
					 }),
					columns: [
						{
							header: '<p align="center">No. Prestamos</p>',
							tooltip: 'No. Prestamos',
							dataIndex: 'NUMPRESTAMOS',
							sortable: true,
							hideable: false,
							width: 100,
							align: 'left',
							hidden: false
						},
						{
							header: '<p align="center">Moneda</p>',
							tooltip: 'Moneda',
							dataIndex: 'MONEDA',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'left',
							hidden: false
						},
						{
							header: '<p align="center">Importe Total</p>',
							tooltip: 'Importe Total',
							dataIndex: 'IMPORTETOTAL',
							sortable: true,
							hideable: false,
							width: 120,
							align: 'right',
							hidden: false,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						}
					]
				},
				NE.util.getEspaciador(10),
				{
					xtype:'panel',
					name:'p_observ',
					id:'p_observ1',
					layout:'form',
					anchor: '100%',
					labelWidth: 1,
					items:[
						{
							xtype:'panel',
							html: txtObs
						},
						{
							xtype:'textarea',
							name: 'observEjecOP',
							id: 'observEjecOP_',
							maxLength: 800,
							width:'400',
							value: (tipoVista=='A')?fp.observOP:fp.causasRech,
							fieldLabel: "",
							listeners:{
								blur:function(obj){									
									fp.observaciones=obj.getValue();
								}
							}
						}
					]
				},
				{
					xtype:'panel',
					name:'p_textoB',
					id:'p_textoB1',
					anchor: '100%',
					html: '<p align="left">'+textoB+'Sin otro particular, reciba un cordial saludo.<br><br>'+
								'ATENTAMENTE<br>' +txtAtte
				},
				{
					xtype:'panel',
					name:'p_textoC',
					id:'p_textoC1',
					anchor: '100%',
					html: '<p align="left">Con fundamento en el articulo 142 de la Ley de Instituciones de Cr�dito, ' +
							'14 fracci�n I y 15 de la Ley Federal de Transparencia y Acceso a la informaci�n P�blica Gubernamental, ' +
							'asi como al art�culo 30 de su Reglamento, el contenido del presente mensaje de correo electr�nico es de caracter Reservado.<br>'
				
				}
			]
	},
	generarBotones: function(fp){
		//var afiliado = fp.tipoAfiliado;
		return [
				{
					text: 'Confirmar y Enviar',
					id: 'btnEnviarAcept',
					formBind: true
				},
				{
					text: 'Regresar',
					id: 'btnRegresar',
					handler: function(){
					}
				},
				{
					text: 'Generar PDF',
					id: 'btnPDF',
					handler: function(){
					}
				}
			]
	},
	setHandlerBtnEnviar: function(fn){
		var btnEnviarAcept = Ext.getCmp('btnEnviarAcept')
			btnEnviarAcept.setHandler(fn);
	},
	setHandlerBtnRegresar: function(fn){
		var btnRegresar = Ext.getCmp('btnRegresar')
			btnRegresar.setHandler(fn);
	},
	setHandlerBtnArchivo: function(fn){
		var btnPDF = Ext.getCmp('btnPDF')
			btnPDF.setHandler(fn);
	}
});