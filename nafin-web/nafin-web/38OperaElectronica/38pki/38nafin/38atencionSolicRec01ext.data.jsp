<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoIfOperElec,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.electronica.*,
		netropology.utilerias.usuarios.*,
		com.netro.pdf.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf" %>

<%
JSONObject jsonObj = new JSONObject();
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String tipoAfiliado = (request.getParameter("tipoAfiliado")!=null)?request.getParameter("tipoAfiliado"):"";
String infoRegresar	=	"";

UtilUsr utilUsr = new UtilUsr();

OperacionElectronica  OperElec = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);

if (informacion.equals("catologoIfSolics")){
	CatalogoIfOperElec catalogo = new CatalogoIfOperElec();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	//catalogo.setOrden("cd_descripcion");
	infoRegresar = catalogo.getJSONElementos();
	
}else if (informacion.equals("catologoEstatus")){
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_estatus_ope");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("opecat_estatus");
	catalogo.setOrden("cd_descripcion");
	infoRegresar = catalogo.getJSONElementos();
	
}else if (informacion.equals("consultaSolics")){
	JSONArray jsObjArray = new JSONArray();
	String cveIf = request.getParameter("cboIf")==null?"1":request.getParameter("cboIf");
	String cboEstatus = request.getParameter("cboEstatus")==null?"":request.getParameter("cboEstatus");
	String fpNumSolic = request.getParameter("fpNumSolic")==null?"":request.getParameter("fpNumSolic");
	String dFechaRegIni = request.getParameter("dFechaRegIni")==null?"":request.getParameter("dFechaRegIni");
	String dFechaRegFin = request.getParameter("dFechaRegFin")==null?"":request.getParameter("dFechaRegFin");
	String usuarioOP = request.getParameter("usuarioOP")==null?"":request.getParameter("usuarioOP");
	String consIni = request.getParameter("consIni")==null?"":request.getParameter("consIni");
	
	if("S".equals(consIni)){
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		dFechaRegIni = fechaActual;
		dFechaRegFin = fechaActual;
	}
	
	
	List lstRegSolic = OperElec.consultaAtencionSolicRec(cveIf, cboEstatus, fpNumSolic, dFechaRegIni, dFechaRegFin, usuarioOP);
	
	jsObjArray = JSONArray.fromObject(lstRegSolic);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if (informacion.equals("catalogoEjecutivoOP")){
	JSONArray jsObjArray = new JSONArray();
	List lstCatalogoEjecOP = new ArrayList();
	List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("OP OP", "N");
	
	if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno ()+" "+usuarioEpo.getApellidoMaterno();

			hmData.put("clave", loginUsuarioOPOP);
			hmData.put("descripcion", nombre);
			hmData.put("loadMsg", "");
			
			lstCatalogoEjecOP.add(hmData);
		}
	}
	
	
	jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
	//System.out.println("infoRegresar = "+infoRegresar);

}else if (informacion.equals("obtenInfoVistaPrevia")){
	String usuarioIfSolic = (request.getParameter("usuarioIfSolic") == null)?"":request.getParameter("usuarioIfSolic");
	HashMap hmInfo = OperElec.obtieneInfoCorreo(usuarioIfSolic);
	HashMap hmGerSub = OperElec.obtieneInfoGerenteSubdirector();
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("dataUserIfObj", hmInfo);
	jsonObj.put("datosGerSub", hmGerSub);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenerArchivo")){
	String cveSolicitud = request.getParameter("cveSolicitud");
	String tipoArchivo = request.getParameter("tipoArchivo");
	
	String nombreArchivo = OperElec.obtieneArchivoSolic(strDirectorioTemp, cveSolicitud, tipoArchivo);
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("consultaSolicCompleta")){
	JSONArray jsObjArray = new JSONArray();
	String cveSolicitud = request.getParameter("cveSolicitud")==null?"":request.getParameter("cveSolicitud");
	
	List lstInfoSolicComp = OperElec.obtieneInfoSolicCompleta(cveSolicitud);
	
	HashMap hmInfoSolic = (HashMap)lstInfoSolicComp.get(0);
	List lstAmort = (List)lstInfoSolicComp.get(1);
	jsObjArray = JSONArray.fromObject(lstAmort);
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("objInfoSolic", hmInfoSolic);
	jsonObj.put("registros", jsObjArray.toString());
	infoRegresar = jsonObj.toString();


}else if (informacion.equals("aceptaSolicsAcepConfirmacion")){
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String fecAbono = (request.getParameter("fecAbono") == null)?"":request.getParameter("observaciones");
	String observaciones = (request.getParameter("observaciones") == null)?"":request.getParameter("observaciones");
	List lstSolicitudes = new ArrayList();
	
	List arrRegistros = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistros.iterator();
	HashMap hmData = new HashMap();
	while (itReg.hasNext()) {
		hmData = new HashMap();
		JSONObject registro = (JSONObject)itReg.next();
		hmData.put("CVESOLICITUD",registro.getString("CVESOLICITUD"));
		hmData.put("NUMPRESTAMO",registro.getString("NUMPRESTAMO"));
		//hmData.put("FECOP",registro.getString("FECOP"));
		//hmData.put("HORAOP",registro.getString("HORAOP"));
		lstSolicitudes.add(hmData);
	}
	
	OperElec.aceptaSolicsParaConfirmacionConCheck(lstSolicitudes, fecAbono, observaciones, strNombreUsuario,iNoUsuario);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("rechazaSolicsRechConfirmacion")){
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String causasRech = (request.getParameter("causasRech") == null)?"":request.getParameter("causasRech");
	String fecRechazo = (request.getParameter("fecRechazo") == null)?"":request.getParameter("fecRechazo");
	String horaRechazo = (request.getParameter("horaRechazo") == null)?"":request.getParameter("horaRechazo");
	
	List lstSolicitudes = new ArrayList();
	
	List arrRegistros = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistros.iterator();
	HashMap hmData = new HashMap();
	while (itReg.hasNext()) {
		hmData = new HashMap();
		JSONObject registro = (JSONObject)itReg.next();
		hmData.put("CVESOLICITUD",registro.getString("CVESOLICITUD"));
		hmData.put("FOLIO",registro.getString("FOLIO"));
		lstSolicitudes.add(hmData);
	}
	
	System.out.println("causasRechcausasRechcausasRech=="+causasRech);
	OperElec.rechazaSolicsParaConfirmacionConCheck(lstSolicitudes, fecRechazo, horaRechazo, causasRech, strNombreUsuario,iNoUsuario);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("procSolicAceptadas")){
	JSONArray jsObjArray = new JSONArray();
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List lstSolicitudes = new ArrayList();
	List lstSolicsAcept = new ArrayList();
	
	List arrRegistros = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistros.iterator();
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		lstSolicitudes.add(registro.getString("CVESOLICITUD"));
	}
	
	lstSolicsAcept = OperElec.obtieneInfoRegAceptRech(lstSolicitudes);
	
	
	jsObjArray = JSONArray.fromObject(lstSolicsAcept);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";	
	
}else if (informacion.equals("elementosCheckList")){
	
	JSONArray jsObjArray = new JSONArray();
	String CVESOLICITUD = request.getParameter("CVESOLICITUD")==null?"":request.getParameter("CVESOLICITUD");
	List lstElemenCheckList = OperElec.consultaCheckList("OP",CVESOLICITUD,iNoUsuario);
	jsObjArray = JSONArray.fromObject(lstElemenCheckList);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
}else if (informacion.equals("guardarCheckList")){

	infoRegresar ="";
	JSONObject jsonObj1 = new JSONObject();
	String check = "";
	boolean resultado = false;
	String numeroRegistros  =(request.getParameter("numeroRegistros")!=null) ? request.getParameter("numeroRegistros"):"";
	String  ic_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
	int i_numReg = Integer.parseInt(numeroRegistros);
	for(int i = 0; i< i_numReg; i++){  
			String chkReviNueva[] = request.getParameterValues("chkReviNueva");
			String chkReviAnt[] = request.getParameterValues("chkReviAnt");
			String listCorre[] = request.getParameterValues("listCorre");
			String listCorreAnt[] = request.getParameterValues("listCorreAnt");
			String listObs[] = request.getParameterValues("listObs");
			String listObsAnt[] = request.getParameterValues("listObsAnt");
			String idElemCheck[] = request.getParameterValues("idElemCheck");
			
			if((!listCorreAnt[i].equals(listCorre[i]))||(!listObsAnt[i].equals(listObs[i]))){
				if((listCorreAnt[i].equals("N")&&listCorre[i].equals("S"))||(listCorreAnt[i].equals("S")&&listCorre[i].equals("N"))||(!listObsAnt[i].equals(listObs[i]))||(listCorreAnt[i].equals("")&&(listCorre[i].equals("S")||listCorre[i].equals("N")))){
					boolean varGuarda = OperElec.guardaChkList(ic_solicitud,iNoUsuario,idElemCheck[i],"OP",listCorre[i], listObs[i] );
				}else if(listCorre[i].equals("")){
					OperElec.limpiarCheck(ic_solicitud,iNoUsuario,idElemCheck[i],"OP");
				}
			}
	}
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("ic_solicitud", ic_solicitud);
	infoRegresar = jsonObj.toString();
}else if (informacion.equals("validaListCheck")){
	String jsonRegistros[] =  request.getParameterValues("registros");
	String cveEjecOP = (request.getParameter("cveEjecOP") == null)?"":request.getParameter("cveEjecOP");
	String listClave ="";
	String listClaveAux ="";
	String listAcuse = "";
	String listClaveC ="";
	String listClaveAuxC ="";
	String listClaveInC ="";
	String listClaveAuxInC ="";
	String listAcuseC= "";
	String listAcuseInC = "";
	HashMap	hmData = new HashMap();
	if(jsonRegistros!=null && jsonRegistros.length>0){
		for(int x=0; x<jsonRegistros.length; x++){
			hmData = new HashMap();
			String clave =(String)jsonRegistros[x];
			hmData =OperElec.validaListCheck(clave, iNoUsuario, "OP");
			if(!hmData.get("TOTAL_ELE").equals("19")){
				
				listClave += clave +",";
			}else{
				if(!hmData.get("TOTAL_INCORRECTO").equals("0")){
					listClaveInC += clave +",";
				}else{
					listClaveC += clave +",";
					
				}
			}
		}
	}
	
	if(!listClave.equals("")){
		listClaveAux = listClave.substring(0,(listClave.length()-1));
		listAcuse = OperElec.getAcuseCheck(listClaveAux);
	}
	
	if(!listClaveInC.equals("")){
		listClaveAuxInC = listClaveInC.substring(0,(listClaveInC.length()-1));
		listAcuseInC = OperElec.getAcuseCheck(listClaveAuxInC);
	}
	if(!listClaveC.equals("")){
		listClaveAuxC = listClaveC.substring(0,(listClaveC.length()-1));
		listAcuseC = OperElec.getAcuseCheck(listClaveAuxC);
	}

	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("clave", listAcuse);
	jsonObj.put("listAcuseInC", listAcuseInC);
	jsonObj.put("listAcuseC", listAcuseC);
	infoRegresar = jsonObj.toString();
}else if (informacion.equals("bitacoraReviciones")){
	JSONArray jsObjArray = new JSONArray();
	String CVESOLICITUD = request.getParameter("CVESOLICITUD")==null?"":request.getParameter("CVESOLICITUD");
	List lsBitacora = OperElec.consultarBitacoraRevision(CVESOLICITUD);
	jsObjArray = JSONArray.fromObject(lsBitacora);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
	
}else if (informacion.equals("archivoBitacora")){
	String IC_CHKLIST_SOLICITUD = request.getParameter("IC_CHKLIST_SOLICITUD")==null?"":request.getParameter("IC_CHKLIST_SOLICITUD");
	try {
		
		String nombreArchivo =	OperElec.obtenChkBitacora (strDirectorioTemp,IC_CHKLIST_SOLICITUD );	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}	
	infoRegresar = jsonObj.toString(); 
	
}

%>

<%=infoRegresar%>