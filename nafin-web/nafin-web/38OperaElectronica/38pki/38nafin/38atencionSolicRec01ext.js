
function checkRevisado(check,rowIndex,colIds){
	var gridConsulta = Ext.getCmp('gridCheckList');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	if(reg.get('CS_CORRECTO_REV') == 'checked'){
		check.checked = true
	}else{
		if(check.checked == true)  {
			reg.set('CS_CORRECTO_REV','S');
		}else{
			reg.set('CS_CORRECTO_REV','N');
		}	
	}
}
Ext.onReady(function() {
var vistaPrevia = null;
var registros_id = [];
	
//-----------------HANDLERS-----------------------------------------------------
	function procesarValidaCheck(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			var registrosEnviar = [];
			var existeSelec = false;
			var mismoIF = true;
			var totalReg = 0;
			var totalImporte = 0.0;
			var nombreIF = '';
			var primerIF = '';
			storeSolicsData.each(function(record){
				if(record.data['SELECCION']=='S'){
					//existeSelec = true;
					totalReg++;
					totalImporte += parseFloat(record.data['IMPORTESOLIC']);
					registrosEnviar.push(record.data);
					
					if(!existeSelec){
						primerIF = record.data['CVEIF'];
						nombreIF = record.data['NOMBREINTER'];
					}else{
						if(primerIF != record.data['CVEIF']){
							mismoIF = false;
						}
					}
					existeSelec = true;
				}
			});
			if(jsonData.clave!=''){
					Ext.Msg.alert('Aviso', 'Favor de revisar o completar el check lis de la(s) solicitud(es): [ '+jsonData.clave+' ]');
			}else{
				if(jsonData.listAcuseInC==''){
					Ext.Ajax.request({
						url: '38atencionSolicRec01ext.data.jsp',
						params: {
							informacion: 'procSolicAceptadas',
							registros: Ext.encode(registrosEnviar)
						},
						callback: function(opts, success, response) {
							if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
								var resp = 	Ext.util.JSON.decode(response.responseText);
								var d = new Date();
								var m = d.getMonth() + 1;
								var m = (m < 10) ? '0' + m : m;
								var fecActual = d.getDate()+'/'+m+'/'+d.getFullYear();
								var horaActual = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
								Ext.getCmp('lNombreIf').setText(nombreIF);
								Ext.getCmp('lFecOperado').setText(fecActual);
								Ext.getCmp('lHoraOperado').setText(horaActual);
								Ext.getCmp('lUsuarioOp').setText(Ext.getDom('strNombreUsuario').value);
								storeSolicAceptaData.loadData(resp);
								gridSolicsRec.hide();
								fpCritBusq.hide();
										//fpEjcutivoOper.hide();
										
								pnlAceptadas.show();
										
												//var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
												//contenedorPrincipalCmp.add(pnlAceptadas);
												//contenedorPrincipalCmp.doLayout();
						
							}else{
								NE.util.mostrarConnError(response,opts);
							}
						}
					});// termona ajax
				}else{
					Ext.Msg.alert('Aviso', 'La(s) solicitud(es): [ '+jsonData.listAcuseInC+' ] tiene(n) observaciones no se puede(n) Autorizar,  favor de revisar el check list.');
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarValidaCheckRechazo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			var registrosEnviar = [];
			var existeSelec = false;
			var mismoIF = true;
			var totalReg = 0;
			var totalImporte = 0.0;
			//var cboIf = Ext.getCmp('cboIf_');
			//var record = cboIf.findRecord(cboIf.valueField, cboIf.getValue());		
			//var nombreIF = record.get(cboIf.displayField);
			var nombreIF = '';
			var primerIF = '';
				
			storeSolicsData.each(function(record){
				if(record.data['SELECCION']=='S'){
					//existeSelec = true;
					totalReg++;
					totalImporte += parseFloat(record.data['IMPORTESOLIC']);
					registrosEnviar.push(record.data);
					
					if(!existeSelec){
						primerIF = record.data['CVEIF'];
						nombreIF = record.data['NOMBREINTER'];
					}else{
						if(primerIF != record.data['CVEIF']){
							mismoIF = false;
						}
					}
					existeSelec = true;
					
				}
				
					
					
			});
			if(jsonData.clave!=''){
				Ext.Msg.alert('Aviso', 'Favor de revisar o completar el check lis de la(s) solicitud(es): [ '+jsonData.clave+' ]');
			}else{
				if(jsonData.listAcuseC==''){
					Ext.Ajax.request({
						url: '38atencionSolicRec01ext.data.jsp',
						params: {
							informacion: 'procSolicAceptadas',
							registros: Ext.encode(registrosEnviar)
						},
						callback: function(opts, success, response) {
							if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
								var resp = 	Ext.util.JSON.decode(response.responseText);
								var d = new Date();
								var m = d.getMonth() + 1;
								var m = (m < 10) ? '0' + m : m;
								var fecActual = d.getDate()+'/'+m+'/'+d.getFullYear();
								var horaActual = d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
								Ext.getCmp('lrNombreIf').setText(nombreIF);
								Ext.getCmp('lrFecRechazo').setText(fecActual);
								Ext.getCmp('lrHoraRechazo').setText(horaActual);
								Ext.getCmp('lrUsuarioOp').setText(Ext.getDom('strNombreUsuario').value);
								storeSolicRechazadaData.loadData(resp);
								gridSolicsRec.hide();
								fpCritBusq.hide();
								pnlRechazadas.show();
							}else{
								NE.util.mostrarConnError(response,opts);
							}
						}
					});
				}else{
					Ext.Msg.alert('Aviso', 'La(s) solicitud(es): [ '+jsonData.listAcuseC+' ] no tiene(n) observaciones, no es necesario realizar un rechazo.');
				}
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsSolicData = function(store, arrRegistros, opts) {
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!gridSolicsRec.isVisible()) {
				contenedorPrincipalCmp.add(gridSolicsRec);
				contenedorPrincipalCmp.add(NE.util.getEspaciador(10));
				//contenedorPrincipalCmp.add(fpEjcutivoOper);
				contenedorPrincipalCmp.doLayout();
			}
			
			var el = gridSolicsRec.getGridEl();
			var cm = gridSolicsRec.getColumnModel();
			var strPerfil  = Ext.getDom('strPerfil').value;   
			if(store.getTotalCount() > 0) {
				el.unmask();
				if(strPerfil=='OP OP'){   
					gridSolicsRec.getColumnModel().setHidden(cm.findColumnIndex('PDFBOLETARUG'), true);		
				}
				Ext.getCmp('btnAceptaConfirmacion').enable();
				Ext.getCmp('btnRechazaConfirmacion').enable();
			}else {
				Ext.getCmp('btnAceptaConfirmacion').disable();
				Ext.getCmp('btnRechazaConfirmacion').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	};
	
	var processSuccessFailureObtieneArchivo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var archivo = resp.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			
			fpArchivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpArchivo.getForm().getEl().dom.submit();
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};



var procesarSuccessAceptadaConfirmacion =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			Ext.Msg.alert('Aviso', 'Las solicitudes han sido aceptadas', function(){
				window.location.href='38atencionSolicRec01ext.jsp';
			});
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

var procesarSuccessRechazadaConfirmacion =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			Ext.Msg.alert('Aviso', 'Las solicitudes han sido rechazadas', function(){
				window.location.href='38atencionSolicRec01ext.jsp';
			});
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	

var procesaInfoSolicCompleta =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('FECSOLICITUD').setText(resp.objInfoSolic.FECSOLICITUD);
			Ext.getCmp('NOMBREBANCO').setText(resp.objInfoSolic.NOMBREBANCO);
			Ext.getCmp('NUMCUENTA').setText(resp.objInfoSolic.NUMCUENTA);
			Ext.getCmp('BANCO').setText(resp.objInfoSolic.BANCO);
			Ext.getCmp('CUENTACLABE').setText(resp.objInfoSolic.CUENTACLABE);
			
			Ext.getCmp('NOMBRECONTRATO').setText(resp.objInfoSolic.NOMBRECONTRATO);
			Ext.getCmp('FECFIRMACONTRATO').setText(resp.objInfoSolic.FECFIRMACONTRATO);
			Ext.getCmp('NOMBRECONTRATOOE').setText(resp.objInfoSolic.NOMBRECONTRATOOE);
			Ext.getCmp('FECFIRMACONTRATOOE').setText(resp.objInfoSolic.FECFIRMACONTRATOOE);
			
			Ext.getCmp('IMPORTESOLIC').setText(Ext.util.Format.number(resp.objInfoSolic.IMPORTESOLIC,'$0,0.00'));
			Ext.getCmp('NOMBREMONEDA').setText(resp.objInfoSolic.NOMBREMONEDA);
			
			Ext.getCmp('DESTINORECURSO').setText(resp.objInfoSolic.DESTINORECURSO);
			Ext.getCmp('FECPAGOCAP').setText(resp.objInfoSolic.FECPAGOCAP);
			Ext.getCmp('FECPAGOINT').setText(resp.objInfoSolic.FECPAGOINT);
			Ext.getCmp('FECVENCIMIENTO').setText(resp.objInfoSolic.FECVENCIMIENTO);
			Ext.getCmp('TASAINTERES').setText(resp.objInfoSolic.TASAINTERES);
			Ext.getCmp('OBSERVOP').setText(resp.objInfoSolic.OBSERVOP);
			Ext.getCmp('USUARIOOP').setText(resp.objInfoSolic.NOMBREUSEROP);
			
			var btnCotizaSC = Ext.getCmp('btnCotizaSolicCompleta');
			btnCotizaSC.setHandler(function(){
				Ext.Ajax.request({
						url : '38atencionSolicRec01ext.data.jsp',
						params: Ext.apply({
							informacion: "obtenerArchivo",
							cveSolicitud: resp.objInfoSolic.CVESOLICITUD,
							tipoArchivo: 'COTIZA'
						}),
						callback: processSuccessFailureObtieneArchivo});
			});
			
			gridSolicsRec.hide();
			//fpEjcutivoOper.hide();
			fpCritBusq.hide();
			fpSolicCompleta.show();
			
			/*var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			contenedorPrincipalCmp.add(fpSolicCompleta);
			contenedorPrincipalCmp.doLayout();*/
			
			storeAmortData.loadData(resp);
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	function validaCheck(opts, success, response) {
	
			Ext.Ajax.request({
				url: '38atencionSolicRec01ext.data.jsp',
				params: {
					informacion: 'validaListCheck',
					registros: registros_id								
				},
				callback: procesarValidaCheck
			});			
							
	}
	function validaCheckRechazo(opts, success, response) {
	
			Ext.Ajax.request({
				url: '38atencionSolicRec01ext.data.jsp',
				params: {
					informacion: 'validaListCheck',
					registros: registros_id								
				},
				callback: procesarValidaCheckRechazo
			});			
							
	}

//----------------------------------FUNCTONS------------------------------------
	var hdrAceptadaConfrimacion = function(opts, success, response){
		var registrosEnviar = [];
		var existeSelec = false;
		var mismoIF = true;
		var totalReg = 0;
		var totalImporte = 0.0;
		//var cboIf = Ext.getCmp('cboIf_');
		//var record = cboIf.findRecord(cboIf.valueField, cboIf.getValue());		
		//var nombreIF = record.get(cboIf.displayField);
		var nombreIF = '';
		var primerIF = '';
		registros_id = [];
		storeSolicsData.each(function(record){
			if(record.data['SELECCION']=='S'){
				//existeSelec = true;
				totalReg++;
				totalImporte += parseFloat(record.data['IMPORTESOLIC']);
				registrosEnviar.push(record.data);
				
				if(!existeSelec){
					primerIF = record.data['CVEIF'];
					nombreIF = record.data['NOMBREINTER'];
				}else{
					if(primerIF != record.data['CVEIF']){
						mismoIF = false;
					}
				}
				existeSelec = true;
				registros_id.push(record.data['CVESOLICITUD']);
			}
			
			
			
			
		});
		
		if(existeSelec ){
			//Ext.getDom('iNoUsuario').value;
			if(!mismoIF){
				Ext.Msg.alert('Aviso','Favor de seleccionar solicitudes con un mismo Intermediario Financiero');
			}else{
				validaCheck(opts, success, response);
			}

		}else{
			Ext.Msg.alert('Aviso','Seleccione al menos una solicitud');
		}
	}
	
	var hdrRechazadaConfrimacion = function(opts, success, response){
		var registrosEnviar = [];
		var existeSelec = false;
		var mismoIF = true;
		var totalReg = 0;
		var totalImporte = 0.0;
		//var cboIf = Ext.getCmp('cboIf_');
		//var record = cboIf.findRecord(cboIf.valueField, cboIf.getValue());		
		//var nombreIF = record.get(cboIf.displayField);
		var nombreIF = '';
		var primerIF = '';
		registros_id = [];
		storeSolicsData.each(function(record){
			if(record.data['SELECCION']=='S'){
				//existeSelec = true;
				totalReg++;
				totalImporte += parseFloat(record.data['IMPORTESOLIC']);
				registrosEnviar.push(record.data);
				
				if(!existeSelec){
					primerIF = record.data['CVEIF'];
					nombreIF = record.data['NOMBREINTER'];
				}else{
					if(primerIF != record.data['CVEIF']){
						mismoIF = false;
					}
				}
				existeSelec = true;
				registros_id.push(record.data['CVESOLICITUD']);
				
			}
			
			
			
		});
		
		if(existeSelec ){
			//Ext.getDom('iNoUsuario').value;
			if(!mismoIF){
				Ext.Msg.alert('Aviso','Favor de seleccionar solicitudes con un mismo Intermediario Financiero');
			}else{
				validaCheckRechazo(opts, success, response);
			}
		}else{
			Ext.Msg.alert('Aviso','Seleccione al menos una solicitud');
		}
	}

//-----------------STORES CATALOGOS---------------------------------------------

	var storeCatIfData = new Ext.data.JsonStore
	({
		id: 'storeCatIfData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '38atencionSolicRec01ext.data.jsp',
		baseParams: 
		{
			informacion: 'catologoIfSolics'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners:
		{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var storeSolicsData = new Ext.data.JsonStore({
		id:'storeSolicsData1',
		root : 'registros',
		url : '38atencionSolicRec01ext.data.jsp',
		baseParams: {
			informacion: 'consultaSolics'
		},
		fields: [
			{name: 'CVESOLICITUD'},
			{name: 'CCACUSE'},
			{name: 'CVEIF'},
			{name: 'CVECONTRATO'},
			{name: 'NOMBRECONTRATO'},
			{name: 'FECSOLICITUD'},
			{name: 'FECFIRMACONTRATO'},
			{name: 'COUNTMODIF'},
			{name: 'ALLFECMODCONT'},
			{name: 'NOMBRECONTRATOOE'},
			{name: 'FECFIRMACONTRATOOE'},
			{name: 'IMPORTESOLIC'},
			{name: 'CVEMONEDA'},
			{name: 'NOMBREMONEDA'},
			{name: 'DESTINORECURSO'},
			{name: 'FECPAGOCAPITAL'},
			{name: 'FECPAGOINTERES'},
			{name: 'FECVENCIMIENTO'},
			{name: 'ESTATUSSOLIC'},
			{name: 'NOMBREESTATUS'},
			{name: 'OBSERVIF'},
			{name: 'OBSERVOP'},
			{name: 'TASAINTERES'},
			{name: 'TIPOTASA'},
			{name: 'CVETASA'},
			{name: 'NOMBRETASA'},
			{name: 'NUMPRESTAMO'},
			{name: 'FECOP'},
			//{name: 'HORAOP'},
			{name: 'NUMCUENTA'},
			{name: 'NOMBREBANCO'},
			{name: 'FOLIO'},
			{name: 'USUARIOIF'},
			{name: 'NOMBREUSERIF'},
			{name: 'CAUSASRECH'},
			{name: 'EXISTEFILECOTIZA'},
			{name: 'NOMBREINTER'},
			{name: 'FECASIGNAEJEOP'},
			{name: 'NOMBREUSRCON1'},
			{name: 'SELECCION'},
			{name: 'PDFBOLETARUG'},
			{name: 'EXISTEFILECARTERAPDF'},
			{name: 'EXISTE_REVISION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsSolicData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsSolicData(null, null, null);
				}
			}
		}
	});

	var storeSolicAceptaData = new Ext.data.JsonStore({
		id:'storeSolicAceptaData1',
		root : 'registros',
		url : '38atencionSolicRec01ext.data.jsp',
		baseParams: {
			informacion: 'consultaSolics'
		},
		fields: [
			{name: 'CVESOLICITUD'},
			{name: 'CCACUSE'},
			{name: 'IMPORTESOLIC'},
			{name: 'CVEMONEDA'},
			{name: 'NOMBREMONEDA'},
			{name: 'OBSERVIF'},
			{name: 'OBSERVOP'},
			{name: 'TASAINTERES'},
			{name: 'NUMPRESTAMO'},
			//{name: 'FECOP', type: 'date', dateFormat: 'd/m/Y'},
			//{name: 'HORAOP'},
			{name: 'NUMCUENTA'},
			{name: 'DESCBANCO'},
			{name: 'USUARIOIF'},
			{name: 'NOMBREUSERIF'},
			{name: 'SELECCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false
		/*listeners: {
			load: procesarConsSolicData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsSolicData(null, null, null);
				}
			}
		}*/
	});
	
	var storeSolicRechazadaData = new Ext.data.JsonStore({
		id:'storeSolicRechazadaData1',
		root : 'registros',
		url : '38atencionSolicRec01ext.data.jsp',
		baseParams: {
			informacion: 'consultaSolics'
		},
		fields: [
			{name: 'CVESOLICITUD'},
			{name: 'CCACUSE'},
			{name: 'IMPORTESOLIC'},
			{name: 'CVEMONEDA'},
			{name: 'NOMBREMONEDA'},
			{name: 'FOLIO'},
			{name: 'USUARIOIF'},
			{name: 'NOMBREUSERIF'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false
		/*listeners: {
			load: procesarConsSolicData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsSolicData(null, null, null);
				}
			}
		}*/
	});
	
var storeAmortData = new Ext.data.JsonStore({
		root : 'registros',
		fields: [
			{name: 'CVESOLICITUD'},
			{name: 'CVETABLAAMORT'},
			{name: 'NUMPERIODO'},
			{name: 'FECALTA',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'MONTOAMORT'}

		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false
		/*listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}*/
		
	});
	function procesarGuardaChickList(opts, success, response) {
		var btnGuardar = Ext.getCmp('btnGrabar');
		Ext.getCmp('btnGrabar').enable();
		btnGuardar.setIconClass('icoGuardar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			consultaCheck.load({
				params: Ext.apply(
				{
						CVESOLICITUD: resp.ic_solicitud,
						informacion: 'elementosCheckList'
				})
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	var validaCheckList = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		var CVESOLICITUD = registro.get('CVESOLICITUD'); 
		var acuse = registro.get('CCACUSE');
		Ext.getCmp('gridCheckList').setTitle('Check list de revisi�n : Solicitud '+acuse);
		Ext.getCmp('ic_solicitud_aux').setValue(CVESOLICITUD);
		consultaCheck.load({
					params: Ext.apply(
					{
						CVESOLICITUD: CVESOLICITUD,
						informacion: 'elementosCheckList'
					})
				});
		var ProcesarCheckList = Ext.getCmp('ProcesarCheckList');
		if(ProcesarCheckList){
			ProcesarCheckList.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 800,
				height: 550,												
				resizable: false,
				closable:false,
				id: 'ProcesarCheckList',
				closeAction: 'hide',
				items: [					
					gridCheckList					
				]
			}).show();
			
		}
		
	}
	var bitacoraReviciones = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		var CVESOLICITUD = registro.get('CVESOLICITUD');
		var acuse = registro.get('CCACUSE');
		Ext.getCmp('gridBitacoraRevi').setTitle('Bit�cora de revisiones : Solicitud '+acuse);
		Ext.getCmp('ic_solicitud_aux').setValue(CVESOLICITUD);
		consultaBitacoraRevi.load({
					params: Ext.apply(
					{
						CVESOLICITUD: CVESOLICITUD,
						informacion: 'bitacoraReviciones'
					})
				});
		var ProcesarBitacoraRevi = Ext.getCmp('ProcesarBitacoraRevisiones');
		if(ProcesarBitacoraRevi){
			ProcesarBitacoraRevi.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 700,
				height: 400,												
				resizable: false,
				closable:false,
				id: 'ProcesarBitacoraRevisiones',
				closeAction: 'hide',
				items: [					
					gridBitacoraRevi				
				]
			}).show();
		}
	}
	var consultaCheck  = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '38atencionSolicRec01ext.data.jsp',
		baseParams: {
			informacion: 'elementosCheckList'
		},		
		fields: [
			{	name: 'IC_SOLICITUD'},
			{	name: 'CC_USUARIO'},
			{	name: 'IC_CHKLIST_ELEMENTO'},
			{	name: 'CC_TIPO_USUARIO'},
			{	name: 'CS_CORRECTO'},
			{	name: 'CS_CORRECTO_ANT'},
			{	name: 'CS_CORRECTO_REV'},
			{	name: 'CS_CORRECTO_REV_AUX'},
			{	name: 'CG_OBSERVACIONES'},
			{	name: 'CG_OBSERVACIONES_ANT'},
			{	name: 'CG_DESCRIPCION'}
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}		
	});
	var consultaBitacoraRevi  = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '38atencionSolicRec01ext.data.jsp',
		baseParams: {
			informacion: 'bitacoraReviciones'
		},		
		fields: [
			{	name: 'IC_CHKLIST_SOLICITUD'},
			{	name: 'IC_SOLICITUD'},
			{	name: 'IC_ESTATUS_OPE'},
			{	name: 'CD_DESCRIPCION'},
			{	name: 'CC_USUARIO_NOM'},
			{	name: 'DF_ALTA'},
			{	name: 'CG_RECHAZO_INT'}
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}		
	});
	var procesarGuardar = function(store, arrRegistros, opts){
		var	gridCheck = Ext.getCmp('gridCheckList');
		var 	store = gridCheck.getStore();
		var 	columnModelGrid = gridCheck.getColumnModel();
		var 	jsonData = store.data.items;
		var 	numRegistros=0;
		var	chkReviNueva =[];
		var	chkReviAnt =[];
		var   listCorre = [];
		var 	listObs = [];
		var   listCorreAnt = [];
		var 	listObsAnt = [];
		var 	idElemCheck = [];
		Ext.each(jsonData, function(item,inx,arrItem){
			chkReviNueva.push(item.data.CS_CORRECTO_REV);	
			chkReviAnt.push(item.data.CS_CORRECTO_REV_AUX);	
			listCorre.push(item.data.CS_CORRECTO);
			listObs.push(item.data.CG_OBSERVACIONES);
			idElemCheck.push(item.data.IC_CHKLIST_ELEMENTO);
			listCorreAnt.push(item.data.CS_CORRECTO_ANT);
			listObsAnt.push(item.data.CG_OBSERVACIONES_ANT);
			numRegistros++;
		});
		var valor = true ;
		store.each(function(record) {
			numReg = store.indexOf(record);
				if(record.data['CS_CORRECTO_REV']=='S'&& record.data['CS_CORRECTO']==null){
					Ext.MessageBox.alert('Error de validaci�n','La validaci�n es obligatoria.',
					function(){
						gridCheck.startEditing(numReg, columnModelGrid.findColumnIndex('CS_CORRECTO'));
					});
					valor= false ;
					return false;
					
				}else {
					if(record.data['CS_CORRECTO']=='N'&& record.data['CG_OBSERVACIONES']==null){
						Ext.MessageBox.alert('Error de validaci�n','Las observaciones son obligatorias.',
						function(){
							gridCheck.startEditing(numReg, columnModelGrid.findColumnIndex('CG_OBSERVACIONES'));
						});
						valor= false ;
						return false;
					}
				}
		});
		
		if(valor != false){
		var btnGuardar = Ext.getCmp('btnGrabar');
		btnGuardar.disable();
		btnGuardar.setIconClass('loading-indicator');
		Ext.Ajax.request({
				url: '38atencionSolicRec01ext.data.jsp',
				params: {
					informacion: 'guardarCheckList',
					chkReviNueva:chkReviNueva,
					chkReviAnt:chkReviAnt,
					listCorre:listCorre,
					listObs:listObs,
					listCorreAnt:listCorreAnt,
					listObsAnt:listObsAnt,
					idElemCheck : idElemCheck,
					numeroRegistros:numRegistros,
					ic_solicitud:Ext.getCmp('ic_solicitud_aux').getValue()
				},
				callback: procesarGuardaChickList
			});
		}
	}
	
//------------------------------------------------------------------------------	
var gridAmort = new Ext.grid.GridPanel({
		id: 'gridAmort',
		width: 450,
		height: 150,
		title: 'Tabla Amortizaci�n',
		frame: true,
		store: storeAmortData,
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{//1
				header: 'Periodo',
				tooltip: 'Periodo',
				dataIndex: 'NUMPERIODO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{//2
				header: 'Fecha',
				tooltip: 'Fecha',
				dataIndex: 'FECALTA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{//3
				header: 'Amortizaci�n',
				tooltip: 'Amortizaci�n',
				dataIndex: 'MONTOAMORT',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		]
	});
	var tipoCorrecto = new Ext.data.ArrayStore({
		fields: ['clave', 'descripcion'],
		data : [
			['S','SI'],
			['N','NO']
		 ]
	});
		var comboCorrecto = new Ext.form.ComboBox({  
			id: 'cmbCorrecto',
			mode: 'local',
			displayField: 'descripcion',
			store:tipoCorrecto,
			forceSelection : true,
			triggerAction : 'all',
			allowBlank: true,
			valueField: 'clave',
			typeAhead: true,
			minChars : 1
		});
		Ext.util.Format.comboRenderer = function(comboCorrecto){
			return function(value,metadata,registro,rowIndex,colIndex,store){				
				if(value!=''){
					var record = comboCorrecto.findRecord(comboCorrecto.valueField, value);
					return record ? record.get(comboCorrecto.displayField) : comboCorrecto.valueNotFoundText;
				}				
			}
		}
	var gridCheckList = new Ext.grid.EditorGridPanel({	
		store: consultaCheck,
		id: 'gridCheckList',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Check list de revisi�n',
		clicksToEdit: 1,
		hidden: false,		
		columns: [	
			{
				header: '<center>Punto a revisar</center>',
				tooltip: 'Punto a revisar',
				dataIndex: 'CG_DESCRIPCION',
				sortable: true,
				width: 350,			
				resizable: true,	
				hidden:false,
				align: 'left'
			},
			{
				header:'Revisado',
				dataIndex : 'CS_CORRECTO_REV',
				width : 100,
				align: 'center',
				sortable: false,
            hideable: false,
				renderer: function(value, metadata, record, rowIndex, colIndex, store){				;
					
					if(record.data['CS_CORRECTO_REV'] == ''){
						if(record.data['CS_CORRECTO_REV']=='checked' ){
							return '<input  id="chkRevisado" type="checkbox" checked  onclick="checkRevisado(this, '+rowIndex +','+colIndex+');" />';
						}else{
							return '<input  id="chkRevisado" type="checkbox"  onclick="checkRevisado(this, '+rowIndex +','+colIndex+');" />';
						}
				
				}else{
					if(record.data['CS_CORRECTO_REV'] == 'S'){
						return '<input  id="chkRevisado"  type="checkbox" checked  onclick="checkRevisado(this, '+rowIndex +','+colIndex+');" />';
					}else{
						return '<input  id="chkRevisado" type="checkbox"  onclick="checkRevisado(this, '+rowIndex +','+colIndex+');" />';
					}
				}
					
				}
			},
			{
				header: '�Es correcto?',
				tooltip: '�Es correcto?',
				dataIndex: 'CS_CORRECTO',
				sortable: true,
				resizable: true,
				width: 100,
				hidden: false,		
				minChars : 1,
				align: 'center',
				editor:comboCorrecto,
				renderer: Ext.util.Format.comboRenderer(comboCorrecto)					
			},
			{
					header: '<center>Observaciones</center>',
					tooltip: 'Observaciones',
					dataIndex: 'CG_OBSERVACIONES',
					sortable: true,
					width: 200,
					align: 'left',
					editor: {
						xtype: 'textfield',
						allowBlank:false
					},
					renderer:function(value,metadata,registro){
						return NE.util.colorCampoEdit(value,metadata,registro);
					}				
			}
		],	
		bbar: {
			xtype: 'toolbar',	buttonAlign:'center',	
			buttons: ['-',
				{	
					xtype:	'button',
					id:		'btnGrabar',
					text:		'Guardar',
					iconCls: 'icoGuardar ',
					handler: procesarGuardar
				},
				{
							xtype: 'button',
							text: 'Cerrar',					
							tooltip:	'Cerrar',
							iconCls: 'icoCerrar',
							id: 'btnCerrarCheck',
							handler: function(){Ext.getCmp('ProcesarCheckList').hide();
							}
				}
			]
		},
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 750,
		width: 700,
		align: 'center',
		frame: false
	});
	var gridBitacoraRevi = new Ext.grid.EditorGridPanel({	
		store: consultaBitacoraRevi,
		id: 'gridBitacoraRevi',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		//title: 'Bit�cora de revisiones',
		title: 'Bit�cora de revisiones : Solicitud',
		clicksToEdit: 1,
		hidden: false,		
		columns: [	
			{
				header: '<center>Estatus</center>',
				tooltip: 'Estatus',
				dataIndex: 'CD_DESCRIPCION',
				sortable: true,
				width: 200,			
				resizable: true,	
				hidden:false,
				align: 'left'
			},
			{
				header: '<center>Usuario</center>',
				tooltip: 'Usuario',
				dataIndex: 'CC_USUARIO_NOM',
				sortable: true,
				width: 250,			
				resizable: true,	
				hidden:false,
				align: 'left'
			},
			{//2
				header: 'Fecha y hora',
				tooltip: 'Fecha y hora',
				dataIndex: 'DF_ALTA',
				sortable: true,
				hideable: false,
				width: 150,
				align: 'CENTER'
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				xtype:	'actioncolumn',
				header: 'Check list',
				tooltip: 'Check list',
				align: 'center',	
				sortable: true,	
				resizable: true,	
				width: 70,
				hideable: false, 
				hidden: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							
									this.items[0].tooltip = 'Ver';
									return 'icoXls';
								
							
						},
						handler:function(grid, rowIndex, colIndex, item, event){
							var registro = grid.getStore().getAt(rowIndex);
								Ext.Ajax.request({
								url : '38atencionSolicRec01ext.data.jsp',
								params: Ext.apply({
									informacion: "archivoBitacora",
									IC_CHKLIST_SOLICITUD: registro.data['IC_CHKLIST_SOLICITUD']
								}),
								callback: processSuccessFailureObtieneArchivo});
							
					
						}
					}
			]
		}
		],	
		bbar: {
			xtype: 'toolbar',	buttonAlign:'center',
			buttons: [
				
				{
							xtype: 'button',
							text: '<center>Cerrar</center>',					
							tooltip:	'Cerrar',
							iconCls: 'icoCerrar',
							id: 'btnCerrarBita',
							iconCls: 'icoLimpiar',
							handler: function(){Ext.getCmp('ProcesarBitacoraRevisiones').hide();
							}
				}
			]
		},
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 700,
		align: 'center',
		frame: false
	});
	
	elementosForma = [
		{
			//MONEDA
			xtype: 'combo',
			fieldLabel: 'Intermediario',
			emptyText: 'Seleccionar',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			//allowBlank: false,
			store: storeCatIfData,
			tpl: NE.util.templateMensajeCargaCombo,
			name:'cboIf',
			id: 'cboIf_',
			mode: 'local',
			hiddenName: 'cboIf',
			forceSelection: true,
			anchor: '90%'
		},
		{
			xtype:			'numberfield',
			name:				'fpNumSolic',
			id:				'fpNumSolic_',
			fieldLabel: 	'No. Solicitud',
			allowBlank:		true,
			allowDecimals:	false,
			maxLength: 8,
			width:			300,
			msgTarget:		'side',
			margins:			'0 20 0 0',  //necesario para mostrar el icono de error
			anchor: '90%'
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Solicitud',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '90%',
			items: [
				{
					// Fecha Inicio
					xtype: 'datefield',
					name: 'dFechaRegIni',
					id: 'dFechaRegIni',
					vtype: 'rangofecha',
					campoFinFecha: 'dFechaRegFin',
					allowBlank: true,
					msgTarget: 'side',
					width: 100,
					startDay: 0,
					width: 100,
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'hasta',
					width: 50
				},
				{
					// Fecha Final
					xtype: 'datefield',
					name: 'dFechaRegFin',
					id: 'dFechaRegFin',
					vtype: 'rangofecha',
					campoInicioFecha: 'dFechaRegIni',
					allowBlank: true,
					msgTarget: 'side',
					width: 100,
					startDay: 0,
					width: 100,
					margins: '0 20 0 0'
				}
			]
		},
		{ 	xtype: 'textfield',  
			hidden: true,
			id: 'ic_solicitud_aux', 	
			value: '' 
		}
	]
	
	var elementosSolicCopmleta = [
		{
			xtype: 'panel',
			id: 'infoSolicComp',
			layout: {
				type: 'table',
				columns: 2
			},
			width		: 765,		
			bodyStyle: 	'padding: 2px; padding-bottom:5px;',
			items:[
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'box', width: 205, id: 'FECSOLICITUD_L', name: 'FECSOLICITUD_L', html: '<p align="right">Fecha Solicitud: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{xtype:'label', id: 'FECSOLICITUD', name: 'FECSOLICITUD'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'NOMBREBANCO_L', name: 'NOMBREBANCO_L', html: '<p align="right">Nombre IF: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'NOMBREBANCO', name: 'NOMBREBANCO'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'NUMCUENTA_L', name: 'NUMCUENTA_L', html: '<p align="right">N�mero de Cuenta: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'NUMCUENTA', name: 'NUMCUENTA'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'BANCO_L', name: 'BANCO_L', html: '<p align="right">Banco: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'BANCO', name: 'BANCO'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'CUENTACLABE_L', name: 'CUENTACLABE_L', html: '<p align="right">Cuenta CLABE: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'CUENTACLABE', name: 'CUENTACLABE'}]
				}
			]
		},
		{
			xtype: 'panel',
			id: 'infoContratos',
			title: 'Contrato(s)',
			layout: {
				type: 'table',
				columns: 2
			},
			width		: 765,		
			bodyStyle: 	'padding: 2px; padding-bottom:5px;',
			items:[
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'box', width: 205, id: 'NOMBRECONTRATO_L', name: 'NOMBRECONTRATO_L', html: '<p align="right">Nombre Contrato: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{xtype:'label', id: 'NOMBRECONTRATO', name: 'NOMBRECONTRATO'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'FECFIRMACONTRATO_L', name: 'FECFIRMACONTRATO_L', html: '<p align="right">Fecha Firma Contrato: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'FECFIRMACONTRATO', name: 'FECFIRMACONTRATO'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'NOMBRECONTRATOOE_L', name: 'NOMBRECONTRATOOE_L', html: '<p align="right">Nombre Contrato OE: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'NOMBRECONTRATOOE', name: 'NOMBRECONTRATOOE'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'FECFIRMACONTRATOOE_L', name: 'FECFIRMACONTRATOOE_L', html: '<p align="right">Fecha Firma Contrato OE: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'FECFIRMACONTRATOOE', name: 'FECFIRMACONTRATOOE'}]
				}
			]
		},
		{
			xtype: 'panel',
			id: 'infoSolicitud',
			title: 'Solicitud',
			layout: {
				type: 'table',
				columns: 2
			},
			width		: 765,		
			bodyStyle: 	'padding: 2px; padding-bottom:5px;',
			items:[
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'box', width: 205, id: 'IMPORTESOLIC_L', name: 'IMPORTESOLIC_L', html: '<p align="right">Importe: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{xtype:'label', id: 'IMPORTESOLIC', name: 'IMPORTESOLIC'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'NOMBREMONEDA_L', name: 'NOMBREMONEDA_L', html: '<p align="right">Moneda: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'NOMBREMONEDA', name: 'NOMBREMONEDA'}]
				}
			]
		},
		{
			xtype: 'panel',
			id: 'infoPrestamo',
			title: 'Datos del pr�stamo',
			layout: {
				type: 'table',
				columns: 2
			},
			width		: 765,		
			bodyStyle: 	'padding: 2px; padding-bottom:5px;',
			items:[
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'box', width: 205, id: 'DESTINORECURSO_L', name: 'DESTINORECURSO_L', html: '<p align="right">Destino de los recursos: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{xtype:'label', id: 'DESTINORECURSO', name: 'DESTINORECURSO'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'FECPAGOCAP_L', name: 'FECPAGOCAP_L', html: '<p align="right">Fecha de primer pago de Capital: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'FECPAGOCAP', name: 'FECPAGOCAP'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'FECPAGOINT_L', name: 'FECPAGOINT_L', html: '<p align="right">Fecha de primer pago de Interes: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'FECPAGOINT', name: 'FECPAGOINT'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'FECVENCIMIENTO_L', name: 'FECVENCIMIENTO_L', html: '<p align="right">Fecha de Vencimiento: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'FECVENCIMIENTO', name: 'FECVENCIMIENTO'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'TASAINTERES_L', name: 'TASAINTERES_L', html: '<p align="right">Tasa de Inter�s: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',layout: {type: 'table', columns: 2},
					items:[{ xtype:'label', id: 'TASAINTERES', name: 'TASAINTERES'}, {xtype:'button', id:'btnCotizaSolicCompleta', iconCls:'icoPdf'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'OBSERVOP_L', name: 'OBSERVOP_L', html: '<p align="right">Observaciones: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'OBSERVOP', name: 'OBSERVOP'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px;',
					items:[{xtype:'box', width: 205, id: 'USUARIOOP_L', name: 'USUARIOOP_L', html: '<p align="right">Usuario OP: </p>'}]
				},
				{
					xtype:'panel', bodyStyle: 	'padding: 2px; padding-bottom:5px; padding-right:5px;',
					items:[{ xtype:'label', id: 'USUARIOOP', name: 'USUARIOOP'}]
				}
			]
		},
		{
			xtype: 'panel',
			id: 'infoGridAmort',
			width		: 765,		
			bodyStyle: 	'padding: 2px; padding-bottom:5px;',
			items:[gridAmort]
		}
		
	]
	
	var fpCritBusq = new Ext.form.FormPanel({
		id: 'fpCritBusq',
		title: 'Solicitides de Recursos',
		hidden: false,
		height: 'auto',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) {
					
					var fecIni = Ext.getCmp('dFechaRegIni');
					var fecFin = Ext.getCmp('dFechaRegFin');
					
					if(fecIni.getValue()!='' && fecFin.getValue()==''){
						fecFin.markInvalid('Indicar Fecha final de Solicitud');
						return;
					}else if(fecFin.getValue()!='' && fecIni.getValue()==''){
						fecIni.markInvalid('Indicar Fecha inicial de Solicitud');
						return;
					}
					
					storeSolicsData.load({
						params: Ext.apply(fpCritBusq.getForm().getValues(),{
							cboEstatus:'2',
							usuarioOP: Ext.getDom('iNoUsuario').value
						})
					});
				}
			 },
			 {
				xtype: 'button',
				text: 'Limpiar',
				name: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				hidden: false,
				handler: function(boton, evento) { 
					window.location.href='38atencionSolicRec01ext.jsp'; 
				}
			 }
		]
	});
	
	var selectModel = new Ext.grid.CheckboxSelectionModel({
        checkOnly: true,
		  listeners: {
            rowselect: function(selectModel, rowIndex, record) {
							record.data['SELECCION']='S';
            },
				rowdeselect: function(selectModel, rowIndex, record) {
					record.data['SELECCION']='N';
				}
			}
		  /*renderer: function(v, p, record){
				if (record.data['SELECBOOL']){
					return '<div class="x-grid3-row-checker">&#160;</div>';
				}else{
					return '<div>&#160;</div>';
				}
			}*/
    });
	
	var gridSolicsRec = new Ext.grid.EditorGridPanel({
	id: 'gridSolicsRec',
	store: storeSolicsData,
	margins: '20 0 0 0',
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	sm: selectModel,
	columns: [
		selectModel,
		{//1
			header: 'Intermediario',
			tooltip: 'Intermediario',
			dataIndex: 'NOMBREINTER',
			sortable: true,
			width: 100,
			resizable: true,
			hidden: false
		},
		{//1
			header: 'No. Solicitud',
			tooltip: 'No. Solicitud',
			dataIndex: 'CCACUSE',
			sortable: true,
			width: 100,
			resizable: true,
			hidden: false
		},
		{//2
			header: 'Fecha de Solicitud',
			tooltip: 'Fecha de Solicitud',
			dataIndex: 'FECSOLICITUD',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//3
			header: 'Nombre Contrato',
			tooltip: 'Nombre Contrato',
			dataIndex: 'NOMBRECONTRATO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{//4
			header : 'Fecha Firma Contrato',
			tooltip: 'Fecha Firma Contrato',
			dataIndex : 'FECFIRMACONTRATO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//5
			header : 'Contratos Modificatorios',
			tooltip: 'Contratos Modificatorios',
			dataIndex : 'COUNTMODIF',
			width : 100,
			sortable : true
		},
		{//6
			header : 'Fecha Firma Modificatorios',
			tooltip: 'Fecha Firma Modificatorios',
			dataIndex : 'ALLFECMODCONT',
			width : 150,
			sortable : true,
			align: 'left',
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		},
		{//7
			header : 'Nombre Contrato OE',
			tooltip: 'Nombre Contrato OE',
			dataIndex : 'NOMBRECONTRATOOE',
			width : 150,
			sortable : true
		},
		{//8
			header : 'Fecha Firma Contrato OE',
			tooltip: 'Fecha Firma Contrato Operaci�n Electr�nica',
			dataIndex : 'FECFIRMACONTRATOOE',
			sortable : true,
			width : 100,
			align: 'left',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//9
			header : 'Importe',
			tooltip: 'Importe',
			dataIndex : 'IMPORTESOLIC',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'NOMBREMONEDA',
			width : 150,
			sortable : true
		},
		{//12
			header : 'Destino de los recursos',
			tooltip: 'Destino de los recursos',
			dataIndex : 'DESTINORECURSO',
			width : 150,
			sortable : true
		},
		{//13
			header : 'Fecha primer pago de Capital',
			tooltip: 'Fecha primer pago de Capital',
			dataIndex : 'FECPAGOCAPITAL',
			sortable : true,
			//hidden: true,
			width : 100,
			align: 'left'
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//14
			header : 'Fecha primer pago de Interes',
			tooltip: 'Fecha primer pago de Interes',
			dataIndex : 'FECPAGOINTERES',
			//hidden: true,
			sortable : true,
			width : 100,
			align: 'left'
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//15
			header : 'Fecha de Vencimiento',
			tooltip: 'Fecha de Vencimiento',
			dataIndex : 'FECVENCIMIENTO',
			//hidden: true,
			sortable : true,
			width : 100,
			align: 'left'
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//15
			header : 'Tipo de Tasa',
			tooltip: 'Tipo de Tasa',
			dataIndex : 'TIPOTASA',
			//hidden: true,
			sortable : true,
			width : 80,
			align: 'left'
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//15
			header : 'Tasa',
			tooltip: 'Tasa',
			dataIndex : 'NOMBRETASA',
			//hidden: true,
			sortable : true,
			width : 100,
			align: 'left'
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//15
			header : 'Valor',
			tooltip: 'Valor',
			dataIndex : 'TASAINTERES',
			//hidden: true,
			sortable : true,
			width : 80,
			align: 'left',
			renderer:  function (causa, columna, registro){
					//columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					if(registro.get('TIPOTASA') == 'Variable'){
						if(causa!='' && causa!='null'){
							return  Ext.util.Format.number(causa, '+0.00%');
						}else  {
							return 'N/A';
						}				
					}else{
						if(causa!='' && causa!='null'){
							return  Ext.util.Format.number(causa, '0.00%');
						}else  {
							return 'N/A';
						}				
					}	
				}
			//renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{
			xtype:	'actioncolumn',
			header: 'Carga Cotizaci�n', tooltip: 'Carga Cotizaci�n',
			dataIndex: 'CVESOLICITUD',	align: 'center',	sortable: true,	resizable: true,	width: 120,	hideable: false, hidden: false,
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						
							if(registro.data['EXISTEFILECOTIZA']=='S'){
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}else{
								return '';
							}
						
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						
						var registro = grid.getStore().getAt(rowIndex);
						
						if(registro.data['EXISTEFILECOTIZA']=='S'){
							Ext.Ajax.request({
							url : '38atencionSolicRec01ext.data.jsp',
							params: Ext.apply({
								informacion: "obtenerArchivo",
								cveSolicitud: registro.data['CVESOLICITUD'],
								tipoArchivo: 'COTIZA'
							}),
							callback: processSuccessFailureObtieneArchivo});
						}
						
				
					}
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'Tabla de Amortizaci�n', tooltip: 'Tabla de Amortizaci�n',
			dataIndex: 'CVESOLICITUD',	align: 'center',	sortable: true,	resizable: true,	width: 120,	hideable: false, hidden: false,
			/*renderer: function(value, metadata, record, rowindex, colindex, store) {
							return value+'&nbsp;&nbsp;';
						 },*/
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						
							this.items[0].tooltip = 'Ver';
							return 'icoXls';
						
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						var registro = grid.getStore().getAt(rowIndex);
						
											
						Ext.Ajax.request({
						url : '38atencionSolicRec01ext.data.jsp',
						params: Ext.apply({
							informacion: "obtenerArchivo",
							cveSolicitud: registro.data['CVESOLICITUD'],
							tipoArchivo: 'AMORTIZACION'
						}),
						callback: processSuccessFailureObtieneArchivo});
				
					}
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'Carga Cartera en Prenda', tooltip: 'Carga Cartera en Prenda',
			dataIndex: 'CVESOLICITUD',	align: 'center',	sortable: true,	resizable: true,	width: 120,	hideable: false, hidden: false,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
							if(record.data['CVECONTRATO']=='1' || record.data['CVECONTRATO']=='4' || record.data['CVECONTRATO']=='5'){
								return 'N/A&nbsp;&nbsp;';
							}
						 },
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						if(registro.data['CVECONTRATO']=='1' || registro.data['CVECONTRATO']=='4' || registro.data['CVECONTRATO']=='5'){
							return '';
						}else{
							this.items[0].tooltip = 'Ver';
							return 'icoXls';
						}
						
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						var registro = grid.getStore().getAt(rowIndex);
						if(registro.data['CVECONTRATO']!='1' && registro.data['CVECONTRATO']!='4' && registro.data['CVECONTRATO']!='5'){
							Ext.Ajax.request({
							url : '38atencionSolicRec01ext.data.jsp',
							params: Ext.apply({
								informacion: "obtenerArchivo",
								cveSolicitud: registro.data['CVESOLICITUD'],
								tipoArchivo: 'CARTERA'
							}),
							callback: processSuccessFailureObtieneArchivo});
						}
				
					}
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: ' PDF Cartera en Prenda', 
			tooltip: 'PDF Cartera en Prenda',
			dataIndex: 'EXISTEFILECARTERAPDF',
			align: 'center',	
			sortable: true,
			resizable: true,
			width: 120,
			hideable: false,
			hidden: false,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
							if(record.data['CVECONTRATO']=='1' || record.data['CVECONTRATO']=='4' || record.data['CVECONTRATO']=='5'|| record.data['EXISTEFILECARTERAPDF']=='N'){
								return 'N/A&nbsp;&nbsp;';
							}
						 },
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						if(registro.data['CVECONTRATO']=='1' || registro.data['CVECONTRATO']=='4' || registro.data['CVECONTRATO']=='5'){
							return 'N/A';
						}else{
							
							if(registro.data['EXISTEFILECARTERAPDF']=='S'){  
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';   
							}else{
								return 'N/A&nbsp;&nbsp';
							}
						}
						   
					},
					handler:function(grid, rowIndex, colIndex, item, event){    
						var registro = grid.getStore().getAt(rowIndex);
						if(registro.data['EXISTEFILECARTERAPDF']=='S'){
							if(registro.data['CVECONTRATO']!='1' && registro.data['CVECONTRATO']!='4' && registro.data['CVECONTRATO']!='5'){
								Ext.Ajax.request({
								url : '38atencionSolicRec01ext.data.jsp',
								params: Ext.apply({
									informacion: "obtenerArchivo",   
									cveSolicitud: registro.data['CVESOLICITUD'],
									tipoArchivo: 'CARTERA_PDF'
								}),
								callback: processSuccessFailureObtieneArchivo});
							}
						}
					}
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'PDF Comprobante Boleta RUG',
			tooltip: 'PDF Comprobante Boleta RUG',
			dataIndex: 'PDFBOLETARUG',
			align: 'center',	
			sortable: true,
			resizable: true,
			width: 120,
			hideable: false,
			hidden: false,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
							if(record.data['CVECONTRATO']=='1' || record.data['CVECONTRATO']=='4' || record.data['CVECONTRATO']=='5'){
								return 'N/A&nbsp;&nbsp;';
							}
						 },
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						
						if(registro.data['CVECONTRATO']=='1' || registro.data['CVECONTRATO']=='4' || registro.data['CVECONTRATO']=='5'){
							return '';
						}else{
							this.items[0].tooltip = 'Ver';
							return 'icoPdf';
						}
						
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						var registro = grid.getStore().getAt(rowIndex);
						if(registro.data['CVECONTRATO']!='1' && registro.data['CVECONTRATO']!='4' && registro.data['CVECONTRATO']!='5'){
							Ext.Ajax.request({
							url : '38atencionSolicRec01ext.data.jsp',
							params: Ext.apply({
								informacion: "obtenerArchivo",
								cveSolicitud: registro.data['CVESOLICITUD'],
								tipoArchivo: 'RUG'
							}),
							callback: processSuccessFailureObtieneArchivo});
						}
					}
				}
			]
		},
		/*{
			xtype:	'actioncolumn',
			header: 'Documentaci�n Prenda', tooltip: 'PDF Documentaci�n Prenda',
			dataIndex: 'CVESOLICITUD',	align: 'center',	sortable: true,	resizable: true,	width: 120,	hideable: false, hidden: false,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
							if(record.data['CVECONTRATO']=='1' || record.data['CVECONTRATO']=='4' || record.data['CVECONTRATO']=='5'){
								return 'N/A&nbsp;&nbsp;';
							}
						 },
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						if(registro.data['CVECONTRATO']=='1' || registro.data['CVECONTRATO']=='4' || registro.data['CVECONTRATO']=='5'){
							return '';
						}else{
							this.items[0].tooltip = 'Ver';
							return 'icoTxt';
						}
						
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						var registro = grid.getStore().getAt(rowIndex);
						
						if(registro.data['CVECONTRATO']!='1' && registro.data['CVECONTRATO']!='4' && registro.data['CVECONTRATO']!='5'){
							Ext.Ajax.request({
							url : '38atencionSolicRec01ext.data.jsp',
							params: Ext.apply({
								informacion: "obtenerArchivo",
								cveSolicitud: registro.data['CVESOLICITUD'],
								tipoArchivo: 'PRENDA'
							}),
							callback: processSuccessFailureObtieneArchivo});
						}
					}
				}
			]
		},*/
		{
			xtype:	'actioncolumn',
			header: 'Solicitud Completa', tooltip: 'Solicitud Completa',
			dataIndex: 'CVESOLICITUD',	align: 'center',	sortable: true,	resizable: true,	width: 120,	hideable: false, hidden: false,
			/*renderer: function(value, metadata, record, rowindex, colindex, store) {
							return value+'&nbsp;&nbsp;';
						 },*/
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						
							this.items[0].tooltip = 'Ver';
							return 'icoVistaPrevia';
						
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						
						var registro = grid.getStore().getAt(rowIndex);
						
											
						Ext.Ajax.request({
						url : '38atencionSolicRec01ext.data.jsp',
						params: Ext.apply({
							informacion: "obtenerArchivo",
							cveSolicitud: registro.data['CVESOLICITUD'],
							tipoArchivo: 'ACUSE'
						}),
						callback: processSuccessFailureObtieneArchivo
						});
						
				
					}
				}
			]
		},
		{
			header : 'Estatus',
			tooltip: 'Estatus',
			dataIndex : 'NOMBREESTATUS',
			hidden: false,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{
			xtype:	'actioncolumn',
			header: 'Validar check list',
			tooltip: 'Validar check list',					
			align: 'center',				
			width: 150,
			items: [
				{
					getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Modificar';
							return 'modificar';		
					}
					,handler:	validaCheckList  
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'Bit�cora de revisiones',
			tooltip: 'Bit�cora de revisiones',					
			align: 'center',				
			width: 100,
			items: [
				{
					getClass: function(value, metadata, record, rowIndex, colIndex, store) {
						if(record.get('EXISTE_REVISION') !=0 ){
							this.items[0].tooltip = 'ver';
							return 'iconoLupa';	
						}
					}
					,handler:	bitacoraReviciones  
				}
			]
		},
		{
			header : 'Fecha y Hora de Asignaci�n',
			tooltip: 'Fecha y Hora de Asignaci�n',
			dataIndex : 'FECASIGNAEJEOP',
			sortable : true,
			width : 120,
			align: 'center'
		},
		{
			header : 'Usuario que envi�',
			tooltip: 'Usuario que envi�',
			dataIndex : 'NOMBREUSRCON1',
			sortable : true,
			width : 150,
			align: 'center'
		}
		/*{
			header : 'Observaciones',
			tooltip: 'Observaciones Ejecutivo de Operaci�n',
			dataIndex : 'OBSERVOP',
			sortable : true,
			width : 250,
			align: 'center'
		}*/
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 300,
	width: 943,
	style: 'margin:0 auto;',
	title: '',
	frame: true,
	listeners: {
		//viewReady: selectDefaultDoctos
	},
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
				text: 'Aceptar',
				id: 'btnAceptaConfirmacion',
				iconCls: 'icoAceptar',
				handler: function(btn){
					hdrAceptadaConfrimacion();
				}
			},
			'-',
			{
				text: 'Rechazar',
				id: 'btnRechazaConfirmacion',
				iconCls: 'icoRechazar',
				handler: hdrRechazadaConfrimacion
			}
		]
	}
	});
	
	
//----------------PANEL VISTA PREVIA CORREO-------------------------------------
var fpVista = new Ext.form.FormPanel({
		id: 'fpVista',
		width: 600,
		title: 'Vista Previa del Correo',
		frame: true,
		hidden: false,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;'
	});


//--------------PANEL PARA SOLICITUD COMPLETA---------------------------------------
	var fpSolicCompleta = new Ext.form.FormPanel({
		id: 'fpSolicCompleta',
		width: 800,
		title: 'Mesa de Control (Env�o datos por parte del IF)',
		frame: true,
		hidden: true,
		//labelWidth: 300,
		//bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		items: elementosSolicCopmleta,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Cerrar',
			  name: 'btnCerrar',
			  iconCls: 'icoCancelar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) {
					fpSolicCompleta.hide();
					gridSolicsRec.show();
					//fpEjcutivoOper.show();
					fpCritBusq.show();
					
				}
			 }
		]
		
	});
//--------------PANEL PARA ABRIR ARCHIVOS---------------------------------------
	var fpArchivo = new Ext.form.FormPanel({
		id: 'fpArchivo1',
		width: 600,
		title: '',
		frame: true,
		hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;'
	});

//--------------PANEL ACEPTADAS PARA CONFIRMACION----------------------------------

 var fpAceptada = new Ext.form.FormPanel({
		id: 'fpAceptada1',
		width: 500,
		title: 'Mesa de Control - Aceptada para Autorizaci�n',
		labelWidth: 150,
		frame: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		items:[
			/*
			{
				xtype: 'datefield',
				fieldLabel: '<p align="right">Fecha Abono Recursos',
				name: 'dFechaAbonoRec',
				id: 'dFechaAbonoRec_',
				allowBlank: false,
				msgTarget: 'side',
				width: 100,
				startDay: 0,
				width: 100,
				margins: '0 20 0 0'
			},
			*/
			{
				xtype: 'label',
				id: 'lNombreIf',
				fieldLabel: '<p align="right">Nombre IF'
			},
			{
				xtype: 'label',
				id: 'lFecOperado',
				fieldLabel: '<p align="right">Fecha Abono Recursos'
			},
			{
				xtype: 'label',
				id: 'lHoraOperado',
				fieldLabel: '<p align="right">Hora Operado'
			},
			{
				xtype: 'label',
				id: 'lUsuarioOp',
				fieldLabel: '<p align="right">Usuario Ejecutivo de Operaci�n'
				//text: 'Maximinimo Garcia Solis'
			}
		]
	});
	
 var fpRechazada = new Ext.form.FormPanel({
		id: 'fpRechazada1',
		width: 500,
		title: 'Mesa de Control - Solicitud de Recursos Rechazada',
		labelWidth: 200,
		frame: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		items:[
			{
				xtype: 'label',
				id: 'lrNombreIf',
				bodyStyle: 'padding: 6px',
				fieldLabel: '<p align="right">Nombre IF'
				//text: 'AF BANREGIO S.A. DE C.V.'
			},
			{
				xtype: 'label',
				id: 'lrFecRechazo',
				fieldLabel: '<p align="right">Fecha de Rechazo'
				//text: 'AF BANREGIO S.A. DE C.V.'
			},
			{
				xtype: 'label',
				id: 'lrHoraRechazo',
				fieldLabel: '<p align="right">Hora de Rechazo'
				//text: 'AF BANREGIO S.A. DE C.V.'
			},
			{
				xtype: 'label',
				id: 'lrUsuarioOp',
				fieldLabel: '<p align="right">Usuario Ejecutivo de Operaci�n'
				//text: 'Maximinimo Garcia Solis'
			}
		]
	});
	
 var griAceptSolic = new Ext.grid.EditorGridPanel({
			id: 'griAceptSolic1',
			store: storeSolicAceptaData,
			margins: '20 0 0 0',
			style: 'margin: 0 auto',
			title: '',
			clicksToEdit: 1,
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			columns: [
				{
					header: 'No. de Solicitud',
					tooltip: 'No. de Solicitud',
					dataIndex: 'CCACUSE',
					sortable: true,
					width: 100,
					resizable: true,
					hidden: false
				},
				{
					header: 'No. Pr�stamo',
					tooltip: 'No. Pr�stamo',
					dataIndex: 'NUMPRESTAMO',
					sortable: true,
					width: 120,
					resizable: true,
					hidden: false,
					editor: new Ext.form.NumberField({
						 allowBlank: false,
						 allowDecimals: false,
						 allowNegative: false,
						 maxValue: 999999999999
					}),
					renderer:function(value,metadata,registro, rowIndex, colIndex){
						return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},
				/*{
					header: 'Fecha Operado',
					tooltip: 'Fecha Operado',
					dataIndex: 'FECOP',
					sortable: true,
					width: 120,
					resizable: true,
					hidden: false,
					editor: new Ext.form.DateField({
						 format: 'd/m/Y',
						 allowBlank: false,
						 startDay: 0
						 //disabledDays: [0, 6],
						 //disabledDaysText: 'Fin de semana inhabilitado'
					}),
					renderer:function(value,metadata,registro, rowIndex, colIndex){
						return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
					}
				},
				{
					header: 'Hora Operado',
					tooltip: 'Hora Operado',
					dataIndex: 'HORAOP',
					sortable: true,
					width: 120,
					resizable: true,
					hidden: false,
					editor: new Ext.form.TextField({
							 regex: /(^(((\d){2}))((:){1}(\d){2})((:){1}(\d){2})$)|(^((\d){2})((:){1}(\d){2})$)/
						}),
					renderer:function(value,metadata,registro, rowIndex, colIndex){
						return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},*/
				{//9
					header : 'Importe',
					tooltip: 'Importe',
					dataIndex : 'IMPORTESOLIC',
					sortable : true,
					width : 100,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},
				{//11
					header : 'Moneda',
					tooltip: 'Moneda',
					dataIndex : 'NOMBREMONEDA',
					width : 150,
					sortable : true
				},
				{//9
					header : 'No. de Cuenta',
					tooltip: 'No. de Cuenta',
					dataIndex : 'NUMCUENTA',
					sortable : true,
					width : 100
				},
				{//11
					header : 'Banco',
					tooltip: 'Banco',
					dataIndex : 'DESCBANCO',
					width : 150
				},
				{//11
					header : 'Usuario IF',
					tooltip: 'Usuario IF',
					dataIndex : 'NOMBREUSERIF',
					width : 150
				}
			],
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 250,
			width: 800,
			frame: true,
			bbar: {
				xtype: 'toolbar',
				items: [
					'->',
					'-',
					{
					text: 'Aceptar',
					id: 'btnAceptaAcept',
					iconCls: 'aceptar',
					handler: function(){
							var infoCompleta = true;
							var registrosEnviar = [];
							var observ = Ext.getCmp('fpObservaciones_').getValue();
							//var fecAbono = Ext.getCmp('dFechaAbonoRec_').getValue();
							
							if(!Ext.getCmp('fpObservaciones_').isValid()){
								return;
							//}else if(fecAbono == ''){
							//	Ext.Msg.alert('Aviso', 'Favor de indicar la Fecha de Abono de Recursos');
							//}else if(observ == ''){
							//	Ext.Msg.alert('Aviso', 'Favor de indicar las observaciones');
							}else{
							
								storeSolicAceptaData.each(function(record){
									registrosEnviar.push(record.data);
									
									if(record.data['NUMPRESTAMO'] == '' ){
										infoCompleta = false;
									}
									
									//var valFecOp = Ext.util.Format.date(record.data['FECOP'],'d/m/Y');
									//record.data['FECOP'] = valFecOp;
								});
								
								if(infoCompleta){
									Ext.Msg.confirm('Confirmaci�n', '�Confirma aceptar la(s) solicitud(es)?', function(btn){
										if(btn=='yes'){
											Ext.Ajax.request({
												url: '38atencionSolicRec01ext.data.jsp',
												params: {
													informacion: 'aceptaSolicsAcepConfirmacion',
													registros: Ext.encode(registrosEnviar),
													observaciones: observ
												},
												callback: procesarSuccessAceptadaConfirmacion
											});
										}
									});
								}else{
									Ext.Msg.alert('Aviso', 'Es necesario llenar los campos vacios');
								}
							}
						}
					},
					'-',
					{
					text: 'Cancelar',
					id: 'btnCancelarAcept',
					iconCls: 'icoCancelar',
					handler: function(btn){
							pnlAceptadas.hide();
							fpCritBusq.show();
							gridSolicsRec.show();
						}
					}
				]
			}
	});
	
	var gridRechazaSolic = new Ext.grid.EditorGridPanel({
			id: 'gridRechazaSolic1',
			store: storeSolicRechazadaData,
			margins: '20 0 0 0',
			style: 'margin: 0 auto',
			title: '',
			clicksToEdit: 1,
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			columns: [
				{
					header: 'No. de Solicitud',
					tooltip: 'No. de Solicitud',
					dataIndex: 'CCACUSE',
					sortable: true,
					width: 100,
					resizable: true,
					hidden: false
				},
				{
					header: 'No. Folio',
					tooltip: 'No. Folio',
					dataIndex: 'FOLIO',
					sortable: true,
					width: 120,
					resizable: true,
					hidden: false,
					editor: new Ext.form.NumberField({
						 allowBlank: false,
						 allowDecimals: false,
						 allowNegative: false,
						 maxValue: 999999999999
					}),
					renderer:function(value,metadata,registro, rowIndex, colIndex){
						return NE.util.colorCampoEdit(value,metadata,registro);
					}
				},
				{
					header : 'Importe',
					tooltip: 'Importe',
					dataIndex : 'IMPORTESOLIC',
					sortable : true,
					width : 100,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},
				{
					header : 'Moneda',
					tooltip: 'Moneda',
					dataIndex : 'NOMBREMONEDA',
					width : 150,
					sortable : true
				},
				{
					header : 'Usuario IF',
					tooltip: 'Usuario IF',
					dataIndex : 'NOMBREUSERIF',
					width : 150
				}
			],
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 250,
			width: 700,
			frame: true,
			bbar: {
				xtype: 'toolbar',
				items: [
					'->',
					'-',
					{
					text: 'Aceptar',
					id: 'btnAceptaAceptR',
					iconCls: 'aceptar',
					handler: function(){
							var infoCompleta = true;
							var registrosEnviar = [];
							var vFecRechazo = Ext.getCmp('lrFecRechazo').text;
							var vHoraRechazo = Ext.getCmp('lrHoraRechazo').text;
							var causasRechazo = Ext.getCmp('fpCausasRech_').getValue();
							
							if(!Ext.getCmp('fpCausasRech_').isValid()){
								return;
							}else if(causasRechazo==''){
								Ext.Msg.alert('Aviso', 'Favor de ingresar Causas de Rechazo');
							}else{
								storeSolicRechazadaData.each(function(record){
									registrosEnviar.push(record.data);
									if(record.data['FOLIO'] == ''){
										infoCompleta = false;
									}
								});
								
								if(infoCompleta){
									Ext.Msg.confirm('Confirmaci�n', '�Confirma rechazar la(s) solicitud(es)?', function(btn){
										if(btn=='yes'){
											Ext.Ajax.request({
												url: '38atencionSolicRec01ext.data.jsp',
												params: {
													informacion: 'rechazaSolicsRechConfirmacion',
													fecRechazo: vFecRechazo,
													horaRechazo: vHoraRechazo,
													registros: Ext.encode(registrosEnviar),
													causasRech: causasRechazo
												},
												callback: procesarSuccessRechazadaConfirmacion
											});
										}
									});
								}else{
									Ext.Msg.alert('Aviso', 'Es necesario llenar los campos vacios');
								}
							}
						}
					},
					'-',
					{
					text: 'Cancelar',
					id: 'btnCancelarAceptR',
					iconCls: 'icoCancelar',
					handler: function(btn){
							pnlRechazadas.hide();
							fpCritBusq.show();
							gridSolicsRec.show();
						}
					}
				]
			}
	});

	var fpObservaciones = new Ext.form.FormPanel({
		id: 'fpObservaciones',
		width: 500,
		title: '',
		frame: true,
		//hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		items:[
			{
				xtype: 'textarea',
				fieldLabel: 'Observaciones',
				allowBlank: true,
				name:'observaciones',
				maxLength: 800,
				id: 'fpObservaciones_',
				anchor: '100%'
			}
		]
	});
	
	var fpCausasRech = new Ext.form.FormPanel({
		id: 'fpCausasRech',
		width: 500,
		title: '',
		frame: true,
		//hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		items:[
			{
				xtype: 'textarea',
				fieldLabel: 'Causas de Rechazo',
				allowBlank: false,
				maxLength: 800,
				name:'causasRech',
				id: 'fpCausasRech_',
				anchor: '100%'
			}
		]
	});

 var pnlAceptadas = new Ext.Panel({
	id: 'pnlAceptadas',
	width: 890,
	hidden: true,
	items: [fpAceptada, NE.util.getEspaciador(10), griAceptSolic, NE.util.getEspaciador(10), fpObservaciones]
 });
 
  var pnlRechazadas = new Ext.Panel({
	id: 'pnlRechazadas',
	width: 890,
	hidden: true,
	items: [fpRechazada, NE.util.getEspaciador(10), gridRechazaSolic, NE.util.getEspaciador(10), fpCausasRech]
 });

//--------------CONTENEDOR PRINCIPAL--------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fpArchivo,
			NE.util.getEspaciador(10),
			fpCritBusq,
			fpSolicCompleta,
			pnlAceptadas,
			pnlRechazadas,
			NE.util.getEspaciador(10)
		]
	});
	
	storeSolicsData.load({
		params: Ext.apply(fpCritBusq.getForm().getValues(),{
			cboEstatus:'2',
			consIni:'S',
			usuarioOP: Ext.getDom('iNoUsuario').value
		})
	});


});