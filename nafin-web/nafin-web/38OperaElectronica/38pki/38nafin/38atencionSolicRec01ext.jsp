<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession.jspf"%>


<html>
<head>
<title>Nafi@net - Autorizaciones de Solicitudes</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>

<script type="text/javascript" src="38atencionSolicRec01ext.js?<%=session.getId()%>"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
<%}%>
		 <div id="areaContenido"></div>                                                                                          
<%if(esEsquemaExtJS){%>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>
<input type="hidden" id="strNombreUsuario" value="<%=strNombreUsuario%>">
<input type="hidden" id="iNoUsuario" value="<%=iNoUsuario%>">
<input type="hidden" id="strPerfil" value="<%=strPerfil%>">
</body>
</html>
