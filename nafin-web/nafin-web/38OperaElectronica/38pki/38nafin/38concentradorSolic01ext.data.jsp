<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoIfOperElec,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.electronica.*,
		netropology.utilerias.usuarios.*,
		com.netro.pdf.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf" %>

<%
JSONObject jsonObj = new JSONObject();
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String tipoAfiliado = (request.getParameter("tipoAfiliado")!=null)?request.getParameter("tipoAfiliado"):"";
String infoRegresar	=	"";

UtilUsr utilUsr = new UtilUsr();
OperacionElectronica  OperElec = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);

if (informacion.equals("catologoIfSolics")){
	CatalogoIfOperElec catalogo = new CatalogoIfOperElec();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	//catalogo.setOrden("cd_descripcion");
	infoRegresar = catalogo.getJSONElementos();
	
}else if (informacion.equals("catologoEstatus")){
	List lstIn = new ArrayList();
	lstIn.add(new Integer("1"));
	lstIn.add(new Integer("3"));
	lstIn.add(new Integer("4"));
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_estatus_ope");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("opecat_estatus");
	catalogo.setOrden("cd_descripcion");
	catalogo.setCondicionIn(lstIn);
	infoRegresar = catalogo.getJSONElementos();
	
}else if (informacion.equals("consultaSolics")){
	JSONArray jsObjArray = new JSONArray();
	String cveIf = request.getParameter("cboIf")==null?"1":request.getParameter("cboIf");
	String cboEstatus = request.getParameter("cboEstatus")==null?"":request.getParameter("cboEstatus");
	String fpNumSolic = request.getParameter("fpNumSolic")==null?"":request.getParameter("fpNumSolic");
	String dFechaRegIni = request.getParameter("dFechaRegIni")==null?"":request.getParameter("dFechaRegIni");
	String dFechaRegFin = request.getParameter("dFechaRegFin")==null?"":request.getParameter("dFechaRegFin");
	String consIni = request.getParameter("consIni")==null?"":request.getParameter("consIni");
	
	if("S".equals(consIni)){
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		dFechaRegIni = fechaActual;
		dFechaRegFin = fechaActual;
	}
	
	List lstRegSolic = OperElec.consultaSolicRec(cveIf, cboEstatus, fpNumSolic, dFechaRegIni, dFechaRegFin);
	
	jsObjArray = JSONArray.fromObject(lstRegSolic);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
}else if (informacion.equals("catalogoEjecutivoOP")){
	JSONArray jsObjArray = new JSONArray();
	List lstCatalogoEjecOP = new ArrayList();
	List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("OP OP", "N");
	
	if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();

			hmData.put("clave", loginUsuarioOPOP);
			hmData.put("descripcion", nombre);
			hmData.put("loadMsg", "");
			
			lstCatalogoEjecOP.add(hmData);
		}
	}
	
	
	jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
	//System.out.println("infoRegresar = "+infoRegresar);

}else if (informacion.equals("obtenInfoVistaPrevia")){
	String usuarioIfSolic = (request.getParameter("usuarioIfSolic") == null)?"":request.getParameter("usuarioIfSolic");
	HashMap hmInfo = OperElec.obtieneInfoCorreo(usuarioIfSolic);
	HashMap hmGerConc = OperElec.obtieneInfoConcentrador(iNoUsuario);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
	fechaActual = "México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual;
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("fechaActual", fechaActual);
	jsonObj.put("dataUserIfObj", hmInfo);
	if(hmGerConc.containsKey("CONCENTRADOR")!=false){
		jsonObj.put("datosGerCon", hmGerConc);
	}else{
		jsonObj.put("datosGerCon", null);
	}

	
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenerArchivo")){
	String cveSolicitud = request.getParameter("cveSolicitud");
	String tipoArchivo = request.getParameter("tipoArchivo");
	
	String nombreArchivo = OperElec.obtieneArchivoSolic(strDirectorioTemp, cveSolicitud, tipoArchivo);
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("consultaSolicCompleta")){
	JSONArray jsObjArray = new JSONArray();
	String cveSolicitud = request.getParameter("cveSolicitud")==null?"":request.getParameter("cveSolicitud");
	
	List lstInfoSolicComp = OperElec.obtieneInfoSolicCompleta(cveSolicitud);
	
	HashMap hmInfoSolic = (HashMap)lstInfoSolicComp.get(0);
	List lstAmort = (List)lstInfoSolicComp.get(1);
	jsObjArray = JSONArray.fromObject(lstAmort);
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("objInfoSolic", hmInfoSolic);
	jsonObj.put("registros", jsObjArray.toString());
	infoRegresar = jsonObj.toString();


}else if (informacion.equals("asignaEjecutivo")){
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String cveEjecOP = (request.getParameter("cveEjecOP") == null)?"":request.getParameter("cveEjecOP");
	String nombreIf = (request.getParameter("nombreIf") == null)?"":request.getParameter("nombreIf");
	List lstSolicitudes = new ArrayList();
	List lstEstatus = new ArrayList();
	
	List arrRegistros = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistros.iterator();
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		lstSolicitudes.add(registro.getString("CVESOLICITUD"));
		lstEstatus.add(registro.getString("ESTATUSSOLIC"));
	}
	
	
	OperElec.asignaEjecutivoOP(nombreIf, cveEjecOP, lstSolicitudes,lstEstatus, iNoUsuario, strNombreUsuario);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("cambiaEstatusSolics")){
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String cveEstatusGral = (request.getParameter("cveEstatusGral") == null)?"":request.getParameter("cveEstatusGral");
	String observacionesIf = (request.getParameter("observacionesIf") == null)?"":request.getParameter("observacionesIf");
	String nombreIf = (request.getParameter("nombreIf") == null)?"":request.getParameter("nombreIf");
	List lstSolicitudes = new ArrayList();
	
	List arrRegistros = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistros.iterator();
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		lstSolicitudes.add(registro.getString("CVESOLICITUD"));
	}
	
	OperElec.cambiaEstatusSolic(cveEstatusGral, observacionesIf, nombreIf, lstSolicitudes, iNoUsuario, strNombreUsuario);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("generarVistaPrevPdf")){
	int contador = 0;
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String tipoVista = (request.getParameter("tipoVista") == null)?"":request.getParameter("tipoVista");
	String observacionesIf = (request.getParameter("observacionesIf") == null)?"":request.getParameter("observacionesIf");
	String importeTotal = (request.getParameter("importeTotal") == null)?"":request.getParameter("importeTotal");
	String nombreMoneda = (request.getParameter("nombreMoneda") == null)?"":request.getParameter("nombreMoneda");
	String nombreIF = (request.getParameter("nombreIF") == null)?"":request.getParameter("nombreIF");
	String nombreUserIf = (request.getParameter("nombreUserIf") == null)?"":request.getParameter("nombreUserIf");
	
	String titulo = "";
	String textoA = "";
	String textoB = "";
	String textoC = "";
	String txtObs = "";
	
	List lstSolicitudes = new ArrayList();
	List arrRegistros = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistros.iterator();
	HashMap hmInfoGerConc = OperElec.obtieneInfoConcentrador(iNoUsuario);
	String puesto = "";
	if("A".equals(tipoVista)){
		titulo = "Confirmación de Abono";
		textoA = "Por este conducto, hago de su conocimiento que el día de hoy se abonó a la cuenta "+
					"de cheques que nos fue proporcionada, la cantidad de "+"$ "+Comunes.formatoDecimal(importeTotal,2)+" "+nombreMoneda+
					" derivado de la realización de sus operaciones de crédito solicitadas ante esta institución, de acuerdo con  el siguiente detalle:\n\n";
		if(hmInfoGerConc.get("CONCENTRADOR").equals("1")){
			puesto ="Gerente de Operaciones de Crédito";
		}
		if(hmInfoGerConc.get("CONCENTRADOR").equals("2")){
			puesto ="Mesa de Control de Crédito";
		}
		textoB = "Sin otro particular, reciba un cordial saludo.\n\n"+
					"ATENTAMENTE\n" +
					hmInfoGerConc.get("GERENTE_NOMBRE")+"\n" +
					puesto+"\n" +
					hmInfoGerConc.get("GERENTE_MAIL")+"\n" +
					hmInfoGerConc.get("GERENTE_TELEFONO")+"\n\n";
		txtObs = "\n\nObservaciones Ejecutivo OP:\n";
	}else if("R".equals(tipoVista)){
		titulo = "Solicitud de Recursos Rechazada";
		textoA = "Por este medio, se hace de su conocimiento, que la operación de crédito solicitada a Nacional Financiera, " +
					"S.N.C. por un importe total de $ "+Comunes.formatoDecimal(importeTotal,2)+" "+nombreMoneda+", no se concluyó como trámite de disposición, en virtud de las causas señaladas a continuación:\n\n";
		if(hmInfoGerConc.get("CONCENTRADOR").equals("1")){
			puesto ="Gerente de Operaciones de Crédito";
		}
		if(hmInfoGerConc.get("CONCENTRADOR").equals("2")){
			puesto ="Mesa de Control de Crédito";
		}
		textoB = "Derivado de lo anterior y con el propósito de estar en posibilidades de atender su solitud de recursos, le agradecemos a la brevedad posible las correcciones y/o aclaraciones correspondientes.\n\n" +
					"Sin otro particular, reciba un cordial saludo.\n\n"+
					"ATENTAMENTE\n" +
					hmInfoGerConc.get("GERENTE_NOMBRE")+"\n" +
					puesto+"\n" +
					hmInfoGerConc.get("GERENTE_MAIL")+"\n" +
					hmInfoGerConc.get("GERENTE_TELEFONO")+"\n\n";
		txtObs = "\n\nCausas de Rechazo:\n";
	}
	textoC = "Con fundamento en el articulo 142 de la Ley de Instituciones de Crédito, " +
				"14 fracción I y 15 de la Ley Federal de Transparencia y Acceso a la información Pública Gubernamental, " +
				"asi como al artículo 30 de su Reglamento, el contenido del presente mensaje de correo electrónico es de caracter Reservado.";
	
	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = archivo.nombreArchivo()+".pdf";
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),(session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString(),
							 (String)session.getAttribute("sesExterno"), (String) session.getAttribute("strNombre"), (String) session.getAttribute("strNombreUsuario"),
							 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);

	pdfDoc.addText("\n\n"+titulo,"formas",ComunesPDF.CENTER);
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	
	pdfDoc.addText(nombreIF+"\n"+nombreUserIf+"\n\n","formas",ComunesPDF.LEFT);
	pdfDoc.addText(textoA,"formas",ComunesPDF.LEFT);
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
		lstSolicitudes.add(registro.getString("CVESOLICITUD"));
		if(contador == 0){
			if("R".equals(tipoVista)){
				pdfDoc.setTable(3,40);
				pdfDoc.setCell("Número de Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Folio","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Importe","celda01",ComunesPDF.CENTER);
				//pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
			}else if("A".equals(tipoVista)){
				pdfDoc.setTable(7,70);
				pdfDoc.setCell("Préstamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Operado","celda01",ComunesPDF.CENTER);
				//pdfDoc.setCell("Hora Operado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Número de Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta CLABE","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Banco","celda01",ComunesPDF.CENTER);
			}
		}
		if("R".equals(tipoVista)){
			pdfDoc.setCell(registro.getString("CCACUSE"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(registro.getString("FOLIO"),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(registro.getString("IMPORTESOLIC"),2),"formas",ComunesPDF.RIGHT);
			//pdfDoc.setCell(registro.getString("NOMBREMONEDA"),"formas",ComunesPDF.CENTER);
		}else if("A".equals(tipoVista)){
			pdfDoc.setCell(registro.getString("NUMPRESTAMO"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(registro.getString("IMPORTESOLIC"),2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(registro.getString("NOMBREMONEDA"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(registro.getString("FECOP"),"formas",ComunesPDF.RIGHT);
			//pdfDoc.setCell(registro.getString("HORAOP"),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(registro.getString("NUMCUENTA"),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(registro.getString("NUMCLABE"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(registro.getString("NOMBREBANCO"),"formas",ComunesPDF.CENTER);
		}
		contador++;
	}
	pdfDoc.addTable();
	
	if("R".equals(tipoVista)){
		pdfDoc.setTable(3,40);
		pdfDoc.setCell("Números de Solicitud","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Importe Total","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(contador),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(nombreMoneda,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(importeTotal,2),"formas",ComunesPDF.CENTER);
	}else if("A".equals(tipoVista)){
		pdfDoc.setTable(3,40);
		pdfDoc.setCell("No. de Préstamos","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Importe Total","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(contador),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(nombreMoneda,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(importeTotal,2),"formas",ComunesPDF.CENTER);
	}
				
	pdfDoc.addTable();
	
	pdfDoc.addText(txtObs,"formas",ComunesPDF.LEFT);
	pdfDoc.addText(observacionesIf+"\n\n","formas",ComunesPDF.LEFT);
	pdfDoc.addText(textoB,"formas",ComunesPDF.LEFT);
	pdfDoc.addText(textoC,"formas",ComunesPDF.LEFT);
	
	pdfDoc.endDocument();
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
}else if (informacion.equals("elementosCheckList")){

	JSONArray jsObjArray = new JSONArray();
	String CVESOLICITUD = request.getParameter("CVESOLICITUD")==null?"":request.getParameter("CVESOLICITUD");
	List lstElemenCheckList = OperElec.consultaCheckList("CON",CVESOLICITUD,iNoUsuario);
	jsObjArray = JSONArray.fromObject(lstElemenCheckList);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
}else if (informacion.equals("guardarCheckList")){
	infoRegresar ="";
	JSONObject jsonObj1 = new JSONObject();
	String check = "";
	boolean resultado = false;
	String numeroRegistros  =(request.getParameter("numeroRegistros")!=null) ? request.getParameter("numeroRegistros"):"";
	String  ic_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
	int i_numReg = Integer.parseInt(numeroRegistros);
	for(int i = 0; i< i_numReg; i++){  
			String chkReviNueva[] = request.getParameterValues("chkReviNueva");
			String chkReviAnt[] = request.getParameterValues("chkReviAnt");
			String listCorre[] = request.getParameterValues("listCorre");
			String listObs[] = request.getParameterValues("listObs");
			String listCorreAnt[] = request.getParameterValues("listCorreAnt");
			String listObsAnt[] = request.getParameterValues("listObsAnt");
			String idElemCheck[] = request.getParameterValues("idElemCheck");
			if((!listCorreAnt[i].equals(listCorre[i]))||(!listObsAnt[i].equals(listObs[i]))){
				if((listCorreAnt[i].equals("N")&&listCorre[i].equals("S"))||(listCorreAnt[i].equals("S")&&listCorre[i].equals("N"))||(!listObsAnt[i].equals(listObs[i]))||(listCorreAnt[i].equals("")&&(listCorre[i].equals("S")||listCorre[i].equals("N")))){
					
					boolean varGuarda = OperElec.guardaChkList(ic_solicitud,iNoUsuario,idElemCheck[i],"CON",listCorre[i], listObs[i] );
				}else if(listCorre[i].equals("")){
					OperElec.limpiarCheck(ic_solicitud,iNoUsuario,idElemCheck[i],"CON");
				}
			}
	}
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("ic_solicitud", ic_solicitud);
	infoRegresar = jsonObj.toString();
}else if (informacion.equals("validaListCheck")){
	String jsonRegistros[] =  request.getParameterValues("registros");
	String cveEjecOP = (request.getParameter("cveEjecOP") == null)?"":request.getParameter("cveEjecOP");
	String listClave ="";
	String listClaveAux ="";
	String listAcuse = "";
	String rechazoInterno = "";
	String rechazoInternoAux = "";
	String listAcuseRechazo = "";
	String listClaveC ="";
	String listClaveAuxC ="";
	String listClaveInC ="";
	String listClaveAuxInC ="";
	String listAcuseC= "";
	String listAcuseInC = "";
	HashMap	hmData = new HashMap();
	if(jsonRegistros!=null && jsonRegistros.length>0){
		for(int x=0; x<jsonRegistros.length; x++){
			hmData = new HashMap();
			String clave =(String)jsonRegistros[x];
			hmData =OperElec.validaListCheck(clave, iNoUsuario, "CON");
			if(!hmData.get("TOTAL_ELE").equals("9")){
				String rechazo = OperElec.existeRechazoInterno(clave,cveEjecOP);
				if(rechazo.equals("si")){
					rechazoInterno += clave +",";
				}
				listClave += clave +",";
			}else{
				if(!hmData.get("TOTAL_INCORRECTO").equals("0")){
					listClaveInC += clave +",";
				}else{
					listClaveC += clave +",";
					
				}
			}
		}
	}
	
	if(!listClave.equals("")){
		listClaveAux = listClave.substring(0,(listClave.length()-1));
		listAcuse = OperElec.getAcuseCheck(listClaveAux);
	}
	if(!rechazoInterno.equals("")){
		rechazoInternoAux = rechazoInterno.substring(0,(rechazoInterno.length()-1));
		listAcuseRechazo = OperElec.getAcuseCheck(rechazoInternoAux);
	}
	
	if(!listClaveInC.equals("")){
		listClaveAuxInC = listClaveInC.substring(0,(listClaveInC.length()-1));
		listAcuseInC = OperElec.getAcuseCheck(listClaveAuxInC);
	}
	if(!listClaveC.equals("")){
		listClaveAuxC = listClaveC.substring(0,(listClaveC.length()-1));
		listAcuseC = OperElec.getAcuseCheck(listClaveAuxC);
	}

	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("clave", listAcuse);
	jsonObj.put("listAcuseRechazo", listAcuseRechazo);
	jsonObj.put("listAcuseInC", listAcuseInC);
	jsonObj.put("listAcuseC", listAcuseC);
	infoRegresar = jsonObj.toString();
	System.out.println("infoRegresar ** validaciones "+infoRegresar );
	
}else if (informacion.equals("bitacoraReviciones")){
	JSONArray jsObjArray = new JSONArray();
	String CVESOLICITUD = request.getParameter("CVESOLICITUD")==null?"":request.getParameter("CVESOLICITUD");
	List lsBitacora = OperElec.consultarBitacoraRevision(CVESOLICITUD);
	jsObjArray = JSONArray.fromObject(lsBitacora);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
	
}else if (informacion.equals("archivoBitacora")){
	String IC_CHKLIST_SOLICITUD = request.getParameter("IC_CHKLIST_SOLICITUD")==null?"":request.getParameter("IC_CHKLIST_SOLICITUD");
	try {
		
		String nombreArchivo =	OperElec.obtenChkBitacora (strDirectorioTemp,IC_CHKLIST_SOLICITUD );	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}	
	infoRegresar = jsonObj.toString(); 
	
}


%>

<%=infoRegresar%>