<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,
	java.util.*,
	java.math.*,
	netropology.utilerias.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,		
	com.netro.exception.*,
	com.netro.model.catalogos.*,	
	com.netro.electronica.*,
	org.apache.commons.logging.Log,
	java.text.*, 
	java.io.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf"%>
<%@ include file="../../certificado.jspf" %>
	<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());	
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String fecha_Solicitud = (request.getParameter("fecha_Solicitud")!=null)?request.getParameter("fecha_Solicitud"):fechaHoy;
String importe = (request.getParameter("importe")!=null)?request.getParameter("importe"):"";
String claveMoneda = (request.getParameter("claveMoneda")!=null)?request.getParameter("claveMoneda"):"";
String desRecuersos = (request.getParameter("desRecuersos")!=null)?request.getParameter("desRecuersos"):"";
String fecha_PagoCapital = (request.getParameter("fecha_PagoCapital")!=null)?request.getParameter("fecha_PagoCapital"):"";
String fecha_PagoInteres = (request.getParameter("fecha_PagoInteres")!=null)?request.getParameter("fecha_PagoInteres"):"";
String fecha_vencimiento = (request.getParameter("fecha_vencimiento")!=null)?request.getParameter("fecha_vencimiento"):"";
String cboInteres = (request.getParameter("cboInteres")!=null)?request.getParameter("cboInteres"):"";
String tasaInteres = (request.getParameter("tasaInteres")!=null)?request.getParameter("tasaInteres"):"";
String observaciones = (request.getParameter("observaciones")!=null)?request.getParameter("observaciones"):"";
String  ic_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
String  nombreArchivo = (request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";
String  ic_contrato = (request.getParameter("ic_contrato")!=null)?request.getParameter("ic_contrato"):"";
String  tipoSolicitud = (request.getParameter("tipoSolicitud")!=null)?request.getParameter("tipoSolicitud"):"";
String  _acuse = (request.getParameter("_acuse")!=null)?request.getParameter("_acuse"):"";
String  pantalla = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";
String  ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
String  tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String  extension = (request.getParameter("extension")!=null)?request.getParameter("extension"):"";
String  txtcargaPrenda = (request.getParameter("txtcargaPrenda")!=null)?request.getParameter("txtcargaPrenda"):"";
String  txtcargaPrendaPdf = (request.getParameter("txtcargaPrendaPdf")!=null)?request.getParameter("txtcargaPrendaPdf"):"";

String  txtcargaRUG = (request.getParameter("txtcargaRUG")!=null)?request.getParameter("txtcargaRUG"):"";
String  combTasa = (request.getParameter("combTasa")!=null)?request.getParameter("combTasa"):"";

String infoRegresar ="",  mensajeHorario ="",  esdiaInhabil ="N", mensajeServicio ="", fueradHorario = "N",
horaEnvio = "11:00 AM", horaReenvio ="13:00 PM";

JSONObject jsonObj = new JSONObject();
JSONArray jsonArr = new JSONArray();
JSONArray jsonArrErr = new JSONArray();
JSONArray jsonArrOK = new JSONArray();
int nuErrores =0,  nuCorrectos =0, numOK = 0;
List parametros = new ArrayList();
List parametrosPDF = new ArrayList();
List firmas = new ArrayList();	
HashMap	registros = new HashMap();
HashMap	contratos = new HashMap();
HashMap	datosSolicitud = new HashMap();
HashMap	fechas = new HashMap();	
StringBuffer mensajeModifi = new StringBuffer();
StringBuffer mensajeTasa = new StringBuffer();
boolean horario11  = true, horario13 = true;
if(iTipoAfiliado.equals("I"))  {  	ic_if = iNoCliente;    }
String envio ="", reenvio="";

OperacionElectronica  eletronicaBean = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);

String  lblFondoLiquido =  eletronicaBean.getSaldoLiquido (ic_if);		//Saldo Fondo Liquido
mensajeTasa.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
				" <tr> <td align='justify' class='formas'>"+
				" <br>La presente solicitud de recursos estará sujeta a validación  por parte de Nacional Financiera, S.N.C. para su autorización.  "+
				" </td></tr><table> <br>");
				
if (informacion.equals("valoresIniciales")  ) {
	
	registros = eletronicaBean.getDatosIF (ic_if);// Obtengo los datos del IF
	if(registros.size()>0) {
		envio = registros.get("HORAENVIO").toString();
		reenvio = registros.get("HORAREENVIO").toString();
	}
	
	if(!envio.equals(""))   {  horaEnvio  =envio;  }
	if(!reenvio.equals("")) {  horaReenvio  =reenvio; }	
	
	if(tipoSolicitud.equals("Captura") ) {
		horario11 =eletronicaBean.getValidaHora(horaEnvio);		
	}else  {
		horario13 =eletronicaBean.getValidaHora(horaReenvio); 	
	}
		
	if(!eletronicaBean.getvalidaDiaHabil(fechaHoy)){	
		esdiaInhabil ="S";
		mensajeServicio = "<center><B><H1>Servicio no disponible </center>";
	}			
	if(!horario11  ||  !horario13) {  	
		fueradHorario = "S";			
	}	
		
	if(esdiaInhabil.equals("N")) {
	
		if(fueradHorario.equals("S"))  {
			if(tipoSolicitud.equals("Captura")  && !horario11) {
				mensajeHorario =  "<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
										"<tr> <td align='center' class='formas'> <H1>Esta fuera del horario para el registro de solicitudes con depósito mismo día. </td></tr> "+
										" </table> ";
			}else  if(!tipoSolicitud.equals("Captura")  && !horario13) {
				
				mensajeHorario =  "<table align='center' style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
										" <tr> <td align='center' class='formas'> <H1>Esta fuera del horario para el reenvió de solicitudes con depósito mismo día. </td> </tr> "+								
										" </table> ";
			}
		}else  {	
				mensajeHorario =  "<table align='center' style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
										" <tr> <td align='justify' class='formas'>  <br> <H1>Favor de verificar que la (s) solicitud (es) de recursos cuente (n) con la (s) tasa (s) y condiciones de crédito que correspondan. </td> </tr> "+
										" </table> ";
		}
		
		if(esdiaInhabil.equals("N")  && fueradHorario.equals("N")  ) { 
			firmas =  eletronicaBean.getFirmasModificatorias(ic_if);  //obtengo las firmas Modificatorias
			contratos = eletronicaBean.getContratoXPerfil(strPerfil);// obtengo el tipo de contrato de acuerdo al Perfil 
			
			
			if(tipoSolicitud.equals("Modificar") ) {			
				datosSolicitud = eletronicaBean.getDatosSolicitud( ic_solicitud, ic_if);
			}		
		}
			
		for (int i = 0; i <firmas.size(); i++) {
			HashMap datos = (HashMap)firmas.get(i);		
			String descripcion  = datos.get("DESCRIPCION").toString();
			String fecha  = datos.get("FECHA").toString();
			String lblDescCampo  = "lblDescCampo"+i;		
			String campo =   "	{ "+
				"	xtype: 'displayfield', "+
				"	id: '"+lblDescCampo+"',"+
				"	style: 'text-align:left;',"+
				"	fieldLabel: '&nbsp;&nbsp;"+descripcion+"',"+
				"	value: '&nbsp;&nbsp;"+fecha+"'"+
				"}  ";		
			fechas.put(lblDescCampo,campo);				
		}	
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("strPerfil", strPerfil);
	if(registros.size()>0) {
		jsonObj.put("lblNombreIF","<b>"+registros.get("NOMBRE_IF"));	
		jsonObj.put("lblNumCuenta", "<b>"+registros.get("NUM_CUENTAS"));	
		jsonObj.put("lblBanco", "<b>"+registros.get("BANCO"));	
		jsonObj.put("lblCuentaCLABE","<b>"+ registros.get("CLABE"));		
		jsonObj.put("lblFechaFirmaContrato", registros.get("FECHACONTRATO"));		
		jsonObj.put("lblModificatorios", registros.get("MODIFICATORIOS"));		
		jsonObj.put("lblNomContratoOE", registros.get("NOMBRECONTRATO_OE"));	
		jsonObj.put("lblFechaFirmaContratoOE", registros.get("FECHACONTRATO_OE"));	
	}
	jsonObj.put("lblUsuarioIF", strNombreUsuario);
	jsonObj.put("ic_solicitud", ic_solicitud);	
	jsonObj.put("fecha_Solicitud","<b>"+ fecha_Solicitud);
	jsonObj.put("fechas", fechas);
	jsonObj.put("totalFechas", String.valueOf(firmas.size()));
	jsonObj.put("mensajeHorario", mensajeHorario);
	jsonObj.put("esdiaInhabil", esdiaInhabil);
	jsonObj.put("mensajeServicio", mensajeServicio);
	jsonObj.put("fueradHorario", fueradHorario);
	//MODIFICACIÓN
	jsonObj.put("tipoSolicitud", tipoSolicitud);	
	jsonObj.put("_acuse", datosSolicitud.get("ACUSE"));
	jsonObj.put("importe", datosSolicitud.get("IMPORTE"));	
	jsonObj.put("claveMoneda", datosSolicitud.get("IC_MONEDA"));	
	jsonObj.put("desRecuersos", datosSolicitud.get("RECURSOS"));
	jsonObj.put("fecha_PagoCapital", datosSolicitud.get("FECHACAPITAL"));
	jsonObj.put("fecha_PagoInteres", datosSolicitud.get("FECHAINTERES"));
	jsonObj.put("fecha_vencimiento", datosSolicitud.get("FECHAVENCIMIENTO"));
	jsonObj.put("cboInteres", datosSolicitud.get("CG_TIPO_TASA"));
	jsonObj.put("tasaInteres", datosSolicitud.get("CG_TASA_INTERES"));
	jsonObj.put("observaciones", datosSolicitud.get("OBSERVACIONES"));	
	if(iTipoAfiliado.equals("I"))  {
		jsonObj.put("ic_contrato", contratos.get("IC_CONTRATO"));
		jsonObj.put("lblNomContrato", "<b>"+contratos.get("CG_NOMBRE_CONTRATO"));	
	}else  {
		jsonObj.put("ic_contrato", datosSolicitud.get("IC_CONTRATO"));
		jsonObj.put("lblNomContrato", "<b>"+datosSolicitud.get("NOMBRECONTRATO"));	
	}
	jsonObj.put("lblFondoLiquido", "$"+Comunes.formatoDecimal(lblFondoLiquido, 2));						
	jsonObj.put("combTasa", datosSolicitud.get("CLAVE_TASA"));		
	infoRegresar = jsonObj.toString();	
}else if (informacion.equals("catMonedaData")){
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
	
}else if (informacion.equals("catTasasInteresData")){

  
  CatalogoTasa cat = new CatalogoTasa();
  cat.setCampoClave("ic_tasa");
  cat.setCampoDescripcion("cd_nombre"); 
  cat.setMoneda(claveMoneda);  
  if(claveMoneda.equals("54")  ) {  cat.setValoresCondicionIn("21,22, 23, 24", Integer.class);   } 
  if(claveMoneda.equals("1")  ) { cat.setValoresCondicionIn("20", Integer.class);   } 
  cat.setOrden("ic_tasa");
  infoRegresar = cat.getJSONElementos();	
}else if (informacion.equals("validaMonto")  ) {
	
	String rutaNombreArchivo =(String)application.getAttribute("strDirectorioPublicacion")+"38OperaElectronica/38pki/38if/38MontosSirac.txt";

	Cons_Operaciones paginador = new Cons_Operaciones (); 
	paginador.setRutaNombreArchivo(rutaNombreArchivo);
	HashMap regMontos =  paginador.getSaldos (ic_if);		
	String montoDisponible =   regMontos.get("MONTO_DISPONIBLE").toString();
	String mensajeMonto = "";
	int valor   = new BigDecimal(importe).compareTo(new BigDecimal(montoDisponible) );
	if(valor>0) {	
		mensajeMonto ="El importe de la Solicitud de Recursos es mayor al Monto Disponible";
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensajeMonto",mensajeMonto);
	infoRegresar  = jsonObj.toString();

}else  if (informacion.equals("valoresCargaAmortizacion")){

	String rutaNombreArchivo = strDirectorioTemp +nombreArchivo;
	String  montoSolicitado = (request.getParameter("montoSolicitado")!=null)?request.getParameter("montoSolicitado"):"";
	List resultados  =  new ArrayList();	
	resultados = eletronicaBean.validaTasaAmortizacion(ic_solicitud,  rutaNombreArchivo, montoSolicitado );
	
	List datosError =  new ArrayList();
	List datosOK =  new ArrayList();
	List regOK=  new ArrayList();
	int nuLineasE =0;
	HashMap mapa=new HashMap();
	List datos = new ArrayList();
	

	if(resultados.size()>0){  
		datosError = (ArrayList)resultados.get(0);	 
		datosOK = (ArrayList)resultados.get(1);	
		regOK = (ArrayList)resultados.get(2);	
	}	
	
	// Para el Store de storeBienData
	if(datosOK.size()>0){
		nuCorrectos =datosOK.size();
		for(int x=0; x<datosOK.size(); x++) {	 
			mapa=new HashMap();
			mapa.put("DESCRIPCION",(String)datosOK.get(x));				
			datos.add(mapa);
		}	
		jsonArr = JSONArray.fromObject(datos);
	}	
	
	
	// Para el Store de storeBienData	
	if(datosError.size()>0){
		nuErrores =datosError.size();
		datos = new ArrayList();
		for(int x=0; x<datosError.size(); x++) {	 
			List infor = (ArrayList)datosError.get(x);			
			for(int y=0; y<infor.size(); y++) {
				mapa=new HashMap();
				mapa.put("DESCRIPCION",(String)infor.get(y));				
				datos.add(mapa);
			}
		}	
		jsonArrErr = JSONArray.fromObject(datos);
	}	
	
	// Para el Store de storeRegistrosOKData	
	
	if(regOK.size()>0){
		numOK =regOK.size();
		datos = new ArrayList();
		for(int x=0; x<regOK.size(); x++) {	 
			List infor = (ArrayList)regOK.get(x);		
				mapa=new HashMap();
				mapa.put("PERIODO",(String)infor.get(0));
				mapa.put("FECHA",(String)infor.get(1));	
				mapa.put("AMORTIZACION",(String)infor.get(2));	
				datos.add(mapa);
			
		}	
		jsonArrOK = JSONArray.fromObject(datos);
	}	
	
	
}else  if (informacion.equals("guardartablaAmortizacionTMP")){

	String periodo[] = request.getParameterValues("periodo");
	String fecha[] = request.getParameterValues("fecha");
	String amortizacion[] = request.getParameterValues("amortizacion");
	String hayAmortizacion = "N";
	
	hayAmortizacion =  	eletronicaBean.guardaTasaAmortizacionTMP(ic_solicitud,  periodo,  fecha, amortizacion) ;
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("hayAmortizacion",hayAmortizacion);
	infoRegresar  = jsonObj.toString();
	
}else  if (informacion.equals("ArchivosSolic")){

	nombreArchivo =  eletronicaBean.consArchCotizacion(strDirectorioTemp, ic_solicitud, pantalla,  tipoArchivo,  extension , tipoSolicitud );

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar  = jsonObj.toString();			
	

}else  if (informacion.equals("Carga_PreAcuse")){

	parametros = new ArrayList();	
	parametros.add(ic_if);
	parametros.add(importe);
	parametros.add(claveMoneda);
	parametros.add(desRecuersos);
	parametros.add(fecha_PagoCapital);
	parametros.add(fecha_PagoInteres);
	parametros.add(fecha_vencimiento);
	parametros.add(cboInteres);
	parametros.add(tasaInteres);
	parametros.add(observaciones);
	parametros.add(ic_solicitud);
	parametros.add(ic_contrato);
	parametros.add(fecha_Solicitud);
	parametros.add(combTasa);
	parametros.add(iNoUsuario);
		
	try{
	
		eletronicaBean.cargaSolicTMP(parametros);
	
		registros = eletronicaBean.getDatosSolicTMP(ic_solicitud, ic_if); // Obtengo los datos de la solicitud 
		firmas =  eletronicaBean.getFirmasModificatorias(ic_if);
		mensajeModifi = new StringBuffer();
		
		if( ( strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP") )  && ( pantalla.equals("Prenda")  || pantalla.equals("RUG")|| pantalla.equals("Prenda_Pdf")  ) ) {
			if(txtcargaPrenda.equals("S")  &&  pantalla.equals("Prenda")  || (txtcargaPrendaPdf.equals("S")  &&  pantalla.equals("Prenda_Pdf")) )  {
				mensajeModifi = new StringBuffer();
				
				mensajeModifi.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
				"<tr> <td align='justify' class='formas'>"+
				" <BR>Bajo protesta de decir verdad, "+strNombre + " manifiesta que la información que envía en archivo electrónico mediante la herramienta 'Carga Cartera en Prenda', corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN "+
				" que garantiza la presente disposición de recursos del crédito, formalizada en términos del Contrato de Línea de Crédito en Cuenta Corriente celebrado con NAFIN. "+
				"</td></tr><table> <br>");
				
				/* 01/03/2017 IHJ
				mensajeModifi.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
				"<tr> <td align='justify' class='formas'>"+
				" <BR>Bajo protesta de decir verdad, "+strNombre + " manifiesta que la información que envía en archivo electrónico mediante la herramienta 'Carga Cartera en Prenda' corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN  "+
				" que garantiza la presente disposición de recursos del crédito. En términos del Contrato de Línea de Crédito en Cuenta Corriente celebrado con NAFIN, la ACREDITADA se obliga a enviar a NAFIN ,de manera física y debidamente firmada, la información "+
				" correspondiente a la cartera crediticia otorgada en prenda a favor de NAFIN, dentro de los siguientes 5 días hábiles posteriores a la presente solicitud electrónica de disposición de recursos."+
				"</td></tr><table> <br>");
				*/
				
			}else  if(txtcargaRUG.equals("S") && pantalla.equals("RUG")  )  {
				mensajeModifi = new StringBuffer();			
				mensajeModifi.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
				"<tr> "+
				"<td align='justify' class='formas'>"+
				" <BR>  Bajo protesta de decir verdad, "+strNombre + "  manifiesta que la documentación en formato PDF que se envía es copia fiel de la original de la formalización e inscripción de la prenda sobre su cartera crediticia que otorga a favor de NAFIN, en términos del "+
				" Contrato de Línea de Crédito en Cuenta Corriente celebrado con NAFIN. Dicha información será enviada a NAFIN, de manera física, dentro de los siguientes 5 días hábiles posteriores a la presente solicitud electrónica de disposición de recursos."+
				" </td></tr><table> <br>");
			}				
		}
		if(pantalla.equals("PreAcuse") ) {
			mensajeModifi = new StringBuffer();
			mensajeModifi.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
					"<tr> "+
					"<td align='justify' class='formas'>"+
					" La presente solicitud de recursos se realiza al amparo del "+registros.get("NOMBRECONTRATO")+" con fecha de firma "+registros.get("FECHACONTRATO"));
				int x = 1;
				if( firmas.size()>0){
					mensajeModifi.append(" asi como,   ");
					for (int i = 0; i <firmas.size(); i++) {
						HashMap datos = (HashMap)firmas.get(i);												
						mensajeModifi.append(" Contrato Modificatorio "+x+" con fecha de firma " +datos.get("FECHA").toString() + ", ");										
						x++;
					}
				}
				mensajeModifi.append(" y el Contrato de Operación Electrónica con fecha de firma  "+registros.get("FECHACONTRATO_OE")+
				", celebrados entre  "+registros.get("NOMBRE_IF")+"  y Nacional Financiera, S.N.C. <BR>"+
				"<BR>Con fundamento en lo dispuesto por los artículos 1205 y 1298-A del Código de Comercio y del 52 de la ley de Instituciones de Crédito,  la información e instrucciones que el Intermediario y Nacional Financiera, S.N.C. se transmitan o comuniquen mutuamente mediante el sistema, tendrán pleno valor probatorio y fuerza legal para acreditar la operación realizada, el importe de la misma, su naturaleza; así como, las características y alcance de sus instrucciones."+
				"<BR> "+
				"</td></tr>");
				
				if(  strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP") ) {
					
					mensajeModifi.append("<tr> <td align='justify' class='formas'>"+					
					" <BR>Bajo protesta de decir verdad, " +registros.get("NOMBRE_IF") +" manifiesta que la información que envía en archivo electrónico mediante la herramienta 'Carga Cartera en Prenda', corresponde a la cartera crediticia "+
					"otorgada en prenda a favor de NAFIN que garantiza la presente disposición de recursos del crédito, formalizada en términos del Contrato de Línea de Crédito en Cuenta Corriente celebrado con NAFIN."+
					"</td></tr>");
					
					/* 01/03/2017 IHJ
					mensajeModifi.append("<tr> <td align='justify' class='formas'>"+					
					" <BR>Bajo protesta de decir verdad, " +registros.get("NOMBRE_IF") +" manifiesta que la información que envía en archivo electrónico mediante la herramienta 'Carga Cartera en Prenda' corresponde a la cartera crediticia"+
					"otorgada en prenda a favor de NAFIN  que garantiza la presente disposición de recursos del crédito. En términos del Contrato de Línea de Crédito en Cuenta Corriente celebrado con NAFIN, la ACREDITADA se obliga a enviar a NAFIN , "+
					"de manera física y debidamente firmada, la información correspondiente a la cartera crediticia otorgada en prenda a favor de NAFIN, dentro de los siguientes 5 días hábiles posteriores a la presente solicitud electrónica de disposición de recursos."+
					"</td></tr>");					
					*/
				}
				
				mensajeModifi.append("<table> <BR> ");
		}	
		
		String  montoMoneda= "<table align='center'  style='text-align: justify;' border='0' width='800' cellpadding='0' cellspacing='0'> "+
				"<tr> <td align='center' class='formas'> <h1> Importe:  <b>"+ "$"+Comunes.formatoDecimal(registros.get("IMPORTE"),2 )+"</td></tr> "+
				"<tr> <td align='center' class='formas'> <h1> Moneda:  <b>"+ registros.get("MONEDA")+"</td></tr>"+
				"<table> <br>";							
			
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("montoMoneda",montoMoneda );
		jsonObj.put("lblFecha_Solicitud", "<b>"+registros.get("FECHASOLICITUD"));
		jsonObj.put("lblNombreIFPre", "<b>"+ registros.get("NOMBRE_IF"));
		jsonObj.put("lblNumCuentaPre", "<b>"+ registros.get("NUM_CUENTAS"));
		jsonObj.put("lblBancoPre", "<b>"+ registros.get("BANCO"));
		jsonObj.put("lblCuentaCLABPre", "<b>"+ registros.get("CLABE"));
		
		jsonObj.put("lblDesRecuersos", registros.get("RECURSOS"));
		jsonObj.put("lblFecha_PagoCapital", registros.get("FECHACAPITAL"));
		jsonObj.put("lblFecha_PagoInteres", registros.get("FECHAINTERES"));
		jsonObj.put("lblFecha_vencimiento", registros.get("FECHAVENCIMIENTO"));		
		jsonObj.put("lblTasaInteres", "&nbsp;&nbsp;"+registros.get("TASAINTERES"));		
		jsonObj.put("lblObservaciones", registros.get("OBSERVACIONES"));
		jsonObj.put("lblUsuarioIFPreAcuse", strNombreUsuario);
		jsonObj.put("mensajeModifi", mensajeModifi.toString());
		jsonObj.put("mensajeTasa", mensajeTasa.toString());
		jsonObj.put("pantalla", pantalla);		
		
	} catch(Exception ex) {
		System.out.println("La Exception: "+ex.toString()); 
		jsonObj.put("success", new Boolean(false));
	}	
	infoRegresar  = jsonObj.toString();


}else  if (informacion.equals("Guarda_Solicitud")){

	String nombreUsuario = iNoUsuario+" - "+strNombreUsuario;
	String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
	String externContent = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
	String hidFechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());	
	String descripMoneda = (request.getParameter("descripMoneda")!=null)?request.getParameter("descripMoneda"):"";
	String icSolicitud ="";
	ArrayList parametrosG = new ArrayList();
	Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
	String folioCert  ="", mensajeError  = "", mensajeExito ="";
	char getReceipt = 'Y';
	
	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		folioCert = acuse.toString();			
		Seguridad s = new Seguridad();
				
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			if(tipoSolicitud.equals("Captura") ) {
				_acuse = s.getAcuse();
			}
			parametrosG = new ArrayList();	
			parametrosG.add(ic_solicitud);
			parametrosG.add(nombreUsuario);
			parametrosG.add(hidFechaCarga);
			parametrosG.add(ic_if);
			parametrosG.add(_acuse);
			parametrosG.add(tipoSolicitud);
			parametrosG.add(combTasa);
			parametrosG.add(importe);
			parametrosG.add(claveMoneda);
			parametrosG.add(desRecuersos);
			parametrosG.add(fecha_PagoCapital);
			parametrosG.add(fecha_PagoInteres);
			parametrosG.add(fecha_vencimiento);
			parametrosG.add(cboInteres);
			parametrosG.add(tasaInteres);
			parametrosG.add(observaciones);					
			parametrosG.add(ic_contrato);
			parametrosG.add(fecha_Solicitud);	
			parametrosG.add(iNoUsuario);
			parametrosG.add(lblFondoLiquido);	
			parametrosG.add(strPerfil);//20
			icSolicitud =  eletronicaBean.guardaSolicitud1(
				parametrosG,
				ic_if,
				strDirectorioTemp,
				(String)session.getAttribute("strPais"),
				iNoNafinElectronico,
				strNombre,
				strNombreUsuario,
				strDirectorioPublicacion,
				lblFondoLiquido,
				strPerfil
			);
			if(!icSolicitud.equals("")){
				mensajeExito = " <br>La autentificación no se llevo acabo con éxito"+"<br>";
			}
			registros = eletronicaBean.getDatosSolicitud( icSolicitud, ic_if); //consulta
									
			mensajeExito = "<br>La autentificación se llevó a cabo con éxito <br> Acuse de Solicitud: "+_acuse+"<br>";
		}else  {
			mensajeError = "<br>La autentificación no se llevo acabo con éxito"+s.mostrarError()+"<br>";
		}
	}else  {
		mensajeError = " <br>La autentificación no se llevo acabo con éxito"+"<br>";
	}
	
	
			firmas =  eletronicaBean.getFirmasModificatorias(ic_if);
			mensajeModifi = new StringBuffer();
			mensajeModifi.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
					"<tr> "+
					"<td align='justify' class='formas'>"+
					" La presente solicitud de recursos se realiza al amparo del "+registros.get("NOMBRECONTRATO")+" con fecha de firma "+registros.get("FECHACONTRATO"));
				int x = 1;
				if( firmas.size()>0){
					mensajeModifi.append(" asi como,   ");
					for (int i = 0; i <firmas.size(); i++) {
						HashMap datos = (HashMap)firmas.get(i);												
						mensajeModifi.append(" Contrato Modificatorio "+x+" con fecha de firma " +datos.get("FECHA").toString() + ", ");										
						x++;
					}
				}
				mensajeModifi.append(" y el Contrato de Operación Electrónica con fecha de firma "+registros.get("FECHACONTRATO_OE")+
				", celebrados entre  "+registros.get("NOMBRE_IF")+"  y Nacional Financiera, S.N.C. <BR>"+
				"<BR>Con fundamento en lo dispuesto por los artículos 1205 y 1298-A del Código de Comercio y del 52 de la ley de Instituciones de Crédito,  la información e instrucciones que el Intermediario y Nacional Financiera, S.N.C. se transmitan o comuniquen mutuamente mediante el sistema, tendrán pleno valor probatorio y fuerza legal para acreditar la operación realizada, el importe de la misma, su naturaleza; así como, las características y alcance de sus instrucciones."+
				"<BR> "+
				"</td></tr>");
				
				if(  strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP") ) {
					
					mensajeModifi.append("<tr> <td align='justify' class='formas'>"+
					" <BR> Bajo protesta de decir verdad, " +registros.get("NOMBRE_IF") +" manifiesta que la información que envía en archivo electrónico mediante la herramienta 'Carga Cartera en Prenda' corresponde a la cartera"+
					"crediticia otorgada en prenda a favor de NAFIN  que garantiza la presente disposición de recursos del crédito. En términos del Contrato de Línea de Crédito en Cuenta Corriente celebrado con NAFIN."+
					" </td></tr>");	
					
					/* 21/03/2017 IHJ
					mensajeModifi.append("<tr> <td align='justify' class='formas'>"+
					" <BR> Bajo protesta de decir verdad, " +registros.get("NOMBRE_IF") +" manifiesta que la información que envía en archivo electrónico mediante la herramienta 'Carga Cartera en Prenda' corresponde a la cartera"+
					"crediticia otorgada en prenda a favor de NAFIN  que garantiza la presente disposición de recursos del crédito. En términos del Contrato de Línea de Crédito en Cuenta Corriente celebrado con NAFIN, la ACREDITADA se obliga a "+
					"enviar a NAFIN ,de manera física y debidamente firmada, la información correspondiente a la cartera crediticia otorgada en prenda a favor de NAFIN, dentro de los siguientes 5 días hábiles posteriores a la presente solicitud electrónica de disposición de recursos."+
					" </td></tr>");	
					*/
					
				}
				
				mensajeModifi.append("<table> <BR> ");
		
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("mensajeModifiAcuse", mensajeModifi.toString());
	jsonObj.put("importe", "$"+Comunes.formatoDecimal(importe,2) );
	jsonObj.put("descripMoneda", registros.get("MONEDA"));
	
	jsonObj.put("hidHoraCarga", registros.get("HORACARGA"));
	
	jsonObj.put("mensajeError", mensajeError);
	jsonObj.put("mensajeExito", mensajeExito);
	jsonObj.put("lblFecha_SolicitudAcuse", "<b>"+registros.get("FECHASOLICITUD"));
	jsonObj.put("lblNombreIFAcuse", "<b>"+ registros.get("NOMBRE_IF"));
	jsonObj.put("lblNumCuentaAcuse", "<b>"+ registros.get("NUM_CUENTAS"));
	jsonObj.put("lblBancoAcuse", "<b>"+ registros.get("BANCO"));
	jsonObj.put("lblCuentaCLABEAcuse", "<b>"+ registros.get("CLABE"));	
	jsonObj.put("pantalla", "Acuse");
	
	jsonObj.put("ic_solicitud", icSolicitud);
	jsonObj.put("nombreUsuario", nombreUsuario);	
	jsonObj.put("hidFechaCarga", hidFechaCarga);
	jsonObj.put("ic_if", ic_if);
	jsonObj.put("_acuse", _acuse);
	jsonObj.put("tipoSolicitud", tipoSolicitud);
	jsonObj.put("combTasa", combTasa);
	jsonObj.put("importe", importe);
	jsonObj.put("claveMoneda", claveMoneda);
	jsonObj.put("desRecuersos", desRecuersos);
	jsonObj.put("fecha_PagoCapital", fecha_PagoCapital);
	jsonObj.put("fecha_PagoInteres", fecha_PagoInteres);
	jsonObj.put("fecha_vencimiento", fecha_vencimiento);
	jsonObj.put("cboInteres", cboInteres);
	jsonObj.put("tasaInteres", tasaInteres);
	jsonObj.put("observaciones", observaciones);
	jsonObj.put("ic_contrato", ic_contrato);
	jsonObj.put("fecha_Solicitud", fecha_Solicitud);
	jsonObj.put("iNoUsuario", iNoUsuario);
	jsonObj.put("lblFondoLiquido", lblFondoLiquido);
	jsonObj.put("strPerfil", strPerfil);
	
	infoRegresar  = jsonObj.toString();
	
}else  if (informacion.equals("ImprimirAcuse")){   
			
	parametros.add(strDirectorioTemp);
	parametros.add(ic_solicitud);
	parametros.add(ic_if);
	parametros.add((String)session.getAttribute("strPais"));
	parametros.add(iNoNafinElectronico);
	parametros.add(strNombre);
	parametros.add(strNombreUsuario);
	parametros.add(strLogo);
	parametros.add(strDirectorioPublicacion);	
	parametros.add(lblFondoLiquido);
	parametros.add(strPerfil);
	parametros.add(strTipoUsuario);
		
	nombreArchivo =  eletronicaBean.generarArchivoPDF(parametros);

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar  = jsonObj.toString();	
	

}else  if (informacion.equals("enviar_correo")){
	String ic_solicitud1 = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
	String nombreUsuario1 = (request.getParameter("nombreUsuario")!=null)?request.getParameter("nombreUsuario"):"";
	String hidFechaCarga1 = (request.getParameter("hidFechaCarga")!=null)?request.getParameter("hidFechaCarga"):"";
	String ic_if1 = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
	String _acuse1 = (request.getParameter("_acuse")!=null)?request.getParameter("_acuse"):"";
	String tipoSolicitud1 = (request.getParameter("tipoSolicitud")!=null)?request.getParameter("tipoSolicitud"):"";
	String combTasa1 = (request.getParameter("combTasa")!=null)?request.getParameter("combTasa"):"";
	String importe1 = (request.getParameter("importe")!=null)?request.getParameter("importe"):"";
	String claveMoneda1 = (request.getParameter("claveMoneda")!=null)?request.getParameter("claveMoneda"):"";
	String desRecuersos1 = (request.getParameter("desRecuersos")!=null)?request.getParameter("desRecuersos"):"";
	String fecha_PagoCapital1 = (request.getParameter("fecha_PagoCapital")!=null)?request.getParameter("fecha_PagoCapital"):"";
	String fecha_PagoInteres1 = (request.getParameter("fecha_PagoInteres")!=null)?request.getParameter("fecha_PagoInteres"):"";
	String fecha_vencimiento1 = (request.getParameter("fecha_vencimiento")!=null)?request.getParameter("fecha_vencimiento"):"";
	String cboInteres1 = (request.getParameter("cboInteres")!=null)?request.getParameter("cboInteres"):"";
	String tasaInteres1 = (request.getParameter("tasaInteres")!=null)?request.getParameter("tasaInteres"):"";
	String observaciones1 = (request.getParameter("observaciones")!=null)?request.getParameter("observaciones"):"";
	String ic_contrato1 = (request.getParameter("ic_contrato")!=null)?request.getParameter("ic_contrato"):"";
	String fecha_Solicitud1 = (request.getParameter("fecha_Solicitud")!=null)?request.getParameter("fecha_Solicitud"):"";
	String iNoUsuario1 = (request.getParameter("iNoUsuario")!=null)?request.getParameter("iNoUsuario"):"";
	String lblFondoLiquido1 = (request.getParameter("lblFondoLiquido")!=null)?request.getParameter("lblFondoLiquido"):"";
	String strPerfil1 = (request.getParameter("strPerfil")!=null)?request.getParameter("strPerfil"):"";
	ArrayList parametrosG = new ArrayList();
	boolean reCorreo = true;
	parametrosG = new ArrayList();	
	parametrosG.add(ic_solicitud1);
	parametrosG.add(nombreUsuario1);
	parametrosG.add(hidFechaCarga1);
	parametrosG.add(ic_if1);
	parametrosG.add(_acuse1);
	parametrosG.add(tipoSolicitud1);
	parametrosG.add(combTasa1);
	parametrosG.add(importe1);
	parametrosG.add(claveMoneda1);
	parametrosG.add(desRecuersos1);
	parametrosG.add(fecha_PagoCapital1);
	parametrosG.add(fecha_PagoInteres1);
	parametrosG.add(fecha_vencimiento1);
	parametrosG.add(cboInteres1);
	parametrosG.add(tasaInteres1);
	parametrosG.add(observaciones1);					
	parametrosG.add(ic_contrato1);
	parametrosG.add(fecha_Solicitud1);	
	parametrosG.add(iNoUsuario1);
	parametrosG.add(lblFondoLiquido1);	
	parametrosG.add(strPerfil1);
	
	if(!ic_solicitud1.equals("")){
		reCorreo = eletronicaBean.envioCorreoAux(parametrosG, ic_solicitud1 ); //envio de correo 
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar  = jsonObj.toString();			
	

}

%>


<%if (informacion.equals("valoresCargaAmortizacion")){ %>

 {
	 "success": true,
	 "todOK":{
	 "registros":<%=jsonArr.toString()%>,
	 "total":<%=nuCorrectos%>},
	 "todError":{
	 "registros":<%=jsonArrErr.toString()%>,
	 "total":<%=nuErrores%>},		 
	 "todRegOK":{
	 "registros":<%=jsonArrOK.toString()%>,
	 "total":<%=numOK%>},	 
	 "nuErrores":"<%=nuErrores%>"
 }
 
 <%}else { %>
 <%=infoRegresar%>
 
 <%}%>