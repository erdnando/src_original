<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.math.*, 
	java.text.*, 
	java.sql.*,
	netropology.utilerias.*,		
	java.text.SimpleDateFormat,
	java.util.Date,		
	com.netro.exception.*,
	com.netro.model.catalogos.*,	
	com.netro.electronica.*,
	org.apache.commons.logging.Log,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf"%>
<%@ include file="../../certificado.jspf" %>
	<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
String fechaRecepcionIni = (request.getParameter("fechaRecepcionIni")!=null)?request.getParameter("fechaRecepcionIni"):"";
String fechaRecepcionFin = (request.getParameter("fechaRecepcionFin")!=null)?request.getParameter("fechaRecepcionFin"):"";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";


String infoRegresar ="", consulta = "", mensajeMontoMaximo = "", validaMontoMax ="N";
JSONObject jsonObj = new JSONObject();
JSONArray jsonArrErr = new JSONArray();
JSONArray jsonArrOK = new JSONArray();
JSONArray jsonArrResumen = new JSONArray();
JSONArray jsonValidaDatos = new JSONArray();
StringBuffer mensajeModifi = new StringBuffer();
int nuErrores =0,  nuCorrectos =0, nuValiDatos = 0, nuErroresVm =0;

OperacionElectronica  eletronicaBean = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);

ConsRevSolRecursos pag = new ConsRevSolRecursos();
pag.setNumSolic(no_solicitud);
pag.setFecRecpIni(fechaRecepcionIni);
pag.setFecRecpFin(fechaRecepcionFin);
CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( pag);

if (informacion.equals("Consultar")){
	
	Registros reg	=	queryHelper.doSearch();
	String userIF = "";
	String nombreUserIF = "";
	while(reg.next()){
		String FechasModificatorias = pag.getFirmasModificatorias (reg.getString("CVEIF")) ;
		userIF = reg.getString("usuarioif")==null?"":reg.getString("usuarioif");
		reg.setObject("ALLFECMODCONT",FechasModificatorias);	
		if(userIF!=null && !"".equals(userIF)){
			nombreUserIF = (String)(eletronicaBean.obtieneInfoCorreo(userIF)).get("USUARIO_NOMBRE");
		}
		reg.setObject("NOMBREUSERIF",nombreUserIF);	
	}	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();  
	
}else if (informacion.equals("obtenerArchivo")){
	String cveSolicitud = request.getParameter("cveSolicitud");
	String tipoArchivo = request.getParameter("tipoArchivo");
	String  nombreArchivo = eletronicaBean.obtieneArchivoSolic(strDirectorioTemp, cveSolicitud, tipoArchivo);
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
	
}else  if ( informacion.equals("Confirmar") ) {//Confirmar
	String num_acuse		=	(request.getParameter("num_acuse") == null)?"":request.getParameter("num_acuse");
	String cveSolicitud	=	(request.getParameter("cveSolicitud") == null)?"":request.getParameter("cveSolicitud");
	String pkcs7		=	(request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
	String TextoFirmado		=	(request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
	String ic_estatus		=	(request.getParameter("ic_estatus")==null)?"":request.getParameter("ic_estatus");
	String causas_rechazo		=	(request.getParameter("causas_rechazo")==null)?"":request.getParameter("causas_rechazo");
	List parametros = new ArrayList();
	String hidFechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
	char 	getReceipt = 'Y';
	Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
	String folioCert  ="", mensajeError  = "", mensajeExito ="";
	if (!_serial.equals("") && !TextoFirmado.equals("") && !pkcs7.equals("")) {
		folioCert = acuse.toString();			
		Seguridad s = new Seguridad();
		if (s.autenticar(folioCert, _serial, pkcs7, TextoFirmado, getReceipt)) {
			boolean bOkActualiza = true;
			try {
						HashMap hmInfoGerConc = eletronicaBean.obtieneInfoDeFirmaMancom(iNoCliente, iNoUsuario);
						parametros = new ArrayList();
						parametros.add(iNoUsuario+" - "+strNombreUsuario);//0
						parametros.add((String)hmInfoGerConc.get("NUM_AUTORIZADOR"));//1
						parametros.add((String)hmInfoGerConc.get("OPERADOR_USUARIO"));//2
						parametros.add((String)hmInfoGerConc.get("OPERADOR_NOMBRE"));//3
						parametros.add((String)hmInfoGerConc.get("OPERADOR_MAIL"));//4
						parametros.add((String)hmInfoGerConc.get("OPERADOR_TELEFONO"));//5
						parametros.add(iNoCliente);//6
						parametros.add(iNoUsuario);//7
						parametros.add(strPerfil);//8
						parametros.add(hidFechaCarga);//9
						String Num_Autorizador = (String)hmInfoGerConc.get("NUM_AUTORIZADOR");
						String Autorizador = (String)hmInfoGerConc.get("AUTORIZADOR");
						if("Eliminar".equals(operacion)){
							parametros.add(causas_rechazo);//10
							parametros.add((String)hmInfoGerConc.get("NUM_AUTORIZADOR"));//11
							parametros.add((String)hmInfoGerConc.get("AUTORIZADOR"));//12
							parametros.add(ic_estatus);//13
							if(Num_Autorizador.equals("1")||Num_Autorizador.equals("2") ){
								if(Autorizador.equals("1")){
									bOkActualiza = eletronicaBean.actualizaEstatusManto(iNoCliente,num_acuse,"6", cveSolicitud,"1",iNoUsuario,causas_rechazo,parametros,operacion );
									if(bOkActualiza==true){
										mensajeExito = "La solicitud fue rechazada";
									}
								}
								if(Autorizador.equals("2")){
									bOkActualiza = eletronicaBean.actualizaEstatusManto(iNoCliente,num_acuse,"6", cveSolicitud,"2",iNoUsuario,causas_rechazo,parametros,operacion );
									if(bOkActualiza==true){
										mensajeExito = "La solicitud fue rechazada";
									}
								}
							}/*else if(Num_Autorizador.equals("2")){
								bOkActualiza = eletronicaBean.actualizaEstatusManto(iNoCliente,num_acuse,"6", cveSolicitud,"1",iNoUsuario,causas_rechazo,parametros,operacion );
								if(bOkActualiza==true){
										mensajeExito = "La solicitud fue rechazada";
								}
								if(Autorizador.equals("1")){
									if(ic_estatus.equals("7")){
										bOkActualiza = eletronicaBean.actualizaEstatusManto(iNoCliente,num_acuse,"6", cveSolicitud,"1",iNoUsuario,causas_rechazo,parametros,operacion );
										if(bOkActualiza==true){
											mensajeExito = "La solicitud fue rechazada";
										}
									}
								}else if(Autorizador.equals("2")){
									if(ic_estatus.equals("8")){
										bOkActualiza = eletronicaBean.actualizaEstatusManto(iNoCliente,num_acuse,"6", cveSolicitud,"2",iNoUsuario,causas_rechazo,parametros,operacion );
										if(bOkActualiza==true){
											mensajeExito = "La solicitud fue rechazada";
										}
									}
								}
							}*/
						}
						if("Generar".equals(operacion)){
							parametros.add((String)hmInfoGerConc.get("NUM_AUTORIZADOR"));//10
							parametros.add((String)hmInfoGerConc.get("AUTORIZADOR"));//11
							parametros.add(ic_estatus);//12
							if(Num_Autorizador.equals("1")){
								if(ic_estatus.equals("7")&&Autorizador.equals("1")){
									bOkActualiza = eletronicaBean.actualizaEstatusManto(iNoCliente,num_acuse,"1", cveSolicitud,"1",iNoUsuario,"",parametros,operacion);
									if(bOkActualiza==true){
										mensajeExito = "La solicitud fue autorizada";
									}
								}
							}else if(Num_Autorizador.equals("2")){  
								if(Autorizador.equals("1")){
									if(ic_estatus.equals("7")){
										bOkActualiza = eletronicaBean.actualizaEstatusManto(iNoCliente,num_acuse,"8", cveSolicitud,"1",iNoUsuario,"",parametros,operacion);
										if(bOkActualiza==true){
											mensajeExito = "La solicitud fue autorizada";
										}
									}
								}else if(Autorizador.equals("2")){
									if(ic_estatus.equals("8")){
										bOkActualiza = eletronicaBean.actualizaEstatusManto(iNoCliente,num_acuse,"1", cveSolicitud,"2",iNoUsuario,"",parametros,operacion);
										if(bOkActualiza==true){
											mensajeExito = "La solicitud fue autorizada";
										}
									}
								}
							}
					}
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("mensaje",mensajeExito);
				} catch(Exception e) {
					bOkActualiza = false;
					String _error = s.mostrarError();
					jsonObj.put("success", new Boolean(false));
					jsonObj.put("msg", "Error al actualizar. " + _error + ".<br>Proceso CANCELADO");
				}
		
		}else  {
			mensajeError = "<br>La autentificación no se llevo acabo con éxito"+s.mostrarError()+"<br>";
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", mensajeError + ".<br>Proceso CANCELADO");
		}
	}else  {
		mensajeError = " <br>La autentificación no se llevo acabo con éxito"+"<br>";
	}
	infoRegresar = jsonObj.toString();
}
	
%>
 <%=infoRegresar%>
<%!
%>