Ext.onReady(function() {
	var typeClose = 'N';
	var strPerfil = Ext.getDom('auxStrPerfil').value;
	var iNoCliente = Ext.getDom('auxiNoCliente').value;
	var iNoUsuario = Ext.getDom('auxiNoUsuario').value;
	var form = {num_acuse:null, clave_solicitud:null,estatus:null, textoFirmar:null, pkcs7:null, causa:null}
	var processSuccessFailureObtieneArchivo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var archivo = resp.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else {
			NE.util.mostrarConnError(response,opts);   
		}
	};
	function regresar(pagina){
		window.location = pagina;
	}
	function processResult(res){        
		if (res.toLowerCase() == 'ok' || res.toLowerCase() == 'yes'){
			regresar('38ReviSolRecursos.jsp');
		}
	}
	function procesarComfirma(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var mensaje = resp.mensaje;
			if(mensaje!=""){
				Ext.Msg.show({
					title:	'Mensaje',
					msg:		'La Solicitud fue autorizada',
					buttons:	Ext.Msg.OK,
					fn: processResult,
					closable:false,
					icon: Ext.MessageBox.INFO
				});
			}
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarRegRechazado(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var mensaje = resp.mensaje;
			if(mensaje!=""){
				Ext.Msg.show({
					title:	'Mensaje',
					msg:		mensaje,
					buttons:	Ext.Msg.OK,
					fn: processResult,
					closable:false,
					icon: Ext.MessageBox.INFO
				});
			}
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var confirmar = function(pkcs7, textoFirmar){
	
		if (Ext.isEmpty(pkcs7)) {   
			return;	//Error en la firma. Termina...
		}else  {
			pnl.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '38ReviSolRecursos.data.jsp',
				params: Ext.apply({
					informacion: 'Confirmar',
					num_acuse :form.num_acuse,
					cveSolicitud : form.clave_solicitud,
					Pkcs7 : pkcs7,
					TextoFirmado : textoFirmar,
					ic_estatus : form.estatus,
					operacion :'Generar'
				}),
				callback: procesarComfirma
			});
		}
	
	}
	
	var confirmarRechazo = function(vpkcs7, vtextoFirmar, vform){
		vform.pkcs7 = vpkcs7;
		if (Ext.isEmpty(vform.pkcs7)) {
			return;	//Error en la firma. Termina...
		}else  {
			pnl.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '38ReviSolRecursos.data.jsp',
				params: Ext.apply({
					informacion: 'Confirmar',
					num_acuse :vform.num_acuse,
					cveSolicitud : vform.clave_solicitud,
					Pkcs7 : vform.pkcs7,
					TextoFirmado : vform.textoFirmar,
					ic_estatus : vform.estatus,
					operacion :'Eliminar',
					causas_rechazo : vform.causa
				}),
				callback: procesarRegRechazado
			});
		}
	
	}
	
	var procesarAutorizar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var CCACUSE = registro.get('CCACUSE');  
		var CVESOLICITUD = registro.get('CVESOLICITUD'); 
		var ESTATUSSOLIC = registro.get('ESTATUSSOLIC'); 
		form.num_acuse = registro.get('CCACUSE');  
		form.clave_solicitud = registro.get('CVESOLICITUD');   
		form.estatus =  registro.get('ESTATUSSOLIC'); 
		form.textoFirmar='No. Solicitud|Fecha y hora env�o|Nombre Contrato|Fecha Firma Contrato|Contratos Modificatorios|Fecha Firma Modificatorios|Nombre Contrato OE|Fecha Firma Contrato Operaci�n Electr�nica|Importe|Moneda|Destino de los Recursos|Fecha primer pago de Capital|Fecha primer pago de Intereses|Fecha de Vencimiento|Tipo de Tasa|Tasa|Valor|Estatus|Observaciones Intermediario Operador|\n';
		form.textoFirmar+= registro.get('CCACUSE')+"|"+registro.get('FECSOLICITUD')+"|"+registro.get('NOMBRECONTRATO')+"|"+registro.get('FECFIRMACONTRATO')+"|"+registro.get('COUNTMODIF');
		form.textoFirmar+= registro.get('ALLFECMODCONT')+"|"+ registro.get('NOMBRECONTRATOOE')+"|"+registro.get('FECFIRMACONTRATOOE')+"|"+registro.get('IMPORTESOLIC')+"|"+registro.get('NOMBREMONEDA');
		form.textoFirmar+= registro.get('DESTINORECURSO')+"|"+ registro.get('FECPAGOCAPITAL')+"|"+registro.get('FECPAGOINTERES')+"|"+registro.get('FECVENCIMIENTO')+"|"+registro.get('TIPOTASA');
		form.textoFirmar+= registro.get('NOMBRETASA')+"|"+registro.get('TASAINTERES')+"|"+registro.get('NOMBREESTATUS')+"|"+registro.get('OBSERVIF')+"|"+registro.get('CAUSA_RECHAZO');
		Ext.Msg.show({
			title: 'Confirmaci�n',   
			msg: '�Confirma autorizar la Solicitud de Recursos?',
			buttons: Ext.Msg.OKCANCEL,   
			fn: function peticionAjax(btn){
				if (btn == 'ok'){
				
				NE.util.obtenerPKCS7(confirmar, form.textoFirmar);
								
				}else{
					regresar('38ReviSolRecursos.jsp');
				}
			}
		});
	}
	var procesarRechazar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var columnModelGrid = grid.getColumnModel();
		var CCACUSE = registro.get('CCACUSE');  
		var CVESOLICITUD = registro.get('CVESOLICITUD'); 
		var ESTATUSSOLIC = registro.get('ESTATUSSOLIC'); 
		var causa_recha = registro.get('CAUSA_RECHAZO'); 
		form.num_acuse = registro.get('CCACUSE');  
		form.clave_solicitud = registro.get('CVESOLICITUD');   
		form.estatus =  registro.get('ESTATUSSOLIC'); 
		form.causa =  registro.get('CAUSA_RECHAZO');
		form.textoFirmar='No. Solicitud|Fecha y hora env�o|Nombre Contrato|Fecha Firma Contrato|Contratos Modificatorios|Fecha Firma Modificatorios|Nombre Contrato OE|Fecha Firma Contrato Operaci�n Electr�nica|Importe|Moneda|Destino de los Recursos|Fecha primer pago de Capital|Fecha primer pago de Intereses|Fecha de Vencimiento|Tipo de Tasa|Tasa|Valor|Estatus|Observaciones Intermediario Operador|\n';
		form.textoFirmar+= registro.get('CCACUSE')+"|"+registro.get('FECSOLICITUD')+"|"+registro.get('NOMBRECONTRATO')+"|"+registro.get('FECFIRMACONTRATO')+"|"+registro.get('COUNTMODIF');
		form.textoFirmar+= registro.get('ALLFECMODCONT')+"|"+ registro.get('NOMBRECONTRATOOE')+"|"+registro.get('FECFIRMACONTRATOOE')+"|"+registro.get('IMPORTESOLIC')+"|"+registro.get('NOMBREMONEDA');
		form.textoFirmar+= registro.get('DESTINORECURSO')+"|"+ registro.get('FECPAGOCAPITAL')+"|"+registro.get('FECPAGOINTERES')+"|"+registro.get('FECVENCIMIENTO')+"|"+registro.get('TIPOTASA');
		form.textoFirmar+= registro.get('NOMBRETASA')+"|"+registro.get('TASAINTERES')+"|"+registro.get('NOMBREESTATUS')+"|"+registro.get('OBSERVIF')+"|"+registro.get('CAUSA_RECHAZO');
		if(causa_recha!=''){
			Ext.Msg.show({
				title: 'Confirmaci�n',
				msg: '�Confirma rechazar la Solicitud de Recursos?',
				buttons: Ext.Msg.OKCANCEL,
				fn: function peticionAjax(btn){
					if (btn == 'ok'){
						NE.util.obtenerPKCS7(confirmarRechazo, form.textoFirmar, form);
					}else{
						regresar('38ReviSolRecursos.jsp');
					}
				}
			});
		 }else{
			grid.startEditing(rowIndex, columnModelGrid.findColumnIndex('CAUSA_RECHAZO'));
			return;
		 }
	}
	//------Consulta ------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			var  au2 = jsonData.AUTORIZADOR2;
			var  au1 = jsonData.AUTORIZADOR1;
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			var jsonData = store.reader.jsonData;	    
			var cm = gridConsulta.getColumnModel();
			
			if(jsonData.strPerfil =='IF 5CP'   ||  jsonData.strPerfil =='IF 4CP' )  {   
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('EXISTEFILECARTERAPDF'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('EXISTEFILECARTERA'), false);
			}
			if(au1==iNoUsuario || au2==iNoUsuario){
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('ACCION'), false);
			}else{
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('ACCION'), true);
			}
			if(store.getTotalCount() > 0) {			
				el.unmask();	
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '38ReviSolRecursos.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{name: 'CVESOLICITUD'},
			{name: 'CCACUSE'},
			{name: 'CVEIF'},
			{name: 'CVECONTRATO'},
			{name: 'NOMBRECONTRATO'},
			{name: 'FECSOLICITUD'},
			{name: 'FECFIRMACONTRATO'},
			{name: 'COUNTMODIF'},
			{name: 'ALLFECMODCONT'},
			{name: 'NOMBRECONTRATOOE'},
			{name: 'FECFIRMACONTRATOOE'},
			{name: 'IMPORTESOLIC'},
			{name: 'CVEMONEDA'},
			{name: 'NOMBREMONEDA'},
			{name: 'DESTINORECURSO'},
			{name: 'FECPAGOCAPITAL'},
			{name: 'FECPAGOINTERES'},
			{name: 'FECVENCIMIENTO'},
			{name: 'ESTATUSSOLIC'},
			{name: 'NOMBREESTATUS'},
			{name: 'OBSERVIF'},
			{name: 'OBSERVOP'},
			{name: 'TASAINTERES'},
			{name: 'TIPOTASA'},
			{name: 'CVETASA'},
			{name: 'NOMBRETASA'},
			{name: 'NUMPRESTAMO'},
			{name: 'FECOP'},
			{name: 'NUMCUENTA'},
			{name: 'NOMBREBANCO'},
			{name: 'FOLIO'},
			{name: 'USUARIOIF'},
			{name: 'NOMBREUSERIF'},
			{name: 'CAUSASRECH'},
			{name: 'EXISTEFILECOTIZA'},
			{name: 'EXISTEFILECARTERA'},
			{name: 'EXISTEFILECARTERAPDF'},
			{name: 'EXISTEFILEBOLETARUG'},
			{name: 'EXISTEFILEACUSESOL'},
			{name: 'NOMBREINTER'},
			{name: 'FECASIGNAEJEOP'},
			{name: 'NOMBREUSRCON1'},
			{name: 'SELECCION'},
			{name: 'CAUSA_RECHAZO'},
			{name: 'CG_USUARIO_AUTOR2'},
			{name: 'CG_USUARIO_AUTOR1'},
			{name: 'ACCION'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [				
			{//1
			header: 'No. Solicitud',
			tooltip: 'No. Solicitud',
			dataIndex: 'CCACUSE',
			sortable: true,
			width: 100,
			resizable: true,
			hidden: false,
			align:		'center'
		},
		{//2
			header: 'Fecha y hora env�o',
			tooltip: 'Fecha y hora env�o',
			dataIndex: 'FECSOLICITUD',
			sortable: true,
			width: 150,
			align: 'center'
		},
		{//3
			header: '<center>Nombre Contrato</center>',
			tooltip: 'Nombre Contrato',
			dataIndex: 'NOMBRECONTRATO',
			sortable: true,
			width: 200,
			align: 'left'
		},
		{//4
			header : 'Fecha<br>Firma Contrato',
			tooltip: 'Fecha<br>Firma Contrato',
			dataIndex : 'FECFIRMACONTRATO',
			sortable: true,
			width: 100,
			align: 'center'
		},
		{//5
			header : 'Contratos<br>Modificatorios',
			tooltip: 'Contratos<br>Modificatorios',
			dataIndex : 'COUNTMODIF',
			width : 100,
			sortable : true,
			align:		'center'
		},
		{//6
			header : 'Fecha Firma Modificatorios',
			tooltip: 'Fecha Firma Modificatorios',
			dataIndex : 'ALLFECMODCONT',
			width : 200,
			sortable : true,
			align: 'center',
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		},
		{//7
			header : 'Nombre Contrato OE',
			tooltip: 'Nombre Contrato OE',
			dataIndex : 'NOMBRECONTRATOOE',
			width : 150,
			sortable : true,
			align: 'center'
		},
		{//8
			header : 'Fecha Firma<br>Contrato Operaci�n Electr�nica',
			tooltip: 'Fecha Firma<br>Contrato Operaci�n Electr�nica',
			dataIndex : 'FECFIRMACONTRATOOE',
			sortable : true,
			width : 100,
			align: 'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//9
			header : '<center>Importe</center>',
			tooltip: 'Importe',
			dataIndex : 'IMPORTESOLIC',
			sortable : true,
			align: 'right',
			width : 100,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'NOMBREMONEDA',
			width : 150,
			align: 'center',
			sortable : true
		},
		{//12
			header : 'Destino de los recursos',
			tooltip: 'Destino de los recursos',
			dataIndex : 'DESTINORECURSO',
			width : 150,
			align: 'left',
			sortable : true
		},
		{//13
			header : 'Fecha<br>primer pago de Capital',
			tooltip: 'Fecha<br>primer pago de Capital',
			dataIndex : 'FECPAGOCAPITAL',
			sortable : true,
			//hidden: true,
			align: 'center',
			width : 100
		},
		{//14
			header : 'Fecha<br>primer pago de Interes',
			tooltip: 'Fecha<br>primer pago de Interes',
			dataIndex : 'FECPAGOINTERES',
			//hidden: true,
			sortable : true,
			align: 'center',
			width : 100
		},
		{//15
			header : 'Fecha de<br>Vencimiento',
			tooltip: 'Fecha de<br>Vencimiento',
			dataIndex : 'FECVENCIMIENTO',
			//hidden: true,
			sortable : true,
			align: 'center',
			width : 100
		},
		{//15
			header : 'Tipo de Tasa',
			tooltip: 'Tipo de Tasa',
			dataIndex : 'TIPOTASA',
			//hidden: true,
			sortable : true,
			width : 80,
			align: 'center'
		},
		{//15
			header : 'Tasa',
			tooltip: 'Tasa',
			dataIndex : 'NOMBRETASA',
			//hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//15
			header : 'Valor',
			tooltip: 'Valor',
			dataIndex : 'TASAINTERES',
			//hidden: true,
			sortable : true,
			width : 80,
			align: 'center',
			renderer:  function (causa, columna, registro){
					if(registro.get('TIPOTASA') == 'Variable'){
						if(causa!='' && causa!='null'){
							return  Ext.util.Format.number(causa, '+0.00%');
						}else  {
							return 'N/A';
						}				
					}else{
						if(causa!='' && causa!='null'){
							return  Ext.util.Format.number(causa, '0.00%');
						}else  {
							return 'N/A';
						}				
					}	
				}
		},
		{
			xtype:	'actioncolumn',
			header: 'Carga Cotizaci�n', tooltip: 'Carga Cotizaci�n',
			dataIndex: 'EXISTEFILECOTIZA',	align: 'center',	sortable: true,	resizable: true,	width: 120,	 hidden: false,
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.data['EXISTEFILECOTIZA']=='S'){
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}else{
								return '';
							}
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						var registro = grid.getStore().getAt(rowIndex);
						if(registro.data['EXISTEFILECOTIZA']=='S'){
							Ext.Ajax.request({
							url : '38ReviSolRecursos.data.jsp',
							params: Ext.apply({
								informacion: "obtenerArchivo",
								cveSolicitud: registro.data['CVESOLICITUD'],
								tipoArchivo: 'COTIZA'
							}),
							callback: processSuccessFailureObtieneArchivo});
						}
					}
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'Tabla de Amortizaci�n', tooltip: 'Tabla de Amortizaci�n',
			dataIndex: 'CVESOLICITUD',	align: 'center',	sortable: true,	resizable: true,	width: 120, hidden: false,
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoXls';
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						var registro = grid.getStore().getAt(rowIndex);
						Ext.Ajax.request({
						url : '38ReviSolRecursos.data.jsp',
						params: Ext.apply({
							informacion: "obtenerArchivo",
							cveSolicitud: registro.data['CVESOLICITUD'],
							tipoArchivo: 'AMORTIZACION'
						}),
						callback: processSuccessFailureObtieneArchivo});
					}
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'Carga Cartera en Prenda', tooltip: 'Carga Cartera en Prenda',
			dataIndex: 'EXISTEFILECARTERA',	align: 'center',	sortable: true,	resizable: true,	width: 120, hidden: true,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
							if(record.data['CVECONTRATO']=='1' || record.data['CVECONTRATO']=='4' || record.data['CVECONTRATO']=='5'){
								return 'N/A&nbsp;&nbsp;';
							}
						 },
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						if(registro.data['CVECONTRATO']=='1' || registro.data['CVECONTRATO']=='4' || registro.data['CVECONTRATO']=='5'){
							return '';
						}else{
							if(registro.data['EXISTEFILECARTERA']=='S'){
								this.items[0].tooltip = 'Ver';
								return 'icoXls';
							}else{
								return '';
							}
						}
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						var registro = grid.getStore().getAt(rowIndex);
						if(registro.data['CVECONTRATO']!='1' && registro.data['CVECONTRATO']!='4' && registro.data['CVECONTRATO']!='5'){
							if(registro.data['EXISTEFILECARTERA']=='S'){
								Ext.Ajax.request({
								url : '38ReviSolRecursos.data.jsp',
								params: Ext.apply({
									informacion: "obtenerArchivo",
									cveSolicitud: registro.data['CVESOLICITUD'],
									tipoArchivo: 'CARTERA'
								}),
								callback: processSuccessFailureObtieneArchivo});
							}
						}
					}
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'Carga PDF Cartera en Prenda ',
			tooltip: 'Carga PDF Cartera en Prenda ',
			dataIndex: 'EXISTEFILECARTERAPDF',
			align: 'center',
			sortable: true,
			resizable: true,
			width: 120,
			hidden:true,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
							if(record.data['CVECONTRATO']=='1' || record.data['CVECONTRATO']=='4' || record.data['CVECONTRATO']=='5'){
								return 'N/A&nbsp;&nbsp;';
							}
						 },
			items: [      
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						if(registro.data['CVECONTRATO']=='1' || registro.data['CVECONTRATO']=='4' || registro.data['CVECONTRATO']=='5'){
							return '';
						}else{
							if(registro.data['EXISTEFILECARTERAPDF']=='S'){
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}else{
								return '';
							}
						}
						
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						var registro = grid.getStore().getAt(rowIndex);
						if(registro.data['CVECONTRATO']!='1' && registro.data['CVECONTRATO']!='4' && registro.data['CVECONTRATO']!='5'){
							if(registro.data['EXISTEFILECARTERAPDF']=='S'){
								Ext.Ajax.request({
								url : '38ReviSolRecursos.data.jsp',
								params: Ext.apply({
									informacion: "obtenerArchivo",
									cveSolicitud: registro.data['CVESOLICITUD'],
									tipoArchivo: 'CARTERA_PDF'
								}),
								callback: processSuccessFailureObtieneArchivo});
							}
						}
				
					}
				}
			]
		},      
		{
			xtype:	'actioncolumn',
			header: 'PDF Comprobante Boleta RUG', tooltip: 'PDF Comprobante Boleta RUG',
			dataIndex: 'EXISTEFILEBOLETARUG',	align: 'center',	sortable: true,	resizable: true,	width: 120, hidden: true,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
							if(record.data['CVECONTRATO']=='1' || record.data['CVECONTRATO']=='4' || record.data['CVECONTRATO']=='5'){
								return 'N/A&nbsp;&nbsp;';
							}
						 },
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						
						if(registro.data['CVECONTRATO']=='1' || registro.data['CVECONTRATO']=='4' || registro.data['CVECONTRATO']=='5'){
							return '';
						}else{
							if(registro.data['EXISTEFILEBOLETARUG']=='S'){
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}else{
								return '';
							}
						}
						
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						var registro = grid.getStore().getAt(rowIndex);
						if(registro.data['CVECONTRATO']!='1' && registro.data['CVECONTRATO']!='4' && registro.data['CVECONTRATO']!='5'){
							if(registro.data['EXISTEFILEBOLETARUG']=='S'){
								Ext.Ajax.request({
								url : '38ReviSolRecursos.data.jsp',
								params: Ext.apply({
									informacion: "obtenerArchivo",
									cveSolicitud: registro.data['CVESOLICITUD'],
									tipoArchivo: 'RUG'
								}),
								callback: processSuccessFailureObtieneArchivo});
							}
						}
					}
				}
			]
		},
		{
			xtype:	'actioncolumn',
			header: 'Solicitud Completa', tooltip: 'Solicitud Completa',
			dataIndex: 'EXISTEFILEACUSESOL',	align: 'center',	sortable: true,	resizable: true,	width: 120, hidden: false,
			
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						if(registro.data['EXISTEFILEACUSESOL']=='S'){
							this.items[0].tooltip = 'Ver';
							return 'icoVistaPrevia';
						}else{
							return '';
						}
						
					},
					handler:function(grid, rowIndex, colIndex, item, event){
						
						var registro = grid.getStore().getAt(rowIndex);
						
						if(registro.data['EXISTEFILEACUSESOL']=='S'){						
							Ext.Ajax.request({
							url : '38ReviSolRecursos.data.jsp',
							params: Ext.apply({
								informacion: "obtenerArchivo",
								cveSolicitud: registro.data['CVESOLICITUD'],
								tipoArchivo: 'ACUSE'
							}),
							callback: processSuccessFailureObtieneArchivo
							});
						
						}
					}
				}
			]
		},
		{
			header : 'Estatus',
			tooltip: 'Estatus',
			dataIndex : 'NOMBREESTATUS',   
			hidden: false,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{
			header : 'Observaciones Intermedierio Operador',
			tooltip: 'Observaciones Intermedierio Operador',
			dataIndex : 'OBSERVIF',
			sortable : true,
			width : 120,
			align: 'left'
		},
		{
				header: '<center>Causa de Rechazo</center>',
				tooltip: 'Causa de Rechazo',
				dataIndex: 'CAUSA_RECHAZO',
				sortable: true,
				width: 200,
				align: 'left',
				editor: {
					xtype: 'textfield',
					allowBlank:false
				},
				renderer:function(value,metadata,registro){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}				
		},
		{
			xtype		: 'actioncolumn',
			header: 'Acci�n',
			tooltip: 'Acci�n ',				
			sortable: true,
			width: 150,
			dataIndex: 'ACCION',
			resizable: true,	
			id:'idAcci',
			hidden :false,
			align: 'center',
			items		: [
				{   
					getClass: function(value,metadata,registro,rowIndex,colIndex,store){
						if(registro.data['CG_USUARIO_AUTOR1']==iNoUsuario||registro.data['CG_USUARIO_AUTOR2']==iNoUsuario){
							this.items[0].tooltip = '';
							return '';
						}else{
							this.items[0].tooltip = 'Autorizar';
							return 'correcto';
						}
					}
					,handler: procesarAutorizar
				},
				{
					getClass: function(value,metadata,registro,rowIndex,colIndex,store){
						if(registro.data['CG_USUARIO_AUTOR1']==iNoUsuario||registro.data['CG_USUARIO_AUTOR2']==iNoUsuario){
							this.items[0].tooltip = '';
							return '';
						}else{
							this.items[1].tooltip = 'Rechazar';
							return 'icoRechazar';
						}
					}
					,handler: procesarRechazar
				}
			]
		}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false
	});
//*************** componentes de la Forma********************
	var  elementosForma  = [
		{ 
         xtype:         'bigdecimal',
         name:          'no_solicitud',
         id:            'no_solicitud1',
         allowDecimals: false,
         allowNegative: false,
         fieldLabel:    'No. Solicitud',
         blankText:     'Favor de capturar el n�mero de Solicitud',       
         hidden:        false,
         maxLength:     22, 
         msgTarget:     'side',
         anchor:        '-20',
         maxValue:      '9999999999999999999999',
         format:        '0000000000000000000000',
			margins: '0 20 0 0'	  //necesario para mostrar el icono de error
      },	
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaRecepcionIni',
					id: 'fechaRecepcionIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'fechaRecepcionFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaRecepcionFin',
					id: 'fechaRecepcionFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fechaRecepcionIni',
					margins: '0 20 0 0'  
				}
			]
		}
	];	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		monitorValid: true,
		title: 'Revisi�n Solicitudes de Recursos',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {
					var fechaRecepcionIni = Ext.getCmp('fechaRecepcionIni');
					var fechaRecepcionFin = Ext.getCmp('fechaRecepcionFin');
					if(!Ext.isEmpty(fechaRecepcionIni.getValue()) || !Ext.isEmpty(fechaRecepcionFin.getValue())){
						if(Ext.isEmpty(fechaRecepcionIni.getValue())){
							fechaRecepcionIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaRecepcionIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaRecepcionFin.getValue())){
							fechaRecepcionFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaRecepcionFin.focus();
							return;
						}
					}
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{  
							informacion: 'Consultar'
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('forma').getForm().reset();			
				}
			}
		]
	});
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});
	consultaData.load({
		params: Ext.apply(fp.getForm().getValues(),{  
		informacion: 'Consultar'
		})
	});		
});