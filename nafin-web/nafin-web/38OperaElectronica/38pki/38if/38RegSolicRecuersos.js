
Ext.onReady(function() {

	var ic_solicitud  =  Ext.getDom('ic_solicitud').value;
	var tipoSolicitud =  Ext.getDom('tipoSolicitud').value;
	var ic_if =  Ext.getDom('ic_if').value;
	
	var myValidFnCSV = function(v) {
		var myRegex = /^.+\.([cC][sS][vV])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivoCsv 		: myValidFnCSV,
		archivoCsvText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): CSV.'
	});
	
		
	var myValidFnPdf = function(v) {
		var myRegex = /^.+\.([pP][dD][fF])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivopdf 		: myValidFnPdf,
		archivopdfText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): PDF.'
	});
	
	
	function procesarImporte(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			var importe = Ext.getCmp('importe1');
			
			if(jsonData.mensajeMonto!='') {
				importe.markInvalid(jsonData.mensajeMonto);
				Ext.getCmp('rebaseLimite').setValue('S');
				return;
			}else  {
				Ext.getCmp('rebaseLimite').setValue('N');			
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function transmite_Correo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//*******************Trannsmite Acuse *****************
	function transmite_Acuse(opts, success, response) {
		var fp = Ext.getCmp('forma');
			fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				Ext.getCmp('mensajeHorario').hide();
				
			if(	jsonData.mensajeError =='')  {
				Ext.getCmp("btnSalir").show();	
				Ext.getCmp("btnCancelar").show();
				Ext.getCmp("btnImprimirAcuse").show();					
				Ext.getCmp('lblAcuseResumen').update(jsonData.mensajeExito);
				Ext.getCmp("divAcuseResumen").show();	
				Ext.getCmp("divAcuseResumen_2").show();	
				Ext.getCmp("divPreAcuse").hide();	
				Ext.getCmp("divPreAcuse_2").hide();
				Ext.getCmp("mensajeModifiPre").hide();
				Ext.getCmp("montoMoneda").hide();
				
				Ext.getCmp('mensajeModifiAcuse').update(jsonData.mensajeModifiAcuse);
				Ext.getCmp('lblFecha_SolicitudAcuse').update(jsonData.lblFecha_SolicitudAcuse);
				Ext.getCmp('lblNombreIFAcuse').update(jsonData.lblNombreIFAcuse);
				Ext.getCmp('lblNumCuentaAcuse').update(jsonData.lblNumCuentaAcuse);
				Ext.getCmp('lblBancoAcuse').update(jsonData.lblBancoAcuse);
				Ext.getCmp('lblCuentaCLABEAcuse').update(jsonData.lblCuentaCLABEAcuse);
				Ext.getCmp('ic_solicitud1').setValue(jsonData.ic_solicitud);
				Ext.getCmp('pantalla').setValue(jsonData.pantalla);	
				
				var acuseResumen = [
					['Importe ', jsonData.importe],
					['Moneda ', jsonData.descripMoneda],
					['Fecha de Env�o Solicitud ', jsonData.hidFechaCarga],
					['Hora de Env�o ', jsonData.hidHoraCarga],
					['Usuario IF ', jsonData.nombreUsuario]
				];
				
				storeResumenData.loadData(acuseResumen);				
				
				Ext.getCmp("btnCancelar").hide();	
				Ext.getCmp("btnConfirmarSolicitud").hide();
				Ext.Ajax.request({
				url: '38RegSolicRecuersos.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'enviar_correo',
					ic_solicitud:jsonData.ic_solicitud,
					nombreUsuario:jsonData.nombreUsuario,
					hidFechaCarga:jsonData.hidFechaCarga,
					ic_if:jsonData.ic_if,
					_acuse:jsonData._acuse,
					tipoSolicitud:jsonData.tipoSolicitud,
					combTasa:jsonData.combTasa,
					importe:jsonData.importe,
					claveMoneda:jsonData.claveMoneda,
					desRecuersos:jsonData.desRecuersos,
					fecha_PagoCapital:jsonData.fecha_PagoCapital,
					fecha_PagoInteres:jsonData.fecha_PagoInteres,
					fecha_vencimiento:jsonData.fecha_vencimiento,
					cboInteres:jsonData.cboInteres,
					tasaInteres:jsonData.tasaInteres,
					observaciones:jsonData.observaciones,
					ic_contrato:jsonData.ic_contrato,
					fecha_Solicitud:jsonData.fecha_Solicitud,
					iNoUsuario:jsonData.iNoUsuario,
					lblFondoLiquido:jsonData.lblFondoLiquido,
					strPerfil:jsonData.strPerfil
				}),
				callback: transmite_Correo
			});
				
			}else  {
				Ext.MessageBox.alert("MensajeError",jsonData.mensajeError);				
				
			}
					

			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var confirmarAcuse = function(pkcs7, vtextoFirmar){	
		
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnConfirmarSolicitud").enable();	
			return;	//Error en la firma. Termina...
		}else  {	
			fp.el.mask('Procesando Solicitud...', 'x-mask-loading');			
		
			Ext.getCmp("btnConfirmarSolicitud").disable();				
			Ext.Ajax.request({
				url: '38RegSolicRecuersos.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Guarda_Solicitud',
					ic_solicitud:Ext.getCmp('ic_solicitud1').getValue(),
					ic_contrato:Ext.getCmp("ic_contrato").getValue(),
					pkcs7: pkcs7,
					textoFirmado: vtextoFirmar,
					ic_if:ic_if
				}),
				callback: transmite_Acuse
			});
				
		}
	
	}
	
	
	
	function ConfirmaSolicitud(opts, success, response) {
		var  textoFirmar = "Fecha Solicitud: "+Ext.getCmp('lblFecha_SolicitudPre').getValue().replace('<b>',"").replace('</b>',"")+"\n"+
								 "Nombre IF:  "+Ext.getCmp('lblNombreIFPre').getValue().replace('<b>',"").replace('</b>',"")+"\n"+
								 "N�mero de Cuenta:  "+Ext.getCmp('lblNumCuentaPre').getValue().replace('<b>',"").replace('</b>',"")+"\n"+
								 "banco: "+Ext.getCmp('lblBancoPre').getValue().replace('<b>',"").replace('</b>',"")+"\n"+
								 "Cuenta CLABE:  "+Ext.getCmp('lblCuentaCLABPre').getValue().replace('<b>',"").replace('</b>',"")+"\n"+
								 "Datos del Pr�stamo \n "+								 
								 "Destino de los Recursos: "+Ext.getCmp('lblDesRecuersos').getValue().replace('<b>',"").replace('</b>',"")+"\n"+
								 "Fecha de primer pago Capital:  "+Ext.getCmp('lblFecha_PagoCapital').getValue().replace('<b>',"").replace('</b>',"")+"\n"+
								 "Fecha de primer pago Interes: "+Ext.getCmp('lblFecha_PagoInteres').getValue().replace('<b>',"").replace('</b>',"")+"\n"+
								 "Fecha vencimiento: "+Ext.getCmp('lblFecha_vencimiento').getValue().replace('<b>',"").replace('</b>',"")+"\n"+
								 "Observaciones:  "+Ext.getCmp('lblObservaciones').getValue().replace('<b>',"").replace('</b>',"")+"\n";		
		
				
		NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar);	
	
	}
	
	
		//Grid del Acuse 
	var storeResumenData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridResumen = new Ext.grid.GridPanel({
		id: 'gridResumen',
		store: storeResumenData,
		margins: '20 0 0 0',				
		align: 'center',
		title   : 'Resumen de Solicitud',		
		columns: [
			{
				header : '',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true,
				align: 'left'	
			},
			{
				header : '',			
				dataIndex : 'informacion',
				width : 348,
				align: 'left',
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 550,
		height: 150,
		style: 'margin:0 auto;',		
		frame: true
	});
	
	
	//*******************Trannsmite PreAcuse *****************

	function transmiteCarga_PreAcuse(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
				
				//Pantalla PreAcuse 
				
				Ext.getCmp('divPrestamoPreAcuse').show();
				Ext.getCmp('divPrestamoPreAcuse_2').show();				
				Ext.getCmp('btnCancelar').show();				
				Ext.getCmp('divRegistro').hide();	
				Ext.getCmp('divPreAcuse').show();	
				Ext.getCmp("divPreAcuse_2").show();
				Ext.getCmp('mensajeModifiPre').update(jsonData.mensajeModifi);
				Ext.getCmp('mensajeTasa').update(jsonData.mensajeTasa);				
				Ext.getCmp('montoMoneda').update(jsonData.montoMoneda);
						
				Ext.getCmp('lblFecha_SolicitudPre').update(jsonData.lblFecha_Solicitud);
				
				Ext.getCmp('lblNombreIFPre').update(jsonData.lblNombreIFPre);
				Ext.getCmp('lblNumCuentaPre').update(jsonData.lblNumCuentaPre);
				Ext.getCmp('lblBancoPre').update(jsonData.lblBancoPre);
				Ext.getCmp('lblCuentaCLABPre').update(jsonData.lblCuentaCLABPre);
				
				Ext.getCmp('lblDesRecuersos').update(jsonData.lblDesRecuersos);
				Ext.getCmp('lblFecha_PagoCapital').update(jsonData.lblFecha_PagoCapital);
				Ext.getCmp('lblFecha_PagoInteres').update(jsonData.lblFecha_PagoInteres);
				Ext.getCmp('lblFecha_vencimiento').update(jsonData.lblFecha_vencimiento);
				Ext.getCmp('lblTasaInteres').update(jsonData.lblTasaInteres);				
				Ext.getCmp('lblObservaciones').update(jsonData.lblObservaciones);				
				Ext.getCmp('lblUsuarioIFPreAcuse').update(jsonData.lblUsuarioIFPreAcuse);				
				Ext.getCmp('pantalla').setValue(jsonData.pantalla);
				Ext.getCmp('comTasaArchCotiza').show();
				Ext.getCmp("mensajeModifiPre").show();	
				
				if(Ext.getCmp('txtcargaCotiza').getValue()=='S')  {
					Ext.getCmp('btnDescargCotiza').show();						
				}else  {
					Ext.getCmp('btnDescargCotiza').hide();
				}
				
			/*	if(Ext.getCmp('txtcargaRUG').getValue()=='S')  {
					Ext.getCmp('comprobanteRUG').show();					
				}				
			*/	
				if(Ext.getCmp('txtcargaPrenda').getValue()=='S')  {
					Ext.getCmp('carteraPrenda').show();					
				}
				if(Ext.getCmp('txtcargaPrendaPdf').getValue()=='S')  {
					Ext.getCmp('carteraPrendaPdf').show();					
				}
				
				if(Ext.getCmp('hayAmortizacion').getValue()=='S')  {							
					Ext.getCmp('divAmortizacion').show();
				}		
								
				if ( jsonData.pantalla=='Prenda'   ||  jsonData.pantalla=='RUG' ||  jsonData.pantalla=='Prenda_Pdf'  ){		
					if ( Ext.getCmp('strPerfil').getValue()=='IF 4CP' || Ext.getCmp('strPerfil').getValue()=='IF 5CP' ){	
						Ext.getCmp('montoMoneda').hide();
						
						if(Ext.getCmp('txtcargaPrenda').getValue()=='S'  &&   jsonData.pantalla=='Prenda' ||(Ext.getCmp('txtcargaPrendaPdf').getValue()=='S'  &&   jsonData.pantalla=='Prenda')  )  {
							Ext.getCmp("divPreAcuse").setTitle('Carga de Cartera en Prenda');
						}
					/*	if(Ext.getCmp('txtcargaRUG').getValue()=='S'  && jsonData.pantalla=='RUG'  )  {
							Ext.getCmp("divPreAcuse").setTitle('Carga PDF Comprobante RUG - Confirmaci�n');
						}
						*/
					}
				}else  {
					Ext.getCmp("divPreAcuse").setTitle('Pre acuse - Resumen de la Solicitud');
					Ext.getCmp('montoMoneda').show();					
				}
				
				//Pantalla Captura 				
				Ext.getCmp('fecha_Solicitud').hide()
				Ext.getCmp('divPrestamo').hide();
				Ext.getCmp('btnContinuar').hide();
				Ext.getCmp('btnCargaTabla').hide();
				Ext.getCmp('divSolicitud').hide();
				Ext.getCmp('btnCartera').hide();
				Ext.getCmp('btnPDFCartera').hide();
				//Ext.getCmp('btnRUG').hide();


			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarImporteCont(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			var importe = Ext.getCmp('importe1');
			
			if(jsonData.mensajeMonto!='') {
				importe.markInvalid(jsonData.mensajeMonto);
				Ext.getCmp('rebaseLimite').setValue('S');
				return;
			}else  {
				Ext.getCmp('rebaseLimite').setValue('N');			
			}
			
			validaContinua2(opts, success, response);
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function validaContinua(opts, success, response) {
		var importe = Ext.getCmp('importe1');
	
			Ext.Ajax.request({
				url: '38RegSolicRecuersos.data.jsp',
				params: {
					informacion: 'validaMonto',
					importe:importe.getValue()									
				},
				callback: procesarImporteCont
			});			
							
	}
	
	
	function validaContinua2(opts, success, response) {
	
		var importe = Ext.getCmp('importe1');
		var claveMoneda = Ext.getCmp('claveMoneda1');
		var desRecuersos = Ext.getCmp('desRecuersos1');
		var fecha_PagoCapital = Ext.getCmp('fecha_PagoCapital');
		var fecha_PagoInteres = Ext.getCmp('fecha_PagoInteres');
		var fecha_vencimiento = Ext.getCmp('fecha_vencimiento');
		var cboInteres = Ext.getCmp('cboInteres1');
		var tasaInteres = Ext.getCmp('tasaInteres1');
		var hayAmortizacion = Ext.getCmp('hayAmortizacion');
		var strPerfil = Ext.getCmp('strPerfil');
		var txtcargaPrenda = Ext.getCmp('txtcargaPrenda');
		var txtcargaPrendaPdf = Ext.getCmp('txtcargaPrendaPdf');
		var combTasa = Ext.getCmp('combTasa1');		
	//	var txtcargaRUG = Ext.getCmp('txtcargaRUG');
		var rebaseLimite = Ext.getCmp('rebaseLimite');
		
		if (Ext.isEmpty(importe.getValue()) ){
			importe.markInvalid('Este campo es obigatorio');
			return;
		}	
		
		if (rebaseLimite.getValue() =='S' ){
			importe.markInvalid('El importe de la Solicitud de Recursos es mayor al Monto Disponible');
			return;
		}
		
		if (Ext.isEmpty(claveMoneda.getValue()) ){
			claveMoneda.markInvalid('Este campo es obigatorio');
			return;
		}
		if (Ext.isEmpty(desRecuersos.getValue()) ){
			desRecuersos.markInvalid('Este campo es obigatorio');
			return;
		}
		
		if (Ext.isEmpty(fecha_PagoCapital.getValue()) ){
			fecha_PagoCapital.markInvalid('Este campo es obigatorio');
			return;
		}
		if (Ext.isEmpty(fecha_PagoInteres.getValue()) ){
			fecha_PagoInteres.markInvalid('Este campo es obigatorio');
			return;
		}
		if (Ext.isEmpty(fecha_vencimiento.getValue()) ){
			fecha_vencimiento.markInvalid('Este campo es obigatorio');
			return;
		}
		if (Ext.isEmpty(cboInteres.getValue()) ){
			cboInteres.markInvalid('Este campo es obigatorio');
			return;
		}
		
		if (Ext.isEmpty(combTasa.getValue())  &&  cboInteres.getValue()=='Variable' ) {
			combTasa.markInvalid('Este campo es obigatorio');
			return;
		}
		if (Ext.isEmpty(tasaInteres.getValue())  &&   	cboInteres.getValue()=='Variable' ) {
			tasaInteres.markInvalid('Este campo es obigatorio');
			return;
		}
		
		if (Ext.isEmpty(tasaInteres.getValue())  &&   	cboInteres.getValue()=='Fija' ) {
			tasaInteres.markInvalid('Este campo es obigatorio');
			return;
		}
							
		if (hayAmortizacion.getValue()!='S' ){		
			Ext.MessageBox.alert("Mensaje","Es necesario realizar la carga de la tabla de amortizaci�n");
			return;
		}
		
		if (txtcargaPrenda.getValue()!='S'  &&  (  strPerfil.getValue()=='IF 4CP' || strPerfil.getValue()=='IF 5CP')  ){		
			Ext.MessageBox.alert("Mensaje","Carga Cartera en Prenda es Obligatoria");
			return;
		}	
		if (txtcargaPrendaPdf.getValue()!='S'  &&  (  strPerfil.getValue()=='IF 4CP' || strPerfil.getValue()=='IF 5CP')  ){		
			Ext.MessageBox.alert("Mensaje","Carga PDF Cartera en Prenda es Obligatoria");
			return;
		}	
		/*if (txtcargaRUG.getValue()!='S'  &&  (  strPerfil.getValue()=='IF 4CP' || strPerfil.getValue()=='IF 5CP')  ){		
			Ext.MessageBox.alert("Mensaje","PDF Comprobante RUG es Obligatorio ");
			return;
		}*/	
		
					
		Ext.Ajax.request({
			url: '38RegSolicRecuersos.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
			informacion: 'Carga_PreAcuse',
			ic_solicitud:Ext.getCmp('ic_solicitud1').getValue(),
			ic_contrato:Ext.getCmp("ic_contrato").getValue(),
			ic_if:ic_if,			
			pantalla:'PreAcuse'
			}),
				callback: transmiteCarga_PreAcuse
			});
			
			Ext.getCmp('btnConfirmarSolicitud').show();
			
	}


	function procesarDescargaArch(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
//********************* Proceso para la carga del archivo de cotizaci�n**********************
	
	function transmiteTablaAmortizacion(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
				
				Ext.MessageBox.alert('Mensaje','El Archivo se ha agregado a la solicitud',
					function(){ 					
						Ext.getCmp('VerArchivoTabla').destroy();
						Ext.getCmp('hayAmortizacion').setValue(jsonData.hayAmortizacion);
				});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var continuarTablaAmortizacion = function() {
		var  gridTabla = Ext.getCmp('gridTabla');
		var store = gridTabla.getStore();	
		
		var periodo = [];
		var fecha = [];
		var amortizacion = [];
			
		store.each(function(record) {		
			periodo.push(record.data['PERIODO']);
			fecha.push(record.data['FECHA']);
			amortizacion.push(record.data['AMORTIZACION']);
		});
		
			Ext.Ajax.request({
				url: '38RegSolicRecuersos.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'guardartablaAmortizacionTMP',
					periodo:periodo,
					fecha:fecha,					
					amortizacion:amortizacion,
					ic_solicitud:Ext.getCmp('ic_solicitud1').getValue(),
					ic_contrato:Ext.getCmp("ic_contrato").getValue(),
					ic_if:ic_if
				}),
				callback: transmiteTablaAmortizacion
			});
	}
		
	
	function procesaCargaAmortizacion(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
				
				Ext.getCmp('TabGrid').show();					
				storeErrorData.loadData(jsonData.todError);
				storeBienData.loadData(jsonData.todOK);
				storeRegistrosOKData.loadData(jsonData.todRegOK);
				
				if(jsonData.nuErrores!='0') {
					Ext.getCmp('btnCotinuarT').hide();	
				}else  {
					Ext.getCmp('btnCotinuarT').show();	
				}
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var storeRegistrosOKData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'PERIODO'},
			{name: 'FECHA'},
			{name: 'AMORTIZACION'}
		],
		totalProperty : 'total',
		autoLoad: false	
	});
	
		
	var gridTabla = new Ext.grid.GridPanel	({
		title      : 'Tabla Amortizaci�n',
		id 		  : 'gridTabla',
		store 	  : storeRegistrosOKData,
		style 	  : 'margin:0 auto;',
		stripeRows : true,
		loadMask	  : false,
		hidden	  : false,
		width		  : 380,
		//height	  : 200,		
		frame		  : false, 
		header	  : true,
		align    : 'center',
		columns    : [
			{
				header		: 'Per�odo',
				tooltip		: 'PERIODO',			
				dataIndex	: 'PERIODO',	
				sortable		:true,	
				resizable	:true,	
				width			:80,
				align: 'center'
			},		
			{
				header		: 'Fecha',
				tooltip		: 'FECHA',			
				dataIndex	: 'FECHA',	
				sortable		: true,	
				resizable	: true,	
				width			:100,
				align: 'center'
			},
			{
				header		: 'Amortizaci�n',
				tooltip		: 'AMORTIZACION',			
				dataIndex	: 'AMORTIZACION',	
				sortable		: true,	
				resizable	: true,	
				width			:200,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		]		
	});
	
	
	var storeErrorData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'DESCRIPCION'}			
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	var gridError =[{
		id: 'gridError',
		hidden: false,
		xtype:'grid',
		title:'',
		store: storeErrorData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 680,
				align: 'left'
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 690,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	
	
	var storeBienData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'DESCRIPCION'}			
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	var gridBien =[{
		id: 'gridBien',
		hidden: false,
		xtype:'grid',
		title:'',
		store: storeBienData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'Periodo',
				tooltip: 'Periodo',
				dataIndex: 'DESCRIPCION',
				sortable: true,
				width: 680,
				align: 'left'
			}				
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 690,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	var TabGrid = {
		xtype: 'tabpanel',		
		id:'TabGrid',
		activeTab:0,
		width:	690,
		plain:	true,
		hidden: true,
		defaults:	{autoHeight: true},
		items:[
			{
				title:	'Registros sin Errores',
				id:		'tabRegSinErrores',
				style:	'margin:0 auto;',
				items: [
					NE.util.getEspaciador(10),
					gridBien,
					NE.util.getEspaciador(10)
				]
			},
			{
				title:	'Registros con Errores',
				id:		'tabRegConErrores',
				style:	'margin:0 auto;',
				items: [
					NE.util.getEspaciador(10),
					gridError,
					NE.util.getEspaciador(10)
				]
			}
		]
	};
	
	var enviarArchivoTabla = function() {
		var fpCargaTabla = Ext.getCmp('fpCargaTabla');
		
		var archivo = Ext.getCmp('archivoTabla');
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}		
		var  parametros = "ic_solicitud="+Ext.getCmp('ic_solicitud1').getValue()+"&tipoArchivo=Tabla";
		
		fpCargaTabla.getForm().submit({
			url: '38RegSolicRecuersos.ma.jsp?'+parametros,									
			waitMsg: 'Enviando datos...',
			waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;
				var resultado = resp.resultado;
				var mensaje = resp.mensaje;
				var nombreArchivo = resp.nombreArchivo;
				
				if(resultado=='S') {						
					Ext.Ajax.request({
						url: '38RegSolicRecuersos.data.jsp',
						params: {
							informacion: "valoresCargaAmortizacion",
							ic_solicitud:ic_solicitud,
							nombreArchivo:nombreArchivo,
							ic_if:ic_if,
							montoSolicitado:Ext.getCmp('importe1').getValue()
						},
						callback: procesaCargaAmortizacion
					});
				}else if(resultado=='N') {
					Ext.MessageBox.alert("Mensaje",mensaje);
					return;
				}								
			}
			,failure: NE.util.mostrarSubmitError
		});				
	}
	
	//-------Grid de Ayuda ------------------------------
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{	name: 'NUMERO'},
			{	name: 'NOMBRE'},
			{	name: 'TIPODATO'},
			{	name: 'LONGITUD'},
			{	name: 'CONDICION'}
		]
	});
	
	var infoLayout = [
		['1','Per�odo','NUMBER','3', 'Obligatorio'],
		['2','Fecha','DATE','10','Obligatorio'],
		['3','Amortizaci�n','NUMBER','17,2','Obligatorio']
	];

	storeLayoutData.loadData(infoLayout); 
	
	var gridLayout = {
		xtype: 'grid',
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: 'Layout Tabla de Amortizaci�n',
		columns: [
			{
				header : 'No.',
				dataIndex : 'NUMERO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Nombre del Campo',
				tooltip: 'Nombre del Campo',
				dataIndex : 'NOMBRE',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Tipo de Dato',
				dataIndex : 'TIPODATO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Longitud M�xima',
				dataIndex : 'LONGITUD',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Condici�n',
				dataIndex : 'CONDICION',
				width : 100,
				sortable : true,
				align: 'center'
			}
		],
		stripeRows: true,		
		loadMask: true,
		width: 520,
		height: 150,
		frame: true
	};

	var fpCargaTabla = {
		xtype: 'form',
		id: 'fpCargaTabla',
		layout: 'form',		
		width: 700,
		title: '',
		frame: true,	
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},	
		monitorValid: true,
		items: [	
			{
				xtype: 'button',
				id: 'btnAyuda',
				columnWidth: .05,
				autoWidth: true,
				autoHeight: true,
				iconCls: 'icoAyuda',
				handler: function(){	
					var gridLayout = Ext.getCmp('gridLayout');
					if (!gridLayout.isVisible()) {
						gridLayout.show();
					}else{
						gridLayout.hide();
					}	
				}
			},				
			{
				xtype: 'panel',
				id:		'pnlArchivo',				
				layout: 'form',
				fileUpload: true,
				labelWidth: 150,					
				items: [
					{
						xtype: 'fileuploadfield',
						id: 'archivoTabla',
						width: 150,	  
						emptyText: 'Carga Tabla de Amortizaci�n',
						fieldLabel: 'Carga Tabla de Amortizaci�n',
						name: 'archivoTabla',
						buttonText: 'Examinar...',						
						anchor: '90%',
						vtype: 'archivoCsv'
						
					 }					
				]
			},
			{
				xtype: 'displayfield',
				value: 'El archivo debe de tener formato CSV'			
			}
			,gridLayout			
		],
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptarT',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivoTabla
			}
		]
	};
	
	function cargaTabla(){
	
		var importe = Ext.getCmp('importe1');
		if (Ext.isEmpty(importe.getValue()) ){
			importe.markInvalid('Este campo es obigatorio');
			return;
		}	
		
		var ventana1 = Ext.getCmp('VerArchivoTabla');
		if (ventana1) {
			ventana1.show();
		} else {
			new Ext.Window({				
				width: 710,	
				id:'VerArchivoTabla',				
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,				
				items: [						
					fpCargaTabla,
					TabGrid						
				],
				title: 'Carga Tabla Amortizaci�n',
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',					
						{	
							xtype: 'button',	
							text: 'Continuar ',	
							iconCls: 'icoAceptar', 	
							id: 'btnCotinuarT',
							hidden: true,
							handler: continuarTablaAmortizacion
						},
						'-',
						{	
							xtype: 'button',	
							text: 'Cancelar ',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerrarT', 
							handler: function(){
								Ext.getCmp('VerArchivoTabla').destroy();																								
							} 
						}
					]
				}
			}).show();
		}	
	}
	
	
	//********************* Proceso para la carga PDF Comprobante RUG **********************
	
	/*
	var enviarArchivoRUG = function() {
		
		var archivo = Ext.getCmp('archivoRUG');
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}
		
		var  parametros = "ic_solicitud="+Ext.getCmp('ic_solicitud1').getValue()+"&tipoArchivo=RUG&tipoSolicitud="+ Ext.getCmp('tipoSolicitud').getValue();
		var fpCargaRUG = Ext.getCmp('fpCargaRUG');
		fpCargaRUG.getForm().submit({
			 url: '38RegSolicRecuersos.ma.jsp?'+parametros,									
			 waitMsg: 'Enviando datos...',
			 waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;
				var resultado = resp.resultado;
				var mensaje = resp.mensaje;
			
				if(resultado=='S') {
					Ext.getCmp('VerArchivoRUG').destroy();						
					Ext.getCmp('txtcargaRUG').setValue("S");
					Ext.getCmp('btnAceptarC').show();
					Ext.getCmp('btnConfirmarSolicitud').hide();
						
					Ext.Ajax.request({
						url: '38RegSolicRecuersos.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Carga_PreAcuse',
						ic_solicitud:Ext.getCmp('ic_solicitud1').getValue(),
						ic_contrato:Ext.getCmp("ic_contrato").getValue(),
						ic_if:ic_if,
						pantalla:'RUG'
						}),
						callback: transmiteCarga_PreAcuse
					});			
					
					
				}else if(resultado=='N') {
					Ext.MessageBox.alert("Mensaje",mensaje);
					Ext.getCmp('txtcargaRUG').setValue("N");
					return;
				}								
			}
			,failure: NE.util.mostrarSubmitError
		});			
	}
	
	var fpCargaRUG = {
		xtype: 'form',
		id: 'fpCargaRUG',
		layout: 'form',	
		width: 600,
		title: '',
		frame: true,	
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 30,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},	
		monitorValid: true,
		items: [	
			{
				xtype: 'panel',
				id:		'pnlArchivoR',				
				layout: 'form',
				fileUpload: true,
				labelWidth: 150,					
				items: [
					{
						xtype: 'fileuploadfield',
						id: 'archivoRUG',
						width: 150,	  
						emptyText: 'Carga Archivo de',
						fieldLabel: 'Carga Archivo de',
						name: 'archivoRUG',
						buttonText: 'Examinar...',						
						anchor: '90%',
						vtype: 'archivopdf'
					 }					
				]
			},
			{
				xtype: 'displayfield',
				value: 'El archivo debe tener formato pdf <br> El tama�o del archivo debe ser menor a 10MB'				
			}
		],
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptarRUG',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivoRUG
			}
		]
	};
	
	function cargaRUG(){
	
		var ventana2 = Ext.getCmp('VerArchivoRUG');
		
		if (ventana2) {
			ventana2.show();
		} else {
			new Ext.Window({				
				width: 610,	
				id:'VerArchivoRUG',
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,		
				items: [					  
					fpCargaRUG
				],
				title: 'PDF Comprobante RUG',
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	
							xtype: 'button',	
							text: 'Cancelar ',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerrarRUG', 
							handler: function(){
								Ext.getCmp('VerArchivoRUG').destroy();									
							} 
						},
						'-'							
					]
				}
			}).show();
		}	
	}
	
	*/
	
	//********************* Proceso para la carga Cartera en Prenda **********************
	var enviarArchivoPrenda = function() {
		
		var archivo = Ext.getCmp('archivoPrenda');
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}
		
		var  parametros = "ic_solicitud="+Ext.getCmp('ic_solicitud1').getValue()+"&tipoArchivo=Cartera_Prenda&tipoSolicitud="+ Ext.getCmp('tipoSolicitud').getValue();
		var fpCargaPrenda = Ext.getCmp('fpCargaPrenda');
		fpCargaPrenda.getForm().submit({
			 url: '38RegSolicRecuersos.ma.jsp?'+parametros,									
			 waitMsg: 'Enviando datos...',
			 waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;
				var resultado = resp.resultado;
				var mensaje = resp.mensaje;
			
				if(resultado=='S') {
					Ext.getCmp('VerArchivoPrenda').destroy();						
					Ext.getCmp('txtcargaPrenda').setValue("S");
					Ext.getCmp('btnAceptarC').show();
					Ext.getCmp('btnConfirmarSolicitud').hide();
						
					Ext.Ajax.request({
						url: '38RegSolicRecuersos.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Carga_PreAcuse',
						ic_solicitud:Ext.getCmp('ic_solicitud1').getValue(),
						ic_contrato:Ext.getCmp("ic_contrato").getValue(),
						ic_if:ic_if,
						pantalla:'Prenda'
					}),
						callback: transmiteCarga_PreAcuse
					});																
						
				}else if(resultado=='N') {
					Ext.MessageBox.alert("Mensaje",mensaje);
					Ext.getCmp('txtcargaPrenda').setValue("N");
					return;
				}								
			}
			,failure: NE.util.mostrarSubmitError
		});			
	}
	var enviarArchivoPrendaPDF = function() {
		var archivo = Ext.getCmp('archivoPrendaPDF');
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}
		var  parametros = "ic_solicitud="+Ext.getCmp('ic_solicitud1').getValue()+"&tipoArchivo=Cartera_Prenda_Pdf&tipoSolicitud="+ Ext.getCmp('tipoSolicitud').getValue();
		var fpCargaPrendaPdf = Ext.getCmp('fpCargaPrendaPDF');
		fpCargaPrendaPdf.getForm().submit({
			 url: '38RegSolicRecuersos.ma.jsp?'+parametros,									
			 waitMsg: 'Enviando datos...',
			 waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;
				var resultado = resp.resultado;
				var mensaje = resp.mensaje;
				if(resultado=='S') {
					Ext.getCmp('VerArchivoPrendaPDF').destroy();						
					Ext.getCmp('txtcargaPrendaPdf').setValue("S");
					Ext.getCmp('btnAceptarC').show();
					Ext.getCmp('btnConfirmarSolicitud').hide();
					Ext.Ajax.request({
						url: '38RegSolicRecuersos.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Carga_PreAcuse',
						ic_solicitud:Ext.getCmp('ic_solicitud1').getValue(),
						ic_contrato:Ext.getCmp("ic_contrato").getValue(),
						ic_if:ic_if,
						pantalla:'Prenda_Pdf'
					}),
						callback: transmiteCarga_PreAcuse
					});																
						
				}else if(resultado=='N') {
					Ext.MessageBox.alert("Mensaje",mensaje);
					Ext.getCmp('txtcargaPrendaPdf').setValue("N");
					return;
				}								
			}
			,failure: NE.util.mostrarSubmitError
		});		
	}
	
	var fpCargaPrenda = {
		xtype: 'form',
		id: 'fpCargaPrenda',
		layout: 'form',	
		width: 600,
		title: '',
		frame: true,	
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 30,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},	
		monitorValid: true,
		items: [	
			{
				xtype: 'panel',
				id:		'pnlArchivo',				
				layout: 'form',
				fileUpload: true,
				labelWidth: 150,					
				items: [
					{
						xtype: 'fileuploadfield',
						id: 'archivoPrenda',
						width: 150,	  
						emptyText: 'Carga Archivo de',
						fieldLabel: 'Carga Archivo de',
						name: 'archivoPrenda',
						buttonText: 'Examinar...',						
						anchor: '90%',
						vtype: 'archivoCsv'
					 }					
				]
			},
			{
				xtype: 'displayfield',
				value: 'El archivo debe tener formato csv'				
			}
		],
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptarPrenda',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivoPrenda
			}
		]
	};
	var fpCargaPrendaPDF = {
		xtype: 'form',
		id: 'fpCargaPrendaPDF',
		layout: 'form',	
		width: 600,
		title: '',
		frame: true,	
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 30,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},	
		monitorValid: true,
		items: [	
			{
				xtype: 'panel',
				id:		'pnlArchivoPDF',				
				layout: 'form',
				fileUpload: true,
				labelWidth: 150,					
				items: [
					{
						xtype: 'fileuploadfield',
						id: 'archivoPrendaPDF',
						width: 150,	  
						emptyText: 'Carga Archivo de',
						fieldLabel: 'Carga Archivo de',
						name: 'archivoPrenda',
						buttonText: 'Examinar...',						
						anchor: '90%',
						vtype: 'archivopdf'
					 }					
				]
			},
			{
				xtype: 'displayfield',
				value: 'El archivo debe tener formato PDF'				
			}
		],
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptarPrendaPDF',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivoPrendaPDF
			}
		]
	};
	
	function cargaCartera(){
		
		var ventana3 = Ext.getCmp('VerArchivoPrenda');
		if (ventana3) {
			ventana3.show();
		} else {
			new Ext.Window({				
				width: 610,	
				id:'VerArchivoPrenda',
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,		
				items: [					  
					fpCargaPrenda
				],
				title: 'Carga Cartera en Prenda',
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	
							xtype: 'button',	
							text: 'Cancelar ',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerrarPrenda', 
							handler: function(){
								Ext.getCmp('VerArchivoPrenda').destroy();								
							} 
						},
						'-'							
					]
				}
			}).show();
		}	
	}
	
	function cargaCarteraPDF(){
		
		var ventana4 = Ext.getCmp('VerArchivoPrendaPDF');
		if (ventana4) {
			ventana4.show();
		} else {
			new Ext.Window({				
				width: 610,	
				id:'VerArchivoPrendaPDF',
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,		
				items: [					  
					fpCargaPrendaPDF
				],
				title: 'Carga PDF Cartera en Prenda',
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	
							xtype: 'button',	
							text: 'Cancelar ',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerrarPrendaPDF', 
							handler: function(){
								Ext.getCmp('VerArchivoPrendaPDF').destroy();								
							} 
						},
						'-'							
					]
				}
			}).show();
		}	
	}
	

	//********************* Proceso para la carga del archivo de cotizaci�n**********************
	var enviarArchivoCotiza = function() {
		
		var archivo = Ext.getCmp('archivoCotiza');
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}
		
		var  parametros = "ic_solicitud="+Ext.getCmp('ic_solicitud1').getValue()+"&tipoArchivo=Cotiza&tipoSolicitud="+Ext.getCmp('tipoSolicitud').getValue();
		var fpCargaCotiza = Ext.getCmp('fpCargaCotiza');
			fpCargaCotiza.getForm().submit({
			 url: '38RegSolicRecuersos.ma.jsp?'+parametros,									
			 waitMsg: 'Enviando datos...',
			 waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;
				var resultado = resp.resultado;
				var mensaje = resp.mensaje;
			
				if(resultado=='S') {
					Ext.MessageBox.alert('Mensaje','El Archivo se ha agregado a la solicitud',
					function(){ 					
						Ext.getCmp('VerArchivoCotiza').destroy();						
						Ext.getCmp('txtcargaCotiza').setValue("S");
					});
					
				}else if(resultado=='N') {
					Ext.MessageBox.alert("Mensaje",mensaje);
					Ext.getCmp('txtcargaCotiza').setValue("N");
					return;
				}								
			}
			,failure: NE.util.mostrarSubmitError
		});			
	}

	var fpCargaCotiza = {
		xtype: 'form',
		id: 'fpCargaCotiza',
		layout: 'form',				
		width: 600,
		title: '',
		frame: true,	
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 30,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},	
		monitorValid: true,
		items: [	
			{
				xtype: 'panel',
				id:		'pnlArchivo',				
				layout: 'form',
				fileUpload: true,
				labelWidth: 150,					
				items: [
					{
						xtype: 'fileuploadfield',
						id: 'archivoCotiza',
						width: 150,	  
						emptyText: 'Carga Archivo de',
						fieldLabel: 'Carga Archivo de',
						name: 'archivoCotiza',
						buttonText: 'Examinar...',						
						anchor: '90%',
						vtype: 'archivopdf'
					 }					
				]
			},
			{
				xtype: 'displayfield',
				value: 'El archivo debe de un formato PDF <br> El tama�o del archivo debe de ser menor a 5MB'			
			}			
		],
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivoCotiza
			}
		]
	};
	
	function cargaCotizacion(){
		
		var ventana4 = Ext.getCmp('VerArchivoCotiza');
		
		if (ventana4) {
			ventana4.show();
		} else {
			new Ext.Window({				
				width: 610,	
				id:'VerArchivoCotiza',
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,		
				items: [					  
					fpCargaCotiza
				],
				title: 'Carga Cotizaci�n',
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{	
							xtype: 'button',	
							text: 'Cancelar ',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerrarC', 
							handler: function(){
								Ext.getCmp('VerArchivoCotiza').destroy();
							} 
						},
						'-'							
					]
				}
			}).show();
		}	
	}
	
	//**************Inicializa Valores **************************************
	function procesaValoresIniciales(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
					
					var fp = Ext.getCmp('forma');
					fp.el.unmask();		
					
				if(jsonData.esdiaInhabil =='S' ||  jsonData.fueradHorario=='S' )  {	// quitar N y colocar 'S'
					Ext.getCmp("divRegistro").hide();
					Ext.getCmp("divContratos").hide();
					Ext.getCmp("divContratosOE").hide();
					Ext.getCmp("divSolicitud").hide();
					Ext.getCmp("divPrestamo").hide();
					Ext.getCmp("comTasaArchCotiza").hide();
					Ext.getCmp("btnCargaTabla").hide();
					Ext.getCmp("btnCartera").hide();
					Ext.getCmp("btnPDFCartera").hide();
					//Ext.getCmp("btnRUG").hide();
					Ext.getCmp("btnContinuar").hide();
					
					if(jsonData.esdiaInhabil =='S')  {
						Ext.getCmp('mensajeServicio').update(jsonData.mensajeServicio);					
					}else  	if(jsonData.fueradHorario =='S')  {
					Ext.getCmp('mensajeHorario').update(jsonData.mensajeHorario);	
					}
				}else  {
				
					Ext.getCmp("fecha_Solicitud").update(jsonData.fecha_Solicitud);						
					Ext.getCmp('lblNombreIF').update(jsonData.lblNombreIF);
					Ext.getCmp('lblNumCuenta').update(jsonData.lblNumCuenta);
					Ext.getCmp('lblBanco').update(jsonData.lblBanco);
					Ext.getCmp('lblCuentaCLABE').update(jsonData.lblCuentaCLABE);
					Ext.getCmp('lblNomContrato').update(jsonData.lblNomContrato);
					Ext.getCmp('lblFechaFirmaContrato').update(jsonData.lblFechaFirmaContrato);
					if(jsonData.lblModificatorios!='0') {
						Ext.getCmp('lblModificatorios').update(jsonData.lblModificatorios);
						Ext.getCmp('lblModificatorios').show();
					}else  {
						Ext.getCmp('lblModificatorios').hide();
					}
					Ext.getCmp('lblNomContratoOE').update(jsonData.lblNomContratoOE);
					Ext.getCmp('lblFechaFirmaContratoOE').update(jsonData.lblFechaFirmaContratoOE);
					Ext.getCmp('lblUsuarioIF').update(jsonData.lblUsuarioIF);
					Ext.getCmp('ic_solicitud1').setValue(jsonData.ic_solicitud);
					Ext.getCmp('ic_contrato').setValue(jsonData.ic_contrato);
					Ext.getCmp('mensajeHorario').update(jsonData.mensajeHorario);								
					Ext.getCmp("tipoSolicitud").setValue(jsonData.tipoSolicitud);					
					Ext.getCmp("strPerfil").setValue(jsonData.strPerfil);
					Ext.getCmp('lblFondoLiquido').update(jsonData.lblFondoLiquido);
										
					if(jsonData.strPerfil =='IF 4CP' ||  jsonData.strPerfil =='IF 5CP')  {
						Ext.getCmp("btnCartera").show();
						Ext.getCmp("btnPDFCartera").show();
						//Ext.getCmp("btnRUG").show();
					}	
					
					if(jsonData.strPerfil =='IF 5MIC' ||  jsonData.strPerfil =='IF 5CP'   ||  jsonData.strPerfil =='IF 4MIC')  {
						Ext.getCmp("lblFondoLiquidos").show();
					}
					
					if(jsonData.tipoSolicitud =='Modificar'){
						Ext.getCmp("_acuse").setValue(jsonData._acuse);
						Ext.getCmp("importe1").setValue(jsonData.importe);
						Ext.getCmp("hidclaveMoneda1").setValue(jsonData.claveMoneda);
						Ext.getCmp("desRecuersos1").setValue(jsonData.desRecuersos);
						Ext.getCmp("fecha_PagoCapital").setValue(jsonData.fecha_PagoCapital);
						Ext.getCmp("fecha_PagoInteres").setValue(jsonData.fecha_PagoInteres);
						Ext.getCmp("fecha_vencimiento").setValue(jsonData.fecha_vencimiento);
						Ext.getCmp("cboInteres1").setValue(jsonData.cboInteres);
						Ext.getCmp("tasaInteres1").setValue(jsonData.tasaInteres);
						Ext.getCmp("observaciones1").setValue(jsonData.observaciones);
						Ext.getCmp("divRegistro").setTitle('Reenv�o de Solicitud de Recursos');
						Ext.getCmp("hidcombTasa1").setValue(jsonData.combTasa);	
						
						if(jsonData.cboInteres=='Fija') {
							Ext.getCmp('tasaInteres1').show();
							Ext.getCmp('combTasa1').disable();
						}else  if(jsonData.cboInteres=='Variable')  {
							Ext.getCmp('tasaInteres1').show();
							Ext.getCmp('combTasa1').enable();
						}
											
						catTasasInteresData.load({
							params : Ext.apply({
								claveMoneda:jsonData.claveMoneda,
								cboInteres:jsonData.cboInteres
							})
						});
						
					}
					
					if(jsonData.totalFechas>0) {
					
						var fechas  =  jsonData.fechas;
						var forma= Ext.getCmp('forma');	
						var x =0;
						
						for(i=0;i<jsonData.totalFechas;i++){	
							var lbfechas			= eval('fechas.lblDescCampo'+i);  
							var lbfechas_d	=  Ext.decode(lbfechas);
							var campo = 'lblDescCampo'+x;
							
							if(i==0) {
								var indice1 = forma.items.indexOfKey('divContratos')+1;
								forma.insert(indice1,lbfechas_d);	
							}else {												
								var indice1 = forma.items.indexOfKey(campo)+1;
								forma.insert(indice1,lbfechas_d);	
								x++;
							}							
						}
						forma.doLayout();  					
					}
					
					catMonedaData.load();	
				}					
			}
		} else {
			
			NE.util.mostrarConnError(response,opts);
		}
	}	
	

	
	//**************Catalogos **************************************
	var catTasasData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			["Fija",'FIJA'],
			["Variable",'VARIABLE']			
		 ]
	});	

	var procesarCatMonedaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			
			var claveMoneda1 = Ext.getCmp('claveMoneda1');
			var hidclaveMoneda1 = Ext.getCmp('hidclaveMoneda1');				
			if(hidclaveMoneda1.getValue()!=''){
				claveMoneda1.setValue(hidclaveMoneda1.getValue());
			}
		}
  }
  
	var catMonedaData = new Ext.data.JsonStore({
		id: 'catMonedaData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '38RegSolicRecuersos.data.jsp',
		baseParams: {
			informacion: 'catMonedaData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var procesarCatTasasInteresData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var combTasa1 = Ext.getCmp('combTasa1');
			var hidcombTasa1 = Ext.getCmp('hidcombTasa1');				
			if(hidcombTasa1.getValue()!=''){
				combTasa1.setValue(hidcombTasa1.getValue());
			}
		}
  }
  
  
	var catTasasInteresData = new Ext.data.JsonStore({
		id: 'catTasasInteresData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '38RegSolicRecuersos.data.jsp',
		baseParams: {
			informacion: 'catTasasInteresData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatTasasInteresData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	//**************Datos de la Foma  **************************************
	
	var datosReg_izq = {
		xtype			: 'fieldset',
		id 			: 'datosReg_izq',
		border		: false,
		labelWidth	: 250,
		heigth:	900,
		items			: 
		[	
			{
				xtype: 'displayfield',
				id: 'fecha_Solicitud',
				style: 'text-align:left;',
				fieldLabel: 'Fecha Solicitud ',
				text: '-'
			},
			
			{
				xtype: 'displayfield',
				id: 'lblNombreIF',
				style: 'text-align:left;',
				fieldLabel: 'Nombre IF ',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblNumCuenta',
				style: 'text-align:left;',
				fieldLabel: 'N�mero de Cuenta',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblBanco',
				style: 'text-align:left;',
				fieldLabel: 'Banco',
				text: '-'
			},		
			{
				xtype: 'displayfield',
				id: 'lblCuentaCLABE',
				style: 'text-align:left;',
				fieldLabel: 'Cuenta CLABE',
				text: '-'
			}
		]
	};

	
	var datosReg_izqPre_Acuse_2 = {
		xtype			: 'fieldset',
		id 			: 'datosReg_izqPre_Acuse_2',
		border		: false,
		labelWidth	: 250,
		width		: 800,
		items			:  [			
			{
				xtype: 'displayfield',
				id: 'lblFecha_SolicitudPre',
				style: 'text-align:left;',
				fieldLabel: 'Fecha Solicitud ',				
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblNombreIFPre',
				style: 'text-align:left;',
				fieldLabel: 'Nombre IF ',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblNumCuentaPre',
				style: 'text-align:left;',
				fieldLabel: 'N�mero de Cuenta',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblBancoPre',
				style: 'text-align:left;',
				fieldLabel: 'Banco',
				text: '-'
			},		
			{
				xtype: 'displayfield',
				id: 'lblCuentaCLABPre',
				style: 'text-align:left;',
				fieldLabel: 'Cuenta CLABE',
				text: '-'
			}
		]
	};
	
	
	
	var datosReg_izqAcuse = {
		xtype			: 'fieldset',
		id 			: 'datosReg_izqAcuse',
		border		: false,
		labelWidth	: 250,
		width		: 800,
		heigth:	800,
		items			: 
		[	
			{
				xtype: 'label',
				id: 'lblAcuseResumen',
				style: 'font-weight:bold;text-align:center;display:block;',
				fieldLabel: '',
				text: ''
			},		
			NE.util.getEspaciador(20),
			gridResumen	, 						
			{
				xtype: 'label',
				id: 'mensajeModifiAcuse',				
				fieldLabel: '',
				text: ''
			},	
			NE.util.getEspaciador(20),
			{
				xtype: 'displayfield',
				id: 'lblFecha_SolicitudAcuse',
				style: 'text-align:left;',
				fieldLabel: 'Fecha Solicitud ',
				text: '-'
			},			
			{
				xtype: 'displayfield',
				id: 'lblNombreIFAcuse',
				style: 'text-align:left;',
				fieldLabel: 'Nombre IF ',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblNumCuentaAcuse',
				style: 'text-align:left;',
				fieldLabel: 'N�mero de Cuenta',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblBancoAcuse',
				style: 'text-align:left;',
				fieldLabel: 'Banco',
				text: '-'
			},		
			{
				xtype: 'displayfield',
				id: 'lblCuentaCLABEAcuse',
				style: 'text-align:left;',
				fieldLabel: 'Cuenta CLABE',
				text: '-'
			}
		]
	};
	
	
	
	var datosReg_izqAcuse = {
		xtype			: 'fieldset',
		id 			: 'datosReg_izqAcuse',
		border		: false,
		labelWidth	: 250,
		width		: 800,		
		items			: 
		[	
			{
				xtype: 'label',
				id: 'lblAcuseResumen',
				style: 'font-weight:bold;text-align:center;display:block;',
				fieldLabel: '',
				text: ''
			},				
			gridResumen				
			]
		};
			
	var datosReg_izqAcuse_2 = {
		xtype			: 'fieldset',
		id 			: 'datosReg_izqAcuse_2',
		border		: false,
		labelWidth	: 250,
		width		: 800,
		//heigth:	800,
		items			: 	[		
			{
				xtype: 'displayfield',
				id: 'lblFecha_SolicitudAcuse',
				style: 'text-align:left;',
				fieldLabel: 'Fecha Solicitud ',
				text: '-'
			},			
			{
				xtype: 'displayfield',
				id: 'lblNombreIFAcuse',
				style: 'text-align:left;',
				fieldLabel: 'Nombre IF ',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblNumCuentaAcuse',
				style: 'text-align:left;',
				fieldLabel: 'N�mero de Cuenta',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblBancoAcuse',
				style: 'text-align:left;',
				fieldLabel: 'Banco',
				text: '-'
			},		
			{
				xtype: 'displayfield',
				id: 'lblCuentaCLABEAcuse',
				style: 'text-align:left;',
				fieldLabel: 'Cuenta CLABE',
				text: '-'
			}
		]
	};
	
	
	
	var contratos_izq = {
		xtype			: 'fieldset',
		id 			: 'contratos_izq',
		border		: false,
		labelWidth	: 250,
		items			:  [ 
			{
				xtype: 'displayfield',
				id: 'lblNomContrato',
				style: 'text-align:left;',
				fieldLabel: 'Nombre Contrato',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblFechaFirmaContrato',
				style: 'text-align:left;',
				fieldLabel: 'Fecha Firma Contrato',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblModificatorios',
				style: 'text-align:left;',
				fieldLabel: 'Contratos Modificatorios',
				text: '-'
			}
		]
	};
	
	var contratos_izqOE = {
		xtype			: 'fieldset',
		id 			: 'contratos_izqOE',
		border		: false,
		labelWidth	: 250,
		items			:  [ 
			{
				xtype: 'displayfield',
				id: 'lblNomContratoOE',
				style: 'text-align:left;',
				fieldLabel: 'Nombre Contrato',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblFechaFirmaContratoOE',
				style: 'text-align:left;',
				fieldLabel: 'Fecha Firma Contrato Operaci�n Electr�nica',
				text: '-'
			}		
		]
	};	
	
	var solicitud_izq = {
		xtype			: 'fieldset',
		id 			: 'solicitud_izq',
		border		: false,
		labelWidth	: 250,
		align: 'center',
		items			:  [  			
			{	
				width: 40,	
				fieldLabel: 'Importe',
				xtype:'bigdecimal',		
				maxText:'El tama�o m�ximo del campos es de 17 enteros 2 decimales',				
				maxValue: '99999999999999999.99', 
				allowDecimals: true,
				allowNegative: false,							
				hidden: 			false,	
				allowBlank: true,
				msgTarget: 		'side',
				anchor:			'-20',						
				format:			'00000000000000000.00',
				id:'importe1',	
				name:'importe'	,
				listeners:{
					blur: function(field){ 								
						if(field.getValue() != '') {	
							Ext.Ajax.request({
								url: '38RegSolicRecuersos.data.jsp',
								params: {
									informacion: 'validaMonto',
									importe:field.getValue()									
								},
								callback: procesarImporte
							});							
						}
					}
				}				
			},						
			{
				xtype: 'combo',
				name: 'claveMoneda',
				id: 'claveMoneda1',
				fieldLabel: 'Moneda',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'claveMoneda',
				emptyText: 'Seleccione...',					
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				allowBlank: true,
				store : catMonedaData,
				tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
					select: {
						fn: function(combo) {
							var combTasa = Ext.getCmp('combTasa1').getValue();	
							var cboInteres = Ext.getCmp('cboInteres1');	
							var cmbTasa = Ext.getCmp('combTasa1');								
							cmbTasa.store.load({
								params: {
									claveMoneda:combo.getValue(),
									cboInteres:cboInteres.getValue()
								}
							});
							cmbTasa.setValue('');
							Ext.getCmp('hidcombTasa1').setValue('');								
							
						}
					}
				}	
			}		
		]
	};
	
	var prestamo_izq = {
		xtype			: 'fieldset',
		id 			: 'prestamo_izq',
		border		: false,
		labelWidth	: 250,
		width: 800,		
		items			:  [
			{
				xtype: 'textfield',
				name: 'desRecuersos',
				fieldLabel: 'Destino de los Recursos',
				id: 'desRecuersos1',
				maxLength: 80,
				width: 300,
				msgTarget: 'side'
			},
			{
				xtype			: 'datefield',
				id          : 'fecha_PagoCapital',
				name			: 'fecha_PagoCapital',
				allowBlank	: false,
				minValue: '01/01/1901',
				width			: 130,
				msgTarget	: 'side',
				fieldLabel  : 'Fecha de primer pago de Capital',
				margins		: '0 20 0 0'
			},
			{
				xtype			: 'datefield',
				id          : 'fecha_PagoInteres',
				name			: 'fecha_PagoInteres',
				allowBlank	: false,
				minValue: '01/01/1901',
				width			: 130,
				msgTarget	: 'side',
				fieldLabel  : 'Fecha de primer pago de Intereses',
				margins		: '0 20 0 0'
			},
			{
				xtype			: 'datefield',
				id          : 'fecha_vencimiento',
				name			: 'fecha_vencimiento',
				allowBlank	: false,
				minValue: '01/01/1901',
				width			: 130,
				msgTarget	: 'side',
				fieldLabel  : 'Fecha de vencimiento',
				margins		: '0 20 0 0'
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Tasa de Inter�s',
				combineErrors: false,
				msgTarget: 'side',
				id: 'campoCompuesto8',
				width: 800,
				items: [
					{
						xtype: 'combo',
						name: 'cboInteres',
						id: 'cboInteres1',
						fieldLabel: '',
						mode: 'local', 
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'cboInteres',
						emptyText: 'Seleccione...',					
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						allowBlank: true,
						width: 85,
						store : catTasasData,						
						listeners: {
							select: {
								fn: function(combo) {
								
									if(combo.getValue()=='Fija') {
										Ext.getCmp('tasaInteres1').show();
										Ext.getCmp('combTasa1').disable();
										Ext.getCmp('combTasa1').setValue('');
										Ext.getCmp('displayTxtTasa').setValue('puntos porcentuales');
									}else  if(combo.getValue()=='Variable')  {
										Ext.getCmp('tasaInteres1').show();
										Ext.getCmp('combTasa1').enable();
										Ext.getCmp('displayTxtTasa').setValue('puntos adicionales a la tasa de referencia');
									}
									
									var claveMoneda1 = Ext.getCmp('claveMoneda1');
									var combTasa1 = Ext.getCmp('combTasa1');
									
									if(claveMoneda1.getValue() !='' )  {
										combTasa1.setValue('');
										Ext.getCmp('hidcombTasa1').setValue('');		
										combTasa1.store.load({
											params: {
												cboInteres:combo.getValue(),
												claveMoneda:claveMoneda1.getValue()
											}
										});
									}
									
									
								}
							}
						}
					},					
					{
						xtype: 'combo',
						name: 'combTasa',
						id: 'combTasa1',
						fieldLabel: '',
						mode: 'local', 
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'combTasa',
						emptyText: 'Seleccione...',					
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						allowBlank: true,
						width: 180,
						store : catTasasInteresData						
					},
					{	
						width:			70,
						fieldLabel: '',
						xtype:'bigdecimal',		
						maxText:'El tama�o m�ximo del campos es de 3 enteros 5 decimales',				
						maxValue: '999.99999', 
						allowDecimals: true,
						allowNegative: false,							
						hidden: 			false,	
						allowBlank: false,
						msgTarget: 		'side',
						anchor:			'-20',						
						format:			'000.00000',
						id:'tasaInteres1',	
						name:'tasaInteres'	
					},		
					{
						xtype: 'displayfield',
						id: 'displayTxtTasa',
						value: '',
						height: 50,
						width: 130
					}						
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: '',
				combineErrors: false,
				msgTarget: 'side',
				id: 'campoCompuesto9',
				width: 800,
				items: [
					{
						xtype: 'displayfield',
						value: ''						
					}	,
					{
						xtype: 'button',
						text: 'Carga Cotizaci�n',			
						iconCls: 'icoLimpiar',
						id: 'btnCargaCotizacion',					
						handler: cargaCotizacion
					}
				]
			},
			{
				xtype:				'textarea',
				id:					'observaciones1',
				name:					'observaciones',
				height:				60,
				width:				300,
				maxLength:			800,
				fieldLabel:			'Observaciones',
				allowBlank:			true,
				enableKeyEvents:	true			
			},
			{
				xtype: 'displayfield',
				id: 'lblUsuarioIF',
				style: 'text-align:left;',
				fieldLabel: 'Usuario IF ',
				text: '-'
			}
		]
	};
	
	var prestamo_izq_PreAcuse = {
		xtype			: 'fieldset',
		id 			: 'prestamo_der_PreAcuse',
		border		: false,
		labelWidth	: 250,
		width: 800,
		items			:  [
			{
				xtype: 'displayfield',
				id: 'lblDesRecuersos',
				style: 'text-align:left;',
				fieldLabel: 'Destino de los Recursos',
				align: 'right',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblFecha_PagoCapital',
				style: 'text-align:left;',
				fieldLabel: 'Fecha de primer pago de Capital',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblFecha_PagoInteres',
				style: 'text-align:left;',
				fieldLabel: 'Fecha de primer pago de Intereses',
				text: '-'
			},
			{
				xtype: 'displayfield',
				id: 'lblFecha_vencimiento',
				style: 'text-align:left;',
				fieldLabel: 'Fecha de vencimiento',
				text: '-'
			}		
		]
	};
	
		var prestamo_izq_PreAcuse_2 = {
		xtype			: 'fieldset',
		id 			: 'prestamo_izq_PreAcuse_2',
		border		: false,
		labelWidth	: 250,
		width: 800,
		items			:  [
			{
				xtype: 'textarea',
				id: 'lblObservaciones',
				style: 'text-align:left;',
				fieldLabel: 'Observaciones',
				text: '-',
				width: 500,				
				readOnly  : true,
				style: 		'background: gainsboro;'
			},
			{
				xtype: 'displayfield',
				id: 'lblUsuarioIFPreAcuse',
				style: 'text-align:left;',
				fieldLabel: 'Usuario IF ',
				text: '-'
			}
		]
	};
	
	var datosTabla = {
		xtype			: 'fieldset',
		id 			: 'datosTabla',
		border		: false,
		labelWidth	: 250,
		width		: 800,
		//heigth:	800,
		items			: 	[	gridTabla	]
		};
	
	
	
	var lblFondoLiquido1 = {
		xtype			: 'fieldset',
		id 			: 'lblFondoLiquido1',
		border		: false,
		labelWidth	: 250,
		width: 800,		
		items			:  [
			{
				xtype: 'displayfield',
				id: 'lblFondoLiquido',
				style: 'text-align:left;',
				fieldLabel: 'Fondo L�quido',
				text: '-'				
			}			
		]
	};
	
	
	
	var fp = new Ext.form.FormPanel({
		id					: 'forma',
		layout			: 'form',
		width				: 850,
		style				: ' margin:0 auto;',
		frame				: true,		
		collapsible		: false,
		titleCollapse	: false	,
		bodyStyle		: 'padding: 8px',
		labelWidth			: 250,
		monitorValid: true,
		defaults: { 	msgTarget: 'side', 	anchor: '-20'
		},
		items				: 
		[  
			{
				xtype: 'label',
				width		: 500,	
				id: 'mensajeServicio',
				align: 'center',
				value: ''
			},		
			{
				xtype: 'label',
				width		: 800,	
				id: 'mensajeHorario',
				align: 'center',
				value: ''
			},				
			{
				layout	: 'hbox',
				title		: 'Registro de Solicitud de Recursos',
				id			: 'divRegistro',
				align    : 'center',
				width		: 800,
				items		: [datosReg_izq ]
			},
			{
				layout	: 'hbox',
				title		: 'Pre acuse - Resumen de la Solicitud',
				hidden	: true,
				id			: 'divPreAcuse',
				align    : 'center',
				width		: 800				
			},	
			{
				width		: 800,	
				xtype: 'label',
				id: 'montoMoneda',	
				align: 'center',
				text: ''
			},	
			{
				width		: 800,	
				xtype: 'label',
				id: 'mensajeModifiPre',	
				align: 'center',
				text: ''
			},		
			{
				layout	: 'hbox',
				title		: '',
				hidden	: true,
				id			: 'divPreAcuse_2',
				align    : 'center',
				width		: 800,						
				items		: [datosReg_izqPre_Acuse_2]
			},
			{
				layout	: 'hbox',
				title		: 'Acuse - Resumen de  Solicitud',
				hidden	: true,
				id			: 'divAcuseResumen',
				align    : 'center',
				width		: 800,
				items		: [datosReg_izqAcuse]
			},		
			{
				width		: 800,	
				xtype: 'label',
				id: 'mensajeModifiAcuse',	
				align: 'center',
				text: ''
			},	
			{
				layout	: 'hbox',
				title		: '',
				hidden	: true,
				id			: 'divAcuseResumen_2',
				align    : 'center',
				width		: 800,				
				items		: [datosReg_izqAcuse_2]
			},					
			{
				layout	: 'hbox',
				title		: 'Contratos(s)',
				id			: 'divContratos',
				width		: 800,
				items		: [contratos_izq ]
			},				
			{
				layout	: 'hbox',
				title		: '',
				id			: 'divContratosOE',
				width		: 800,
				items		: [contratos_izqOE]
			},				
			{
				layout	: 'hbox',
				title		: 'Monto',
				id			: 'divSolicitud',
				width		: 800,
				items		: [solicitud_izq ]
			},
			{
				layout	: 'hbox',
				title		: 'Datos del Pr�stamo',
				id			: 'divPrestamo',
				width		: 800,
				items		: [prestamo_izq ]
			},
			{
				layout	: 'hbox',
				title		: 'Datos del Pr�stamo',
				id			: 'divPrestamoPreAcuse',
				hidden	  : true,
				width		: 800,
				items		: [prestamo_izq_PreAcuse ]
			},		
			
			{
				xtype: 'compositefield',
				fieldLabel: '&nbsp;&nbsp;Tasa de Inter�s',
				id: 'comTasaArchCotiza',						
				hidden	  : true,
				width: 800,				
				items: [		
					{
						xtype: 'displayfield',
						id: 'lblTasaInteres',
						style: 'text-align:left;',
						fieldLabel: 'Tasa de Inter�s',
						text: '-',
						width: 300
					},						
					{
						xtype: 'displayfield',
						value: ' ',
						width: 20
					},					
					{
						xtype: 'button',
						text: '',			
						iconCls: 'icoPdf',
						id: 'btnDescargCotiza',				
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '38RegSolicRecuersos.data.jsp',
								params: {
									informacion: 'ArchivosSolic',
									ic_solicitud:Ext.getCmp("ic_solicitud1").getValue(),
									ic_contrato:Ext.getCmp("ic_contrato").getValue(),
									pantalla:Ext.getCmp("pantalla").getValue(),
									ic_if:ic_if,
									tipoArchivo:'Cotiza',
									extension:'pdf',
									tipoSolicitud: Ext.getCmp('tipoSolicitud').getValue()
								},
								callback: procesarDescargaArch
							});
						}
					}
				]
			},		
			{
				xtype: 'compositefield',
				fieldLabel: '&nbsp;&nbsp;Carga Cartera  Prenda',
				id: 'carteraPrenda',						
				hidden	  : true,
				width: 800,				
				items: [						
					{
						xtype: 'displayfield',
						value: ' ',
						width: 20
					},					
					{
						xtype: 'button',
						text: '',			
						iconCls: 'icoXls',
						id: 'btnDescargPrenda',				
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '38RegSolicRecuersos.data.jsp',
								params: {
									informacion: 'ArchivosSolic',
									ic_solicitud:Ext.getCmp("ic_solicitud1").getValue(),
									ic_contrato:Ext.getCmp("ic_contrato").getValue(),
									pantalla:Ext.getCmp("pantalla").getValue(),
									ic_if:ic_if,
									tipoArchivo:'Cartera',
									extension:'csv',
									tipoSolicitud: Ext.getCmp('tipoSolicitud').getValue()
								},
								callback: procesarDescargaArch
							});
						}
					}
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: '&nbsp;&nbsp;Carga PDF Cartera en Prenda',
				id: 'carteraPrendaPdf',						
				hidden	  : true,
				width: 800,				
				items: [						
					{
						xtype: 'displayfield',
						value: ' ',
						width: 20
					},					
					{
						xtype: 'button',
						text: '',			
						iconCls: 'icoPdf',
						id: 'btnDescargPrendaPdf',				
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '38RegSolicRecuersos.data.jsp',
								params: {
									informacion: 'ArchivosSolic',
									ic_solicitud:Ext.getCmp("ic_solicitud1").getValue(),
									ic_contrato:Ext.getCmp("ic_contrato").getValue(),
									pantalla:Ext.getCmp("pantalla").getValue(),
									ic_if:ic_if,
									tipoArchivo:'Cartera_Pdf',
									extension:'pdf',
									tipoSolicitud: Ext.getCmp('tipoSolicitud').getValue()
								},
								callback: procesarDescargaArch
							});
						}
					}
				]
			},	
			/*{
				xtype: 'compositefield',
				fieldLabel: '&nbsp;&nbsp;PDF Comprobante RUG',
				id: 'comprobanteRUG',						
				hidden	  : true,
				width: 800,				
				items: [		
				{
						xtype: 'displayfield',
						value: ' ',
						width: 20
					},					
					{
						xtype: 'button',
						text: '',			
						iconCls: 'icoPdf',
						id: 'btnDescargRUG',				
						handler: function(boton, evento) {
							Ext.Ajax.request({
								url: '38RegSolicRecuersos.data.jsp',
								params: {
									informacion: 'ArchivosSolic',
									ic_solicitud:Ext.getCmp("ic_solicitud1").getValue(),
									ic_contrato:Ext.getCmp("ic_contrato").getValue(),
									pantalla:Ext.getCmp("pantalla").getValue(),
									ic_if:ic_if,
									tipoArchivo:'Boleta',
									extension:'pdf',
									tipoSolicitud: Ext.getCmp('tipoSolicitud').getValue()
								},
								callback: procesarDescargaArch
							});
						}
					}
				]
			},	
			*/
			{
				layout	: 'hbox',
				title		: '',
				id			: 'divPrestamoPreAcuse_2',
				hidden	  : true,
				width		: 800,
				items		: [prestamo_izq_PreAcuse_2 ]
			},					
			{
				layout	: 'hbox',
				title		: '',
				id			: 'lblFondoLiquidos',
				hidden	  : true,
				width		: 800,
				items		: [lblFondoLiquido1 ]
			},
			//Tabla de Amortizaci�n
			{
				layout	: 'hbox',
				title		: '',
				hidden	  : true,
				id			: 'divAmortizacion',
				align    : 'center',
				width		: 800,
				//height	: 300,				
				items		: [datosTabla ]
			},
			{
				width		: 800,	
				xtype: 'label',
				id: 'mensajeTasa',	
				align: 'center',
				text: ''
			},				
			{ 	xtype: 'textfield',  hidden: true, id: 'ic_solicitud1', 	value: '' },
			{ 	xtype: 'textfield', 	hidden: true, id: 'hayAmortizacion', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'ic_contrato', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'txtcargaCotiza', 	value: 'N' },
			{ 	xtype: 'textfield',  hidden: true, id: 'tipoSolicitud', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: '_acuse', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'pantalla', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'txtcargaPrenda', 	value: 'N' },
			{ 	xtype: 'textfield',  hidden: true, id: 'txtcargaPrendaPdf', 	value: 'N' },
		//	{ 	xtype: 'textfield',  hidden: true, id: 'txtcargaRUG', 	value: 'N' },
			{ 	xtype: 'textfield',  hidden: true, id: 'strPerfil', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'hidclaveMoneda1', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'hidcombTasa1', 	value: '' },
			{ 	xtype: 'textfield',  hidden: true, id: 'rebaseLimite', 	value: '' }		
			
		],
		buttons: [	
			{
				text: 'Carga Tabla de Amortizaciones',
				id: 'btnCargaTabla',
				iconCls: 'icon-register-add',
				formBind: true,
				handler: cargaTabla
			},	
			{
				text: 'Carga Cartera en Prenda',
				id: 'btnCartera',
				iconCls: 'icoXls',
				formBind: true,
				hidden : true,
				handler: cargaCartera
			},	
			{
				text: 'Cargar PDF Cartera en Prenda',
				id: 'btnPDFCartera',
				iconCls: 'icoPdf',
				formBind: true,
				hidden : true,
				handler: cargaCarteraPDF
			},	
			/*{
				text: 'PDF Comprobante RUG',
				id: 'btnRUG',
				iconCls: 'icoPdf',
				formBind: true,
				hidden : true,
				handler: cargaRUG
			},		*/		
			{
				text: 'Continuar',
				id: 'btnContinuar',
				iconCls: 'icoAceptar',
				formBind: true,			
				handler: validaContinua
			},
			{
				text: 'Confirmar y Enviar Solicitud',
				id: 'btnConfirmarSolicitud',
				iconCls: 'icon-register-add',
				formBind: true,
				hidden : true,
				handler: ConfirmaSolicitud
			},			
			{
				text: 'Imprimir Acuse ',
				id: 'btnImprimirAcuse',
				iconCls: 'icoImprimir',
				formBind: true,
				hidden	  : true	,
				handler: function(boton, evento) {
					Ext.Ajax.request({
						url: '38RegSolicRecuersos.data.jsp',
						params: {
							informacion: 'ImprimirAcuse',
							ic_solicitud:Ext.getCmp("ic_solicitud1").getValue(),
							ic_contrato:Ext.getCmp("ic_contrato").getValue(),
							ic_if:ic_if
						},
						callback: procesarDescargaArch
					});
				}				
			},
			{
				text: 'Salir',
				id: 'btnSalir',
				iconCls: 'cancelar',
				formBind: true,
				hidden	  : true,
				handler: function() {
					if(tipoSolicitud =='Modificar'){
						window.location = '38ConsOperaciones.jsp';
					}else  {
						window.location = '38RegSolicRecuersos.jsp';		
					}
				}
			},	
			{
				text: 'Aceptar',
				id: 'btnAceptarC',
				iconCls: 'icoAceptar',
				hidden	  : true,
				handler: function() {
					Ext.MessageBox.alert('Mensaje','El Archivo se ha agregado a la solicitud',
					function(){ 
					
						Ext.getCmp("divRegistro").show();
						Ext.getCmp("divContratos").show();
						Ext.getCmp("divContratosOE").show();
						Ext.getCmp("divSolicitud").show();
						Ext.getCmp("divPrestamo").show();
						Ext.getCmp("comTasaArchCotiza").show();
						Ext.getCmp("btnCargaTabla").show();
						Ext.getCmp("btnCartera").show();
						Ext.getCmp("btnPDFCartera").show();
						//Ext.getCmp("btnRUG").show();
						Ext.getCmp("btnContinuar").show();
						
						Ext.getCmp("divPreAcuse").hide();	
						Ext.getCmp("btnAceptarC").hide();	
						Ext.getCmp("mensajeModifiPre").hide();		
						
						Ext.getCmp("btnCancelar").hide();
						Ext.getCmp("divAcuseResumen").hide();	
						Ext.getCmp("divAcuseResumen_2").hide();	
						Ext.getCmp("divPreAcuse").hide();	
						Ext.getCmp("divPreAcuse_2").hide();
						
						Ext.getCmp('divAmortizacion').hide();
						Ext.getCmp('divPrestamoPreAcuse').hide();
						Ext.getCmp('divPrestamoPreAcuse_2').hide();
						
						Ext.getCmp('comTasaArchCotiza').hide();
						Ext.getCmp('carteraPrenda').hide();
						Ext.getCmp('carteraPrendaPdf').hide();
					//	Ext.getCmp('comprobanteRUG').hide();
						Ext.getCmp('divAmortizacion').hide();
						Ext.getCmp('mensajeTasa').hide();
					});	
				}
			},
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				iconCls: 'cancelar',
				formBind: true,
				hidden	  : true,
				handler: function() {
					Ext.MessageBox.alert('Aviso','El usuario a cancelado operaci�n',
						function(){
							if(tipoSolicitud =='Modificar'){
								window.location = '38ConsOperaciones.jsp';
							}else  {
								window.location = '38RegSolicRecuersos.jsp';		
							}
						}
					);
				}
			}	
		]
	});
		
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,						
			NE.util.getEspaciador(20)			
		]
	});
	


  
 //Peticion para obtener valores iniciales y la parametrizaci�n
 fp.el.mask('Cargando...', 'x-mask-loading');			
	Ext.Ajax.request({
		url: '38RegSolicRecuersos.data.jsp',
		params: {
			informacion: "valoresIniciales",
			ic_solicitud:ic_solicitud,
			tipoSolicitud:tipoSolicitud,
			ic_if:ic_if
		},
		callback: procesaValoresIniciales
	});
	
 
});
