Ext.onReady(function() {
//--------------------------Handlers------------------------------------
var nombreArchivo;
var periodo;
var numeroIFNB;  
var validaIFNB;   
var strPerfil = Ext.getDom('auxStrPerfil').value;

var procesaValidacionArchivo = function(opts, success, response) {
		panelFormaValidaInicial.el.unmask();
		var resp = 	Ext.util.JSON.decode(response.responseText);
		if (success == true &&resp.success == true) {
			Ext.getCmp('numeroIFNB').setValue(resp.numeroIFNB);
			Ext.getCmp('nombreIFNB').setValue(resp.nombreIFNB);
			Ext.getCmp('periodo').setValue(resp.periodoFinal);
			Ext.getCmp('numeroArchivo').setValue(resp.numeroArchivo);
			Ext.getCmp('fecha').setValue(resp.fechaHoy);
			Ext.getCmp('hora').setValue(resp.horaHoy);
			panelFormaValidaInicial.setVisible(false);
			panelFormaTransferenciaFinal.setVisible(true);
		} else {
				NE.util.mostrarConnError(response,opts);
			}
	}
	var procesaErrores = function(opts, success, response) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		if (success == true &&resp.success == true) {
			storeRegErroneosData.loadData(resp.regErroneos);
			if(resp.regErroneos!=""){
				Ext.getCmp('gridRegErroneos').show();
				return;
			}else{
				Ext.getCmp('gridRegErroneos').hide();
			}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}

var procesaCargaArchivo = function(opts, success, response) {
		var resp = 	Ext.util.JSON.decode(response.responseText);
		if (success == true &&resp.success == true) {
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');

		if(!Ext.isEmpty(resp.mensaje)){
				Ext.Msg.alert(
					'Mensaje',
					resp.mensaje,
					function(btn, text){
						//cargaArchivo(resp.estadoSiguiente,resp);
					}
				);
		}else{
			if(strPerfil=="IF 4CP"||strPerfil=="IF 5CP"){
				Ext.getCmp('gridRegErroneos').hide();
				if(resp.EXISTEERROR2=='N'){
					Ext.Msg.show({
						title: 'Confirmar',
						msg: 'La versi�n de Portafolio no es correcta',
						buttons: Ext.Msg.OK,
						fn: function peticionAjax(btn){
							if (btn == 'ok'){
								window.location = '38CargaCartera.jsp';
							}
						}
					});
					return;
				}else if(resp.EXISTEERROR=='S'){
					Ext.Ajax.request({
						url: '38CargaCartera.data.jsp',
						params: Ext.apply({//fp.getForm().getValues(),{
							informacion: 'insertaErrores',
							archivo: resp.nombreArchivo,
							numeroIFNB:resp.numeroIFNB
						}),
						callback: procesaErrores
					});
					return;
				}
			}
			storeValidacionIni.loadData(resp.VALIDA_INICIAL);
			nombreArchivo= resp.nombreArchivo;
			numeroIFNB=resp.numeroIFNB;
			periodo= resp.periodo;
			validaIFNB=resp.validaArchivo;
			panelFormaCargaArchivo.setVisible(false);
			panelFormaTransferencia.setVisible(true);
		}
		}else{
		// Mostrar mensaje de error
		if(!Ext.isEmpty(resp.mensaje)){
				Ext.Msg.alert(
					'Mensaje',
					resp.mensaje,
					function(btn, text){
						//cargaArchivo(resp.estadoSiguiente,resp);
					}
				);
			} else {
				NE.util.mostrarConnError(response,opts);
			}
		}
		
	}
	
	//---------Fin Handlers----------
	
	//-----------------Stores----------
	 var storeValidacionIni = new Ext.data.JsonStore({
		fields: [
			{name:'MENSAJE'},{name:'RESULTADO'}
			],
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var storeRegErroneosData = new Ext.data.JsonStore({
		fields: [
		  {name: 'ERROR'}
		],
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	
	
	
	
	
	//------------------Fin Stores--------------
	
	
	var elementosTransferencia = [
		{
			xtype: 	'label',
			anchor:  '100%',
			html: 	
						"       El archivo se transfiri� exitosamente.<br/>Para iniciar la validaci�n del archivo presione el bot�n.   "  
						 ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginTop: 		'20px', 
				textAlign:		'center'
			}
		},{
					xtype: 		'button',
					text: 		'Validar',
					width: 		137,
					margins: 	' 3',
					//iconCls: 	'icoImprimir',
					id: 			'Validar',
					urlArchivo: '',
					handler:    function(boton, evento) {
						
						if(validaIFNB){
							panelFormaTransferencia.setVisible(false);
							panelFormaValidaInicial.setVisible(true);
						}else{
							Ext.Msg.alert(
								'Error',
								'El Codigo IFNB no Corresponde',
								function(btn, text){
								
								});
						}
						
					},
					style: {
						width: 137,
						textAlign:		'center'
					}
				}
	]
	
	var elementosValidacionIni = [
		{
			xtype: 	'label',
			anchor:  '100%',
			html: 	
						"       �Es Correcto el resumen? "  
						 ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginTop: 		'20px', 
				textAlign:		'center'
			}
		},
		
		{
				xtype: 'panel',
				layoutConfig:{columns:2},
				items: [
					
				]
			}
	]
	
	
	var gridRegErroneos = new Ext.grid.GridPanel({
		id: 'gridRegErroneos',
		store: storeRegErroneosData,
		margins: '20 0 0 0',
		columns: [
			{
				//header : 'Descripci�n',
				dataIndex : 'ERROR',
				width : 600,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
						metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
						return value;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 620,
		height: 260,
		style: 'margin:0 auto;',
		//autoHeight : true,
		title: 'Detalle de Errores',
		hidden:true,
		frame: true
	});
	var gridProcFinal = new Ext.grid.GridPanel({
				id: 'gridProcFinal',
				//title: '',
				hidden: false,
				header: true,
				store: storeValidacionIni,
				stripeRows: false,
				//loadMask: true,
				height: 135,
				width: 500,
				enableColumnResize: false,
				frame: true,
				columns:[
				//CAMPOS DEL GRID
						
						{
						
						dataIndex: 'MENSAJE',
						width: 245,
						align: 'left',
						menuDisabled: true,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						
						dataIndex: 'RESULTADO',
						width: 245,
						align: 'right',
						menuDisabled: true,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						}
				]
				
		});
	var elementosFinal = [
			{
				xtype: 'displayfield',
				fieldLabel: 'N�mero IFNB',
				id: 'numeroIFNB',
				width: 15
			},{
				xtype: 'displayfield',
				fieldLabel: 'Nombre IFNB',
				id: 'nombreIFNB',
				width: 15
			},
			{
				xtype: 'displayfield',
				fieldLabel: 'Periodo Cargado',
				id: 'periodo',
				width: 15
			},
			{
				xtype: 'displayfield',
				fieldLabel: 'Archivo #',
				id: 'numeroArchivo',
				width: 15
			},{
			xtype: 	'label',
			anchor:  '100%',
			html: 	
						"       Resumen de la informaci�n recibida "  
						 ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginTop: 		'20px', 
				textAlign:		'center'
			}
		},gridProcFinal
			//Grid
			,{
				xtype: 'displayfield',
				fieldLabel: 'Fecha de Recepci�n',
				id: 'fecha',
				width: 15
			},
			{
				xtype: 'displayfield',
				fieldLabel: 'Hora de Recepci�n',
				id: 'hora',
				width: 15
			}
								
	
	]
	
	var elementosForma = [
		
		{
			xtype: 	'label',
			anchor:  '100%',
			html: 	
						"        Seleccione el archivo que desea procesar y presione el bot�n enviar (IFNB_AAAAMM.prn) <br/><br/>  "  
						 ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginTop: 		'20px', 
				textAlign:		'center'
			}
		},
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivo',
		  name: 			'archivo',
		  emptyText: 	'Seleccione un archivo',
		  fieldLabel: 	"Cargar Archivo de", 
		  buttonText: 	'Examinar..',
		  buttonCfg: {
			  //iconCls: 	'upload-icon'
		  },
		  anchor: 		'-20'
		  //vtype: 		'archivotxt'
		},
		{
			xtype: 		'panel',
			//anchor: 		'100%',
			anchor: 		'-20',
			style: {
				marginTop: 		'10px'
			},
			layout: {
				type: 'hbox',
				pack: 'end',
				align: 'middle'
			},
			items: [
				{
					xtype: 			'button',
					text: 			' Enviar',
					iconCls: 		'icoContinuar',
					id: 				'botonContinuarCargaArchivo',				
					handler:    function(boton, evento) {
						
						// Revisar si la forma es invalida
						var forma = Ext.getCmp("panelFormaCargaArchivo").getForm();
						if(!forma.isValid()){
							return;
						}
	 
						// Validar que se haya especificado un archivo
						var archivo = Ext.getCmp("archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}
						
						// Revisar el tipo de extension del archivo
						var myRegexPRN = /^.+\.([pP][rR][nN])$/;
						
						
						var nombreArchivo 	= archivo.getValue();
						 if( !myRegexPRN.test(nombreArchivo)  ) {
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n prn");
							return;
						}
						Ext.getCmp("panelFormaCargaArchivo").getForm().submit({
							clientValidation: 	true,
							url: 						'38CargaCartera.data.jsp?informacion=SubirArchivo',
							waitMsg:   				'Subiendo archivo...',
							waitTitle: 				'Por favor espere',
							success: function(form, action) {
					
								 procesaCargaArchivo(null,  true,  action.response );
							},
							failure: function(form, action) {
								 action.response.status = 200;
								 procesaCargaArchivo(null,  false, action.response );
							}
						});
						
						// Cargar archivo
						//cargaArchivo("SUBIR_ARCHIVO",null);

						}
					}
				]
			},
			{
			xtype: 	'label',
			anchor:  '100%',
			html: 	
						"         - El archivo debe tener formato .prn "   ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginTop: 		'20px', 
				textAlign:		'left'
			}
		}
	]
	
	var gridProc = new Ext.grid.GridPanel({
				id: 'gridProc',
				//title: '',
				hidden: false,
				header: true,
				store: storeValidacionIni,
				stripeRows: false,
				//loadMask: true,
				height: 135,
				width: 500,
				enableColumnResize: false,
				frame: true,
				columns:[
				//CAMPOS DEL GRID
						
						{
						
						dataIndex: 'MENSAJE',
						width: 245,
						align: 'left',
						menuDisabled: true,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						},
						{
						
						dataIndex: 'RESULTADO',
						width: 245,
						align: 'right',
						menuDisabled: true,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
						}
				]
				
		});
	var tabRegErrores = new Ext.Panel({
		title:'Con Errores',
		frame: true,
		id:'tabRegErrores1',
		height: 300,
		width: 590,
		//autoScroll: true,
		items:[
			NE.util.getEspaciador(5),
			gridRegErroneos
		]
	});
	var tabResultValidFile = new Ext.TabPanel({
		id: 'tabResultValidFile1',
		border: true,
		frame: true,
		activeTab: 0,
		//width: 610,
		width: 600,
		height : 450,
		style:			'margin:0 auto;',
		items:[  tabRegErrores]
	});
		
	var panelFormaCargaArchivo = new Ext.form.FormPanel({
		id: 				'panelFormaCargaArchivo',
		width: 			600,
		title: 			'Selecci�n de Archivo',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		fileUpload: 	true,
		hidden:			false,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	190,
		defaultType: 	'textfield',
	
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			[elementosForma],
		monitorValid: 	false
	});
	
	var panelFormaTransferencia = new Ext.Panel({
		id: 				'panelFormaTransferencia',
		width: 			600,
		title: 			'Resultado de la Transferencia',
		frame: 			true,
		collapsible: 	true,
		bodyCfg: {tag:'center'},
		titleCollapse: true,
		hidden:			true,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	190,
		defaultType: 	'textfield',
	
		defaults: {
			msgTarget: 	'side',
			anchor: 		'100%'
		},
		items: 			[elementosTransferencia]
	});
	
	var panelFormaValidaInicial = new Ext.form.FormPanel({
		id: 				'panelFormaValidaInicial',
		width: 			600,
		title: 			'Resultado de la Validaci�n del Archivo',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		bodyCfg: {tag:'center'},
		hidden:			true,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	190,
		defaultType: 	'textfield',
	
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			[gridProc,elementosValidacionIni],// en este monento ya realizo la carga del grid 
		buttonAlign: 'center',
		buttons:[// 
		{
					xtype: 		'button',
					text: 		'Si',
					width: 		60,
					margins: 	' 3',
					//iconCls: 	'icoImprimir',
					handler:    function(boton, evento) {
					
					panelFormaValidaInicial.el.mask('Validando...', 'x-mask-loading');			
						Ext.Ajax.request({
											url: '38CargaCartera.data.jsp',
											params: Ext.apply({//fp.getForm().getValues(),{
												informacion: 'ValidaCargaArchivo',
												archivo: nombreArchivo,
												periodo:periodo,
												numeroIFNB:numeroIFNB
													}),
											callback: procesaValidacionArchivo
										});
						
					},
					style: {
						width: 137,
						textAlign:		'center'
					}
				},{
					xtype: 		'button',
					text: 		'No',
					width: 		60,
					margins: 	' 3',
					handler:    function(boton, evento) {
						Ext.Msg.alert(
					'Resultado de la Validaci�n.',
					'Validaci�n Cancelada<br/>El resumen no fue aceptado.',
					function(btn, text){
						location.reload();
					}
				);
						
						
					},
					style: {
						width: 137,
						textAlign:		'center'
						}
					}
		]
		
	});
	
	var panelFormaTransferenciaFinal = new Ext.form.FormPanel({
		id: 				'panelFormaTransferenciaFinal',
		width: 			600,
		title: 			'Acuse de Recibo',
		frame: 			true,
		collapsible: 	true,
		bodyCfg: {tag:'center'},
		titleCollapse: true,
		hidden:			true,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	190,
		defaultType: 	'textfield',
	
		defaults: {
			msgTarget: 	'side',
			anchor: 		'100%'
		},
		items: 			[elementosFinal]
	});
	
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			panelFormaCargaArchivo,panelFormaTransferencia,panelFormaValidaInicial,panelFormaTransferenciaFinal,
			NE.util.getEspaciador(20),
			gridRegErroneos
		]
	});

	
	
});