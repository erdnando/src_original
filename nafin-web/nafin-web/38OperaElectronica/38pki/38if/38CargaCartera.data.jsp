<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.math.*, 
	java.text.*, 
	java.sql.*,
	netropology.utilerias.*,		
	java.text.SimpleDateFormat,
	java.util.Date,		
	com.netro.exception.*,
	com.netro.model.catalogos.*,	
	com.netro.electronica.*,   
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
String fechaSolicitudIni = (request.getParameter("fechaSolicitudIni")!=null)?request.getParameter("fechaSolicitudIni"):"";



String infoRegresar ="", consulta = "";
JSONObject jsonObj = new JSONObject();

OperacionElectronica eletronicaBean = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);

if(informacion.equals("insertaErrores")){
	//String periodo = (request.getParameter("periodo")!=null)?request.getParameter("periodo"):"";
	String ifnbCode = (request.getParameter("numeroIFNB")!=null)?request.getParameter("numeroIFNB"):"";
	String file = (request.getParameter("archivo")!=null)?request.getParameter("archivo"):"";
	JSONArray jsObjArray = new JSONArray();
	JSONObject	resultado	= new JSONObject();
	try {
		if(strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP")){
			List existeError = eletronicaBean.insertarTablaDetalle(file,strDirectorioTemp,iNoCliente,ifnbCode);
			List lstErroneos = new ArrayList();
			String ic_tmp ="";
			if(existeError.size()>0){
				for(int i = 0; i<existeError.size();i++){
					ic_tmp+=existeError.get(i)+",";
				}
				int numReg = ic_tmp.length();
				String auxReg = ic_tmp.substring(0,numReg-1);
				HashMap consError = eletronicaBean.obtieneErrorDoc(ifnbCode,iNoCliente,auxReg);
				lstErroneos = (List)consError.get("ERRONEOS");
			}
			jsObjArray = JSONArray.fromObject(lstErroneos);
		}
		if(strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP")){
			resultado.put("regErroneos",jsObjArray.toString());
		}else{
			resultado.put("regErroneos","");
		}
	} catch(Exception e) {
		e.printStackTrace();
	}
	resultado.put("success", 					new Boolean(true)		);
 
	infoRegresar = resultado.toString();

}else if(informacion.equals("SubirArchivo")){
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
	String nombreArchivo="";
	JSONObject	resultado	= new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	boolean		success		= true;
	List regs=new ArrayList();
	HashMap reg = new HashMap();
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(8097152);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		e.printStackTrace();
		if(myUpload.getSize() > 8097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
		
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	
	// Guardar Archivo en Disco
	int numFiles = 0;
	String mensaje = "";
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		nombreArchivo =  myUpload.getFiles().getFile(0).getFileName();
		int icFinanciera = Integer.parseInt(eletronicaBean.getIcFinanciera(iNoCliente));
		try{
			 if (icFinanciera==0) icFinanciera = Integer.parseInt(nombreArchivo.substring(0,6));
			} catch (NumberFormatException nfe){
				icFinanciera = 0;
			}
		
		int fileIfnb = 0;
		int fileYear =0;
		int fileMonth =0;
		try{
				 fileIfnb = Integer.parseInt(nombreArchivo.substring(0,6));
				 fileYear = Integer.parseInt(nombreArchivo.substring(7,11));
				 fileMonth = Integer.parseInt(nombreArchivo.substring(11,13));
			}catch(NumberFormatException nfe){
				
				nfe.printStackTrace();
				mensaje = "El nombre del archivo es incorrecto.";
				throw new AppException("El nombre del archivo es incorrecto.");
			}
		 if (fileMonth>12 || fileIfnb!=icFinanciera ){
		 mensaje = "El nombre del archivo es incorrecto.";
        throw new AppException("El nombre del archivo es incorrecto.");
      }
		String completaFecha="";
		if(fileMonth<10){
			completaFecha ="0";
		}
		int fechaPrcesada=Integer.parseInt(eletronicaBean.fechaProcesada(completaFecha+fileMonth+"/"+fileYear,fileIfnb+""));
		if(fechaPrcesada>0){
			mensaje = "Error. Se intento procesar un periodo que ya esta cargado.";
			throw new AppException("Error. Se intento procesar un periodo que ya esta cargado.");
		}
		myFile.saveAs(strDirectorioTemp + nombreArchivo	 );
		HashMap mapaRetorno = eletronicaBean.getValidacionInicial(nombreArchivo,strDirectorioTemp);
		reg.put("MENSAJE","Total de créditos descontados:");
		reg.put("RESULTADO",mapaRetorno.get("DISC_NUM_RECORDS"));
		regs.add(reg);
		reg= new HashMap();
		reg.put("MENSAJE","Saldo:");
		reg.put("RESULTADO","$"+Comunes.formatoDecimal(mapaRetorno.get("DISC_BALANCE"),2));
		regs.add(reg);
		reg= new HashMap();
		reg.put("MENSAJE","Total de créditos Procesados:");
		reg.put("RESULTADO",mapaRetorno.get("TOTAL_REGISTROS"));
		regs.add(reg);
		reg= new HashMap();
		reg.put("MENSAJE","Saldo:");
		reg.put("RESULTADO","$"+Comunes.formatoDecimal(mapaRetorno.get("TOTAL_BALANCE"),2));
		regs.add(reg);
		resultado.put("VALIDA_INICIAL",regs);
		resultado.put("retorno",mapaRetorno);
		resultado.put("validaArchivo", new Boolean(eletronicaBean.checkIfnbCode (  fileIfnb,strDirectorioTemp+nombreArchivo) ));
		resultado.put("numeroIFNB",fileIfnb+"");
		resultado.put("periodo",fileMonth+"/"+fileYear);
		resultado.put("nombreArchivo", 	nombreArchivo			);
		resultado.put("EXISTEERROR",mapaRetorno.get("EXISTEERROR"));
		resultado.put("EXISTEERROR2",mapaRetorno.get("EXISTEERROR2"));
		
	}catch(AppException e){
		mensaje = e.getMessage();
		success		= false;
		e.printStackTrace();
		resultado.put("mensaje",mensaje);
		resultado.put("success", 					new Boolean(success)		);
		infoRegresar = resultado.toString();
		
	}
	catch (Exception e){
		if(!mensaje.equals("")){
			throw new AppException("Ocurrio un error inesperado ",e);
		}
	}
	
	// Enviar parametros adicionales

 
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 		"REALIZAR_VALIDACION" 	);
	resultado.put("mensaje",mensaje);
	// Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

}
else
if(informacion.equals("validaInicial")){



}
else
if(informacion.equals("ValidaCargaArchivo")){
	String periodo = (request.getParameter("periodo")!=null)?request.getParameter("periodo"):"";
	String ifnbCode = (request.getParameter("numeroIFNB")!=null)?request.getParameter("numeroIFNB"):"";
	String file = (request.getParameter("archivo")!=null)?request.getParameter("archivo"):"";

	try{
		String completaFecha ="";
		if(periodo.length()<7){
			completaFecha ="0";
		}
		String errores = "0";
		eletronicaBean.insertRecord(completaFecha+periodo,Integer.parseInt(ifnbCode));
		eletronicaBean.insertRecordDetail(completaFecha+periodo,Integer.parseInt(ifnbCode),file,strDirectorioTemp);
		String nombreIFNB = eletronicaBean.getIfnbNombre(Integer.parseInt(ifnbCode));
		String numeroArchivo = eletronicaBean.getIfnbNumeroArchivo(Integer.parseInt(ifnbCode),completaFecha+periodo);
		String fechaHoy = Fecha.getFechaActual();
		String horaHoy = Fecha.getHoraActual("hh:mi:ss");
		
		String periodoFinal = Fecha.getUltimoDiaDelMes("01/"+periodo,"dd/mm/yyyy");
		jsonObj.put("numeroIFNB",ifnbCode);
		jsonObj.put("periodoFinal",periodoFinal);
		jsonObj.put("errores",errores);
		jsonObj.put("nombreIFNB",nombreIFNB);
		jsonObj.put("numeroArchivo",numeroArchivo);
		jsonObj.put("fechaHoy",fechaHoy);
		jsonObj.put("horaHoy",horaHoy);
		jsonObj.put("success", 					new Boolean(true)		);
		
		
	}catch(Throwable t){
		throw new AppException("Error inesperado...", t);
	}
	
	infoRegresar = jsonObj.toString();
	
}
	
%>
	
<%=infoRegresar%>