<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.math.*, 
	java.text.*, 
	java.sql.*,
	netropology.utilerias.*,		
	java.text.SimpleDateFormat,
	java.util.Date,		
	com.netro.exception.*,
	com.netro.model.catalogos.*,	
	com.netro.electronica.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
String fechaSolicitudIni = (request.getParameter("fechaSolicitudIni")!=null)?request.getParameter("fechaSolicitudIni"):"";
String fechaSolicitudFin = (request.getParameter("fechaSolicitudFin")!=null)?request.getParameter("fechaSolicitudFin"):"";
String fechaOperacionIni = (request.getParameter("fechaOperacionIni")!=null)?request.getParameter("fechaOperacionIni"):"";
String fechaOperacionFin = (request.getParameter("fechaOperacionFin")!=null)?request.getParameter("fechaOperacionFin"):"";
String fechaRechazoIni = (request.getParameter("fechaRechazoIni")!=null)?request.getParameter("fechaRechazoIni"):"";
String fechaRechazoFin = (request.getParameter("fechaRechazoFin")!=null)?request.getParameter("fechaRechazoFin"):"";
String montoIni = (request.getParameter("montoIni")!=null)?request.getParameter("montoIni"):"";
String montoFin = (request.getParameter("montoFin")!=null)?request.getParameter("montoFin"):"";
String inicializa = (request.getParameter("inicializa")!=null)?request.getParameter("inicializa"):"";


String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String extension = (request.getParameter("extension")!=null)?request.getParameter("extension"):"";
if(iTipoAfiliado.equals("I")) {  ic_if = iNoCliente;  }
String infoRegresar ="", consulta = "", saldoFondeoLiquido ="", saldoLineaCredito ="";
JSONObject jsonObj = new JSONObject();



OperacionElectronica eletronicaBean = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);


String rutaNombreArchivo =(String)application.getAttribute("strDirectorioPublicacion")+"38OperaElectronica/38pki/38if/38MontosSirac.txt";

//Declaración de la clase para la consulta  
Cons_Operaciones paginador = new Cons_Operaciones (); 
paginador.setIc_if(ic_if);
paginador.setIc_solicitud(no_solicitud);
paginador.setFechaSolicitudIni(fechaSolicitudIni);
paginador.setFechaSolicitudFin(fechaSolicitudFin);
paginador.setFechaOperacionIni(fechaOperacionIni);
paginador.setFechaOperacionFin(fechaOperacionFin);
paginador.setFechaRechazoIni(fechaRechazoIni);
paginador.setFechaRechazoFin(fechaRechazoFin);
paginador.setMontoIni(montoIni);
paginador.setMontoFin(montoFin);
paginador.setITipoAfiliado(iTipoAfiliado);
paginador.setStrPerfil(strPerfil);
paginador.setInicializa(inicializa);
paginador.setINoUsuario(iNoUsuario);
paginador.setRutaNombreArchivo(rutaNombreArchivo);


CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

String IfOperador = "";
String OperaFirma ="";
if (informacion.equals("valoresIniciales")){
System.out.println("opera firma "+iNoCliente);
	IfOperador = eletronicaBean.getCampoRegDatosIf("CG_OPERADOR",iNoCliente);
	OperaFirma= eletronicaBean.getOperaFirmaMancomunada(iNoCliente);
	HashMap registros =  paginador.getSaldos (ic_if);	
	jsonObj.put("montoLineaCredito", "$"+Comunes.formatoDecimal(registros.get("MONTO_ASIGNADO").toString(), 2)); 
	jsonObj.put("montoUtilizado","$"+Comunes.formatoDecimal(registros.get("MONTO_UTILIZADO").toString(), 2)); 
	jsonObj.put("montoDisponible","$"+Comunes.formatoDecimal(registros.get("MONTO_DISPONIBLE").toString(), 2)); 
	jsonObj.put("num_Prestamos",registros.get("NUMERO_PRESTAMO").toString()); 
	jsonObj.put("",""); 			
	if(strPerfil.equals("IF 5CP")  ||  strPerfil.equals("IF 4MIC")   ||  strPerfil.equals("IF 5MIC") ) {
		jsonObj.put("saldoFondoLiquido","$"+Comunes.formatoDecimal(registros.get("SALDO_FONDO_LIQUIDO").toString(), 2)); 						
	}			
							
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("iTipoAfiliado", iTipoAfiliado); 
	jsonObj.put("strPerfil", strPerfil); 
	jsonObj.put("inicializa", inicializa); 
	jsonObj.put("ic_if", ic_if); 
	jsonObj.put("IfOperador", IfOperador); 
	jsonObj.put("OperaFirma", OperaFirma); 

	infoRegresar = jsonObj.toString();  
  
}else  if (informacion.equals("catIFData")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setTabla("comcat_if");		
	catalogo.setOrden("cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();	
}else  if (informacion.equals("Consultar")){
	Registros reg	=	queryHelper.doSearch();    
	String  revi = "";
	while(reg.next()){
		String FechasModificatorias = paginador.getFirmasModificatorias (reg.getString("IC_IF")) ;
		String nomUsuario = (reg.getString("CG_NOMBRE_USUARIO") == null) ? "" : reg.getString("CG_NOMBRE_USUARIO");
		String nomUsuario1 = nomUsuario.substring(nomUsuario.lastIndexOf("-")+1, nomUsuario.length());
		HashMap registros =  paginador.getSaldos (reg.getString("IC_IF"));	
		String no_solicutid= (reg.getString("NO_SOLICITUD") == null) ? "" : reg.getString("NO_SOLICITUD");
		System.out.println("NO_SOLICITUD "+no_solicutid);
		reg.setObject("FECHA_FIRMA_MODIFICATORIOS",FechasModificatorias);	
		reg.setObject("CG_NOMBRE_USUARIO",nomUsuario1);	
		reg.setObject("TIPO_USUARIO",iTipoAfiliado);	
		reg.setObject("MONTO_ASIGNADO",registros.get("MONTO_ASIGNADO").toString());	
		reg.setObject("MONTO_UTILIZADO",registros.get("MONTO_UTILIZADO").toString());	
		reg.setObject("MONTO_DISPONIBLE",registros.get("MONTO_DISPONIBLE").toString());	
		reg.setObject("NUMERO_PRESTAMO",registros.get("NUMERO_PRESTAMO").toString());	
		reg.setObject("SALDO_FONDO_LIQUIDO",registros.get("SALDO_FONDO_LIQUIDO").toString());	
		revi = eletronicaBean.getExisteRevision(no_solicutid);
		reg.setObject("EXISTE_REVISION",revi);	
	}	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("iTipoAfiliado", iTipoAfiliado); 
	jsonObj.put("strPerfil", strPerfil); 
	infoRegresar = jsonObj.toString();  		


}else  if ( informacion.equals("ArchivoCSVCons") ) {
	try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	infoRegresar = jsonObj.toString(); 

}else  if ( informacion.equals("ArchivoTablaAmortizacion") ) {

	try {
		
		String nombreArchivo =	eletronicaBean.getArchivoTablaAmortizacion (strDirectorioTemp,  no_solicitud  );	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}	
	infoRegresar = jsonObj.toString(); 

}else  if ( informacion.equals("desArchivoCotiza") ) {
	
	try {
		String nombreArchivo =	eletronicaBean.consDescargaArchivos(strDirectorioTemp,  no_solicitud, tipoArchivo,  extension );
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}	
	infoRegresar = jsonObj.toString(); 
	
}else if (informacion.equals("bitacoraReviciones")){
	JSONArray jsObjArray = new JSONArray();
	String CVESOLICITUD = request.getParameter("CVESOLICITUD")==null?"":request.getParameter("CVESOLICITUD");
	List lsBitacora = eletronicaBean.consultarBitacoraRevision(CVESOLICITUD);
	jsObjArray = JSONArray.fromObject(lsBitacora);
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
	
}else if (informacion.equals("archivoBitacora")){
	String IC_CHKLIST_SOLICITUD = request.getParameter("IC_CHKLIST_SOLICITUD")==null?"":request.getParameter("IC_CHKLIST_SOLICITUD");
	try {
		
		String nombreArchivo =	eletronicaBean.obtenChkBitacora (strDirectorioTemp,IC_CHKLIST_SOLICITUD );	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}	
	infoRegresar = jsonObj.toString(); 
	
}
	
%>
	
<%=infoRegresar%>