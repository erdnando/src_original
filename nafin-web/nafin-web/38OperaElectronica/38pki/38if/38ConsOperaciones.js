Ext.onReady(function() {
	
	var iNoUsuario = Ext.getDom('iNoUsuario').value;
	var IfOperador="";
	var OperaFirma="";
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnArchivoCons').setIconClass('icoXls');				
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}   
	
	var reenviarSolicitud= function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		var ic_if = registro.get('IC_IF');
		var  parametros = "ic_solicitud="+no_solicitud+"&tipoSolicitud=Modificar&ic_if="+ic_if;
		document.location.href = "38RegSolicRecuersos.jsp?"+parametros; 	
		
	}
	
	
	
	//Descarga del Archivo de la Columna Carga de Empresas Apoyadas
	var desArchivoEmpresas= function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '38ConsOperaciones.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoCotiza',
				tipoArchivo:'Empresa',
				extension:'txt',				
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	
	//Descarga del Archivo de la Columna PDF Comprobante Boleta RUG****
	var desArchivoBoleta = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '38ConsOperaciones.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoCotiza',
				tipoArchivo:'Boleta',
				extension:'pdf',				
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	//Descarga del Archivo de la Columna Carga Cartera en Prenda
	var desArchivoCartera = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');  
		Ext.Ajax.request({
			url: '38ConsOperaciones.data.jsp',   
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoCotiza',
				tipoArchivo:'Cartera',
				extension:'csv',				
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	//Descarga del Archivo de la Columna Carga Cartera en Prenda
	var desArchivoCarteraPdf = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		Ext.Ajax.request({
			url: '38ConsOperaciones.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoCotiza',
				tipoArchivo:'Cartera_Pdf',
				extension:'pdf',				
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	
		//Descarga del Archivo de la Columna de Tabla Amortizaci�n
	var desArchivoCotiza = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '38ConsOperaciones.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoCotiza',
				tipoArchivo:'Cotiza',
				extension:'pdf',				
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	
	//Descarga del Archivo de la Columna de Tabla Amortizaci�n
	var desArchivoTabla = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '38ConsOperaciones.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'ArchivoTablaAmortizacion',
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	var processSuccessFailureObtieneArchivo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var archivo = resp.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	//------Consulta ------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta.getColumnModel();
			
			if(jsonData.iTipoAfiliado =='N')  {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);		
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_ASIGNADO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_UTILIZADO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DISPONIBLE'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUMERO_PRESTAMO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SALDO_FONDO_LIQUIDO'), false);
			}
			
			if(jsonData.iTipoAfiliado =='I' ||   jsonData.strPerfil =='ADMIN NAFIN')  {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('ACCIONES'), false);   
			}   
			
			
			if(jsonData.strPerfil =='IF LI'  ||   jsonData.strPerfil =='IF 4MIC'   ||  jsonData.strPerfil =='IF 5MIC' )  {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DOC_CARTERA'), true);
								
			}
			if(jsonData.strPerfil =='IF 5CP'  ||   jsonData.strPerfil =='IF 4CP' )  {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DOC_CARTERA_PDF'), false);
			}   
			if(jsonData.strPerfil =='OP CON'  ||   jsonData.strPerfil =='OP OP' )  {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('EXISTE_REVISION'), false);
			}
			if(store.getTotalCount() > 0) {		   	
				el.unmask();	
				Ext.getCmp('btnArchivoCons').enable();   
			} else {		   
				Ext.getCmp('btnArchivoCons').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '38ConsOperaciones.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{	name: 'IC_IF'},
			{	name: 'NOMBRE_IF'},
			{	name: 'FECHA_SOLICITUD'},
			{	name: 'NO_SOLICITUD'},
			{	name: 'NOMBRE_CONTRATO'},
			{	name: 'FECHA_FIRMA_CONTRATO'},			
			{	name: 'NO_MODIFICATORIOS'},
			{	name: 'FECHA_FIRMA_MODIFICATORIOS'},
			{	name: 'NOMBRE_CONTRATO_M'},
			{	name: 'FECHA_CONTRATOS_OE'},
			{	name: 'IMPORTE'},
			{	name: 'MONEDA'},			
			{	name: 'DESTINO_RECURSOS'},
			{	name: 'FECHA_PAGO_CAPITAL'},
			{	name: 'FECHA_PAGO_INTERES'},
			{	name: 'FECHA_VENCIMIENTO'},
			{	name: 'NOMBRE_TASA'},			
			{	name: 'TASA_INTERES'},
			{	name: 'TIPO_TASA'},
			{	name: 'ESTATUS'},
			{	name: 'FECHA_OPERACION_EJECUTIVO'},
			{	name: 'FECHA_OP_CONCENTRADOR'},
			{	name: 'CAUSA_RECHAZO'},
			{	name: 'CAUSA_RECHAZO_AUTOR'},
			{	name: 'OBSER_INTERMEDIARIO'},
			{	name: 'OBSER_EJECUTIVO'},
			{	name: 'DOC_COTIZACION'},
			{	name: 'DOC_CARTERA'},
			{	name: 'DOC_BOLETA'},			
			{	name: 'DOC_EMPRESAS'},
			{	name: 'IC_ESTATUS'},			
			{  name : 'CG_NOMBRE_USUARIO' },
			{  name : 'USUARIO_EJE_OP' },
			{  name : 'USUARIO_EJE_CON2' },
			{  name : 'CS_ACUSE' },
			{  name : 'DF_RECHAZO' },
			{  name : 'DF_RECHAZADO_CON' },
			{  name : 'TIPO_USUARIO' },
			{  name : 'ACCIONES' },
			{  name : 'MONTO_ASIGNADO' },
			{  name : 'MONTO_UTILIZADO' },
			{  name : 'MONTO_DISPONIBLE' },
			{  name : 'NUMERO_PRESTAMO' },
			{  name : 'SALDO_FONDO_LIQUIDO' },
			{  name : 'NO_PRESTAMO' },
			{	name : 'DOC_CARTERA_PDF'},
			{	name: 'EXISTE_REVISION'}
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});	
	var bitacoraReviciones = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		var CVESOLICITUD = registro.get('NO_SOLICITUD');
		var acuse = registro.get('CS_ACUSE');
		Ext.getCmp('gridBitacoraRevi').setTitle('Bit�cora de revisiones : Solicitud '+acuse);
		consultaBitacoraRevi.load({
					params: Ext.apply(
					{
						CVESOLICITUD: CVESOLICITUD,
						informacion: 'bitacoraReviciones'
					})
				});
		var ProcesarBitacoraRevi = Ext.getCmp('ProcesarBitacoraRevisiones');
		if(ProcesarBitacoraRevi){
			ProcesarBitacoraRevi.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 700,
				height: 400,												
				resizable: false,
				closable:false,
				id: 'ProcesarBitacoraRevisiones',
				closeAction: 'hide',
				items: [					
					gridBitacoraRevi				
				]
			}).show();
			
		}
	}
	var consultaBitacoraRevi  = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '38ConsOperaciones.data.jsp',
		baseParams: {
			informacion: 'bitacoraReviciones'
		},		
		fields: [
			{	name: 'IC_CHKLIST_SOLICITUD'},
			{	name: 'IC_SOLICITUD'},
			{	name: 'IC_ESTATUS_OPE'},
			{	name: 'CD_DESCRIPCION'},
			{	name: 'CC_USUARIO_NOM'},
			{	name: 'DF_ALTA'},
			{	name: 'CG_RECHAZO_INT'}
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}		
	});
	var gridBitacoraRevi = new Ext.grid.EditorGridPanel({	
		store: consultaBitacoraRevi,
		id: 'gridBitacoraRevi',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Bit�cora de revisiones',
		clicksToEdit: 1,
		hidden: false,		
		columns: [	
			{
				header: '<center>Estatus</center>',
				tooltip: 'Estatus',
				dataIndex: 'CD_DESCRIPCION',
				sortable: true,
				width: 200,			
				resizable: true,	
				hidden:false,
				align: 'left'
			},
			{
				header: '<center>Usuario</center>',
				tooltip: 'Usuario',
				dataIndex: 'CC_USUARIO_NOM',
				sortable: true,
				width: 250,			
				resizable: true,	
				hidden:false,
				align: 'left'
			},
			{//2
				header: 'Fecha y hora',
				tooltip: 'Fecha y hora',
				dataIndex: 'DF_ALTA',
				sortable: true,
				hideable: false,
				width: 150,
				align: 'CENTER'
				//renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				xtype:	'actioncolumn',
				header: 'Check list',
				tooltip: 'Check list',
				align: 'center',	
				sortable: true,	
				resizable: true,	
				width: 70,
				hideable: false, 
				hidden: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							
									this.items[0].tooltip = 'Ver';
									return 'icoXls';
								
							
						},
						handler:function(grid, rowIndex, colIndex, item, event){
							var registro = grid.getStore().getAt(rowIndex);
							
								Ext.Ajax.request({
								url : '38ConsOperaciones.data.jsp',
								params: Ext.apply({
									informacion: "archivoBitacora",
									IC_CHKLIST_SOLICITUD: registro.data['IC_CHKLIST_SOLICITUD']
								}),
								callback: processSuccessFailureObtieneArchivo});
							
					
						}
					}
			]
		}
		],	
		bbar: {
			xtype: 'toolbar',	buttonAlign:'center',
			buttons: [
				
				{
							xtype: 'button',
							text: '<center>Cerrar</center>',					
							tooltip:	'Cerrar',
							iconCls: 'icoCerrar',
							id: 'btnCerrarBita',
							iconCls: 'icoLimpiar',
							handler: function(){Ext.getCmp('ProcesarBitacoraRevisiones').hide();
							}
				}
			]
		},
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 700,
		align: 'center',
		frame: false
	});		
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,	
				hidden:true,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha de Solicitud',
				tooltip: 'Fecha de Solicitud',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. de Solicitud',
				tooltip: 'No. de Solicitud',
				dataIndex: 'CS_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. Pr�stamo',
				tooltip: 'No. Pr�stamo',
				dataIndex: 'NO_PRESTAMO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Nombre Contrato',
				tooltip: 'Nombre Contrato',
				dataIndex: 'NOMBRE_CONTRATO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha Firma Contrato',
				tooltip: 'Fecha Firma Contrato',
				dataIndex: 'FECHA_FIRMA_CONTRATO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Contratos Modificatorios',
				tooltip: 'Contratos Modificatorios',
				dataIndex: 'NO_MODIFICATORIOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Firma Modificatorios',
				tooltip: 'Fecha Firma Modificatorios',
				dataIndex: 'FECHA_FIRMA_MODIFICATORIOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Nombre Contrato',
				tooltip: 'Nombre Contrato',
				dataIndex: 'NOMBRE_CONTRATO_M',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha Firma Contrato Operaci�n Electr�nica',
				tooltip: 'Fecha Firma Contrato Operaci�n Electr�nica',
				dataIndex: 'FECHA_CONTRATOS_OE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'IMPORTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Destino de los Recursos',
				tooltip: 'Destino de los Recursos',
				dataIndex: 'DESTINO_RECURSOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha de Primer Pago Capital',
				tooltip: 'Fecha de Primer Pago Capital',
				dataIndex: 'FECHA_PAGO_CAPITAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Primer Pago Interes',
				tooltip: 'Fecha de Primer Pago Interes',
				dataIndex: 'FECHA_PAGO_INTERES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Tipo de Tasa',
				tooltip: 'Tipo de Tasa',
				dataIndex: 'TIPO_TASA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Tasa ',
				tooltip: 'Tasa',
				dataIndex: 'NOMBRE_TASA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('NOMBRE_TASA') !='' ) {
						return registro.get('NOMBRE_TASA');
					}else  {
						return 'N/A';
					}				
				}
			},		
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'TASA_INTERES',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('TIPO_TASA') == 'Variable'){
						if(registro.get('TASA_INTERES') !='' ) {
							return  Ext.util.Format.number(value, '+0.00%');
						}else  {
							return 'N/A';
						}				
					}else{
						if(registro.get('TASA_INTERES') !='' ) {
							return  Ext.util.Format.number(value, '0.00%');
						}else  {
							return 'N/A';
						}				
					}
					
				
				}					
			},			
			{
				xtype: 'actioncolumn',
				header: 'Carga Cotizaci�n',
				tooltip: 'Carga Cotizaci�n',
				dataIndex: 'DOC_COTIZACION',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('DOC_COTIZACION') !=0 ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';	
							}
						}
						,handler: desArchivoCotiza
					}
				]				
			},
			
			{
				xtype: 'actioncolumn',
				header: 'Tabla Amortizaci�n',
				tooltip: 'Tabla Amortizaci�n',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoXls';										
						}
						,handler: desArchivoTabla
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Carga Cartera en Prenda',
				tooltip: 'Carga Cartera en Prenda',
				dataIndex: 'DOC_CARTERA',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('DOC_CARTERA') ==0 ) {
						return 'N/A';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('DOC_CARTERA') !=0 ) {
								this.items[0].tooltip = 'Ver';
								return 'icoXls';		
							}
						}
						,handler: desArchivoCartera
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Carga PDF Cartera en Prenda',
				tooltip: 'Carga PDF Cartera en Prenda',
				dataIndex: 'DOC_CARTERA_PDF',
				width: 130,
				hidden:true,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('DOC_CARTERA_PDF') ==0 ) {
						return 'N/A';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('DOC_CARTERA_PDF') !=0 ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';		
							}
						}
						,handler: desArchivoCarteraPdf
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'PDF Comprobante Boleta RUG',
				tooltip: 'PDF Comprobante Boleta RUG',
				dataIndex: 'DOC_BOLETA',
				width: 130,
				align: 'center',
				hidden:true,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('DOC_BOLETA') ==0 ) {
						return 'N/A';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('DOC_BOLETA') !=0 ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';		
							}								
						}
						,handler: desArchivoBoleta
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Carga de Empresas Apoyadas',
				tooltip: ' Carga de Empresas Apoyadas',
				dataIndex: 'DOC_EMPRESAS',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('IC_ESTATUS') ==5  && registro.get('DOC_EMPRESAS')  !=0 ) {					
						return 'Descargar';	
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('IC_ESTATUS') ==5  && registro.get('DOC_EMPRESAS')  !=0 ) {
								this.items[0].tooltip = 'Descargar';
								return 'icoTxt';		
							}										
						}
						,handler: desArchivoEmpresas
					}
				]				
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				xtype:	'actioncolumn',
				header: 'Bit�cora de revisiones',
				tooltip: 'Bit�cora de revisiones',	
				dataIndex: 'EXISTE_REVISION',
				align: 'center',
				hidden:true,
				width: 100,
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							if(record.get('EXISTE_REVISION') !=0 ){
								this.items[0].tooltip = 'ver';
								return 'iconoLupa';	
							}
						}
						,handler:	bitacoraReviciones  
					}
				]
			},
			{
				header: 'Usuario IF',
				tooltip: 'Usuario IF',
				dataIndex: 'CG_NOMBRE_USUARIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha Operaci�n Ejecutivo OP',
				tooltip: 'Fecha Operaci�n Ejecutivo OP',
				dataIndex: 'FECHA_OPERACION_EJECUTIVO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Rechazo Ejecutivo OP',
				tooltip: 'Fecha Rechazo Ejecutivo OP',
				dataIndex: 'DF_RECHAZO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Usuario Ejecutivo OP',
				tooltip: 'Usuario Ejecutivo OP',
				dataIndex: 'USUARIO_EJE_OP',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Operaci�n ',
				tooltip: 'Fecha Operaci�n ',
				dataIndex: 'FECHA_OP_CONCENTRADOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Rechazo Concentrador',
				tooltip: 'Fecha Rechazo Concentrador',
				dataIndex: 'DF_RECHAZADO_CON',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Usuario Concentrador',
				tooltip: 'Usuario Concentrador',
				dataIndex: 'USUARIO_EJE_CON2',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Causa de Rechazo',
				tooltip: 'Causa de Rechazo',
				dataIndex: 'CAUSA_RECHAZO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			
			{
				header: 'Observaciones Intermediario Financiero',
				tooltip: 'Observaciones Intermediario Financiero',
				dataIndex: 'OBSER_INTERMEDIARIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header : 'Observaciones Ejecutivo de Operaci�n', 
				tooltip: 'Observaciones Ejecutivo de Operaci�n',
				dataIndex : 'OBSER_EJECUTIVO', 
				fixed:true,
				sortable : false,	
				width : 300,
				hiddeable: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}			
			},	
			
			{
				header : 'Monto l�nea de cr�dito', 
				tooltip: 'Monto l�nea de cr�dito',
				dataIndex : 'MONTO_ASIGNADO', 
				fixed:true,
				sortable : false,	
				width : 300,
				hiddeable: false,
				align: 'right',
				hidden:true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')						
			},			
			{
				header : 'Monto utilizado ', 
				tooltip: 'Monto utilizado ',
				dataIndex : 'MONTO_UTILIZADO', 
				fixed:true,
				sortable : false,	
				width : 300,
				hiddeable: false,
				hidden:true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')						
			},	
			{
				header : 'Monto Disponible', 
				tooltip: 'Monto Disponible',
				dataIndex : 'MONTO_DISPONIBLE', 
				fixed:true,
				sortable : false,	
				width : 300,
				hiddeable: false,
				hidden:true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')						
			},	
			{
				header : 'N�mero de Pr�stamos', 
				tooltip: 'N�mero de Pr�stamose',
				dataIndex : 'NUMERO_PRESTAMO', 
				fixed:true,
				sortable : false,	
				width : 300,
				hiddeable: false,
				hidden:true,
				align: 'center'									
			},	
			{
				header : 'Saldo de Fondo L�quido', 
				tooltip: 'Saldo de Fondo L�quido',
				dataIndex : 'SALDO_FONDO_LIQUIDO', 
				fixed:true,
				sortable : false,	
				width : 300,
				hiddeable: false,
				align: 'right',
				hidden:true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')								
			},	
			{
				header: 'Causas de Rechazo Autorizador',
				tooltip: 'Causas de Rechazo Autorizador',
				dataIndex: 'CAUSA_RECHAZO_AUTOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden :false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				xtype: 'actioncolumn',
				header: 'Acciones',
				tooltip: 'Acciones',
				dataIndex : 'ACCIONES', 
				width: 130,
				hidden:true,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(OperaFirma!='N'){
								if(IfOperador==iNoUsuario){
									if(registro.get('IC_ESTATUS') ==6 || registro.get('IC_ESTATUS') ==4   )  { 
										this.items[0].tooltip = 'Reenviar Solicitudes';
										return 'modificar';		
									}
								}else{
									return '';
								}
							}else{
								if(registro.get('IC_ESTATUS') ==6 || registro.get('IC_ESTATUS') ==4   )  { 
										this.items[0].tooltip = 'Reenviar Solicitudes';
										return 'modificar';		
									}
							}
						}
						,handler: reenviarSolicitud
					}
				]				
			}		
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,		
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCons',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '38ConsOperaciones.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSVCons'
							}),
							callback: procesarDescargaArchivos
						});
						
					}
				}
			]
		}
	});
//*************** componentes de la Forma********************
	
	function procesaValoresIniciales(opts, success, response) {
		var fp = Ext.getCmp('forma');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				IfOperador = jsonData.IfOperador;
				OperaFirma = jsonData.OperaFirma;
				if(jsonData.iTipoAfiliado =='N') {
					Ext.getCmp('ic_if1').show();
				}
				Ext.getCmp('montoLineaCredito').update(jsonData.montoLineaCredito);
				Ext.getCmp('montoUtilizado').update(jsonData.montoUtilizado);
				Ext.getCmp('montoDisponible').update(jsonData.montoDisponible);
				Ext.getCmp('num_Prestamos').update(jsonData.num_Prestamos);
				if(jsonData.strPerfil =='IF 5CP'  ||  jsonData.strPerfil =='IF 4MIC'   ||  jsonData.strPerfil =='IF 5MIC') {
					Ext.getCmp('saldoFondoLiquido').show();
					Ext.getCmp('saldoFondoLiquido').update(jsonData.saldoFondoLiquido);
				}
				if(jsonData.iTipoAfiliado=='N' && jsonData.inicializa=='S'  ) {
					Ext.getCmp('montoLineaCredito').hide();
					Ext.getCmp('montoUtilizado').hide();
					Ext.getCmp('montoDisponible').hide();
					Ext.getCmp('num_Prestamos').hide();
				} 	if(jsonData.iTipoAfiliado=='N' && jsonData.inicializa=='IF' && jsonData.ic_if!=''  ) {
					Ext.getCmp('montoLineaCredito').show();
					Ext.getCmp('montoUtilizado').show();
					Ext.getCmp('montoDisponible').show();
					Ext.getCmp('num_Prestamos').show();
				}

				fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						inicializa:'S'
						})
					});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
			fp.el.unmask();
		}
	}

	var procesarTodosIFs = function(store, arrRegistros, opts) {
		store.insert(0,new Todas({ 
		clave: "", 
		descripcion: "Seleccionar", 
		loadMsg: ""})); 		
		store.commitChanges(); 
		Ext.getCmp('ic_if1').setValue('');
	}
	
	Todas = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
	
	var catIFData = new Ext.data.JsonStore({
		id: 'catIFData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '38ConsOperaciones.data.jsp',
		baseParams: {
			informacion: 'catIFData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarTodosIFs,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma = [{
		width:               380,
		xtype:               'combo',
		name:                'ic_if',
		id:                  'ic_if1',
		fieldLabel:          'Intermediario',
		mode:                'local',
		displayField:        'descripcion',
		valueField:          'clave',
		hiddenName:          'ic_if',
		emptyText:           'Seleccione...',
		triggerAction:       'all',
		minChars:            1,
		forceSelection:      true,
		typeAhead:           true,
		editable:            true,
		allowBlank:          true,
		hidden:              true,
		store:               catIFData,
		tpl:                 NE.util.templateMensajeCargaCombo,
		listeners: {
			select: {
				fn: function(combo) {
					Ext.getCmp('montoLineaCredito').hide();
					Ext.getCmp('montoUtilizado').hide();
					Ext.getCmp('montoDisponible').hide();
					Ext.getCmp('num_Prestamos').hide();
					Ext.Ajax.request({
						url: '38ConsOperaciones.data.jsp',
						params: {
							informacion: "valoresIniciales",
							inicializa:"IF",
							ic_if:combo.getValue()
						},
						callback: procesaValoresIniciales
					});
				}
			}
		}
	},{
		width:               380,
		xtype:               'bigdecimal',
		name:                'no_solicitud',
		id:                  'no_solicitud1',
		fieldLabel:          'No. Solicitud',
		blankText:           'Favor de capturar el n�mero de solicitud',
		msgTarget:           'side',
		maxValue:            '9999999999999999999999',
		format:              '0000000000000000000000',
		maxLength:           22,
		allowDecimals:       false,
		allowNegative:       false,
		hidden:              false
	},{
		xtype:               'compositefield',
		fieldLabel:          'Fecha de Solicitud',
		msgTarget:           'side',
		combineErrors:       false,
		items: [{
			width:            155,
			xtype:            'datefield',
			name:             'fechaSolicitudIni',
			id:               'fechaSolicitudIni',
			msgTarget:        'side',
			vtype:            'rangofecha', 
			minValue:         '01/01/1901',
			campoFinFecha:    'fechaSolicitudFin',
			margins:          '0 20 0 0',
			startDay:         0,
			allowBlank:       true
		},{
			width:            45,
			xtype:            'displayfield',
			value:            '&nbsp; al'
		},{
			width:            155,
			xtype:            'datefield',
			name:             'fechaSolicitudFin',
			id:               'fechaSolicitudFin',
			msgTarget:        'side',
			vtype:            'rangofecha', 
			minValue:         '01/01/1901',
			campoInicioFecha: 'fechaSolicitudIni',
			margins:          '0 20 0 0',
			startDay:         1,
			allowBlank:       true
		}]
	},{
		xtype:               'compositefield',
		fieldLabel:          'Fecha de Operaci�n',
		msgTarget:           'side',
		combineErrors:       false,
		items: [{
			width:            155,
			xtype:            'datefield',
			name:             'fechaOperacionIni',
			id:               'fechaOperacionIni',
			msgTarget:        'side',
			vtype:            'rangofecha',
			minValue:         '01/01/1901',
			campoFinFecha:    'fechaOperacionFin',
			margins:          '0 20 0 0',
			startDay:         0,
			allowBlank:       true
		},{
			width:            45,
			xtype:            'displayfield',
			value:            '&nbsp; al'
		},{
			width:            155,
			xtype:            'datefield',
			name:             'fechaOperacionFin',
			id:               'fechaOperacionFin',
			msgTarget:        'side',
			vtype:            'rangofecha',
			minValue:         '01/01/1901',
			campoInicioFecha: 'fechaOperacionIni',
			margins:          '0 20 0 0',
			startDay:         1,
			allowBlank:       true
		}]
	},{
		xtype:               'compositefield',
		fieldLabel:          'Fecha de Rechazo',
		msgTarget:           'side',
		combineErrors:       false,
		items: [{
			width:            155,
			xtype:            'datefield',
			name:             'fechaRechazoIni',
			id:               'fechaRechazoIni',
			msgTarget:        'side',
			vtype:            'rangofecha', 
			minValue:         '01/01/1901',
			campoFinFecha:    'fechaRechazoFin',
			margins:          '0 20 0 0',
			startDay:         0,
			allowBlank:       true
		},{
			width:            45,
			xtype:            'displayfield',
			value:            '&nbsp; al'
		},{
			width:            155,
			xtype:            'datefield',
			name:             'fechaRechazoFin',
			id:               'fechaRechazoFin',
			msgTarget:        'side',
			vtype:            'rangofecha',
			minValue:         '01/01/1901',
			campoInicioFecha: 'fechaRechazoIni',
			margins:          '0 20 0 0',
			startDay:         1,
			allowBlank:       true
		}]
	},{
		xtype:               'compositefield',
		fieldLabel:          'Monto de Solicitud de Recursos',
		msgTarget:           'side',
		combineErrors:       false,
		items: [{
			width:            155,
			xtype:            'bigdecimal',
			name:             'montoIni',
			id:               'montoIni',
			maxText:          'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
			maxValue:         '99999999999999999.99',
			msgTarget:        'side',
			format:           '00000000000000000.00',
			vtype:            'rangoValor',
			campoFinValor:    'montoFin',
			margins:          '0 20 0 0', //necesario para mostrar el icono de error
			allowDecimals:    true,
			allowNegative:    false,
			hidden:           false,
			allowBlank:       true
		},{
			width:            45,
			xtype:            'displayfield',
			value:            'hasta'
		},{
			width:            155,
			xtype:            'bigdecimal',
			name:             'montoFin',
			id:               'montoFin',
			maxText:          'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
			maxValue:         '99999999999999999.99',
			msgTarget:        'side',
			format:           '00000000000000000.00',
			vtype:            'rangoValor',
			campoInicioValor: 'montoIni',
			margins:          '0 20 0 0', //necesario para mostrar el icono de error
			allowDecimals:    true,
			allowNegative:    false,
			hidden:           false,
			allowBlank:       true
		}]
	},{
		xtype:               'displayfield',
		id:                  'montoLineaCredito',
		style:               'text-align:left;',
		fieldLabel:          'Monto L�nea Credito',
		text:                '-'
	},{
		xtype:               'displayfield',
		id:                  'montoUtilizado',
		style:               'text-align:left;',
		fieldLabel:          'Monto Utilizado',
		text:                '-'
	},{
		xtype:               'displayfield',
		id:                  'montoDisponible',
		style:               'text-align:left;',
		fieldLabel:          'Monto Disponible',
		text:                '-'
	},{
		xtype:               'displayfield',
		id:                  'num_Prestamos',
		style:               'text-align:left;',
		fieldLabel:          'N�mero de Prestamos',
		text:                '-'
	},{
		xtype:               'displayfield',
		id:                  'saldoFondoLiquido',
		style:               'text-align:left;',
		fieldLabel:          'Saldo Fondo L�quido',
		text:                '-',
		hidden:              true
	}];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Consulta de Operaciones',
		frame: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 140,
		//monitorValid: true,
		defaults: {
			msgTarget: 'side'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {

				// El siguiente IF es solo para validar el formato de las fechas
				if(!Ext.getCmp('forma').getForm().isValid()){
					return;
				}
				if(Ext.getCmp('fechaSolicitudIni').getValue() != '' && Ext.getCmp('fechaSolicitudFin').getValue() == ''){
					Ext.getCmp('fechaSolicitudFin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco')
					return;
				}
				if(Ext.getCmp('fechaSolicitudIni').getValue() == '' && Ext.getCmp('fechaSolicitudFin').getValue() != ''){
					Ext.getCmp('fechaSolicitudIni').markInvalid('Debe capturar ambas fechas o dejarlas en blanco')
					return;
				}
				if(Ext.getCmp('fechaOperacionIni').getValue() != '' && Ext.getCmp('fechaOperacionFin').getValue() == ''){
					Ext.getCmp('fechaOperacionFin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco')
					return;
				}
				if(Ext.getCmp('fechaOperacionIni').getValue() == '' && Ext.getCmp('fechaOperacionFin').getValue() != ''){
					Ext.getCmp('fechaOperacionIni').markInvalid('Debe capturar ambas fechas o dejarlas en blanco')
					return;
				}
				if(Ext.getCmp('fechaRechazoIni').getValue() != '' && Ext.getCmp('fechaRechazoFin').getValue() == ''){
					Ext.getCmp('fechaRechazoFin').markInvalid('Debe capturar ambas fechas o dejarlas en blanco')
					return;
				}
				if(Ext.getCmp('fechaRechazoIni').getValue() == '' && Ext.getCmp('fechaRechazoFin').getValue() != ''){
					Ext.getCmp('fechaRechazoIni').markInvalid('Debe capturar ambas fechas o dejarlas en blanco')
					return;
				}
					var montoIni = Ext.getCmp('montoIni');
					var montoFin = Ext.getCmp('montoFin');

					if(!Ext.isEmpty(montoIni.getValue()) || !Ext.isEmpty(montoFin.getValue())){
						if(Ext.isEmpty(montoIni.getValue())){
							montoIni.markInvalid('Debe capturar ambas montos o dejarlas en blanco');
							montoIni.focus();
							return;
						}
						if(Ext.isEmpty(montoFin.getValue())){
							montoFin.markInvalid('Debe capturar ambas montos o dejarlas en blanco');
							montoFin.focus();
							return;
						}
					}

					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '38ConsOperaciones.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});

	catIFData.load();
	
	fp.el.mask('Cargando Datos ...', 'x-mask-loading');		
	
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '38ConsOperaciones.data.jsp',
		params: {
			informacion: "valoresIniciales",
			inicializa:"S"
		},
		callback: procesaValoresIniciales
	});
	
	
});