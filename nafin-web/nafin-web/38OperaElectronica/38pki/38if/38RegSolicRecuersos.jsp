<!DOCTYPE html>
<%@ page import="java.util.*, com.netro.electronica.*," 
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession.jspf" %>
<%@ include file="/38OperaElectronica/certificado.jspf" %>

<%
	String  ic_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
	String  tipoSolicitud = (request.getParameter("tipoSolicitud")!=null)?request.getParameter("tipoSolicitud"):"Captura";
	String  ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
	
	OperacionElectronica  eletronicaBean = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class); 

	if(tipoSolicitud.equals("Captura") ) {
		if(ic_solicitud.equals("")){ 
			ic_solicitud = ic_solicitud = eletronicaBean.getNumaxSolicitud();
		}else {
			ic_solicitud = ic_solicitud;
		}
	}
	
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>


<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
<%@ include file="/00utils/componente_firma.jspf" %>
<script type="text/javascript" src="38RegSolicRecuersos.js?<%=session.getId()%>"></script>

</head>

<%@ include file="/01principal/menu.jspf"%>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01if/cabeza.jspf"%>

<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
      <div id="areaContenido"><div style="height:230px"></div></div>
   </div>
</div>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="ic_solicitud" name="ic_solicitud" value="<%=ic_solicitud%>"/>		
	<input type="hidden" id="tipoSolicitud" name="tipoSolicitud" value="<%=tipoSolicitud%>"/>	
	<input type="hidden" id="ic_if" name="ic_if" value="<%=ic_if%>"/>	
</form>

<%@ include file="/01principal/01if/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>

</body>
</html>