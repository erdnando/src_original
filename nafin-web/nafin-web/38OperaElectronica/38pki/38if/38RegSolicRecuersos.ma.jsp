<%@ page 	import="	java.util.*, 
				netropology.utilerias.*, 
				javax.naming.*,
				java.io.*,				
				org.apache.commons.fileupload.disk.*,
				org.apache.commons.fileupload.servlet.*,
				org.apache.commons.fileupload.*,	
				com.netro.electronica.*,
				net.sf.json.*"
	contentType="text/html; charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"							
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf" %>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/>

<%
String  ic_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";
String  tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String  tipoSolicitud = (request.getParameter("tipoSolicitud")!=null)?request.getParameter("tipoSolicitud"):"";

String path_destino = strDirectorioTemp;
String mensaje ="", resultadoT= "N", nombreArchivo= "", extension ="";
String clave[]  = null;
int registros   =0,	 numFiles = 0;
boolean ok_file = true;
String rutaArchivoTemporal = null;
ParametrosRequest req = null;
String infoRegresar = "";
int tamanio = 0;
String itemArchivo = "";
boolean resultado  = false;
int  tamaniArch_1 =5060720;
int  tamanioMB =5;
String mensajeL= "Error, el Archivo es muy Grande, excede el Límite que es de 5 MB.";

if(tipoArchivo.equals("RUG") ) { 
	tamanioMB = 10; 
	tamaniArch_1 =10121440 ;
	mensajeL = "Error,  el Archivo es muy Grande, excede el Límite que es de 10 MB.";
	}  


JSONObject jsonObj = new JSONObject();


if (ServletFileUpload.isMultipartContent(request)) {

	try {	
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		factory.setRepository(new File(strDirectorioTemp));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// Set overall request size constraint
		upload.setSizeMax(tamanioMB * tamaniArch_1);	//MB
		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));		
		// Process the uploaded items
		FileItem item = (FileItem)req.getFiles().get(0);
		
		itemArchivo		= (String)item.getName();
		tamanio			= (int)item.getSize();
		
		int  top  = itemArchivo.length();
		int  pos  = itemArchivo.indexOf(".");		
		extension=itemArchivo.substring(pos, top);
	
		nombreArchivo= Comunes.cadenaAleatoria(16)+extension;
		
		if(tamanio > tamaniArch_1) {					    
			mensaje= mensajeL;
			ok_file=false;
		}else  {		
			System.out.println("   nombreArchivo "+nombreArchivo);						 
			path_destino = strDirectorioTemp+nombreArchivo;
			System.out.println("path_destino    path_destino "+path_destino);	
			item.write(new File(path_destino));
				
			/************************************************************/
			//se ingresa la imagen del archivo			
			OperacionElectronica  eletronicaBean = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class); 
		
			String archivo1=path_destino;
			if(tipoArchivo.equals("Cotiza")  ||  tipoArchivo.equals("Cartera_Prenda")||  tipoArchivo.equals("Cartera_Prenda_Pdf")  ||   tipoArchivo.equals("RUG") ) {
				resultado= 	eletronicaBean.guardarArchivoSolic(ic_solicitud, archivo1, tipoArchivo, tipoSolicitud  );
				
			}else  if(tipoArchivo.equals("Tabla")) {
			
				if(mensaje.equals("")){
					resultado=true;
				}
			}
		
			if(resultado){  
				resultadoT = "S";  mensaje =""; 
			}else  {
				resultadoT = "N";
			}		
		}
		
	} catch(Exception e) {		
		mensaje= mensajeL;
		e.printStackTrace();  
		System.out.println("Error al actualizar archivo"+e);
  }
  
  
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", resultadoT);
	jsonObj.put("mensaje", mensaje);	
	jsonObj.put("nombreArchivo", nombreArchivo);	
	infoRegresar	 =jsonObj.toString();
		
}

%>
<%=infoRegresar%>
