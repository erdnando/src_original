<!DOCTYPE html>
<%@ page import="java.util.*, com.netro.electronica.*" 
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession.jspf" %>
<%@ include file="/38OperaElectronica/certificado.jspf" %>
<%String auxStrPerfil = strPerfil;
String auxiNoCliente = iNoCliente;

%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
<script type="text/javascript" src="38MantoSolRecursos.js?<%=session.getId()%>"></script>
<%@ include file="/00utils/componente_firma.jspf" %>
</head>

<%@ include file="/01principal/menu.jspf"%>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01if/cabeza.jspf"%>

<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
      <div id="areaContenido"><div style="height:230px"></div></div>
   </div>
</div>

<%@ include file="/01principal/01if/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
<form id='formParametros' name="formParametros">
	<input type="hidden" id="auxStrPerfil" name="auxStrPerfil" value="<%=auxStrPerfil%>"/>
		<input type="hidden" id="auxiNoCliente" name="auxiNoCliente" value="<%=auxiNoCliente%>"/>

</form>

</body>
</html>