<!DOCTYPE html>
<%@ page import="java.util.*, com.netro.electronica.*," 
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession.jspf" %>
<%String auxStrPerfil = strPerfil;
String auxiNoCliente = iNoCliente;
String iTipoAfiliadoAux = iTipoAfiliado;
String iNoUsuariAux = iNoUsuario;

%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
<script type="text/javascript" src="38ConsOperaciones.js?<%=session.getId()%>"></script>
<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="/38OperaElectronica/certificado.jspf" %>
</head>

<%@ include file="/01principal/menu.jspf"%>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%if(iTipoAfiliado.equals("I"))  {%>
<%@ include file="/01principal/01if/cabeza.jspf"%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
      <div id="areaContenido"><div style="height:230px"></div></div>
   </div>
</div>
<%@ include file="/01principal/01if/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
<form id='formParametros' name="formParametros">
	
						<input type="hidden" id="iNoUsuario" name="auxiNoCliente" value="<%=iNoUsuariAux%>"/>


</form>

<%}else if(iTipoAfiliado.equals("N"))  {%>

<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
      <div id="areaContenido"><div style="height:230px"></div></div>
   </div>
</div>

<%@ include file="/01principal/01nafin/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
<form id='formParametros' name="formParametros">
	
						<input type="hidden" id="iNoUsuario" name="auxiNoCliente" value="<%=iNoUsuariAux%>"/>


</form>
<%}%>


</body>


</html>