Ext.onReady(function() {
 //OBJETO DATOS GENERALES///////////////////////////////////////////////////////
 var ObjGral = {
	numProc: '0',
	recibo: '-'
 };
 
 //HANDLERS PARA OBJETOS BUTTON/////////////////////////////////////////////////


 
 var ajaxPeticion = function(numProceso, flag){ 
		
		if(flag=='1'){
			gridLayout.hide();
		}
		
		Ext.Ajax.request({
			url: '38cargaFondoLiquido01ext.data.jsp',
			params: {
				informacion: 'ValidaCargaArchivo',
				numProc: numProceso,
				flagValidaThread: flag
			},
			callback: procesarSuccessValidaCarga
		});
	
	};

 
	var ajaxPeticionCarga = function(numProc, flag){
		if(flag=='1'){
			pnl.el.mask('Procesando...', 'x-mask-loading');
		}
		
		Ext.Ajax.request({
			url : '15consCargaProvEpoExt.data.jsp',
			params :{
				informacion: 'enviaCorreo',
				numProc : ObjGral.numProc
			},
			callback: procesarSuccessEnviaCorreo
		});
	
	}
 
 var processSuccessFailureObtieneArchivo =  function(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var archivo = resp.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			
			fpArchivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpArchivo.getForm().getEl().dom.submit();
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
 
 var procesarSuccessValidaCarga = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			if(resp.flagValidaThread == '2'){
				ajaxPeticion(resp.numProc, resp.flagValidaThread);
			}
			if(resp.flagValidaThread == '3'){
				ObjGral.numProc = resp.numProc;
				storeRegErroneosData.loadData(resp);
				storeRegCorrectosData.loadData(resp);
				contenedorPrincipalCmp.add(tabResultValidFile);
				contenedorPrincipalCmp.doLayout();
				var cont = 0;
				var contErr = 0;
				var monto = resp.MONTO;
				storeRegErroneosData.each(function(record){
					contErr++;
				});
				storeRegCorrectosData.each(function(record){
					cont++;
				});
				Ext.getCmp('totalRegC').setValue(cont);
				//Ext.getCmp('totalSaldoC').setValue(Ext.util.Format.number(monto,'$0,0.00'));
				Ext.getCmp('totalSaldoC').setValue("$ "+monto);

				if(contErr>0){
					Ext.getCmp('btnEnvCorreo').disable();
				}else if(cont>0){
					Ext.getCmp('btnEnvCorreo').enable();
				}
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}   
	}

	var procesarSuccessEnviaCorreo = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			if(resp.flagEnviaThread == '2'){
				ajaxPeticionCarga(resp.numProc, resp.flagEnviaThread);
			}
			if(resp.flagValidaThread == '3'){
				pnl.el.unmask();
				Ext.Msg.alert('Aviso', 'La carga y env�os de Saldos Diarios se realiz� con �xito', function(){
					location.reload();
				});
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
 var btnContinuarCarga = function(){
	var objRadio = Ext.getCmp("tipoAjuste");
	var tipoModifi = objRadio.getValue().inputValue;
	var record = storeResumValidData.getAt(0);
	
	if(record.data['TOTALREG']=='0'){
		Ext.MessageBox.alert('Aviso','No hay registros que procesar');
	}else{

		ajaxPeticionCarga(ObjGral.numProc,tipoModifi,'1');

	}
  }
  

 

 
 var procesarSuccessGenerarArchivoPDF = function(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var btnGenararPDF = Ext.getCmp('btnGenararPDF');
		
		btnGenararPDF.setIconClass('');
		btnGenararPDF.enable();
		btnGenararPDF.el.highlight('FFF700', {duration: 8, easing:'bounceOut'});
		btnGenararPDF.setHandler( function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
		btnGenararPDF.setText("Abrir PDF");
	}else{
		NE.util.mostrarConnError(response,opts);
	}
 
 }
 
//STORES////////////////////////////////////////////////////////////////////////
 var storeLayoutData = new Ext.data.ArrayStore({
	  fields: [
		  {name: 'NUMCAMPO'},
		  {name: 'DESC'},
		  {name: 'TIPODATO'},
		  {name: 'LONGITUD'},
		  {name: 'OBLIGATORIO'},
		  {name: 'OBSERVACION'}
	  ],
	  data: [
				['1','N�mero de Contrato','Num�rico','10','Si', 'N�mero de contrato del intermediario'],
				['2','Nombre del Intermediario','Alfanum�rico','100','Si', 'Nombre completo del intermediario'],
				['3','Clave SIRAC del IF','Num�rico','6','Si', 'Clave asignada por SIRAC al intermediario'],
				['4','Saldo Hoy','Num�rico','17,5','Si', 'Saldo Fondo L�quido'],
				['5','Moneda','Num�rico','3','Si', 'Clave 1(Moneda Nacional) � 54(Dolar Americano)']
			]
 });
 
	var storeRegCorrectosData = new Ext.data.JsonStore({
		root : 'regCorrectos',
		fields: [
		  {name: 'LINEA'},
		  {name: 'CONTRATO'},
		  {name: 'SALDOHOY'}
		]
	});
 
	var storeRegErroneosData = new Ext.data.JsonStore({
		root : 'regErroneos',
		fields: [
		  {name: 'LINEA'},
		  {name: 'CONTRATO'},
		  {name: 'ERROR'}
		]
	});

//OBJETOS AGREGADOS EN CONTENEDORES///////////////////////////////////////////// 
 

 
 var elementosFormCarga = [
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){
						var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
						
						if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}
					
					}
				},{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo1',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									width: 320,
									emptyText: 'Ruta del Archivo',
									fieldLabel: 'Ruta del Archivo',
									name: 'archivoCesion',
									buttonText: 'Examinar...',
									buttonCfg: {
									  //iconCls: 'upload-icon'
									},
									//anchor: '95%',
									regex: /^.*\.(csv|CSV)$/,
									regexText:'el archivo debe tener formato CSV'
								},
								{
									xtype: 'hidden',
									id:	'hidExtension',
									name:	'hidExtension',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidNumTotal',
									name:	'hidNumTotal',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidTotalMonto',
									name:	'hidTotalMonto',
									value:	''
								},
								{
									xtype: 	'button',
									text: 	'Continuar',
									id: 		'btnContinuar',
									iconCls: 'icoContinuar',
									style: { 
										  marginBottom:  '10px' 
									},
									handler: function(){
										var cargaArchivo = Ext.getCmp('archivo');
										if (!cargaArchivo.isValid()){
											cargaArchivo.focus();
											return;
										}else if (cargaArchivo.getValue()==''){
											Ext.Msg.alert("Aviso", "Seleccione un archivo");
											return;
										}
										var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
										var extArchivo = Ext.getCmp('hidExtension');
										//var numTotal = Ext.getCmp('hidNumTotal');
										//var totalMonto = Ext.getCmp('hidTotalMonto');
										var objMessage = Ext.getCmp('pnlMsgValid1');
										
										
										objMessage.hide();
										//numTotal.setValue(Ext.getCmp('numtotalvenc1').getValue());
										//totalMonto.setValue(Ext.getCmp('montototalvenc1').getValue());
										
										if (/^.*\.(csv)$/.test(ifile)){
											extArchivo.setValue('csv');
										}
										
										fpCarga.getForm().submit({
											url: '38cargaFondoLiquidoFile01ext.jsp',
											waitMsg: 'Enviando datos...',
											success: function(form, action) {
												var resp = action.result;
												var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
												var objMsg = Ext.getCmp('pnlMsgValid1');

												if(!resp.flag){
													//SE MOSTRARAN LEYENDAS DE VALIDACIONES
													objMsg.show();
													//objMsg.body.update('El tama�o del archivo ZIP es mayor al permitido');
													objMsg.body.update(resp.msgError);
												}else if(!resp.flagExt){
													objMsg.show();
													objMsg.body.update('El archivo contenido dentro del ZIP no es un archivo TXT');
												}else if(!resp.flagReg){
													objMsg.show();
													objMsg.body.update('El archivo TXT no puede contener mas de 30000 registros');
												}else{
													ObjGral.numProc = resp.numProceso;
													ajaxPeticion(resp.numProceso, '1');
												
												}

											},
											failure: NE.util.mostrarSubmitError
										})	
									}
								}
							]
						}
							
					]
				},
				{
					xtype: 'panel',
					name: 'pnlMsgValid',
					id: 'pnlMsgValid1',
					width: 500,
					style: 'margin:0 auto;',
					frame: false,
					hidden: true
				
				}
			]
		},
		{
			xtype: 'panel',
			name: 'pnlMsgFormato',
			id: 'pnlMsgFormato',
			width: 500,
			style: 'margin:0 auto;',
			frame: false,
			html : '<table width="300" border="0" align="center" bgcolor="#CAC6C6"><tr><td align="center">El archivo debe tener formato CSV</td></tr></table>'
		
		}
		//barraProgreso
	];
 
 //CONTENEDORES/////////////////////////////////////////////////////////////////

	
	 var fpCarga = new Ext.form.FormPanel({
		id: 'formaCarga',
		width: 600,
		title: 'Carga de Archivo',
		frame: true,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px; text-align:left;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormCarga,		
		monitorValid: true
	});
	
	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
			{
				header : 'No.',
				dataIndex : 'NUMCAMPO',
				width : 30,
				sortable : true
			},
			{
				header : 'Nombre del Campo',
				tooltip: 'Nombre del Campo',
				dataIndex : 'DESC',
				width : 140,
				sortable : true
			},
			{
				header : 'Tipo',
				dataIndex : 'TIPODATO',
				width : 100,
				sortable : true
			},
			{
				header : 'Longitud M�xima',
				tooltip: 'Longitud M�xima',
				dataIndex : 'LONGITUD',
				width : 99,
				sortable : true
			},
			{
				header : 'Obligatorio',
				tooltip: 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 65,
				sortable : true
			},
			{
				header : 'Observaciones',
				dataIndex : 'OBSERVACION',
				width : 270,
				sortable : true
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 600,
		height: 190,
		title: '<div align="left">Descripci�n del Layout de Carga</div>',
		frame: true
	});


	
	var gridRegErroneos = new Ext.grid.GridPanel({
		id: 'gridRegErroneos1',
		store: storeRegErroneosData,
		margins: '20 0 0 0',
		//hideHeaders : true,
		columns: [
			{
				header : 'Linea',
				dataIndex : 'LINEA',
				width : 50,
				sortable : true
			},
			{
				header : 'Contratos',
				dataIndex : 'CONTRATO',
				width : 80,
				sortable : true
			},
			{
				header : 'Descripci�n',
				dataIndex : 'ERROR',
				width : 400,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = "ext:qtip='" + Ext.util.Format.nl2br(causa) + "'";
					columna.attr = 'style="white-space: normal; word-wrap:break-word;" ';
					//return "<pre>"+causa+"</pre>";
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 580,
		height: 260,
		style: 'margin:0 auto;',
		//autoHeight : true,
		title: '',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Cancelar',
					id:'btnCancelarE',
					handler: function(btn){
						window.location.href='38cargaFondoLiquido01ext.jsp';
					}
				},
				'-',
				{
					text: 'Extraer Errores',
					id:'btnErrores',
					//disabled: true,
					handler: function(btn){
						pnl.el.mask('Generando Archivo...', 'x-mask-loading');
						Ext.Ajax.request({
							url : '38cargaFondoLiquido01ext.data.jsp',
							params :{
								informacion: 'generarArchivoErrores',
								numProc : ObjGral.numProc
							},
							callback: processSuccessFailureObtieneArchivo
						});
					}
				}
			]
			
		}
	});
	
	var gridRegCorrectos = new Ext.grid.GridPanel({
		id: 'gridRegCorrectos1',
		store: storeRegCorrectosData,
		margins: '20 0 0 0',
		//hideHeaders : true,
		columns: [
			{
				header : 'Linea',
				dataIndex : 'LINEA',
				width : 50,
				sortable : true
			},
			{
				header : 'Contratos',
				dataIndex : 'CONTRATO',
				width : 480,
				sortable : true
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 580,
		height: 260,
		style: 'margin:0 auto;',
		//autoHeight : true,
		title: '',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Enviar Correo',
					id:'btnEnvCorreo',
					disabled: true,
					handler: function(btn){
						pnl.el.mask('Procesando...', 'x-mask-loading');
						Ext.Ajax.request({
							url : '38cargaFondoLiquido01ext.data.jsp',
							params :{
								informacion: 'enviaCorreo',
								numProc : ObjGral.numProc
							},
							callback: procesarSuccessEnviaCorreo
						});
					}
				},
				'-',
				{
					text: 'Cancelar',
					id:'btnCancelar',
					handler: function(btn){
						window.location.href='38cargaFondoLiquido01ext.jsp';
					}
				}
			]
			
		}
	});
	
	var tabRegErrores = new Ext.Panel({
		title:'Con Errores',
		frame: true,
		id:'tabRegErrores1',
		height: 300,
		width: 590,
		//autoScroll: true,
		items:[
			NE.util.getEspaciador(5),
			gridRegErroneos
		]
	});
	
	var pnlAcumulado = new Ext.Panel({
		title:'',
		frame: false,
		id:'pnlAcumulado',
		height: 100,
		width: 570,
		labelWidth: 230,
		layout:'form',
		style: 'margin:0 auto;',
		//autoScroll: true,
		items:[
			{
				xtype:'textfield',
				name: 'totalRegC',
				fieldLabel: 'Total de Contratos de Saldos Diarios',
				readOnly: true,
				id: 'totalRegC'
			},
			{
				xtype:'textfield',
				name: 'totalSaldoC',
				readOnly: true,
				fieldLabel: 'Monto de Saldo Diario',
				id: 'totalSaldoC'
			}
		]
	});
	
	var tabRegCorrectos = new Ext.Panel({
		title:'Sin Errores',
		frame: true,
		id:'tabRegCorrectos1',
		height: 450,
		width: 590,
		//autoHeight : true,
		items:[
			NE.util.getEspaciador(5),
			gridRegCorrectos,
			NE.util.getEspaciador(5),
			pnlAcumulado
		]
	});
	
	
	var tabResultValidFile = new Ext.TabPanel({
		id: 'tabResultValidFile1',
		border: true,
		frame: true,
		activeTab: 0,
		//width: 610,
		width: 600,
		height : 450,
		style:			'margin:0 auto;',
		items:[ tabRegCorrectos, tabRegErrores]
	});
	
	
	//--------------PANEL PARA ABRIR ARCHIVOS---------------------------------------
	var fpArchivo = new Ext.form.FormPanel({
		id: 'fpArchivo1',
		width: 600,
		title: '',
		frame: true,
		hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;'
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fpArchivo,
			NE.util.getEspaciador(10),
			fpCarga,
			NE.util.getEspaciador(20),
			gridLayout
		]
	});
	
	

});