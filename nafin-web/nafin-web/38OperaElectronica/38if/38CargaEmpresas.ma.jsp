<%@ page 	import="	java.util.*, 
				netropology.utilerias.*, 
				javax.naming.*,
				java.io.*,				
				org.apache.commons.fileupload.disk.*,
				org.apache.commons.fileupload.servlet.*,
				org.apache.commons.fileupload.*,	
				com.netro.electronica.*,
				net.sf.json.*"
	contentType="text/html; charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"							
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf" %>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/>

<%
String  ic_solicitud = (request.getParameter("ic_solicitud")!=null)?request.getParameter("ic_solicitud"):"";

String path_destino = strDirectorioTemp;
String mensaje ="", resultadoT= "N";
String clave[]  = null;
int registros   =0,	 numFiles = 0;
boolean ok_file = true;
String rutaArchivoTemporal = null;
ParametrosRequest req = null;
String infoRegresar = "";
int tamanio = 0;
String itemArchivo = "";

JSONObject jsonObj = new JSONObject();


if (ServletFileUpload.isMultipartContent(request)) {

	try {	
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		factory.setRepository(new File(strDirectorioTemp));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// Set overall request size constraint
		upload.setSizeMax(10 * 2097152);	//10 Kb
		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));		
		// Process the uploaded items
		FileItem item = (FileItem)req.getFiles().get(0);
		
		itemArchivo		= (String)item.getName();
		tamanio			= (int)item.getSize();
		
		if(tamanio > 5242880) {
			mensaje= "Error, el Archivo es muy Grande, excede el Límite que es de 5 MB.";
			ok_file=false;
		}		
		if(itemArchivo.length()>39){
			itemArchivo =itemArchivo.substring(0,35)+".txt";
		}
		path_destino = strDirectorioTemp+itemArchivo;
		item.write(new File(path_destino));
		
	} catch(Exception e) {
		mensaje = mensaje;
		e.printStackTrace();  
		System.out.println(mensaje +e);
  }
  
  
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("mensaje", mensaje);	
	jsonObj.put("nombreArchivo", itemArchivo);	
	infoRegresar	 =jsonObj.toString();

}

%>
<%=infoRegresar%>
