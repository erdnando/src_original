<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession.jspf"%>

<html>
<head>
<title>Nafi@net - Autorizaciones de Solicitudes</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<%@ include file="/01principal/menu.jspf"%>

<script type="text/javascript" src="38cargaFondoLiquido01ext.js?<%=session.getId()%>"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
      <div id="areaContenido"><div style="height:230px"></div></div>
   </div>
</div>
<%@ include file="/01principal/01nafin/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
<input type="hidden" name="strNombreUsuario" value="<%=strNombreUsuario%>">
<input type="hidden" name="iNoUsuario" value="<%=iNoUsuario%>">
</body>
</html>
