var showPanelDescripcionLayoutCsv;
var showPanelDescripcionLayoutPdf;

var showPanelDescripcionLayout = function(campo){
	if( campo === 'archivoCsv'){
		showPanelDescripcionLayoutCsv();
	} else if( campo === 'archivoPdf' ){
		showPanelDescripcionLayoutPdf();
	}
};

Ext.onReady(function() {
	
	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});
	
	//-------------------------------- VALIDACIONES -----------------------------------
 
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaArchivo(resp.estadoSiguiente,resp);
					}
				).getDialog().setZIndex(1000000);
			} else {
				cargaArchivo(resp.estadoSiguiente,resp);
			}
			
		} else {
			
			// Ocultar ventana preparar validacion si esta esta siendo mostrada
         hideWindowPrepararValidacion();
         
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceValidacion();
         
         // Ocultar mascara de la forma de carga de archivos
         var element = Ext.getCmp("panelFormaCargaArchivos").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
         // Ocultar mascara del panel de resultados de la validacion
         var element = Ext.getCmp("panelResultadosValidacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var cargaArchivo = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"					){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'38actcarpre01ext.data.jsp',
				params: 	{
					informacion:		'CargaArchivo.inicializacion'
				},
				callback: 				procesaCargaArchivo
			});
			
		} else if(			estado == "MOSTRAR_AVISO"								){

			// Ocultar Box con animacion de inicializacion
			Ext.getCmp('contenedorInicializacion').hide();
			
			if(!Ext.isEmpty(respuesta.aviso)){
				
				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);
				
				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();
				
			}

		} else if(			estado == "ESPERAR_CAPTURA_ARCHIVOS"	){
			 
			// Ocultar Box con animacion de inicializacion
			Ext.getCmp('contenedorInicializacion').hide();
			
			// Mostrar panel de carga de archivo
			var panelFormaCargaArchivos = Ext.getCmp("panelFormaCargaArchivos");
			panelFormaCargaArchivos.show();
			
			// Definir el texto del layout de carga del archivo CSV
			if(!Ext.isEmpty(respuesta.descripcionLayoutCsv)){
				Ext.getCmp("labelLayoutCargaCsv").setText(respuesta.descripcionLayoutCsv,false);
			}
			
			// Definir el texto del layout de carga del archivo PDF
			if(!Ext.isEmpty(respuesta.descripcionLayoutPdf)){
				Ext.getCmp("labelLayoutCargaPdf").setText(respuesta.descripcionLayoutPdf,false);
			}
			
		} else if(			estado == "SUBIR_ARCHIVOS"								){
			
			var detalleRegistrosConErrores 				= Ext.getCmp("detalleRegistrosConErrores").setValue(detalleRegistrosConErrores);
			var numeroRegistrosConErrores  				= Ext.getCmp("numeroRegistrosConErrores").setValue(numeroRegistrosConErrores);
         
			detalleRegistrosConErrores.setValue(null);
			numeroRegistrosConErrores.setValue(null);
				
			// Mostrar espaciador del Panel de resultados
			Ext.getCmp("espaciadorPanelResultadosValidacion").hide();
			// Mostrar Panel de resultados
			Ext.getCmp("panelResultadosValidacion").hide();

			Ext.getCmp("panelFormaCargaArchivos").getForm().submit({
				clientValidation: 	true,
				url: 						'38actcarpre01ext.data.jsp?informacion=CargaArchivo.subirArchivos',
				waitMsg:   				'Subiendo archivo(s)...',
				waitTitle: 				'Por favor espere',
				success: function(form, action) {
					 procesaCargaArchivo(null,  true,  action.response );
				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaCargaArchivo(null,  false, action.response );
				}
			});
			
		} else if(			estado == "REALIZAR_VALIDACION"         			){

			// Mostrar ventana de inicializacion de la validacion
			showWindowPrepararValidacion();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 		'38actcarpre01ext.data.jsp',
				params: 	{
					informacion:		'CargaArchivo.realizarValidacion',
					archivoCsv:			respuesta.archivoCsv,
					archivoCsvReal:	respuesta.archivoCsvReal,
					archivoPdf:			respuesta.archivoPdf,
					archivoPdfReal:	respuesta.archivoPdfReal
				},
				callback:	procesaCargaArchivo
			});
			
		} else if(			estado == "SOLICITAR_AUTORIZACION_VALIDACION"		         ){

			// Ocultar ventana de inicializacion de la validacion
			hideWindowPrepararValidacion();
			
			// Solicitar Autorizacion de Validacion
			var parametros = {
      		archivoCsv: 						respuesta.archivoCsv,
      		archivoPdf:							respuesta.archivoPdf,
      		numeroTotalRegistrosProcesar:	respuesta.numeroTotalRegistrosProcesar
      	};
			showWindowSolicitarAutorizacionValidacion(parametros);
			
		} else if(			estado == "MUESTRA_PANTALLA_AVANCE"		         ){

			// Ocultar ventana de inicializacion de la validacion
			hideWindowPrepararValidacion();
			
			// Mostrar popup de avance de la validacion
			showWindowAvanceValidacion(respuesta.numeroTotalRegistrosProcesar);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 		'38actcarpre01ext.data.jsp',
				params: 	{
					informacion: 						'CargaArchivo.muestraPantallaAvance',
					archivoCsv: 						respuesta.archivoCsv,
					archivoPdf:							respuesta.archivoPdf,
					numeroTotalRegistrosProcesar:	respuesta.numeroTotalRegistrosProcesar
				},
				callback: 		 procesaCargaArchivo
			});
	
		} else if( estado == "ACTUALIZAR_AVANCE"						) {
 			
			// Actualizar porcensaje de avance

			var avance								= respuesta.avance;
			var textoPorcentajeAvance 			= formateaPorcentajeAvance(respuesta.porcentajeAvance);
			var numeroRegistrosProcesados		= respuesta.numeroRegistrosProcesados;
	
 			var barrarAvanceValidacion			= Ext.getCmp("barrarAvanceValidacion");
			var valorRegistrosProcesados		= Ext.getCmp("valorRegistrosProcesados");

			barrarAvanceValidacion.updateProgress( Number(avance), textoPorcentajeAvance, true);
			valorRegistrosProcesados.setText(numeroRegistrosProcesados);
		
			// En 1.618 segundos "aureos" volver a consultar el porcentaje de avance
			var actualizarAvanceTask = new Ext.util.DelayedTask(
				function(){
					Ext.Ajax.request({
						url: 		'38actcarpre01ext.data.jsp',
						params: 	{
							informacion: 			'CargaArchivo.actualizarAvance',
							envioFinalizado: 		respuesta.envioFinalizado,
							archivoCsv: 			respuesta.archivoCsv,
							archivoPdf:				respuesta.archivoPdf
						},
						callback: procesaCargaArchivo 
					});
				},
				this
			);
			actualizarAvanceTask.delay(1618);
		
		} else if(  estado == "SOLICITAR_AUTORIZACION_ENVIO"   ){
			
			// Ocultar ventana de avance validacion
			hideWindowAvanceValidacion();
			
			// Solicitar al usuario confirmar la operaci�n
			showWindowAutorizacionEnvio(respuesta.numeroRegistros);
			
		} else if(  estado == "PREPARAR_DETALLE_ERRORES_VALIDACION" ){
			
			Ext.Ajax.request({
				url: 		'38actcarpre01ext.data.jsp',
				params: 	{
					informacion:	'CargaArchivo.prepararDetalleErroresValidacion',
					archivoCsv:		respuesta.archivoCsv,
					archivoPdf:		respuesta.archivoPdf
				},
				callback: 			procesaCargaArchivo
			});
			
		} else if(  estado == "PRESENTAR_DETALLE_ERRORES_VALIDACION" ){
			
			var hayErroresContenidoArchivoCsv = Ext.isDefined(respuesta.detalleRegistrosConErrores) || Ext.isDefined(respuesta.numeroRegistrosConErrores) ?true:false;
			
			if( hayErroresContenidoArchivoCsv ){
				
				var detalleRegistrosConErrores 						= respuesta.detalleRegistrosConErrores;
				var numeroRegistrosConErrores 						= respuesta.numeroRegistrosConErrores;
				
				// Inicializar panel de resultados
			
				Ext.getCmp("detalleRegistrosConErrores").setValue(detalleRegistrosConErrores);
				Ext.getCmp("numeroRegistrosConErrores").setValue(numeroRegistrosConErrores);
         
			}

			// Resetear campo de carga de archivo
			// Ext.getCmp("archivoCsv").reset();
			// Ext.getCmp("archivoPdf").reset();
			
			// Ocultar ventana de avance validacion
			hideWindowPrepararValidacion();
			hideWindowAvanceValidacion();
			 
			if( hayErroresContenidoArchivoCsv ){
				
				// Mostrar espaciador del Panel de resultados
				Ext.getCmp("espaciadorPanelResultadosValidacion").show();
				// Mostrar Panel de resultados
				Ext.getCmp('panelResultadosValidacion').show();
				
         }
         
         if(!Ext.isEmpty(respuesta.msgErrores)){
         	Ext.Msg.alert(
					'Mensaje',
					respuesta.msgErrores
				).getDialog().setZIndex(1000000);
         }
 
      } else if(  estado == "TRANSMITIR_REGISTROS" 		){ 
						
			// Agregar mensaje transmitiendo registros
			Ext.getCmp("panelFormaCargaArchivos").el.mask("Transmitiendo archivos...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 												'38actcarpre01ext.data.jsp',
				params: {	
					informacion:								'CargaArchivo.transmitirRegistros'
				},
				callback: 										procesaCargaArchivo
			});

      } else if(  estado == "ESPERAR_DECISION"  			){
      	
      	// Ocultar ventana preparar validacion si esta esta siendo mostrada
         hideWindowPrepararValidacion();

		} else if(	estado == "FIN"								){
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "38actcarpre01ext.jsp";
			forma.target	= "_self";
			forma.submit();
 
		}
		
		return;
		
	}

	//---------------------------- 10. PANEL LAYOUT DE CARGA PDF ------------------------------
	
	var hidePanelDescripcionLayoutPdf = function(){
		Ext.getCmp('panelDescripcionLayoutPdf').hide();
		Ext.getCmp('espaciadorPanelDescripcionLayoutPdf').hide();
	}
	
	showPanelDescripcionLayoutPdf = function(){
		Ext.getCmp('espaciadorPanelDescripcionLayoutPdf').show();
		Ext.getCmp('panelDescripcionLayoutPdf').show();
	}
	                             
	var elementosLayoutNotificacionPdf = [
		{
			xtype: 	'label',
			id:		'labelLayoutCargaPdf',
			name:		'labelLayoutCargaPdf',
			html: 	"" ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginBottom: 	'10px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		}
	];
	
	var panelDescripcionLayoutPdf = {
		xtype:			'panel',
		id: 				'panelDescripcionLayoutPdf',
		hidden:			true,
		width: 			700,
		title: 			'Ayuda del Archivo .PDF',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosLayoutNotificacionPdf,
		tools: [
			{
				id:			'close',
				handler: function(event, toolEl, panel){
					hidePanelDescripcionLayoutPdf();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      ),
      listeners: {
      	// Debido a que el panel se muestra en una secci�n que no es visible para el usuario
      	// por eso se solicita que el panel tome el foco.
      	show: function(panel){
      		setTimeout(
      			function(){
      				panel.focus()
      			},
      			200
      		);
      	}
      }
      
	}
	
	var espaciadorPanelDescripcionLayoutPdf = {
		xtype: 	'box',
		id:		'espaciadorPanelDescripcionLayoutPdf',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//---------------------------- 9. PANEL LAYOUT DE CARGA CSV ------------------------------
	
	var hidePanelDescripcionLayoutCsv = function(){
		Ext.getCmp('panelDescripcionLayoutCsv').hide();
		Ext.getCmp('espaciadorPanelDescripcionLayoutCsv').hide();
	}
	
	showPanelDescripcionLayoutCsv = function(){
		Ext.getCmp('espaciadorPanelDescripcionLayoutCsv').show();
		Ext.getCmp('panelDescripcionLayoutCsv').show();
	}
	                             
	var elementosLayoutNotificacionCsv = [
		{
			xtype: 	'label',
			id:		'labelLayoutCargaCsv',
			name:		'labelLayoutCargaCsv',
			html: 	"" ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginBottom: 	'10px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		}
	];
	
	var panelDescripcionLayoutCsv = {
		xtype:			'panel',
		id: 				'panelDescripcionLayoutCsv',
		hidden:			true,
		width: 			700,
		title: 			'Descripci�n de campos del Archivo .CSV',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosLayoutNotificacionCsv,
		tools: [
			{
				id:			'close',
				handler: function(event, toolEl, panel){
					hidePanelDescripcionLayoutCsv();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      ),
      listeners: {
      	// Debido a que el panel se muestra en una secci�n que no es visible para el usuario
      	// por eso se solicita que el panel tome el foco.
      	show: function(panel){
      		setTimeout(
      			function(){
      				panel.focus()
      			},
      			200
      		);
      	}
      }
	}
	
	var espaciadorPanelDescripcionLayoutCsv = {
		xtype: 	'box',
		id:		'espaciadorPanelDescripcionLayoutCsv',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//------------------------- 8. PANEL RESULTADOS VALIDACION ---------------------------

	var elementosResultadosValidacion = [
		{
			xtype: 			'textarea',
			hideLabel: 		true,
			name: 			'detalleRegistrosConErrores',
			id: 				'detalleRegistrosConErrores',
			readOnly:		true,
			anchor:			'100%',
			height:			290
      },
		{ 
			xtype: 			'textfield',
			fieldLabel:		'Total de Registros con Errores',
			name: 			'numeroRegistrosConErrores',
			id: 				'numeroRegistrosConErrores',
			readOnly:		true,
			//maxLength: 		6,	// Maximo 6
			msgTarget: 		'side',
			//anchor:			'-220',
			style: {
				textAlign: 'right'
			}
		}
	];
	
	var panelResultadosValidacion = {
		title:			'Detalle de Errores',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosValidacion',
		width: 			700, //949,
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 10px',
		items: 			elementosResultadosValidacion,
		layout:			'form',
		labelWidth: 	174
	};

	var espaciadorPanelResultadosValidacion = {
		xtype: 	'box',
		id:		'espaciadorPanelResultadosValidacion',
		hidden:	true,
		height: 	7,
		width: 	800
	}
	
	//---------------------------- 7. ALERT AUTORIZACION ENVIO ------------------------------
	
	var showWindowAutorizacionEnvio = function(numeroRegistros){
		
		Ext.Msg.confirm(
			"Confirmaci�n",
			"El total de cr�ditos procesados es de " + numeroRegistros + " �Es correcto?",
      	function(confirmBoton){
      		if( confirmBoton == "yes" ){
					cargaArchivo("TRANSMITIR_REGISTROS",null);
				} else {
					cargaArchivo("FIN",null);
				}
			}
		).getDialog().setZIndex(1000000);
		
	}
			
	// ---------------------------- 6. PANEL AVANCE VALIDACION ------------------------------
	
	var formateaPorcentajeAvance = function(porcentajeAvance){
		var porcentajeFormateado  = Ext.util.Format.number( Number(porcentajeAvance), "0.00" ) + " %";
		return porcentajeFormateado;
	}
	
	var elementosAvanceValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Validando Registros...'
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceValidacion',
			name:	 'barrarAvanceValidacion',
			cls:	 'center-align',
			value: 0.3756,
			text:  '37.56 %'
		},
		{
			xtype: 'panel',
			layout: 'hbox',
			border:			false,
			baseCls:			'x-plain',
			layoutConfig: {
				padding:    '5 0 0 0',
				pack:			'start',
				align:		'middle'
			},
			defaults:{
				margins:'0 5 0 0'
			},
			items: [
				{
					xtype: 	'label',
					width: 	'130',
					cls: 		'x-form-item',
					text: 	'Registros Validados:'
				},
				{
					xtype: 	'label',
					id:		'valorRegistrosProcesados',
					name:		'valorRegistrosProcesados',
					cls: 		'x-form-item',
					style:	{
						textAlign: 'right'
					}
				}
			]
		},
		{
			xtype: 'panel',
			layout: 'hbox',
			border:			false,
			baseCls:			'x-plain',
			layoutConfig: {
				padding:    '0 0 0 0',
				pack:			'start',
				align:		'middle'
			},
			defaults:{
				margins:'0 5 0 0'
			},
			items: [
				{
					xtype: 	'label',
					width: 	'130',
					cls: 		'x-form-item',
					text: 	'Total de Registros:'
				},
				{
					xtype: 	'label',
					id:		'valorTotalRegistros',
					name:		'valorTotalRegistros',
					cls: 		'x-form-item'
				}
			]
		}
	];
	
	var panelAvanceValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceValidacion',
		frame: 			false,
		border:			false,
		collapsible: 	false,
		titleCollapse: false,
		bodyStyle:		'padding: 20px',
		baseCls:			'x-plain',
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		items: 			elementosAvanceValidacion
	});
	
	var hideWindowAvanceValidacion = function(){
		
		var ventanaAvanceValidacion = Ext.getCmp('windowAvanceValidacion');
		
		if(ventanaAvanceValidacion && ventanaAvanceValidacion.isVisible() ){
			var barrarAvanceValidacion	 = Ext.getCmp("barrarAvanceValidacion");
			barrarAvanceValidacion.reset(); 
 
			if (ventanaAvanceValidacion) {
				ventanaAvanceValidacion.hide();
			}
      }
      
	}
	
	var showWindowAvanceValidacion = function(numeroRegistros){
		
		var barrarAvanceValidacion		= Ext.getCmp("barrarAvanceValidacion");
		var valorRegistrosProcesados	= Ext.getCmp("valorRegistrosProcesados");
		var valorTotalRegistros			= Ext.getCmp("valorTotalRegistros");
 
		barrarAvanceValidacion.updateProgress(0.00,"0.00 %",true);
		valorTotalRegistros.setText(numeroRegistros); 
		valorRegistrosProcesados.setText("0");
			
		var ventana = Ext.getCmp('windowAvanceValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				resizable:		false,
				id: 				'windowAvanceValidacion',
				modal:			true,
				closable: 		false,
				width:			Ext.isIE7?500:undefined,
				height: 			Ext.isIE7?144:undefined,
				listeners: 		{
					show: function(component){
						var valorRegistrosProcesados	= Ext.getCmp("valorRegistrosProcesados");
						var valorTotalRegistros			= Ext.getCmp("valorTotalRegistros");
						valorRegistrosProcesados.setWidth(valorTotalRegistros.getWidth());
					}
				},
				items: [
					Ext.getCmp("panelAvanceValidacion")
				]
			}).show();
			
		}
		
	}

	//---------------------------- 5. ALERT AUTORIZACION VALIDACION ------------------------------
	
	var showWindowSolicitarAutorizacionValidacion = function(respuesta){
		
		Ext.Msg.confirm(
			"Confirmaci�n",
			"Los cr�ditos del periodo que previamente se registr� ser�n sustituidos. �Desea continuar?",
      	function(confirmBoton){
      		if( confirmBoton == "yes" ){
					cargaArchivo("MUESTRA_PANTALLA_AVANCE",respuesta);
				} else {
					cargaArchivo("FIN",null);
				}
			}
		).getDialog().setZIndex(1000000);
		
	}
		
	// ----------------------------- 4. PANEL PREPARAR VALIDACION ------------------------------

	var elementosPrepararValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Preparando Validaci�n...'
		},
		{
			xtype: 'progress',
			id:	 'barrarPrepararValidacion',
			name:	 'barrarPrepararValidacion',
			cls:	 'center-align',
			value: 0.00,
			text:  '0.00%'
		}
	];
	
	var panelPrepararValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelPrepararValidacion',
		frame: 			true,
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		items: 			elementosPrepararValidacion
	});
	
	var showWindowPrepararValidacion = function(){
		
		var barrarPrepararValidacion		= Ext.getCmp("barrarPrepararValidacion");
 
		barrarPrepararValidacion.updateProgress(0.00,"0.00 %",true);
		barrarPrepararValidacion.wait({
            interval:	200,
            increment:	15
      });
 
		var ventana = Ext.getCmp('windowPrepararValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				width:			300,
				resizable:		false,
				id: 				'windowPrepararValidacion',
				modal:			true,
				closable: 		false,
				items: [
					Ext.getCmp("panelPrepararValidacion")
				]
			}).show();
			
		}
		
	}	

	var hideWindowPrepararValidacion = function(){
		
		var ventanaPrepararValidacion = Ext.getCmp('windowPrepararValidacion');
		
		if(ventanaPrepararValidacion && ventanaPrepararValidacion.isVisible() ){
			
			var barrarPrepararValidacion	 = Ext.getCmp("barrarPrepararValidacion");
			barrarPrepararValidacion.reset(); 

			if (ventanaPrepararValidacion) {
				ventanaPrepararValidacion.hide();
			}
			
      }
      
	}
 
	//-------------------------- 3. FORMA DE CARGA DE ARCHIVO ---------------------------
 
	var elementosFormaCargaArchivo = [
		{
			xtype: 	'label',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;margin-top:0px;',
			html:  	'Seleccione ambos archivos y presione el bot�n enviar.'
		},
		{
			xtype: 	'label',
			cls:		'x-form-item',
			style: 	'text-align:center;margin:14px;',
			html:  	'Archivo CSV (IFNB_A5_AAAAMM.csv)'
		},
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivoCsv',
		  name: 			'archivoCsv',
		  ftype:			'Csv', // Para facilitar la invocacion del metodo de la ayuda
		  emptyText: 	'Seleccione un archivo',
		  fieldLabel: 	"Carga Archivo de", 
		  buttonText: 	null,
		  allowBlank:	false,
		  validateOnBlur: false,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'-20',
		  getErrors: function(value) {
		  	  var errors = Ext.ux.form.FileUploadField.superclass.getErrors.call(this, value);
		  	  if( !Ext.isEmpty(errors) ){
		  	  	  return errors;
		  	  }
		  	  // Nota: Se agregan las validaciones aqu� debido a que al dar click en el icono de la ayuda
		  	  // se perd�a el mensaje de error
			  if( !Ext.isEmpty( value ) && !/^.+\.([cC][sS][vV])$/.test(value) ){
			  	  errors.push("El formato del archivo de origen no es el correcto. Debe tener extensi�n csv.");
			  }
			  return errors;
		  }
		  //vtype: 		'archivotxt'
		},
		{
			xtype: 	'label',
			cls:		'x-form-item',
			style: 	'text-align:center;margin:14px;',
			html:  	'Archivo PDF (IFNB_A5_AAAAMM.pdf)'
		},
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivoPdf',
		  name: 			'archivoPdf',
		  ftype:			'Pdf', // Para facilitar la invocacion del metodo de la ayuda
		  emptyText: 	'Seleccione un archivo',
		  fieldLabel: 	"Carga Archivo de", 
		  buttonText: 	null,
		  allowBlank:	false,
		  validateOnBlur: false,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'-20',
		  getErrors: function(value) {
		  	  var errors = Ext.ux.form.FileUploadField.superclass.getErrors.call(this, value);
		  	  if( !Ext.isEmpty(errors) ){
		  	  	  return errors;
		  	  }
		  	  // Nota: Se agregan las validaciones aqu� debido a que al dar click en el icono de la ayuda
		  	  // se perd�a el mensaje de error
			  if( !Ext.isEmpty( value ) && !/^.+\.([pP][dD][fF])$/.test(value) ){
			  	  errors.push("El formato del archivo de origen no es el correcto. Debe tener extensi�n pdf.");
			  }
			  return errors;
		  }
		  //vtype: 		'archivotxt'
		},
		{
			xtype: 		'panel',
			anchor: 		'100%',
			style: {
				marginTop: 		'10px'
			},
			layout: {
				type: 'hbox',
				pack: 'end',
				align: 'middle'
			},
			items: [
				{
					xtype: 			'button',
					text: 			'Enviar',
					iconCls: 		'icoContinuar',
					id: 				'botonEnviarArchivo',				
					handler:    function(boton, evento) {
						
						// Revisar si la forma es invalida
						var forma = Ext.getCmp("panelFormaCargaArchivos").getForm();
						if(!forma.isValid()){
							return;
						}
						
						// Cargar archivo
						cargaArchivo("SUBIR_ARCHIVOS",null);

					}
				}
			]
		}
	];
	
	var panelFormaCargaArchivos = new Ext.form.FormPanel({
		id: 				'panelFormaCargaArchivos',
		width: 			700,
		title: 			'Selecci&oacute;n de Archivos',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		fileUpload: 	true,
		hidden:			true,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	120,
		defaultType: 	'textfield',
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'archivoCsv\' || id==\'archivoPdf\'">',
           		 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
						  '<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showPanelDescripcionLayout(\'{id}\');"></a>&nbsp;',
						  		'{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
						  '</div>',
					 '</div>',
			  '</tpl>',
			  '<tpl if="id!=\'archivoCsv\' && id!=\'archivoPdf\'">',
					 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
					 '</div>',
			  '</tpl>'
		   )
		},
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementosFormaCargaArchivo,
		monitorValid: 	false
	});

	//------------------------------------ 2. PANEL AVISOS ---------------------------------------
	
	var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	''
		}
	];
	
	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			700,
		title: 			'Aviso',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}
	
	//--------------------------------- 1. BOX DE INICIALIZACION ---------------------------------------
	
	var boxInicializacion = [
		{
			xtype: 		'box',
			id: 			'contenedorInicializacion',
			cls:			'realizandoActividad',
			style: 		'margin: 0 auto',
			width: 		16,
			height: 		16
		}
	];
		
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			boxInicializacion,
			panelAvisos,
			panelFormaCargaArchivos,
			espaciadorPanelResultadosValidacion,
			panelResultadosValidacion,
			espaciadorPanelDescripcionLayoutCsv,
			panelDescripcionLayoutCsv,
			espaciadorPanelDescripcionLayoutPdf,
			panelDescripcionLayoutPdf
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	cargaArchivo("INICIALIZACION",null);

});;