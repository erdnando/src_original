<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<% if( esEsquemaExtJS ){ %>
	<%@ include file="/01principal/menu.jspf"%>
<% } %>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
<script type="text/javascript" src="38actcarpre01ext.js?<%=session.getId()%>"></script>
<%-- El jsp que contiene el dise�o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<script language="JavaScript1.2" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01if/cabeza.jspf"%>
	<% } %>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
	<% } %>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01if/pie.jspf"%>
	<% } %>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>