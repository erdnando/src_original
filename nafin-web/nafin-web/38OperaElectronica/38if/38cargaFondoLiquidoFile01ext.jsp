<%@ page
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		com.netro.electronica.*,
		com.netro.zip.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="text/html; charset=UTF-8"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/33fondojr/33secsession_extjs.jspf" %>

<%
	JSONObject archivoCSV = new JSONObject();
	JSONObject archivoPDF = new JSONObject();
	JSONObject registroAcuse = new JSONObject();
	JSONArray arrAcuse = new JSONArray();
	boolean flag = true;
	boolean flagExt = true;
	boolean flagReg = true;
	ParametrosRequest req = null;
	String rutaArchivo = "";
	String PATH_FILE	=	strDirectorioTemp;
	String itemArchivo = "";
	String extension = "";
	String folio = "";
	String fechaHora = "";
	String numProceso = "";
	String numTotal = "";
	String totalMonto = "";
	String msgError = "";
	int totalRegistros = 0;
	int tamanio = 0;
	
	BigDecimal montoTotal = new BigDecimal("0.0");
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		extension = (req.getParameter("hidExtension") == null)? "" : req.getParameter("hidExtension");
		numTotal = (req.getParameter("hidNumTotal") == null)? "" : req.getParameter("hidNumTotal");
		totalMonto = (req.getParameter("hidTotalMonto") == null)? "" : req.getParameter("hidTotalMonto");
		
		System.out.println("numTotal=="+numTotal);
		System.out.println("totalMonto=="+totalMonto);
		try{
			int tamaPer = 0;
			if (extension.equals("csv")){
				upload.setSizeMax(200 * 1024);	//700 Kb
				tamaPer = (200 * 1024);
			}

			FileItem fItem = (FileItem)req.getFiles().get(0);
			//itemArchivo		= (String)fItem.getName();
			itemArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			InputStream archivoCsv = fItem.getInputStream();
			tamanio			= (int)fItem.getSize();

			rutaArchivo = PATH_FILE+itemArchivo;
			
			fItem.write(new File(rutaArchivo));		
			System.out.println("tamanio=="+tamanio);
			System.out.println("tamaPer=="+tamaPer);
			System.out.println("itemArchivo=="+itemArchivo);
			
			if (tamanio > tamaPer){
				flag = false;
				msgError = "El tamaño del archivo CSV es mayor al permitido";
			}
			if (flag){
				fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				try{
					
					OperacionElectronica OperElec = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
				
					if(!("CSV".equals(extension.toUpperCase()))) {
						flagExt = false;
						msgError = "El archivo no es CSV";
					}else{
						BufferedReader brt=  null;
						String linea = "";
						
						java.io.File ft = new java.io.File(rutaArchivo);
						brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
						
						String numContrato	= "";
						String nombreIF		= "";
						String cveSirac	= "";
						String saldoHoy	= "";
						String cveMoneda	= "";
						
						VectorTokenizer vtd = null;
						Vector vecdet = null;
		
						while((linea=brt.readLine())!=null){
							System.out.println("LINEA=========="+linea);
							totalRegistros++;
						}
						
						if(totalRegistros>0 && totalRegistros<=30000){
							numProceso = OperElec.cargaDatosTmpFondoLiquido(rutaArchivo);
						}else{
							flagReg = false;
							msgError = "'El archivo CSV no puede contener mas de 30000 registros";
						}
					}
				}catch (Exception e){
						throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			}
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>	
{
	"success": true,
	"flag":	<%=(flag)?"true":"false"%>,
	"flagExt":	<%=(flagExt)?"true":"false"%>,
	"flagReg":	<%=(flagReg)?"true":"false"%>,
	"numProceso":	'<%=numProceso%>',
	"totalRegistros":	'<%=totalRegistros%>',
	"msgError":	'<%=msgError%>'
	
}