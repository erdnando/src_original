<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.descuento.*,
		com.netro.electronica.*,
		com.netro.exception.*,
		com.netro.seguridadbean.*,
		javax.naming.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf" %>

<%
String infoRegresar = "";
String datosIniciales = (request.getParameter("datosIniciales")!=null)?request.getParameter("datosIniciales"):"";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

OperacionElectronica OperElec = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);


if(informacion.equals("ValidaCargaArchivo")){
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	try{
		String numProc = (request.getParameter("numProc")!=null)?request.getParameter("numProc"):"";
		String flagValidaThread = (request.getParameter("flagValidaThread")!=null)?request.getParameter("flagValidaThread"):"";
		
		System.out.println("flagValidaThread===="+flagValidaThread);
		jsonObj.put("numProc", numProc);
		
		if("1".equals(flagValidaThread)){
			session.removeAttribute("validaThread");
			ValidaDatosOperElectThread validaThread = new ValidaDatosOperElectThread();
			validaThread.setCveCargaTmp(numProc);
			//cargaThread.setINoUsuario(iNoUsuario);
			validaThread.setRunning(true);
			new Thread(validaThread).start();
			
			session.setAttribute("validaThread", validaThread);
			
			jsonObj.put("flagValidaThread", "2");
			System.out.println("INICIA EL THREAD");
		
		}else if("2".equals(flagValidaThread)){
			ValidaDatosOperElectThread validaThread = (ValidaDatosOperElectThread)session.getAttribute("validaThread");
			if (validaThread.isRunning()) {
				jsonObj.put("flagValidaThread", "2");
			}else
			if(validaThread.hasError()){
				jsonObj.put("flagValidaThread", "4");
			}else
			if(!validaThread.getTerminoValid() && validaThread.isRunning()){
				jsonObj.put("flagValidaThread", "2");
			}else
			if(!validaThread.isRunning() && validaThread.getTerminoValid()){
				jsonObj.put("flagValidaThread", "3");
				//jsonObj.put("recibo",cargaThread.getTerminoValid());
				HashMap hm = OperElec.obtieneCargaTmpFondoLiquido(numProc);
				List lstCorrectos = (List)hm.get("CORRECTOS");
				List lstErroneos = (List)hm.get("ERRONEOS");
				List  monto = (List)hm.get("MONTO");
				
				jsObjArray = JSONArray.fromObject(lstErroneos);
				jsonObj.put("regErroneos", jsObjArray.toString());
				jsObjArray = JSONArray.fromObject(lstCorrectos);
				jsonObj.put("regCorrectos", jsObjArray.toString());
				jsObjArray = JSONArray.fromObject(monto);    
				jsonObj.put("MONTO", jsObjArray.toString());
				jsonObj.put("flagValidaThread", "3");
				jsonObj.put("nomUser", strNombreUsuario);     
			}	
		}
		jsonObj.put("success", new Boolean(true));
	
	}catch(Throwable t){
		throw new AppException("Error inesperado...", t);
	}
	
	informacion = jsonObj.toString();
	
}else if(informacion.equals("enviaCorreo")){
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	try{
		String numProc = (request.getParameter("numProc")!=null)?request.getParameter("numProc"):"";
		
		OperElec.enviaCargaFondoLiquido(numProc, iNoUsuario, strNombreUsuario);
		jsonObj.put("flagValidaThread", "3");
		jsonObj.put("success", new Boolean(true));
	
	}catch(Throwable t){
		throw new AppException("Error inesperado...", t);
	}
	
	informacion = jsonObj.toString();
}



if(informacion.equals("generarArchivoErrores")){
	JSONObject jsonObj = new JSONObject();
	String numProc = (request.getParameter("numProc")!=null)?request.getParameter("numProc"):"";
	try{
	
		String nombreArchivo = OperElec.obtieneArchivoErroresFL(strDirectorioTemp, numProc);
						
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}catch(Throwable t){
		t.printStackTrace();
		throw new AppException("Error al generar XLS", t);
	}

	informacion = jsonObj.toString();
	System.out.println("informacion==="+informacion);
}


%>

<%=informacion%>