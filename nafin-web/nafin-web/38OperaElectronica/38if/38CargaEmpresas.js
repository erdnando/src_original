Ext.onReady(function() {
	var typeClose = 'N';
	var strPerfil = Ext.getDom('auxStrPerfil').value;
	var mujEmpresaria = Ext.getDom('mujEmpresaria').value;
	function cancelarProc(){
		Ext.getCmp('AgregarArchivo').destroy();	
								
		Ext.Ajax.request({
			url: '38CargaEmpresas.data.jsp',
			params: {
				informacion: "CancelarCarga",										
				no_solicitud2 :Ext.getCmp('no_solicitud2').getValue(),
				claveCarga :Ext.getCmp('claveCarga').getValue(),
				no_Prestamo:Ext.getCmp('no_PrestamoCar').getValue(),
				fechaActMontos:Ext.getCmp('fechaActMontos').getValue()
			},
			callback: procesaCargaArchivo
		});
	}
	
	function procesaCargaArchivo(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
					
					if(jsonData.boton=="Continuar") {	
						Ext.getCmp('btnContinuarP').setIconClass('icoAceptar');	
					}else  {					
						var fpCargaArchivo = Ext.getCmp('fpCargaArchivo');
						fpCargaArchivo.el.unmask();
					}
					
					Ext.getCmp('fechaActMontos').setValue(jsonData.fechaActMontos);					
					Ext.getCmp('claveCarga').setValue(jsonData.claveCarga);	
					if(Ext.getCmp('gridErrores')) Ext.getCmp('gridErrores').hide();			
					if(Ext.getCmp('gridBien')) Ext.getCmp('gridBien').hide();	
					if(Ext.getCmp('lblTitulo1')) Ext.getCmp('lblTitulo1').hide();	
					if(Ext.getCmp('gridResumenDatos'))Ext.getCmp('gridResumenDatos').hide();	
					if(Ext.getCmp('gridResumenRegistros'))Ext.getCmp('gridResumenRegistros').hide();	
					if(Ext.getCmp('gridLayout'))Ext.getCmp('gridLayout').hide();						
					if(Ext.getCmp('btnContinuarP'))Ext.getCmp('btnContinuarP').hide();
					if(Ext.getCmp('btnCancelar'))Ext.getCmp('btnCancelar').hide();
					if(Ext.getCmp('btnCerrar'))Ext.getCmp('btnCerrar').hide();
					typeClose='N';
					
					// cuando hay errores en el monto , venta y la cantidad de campos no es la correcta
				if(jsonData.nuErrores >0 ||  jsonData.nuErroresVm >0) {
					Ext.getCmp('gridErrores').show();			
					Ext.getCmp('gridBien').show();	
					Ext.getCmp('lblTitulo1').show();					
					storeErroresData.loadData(jsonData.todError);
					storeBienData.loadData(jsonData.todOK);
					if(jsonData.nuErroresVm >0 && jsonData.nuErrores==0 )  {  
						Ext.getCmp('fpCargaArchivo').hide();
						Ext.getCmp('btnContinuarP').show();	
						Ext.getCmp('btnCancelar').show();
						typeClose = 'C';
					}					
					if(jsonData.nuErrores >0)  {  Ext.getCmp('fpCargaArchivo').show(); }
				
				//cuando hay erroes en le validaci�n del proceso Linrev_Carga_Lineas_Sp
				}else if(jsonData.nuErrores ==0 &&  jsonData.nuValiDatos>0 ) {
				
					storeResunenDatosData.loadData(jsonData.todResumen);
					Ext.getCmp('gridResumenDatos').show();	
					Ext.getCmp('gridResumenDatos').setTitle('<center>EXISTEN ERRORES EN LA CARGA DE DATOS</center>');					
					storeResumenData.loadData(jsonData.todValidaDatos);
					Ext.getCmp('gridResumenRegistros').show();	
					Ext.getCmp('fpCargaArchivo').show();
					
				//cuado todas las validaciones anteriores estan bien 
				}else if(jsonData.nuErrores ==0 &&  jsonData.nuValiDatos==0 ) {
					
					Ext.getCmp('btnCerrar').show();	
				   Ext.getCmp('AgregarArchivo').setTitle('<div><div style="float:left" align="center">Confirmaci�n Acuse </div>');
					Ext.getCmp('regCorrectos').setValue(jsonData.nuCorrectos);
					Ext.getCmp('fpCargaArchivo').hide();	
					
					var acuseResumen = [
						['<b>Importe ', Ext.util.Format.number(jsonData.importe, '$0,0.00')  ],
						['<b>Moneda ', jsonData.moneda],
						['<b>No. Pr�stamo ', jsonData.no_Prestamo],
						['<b>Fecha de Env�o Informaci�n ', jsonData.fecha],
						['<b>Hora de Env�o ', jsonData.hora],
						['<b>Usuario IF ', jsonData.nombreUsuario]
					];
					storeAcuseData.loadData(acuseResumen);			
					Ext.getCmp('gridAcuse').show();	
					
					storeResunenDatosData.loadData(jsonData.todResumen);
					Ext.getCmp('gridResumenDatos').show();	
					Ext.getCmp('gridResumenDatos').setTitle('<center>CARGA DE DATOS FINALIZADA CORRECTAMENTE</center>');	
					Ext.getCmp('lblTitulo2').update('<b>La Solicitud se envi� con �xito <br> Acuse Solicitud  '+jsonData.acuse);
					Ext.getCmp('lblTitulo2').show();	
					Ext.getCmp('lblTitulo3').show();	
					Ext.getCmp('mensajeModifi').update(jsonData.mensajeModifi);
					Ext.getCmp('mensajeModifi').show();	
					Ext.getCmp('btnDescargaPDF').show();	
				}
								
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var enviarArchivo = function() {
		
		var archivo = Ext.getCmp('archivoCarga');
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}
		
		var fpCargaArchivo = Ext.getCmp('fpCargaArchivo');
				
		fpCargaArchivo.getForm().submit({
			url: '38CargaEmpresas.ma.jsp',									
			waitMsg: 'Enviando datos...',
			waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;				
				var mensaje = resp.mensaje;
				var nombreArchivo = resp.nombreArchivo;
				Ext.getCmp('nombreArchivo').setValue(nombreArchivo);
				
				if(mensaje=='') {			
		
					fpCargaArchivo.el.mask('Procesando...', 'x-mask-loading');			
		
					Ext.Ajax.request({
						url: '38CargaEmpresas.data.jsp',
						params: {
							informacion: "CargaArchivoEmpresa",
							no_Prestamo:Ext.getCmp('no_PrestamoCar').getValue(),
							nombreArchivo:nombreArchivo,
							acuse:Ext.getCmp('acuse').getValue(),
							moneda:Ext.getCmp('moneda').getValue(),
							importe:Ext.getCmp('monto').getValue(),
							nombreContrato:Ext.getCmp('nombreContrato').getValue(),
							fechaContrato:Ext.getCmp('fechaContrato').getValue(),
							fechaContrato_OE:Ext.getCmp('fechaContrato_OE').getValue(),
							no_solicitud2 :Ext.getCmp('no_solicitud2').getValue(),
							claveCarga :Ext.getCmp('claveCarga').getValue(),
							no_Prestamo:Ext.getCmp('no_PrestamoCar').getValue(),
							banderaConfirmar:"N"
						},
						callback: procesaCargaArchivo
					});
				}else {
					Ext.MessageBox.alert("Mensaje",mensaje);
					return;
				}								
			}
			,failure: NE.util.mostrarSubmitError
		});				
	}
	
	
//*****************Grid del Acuse ************************************
	var storeAcuseData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridAcuse = {
		xtype: 'grid',
		id: 'gridAcuse',
		store: storeAcuseData,
		margins: '20 0 0 0',				
		align: 'center',
		hidden: true,
		title   : 'Resumen de Solicitud',		
		columns: [
			{
				header : '',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true,
				align: 'left'	
			},
			{
				header : '',			
				dataIndex : 'informacion',
				width : 348,
				align: 'left',
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 200,
		style: 'margin:0 auto;',		
		frame: true
	};
	
	
	//**************Grid de Resumen con todos los datos cargados en el archivo***************
	var storeResumenData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'ERROR'},
			{name: 'FECHA_OPERACION'},
			{name: 'NOMBRE_IF'},
			{name: 'CREDITO'},
			{name: 'MONEDA'},
			{name: 'FECHA'},
			{name: 'NOMBRE_1'},
			{name: 'NOMBRE_2'},
			{name: 'AP_PATERNO'},
			{name: 'AP_MATERNO'},
			{name: 'RFC'},
			{name: 'FECHA_NACIMIENTO'},
			{name: 'MONTO'},
			{name: 'PRODUCTO'},
			{name: 'ACT_ECONOMICA'},
			{name: 'SECTOR'},
			{name: 'ESTADO'},
			{name: 'MUNICIPIO'},
			{name: 'ESTRATO'},
			{name: 'VENTAS'},
			{name: 'NO_EMPLEADOS'}			
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	
	var gridResumenRegistros =[{
		id: 'gridResumenRegistros',
		hidden: true,
		xtype:'grid',
		title:'',
		store: storeResumenData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'Error',
				tooltip: 'Error',
				dataIndex: 'ERROR',
				sortable: true,
				width: 200,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha Operaci�n',
				tooltip: 'Fecha Operaci�n',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Intermediario',
				tooltip: 'Intermediario',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,
				align: 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'N�mero de Pr�stamo',
				tooltip: 'N�mero de Pr�stamo ',
				dataIndex: 'CREDITO',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Fecha',
				tooltip: 'Fecha',
				dataIndex: 'FECHA',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Nombre 1',
				tooltip: 'Nombre 1',
				dataIndex: 'NOMBRE_1',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Nombre 2',
				tooltip: 'Nombre 2',
				dataIndex: 'NOMBRE_2',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Apellido Paterno',
				tooltip: 'Apellido Paterno',
				dataIndex: 'AP_PATERNO',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Apellido Materno',
				tooltip: 'Apellido Materno',
				dataIndex: 'AP_MATERNO',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Fecha de Nacimiento',
				tooltip: 'Fecha de Nacimiento',
				dataIndex: 'FECHA_NACIMIENTO',
				sortable: true,
				width: 150,
				align: 'center'			
			},			
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Producto',
				tooltip: 'Producto',
				dataIndex: 'PRODUCTO',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Actividad Econ�mica',
				tooltip: 'Actividad Econ�mica',
				dataIndex: 'ACT_ECONOMICA',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Sector',
				tooltip: 'Sector',
				dataIndex: 'SECTOR',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Estado',
				tooltip: 'Estado',
				dataIndex: 'ESTADO',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Municipio',
				tooltip: 'Municipio',
				dataIndex: 'MUNICIPIO',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Estrato',
				tooltip: 'Estrato',
				dataIndex: 'ESTRATO',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'Ventas',
				tooltip: 'Ventas',
				dataIndex: 'VENTAS',
				sortable: true,
				width: 150,
				align: 'center'			
			},
			{
				header: 'No. de Empleados',
				tooltip: 'No. de Empleados',
				dataIndex: 'NO_EMPLEADOS',
				sortable: true,
				width: 150,
				align: 'center'			
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 300,
		width: 680,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	//***********Grid de Resumen de Datos **********************+
	var storeResunenDatosData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'NO_CORRECTOS'},
			{name: 'NO_INCORRECTOS'}				
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	var gridResumenDatos =[{
		id: 'gridResumenDatos',
		hidden: true,
		xtype:'grid',
		title:'<center>CARGA DE DATOS FINALIZADA CORRECTAMENTE</center>',
		store: storeResunenDatosData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'No. de Registros Correctos',
				tooltip: 'No. de Registros Correctos',
				dataIndex: 'NO_CORRECTOS',
				sortable: true,
				width: 200,
				align: 'center'
			},
			{
				header: 'No. de Registros Incorrectos',
				tooltip: 'No. de Registros Incorrectos',
				dataIndex: 'NO_INCORRECTOS',
				sortable: true,
				width: 200,
				align: 'center'
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 100,
		width: 420,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	
	
	//***********Grid Registro sin Errores **********************
	
	var storeBienData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'MONEDA'},
			{name: 'NO_REGISTROS'},	
			{name: 'MONTO'}	
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	var gridBien =[{
		id: 'gridBien',
		hidden: true,
		xtype:'grid',
		title:'Registro sin Errores',
		store: storeBienData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 250,
				align: 'left'
			},
			{
				header: 'No. de Registros',
				tooltip: 'No. de Registrosa',
				dataIndex: 'NO_REGISTROS',
				sortable: true,
				width: 150,
				align: 'center'
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,				
				align: 'right',
				width: 200,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}		
		],
		stripeRows: true,
		loadMask: true,
		height: 120,
		width: 660,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	//***********Grid Registro con Errores **********************
	var storeErroresData = new Ext.data.JsonStore({
		root : 'registros',		
		fields: [			
			{name: 'ARCHIVO'},
			{name: 'NO_LINEA'},	
			{name: 'ERROR'}	
		],
		totalProperty : 'total',
		autoLoad: false		
	});
	
	var gridErrores =[{
		id: 'gridErrores',
		hidden: true,
		xtype:'grid',
		title:'Registro con Errores',
		store: storeErroresData,
		style: 'margin:0 auto;',
		columns:[			
			{
				header: 'Archivo',
				tooltip: 'Archivo',
				dataIndex: 'ARCHIVO',
				sortable: true,
				width: 150,
				align: 'left'
			},
			{
				header: 'No. de L�nea / RFC',
				tooltip: 'No. de L�nea / RFC',
				dataIndex: 'NO_LINEA',
				sortable: true,
				width: 150,
				align: 'center'
			},
			{
				header: 'Error',
				tooltip: 'Error',
				dataIndex: 'ERROR',
				sortable: true,
				width: 330,
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			}		
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 660,
		style: 'margin:0 auto;',
		frame: true
	}];
	
	
	//-------Grid de Ayuda ------------------------------
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{	name: 'NUMERO'},
			{	name: 'NOMBRE'},
			{	name: 'OBLIGATORIO'}			
		]
	});
	
	var infoLayout = [
		['1','Fecha de Operaci�n','S�'],
		['2','Intermediario Financiero','S�'],
		['3','N�mero de Pr�stamo','S�'],	
		['4','Moneda','S�'],		
		['5','Fecha','S�'],		
		['6','Nombre','S�'],	
		['7','Nombre2','S�'],		
		['8','Apelilido Paterno','S�'],		
		['9','Apellido Materno','S�'],
		['10','RFC','S�'],		
		['11','Fecha de Nacimiento','S�'],
		['12','Recursos Solicitados a NAFIN','S�'],	
		['13','Producto','S�'],		
		['14','Actividad Econ�mica','S�'],
		['15','Estado','S�'],		
		['16','Municipio','S�'],
		['17','Estrato','S�'],		
		['18','Ventas','S�'],
		['19','No.de Empleados','S�']	,
		['20','Saldo Capital Vigente','S�'],	
		['21','Monto Otorgado ','S�']
	];
	var infoLayoutAgrega = [
		['1','Fecha de Operaci�n','S�'],
		['2','Intermediario Financiero','S�'],
		['3','N�mero de Pr�stamo','S�'],	
		['4','Moneda','S�'],		
		['5','Fecha','S�'],		
		['6','Nombre','S�'],	
		['7','Nombre2','S�'],		
		['8','Apelilido Paterno','S�'],		
		['9','Apellido Materno','S�'],
		['10','RFC','S�'],		
		['11','Fecha de Nacimiento','S�'],
		['12','Recursos Solicitados a NAFIN','S�'],	
		['13','Producto','S�'],		
		['14','Actividad Econ�mica','S�'],
		['15','Estado','S�'],		
		['16','Municipio','S�'],
		['17','Estrato','S�'],		
		['18','Ventas','S�'],
		['19','No.de Empleados','S�']	,
		['20','Saldo Capital Vigente','S�'],	
		['21','Monto Otorgado ','S�'],
		['22','Domicilio','S�']	,
		['23','Tel�fono','S�'],	
		['24','Correo Electr�nico ','S�']
	];
	if(strPerfil=="IF 5CP" || strPerfil=="IF 4CP"){
		storeLayoutData.loadData(infoLayoutAgrega);
	}else if((strPerfil=="IF 4MIC" || strPerfil=="IF 5MIC")&&mujEmpresaria=="S"){
		infoLayout.push(['22','Calle ','S�']);
		infoLayout.push(['23','No. Interior ','S�']);
		infoLayout.push(['24','No. Externo ','S�']);
		infoLayout.push(['25','Colonia ','S�']);
		infoLayout.push(['26','C.P ','S�']);
		infoLayout.push(['27','CURP ','S�']);
		infoLayout.push(['28','Tasa de Int. (% mensual) ','S�']);
		infoLayout.push(['29','Comisi�n (% monto dispuesto) ','S�']);
		infoLayout.push(['30','Tasa de Int. (% anual) ','S�']);
		
		storeLayoutData.loadData(infoLayout);
	}else{
		storeLayoutData.loadData(infoLayout);
	}
 
	var gruposLayout = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '<center>Layout de Carga de Empresas Apoyadas <br> Los campos debe de estar separados por ("|")</center>', colspan: 3, align: 'center'}
			]
		]
	});
	
	var gridLayout = {
		xtype: 'grid',	
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: '<center>Layout Carga de Empresas Apoyadas <br>  Estructura de Archivo  <br> Carga de Empresas Apoyadas </center>',
		plugins: gruposLayout,
		columns: [
			{
				header : 'No.',
				dataIndex : 'NUMERO',
				width : 50,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Nombre del Campo',
				tooltip: 'Nombre del Campo',
				dataIndex : 'NOMBRE',
				width : 200,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 100,
				sortable : true,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 370,
		height: 540,
		frame: true
	};




	var myValidFnTXT = function(v) {
		var myRegex = /^.+\.([tT][xX][tT])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivoTxt		: myValidFnTXT,
		archivoTxtText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): TXT.'
	});
	
	
	var fpCargaArchivo = {
		xtype: 'form',
		id:'fpCargaArchivo',	
		width: 700,
		title: '',
		frame: true,	
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},	
		monitorValid: true,
		items: [	
			{
				xtype: 'button',
				id: 'btnAyuda',
				columnWidth: .05,
				autoWidth: true,
				autoHeight: true,
				iconCls: 'icoAyuda',
				handler: function(){	
					var gridLayout = Ext.getCmp('gridLayout');
					if (!gridLayout.isVisible()) {
						gridLayout.show();
					}else{
						gridLayout.hide();
					}				
				}				
			},	
			{
				xtype: 'panel',
				id:		'pnlArchivo',				
				layout: 'form',
				fileUpload: true,
				labelWidth: 150,					
				items: [
					{
						xtype: 'fileuploadfield',
						id: 'archivoCarga',
						width: 150,	  
						emptyText: 'Carga Archivo de',
						fieldLabel: 'Carga Archivo de',
						name: 'archivoCarga',
						buttonText: 'Examinar...',						
						anchor: '90%',
						vtype: 'archivoTxt'
						
					 }					
				]
			},
			{
				xtype: 'displayfield',
				value: '- El registro con cantidad se campos sea el correcto <br> - Se requiere el valor del Monto <br> - Se requiere el valor de la Venta'				
			}			
		],
		buttons: [
			{
				text: 'Continuar',
				id: 'btnContinuar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivo
			}
		]
	};
	
			//Carga del Archivo de la Columna Carga de Empresas Apoyadas
	var CargaArchivoEmpresas= function (grid,rowIndex,colIndex,item,event){
		
		var registro = grid.getStore().getAt(rowIndex);
		var ventana = Ext.getCmp('AgregarArchivo');	
		
		Ext.getCmp('no_PrestamoCar').setValue(registro.get('NO_PRESTAMO'));
		Ext.getCmp('acuse').setValue(registro.get('ACUSE'));
		Ext.getCmp('moneda').setValue(registro.get('MONEDA'));
		Ext.getCmp('monto').setValue(registro.get('IMPORTE'));		
		Ext.getCmp('nombreContrato').setValue(registro.get('NOMBRE_CONTRATO'));
		Ext.getCmp('fechaContrato').setValue(registro.get('FECHA_FIRMA_CONTRATO'));
		Ext.getCmp('fechaContrato_OE').setValue(registro.get('FECHA_CONTRATOS_OE'));
		Ext.getCmp('no_solicitud2').setValue(registro.get('NO_SOLICITUD'));
	
		
		if (ventana) {
			ventana.show();
		} else {
			new Ext.Window({	
				width: 710,	
				id:'AgregarArchivo',				
				modal: true,
				autoHeight: true,
				resizable:false,
				closeAction: 'destroy',
				closable:true,
				autoDestroy:true,	
				items: [					  
					fpCargaArchivo,
					gridLayout,
					{
						xtype: 'label',
						id: 'lblTitulo1',
						style: 'font-weight:bold;color:#006699;text-align:center;display:block;',						
						text: 'Resumen de Tranferencia de Archivo(s)',
						hidden	: true
					}	,	
					gridBien,
					gridErrores,
					{
						xtype: 'label',
						id: 'lblTitulo2',
						style: 'font-weight:bold;color:#006699;text-align:center;display:block;',						
						text: '',
						hidden	: true
					},						
					gridAcuse,					
					{
						xtype: 'label',
						id: 'mensajeModifi',				
						fieldLabel: '',
						hidden	: true,
						text: ''
					},			
					{
						xtype: 'label',
						id: 'lblTitulo3',
						style: 'font-weight:bold;color:#006699;text-align:center;display:block;',						
						text: 'RESUMEN DE CARGA DE DATOS',
						hidden	: true
					},						
					gridResumenDatos,
					gridResumenRegistros					
				],
				listeners:{
					beforedestroy: function(p){
						//alert(typeClose);
						if (typeClose!='N'){
							//Ext.getCmp('AgregarArchivo').destroy();	
								
							Ext.Ajax.request({
								url: '38CargaEmpresas.data.jsp',
								params: {
									informacion: "CancelarCarga",										
									no_solicitud2 :Ext.getCmp('no_solicitud2').getValue(),
									claveCarga :Ext.getCmp('claveCarga').getValue(),
									no_Prestamo:Ext.getCmp('no_PrestamoCar').getValue(),
									fechaActMontos:Ext.getCmp('fechaActMontos').getValue()
								},
								callback: procesaCargaArchivo
							});
						}

					}
				},
				title: 'Agregar Archivo',
						bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: ['->',
						{
							xtype: 'button',
							style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
							text: 'Imprimir PDF',			
							iconCls: 'icoPdf',
							hidden	: true,
							align: 'right',
							id: 'btnDescargaPDF',				
							handler: function(boton, evento) {
								Ext.Ajax.request({
									url: '38CargaEmpresas.data.jsp',
									params: {
										informacion: 'ArchivoAcuse',
										no_Prestamo:registro.get('NO_PRESTAMO'),
										acuse:registro.get('ACUSE'),
										moneda:registro.get('MONEDA'),
										importe:registro.get('IMPORTE'),
										nombreContrato: registro.get('NOMBRE_CONTRATO'),
										fechaContrato:registro.get('FECHA_FIRMA_CONTRATO'),
										fechaContrato_OE:registro.get('FECHA_CONTRATOS_OE'),
										regCorrectos:Ext.getCmp('regCorrectos').getValue(), 
									   no_solicitud2 :Ext.getCmp('no_solicitud2').getValue()
									}
									,callback: procesarDescargaArchivos
								});
							}											
						},						
						{	
							xtype: 'button',	
							text: 'Continuar ',	
							iconCls: 'icoAceptar', 	
							id: 'btnContinuarP', 
							hidden:true,
							handler: function(boton, evento) {
							
								boton.setIconClass('loading-indicator');
								Ext.getCmp("btnContinuarP").disable();										
								Ext.Ajax.request({
									url: '38CargaEmpresas.data.jsp',
									params: {
										
										informacion: "CargaArchivoEmpresa",
										no_Prestamo:Ext.getCmp('no_PrestamoCar').getValue(),
										nombreArchivo:Ext.getCmp('nombreArchivo').getValue(),
										acuse:Ext.getCmp('acuse').getValue(),
										moneda:Ext.getCmp('moneda').getValue(),
										importe:Ext.getCmp('monto').getValue(),
										nombreContrato:Ext.getCmp('nombreContrato').getValue(),
										fechaContrato:Ext.getCmp('fechaContrato').getValue(),
										fechaContrato_OE:Ext.getCmp('fechaContrato_OE').getValue(),
										no_solicitud2 :Ext.getCmp('no_solicitud2').getValue(),
										claveCarga :Ext.getCmp('claveCarga').getValue(),
										banderaConfirmar:"S",
										boton:"Continuar"
									},
									callback: procesaCargaArchivo
								});
							}
						},
						{
							xtype: 'button',
							text: 'Cancelar ',
							hidden:true,
							iconCls: 'icoLimpiar', 	
							id: 'btnCancelar', 
							handler: function(){
								
								typeClose = 'C';
								Ext.getCmp('AgregarArchivo').destroy();	
								
								/*
								Ext.Ajax.request({
									url: '38CargaEmpresas.data.jsp',
									params: {
										informacion: "CancelarCarga",										
										no_solicitud2 :Ext.getCmp('no_solicitud2').getValue(),
										claveCarga :Ext.getCmp('claveCarga').getValue(),
										no_Prestamo:Ext.getCmp('no_PrestamoCar').getValue(),
										fechaActMontos:Ext.getCmp('fechaActMontos').getValue()
									},
									callback: procesaCargaArchivo
								});
								*/
					
							} 
						},
						{	
							xtype: 'button',	
							text: 'Salir ',	
							iconCls: 'icoLimpiar', 	
							id: 'btnCerrar', 
							handler: function(){
								typeClose = 'N';
								Ext.getCmp('AgregarArchivo').destroy();	
								fp.el.mask('Enviando...', 'x-mask-loading');			
								consultaData.load({
									params: Ext.apply(fp.getForm().getValues(),{  
										informacion: 'Consultar',	
										inicializa:'N'
									})
								});
					
							} 
						},
						'-'							
					]
				}
				
			}).show();
		}	
	}
	
	
	//Descarga del Archivo de la Columna PDF Comprobante Boleta RUG
	var desArchivoBoleta = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '38CargaEmpresas.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoCotiza',
				tipoArchivo:'Boleta',
				extension:'pdf',				
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	//Descarga del Archivo de la Columna Carga Cartera en Prenda
	var desArchivoCartera = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '38CargaEmpresas.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoCotiza',
				tipoArchivo:'Cartera',
				extension:'csv',				
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
		//Descarga del Archivo de la Columna de Tabla Amortizaci�n
	var desArchivoCotiza = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '38CargaEmpresas.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'desArchivoCotiza',
				tipoArchivo:'Cotiza',
				extension:'pdf',				
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	
	//Descarga del Archivo de la Columna de Tabla Amortizaci�n
	var desArchivoTabla = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var no_solicitud = registro.get('NO_SOLICITUD');
		
		Ext.Ajax.request({
			url: '38CargaEmpresas.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'ArchivoTablaAmortizacion',
				no_solicitud: no_solicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	
	//------Consulta ------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta.getColumnModel();
			
			if(jsonData.strPerfil =='IF LI'  ||   jsonData.strPerfil =='IF 4MIC'   ||  jsonData.strPerfil =='IF 5MIC' )  {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DOC_CARTERA'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DOC_BOLETA'), true);				
			}
			
			if(store.getTotalCount() > 0) {			
				el.unmask();	
				Ext.getCmp('btnArchivoCons').enable();
			} else {		
				Ext.getCmp('btnArchivoCons').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '38CargaEmpresas.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{	name: 'IC_IF'},			
			{	name: 'FECHA_SOLICITUD'},
			{	name: 'NO_SOLICITUD'},
			{	name: 'NOMBRE_CONTRATO'},
			{	name: 'FECHA_FIRMA_CONTRATO'},			
			{	name: 'NO_MODIFICATORIOS'},
			{	name: 'FECHA_FIRMA_MODIFICATORIOS'},
			{	name: 'NOMBRE_CONTRATO_M'},
			{	name: 'FECHA_CONTRATOS_OE'},
			{	name: 'IMPORTE'},
			{	name: 'MONEDA'},
			{	name: 'IC_MONEDA'},
			{	name: 'DESTINO_RECURSOS'},
			{	name: 'FECHA_PAGO_CAPITAL'},
			{	name: 'FECHA_PAGO_INTERES'},
			{	name: 'FECHA_VENCIMIENTO'},
			{	name: 'TIPO_TASA'},	
			{	name: 'TASA'},				
			{	name: 'VALOR'},				
			{	name: 'ESTATUS'},
			{	name: 'FECHA_OPERACION_EJECUTIVO'},
			{	name: 'FECHA_OP_CONCENTRADOR'},			
			{	name: 'OBSER_INTERMEDIARIO'},
			{	name: 'OBSER_EJECUTIVO'},
			{	name: 'DOC_COTIZACION'},
			{	name: 'DOC_CARTERA'},
			{	name: 'DOC_BOLETA'},
			{	name: 'DOC_PRENDA'},
			{	name: 'DOC_EMPRESAS'},
			{	name: 'IC_ESTATUS'},
			{	name: 'PERFIL'},
			{	name: 'NO_PRESTAMO'},
			{	name: 'ACUSE'},
			{  name: 'CARGA_APOYADAS'},
			{	name: 'CG_NOMBRE_USUARIO'},	
			{	name: 'FECHA_OPERACION_EJECUTIVO'},				
			{	name: 'USUARIO_EJE_OP'},
			{	name: 'FECHA_OP_CONCENTRADOR'},
			{	name: 'USUARIO_EJE_CON2'},
			{  name: 'CS_ACUSE'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});	
			
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [				
			{
				header: 'Fecha de Solicitud',
				tooltip: 'Fecha de Solicitud',
				dataIndex: 'FECHA_SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'No. Pr�stamo',
				tooltip: 'No. Pr�stamo',
				dataIndex: 'NO_PRESTAMO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. de Solicitud',
				tooltip: 'No. de Solicitud',
				dataIndex: 'CS_ACUSE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Nombre Contrato',
				tooltip: 'Nombre Contrato',
				dataIndex: 'NOMBRE_CONTRATO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha Firma Contrato',
				tooltip: 'Fecha Firma Contrato',
				dataIndex: 'FECHA_FIRMA_CONTRATO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Contratos Modificatorios',
				tooltip: 'Contratos Modificatorios',
				dataIndex: 'NO_MODIFICATORIOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Firma Modificatorios',
				tooltip: 'Fecha Firma Modificatorios',
				dataIndex: 'FECHA_FIRMA_MODIFICATORIOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Nombre Contrato',
				tooltip: 'Nombre Contrato',
				dataIndex: 'NOMBRE_CONTRATO_M',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha Firma Contrato Operaci�n Electr�nica',
				tooltip: 'Fecha Firma Contrato Operaci�n Electr�nica',
				dataIndex: 'FECHA_CONTRATOS_OE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'IMPORTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Destino de los Recursos',
				tooltip: 'Destino de los Recursos',
				dataIndex: 'DESTINO_RECURSOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: 'Fecha de Primer Pago Capital',
				tooltip: 'Fecha de Primer Pago Capital',
				dataIndex: 'FECHA_PAGO_CAPITAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Primer Pago Interes',
				tooltip: 'Fecha de Primer Pago Interes',
				dataIndex: 'FECHA_PAGO_INTERES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{				
				header: 'Tipo Tasa',
				tooltip: 'Tipo Tasa',
				dataIndex: 'TIPO_TASA',
				width: 130,
				align: 'center'				
			},	
			{
				header: 'Tasa ',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('TASA') !='' ) {
						return registro.get('TASA');
					}else  {
						return 'N/A';
					}				
				}
			},		
			{
				header: 'Valor ',
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('TIPO_TASA') == 'Variable'){
						if(registro.get('VALOR') !='' ) {
							return  Ext.util.Format.number(value, '+0.00%');
						}else  {
							return 'N/A';
						}				
					}else{
						if(registro.get('VALOR') !='' ) {
							return  Ext.util.Format.number(value, '0.00%');
						}else  {
							return 'N/A';
						}				
					}			
				}
			},	
			{
				xtype: 'actioncolumn',
				header: 'Carga Cotizaci�n',
				tooltip: 'Carga Cotizaci�n',
				dataIndex: 'DOC_COTIZACION',
				width: 130,
				align: 'center',				
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('DOC_COTIZACION') !=0 ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';	
							}
						}
						,handler: desArchivoCotiza
					}
				]				
			},			
			{
				xtype: 'actioncolumn',
				header: 'Tabla Amortizaci�n',
				tooltip: 'Tabla Amortizaci�n',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'icoXls';										
						}
						,handler: desArchivoTabla
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Carga Cartera en Prenda',
				tooltip: 'Carga Cartera en Prenda',
				dataIndex: 'DOC_CARTERA',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('DOC_CARTERA') ==0 ) {
						return 'N/A';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('DOC_CARTERA') !=0 ) {
								this.items[0].tooltip = 'Ver';
								return 'icoXls';		
							}
						}
						,handler: desArchivoCartera
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'PDF Comprobante Boleta RUG',
				tooltip: 'PDF Comprobante Boleta RUG',
				dataIndex: 'DOC_BOLETA',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('DOC_BOLETA') ==0 ) {
						return 'N/A';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('DOC_BOLETA') !=0 ) {
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';		
							}								
						}
						,handler: desArchivoBoleta
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Carga de Empresas Apoyadas',
				tooltip: 'Carga de Empresas Apoyadas',
				width: 130,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
						if(registro.get('IC_ESTATUS') ==5  && registro.get('CARGA_APOYADAS') =='S' && registro.get('IC_MONEDA') =='1' ) { //estatus Confirmada
							return '<span style="color:Green;"><b>Carga Realizada</span>';																	
						}else  if(registro.get('IC_ESTATUS') ==5  && registro.get('IC_MONEDA') =='54' ) { //estatus Confirmada
							return 'No Aplica';										
						}
				},				
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {						
							if(registro.get('IC_ESTATUS') ==5  && registro.get('CARGA_APOYADAS') =='N'   && registro.get('IC_MONEDA') =='1'  ) { //estatus Confirmada
								this.items[0].tooltip = 'Descargar';
								return 'icoActualizar';										
							}
						}
						,handler: CargaArchivoEmpresas
					}
				]				
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
				{
				header: 'Usuario IF',
				tooltip: 'Usuario IF',
				dataIndex: 'CG_NOMBRE_USUARIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha Operaci�n Ejecutivo OP',
				tooltip: 'Fecha Operaci�n Ejecutivo OP',
				dataIndex: 'FECHA_OPERACION_EJECUTIVO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Usuario Ejecutivo OP',
				tooltip: 'Usuario Ejecutivo OP',
				dataIndex: 'USUARIO_EJE_OP',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Operaci�n Concentrador',
				tooltip: 'Fecha Operaci�n Concentrador',
				dataIndex: 'FECHA_OP_CONCENTRADOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Usuario Concentrador',
				tooltip: 'Usuario Concentrador',
				dataIndex: 'USUARIO_EJE_CON2',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Observaciones Intermediario Financiero',
				tooltip: 'Observaciones Intermediario Financiero',
				dataIndex: 'OBSER_INTERMEDIARIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header : 'Observaciones Ejecutivo de Operaci�n', 
				tooltip: 'Observaciones Ejecutivo de Operaci�n',
				dataIndex : 'OBSER_EJECUTIVO', 
				fixed:true,
				sortable : false,	
				width : 300,
				hiddeable: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}			
			}				
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,		
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCons',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '38CargaEmpresas.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSVCons'
							}),
							callback: procesarDescargaArchivos
						});
						
					}
				}
			]
		}
	});
		
			
//*************** componentes de la Forma********************

	var  elementosForma  = [
	
		{ 
         xtype:         'bigdecimal',
         name:          'no_Prestamo',
         id:            'no_Prestamo1',
         allowDecimals: false,
         allowNegative: false,
         fieldLabel:    'No. Pr�stamo',
         blankText:     'Favor de capturar el n�mero de Pr�stamo',       
         hidden:        false,
         maxLength:     22, 
         msgTarget:     'side',
         anchor:        '-20',
         maxValue:      '9999999999999999999999',
         format:        '0000000000000000000000',
			margins: '0 20 0 0'	  //necesario para mostrar el icono de error
      }	,		
		{ 
         xtype:         'bigdecimal',
         name:          'no_solicitud',
         id:            'no_solicitud1',
         allowDecimals: false,
         allowNegative: false,
         fieldLabel:    'No. Solicitud',
         blankText:     'Favor de capturar el n�mero de Solicitud',       
         hidden:        false,
         maxLength:     22, 
         msgTarget:     'side',
         anchor:        '-20',
         maxValue:      '9999999999999999999999',
         format:        '0000000000000000000000',
			margins: '0 20 0 0'	  //necesario para mostrar el icono de error
      },	
		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaRecepcionIni',
					id: 'fechaRecepcionIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'fechaRecepcionFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaRecepcionFin',
					id: 'fechaRecepcionFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fechaRecepcionIni',
					margins: '0 20 0 0'  
				}
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'no_PrestamoCar', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'acuse', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'moneda', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'monto', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'nombreContrato', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'fechaContrato', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'fechaContrato_OE', 	value: '' },			
		{ 	xtype: 'textfield',  hidden: true, id: 'regCorrectos', 	value: '' },	
		{ 	xtype: 'textfield',  hidden: true, id: 'no_solicitud2', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'claveCarga', 	value: '' }	,
		{ 	xtype: 'textfield',  hidden: true, id: 'nombreArchivo', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'fechaActMontos', 	value: '' }
	];	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		monitorValid: true,
		title: 'Carga de Empresas Apoyadas',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {

					var fechaRecepcionIni = Ext.getCmp('fechaRecepcionIni');
					var fechaRecepcionFin = Ext.getCmp('fechaRecepcionFin');
					
					if(!Ext.isEmpty(fechaRecepcionIni.getValue()) || !Ext.isEmpty(fechaRecepcionFin.getValue())){
						if(Ext.isEmpty(fechaRecepcionIni.getValue())){
							fechaRecepcionIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaRecepcionIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaRecepcionFin.getValue())){
							fechaRecepcionFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaRecepcionFin.focus();
							return;
						}
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{  
							informacion: 'Consultar',	
							inicializa:'N'
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '38CargaEmpresas.jsp';					
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});

	consultaData.load({
		params: Ext.apply(fp.getForm().getValues(),{  
		informacion: 'Consultar',
		inicializa:'S'
		})
	});
	
					
});