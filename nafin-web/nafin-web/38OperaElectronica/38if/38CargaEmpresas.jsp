<!DOCTYPE html>
<%@ page import="java.util.*, com.netro.electronica.*," 
	contentType="text/html;charset=windows-1252"
	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession.jspf" %>
<%
String auxStrPerfil = strPerfil;
OperacionElectronica  elet = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
String mujEmpresaria = elet.getCampoRegDatosIf("CS_MUJERES_EMPRESARIAS",iNoCliente);

%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
<script type="text/javascript" src="38CargaEmpresas.js?<%=session.getId()%>"></script>

</head>

<%@ include file="/01principal/menu.jspf"%>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01if/cabeza.jspf"%>

<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
      <div id="areaContenido"><div style="height:230px"></div></div>
   </div>
</div>

<%@ include file="/01principal/01if/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
<form id='formParametros' name="formParametros">
	<input type="hidden" id="auxStrPerfil" name="auxStrPerfil" value="<%=auxStrPerfil%>"/>
	<input type="hidden" id="mujEmpresaria" name="mujEmpresaria" value="<%=mujEmpresaria%>"/>
	
</form>

</body>
</html>