<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.math.*, 
	java.text.*, 
	java.sql.*,
	netropology.utilerias.*,		
	java.text.SimpleDateFormat,
	java.util.Date,		
	com.netro.exception.*,
	com.netro.model.catalogos.*,	
	com.netro.electronica.*,
	org.apache.commons.logging.Log,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf"%>
	<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String no_solicitud = (request.getParameter("no_solicitud")!=null)?request.getParameter("no_solicitud"):"";
String no_solicitud2 = (request.getParameter("no_solicitud2")!=null)?request.getParameter("no_solicitud2"):"";
String fechaRecepcionIni = (request.getParameter("fechaRecepcionIni")!=null)?request.getParameter("fechaRecepcionIni"):"";
String fechaRecepcionFin = (request.getParameter("fechaRecepcionFin")!=null)?request.getParameter("fechaRecepcionFin"):"";
String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String extension = (request.getParameter("extension")!=null)?request.getParameter("extension"):"";
String no_Prestamo = (request.getParameter("no_Prestamo")!=null)?request.getParameter("no_Prestamo"):"";
String  importe = (request.getParameter("importe")!=null)?request.getParameter("importe"):"";
String  moneda = (request.getParameter("moneda")!=null)?request.getParameter("moneda"):"";
String  acuse = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
String fecha	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String hora = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());	
String usuario = iNoUsuario+" - "+strNombreUsuario;
String nombreContrato = (request.getParameter("nombreContrato")!=null)?request.getParameter("nombreContrato"):"";
String fechaContrato = (request.getParameter("fechaContrato")!=null)?request.getParameter("fechaContrato"):"";
String fechaContrato_OE = (request.getParameter("fechaContrato_OE")!=null)?request.getParameter("fechaContrato_OE"):"";
String regCorrectos = (request.getParameter("regCorrectos")!=null)?request.getParameter("regCorrectos"):"";
String  nombreArchivo = (request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";
String rutaNombreArchivo = strDirectorioTemp +nombreArchivo;
String  claveCarga = (request.getParameter("claveCarga")!=null)?request.getParameter("claveCarga"):"";
String  fechaActMontos = (request.getParameter("fechaActMontos")!=null)?request.getParameter("fechaActMontos"):"";
String  inicializa = (request.getParameter("inicializa")!=null)?request.getParameter("inicializa"):"";
String  banderaConfirmar = (request.getParameter("banderaConfirmar")!=null)?request.getParameter("banderaConfirmar"):"N";
String  boton = (request.getParameter("boton")!=null)?request.getParameter("boton"):"";


String rutaArchivo = strDirectorioTemp+nombreArchivo;
	
String infoRegresar ="", consulta = "", mensajeMontoMaximo = "", validaMontoMax ="N";
JSONObject jsonObj = new JSONObject();
JSONArray jsonArrErr = new JSONArray();
JSONArray jsonArrOK = new JSONArray();
JSONArray jsonArrResumen = new JSONArray();
JSONArray jsonValidaDatos = new JSONArray();
StringBuffer mensajeModifi = new StringBuffer();
int nuErrores =0,  nuCorrectos =0, nuValiDatos = 0, nuErroresVm =0;
List parametros  =  new ArrayList();

OperacionElectronica eletronicaBean = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);

//Declaración de la clase para la consulta  
Cons_CargaEmpresas paginador = new Cons_CargaEmpresas ();
paginador.setIc_if(iNoCliente);
paginador.setIc_solicitud(no_solicitud);
paginador.setFechaRecepcionIni(fechaRecepcionIni);
paginador.setFechaRecepcionFin(fechaRecepcionFin);
paginador.setStrPerfil(strPerfil);
paginador.setNo_Prestamo(no_Prestamo);
paginador.setInicializa(inicializa);


CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
 
if (informacion.equals("Consultar")){
	
	Registros reg	=	queryHelper.doSearch();
	while(reg.next()){
		String FechasModificatorias = paginador.getFirmasModificatorias (reg.getString("IC_IF")) ;
		String nomUsuario = (reg.getString("CG_NOMBRE_USUARIO") == null) ? "" : reg.getString("CG_NOMBRE_USUARIO");
		String nomUsuario1 = nomUsuario.substring(nomUsuario.lastIndexOf("-")+1, nomUsuario.length());		
		reg.setObject("FECHA_FIRMA_MODIFICATORIOS",FechasModificatorias);		
		reg.setObject("PERFIL",strPerfil);	
		reg.setObject("CG_NOMBRE_USUARIO",nomUsuario1);	
	}	
	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("iTipoAfiliado", iTipoAfiliado); 
	jsonObj.put("strPerfil", strPerfil); 
	infoRegresar = jsonObj.toString();  		


}else  if ( informacion.equals("ArchivoCSVCons") ) {
	try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	infoRegresar = jsonObj.toString(); 

}else  if ( informacion.equals("ArchivoTablaAmortizacion") ) {

	try {
		nombreArchivo =	eletronicaBean.getArchivoTablaAmortizacion (strDirectorioTemp,  no_solicitud  );				
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}	
	infoRegresar = jsonObj.toString(); 

}else  if ( informacion.equals("desArchivoCotiza") ) {
	
	try {
		nombreArchivo =	eletronicaBean.consDescargaArchivos(strDirectorioTemp,  no_solicitud, tipoArchivo,  extension );
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}	
	infoRegresar = jsonObj.toString(); 

}else  if ( informacion.equals("CargaArchivoEmpresa") ) {

	String  ruta= strDirectorioTemp+ nombreArchivo;
	claveCarga  = eletronicaBean.claveCarga(); // Clave de la Carga del Archivo
	
	List resultados = eletronicaBean.CargaListadeArchivosTMP(no_Prestamo, strDirectorioTemp, nombreArchivo, claveCarga , strPerfil, importe,iNoCliente );
	
	List datosError =  new ArrayList();
	List datosOK =  new ArrayList();
	List datos =  new ArrayList();
		
	if(resultados.size()>0){  
		datosError = (ArrayList)resultados.get(0);	 
		datosOK = (ArrayList)resultados.get(1);			
	}	
	nuCorrectos = datosOK.size();
	nuErrores = datosError.size();	
	
	if(nuErrores>0) {
		jsonArrErr = JSONArray.fromObject(datosError);		
	}	
	jsonArrOK = JSONArray.fromObject(datosOK);	
				
				
	// si no hubo errores se ejecuta el proceso 		  
	if(nuErrores== 0 )  {
		
		List datosValidos =  eletronicaBean.validaCargaDatos( claveCarga);  //se ejecuta el Proceso 
		
		List datosTotales =  eletronicaBean.totalCargaLinea (claveCarga); //Obtengo los  totales del resumen 		
		jsonArrResumen = JSONArray.fromObject(datosTotales);
				
		if(datosValidos !=null) {
			nuValiDatos = datosValidos.size();
			jsonValidaDatos = JSONArray.fromObject(datosValidos);
			
			if(datosValidos.size()==0) {
				
				if(nuErroresVm==0 &&  banderaConfirmar.equals("N")  ) {
				
					parametros  =  new ArrayList();
					parametros.add(no_solicitud2);
					parametros.add(claveCarga);
					parametros.add(no_Prestamo);
					parametros.add(strPerfil);
					parametros.add(nombreArchivo);	
									
					List resultadosMonto =   eletronicaBean.validaRebasaLimite(parametros ); //Valida el Monto Limite 
					List validaMontoLimite = null;
					List fechasMonto = null;				
					
					if(resultadosMonto.size()>0){  
						validaMontoLimite = (ArrayList)resultadosMonto.get(0);	 
						fechasMonto = (ArrayList)resultadosMonto.get(1);	
						
						for (int i = 0; i <fechasMonto.size(); i++) {
							fechaActMontos +=  fechasMonto.get(i).toString()+",";
						}
					}	
			
					nuErroresVm = validaMontoLimite.size();
					
					if(nuErroresVm>0){
						jsonArrErr = JSONArray.fromObject(validaMontoLimite);	
					}else {
						banderaConfirmar ="S"; //para continuar la carga 
					}
				}
				
				if(nuErroresVm==0 &&  banderaConfirmar.equals("S")  ) {
				 
				 parametros  =  new ArrayList();
				 parametros.add(no_solicitud2);
				 parametros.add("N");
				 parametros.add(ruta);					 
				 
				 eletronicaBean.confirmaCargaCargaEmpresas(parametros ); //confirma la Carga de Empresas Apoyadas
				 				 
					//parametros para el envio de correo 
					parametros  =  new ArrayList();
					parametros.add(importe);
					parametros.add(moneda);
					parametros.add(no_Prestamo);
					parametros.add(fecha);
					parametros.add(hora);
					parametros.add(usuario);
					parametros.add(strNombre);		
					parametros.add(iNoCliente);					
					eletronicaBean.envioCorreoCargaEmpresas( parametros ) ;//Se envia correo
			
					HashMap totalesD = (HashMap)datosTotales.get(0);
					nuCorrectos  = Integer.parseInt(totalesD.get("NO_CORRECTOS").toString());			
				
					List  firmas =  eletronicaBean.getFirmasModificatorias(iNoCliente);  //obtengo las firmas Modificatorias
					HashMap	registros = eletronicaBean.getDatosSolicitud(no_solicitud2, iNoCliente); // Obtengo los datos de la solicitud 
					
					mensajeModifi.append("<table align='center'  style='text-align: justify;' border='0' width='600' cellpadding='0' cellspacing='0'> "+
												"<tr><td align='justify' class='formas' text-align: justify;' >"+
												" Al amparo del Contrato "+nombreContrato+" con fecha de firma "+fechaContrato);									
												int x=1;
												if( firmas.size()>0){
													mensajeModifi.append(" asi como,   ");
													for (int i = 0; i <firmas.size(); i++) {
														HashMap datosF = (HashMap)firmas.get(i);												
														mensajeModifi.append(" Contrato Modificatorio "+x+" con fecha de firma " +datosF.get("FECHA").toString() + ", ");										
														x++;
													}
												}
					mensajeModifi.append(" y el Contrato de Operación Electrónica con fecha de firma "+fechaContrato_OE+							
												", celebrados entre  "+ registros.get("NOMBRE_IF")+"  y Nacional Financiera, S.N.C. se envían los datos de las empresas apoyadas con recursos NAFIN.<BR>"+									
												"<BR>Con fundamento en lo dispuesto por los artículos 1205 y 1298-A del Código de Comercio y del 52 de la ley de Instituciones de Crédito, la información e instrucciones que el Intermediario y Nacional Financiera, S.N.C. "+
												" se transmitan o comuniquen mutuamente mediante el sistema, tendrán pleno valor probatorio y fuerza legal para acreditar la operación realizada, el importe de la misma, su naturaleza; así como, las características y alcance de sus instrucciones."+
												"<BR>"+
												"</td></tr><table>");	
												
					eletronicaBean.guardarArchivoSolic(no_solicitud2,  rutaNombreArchivo, "Empresa",  "Modificar" ); //Carga el archivo txt en la base 
				
				
				}else {
				
					parametros  =  new ArrayList();
					parametros.add(no_solicitud2);
					parametros.add(claveCarga);	
					parametros.add(no_Prestamo);	
					List Fechas = Arrays.asList(fechaActMontos.split("\\s*,\\s*"));
					parametros.add(Fechas);					
					eletronicaBean.cancelaCargaEmpresas(parametros );					
					
				}
				
			}else  {
			
				parametros  =  new ArrayList();
				parametros.add(no_solicitud2);
				parametros.add(claveCarga);
				parametros.add(no_Prestamo);
				parametros.add(strPerfil);
				parametros.add(nombreArchivo);
			
				List resultadosMonto =   eletronicaBean.validaRebasaLimite(parametros ); //Valida el Monto Limite 
				
				
			}
		}
										
	}

/*	
}else  if ( informacion.equals("ContinuarCarga") ) {

	parametros  =  new ArrayList();
	parametros.add(no_solicitud2);
	parametros.add("S");
	parametros.add(strDirectorioTemp+nombreArchivo);		
	
	eletronicaBean.confirmaCargaCargaEmpresas(parametros ); //confirma la Carga de Empresas Apoyadas
	
	List datosTotales =  eletronicaBean.totalCargaLinea (claveCarga); //Obtengo los  totales del resumen 	
	HashMap totalesD = (HashMap)datosTotales.get(0);
	nuCorrectos  = Integer.parseInt(totalesD.get("NO_CORRECTOS").toString());
		
	List  firmas =  eletronicaBean.getFirmasModificatorias(iNoCliente);  //obtengo las firmas Modificatorias
	HashMap	registros = eletronicaBean.getDatosSolicitud(no_solicitud2, iNoCliente); // Obtengo los datos de la solicitud 
	mensajeModifi.append("<table align='center'  style='text-align: justify;' border='0' width='600' cellpadding='0' cellspacing='0'> "+
								"<tr><td align='justify' class='formas' text-align: justify;' >"+
								" Al amparo del Contrato "+nombreContrato+" con fecha de firma  "+fechaContrato);									
								int x=1;
								if( firmas.size()>0){
									mensajeModifi.append(" asi como,   ");
									for (int i = 0; i <firmas.size(); i++) {
										HashMap datosF = (HashMap)firmas.get(i);												
										mensajeModifi.append(" Contrato Modificatorio "+x+" con fecha de firma " +datosF.get("FECHA").toString() + ", ");										
										x++;
									}
								}
	mensajeModifi.append(" y el Contrato de Operación Electrónica con fecha de firma "+fechaContrato_OE+							
								", celebrados entre  "+ registros.get("NOMBRE_IF")+"  y Nacional Financiera, S.N.C. se envían los datos de las empresas apoyadas con recursos NAFIN.<BR>"+									
								"<BR>Con fundamento en lo dispuesto por los artículos 1205 y 1298-A del Código de Comercio y del 52 de la ley de Instituciones de Crédito, la información e instrucciones que el Intermediario y Nacional Financiera, S.N.C. "+
								" se transmitan o comuniquen mutuamente mediante el sistema, tendrán pleno valor probatorio y fuerza legal para acreditar la operación realizada, el importe de la misma, su naturaleza; así como, las características y alcance de sus instrucciones."+
								"<BR>"+
								"</td></tr><table>");	
	
	jsonArrResumen = JSONArray.fromObject(datosTotales);
	
	eletronicaBean.guardarArchivoSolic(no_solicitud2,  rutaNombreArchivo, "Empresa",  "Modificar" ); //Carga el archivo txt en la base 	
		
	//parametros para el envio de correo 
	parametros  =  new ArrayList();
	parametros.add(importe);
	parametros.add(moneda);
	parametros.add(no_Prestamo);
	parametros.add(fecha);
	parametros.add(hora);
	parametros.add(usuario);
	parametros.add(strNombre);		
	parametros.add(iNoCliente);			
	
	//eletronicaBean.envioCorreoCargaEmpresas( parametros ) ;//Se envia correo
			
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("claveCarga", claveCarga);
	jsonObj.put("nuCorrectos", String.valueOf(nuCorrectos));
	infoRegresar = jsonObj.toString(); 	
*/
}else  if ( informacion.equals("ArchivoAcuse") ) {

	parametros  =  new ArrayList();
	parametros.add(strDirectorioTemp);
	parametros.add(iNoCliente);
	parametros.add((String)session.getAttribute("strPais"));
	parametros.add(iNoNafinElectronico);
	parametros.add(strNombre);
	parametros.add(strNombreUsuario);
	parametros.add("if.gif");
	parametros.add(strDirectorioPublicacion);		
	parametros.add(no_Prestamo);	
	parametros.add(acuse);	
	parametros.add(moneda);	
	parametros.add(importe);	
	parametros.add(nombreContrato);	
	parametros.add(fechaContrato);	
	parametros.add(fechaContrato_OE);	
	parametros.add(regCorrectos);	
	parametros.add(fecha);	
	parametros.add(hora);	
	parametros.add(usuario);	
	parametros.add(no_solicitud2);	
	//parametros.add(strTipoUsuario);
	
	try {
		nombreArchivo =	eletronicaBean.getArchivoAcuse(parametros );			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo CSV", e);
	}	
	
	infoRegresar = jsonObj.toString(); 


}else  if ( informacion.equals("CancelarCarga") ) {

	parametros  =  new ArrayList();
	parametros.add(no_solicitud2);
	parametros.add(claveCarga);	
	parametros.add(no_Prestamo);	
	List Fechas = Arrays.asList(fechaActMontos.split("\\s*,\\s*"));
	parametros.add(Fechas);	

	eletronicaBean.cancelaCargaEmpresas(parametros );
	
	jsonObj.put("success", new Boolean(true));	
	infoRegresar = jsonObj.toString(); 	

	
	
}
	
%>
<%if (informacion.equals("CargaArchivoEmpresa")   ||  informacion.equals("ContinuarCarga")  ){ %>

 {
	 "success": true,
	 
	 "todOK":{
	 "registros":<%=jsonArrOK.toString()%>,
	 "total":<%=nuCorrectos%>},
	 
	 "todError":{
	 "registros":<%=jsonArrErr.toString()%>,
	 "total":<%=nuErrores%>},
	 
	 "todResumen":{
	 "registros":<%=jsonArrResumen.toString()%>,
	 "total":1},
	
	"todValidaDatos":{
	 "registros":<%=jsonValidaDatos.toString()%>,
	 "total":<%=nuValiDatos%>},
	 
	 "nuErrores":"<%=nuErrores%>",
	 "nuCorrectos":"<%=nuCorrectos%>",
	 "nuErroresVm":"<%=nuErroresVm%>",
	 "fechaActMontos":"<%=fechaActMontos%>",
	 
	 
	 "importe":"<%=importe%>",
	 "moneda":"<%=moneda%>",
	 "no_Prestamo":"<%=no_Prestamo%>",
	 "acuse":"<%=acuse%>",
	 "fecha":"<%=fecha%>",
	 "hora":"<%=hora%>",
	"mensajeModifi":"<%=mensajeModifi.toString()%>",
	 "nombreUsuario":"<%=usuario%>",
	 "claveCarga":"<%=claveCarga%>",
	  "boton":"<%=boton%>",
	 "nuValiDatos":"<%=nuValiDatos%>"
	 
 }
 
  
 <%}else { %>
 <%=infoRegresar%>
 
 <%}%>


<%!


	
%>