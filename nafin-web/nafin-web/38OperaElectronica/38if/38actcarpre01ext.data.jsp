<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.File,
		java.io.FileInputStream,
		java.io.BufferedOutputStream,
		java.io.FileInputStream,
		java.io.FileOutputStream,
		java.io.InputStream,
		java.io.OutputStream,
		java.math.BigDecimal,
		org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileUploadException,
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		netropology.utilerias.carga.ValidaCargaThread,
		netropology.utilerias.carga.ValidacionResult,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*,
		org.apache.commons.logging.Log,
		com.netro.electronica.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf" %>
<%!

	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	private final int MAX_PLANTILLA_FILE_SIZE = 52428800; // 50 MB   

%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CargaArchivo.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia del EJB de Operación Electrónica
	OperacionElectronica operacionElectronica = null;
	try {
		
		operacionElectronica = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
	
   } catch(Exception e){
   
   	log.error("CargaArchivo.inicializacion(Exception): No se pudo obtener instancia del EJB de Operación Electrónica.");
		e.printStackTrace();
		throw new AppException("No se pudo obtener instancia del EJB de Operación Electrónica.",e);
        		
   }

	// Validar que el usuario firmado en el sistema tenga clave ic_financiera
	if(!operacionElectronica.existeClaveFinanciera(claveIF)){
		estadoSiguiente 	= "MOSTRAR_AVISO";
		aviso					= "No se tiene asignada la Clave de Financiera, la cual es requerida para realizar la carga.";
		hayAviso				= true;
	}
      
	// 1. Remover objetos de sesion
	if ( !hayAviso ){
		
		// Revisar si hay un thread de una opracion anterior, y si es así detenerlo.
		ValidaCargaThread thread = (ValidaCargaThread) session.getAttribute("38ACTCARPRE01EXT_THREAD");
		if(thread != null){
			thread.setRequestStop(true);
			// Esperar a que el thread finalice
			while(thread.isRunning() || !thread.isCompleted()){
				Thread.sleep(500);
			}
			session.removeAttribute("38ACTCARPRE01EXT_THREAD"); // Remover thread de sesion 
		}
		
	}
 
	// 2. Definir el layout a mostrar ( en caso de que las validaciones sean exitosas )
	if(!hayAviso){

		String descripcionLayoutCsv = 
			"<table>"  +
			"<tr>"  +
			"	<td class=\"titulos\" align=\"center\">"  +
			"		<span class=\"titulos\">"  +
			"			Estructura de Archivo<br>Carga masiva"  +
			"		</span>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td colspan=\"2\" align=\"center\">"  +
			"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" >"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"left\" colspan=\"10\"  style=\"height:30px;padding:10px;\" >"  +
			"					El nombre del archivo CSV: IFNB_A5_AAAAMM.csv, deber&aacute; cumplir con el siguiente formato:<br><br>"  +
			"					<ul style=\"margin-left: 10px;\" > "  +
			"						<li style=\"margin-left: 10px;list-style-type: disc;\" ><b>IFNB</b>: es la nomenclatura de 6 d&iacute;gitos correspondiente a la clave del Intermediario Financiero No Bancario (ic_financiera), por ejemplo si la clave del IF es 3 se deber&aacute;n anteponer 5 ceros (0) al 3, quedando 000003.</li> "  +
			"						<li style=\"margin-left: 10px;list-style-type: disc;\" ><b>A5</b>: corresponde al n&uacute;mero del anexo, en este caso al Anexo 5 (Actualizaci&oacute;n de la Cartera Prendaria).</li> "  +
			"						<li style=\"margin-left: 10px;list-style-type: disc;\" ><b>AAAAMM</b>: es el a&ntilde;o y mes actual en que se carga la cartera, por ejemplo 201410 (a&ntilde;o 2014 mes 10 (Octubre)). </li> "  +
			"						<li style=\"margin-left: 10px;list-style-type: disc;\" ><b>.csv</b>: extensi&oacute;n del tipo de archivo a cargar. </li> "  +
			"					</ul> "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" width=\"25px\"  style=\"height:30px;\" >No.</td>"  +
			"				<td class=\"celda01\" align=\"center\">Nombre del Campo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Tipo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Longitud<br>M&aacute;xima</td>"  +
			"				<td class=\"celda01\" align=\"center\">Obligatorio</td>"  +
			"				<td class=\"celda01\" align=\"center\">Descripci&oacute;n</td>"  +
			"			</tr>"  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >1</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					No. Consecutivo NAFIN "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">-</td> "  +
			"				<td class=\"formas\" align=\"center\">-</td> "  +
			"				<td class=\"formas\" align=\"center\">No</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					N&uacute;mero consecutivo que registre la garant&iacute;a otorgada a NAFIN (1,2,3,4...). "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >2</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					N&uacute;mero de Cr&eacute;dito "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Alfanum&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">50</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					N&uacute;mero de Cr&eacute;dito asignado por la Acreditada al cr&eacute;dito otorgado a su cliente/acreditado. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >3</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Nombre Acreditado "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Alfanum&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">100</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Nombre del Acreditado/Cliente de la Acreditada. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >4</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Tipo Operaci&oacute;n "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Alfanum&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">50</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Tipo de Operaci&oacute;n del Cr&eacute;dito (Cr&eacute;dito Simple, Arrendamiento Financiero, Arrendamiento Puro, Factoraje, etc.). "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >5</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Fecha de Disposici&oacute;n de los recursos "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Fecha</td> "  +
			"				<td class=\"formas\" align=\"center\">10</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Fecha de disposici&oacute;n de los recursos solicitados a NAFIN; debe tener formato DD/MM/AAAA."  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >6</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Tipo Tasa "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Alfanum&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">50</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					'Fija' o 'Variable', de acuerdo con el tipo de tasa que la acreditada haya otorgado a su Cliente/Acreditado. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >7</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Tasa Base "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Alfanum&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">50</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					En caso de que la Acreditada haya otorgado el Cr&eacute;dito con TASA FIJA a su cliente/acreditado, ser&aacute; el porcentaje correspondiente a &eacute;sta. En caso de que la Acreditada haya otorgado el Cr&eacute;dito con TASA VARIABLE a su cliente/acreditado, ser&aacute; la base correspondiente: TIIE, LIBOR, etc.  "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >8</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Puntos Adicionales a la Tasa "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">2,2</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  + // , cuando el Tipo de Tasa es VARIABLE. Si el Tipo de Tasa es FIJA se deberá enviar con valor 0.
			"				<td class=\"formas\" align=\"left\"> "  +
			"					En caso de que la Acreditada haya otorgado el cr&eacute;dito con TASA VARIABLE a su cliente/acreditado, se registraran los puntos base adicionales la base indicada en el campo 7(Tasa Base). En caso de que se haya otorgado el cr&eacute;dito con TASA FIJA los puntos no aplicaran. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >9</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto Otorgado (pesos) "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">14,5</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto del Cr&eacute;dito otorgado por la Acreditada a su cliente/acreditado. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >10</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Recursos Solicitados a NAFIN "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">14,5</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto de los recursos solicitados a NAFIN. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >11</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Plazo Original (meses) "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">3</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Meses que corresponden al plazo del cr&eacute;dito otorgado por la Acreditada a su cliente/acreditado "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >12</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Capital Vigente (pesos) "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">14,5</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto del capital vigente del cr&eacute;dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr&eacute;dito registra menos de 90 d&iacute;as de atraso en el pago.  "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >13</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Intereses Vigentes (pesos) "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">14,5</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto de los intereses vigentes del cr&eacute;dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr&eacute;dito registra menos de 90 d&iacute;as de atraso en el pago. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >14</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Capital Vencido (pesos) "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">14,5</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto del capital vencido del cr&eacute;dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr&eacute;dito registra al menos, 90 d&iacute;as de atraso en el pago. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >15</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Intereses Vencidos (pesos) "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">14,5</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto de los intereses vencidos del cr&eacute;dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr&eacute;dito registra al menos, 90 d&iacute;as de atraso en el pago. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >16</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Intereses Moratorios (pesos) "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">14,5</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Monto de los intereses moratorios del cr&eacute;dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr&eacute;dito est&aacute; vencido (registra m&aacute;s de 90 d&iacute;as de atraso en el pago). "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >17</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Saldo Total (pesos) "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">14,5</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Saldo del monto del cr&eacute;dito otorgado por la Acreditada  a su cliente/acreditado, al cierre del mes inmediato anterior a la fecha de disposici&oacute;n  de la Acreditada. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >18</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					D&iacute;as de Retraso en el Pago "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Num&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">4</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  + // , cuando existe capital ó intereses vencidos; en caso contrario se deberá enviar con valor 0.
			"				<td class=\"formas\" align=\"left\"> "  +
			"					N&uacute;mero de d&iacute;as de atraso en el pago que presente el cr&eacute;dito otorgado por la Acreditada a su cliente/acreditado. "  +
			"				</td> "  +
			"			</tr> "  +
			"			<tr> "  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >19</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Descripci&oacute;n del bien "  +
			"				</td> "  +
			"				<td class=\"formas\" align=\"center\">Alfanum&eacute;rico</td> "  +
			"				<td class=\"formas\" align=\"center\">20</td> "  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td> "  +
			"				<td class=\"formas\" align=\"left\"> "  +
			"					Breve descripci&oacute;n del bien o del destino del cr&eacute;dito. "  +
			"				</td> "  +
			"			</tr> "  +
			"		</table>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"</table>";	
					
		resultado.put("descripcionLayoutCsv", 	descripcionLayoutCsv	);
	
		String descripcionLayoutPdf = 
			"<table>"  +
			"<tr>"  +
			"	<td class=\"titulos\" align=\"center\">"  +
			"		<span class=\"titulos\">"  +
			"			Estructura de Archivo PDF"  +
			"		</span>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td colspan=\"2\" align=\"center\">"  +
			"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" >"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"left\" colspan=\"10\"  style=\"height:30px;padding:10px;\" >"  +
			"					El nombre del archivo PDF: IFNB_A5_AAAAMM.pdf, deber&aacute; cumplir con el siguiente formato:<br><br>"  +
			"					<ul style=\"margin-left: 10px;\" > "  +
			"						<li style=\"margin-left: 10px;list-style-type: disc;\" ><b>IFNB</b>: es la nomenclatura de 6 d&iacute;gitos correspondiente a la clave del Intermediario Financiero No Bancario (ic_financiera), por ejemplo si la clave del IF es 3 se deber&aacute;n anteponer 5 ceros (0) al 3, quedando 000003.</li> "  +
			"						<li style=\"margin-left: 10px;list-style-type: disc;\" ><b>A5</b>: corresponde al n&uacute;mero del anexo, en este caso al Anexo 5 (Actualizaci&oacute;n de la Cartera Prendaria).</li> "  +
			"						<li style=\"margin-left: 10px;list-style-type: disc;\" ><b>AAAAMM</b>: es el a&ntilde;o y mes actual en que se carga la cartera, por ejemplo 201410 (a&ntilde;o 2014 mes 10 (Octubre)). </li> "  +
			"						<li style=\"margin-left: 10px;list-style-type: disc;\" ><b>.pdf</b>: extensi&oacute;n del tipo de archivo a cargar. </li> "  +
			"					</ul> "  +
			"				</td>"  +
			"			</tr>"  +
			"		</table>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"</table>";	
					
		resultado.put("descripcionLayoutPdf", 	descripcionLayoutPdf );
		
	}

	// 3. Definir el estado siguiente ( en caso de que las validaciones sean exitosas )
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_ARCHIVOS";
	}
	 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();
	
} else if ( informacion.equals("CargaArchivo.subirArchivos")      			) {
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);

	// Variables adicionales
	String   	directorioTemporal  		= strDirectorioTemp ;
	String 		login 						= iNoUsuario;
	
	// Variables de la Respuesta
	JSONObject	resultado					= new JSONObject();
	boolean		success						= true;
 
	// Create a factory for disk-based file items
	DiskFileItemFactory 	factory 			= new DiskFileItemFactory(DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD, new File(directorioTemporal) );
	// Create a new file upload handler
	ServletFileUpload 	upload 			= new ServletFileUpload(factory);
	List						fileItemList	= null; // List of FileItem
	try {
		
		// Definir el tamaño máximo que pueden tener los archivos
		upload.setSizeMax(MAX_PLANTILLA_FILE_SIZE);
		// Parsear request
		fileItemList = upload.parseRequest(request);
		
	} catch(Throwable t) {
			
		success		= false;
		log.error("CargaPlantilla.subirArchivo(Exception): Cargar en memoria o disco el contenido del archivo");
		t.printStackTrace();
		if( t instanceof SizeLimitExceededException  ) {
			throw new AppException("El Archivo es muy Grande, excede el límite que es de " + ( MAX_PLANTILLA_FILE_SIZE / 1048576 ) + " MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros
	String archivoCsv = "";
	String archivoPdf = "";
	
	// Leer parametros
	for(int i=0;i<fileItemList.size();i++){
		
		FileItem fileItem = (FileItem) fileItemList.get(i);

		if( !fileItem.isFormField() &&  "archivoCsv".equals( fileItem.getFieldName() ) ){
		
		 	 archivoCsv = fileItem.getName();
		 	 archivoCsv = archivoCsv == null?"":archivoCsv;
		 	 
		 	 // Validar que la extension del archivoCsv cargado sea csv
		 	 if( !archivoCsv.matches("(?i)^.*\\.csv$") ){
		 	 	 throw new AppException("El formato del archivo de origen no es el correcto. Debe tener extensión csv.");
		 	 }
		 	 
		 	 // Guardar Archivo en Disco
		 	 try {
		 
				// Generar Nombre del Archivo
				String fileName		= Comunes.cadenaAleatoria(16)+".csv";
				
				// Guardar Archivo en Disco
				File 	 uploadedFile	= new File( directorioTemporal + login + "." + fileName );
				fileItem.write(uploadedFile);
				
				resultado.put("archivoCsv", 		archivoCsv );
				resultado.put("archivoCsvReal",  fileName   );
				
				
			 } catch(Throwable e) {
				
				success		= false;
				log.error("CargaPlantilla.subirArchivo(Exception): Guardar Archivo en Disco");
				e.printStackTrace();
				throw new AppException("Ocurrió un error al guardar el archivo CSV");
				
			 }
			 
		 } else if( !fileItem.isFormField() &&  "archivoPdf".equals( fileItem.getFieldName() ) ){
		
		 	 archivoPdf = fileItem.getName();
		 	 archivoPdf = archivoPdf == null?"":archivoPdf;
		 	 
		 	 // Validar que la extension del archivoPdf cargado sea zip
		 	 if( !archivoPdf.matches("(?i)^.*\\.pdf$") ){
		 	 	 throw new AppException("El formato del archivo de origen no es el correcto. Debe tener extensión pdf.");
		 	 }
		 	 
		 	 // Guardar Archivo en Disco
		 	 try {
		 
				// Generar Nombre del Archivo
				String fileName		= Comunes.cadenaAleatoria(16)+".pdf";
				
				// Guardar Archivo en Disco
				File 	 uploadedFile	= new File( directorioTemporal + login + "." + fileName );
				fileItem.write(uploadedFile);
				
				resultado.put("archivoPdf", 		archivoPdf );
				resultado.put("archivoPdfReal",  fileName   );
					
			 } catch(Throwable e) {
				
				success		= false;
				log.error("CargaPlantilla.subirArchivo(Exception): Guardar Archivo en Disco");
				e.printStackTrace();
				throw new AppException("Ocurrió un error al guardar el archivo PDF");
				
			 }
			 
		 }
		 
	}
	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "REALIZAR_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)		);

	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacion") 				)	{
	
	JSONObject	resultado				= new JSONObject();
	boolean		success					= true;
	String 		estadoSiguiente 		= null;
	String 		msg						= null;
	
	// Leer Parametros
	String 		archivoCsv 				= (request.getParameter("archivoCsv")		== null)?"":request.getParameter("archivoCsv");
	String 		archivoCsvReal 		= (request.getParameter("archivoCsvReal")	== null)?"":request.getParameter("archivoCsvReal");
	String 		archivoPdf 				= (request.getParameter("archivoPdf")		== null)?"":request.getParameter("archivoPdf");
	String 		archivoPdfReal 		= (request.getParameter("archivoPdfReal")	== null)?"":request.getParameter("archivoPdfReal");

	// Variables adicionales
	String   	directorioTemporal	= strDirectorioTemp ;
	String 		login 					= iNoUsuario;
	
	String 		rutaArchivoCsv			= directorioTemporal + login + "." + archivoCsvReal;
	String 		rutaArchivoPdf			= directorioTemporal + login + "." + archivoPdfReal;
	String 		claveIF 					= iNoCliente;
	
	log.info("[CargaArchivo.realizarValidacion]: claveIF            = <" + claveIF            + ">");
	log.info("[CargaArchivo.realizarValidacion]: directorioTemporal = <" + directorioTemporal + ">");
	log.info("[CargaArchivo.realizarValidacion]: login              = <" + login              + ">");
	log.info("[CargaArchivo.realizarValidacion]: rutaArchivoCsv     = <" + rutaArchivoCsv     + ">");
	log.info("[CargaArchivo.realizarValidacion]: archivoCsv         = <" + archivoCsv         + ">");
	log.info("[CargaArchivo.realizarValidacion]: rutaArchivoPdf     = <" + rutaArchivoPdf     + ">");
	log.info("[CargaArchivo.realizarValidacion]: archivoPdf         = <" + archivoPdf         + ">");
	
	// Agregar parametros especificos
   CarteraPrendariaValidator fileValidator = new CarteraPrendariaValidator();
	fileValidator.setClaveIF(claveIF);
	fileValidator.setDirectorioTemporal(directorioTemporal);
	fileValidator.setLogin(login);
	fileValidator.setRutaArchivoCsv(rutaArchivoCsv);
	fileValidator.setArchivoCsv(archivoCsv);
	fileValidator.setRutaArchivoPdf(rutaArchivoPdf);
   fileValidator.setArchivoPdf(archivoPdf);
	fileValidator.setOperacionElectronicaBean(new OperacionElectronicaBean());
	
	// Crear thread para realizar la validación de los archivos
	ValidaCargaThread validaArchivo = new ValidaCargaThread();
	validaArchivo.setFileValidator(fileValidator);

	// Inicializar la carga
	boolean exitoInicializacion = validaArchivo.inicializar();
	request.getSession().setAttribute("38ACTCARPRE01EXT_THREAD",validaArchivo); // #TAG

	// Determinar si solo existe carga de los archivos en NAFINET
	boolean existeCargaPreviaSoloNafinet = exitoInicializacion && fileValidator.getExisteCargaPreviaSoloNafinet()?true:false;

	// Insertar en bitácora del CCBE el "error de programación"
	if( !exitoInicializacion ){
					
		try {
							
			// Obtener instancia del EJB
			OperacionElectronica operacionElectronica = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
						
			// Insertar en bitácora
					
			CarteraPrendariaValidator validator = (CarteraPrendariaValidator) validaArchivo.getFileValidator();
					
			String archivoCsv01		= validator.getArchivoCsv();
			String claveFinanciera	= archivoCsv01.substring(0,6).replaceFirst("^[0]+","");
			String claveMesCarga	  	= archivoCsv01.substring(10,16);
					
			claveFinanciera			= !claveFinanciera.matches("^[\\d]+$")?"0":claveFinanciera;
				
			operacionElectronica.registraEnBitacoraCCBE(
				  AuxiliarCarteraPrendaria.EVENT_CARGA_ANEXO_4,
				  validaArchivo.hasError()?AuxiliarCarteraPrendaria.MESSAGE_ERROR_NO_IDENTIFICADO:AuxiliarCarteraPrendaria.MESSAGE_ANEXO4_ERROR_DETALLE,
				  Integer.parseInt(claveFinanciera),
				  null, // No se usa, se deja por compatibilidad
				  claveMesCarga.substring(4,6) + "-" + claveMesCarga.substring(0,4), 
				  login, 
				  null // "No se aceptó el detalle del contenido."
			);
					
		} catch(Exception e){
						
			log.error("CargaArchivo.actualizarAvance(Exception): No se pudo registrar error en bitácora del CCBE.");
			e.printStackTrace();
						
		}
			
		
		
	}

	// Determinar el estado siguiente
	if( exitoInicializacion && existeCargaPreviaSoloNafinet ){
		estadoSiguiente = "SOLICITAR_AUTORIZACION_VALIDACION";
	} else if( exitoInicializacion ){
		estadoSiguiente = "MUESTRA_PANTALLA_AVANCE";
	// La inicializacion falló y/o se presentó un error de programación
	} else {
		estadoSiguiente = "PRESENTAR_DETALLE_ERRORES_VALIDACION";
	}
	
	// En caso de que haya algun mensaje mostrarlo
	if( exitoInicializacion  && validaArchivo.getMensajes().length() > 0 ){
		msg = validaArchivo.getMensajes();
	} else {
		resultado.put("msgErrores", validaArchivo.getMensajes() );
	}
	
	// Enviar detalle del envío de las notificaciones
	resultado.put("numeroTotalRegistrosProcesar",	String.valueOf( fileValidator.getRecordsNumber() )	);
	resultado.put("numeroRegistrosProcesados",		"0"	 															);
	resultado.put("avance",									"0"	 															);
	resultado.put("porcentajeAvance",					"0"	 															);
	resultado.put("envioFinalizado",						new Boolean(false)	 										);

	// Enviar parametros de la forma
	resultado.put("archivoCsv",							archivoCsv		);
	resultado.put("archivoPdf",							archivoPdf		);
	
	// Enviar resultado de la operacion
	resultado.put("success", 								new Boolean(success)	);
	resultado.put("estadoSiguiente", 					estadoSiguiente 		);
	resultado.put("msg",										msg						);

	infoRegresar = resultado.toString();
	
} else if( informacion.equals("CargaArchivo.muestraPantallaAvance") ){
	
	JSONObject	resultado 							= new JSONObject();
	boolean		success	 							= true;
	String 		estadoSiguiente 					= null;
	String		msg									= null;
	
	String 		archivoCsv 							= (request.getParameter("archivoCsv")		== null)?"":request.getParameter("archivoCsv");
	String 		archivoPdf 							= (request.getParameter("archivoPdf")		== null)?"":request.getParameter("archivoPdf");

	String 		numeroTotalRegistrosProcesar	= (request.getParameter("numeroTotalRegistrosProcesar")	== null)?"0":request.getParameter("numeroTotalRegistrosProcesar");
	
	// Lanzar Thread
	ValidaCargaThread validaArchivo 				= (ValidaCargaThread) session.getAttribute("38ACTCARPRE01EXT_THREAD");
	validaArchivo.setRunning(true);
	new Thread(validaArchivo).start();
	
	resultado.put("archivoCsv",						archivoCsv			);
	resultado.put("archivoPdf",						archivoPdf			);
	
	resultado.put("numeroTotalRegistrosProcesar",numeroTotalRegistrosProcesar	);
	resultado.put("numeroRegistrosProcesados",	"0"	 								);
	resultado.put("avance",								"0"	 								);
	resultado.put("porcentajeAvance",				"0"	 								);
	resultado.put("envioFinalizado",					new Boolean(false)				);
	
	// Determinar el estado siguiente
	estadoSiguiente = "ACTUALIZAR_AVANCE";
	
	// Enviar resultado de la operacion
	resultado.put("success", 							new Boolean(success)				);
	resultado.put("estadoSiguiente", 				estadoSiguiente 					);
	resultado.put("msg",									msg									);
	
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("CargaArchivo.actualizarAvance") 		   ){
	
	JSONObject	resultado 						= new JSONObject();
	boolean		success	 						= true;
	String 		estadoSiguiente 				= null;
	String		msg								= null;
	
	// LEER PARAMETROS
	boolean		envioFinalizado 				= "true".equals(request.getParameter("envioFinalizado"))?true:false;
	String 		archivoCsv 						= (request.getParameter("archivoCsv") == null)?"":request.getParameter("archivoCsv");
	String 		archivoPdf 						= (request.getParameter("archivoPdf") == null)?"":request.getParameter("archivoPdf");

	// LEER THREAD DE SESION
	ValidaCargaThread thread 					= (ValidaCargaThread) session.getAttribute("38ACTCARPRE01EXT_THREAD");
	
	// UNA VEZ QUE AE HA MOSTRADO QUE SE HA LLEGADO AL 100%, PRESENTAR RESULTADOS
	if(        envioFinalizado && !thread.hasError() ){ // El envio a flujo de fondos fue exitoso

		resultado.put("archivoCsv", archivoCsv	);
		resultado.put("archivoPdf", archivoPdf	);
	
		// Obtener Resultado de la Validación
		ValidacionResult 	validacionResult	= thread.getResultados();
		
		boolean 				hayIncorrectos 	= validacionResult.getErrores().matches("^\\s*$")?false:true;
		boolean 				hayErrorCifras 	= validacionResult.getCifras().matches("^\\s*$") ?false:true;
					
		// Determinar si se mostrará el boton continuar carga	
		boolean 				exitoValidacion	= hayIncorrectos || hayErrorCifras ?false:true;
		
		if(exitoValidacion){
			String numeroRegistros = Comunes.formatoDecimal(validacionResult.getNumRegOk(),0);
			resultado.put("numeroRegistros",numeroRegistros);
		} 
		
		// Insertar en bitácora del CCBE "error de datos"
		if(!exitoValidacion){
			
			try {
						
				// Obtener instancia del EJB
				
				OperacionElectronica operacionElectronica = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
					
				// Insertar en bitácora
				
				CarteraPrendariaValidator validator = (CarteraPrendariaValidator) thread.getFileValidator();
				
				String archivoCsv01		= validator.getArchivoCsv();
				String claveFinanciera	= archivoCsv01.substring(0,6).replaceFirst("^[0]+","");
				String claveMesCarga	  	= archivoCsv01.substring(10,16);
				String login				= validator.getLogin();
				
				claveFinanciera			= !claveFinanciera.matches("^[\\d]+$")?"0":claveFinanciera;
				
				operacionElectronica.registraEnBitacoraCCBE(
					  AuxiliarCarteraPrendaria.EVENT_CARGA_ANEXO_4,
					  AuxiliarCarteraPrendaria.MESSAGE_ANEXO4_ERROR_DETALLE,
					  Integer.parseInt(claveFinanciera),
					  null, // No se usa, se deja por compatibilidad
					  claveMesCarga.substring(4,6) + "-" + claveMesCarga.substring(0,4), 
					  login, 
					  null // "No se aceptó el detalle del contenido."
				);

			} catch(Exception e){
				
				log.error("CargaArchivo.actualizarAvance(Exception): No se pudo registrar error en bitácora del CCBE.");
				e.printStackTrace();
					
			}
			
		}
		
		// Determinar el estado siguiente
		if(exitoValidacion){
			estadoSiguiente = "SOLICITAR_AUTORIZACION_ENVIO";
		} else {
			estadoSiguiente = "PREPARAR_DETALLE_ERRORES_VALIDACION";
		}

	// EN CASO DE QUE HAYA FALLADO EL THREAD, MOSTRAR DETALLE DEL ERROR Y REINICIALIZAR LA PANTALLA
	} else if( envioFinalizado &&  thread.hasError() ){ 
	
		// Mostrar mensaje de error mediante un alert.
		msg = thread.getMensajes();
			
		// Insertar en bitácora del CCBE el "error de programación"
		try {
					
			// Obtener instancia del EJB
			OperacionElectronica operacionElectronica = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
				
				
			// Insertar en bitácora
			
			CarteraPrendariaValidator validator = (CarteraPrendariaValidator) thread.getFileValidator();
			
			String archivoCsv01		= validator.getArchivoCsv();
			String claveFinanciera	= archivoCsv01.substring(0,6).replaceFirst("^[0]+","");
			String claveMesCarga	  	= archivoCsv01.substring(10,16);
			String login				= validator.getLogin();
			
			claveFinanciera			= !claveFinanciera.matches("^[\\d]+$")?"0":claveFinanciera;
			
			operacionElectronica.registraEnBitacoraCCBE(
				  AuxiliarCarteraPrendaria.EVENT_CARGA_ANEXO_4,
				  AuxiliarCarteraPrendaria.MESSAGE_ERROR_NO_IDENTIFICADO,
				  Integer.parseInt(claveFinanciera),
				  null, // No se usa, se deja por compatibilidad
				  claveMesCarga.substring(4,6) + "-" + claveMesCarga.substring(0,4), 
				  login, 
				  null // "Error no identificado. " + msg
			);
			
				
      } catch(Exception e){
        	log.error("CargaArchivo.actualizarAvance(Exception): No se pudo registrar error en bitácora del CCBE.");
        	e.printStackTrace();
        		
      }
        	
		// Remover thread de sesion
		session.removeAttribute("38ACTCARPRE01EXT_THREAD");
		
		// DETERMINAR EL ESTADO SIGUIENTE
		estadoSiguiente = "FIN";
			
	// ENVIAR PORCENTAJE DE AVANCE ACTUALIZADO
	} else {
		
		resultado.put("archivoCsv",			archivoCsv	);
		resultado.put("archivoPdf",			archivoPdf	);
		
		// ENVIAR PORCENTAJE DE AVANCE
		resultado.put("numeroTotalRegistrosProcesar",new Integer(thread.getRecordsNumber())							);
		resultado.put("numeroRegistrosProcesados",	new Integer(thread.getProcessedRegisters())					);
		resultado.put("avance",								new Double(thread.getPercent()/100.0f)							);
		resultado.put("porcentajeAvance",				new Double(thread.getPercent())									);
		resultado.put("envioFinalizado",             new Boolean(!thread.isRunning() && thread.isCompleted())	);
			
		// DETERMINAR EL ESTADO SIGUIENTE
		estadoSiguiente = "ACTUALIZAR_AVANCE";
		
	}
 
	// ENVIAR RESULTADO DE LA OPERACION
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("CargaArchivo.prepararDetalleErroresValidacion")  ){

	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	
	// Leer Parametros
	String 		archivoCsv 	= (request.getParameter("archivoCsv") == null)?"":request.getParameter("archivoCsv");
	String 		archivoPdf 	= (request.getParameter("archivoPdf") == null)?"":request.getParameter("archivoPdf");

	ValidaCargaThread validaArchivo 		= (ValidaCargaThread) request.getSession().getAttribute("38ACTCARPRE01EXT_THREAD");
	
	// Obtener Resultado de la Validación
	ValidacionResult 	validacionResult  = validaArchivo.getResultados();

	//  Leer Resultado de la Validacion ( Registros con Errores )
	String detalleRegistrosConErrores 	= validacionResult.getErrores();
	String numeroRegistrosConErrores 	= Comunes.formatoDecimal(validacionResult.getNumRegErr(),0); 
	
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	/* String erroresVsCifrasControl = resultado.getCifras(); */

	// Remover thread de sesion
	session.removeAttribute("38ACTCARPRE01EXT_THREAD");
		
	// Enviar resultado general de la validacion
	resultado.put("detalleRegistrosConErrores",						detalleRegistrosConErrores				);
	resultado.put("numeroRegistrosConErrores",						numeroRegistrosConErrores				);

	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "PRESENTAR_DETALLE_ERRORES_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.transmitirRegistros")      )  {
	
	JSONObject		resultado				= new JSONObject();
	String 			estadoSiguiente		= null;
	boolean			success					= true;
	String[] 		msg 						= new String[1];
	
	ValidaCargaThread thread 				= (ValidaCargaThread) request.getSession().getAttribute("38ACTCARPRE01EXT_THREAD");
	
	// Obtener Resultado de la Validación
	CarteraPrendariaValidator 	carteraPrendaria	= (CarteraPrendariaValidator) thread.getFileValidator();
	
	AuxiliarCarteraPrendaria carga = new AuxiliarCarteraPrendaria();
	if( carga.transmiteCarteraPrendaria(carteraPrendaria,msg) ){
		msg[0] = "Archivos procesados exitosamente";
	}
	
	// Remover thread de sesion
	session.removeAttribute("38ACTCARPRE01EXT_THREAD"); 
	
	// Especificar el estado siguiente
	estadoSiguiente = "FIN";
	
	resultado.put("msg",											msg[0]								);	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 						estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 									new Boolean(success)				);
	
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("38actcarpre01ext.jsp"); 
	 
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>