Ext.onReady(function(){
	var tipoUsr = Ext.getDom('strTipoUsuario').value;
	tipoUsr = (tipoUsr=='NAFIN')?false:true;
//----------------------------------------------------------------------
	
	var procesarConsultaData = function(store,arrRegistros,opts){
		if(arrRegistros!=null){
			gridConsFL.show();			
		var el = gridConsFL.getGridEl();
		if(store.getTotalCount()>0){
			el.unmask();
			//consultaTotales.load();
			Ext.getCmp('btnGenerarArchivoXLS').enable();
		}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
				gridTotales.hide();
				Ext.getCmp('btnGenerarArchivoXLS').disable();
			}
		}
	}
	
	var procesarSuccessFailureGenerarArchivoTodas =  function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var archivo = resp.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fpArchivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpArchivo.getForm().getEl().dom.submit();

		} else{
			NE.util.mostrarConnError(response,opts);
		}

            Ext.getCmp('btnGenerarArchivoXLS').setIconClass('icoXls');
            Ext.getCmp('btnGenerarArchivoXLS').enable();

	}

function procesarConsultaTotal(store,arrRegistros,opts) {
	if(arrRegistros!=null){
		gridTotales.show();
		var el = gridTotales.getGridEl();
		if(store.getTotalCount()>0){
			if(arrRegistros==''){
				//consultaTotales.load();
			}
			gridTotales.el.unmask();
		}else{		
				gridTotales.el.mask('No se cargaron los totales', 'x-mask');
		}
	}
}

//-----------------STORES CATALOGOS---------------------------------------------

	var storeCatIfData = new Ext.data.JsonStore
	({
		id: 'storeCatIfData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '38consultaContrModif01ext.data.jsp',
		baseParams: 
		{
			informacion: 'CatalogoIF'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners:
		{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '38consultaContrModif01ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'CG_RAZON_SOCIAL'},
				{name: 'CG_CONTRATO_OE'},
				{name: 'TIPO'},
                                {name: 'NUM_CONT'},
				{name: 'DF_FECHA_FIRMA'},
				{name: 'DF_FIRMA_OE'},
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
						procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var consultaTotales = new Ext.data.JsonStore({
		root: 'registros',
		url: '38consultaContrModif01ext.data.jsp',
		baseParams: {
			informacion: 'Totales'
		},
		fields: [
					{name: 'REGTOTAL'},
					{name: 'SALDOTOTAL'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotal,
			exception: {
						fn: function(proxy,type,action,optionsRequest,response,args){
								NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
								procesarConsultaTotal(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
						}
			}
		}
	});

//------------------------------------------------------------------------------
elementosForma = [
                {
			//MONEDA
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			emptyText: 'Seleccionar',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			allowBlank: true,
			store: storeCatIfData,
			tpl: NE.util.templateMensajeCargaCombo,
			name:'cboIf',
			id: 'cboIf_',
			mode: 'local',
			hiddenName: 'cboIf',
			forceSelection: true,
			hidden: tipoUsr,
			anchor: '90%'
		},
                {
			xtype: 'compositefield',
			fieldLabel: 'Fecha Firma Contrato',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '90%',
			items: [
				{
					// Fecha Inicio
					xtype: 'datefield',
					name: 'fmFechaCargaIni',
					id: 'fmFechaCargaIni',
					vtype: 'rangofecha',
					campoFinFecha: 'fmFechaCargaFin',
					allowBlank: true,
					msgTarget: 'side',
					width: 100,
					startDay: 0,
					width: 100,
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'hasta',
					width: 50
				},
				{
					// Fecha Final
					xtype: 'datefield',
					name: 'fmFechaCargaFin',
					id: 'fmFechaCargaFin',
					vtype: 'rangofecha',
					campoInicioFecha: 'fmFechaCargaIni',
					allowBlank: true,
					msgTarget: 'side',
					width: 100,
					startDay: 0,
					width: 100,
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Firma Operaci�n Electr�nica',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '90%',
			items: [
				{
					// Fecha Inicio
					xtype: 'datefield',
					name: 'fmFechaCargaOpeIni',
					id: 'fmFechaCargaOpeIni',
					vtype: 'rangofecha',
					campoFinFecha: 'fmFechaCargaOpeFin',
					allowBlank: true,
					msgTarget: 'side',
					width: 100,
					startDay: 0,
					width: 100,
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'hasta',
					width: 50
				},
				{
					// Fecha Final
					xtype: 'datefield',
					name: 'fmFechaCargaOpeFin',
					id: 'fmFechaCargaOpeFin',
					vtype: 'rangofecha',
					campoInicioFecha: 'fmFechaCargaOpeIni',
					allowBlank: true,
					msgTarget: 'side',
					width: 100,
					startDay: 0,
					width: 100,
					margins: '0 20 0 0'
				}
			]
		}
		
	]

	var fpCritBusq = new Ext.form.FormPanel({
		width:            600,
		labelWidth:       170,
		id:               'fpCritBusq',
		height:           'auto',
		title:            'Consulta de Contratos',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		defaultType:      'textfield',
		trackResetOnLoad: true,
		frame:            true,
		monitorValid:     false,
		hidden:           false,
		defaults: 
		{
			msgTarget:    'side',
			anchor:       '-20'
		},
		items:            elementosForma,
		buttons:[{
			//Bot�n BUSCAR
			xtype:    'button',
			text:     'Buscar',
			name:     'btnBuscar',
			iconCls:  'icoBuscar',
			hidden:   false,
			formBind: true,
			handler:  function(boton, evento){
				var fecIni = Ext.getCmp('fmFechaCargaIni');
				var fecFin = Ext.getCmp('fmFechaCargaFin');

				var fecIniOpe = Ext.getCmp('fmFechaCargaOpeIni');
				var fecFinOpe = Ext.getCmp('fmFechaCargaOpeFin');

				if(fecIni.getValue()!='' && fecFin.getValue()==''){
					fecFin.markInvalid('Indicar Fecha final de Solicitud');
					return;
				}else if(fecFin.getValue()!='' && fecIni.getValue()==''){
					fecIni.markInvalid('Indicar Fecha inicial de Solicitud');
					return;
				}
				if(fecIniOpe.getValue()!='' && fecFinOpe.getValue()==''){
					fecFinOpe.markInvalid('Indicar Fecha final de Solicitud');
					return;
				}else if(fecFinOpe.getValue()!='' && fecIniOpe.getValue()==''){
					fecIniOpe.markInvalid('Indicar Fecha inicial de Solicitud');
					return;
				}

				consulta.load({ params: Ext.apply(fpCritBusq.getForm().getValues(),{
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15})
				});
			}
		},{
			text:    'Limpiar',
			iconCls: 'icoLimpiar',
			handler: function() {
                                Ext.getCmp('fmFechaCargaIni').setValue('');
				Ext.getCmp('fmFechaCargaFin').setValue('');
                                Ext.getCmp('fmFechaCargaOpeIni').setValue('');
				Ext.getCmp('fmFechaCargaOpeFin').setValue('');
                                Ext.getCmp('cboIf_').setValue('');
                                
			}
		}]
	});
	
	var gridConsFL = new Ext.grid.GridPanel({
				id: 'gridConsFL',
				hidden: true,
				header:true,
				title: 'Consulta de Contratos y Modificatorios',
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						header:'Nombre del Intermediario',
						tooltip: 'Nombre del Intermediario',
						sortable: true,
						dataIndex: 'CG_RAZON_SOCIAL',
						width: 300,
						align: 'left'
						},
						{
						header:'Tipo',
						tooltip: 'Nombre Contrato',
						sortable: true,
						dataIndex: 'TIPO',
						width: 90,
						align: 'left'
						},
						{
						header:'N�mero',
						tooltip: 'Numero',
						sortable: true,
						dataIndex: 'NUM_CONT',
						width: 70,
						align: 'left'
						},
						{
						header:'Nombre del Contrato',
						tooltip: 'Nombre del Contrato',
						sortable: true,
						dataIndex: 'CG_CONTRATO_OE',
						width: 240,
						align: 'left'
						},
						{
						header:'Fecha de Firma',
						tooltip: 'Fecha de Firma de Contrato',
						sortable: true,
						dataIndex: 'DF_FECHA_FIRMA',
						width: 100,
						align: 'center'
						},
						{
						header:'Fecha Firma de<br/>Operaci�n Electr�nica',
						tooltip: 'Fecha Firma de<br/>Operaci�n Electr�nica',
						sortable: true,
						dataIndex: 'DF_FIRMA_OE',
						width: 120,
						align: 'center'
						}
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					items: ['->','-',
								{
									xtype: 'button',
									text: 'Descargar Archivo',
									id: 'btnGenerarArchivoXLS',
									iconCls:'icoXls',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '38consultaContrModif01ext.data.jsp',
											params: Ext.apply(fpCritBusq.getForm().getValues(),{
												informacion: 'ArchivoXLS'}),
											callback: procesarSuccessFailureGenerarArchivoTodas
										});
									}
								}
							]
				}
		});

	var gridTotales = new Ext.grid.EditorGridPanel({
		store: consultaTotales,
		title: 'Totales de Saldos',
		id: 'gridTotales',
		hidden:	true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'N�mero de Saldos',
				dataIndex: 'REGTOTAL',
				align: 'center',	width: 150
			},{
				header: 'Total Monto',
				dataIndex: 'SALDOTOTAL',
				width: 200,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
		],
		width: 360,
		height: 100
		//frame: false
	}); 
	

	var fpArchivo = new Ext.form.FormPanel({
		id: 'fpArchivo1',
		width: 600,
		title: '',
		frame: true,
		hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;'
	});

//------------------Principal-------------------
	var pnl = new Ext.Container
	({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
		 [ fpArchivo,
			fpCritBusq,
			NE.util.getEspaciador(30),
			gridConsFL,
			NE.util.getEspaciador(10),
			gridTotales
		 
		 ]
	});
  
  
});