<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.math.*, 
	java.text.*, 
	java.sql.*,
	netropology.utilerias.*,		
	java.text.SimpleDateFormat,
	java.util.Date,		
	com.netro.exception.*,
	com.netro.model.catalogos.*,	
	com.netro.electronica.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf"%>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
String fechaOperacionIni = (request.getParameter("fechaOperacionIni")!=null)?request.getParameter("fechaOperacionIni"):"";
String fechaOperacionFin = (request.getParameter("fechaOperacionFin")!=null)?request.getParameter("fechaOperacionFin"):"";
String no_Prestamo = (request.getParameter("no_Prestamo")!=null)?request.getParameter("no_Prestamo"):"";

String infoRegresar ="";
JSONObject jsonObj = new JSONObject();

//Declaración de la clase para la consulta  
Rep_Prestamos paginador = new Rep_Prestamos (); 
paginador.setIc_if(ic_if);
paginador.setFechaOperacionIni(fechaOperacionIni);
paginador.setFechaOperacionFin(fechaOperacionFin);
paginador.setNo_Prestamo(no_Prestamo);


CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);


if (informacion.equals("catIFData")){

	CatalogoIFElectronica catalogo = new CatalogoIFElectronica();
	catalogo.setCampoClave("i.ic_if");
	catalogo.setCampoDescripcion("i.cg_razon_social");	
	catalogo.setOrden("i.cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();	

}else  if (informacion.equals("Consultar")){

	try {
		String noPrestamoAnterior ="";
		int groupId = 0;
		
		
		Registros reg	=	queryHelper.doSearch();
		while(reg.next()){
		
		String noPrestamo = (reg.getString("NO_PRESTAMO") == null) ? "" : reg.getString("NO_PRESTAMO");
		if( !noPrestamoAnterior.equals(noPrestamo) ){
			noPrestamoAnterior = noPrestamo;
			groupId++;
		} 
		reg.setObject("GROUP_ID",String.valueOf(groupId));					
	}	
	
	String consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);
	
	infoRegresar = jsonObj.toString();  
	
	
		
		
		infoRegresar	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	}	catch(Exception e) {
		throw new AppException("Error en la paginación", e);
	}

}else  if ( informacion.equals("ArchivoCSVCons") ) {
	try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	infoRegresar = jsonObj.toString(); 
}	
%>

<%=infoRegresar%>
 

