Ext.onReady(function(){

	var procesarCargaRegistro = function(store, registros, opts){
		var jsonData = store.reader.jsonData;
		if (registros != null) {
			if(store.getTotalCount() > 0) {
				var registro = store.getAt(0);
				Ext.getCmp('_cbo_sub').setValue(registro.get('USR_SUB'));
				Ext.getCmp('_tel_sub').setValue(registro.get('TEL_SUB'));
				Ext.getCmp('_cbo_con').setValue(registro.get('USR_CON'));
				Ext.getCmp('_tel_con').setValue(registro.get('TEL_CON'));
				Ext.getCmp('_mails_fondo').setValue(registro.get('MAILS'));
				Ext.getCmp('_ic_mail').setValue(registro.get('IC_MAIL'));
				
				Ext.getCmp('cmbConcentrador2').setValue(registro.get('USR_CON_DOS'));
				Ext.getCmp('tfTelefono').setValue(registro.get('TEL_CON_DOS'));
				Ext.getCmp('correoDirector').setValue(registro.get('EMAIL_DIR'));
				
			}
			// Actualizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply(
				store.baseParams,
				{
					operacion: ''
				}
			);
		}
	}
	
	function procesarSuccessFailureGuardar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			Ext.Msg.alert('Mensaje','La operaci�n ha sido realizada con �xito');	
			registroExistenteData.load();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){
			
			if( estadoSiguiente == "ACTUALIZAR" ){
				Ext.Ajax.request({
							url: '38paramGrales.data.jsp',
								params:	Ext.apply(fpDatos.getForm().getValues(),
										{
											informacion:'ActualizaDatos'
										}
								),
								callback: procesarSuccessFailureGuardar
					});
				
			}
			
	}

//--------------------------- STORE�S -----------------------------

	var registroExistenteData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registroExistenteDataStore',
			url: 			'38paramGrales.data.jsp',
			baseParams: {	informacion:	'CargarDatos'	},
			fields: [
				{ name: 'IC_MAIL'},
				{ name: 'USR_SUB'},
				{ name: 'TEL_SUB'},
				{ name: 'USR_CON'},
				{ name: 'TEL_CON'},
				{ name: 'MAILS'},
				{ name: 'USR_CON_DOS'},
				{ name: 'TEL_CON_DOS'},
				{ name: 'EMAIL_DIR'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				load: 	procesarCargaRegistro,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarCargaRegistro(null, null,null);
					}
				}
			}
	});
	
	
	var catalogoConcentrador = new Ext.data.JsonStore({
		id: '_catalogoConcentrador',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '38paramGrales.data.jsp',
		baseParams: {
			informacion: 'catalogoConcentrador'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
//--------------------------- COMPONENTES -----------------------------
	
	var mesaPanel = {
		xtype		: 'panel',
		id 		: '_mesaPanel',
		items		: [{
				layout		: 'form',
				labelWidth	: 195,
				bodyStyle: 	'padding: 2px; padding-right:20px;padding-left:60px;',
				items		:	
				[
					{
						xtype:'displayfield',
						border: false,
						align: 'left',
						value:'Subdirector de Mesa de Control de Cr�dito'
					},{
						xtype:'hidden',
						id:	'_ic_mail',
						name: 'ic_mail',
						value:''
					},
					{
						xtype				: 'combo',
						id					: '_cbo_sub',
						hiddenName 		: 'cbo_sub',
						fieldLabel		: 'Usuario Subdirector',
						emptyText: 'Seleccione...',
						width				: 330,
						forceSelection	: true,
						msgTarget: 'side',
						triggerAction	: 'all',
						mode				: 'local',
						valueField		: 'clave',
						displayField	: 'descripcion',
						margins		: '0 20 0 0',
						allowBlank	: false,
						store				: catalogoConcentrador
					},
					{
						xtype			: 'textfield',
						name			: 'tel_sub',
						id				: '_tel_sub',
						fieldLabel	: 'Tel�fono',
						msgTarget: 'side',
						maskRe:		/[0-9]/,
						maxLength	: 8,
						margins		: '0 20 0 0',
						width			: 200,
						allowBlank	: false,
						tabIndex		:	1
					}
				]
		}]
	};
	
	var gerenciaPanel = {
		xtype		: 'panel',
		id 		: '_gerenciaPanel',
		items		: [{
				layout		: 'form',
				labelWidth	: 195,
				bodyStyle: 	'padding: 2px; padding-right:20px;padding-left:60px;',
				items		:	
				[
					{
						xtype:'displayfield',
						border: false,
						align: 'left',
						value:'Gerente Operaciones de Cr�dito'
					},
					{
						xtype				: 'combo',
						id					: '_cbo_con',
						hiddenName 		: 'cbo_con',
						fieldLabel		: 'Usuario Concentrador',
						emptyText: 'Seleccione...',
						width				: 330,
						forceSelection	: true,
						msgTarget: 'side',
						allowBlank	: false,
						triggerAction	: 'all',
						mode				: 'local',
						valueField		: 'clave',
						displayField	: 'descripcion',
						margins		: '0 20 0 0',
						store				: catalogoConcentrador
					},
					{
						xtype			: 'textfield',
						name			: 'tel_con',
						id				: '_tel_con',
						fieldLabel	: 'Tel�fono',
						msgTarget: 'side',
						maskRe:		/[0-9]/,
						maxLength	: 8,
						margins		: '0 20 0 0',
						width			: 200,		
						allowBlank	: false,
						tabIndex		:	1
					}
				]
		}]
	};

var gerenciaPanel2 = {
	xtype		: 'panel',
	id 		: 'gerenciaPanel2',
	items		: [{
			layout		: 'form',
			labelWidth	: 195,
			bodyStyle: 	'padding: 2px; padding-right:20px;padding-left:60px;',
			items		:	
			[
				{
					xtype:'displayfield',
					border: false,
					align: 'left',
					value:'Gerente Operaciones de Cr�dito 2'
				},
				{
					xtype				: 'combo',
					id					: 'cmbConcentrador2',
					hiddenName 		: 'cmbConcentrador2Hidden',
					fieldLabel		: 'Usuario Concentrador 2',
					emptyText		: 'Seleccione...',
					width				: 330,
					forceSelection	: true,
					msgTarget		: 'side',
					allowBlank		: false,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					margins			: '0 20 0 0',
					store				: catalogoConcentrador
				},
				{
					xtype			: 'textfield',
					id				: 'tfTelefono',
					name			: 'telefonoName',
					fieldLabel	: 'Tel�fono',
					msgTarget: 'side',
					maskRe:		/[0-9]/,
					maxLength	: 30,
					margins		: '0 20 0 0',
					width			: 200,		
					allowBlank	: false,
					tabIndex		:	1
				}
			]
	}]
};


var direccionAC = {
	xtype		: 'panel',
	id 		: 'direccionAC',
	items		: [{
			layout		: 'form',
			labelWidth	: 195,
			bodyStyle: 	'padding: 2px; padding-right:20px;padding-left:60px;',
			items		:	
			[
				{
					xtype:'displayfield',
					border: false,
					align: 'left',
					value:'Director de Administraci�n Crediticia'
				},
				{
					xtype			: 'textarea',
					id				: 'correoDirector',
					name			: 'correoDirectorName',					
					fieldLabel	: 'Correo Director',
					msgTarget	: 'side',
					maxLength	: 200,
					margins		: '0 20 0 0',
					width			: 500,
					height		: 120,
					allowBlank	: false
				}
			]
	}]
};
	
	var fondoPanel = {
		xtype		: 'panel',
		id 		: '_fondoPanel',
		items		: [{
				layout		: 'form',
				labelWidth	: 195,
				bodyStyle: 	'padding: 2px; padding-right:20px;padding-left:60px;',
				items		:	
				[
					{
						xtype			: 'textarea',
						name			: 'mails_fondo',
						id				: '_mails_fondo',
						fieldLabel	: 'Correos envi� Fondo Liquido (Para)',
						msgTarget: 'side',
						maxLength	: 2000,
						margins		: '0 20 0 0',
						width			: 500,
						height		: 120,
						allowBlank	: false
					}
				]
		}]
	};
	
		var fpDatos = new Ext.form.FormPanel({
		id					: 'forma',
		layout			: 'form',
		width				: 900,
		style				: ' margin:0 auto;',
		frame				: true,
		collapsible		: false,
		titleCollapse	: false,
		title:	'Par�metros Generales',
		bodyStyle		: 'padding: 16px',
		defaults			: { msgTarget: 'side',anchor: '-20' },
		items				: 
		[
			{
				layout	: 'hbox',
				title		: 'Mesa de Control de Cr�dito',
				padding	:	'10 0 0 0',
				width		: 900,
				items		: [mesaPanel ]
			},
			{
				layout	: 'hbox',
				title 	: 'Gerencia de Operaciones de Cr�dito',
				padding	:	'10 0 0 0',
				width		: 900,
				items		: [gerenciaPanel]
			},
			{
				layout	: 'hbox',
				title 	: 'Gerencia de Operaciones de Cr�dito 2',
				padding	:	'10 0 0 0',
				width		: 900,
				items		: [gerenciaPanel2]
			},
			{
				layout	: 'hbox',
				title 	: 'Direcci�n de Administraci�n Crediticia',
				padding	:	'10 0 0 0',
				width		: 900,
				items		: [direccionAC]
			},
			{
				layout	: 'hbox',
				title		: 'Fondo Liquido',
				padding	:	'10 0 0 0',
				width		: 900,
				items		: [fondoPanel ]
			}
		],
		//monitorValid	: true,
		buttons			: [
			{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls:'icoGuardar',			
				handler: function(boton, evento) 
				{
					/********************** Validaciones ********************/ 
					if(Ext.getCmp('cmbConcentrador2').isValid() && Ext.getCmp('tfTelefono').isValid() && Ext.getCmp('correoDirector').isValid()){
					if( Ext.getCmp('_cbo_sub').getValue()=='' || Ext.getCmp('_tel_sub').getValue()=='' || Ext.getCmp('_cbo_con').getValue()==''
					|| Ext.getCmp('_tel_con').getValue()=='' || Ext.getCmp('_mails_fondo').getValue()=='' ){
						if(Ext.getCmp('_cbo_sub').getValue()=='')
							Ext.getCmp('_cbo_sub').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_tel_sub').getValue()=='')
							Ext.getCmp('_tel_sub').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_cbo_con').getValue()=='')
							Ext.getCmp('_cbo_con').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_tel_con').getValue()=='')
							Ext.getCmp('_tel_con').markInvalid("Campo obligatorio");
						else if(Ext.getCmp('_mails_fondo').getValue()=='')
							Ext.getCmp('_mails_fondo').markInvalid("Campo obligatorio");
					   
					}else{
							accionConsulta("ACTUALIZAR", null);
					}
						
					}
				}
			
			},
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				iconCls:'icoCancelar',			
				handler: function(boton, evento) 
				{
					window.location = '/nafin/38OperaElectronica/38nafin/38paramGrales.jsp?idMenu=38REGIS_CORREOS';
				}
			
			}
		]
	});
	
//--------------------------- CONTENEDOR -----------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items:
		[
			NE.util.getEspaciador(10),
			fpDatos
		]
	});
	
	catalogoConcentrador.load();
	registroExistenteData.load();
	
});