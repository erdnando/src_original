<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.sql.*,java.text.*,netropology.utilerias.*,	
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,	
	javax.naming.*,
	com.netro.electronica.*,
	com.netro.model.catalogos.*," 
	errorPage="/00utils/error_extjs.jsp"  
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../38secsession_extjs.jspf" %>  

<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar ="";

OperacionElectronica opeElec = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
if(informacion.equals("CargarDatos")){
	
	JSONObject resultado = new JSONObject();
	
	List consulta = opeElec.cargaNomContratos();
	
	resultado.put("registros", consulta);
	infoRegresar = resultado.toString();
	
}else if(informacion.equals("GuardarDatos")){
	
	JSONObject resultado = new JSONObject();
	String [] a = request.getParameterValues("arr");
	boolean resp= false;
	
	for(int cont=0; cont < a.length; cont++){
				String [] b = a[cont].split("¬");
				String cveCont = b[0];
				String nomCont = b[1];
			
				resp = opeElec.actualizaNombreContratos(cveCont, nomCont);
	}
	
	if(resp)
		resultado.put("success", new Boolean(true));
	infoRegresar = resultado.toString();
	
}

%>
<%=infoRegresar%>