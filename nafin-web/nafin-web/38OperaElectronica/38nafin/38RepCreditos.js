Ext.onReady(function() {

	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//------Consulta ------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
		
			
			if(store.getTotalCount() > 0) {			
				el.unmask();	
				Ext.getCmp('btnArchivoCons').enable();
			} else {		
				Ext.getCmp('btnArchivoCons').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.GroupingStore({	
		root : 'registros',
		url : '38RepCreditos.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},	
		sortInfo: 			{ field: 'NOMBRE_IF', direction: "DESC" },
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [				
				{	name: 'GROUP_ID'},	
				{	name: 'NOMBRE_IF'},	
				{	name: 'NO_PRESTAMO'},	
				{	name: 'FECHA_OPERACION'},	
				{	name: 'MONEDA'},	
				{	name: 'FECHA'},	
				{	name: 'NOMBRE'},	
				{	name: 'NOMBRE2'},	
				{	name: 'APE_PATERNO'},	
				{	name: 'APE_MATERNO'},	
				{	name: 'RFC'},	
				{	name: 'IMPORTE'},	
				{	name: 'PRODUCTO'},	
				{	name: 'ACT_ECONOMICA'},	
				{	name: 'ESTADO'},	
				{	name: 'MUNICIPIO'},	
				{	name: 'ESTRATO'},	
				{	name: 'VENTAS'},	
				{	name: 'NO_EMPLEADOS'}
			]
		}),
		groupField: 'GROUP_ID',
		sortInfo:{field: 'GROUP_ID'},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});	
			
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [
		{
				header: 		'GROUP ID',
				tooltip: 	'GROUP ID',
				dataIndex: 	'GROUP_ID', // IC_IF + ":" + IC_MONEDA 
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				//width: 		315,
				hidden: 		true,
				hideable:	false,
				groupRenderer: function(value,unused,record,rowIndex,colIndex,dsStore){
					return  record.json['NOMBRE_IF'] + "&nbsp;&nbsp;No. de Pr�stamo: " + record.json['NO_PRESTAMO'] +"&nbsp;";
				}
			},
			
			{
				header: 'NOMBRE_IF',
				tooltip: 'NOMBRE_IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha de Operaci�n',
				tooltip: 'Fecha de Operaci�n',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Moneda ',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha ',
				tooltip: 'Fecha',
				dataIndex: 'FECHA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Nombre',
				tooltip: 'Nombre',
				dataIndex: 'NOMBRE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Nombre 2',
				tooltip: 'Nombre 2',
				dataIndex: 'NOMBRE2',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Apellido Parterno',
				tooltip: 'Apellido Parterno',
				dataIndex: 'APE_PATERNO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Apellido Materno',
				tooltip: 'Apellido Materno',
				dataIndex: 'APE_MATERNO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'RFC',
				tooltip: 'RFC',
				dataIndex: 'RFC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Importe',
				tooltip: 'Importe',
				dataIndex: 'IMPORTE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Producto',
				tooltip: 'Producto',
				dataIndex: 'PRODUCTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Actividad Econ�mica',
				tooltip: 'Actividad Econ�mica',
				dataIndex: 'ACT_ECONOMICA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},		
			{
				header: 'Estado',
				tooltip: 'Estado',
				dataIndex: 'ESTADO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Municipio',
				tooltip: 'Municipio',
				dataIndex: 'MUNICIPIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Estrato',
				tooltip: 'Estrato',
				dataIndex: 'ESTRATO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Ventas',
				tooltip: 'Ventas',
				dataIndex: 'VENTAS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. de Empleados',
				tooltip: 'No. de Empleados',
				dataIndex: 'NO_EMPLEADOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,	
		view:		 		new Ext.grid.GroupingView(
			{
				forceFit:				false, 
				groupTextTpl: 			'{group}', 
				hideGroupedColumn: 	true,
				enableGroupingMenu:	false
			}),
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Descargar Archivo',					
					tooltip:	'Descargar Archivo ',
					iconCls: 'icoXls',
					id: 'btnArchivoCons',
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '38RepCreditos.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSVCons'
							}),
							callback: procesarDescargaArchivos
						});
						
					}
				}
			]
		}
	});
		
			
//*************** componentes de la Forma********************

	var catIFData = new Ext.data.JsonStore({
		id: 'catIFData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '38RepCreditos.data.jsp',
		baseParams: {
			informacion: 'catIFData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var  elementosForma  = [
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'ic_if1',
			fieldLabel: 'Intermediario',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_if',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catIFData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'No. Pr�stamo',
			name: 'no_Prestamo',
			id: 'no_Prestamo1',
			allowBlank: true,
			maxLength: 12,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaOperacionIni',
					id: 'fechaOperacionIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'fechaOperacionFin',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaOperacionFin',
					id: 'fechaOperacionFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoInicioFecha: 'fechaOperacionIni',
					margins: '0 20 0 0'  
				}
			]
		}
	];
	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		monitorValid: true,
		title: 'Reporte de Pr�stamos',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {

					var fechaOperacionIni = Ext.getCmp('fechaOperacionIni');
					var fechaOperacionFin = Ext.getCmp('fechaOperacionFin');
					
					if(!Ext.isEmpty(fechaOperacionIni.getValue()) || !Ext.isEmpty(fechaOperacionFin.getValue())){
						if(Ext.isEmpty(fechaOperacionIni.getValue())){
							fechaOperacionIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaOperacionIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaOperacionFin.getValue())){
							fechaOperacionFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaOperacionFin.focus();
							return;
						}
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{  
							informacion: 'Consultar'						
						})
					});
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '38RepCreditos.jsp';					
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});
	
	catIFData.load();

});