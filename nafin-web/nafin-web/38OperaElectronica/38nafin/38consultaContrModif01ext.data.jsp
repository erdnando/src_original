<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.text.*,netropology.utilerias.*,
		com.netro.cadenas.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoIfFondoLiquido,
		com.netro.electronica.*,
		netropology.utilerias.usuarios.*,
		com.netro.pdf.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/38OperaElectronica/38secsession_extjs.jspf" %>

<%
JSONObject jsonObj = new JSONObject();
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String infoRegresar	=	"";

UtilUsr utilUsr = new UtilUsr();

if(informacion.equals("CatalogoIF")){
	
	List lista = new ArrayList();
	lista.add("NB");
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_if");
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setCampoLlave("cs_tipo");
	cat.setValoresCondicionIn(lista);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("Consulta")||informacion.equals("ArchivoPaginaPDF")||informacion.equals("ArchivoXLS")){
	
	String cveIf = request.getParameter("cboIf")==null?"":request.getParameter("cboIf");
	String fmFechaCargaIni = request.getParameter("fmFechaCargaIni")==null?"":request.getParameter("fmFechaCargaIni");
	String fmFechaCargaFin = request.getParameter("fmFechaCargaFin")==null?"":request.getParameter("fmFechaCargaFin");
	String fmFechaCargaOpeIni = request.getParameter("fmFechaCargaOpeIni")==null?"":request.getParameter("fmFechaCargaOpeIni");
	String fmFechaCargaOpeFin = request.getParameter("fmFechaCargaOpeFin")==null?"":request.getParameter("fmFechaCargaOpeFin");
	
	int start=0,limit=0;
	Cons_Cont_Modif paginador=new Cons_Cont_Modif();
	paginador.setCveIF(cveIf);
	paginador.setFechaCargaIni(fmFechaCargaIni);
	paginador.setFechaCargaFin(fmFechaCargaFin);
	paginador.setFechaCargaOpeIni(fmFechaCargaOpeIni);
	paginador.setFechaCargaOpeFin(fmFechaCargaOpeFin);

        	
	
	CQueryHelperRegExtJS cqhelper = new CQueryHelperRegExtJS(paginador);
	try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));	
		}catch(Exception e){
				System.out.println("Error en parametros");
		}
	if (operacion.equals("Generar")) {	//Nueva consulta
			
			cqhelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
	}
	if (informacion.equals("Consulta")){
		String  consultar = cqhelper.getJSONPageResultSet(request,start,limit);	
		jsonObj = JSONObject.fromObject(consultar);
		infoRegresar=jsonObj.toString();
	}else	 if(informacion.equals("ArchivoPaginaPDF")){
		try{
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
			String nombreArchivo = cqhelper.getCreatePageCustomFile(request, start, limit,strDirectorioTemp, "PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}catch(Throwable e){
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}else if(informacion.equals("ArchivoXLS")){
		
		String nombreArchivo = cqhelper.getCreateCustomFile(request,strDirectorioTemp,"XLS");
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
		infoRegresar = jsonObj.toString();
	}


}else if (informacion.equals("Totales")){
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesión
		infoRegresar = queryHelper.getJSONResultCount(request);
} 


%>

<%=infoRegresar%>