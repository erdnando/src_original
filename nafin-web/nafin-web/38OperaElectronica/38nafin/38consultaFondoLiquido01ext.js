Ext.onReady(function(){
	var tipoUsr = Ext.getDom('strTipoUsuario').value;
	tipoUsr = (tipoUsr=='NAFIN')?false:true;
//----------------------------------------------------------------------
	
	var procesarConsultaData = function(store,arrRegistros,opts){
		if(arrRegistros!=null){
			gridConsFL.show();			
		var el = gridConsFL.getGridEl();
		if(store.getTotalCount()>0){
			el.unmask();
			//consultaTotales.load();
			Ext.getCmp('btnGenerarArchivoXLS').enable();
		}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
				gridTotales.hide();
				Ext.getCmp('btnGenerarArchivoXLS').disable();
			}
		}
	}
	
	var procesarSuccessFailureGenerarArchivoTodas =  function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			var archivo = resp.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fpArchivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fpArchivo.getForm().getEl().dom.submit();

		} else{
			NE.util.mostrarConnError(response,opts);
		}

            Ext.getCmp('btnGenerarArchivoXLS').setIconClass('icoXls');
            Ext.getCmp('btnGenerarArchivoXLS').enable();

	}

function procesarConsultaTotal(store,arrRegistros,opts) {
	if(arrRegistros!=null){
		gridTotales.show();
		var el = gridTotales.getGridEl();
		if(store.getTotalCount()>0){
			if(arrRegistros==''){
				//consultaTotales.load();
			}
			gridTotales.el.unmask();
		}else{		
				gridTotales.el.mask('No se cargaron los totales', 'x-mask');
		}
	}
}

//-----------------STORES CATALOGOS---------------------------------------------

	var storeCatIfData = new Ext.data.JsonStore
	({
		id: 'storeCatIfData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '38consultaFondoLiquido01ext.data.jsp',
		baseParams: 
		{
			informacion: 'catologoIfFondoLiquido'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners:
		{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '38consultaFondoLiquido01ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'NUMCONTRATO'},
				{name: 'NOMBREINTER'},
				{name: 'CVESIRAC'},
				{name: 'SALDOHOY'},
				{name: 'NOMBREMONEDA'},
				{name: 'FECHACARGA'},
				{name: 'NUMUSUARIO'},
				{name: 'NOMBREUSUARIO'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
						procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var consultaTotales = new Ext.data.JsonStore({
		root: 'registros',
		url: '38consultaFondoLiquido01ext.data.jsp',
		baseParams: {
			informacion: 'Totales'
		},
		fields: [
					{name: 'REGTOTAL'},
					{name: 'SALDOTOTAL'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotal,
			exception: {
						fn: function(proxy,type,action,optionsRequest,response,args){
								NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
								procesarConsultaTotal(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
						}
			}
		}
	});

//------------------------------------------------------------------------------
elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Carga',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '90%',
			items: [
				{
					// Fecha Inicio
					xtype: 'datefield',
					name: 'fmFechaCargaIni',
					id: 'fmFechaCargaIni',
					vtype: 'rangofecha',
					campoFinFecha: 'fmFechaCargaFin',
					allowBlank: true,
					msgTarget: 'side',
					width: 100,
					startDay: 0,
					width: 100,
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'hasta',
					width: 50
				},
				{
					// Fecha Final
					xtype: 'datefield',
					name: 'fmFechaCargaFin',
					id: 'fmFechaCargaFin',
					vtype: 'rangofecha',
					campoInicioFecha: 'fmFechaCargaIni',
					allowBlank: true,
					msgTarget: 'side',
					width: 100,
					startDay: 0,
					width: 100,
					margins: '0 20 0 0'
				}
			]
		},
		{
			//MONEDA
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			emptyText: 'Seleccionar',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			allowBlank: true,
			store: storeCatIfData,
			tpl: NE.util.templateMensajeCargaCombo,
			name:'cboIf',
			id: 'cboIf_',
			mode: 'local',
			hiddenName: 'cboIf',
			forceSelection: true,
			hidden: tipoUsr,
			anchor: '90%'
		}
		
	]

	var fpCritBusq = new Ext.form.FormPanel({
		width:            600,
		labelWidth:       170,
		id:               'fpCritBusq',
		height:           'auto',
		title:            'Consulta de Fondo L�quido',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		defaultType:      'textfield',
		trackResetOnLoad: true,
		frame:            true,
		monitorValid:     false,
		hidden:           false,
		defaults: 
		{
			msgTarget:    'side',
			anchor:       '-20'
		},
		items:            elementosForma,
		buttons:[{
			//Bot�n BUSCAR
			xtype:    'button',
			text:     'Buscar',
			name:     'btnBuscar',
			iconCls:  'icoBuscar',
			hidden:   false,
			formBind: true,
			handler:  function(boton, evento){
				var fecIni = Ext.getCmp('fmFechaCargaIni');
				var fecFin = Ext.getCmp('fmFechaCargaFin');

				if(fecIni.getValue()!='' && fecFin.getValue()==''){
					fecFin.markInvalid('Indicar Fecha final de Solicitud');
					return;
				}else if(fecFin.getValue()!='' && fecIni.getValue()==''){
					fecIni.markInvalid('Indicar Fecha inicial de Solicitud');
					return;
				}

				consulta.load({ params: Ext.apply(fpCritBusq.getForm().getValues(),{
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15})
				});
			}
		},{
			text:    'Limpiar',
			iconCls: 'icoLimpiar',
			handler: function() {
				location.reload();
			}
		}]
	});
	
	var gridConsFL = new Ext.grid.GridPanel({
				id: 'gridConsFL',
				hidden: true,
				header:true,
				title: ' ',
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						header:'<center>N�mero de Contrato</center>',
						tooltip: 'N�mero de Contrato',
						sortable: true,
						dataIndex: 'NUMCONTRATO',
						width: 130,
						align: 'center'
						},
						{
						header:'<center>Nombre del Intermediario</center>',
						tooltip: 'Nombre del Intermediario',
						sortable: true,
						dataIndex: 'NOMBREINTER',
						hidden: tipoUsr,
						width: 130,
						align: 'center'
						},
						{
						header:'<center>Clave SIRAC</center>',
						tooltip: 'Clave SIRAC',
						sortable: true,
						dataIndex: 'CVESIRAC',
						hidden: tipoUsr,
						width: 130,
						align: 'center'
						},
						{
						header:'<center>Saldo</center>',
						tooltip: 'Saldo',
						sortable: true,
						dataIndex: 'SALDOHOY',
						width: 130,
						align: 'center',
						renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
						},
						{
						header:'<center>Moneda</center>',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},
						{
						header:'<center>Fecha y Hora de Carga</center>',
						tooltip: 'Fecha y Hora de Carga',
						sortable: true,
						dataIndex: 'FECHACARGA',
						width: 130,
						align: 'center'
						},
						{
						header:'<center>Usuario de Carga</center>',
						tooltip: 'Usuario de Carga',
						sortable: true,
						dataIndex: 'NOMBREUSUARIO',
						hidden: tipoUsr,
						width: 130,
						align: 'center',
						renderer:  function (causa, columna, registro){
							columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
							return registro.data['NUMUSUARIO']+' - '+causa;
						}
						}
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
					xtype: 'paging',
					autoScroll:true,
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consulta,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					items: ['->','-',
								{
									xtype: 'button',
									text: 'Descargar Archivo',
									id: 'btnGenerarArchivoXLS',
									iconCls:'icoXls',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '38consultaFondoLiquido01ext.data.jsp',
											params: Ext.apply(fpCritBusq.getForm().getValues(),{
												informacion: 'ArchivoXLS'}),
											callback: procesarSuccessFailureGenerarArchivoTodas
										});
									}
								}
							]
				}
		});

	var gridTotales = new Ext.grid.EditorGridPanel({
		store: consultaTotales,
		title: '<center>Totales de Saldos</center>',
		id: 'gridTotales',
		hidden:	true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: '<center>N�mero de Saldos</center>',
				dataIndex: 'REGTOTAL',
				align: 'center',	width: 150
			},{
				header: '<center>Total Monto</center>',
				dataIndex: 'SALDOTOTAL',
				width: 200,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
		],
		width: 360,
		height: 100
		//frame: false
	}); 
	

	var fpArchivo = new Ext.form.FormPanel({
		id: 'fpArchivo1',
		width: 600,
		title: '',
		frame: true,
		hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;'
	});

//------------------Principal-------------------
	var pnl = new Ext.Container
	({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
		 [ fpArchivo,
			fpCritBusq,
			NE.util.getEspaciador(30),
			gridConsFL,
			NE.util.getEspaciador(10),
			gridTotales
		 
		 ]
	});
  
  
});