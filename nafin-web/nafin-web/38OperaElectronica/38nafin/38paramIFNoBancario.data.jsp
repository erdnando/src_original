<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.sql.*,
	com.netro.seguridadbean.*,
	com.netro.electronica.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.electronica.*,
	netropology.utilerias.*,
	com.netro.seguridad.*, 
        org.apache.commons.logging.Log,
	javax.naming.*,
	netropology.utilerias.usuarios.*,
	 com.netro.cotizador.*,
	 com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"  
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../38secsession_extjs.jspf"   %>
 <%!
 private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
 %>
<%

String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar ="";
log.info("informacion: "+informacion);
OperacionElectronica opeElec = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);

if(informacion.equals("CatalogoIF")){
	
	List lista = new ArrayList();
	lista.add("NB");
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_if");
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setCampoLlave("cs_tipo");
	cat.setValoresCondicionIn(lista);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}else if(informacion.equals("CatalogoPromo")){
	JSONArray jsObjArray = new JSONArray();
   List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
   List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("EJE PROMO", "N");
                
   if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
			hmData.put("clave", loginUsuarioOPOP/* +" - "+ nombre*/);
			hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
		                                        
			lstCatalogoEjecOP.add(hmData);
		}
   }
                
                
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
		
}else if(informacion.equals("catalogoSupervisor")){
	JSONArray jsObjArray = new JSONArray();
   List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
   List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("SUP SEG", "N");
                
   if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoPaterno()+" "+usuarioEpo.getApellidoMaterno();
			hmData.put("clave", loginUsuarioOPOP/* +" - "+ nombre*/);
			hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
		                                        
			lstCatalogoEjecOP.add(hmData);
		}
   }
                
                
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
		
} else if(informacion.equals("catalogoIFOperador")){
	
	JSONArray jsObjArray = new JSONArray();
   List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
	
	String[] listaIF	  = request.getParameterValues("listaIF");	
	List listsIF 	= Arrays.asList(listaIF);
	String operador = (request.getParameter("operador") != null) ? request.getParameter("operador") : "";
	String autorizadorUno = (request.getParameter("autorizadorUno") != null) ? request.getParameter("autorizadorUno") : "";

		ArrayList 	listaUsuarios 	= new ArrayList();
		UtilUsr 		oUtilUsr 		= new UtilUsr();
		Iterator itIFS = listsIF.iterator();
		String miPerfil="";

		while(itIFS.hasNext()){
			List cuentas = utilUsr.getUsuariosxAfiliado( (String)itIFS.next(), "I");			
			Iterator itCuentas = cuentas.iterator();
			while (itCuentas.hasNext()) {
				netropology.utilerias.usuarios.Usuario oUsuario = null;
				String login = (String) itCuentas.next();
				oUsuario = oUtilUsr.getUsuario(login);
				if("IF LI".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 4CP".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 5CP".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 4MIC".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				} else if("IF 5MIC".equals(oUsuario.getPerfil())) {		
					listaUsuarios.add(oUsuario.getLogin());
				}
			}
		}
		
   List usuariosLI = listaUsuarios;
	
	List listaAux =null;
	
		listaAux= listaUsuarios;
		if(listaAux!=null && listaAux.size()>0){
			HashMap hmData = null;
			for(int i=0;i<listaAux.size();i++){
				hmData = new HashMap();
				String loginUsuarioOPOP = (String)listaAux.get(i);
				Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
				String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno();
				if(operador.equals("")){
					hmData.put("clave", loginUsuarioOPOP);
					hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
					lstCatalogoEjecOP.add(hmData);
				} else {
					if(autorizadorUno.equals("")){
						if(loginUsuarioOPOP.equals(operador)){
						} else {
							hmData.put("clave", loginUsuarioOPOP);
							hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
							lstCatalogoEjecOP.add(hmData);
						}
					} else {
						if(loginUsuarioOPOP.equals(operador) || loginUsuarioOPOP.equals(autorizadorUno)){
						} else {
							hmData.put("clave", loginUsuarioOPOP);
							hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
							lstCatalogoEjecOP.add(hmData);
						}
					}
				}
			}
		}
	 
    if(!autorizadorUno.equals("")){
		HashMap hmData = new HashMap();
		hmData.put("clave", "0");
		hmData.put("descripcion", "No Aplica");
		lstCatalogoEjecOP.add(hmData);
	 }  
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";
	 
} else if(informacion.equals("CargarDatos")){
	
	
	String consulta = "";
	JSONObject resultado = new JSONObject();

	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	String cmp = (request.getParameter("cmp") != null) ? request.getParameter("cmp") : "";
	
	CargaIFOE  carga = new CargaIFOE();
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( carga );
	
	carga.setCve_if(cve_if);
	carga.setValida(cmp);
	
	try {
		Registros reg	=	queryHelper.doSearch();
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		 
	}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
	}
	
	resultado = JSONObject.fromObject(consulta);
	infoRegresar = resultado.toString();
	
} else if(informacion.equals("cargarContrato")){
	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";	
	JSONObject jsonObj = new JSONObject();
	
	List consulta = opeElec.getContrato(cve_if);	
	if(!consulta.isEmpty()){
            HashMap registros = (HashMap)consulta.get(0);
            jsonObj.put("NUM_CONT",registros.get("NUM_CONT"));
            jsonObj.put("NOM_CONT",registros.get("NOM_CONT"));
            jsonObj.put("FECHA_FIR_CONT",registros.get("FECHA_FIR_CONT"));
            jsonObj.put("FECHA_FIR_OPE_ELE",registros.get("FECHA_FIR_OPE_ELE"));
            jsonObj.put("TOT_MODIFI",registros.get("TOT_MODIFI"));
            jsonObj.put("IC_IF",registros.get("IC_IF"));
        }
        jsonObj.put("success",  new Boolean(true)); 
        infoRegresar = jsonObj.toString();

}else if(informacion.equals("GuardarDatos") || informacion.equals("ActualizaDatos")){
	
	JSONObject resultado = new JSONObject();
	boolean resp = false;
	String numContrato = (request.getParameter("numContrato") != null) ? request.getParameter("numContrato") : "";
        String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	String cuenta = (request.getParameter("num_cuenta") != null) ? request.getParameter("num_cuenta") : "";
	String banco = (request.getParameter("banco") != null) ? request.getParameter("banco") : "";
	String clabe = (request.getParameter("cu_clabe") != null) ? request.getParameter("cu_clabe") : "";
	String fec_fir_cont = (request.getParameter("fec_firma") != null) ? request.getParameter("fec_firma") : "";
	String cont_modif = (request.getParameter("con_modif") != null) ? request.getParameter("con_modif") : "";
	String nom_cont = (request.getParameter("nom_contrato") != null) ? request.getParameter("nom_contrato") : "";
	String fec_fir_ope = (request.getParameter("fec_ope_elec") != null) ? request.getParameter("fec_ope_elec") : "";
	String usr_promo = (request.getParameter("cbo_user_promo") != null) ? request.getParameter("cbo_user_promo") : "";
	String tel_promo = (request.getParameter("num_telefono_promo") != null) ? request.getParameter("num_telefono_promo") : "";
	String usr_super = (request.getParameter("cbo_user_super") != null) ? request.getParameter("cbo_user_super") : "";
	String tel_super = (request.getParameter("num_telefono_super") != null) ? request.getParameter("num_telefono_super") : "";	
	String hr_envio = (request.getParameter("hr_envio") != null) ? request.getParameter("hr_envio") : "";
	String hr_reenvio = (request.getParameter("hr_reenvio") != null) ? request.getParameter("hr_reenvio") : "";
	String valida = (request.getParameter("valida") != null) ? request.getParameter("valida") : "";
	String [] a = request.getParameterValues("arr");
	String operaFirmaMancomunada = (request.getParameter("rbGroup") != null) ? request.getParameter("rbGroup") : "";
	String ifOperador = (request.getParameter("cmbIFOperadoHidden") != null) ? request.getParameter("cmbIFOperadoHidden") : "";
	String autorizadorUno = (request.getParameter("cmbIFAutorizadorUnoHidden") != null) ? request.getParameter("cmbIFAutorizadorUnoHidden") : "";
	String autorizadorDos = (request.getParameter("cmbIFautorizadorDosHidden") != null) ? request.getParameter("cmbIFautorizadorDosHidden") : "";
	String numAutorizadores ="";
	String empresarias = (request.getParameter("empresarias") == null) ? "" : request.getParameter("empresarias");
	
	if ( empresarias.equals("on") ){
		empresarias = "S";
	}else{
		empresarias = "N";
	}
	if(autorizadorDos.equals("0")){autorizadorDos=""; numAutorizadores="1";}
	if(autorizadorDos.equals("")){numAutorizadores="1";}
	if(!autorizadorDos.equals("") && !autorizadorDos.equals("0")){numAutorizadores="2";}
	

	if(operaFirmaMancomunada.equals("N")){
		ifOperador="";
		autorizadorUno="";
		autorizadorDos="";
	}
	if(cont_modif.equals(""))
		cont_modif = "0";
	
        if(informacion.equals("GuardarDatos")){
		resp = opeElec.guardaDatosIF( cve_if,  cuenta,  banco,  clabe,  fec_fir_cont, cont_modif,  nom_cont,  fec_fir_ope,  usr_promo,  tel_promo, usr_super,  tel_super,  hr_envio, hr_reenvio, operaFirmaMancomunada, ifOperador, autorizadorUno, autorizadorDos,empresarias  );
		
	}else if(informacion.equals("ActualizaDatos")){
		resp = opeElec.actualizaDatosIF( cve_if,  cuenta,  banco,  clabe,  fec_fir_cont, cont_modif,  nom_cont,  fec_fir_ope,  usr_promo,  tel_promo, usr_super,  tel_super,  hr_envio, hr_reenvio, operaFirmaMancomunada, ifOperador, autorizadorUno, autorizadorDos,empresarias );
	}


        ArrayList<String> arrListModicatotios = new ArrayList<String>();
        for(int cont=0; !cont_modif.equals("0") && cont < a.length; cont++){
            System.out.println(a[cont]);
            arrListModicatotios.add(a[cont]);
        }
        String strContrato=opeElec.guardaContratoIF(numContrato, cve_if, nom_cont, fec_fir_cont, fec_fir_ope);
        opeElec.borraModificatorios(strContrato);
        for (String tmpModif : arrListModicatotios) {
            StringTokenizer st = new StringTokenizer(tmpModif, ",");
            if (st.hasMoreElements()) {
                String desc = st.nextElement().toString();
                String fecMod = st.nextElement().toString();
                opeElec.guardaModificatorioIF(strContrato, desc, fec_fir_cont, fecMod);
            }
        }
	
	if(resp){
		if(valida.equals("S")){
			for(int cont=0; cont < a.length; cont++){
				String [] b = a[cont].split(",");
				String desc = b[0];
				String fecMod = b[1];
		
				opeElec.guardaFecMod(cve_if, nom_cont, fecMod);
                                //opeElec.guardaFecMod(cve_if, desc, fecMod);
			}
		}
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();
	}
	
}else if(informacion.equals("altaContrato")){
	
	JSONObject resultado = new JSONObject();
	boolean resp = false;
        String numContrato = (request.getParameter("numContrato") != null) ? request.getParameter("numContrato") : "";
        String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	String cuenta = (request.getParameter("num_cuenta") != null) ? request.getParameter("num_cuenta") : "";
	String banco = (request.getParameter("banco") != null) ? request.getParameter("banco") : "";
	String clabe = (request.getParameter("cu_clabe") != null) ? request.getParameter("cu_clabe") : "";
	String fec_fir_cont = (request.getParameter("fec_firma") != null) ? request.getParameter("fec_firma") : "";
	String cont_modif = (request.getParameter("con_modif") != null) ? request.getParameter("con_modif") : "";
	String nom_cont = (request.getParameter("nom_contrato") != null) ? request.getParameter("nom_contrato") : "";
	String fec_fir_ope = (request.getParameter("fec_ope_elec") != null) ? request.getParameter("fec_ope_elec") : "";
	String usr_promo = (request.getParameter("cbo_user_promo") != null) ? request.getParameter("cbo_user_promo") : "";
	String tel_promo = (request.getParameter("num_telefono_promo") != null) ? request.getParameter("num_telefono_promo") : "";
	String usr_super = (request.getParameter("cbo_user_super") != null) ? request.getParameter("cbo_user_super") : "";
	String tel_super = (request.getParameter("num_telefono_super") != null) ? request.getParameter("num_telefono_super") : "";	
	String hr_envio = (request.getParameter("hr_envio") != null) ? request.getParameter("hr_envio") : "";
	String hr_reenvio = (request.getParameter("hr_reenvio") != null) ? request.getParameter("hr_reenvio") : "";
	String valida = (request.getParameter("valida") != null) ? request.getParameter("valida") : "";
	String [] arrModif = request.getParameterValues("arr");
	String operaFirmaMancomunada = (request.getParameter("rbGroup") != null) ? request.getParameter("rbGroup") : "";
	String ifOperador = (request.getParameter("cmbIFOperadoHidden") != null) ? request.getParameter("cmbIFOperadoHidden") : "";
	String autorizadorUno = (request.getParameter("cmbIFAutorizadorUnoHidden") != null) ? request.getParameter("cmbIFAutorizadorUnoHidden") : "";
	String autorizadorDos = (request.getParameter("cmbIFautorizadorDosHidden") != null) ? request.getParameter("cmbIFautorizadorDosHidden") : "";
	String numAutorizadores ="";
	String empresarias = (request.getParameter("empresarias") == null) ? "" : request.getParameter("empresarias");
	
	/*if ( empresarias.equals("on") ){
		empresarias = "S";
	}else{
		empresarias = "N";
	}
	if(autorizadorDos.equals("0")){autorizadorDos=""; numAutorizadores="1";}
	if(autorizadorDos.equals("")){numAutorizadores="1";}
	if(!autorizadorDos.equals("") && !autorizadorDos.equals("0")){numAutorizadores="2";}
	

	if(operaFirmaMancomunada.equals("N")){
		ifOperador="";
		autorizadorUno="";
		autorizadorDos="";
	}
	if(cont_modif.equals(""))
		cont_modif = "0";*/
	
        
        /* Este bloque se debe eliminar
        ArrayList<String> arrListModicatotios = new ArrayList<String>();
        for(int cont=0; !cont_modif.equals("0") && cont < arrModif.length; cont++){
            System.out.println(arrModif[cont]);
            arrListModicatotios.add(arrModif[cont]);
        }
        String strContrato=opeElec.guardaContratoIF(numContrato, cve_if, nom_cont, fec_fir_cont, fec_fir_ope);
        opeElec.borraModificatorios(strContrato);
        for (String tmpModif : arrListModicatotios) {
            StringTokenizer st = new StringTokenizer(tmpModif, ",");
            if (st.hasMoreElements()) {
                String desc = st.nextElement().toString();
                String fecMod = st.nextElement().toString();
                opeElec.guardaModificatorioIF(strContrato, desc, fec_fir_cont, fecMod);
            }
        }
        Este bloque se debe eliminar */

	
        
        resultado.put("numContrato", numContrato);
        resultado.put("cve_if", cve_if);
        resultado.put("success", new Boolean(true));
        infoRegresar = resultado.toString();

	
}else if(informacion.equals("actualizaContrato")){
	
	JSONObject resultado = new JSONObject();
	boolean resp = false;
	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	String cuenta = (request.getParameter("num_cuenta") != null) ? request.getParameter("num_cuenta") : "";
	String banco = (request.getParameter("banco") != null) ? request.getParameter("banco") : "";
	String clabe = (request.getParameter("cu_clabe") != null) ? request.getParameter("cu_clabe") : "";
	String fec_fir_cont = (request.getParameter("fec_firma") != null) ? request.getParameter("fec_firma") : "";
	String cont_modif = (request.getParameter("con_modif") != null) ? request.getParameter("con_modif") : "";
	String nom_cont = (request.getParameter("nom_contrato") != null) ? request.getParameter("nom_contrato") : "";
	String fec_fir_ope = (request.getParameter("fec_ope_elec") != null) ? request.getParameter("fec_ope_elec") : "";
	String usr_promo = (request.getParameter("cbo_user_promo") != null) ? request.getParameter("cbo_user_promo") : "";
	String tel_promo = (request.getParameter("num_telefono_promo") != null) ? request.getParameter("num_telefono_promo") : "";
	String usr_super = (request.getParameter("cbo_user_super") != null) ? request.getParameter("cbo_user_super") : "";
	String tel_super = (request.getParameter("num_telefono_super") != null) ? request.getParameter("num_telefono_super") : "";	
	String hr_envio = (request.getParameter("hr_envio") != null) ? request.getParameter("hr_envio") : "";
	String hr_reenvio = (request.getParameter("hr_reenvio") != null) ? request.getParameter("hr_reenvio") : "";
	String valida = (request.getParameter("valida") != null) ? request.getParameter("valida") : "";
	String [] arrModif = request.getParameterValues("arr");
	String operaFirmaMancomunada = (request.getParameter("rbGroup") != null) ? request.getParameter("rbGroup") : "";
	String ifOperador = (request.getParameter("cmbIFOperadoHidden") != null) ? request.getParameter("cmbIFOperadoHidden") : "";
	String autorizadorUno = (request.getParameter("cmbIFAutorizadorUnoHidden") != null) ? request.getParameter("cmbIFAutorizadorUnoHidden") : "";
	String autorizadorDos = (request.getParameter("cmbIFautorizadorDosHidden") != null) ? request.getParameter("cmbIFautorizadorDosHidden") : "";
	String numAutorizadores ="";
	String empresarias = (request.getParameter("empresarias") == null) ? "" : request.getParameter("empresarias");
	
	/*if ( empresarias.equals("on") ){
		empresarias = "S";
	}else{
		empresarias = "N";
	}
	if(autorizadorDos.equals("0")){autorizadorDos=""; numAutorizadores="1";}
	if(autorizadorDos.equals("")){numAutorizadores="1";}
	if(!autorizadorDos.equals("") && !autorizadorDos.equals("0")){numAutorizadores="2";}
	

	if(operaFirmaMancomunada.equals("N")){
		ifOperador="";
		autorizadorUno="";
		autorizadorDos="";
	}
	if(cont_modif.equals(""))
		cont_modif = "0";*/
	
        ArrayList<String> arrListModicatotios = new ArrayList<String>();
        for(int cont=0; !cont_modif.equals("0") && cont < arrModif.length; cont++){
            arrListModicatotios.add(arrModif[cont]);
        }
        //opeElec.guardaContratoIF("1", cve_if, nom_cont, fec_fir_cont, fec_fir_ope, arrListModicatotios);
	
        
        resultado.put("cve_if", cve_if);
        resultado.put("success", new Boolean(true));
        infoRegresar = resultado.toString();

	
}else if(informacion.equals("Modificatorios")){
	
	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
        int num_contratos_mod = Integer.parseInt((request.getParameter("num_contratos_mod") != null) ? request.getParameter("num_contratos_mod") : "");
	JSONArray a= new JSONArray();
	
	List consulta = opeElec.cargaModModificatorios(cve_if, 0, 0);
        if(!consulta.isEmpty()){
            
        }
        for (int j=0; j<num_contratos_mod; j++){
		JSONObject jo=new JSONObject();
		jo.put("DESCRIPCION","Fecha Firma Modificatorio "+ (j+1));
		jo.put("FECHAMOD","");
		jo.put("EDO","N");
		a.add(jo);
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("total",  Integer.toString(a.size()));
	jsonObj.put("registros", a.toString());
	infoRegresar=jsonObj.toString();
	
}else if(informacion.equals("CargaContratos")){
	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	int nuevas = Integer.parseInt((request.getParameter("filasNew") != null) ? request.getParameter("filasNew") : "");
	int inicio = Integer.parseInt((request.getParameter("inicio") != null) ? request.getParameter("inicio") : "");
	
	JSONObject jsonObj = new JSONObject();
	
	List consulta = opeElec.cargaContratos(cve_if, nuevas, inicio);
	int totalContrato=consulta.size();
        
        jsonObj.put("registrosTotal", totalContrato);
        jsonObj.put("registros", consulta);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();	
	
	
}
/*else if(informacion.equals("CargaFecMod")){
	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	int nuevas = Integer.parseInt((request.getParameter("filasNew") != null) ? request.getParameter("filasNew") : "");
	int inicio = Integer.parseInt((request.getParameter("inicio") != null) ? request.getParameter("inicio") : "");
	
	JSONObject jsonObj = new JSONObject();
	
	List consulta = opeElec.cargaFecMod(cve_if, nuevas, inicio);
	
	jsonObj.put("registros", consulta);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();	
	
	
}*/

else if(informacion.equals("cargaModificatorios")){
	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	int nuevas = Integer.parseInt((request.getParameter("filasNew") != null) ? request.getParameter("filasNew") : "");
	int inicio = Integer.parseInt((request.getParameter("inicio") != null) ? request.getParameter("inicio") : "");
	
	JSONObject jsonObj = new JSONObject();
	
	List consulta = opeElec.cargaModModificatorios(cve_if, nuevas, inicio);
	
	jsonObj.put("registros", consulta);
	jsonObj.put("success",  new Boolean(true)); 
   infoRegresar = jsonObj.toString();	
	
	
}
else if(informacion.equals("TotalModificatorios")){
	String cve_if = (request.getParameter("cbo_if") != null) ? request.getParameter("cbo_if") : "";
	String num="";
	
	num = opeElec.getModificatorios(cve_if);
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("_totMod", num);
	infoRegresar=jsonObj.toString();
	
}
else if(informacion.equals("GuardaContrato")){
	String ic_if = request.getParameter("icif");
        String cg_contrato_oe = request.getParameter("nombre");
        String df_fecha_firma = request.getParameter("fechafirma");
        String ic_contrato = request.getParameter("numeroic").equals("") ? null :  request.getParameter("numeroic");
        String df_firma_oe =    request.getParameter("fechaoe");
        log.info(ic_contrato+ " " +ic_if +" " + cg_contrato_oe+ " " + df_fecha_firma +" " + df_firma_oe);
        opeElec.guardaContratoIF(ic_contrato, ic_if, cg_contrato_oe.trim(), df_fecha_firma, df_firma_oe);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("_totMod", 1);
	infoRegresar=jsonObj.toString();
}
else if(informacion.equals("GuardaMod")){
	String icContrato = request.getParameter("icContrato");
        String nombre = request.getParameter("nombre");
        String fechafirma = request.getParameter("fechafirma");
        String fechaoe =    request.getParameter("fechaoe");
        String ic_contrato_mod = request.getParameter("numeroic").equals("") ? null :  request.getParameter("numeroic");
        log.info(ic_contrato_mod+ " " + nombre+ " " + fechafirma +" " + fechaoe);
        if (ic_contrato_mod != null){
            log.error("Esta editando el contrato: " +ic_contrato_mod);
            opeElec.actualizaModificatorioIF(icContrato, ic_contrato_mod, nombre.trim(), fechafirma, fechaoe);
        }else{
            opeElec.guardaModificatorioIF(icContrato, nombre.trim(), fechafirma, fechaoe);
        }
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("_totMod", 1);
	infoRegresar=jsonObj.toString();
}


%>

<%=infoRegresar%>