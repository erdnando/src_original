<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.sql.*,
	com.netro.garantias.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*,
	com.netro.electronica.*,
	com.netro.seguridad.*,
	javax.naming.*,
	netropology.utilerias.usuarios.*,
	 com.netro.cotizador.*,
	 com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"  
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../38secsession_extjs.jspf" %>  

<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar ="";

OperacionElectronica opeElec = ServiceLocator.getInstance().lookup("OperacionElectronicaEJB", OperacionElectronica.class);
if(informacion.equals("catalogoConcentrador")){
	
	JSONArray jsObjArray = new JSONArray();
   List lstCatalogoEjecOP = new ArrayList();
	UtilUsr 		utilUsr 			= new UtilUsr();
   List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("OP CON", "N");
                
   if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
		HashMap hmData = null;
		for(int i=0;i<usuariosPorPerfil.size();i++){
			hmData = new HashMap();
			String loginUsuarioOPOP = (String)usuariosPorPerfil.get(i);
			Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioOPOP);
			String nombre = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno();
			hmData.put("clave", loginUsuarioOPOP);
			hmData.put("descripcion", loginUsuarioOPOP +" - "+ nombre);
		                                        
			lstCatalogoEjecOP.add(hmData);
		}
   }
                        
    jsObjArray = JSONArray.fromObject(lstCatalogoEjecOP);
    infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if(informacion.equals("CargarDatos")){
	Thread.sleep(400);
	JSONObject resultado = new JSONObject();

	String cbo_sub = (request.getParameter("cbo_sub") != null) ? request.getParameter("cbo_sub") : "";
	String cbo_con = (request.getParameter("cbo_con") != null) ? request.getParameter("cbo_con") : "";
	
	List consulta = opeElec.cargaDatosGrales();
	resultado.put("registros", consulta);
	infoRegresar = resultado.toString();
	
}else if(informacion.equals("ActualizaDatos")){
	JSONObject resultado = new JSONObject();
	boolean resp = false;
	String ic_mail = (request.getParameter("ic_mail") != null) ? request.getParameter("ic_mail") : "";
	String usr_sub = (request.getParameter("cbo_sub") != null) ? request.getParameter("cbo_sub") : "";
	String tel_sub = (request.getParameter("tel_sub") != null) ? request.getParameter("tel_sub") : "";
	String usr_con = (request.getParameter("cbo_con") != null) ? request.getParameter("cbo_con") : "";
	String tel_con = (request.getParameter("tel_con") != null) ? request.getParameter("tel_con") : "";
	String mails 	= (request.getParameter("mails_fondo") != null) ? request.getParameter("mails_fondo") : "";
	String usrConc2 = (request.getParameter("cmbConcentrador2Hidden") != null) ? request.getParameter("cmbConcentrador2Hidden") : "";
	String telConc2 = (request.getParameter("telefonoName") != null) ? request.getParameter("telefonoName") : "";
	String emailDir = (request.getParameter("correoDirectorName") != null) ? request.getParameter("correoDirectorName") : "";
  
	resp = opeElec.actualizaParamGrales(usr_sub,tel_sub,usr_con,tel_con,usrConc2,telConc2,emailDir,mails,ic_mail);
		
	if(resp)
		resultado.put("success", new Boolean(true));
	infoRegresar = resultado.toString();

}

%>

<%=infoRegresar%>