Ext.onReady(function() {

    var evalua = true;
    var totalVarModificatorios = 0;
    var numContratoVar = null;
    var nombreContratoVar = null;
    var fecFirmaVar = null;
    var fecOpeElecVar = null;
    var conModifVar = null;
    
    var global_edit_Contrato = false;
    var global_edit_Mod      = false;

    var formatoHora = function(hora) {
        var result = false,
            m;
        var re = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;
        if ((m = hora.match(re))) {
            result = (m[1].length == 2 ? "" : "0") + m[1] + ":" + m[2];
        }
        return result;
    };
    
    function callBackGuardaContratos(){
        Ext.Msg.alert('Guardar', 'Los cambios se han guardado correctamente.');
        global_edit_Contrato= false;
        global_edit_Mod = false;
        var combo = Ext.getCmp("_cbo_if");
        var record = catalogoIF.getById( combo.getValue());
        combo.fireEvent('select',combo, [record]);
    }
    
    function getFechaOK(value){
        if (value instanceof Date){ 
            return Ext.util.Format.date(value,'d/m/Y')
        }else{
            return Ext.util.Format.date(Date.parseDate(value,'d/m/Y'),'d/m/Y');
        }    
    }
    
    function guardaModificatorio(){
        var indice = modificatoriosData.getTotalCount()-1;
        var registro = modificatoriosData.getAt(indice);
        var registroContratos = contratosData.getAt(0);
        var icContrato = registroContratos.get('NUM_CONT');
        if (registro.get('FECHAMOD') == '' || registro.get('DESCRIPCION') == '') {
            alert("�Debe capturar todos los campos!");
            return;
        } else {
            Ext.Msg.confirm('', '�Esta seguro de querer agregar el modificatorio? ', function(botonConf) {
                if (botonConf == 'ok' || botonConf == 'yes') {
                    Ext.Ajax.request({
                        url: '38paramIFNoBancario.data.jsp',
                        params: {
                            informacion: 'GuardaMod',
                            icContrato: icContrato,
                            nombre: registro.get('DESCRIPCION'),
                            fechafirma: getFechaOK(registro.get('FECHAMOD')),
                            fechaoe: getFechaOK(registro.get('FECHAMOD')), 
                            numeroic: global_edit_Mod ? registro.get('IC_MOD'): ''
                        },
                        callback: callBackGuardaContratos
                    });
                } 
            });
        }    
    
    }
    

    function  guardaCambiosContrato(){
        if(!global_edit_Contrato && contratosData.getTotalCount() == 1){
            return;
        }
        var indice = 0;
        if (!global_edit_Contrato) indice = 1;
        var registro = contratosData.getAt(indice);
        if (registro.get('NOM_CONT') == '' || registro.get('FECHA_FIR_OPE_ELE') == '' || registro.get('FECHA_FIR_CONT') == '') {
            alert("�Debe capturar todos los campos!");
            return;
        } else {
            var icif = "";
            Ext.Msg.confirm('', '�Esta seguro de querer dar de alta el contrato? ', function(botonConf) {
                if (botonConf == 'ok' || botonConf == 'yes') {
                    Ext.Ajax.request({
                        url: '38paramIFNoBancario.data.jsp',
                        params: {
                            informacion: 'GuardaContrato',
                            icif: Ext.getCmp('_cbo_if').getValue(),
                            nombre: registro.get('NOM_CONT'),
                            fechafirma: getFechaOK(registro.get('FECHA_FIR_CONT')),
                            fechaoe:    getFechaOK(registro.get('FECHA_FIR_OPE_ELE')),
                            numeroic: global_edit_Contrato ? registro.get('NUM_CONT'): ''
                        },
                        callback: callBackGuardaContratos
                    });
                } 
            });
        }
    }

    var procesarCargaRegistro = function(store, registros, opts) {
        numContratoVar = null;
        nombreContratoVar = null;
        fecFirmaVar = null;
        fecOpeElecVar = null;
        conModifVar = null;
        //Ext.getCmp("tipoOperacion").setValue("tipoOpeAlta", true);
        if (registros != null) {
            if (store.getTotalCount() > 0) {     
                /* Ya tiene registro */
                mostrarGridsContratosYModificatorios(true);
                var registro = store.getAt(0);
                Ext.getCmp('_num_cuenta').setValue(registro.get('NUMCUENTA'));
                Ext.getCmp('_banco').setValue(registro.get('BANCO'));
                Ext.getCmp('_cu_clabe').setValue(registro.get('CLABE'));
                
                Ext.getCmp('_fec_firma').setValue(registro.get('FIRMACONT'));
                Ext.getCmp('_nom_contrato').setValue(registro.get('NOMCONTRATO'));
                Ext.getCmp('_fec_ope_elec').setValue(registro.get('FIRMAOE'));
                //Ext.getCmp('_con_modif').setValue(registro.get('NUMMOD'));
                Ext.getCmp('_cbo_user_promo').setValue(registro.get('USRPROMO'));
                Ext.getCmp('_num_telefono_promo').setValue(registro.get('TELPROMO'));
                Ext.getCmp('_cbo_user_super').setValue(registro.get('USRSEG'));
                Ext.getCmp('_num_telefono_super').setValue(registro.get('TELSEG'));
                Ext.getCmp('_hr_envio').setValue(registro.get('HR_ENVIO'));
                Ext.getCmp('_hr_reenvio').setValue(registro.get('HR_REENVIO'));
                Ext.getCmp('cmbIFOperado').setValue(registro.get('CG_OPERADOR'));
                Ext.getCmp('cmbIFOperado').fireEvent('select', registro.get('CG_OPERADOR'));
                Ext.getCmp('cmbIFAutorizadorUno').setValue(registro.get('CG_AUTORIZADOR_UNO'));
                Ext.getCmp('cmbIFAutorizadorUno').fireEvent('select', registro.get('CG_AUTORIZADOR_UNO'));
                Ext.getCmp('cmbIFAutorizadorDos').setValue(registro.get('CG_AUTORIZADOR_DOS'));
                if (registro.get('CS_MUJERES_EMPRESARIAS') == 'S') {
                    Ext.getCmp("id_empresarias").setValue(true)
                } else {
                    Ext.getCmp("id_empresarias").setValue(false)
                }
                var firma = registro.get('CG_OPERA_FIRMA_MANC');
                if (firma == "S") {
                    Ext.getCmp('s').setValue(true);
                } else {
                    Ext.getCmp('n').setValue(true);
                }
                //Ext.getCmp('botonModifContrato').setVisible(true);
                //Ext.getCmp('tipoOpeActu').setVisible(true);
                //Aqui se debe hacer el llamado para cargar los datos del grid
                Ext.StoreMgr.key('modificatoriosDataStore').load({
                            params:
                                {
                                    informacion: 'cargaModificatorios',
                                    cbo_if: Ext.getCmp('_cbo_if').getValue(),
                                    inicio: 0,
                                    filasNew: 0
                                },
                            callback :function (record, opt, success){
                                if(success && record.length > 0)
                                    Ext.getCmp("btnUpdateModificatorio").setDisabled(false);
                                else
                                    Ext.getCmp("btnUpdateModificatorio").setDisabled(true);
                            }
                });
                Ext.StoreMgr.key('contratosDataStore').load({
                    params: {
                        informacion: 'CargaContratos',
                        cbo_if: Ext.getCmp('_cbo_if').getValue(),
                        inicio: 0,
                        filasNew: 0
                    }
                });
                Ext.Ajax.request({
                    url: '38paramIFNoBancario.data.jsp',
                    params: {
                        informacion: 'cargarContrato',
                        cbo_if: Ext.getCmp('_cbo_if').getValue()
                    },
                    callback: procesarIfContratos
                });
                evalua = false;

            } else {
                /* No tiene registro */
                mostrarGridsContratosYModificatorios(false);
                var aux = Ext.getCmp('_cbo_if').getValue();
                Ext.getCmp('formaAfiliacion').getForm().reset();
                Ext.getCmp('_cbo_if').setValue(aux);
                modificatoriosData.removeAll();
                contratosData.removeAll();
                //Ext.getCmp('botonModifContrato').setVisible(false);
                //Ext.getCmp('tipoOpeActu').setVisible(false);

                evalua = true;

                Ext.StoreMgr.key('contratosDataStore').load({
                    params: {
                        informacion: 'CargaContratos',
                        cbo_if: Ext.getCmp('_cbo_if').getValue(),
                        inicio: 0,
                        filasNew: 0
                    }
                });

                Ext.Ajax.request({
                    url: '38paramIFNoBancario.data.jsp',
                    params: {
                        informacion: 'cargarContrato',
                        cbo_if: Ext.getCmp('_cbo_if').getValue()
                    },
                    callback: procesarIfContratos
                });
                Ext.StoreMgr.key('modificatoriosDataStore').load({
                    params: {
                        informacion: 'cargaModificatorios',
                        cbo_if: Ext.getCmp('_cbo_if').getValue(),
                        inicio: 0,
                        filasNew: 0
                    }
                });

            }
            // Actualizar los parametros base con los parametros de la consulta exitosa
            store.baseParams = opts.params;
            // Resetear el campo operacion para evitar que cuando se utilicen los botones
            // de paginacion del pagingtoolbar se tengan que regenerar la llaves
            Ext.apply(
                store.baseParams, {
                    operacion: ''
                }
            );
        }

    }

    function procesarSuccessFailureGuardar(opts, success, response) {
        if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
            var jsonData = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp('formaAfiliacion').getForm().reset();
            modificatoriosData.removeAll();
            contratosData.removeAll();
            Ext.getCmp('cmbIFOperado').reset();
            Ext.getCmp('cmbIFAutorizadorUno').reset();
            Ext.getCmp('cmbIFAutorizadorDos').reset();
            Ext.Msg.alert('Mensaje', 'Se guard� con �xito la parametrizaci�n del Intemediario Financiero');
        } else {
            NE.util.mostrarConnError(response, opts);
        }
    }

    function procesarSuccessFailureActualizar(opts, success, response) {
        if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
            var jsonData = Ext.util.JSON.decode(response.responseText);
            Ext.getCmp('formaAfiliacion').getForm().reset();
            modificatoriosData.removeAll();
            contratosData.removeAll();
            Ext.getCmp('cmbIFOperado').reset();
            Ext.getCmp('cmbIFAutorizadorUno').reset();
            Ext.getCmp('cmbIFAutorizadorDos').reset();
            Ext.Msg.alert('Mensaje', 'Se actualizo con �xito la parametrizaci�n del Intemediario Financiero');
        } else {
            NE.util.mostrarConnError(response, opts);
        }
    }

    function procesarTotalModificatorios(opts, success, response) {
        if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
            var jsonData = Ext.util.JSON.decode(response.responseText);
            if (parseInt(Ext.getCmp('_con_modif').getValue()) < parseInt(jsonData._totMod)) {
                Ext.Msg.alert('Mensaje', 'El n�mero de Contratos Modificatorios no puede ser menor a los ya registrados: ' + jsonData._totMod);
                Ext.getCmp('_con_modif').setValue(jsonData._totMod);
            } else {
                var ini;
                var num;
                if (jsonData.TOT_MODIFI != null && jsonData.TOT_MODIFI != "") {
                    ini = jsonData._totMod;
                    num = parseInt(Ext.getCmp('_con_modif').getValue()) - parseInt(jsonData._totMod);
                } else {
                    ini = "0";
                    num = parseInt(Ext.getCmp('_con_modif').getValue()) - parseInt(ini);
                }

                Ext.StoreMgr.key('contratosDataStore').load({
                    params: {
                        informacion: 'CargaContratos',
                        cbo_if: Ext.getCmp('_cbo_if').getValue(),
                        inicio: ini,
                        filasNew: num
                    }
                });

                Ext.StoreMgr.key('modificatoriosDataStore').load({
                    params: {
                        informacion: 'CargaFecMod',
                        cbo_if: Ext.getCmp('_cbo_if').getValue(),
                        inicio: ini,
                        filasNew: num
                    }
                });
            }
        } else {
            NE.util.mostrarConnError(response, opts);
        }
    }

    var procesarSuccessContrato = function(opts, success, response) {
        if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
            var jsonData = Ext.util.JSON.decode(response.responseText);
            fpDatos.el.mask('Cargando...', 'x-mask-loading');
            if (jsonData.numContrato != null && jsonData.numContrato != "") {
                Ext.MessageBox.alert('Guardar Contrato', 'El contrato ' + jsonData.numContrato + ' ha sido modificado');
            } else {
                Ext.MessageBox.alert('Guardar Contrato', 'Contrato dado de alta');
            }

            autorizadorUno.removeAll(true);
            autorizadorDos.removeAll(true);
            ifOperador.load({
                params: {
                    informacion: 'catalogoIFOperador',
                    listaIF: jsonData.cve_if
                },
                callback: function(record, options, success) {
                    //fpDatos.el.unmask();
                }
            });
            Ext.StoreMgr.key('registroExistenteDataStore').load({
                params: {
                    informacion: 'CargarDatos',
                    cbo_if: jsonData.cve_if,
                    cmp: 'formulario'
                },
                callback: function(success) {
                    fpDatos.el.unmask();
                }
            });

            //rfc_invalido = 'N';




        }
    }

    var accionContratoModifica = function() {
        bandera = true;
        var _no_vacio = 'N';
        var fechaCont = Ext.getCmp('_fec_firma');
        var fechaOpe = Ext.getCmp('_fec_ope_elec');

        if (fechaCont.getValue() != "") {
            var fechaDe = Ext.util.Format.date(fechaCont.getValue(), 'd/m/Y');
            var fechaA = Ext.util.Format.date(fechaOpe.getValue(), 'd/m/Y');
            if (!isdate(fechaDe)) {
                fechaCont.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
                bandera = false;
            } else if (!isdate(fechaA)) {
                fechaOpe.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
                bandera = false;
            }
            if (datecomp(fechaDe, fechaA) == "1") {
                alert("La fecha de Operacion no puede ser menor a la fecha de contrato");
                fechaOpe.sFechaPagoFin.focus();
                bandera = false;
                return;
            }

        }

        var arr = new Array();
        if (Ext.getCmp('_con_modif').getValue() != '') {
            var j = 0;
            for (var i = 0; i < modificatoriosData.getTotalCount(); i++) {
                var reg = modificatoriosData.getAt(i);
                if (reg.get('FECHAMOD') == '') {
                    alert("Debe capturar las Fechas de los Contratos Modificatorios");
                    bandera = false;
                    break;
                } else if (reg.get('EDO') == 'N') {
                    arr[j++] = reg.get('DESCRIPCION') + ',' + Ext.util.Format.date(reg.get('FECHAMOD', 'd/m/Y'));
                } else if (reg.get('EDO') == 'S') {
                    arr[j++] = reg.get('DESCRIPCION') + ',' + reg.get('FECHAMOD', 'd/m/Y');
                }
            }
        } else {
            alert("Esta capturando un Contrato sin Modificatorios");
        }
        if (bandera) {
            Ext.Msg.confirm('', 'Esta seguro de querer dar de modificar el contrato? ', function(botonConf) {
                if (botonConf == 'ok' || botonConf == 'yes') {
                    Ext.Ajax.request({
                        url: '38paramIFNoBancario.data.jsp',
                        params: Ext.apply(fpDatos.getForm().getValues(), {
                            informacion: 'altaContrato',
                            numContrato: numContratoVar,
                            arr: arr
                        }),
                        callback: procesarSuccessContrato
                    });
                } // if botonConfig
            });
        }
    }

    var accionContrato = function() {
        bandera = true;
        var _no_vacio = 'N';
        var fechaCont = Ext.getCmp('_fec_firma');
        var fechaOpe = Ext.getCmp('_fec_ope_elec');

        if (fechaCont.getValue() != "") {
            var fechaDe = Ext.util.Format.date(fechaCont.getValue(), 'd/m/Y');
            var fechaA = Ext.util.Format.date(fechaOpe.getValue(), 'd/m/Y');
            if (!isdate(fechaDe)) {
                fechaCont.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
                bandera = false;
            } else if (!isdate(fechaA)) {
                fechaOpe.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
                bandera = false;
            }
            if (datecomp(fechaDe, fechaA) == "1") {
                alert("La fecha de Operacion no puede ser menor a la fecha de contrato");
                fechaOpe.sFechaPagoFin.focus();
                bandera = false;
                return;
            }

        }

        var arr = new Array();
        if (Ext.getCmp('_con_modif').getValue() != null && Ext.getCmp('_con_modif').getValue() != '') {
            var j = 0;
            for (var i = 0; i < modificatoriosData.getTotalCount(); i++) {
                var reg = modificatoriosData.getAt(i);
                if (reg.get('FECHAMOD') == '') {
                    alert("Debe capturar las Fechas de los Contratos Modificatorios");
                    bandera = false;
                    break;
                } else if (reg.get('EDO') == 'N') {
                    _no_vacio = 'S';
                    arr[j++] = reg.get('DESCRIPCION') + ',' + Ext.util.Format.date(reg.get('FECHAMOD', 'd/m/Y'));
                }
            }
        } else {
            alert("Esta capturando un Contrato sin Modificatorios");
        }
        if (bandera) {
            Ext.Msg.confirm('', 'Esta seguro de querer dar de alta el contrato? ', function(botonConf) {
                if (botonConf == 'ok' || botonConf == 'yes') {
                    Ext.Ajax.request({
                        url: '38paramIFNoBancario.data.jsp',
                        params: Ext.apply(fpDatos.getForm().getValues(), {
                            informacion: 'altaContrato',
                            arr: arr
                        }),
                        callback: procesarSuccessContrato
                    });
                } // if botonConfig
            });
        }
    }

    var accionConsulta = function(estadoSiguiente, respuesta) {
        bandera = true;
        var _no_vacio = 'N';
        var fechaCont = Ext.getCmp('_fec_firma');
        var fechaOpe = Ext.getCmp('_fec_ope_elec');
        if (fechaCont.getValue() != "") {
            var fechaDe = Ext.util.Format.date(fechaCont.getValue(), 'd/m/Y');
            var fechaA = Ext.util.Format.date(fechaOpe.getValue(), 'd/m/Y');
            if (!isdate(fechaDe)) {
                fechaCont.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
                bandera = false;
            } else if (!isdate(fechaA)) {
                fechaOpe.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
                bandera = false;
            }
        }
        var arr = new Array();
        if (Ext.getCmp('_con_modif').getValue() != '') {
            var j = 0;
            for (var i = 0; i < modificatoriosData.getTotalCount(); i++) {
                var reg = modificatoriosData.getAt(i);
                if (reg.get('FECHAMOD') == '') {
                    alert("Debe capturar las Fechas de los Contratos Modificatorios");
                    bandera = false;
                    break;
                } else if (reg.get('EDO') == 'N') {
                    _no_vacio = 'S';
                    arr[j++] = reg.get('DESCRIPCION') + ',' + Ext.util.Format.date(reg.get('FECHAMOD', 'd/m/Y'));
                }
            }
        }
        if (estadoSiguiente == "CONSULTAR") {
            if (bandera) {
                Ext.Ajax.request({
                    url: '38paramIFNoBancario.data.jsp',
                    params: Ext.apply(fpDatos.getForm().getValues(), {
                        informacion: 'GuardarDatos',
                        valida: _no_vacio,
                        arr: arr,
                        numContrato: numContratoVar
                    }),
                    callback: procesarSuccessFailureGuardar
                });
            }

        } else if (estadoSiguiente == "ACTUALIZAR") {
            if (bandera) {
                Ext.Ajax.request({
                    url: '38paramIFNoBancario.data.jsp',
                    params: Ext.apply(fpDatos.getForm().getValues(), {
                        informacion: 'ActualizaDatos',
                        valida: _no_vacio,
                        arr: arr,
                        numContrato: numContratoVar
                    }),
                    callback: procesarSuccessFailureActualizar
                });
            }

        }

    }

    function procesarIfContratos(opts, success, response) {
        if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
            var jsonData = Ext.util.JSON.decode(response.responseText);

            //Ext.getCmp('').setValue(jsonData.NUM_CONT);
            if (jsonData.NOM_CONT != null && jsonData.NOM_CONT != "") {
                //Ext.getCmp('_nom_contrato').setValue(jsonData.NOM_CONT);
            }
            if (jsonData.FECHA_FIR_CONT != null && jsonData.FECHA_FIR_CONT != "") {
                //Ext.getCmp('_fec_firma').setValue(jsonData.FECHA_FIR_CONT);
            }

            if (jsonData.FECHA_FIR_OPE_ELE != null && jsonData.FECHA_FIR_OPE_ELE != "") {
                //Ext.getCmp('_fec_ope_elec').setValue(jsonData.FECHA_FIR_OPE_ELE);
            }

            if (jsonData.NUM_CONT != null && jsonData.NUM_CONT != "") {
                //Ext.getCmp('botonModifContrato').setVisible(true);
                //Ext.getCmp('tipoOpeActu').setVisible(true);
                if (jsonData.NUM_CONT != null && jsonData.NUM_CONT != "") {
                    totalVarModificatorios = parseInt(jsonData.TOT_MODIFI);
                } else {
                    totalVarModificatorios = parseInt("0");
                }

                if (jsonData.TOT_MODIFI != null && jsonData.TOT_MODIFI != "") {
                    conModifVar = jsonData.TOT_MODIFI;
                } else {
                    conModifVar = "0";
                }


                numContratoVar = parseInt(jsonData.NUM_CONT);
                nombreContratoVar = jsonData.NOM_CONT
                fecFirmaVar = jsonData.FECHA_FIR_CONT;
                fecOpeElecVar = jsonData.FECHA_FIR_OPE_ELE;
                //Ext.getCmp('_con_modif').setValue(jsonData.TOT_MODIFI);
            }
        } else {
            NE.util.mostrarConnError(response, opts);
        }
    }
    //--------------------------- STORE�S -----------------------------

    var registroExistenteData = new Ext.data.JsonStore({
        root: 'registros',
        id: 'registroExistenteDataStore',
        url: '38paramIFNoBancario.data.jsp',
        baseParams: {
            informacion: 'CargarDatos'
        },
        fields: [{
                name: 'CVEIF'
            },
            {
                name: 'NUMCUENTA'
            },
            {
                name: 'BANCO'
            },
            {
                name: 'CLABE'
            },
            {
                name: 'FIRMACONT'
            },
            {
                name: 'NOMCONTRATO'
            },
            {
                name: 'FIRMAOE'
            },
            {
                name: 'NUMMOD'
            },
            {
                name: 'USRPROMO'
            },
            {
                name: 'TELPROMO'
            },
            {
                name: 'USRSEG'
            },
            {
                name: 'TELSEG'
            },
            {
                name: 'HR_ENVIO'
            },
            {
                name: 'HR_REENVIO'
            },
            {
                name: 'CG_OPERA_FIRMA_MANC'
            },
            {
                name: 'CG_OPERADOR'
            },
            {
                name: 'CG_AUTORIZADOR_UNO'
            },
            {
                name: 'CG_AUTORIZADOR_DOS'
            },
            {
                name: 'CS_MUJERES_EMPRESARIAS'
            }
        ],
        totalProperty: 'total',
        messageProperty: 'msg',
        autoLoad: false,
        listeners: {
            load: procesarCargaRegistro,
            exception: {
                fn: function(proxy, type, action, optionsRequest, response, args) {
                    NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
                    //LLama procesar consulta, para que desbloquee los componentes.
                    procesarCargaRegistro(null, null, null);
                }
            }
        }
    });

    var procesarIF = function(store, arrRegistros, opts) {
        var misIF = [];
        store.each(function(record) {
            misIF.push(record.data['clave']);
        });


    }

    var catalogoIF = new Ext.data.JsonStore({
        id: 'catIFData',
        root: 'registros',
        fields: ['clave', 'descripcion', 'loadMsg'],
        url: '38paramIFNoBancario.data.jsp',
        baseParams: {
            informacion: 'CatalogoIF'
        },
        autoLoad: false,
        listeners: {
            //load		: procesarIF,
            exception: NE.util.mostrarDataProxyError
        }
    });

    var catalogoPromo = new Ext.data.JsonStore({
        id: 'catalogoPromo',
        root: 'registros',
        fields: ['clave', 'descripcion', 'loadMsg'],
        url: '38paramIFNoBancario.data.jsp',
        baseParams: {
            informacion: 'CatalogoPromo'
        },
        totalProperty: 'total',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            beforeload: NE.util.initMensajeCargaCombo
        }
    });

    var catalogoSupervisor = new Ext.data.JsonStore({
        id: 'catalogoSupervisor',
        root: 'registros',
        fields: ['clave', 'descripcion', 'loadMsg'],
        url: '38paramIFNoBancario.data.jsp',
        baseParams: {
            informacion: 'catalogoSupervisor'
        },
        totalProperty: 'total',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            beforeload: NE.util.initMensajeCargaCombo
        }
    });

    var procesarOperador = function(store, arrRegistros, opts) {
        if (arrRegistros > 0) {
            //fpDatos.el.unmask();
        }
    }

    var ifOperador = new Ext.data.JsonStore({
        id: 'ifOperador',
        root: 'registros',
        fields: ['clave', 'descripcion', 'loadMsg'],
        url: '38paramIFNoBancario.data.jsp',
        baseParams: {
            informacion: 'catalogoIFOperador'
        },
        totalProperty: 'total',
        autoLoad: false,
        listeners: {
            load: procesarOperador,
            exception: NE.util.mostrarDataProxyError,
            beforeload: NE.util.initMensajeCargaCombo
        }
    });


    var autorizadorUno = new Ext.data.ArrayStore({
        autoDestroy: true,
        storeId: 'myStore1',
        idIndex: 0,
        fields: [{
                name: 'clave'
            },
            {
                name: 'descripcion'
            }
        ]
    });

    var autorizadorDos = new Ext.data.ArrayStore({
        autoDestroy: true,
        storeId: 'myStore2',
        idIndex: 0,
        fields: [{
                name: 'clave'
            },
            {
                name: 'descripcion'
            }
        ]
    });

    var autorizadorUnoA = new Ext.data.JsonStore({
        id: 'autorizadorUnoA',
        root: 'registros',
        fields: ['clave', 'descripcion', 'loadMsg'],
        url: '38paramIFNoBancario.data.jsp',
        baseParams: {
            informacion: 'catalogoIFOperadorA'
        },
        totalProperty: 'total',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            beforeload: NE.util.initMensajeCargaCombo
        }
    });

    var autorizadorDosA = new Ext.data.JsonStore({
        id: 'autorizadorDosA',
        root: 'registros',
        fields: ['clave', 'descripcion', 'loadMsg'],
        url: '38paramIFNoBancario.data.jsp',
        baseParams: {
            informacion: 'catalogoIFOperadorAA'
        },
        totalProperty: 'total',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            beforeload: NE.util.initMensajeCargaCombo
        }
    });

    /****** Store�s Grid�s *******/
    var contratosData = new Ext.data.JsonStore({
        root: 'registros',
        id: 'contratosDataStore',
        url: '38paramIFNoBancario.data.jsp',
        totalProperty: 'CargaContratos',
        fields: [{
                name: 'NUM_CONT'
            },
            {
                name: 'NOM_CONT'
            },
            {
                name: 'FECHA_FIR_CONT'
            },
            {
                name: 'FECHA_FIR_OPE_ELE'
            }
        ],
        messageProperty: 'msg',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }
    });

    var modificatoriosData = new Ext.data.JsonStore({
        root: 'registros',
        id: 'modificatoriosDataStore',
        url: '38paramIFNoBancario.data.jsp',
        totalProperty: 'CargarDatos',
        fields: [{
                name: 'DESCRIPCION'
            },
            {
                name: 'FECHAMOD'
            },
            {
                name: 'EDO'
            },{
                name: 'IC_MOD'
            }
            
        ],
        messageProperty: 'msg',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }
    });

    var modificatoriosTabla = new Ext.data.JsonStore({
        root: 'registros',
        id: 'modificatoriosTablaStore',
        url: '38paramIFNoBancario.data.jsp',
        totalProperty: 'CargarDatos',
        fields: [{
                name: 'DESCRIPCION'
            },
            {
                name: 'FECHAMOD'
            },
            {
                name: 'EDO'
            }
        ],
        messageProperty: 'msg',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }
    });


    //--------------------------- COMPONENTES -----------------------------

    var gridContrato = new Ext.grid.EditorGridPanel({
        title      : 'Contratos',
        id: 'gridContrato',
        clicksToEdit: 1,
        store: contratosData,
        stripeRows: true,
        loadMask: true,
        hidden: true,
        width: 700,
        height: 130,
        frame: false,
        header: true,
        columns: [{
                header: 'N�mero',
                tooltip: 'N�mero',
                dataIndex: 'NUM_CONT',
                sortable: true,
                resizable: true,
                width: 55
            },
            {
                header: 'Nombre del contrato',
                tooltip: 'Nombre del contrato',
                dataIndex: 'NOM_CONT',
                renderer : function(value,metadata,registro, rowIndex, colIndex){
                    if(registro.data['NUM_CONT']=="" || global_edit_Contrato)
                        return NE.util.colorCampoEdit(value,metadata,registro);
                    else
                    return value;
                },
                sortable: true,
                resizable: true,
                width: 270,
                editor: {
                    xtype: 'textfield',
                    name: 'txtNombreContrato',
                    allowBlank: false,
                    maxLength: 100,
                    msgTarget: 'side',
                    margins: '0 20 0 0'
                }
            },
            {
                header: 'Fecha Firma',
                tooltip: 'Fecha Firma',
                editor: {
                    xtype: 'datefield',
                    name: 'txtFechaFirCont',
                    allowBlank: false,
                    format: 'd/m/Y',
                    startDay: 0,
                    maxLength: 10,
                    msgTarget: 'side',
                    margins: '0 20 0 0'
                },
                dataIndex: 'FECHA_FIR_CONT',
                sortable: true,
                resizable: true,
                width: 150,
                renderer : function(value,metadata,registro, rowIndex, colIndex){
                        var dateVal = null;
                        if (value instanceof Date){ 
                            dateVal = Ext.util.Format.date(value,'d/m/Y')
                        }else{
                            dateVal = Ext.util.Format.date(Date.parseDate(value,'d/m/Y'),'d/m/Y');
                        }
                        if(registro.data['NUM_CONT']=="" || global_edit_Contrato)
                                return NE.util.colorCampoEdit(dateVal,metadata,registro);
                        else
                                return  dateVal;
                    }
            },
            {
                header: 'Fecha Firma Operaci�n Electr�nica',
                tooltip: 'Fecha Firma Operaci�n Electr�nica',
                editor: {
                    xtype: 'datefield',
                    name: 'txtFechaFirOpeEle',
                    allowBlank: false,
                    format: 'd/m/Y',
                    //startDay: 0,
                    maxLength: 10,
                    msgTarget: 'side',
                    margins: '0 20 0 0'
                },
                dataIndex: 'FECHA_FIR_OPE_ELE',
                sortable: true,
                resizable: true,
                width: 185,
                renderer : function(value,metadata,registro, rowIndex, colIndex){
                        var dateVal = null;
                        if (value instanceof Date){ 
                            dateVal = Ext.util.Format.date(value,'d/m/Y')
                        }else{
                            dateVal = Ext.util.Format.date(Date.parseDate(value,'d/m/Y'),'d/m/Y');
                        }
                        if(registro.data['NUM_CONT']=="" || global_edit_Contrato)
                                return NE.util.colorCampoEdit(dateVal,metadata,registro);
                        else
                                return  dateVal;
                    }                
            }
        ],
        listeners: {
            beforeedit: function(e) { //esta parte es para deshabilitar los campos editables
                var record = e.record;
                var gridFijos = e.grid;
                var campo = e.field;
                if (record.get('NUM_CONT')== "" || global_edit_Contrato) return true;
                return false;
            }
        },
        bbar: {
            xtype: 'toolbar',
            buttonAlign: 'center',
            items: ['->','-',
            {
                xtype: 'button',
                hidden: false,
                iconCls:'icoAgregar',
                text: 'Agregar Contrato',
                id: 'btnAddContrato',
                handler: function(boton, evento) {
                    global_edit_Contrato= false;
                    Ext.getCmp('btnSaveContratos').setDisabled(false);
                    Ext.StoreMgr.key('contratosDataStore').load({
                        params: {
                            informacion: 'CargaContratos',
                            cbo_if: Ext.getCmp('_cbo_if').getValue(),
                            inicio: 0,
                            filasNew: 1
                        }
                    });                
                }
            },'-',
            {
                xtype: 'button',
                hidden: false,
                text: 'Actualizar Contrato',
                id: 'btnUpdateContrato',
                handler: function(boton, evento) {
                    global_edit_Contrato = true;
                    Ext.getCmp('btnSaveContratos').setDisabled(false);
                    Ext.StoreMgr.key('contratosDataStore').load({
                        params: {
                            informacion: 'CargaContratos',
                            cbo_if: Ext.getCmp('_cbo_if').getValue(),
                            inicio: 0,
                            filasNew: 0
                        }
                    });
                }
            },'-',
            {
                xtype: 'button',
                hidden: false,
                iconCls: 'icoGuardar',
                text: 'Guardar cambios',
                disabled: true,
                id: 'btnSaveContratos',
                handler: function(boton, evento) {
                    guardaCambiosContrato();
                }
            },'-'            
        ]}        
    });

    var gridModificatorios = new Ext.grid.EditorGridPanel({
        title: 'Contratos Modificatorios',
        id: 'grid',
        clicksToEdit: 1,
        store: modificatoriosData,
        stripeRows: true,
        loadMask: true,
        hidden: false,
        width: 700,
        height: 155,
        frame: false,
        header: true,
        columns: [{
                header: 'Nombre del Contrato Modificatorio',
                tooltip: 'Nombre del Contrato Modificatorio',
                dataIndex: 'DESCRIPCION',
                sortable: true,
                resizable: true,
                width: 270,
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                },
                renderer: function(value, metadata, registro, rowIndex, colIndex) {          
                    if (registro.data['EDO'] == "N" || (global_edit_Mod && rowIndex == modificatoriosData.getTotalCount()-1))
                        return NE.util.colorCampoEdit(value, metadata, registro);
                    else
                        return value;
                }                
            },
            {
                header: 'Fecha de Firma',
                tooltip: 'Fecha de Firma',
                editor: {
                    xtype: 'datefield',
                    name: 'txtFechaAplic',
                    allowBlank: false,
                    format: 'd/m/Y',
                    startDay: 0,
                    maxLength: 10,
                    msgTarget: 'side',
                    margins: '0 20 0 0'
                },
                dataIndex: 'FECHAMOD',
                sortable: true,
                resizable: true,
                width: 210,
                renderer: function(value, metadata, registro, rowIndex, colIndex) {
                    var dateVal = null;
                    if (value instanceof Date){ 
                        dateVal = Ext.util.Format.date(value,'d/m/Y')
                    }else{
                        dateVal = Ext.util.Format.date(Date.parseDate(value,'d/m/Y'),'d/m/Y');
                    }
                    if(registro.data['EDO']=="N" ||(global_edit_Mod && rowIndex == modificatoriosData.getTotalCount()-1))
                            return NE.util.colorCampoEdit(dateVal,metadata,registro);
                    else
                            return  dateVal;   
                }
            }
        ],
        listeners: {
            beforeedit: function(e) { //esta parte es para deshabilitar los campos editables 			
                var record = e.record;
                if (record.data['EDO'] == "N" || (global_edit_Mod && e.row == modificatoriosData.getTotalCount()-1)) 
                    return true;
                return false;

            }
        },
        bbar: {
            xtype: 'toolbar',
            buttonAlign: 'center',
            items: ['->','-',
            {
                xtype: 'button',
                hidden: false,
                iconCls:'icoAgregar',
                text: 'Agregar Modificatorio',
                id: 'btnAddModificatorio',
                handler: function(boton, evento) {
                    global_edit_Mod = false;
                    Ext.getCmp('btnSaveModificatoris').setDisabled(false);
                    Ext.StoreMgr.key('modificatoriosDataStore').load({
                        params: {
                            informacion: 'cargaModificatorios',
                            cbo_if: Ext.getCmp('_cbo_if').getValue(),
                            inicio: 0,
                            filasNew: 1
                        }
                    });                    
                }
            },'-',
            {
                xtype: 'button',
                hidden: false,
                text: 'Actualizar Modificatorio',
                id: 'btnUpdateModificatorio',
                disabled:true,
                handler: function(boton, evento) {
                    global_edit_Mod = true;
                    Ext.getCmp('btnSaveModificatoris').setDisabled(false);
                    Ext.StoreMgr.key('modificatoriosDataStore').load({
                        params: {
                            informacion: 'cargaModificatorios',
                            cbo_if: Ext.getCmp('_cbo_if').getValue(),
                            inicio: 0,
                            filasNew: 0
                        }
                    });
                }
            },'-',
            {
                xtype: 'button',
                hidden: false,
                iconCls: 'icoGuardar',
                text: 'Guardar cambios',
                id: 'btnSaveModificatoris',
                disabled: true,
                handler: function(boton, evento) {
                    guardaModificatorio();
                }
            },'-'            
        ]
        }
    });

    var afiliacionPanel = {
        xtype: 'panel',
        id: 'afiliacionPanel',
        items: [{
            layout: 'form',
            labelWidth: 192,
            bodyStyle: 'padding: 2px; padding-right:20px;padding-left:60px;',
            items: [{
                    xtype: 'combo',
                    id: '_cbo_if',
                    hiddenName: 'cbo_if',
                    fieldLabel: 'Intermediario Financiero',
                    emptyText: 'Seleccione...',
                    width: 280,
                    forceSelection: true,
                    msgTarget: 'side',
                    triggerAction: 'all',
                    mode: 'local',
                    valueField: 'clave',
                    displayField: 'descripcion',
                    margins: '0 20 0 0',
                    store: catalogoIF,
                    listeners: {
                        select: function(combo) {
                            var cbo_if = combo.getValue();
                            var misIF = [];
                            misIF.push(cbo_if);
                            fpDatos.el.mask('Cargando...', 'x-mask-loading');
                            autorizadorUno.removeAll(true);
                            autorizadorDos.removeAll(true);
                            ifOperador.load({
                                params: {
                                    informacion: 'catalogoIFOperador',
                                    listaIF: misIF
                                },
                                callback: function(record, options, success) {
                                    //fpDatos.el.unmask();
                                }
                            });
                            Ext.StoreMgr.key('registroExistenteDataStore').load({
                                params: {
                                    informacion: 'CargarDatos',
                                    cbo_if: cbo_if,
                                    cmp: 'formulario'
                                },
                                callback: function(success) {
                                    fpDatos.el.unmask();
                                }
                            });
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    name: 'num_cuenta',
                    id: '_num_cuenta',
                    fieldLabel: 'N�mero de Cuenta',
                    msgTarget: 'side',
                    maskRe: /[0-9]/,
                    maxLength: 25,
                    margins: '0 20 0 0',
                    width: 280,
                    allowBlank: false,
                    tabIndex: 1
                },
                {
                    xtype: 'textfield',
                    name: 'banco',
                    id: '_banco',
                    fieldLabel: 'Banco',
                    maxLength: 80,
                    msgTarget: 'side',
                    width: 280,
                    allowBlank: false,
                    tabIndex: 2
                },
                {
                    xtype: 'textfield',
                    name: 'cu_clabe',
                    id: '_cu_clabe',
                    fieldLabel: 'Cuenta CLABE',
                    maskRe: /[0-9]/,
                    msgTarget: 'side',
                    maxLength: 18,
                    width: 280,
                    allowBlank: false,
                    tabIndex: 2
                }
            ]
        }]
    };

    var contratosPanel = {
        xtype: 'panel',
        id: 'contratosPanel',
        //height: 245,
        items: [
            {
                layout: 'vbox',
                id: 'hboxGridContratos',
                title: '',
                width: 800,
                height: 135,
                bodyStyle: 'padding-left:60px;',
                items: [gridContrato, 
        {
            layout: 'form',
            labelWidth: 192,
            bodyStyle: 'padding-top:5px; padding-right:20px;',
            items: [               
                {
                    xtype: 'textfield',
                    name: 'nom_contrato',
                    id: '_nom_contrato',
                    fieldLabel: 'Nombre Contrato',
                    msgTarget: 'side',
                    maxLength: 80,
                    width: 280,
                    allowBlank: false,
                    tabIndex: 1
                },
                {
                    xtype: 'datefield',
                    name: 'fec_firma',
                    id: '_fec_firma',
                    fieldLabel: 'Fecha Firma Contrato',
                    msgTarget: 'side',
                    maxLength: 100,
                    width: 280,
                    allowBlank: false,
                    tabIndex: 2
                },
                {
                    xtype: 'datefield',
                    name: 'fec_ope_elec',
                    id: '_fec_ope_elec',
                    fieldLabel: 'Fecha Firma Operaci�n Electr�nica',
                    maxLength: 100,
                    msgTarget: 'side',
                    width: 280,
                    allowBlank: false,
                    tabIndex: 2
                },
                {
                    xtype: 'textfield',
                    name: 'con_modif',
                    id: '_con_modif',
                    fieldLabel: 'Contratos Modificatorios',
                    msgTarget: 'side',
                    maxLength: 2,
                    maskRe: /[0-9]/,
                    width: 280,
                    allowBlank: true,
                    tabIndex: 2,
                    listeners: {
                        'change': function(field) {
                            if (!Ext.isEmpty(field.getValue())) {
                                if (evalua && Ext.getCmp('tipoOpeAlta').getValue()) {
                                    Ext.StoreMgr.key('modificatoriosDataStore').load({
                                        params: {
                                            informacion: 'Modificatorios',
                                            num_contratos_mod: field.getValue(),
                                            cbo_if: Ext.getCmp('_cbo_if').getValue()
                                        }
                                    });
                                } else if (totalVarModificatorios > 0) {
                                    if (parseInt(Ext.getCmp('_con_modif').getValue()) < totalVarModificatorios) {
                                        Ext.Msg.alert('Mensaje', 'El n�mero de Contratos Modificatorios no puede ser menor a los ya registrados: ' + totalVarModificatorios);
                                        Ext.getCmp('_con_modif').setValue(totalVarModificatorios);
                                    } else {
                                        Ext.StoreMgr.key('modificatoriosDataStore').load({
                                            params: {
                                                informacion: 'cargaModificatorios',
                                                cbo_if: Ext.getCmp('_cbo_if').getValue(),
                                                inicio: totalVarModificatorios,
                                                filasNew: parseInt(field.getValue()) - totalVarModificatorios
                                            }
                                        });
                                    }
                                } else {
                                    Ext.Ajax.request({
                                        url: '38paramIFNoBancario.data.jsp',
                                        params: {
                                            informacion: 'TotalModificatorios',
                                            cbo_if: Ext.getCmp('_cbo_if').getValue()
                                        },
                                        callback: procesarTotalModificatorios
                                    });
                                }
                            }
                        }
                    }
                }/*, {
                    xtype: 'radiogroup',
                    id: 'tipoOperacion',
                    name: 'tipoOperacion',
                    fieldLabel: 'Tipo de Operaci�n',
                    //hidden:true,
                    items: [{
                            boxLabel: 'AGREGA CONTRATO',
                            name: 'tipoOperaCont',
                            id: 'tipoOpeAlta',
                            inputValue: 'I',
                            checked: true,
                            value: 'I'
                        },
                        {
                            boxLabel: 'ACTUALIZA CONTRATO',
                            name: 'tipoOperaCont',
                            id: 'tipoOpeActu',
                            inputValue: 'U',
                            value: 'U'
                        }
                    ],
                    listeners: {
                        change: function(rgroup, radio) {
                            if (radio.value == 'I') {
                                Ext.getCmp('_nom_contrato').reset();
                                Ext.getCmp('_fec_firma').reset();
                                Ext.getCmp('_fec_ope_elec').reset();
                                Ext.getCmp('_con_modif').reset();
                                modificatoriosData.removeAll();
                                Ext.getCmp('botonAgrContrato').show();
                                //Ext.getCmp('botonModifContrato').hide();
                                Ext.StoreMgr.key('modificatoriosDataStore').load({
                                    params: {
                                        informacion: 'CargaFecMod',
                                        cbo_if: Ext.getCmp('_cbo_if').getValue(),
                                        inicio: 0,
                                        filasNew: 1
                                    }
                                });
                            }
                            if (radio.value == 'U') {
                                Ext.StoreMgr.key('modificatoriosDataStore').load({
                                    params: {
                                        informacion: 'cargaModificatorios',
                                        cbo_if: Ext.getCmp('_cbo_if').getValue(),
                                        inicio: conModifVar,
                                        filasNew: 1
                                    }
                                });

                                Ext.getCmp('_nom_contrato').setValue(nombreContratoVar);
                                Ext.getCmp('_fec_firma').setValue(fecFirmaVar);
                                Ext.getCmp('_fec_ope_elec').setValue(fecOpeElecVar);
                                Ext.getCmp('_con_modif').setValue(conModifVar);
                                //Ext.getCmp('botonModifContrato').show();
                                Ext.getCmp('botonAgrContrato').hide();
                            }
                        }
                    }
                }*/
            ]
        }
        ]}
        ]
    };

    var panelBtnsContrato = {
        xtype: 'panel',
        frame: false,
        border: false,
        bodyStyle: 'background:transparent;',
        layout: 'hbox',
        style: 'margin: 0 auto',
        width: 500,
        height: 75,
        layoutConfig: {
            align: 'middle',
            pack: 'center'
        },
        items: [{
                xtype: 'button',
                hidden: false,
                iconCls: 'icoGuardar',
                text: 'Agregar Contrato',
                id: 'botonAgrContrato',
                handler: function(boton, evento) {
                    /********************** Validaciones ********************/
                    if (Ext.getCmp('_cbo_if').getValue() == '') {
                        Ext.getCmp('_cbo_if').markInvalid("Campo obligatorio");
                    } else if (Ext.getCmp('_nom_contrato').getValue() == '') {
                        Ext.getCmp('_nom_contrato').markInvalid("Campo obligatorio");
                    } else if (Ext.getCmp('_fec_firma').getValue() == '') {
                        Ext.getCmp('_fec_firma').markInvalid("Campo obligatorio");
                    } else if (Ext.getCmp('_fec_ope_elec').getValue() == '') {
                        Ext.getCmp('_fec_ope_elec').markInvalid("Campo obligatorio");
                    } else if (Ext.getCmp('_con_modif').getValue() == '') {
                        Ext.getCmp('_con_modif').markInvalid("Campo obligatorio");
                    } else {
                        accionContrato();
                    }
                }
            },
            {
                xtype: 'box',
                width: 8,
                autoWidth: false
            },
            {
                xtype: 'button',
                hidden: false,
                text: 'Cancelar',
                id: 'botonCancelar',
                handler: function(boton, evento) {
                    /*                                                           
                        var ventana = Ext.getCmp('windowBusquedaAvanzada');
                        if (ventana) {
                            ventana.show();
                        } else {
                            new Ext.Window({
                                layout: 			'fit',
                                width: 			450,
                                height: 			330,
                                minWidth: 		450,
                                minHeight: 		330,
                                modal:			true,
                                title: 			'B�squeda Avanzada',
                                id: 				'windowBusquedaAvanzada',
                                closeAction: 	'hide',
                                items: [
                                    fpBusquedaAvanzada
                                ],
                                listeners: 		{
                                    hide: resetFormaBusquedaAvanzada
                                }
                        }).show();
                        }*/
                }
            },
            {
                xtype: 'box',
                width: 8,
                autoWidth: false
            },
            {
                xtype: 'button',
                hidden: false,
                iconCls: 'icoGuardar',
                text: 'Modificar Contrato',
                id: 'botonModifContrato',
                handler: function(boton, evento) {

                    /********************** Validaciones ********************/
                    if (Ext.getCmp('_cbo_if').getValue() == '') {
                        Ext.getCmp('_cbo_if').markInvalid("Campo obligatorio");
                    } else if (Ext.getCmp('_nom_contrato').getValue() == '') {
                        Ext.getCmp('_nom_contrato').markInvalid("Campo obligatorio");
                    } else if (Ext.getCmp('_fec_firma').getValue() == '') {
                        Ext.getCmp('_fec_firma').markInvalid("Campo obligatorio");
                    } else if (Ext.getCmp('_fec_ope_elec').getValue() == '') {
                        Ext.getCmp('_fec_ope_elec').markInvalid("Campo obligatorio");
                    } else if (Ext.getCmp('_con_modif').getValue() == '') {
                        Ext.getCmp('_con_modif').markInvalid("Campo obligatorio");
                    } else {

                        accionContratoModifica();
                    }
                }
            }           
        ]
    };

    var contratosPanel2 = {
        xtype: 'panel',
        id: 'contratosPanel2',
        items: [{
            layout: 'form',
            labelWidth: 192,
            bodyStyle: 'padding: 2px; padding-right:20px;padding-left:60px;',
            items: []
        }]
    };

    var promocionPanel = {
        xtype: 'panel',
        id: 'promocionPanel',
        items: [{
            layout: 'form',
            labelWidth: 192,
            bodyStyle: 'padding: 2px; padding-right:20px;padding-left:60px;',
            items: [{
                    xtype: 'combo',
                    id: '_cbo_user_promo',
                    hiddenName: 'cbo_user_promo',
                    fieldLabel: 'Seleccionar Usuario',
                    emptyText: 'Seleccione...',
                    width: 280,
                    forceSelection: true,
                    msgTarget: 'side',
                    triggerAction: 'all',
                    mode: 'local',
                    valueField: 'clave',
                    displayField: 'descripcion',
                    store: catalogoPromo
                },
                {
                    xtype: 'textfield',
                    name: 'num_telefono_promo',
                    id: '_num_telefono_promo',
                    fieldLabel: 'Tel�fono',
                    maskRe: /[0-9]/,
                    maxLength: 8,
                    msgTarget: 'side',
                    width: 280,
                    allowBlank: false
                }
            ]
        }]
    };

    var supervisionPanel = {
        xtype: 'panel',
        id: 'supervisionPanel',
        items: [{
            layout: 'form',
            labelWidth: 192,
            bodyStyle: 'padding: 2px; padding-right:20px;padding-left:60px;',
            items: [{
                    xtype: 'combo',
                    id: '_cbo_user_super',
                    hiddenName: 'cbo_user_super',
                    fieldLabel: 'Seleccionar Usuario',
                    emptyText: 'Seleccione...',
                    width: 280,
                    forceSelection: true,
                    msgTarget: 'side',
                    triggerAction: 'all',
                    mode: 'local',
                    valueField: 'clave',
                    displayField: 'descripcion',
                    store: catalogoSupervisor
                },
                {
                    xtype: 'textfield',
                    name: 'num_telefono_super',
                    id: '_num_telefono_super',
                    maskRe: /[0-9]/,
                    fieldLabel: 'Tel�fono',
                    msgTarget: 'side',
                    maxLength: 8,
                    width: 280,
                    allowBlank: false
                }
            ]
        }]
    };


    var horarioPanel = {
        xtype: 'panel',
        id: 'horarioPanel',
        items: [{
            layout: 'form',
            labelWidth: 192,
            bodyStyle: 'padding: 2px; padding-right:20px;padding-left:60px;',
            items: [{
                    xtype: 'textfield',
                    name: 'hr_envio',
                    id: '_hr_envio',
                    fieldLabel: 'Horario envi� hasta',
                    msgTarget: 'side',
                    maxLength: 5,
                    width: 280,
                    listeners: {
                        'change': function(field) {

                            var horaGral = field.getValue();
                            var valor = formatoHora(horaGral);
                            if (!valor)
                                field.setValue('');
                            else
                                field.setValue(valor);
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    name: 'hr_reenvio',
                    id: '_hr_reenvio',
                    fieldLabel: 'Horario reenvi� hasta',
                    msgTarget: 'side',
                    maxLength: 5,
                    width: 280,
                    listeners: {
                        'change': function(field) {
                            var horaGral = field.getValue();
                            var valor = formatoHora(horaGral);
                            if (!valor)
                                field.setValue('');
                            else
                                field.setValue(valor);
                        }
                    }
                }
            ]
        }]
    };

    var Empresarias = {
        xtype: 'panel',
        id: 'EmpresariasPanel',
        items: [{
            layout: 'form',
            labelWidth: 192,
            bodyStyle: 'padding: 2px; padding-right:20px;padding-left:60px;',
            items: [{
                xtype: 'checkbox',
                id: 'id_empresarias',
                name: 'empresarias',
                hiddenName: 'empresarias',
                fieldLabel: 'Mujeres Empresarias',
                tabIndex: 39,
                checked: false,
                enable: true
            }]
        }]
    };

    //---------------------------------radios-------------------------------
    var radios = [{
        xtype: 'radio',
        id: 's',
        boxLabel: 'Si',
        name: 'rbGroup',
        checked: true,
        inputValue: 'S',
        handler: function() {
            var n = Ext.getCmp('n');
            if (!n.getValue()) {
                Ext.getCmp('cmbIFOperado').setVisible(true);
                Ext.getCmp('cmbIFAutorizadorUno').setVisible(true);
                Ext.getCmp('cmbIFAutorizadorDos').setVisible(true);
            }
        }
    }, {
        xtype: 'radio',
        id: 'n',
        boxLabel: 'No',
        name: 'rbGroup',
        inputValue: 'N',
        handler: function() {
            var s = Ext.getCmp('s');
            if (!s.getValue()) {
                Ext.getCmp('cmbIFOperado').setVisible(false);
                Ext.getCmp('cmbIFAutorizadorUno').setVisible(false);
                Ext.getCmp('cmbIFAutorizadorDos').setVisible(false);
            }
        }
    }];
    //---------------------------------radios-------------------------------  

    var firmaMancomunadaPanel = {
        xtype: 'panel',
        id: 'firmaMancomunadaPanel',
        items: [{
            layout: 'form',
            labelWidth: 192,
            bodyStyle: 'padding: 2px; padding-right:20px;padding-left:60px;',
            items: [{
                xtype: 'radiogroup',
                id: 'radioGroup',
                fieldLabel: 'Opera Firma Mancomunada',
                items: radios,
                width: 100,
                columns: 2,
                allowBlank: false
            }, {
                xtype: 'combo',
                id: 'cmbIFOperado',
                name: 'cmbIFOperadoName',
                hiddenName: 'cmbIFOperadoHidden',
                fieldLabel: 'IF Operador',
                emptyText: 'Seleccione...',
                width: 330,
                forceSelection: true,
                //hidden			: false,
                msgTarget: 'side',
                triggerAction: 'all',
                mode: 'local',
                valueField: 'clave',
                displayField: 'descripcion',
                margins: '0 20 0 0',
                allowBlank: true,
                store: ifOperador,
                tpl: NE.util.templateMensajeCargaCombo,
                listeners: {
                    select: {
                        fn: function(combo) {
                            var clave = Ext.getCmp('cmbIFOperado').getValue();
                            Ext.getCmp('cmbIFAutorizadorUno').reset();
                            Ext.getCmp('cmbIFAutorizadorDos').reset();

                            var info = [];
                            var miData = new Array();
                            var row = 0;
                            ifOperador.each(function(record) {
                                row++;
                                var c = record.data.clave;
                                if (c == clave) {

                                } else {
                                    info.push(record.data.clave);
                                    info.push(record.data.descripcion);
                                    miData.push(info);
                                    info = [];
                                }
                            });

                            if (row == 1 || row == 0) {
                                autorizadorDos.removeAll(true);
                                var info = [];
                                var miData = new Array();
                                info.push(0);
                                info.push('No Aplica');
                                miData.push(info);
                                autorizadorDos.loadData(miData);
                            } else if (row > 1) {
                                autorizadorUno.loadData(miData);
                            }

                        } // FIN fn
                    } //FIN select
                } //FIN listeners
            }, {
                xtype: 'combo',
                id: 'cmbIFAutorizadorUno',
                hiddenName: 'cmbIFAutorizadorUnoHidden',
                fieldLabel: 'IF autorizador 1',
                emptyText: 'Seleccione...',
                width: 330,
                forceSelection: true,
                msgTarget: 'side',
                triggerAction: 'all',
                mode: 'local',
                valueField: 'clave',
                displayField: 'descripcion',
                margins: '0 20 0 0',
                allowBlank: true,
                store: autorizadorUno,
                listeners: {
                    select: {
                        fn: function(combo) {

                            Ext.getCmp('cmbIFAutorizadorDos').reset();
                            var clave = Ext.getCmp('cmbIFAutorizadorUno').getValue();

                            var info = [];
                            var miData = new Array();
                            var row = 0;
                            autorizadorUno.each(function(record) {
                                row++;
                                var c = record.data.clave;
                                if (c == clave) {

                                } else {
                                    info.push(record.data.clave);
                                    info.push(record.data.descripcion);
                                    miData.push(info);
                                    info = [];
                                }
                            });
                            if (row > 0) {
                                info.push(0);
                                info.push('No Aplica');
                                miData.push(info);
                                autorizadorDos.loadData(miData);
                            }


                        } // FIN fn
                    } //FIN select
                } //FIN listeners
            }, {
                xtype: 'combo',
                id: 'cmbIFAutorizadorDos',
                hiddenName: 'cmbIFautorizadorDosHidden',
                fieldLabel: 'IF Autorizador 2',
                emptyText: 'Seleccione...',
                width: 330,
                forceSelection: true,
                msgTarget: 'side',
                triggerAction: 'all',
                mode: 'local',
                valueField: 'clave',
                displayField: 'descripcion',
                margins: '0 20 0 0',
                allowBlank: true,
                store: autorizadorDos
            }]
        }]
    };


    var fpDatos = new Ext.form.FormPanel({
        id: 'formaAfiliacion',
        layout: 'form',
        width: 920,
        style: ' margin:0 auto;',
        frame: true,
        collapsible: false,
        titleCollapse: false,
        title: 'Datos del Intermediario Financiero',
        bodyStyle: 'padding: 16px',
        defaults: {
            msgTarget: 'side',
            anchor: '-20'
        },
        items: [{
                layout: 'hbox',
                title: '',
                width: 920,
                items: [afiliacionPanel]
            },
            {
                layout: 'hbox',
                title: 'Contratos y Modificatorios',
                id: 'hboxConMod',
                padding: '10 20 0 0',
                width: 920,
                bodyStyle: 'padding-top:10px;',
                height: 130,
                items: [contratosPanel]
            },/* {
                layout: 'hbox',
                title: '',
                width: 920,
                items: [panelBtnsContrato]
            },*/
            {
                layout: 'hbox',
                id: 'hboxGridModificatorios',
                title: '',
                width: 800,
                height: 0,
                bodyStyle: 'padding-left:60px;',
                items: [gridModificatorios]
            },
            {
                layout: 'hbox',
                title: '',
                padding: '15 20 0 0',
                width: 920,
                items: [contratosPanel2]
            },
            {
                layout: 'hbox',
                title: 'Promoci�n',
                padding: '10 20 0 0',
                width: 920,
                items: [promocionPanel]
            },
            {
                layout: 'hbox',
                title: 'Supervisi�n y Seguimiento de Cr�dito',
                padding: '10 20 0 0',
                width: 920,
                items: [supervisionPanel]
            },
            {
                layout: 'hbox',
                title: 'Horario de Servicio',
                padding: '10 20 0 0',
                width: 920,
                items: [horarioPanel]
            },
            {
                layout: 'hbox',
                title: 'Firma Mancomunada',
                padding: '10 20 0 0',
                width: 920,
                items: [firmaMancomunadaPanel]
            },
            {
                layout: 'hbox',
                title: 'Tipo de Producto',
                padding: '10 20 0 0',
                width: 920,
                items: [Empresarias]
            }
        ],
        monitorValid: true,
        buttons: [{
            text: 'Guardar',
            id: 'btnGuardar',
            formBind: true,
            iconCls: 'icoGuardar',
            handler: function(boton, evento) {
                /********************** Validaciones ********************/

                if (Ext.getCmp('s').getValue()) {
                    if (Ext.getCmp('cmbIFOperado').getValue() != "") {} else {
                        Ext.getCmp('cmbIFOperado').markInvalid("Campo obligatorio");
                        return;
                    }
                    if (Ext.getCmp('cmbIFAutorizadorUno').getValue() != "") {} else {
                        Ext.getCmp('cmbIFAutorizadorUno').markInvalid("Campo obligatorio");
                        return;
                    }
                }

                if (Ext.getCmp('_cbo_if').getValue() == '' || Ext.getCmp('_num_cuenta').getValue() == '' || Ext.getCmp('_banco').getValue() == '' ||
                    Ext.getCmp('_cu_clabe').getValue() == '' || Ext.getCmp('_fec_firma').getValue() == '' || Ext.getCmp('_nom_contrato').getValue() == '' ||
                    Ext.getCmp('_fec_ope_elec').getValue() == '' || Ext.getCmp('_cbo_user_promo').getValue() == '' || Ext.getCmp('_num_telefono_promo').getValue() == '' ||
                    Ext.getCmp('_cbo_user_super').getValue() == '' || Ext.getCmp('_num_telefono_super').getValue() == '') {
                    if (Ext.getCmp('_cbo_if').getValue() == '')
                        Ext.getCmp('_cbo_if').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_num_cuenta').getValue() == '')
                        Ext.getCmp('_num_cuenta').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_banco').getValue() == '')
                        Ext.getCmp('_banco').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_cu_clabe').getValue() == '')
                        Ext.getCmp('_cu_clabe').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_fec_firma').getValue() == '')
                        Ext.getCmp('_fec_firma').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_nom_contrato').getValue() == '')
                        Ext.getCmp('_nom_contrato').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_fec_ope_elec').getValue() == '')
                        Ext.getCmp('_fec_ope_elec').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_cbo_user_promo').getValue() == '')
                        Ext.getCmp('_cbo_user_promo').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_num_telefono_promo').getValue() == '')
                        Ext.getCmp('_num_telefono_promo').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_cbo_user_super').getValue() == '')
                        Ext.getCmp('_cbo_user_super').markInvalid("Campo obligatorio");
                    else if (Ext.getCmp('_num_telefono_super').getValue() == '')
                        Ext.getCmp('_num_telefono_super').markInvalid("Campo obligatorio");

                } else {
                    if (evalua)
                        accionConsulta("CONSULTAR", null)
                    else
                        accionConsulta("ACTUALIZAR", null)
                }
            }

        }]
    });

    //--------------------------- CONTENEDOR -----------------------------
    var pnl = new Ext.Container({
        id: 'contenedorPrincipal',
        applyTo: 'areaContenido',
        width: 'auto',
        height: 'auto',
        items: [
            fpDatos,
            NE.util.getEspaciador(10)
        ]
    });

    Ext.getCmp('_con_modif').setVisible(false);
    //Ext.getCmp('botonModifContrato').setVisible(false);
    //Ext.getCmp('tipoOpeActu').setVisible(false);
    catalogoIF.load();
    catalogoPromo.load();
    catalogoSupervisor.load();

    /** Muestra u oculta los componentes de los Contratos y Modificatorios
     *  toogle = true  -> Mostrar
     *           false -> Oultar
     * */
    function mostrarGridsContratosYModificatorios(toogle){
        var hContratos = Ext.getCmp('hboxGridContratos');
        var hModificatorios = Ext.getCmp('hboxGridModificatorios');
        var panelConratos = Ext.getCmp('contratosPanel');
        var elgridContratos = Ext.getCmp('gridContrato');
        var hbConMod = Ext.getCmp('hboxConMod');
        hbConMod.setHeight(toogle ? 185 : 135);
        //panelConratos.setHeight(toogle ? 145 : 145);
        //hContratos.setHeight(toogle ? 130 : 130);
        
        hModificatorios.setHeight(toogle ? 165 : 0);
        elgridContratos.setVisible(toogle);
        hbConMod.doLayout();
    }   
    

});