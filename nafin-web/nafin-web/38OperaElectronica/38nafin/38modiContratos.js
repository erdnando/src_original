Ext.onReady(function(){

	function procesarSuccessFailureGuardar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			Ext.Msg.alert('Mensaje','"El contrato se ha guardado con �xito"');	
			registrosActuales.load();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){
		
		if(  estadoSiguiente == "GUARDAR" ){
			var bandera = true;
			var arr = new Array();
			for(var i=0; i<registrosActuales.getTotalCount(); i++){
					var reg = registrosActuales.getAt(i);
					if(reg.get('NOM_CONT')=='' || reg.get('NOM_CONT').length>200){
						
						if(reg.get('NOM_CONT')=='')
							Ext.Msg.alert('Alerta','Es necesario capturar un nombre para el contrato');
						else if(reg.get('NOM_CONT').length>200){
							Ext.Msg.alert('Alerta','El nombre del contrato no puede ser mayor a 200 caracteres');
							//Ext.getCmp('_txtNomContrato').markInvalid("El tama�o maximo para este campo es 200");
						}
						bandera=false;
						break;
					}else {
						arr[i]=reg.get('IC_CONT')+'�'+reg.get('NOM_CONT');
					}
			}			
			if(bandera){
				Ext.Msg.confirm('', '�Desea guardar el siguiente contrato?',	function(botonConf) {
						if (botonConf == 'ok' || botonConf == 'yes') {
							Ext.Ajax.request({
								url: '38modiContratos.data.jsp',
								params:
									{
										informacion:'GuardarDatos',
										arr: arr
									},
								callback: procesarSuccessFailureGuardar
							});
						}
				});
			}
		}
		
	}

//--------------------------- STORE�S -----------------------------

	var registrosActuales = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registroActualesDataStore',
			url: 			'38modiContratos.data.jsp',
			baseParams: {	informacion:	'CargarDatos'	},
			fields: [
				{ name: 'IC_CONT'},
				{ name: 'PERFIL_IF'},
				{ name: 'DESC_RIESGO'},
				{ name: 'NOM_CONT'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					}
				}
			}
	});
	
//--------------------------- COMPONENTES -----------------------------
	
	var grid = new Ext.grid.EditorGridPanel
	({
		title      : 'Modificar Contratos',
		id 		  : 'grid',
		clicksToEdit:1,
		store 	  : registrosActuales,
		style 	  : 'margin:0 auto;',
		stripeRows : true,
		loadMask	  : true,
		hidden	  : false,
		width		  : 870,
		height	  : 210,
		frame		  : false, 
		header	  : true,
		columns    : [
			new Ext.grid.RowNumberer(),
			{
				header		: 'Perfil IF',
				tooltip		: 'Perfil IF',			
				dataIndex	: 'PERFIL_IF',	
				sortable		:true,	
				resizable	:true,	
				width			:110
			},
			{
				header		: 'Descripci�n Clasificaci�n de Riesgo',
				tooltip		: 'Descripci�n Clasificaci�n de Riesgo',			
				dataIndex	: 'DESC_RIESGO',	
				sortable		:true,	
				resizable	:true,	
				width			:210
			},
			{
				header		: 'Nombre del Contrato',
				tooltip		: 'Nombre del Contrato',		
				editor: {
					xtype: 'textfield',
					id: '_txtNomContrato',
					name: 'txtNomContrato'//,
					//maxLength:200
				},
				dataIndex	: 'NOM_CONT',
				resizable	: true,	
				width			:523,
				renderer:function(value,metadata,registro, rowIndex, colIndex){
								return NE.util.colorCampoEdit(value,metadata,registro);			
				}
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Guardar',
					id: 'btnGuardar',
					iconCls: 'icoGuardar',
					handler: function(boton, evento){
									//boton.disable();
									accionConsulta("GUARDAR", null);
					}
				},'-'
			]
		}
	});
	
	
//--------------------------- CONTENEDOR -----------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items:
		[
			NE.util.getEspaciador(15),
			grid
			
		]
	});
	
	registrosActuales.load();

});