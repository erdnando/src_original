<%@page 
	errorPage="/00utils/error.jsp"
%>
<%@include file="/appComun.jspf"%>
<%
String perfil = (request.getParameter("perfil")==null)?"":request.getParameter("perfil");	//EPO,PYME,IF,NAFIN
String claveEpo = (request.getParameter("claveEpo")==null)?"":"claveEpo=" + request.getParameter("claveEpo");  //ic_epo
String paginaInicial = "";
if (perfil.equals("EPO")) {
	paginaInicial = "seccion.jsp?seccion=2&" + claveEpo;
} else if (perfil.equals("PYME")) {
	paginaInicial = "seccion.jsp?seccion=1&" + claveEpo;
} else if (perfil.equals("IF")) {
	paginaInicial = "seccion.jsp?seccion=5&";
} else if (perfil.equals("EPOG")) {
	paginaInicial = "seccion.jsp?seccion=3&" + claveEpo;
} else if (perfil.equals("EPOEDO")) {
	paginaInicial = "seccion.jsp?seccion=4&" + claveEpo;
} else {
	paginaInicial = "seccion.jsp?seccion=1&" + claveEpo;
}

response.sendRedirect(paginaInicial);
%>