<%@page import="com.nafin.filter.UtilXSSSanatizer"%>
<%@page 
	errorPage="/00utils/error.jsp"
%>
<%@include file="/appComun.jspf"%>
<%
String sClaveSeccion = (request.getParameter("seccion")==null)?"":UtilXSSSanatizer.HTMLFullEncode(request.getParameter("seccion"));
int claveSeccion = Integer.parseInt(sClaveSeccion);

StringBuffer params = new StringBuffer();
params.append("?");
String claveEpo = (request.getParameter("claveEpo")==null)?"":UtilXSSSanatizer.HTMLFullEncode(request.getParameter("claveEpo"));  //ic_epo
String logo = "";
if (!claveEpo.equals("")) {
	params.append("claveEpo=" + claveEpo + "&");
	logo = appWebContextRoot + "/00archivos/15cadenas/15archcadenas/logos/" + claveEpo + ".gif";
}
%>
<%@ include file="cabeza.jspf" %>
	<div id="logo_cliente" class="left">
<%
if (!claveEpo.equals("")) {
%>
		<img src="<%=logo%>"/>
<%
}
%>
	</div>
	<%@ include file="menu.jspf" %>
</div>

<div id="content">
<%
	com.netro.admcontenido.SeccionContenido sec = new com.netro.admcontenido.SeccionContenido();
	sec.setClaveSeccion(sClaveSeccion);
	netropology.utilerias.Registros reg = sec.getContenidoSeccion();
	int numRegistros = reg.getNumeroRegistros();
	int numRegNuevos = 0;
	java.util.List numRnd = new java.util.ArrayList();
	
	while (reg.next()) {
		if ("S".equals(reg.getString("cs_nuevo"))){
			numRegNuevos++;
		}
	}
	reg.rewind();

	if (numRegistros > 1) {
		for (int g = numRegNuevos; g < numRegistros; g++) {
			numRnd.add(new Integer(g));
		}
		java.util.Collections.shuffle(numRnd);
	}
	
	java.util.List lSeccion = new java.util.ArrayList();
	for (int x = 0; x < numRegNuevos; x++) {
		lSeccion.add(new Integer(x));
	}
	
	lSeccion.addAll(numRnd);
	
	if( numRegistros > 6) {
%>
	<div id="smup" class="alpha" style="height:10px; background-color:transparent; width:240px; position:relative; top:0px; left:0px;cursor:pointer;text-align:center; border-bottom: 1px solid #AAAAAA; overflow:hidden">
		<img src="img/up.gif" style="vertical-align: top; padding-top:3px"/>
	</div>
<%
	}
%>
	<div class="menu_izquierdo">
		<div class="submenu">
<%
	for(int iReg=0; iReg < lSeccion.size(); iReg++) {
		reg.setNumRegistro(((Integer)lSeccion.get(iReg)).intValue());
%>
		<div class="border"><span id='info<%=reg.getString("ic_opcion")%>' class='submenu_pymes <%=(iReg==0)?"active":""%>'><%=reg.getString("cg_descripcion")%>
<%
		if ("S".equals(reg.getString("cs_nuevo"))) {
%>
		<span class="new"/>&nbsp;</span>
<%
		}
%>
		</span></div>
<%
	}
%>
		</div>


	</div>
<%
	if(reg.getNumeroRegistros() > 6) {
%>
	<div id="smdown" style="height:10px; background-color:transparent; width:240px; position:relative; top:0px; left:0px;cursor:pointer;text-align:center; border-top: 1px solid #AAAAAA; overflow:hidden">
		<img src="img/down.gif" style="vertical-align: top; padding-top:2px"/>
	</div>
<script language="JavaScript">
var container_height = $('.menu_izquierdo').css('height');
if (container_height) {
	container_height=1*container_height.replace('px','');
}

var scroll_height = $('.submenu').css('height');
if (scroll_height) {
	scroll_height=1*scroll_height.replace('px','');
}

var margin=$('.submenu').css('margin-top');
if (margin) {
	if(margin == 'auto') {
		margin = 0;
	} else {
		margin=1*margin.replace('px','');
	}
}
$('#smdown').click(function(){
	if((container_height-margin)>scroll_height)
		return;
	else{
		$('#smdown').removeClass('alpha');
		$('#smup').removeClass('alpha');
	}
	margin=margin-50;
	$('.submenu').css('margin-top',margin+'px');
	if((container_height-margin)>scroll_height)
		$('#smdown').addClass('alpha');
});

$('#smup').click(function(){
	if(margin==0)
		return;
	else{
		$('#smdown').removeClass('alpha');
		$('#smup').removeClass('alpha');
	}
	
	margin=50+margin;
	$('.submenu').css('margin-top',margin+'px');
	
	if(margin==0)
		$('#smup').addClass('alpha');
});

</script>
<%
	}
%>

	<div class="slider">
		<img id="img_content" src="img/menu/<%=claveSeccion%>.jpg"/>
	</div>
	<div style="position:absolute; overflow: hidden; width:500px; height:150px; top:168px; left:240px; ">
<%
	reg.rewind();
	while(reg.next()) {
%>
	<div class="detalle" id='detinfo<%=reg.getString("ic_opcion")%>'>
		<span class="title"><%=reg.getString("cg_titulo")%></span>
		<ul>
<%
		sec.setClaveOpcion(reg.getString("ic_opcion"));
		netropology.utilerias.Registros regDet = sec.getContenidoSeccionDetalle();
		while(regDet.next()) {
%>
			<li><%=regDet.getString("cg_descripcion")%></li>
<%
		}
%>
<%--			<li>dummy 1</li>
			<li>dummy 2</li>
			<li>dummy 3</li>
			<li>dummy 4</li>
			<li>dummy 5</li>
			<li>dummy 6</li>
			<li>dummy 7</li>
			<li>dummy 8</li>
			<li>dummy 9</li>
			<li>dummy A</li>
			<li>dummy B</li>
			<li>dummy C</li>
			<li>dummy D</li>
			<li>dummy E</li>
			<li>dummy F</li> --%>
		</ul>
	</div>


<%
	} //fin while
%>
		<div style="position:absolute;top:5px;left:480px;height:155px;width:20px;">
			<div class="scroll_container" id="scrollDetalle_container">
				<span class="up alpha" id="scrollDetalle_up"></span>
				<span class="down" id="scrollDetalle_down"></span>
			</div>
		</div>

	</div>
	
	
<script language="Javascript">
$(document).ready(function(){

	$('#scrollDetalle_down').click(function(){
		if((detalleScrollHeight-detalleMargin)>=detalleScrollHeight*2-150)
			return;
		else{
			$('.down').removeClass('alpha');
			$('.up').removeClass('alpha');
		}
	
		detalleMargin=detalleMargin-50;
		$(detalleEnTurno).css('margin-top',detalleMargin+'px');
	
		if((detalleScrollHeight-detalleMargin)>=detalleScrollHeight*2-150)
			$('.down').addClass('alpha');
	});
	
	$('#scrollDetalle_up').click(function(){
		if(detalleMargin==0)
			return;
		else{
			$('.down').removeClass('alpha');
			$('.up').removeClass('alpha');
		}
	
		detalleMargin=50+detalleMargin;
		$(detalleEnTurno).css('margin-top',detalleMargin+'px');
	
		if(detalleMargin==0)
			$('.up').addClass('alpha');
	});
});
</script>

<%@ include file="banners.jspf" %>
</div>
<%@ include file="pie.jspf" %>