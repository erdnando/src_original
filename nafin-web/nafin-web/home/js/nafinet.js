var detalleEnTurno = 0;
var detalleScrollHeight = 0;
var detalleMargin = 0;
var enlacesBanners = ["", 
                      "http://www.creditojoven.gob.mx",                                                         // 1 Credito Joven
                      "http://www.nafin.com/portalnf/content/financiamiento/mujeres-empresarias.html",          // 2 Mujeres empresarias  
                      "http://www.nafin.com/portalnf/content/financiamiento/crecer-juntos.html",               // 3 Crecer juntos        
                      "http://www.nafin.com/portalnf/content/financiamiento/financiamiento_construccion.html ",  // 4 Industria de la construcción 
                    ];
$(document).ready(function(){
        $('.marquee').bxSlider({
            mode: 'fade',
            captions: true,
            auto: true,
            controls: false
          });

       /* $('.submenu div span').click(function(){
                $('.detalle').fadeOut('slow');
                $('.detalle').fadeIn('slow');
            });*/
        
        $('.banner .item').click(function(event){
                var claveEpo = $('#claveEpo').val();
					 var params = "";
					 if (claveEpo) {
						params = "&claveEpo=" + claveEpo;
					 }
					 var bannerId = event.target.id;
					 if (bannerId == "") {
						bannerId = event.target.parentNode.id
					 }
                var ArraynumBannerId = bannerId.split('_');
                var numBannerId = ArraynumBannerId[1];
                if(numBannerId > 4){
                    window.location='banners.jsp?id=' + bannerId + params;
                }
                else{
                    //window.location = enlacesBanners[numBannerId];
                    window.open(enlacesBanners[numBannerId], "_blank");
                }
            });
        
        $('.item').hide();
        $('#item1').show();
        
        $('.num').mouseover(function(){
                var id=$(this).attr('id');
                var num=$(this).attr('rel');
                $('.item').hide();
                $('#item'+num).show();
        });
        
        if($('.detalle').length > 0) {
            $('.detalle').hide();
            $('#det' + $('.submenu_pymes.active')[0].id).fadeIn('slow');
				
				var generarScrollDetalle = function() {
			
					detalleEnTurno = "#det" + $('.submenu_pymes.active')[0].id;
					detalleScrollHeight=$(detalleEnTurno).css('height');
					if (detalleScrollHeight) {
						detalleScrollHeight=1*detalleScrollHeight.replace('px','');
					}
					
				  if(detalleScrollHeight<=150){
						 $('#scrollDetalle_container').hide();
					}else{
						$('#scrollDetalle_container').show();
						detalleMargin=0;
						$('#scrollDetalle_up').addClass('alpha');
						$('#scrollDetalle_down').removeClass('alpha');
					}
				}
				generarScrollDetalle();

				
            $('.submenu div span').click(function(){
					$('.submenu div span').removeClass('active');
					$(this).addClass('active');
					$('.detalle').hide();
					$('#det' + this.id).fadeIn('slow');
					detalleEnTurno = "#det" + this.id;
					detalleScrollHeight=$(detalleEnTurno).css('height');
					if (detalleScrollHeight) {
						detalleScrollHeight=1*detalleScrollHeight.replace('px','');
					}
					detalleMargin = 0;
					$(detalleEnTurno).css('margin-top',detalleMargin+'px');
					generarScrollDetalle(); //Si es necesario genera scroll
				});
        }
        
        var scroll_height=$('.text_scroll').css('height');
        if (scroll_height) {
            scroll_height=1*scroll_height.replace('px','');
        }
        if(scroll_height<=200){
                $('.scroll_banner').hide();
        }else{
                var margin=$('.text_scroll').css('margin-top');
                if (margin) {
                    margin=1*margin.replace('px','');
                }
                
                $('#scroll_down').click(function(){
                                if((scroll_height-margin)>=860)
                                        return;
                                else{
                                        $('#scroll_down').removeClass('alpha');
                                        $('#scroll_up').removeClass('alpha');
                                }
                                
                                margin=margin-50;
                                $('.text_scroll').css('margin-top',margin+'px');
                                
                                if((scroll_height-margin)>=860)
                                        $('#scroll_down').addClass('alpha');
                        });
                
                $('#scroll_up').click(function(){
                        if(margin==0)
                                return;
                        else{
                                $('#scroll_down').removeClass('alpha');
                                $('#scroll_up').removeClass('alpha');
                        }
                        
                                margin=50+margin;
                                $('.text_scroll').css('margin-top',margin+'px');
                                
                                if(margin==0)
                                        $('#scroll_up').addClass('alpha');
                        });
        }
    });