<%@page 
	errorPage="/00utils/error.jsp" 
	import="com.netro.admcontenido.*, netropology.utilerias.*"
%>
<%@include file="/appComun.jspf"%>
<%
int claveSeccion = 0;
StringBuffer params = new StringBuffer();
params.append("?");
String claveEpo = (request.getParameter("claveEpo")==null)?"":request.getParameter("claveEpo");  //ic_epo
String id = (request.getParameter("id")==null)?"":request.getParameter("id");  //banner
String claveBanner = id.substring(id.lastIndexOf("_") + 1);
claveBanner = "6"; // Los otros banners ahora son enlaces directos
String nombreImg = "banner" + claveBanner + "l";
if (!claveEpo.equals("")) {
	params.append("claveEpo=" + claveEpo + "&");
}
%>
<%@ include file="cabeza.jspf" %>
                    <%@ include file="menu.jspf" %>
                </div>

                <div id="content">
                    <div class="contenido_banner" style="background:url(img/Banners/<%=nombreImg%>.jpg) no-repeat;">
                        <div class="texto_banner">
                            <div class="text_scroll">
<%--<p><%=nombreImg%> --- <%=claveBanner%><hr>--%>
<%
Banner banner = new Banner();
banner.setClaveBanner(claveBanner);
Registros reg = banner.getBanner();
while (reg.next()) {
%>
                                <h3><%=reg.getString("cg_titulo")%></h3>
                                <p>
										  <%=reg.getString("cg_texto").replaceAll("(\r\n|\n)", "<br />")%>
                                </p>
<%
}
%>
                            </div>
                        </div>
                        <div class="scroll_banner">
                            <div class="scroll_container">
                                <span class="up alpha" id="scroll_up"></span>
                                <span class="down" id="scroll_down"></span>
                            </div>
                        </div>
                    </div>
                    
                    <%@ include file="banners.jspf" %>
                </div>
                <%@ include file="pie.jspf" %>