<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	net.sf.json.JSONArray,	
	net.sf.json.JSONObject,
	netropology.utilerias.*,
	netropology.utilerias.usuarios.*,
	java.sql.*,
	java.io.File,
	com.netro.seguridad.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String no_operacion = (request.getParameter("no_operacion") != null) ? request.getParameter("no_operacion") : "";
String remitente = (request.getParameter("remitente") != null) ? request.getParameter("remitente") : "";
String destinatario = (request.getParameter("destinatario") != null) ? request.getParameter("destinatario") : "";
String ccdestinatario = (request.getParameter("ccdestinatario") != null) ? request.getParameter("ccdestinatario") : "";
String asunto = (request.getParameter("asunto") != null) ? request.getParameter("asunto") : "";
String cuerpoCorreo = (request.getParameter("cuerpoCorreo") != null) ? request.getParameter("cuerpoCorreo") : "";
String urlArchivo = (request.getParameter("urlArchivo") != null) ? request.getParameter("urlArchivo") : "";
String ic_bitacora = (request.getParameter("ic_bitacora") != null) ? request.getParameter("ic_bitacora") : "";
String actualizar = (request.getParameter("actualizar") != null) ? request.getParameter("actualizar") : "";
String fechaCorte = (request.getParameter("fechaCorte") != null) ? request.getParameter("fechaCorte") : "";
String id_producto = (request.getParameter("id_producto") != null) ? request.getParameter("id_producto") : "1";
if(id_producto.equals("")) { id_producto = "1"; }

  
	
SimpleDateFormat fecha_hoy = new SimpleDateFormat("dd/MM/yyyy");
String fechaDiaHoy = fecha_hoy.format(new java.util.Date());
String  horaCorte =  (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());

String nombreArchivo = "", nombreProducto ="", infoRegresar ="",consulta ="";
JSONObject jsonObj = new JSONObject();


AvisosNafin paginador = new AvisosNafin (); 
paginador.setTipoConsulta(operacion);
paginador.setNo_operacion(no_operacion);
paginador.setIc_bitacora(ic_bitacora);  
paginador.setActualizarCorte(actualizar); 
paginador.setId_producto(id_producto);  


CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

if (informacion.equals("valoresIniciales")  ) {


	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("strPerfil", strPerfil);
	infoRegresar = jsonObj.toString();  
	
}else  if(informacion.equals("ConsultarServicios"))  {

	Registros reg	=	queryHelper.doSearch();    
	while(reg.next()){
		
		String cs_cierre_servicio = (reg.getString("cs_cierre_servicio") == null) ? "" : reg.getString("cs_cierre_servicio");
		String cs_cierre_servicio_if = (reg.getString("cs_cierre_servicio_if") == null) ? "" : reg.getString("cs_cierre_servicio_if");
		String cs_cierre_servicio_nafin = (reg.getString("cs_cierre_servicio_nafin") == null) ? "" : reg.getString("cs_cierre_servicio_nafin");
		
		if( !cs_cierre_servicio.equals("N") ||  !cs_cierre_servicio_if.equals("N") || !cs_cierre_servicio_nafin.equals("N") ) {
			reg.setObject("SERVICIO","Servicio Cerrado");	
		}		
	}	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();  


}else  if(informacion.equals("ConsResumenOperaDiaria") )  {
	
	if(operacion.equals("inicializa"))  {		
		consulta	=	"{\"success\": true, \"total\": \""	+	"0" + "\", \"registros\": " + "0"+ "}";			
	}else  {		
		Registros reg	=	queryHelper.doSearch(); 
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";	
	}
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();  

}else  if(informacion.equals("InicializaMonitoreoDiario") )  {

	StringBuffer lbl_Dispersiones = new StringBuffer();
	StringBuffer lbl_Tasas = new StringBuffer();
	StringBuffer lbl_Operacion = new StringBuffer();
	StringBuffer lbl_creElectronico = new StringBuffer();
	StringBuffer lbl_creElectronicoMN = new StringBuffer();
  	
	//	Dispersiones 
	
	String totalRegDisp = paginador.getDispersiones ();
	if(!totalRegDisp.equals("0") )  {
	lbl_Dispersiones.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
				" <tr> <td align='justify' class='formas'>"+
				" <br>Existen Dispersiones pendientes de enviar al FFON de "+totalRegDisp +" Cadenas Productivas, con fecha de vencimiento "+fechaDiaHoy+".<br> Por favor verifique. "+
				" </td></tr><table> <br>");				
	}else  {	
		lbl_Dispersiones.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
				" <tr> <td align='justify' class='formas'>"+
				" <br>No se han registrado Dispersiones para ninguna Cadena Productiva."+
				" </td></tr><table> <br>");
	}
		
	// Reducción de Tasas  
	HashMap	datosTasas =  paginador.getReduccionTasas();  
	
	if(!datosTasas.get("EPOS_1").toString().equals("") )  {
		lbl_Tasas.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
				" <tr> <td align='justify' class='formas'>"+
				" <br>Las siguientes Cadenas presentan operación el día de hoy y deben considerarse para la reducción de tasas: ");				
				lbl_Tasas.append(" <br>"+datosTasas.get("EPOS_1") ) ;
				lbl_Tasas.append(" </td></tr><table> <br>");
	}else {
		lbl_Tasas.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
				" <tr> <td align='justify' class='formas'>"+
				" <br> Las "+ datosTasas.get("EPOS_2")+"  aún no han registrado operaciones."	+
				" </td></tr><table> <br>");
	}

	//	Operación Entrega Continua
	HashMap	datosOperacion =  paginador.getEntregaContinua();  
	
	if(!datosOperacion.get("TOTAL").toString().equals("0") )  {
		lbl_Operacion.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
					" <tr> <td align='justify' class='formas'>"+
					" <br>Existen  "+ datosOperacion.get("TOTAL")+ " operaciones de "+ datosOperacion.get("NOMBRE_EPO")+ " por un monto de $"+ Comunes.formatoDecimal(datosOperacion.get("MONTO").toString(),2)+ " , mismas <br> que deberán ser operadas "+
					" </td></tr><table> <br>");
	 }else  {
		lbl_Operacion.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
					" <tr> <td align='justify' class='formas'>"+
					" <br>No hay operaciones a notificar de la Cadena "+ datosOperacion.get("NOMBRE_EPO") +
					" </td></tr><table> <br>"); 
	 }
  
  //Operación Crédito Electrónico  Dolares
	String totalRegDL =  paginador.getCreditoElectronico ("54");
	
	if(!totalRegDL.equals("0") )  {
		lbl_creElectronico.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
					" <tr> <td align='justify' class='formas'>"+
					" <br>Se registraron "+totalRegDL+" operaciones de Crédito Electrónico en Dólares con Tasa cotizada, mismas que deberán enviarse a operaciones. "+
					" </td></tr><table> <br>");
	}else  {
		lbl_creElectronico.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
					" <tr> <td align='justify' class='formas'>"+
					" <br>No se han registrado operaciones en Dólar con Tasa cotizada."+
					" </td></tr><table> <br>");
	
	}
	
	//Operación Crédito Electrónico  Moneda Nacional
	String totalRegMN =  paginador.getCreditoElectronico ("1");	
	
	if(!totalRegMN.equals("0") )  {
		lbl_creElectronicoMN.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
					" <tr> <td align='justify' class='formas'>"+
					" <br>Se registraron  "+totalRegMN+" operaciones de Crédito Electrónico en Moneda Nacional con Tasa cotizada, mismas que deberán enviarse <br>a operaciones."+
					" </td></tr><table> <br>");
	}else  {
		lbl_creElectronicoMN.append("<table align='center'  style='text-align: justify;' border='0' width='780' cellpadding='0' cellspacing='0'> "+
					" <tr> <td align='justify' class='formas'>"+
					" <br>No se han registrado operaciones en Moneda Nacional con Tasa cotizada."+
					" </td></tr><table> <br>");	
	}
				
				
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("lbl_Dispersiones", lbl_Dispersiones.toString());	
	jsonObj.put("lbl_Tasas", lbl_Tasas.toString());	
	jsonObj.put("lbl_Operacion", lbl_Operacion.toString());	
	jsonObj.put("lbl_creElectronico", lbl_creElectronico.toString());	
	jsonObj.put("lbl_creElectronicoMN", lbl_creElectronicoMN.toString());	
	infoRegresar = jsonObj.toString(); 

}else  if(informacion.equals("ConsCortes") )  {
	 
	if(operacion.equals("inicializa"))  {
		consulta	=	"{\"success\": true, \"total\": \""	+	"0" + "\", \"registros\": " + "0"+ "}";			
	}else  {
	 
		fechaCorte = (new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss")).format(new java.util.Date()); 
	
		JSONArray registros = new JSONArray();	
		List  reg =  paginador.getGenerarCorte ();	 
		registros = registros.fromObject(reg);
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";	
	}
	 
	//Obtener la ultima fecha de Corte 
	String   ultimaFecha = paginador.getUltimaFechaCorte(""); 
	String leyendaBarraCorte = "Información obtenida de las  "+ultimaFecha+"  a las "+horaCorte +" del día de hoy";
	List  reg =  paginador.getGenerarCorte ();

	jsonObj = JSONObject.fromObject(consulta);	
	jsonObj.put("leyendaBarraCorte", leyendaBarraCorte);		
	jsonObj.put("operacion", operacion);	
	jsonObj.put("fechaCorte", fechaCorte);		
	infoRegresar = jsonObj.toString();  
	
}else  if(informacion.equals("DatosCorreo") )  {
	
	Registros reg	=	queryHelper.doSearch(); 
	String cuerpoCorreo1 =  "";
	if(reg.next()){
			
		destinatario = (reg.getString("CG_DESTINATARIO") == null) ? "" : reg.getString("CG_DESTINATARIO");
		ccdestinatario = (reg.getString("CG_CCDESTINATARIO") == null) ? "" : reg.getString("CG_CCDESTINATARIO");		
		asunto = (reg.getString("CG_ASUNTO") == null) ? "" : reg.getString("CG_ASUNTO");
		cuerpoCorreo = (reg.getString("CG_CUERPO_CORREO") == null) ? "" : reg.getString("CG_CUERPO_CORREO");
		cuerpoCorreo1=	cuerpoCorreo.replaceAll("\n","<br>");
		
	}
	
	// Agregar la fecha del día actual al Asunto
	if( asunto.matches(".*\\S$") ){
		asunto += " ";
	}
	asunto += Fecha.getFechaActual();
	
	// Para el nombre del archivo xlsx
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = fechaActual.substring(3,5);		
	String anioActual   = fechaActual.substring(6,10);
	String fecha =   diaActual+ mesActual+  anioActual; 
		
	if(no_operacion.equals("1")) { 	operacion = "DescuentoElectronico";  nombreProducto ="Operacion_Desc_"+fecha;  }
	if(no_operacion.equals("2")) { 	operacion = "Distribuidores";   nombreProducto ="Operacion_Dis_"+fecha;  }
	if(no_operacion.equals("3")) { 	operacion = "CreditoElectronico";  nombreProducto ="Operacion_CredElec_"+fecha; }
	
	if(no_operacion.equals("4") || no_operacion.equals("5")   ) { 	 
		operacion = "ConsCortes";   
		if(id_producto.equals("1")) {
			nombreProducto ="CORTE_DescElect_"+fecha;  
		}else  if(id_producto.equals("0")) {
			nombreProducto ="CORTE_CreElect_"+fecha;  
		}
	}
	
	String cuenta="";
	UtilUsr usuario = new UtilUsr(); 
	
	Usuario user = new Usuario(); 
	user = usuario.getUsuario(strLogin);
	remitente = user.getEmail();	
	
	// Registros seleccionados de Corte
	StringBuffer  if_fisosC 	= new StringBuffer();	
	String if_fisos[] = request.getParameterValues("if_fisos");
	if(if_fisos.length>0) {
		for(int i=0;i<if_fisos.length;i++){	
			if_fisosC.append( if_fisos[i]+",");		
		}
		if_fisosC.deleteCharAt(if_fisosC.length()-1);
	}
	// Generación del Archivo				
	paginador.setTipoConsulta(operacion);
	paginador.setNombreProducto(nombreProducto); 
	paginador.setDirectorioPlantilla(strDirectorioPublicacion+"/00archivos/14seguridad/AvisosNafin.template.xlsx");   
	paginador.setIf_fisos(if_fisosC.toString());   
	
	try {
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "xlsx");			
			urlArchivo=  strDirectorioTemp+nombreArchivo;							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo xlsx", e);
		}
		
	JSONArray  vistaArchivosAdjuntos = new JSONArray();
	HashMap info = new HashMap();
	info.put("id", "12342");
	info.put("tituloArchivo", nombreArchivo);	
	vistaArchivosAdjuntos.add(info);
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("remitente", remitente);	
	jsonObj.put("destinatario", destinatario);	
	jsonObj.put("ccdestinatario", ccdestinatario);	
	jsonObj.put("asunto", asunto);	
	jsonObj.put("cuerpoCorreo", cuerpoCorreo1);
	jsonObj.put("urlArchivo", urlArchivo); 
	jsonObj.put("vistaArchivosAdjuntos", vistaArchivosAdjuntos); 	
	jsonObj.put("if_fisosC", if_fisosC.toString()); 	 
	infoRegresar = jsonObj.toString(); 


}else  if(informacion.equals("CreaArchivoAdjunto") )  {

	String tituloArchivo = (request.getParameter("tituloArchivo") != null) ? request.getParameter("tituloArchivo") : "";
	
	HashMap datosArchivo = new HashMap();
	datosArchivo.put("tituloArchivo",	tituloArchivo);
	datosArchivo.put("directorio",	strDirecVirtualTemp);
	datosArchivo.put("nombreArchivo",	tituloArchivo);
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("vistaArchivoAdjunto", strDirecVirtualTemp+tituloArchivo);	
	jsonObj.put("datosArchivo", datosArchivo);	  
	infoRegresar = jsonObj.toString(); 


}else  if(informacion.equals("EnviodeCorreo") )  {

	String ic_if[] = request.getParameterValues("ic_if");
	String cantidad_if[] = request.getParameterValues("cantidad_if");
	String monto_if[] = request.getParameterValues("monto_if");
	String cantidad_sirac[] = request.getParameterValues("cantidad_sirac");
	String monto_sirac[] = request.getParameterValues("monto_sirac");
	
	List parametros = new ArrayList();	
	String respuesta ="Fallo";
	String if_fisosC = (request.getParameter("if_fisosC") != null) ? request.getParameter("if_fisosC") : "";
	
	parametros.add(remitente);
	parametros.add(destinatario);
	parametros.add(ccdestinatario);
	parametros.add(asunto);
	parametros.add(cuerpoCorreo);
	parametros.add(urlArchivo);
	
	
	
	try {
	
		//inserta en la tabla de Bitacora 
		if(no_operacion.equals("4") || no_operacion.equals("5") ) { 	
			paginador.setEnvioCortes (  ic_if,   cantidad_if  ,  monto_if,  cantidad_sirac ,  monto_sirac, fechaCorte  );	 		
		}
		
		//envia el correo
		respuesta =  paginador.procesoEnviodeCorreo (parametros)	;
		
		
		
		
	} catch(Throwable e) {
		throw new AppException("Error enviar correo", e);
	} 
	
	String mensaje = "<center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+respuesta+"</center>"; 
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", mensaje);
	infoRegresar = jsonObj.toString(); 
}
%>

<%=infoRegresar%>