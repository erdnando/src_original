Ext.onReady(function() {
 
 
	var procesaValoresIniciales = function(options, success, response) {
		if (success == true && Ext.JSON.decode(response.responseText).success == true) {			
			var  jsonData = Ext.JSON.decode(response.responseText);
			
			if(jsonData.strPerfil=='ADMIN NAFIN')  {
			
				consServiciosData.load({  
					params: {
						operacion: "ConsultarServicios"			
					}
				}); 
				consResumenOperaDiariaData.load({
					params: {
						operacion: "inicializa"			
					}
				}); 
				Ext.ComponentQuery.query('#tabPrincipal')[0].show();
				Ext.ComponentQuery.query('#gridServicios')[0].show();
			}else  {			
				Ext.ComponentQuery.query('#tabPrincipal')[0].hide();
				Ext.ComponentQuery.query('#gridServicios')[0].hide();
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
 //***************** Vista Previa Correo *************************++
	var procesarEnvioCorreo = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			var  jsonData = Ext.JSON.decode(response.responseText);
			
			var button = 	Ext.ComponentQuery.query('#botonEnviarCorreo')[0];
			button.enable();
			button.setIconCls('icoBotonEnviarCorreo'); 
					
			if(jsonData.respuesta!='Fallo') {
			
				Ext.MessageBox.alert('Mensaje',jsonData.respuesta,
					function(){ 					
						Ext.ComponentQuery.query('#VentanaCorreo')[0].destroy();
						var activeTab = Ext.ComponentQuery.query('#tabPrincipal')[0].getActiveTab();
						if( activeTab.getItemId() === 'tab_3' ){ // => El Panel de Cortes est� seleccionado
							
							consCortesData.load({
								params: {
									operacion: "ConsCortes",
									actualizar:"S",
									id_producto: fpSerCortes.query('#id_producto')[0].getValue()  
								}
							});
							
						}
				}); 	  
				
			}									
						
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}
	
	
	

	var procesarDatosCorreo = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			var  jsonData = Ext.JSON.decode(response.responseText);
					
			Ext.ComponentQuery.query('#remitente')[0].setValue(jsonData.remitente);			
			Ext.ComponentQuery.query('#destinatarios')[0].setValue(jsonData.destinatario);
			Ext.ComponentQuery.query('#ccDestinatarios')[0].setValue(jsonData.ccdestinatario);
			Ext.ComponentQuery.query('#asunto')[0].setValue(jsonData.asunto);
			Ext.ComponentQuery.query('#cuerpoCorreo')[0].setValue(jsonData.cuerpoCorreo);
			Ext.ComponentQuery.query('#urlArchivo')[0].setValue(jsonData.urlArchivo);					
			Ext.ComponentQuery.query('#vistaArchivosAdjuntos')[0].setValue(jsonData.vistaArchivosAdjuntos);		
			Ext.ComponentQuery.query('#vistaArchivosAdjuntos')[0].show();	
				
			fpSerDiarios.query('#if_fisosC')[0].setValue(jsonData.if_fisosC); 
									
						
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}

	
   function FormaCorreo(readOnly ){
   	
   		this.xtype 									= 'mailform';
			this.title 									= '';
			this.hideCcDestinatarios 				= false;
			this.editMode 								= true;
			this.itemId 								= 'formaCorreo';
			this.hideArchivoAdjunto					= true;
			this.hideVistaArchivosAdjuntos   	= false;
			this.url 									= 'index.data.jsp';	
			this.height									= 500;
			this.destinatariosCfg					= {
				maxLength: 	1000
			};
			this.ccDestinatariosCfg 				= {
				allowBlank: true,
				maxLength: 	1000
			};
			this.asuntoCfg								= {
				allowBlank: false,
				maxLength: 	250
			};				
			this.cuerpoCorreoCfg						= {
				allowBlank: false,
				maxLength: 	4000
			};
			this.hideBotonEnviarCorreo 			=  readOnly;
			this.botonEnviarCorreoCfg 				= {
				handler:	function(button){				
				
					if (  Ext.isEmpty(Ext.ComponentQuery.query('#destinatarios')[0].getValue()) ) {  
						Ext.ComponentQuery.query('#destinatarios')[0].markInvalid('Este campo es obligatorio');
						return;
					}
			
					if (  Ext.isEmpty(Ext.ComponentQuery.query('#asunto')[0].getValue()) ) {  
						Ext.ComponentQuery.query('#asunto')[0].markInvalid('Este campo es obligatorio');
						return;
					}					
					
					if (  Ext.isEmpty(Ext.ComponentQuery.query('#cuerpoCorreo')[0].getValue()) ||  Ext.ComponentQuery.query('#cuerpoCorreo')[0].getValue()=='<p>&nbsp;</p>' ) {  
						Ext.ComponentQuery.query('#cuerpoCorreo')[0].markInvalid('Este campo es obligatorio'); 
						return;
					}
													
					var button = Ext.ComponentQuery.query('#botonEnviarCorreo')[0];					
					button.disable();
					button.setIconCls('x-mask-msg-text');		
					
					var gridCortes =  Ext.ComponentQuery.query('#gridCortes')[0];	
					var store = gridCortes.getStore();
					var regSelecc = 0;	
					var  totalReg1= store.getCount();
					var ic_if = [];
					var cantidad_if = [];	
					var monto_if = [];	
					var cantidad_sirac = [];
					var monto_sirac = [];
					
			
									
					store.each(function(record) {						
						if(record.data['SELECCION']==''){	
							regSelecc++;
						}else{							
							ic_if.push(record.data['IC_IF']);	
							cantidad_if.push(record.data['CANTIDAD_IF']);	
							monto_if .push(record.data['MONTO_IF']);	
							cantidad_sirac.push(record.data['CANTIDAD_SIRAC']);	
							monto_sirac.push(record.data['MONTO_SIRAC']);	
						}
					});
			
		
					Ext.Ajax.request({
						url: 'index.data.jsp',
						params: {							
							informacion: 'EnviodeCorreo',
							remitente: Ext.ComponentQuery.query('#remitente')[0].getValue(),
							destinatario: Ext.ComponentQuery.query('#destinatarios')[0].getValue(),
							ccdestinatario: Ext.ComponentQuery.query('#ccDestinatarios')[0].getValue(),
							asunto: Ext.ComponentQuery.query('#asunto')[0].getValue(),
							cuerpoCorreo: Ext.ComponentQuery.query('#cuerpoCorreo')[0].getValue(),
							urlArchivo: Ext.ComponentQuery.query('#urlArchivo')[0].getValue(), 							
							no_operacion  : fpSerDiarios.query('#no_operacion')[0].getValue(),
							if_fisosC  : fpSerDiarios.query('#if_fisosC')[0].getValue(),
							fechaCorte  : fpSerDiarios.query('#fechaCorte')[0].getValue(),
							id_producto :  fpSerCortes.query('#id_producto')[0].getValue(),
							no_operacion: fpSerDiarios.query('#no_operacion')[0].getValue(), 
							ic_if:ic_if,
							cantidad_if :cantidad_if, 
							monto_if:monto_if, 
							cantidad_sirac :cantidad_sirac, 
							monto_sirac:monto_sirac
						},
						callback: procesarEnvioCorreo
					});
				}
			},
			this.items = [];			
		}

	
	var vistaPreviaCorreoResumen = function(mostrar, leyenda, if_fisos  ) {
	
		var VentanaCorreo =   Ext.ComponentQuery.query('#VentanaCorreo')[0];	
		var  no_operacion =   fpSerDiarios.query('#no_operacion')[0].getValue();
		var id_producto  =  fpSerCortes.query('#id_producto')[0].getValue();
		
		var desProducto =""; 
		if(no_operacion ==1 || no_operacion ==4  )  { desProducto=	" Descuento Electr�nico"; 	}
		if(no_operacion ==2  )  {  desProducto=	" Financiamiento a Distribuidores"; 	}
		if(no_operacion ==3 || no_operacion ==5 )  { desProducto=	" Cr�dito Electr�nico "; 	}
			
		if(mostrar=='N') {
			VentanaCorreo.hide();
		}else  {
			var titulo = "Vista Previa Correo - "+ leyenda + desProducto;	
			new Ext.Window({										
				modal: true,
				width: 610,
				//height: 630,	
				resizable: false,
				closable:true,
				id: 'VentanaCorreo',
				autoDestroy:true,
				closeAction: 'destroy',				
				items: [										
					new FormaCorreo(false)
				]				
			}).show().setTitle(titulo);
			
			Ext.Ajax.request({
				url: 'index.data.jsp',
				params: {							
					informacion: 'DatosCorreo',
					operacion: 'DatosCorreo',
					no_operacion: fpSerDiarios.query('#no_operacion')[0].getValue(),
					if_fisos:if_fisos,
					id_producto:id_producto 
					//ic_bitacora: fpSerDiarios.query('#ic_bitacora')[0].getValue()
				},
				callback: procesarDatosCorreo
			});
	
		}
	}
	
	
	
//***************************************Cortes************************+
 
	var validarRegFiso = function() {
	
		var gridCortes =  Ext.ComponentQuery.query('#gridCortes')[0];	
		var store = gridCortes.getStore();
		var regSelecc = 0;	
		var  totalReg1= store.getCount();
		var if_fisos = [];	
		
		store.each(function(record) {						
			if(record.data['SELECCION']==''){	
				regSelecc++;
			}else{
				if_fisos.push(record.data['IC_IF']);				
			}
		});
		
		if(regSelecc==totalReg1) {
			Ext.MessageBox.alert('Mensaje','Debe seleccionar al menos un FISO.');
			return;
		}else {
			vistaPreviaCorreoResumen("S","Cortes", if_fisos );	
		}

	}
	
	

	var procesarConsCortesData= function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var gridCortes =  Ext.ComponentQuery.query('#gridCortes')[0];
			var json =  store.proxy.reader.rawData;
			
			Ext.ComponentQuery.query('#gridCortes')[0].show();			
			
			if(json.operacion!='inicializa') {
				Ext.ComponentQuery.query('#btnGenerarCorte')[0].disable();
				Ext.ComponentQuery.query('#btnGenerarCorte')[0].setIconCls('autorizar');	
			}
			 
			fpSerDiarios.query('#fechaCorte')[0].setValue(json.fechaCorte); 			
			
			if(store.getTotalCount() > 0) {			
				gridCortes.getView().getEl().unmask();					
				Ext.ComponentQuery.query('#btnEnviarCortes')[0].enable();
				Ext.ComponentQuery.query('#btnActuCortes')[0].enable();	
				Ext.ComponentQuery.query('#btnActuCortes')[0].setIconCls('icoActualizar'); 
				Ext.ComponentQuery.query('#btnGenerarCorte')[0].disable();
				Ext.ComponentQuery.query('#btnGenerarCorte')[0].setIconCls('autorizar');	
				Ext.ComponentQuery.query('#btnEnviarCortes')[0].setIconCls('icoBotonEnviarCorreo');				
				Ext.ComponentQuery.query('#textoBarra')[0].update(json.leyendaBarraCorte);
				Ext.ComponentQuery.query('#gridCortes')[0].getView().getEl().unmask();	
				Ext.ComponentQuery.query('#gridCortes')[0].getView().setAutoScroll(true);	
		
			}else  {					
					
				Ext.ComponentQuery.query('#btnEnviarCortes')[0].disable();	
				Ext.ComponentQuery.query('#btnGenerarCorte')[0].enable(); 
				Ext.ComponentQuery.query('#btnActuCortes')[0].disable();	
				Ext.ComponentQuery.query('#btnActuCortes')[0].setIconCls('icoActualizar'); 
				Ext.ComponentQuery.query('#textoBarra')[0].update('');	
								 
				if(json.operacion=='inicializa') {
					Ext.ComponentQuery.query('#gridCortes')[0].getView().getEl().mask('No se ha generado el corte.','x-mask-noloading');
				}else  {
					Ext.ComponentQuery.query('#gridCortes')[0].getView().getEl().mask('No se encontr� ning�n registro.','x-mask-noloading');
				}
				Ext.ComponentQuery.query('#gridCortes')[0].getView().setAutoScroll(false);	
				
			}
		}
	};

	Ext.define('LisRegCortes', {
		extend: 'Ext.data.Model',
		fields: [				
			{ name: 'IC_IF'},		
			{ name: 'NOMBRE_FISOS'},				
			{ name: 'CANTIDAD_IF'},				
			{ name: 'MONTO_IF'},				
			{ name: 'CANTIDAD_SIRAC'},				
			{ name: 'MONTO_SIRAC'},				
			{ name: 'HORA_ULTIMO_CORTE'},
			{ name: 'SELECCION', type: 'boolean' }
		 ]
	});

	var consCortesData = Ext.create('Ext.data.Store', {
		model: 'LisRegCortes',		
		proxy: {
			type: 'ajax',
			url: 'index.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'ConsCortes'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsCortesData
			}
	});
		 
		
	var  gridCortes = Ext.create('Ext.grid.Panel',{	
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridCortes',
		store: consCortesData,
		height: 300,
		width: 670,		
		style: 'margin: 10px auto 0px auto;',			
		xtype: 'grouped-header-grid',		
      title: '',
      frame: true,	 						
		columns: [
			{
				xtype: 'checkcolumn',
				dataIndex: 'SELECCION',
            text: '',
				width:20,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                var cssPrefix = Ext.baseCSSPrefix,
                cls = cssPrefix + 'grid-checkcolumn';
					 
					if(  record.data['SELECCION'] ==false)  { 
						if(record.data['MONTO_IF']!= record.data['MONTO_SIRAC'] )  {						
							  metaData.tdCls += ' ' + this.disabledCls;	
						 }else {
							  cls = cssPrefix + 'grid-checkcolumn';								  
						 }
					 }else  if(  record.data['SELECCION'] ==true)  { 
						cls += ' ' + cssPrefix + 'grid-checkcolumn-checked';						
					 }					
						return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>'; 
					},			           
					listeners: {				    
                beforecheckchange: function(checkcolumn, rowIndex, checked, eOpts){
						var gridCortes = Ext.ComponentQuery.query('#gridCortes')[0];
						var record = gridCortes.getView().getRecord(rowIndex);
						if(record.data['MONTO_IF']== record.data['MONTO_SIRAC'] )  {	
							return true;
						}else  { 
							return false;
						}
					 } 			
            }
				
        },
			{
				text: 'FISOS',
				menuDisabled: false,
				columns: [
					{
						header: '',				
						dataIndex: 'NOMBRE_FISOS',
						sortable: true,				
						width: 150,		
						resizable: true,							
						align: 'left'						
					}				
				]
			}, 
			{
				text: 'Autorizadas por IF',
				menuDisabled: false,
				columns: [
					{
						text:'Cantidad',
						dataIndex:'CANTIDAD_IF',
						sortable: true,				
						width: 90,			
						resizable: true,
						align: 'center'						
					},
					{
						text:'Monto',
						dataIndex:'MONTO_IF',
						sortable: true,				
						width: 90,			
						resizable: true,							
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					}
				]
			},
			//Registradas en SIRAC
			{
				text: 'Registradas en SIRAC',
				menuDisabled: false,
				columns: [
					{
						text:'Cantidad',
						dataIndex:'CANTIDAD_SIRAC',
						sortable: true,				
						width: 90,			
						resizable: true,
						align: 'center'						
					},			
					{
						text:'Monto',
						dataIndex:'MONTO_SIRAC',
						sortable: true,				
						width: 90,			
						resizable: true,							
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$0,0.00')
					}
				]
			},	
			{
				text: 'Hora del �ltimo Corte',
				menuDisabled: false,
				columns: [				
					{
						text:'',
						dataIndex:'HORA_ULTIMO_CORTE',
						sortable: true,				
						width: 120,			
						resizable: true,							
						align: 'left'
					}
				]
			}
		],	
		bbar:{			
			items: [
				{
				  xtype: 'label',
				  forId: 'myFieldId',
				  id: 'textoBarra',
				  text: '',				  
				  margin: '0 0 0 10'
			 },
				//'->',
				{
					xtype: 'button',
					text: 'Generar Corte', 				
					id: 'btnGenerarCorte',
					iconCls: 'autorizar',				
					handler: function(button,event) {
						
						
						var id_producto  =  fpSerCortes.query('#id_producto')[0].getValue();
												
						var button = Ext.ComponentQuery.query('#btnGenerarCorte')[0];		
						button.disable();
						button.setIconCls('x-mask-msg-text');		
						
						consCortesData.load({
							params: {
								operacion: "ConsCortes",
								actualizar:"N",
								id_producto:id_producto
							}
						});
						
						
					}
				},			
				{
					xtype: 'button',
					text: 'Actualizar', 				
					id: 'btnActuCortes',					
					iconCls: 'icoActualizar',						
					handler: function(button,event) {
						
						var id_producto  =  fpSerCortes.query('#id_producto')[0].getValue();	
						
						var button = Ext.ComponentQuery.query('#btnActuCortes')[0];		
						button.disable();
						button.setIconCls('x-mask-msg-text');		
							
						consCortesData.load({
							params: {
								operacion: "ConsCortes",
								actualizar:"S",
								id_producto:id_producto								
							}
						});
					}
				},
				{
					text: 'Enviar',
					id: 'btnEnviarCortes',
					iconCls: 'icoBotonEnviarCorreo',					
					handler: validarRegFiso
					
				}
			]
		}
	});
	



	var  fpSerCortes = Ext.create( 'Ext.form.Panel', {	
      itemId: 'fpSerCortes',
      frame: true,
		border: false,
      bodyPadding: '12 6 12 6',
      width: 690,
      style: 'margin: 0px auto 0px auto;',
      hidden: false,
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 100
		},
		//layout: 'form',	
		title: '',
		items	: 	[ 
			{ 	xtype: 'textfield',  hidden: true, id: 'id_producto', 	value: '' }	, 	
			{
				xtype: 'button',
				text: '<b>Descuento Electr�nico</b>', 
				itemId: 'btnDescElec_Cortes',				
				iconCls: '',			
				handler: function(){
				 
					 fpSerCortes.query('#btnDescElec_Cortes')[0].setText('<b>Descuento Electr�nico</b>');
					 fpSerCortes.query('#btnElectronico_Cortes')[0].setText('Cr�dito Electr�nico');
					 fpSerCortes.query('#id_producto')[0].setValue("1");				
					 fpSerDiarios.query('#no_operacion')[0].setValue("4");
					 
					 Ext.ComponentQuery.query('#btnEnviarCortes')[0].disable();
					 Ext.ComponentQuery.query('#btnActuCortes')[0].disable(); 
					 Ext.ComponentQuery.query('#btnGenerarCorte')[0].enable(); 
					 
					 Ext.ComponentQuery.query('#gridServicios')[0].hide();
					 consCortesData.load({
						params: {
							informacion:"ConsCortes",
							operacion: "inicializa"		
						}
					});	
				}
			},
			{
				xtype: 'button',
				text: 'Cr�dito Electr�nico', 
				itemId: 'btnElectronico_Cortes',				
				iconCls: '',	
				hidden: false,
				handler: function(){
					
					fpSerCortes.query('#btnDescElec_Cortes')[0].setText('Descuento Electr�nico');
					fpSerCortes.query('#btnElectronico_Cortes')[0].setText('<b>Cr�dito Electr�nico</b>');
				   fpSerCortes.query('#id_producto')[0].setValue("0");
					fpSerDiarios.query('#no_operacion')[0].setValue("5"); 
					
					Ext.ComponentQuery.query('#btnEnviarCortes')[0].disable();
				   Ext.ComponentQuery.query('#btnActuCortes')[0].disable(); 
				   Ext.ComponentQuery.query('#btnGenerarCorte')[0].enable(); 
					
					Ext.ComponentQuery.query('#gridServicios')[0].hide();
					 consCortesData.load({
						params: {
							informacion:"ConsCortes",
							operacion: "inicializa"		
						}
					});	
					
									
				}
			}
			,gridCortes
		],
		buttons: [	 ]			
	});
	



//***************************************Monitoreo Diario************************+




	var procesarMonitoreoDiario = function(options, success, response) {

		if (success == true && Ext.JSON.decode(response.responseText).success == true) {
			
			var  jsonData = Ext.JSON.decode(response.responseText);
			
			Ext.getCmp('lbl_Dispersiones').update(jsonData.lbl_Dispersiones);
			Ext.getCmp('lbl_Tasas').update(jsonData.lbl_Tasas);		
			Ext.getCmp('lbl_Operacion').update(jsonData.lbl_Operacion);
			Ext.getCmp('lbl_creElectronico').update(jsonData.lbl_creElectronico);
			Ext.getCmp('lbl_creElectronicoMN').update(jsonData.lbl_creElectronicoMN);
			
			var button = Ext.ComponentQuery.query('#btnActuMonitor')[0];		
			button.enable();
			button.setIconCls('icoActualizar');	
				
			
			
		} else {
			NE.util.mostrarErrorPeticion(response);
		}
	}

	var  elementosMonitoreo  = [
		{
			xtype: 'panel',
			title: 'Dispersiones',
			id:'panelMonitoreo_1',
			collapsible: true,
			titleCollapse: true,
			bodyStyle: {
				background: '#D9E5F3',
				borderColor: '#99BCE8',
				borderWidth: '1px'
			},      			
			rowspan: 1,
			colspan: 1,
			width: '100%',
			items: [
				{
					xtype: 'label',
					id: 'lbl_Dispersiones',
					style: 'background-color: white; font-weight:bold; text-align:center; display:block;',
					fieldLabel: '',
					text: ''
				}
			]
		},
		{
			xtype: 'panel',
			title: 'Reducci�n de Tasas',
			id:'panelMonitoreo_2',
			collapsible: true,
			titleCollapse: true,
			bodyStyle: {
				background: '#D9E5F3',
				borderColor: '#99BCE8',
				borderWidth: '1px'
			},      			
			rowspan: 1,
			colspan: 1,
			width: '100%',
			items: [
				{
					xtype: 'label',
					id: 'lbl_Tasas',
					style: 'background-color: white; font-weight:bold; text-align:center; display:block;',
					fieldLabel: '',
					text: ''
				}
			]
		},
		{
			xtype: 'panel',
			title: 'Operaci�n Entrega Continua',
			id:'panelMonitoreo_3',
			collapsible: true,
			titleCollapse: true,
			bodyStyle: {
				background: '#D9E5F3',
				borderColor: '#99BCE8',
				borderWidth: '1px'
			},      
			layout: {
				pack: 'center',
				type: 'hbox'
			},
			rowspan: 1,
			colspan: 1,
			width: '100%',
			items: [
				{
					xtype: 'label',
					id: 'lbl_Operacion',
					style: 'background-color: white; font-weight:bold; text-align:center; display:block;',
					fieldLabel: '',
					text: ''
				}
			]
		},
		{
			xtype: 'panel',
			title: 'Operaci�n Cr�dito Electr�nico (Dls) ',
			id:'panelMonitoreo_4',
			collapsible: true,
			titleCollapse: true,
			bodyStyle: {
				background: '#D9E5F3',
				borderColor: '#99BCE8',
				borderWidth: '1px'
			},      
			layout: {
				pack: 'center',
				type: 'hbox'
			},
			rowspan: 1,
			colspan: 1,
			width: '100%',
			items: [
				{
					xtype: 'label',
					id: 'lbl_creElectronico',
					style: 'background-color: white; font-weight:bold; text-align:center; display:block;',
					fieldLabel: '',
					text: ''
				}
			]
		},
		{
			xtype: 'panel',
			title: 'Operaci�n Cr�dito Electr�nico (M.N) ',
			id:'panelMonitoreo_5',
			collapsible: true,
			titleCollapse: true,
			bodyStyle: {
				background: '#D9E5F3',
				borderColor: '#99BCE8',
				borderWidth: '1px'
			},
			layout: {
				pack: 'center',
				type: 'hbox'
			},
			rowspan: 1,
			colspan: 1,
			width: '100%',
			items: [
				{
					xtype: 'label',
					id: 'lbl_creElectronicoMN',
					style: 'background-color: white; font-weight:bold; text-align:center; display:block;',
					fieldLabel: '',
					text: ''
				}
			]
		}
	];



//********************** Servicios Diarios **********************************

	var procesarConsResumenOperaDiariaData= function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var gridResumenOperaDiaria =  Ext.ComponentQuery.query('#gridResumenOperaDiaria')[0];
			var json =  store.proxy.reader.rawData;
			
			gridResumenOperaDiaria.show();			
			
			var button = Ext.ComponentQuery.query('#btnActuOpeDiaria')[0];		
			button.enable();
			button.setIconCls('icoActualizar');	
			
			
			if(store.getTotalCount() > 0) {			
				fpSerDiarios.query('#btnEnviarOpeDiaria')[0].show();			
				gridResumenOperaDiaria.getView().getEl().unmask();	
				gridResumenOperaDiaria.getView().setAutoScroll(true);
			}else  {
				fpSerDiarios.query('#btnEnviarOpeDiaria')[0].hide();	
				gridResumenOperaDiaria.getView().getEl().mask('No se han registrado operaciones para ning�n Intermediario.','x-mask-noloading');
				gridResumenOperaDiaria.getView().setAutoScroll(false);				
			}
		}
	};
	

	Ext.define('LisRegDesElectonico', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'INTERMEDIARIO'},				
			{ name: 'MONEDA' },
			{ name: 'OPERACIONES' , type: 'int' },
			{ name: 'MONTO' , type: 'float'  }			
		 ]
	});

	var consResumenOperaDiariaData = Ext.create('Ext.data.Store', {
		model: 'LisRegDesElectonico',
		groupField: 'MONEDA',		
		proxy: {
			type: 'ajax',
			url: 'index.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'ConsResumenOperaDiaria'
			},
			totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsResumenOperaDiariaData
			}
	});
	
	var  gridResumenOperaDiaria = Ext.create('Ext.grid.Panel',{	
		itemId: 'gridResumenOperaDiaria',
		store: consResumenOperaDiariaData,
		height: 300,
		width: 670,		
		style: 'margin: 10px auto 0px auto;',		
		xtype: 'cell-editing',
      title: '',
      frame: true,	 					
		features: [{
			id: 'group',
         ftype: 'groupingsummary',
         groupHeaderTpl: '{name}',
         hideGroupedHeader: true,
         enableGroupingMenu: false
      }],	
		columns: [	
		
			{
				header: 'Intermediario',				
				dataIndex: 'INTERMEDIARIO',
				sortable: true,				
				width: 350,			
				resizable: true,
				summaryRenderer: function(value, summaryData, dataIndex) {
                return '<b>Total';
            }
         },				
			{
				text:'Operaciones',
            dataIndex:'OPERACIONES',
				sortable: true,				
				width: 150,			
				resizable: true,					
				align: 'center',
				summaryType: 'sum',
				summaryRenderer: function(value, summaryData, dataIndex) {
                return  '<b>'+  value; 
            }
			},
			{
				text:'Monto',
            dataIndex:'MONTO',
				sortable: true,				
				width: 150,			
				resizable: true,					
				align: 'right',
				summaryType: 'sum',
				summaryRenderer: function(value, summaryData, dataIndex) {            
				 return  '<b>'+ 	Ext.util.Format.number(value, '$0,0.00');  
			 
            }
			}			
		]		
	});
	


	fpSerDiarios = Ext.create( 'Ext.form.Panel', {
	//	layout: 'form',
      itemId: 'fpSerDiarios',
      frame: true,
		border: false,
      bodyPadding: '12 6 12 6',
      width: 690,
      style: 'margin: 0px auto 0px auto;',
      hidden: false,
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 100
		},
		//layout: 'form',	
		title: 'Resumen de Operaci�n Diaria',
		items	: 	[ 	
			{ 	xtype: 'textfield',  hidden: true, id: 'operacion', 	value: '' }	, 	
			{ 	xtype: 'textfield',  hidden: true, id: 'no_operacion', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: 'urlArchivo', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: 'fechaCorte', 	value: '' }	,
			{ 	xtype: 'textfield',  hidden: true, id: 'if_fisosC', 	value: '' }	,	
			{ 	xtype: 'textfield',  hidden: true, id: 'tab_1_no_operacion', 	value: '' }	, // tab servicios_diarios
			{
				xtype: 'button',
				text: '<b>Descuento Electr�nico</b>', 
				itemId: 'btnDescElec_Diario',				
				iconCls: '',			
				handler: function(){
				
					fpSerDiarios.query('#operacion')[0].setValue("DescuentoElectronico");	
					fpSerDiarios.query('#no_operacion')[0].setValue("1");
					fpSerDiarios.query('#tab_1_no_operacion')[0].setValue("1");
					fpSerDiarios.query('#btnDescElec_Diario')[0].setText('<b>Descuento Electr�nico</b>');
					fpSerDiarios.query('#btnDistribuidores_Diario')[0].setText('Financiamiento a Distribuidores');
					fpSerDiarios.query('#btnCredito_Diario')[0].setText('Cr�dito Electr�nico');
					
					Ext.ComponentQuery.query('#btnEnviarOpeDiaria')[0].hide();	
				
					consResumenOperaDiariaData.load({
					params: {
							operacion: "inicializa"			
						}
					}); 
	
				}
			},
			{
				xtype: 'button',
				text: 'Financiamiento a Distribuidores', 
				itemId: 'btnDistribuidores_Diario',				
				iconCls: '',	
				hidden: false,
				handler: function(){
					
					fpSerDiarios.query('#operacion')[0].setValue("Distribuidores");
					fpSerDiarios.query('#no_operacion')[0].setValue("2");
					fpSerDiarios.query('#tab_1_no_operacion')[0].setValue("2");
					fpSerDiarios.query('#btnDescElec_Diario')[0].setText('Descuento Electr�nico');
					fpSerDiarios.query('#btnDistribuidores_Diario')[0].setText('<b>Financiamiento a Distribuidores</b>');
					fpSerDiarios.query('#btnCredito_Diario')[0].setText('Cr�dito Electr�nico');
					Ext.ComponentQuery.query('#btnEnviarOpeDiaria')[0].hide();
					
					consResumenOperaDiariaData.load({
					params: {
							operacion: "inicializa"			
						}
					}); 
					
				}
			},
			{
				xtype: 'button',
				text: 'Cr�dito Electr�nico', 
				itemId: 'btnCredito_Diario',
				hidden: false,
				iconCls: '',			
				handler: function(){		
				
					fpSerDiarios.query('#operacion')[0].setValue("CreditoElectronico");	
					fpSerDiarios.query('#no_operacion')[0].setValue("3");
					fpSerDiarios.query('#tab_1_no_operacion')[0].setValue("3");
					fpSerDiarios.query('#btnDescElec_Diario')[0].setText('Descuento Electr�nico');
					fpSerDiarios.query('#btnDistribuidores_Diario')[0].setText('Financiamiento a Distribuidores');
					fpSerDiarios.query('#btnCredito_Diario')[0].setText('<b>Cr�dito Electr�nico</b>');
					Ext.ComponentQuery.query('#btnEnviarOpeDiaria')[0].hide();						
					consResumenOperaDiariaData.load({
					params: {
							operacion: "inicializa"			
						}
					}); 
					
				}
			}
			,gridResumenOperaDiaria
		],
		buttons: [		
			{
				text: 'Actualizar',
				id: 'btnActuOpeDiaria',
				iconCls: 'icoActualizar',
				handler: function(){		
					
					var button = Ext.ComponentQuery.query('#btnActuOpeDiaria')[0];		
					button.disable();
					button.setIconCls('x-mask-msg-text');		
					
					var  operacion=  fpSerDiarios.query('#operacion')[0].getValue();	
					if(operacion=='') {
						fpSerDiarios.query('#operacion')[0].setValue("DescuentoElectronico");
						fpSerDiarios.query('#no_operacion')[0].setValue("1");
						fpSerDiarios.query('#tab_1_no_operacion')[0].setValue("1");
					}					
					consResumenOperaDiariaData.load({
						params: {
							operacion: fpSerDiarios.query('#operacion')[0].getValue()
						}
					});					
					
				}
			},
			{
				text: 'Enviar',
				id: 'btnEnviarOpeDiaria',
				iconCls: 'icoBotonEnviarCorreo',
				hidden:true, 
				handler: function(){		
					vistaPreviaCorreoResumen("S","Resumen de Operaci�n Diaria  ", "");				
				}
			}			
		]			
	});
	



//********************** Grid de  Estado de Servicios **********************************
	var procesarConsultaServicios = function(store,  arrRegistros, success, opts) {
		if (arrRegistros != null) {
			var gridServicios =  Ext.ComponentQuery.query('#gridServicios')[0];
			var json =  store.proxy.reader.rawData;
			
			Ext.ComponentQuery.query('#gridServicios')[0].show();	
			
			var button = Ext.ComponentQuery.query('#btnActualizar')[0];		
			button.enable();
			button.setIconCls('icoActualizar');	
			
		}
	};
	

	Ext.define('LisRegServicios', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'PRODUCTO'},	
			{ name: 'SERVICIO' }				
		 ]
	});

	var consServiciosData = Ext.create('Ext.data.Store', {
		model: 'LisRegServicios',	
		proxy: {
			type: 'ajax',
			url: 'index.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'ConsultarServicios'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
				load: procesarConsultaServicios
			}
	});

	var  gridServicios = Ext.create('Ext.grid.Panel',{	
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		itemId: 'gridServicios',
		store: consServiciosData,
		height: 150,
		width: 350,	
		style: 'margin: 10px auto 0px auto;',		
		xtype: 'cell-editing',
      title: 'Estado del Servicio',
      frame: true,	
		hidden: true,
		plugins:[Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		columns: [			
			{
				header: 'Producto',				
				dataIndex: 'PRODUCTO',
				sortable: true,				
				width: 190,			
				resizable: true				
         },			
			{
				text:'Proveedores o Clientes',
            dataIndex:'SERVICIO',
				sortable: true,				
				width: 140,			
				resizable: true,				
				renderer: function(value, metaData, record, row, col, store, gridView){
					if(value=="Servicio Cerrado")  {
						return  '<span style="color:red;">' + value + '</span>';
					}else {
						return  '<span style="color:green;">' + value + '</span>';
					}
				}
			}
		],		
		bbar: [			
			'->','-',	
			{
				text: 'Actualizar',
				id: 'btnActualizar',
				iconCls: 	'icoActualizar',
				handler: function(){
				
					var button = Ext.ComponentQuery.query('#btnActualizar')[0];		
					button.disable();
					button.setIconCls('x-mask-msg-text');		
				
					consServiciosData.load({
						params: {
							operacion: "ConsultarServicios"			
						}
					});						
				}
			}
		]
	});
	
	
	var tabPrincipal = Ext.create(	'Ext.tab.Panel',  {
		region: 			'center',
		id:'tabPrincipal',	
		frame: true,
		hidden: true,
		layout: 'form',
		items: [
			{
				title: 'Servicios Diarios ',
				id:'tab_1',			
				layout: 'form',
				items: [	
					fpSerDiarios	
				]
			} ,
			{
				title: 'Monitoreo Diario ',	
				id:'tab_2',
				layout: 'form',
				items: elementosMonitoreo,
				buttons: ['->', 
					{
						xtype: 'button',
						text: 'Actualizar', 				
						id: 'btnActuMonitor',
						iconCls: 'icoActualizar',			
						handler: function(){	
							var button = Ext.ComponentQuery.query('#btnActuMonitor')[0];		
							button.disable();
							button.setIconCls('x-mask-msg-text');			 
							Ext.Ajax.request({
								url: 'index.data.jsp',
								params: {							
									informacion: 'InicializaMonitoreoDiario'
								},
								callback: procesarMonitoreoDiario
							}); 										
						}
					}				
				]
			},
			{
				title: 'Cortes ',	
				id:'tab_3',
				layout: 'form',				
				items: [	
					fpSerCortes	
				]
			} 
		],
		listeners: {
			tabchange: function(tab, panel){
			
				if (panel.getItemId() == 'tab_1') {
				
					Ext.ComponentQuery.query('#gridServicios')[0].show();
					// Restaurar ultimo no_operacion seleccionado en el tab de Servicios Diarios
					var tab_1_no_operacion = fpSerDiarios.query('#tab_1_no_operacion')[0].getValue();
					fpSerDiarios.query('#no_operacion')[0].setValue(tab_1_no_operacion);
					consResumenOperaDiariaData.load({
					params: {
							operacion: "inicializa"			
						}
					}); 
					
				
				}else  if (panel.getItemId() == 'tab_2') {
				
					Ext.ComponentQuery.query('#gridServicios')[0].hide();					
				
				}else if (panel.getItemId() == 'tab_3') {
				
					Ext.ComponentQuery.query('#gridServicios')[0].hide();
					Ext.ComponentQuery.query('#btnEnviarCortes')[0].disable();
				  Ext.ComponentQuery.query('#btnActuCortes')[0].disable();				
					fpSerDiarios.query('#no_operacion')[0].setValue("4");
					
					consCortesData.load({
							params: {
								informacion:"ConsCortes",
								operacion: "inicializa"		
							}
						});
						
						
				}
			}
		}
	});
	

	forma = Ext.create('Ext.form.Panel',	{
      itemId: 'forma',
      frame: true,
		border: false,
      bodyPadding: '12 6 12 6',
      width: 490,
		height: 520,
      style: 'margin: 0px auto 0px auto;',
      hidden: false,
      fieldDefaults: {
			msgTarget: 'side',
         labelWidth: 50
		},			
		items	:  [ ]
	});
	
		
	var main = Ext.create('Ext.container.Container', {
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 700,
		style: 'margin:0 auto;',
		items: [
			tabPrincipal,
			gridServicios
		]
	});
	
	var panel = Ext.create('Ext.panel.Panel',{
			renderTo:'areaLeyenda',
			width: 945,
			frame: false,
			html: '<table border=0><tr><td><p style="text-align:justify; background:#F3F4F5; font-size:80%">La informaci�n debidamente obtenida y autorizada por el administrador de la plataforma de NAFINET (Nacional Financiera S.N.C., I.B.D) '+
				'tendr� valor probatorio en juicio, en tanto que la obtenci�n de informaci�n almacenada en la base de datos y archivos de la misma, ' + 
				' sin contar con la autorizaci�n correspondiente, o el uso indebido de dicha informaci�n, ser� sancionada en t�rminos de Ley de Instituciones ' +
				' de Cr�dito y dem�s ordenamientos legales aplicables. </p></td></tr></table>'
		});
	
	 
	Ext.Ajax.request({
		url: 'index.data.jsp',
		params: {
			informacion: "valoresIniciales"			
		},
		callback: procesaValoresIniciales
	});
	
	
	
});


