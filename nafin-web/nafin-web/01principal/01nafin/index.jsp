<!DOCTYPE html>
<%@ page import="java.util.*, netropology.utilerias.*"	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%
//Estas lineas solo van a ir los index.jsp, para llevar el registro de que paso por ahi el afiliado
List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
if (pantallasNavegacionComplementaria != null) {
	pantallasNavegacionComplementaria.remove("/01principal/01nafin/index.jsp");  //El nombre debe coincidir con el especificado en Navegacion
}
//Si llega a esta pantalla se coloca una variable en sesión para indicar que la sesión esta lista para empezar a trabajar
if (pantallasNavegacionComplementaria != null && pantallasNavegacionComplementaria.isEmpty()) {
	session.setAttribute("inicializar.FINALIZADO", "S");
} else {
	throw new AppException("La sesión del usuario no fue inicializada correctamente");
}
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>

<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="index.js?<%=session.getId()%>"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="menuLateral.jspf"%>
						<div id="areaContenido" style="width:700px">
						</div>
					</div>
				</div>
				<div id="areaLeyenda"></div>
				<%@ include file="pie.jspf"%>

<form id='formAux' name="formAux" target='_new'></form>

</body>
</html>
