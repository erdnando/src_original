<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.cadenas.*,
		java.text.SimpleDateFormat,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String valProducto = (request.getParameter("valProducto")!=null)?request.getParameter("valProducto"):"DE";

String fecha = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
Fecha fechaC = new Fecha();
String mesActual= fechaC.getMesNombre(fecha);

System.out.println("valProducto==>>>>>>>"+valProducto);

if (informacion.equals("generaGraficas")) {
	JSONObject jsonObj = new JSONObject();
	GraficasIF graficasIf = new GraficasIF();
	GraficasUNI graficasUni = new GraficasUNI();
	String tipoMoneda = (request.getParameter("tipoMoneda") == null)?"":request.getParameter("tipoMoneda");
	boolean bestatusProc = false;
	boolean cargaTerminada = false;
	String xmlProv = "";
	String xmlDoc = "";
	String xmlFact = "";
	String xmlDist = "";
	String xmlVenc = "";
	String xmlOpe = "";
	String xmlUniAnt = "";
	String xmlUniAct = "";
	String xmlNumAcred ="";
	bestatusProc = true;
	cargaTerminada = true;	
	String infoGrafica = "";
			
	if(bestatusProc){
	if(valProducto.equals("DE") || valProducto.equals("CE")){
			if(valProducto.equals("DE")){
				infoGrafica = "'OPERACIONMN', 'OPERACIONDL'";
				List lstGraficaOperacion = graficasIf.getInfoGraficaIF(iNoCliente, infoGrafica);
							
				if(  lstGraficaOperacion!=null & lstGraficaOperacion.size()>0 ){
					String setCategory = "",setValues = "", setValuesDL = "";
					int iColor = 389;
					String CG_ATRIBUTO  = "",  CG_VALOR  = "", CG_GRAFICA  = "",CG_GRAFICAAn = "", mes="", CG_ATRIBUTOAn="";
					for(int i=0; i<lstGraficaOperacion.size(); i++){
						HashMap mp = (HashMap)lstGraficaOperacion.get(i);
							
						CG_ATRIBUTO  = mp.get("CG_ATRIBUTO").toString();
						CG_VALOR  = mp.get("CG_VALOR").toString();
						CG_GRAFICA  = mp.get("CG_GRAFICA").toString();	
						if(CG_ATRIBUTO.equals("01")) mes ="Ene";
						if(CG_ATRIBUTO.equals("02")) mes ="Feb";
						if(CG_ATRIBUTO.equals("03")) mes ="Mar";
						if(CG_ATRIBUTO.equals("04")) mes ="Abr";
						if(CG_ATRIBUTO.equals("05")) mes ="May";
						if(CG_ATRIBUTO.equals("06")) mes ="Jun";
						if(CG_ATRIBUTO.equals("07")) mes ="Jul";
						if(CG_ATRIBUTO.equals("08")) mes ="Ago";
						if(CG_ATRIBUTO.equals("09")) mes ="Sep";
						if(CG_ATRIBUTO.equals("10")) mes ="Oct";
						if(CG_ATRIBUTO.equals("11")) mes ="Nov";  
						if(CG_ATRIBUTO.equals("12")) mes ="Dic";
													
						if(( CG_GRAFICA.equals("OPERACIONMN") || CG_GRAFICA.equals("OPERACIONDL")  )  &&  !CG_ATRIBUTO.equals(CG_ATRIBUTOAn) ){
							setCategory +=" <category label='"+mes+"' /> ";
						}					
						
						if(i==0){ 
							if(CG_GRAFICA.equals("OPERACIONMN")  &&  !CG_GRAFICA.equals(CG_GRAFICAAn)  && !CG_ATRIBUTO.equals(CG_ATRIBUTOAn)){						
								setValues +=" <set value='"+CG_VALOR+"' /> ";
								setValuesDL +=" <set value='0' /> ";
							}if(CG_GRAFICA.equals("OPERACIONDL") &&  !CG_ATRIBUTO.equals(CG_ATRIBUTOAn)  &&  !CG_ATRIBUTO.equals(CG_ATRIBUTOAn)  ){							
								setValuesDL +=" <set value='"+CG_VALOR+"' /> ";
								setValues +=" <set value='0' /> ";
							}
						}else  {	
							if(CG_GRAFICA.equals("OPERACIONDL")){
								if( !CG_GRAFICA.equals(CG_GRAFICAAn)   && !CG_ATRIBUTO.equals(CG_ATRIBUTOAn)   ){
									setValuesDL +=" <set value='"+CG_VALOR+"' /> ";	
								}else if( !CG_GRAFICA.equals(CG_GRAFICAAn)   && CG_ATRIBUTO.equals(CG_ATRIBUTOAn)  ){
									setValuesDL +=" <set value='"+CG_VALOR+"' /> ";
								}else if( CG_GRAFICA.equals(CG_GRAFICAAn)  &&   !CG_ATRIBUTO.equals(CG_ATRIBUTOAn)  ){
									setValuesDL +=" <set value='"+CG_VALOR+"' /> ";	
								}else{
									setValuesDL +=" <set value='0' /> ";
								}	
						}else if(CG_GRAFICA.equals("OPERACIONMN")){
							if( !CG_GRAFICA.equals(CG_GRAFICAAn)   && !CG_ATRIBUTO.equals(CG_ATRIBUTOAn)   ){
								setValues +=" <set value='"+CG_VALOR+"' /> ";
							}else if( !CG_GRAFICA.equals(CG_GRAFICAAn)   && CG_ATRIBUTO.equals(CG_ATRIBUTOAn)  ){
								setValues +=" <set value='"+CG_VALOR+"' /> ";
							}else if( CG_GRAFICA.equals(CG_GRAFICAAn)  &&   !CG_ATRIBUTO.equals(CG_ATRIBUTOAn)  ){
								setValues +=" <set value='"+CG_VALOR+"' /> ";	
							}else{
								setValues +=" <set value='0' /> ";
							}							
						}						
					}
					CG_ATRIBUTOAn= CG_ATRIBUTO;
					CG_GRAFICAAn= CG_GRAFICA;
				}					
				xmlOpe = "<chart formatNumberScale='0' caption='Operación mensual' subcaption='' showValues='0' "+
						"xAxisName='Mes' " +
						"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
						"useRoundEdges='1' exportEnabled='1' exportAtClient='0' exportShowMenuItem='0' "+
						"exportHandler='xxxx.jsp' exportAction='download' "+
						"logoPosition='TL' logoAlpha='30' "+
						"canvasBgAlpha='100' canvasBgColor='cfe0ef' "+
						"yAxisName=''>" +
						"<categories>" +
						setCategory+
						"</categories>" +
						"<dataset seriesName='MX' color='0e16dd' >" +				
						setValues+
						"</dataset>" +
						"<dataset seriesName='USD' color='dd0e16' >" +
						setValuesDL+
						"</dataset>" +				
						"</chart>";
					}
			}
					
			if(valProducto.equals("DE")){
				infoGrafica = "'VENCMESMN', 'VENCMESDL'";
			}else if(valProducto.equals("CE")){
				infoGrafica = "'VENCMESMN_CE', 'VENCMESDL_CE'";
			}
		
			List lstGraficaVencMes = graficasIf.getInfoGraficaIF(iNoCliente, infoGrafica);
		
			if(  lstGraficaVencMes!=null & lstGraficaVencMes.size()>0 ){
				String setCategory = "",setValues = "", setValuesDL = "";
				int iColor = 389;
				String CG_ATRIBUTO  = "",  CG_VALOR  = "", CG_GRAFICA  = "";
				for(int i=0; i<lstGraficaVencMes.size(); i++){
					HashMap mp = (HashMap)lstGraficaVencMes.get(i);
					CG_ATRIBUTO  = mp.get("CG_ATRIBUTO").toString();
					CG_VALOR  = mp.get("CG_VALOR").toString();
					CG_GRAFICA  = mp.get("CG_GRAFICA").toString();	
					
					setCategory +=" <category label='"+CG_ATRIBUTO+"' /> ";								
					if(CG_GRAFICA.equals("VENCMESMN")){
						setValues +=" <set value='"+CG_VALOR+"' /> ";
						setValuesDL +=" <set value='' /> ";
					}else if(CG_GRAFICA.equals("VENCMESDL")){
						setValues +=" <set value='' /> ";
						setValuesDL +=" <set value='"+CG_VALOR+"' /> ";
					}else  if(CG_GRAFICA.equals("VENCMESMN_CE")){
						setValues +=" <set value='"+CG_VALOR+"' /> ";
						setValuesDL +=" <set value='' /> ";
					}else if(CG_GRAFICA.equals("VENCMESDL_CE")){
						setValues +=" <set value='' /> ";
						setValuesDL +=" <set value='"+CG_VALOR+"' /> ";
					}									
				}		
				
				
				xmlVenc = "<chart formatNumberScale='0' caption='Vencimientos en el Mes de "+mesActual+"' subcaption='' showValues='0' "+
					"xAxisName='Fecha' " +
					"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
					"useRoundEdges='1' exportEnabled='1' exportAtClient='0' exportShowMenuItem='0' "+
					"exportHandler='xxxx.jsp' exportAction='download' "+
					"logoPosition='TL' logoAlpha='30' "+
					"canvasBgAlpha='100' canvasBgColor='cfe0ef' "+
					"yAxisName='Saldo'>" +
					"<categories>" +
					setCategory+
					"</categories>" +
					"<dataset seriesName='MX' color='dd6d0e' >" +
					setValues+
					"</dataset>" +
					"<dataset seriesName='USD' color='dd0e16' >" +
					setValuesDL+
					"</dataset>" +	
					"</chart>";
			}
		} //if para DE y CE
//*******************************************************************************************************/

		else if (valProducto.equals("CED")) {  //Credito EDucativo
			//recuerda dbm que solo es para una de las 2 de arriba 
			
			// ***** ***** ***** ***** FECHAS ***** ***** ***** ***** //
			Date ahora = new Date();
			SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
         String fecha_Actual =  formateador.format(ahora);
			String fecha_Anterior ="";
			String diaAhora="", mesAhora="", anioAhora="";
			String mesAnterior="", anioAnterior="", mesAnte="";
			String infoGraficaMesActual = "", infoGraficaMesAnterior="", infoGraficaNoAcreditados;
			StringTokenizer token = new StringTokenizer(fecha_Actual, "-");
			while(token.hasMoreTokens()) {
				diaAhora = token.nextToken();
				mesAhora = token.nextToken();
				anioAhora= token.nextToken();
			}
			int mes = Integer.parseInt(mesAhora);
			
			if(mesAhora.equals("01")) {
				anioAnterior = String.valueOf(Integer.parseInt(anioAhora) -1);
				mesAnterior  = "12";
				fecha_Anterior = "01-"+mesAnterior+"-"+anioAnterior;
			}
			else if (mes<10) {
				anioAnterior = anioAhora;
				mesAnte		 = String.valueOf(Integer.parseInt(mesAhora) -1);
				mesAnterior = "0"+mesAnte;
				fecha_Anterior = "01-"+mesAnterior+"-"+anioAnterior;
			}
			else {
				anioAnterior = anioAhora;
				mesAnterior  = String.valueOf(Integer.parseInt(mesAhora) -1);
				fecha_Anterior = "01-"+mesAnterior+"-"+anioAnterior;
			}
			infoGraficaMesAnterior = "'PerfilUN"+mesAnterior+"/"+anioAnterior+"'";
			infoGraficaMesActual   = "'PerfilUN"+mesAhora+"/"+anioAhora+"'"; 
			////////************************************************/////////
			
			List lstGraficaUNIMesAnt = graficasUni.getInfoGraficaUni(iNoCliente, infoGraficaMesAnterior);
			String aceptados ="", rechazados="";
			if(  lstGraficaUNIMesAnt!=null & lstGraficaUNIMesAnt.size()>0 ){
				String setCategory = "",setValuesA = "", setValuesR = "";
				int iColor = 389;
				String CG_ATRIBUTO  = "",  CG_VALORA  = "", CG_VALORR = "", CG_GRAFICA  = "";
				for(int i=0; i<lstGraficaUNIMesAnt.size(); i++){
					HashMap mp = (HashMap)lstGraficaUNIMesAnt.get(i);
					CG_ATRIBUTO  = mp.get("CG_ATRIBUTO").toString();
					CG_VALORA  	 = mp.get("CG_VALOR").toString();
					CG_VALORR  	 = mp.get("CG_VALORCE").toString();
					CG_GRAFICA   = mp.get("CG_GRAFICA").toString();	
					
					setCategory +=" <category label='"+CG_ATRIBUTO+"' /> ";		
					
					setValuesA +=" <set value='"+CG_VALORA+"' /> ";
					setValuesR +=" <set value='"+CG_VALORR+"' /> ";			
				}		
			mesAnterior= fechaC.getMesNombre(fecha_Anterior);	//obtiene el nombre del mes anterior
			xmlUniAnt = "<chart formatNumberScale='0' caption='Registros de "+mesAnterior+" "+anioAnterior+"' subcaption='' showValues='0' "+
						"xAxisName='Intermediario Financiero' " +
						"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
						"useRoundEdges='1' exportEnabled='1' exportAtClient='0' exportShowMenuItem='0' "+
						"exportHandler='xxxx.jsp' exportAction='download' "+
						"chartLeftMargin='2' logoPosition='TL' logoAlpha='30' "+
						"canvasBgAlpha='100' canvasBgColor='cfe0ef' "+
						"yAxisName='Núm. de Estudiantes'>" +
						"<categories>" +
						setCategory+
						"</categories>" +
						"<dataset seriesName='Estudiantes VALIDOS' color='3a843e' >" +				
						setValuesA+
						"</dataset>" +
						"<dataset seriesName='Estudiantes RECHAZADOS' color='dd0e16' >" +
						setValuesR+
						"</dataset>" +				
						"</chart>";			
			}
					
			List lstGraficaUNIMesAct = graficasUni.getInfoGraficaUni(iNoCliente, infoGraficaMesActual);
			String aceptadosA ="", rechazadosA="";
			if(  lstGraficaUNIMesAct!=null & lstGraficaUNIMesAct.size()>0 ){
				String setCategory = "",setValuesAA = "", setValuesRA = "";
				int iColor = 389;
				String CG_ATRIBUTOA  = "",  CG_VALORA  = "", CG_VALORR = "" ,CG_GRAFICAA  = "";
				for(int i=0; i<lstGraficaUNIMesAct.size(); i++){
					HashMap mp = (HashMap)lstGraficaUNIMesAct.get(i);
					CG_ATRIBUTOA  = mp.get("CG_ATRIBUTO").toString();
					CG_VALORA  	  = mp.get("CG_VALOR").toString();
					CG_VALORR  	  = mp.get("CG_VALORCE").toString();
					CG_GRAFICAA   = mp.get("CG_GRAFICA").toString();	
					
					setCategory +=" <category label='"+CG_ATRIBUTOA+"' /> ";		
					
					
					setValuesAA +=" <set value='"+CG_VALORA+"' /> ";
					setValuesRA +=" <set value='"+CG_VALORR+"' /> ";				
				}		
				
			xmlUniAct = "<chart formatNumberScale='0' caption='Registros de "+mesActual+" "+anioAhora+"' subcaption='' showValues='0' "+
						"xAxisName='Intermediario Financiero' " +
						"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
						"useRoundEdges='1' exportEnabled='1' exportAtClient='0' exportShowMenuItem='0' "+
						"exportHandler='xxxx.jsp' exportAction='download' " +
						//" xAxisNamePadding='20' valuePadding='20'  chartLeftMargin='2' legendPadding='20' "+
						" chartLeftMargin='2' logoPosition='TL' logoAlpha='80' "+
						"canvasBgAlpha='100' canvasBgColor='cfe0ef' "+
						"yAxisName='Núm. de Estudiantes'>" +
						"<categories>" +
						setCategory+
						"</categories>" +
						"<dataset seriesName='Estudiantes VALIDOS' color='3a843e' >" +				
						setValuesAA+
						"</dataset>" +
						"<dataset seriesName='Estudiantes RECHAZADOS' color='dd0e16' >" +
						setValuesRA+
						"</dataset>" +				
						"</chart>";			
			}			
				// ****** ****** ****** Gráfica Núm de Acreditados****** ****** ******* //		
			infoGraficaNoAcreditados = "'Acre_PefilUNI'";
			List lstGraficaNumAcred = graficasUni.getInfoGraficaUni(iNoCliente, infoGraficaNoAcreditados);
			//String aceptadosA ="", rechazadosA="";
			if(  lstGraficaNumAcred!=null & lstGraficaNumAcred.size()>0 ){
				String setCategory = "",setValues = "", setValuesR = "";
				int iColor = 389;
				String CG_ATRIBUTO  = "",  CG_VALOR  = "";
				StringTokenizer tkn_Attri = null; 	String txt_if="",acred="";
				String txt_monto = "Monto total de Financiamiento";
				for(int i=0; i<lstGraficaNumAcred.size(); i++){
					HashMap mp = (HashMap)lstGraficaNumAcred.get(i);
					CG_ATRIBUTO  = mp.get("CG_ATRIBUTO").toString();
					CG_VALOR  	 = mp.get("CG_VALOR").toString();
					
					//setCategory +="<set label='" +CG_ATRIBUTO+"' value='"+CG_VALOR+ "'/> ";				
					tkn_Attri = new StringTokenizer(CG_ATRIBUTO, "/");
					txt_if = tkn_Attri.nextToken();
					acred  = tkn_Attri.nextToken();
										
					setCategory +="<set label='" +txt_if+"\n"+acred+"\n"+ txt_monto+"' value= '"+CG_VALOR+ "'/> ";									
				}		
				xmlNumAcred = "<chart caption='Núm. Acreditados al "+diaAhora+ "/"+mesAhora+"/"+anioAhora+"'"+
						"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
						"exportEnabled='1' exportAtClient='0' exportShowMenuItem='1' logoURL='/nafin/00archivos/15cadenas/15archcadenas/logos/nafin.gif' "+
						"exportHandler='/nafin/FCExporter' exportAction='download' smartLineColor='FFFFFF' "+
						"logoPosition='TL' logoAlpha='30' showExportDataMenuItem='1' "+
						"labelSepChar=':'"+ //para sustituir la coma
						" formatNumberScale='0' showPercentInToolTip = '0' showPercentValues='0' numberPrefix='$'>"+			
						"legendPosition='BOTTOM'  legendNumColumns = '2'"+
						setCategory +
						" </chart>";
							
			}
					
					
				jsonObj.put("success", Boolean.TRUE);
				jsonObj.put("cargaTerminada", new Boolean(cargaTerminada));
				jsonObj.put("xmlUniAnt", xmlUniAnt);
				jsonObj.put("xmlUniAct", xmlUniAct);
				jsonObj.put("xmlNumAcred",xmlNumAcred);
				jsonObj.put("valProducto", valProducto);	
				infoRegresar = jsonObj.toString();
				System.out.println("infoRegresar----->" + infoRegresar);
		}
	} //bestatus
	
	
						
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("cargaTerminada", new Boolean(cargaTerminada));
	jsonObj.put("xmlOpe", xmlOpe);
	jsonObj.put("xmlVenc", xmlVenc);	
	jsonObj.put("valProducto", valProducto);	
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>