	function estableceMensajes(grafica) {
		grafica.configure( {
				"ChartNoDataText" : "No hay informaci�n para graficar",
				"InvalidXMLText"  : "Informaci�n incorrecta",
				"PBarLoadingText" : "Cargando Gr�fica. Por favor espere",
				"XMLLoadingText"  : "Obteniendo informaci�n. Por favor espere",
				"ParsingDataText" : "Procesando informaci�n. Por favor espere",
				"RenderingChartText" : "Generando Gr�fica. Por favor espere",
				"LoadDataErrorText" : "Error al carga la informaci�n"
		});
	}
	var myChart3 = new FusionCharts( NE.appWebContextRoot+"/00utils/charts/MSBar2D.swf","myChartId1","350","400","0","1" );
	var myChart4 = new FusionCharts( NE.appWebContextRoot+"/00utils/charts/MSBar2D.swf","myChartId2","350","400","0","1" );
	var myChart5 = new FusionCharts( NE.appWebContextRoot+"/00utils/charts/Pie3D.swf","myChartId4","700","250","0","1" );
	/*var contenedorCED='<div class="formas" align="center">Gr�ficas que despliegan el total de Estudiantes que la Universidad-Campus asign� con '+
	' <b>REGISTRO VALIDO-UNIVERSIDAD y REGISTRO RECHAZADO-UNIVERSIDAD</b></div> <table> <tr><td id="chartContainer4" ></td> '+
	' <td id="chartContainer5" ></td></tr></tr></table>'+
	'<div class="formas" align="center">Gr�fica que despliega el <b>acumulado</b> del total de alumnos que la'+
	' Universidad - Campus asign� con estatus  <b>REGISTRO VALIDO-UNIVERSIDAD</b> y el Monto Total de Financiamiento</div>'+
	'<table> <tr><td id="chartContainer6"></td> </tr></table>';
	*/	
	Ext.onReady(function() {
	
		var ObjGral = {
				valProducto : 'CED'
		};
	
		taskGraficas = {
			run: function(){
				requestCarga(ObjGral.valProducto);
			},
			interval: 10000 //10 second
		}

		var procesarSuccesFailureGeneraXML = function(opts, success, response) {
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				var resp = 	Ext.util.JSON.decode(response.responseText);
							
				if(!resp.cargaTerminada){
					Ext.get('areaContenido').mask('Cargando informaci�n...','x-mask');
					var runner = new Ext.util.TaskRunner();
					runner.start(taskGraficas);					
				}else{
									
					Ext.getCmp('contenedorPrincipal').show();
					//if(resp.valProducto =='CED'){							
						estableceMensajes(myChart3);
						myChart3.setXMLData(resp.xmlUniAnt);
						myChart3.render("chartContainer1");
					
						estableceMensajes(myChart4);
						myChart4.setXMLData(resp.xmlUniAct);
						myChart4.render("chartContainer2");	
					
						estableceMensajes(myChart5)
						myChart5.setXMLData(resp.xmlNumAcred);
						myChart5.render("chartContainer3");	
						
					//}
					
					Ext.get('areaContenido').unmask();
				}
	
			} else {
				Ext.get('areaContenido').unmask();
				NE.util.mostrarConnError(response,opts);
			}
		}

		var requestCarga = function(producto){				
			Ext.Ajax.request({
				url: 'index.data.jsp',
				params: {
					informacion: 'generaGraficas',
					valProducto:producto
				},
				callback: procesarSuccesFailureGeneraXML
			});  
		}
		
		
		var panel = new Ext.Panel({
			applyTo:'areaLeyenda',
			width: 945,
			frame: false,
			html: '<table border=0><tr><td><p style="text-align:justify; background:#F3F4F5; font-size:80%">La informaci�n debidamente obtenida y autorizada por el administrador de la plataforma de NAFINET (Nacional Financiera S.N.C., I.B.D) '+
				'tendr� valor probatorio en juicio, en tanto que la obtenci�n de informaci�n almacenada en la base de datos y archivos de la misma, ' + 
				' sin contar con la autorizaci�n correspondiente, o el uso indebido de dicha informaci�n, ser� sancionada en t�rminos de Ley de Instituciones ' +
				' de Cr�dito y dem�s ordenamientos legales aplicables. </p></td></tr></table>'
		});

		requestCarga(ObjGral.valProducto);
		/*
		var tabEducativo = {
			title: 'CREDITO EDUCATIVO',
			id: 'tabEducarivo',
			html :contenedorCED			
		};
		
		var tp = new Ext.TabPanel({
			activeTab: 0,
			width:700,
			height: 800,
			plain:true,	
			deferredRender: false,
			defaults:{autoHeight: true},		
			items:[tabEducativo],
			listeners: {
				afterrender: function(tab, panel){	
					requestCarga("CED");	

				}
			}
		});
		*/
		//-------------------------------- PRINCIPAL -----------------------------------
	
		//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
		var pnl = new Ext.Container({
			id: 'contenedorPrincipal',
			layout:'form',
			applyTo: 'areaContenido',		
			hidden: true
			//,items: [	tp			]
		});
	//-------------------------------- ----------------- -----------------------------------
		
	});

