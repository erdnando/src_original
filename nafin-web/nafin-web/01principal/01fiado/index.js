Ext.onReady(function() {
	//-------------------------------- PRINCIPAL -----------------------------------
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		layout:'form',
		applyTo: 'areaContenido',		
		hidden: false
	});
	
	var panel = new Ext.Panel({
		applyTo:'areaLeyenda',
		width: 945,
		frame: false,
		html: '<table border=0><tr><td><p style="text-align:justify; background:#F3F4F5; font-size:80%">La informaci�n debidamente obtenida y autorizada por el administrador de la plataforma de NAFINET (Nacional Financiera S.N.C., I.B.D) '+
			'tendr� valor probatorio en juicio, en tanto que la obtenci�n de informaci�n almacenada en la base de datos y archivos de la misma, ' + 
			' sin contar con la autorizaci�n correspondiente, o el uso indebido de dicha informaci�n, ser� sancionada en t�rminos de Ley de Instituciones ' +
			' de Cr�dito y dem�s ordenamientos legales aplicables. </p></td></tr></table>'
	});
	//-----------------------------------------------------------------------------
		
});

