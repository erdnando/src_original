<!DOCTYPE html>
<%@ page import="java.util.*, netropology.utilerias.*"	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%
String logo = "";
logo = appWebContextRoot + "/01principal/imagenes/home_cedi_p1.png";
String logo1 = "";
logo1 = appWebContextRoot + "/01principal/imagenes/home_cedi_p2.png";

//Estas lineas solo van a ir los index.jsp, para llevar el registro de que paso por ahi el afiliado
List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
if (pantallasNavegacionComplementaria != null) {
	pantallasNavegacionComplementaria.remove("/01principal/01cedi/index.jsp");  //El nombre debe coincidir con el especificado en Navegacion
}
//Si llega a esta pantalla se coloca una variable en sesión para indicar que la sesión esta lista para empezar a trabajar
if (pantallasNavegacionComplementaria != null && pantallasNavegacionComplementaria.isEmpty()) {
	session.setAttribute("inicializar.FINALIZADO", "S");
} else {
	throw new AppException("La sesión del usuario no fue inicializada correctamente");
}
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>

<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="index.js?<%=session.getId()%>"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<div id="logo_cliente" class="left">
							<%@ include file="menuLateral.jspf"%>
							<div id="areaContenido" style="width:700px">
								<table width="700" align="center" border=0>
								<tr >
									<td  align="justify" colspan = "1" width="700"><p  style="text-align:ALIGN_JUSTIFIED;  font-size:11">Bienvenido al portal NAFINET, en el cual podr&aacute; realizar un ejercicio de viabilidad para su incorporaci&oacute;n como Intermediario Financiero a la red de Intermediarios Financieros de Nacional Financiera, S.N.C.</td>
								</tr>
								<tr >
									<td colspan = "1" width="700">&nbsp;&nbsp;</td>
								</tr>	
								<tr >
									<td align="justify" colspan = "1" width="700"><p  style="text-align:center;  font-size:11">Lo invitamos a ingresar a la herramienta electr&oacute;nica de auto evaluaci&oacute;n en el men&uacute;:"<b>Calif&iacute;cate</b>".</p></td>
								</tr>		
								<tr >
									<td colspan = "1" width="700">&nbsp;&nbsp;</td>
								</tr>	
								<tr >
									<td align="justify" colspan = "1" width="700"><p  style="text-align:ALIGN_JUSTIFIED;  font-size:11">
									En este men&uacute encontrar&aacute 5 secciones a requisitar; en la secci&oacute;n <u>"Informaci&oacute;n Financiera"</u> se requiere contar con informaci&oacute;n financiera del mes m&aacute;s reciente y del cierre de los dos &uacute;ltimos a&ntilde;os a la fecha de ingreso a esta herramienta.</p></td>
								</tr>
								<tr >
									<td colspan = "1" width="700">&nbsp;&nbsp;</td>
								</tr>	
								<tr >
									<td align="justify" colspan = "1" width="700"><p  style="text-align:ALIGN_JUSTIFIED;  font-size:11">Una vez que la herramienta le proporcione una "C&eacute;dula Electr&oacute;nica" en el que indique su viabilidad de incorporaci&oacute;n, podr&aacute; ingresar al siguiente men&uacute; de <b>"Seguimiento Pre-an&aacute;lisis"</b> en donde se le indicar&aacute;n los pasos a seguir y un Ejecutivo se <p>pondr&aacute; en contacto con usted.</p></td>
								</tr>
								<tr >
									<td colspan = "1" width="700">&nbsp;&nbsp;</td>
								</tr>	
								<tr >
									<td align="justify" colspan = "1" width="700"><p  style="text-align:center;  font-size:11">Cabe se&ntilde;alar que de las etapas siguientes, usted se encuentra en la Etapa 1:</p></td>
								</tr>
								<tr>
									<td colspan="2"  width="700"> <center><img src="<%=logo%>"/></center></td>							
								</tr>
								<tr >
									<td align="justify" colspan = "1" width="700"><p  style="text-align:ALIGN_JUSTIFIED;  font-size:11">Una vez que haya obtenido el Dictamen de Viabilidad,  se dar&aacute; inicio al proceso de incorporaci&oacute;n en Nacional Financiera,<p> mediante el siguiente flujo, un Ejecutivo le dar&aacute; el seguimiento correspondiente:</p></td>
								</tr>
								<tr >
									<td colspan = "1" width="700"></td>
								</tr>
								<tr>
									<td colspan="2"  width="700" >  <center> <img src="<%=logo1%>"/></center></td>							
								</tr>
								<tr>
									<td colspan="2"><div id="chartContainer5" ></div></td>							
								</tr>
								<tr>
									<td colspan="2"><div id="chartContainer76"></div></td>							
								</tr>						
								</table>
							</div>
						
						</div>
				  </div>
				<div id="areaLeyenda"></div>
				<%@ include file="pie.jspf"%>
				
<form id='formAux' name="formAux" target='_new'></form>

<form id='formParametros' name="formParametros">
			<input type="hidden" id="strPerfil" name=" " value="<%=strPerfil%>"/>	
		</form>
</body>
</html>
