<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.descuento.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

ISeleccionDocumento seleccionDocumentoEJB = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	
if (informacion.equals("ObtieneTiposDeFactoraje")) {
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("factorajeVencido", new Boolean(seleccionDocumentoEJB.manejaFactoraje(iNoEPO,"V")));
	jsonObj.put("factorajeMandato", new Boolean(seleccionDocumentoEJB.manejaFactoraje(iNoEPO,"M")));
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>