Ext.onReady(function() {

//-----------------------------------------------------------------------------------

//-----------------------------HANDLERS--------------------------------------

	var procesaValoresIniciales = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var  tipo_credito = resp.tipo_credito;	
			var  strPerfil = resp.strPerfil;	
			
			if(strPerfil=='ADMIN PYME'){ 
			if(tipo_credito=='F'){
				Ext.getCmp('panel').hide();
				Ext.getCmp('panelF').show();
			}else if(tipo_credito=='D'){
				 window.location = NE.appWebContextRoot + '/24finandist/24pyme/24forma01ext.jsp?idMenu=24PYME24FORMA1';
			}else if(tipo_credito=='C'){
			 window.location = NE.appWebContextRoot + '/24finandist/24pyme/24forma02ext.jsp?idMenu=24PYME24FORMA2'; 
			}else if(tipo_credito=='A'){
				Ext.getCmp('panelF').hide();
				Ext.getCmp('panel').show();
				}
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	//FUNCION: determina valores que se necesitan al momento de cargar la pantalla por primera vez
	var procesarSuccessValoresIniNormal = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			objGralNormal.inicial = 'N'
			objGralNormal.strTipoUsuario = resp.strTipoUsuario;
			objGralNormal.pymeBloq = resp.pymeBloq;
			objGralNormal.tipo_servicio = resp.tipo_servicio;
			objGralNormal.urlCapturaDatos = resp.urlCapturaDatos;
			objGralNormal.fecDiaSigHabil = resp.fecDiaSigHabil;
			objGralNormal.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGralNormal.msgOperFact24hrs = resp.msgOperFact24hrs;
			objGralNormal.msgFecVencPyme = resp.msgFecVencPyme;
			objGralNormal.iNoEPO = resp.iNoEPO;
			objGralNormal.msgSinNumeroProveedor = resp.msgSinNumeroProveedor;
			
			if(objGralNormal.pymeBloq!='S'){
				
				var cboEpo = Ext.getCmp('cboEpoN');
				if(cboEpo.getValue()==''){
					if (storeCatEpoNormalData.findExact("clave", objGralNormal.iNoEPO) != -1 ) {
						cboEpo.setValue(objGralNormal.iNoEPO);
					}
				}
				
				if(resp.msgSinNumeroProveedor!='') {
					tp.el.mask(resp.msgSinNumeroProveedor);
					return;
					//var objMsg = Ext.getCmp('mensajes1');
					//objMsg.body.update(resp.msgSinNumeroProveedor);
					//objMsg.show();
					//fp.hide();
				}
	
				
				if(resp.msgError!=''){
					var objMsg = Ext.getCmp('mensajesN');
					objMsg.body.update(resp.msgError);
					objMsg.show();
					fpNormal.hide();
				}else if(objGralNormal.msgDiaSigHabil!='' || objGralNormal.msgFecVencPyme!=''){
					var objMsg = Ext.getCmp('mensajesN');
					objMsg.body.update(objGralNormal.msgDiaSigHabil+'<br>'+objGralNormal.msgFecVencPyme);
					objMsg.show();
					fpNormal.show();
				}else{
					fpNormal.show();
				}
			}else{
				var objMsg = Ext.getCmp('mensajesN');
					objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
							'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes'+
							'Cd. De M�xico 50-89-61-07. <br>'+
							'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
					objMsg.show();
					fpNormal.hide();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessValoresIniVencido = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			objGralVencido.inicial = 'N'
			objGralVencido.strTipoUsuario = resp.strTipoUsuario;
			objGralVencido.tipo_servicio = resp.tipo_servicio;
			objGralVencido.urlCapturaDatos = resp.urlCapturaDatos;
			objGralVencido.fecDiaSigHabil = resp.fecDiaSigHabil;
			objGralVencido.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGralVencido.msgOperEpoFact24hrs = resp.msgOperEpoFact24hrs;
			objGralVencido.msgFecVencPyme = resp.msgFecVencPyme;
			objGralVencido.iNoEPO = resp.iNoEPO;
			
			var cboEpo = Ext.getCmp('cboEpoV');
			if(cboEpo.getValue()==''){
				if (storeCatEpoVencidoData.findExact("clave", objGralVencido.iNoEPO) != -1 ) {
					cboEpo.setValue(objGralVencido.iNoEPO);
				}
			}
			
			if(resp.msgError!=''){
				var objMsg = Ext.getCmp('mensajesV');
				objMsg.body.update(resp.msgError);
				objMsg.show();
				Ext.getCmp('pnlVencido').hide();
			}else if(objGralVencido.msgDiaSigHabil!='' || objGralVencido.msgFecVencPyme!=''){
				var objMsg = Ext.getCmp('mensajesV');
				objMsg.body.update(objGralVencido.msgDiaSigHabil+'<br>'+objGralVencido.msgFecVencPyme);
				Ext.getCmp('pnlVencido').show();
			}else{
				Ext.getCmp('pnlVencido').show();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessValoresIniMandato = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			objGralMandato.inicial = 'N'
			objGralMandato.strTipoUsuario = resp.strTipoUsuario;
			
			objGralMandato.urlCapturaDatos = resp.urlCapturaDatos;
			objGralMandato.msgFecVencPyme = resp.msgFecVencPyme;
			objGralMandato.iNoEPO = resp.iNoEPO;
			
			var cboEpo = Ext.getCmp('cboEpoM');
			if(cboEpo.getValue()==''){
				if (storeCatEpoMandatoData.findExact("clave", objGralMandato.iNoEPO) != -1 ) {
					cboEpo.setValue(objGralMandato.iNoEPO);
				}
			}

			if(resp.msgError!=''){
				var objMsg = Ext.getCmp('mensajesM');
				objMsg.body.update(resp.msgError);
				objMsg.show();
				Ext.getCmp('pnlMandato').hide();
			}else if(objGralMandato.msgFecVencPyme!=''){
				var objMsg = Ext.getCmp('mensajesM');
				objMsg.body.update(objGralMandato.msgFecVencPyme);
				objMsg.show();
				Ext.getCmp('pnlMandato').show();
			}else{
				Ext.getCmp('pnlMandato').show();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	


	//STORES---------------------------------------------------------------------

	
/*	var storeCatMandanteData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : NE.appWebContextRoot + '/13descuento/13pyme/13forma9ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMandante'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
*/	
	//COMPONENTES---------------------------------------------------------



	//CONTENEDORES--------------------------------------------------------
	var panel = new Ext.Panel({
		width: 600,
		id:'panel',
		hidden: true,
		height: 150,
		frame: true,
		style: 'margin: 0 auto',
		layout: {
			type: 'vbox',
			align: 'stretch',
			//pack: 'center',
			padding: '5'
			
		},
		defaults:{margins:'0 0 20 0'},
		items: [
			{
				xtype:'button',				
            text: '<b>Ir a Selecci�n de Documentos para Financiamiento&nbsp;&nbsp;&nbsp;</b>',
				height: 50,				
				//flex: 1,s
				icon: NE.appWebContextRoot + '/00utils/gif/ico22/agt_forward.png',
				iconAlign: 'right',
				cls: 'x-btn-text-icon',
            handler: function(){
					window.location = NE.appWebContextRoot + '/24finandist/24pyme/24forma01ext.jsp?idMenu=24PYME24FORMA1';
				}
			},
			{
				xtype:'button',				
            text: '<b>Ir a Selecci�n de Documentos Cr�dito en Cuenta Corriente&nbsp;&nbsp;&nbsp;</b>',
				height: 50,
				icon: NE.appWebContextRoot + '/00utils/gif/ico22/agt_forward.png',
				iconAlign: 'right',
				cls: 'x-btn-text-icon',
				//flex: 1,
            handler: function(){
					window.location = NE.appWebContextRoot + '/24finandist/24pyme/24forma02ext.jsp?idMenu=24PYME24FORMA2';
				}
			}			
		]
	});

	var panelF = new Ext.Panel({
		width: 600,
		id:'panelF',
		hidden: true,
		height: 100,
		frame: true,
		style: 'margin: 0 auto',
		layout: {
			type: 'vbox',
			align: 'stretch',
			//pack: 'center',
			padding: '5'
			
		},
		defaults:{margins:'0 0 20 0'},
		items: [			
			{
				xtype:'button',				
            text: '<b>Ir a Selecci�n Factoraje con Recursos</b>',
				height: 50,
				icon: NE.appWebContextRoot + '/00utils/gif/ico22/agt_forward.png',
				iconAlign: 'right',
				cls: 'x-btn-text-icon',
				//flex: 1,
            handler: function(){
					window.location = NE.appWebContextRoot + '/24finandist/24pyme/24forma02Fext.jsp?idMenu=24PYME24FORMA2F';
				}
			}
		]
	});
//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			panel,
			panelF
		]
	});


	
		Ext.Ajax.request({
			url: 'indexDist.data.jsp',
			params: {
				informacion: "valoresIniciales"				
			},			
			callback: procesaValoresIniciales				
		});	
	
	
	
//-------------------------------- ----------------- -----------------------------------
	
});