Ext.onReady(function() {

//-----------------------------------------------------------------------------------

	var objGralNormal={
		inicial:'S',
		strTipoUsuario:null,
		pymeBloq:null,
		tipo_servicio:null,
		iNoEPO:null,
		urlCapturaDatos:null,
		fecDiaSigHabil:null,
		msgDiaSigHabil:null,
		msgOperFact24hrs:null,
		msgFecVencPyme:null,
		valorTC: null,
		paramEpoPef: null,
		sLimiteActivo: null,
		numCamposDinamicos: null,
		mapNombres:null,
		operaNotasDeCredito: null,
		aplicarNotasDeCreditoAVariosDoctos: null,
		bancoDeFondeo: null,
		limites: null,
		sTipoLimite: null,
		monto: null,
		descuento: null,
		interes: null,
		recibir: null,
		valEPO: null,
		valIF: null,
		sFecVencLineaCred: 'N',
		msgError: null,
		msgSinNumeroProveedor : null
	}
	
	
	var objGralVencido={
		inicial:'S',
		strTipoUsuario:null,
		tipo_servicio:null,
		urlCapturaDatos:null,
		fecDiaSigHabil:null,
		msgDiaSigHabil:null,
		msgOperEpoFact24hrs:null,
		msgFecVencPyme:null,
		iNoEPO:null,
		valorTC: null,
		maximos: null,
		utilizados: null,
		intermediarios: null,
		ifValidar: null,
		lineasDisp: null,
		tipoLimite: null,
		montoComprometido: null,
		sNombrePyme: null,
		operaNotasDeCredito: null,
		aplicarNotasDeCreditoAVariosDoctos: null,
		shownotas: null,
		valEPO: null,
		valIF: null,
		msgError: null
	}
	
	var objGralMandato={
		inicial:'S',
		strTipoUsuario:null,
		tipo_servicio:null,
		urlCapturaDatos:null,
		fecDiaSigHabil:null,
		msgDiaSigHabil:null,
		msgOperEpoFact24hrs:null,
		msgFecVencPyme:null,
		iNoEPO:null,
		valorTC: null,
		maximos: null,
		utilizados: null,
		intermediarios: null,
		ifValidar: null,
		lineasDisp: null,
		tipoLimite: null,
		montoComprometido: null,
		sNombrePyme: null,
		operaNotasDeCredito: null,
		aplicarNotasDeCreditoAVariosDoctos: null,
		shownotas: null,
		valEPO:null,
		valIF:null,
		msgError: null
	}
//-----------------------------HANDLERS--------------------------------------
	//FUNCION: determina valores que se necesitan al momento de cargar la pantalla por primera vez
	var procesarSuccessValoresIniNormal = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			objGralNormal.inicial = 'N'
			objGralNormal.strTipoUsuario = resp.strTipoUsuario;
			objGralNormal.pymeBloq = resp.pymeBloq;
			objGralNormal.tipo_servicio = resp.tipo_servicio;
			objGralNormal.urlCapturaDatos = resp.urlCapturaDatos;
			objGralNormal.fecDiaSigHabil = resp.fecDiaSigHabil;
			objGralNormal.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGralNormal.msgOperFact24hrs = resp.msgOperFact24hrs;
			objGralNormal.msgFecVencPyme = resp.msgFecVencPyme;
			objGralNormal.iNoEPO = resp.iNoEPO;
			objGralNormal.msgSinNumeroProveedor = resp.msgSinNumeroProveedor;
			
			if(resp.bloqueoPymeEpo!='B'){
			
			if(objGralNormal.pymeBloq!='S'){
				
				var cboEpo = Ext.getCmp('cboEpoN');
				if(cboEpo.getValue()==''){
					if (storeCatEpoNormalData.findExact("clave", objGralNormal.iNoEPO) != -1 ) {
						cboEpo.setValue(objGralNormal.iNoEPO);
					}
				}
				
				//if(resp.msgSinNumeroProveedor!='') {
					//tp.el.mask(resp.msgSinNumeroProveedor);
					//return;
					//var objMsg = Ext.getCmp('mensajes1');
					//objMsg.body.update(resp.msgSinNumeroProveedor);
					//objMsg.show();
					//fp.hide();
				//}
	
				
				if(resp.msgError!=''){
					var objMsg = Ext.getCmp('mensajesN');
					objMsg.body.update(resp.msgError);
					objMsg.show();
					fpNormal.hide();
				}else if(objGralNormal.msgDiaSigHabil!='' || objGralNormal.msgFecVencPyme!=''){
					var objMsg = Ext.getCmp('mensajesN');
					objMsg.body.update(objGralNormal.msgDiaSigHabil+'<br>'+objGralNormal.msgFecVencPyme);
					objMsg.show();
					fpNormal.show();
					Ext.getCmp('cfFechaVenc').doLayout();
				}else{
					fpNormal.show();
					Ext.getCmp('cfFechaVenc').doLayout();
				}
			}else{
				var objMsg = Ext.getCmp('mensajesN');
					objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
							'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes'+
							'Cd. De M�xico 50-89-61-07. <br>'+
							'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
					objMsg.show();
					fpNormal.hide();
			}
			
			
			}else  {
			
				var objMsg = Ext.getCmp('mensajesN');
					objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
							'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes'+
							'Cd. De M�xico 50-89-61-07. <br>'+
							'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
					objMsg.show();
					fpNormal.hide();
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessValoresIniVencido = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			objGralVencido.inicial = 'N'
			objGralVencido.strTipoUsuario = resp.strTipoUsuario;
			objGralVencido.tipo_servicio = resp.tipo_servicio;
			objGralVencido.urlCapturaDatos = resp.urlCapturaDatos;
			objGralVencido.fecDiaSigHabil = resp.fecDiaSigHabil;
			objGralVencido.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGralVencido.msgOperEpoFact24hrs = resp.msgOperEpoFact24hrs;
			objGralVencido.msgFecVencPyme = resp.msgFecVencPyme;
			objGralVencido.iNoEPO = resp.iNoEPO;
			
			var cboEpo = Ext.getCmp('cboEpoV');
			if(cboEpo.getValue()==''){
				if (storeCatEpoVencidoData.findExact("clave", objGralVencido.iNoEPO) != -1 ) {
					cboEpo.setValue(objGralVencido.iNoEPO);
				}
			}
			
			if(resp.msgError!=''){
				var objMsg = Ext.getCmp('mensajesV');
				objMsg.body.update(resp.msgError);
				objMsg.show();
				Ext.getCmp('pnlVencido').hide();
			}else if(objGralVencido.msgDiaSigHabil!='' || objGralVencido.msgFecVencPyme!=''){
				var objMsg = Ext.getCmp('mensajesV');
				objMsg.body.update(objGralVencido.msgDiaSigHabil+'<br>'+objGralVencido.msgFecVencPyme);
				Ext.getCmp('pnlVencido').show();
			}else{
				Ext.getCmp('pnlVencido').show();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessValoresIniMandato = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			objGralMandato.inicial = 'N'
			objGralMandato.strTipoUsuario = resp.strTipoUsuario;
			
			objGralMandato.urlCapturaDatos = resp.urlCapturaDatos;
			objGralMandato.msgFecVencPyme = resp.msgFecVencPyme;
			objGralMandato.iNoEPO = resp.iNoEPO;
			
			var cboEpo = Ext.getCmp('cboEpoM');
			if(cboEpo.getValue()==''){
				if (storeCatEpoMandatoData.findExact("clave", objGralMandato.iNoEPO) != -1 ) {
					cboEpo.setValue(objGralMandato.iNoEPO);
				}
			}

			if(resp.msgError!=''){
				var objMsg = Ext.getCmp('mensajesM');
				objMsg.body.update(resp.msgError);
				objMsg.show();
				Ext.getCmp('pnlMandato').hide();
			}else if(objGralMandato.msgFecVencPyme!=''){
				var objMsg = Ext.getCmp('mensajesM');
				objMsg.body.update(objGralMandato.msgFecVencPyme);
				objMsg.show();
				Ext.getCmp('pnlMandato').show();
			}else{
				Ext.getCmp('pnlMandato').show();
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var procesarSuccessObtieneTiposDeFactoraje = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = 	Ext.util.JSON.decode(response.responseText);
			
			if (jsonObj.factorajeVencido) {	//Maneja Factoraje Vencido
				tp.add(tabSelDocVencidoItem);
			}
			if (jsonObj.factorajeMandato) {	//Maneja Factoraje Mandato
				tp.add(tabSelDocMandatoItem);
			}
			tp.doLayout();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	
	var iniRequestNormal = function(){
		Ext.Ajax.request({
			url: NE.appWebContextRoot + '/13descuento/13pyme/13forma1ext.data.jsp',
			params: {
				informacion: 'valoresIniciales'
				},
			callback: procesarSuccessValoresIniNormal
		});
	}

	var iniRequestVencido = function(){
		Ext.Ajax.request({
			url: NE.appWebContextRoot + '/13descuento/13pyme/13forma2ext.data.jsp',
			params: {
				informacion: 'valoresIniciales'
				},
			callback: procesarSuccessValoresIniVencido
		});
	}

	var iniRequestMandato = function(){
		Ext.Ajax.request({
			url: NE.appWebContextRoot + '/13descuento/13pyme/13forma9ext.data.jsp',
			params: {
				informacion: 'valoresIniciales'
				},
			callback: procesarSuccessValoresIniMandato
		});
	}
	
	var obtenerTiposDeFactoraje = function(){
		Ext.Ajax.request({
			url: 'index.data.jsp',
			params: {
				informacion: 'ObtieneTiposDeFactoraje'
			},
			callback: procesarSuccessObtieneTiposDeFactoraje
		});
	}

	//STORES---------------------------------------------------------------------
	var storeCatEpoNormalData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : NE.appWebContextRoot + '/13descuento/13pyme/13forma1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, oprion){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						if(objGralNormal.inicial=='S'){
							iniRequestNormal();
						}
					}
			}
		}
	});
	
	var storeCatMonedaNormalData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : NE.appWebContextRoot + '/13descuento/13pyme/13forma1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store, records, oprion){
				if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
					var cboMoneda = Ext.getCmp('cboMonedaN');
					if(cboMoneda.getValue()==''){
						cboMoneda.setValue(records[0].data['clave']);
					}
				}
			}
		}
	});


	var storeCatEpoVencidoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : NE.appWebContextRoot + '/13descuento/13pyme/13forma2ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, oprion){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						if(objGralVencido.inicial=='S'){
							iniRequestVencido();
						}
					}
			}
		}
	});
	
	var storeCatMonedaVencidoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : NE.appWebContextRoot + '/13descuento/13pyme/13forma2ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store, records, oprion){
				if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
					var cboMoneda = Ext.getCmp('cboMonedaV');
					if(cboMoneda.getValue()==''){
						cboMoneda.setValue(records[0].data['clave']);
					}
				}
			}
		}
	});

	var storeCatEpoMandatoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : NE.appWebContextRoot + '/13descuento/13pyme/13forma9ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, oprion){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						if(objGralMandato.inicial=='S'){
							iniRequestMandato();
						}
					}
			}
		}
	});
	
	var storeCatMonedaMandatoData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : NE.appWebContextRoot + '/13descuento/13pyme/13forma9ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store, records, oprion){
				if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
					var cboMoneda = Ext.getCmp('cboMonedaM');
					if(cboMoneda.getValue()==''){
						cboMoneda.setValue(records[0].data['clave']);
					}
				}
			}
		}
	});
	
	var storeCatMandanteData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : NE.appWebContextRoot + '/13descuento/13pyme/13forma9ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMandante'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	//COMPONENTES---------------------------------------------------------
	var elementosFormaNormal = [
		{
			xtype: 'combo',
			name: 'cboEpo',
			id: 'cboEpoN',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEpo',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEpoNormalData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'cboMoneda',
			id: 'cboMonedaN',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMoneda',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMonedaNormalData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			id: 'cfFechaVenc',
			fieldLabel: 'Fecha Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txtFechaVencDe',
					id: 'txtFechaVencDeN',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaVencaN',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtFechaVenca',
					id: 'txtFechaVencaN',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaVencDeN',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	];
	
	var elementosFormaVencido = [
		{
			xtype: 'combo',
			name: 'cboEpo',
			id: 'cboEpoV',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEpo',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEpoVencidoData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'cboMoneda',
			id: 'cboMonedaV',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMoneda',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMonedaVencidoData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txtFechaVencDe',
					id: 'txtFechaVencDeV',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaVencaV',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtFechaVenca',
					id: 'txtFechaVencaV',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaVencDeV',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	];
	
	
	var elementosFormaMandato = [
		{
			xtype: 'combo',
			name: 'cboEpo',
			id: 'cboEpoM',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEpo',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEpoMandatoData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'cboMandante',
			id: 'cboMandanteM',
			fieldLabel: 'Mandante',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMandante',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMandanteData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'cboMoneda',
			id: 'cboMonedaM',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMoneda',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMonedaMandatoData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txtFechaVencDe',
					id: 'txtFechaVencDeM',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaVencaM',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtFechaVenca',
					id: 'txtFechaVencaM',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaVencDeM',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	];
	//CONTENEDORES--------------------------------------------------------
	
	var fpNormal = new Ext.form.FormPanel({
		id: 'pnlNormal',
		width: 600,
		title: 'Selecci�n Documentos',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		hidden: true,
		style: 'margin: 0 auto',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaNormal,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					fpNormal.el.mask('Consultando...', 'x-mask-loading');
					var formaHTML = fpNormal.getForm().getEl().dom;
					formaHTML.action = NE.appWebContextRoot + '/13descuento/13pyme/13forma1ext.jsp?idMenu=13PYME13FORMA1';
					formaHTML.method = "POST";
					formaHTML.submit();
				}
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					fpNormal.getForm().reset();
				}
				
			}
		]
	});

	var fpVencido = {
		xtype: 'form',
		id: 'pnlVencido',
		width: 600,
		title: 'Selecci�n Documentos Descuento a Vencimiento',
		frame: true,
		collapsible: false,
		hidden: true,
		style: 'margin:0 auto;',
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaVencido,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var pnlVencidoCmp = Ext.getCmp('pnlVencido');
					pnlVencidoCmp.el.mask('Consultando...', 'x-mask-loading');
					var formaHTML = pnlVencidoCmp.getForm().getEl().dom;
					formaHTML.action = NE.appWebContextRoot + '/13descuento/13pyme/13forma2ext.jsp?idMenu=13PYME13FORMA2';
					formaHTML.method = "POST";
					formaHTML.submit();
				}
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					var pnlVencidoCmp = Ext.getCmp('pnlVencido');
					pnlVencidoCmp.getForm().reset();
				}
			}
		]
	};

	var fpMandato = {
		xtype: 'form',
		id: 'pnlMandato',
		width: 600,
		title: 'Selecci�n Documentos con Mandato',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		hidden: true,
		style: 'margin:0 auto;',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaMandato,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var pnlMandatoCmp = Ext.getCmp('pnlMandato');
					pnlMandatoCmp.el.mask('Consultando...', 'x-mask-loading');
					var formaHTML = pnlMandatoCmp.getForm().getEl().dom;
					formaHTML.action = NE.appWebContextRoot + '/13descuento/13pyme/13forma9ext.jsp?idMenu=13PYME13FORMAMAN';
					formaHTML.method = "POST";
					formaHTML.submit();
				}
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					var pnlMandatoCmp = Ext.getCmp('pnlMandato');
					pnlMandatoCmp.getForm().reset();
				}
			}
		]
	};

	var tabSelDocNormalItem = {
		title: 'Selecci�n Doctos.',
		id: 'tabSelDocNormal',
		items: [
			{
				xtype: 'panel',
				id: 'mensajesN',
				width: 600,
				frame: true,
				hidden: true,
				style: 'margin: 0 auto'
			},
			NE.util.getEspaciador(20),
			fpNormal,
			NE.util.getEspaciador(50)
		]
	};
	
	var tabSelDocVencidoItem =	{
		title: 'Selecc. Doctos. Venc.',
		id: 'tabSelDocVencido',
		items: [
			{
				xtype: 'panel',
				id: 'mensajesV',
				width: 600,
				frame: true,
				hidden: true,
				style: 'margin: 0 auto'
			},
			NE.util.getEspaciador(20),
			fpVencido,
			NE.util.getEspaciador(50)
		]
	};

	var tabSelDocMandatoItem = {
		title: 'Selecci�n Doctos. Mandato',
		id: 'tabSelDocMandato',
		items: [
			{
				xtype: 'panel',
				id: 'mensajesM',
				width: 600,
				frame: true,
				hidden: true,
				style: 'margin: 0 auto'
			},
			NE.util.getEspaciador(20),
			fpMandato,
			NE.util.getEspaciador(50)
		]
	};


	var tp = new Ext.TabPanel({
		activeTab: 0,
		width:700,
		//height:250,
		plain:false,
		defaults:{autoHeight: true},
		items: tabSelDocNormalItem,
		listeners: {
			tabchange: function(tab, panel){
				if (panel.getItemId() == 'tabSelDocVencido') {
					if (storeCatEpoVencidoData.getCount() == 0) {
						storeCatEpoVencidoData.load();
						storeCatMonedaVencidoData.load();
					}
				} else if (panel.getItemId() == 'tabSelDocMandato') {
					if (storeCatEpoMandatoData.getCount() == 0) {
						storeCatEpoMandatoData.load();
						storeCatMandanteData.load();
						storeCatMonedaMandatoData.load();
					}
				}
			}
		}
	});
	

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			tp
		]
	});
	
	var panel = new Ext.Panel({
		applyTo:'areaLeyenda',
		width: 945,
		frame: false,
		html: '<table border=0><tr><td><p style="text-align:justify; background:#F3F4F5; font-size:80%">La informaci�n debidamente obtenida y autorizada por el administrador de la plataforma de NAFINET (Nacional Financiera S.N.C., I.B.D) '+
			'tendr� valor probatorio en juicio, en tanto que la obtenci�n de informaci�n almacenada en la base de datos y archivos de la misma, ' + 
			' sin contar con la autorizaci�n correspondiente, o el uso indebido de dicha informaci�n, ser� sancionada en t�rminos de Ley de Instituciones ' +
			' de Cr�dito y dem�s ordenamientos legales aplicables. </p></td></tr></table>'
	});

//-------------------------------- ----------------- -----------------------------------
	storeCatEpoNormalData.load();
	storeCatMonedaNormalData.load();
	obtenerTiposDeFactoraje();
	
});