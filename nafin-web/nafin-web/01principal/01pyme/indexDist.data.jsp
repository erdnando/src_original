<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.distribuidores.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";


ISeleccionDocumento seleccionDocumentoEJB = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

JSONObject jsonObj = new JSONObject();
if (informacion.equals("ObtieneTiposDeFactoraje")) {
	jsonObj = new JSONObject();
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("factorajeVencido", new Boolean(seleccionDocumentoEJB.manejaFactoraje(iNoEPO,"V")));
	jsonObj.put("factorajeMandato", new Boolean(seleccionDocumentoEJB.manejaFactoraje(iNoEPO,"M")));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("valoresIniciales")) {

	ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class); 
	String tipo_credito =  BeanParametros.obtieneTipoCredito (iNoEPO); //Fodea 029-2010 Distribuodores Fase III

	jsonObj = new JSONObject();
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("tipo_credito", tipo_credito);
	jsonObj.put("strPerfil", strPerfil); 
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>