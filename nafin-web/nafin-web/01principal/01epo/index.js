function estableceMensajes(grafica) {
	grafica.configure( {
			"ChartNoDataText" : "No hay informaci�n para graficar",
			"InvalidXMLText"  : "Informaci�n incorrecta",
			"PBarLoadingText" : "Cargando Gr�fica. Por favor espere",
			"XMLLoadingText"  : "Obteniendo informaci�n. Por favor espere",
			"ParsingDataText" : "Procesando informaci�n. Por favor espere",
			"RenderingChartText" : "Generando Gr�fica. Por favor espere",
			"LoadDataErrorText" : "Error al carga la informaci�n"
	});
}


var myChart1 = new FusionCharts( NE.appWebContextRoot+"/00utils/charts/MSColumn2D.swf","grafica1","350","300","0","1" );
var myChart2 = new FusionCharts( NE.appWebContextRoot+"/00utils/charts/MSColumn2D.swf","myChartId2","350","300","0","1" );
var myChart3 = new FusionCharts( NE.appWebContextRoot+"/00utils/charts/MSColumn2D.swf","myChartId3","350","300","0","1" );
var myChart4 = new FusionCharts( NE.appWebContextRoot+"/00utils/charts/Pie2D.swf","myChartId4","350","300","0","1", "#EEEEEE" );
var myChart5 = new FusionCharts( NE.appWebContextRoot+"/00utils/charts/MSColumn2D.swf","myChartId5","700","300","0","1" );

Ext.onReady(function() {

var ObjGral = {
		valMoneda : 'MN'
	};

taskGraficas = {
	run: function(){
		
		requestCarga(ObjGral.valMoneda);
	},
	interval: 10000 //10 second
}

var procesarSuccesFailureGeneraXML = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			
			if(!resp.cargaTerminada){
				Ext.get('areaContenido').mask('Cargando informaci�n...','x-mask');
				var runner = new Ext.util.TaskRunner();
				runner.start(taskGraficas);
				
			}else{
				var btn = Ext.getCmp('botonPrueba1');
				if(ObjGral.valMoneda=='MN')
					btn.setText('MONEDA NACIONAL / <b>DOLARES</b>');
				else if(ObjGral.valMoneda=='DL')
					btn.setText('<b>MONEDA NACIONAL</b> / DOLARES');
					
				Ext.getCmp('contenedorPrincipal').show();
				
				estableceMensajes(myChart1);
				myChart1.setXMLData(resp.xmlProv);
				myChart1.render("chartContainer1");
				
				estableceMensajes(myChart2);
				myChart2.setXMLData(resp.xmlDoc);
				myChart2.render("chartContainer2");
				
				estableceMensajes(myChart3);
				myChart3.setXMLData(resp.xmlFact);
				myChart3.render("chartContainer3");
				
				estableceMensajes(myChart4);
				myChart4.setXMLData(resp.xmlDist);
				myChart4.render("chartContainer4");
				
				estableceMensajes(myChart5);
				myChart5.setXMLData(resp.xmlVenc);
				myChart5.render("chartContainer5");
				Ext.get('areaContenido').unmask();
			}

		} else {
			Ext.get('areaContenido').unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}


var requestCarga = function(moneda){
	
	if(moneda == 'DL'){
		ObjGral.valMoneda = 'MN';
		
	}else if(moneda == 'MN'){
		ObjGral.valMoneda = 'DL';
	}
	
	//Ext.getCmp('contenedorPrincipal').hide();
	Ext.Ajax.request({
		url: 'index.data.jsp',
		params: {
			informacion: 'generaGraficas',
			tipoMoneda: moneda
		},
		callback: procesarSuccesFailureGeneraXML
	});  
}

requestCarga(ObjGral.valMoneda);




//------------------------------------------------------------------------------
//-----------------------------HANDLERS-----------------------------------------
//STORES------------------------------------------------------------------------
//COMPONENTES-------------------------------------------------------------------
//CONTENEDORES------------------------------------------------------------------
	
var pnlGraficas = {
			xtype:'panel',
			title: '',
			id: 'pnlgraficas',
			html :'<table>'+
						'<tr>'+
							'<td><div id="chartContainer1" ></div></td>'+
							'<td><div id="chartContainer2"></div></td>'+
						'</tr>'+
						'<tr>'+
							'<td><div id="chartContainer3"></div></td>'+
							'<td><div id="chartContainer4"></div></td>'+
						'</tr>'+
						'<tr>'+
							'<td colspan="2"><div id="chartContainer5"></div></td>'+
						'</tr>'+
				'</table>'
		};

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
var pnl = new Ext.Container({
						id: 'contenedorPrincipal',
						layout:'form',
						applyTo: 'areaContenido',
						//width: 700,
						hidden: true,
						items: [
							{
								xtype:'button',
								name:'botonPrueba',
								id:'botonPrueba1',
								width: 700,
								text:'<b>MONEDA NACIONAL</b> / DOLARES',
								handler: function(btn){
									requestCarga(ObjGral.valMoneda);
								}
							},
							pnlGraficas
						]
					});
					
					var panel = new Ext.Panel({
						applyTo:'areaLeyenda',
						width: 945,
						frame: false,
						html: '<table border=0><tr><td><p style="text-align:justify; background:#F3F4F5; font-size:80%">La informaci�n debidamente obtenida y autorizada por el administrador de la plataforma de NAFINET (Nacional Financiera S.N.C., I.B.D) '+
							'tendr� valor probatorio en juicio, en tanto que la obtenci�n de informaci�n almacenada en la base de datos y archivos de la misma, ' + 
							' sin contar con la autorizaci�n correspondiente, o el uso indebido de dicha informaci�n, ser� sancionada en t�rminos de Ley de Instituciones ' +
							' de Cr�dito y dem�s ordenamientos legales aplicables. </p></td></tr></table>'
					})
//-------------------------------- ----------------- -----------------------------------
	
});