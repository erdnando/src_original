<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.cadenas.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

ISeleccionDocumento seleccionDocumentoEJB = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	

if (informacion.equals("ObtieneTiposDeFactoraje")) {
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("factorajeVencido", new Boolean(seleccionDocumentoEJB.manejaFactoraje(iNoEPO,"V")));
	jsonObj.put("factorajeMandato", new Boolean(seleccionDocumentoEJB.manejaFactoraje(iNoEPO,"M")));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("generaGraficas")) {
	JSONObject jsonObj = new JSONObject();
	GraficasEpo graficasEpo = new GraficasEpo();
	boolean bestatusProc = false;
	boolean cargaTerminada = false;
	String xmlProv = "";
	String xmlDoc = "";
	String xmlFact = "";
	String xmlDist = "";
	String xmlVenc = "";
	
	String tipoMoneda = (request.getParameter("tipoMoneda")!=null)?request.getParameter("tipoMoneda"):"";

	
	GeneraInfoGraficasThread zipThread = (GeneraInfoGraficasThread)session.getAttribute("generaInfoGraficasThread");
	System.out.println("zipThread.isRunning()=="+zipThread.isRunning());
	System.out.println("zipThread.hasError()=="+zipThread.hasError());
	System.out.println("zipThread.getEstatusProc()==="+zipThread.getEstatusProc());
	if (zipThread.isRunning()) {
		jsonObj.put("flagZipThread", "Cargando Informacion");
		bestatusProc = false;
	}else
	if(zipThread.hasError()){
		jsonObj.put("flagZipThread", "Error en la carga de informacion");
		bestatusProc = false;
		cargaTerminada = true;
	}else
	if(zipThread.getEstatusProc()==null && zipThread.isRunning()){
		jsonObj.put("flagZipThread", "Cargando Informacion");
		bestatusProc = false;
	}else
	if(!zipThread.isRunning() && zipThread.getEstatusProc()!=null){
		jsonObj.put("flagZipThread", "Proceso Terminado");
		bestatusProc = true;
		cargaTerminada = true;
	}	
	System.out.println("bestatusProc=====--------------> "+bestatusProc);
	
	if(bestatusProc){
		List lstGraficaProvMn = graficasEpo.getInfoGraficaEpo(iNoCliente, "PROVEEDORMN");
		
		System.out.println("lstGraficaProvMn.size()===="+lstGraficaProvMn.size());
		if(lstGraficaProvMn!=null & lstGraficaProvMn.size()>0){
			String concuentaxpagarm = "0";
			String concuentaxpagarac = "0";
			String concuentaxpagara = "0";
			String registradom = "0";
			String registradoac = "0";
			String registradoa = "0";
			
			for(int i=0; i<lstGraficaProvMn.size(); i++){
				HashMap mp = (HashMap)lstGraficaProvMn.get(i);
				concuentaxpagarm = mp.get("CONCUENTAXPAGARM")==null?concuentaxpagarm:(String)mp.get("CONCUENTAXPAGARM");
				concuentaxpagarac = mp.get("CONCUENTAXPAGARAC")==null?concuentaxpagarac:(String)mp.get("CONCUENTAXPAGARAC");
				concuentaxpagara = mp.get("CONCUENTAXPAGARA")==null?concuentaxpagara:(String)mp.get("CONCUENTAXPAGARA");
				registradom = mp.get("REGISTRADOM")==null?registradom:(String)mp.get("REGISTRADOM");
				registradoac = mp.get("REGISTRADOAC")==null?registradoac:(String)mp.get("REGISTRADOAC");
				registradoa = mp.get("REGISTRADOA")==null?registradoa:(String)mp.get("REGISTRADOA");
			}
		
		
			xmlProv = "<chart caption='Proveedores' formatNumberScale='0' showValues='0' "+
					"xAxisName='Tipo' " +
					"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
					"useRoundEdges='1' exportEnabled='1' exportAtClient='0' exportShowMenuItem='1' logoURL='/nafin/00archivos/15cadenas/15archcadenas/logos/nafin.gif' "+
					"exportHandler='/nafin/FCExporter' exportAction='download' smartLineColor='FFFFFF' "+
					"logoPosition='TL' logoAlpha='30' showExportDataMenuItem='1' "+
					"canvasBgAlpha='100' canvasBgColor='cfe0ef' "+
					"yAxisName='Cantidad'>" +
					"<categories>" +
					"<category label='Mensual' />" +
					"<category label='Anual' />" +
					"<category label='Acumulado' />" +
					"</categories>" +
					"<dataset seriesName='Cuentas por Pagar Proveedores Registrados'>" +
					"<set value='"+concuentaxpagarm+"' />" +
					"<set value='"+concuentaxpagara+"' />" +
					"<set value='"+concuentaxpagarac+"' />" +
					"</dataset>" +
					"<dataset seriesName='Registrados'>" +
					"<set value='"+registradom+"' />" +
					"<set value='"+registradoa+"' />" +
					"<set value='"+registradoac+"' />" +
					"</dataset>" +
					"</chart>";
		}
		
		List lstGraficaDocMn = graficasEpo.getInfoGraficaEpo(iNoCliente, "DOCREGMN");
		
		if(lstGraficaDocMn!=null & lstGraficaDocMn.size()>0){
			String montosuscfacta = "0";
			String montototala = "0";
			String montosuscfactac = "0";
			String montototalac = "0";
			String montosuscfactm = "0";
			String montototalm = "0";
			
			for(int i=0; i<lstGraficaDocMn.size(); i++){
				HashMap mp = (HashMap)lstGraficaDocMn.get(i);
				montosuscfactm = mp.get("MONTOSUSCFACTM")==null?montosuscfactm:(String)mp.get("MONTOSUSCFACTM");
				montosuscfacta = mp.get("MONTOSUSCFACTA")==null?montosuscfacta:(String)mp.get("MONTOSUSCFACTA");
				montosuscfactac = mp.get("MONTOSUSCFACTAC")==null?montosuscfactac:(String)mp.get("MONTOSUSCFACTAC");
				montototalm = mp.get("MONTOTOTALM")==null?montototalm:(String)mp.get("MONTOTOTALM");
				montototala = mp.get("MONTOTOTALA")==null?montototala:(String)mp.get("MONTOTOTALA");
				montototalac = mp.get("MONTOTOTALAC")==null?montototalac:(String)mp.get("MONTOTOTALAC");
			}
			
			xmlDoc = "<chart caption='Documentos Registrados' formatNumberScale='0' showValues='0' "+
				"xAxisName='Tipo' " +
				"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
				"useRoundEdges='1' exportEnabled='1' exportAtClient='0' exportShowMenuItem='1' logoURL='/nafin/00archivos/15cadenas/15archcadenas/logos/nafin.gif' "+
				"exportHandler='/nafin/FCExporter' exportAction='download' smartLineColor='FFFFFF' "+
				"logoPosition='TL' logoAlpha='30' showExportDataMenuItem='1' "+
				"canvasBgAlpha='100' canvasBgColor='cfe0ef' "+
				"yAxisName='Monto'>" +
				"<categories>" +
				"<category label='Mensual' />" +
				"<category label='Anual' />" +
				"</categories>" +
				"<dataset seriesName='Monto Susceptible de Factoraje'>" +
				"<set value='"+montosuscfactm+"' />" +
				"<set value='"+montosuscfacta+"' />" +
				"</dataset>" +
				"<dataset seriesName='Monto Total'>" +
				"<set value='"+montototalm+"' />" +
				"<set value='"+montototala+"' />" +
				"</dataset>" +
				"</chart>";
			
		}
		
		List lstGraficaFactMn = graficasEpo.getInfoGraficaEpo(iNoCliente, "FACTORAJEMN");
		
		if(lstGraficaFactMn!=null & lstGraficaFactMn.size()>0){
			String operadom = "0";
			String operadoa = "0";
			String operadoac = "0";
			String publicadom = "0";
			String publicadoa = "0";
			String publicadoac = "0";
			
			for(int i=0; i<lstGraficaFactMn.size(); i++){
				HashMap mp = (HashMap)lstGraficaFactMn.get(i);
				operadom = mp.get("OPERADOM")==null?operadom:(String)mp.get("OPERADOM");
				operadoa = mp.get("OPERADOA")==null?operadoa:(String)mp.get("OPERADOA");
				operadoac = mp.get("OPERADOAC")==null?operadoac:(String)mp.get("OPERADOAC");
				publicadom = mp.get("PUBLICADOM")==null?publicadom:(String)mp.get("PUBLICADOM");
				publicadoa = mp.get("PUBLICADOA")==null?publicadoa:(String)mp.get("PUBLICADOA");
				publicadoac = mp.get("PUBLICADOAC")==null?publicadoac:(String)mp.get("PUBLICADOAC");
			}
			
			xmlFact = "<chart caption='Factoraje' formatNumberScale='0' showValues='0' "+
				"xAxisName='Tipo' " +
				"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
				"useRoundEdges='1' exportEnabled='1' exportAtClient='0' exportShowMenuItem='1' logoURL='/nafin/00archivos/15cadenas/15archcadenas/logos/nafin.gif' "+
				"exportHandler='/nafin/FCExporter' exportAction='download' smartLineColor='FFFFFF' "+
				"logoPosition='TL' logoAlpha='30' showExportDataMenuItem='1' "+
				"canvasBgAlpha='100' canvasBgColor='cfe0ef' "+
				"yAxisName='Monto'>" +
				"<categories>" +
				"<category label='Mensual' />" +
				"<category label='Anual' />" +
				"<category label='Acumulable' />" +
				"</categories>" +
				"<dataset seriesName='Operado'>" +
				"<set value='"+operadom+"' />" +
				"<set value='"+operadoa+"' />" +
				"<set value='"+operadoac+"' />" +
				"</dataset>" +
				"<dataset seriesName='Publicado'>" +
				"<set value='"+publicadom+"' />" +
				"<set value='"+publicadoa+"' />" +
				"<set value='"+publicadoac+"' />" +
				"</dataset>" +
				"</chart>";
		}
		
		List lstGraficaDistLineastMn = graficasEpo.getInfoGraficaEpo(iNoCliente, "DISTLINEASAUT"+tipoMoneda);
		
		if(lstGraficaDistLineastMn!=null & lstGraficaDistLineastMn.size()>0){
			String setLabels = "";
			List lstColor = new ArrayList();
			lstColor.add("B45F04");
			lstColor.add("DF7401");
			lstColor.add("FF8000");
			lstColor.add("FE9A2E");
			lstColor.add("FAAC58");
			lstColor.add("DBA901");
			lstColor.add("FFBF00");
			lstColor.add("FACC2E");
			lstColor.add("F7D358");
	
			
			int iColor = 0;
			for(int i=0; i<lstGraficaDistLineastMn.size(); i++){
				HashMap mp = (HashMap)lstGraficaDistLineastMn.get(i);
				
				Iterator it = mp.entrySet().iterator();
				while (it.hasNext()) {
						Map.Entry e = (Map.Entry)it.next();
						System.out.println(e.getKey() + " " + e.getValue());
						setLabels +=" <set label='"+e.getKey()+"' value='"+e.getValue()+"' color='"+(String)lstColor.get(iColor)+"' isSliced='1'/> ";
				}
				iColor++;
				if(iColor==9)iColor=0;
	
			}
			
			xmlDist = "<chart caption='Distribuición de Líneas autorizadas por IF' subcaption='' "+
				"xAxisName='Estatus' showlegend='0' " +
				"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
				"exportEnabled='1' exportAtClient='0' exportShowMenuItem='1' logoURL='/nafin/00archivos/15cadenas/15archcadenas/logos/nafin.gif' "+
				"exportHandler='/nafin/FCExporter' exportAction='download' smartLineColor='FFFFFF' "+
				"logoPosition='TL' logoAlpha='30' showExportDataMenuItem='1' "+
				"yAxisName='Cantidad' showpercentvalues='1'>"+
				setLabels+
				"<styles> " +
				"	<definition> " +
				"		<style name='LabelFont' type='FONT' color='2E4A89' bgColor='FFFFFF' bold='1' /> " +
				"	</definition> " +
				"	<application> " +
				"		<apply toObject='DATALABELS' styles='LabelFont' /> " +
				"	</application> " +
				"</styles> " +
				"</chart>";
		}
		
		List lstGraficaVencMesMn = graficasEpo.getInfoGraficaEpo(iNoCliente, "VENCDELMESMN");
		
		if(lstGraficaVencMesMn!=null & lstGraficaVencMesMn.size()>0){
			String setCategory = "";
			String setValues = "";
			
			int iColor = 0;
			for(int i=0; i<lstGraficaVencMesMn.size(); i++){
				HashMap mp = (HashMap)lstGraficaVencMesMn.get(i);
				//iColor++;
				Iterator it = mp.entrySet().iterator();
				while (it.hasNext()) {
						Map.Entry e = (Map.Entry)it.next();
						System.out.println(e.getKey() + " " + e.getValue());
						setCategory +=" <category label='"+e.getKey()+"' /> ";
						setValues +=" <set value='"+e.getValue()+"' /> ";
				}
				//if(iColor==9)iColor=0;
	
			}
			
			xmlVenc = "<chart formatNumberScale='0' caption='Vencimientos del Mes' subcaption='' showValues='0' "+
				"xAxisName='Fecha' " +
				"bgcolor='b7d3ec,ffffff' bgalpha='100' " +
				"useRoundEdges='1' exportEnabled='1' exportAtClient='0' exportShowMenuItem='0' logoURL='/nafin/00archivos/15cadenas/15archcadenas/logos/nafin.gif' "+
				"exportHandler='xxxx.jsp' exportAction='download' "+
				"logoPosition='TL' logoAlpha='30' "+
				"canvasBgAlpha='100' canvasBgColor='cfe0ef' "+
				"yAxisName='Saldo'>" +
				"<categories>" +
				setCategory+
				"</categories>" +
				"<dataset seriesName='Monto vencido' color='dd6d0e' >" +
				setValues+
				"</dataset>" +
				"</chart>";
		}
	}
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("cargaTerminada", new Boolean(cargaTerminada));
	jsonObj.put("xmlProv", xmlProv);
	jsonObj.put("xmlDoc", xmlDoc);
	jsonObj.put("xmlFact", xmlFact);
	jsonObj.put("xmlDist", xmlDist);
	jsonObj.put("xmlVenc", xmlVenc);
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>