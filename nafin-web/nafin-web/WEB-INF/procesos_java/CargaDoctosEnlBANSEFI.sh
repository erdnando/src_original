#!/usr/bin/ksh
numCampos=`echo $0 | awk -F/ '{print NF-1 }'`
DIR_PROCESOS_JAVA=`echo $0 | cut -d/ -f 1-$numCampos`


cd $DIR_PROCESOS_JAVA

. ../_DIRAPP.sh
. ../var_procesos_java.sh

CLASSES=$DIRIAS/j2ee/home/oc4j.jar:$DIRIAS/j2ee/home/lib/ejb.jar:$DIRCONTENEDOR/applib:$DIRCONTENEDOR/applib/utilerias.jar
EJBJAR=$DIRAPP/CargaDocumentoEJB.jar:$DIRAPP/NafinetExceptionEJB.jar

CLASSPATH=$CLASSPATH:$CLASSES:$EJBJAR:.
export CLASSPATH

ARCHIVO_LOG=../log/descuento/BANSEFIEnlCliente.log
echo
echo "LOG: $ARCHIVO_LOG"
echo

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
echo
echo "****** INICIO PROCESO $FECHA $HORA ******" | tee -a $ARCHIVO_LOG

$DIRJAVA/java mx.gob.nafin.procesosexternos.BANSEFIEnlCliente 730  >> $ARCHIVO_LOG 2>>$ARCHIVO_LOG; ESTADO_PROCESO=$?

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
if [[ $ESTADO_PROCESO = 0 ]]; then
	echo
	echo "PROCESO: BANSEFIEnlCliente  ... TERMINADO SATISFACTORIAMENTE  ($ESTADO_PROCESO)" | tee -a $ARCHIVO_LOG
	echo
	echo "****** FIN PROCESO $FECHA $HORA ******" | tee -a $ARCHIVO_LOG
else
	echo
	echo "PROCESO: BANSEFIEnlCliente ... TERMINADO CON ERROR" | tee -a $ARCHIVO_LOG
	echo
	echo "Codigo de salida : $ESTADO_PROCESO" >> $ARCHIVO_LOG
	echo
	echo "****** FIN PROCESO $FECHA $HORA CON ERROR ******" | tee -a $ARCHIVO_LOG
fi
echo
echo "Codigo de salida : $ESTADO_PROCESO"
echo
exit $ESTADO_PROCESO


