#!/usr/bin/ksh
numCampos=`echo $0 | awk -F/ '{print NF-1 }'`
DIR_PROCESOS_JAVA=`echo $0 | cut -d/ -f 1-$numCampos`


cd $DIR_PROCESOS_JAVA

. ../_DIRAPP.sh
. ../var_procesos_java.sh

# Nota (25/08/2015 04:21:55 p.m., By jshernandez):
#
# * Se agrega al class path librer�a con nuevo cliente Argus WS (v2.5): OIDWebService.jar
#   y sus dependencias:
#   
#      applib/commons-discovery-0.2.jar
#      applib/jaxrpc.jar
#      applib/saaj.jar
#      applib/wsdl4j-1.5.1.jar
#      applib/axis.jar
#      applib/ServicesOIDProxy.jar
#   
# * En caso de que el archivo "$DIRAPP/WEB-INF/lib/ArgusWebService.jar" exista 
#   asumir� que se trata de un ambiente de desarrollo y usar� la versi�n anterior
#   del Argus WS: ArgusWebService.jar 
#
if [[ -f "$DIRAPP/WEB-INF/lib/ArgusWebService.jar" ]] ; then 
	# Si "$DIRAPP/WEB-INF/lib/ArgusWebService.jar" existe, usar version anterior de ARGUS
	export CLASSPATH=$CLASSPATH:$DIRAPP/WEB-INF/lib/ArgusWebService.jar
else
	# Dependencias de la Versi�n 2.5 de Argus
	export CLASSPATH=$CLASSPATH:$DIRAPP/WEB-INF/lib/commons-discovery-0.2.jar:$DIRAPP/WEB-INF/lib/jaxrpc.jar:$DIRAPP/WEB-INF/lib/saaj.jar:$DIRAPP/WEB-INF/lib/wsdl4j-1.5.1.jar:$DIRAPP/WEB-INF/lib/axis.jar:$DIRAPP/WEB-INF/lib/OIDWebServiceProxy.jar:$DIRAPP/WEB-INF/lib/ServicesOIDProxy.jar:$DIRWLS/wlserver/server/lib/com.bea.core.utils.full_2.3.0.0.jar
fi

CLASSPATH=$CLASSPATH:$CLASSES:$EJBJAR:.
export CLASSPATH

ARCHIVO_LOG=../log/garantias/SincGarantias.log
echo
echo "LOG: $ARCHIVO_LOG"
echo

PROCESO=$$;
echo "PID:$PROCESO -" `date`  >> $ARCHIVO_LOG


FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
echo
echo "****** INICIO PROCESO $FECHA $HORA ******" | tee -a $ARCHIVO_LOG

#time nohup $DIRJAVA/java SincGarantias $PROCESO >>$ARCHIVO_LOG 2>>$ARCHIVO_LOG;
#ESTADO=$?

INTENTO=0
ESTADO=0
NUM_INTENTOS=6
while [[ $INTENTO -lt $NUM_INTENTOS ]]; do
 
   INTENTO=$((INTENTO+1))

	# EJECUTAR PROCESO
	(time nohup $DIRJAVA/java mx.gob.nafin.procesosexternos.SincGarantias $PROCESO 2>&1; echo $? > ../log/garantias/SincGarantias.STATUS ) | tee -a $ARCHIVO_LOG;
	ESTADO=$(cat ../log/garantias/SincGarantias.STATUS)
	rm ../log/garantias/SincGarantias.STATUS
	
	FECHA=`date +%d/%m/%y`
	HORA=`date +%H:%M:%S`
	
   # SI EL PROCESO SE EJECUTO SATISFACTORIAMENTE NO ES NECESARIO VOLVER A EJECUTAR EL PROCESO
   if [[ $ESTADO -eq 0 ]]; then
     break
   else
      if [[ $INTENTO -ne 6 ]]; then
         echo "---------------------------------------------------------------" | tee -a $ARCHIVO_LOG
         echo "Sincronizacion Garantias NE-SIAG terminado prematuramente con ERROR $FECHA $HORA (PID:$PROCESO)" | tee -a $ARCHIVO_LOG
         echo "REINTENTANDO AUTOMATICAMENTE... ... ... ... ... .. ... ... ...." | tee -a $ARCHIVO_LOG
         echo "---------------------------------------------------------------" | tee -a $ARCHIVO_LOG
      fi
   fi

done

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
if [[ $ESTADO = 0 ]]; then
	echo
	echo "SINCRONIZACION GARANTIAS NE-SIAG TERMINADO SATISFACTORIAMENTE $FECHA $HORA (PID:$PROCESO)" | tee -a $ARCHIVO_LOG
	echo
else
	echo
	echo "SINCRONIZACION GARANTIAS NE-SIAG TERMINADO CON ERROR $FECHA $HORA (PID:$PROCESO)" | tee -a $ARCHIVO_LOG
fi
echo
echo "Codigo de salida : $ESTADO"
echo
exit $ESTADO


