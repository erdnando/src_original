#!/usr/bin/ksh
numCampos=`echo $0 | awk -F/ '{print NF-1 }'`
DIR_PROCESOS_JAVA=`echo $0 | cut -d/ -f 1-$numCampos`


cd $DIR_PROCESOS_JAVA

. ../_DIRAPP.sh
. ../var_procesos_java.sh

ARCHIVO_LOG=../log/descuento/enlacePemex.log
echo
echo "LOG: $ARCHIVO_LOG"
echo


FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
echo
echo "****** INICIO PROCESO $FECHA $HORA ******" | tee -a $ARCHIVO_LOG

if [[ $# = 0 ]]; then
	echo "Ejem. Debe ejecutarlo --> CargaDoctosEnlPemex.sh <IC_EPO>. Donde IC_EPO es la clave de la EPO."
fi
$DIRJAVA/java mx.gob.nafin.procesosexternos.PemexEnlCliente $1 >> $ARCHIVO_LOG 2>> $ARCHIVO_LOG; ESTADO1=$?

ESTADO_GLOBAL=$ESTADO1


FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
if [[ $ESTADO_GLOBAL = 0 ]]; then
	echo
	echo "PROCESO TERMINADO SATISFACTORIAMENTE $FECHA $HORA" | tee -a $ARCHIVO_LOG
	echo
else
	echo
	echo "PROCESO TERMINADO CON ERROR $FECHA $HORA" | tee -a $ARCHIVO_LOG
fi
echo
echo "Codigo de salida : $ESTADO_GLOBAL"
echo
exit $ESTADO_GLOBAL

