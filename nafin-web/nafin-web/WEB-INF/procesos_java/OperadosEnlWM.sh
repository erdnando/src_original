#!/usr/bin/ksh
numCampos=`echo $0 | awk -F/ '{print NF-1 }'`
DIR_PROCESOS_JAVA=`echo $0 | cut -d/ -f 1-$numCampos`


cd $DIR_PROCESOS_JAVA

. ../_DIRAPP.sh
. ../var_procesos_java.sh

ARCHIVO_LOG=../log/descuento/Walmart.log
echo
echo "LOG: $ARCHIVO_LOG"
echo

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`

echo "****** INICIO PROCESO WALMART 7 $FECHA $HORA ******" | tee -a $ARCHIVO_LOG

$DIRJAVA/java mx.gob.nafin.procesosexternos.WMEnlOperCliente 7 >> $ARCHIVO_LOG 2>> $ARCHIVO_LOG; ESTADO_PROCESO=$?

if [[ $ESTADO_PROCESO = 0 ]]; then
echo
echo "PROCESO: Walmart ... TERMINADO SATISFACTORIAMENTE  ($ESTADO_PROCESO)" | tee -a $ARCHIVO_LOG
echo
echo "****** FIN PROCESO $FECHA $HORA ******" | tee -a $ARCHIVO_LOG
exit
else
echo
echo "PROCESO: Walmart ... TERMINADO CON ERROR" | tee -a $ARCHIVO_LOG
echo
echo "Codigo de salida proceso Walmart : $ESTADO_PROCESO" | tee -a $ARCHIVO_LOG
echo
echo "****** FIN PROCESO $FECHA $HORA CON ERROR ******" | tee -a $ARCHIVO_LOG
echo
exit 1
fi

