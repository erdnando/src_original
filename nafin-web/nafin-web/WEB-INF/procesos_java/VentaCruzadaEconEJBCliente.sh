#!/usr/bin/ksh
numCampos=`echo $0 | awk -F/ '{print NF-1 }'`
DIR_PROCESOS_JAVA=`echo $0 | cut -d/ -f 1-$numCampos`


cd $DIR_PROCESOS_JAVA

. ../_DIRAPP.sh
. ../var_procesos_java.sh

ARCHIVO_LOG=../log/cadenas/carga_y_cruce_DB.log
echo
echo "LOG: $ARCHIVO_LOG"
echo

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
echo
echo "****** INICIO PROCESO RETORNO VENTA CRUZADA ECONTRACT $FECHA $HORA ******" | tee -a $ARCHIVO_LOG

$DIRJAVA/java mx.gob.nafin.procesosexternos.VentaCruzadaEconEJBCliente $DIRAPP >> $ARCHIVO_LOG 2>> $ARCHIVO_LOG; ESTADO=$?

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
if [[ $ESTADO = 0 ]]; then
	echo
	echo "PROCESO RETORNO VENTA CRUZADA ECONTRACT TERMINADO SATISFACTORIAMENTE $FECHA $HORA" | tee -a $ARCHIVO_LOG
	echo
else
	echo
	echo "PROCESO RETORNO VENTA CRUZADA ECONTRACT TERMINADO CON ERROR $FECHA $HORA" | tee -a $ARCHIVO_LOG
fi
echo
echo "Codigo de salida : $ESTADO"
echo
exit $ESTADO
