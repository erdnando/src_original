#!/usr/bin/ksh
numCampos=`echo $0 | awk -F/ '{print NF-1 }'`
DIR_PROCESOS_JAVA=`echo $0 | cut -d/ -f 1-$numCampos`


cd $DIR_PROCESOS_JAVA

. ../_DIRAPP.sh
. ../var_procesos_java.sh

ARCHIVO_LOG=../log/descuento/DescuentoAutomatico.log
echo
echo "LOG: $ARCHIVO_LOG"
echo

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`

ICEPO="-"
ICPYME="-"
ICIF="-"
if [[ $2 = "" ]];then
  ICEPO="-"
else
  ICEPO=$2
fi
if [[ $3 = "" ]];then
  ICPYME="-"
else
  ICPYME=$3
fi
if [[ $4 = "" ]];then
  ICIF="-"
else
  ICIF=$4
fi

echo EPO $ICEPO PYME $ICPYME IF $ICIF

echo
echo "****** INICIO PROCESO $FECHA $HORA - $0  $* ******" | tee -a $ARCHIVO_LOG

$DIRJAVA/java mx.gob.nafin.procesosexternos.DescuentoAutomaticoEJBCliente 0 $ICEPO $ICPYME $ICIF $DIRAPP $1>> $ARCHIVO_LOG 2>> $ARCHIVO_LOG; ESTADO=$?

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
if [[ $ESTADO = 0 ]]; then
	echo
	echo "PROCESO TERMINADO SATISFACTORIAMENTE $FECHA $HORA - $0 $* " | tee -a $ARCHIVO_LOG
	echo
else
	echo
	echo "PROCESO TERMINADO CON ERROR $FECHA $HORA - $0 $* " | tee -a $ARCHIVO_LOG
fi
echo
echo "Codigo de salida : $ESTADO"
echo
exit $ESTADO

