#!/usr/bin/ksh
numCampos=`echo $0 | awk -F/ '{print NF-1 }'`
DIR_PROCESOS_JAVA=`echo $0 | cut -d/ -f 1-$numCampos`


cd $DIR_PROCESOS_JAVA

. ../_DIRAPP.sh
. ../var_procesos_java.sh

ARCHIVO_LOG=../log/Solicitud_Autorizada.log
echo
echo "LOG: $ARCHIVO_LOG"
echo

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
echo
echo "****** INICIO PROCESO $FECHA $HORA ******" | tee -a $ARCHIVO_LOG

$DIRJAVA/java mx.gob.nafin.procesosexternos.ProcesoSolicitudAutorizacionOPE $DIRAPP >> $ARCHIVO_LOG 2>> $ARCHIVO_LOG; ESTADO_PROCESO=$?

FECHA=`date +%d/%m/%y`
HORA=`date +%H:%M:%S`
if [[ $ESTADO_PROCESO = 0 ]]; then
        echo
        echo "PROCESO: SolicitudAutorizadaOPE.sh ...  ESTADO_PROCESO ($ESTADO_PROCESO)" | tee -a $ARCHIVO_LOG
        echo
        echo "****** FIN PROCESO $FECHA $HORA ******" | tee -a $ARCHIVO_LOG
else
        echo
        echo "PROCESO: SolicitudAutorizadaOPE.sh ... TERMINADO CON ERROR" | tee -a $ARCHIVO_LOG
        echo
        echo "Codigo de salida : $ESTADO_PROCESO"  | tee -a $ARCHIVO_LOG
        echo
        echo "****** FIN PROCESO $FECHA $HORA CON ERROR ******"  | tee -a $ARCHIVO_LOG
fi
echo
echo "Codigo de salida : $ESTADO_PROCESO"
echo
exit $ESTADO_PROCESO
