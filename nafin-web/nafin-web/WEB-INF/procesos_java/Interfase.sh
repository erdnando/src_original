#!/usr/bin/ksh
numCampos=`echo $0 | awk -F/ '{print NF-1 }'`
DIR_PROCESOS_JAVA=`echo $0 | cut -d/ -f 1-$numCampos`


cd $DIR_PROCESOS_JAVA


. ../_DIRAPP.sh
. ../var_procesos_java.sh

accion=${1:-"restart"}

if [ -f ../log/interfaces/Interfaces.pid ]
then
	cat ../log/interfaces/Interfaces.pid |xargs kill -9
else
	touch ../log/interfaces/Interfaces.pid
fi

if [ $accion = "stop" ]
then
	cat /dev/null > ../log/interfaces/Interfaces.pid 
	exit 0
fi

date >> ../log/interfaces/inicio.log
echo Estatus de interfaces antes de su inicio>> ../log/interfaces/inicio.log

for pid in `cat ../log/interfaces/Interfaces.pid`;do echo "-p"$pid;done | xargs ps -f >> ../log/interfaces/inicio.log
fecha=`/bin/date +%d%m%Y`

cat ../log/interfaces/diversos.log >> ../log/interfaces/diversos$fecha.log
cat ../log/interfaces/diversos.err >> ../log/interfaces/diversos$fecha.err
date > ../log/interfaces/diversos.log
date > ../log/interfaces/diversos.err
nohup $DIRJAVA/java mx.gob.nafin.procesosexternos.Interfaz DIVERSOS > ../log/interfaces/diversos.log 2>../log/interfaces/diversos.err &
echo $! > ../log/interfaces/Interfaces.pid

cat ../log/interfaces/diversosE.log >> ../log/interfaces/diversosE$fecha.log
cat ../log/interfaces/diversosE.err >> ../log/interfaces/diversosE$fecha.err
date > ../log/interfaces/diversosE.log
date > ../log/interfaces/diversosE.err
nohup $DIRJAVA/java -Xmx128m -Xss2048k mx.gob.nafin.procesosexternos.Interfaz DIVERSOSE > ../log/interfaces/diversosE.log 2>../log/interfaces/diversosE.err &
echo $! >> ../log/interfaces/Interfaces.pid
cat ../log/interfaces/proyectos.log >> ../log/interfaces/proyectos$fecha.log
cat ../log/interfaces/proyectos.err >> ../log/interfaces/proyectos$fecha.err
date > ../log/interfaces/proyectos.log
date > ../log/interfaces/proyectos.err
nohup $DIRJAVA/java mx.gob.nafin.procesosexternos.Interfaz PROYECTOS > ../log/interfaces/proyectos.log 2>../log/interfaces/proyectos.err &
echo $! >> ../log/interfaces/Interfaces.pid

cat ../log/interfaces/proyectosE.log >> ../log/interfaces/proyectosE$fecha.log
cat ../log/interfaces/proyectosE.err >> ../log/interfaces/proyectosE$fecha.err
date > ../log/interfaces/proyectosE.log
date > ../log/interfaces/proyectosE.err
#nohup $DIRJAVA/java -Xmx128m -Xss2048k -Xms32m Interfase PROYECTOSE > ../log/interfaces/proyectosE.log 2>../log/interfaces/proyectosE.err &
nohup $DIRJAVA/java -Xmx128m -Xss2048k mx.gob.nafin.procesosexternos.Interfaz PROYECTOSE > ../log/interfaces/proyectosE.log 2>../log/interfaces/proyectosE.err &
echo $! >> ../log/interfaces/Interfaces.pid

cat ../log/interfaces/contratos.log >> ../log/interfaces/contratos$fecha.log
cat ../log/interfaces/contratos.err >> ../log/interfaces/contratos$fecha.err
date > ../log/interfaces/contratos.log
date > ../log/interfaces/contratos.err
nohup $DIRJAVA/java mx.gob.nafin.procesosexternos.Interfaz CONTRATOS > ../log/interfaces/contratos.log 2>../log/interfaces/contratos.err &
echo $! >> ../log/interfaces/Interfaces.pid

cat ../log/interfaces/contratosE.log >> ../log/interfaces/contratosE$fecha.log
cat ../log/interfaces/contratosE.err >> ../log/interfaces/contratosE$fecha.err
date > ../log/interfaces/contratosE.log
date > ../log/interfaces/contratosE.err
#nohup $DIRJAVA/java -Xmx128m -Xss2048k -Xms32m Interfase CONTRATOSE > ../log/interfaces/contratosE.log 2>../log/interfaces/contratosE.err &
nohup $DIRJAVA/java -Xmx128m -Xss2048k mx.gob.nafin.procesosexternos.Interfaz CONTRATOSE > ../log/interfaces/contratosE.log 2>../log/interfaces/contratosE.err &
echo $! >> ../log/interfaces/Interfaces.pid

cat ../log/interfaces/prestamos.log >> ../log/interfaces/prestamos$fecha.log
cat ../log/interfaces/prestamos.err >> ../log/interfaces/prestamos$fecha.err
date > ../log/interfaces/prestamos.log
date > ../log/interfaces/prestamos.err
nohup $DIRJAVA/java mx.gob.nafin.procesosexternos.Interfaz PRESTAMOS > ../log/interfaces/prestamos.log 2>../log/interfaces/prestamos.err &
echo $! >> ../log/interfaces/Interfaces.pid

cat ../log/interfaces/prestamosE.log >> ../log/interfaces/prestamosE$fecha.log
cat ../log/interfaces/prestamosE.err >> ../log/interfaces/prestamosE$fecha.err
date > ../log/interfaces/prestamosE.log
date > ../log/interfaces/prestamosE.err
#nohup $DIRJAVA/java -Xmx128m -Xss2048k -Xms32m Interfase PRESTAMOSE > ../log/interfaces/prestamosE.log 2>../log/interfaces/prestamosE.err &
nohup $DIRJAVA/java -Xmx128m -Xss2048k mx.gob.nafin.procesosexternos.Interfaz PRESTAMOSE > ../log/interfaces/prestamosE.log 2>../log/interfaces/prestamosE.err &
echo $! >> ../log/interfaces/Interfaces.pid

cat ../log/interfaces/disposiciones.log >> ../log/interfaces/disposiciones$fecha.log
cat ../log/interfaces/disposiciones.err >> ../log/interfaces/disposiciones$fecha.err
date > ../log/interfaces/disposiciones.log
date > ../log/interfaces/disposiciones.err
nohup $DIRJAVA/java mx.gob.nafin.procesosexternos.Interfaz DISPOSICIONES > ../log/interfaces/disposiciones.log 2>../log/interfaces/disposiciones.err &
echo $! >> ../log/interfaces/Interfaces.pid

cat ../log/interfaces/disposicionesE.log >> ../log/interfaces/disposicionesE$fecha.log
cat ../log/interfaces/disposicionesE.err >> ../log/interfaces/disposicionesE$fecha.err
date > ../log/interfaces/disposicionesE.log
date > ../log/interfaces/disposicionesE.err
#nohup $DIRJAVA/java -Xmx128m -Xss2048k -Xms32m Interfase DISPOSICIONESE > ../log/interfaces/disposicionesE.log 2>../log/interfaces/disposicionesE.err &
nohup $DIRJAVA/java -Xmx128m -Xss2048k mx.gob.nafin.procesosexternos.Interfaz DISPOSICIONESE > ../log/interfaces/disposicionesE.log 2>../log/interfaces/disposicionesE.err &
echo $! >> ../log/interfaces/Interfaces.pid

date >> ../log/interfaces/inicio.log
echo Estatus de interfaces despues de su inicio>> ../log/interfaces/inicio.log
for pid in `cat ../log/interfaces/Interfaces.pid`;do echo "-p"$pid;done | xargs ps -f >> ../log/interfaces/inicio.log
