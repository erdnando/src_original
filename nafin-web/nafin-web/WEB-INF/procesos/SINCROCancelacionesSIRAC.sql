--Cambio de estatus de solicitud de operada a rechazada nafin 
set term on time on timing on
whenever oserror exit 9
whenever sqlerror exit sql.sqlcode

UPDATE com_solic_portal
   SET ic_bloqueo = 5,
       ic_estatus_solic = 4,
       df_cancelacion = SYSDATE
 WHERE ig_numero_prestamo IN (
          SELECT numero_prestamo
            FROM pr_prestamos
           WHERE estado = 9
             AND numero_prestamo IN (
                    SELECT ig_numero_prestamo
                      FROM com_solic_portal
                     WHERE df_operacion >= TRUNC (SYSDATE)
                       AND df_operacion <= TRUNC (SYSDATE) + 1
                       AND ic_estatus_solic = 3));
					   
COMMIT;

DELETE      com_interfase
      WHERE ic_solic_portal IN (
               SELECT numero_prestamo
                 FROM pr_prestamos
                WHERE estado = 9
                  AND numero_prestamo IN (
                         SELECT ig_numero_prestamo
                           FROM com_solic_portal
                          WHERE df_operacion >= TRUNC (SYSDATE)
                            AND df_operacion <= TRUNC (SYSDATE) + 1
                            AND ic_estatus_solic = 4
                            AND ic_bloqueo = 5));					  					   

COMMIT;
EXIT;



 
						