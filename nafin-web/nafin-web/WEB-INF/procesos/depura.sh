#!/usr/bin/ksh
numCampos=`echo $0 | awk -F/ '{print NF-1 }'`
DIR_PROCESOS=`echo $0 | cut -d/ -f 1-$numCampos`

cd $DIR_PROCESOS
echo $DIR_PROCESOS

. ./_DIRAPP.sh

echo $DIRAPP

fecha=`/bin/date "+%d%m%Y_%H%M%S"`
echo Ini $fecha

find $PROC_EXT/procesos/nafin/procesosExternos/ \( -name "*.log" \)  -exec zip $PROC_EXT/procesos/nafin/procesosExternos/logs$fecha.zip {} \;
for item in $( find $PROC_EXT/procesos/nafin/procesosExternos/ -name "*.log") ;do cat /dev/null > $item ;done
find $DIRAPP/00tmp/ -name "*.*" -exec rm -f {} \;
find $PROC_EXT/procesos/nafin/procesosExternos/ \( -name "*.log" -o -name "*.err" -o -name "*.txt" -o -name "*.zip" -o -name "*.csv" \)  -mtime +15 -exec rm -f {} \;


fecha=`/bin/date "+%d%m%Y_%H%M%S"`
echo Fin $fecha