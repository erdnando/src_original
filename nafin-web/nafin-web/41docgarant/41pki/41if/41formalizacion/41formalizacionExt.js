
Ext.onReady(function() {

    var ESTATUS_POR_SOLICITAR = 100;
    var ESTATUS_EN_PROCESO    = 110;
    var ESTATUS_EN_VALIDACION = 120;
    var VALIDACION_JURIDICO   = 130;
    var FORMALIZACION_IF      = 140;
    var SIN_PARAMETRIZAR = "Sin parametrizar";
    var TERMINOS_CONDICIONES = 1001;
    var REGLAMENTO_OPERATIVO_SELECTIVA   = 1002;
    var REGLAMENTO_OPERATIVO_AUTOMATICA   = 1003;


    /****************************** FIRMAR DOCUMENTO ****************************/
        var storeCifrasData = new Ext.data.ArrayStore({
              fields: [
                  {name: 'etiqueta'},
                  {name: 'informacion'}
              ]
	 });

        var gridCifrasControl = new Ext.grid.GridPanel({
            id: 'gridCifrasControl',
            store: storeCifrasData,
            margins: '20 0 0 0',
            style: 'margin:0 auto;',
            hideHeaders : true,
            hidden: true,
            align: 'center',
            columns: [
                {
                    header : 'Etiqueta',
                    dataIndex : 'etiqueta',
                    width : 200,
                    sortable : false
                },
                {
                    header : 'Informacion',
                    dataIndex : 'informacion',
                    width : 350,
                    sortable : false,
                    renderer:  function (causa, columna, registro){
                        columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
                        return causa;
                    }
                }
            ],
            stripeRows: true,
            columnLines : true,
            loadMask: true,
            width: 561,
            style: 'margin:0 auto;',
            autoHeight : true,
            //title: '',
            frame: true,
            scroll  : false
	});

	var fnProcConfirmarCallback = function(vpkcs7, vtextoFirmar, vrecord){

		if (Ext.isEmpty(vpkcs7)) {
			Ext.MessageBox.show({
			   title: 'Firma Electr�nica',
			   msg: '�Error al realizar la firma electr�nica con Pkcs7!',
			   buttons: Ext.MessageBox.OK,
			   fn: showResult,
			   icon: Ext.MessageBox.ERROR
		   });
			return;
		}

		pnl.mask('Firmando Electr�nicamente...', 'x-mask-loading');
		Ext.Ajax.request({
			url : '41formalizacionExt.data.jsp',
			params : {
				pkcs7: vpkcs7,
				textoFirmado: vtextoFirmar,
				icDocumento: vrecord.get('icDocumento'),
				icIF:   vrecord.get('icIF'),
				icArchivo: vrecord.get('icArchivo'),
				informacion: 'ACUSE'
			},
			callback: procesarSuccessFailureGuardar
		});
	}

	function procesarConfirmar(record) {
            var folio = record.get('folioSolicitud');
            var nombre = record.get('nombreProducto');
            var nombreIF = record.get('nombreIF');
            var textoFirmar = nombre+ " Folio: "+ folio+ " para "+ nombreIF;

            NE.util.obtenerPKCS7(fnProcConfirmarCallback, textoFirmar, record);

	}

        var procesarSuccessFailureGuardar =  function(opts, success, response) {
                var jsondeAcuse = Ext.JSON.decode(response.responseText);
		if (success == true && jsondeAcuse.success == true) {
			if (jsondeAcuse != null){
				var acuseCifras = [
                                    ['N�mero de Acuse', jsondeAcuse.acuse],
                                    ['Folio del Documento ', jsondeAcuse.folioDocumento],
                                    ['Fecha ', jsondeAcuse.fecha],
                                    ['Hora ', jsondeAcuse.hora],
                                    ['Usuario ', jsondeAcuse.usuario]
				];
                                gridStore.reload();
				storeCifrasData.loadData(acuseCifras);
				gridCifrasControl.show();
                                pnl.unmask();
			}
		} else {
                        pnl.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}




    function descargaArchivos(opts, success, response) {
        var jsonData  = Ext.JSON.decode(response.responseText);
        if (success == true && jsonData.success == true ) {
            var archivo = jsonData.urlArchivo;
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};
            var forma  	= Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method 		= 'post';
            forma.target 		= '_self';
            forma.submit();
        }
        else {
            NE.util.mostrarConnError(response,opts);
        }
        Ext.getCmp("winFirmasDocumento").unmask();
    }


    Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }
		]
	});






    function fnRechazarSolicitud(record){
        Ext.MessageBox.show({
           title: 'Rechazar Documento',
           msg: '�Est� seguro de rechazar el documento con folio de solicitud: <strong>'+record.get('folioSolicitud')+'</strong>?\nCapture el motivo del rechazo:',
           width:400,
           buttons: Ext.MessageBox.YESNOCANCEL,
           icon: Ext.Msg.QUESTION,
           multiline: true,
           fn: function(btn, texto){
                if(btn == 'yes'){
                    var textoLimpio = texto.trim();
                    if (textoLimpio.length < 1){
                        Ext.MessageBox.show({
                           title: 'Rechazar Solicitud',
                           msg: '�Debe capturar la razon del rechazo!\nIntentelo nuevamente',
                           buttons: Ext.MessageBox.OK,
                           icon: Ext.MessageBox.ERROR
                       });
                    }
                    else{
                        Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                        Ext.Ajax.request({
                        url: '41formalizacionExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'rejectSol',
                            toRefuse:   texto,
                            icSolicitud: record.get('icSolicitud')
                        }),
                        callback:  function(opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true ) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert('Rechazar Solicitud', info.msg);
                            Ext.getCmp('contenedorPrincipal').unmask();
                            }
                        })
                    }
                }
           }
       });
    }

	 Ext.define('HomeSolicitudesDataModel', {
		extend: 'Ext.data.Model',
		fields: [
                   {name: 'icSolicitud', 	    mapping : 'icSolicitud' 	        },
                   {name: 'icIF', 	    mapping : 'icIF' 		},
		   {name: 'icArchivo',	    mapping : 'icArchivo'	},
                   {name: 'icEstatus',	            mapping : 'icEstatus'        	},
		   {name: 'fechaLiberacion', 	    mapping : 'fechaLiberacion'  	},
		   {name: 'nombre',          	    mapping : 'nombre' 	        },
                   {name: 'nombreIF',         mapping : 'nombreIF'          },
		   {name: 'folioSolicitud' ,                 mapping : 'folioSolicitud' 	                },
		   {name: 'nombreIntermediario',    mapping : 'nombreIntermediario'   },
		   {name: 'tipoDocumento', 	    mapping : 'tipoDocumento'	},
                   {name: 'nombreEjecutivoAtencion', 	    mapping : 'nombreEjecutivoAtencion' 	},
		   {name: 'nombreEstatus', 	    mapping : 'nombreEstatus' 		},
                   {name: 'version', 	    mapping : 'version' 		},
                   {name: 'nombreProducto', 	    mapping : 'nombreProducto' 		},
                   {name: 'firmasRequeridas', 	    mapping : 'firmasRequeridas' 		},
                   {name: 'icDocumento', 	    mapping : 'icDocumento' 		},
                   {name: 'firmasRegistradas', 	    mapping : 'firmasRegistradas' 	},
                   {name: 'nombrePortafolio', 	    mapping : 'nombrePortafolio' 	},
                   {name: 'fechaMaximaFormalizacion', 	    mapping : 'fechaMaximaFormalizacion' 	},
                   {name: 'icTipoDocumento', 	    mapping : 'icTipoDocumento' },
                   {name: 'cumplePrelacionIF', 	    mapping : 'cumplePrelacionIF' },
                   {name: 'icUsuariosFirmantes', 	    mapping : 'icUsuariosFirmantes' },
                   {name: 'cumpleAFU', 	    mapping : 'cumpleAFU' }
		]
	 });


    var procesarConsulta = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if(store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso','No existe informaci�n con los criterios determinados');
            }
        }
        gridResultados.unmask();
    }


	var gridStore = Ext.create('Ext.data.Store', {
	   model: 'HomeSolicitudesDataModel',
            proxy:              {
                type:            'ajax',
                url:             '41formalizacionExt.data.jsp',
                reader:          {
                    type:         'json',
                    root:         'registros'
                },
                extraParams:     {
                    informacion:  'formalizacion'
                }
            },
            autoLoad: true,
            listeners: {
                exception: NE.util.mostrarDataProxyError,
                load: procesarConsulta
            }
        });


    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    });

//********************************** GRID PRINCIPAL ************************************//

        var gridResultados = Ext.create('Ext.grid.Panel', {
           id                : 'gridId',
           store             : gridStore,
           stripeRows        : true,
           title             : 'Documentos a validar por el Intermediario Financiero',
           width             : 944,
           height            : 400,
           collapsible       : false,
           plugins: [cellEditing],
           enableColumnMove  : true,
           enableColumnResize:true,
           autoScroll: true,
		   columns           :
		   [{
			  header: "Fecha de<br/>Recepci�n",
			  dataIndex: 'fechaLiberacion',
			  width:80,
			  sortable: true,
			  hideable: true
		   },
			{
			  header: "Tipo de<br/>Documento",
			  dataIndex: 'tipoDocumento',
			  align: 'center',
			  width: 150,
			  sortable: true,
			  hideable: true
		   }
                    ,{
                      header: "Portafolio",
                      dataIndex: 'nombrePortafolio',
                      align: 'center',
                      width: 150,
                      sortable: true,
                      hideable: true,
                      renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                        var tipoDocumento = record.get("icTipoDocumento");
                        if(tipoDocumento == TERMINOS_CONDICIONES || tipoDocumento == REGLAMENTO_OPERATIVO_SELECTIVA || tipoDocumento == REGLAMENTO_OPERATIVO_AUTOMATICA ){
                            return "---";
                        }
                        else{
                         return value;
                        }
                      }
		   },
                    {
                    header: "Producto/Empresa",
                    align: 'center',
                    dataIndex: 'nombreProducto',
                    hideable: true,
                    sortable: true,
                    width:120
                }
                ,{
                  header: "Versi�n del<br/>documento",
                  dataIndex: 'version',
                  width: 80,
                  align: 'center',
                  sortable: true,
                  hideable: true,
                  renderer: function(value) {
                     return "V"+value;
                  }
            },
            {
                header: "Documentos a<br/> formalizar",
                menuText: "Documentos a<br/> formalizar",
                xtype:'actioncolumn',
                align: 'center',
                hideable: true,
                sortable: true,
                width:90,
                items: [{
                    iconCls: 'page_paintbrush',
                    tooltip: 'Ver Documento',
                    handler: function(grid, rowIndex, colIndex) {
                        var record = gridResultados.getStore().getAt(rowIndex);
                        mostrarVentanaDocuFirmantes(record.get("icDocumento"), record.get("icArchivo"));
                    }
                }]
            }
            ,{
              header: "Fecha M�xima de<br/>Formalizaci�n",
              dataIndex: 'fechaMaximaFormalizacion',
              align: 'center',
              width: 100,
              sortable: true,
              hideable: true
             }
            ,{
              header: "Firmantes IF<br/>requeridos",
              dataIndex: 'firmasRequeridas',
              align: 'center',
              width: 100,
              sortable: true,
              hideable: true,
              renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                var registrados = record.get('firmasRegistradas');
                 return registrados+" de "+value;
              }
             }
            ,{
              header: "Acciones",
              menuText: "Acciones",
              xtype:'actioncolumn',
              dataIndex: 'icDocumento',
              align: 'center',
              width: 80,
              sortable: true,
              hideable: true,
                items: [{
                    iconCls: 'correcto',
                    tooltip: 'Validar Documento',
                    border: '10 10 10 10',
                    handler: function (grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        procesarConfirmar(record);
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        var cumplePrelacionIF = record.get('cumplePrelacionIF')+'' == 'true';
                        var cumpleAFU = record.get("cumpleAFU")+'' == 'true';
                        return record.get('icEstatus') != FORMALIZACION_IF || !cumplePrelacionIF || !cumpleAFU;
                    }
                }, {
                    iconCls: 'icoRechazar marginL15p',
                    tooltip: 'Rechazar Documento',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        fnRechazarSolicitud(rec);
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        var cumplePrelacionIF = record.get('cumplePrelacionIF')+'' == 'true';
                        var cumpleAFU = record.get("cumpleAFU")+'' == 'true';
                        return record.get('icEstatus') != FORMALIZACION_IF || !cumplePrelacionIF || !cumpleAFU;
                    }
                }]
             }
            ,{
              header: "Estatus",
              dataIndex: 'nombreEstatus',
              align: 'center',
              width: 130,
              sortable: true,
              hideable: true
             }
            ,{
              header: "Base de Operaci�n",
              dataIndex: 'baseOperacion',
              align: 'center',
              width: 130,
              sortable: true,
              hideable: true
             }
       ]
});


//----------************* CATALOGOS  *************----------//
	var  storeEstatus = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
                autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41formalizacionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catEstatus'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});

    	var  storeCatDocumentos = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
               autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41formalizacionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catDocumentos'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});


        var  storeCatPortafolios = Ext.create('Ext.data.Store', {
            model: 'ModelCatalogos',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: '41formalizacionExt.data.jsp',
                reader: {
                        type: 'json',
                        root: 'registros'
                },
                extraParams: {
                        informacion: 'catPortafolios'
                },
                listeners: {
                        exception: NE.util.mostrarProxyAjaxError
                }
            }
	});

          var  storeCatProductos = Ext.create('Ext.data.Store', {
            model: 'ModelCatalogos',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: '41formalizacionExt.data.jsp',
                reader: {
                        type: 'json',
                        root: 'registros'
                },
                extraParams: {
                        informacion: 'catProductos'
                },
                listeners: {
                        exception: NE.util.mostrarProxyAjaxError
                }
            }
	});


var error1Fechas = "Debes capturar la fecha inicial y final";
var error2Fechas = "La fecha final debe ser posterior a la inicial";

var validarFechas =  function (val) {
    var fechaUno = Ext.getCmp('fechaSolicitud1');
    var fechaDos = Ext.getCmp('fechaSolicitud2');
    var fechaUnoVal = fechaUno.getValue();
    var fechaDosVal = fechaDos.getValue();
    if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)){
        fechaUno.markInvalid(error1Fechas);
        fechaDos.markInvalid(error1Fechas);
        return error1Fechas;
    }
    if(fechaUno != null && fechaUno > fechaDos){
        fechaUno.markInvalid(error2Fechas);
        fechaDos.markInvalid(error2Fechas);
        fechaDos.focus();
        return error2Fechas;
    }
    return true;
}

/********* Ventana de documento a formalizar y firmantes hasta le momento ******/
    var storeFirmantes = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        /*proxy:              {
            type:            'ajax',
            url:             '41formalizacionExt.data.jsp',
            reader:          {
                type:         'json',
                root:         'registros'
            },
            extraParams:     {
                informacion:  'getFirmantesDoc'
            }
        },*/
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarCargaFirmantes
        }
    });


    function mostrarVentanaDocuFirmantes(icDocumento, icArchivo){
        storeFirmantes.removeAll(true);
        Ext.Ajax.request({
            url: '41formalizacionExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'firmantesDoc',
                icDocumento: icDocumento
            }),
            callback:  function(opts, success, response) {
                var info = Ext.JSON.decode(response.responseText);
                if (success == true && info.success == true ) {
                    var registros = info.registros;
                    var icDocumento = info.icDocumento;
                    if (registros.length==0){
                        storeFirmantes.add({'clave': "Documento sin formalizar" ,'descripcion': ""});
                    }
                    else{
                        for(i=0; i<registros.length; i++){
                            storeFirmantes.add({'clave': registros[i].clave ,'descripcion': registros[i].descripcion});
                        }
                    }
                }
                procesarCargaFirmantes(icArchivo);
                Ext.getCmp('contenedorPrincipal').unmask();
            }
        });
    }
    function  procesarCargaFirmantes(icArchivo){

        var boton = Ext.create('Ext.Button', {
            text: 'Descargar documento a formalizar',
            renderTo: Ext.getBody(),
            iconCls : 'icoPdf',
            handler: function() {
                Ext.getCmp("winFirmasDocumento").mask("Descargando archivo...");
                Ext.Ajax.request({
                    url: '41formalizacionExt.data.jsp',
                    params: Ext.apply(  {
                        informacion: 'getDocFile',
                        icArchivo: icArchivo
                    }),
                    callback: descargaArchivos
                });
            }
        });


        var contenedorElemntos = Ext.create('Ext.Container', {
            //renderTo: Ext.getBody(),
            items: [
                NE.util.getEspaciador(10),
                boton,
                NE.util.getEspaciador(10),
                {   // GRID DE firmantes
                xtype: 'grid',
                border: false,
                //title: 'Firmantes',
                enableColumnMove  : false,
                enableColumnResize: true,
                enableColumnHide  : false,
                scroll  : false,
                columns: [
                    {
                        header: 'Nombre del Firmante',
                        dataIndex: 'clave',
                        width: 400,
                        sortable: false
                    },
                    {
                        header: 'Fecha y hora de firma',
                        dataIndex: 'descripcion',
                        width: 190,
                        sortable: false
                    }
                ],
                store: storeFirmantes
            }
            ]
        });

        Ext.create('Ext.window.Window', {
            title: 'Documento a Formalizar ',
            id: 'winFirmasDocumento',
            height: 200,
            width: 600,
            layout: 'fit',
            modal: true,
            items: [contenedorElemntos]
        }).show();
    }



//----------*************  PANEL BUSQUEDA  *************----------//
var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        width: 943,
        height: 'auto',
        frame: true,
        title: 'Filtros de B�squeda',
        layout: 'column',
        items: [
            {
                xtype: 'fieldset',
                id:'fieldsBusqueda1',
                height: 150,
                border:false,
                columnWidth: 0.5,
                items: [
                        {
                            xtype: 'combo',
                            id: 'cbTipoDocumento',
                            name: 'cbTipoDocumento',
                            fieldLabel: 'Tipo de Documento',
                            padding: 5,
                            width: 400,
                            forceSelection: true,
                            allowBlank: true,
                            emptyText: 'Todos los Solicitantes',
                            store: storeCatDocumentos, displayField: 'descripcion', valueField: 'clave'
                        },
                        {
                            xtype: 'combo',
                            id: 'cbTipoPortafolio',
                            name: 'cbTipoPortafolio',
                            fieldLabel: 'Tipo de Portafolio',
                            padding: 5,
                            width: 400,
                            forceSelection: true,
                            allowBlank: true,
                            emptyText: 'Todos los Solicitantes',
                            store: storeCatPortafolios, displayField: 'descripcion', valueField: 'clave'
                        },
                        {
                            xtype: 'combo',
                            id: 'cbProducto',
                            name: 'cbProducto',
                            fieldLabel: 'Producto',
                            padding: 5,
                            width: 400,
                            forceSelection: true,
                            allowBlank: true,
                            emptyText: 'Todos los Solicitantes',
                            store: storeCatProductos, displayField: 'descripcion', valueField: 'clave'
                        },
                        {
                        xtype:'fieldset',
                        collapsible:false,
                        border: false,
                        defaultType: 'textfield',
                        defaults: {anchor: '100%'},
                        padding: 0,
                        layout: 'hbox',
                        items :[
                                {
                                    xtype: 'datefield',
                                    id : 'fechaSolicitud1',
                                    name: 'fechaSolicitud1',
                                    fieldLabel: 'Fecha de Recepci�n',
                                    padding: 5,
                                    width: 215,
                                    emptyText: 'Desde',
                                    displayField: 'descripcion',
                                    valueField: 'clave',
                                    forceSelection: true,
                                    allowBlank: true
                                },
                                {
                                    xtype: 'datefield',
                                    id : 'fechaSolicitud2',
                                    name: 'fechaSolicitud2',
                                    fieldLabel: 'al',
                                    labelWidth: 55,
                                    padding: 10,
                                    width: 170,
                                    emptyText: 'Hasta',
                                    valueField: 'clave',
                                    forceSelection: true,
                                    allowBlank: true
                                }
                            ]
                        }
                    ]
            },
            {
                xtype: 'fieldset',
                height: 150,
                border:false,
                columnWidth: 0.5,
                margins: '0 20 0 0',
			items: [
                        {
                            xtype: 'combo',
                            id: 'estatus',
                            name: 'estatus',
                            fieldLabel: 'Estatus del Documento:',
                            padding: 5,
                            width: 400,
                            store: storeEstatus,
                            emptyText: 'Todos los Estatus',
                            displayField: 'descripcion',
                            valueField: 'clave',
                            forceSelection: true,
                            allowBlank: true
                        },
                        {
                        xtype:'fieldset',
                        collapsible:false,
                        border: false,
                        defaultType: 'textfield',
                        defaults: {anchor: '100%'},
                        padding: 0,
                        layout: 'hbox',
                        items :[
                                {
                                    xtype: 'datefield',
                                    id : 'fechaMaxFormalizacion1',
                                    name: 'fechaMaxFormalizacion1',
                                    fieldLabel: 'Fecha M�xima de formalizaci�n',
                                    padding: 5,
                                    width: 215,
                                    emptyText: 'Desde',
                                    displayField: 'descripcion',
                                    valueField: 'clave',
                                    forceSelection: true,
                                    allowBlank: true
                                },
                                {
                                    xtype: 'datefield',
                                    id : 'fechaMaxFormalizacion2',
                                    name: 'fechaMaxFormalizacion2',
                                    fieldLabel: 'al',
                                    labelWidth: 55,
                                    padding: 10,
                                    width: 170,
                                    emptyText: 'Hasta',
                                    valueField: 'clave',
                                    forceSelection: true,
                                    allowBlank: true
                                }
                            ]
                        }
                ]
            }
        ],
        buttons: [
        {
            text: 'Buscar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarFechas();
                if(resultado != true){
                    Ext.MessageBox.alert('Aviso',resultado);
                }
                else{
                gridResultados.el.mask('Cargando...', 'x-mask-loading');
                gridStore.load({
                    informacion:  'HomeSolicitudes',
                    params: Ext.apply(formBusqueda.getForm().getValues())
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                Ext.getCmp('cbTipoDocumento').setValue('');
                Ext.getCmp('cbTipoPortafolio').setValue('');
                Ext.getCmp('cbProducto').setValue('');
                Ext.getCmp('fechaSolicitud1').setValue('');
                Ext.getCmp('fechaSolicitud2').setValue('');
                Ext.getCmp('estatus').setValue('');
                Ext.getCmp('fechaMaxFormalizacion1').setValue('');
                Ext.getCmp('fechaMaxFormalizacion2').setValue('');
            }
        }]
    });

    function descargaArchivo(opts, success, response) {
        var jsonData  = Ext.JSON.decode(response.responseText);
        if (success == true && jsonData.success == true ) {
            var archivo = jsonData.urlArchivo;
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};
            var forma  	= Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method 		= 'post';
            forma.target 		= '_self';
            forma.submit();
        }
        else {
            NE.util.mostrarConnError(response,opts);
        }
        pnl.unmask();
    }

    var  botonContratoPDF = Ext.create('Ext.Button', {
        text: 'Ver Convenio Aut�grafo',
        renderTo: Ext.getBody(),
        iconCls : 'icoPdf',
            handler: function() {
                pnl.mask("Descargando contrato aut�grafo...");
                Ext.Ajax.request({
                    url: '41formalizacionExt.data.jsp',
                    params: Ext.apply(  {
                        informacion: 'getDocFile',
                        icArchivo: icContratoAutografo
                    }),
                    callback: descargaArchivo
                });
            }
    });



//----------*************  CONTENEDOR PRINCIPAL  *************----------//
	var pnl = new Ext.Container({
            id: 'contenedorPrincipal',
            renderTo: 'areaContenido',
            width: 949,
            style: 'margin:0 auto;',
            items: [
                NE.util.getEspaciador(20),
                formBusqueda,
                NE.util.getEspaciador(10),
                botonContratoPDF,
                NE.util.getEspaciador(10),
                gridCifrasControl,
                NE.util.getEspaciador(10),
                gridResultados,
                NE.util.getEspaciador(20)]
	});



});