<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.nafin.docgarantias.formalizacion.*,
        com.nafin.docgarantias.baseoperaciones.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%@ include file="../../certificado.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    private static final Integer ESTATUS_POR_VALIDAR_BO = 1;
    private static final Integer ESTATUS_OPERANDO_BO = 3;
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String icSolicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
String infoRegresar="";

log.info("informacion: "+informacion);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);



if (informacion.equals("formalizacion")){   //<------------------------------------------------------
    Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);    
    
    String fechaSolicitud1 = request.getParameter("fechaSolicitud1") == null ? "" : request.getParameter("fechaSolicitud1");
    String fechaSolicitud2 = request.getParameter("fechaSolicitud2") == null ? "": request.getParameter("fechaSolicitud2");
    String intermediario = request.getParameter("intermediario") == null ? "" : request.getParameter("intermediario");
    String usuarioSolicitante = request.getParameter("usuarioSolicitante") == null ? "" : request.getParameter("usuarioSolicitante");
    String usuarioEjecutivo = request.getParameter("usuarioEjecutivo") == null ? "" : request.getParameter("usuarioEjecutivo");
    String estatus = request.getParameter("estatus") == null ? "" : request.getParameter("estatus");
    
    Integer icIF = Integer.parseInt(iNoCliente);


    Map<String,String> params = new HashMap<>();
    params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF.toString());
    params.put(ConstantesFormalizacion.PARAM_IC_USUARIO, strLogin);
    ArrayList<DocValidacion> listado = formalizacionBean.getDocFormalizacionIF(params);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("getDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }catch(Exception e){
        log.error(e.getMessage());
        e.printStackTrace();
        exito = false;
        jsonObj.put("msg","Ocurrio un Error: "+e.getMessage());
    }
    finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if(informacion.equals("ACUSE")) {		
	Seguridad s = new Seguridad();
	String acusePki	= "";
	char generarAcuse = 'Y'; 
       	String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
        String usuario = null;
	try{
            String cadenaOriginal = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";
            String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";      
            //String strPreacuse	= (request.getParameter("strPreacuse")==null)?"":request.getParameter("strPreacuse").trim(); 
            usuario = 	strLogin+" - "+strNombreUsuario;
            String documento = request.getParameter("icDocumento") ;
            String archivo =  request.getParameter("icArchivo") ;
            String intermediario = request.getParameter("icIF");
            Integer icIF = Integer.parseInt(intermediario);
            Integer icDocumento = Integer.parseInt(documento);
            Integer icArchivo = Integer.parseInt(archivo);
            Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);
            if (!formalizacionBean.usuarioIFPuedeFirmarDocumento(icIF, icDocumento, strLogin)){
                log.info("InvalidUserSign: "+ strLogin + ","+icIF+","+icDocumento);
             throw new NafinException("El documento no se puede formalizar por el usuario");
            }
            if (!formalizacionBean.validaPrelacionDocumentoParaFirmar(icDocumento, icIF)){
                throw new NafinException("El documento no se puede formalizar debido a que no cumple con la prelacion establecida.");
            }
            
            if (!_serial.equals("") && cadenaOriginal!=null && pkcs7!=null) {
                System.out.println("_serial "+_serial);
                System.out.println("pkcs7 "+pkcs7);
                System.out.println("externContent "+cadenaOriginal);
                System.out.println("getReceipt "+generarAcuse);
                // Se AUTENTICA el texto firmado 
                if (s.autenticar(documento, _serial, pkcs7, cadenaOriginal, generarAcuse)){
                    acusePki = s.getAcuse(); // Este es el Folio del acuse del PKI			
                    log.info("ACUSE: "+ acusePki);
                    
                    HashMap<String, String> parametros = new HashMap<>();
                    parametros.put(ConstantesFormalizacion.PARAM_CADENA_FIRMADA, pkcs7);
                    parametros.put(ConstantesFormalizacion.PARAM_FOLIO_PKI, acusePki);
                    parametros.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento.toString());
                    parametros.put(ConstantesFormalizacion.PARAM_IC_USUARIO, strLogin);
                    parametros.put(ConstantesFormalizacion.PARAM_IC_IF, icIF.toString());
                    parametros.put(ConstantesFormalizacion.PARAM_TIPO_FIRMA, ConstantesFormalizacion.FIRMA_IF+"");
                    if (formalizacionBean.guardarAcuse(parametros)){
                        parametros.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo.toString());                        
                        // AVANZAR DOCUMENTO DE ESTATUS 
                        FlujoDocumentos flujoDocumentos = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
                        flujoDocumentos.avanzarDocumentoIF(icDocumento, strLogin, icIF, null);                    
                    }
                    else{
                        log.info("FIRMA DUPLICADA: "+ icDocumento+" "+icIF+" "+ strLogin);
                        throw new NafinException("GDOC-Formalizacion","No se pudo guardar el acuse");
                    }
                }
                else{
                    throw new NafinException("GDOC-Formalizacion","No se pudo autenticar el texto firmado");
                }
            }
	}catch(Exception e){
            e.printStackTrace();
            throw new NafinException("Error inesperado: "+ e.getMessage());
	}           
            
        JSONObject 	resultado	= new JSONObject();
        resultado.put("success", new Boolean(true));
        resultado.put("acuse", acusePki);
        resultado.put("fecha",fechaHoy);	
        resultado.put("hora",horaActual);	
        resultado.put("usuario",usuario.toString());
        resultado.put("folioDocumento",acusePki);
        infoRegresar = resultado.toString();
}
else if(informacion.equals("catEstatus")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatEstatusSolicitud();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catDocumentos")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatDocumentos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";

}else if(informacion.equals("catPortafolios")){
    ArrayList<ElementoCatalogo> listado = solicitudesBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catProductos")){
    Integer icIF = Integer.parseInt(iNoCliente);
    ArrayList<ElementoCatalogo> listado = solicitudesBean.getCatProducto(130, icIF);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("firmantesDoc")){
    Integer icIF = Integer.parseInt(iNoCliente);
    String icDocumento = request.getParameter("icDocumento");
    Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);
    ArrayList<ElementoCatalogo> listado = formalizacionBean.getFirmatesIFDocumento(icIF, Integer.parseInt(icDocumento));
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("rejectSol")){
    boolean exito = false;
    FlujoDocumentos flujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
    
    Integer icSol = Integer.parseInt(icSolicitud);
    String motivoRechazo =  request.getParameter("toRefuse");
    SolicitudDoc sol  = solicitudesBean.getSolicitud(icSol);
    exito = flujoDocumentosBean.rechazarDocumento(sol, iNoUsuario, motivoRechazo);
    JSONObject jsonObj   =  new JSONObject();
    if (exito){
        jsonObj.put("msg","Se ha rechazado la solicitud correctamente.");
    }
    else{
        jsonObj.put("msg","Error al rechazar la solicitud");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("rechazoBO")){
    boolean exito = false;
    BaseOperacion baseOperacionBean = ServiceLocator.getInstance().lookup("BaseOperacion",BaseOperacion.class);
    String documento = request.getParameter("icDocumento") == null ? "0" : request.getParameter("icDocumento");
    Integer icDocumento = Integer.parseInt(documento);
    String motivoRechazo =  request.getParameter("toRefuse");
    Integer icIF = Integer.parseInt(iNoCliente);
    exito = baseOperacionBean.rechazarBO(icDocumento, icIF, iNoUsuario, motivoRechazo);
    JSONObject jsonObj   =  new JSONObject();
    if (exito){
        jsonObj.put("msg","Se ha rechazado la base de operación correctamente.");
    }
    else{
        jsonObj.put("msg","Error al rechazar la base de operación");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("validacionBO")){
    boolean exito = false;
    BaseOperacion baseOperacionBean = ServiceLocator.getInstance().lookup("BaseOperacion",BaseOperacion.class);
    String documento = request.getParameter("icDocumento") == null ? "0" : request.getParameter("icDocumento");
    Integer icDocumento = Integer.parseInt(documento);
    Integer icIF = Integer.parseInt(iNoCliente);
    exito = baseOperacionBean.avanzaEstatusDocto(icDocumento, icIF, iNoUsuario, ESTATUS_OPERANDO_BO);
    JSONObject jsonObj   =  new JSONObject();
    if (exito){
        jsonObj.put("msg","Se ha validado la base de operación correctamente.");
    }
    else{
        jsonObj.put("msg","Error al validar la base de operación");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}


log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>