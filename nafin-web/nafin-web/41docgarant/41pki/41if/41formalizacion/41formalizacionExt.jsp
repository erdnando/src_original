<!DOCTYPE html>
<%@ page import="
		java.util.*,
                org.apache.commons.logging.Log,
                com.nafin.docgarantias.formalizacion.*,
                com.nafin.docgarantias.parametrizacion.usuarios.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%
    
    ParametrizacionUsuarios parametrizacion = ServiceLocator.getInstance().lookup("ParametrizacionUsuariosBean",ParametrizacionUsuarios.class);
    ParametrosIF param = parametrizacion.getParametrosIF(Integer.valueOf(iNoCliente), false);
    Integer icArchivo = param.getIcDocumentoAutografo();
%>
<script type="text/javascript" >
    var icContratoAutografo = <%=icArchivo%>;
</script>
<script type="text/javascript" src="41formalizacionExt.js?&lt;%=session.getId()%>"></script>
<%@ include file="/00utils/componente_firma4.jspf" %>
<%@ include file="../../certificado.jspf" %>


<style>
.cursorPointer{
    cursor: pointer;
}
.marginL15p{
    margin-left: 15px;
}
</style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
        <div id="areaContenido"></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>

</html>