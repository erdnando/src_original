
Ext.onReady(function() {


var ERROR1FECHAS = "Debes capturar la fecha inicial y final";
var ERROR2FECHAS = "La fecha final debe ser posterior a la inicial";

    var ESTATUS_EN_VALIDACION = 120; 
    var ESTATUS_RUBRICA_NAFIN  = 150;
    var ESTATUS_FORMALIZADO   = 170;
    var ESTATUS_FORMALIZADO_CARGA_BO = 175;
    
    var FICHA_IF_G_AUTOMATICA = 1004;
    var FICHA_NAFIN_G_AUTOMATICA = 1005;
    var FICHA_G_SELECTIVA = 1007;
    
    
    const TERMINIOS_CONDICIONES = 1001;

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();		
            }else {
                NE.util.mostrarConnError(response,opts);
        }
        Ext.getCmp('winDoc').unmask();
    }

    


	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }				
		]
	});
    
    function  procesarCargaNombresArchivos(record){
        var folio = record.get("folioSolicitud");
        var winDoc = Ext.create('Ext.window.Window', {
            title: 'Documentos',
            id: 'winDoc',
            height: 220,
            width: 500,
            bodyStyle: 'background:#FFF;',
            modal: true,
            items: [{   // GRID DE DOCUMENTOS SOPORTE 
                xtype: 'grid',
                border: false,
                enableColumnMove  : false,
                enableColumnResize: true,
                enableColumnHide  : false,
                columns: [
                    {   
                        header: 'Documentos Soporte',
                        dataIndex: 'descripcion',
                        width: 348,
                        sortable: false
                    },
                    {
                        xtype:'actioncolumn',
                        header: "Ver Documento",
                        align: 'center',
                        sortable: false, 
                        width:140,
                        items: [{
                            iconCls: 'icoLupa',
                            tooltip: 'Ver Documento',
                            handler: function(grid, rowIndex, colIndex) {
                                winDoc.mask("Descargando archivo...");
                                var recordIn = grid.getStore().getAt(rowIndex); 
                                Ext.Ajax.request({
                                    url: '41vistobuenoExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'getDocFile',
                                        icArchivo: recordIn.get('clave')
                                    }),
                                callback: descargaArchivos
                                });                                          
                            }
                        }]
                    }                        
                ],               
                store: storeArchivosSoporte
            },
            NE.util.getEspaciador(40),	
            {   // GRID DE DOCUMENTOS FORMALIZAR 
                xtype: 'grid',
                border: false,
                enableColumnMove  : false,
                enableColumnResize: true,
                enableColumnHide  : false,
                columns: [
                    {   
                        header: 'Documento a Formalizar',
                        dataIndex: 'descripcion',
                        width: 348,
                        sortable: false
                    },
                    {
                        xtype:'actioncolumn',
                        header: "Ver Documento",
                        align: 'center',
                        sortable: false, 
                        width:140,
                        items: [{
                            iconCls: 'icoLupa',
                            tooltip: 'Ver Documento',
                            handler: function(grid, rowIndex, colIndex) {
                                winDoc.mask("Descargando archivo...");
                                var recordIn = grid.getStore().getAt(rowIndex); 
                                Ext.Ajax.request({
                                    url: '41vistobuenoExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'getDocFile',
                                        icArchivo: recordIn.get('clave')
                                    }),
                                callback: descargaArchivos
                                });                                          
                            }
                        }]
                    }                        
                ],               
                store: storeArchivoFormalizar
            }        
            ]
        }).show();
    }


    function firmantesValidos (esTerminosCondidiones){
        var valCombo1 = Ext.getCmp("wfirmante1").getValue();
        if (!valCombo1) return false;
        var valCombo2 = Ext.getCmp("wfirmante2").getValue();
        
        var arrValor = Ext.getCmp("wfirmante1").getValue().split("|");
        var requiereOtraFirma = !(arrValor[1] == "0");
        if (esTerminosCondidiones && requiereOtraFirma){
            if (!valCombo2) return false;    
            if (valCombo2 == arrValor[0]) return false;
        }
    return true;
    }


    var storeComboEjcutivos = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        proxy:              {
            type:            'ajax',
            url:             '41formalizacionExt.data.jsp',
            reader:          {
                type:         'json',
                root:         'registros'
            },
            extraParams:     {
                informacion:  'catEjecutivo'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }            
    });     
    

    var storeTipoDocumento= Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        proxy:              {
            type:            'ajax',
            url:             '41formalizacionExt.data.jsp',
            reader:          {
                type:         'json',
                root:         'registros'
            },
            extraParams:     {
                informacion:  'catEjecutivo'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }   
        
    });     

    var storeArchivosSoporte = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }            
    });     
    
    var storeArchivoFormalizar = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }            
    });      
    
     
    
    var storeApoderados = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        proxy:              {
            type:            'ajax',
            url:             '41vistobuenoExt.data.jsp',
            reader:          {
                type:         'json',
                root:         'registros'
            },
            extraParams:     {
                informacion:  'getCatApoderados'
            }
        },       
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }            
    });  
    
        var storeApoderadosMancomunados = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',     
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }            
    }); 


    function mostrarVentanaDocumentos(record){
        var icSolicitud = record.get("icDocumento");
        storeArchivosSoporte.removeAll(true);
        storeArchivoFormalizar.removeAll(true);
        Ext.Ajax.request({
            url: '41vistobuenoExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'filesSol',
                icDocumento: icSolicitud
            }),
            callback:  function(opts, success, response) {
                var info = Ext.JSON.decode(response.responseText);
                if (success == true && info.success == true ) {
                    gridStore.reload();
                    var documentosSoporte = info.documentosSoporte;
                    var documentosFormalizacion = info.documentosFormalizar;
                    for(i=0; i<documentosSoporte.length; i++){
                        storeArchivosSoporte.add({'clave': documentosSoporte[i].clave ,'descripcion': documentosSoporte[i].descripcion});
                    }
                    for(i=0; i<documentosFormalizacion.length; i++){
                        storeArchivoFormalizar.add({'clave': documentosFormalizacion[i].clave ,'descripcion': documentosFormalizacion[i].descripcion});
                    }
                }
                procesarCargaNombresArchivos(record);
                Ext.getCmp('contenedorPrincipal').unmask();                            
            }
        });
    }
    


        var procesarSuccessFailureGuardar =  function(opts, success, response) {
                var jsondeAcuse = Ext.JSON.decode(response.responseText);
		if (success == true && jsondeAcuse.success == true) {			
                        gridStore.reload();
                        Ext.getCmp("wfirmantes").close();
                        Ext.MessageBox.alert('Visto Bueno', 'Se ha guardado el Visto Bueno correctamente.');
		} else {
			NE.util.mostrarConnError(response,opts);
		}
                Ext.getCmp('contenedorPrincipal').unmask();
	}


    function mostrarVentanaFirmantes(record){
        var icIF = record.get("icIF");
        var icDocumento = record.get("icDocumento");
        var tipoDoc = record.get("icTipoDocumento");
        var esTerminosCondidiones = tipoDoc == TERMINIOS_CONDICIONES;
    Ext.create('Ext.window.Window', {
            title: 'Firmantes',
            id: 'wfirmantes',
            height: 200,
            width: 500,
            bodyPadding: 5,
            items: [{
                xtype: 'label',
                text: 'Favor de Indicar a los Firmantes',
                margin: '10 10 10 10'
            }, {
                xtype: 'combo',
                name: 'wfirmante1',
                id: 'wfirmante1',
                fieldLabel: 'Firmante titular',
                margin: '15 10 10 10',
                width: 470,
                labelWidth: 130,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Seleccionar Firmante',
                store: storeApoderados, 
                displayField: 'descripcion',
                valueField: 'clave',
                  listeners: {
                    select: function(combo, record, index) {
                      var arrValor = combo.getValue().split("|");
                      var requiereOtraFirma = !(arrValor[1] == "0");
                        if (esTerminosCondidiones &&  requiereOtraFirma){
                            Ext.getCmp("wfirmante2").setVisible(true);
                            if (combo.store.find('clave', arrValor[1]) != -1){
                                Ext.getCmp("wfirmante2").setValue(arrValor[1]);
                            }
                            else{
                                Ext.MessageBox.show({
                                       title: 'Error',
                                       msg: 'Falta la parametrizacion del usuario que firma de forma mancomunada con el usuario seleccionado',
                                       buttons: Ext.MessageBox.OK,
                                       icon: Ext.MessageBox.ERROR
                                   });                        
                            }
                        }
                        else{
                            Ext.getCmp("wfirmante2").setVisible(false);
                        }
                    }     
                }
            }, {
                xtype: 'combo',
                name: 'wfirmante2',
                id: 'wfirmante2',
                fieldLabel: 'Segundo Firmante',
                margin: '15 10 10 10',
                width: 470,
                labelWidth: 130,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Seleccionar Firmante',
                store: storeApoderadosMancomunados, 
                displayField: 'descripcion',
                valueField: 'clave',
                hidden: true
            }, {
                xtype: 'button',
                text: 'Aceptar',
                id: 'wbtnAceptar',
                margin: '15 10 10 10',
                iconCls: 'icoGuardar',
                handler: function (boton, evento) {
                    if (!firmantesValidos(esTerminosCondidiones)) {
                        Ext.MessageBox.alert('Aviso',"Falta seleccionar firmantes");
                    } else {
                        var valorFirmanteUno = Ext.getCmp("wfirmante1").getValue().split("|");
                        var usuarioUno = valorFirmanteUno[0];
                        Ext.getCmp("wfirmantes").mask('Guardando...', 'x-mask-loading');
                        Ext.Ajax.request({
                            url : '41vistobuenoExt.data.jsp',
                            params : {
                                icDocumento: record.get('icDocumento'),
                                icIF:   icIF,
                                informacion: 'guardaVistoBueno',
                                wfirmante1: usuarioUno,
                                wfirmante2: Ext.getCmp("wfirmante2").getValue()
                            },
                            callback: procesarSuccessFailureGuardar
                        });	                        
                    }
                }
            }, {
                xtype: 'button',
                text: 'Cancelar',
                id: 'wbtnCancelar',
                margin: '15 10 10 10',
                iconCls: 'icoCancelar',
                handler: function (boton, evento) {
                        Ext.getCmp('contenedorPrincipal').unmask();
                        Ext.getCmp("wfirmantes").close();
                        
                }
            }
            ,{
                xtype: 'hiddenfield',
                name: 'hidden_icIF',
                id :'hidden_icIF',
                value: icIF
            },{
                xtype: 'hiddenfield',
                id:'hidden_icDoc',
                name: 'hidden_icDoc',
                value: icDocumento
            }          
            ],
            onEsc: function() {
                var me = this;
                Ext.getCmp('contenedorPrincipal').unmask();
                me.close();
            }
        }).show();    
    }
    
    
    function mostrarSeleccionFirmantes(record){
        Ext.getCmp('contenedorPrincipal').mask();
        storeApoderados.removeAll(true);
        storeApoderadosMancomunados.removeAll(true);
        var tipoDoc = record.get("icTipoDocumento");
        Ext.Ajax.request({
            url: '41vistobuenoExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getCatApoderados',
                icTipoDocumento: tipoDoc
            }),
            callback:  function(opts, success, response) {
                var info = Ext.JSON.decode(response.responseText);
                if (success == true && info.success == true ) {
                    var registros = info.registros;
                    for(i=0; i<registros.length; i++){
                        storeApoderados.add({'clave': registros[i].clave ,'descripcion': registros[i].descripcion});
                        var arregloClave = registros[i].clave.split('|');
                        storeApoderadosMancomunados.add({'clave': arregloClave[0] ,'descripcion': registros[i].descripcion});
                    }
                    mostrarVentanaFirmantes(record);
                }                                              
            }
        });
    }
    

    function validarDocumento(record){
        Ext.Msg.show({
            title: "Visto Bueno",
            msg: '�Desea dar Visto Bueno al documento Folio: <strong>'+record.get('folioSolicitud')+'<\/strong> de <strong>'+ record.get("nombreIF")+'<\/strong>?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    mostrarSeleccionFirmantes(record);
                } 
            }
        });  
    }
    
    

    
    function rechazarDocumento(record){
        Ext.MessageBox.show({
           title: 'Rechazar Solicitud',
           msg: '�Est� seguro de rechazar la solicitud con folio: <strong>'+record.get('folio')+'<\/strong>?\nCapture el motivo del rechazo:',
           width:400,
           buttons: Ext.MessageBox.YESNOCANCEL,
           icon: Ext.Msg.QUESTION,
           multiline: true,
           fn: function(btn, texto){
                if(btn == 'yes'){
                    var textoLimpio = texto.trim();
                    if (textoLimpio.length < 1){
                        Ext.MessageBox.show({
                           title: 'Rechazar Solicitud',
                           msg: '�Debe capturar la razon del rechazo!\nIntentelo nuevamente',
                           buttons: Ext.MessageBox.OK,
                           icon: Ext.MessageBox.ERROR
                       });
                    }
                    else{
                        Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                        Ext.Ajax.request({
                        url: '41vistobuenoExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'guardaRechazoVoBo',
                            motivo:   texto,
                            icDocumento: record.get('icDocumento'),
                            icIF    : record.get('icIF')
                        }),
                        callback:  function(opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true ) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert('Rechazar Solicitud', info.msg);
                            Ext.getCmp('contenedorPrincipal').unmask();                            
                            }
                        })                    
                    }
                }
           }
       });    
    }
    

    

    
    var verSolicitud = function(icSolicitud){
        Ext.Ajax.request({
            url: '../41altadoctos/41altaDoctobExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'solData',
                icSolicitud: icSolicitud
            }),
        callback: fillWinSolDet
        });       
    }
    
     Ext.define('VistoBuenoDataModel', {
        extend: 'Ext.data.Model',
        fields: [
             {name: 'icSolicitud',          	mapping: 'icSolicitud'},
             {name: 'icDocumento',          	mapping: 'icDocumento'},
             {name: 'icArchivo',          	mapping: 'icArchivo'},
             {name: 'icIF',          		mapping: 'icIF'},
             {name: 'nombreIF',          	mapping: 'nombreIF'},
             {name: 'fechaLiberacion',          mapping: 'fechaLiberacion'},
             {name: 'folioSolicitud',           mapping: 'folioSolicitud'},
             {name: 'version',          	mapping: 'version'},
             {name: 'tipoDocumento',          	mapping: 'tipoDocumento'},
             {name: 'icTipoDocumento',          mapping: 'icTipoDocumento'},
             {name: 'nombreEjecutivoAtencion',	mapping: 'nombreEjecutivoAtencion'},
             {name: 'icEjecutivoAtencion',  	mapping: 'icEjecutivoAtencion'},
             {name: 'firmasRequeridas',         mapping: 'firmasRequeridas'},
             {name: 'firmasRegistradas',        mapping: 'firmasRegistradas'},
             {name: 'nombreProducto',           mapping: 'nombreProducto'},
             {name: 'nombreEstatus',          	mapping: 'nombreEstatus'},
             {name: 'nombrePortafolio',         mapping: 'nombrePortafolio'},
             {name: 'fechaMaximaFormalizacion', mapping: 'fechaMaximaFormalizacion'},
             {name: 'icEstatus',          	mapping: 'icEstatus'},
             {name: 'cumplePrelacionIF',        mapping: 'cumplePrelacionIF'},
             {name: 'icUsuariosFirmantes',      mapping: 'icUsuariosFirmantes'}
        ]
     });
     
     
    var procesarConsulta = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if(store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso','No existe informaci�n con los criterios determinados');
            }
        }
        storeComboEjcutivos.load();
        gridResultados.el.unmask();
    }
        

        

	var gridStore = Ext.create('Ext.data.Store', {
	   model: 'VistoBuenoDataModel',
            proxy:              {
                type:            'ajax',
                url:             '41vistobuenoExt.data.jsp',
                reader:          {
                    type:         'json',
                    root:         'registros'
                },
                extraParams:     {
                    informacion:  'vistoBueno',
                    fechaRecepcion1: Ext.Date.format(Ext.Date.subtract(new Date(), Ext.Date.MONTH, 1), 'd/m/Y'),
                    fechaRecepcion2: Ext.Date.format(new Date(), 'd/m/Y')
                }
            },
            autoLoad: true,
            listeners: {
                exception: NE.util.mostrarDataProxyError,
                load: procesarConsulta
            }            
		});        
        
        
    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    });
    
    
/*************** P R O R R O G A *********************************/
    function definirProrroga(record){
        if (!esAdminNafinGestgar){
            return;
        }
        // mostrar ventana para agregar fecha de prorroga
         Ext.create('Ext.window.Window', {
            title: 'Agregar nueva fecha m�xima de formalizaci�n',
            id: 'winFechaProrroga',
            height: 150,
            width: 400,
            bodyStyle: 'background:#FFF;',
            modal: true,
            items: [
                {
                    xtype: 'datefield',
                    id : 'nuevaFecha',
                    name: 'nuevaFecha',
                    padding: 5,
                    fieldLabel: 'Fecha de Prorroga',
                    padding: "20 0 0 25",
                    width: 332,
                    labelWidth: 150,
                    emptyText: 'nueva fecha',
                    allowBlank: false,
                    minValue: new Date()
                },
                NE.util.getEspaciador(20),                
                {
                    xtype: 'button',
                    text: 'Aceptar',
                    style: {
                        marginLeft: '110px'
                    },
                    iconCls: 'icoGuardar',
                    handler: function() {
                        var campoFecha = Ext.getCmp("nuevaFecha");
                        if (campoFecha.isValid()){
                            Ext.Ajax.request({
                                url: '41vistobuenoExt.data.jsp',
                                params: Ext.apply(  {
                                    informacion: 'setProrroga',
                                    icDocumento: record.get('icDocumento'),
                                    icIF: record.get('icIF'),
                                    newDate : campoFecha.getRawValue()
                                }),
                            callback: function(opts, success, response) {
                                var jsondeAcuse = Ext.JSON.decode(response.responseText);
                                if (success == true && jsondeAcuse.success == true) {			
                                        gridStore.reload();
                                        Ext.MessageBox.alert('Fecha de Prorroga', 'Se ha guardado la Fecha correctamente.');
                                } else {
                                        NE.util.mostrarConnError(response,opts);
                                }
                                var windowFecha = Ext.getCmp("winFechaProrroga");
                                windowFecha.close();
                                }
                            });  
                        }
                        else{
                            return;
                        }
                    }
                },
                {
                    xtype: 'button',
                    text: 'Cancelar',
                    iconCls: 'icoCancelar',
                    style: {
                        marginLeft: '25px'
                    },
                    handler: function() {
                        var windowFecha = Ext.getCmp("winFechaProrroga");
                        windowFecha.close();
                    }
                }        
            ]
        }).show();        
        
    }
    
    
		
//********************************** GRID PRINCIPAL ************************************//

        var gridResultados = Ext.create('Ext.grid.Panel', {
           id                : 'gridId',
           store             : gridStore,
           stripeRows        : true,
           title             : 'Documentos ',  
           width             : 944,
           height            : 400,
           collapsible       : false,  
           plugins: [cellEditing],
           enableColumnMove  : true,
           enableColumnResize:true,
           autoScroll: true,
		   columns           :
		   [{ 
			  header: "Fecha de<br/>Recepci�n",
			  dataIndex: 'fechaLiberacion',	
			  width:90,
			  sortable: true,  
			  hideable: true  
		   },{
			  header: "Folio de<br/>Solicitud", 
			  dataIndex: 'folioSolicitud',
			  width: 70,
			  sortable: true,
			  hideable: true,
                          tdCls : 'cursorPointer',
                          listeners:{             
                                click: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) { 
                                var rec = gridStore.getAt(iRowIdx.index);
                                verSolicitud(rec.get("icSolicitud"));
                            }
                          }
		   }
                    ,{
                      header: "Intermediario<br/>Financiero", 
                      dataIndex: 'nombreIF',
                      width: 190,
                      sortable: true,
                      hideable: true   
               }               
                    ,{
                      header: "Tipo de<br/>Documento", 
                      dataIndex: 'tipoDocumento',
                      align: 'left',
                      width: 180,
                      sortable: true,
                      hideable: true   
               }  
                    ,{
                      header: "Portafolio", 
                      dataIndex: 'nombrePortafolio',
                      align: 'left',
                      width: 130,
                      sortable: true,
                      hideable: true                        
               },
            {
                header: "Ejecutivo",
                dataIndex: 'nombreEjecutivoAtencion',
                hideable: true,
                sortable: true, 
                width:130
            }
                ,{
                  header: "Producto/<br/>Empresa", 
                  dataIndex: 'nombreProducto',
                  width: 150,
                  align: 'left',
                  sortable: true,
                  hideable: true,
                    renderer: function(value) {
                        return '<span data-qtitle="Producto/Empresa" data-qwidth="250" ' +
                            'data-qtip="' + value + '">' +
                            value + '<\/span>';
                    }                        
           },
            {
                header: "Versi�n del<br/>documento",
                dataIndex: 'version',
                align: 'center',
                hideable: true,
                sortable: true, 
                width:80,
                   renderer: function(value, metaData, record, row, col, store, gridView){
                    return "V"+value;
                  }
            } 
            ,{
              header: "Documentos a<br/>formalizar",
              menuText: "Documentos a<br/>formalizar",
              xtype:'actioncolumn',              
              dataIndex: 'icDocumento',
              align: 'center',
              width: 120,
              sortable: true,
              hideable: true,
                items: [{
                    iconCls: 'icoFolderTable',
                    tooltip: 'Mostrar Documentos',                   
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex); 
                        mostrarVentanaDocumentos(record);
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        return record.get('numeroArchivos')==0;
                    }                    
                }]              
             } 
            ,{
                  header: "Fecha M�xima para<br/>Formalizaci�n IF", 
                  dataIndex: 'fechaMaximaFormalizacion',
                  align: 'center',
                  width: 120,
                  sortable: true,
                  hideable: true,
                  listeners:{             
                        click: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) { 
                            var record = gridStore.getAt(iRowIdx.index);
                            definirProrroga(record);
                        }
                  }              
             } 
            ,{
              header: "Firmantes requeridos<br/>por el IF", 
              dataIndex: 'firmasRequeridas',
              align: 'center',
              width: 100,
              sortable: true,
              hideable: true,
              renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                var registrados = record.get('firmasRegistradas');
                 return registrados+" de "+value;
              }                
             } 
            ,{
              header: "Acciones", 
              xtype:'actioncolumn',              
              menuText: "Acciones",
              dataIndex: 'firmasRequeridas',
              align: 'center',
              width: 100,
                items: [{
                    iconCls: 'correcto',
                    tooltip: 'Aceptar',                   
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex); 
                        validarDocumento(record);
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        return record.get('icEstatus') != ESTATUS_RUBRICA_NAFIN;
                    }
                },
                {               
                    iconCls: 'icoRechazar marginL15p',
                    tooltip: 'Rechazar',                   
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex); 
                        rechazarDocumento(record)
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        return record.get('icEstatus') != ESTATUS_RUBRICA_NAFIN;
                    }
                }                
                ]               
             } 
            ,{
              header: "Estatus", 
              dataIndex: 'nombreEstatus',
              align: 'left',
              width: 120,
              sortable: true,
              hideable: true  
             },              
            {
                header: "Bases de<br/> operaci�n", 
                menuText: "Bases de<br/> operaci�n", 
                dataIndex: 'nombreEstatus',
                xtype:'actioncolumn',    
                align: 'center',
                width: 100,
                sortable: true,
                hideable: true, 
                items: [{
                        iconCls: 'correcto',
                        tooltip: 'Aceptar Base de Operacion',                   
                        handler: function(grid, rowIndex, colIndex) {
                            var record = grid.getStore().getAt(rowIndex); 
                            var icDocumento = record.get('icDocumento');
                            var icIF = record.get('icIF');
                            validarBaseOperacion(icDocumento, icIF);
                        },
                        isDisabled: function(view, rowIndex, colIndex, item, record) {
                            var tipoDoc = record.get("icTipoDocumento");
                            var esFicha = tipoDoc == FICHA_IF_G_AUTOMATICA || tipoDoc == FICHA_NAFIN_G_AUTOMATICA ||
                                tipoDoc == FICHA_G_SELECTIVA;
                            return !esFicha && record.get('icEstatus') != ESTATUS_FORMALIZADO_CARGA_BO;
                        } 
                    },
                    {               
                        iconCls: 'icoRechazar marginL15p',
                        tooltip: 'Rechazar Base de Operacion',                   
                        handler: function(grid, rowIndex, colIndex) {
                            var record = grid.getStore().getAt(rowIndex); 
                            actualFolio = record.get('folio');
                            rechazarBaseOperacion(record.get('icSolicitud'));
                        },
                        isDisabled: function(view, rowIndex, colIndex, item, record) {
                            var tipoDoc = record.get("icTipoDocumento");
                            var esFicha = tipoDoc == FICHA_IF_G_AUTOMATICA || tipoDoc == FICHA_NAFIN_G_AUTOMATICA ||
                                tipoDoc == FICHA_G_SELECTIVA;
                            return !esFicha && record.get('icEstatus') != ESTATUS_FORMALIZADO_CARGA_BO;
                        }
                    }                
                ]  
            }        
       ]
});


//----------************* CATALOGOS  *************----------//
	var  storeEstatus = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41formalizacionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catEstatus'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});    
  
    	var  storeEjecutivos = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41formalizacionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catEjecutivo'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});
    
    
	var  storeFolioSolicitud = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41formalizacionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catFolios'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});    




    
    
  
    
	var  storeSolicitantes = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41formalizacionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catSolicitante'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});      
    
    
    
    	var  storeSolicitudesIF = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41formalizacionExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catSolicitudesIF'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});   



var validarFechas =  function () {
    var fechasValidar = ['fechaFormalizacion', 'fechaRecepcion'];
    //for (i= 0; fechaValidar){}
    var fechaUno = Ext.getCmp('fechaFormalizacion1');
    var fechaDos = Ext.getCmp('fechaFormalizacion2');
    var fechaUnoVal = fechaUno.getValue();
    var fechaDosVal = fechaDos.getValue();
    if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)){
        fechaUno.markInvalid(ERROR1FECHAS);
        fechaDos.markInvalid(ERROR1FECHAS);
        return ERROR1FECHAS;
    }
    if(fechaUno != null && fechaUno > fechaDos){
        fechaUno.markInvalid(ERROR2FECHAS);
        fechaDos.markInvalid(ERROR2FECHAS);
        fechaDos.focus();
        return ERROR2FECHAS;
    }
    return true;
}

//----------*************  PANEL BUSQUEDA  *************----------//
var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        width: 943,
        frame: true,
        height: 'auto',
        title: 'Filtros de B�squeda',
        layout: 'column',
        items: [
            {
                xtype: 'fieldset',
                id:'fieldsBusqueda1',
                height: 190,
                border:false,
                columnWidth: 0.5,
                items: [
                        {
                            xtype: 'combo',
                            name: 'tipoDocumento',
                            id: 'tipoDocumento',
                            fieldLabel: 'Tipo de Documento',
                            padding: 5,
                            width: 400,
                            forceSelection: true, 
                            queryMode: 'local', 
                            allowBlank: true,
                            emptyText: 'Todos los Documentos',
                            store: storeTipoDocumento, 
                            displayField: 'descripcion', 
                            valueField: 'clave'
                        },
                        {
                            xtype: 'combo',
                            id: 'folioSolicitud',
                            name: 'folioSolicitud',
                            fieldLabel: 'Folio de la Solicitud',
                            padding: 5,
                            width: 400,
                            forceSelection: true, 
                            allowBlank: true,
                            emptyText: 'Todos los folios',
                            store: storeSolicitantes, displayField: 'descripcion', valueField: 'clave'                             
                        },                                         
                        {
                            xtype: 'combo',
                            id: 'portafolio',
                            name: 'portafolio',
                            fieldLabel: 'Portafolio',
                            padding: 5,
                            width: 400,
                            store: storeEstatus,
                            emptyText: 'Todos los Portafolios',
                            displayField: 'descripcion', 
                            valueField: 'clave',
                            forceSelection: true,
                            allowBlank: true
                        },
                        {
                            xtype: 'combo',
                            id: 'productoNombre',
                            name: 'productoNombre',
                            fieldLabel: 'Producto/Nombre',
                            padding: 5,
                            width: 400,
                            store: storeEstatus,
                            emptyText: 'Todos los Productos',
                            displayField: 'descripcion', 
                            valueField: 'clave',
                            forceSelection: true,
                            allowBlank: true
                        },                       
                        {
                        xtype:'fieldset',
                        id: 'ffechaRecepcion',
                        collapsible:false,
                        border: false,
                        defaultType: 'textfield',
                        defaults: {anchor: '100%'},
                        padding: 0,
                        layout: 'hbox',
                        items :[
                                {
                                    xtype: 'datefield',
                                    id : 'fechaRecepcion1',
                                    name: 'fechaRecepcion1',
                                    fieldLabel: 'Fecha de Recepcion',
                                    padding: 5,
                                    width: 215,
                                    emptyText: 'Desde',
                                    displayField: 'descripcion',
                                    valueField: 'clave',
                                    forceSelection: true, 
                                    allowBlank: true,
                                    value: Ext.Date.subtract(new Date(), Ext.Date.MONTH, 1)
                                },
                                {
                                    xtype: 'datefield',
                                    id : 'fechaRecepcion2',
                                    name: 'fechaRecepcion2',
                                    fieldLabel: 'al',
                                    labelWidth: 55,
                                    padding: 10,
                                    width: 170,
                                    emptyText: 'Hasta',
                                    valueField: 'clave',
                                    forceSelection: true, 
                                    allowBlank: true,
                                    value: new Date()
                                }
                            ]
                        }                            
                    ]                
            },
            {
                xtype: 'fieldset',
                height: 150,
                border:false,
                columnWidth: 0.5,
                margins: '0 20 0 0',
			items: [
                    {
                        xtype: 'combo',
                        id: 'estatus',
                        name: 'estatus',
                        fieldLabel: 'Estatus',
                        padding: 5,
                        width: 440,
                        forceSelection: true, 
                        queryMode: 'local', 
                        allowBlank: true,
                        store: storeEstatus,
                        emptyText: 'Todos los Estatus',
                        displayField: 'descripcion', 
                        valueField: 'clave'
                    },
                    {
                        xtype: 'combo',
                        id: 'nombreEjecutivo',
                        name: 'nombreEjecutivo',
                        fieldLabel: 'Ejecutivo:',
                        padding: 5,
                        width: 440,
                        store: storeSolicitudesIF,
                        forceSelection: true,
                        queryMode: 'local', 
                        allowBlank: true,
                        emptyText: 'Todos los Ejecutivos',
                        displayField: 'descripcion', 
                        valueField: 'clave'
                    },
                    {
                        xtype: 'combo',
                        id: 'intermediarioFinanciero',
                        name: 'intermediarioFinanciero',
                        fieldLabel: 'Intermediario Financiero:',
                        padding: 5,
                        width: 440,
                        store: storeSolicitudesIF,
                        forceSelection: true,
                        queryMode: 'local', 
                        allowBlank: true,
                        emptyText: 'Todos los Intermediarios:',
                        displayField: 'descripcion', 
                        valueField: 'clave'
                    },
                    {
                    xtype:'fieldset',
                    collapsible:false,
                    id: 'ffechaFormalizacion',
                    border: false,
                    defaultType: 'textfield',
                    defaults: {anchor: '100%'},
                    padding: 0,
                    layout: 'hbox',
                    items :[
                            {
                                xtype: 'datefield',
                                id : 'fechaFormalizacion1',
                                name: 'fechaFormalizacion1',
                                fieldLabel: 'Fecha m�xima de Formalizacion:',
                                padding: 5,
                                width: 215,
                                emptyText: 'Desde',
                                displayField: 'descripcion',
                                valueField: 'clave',
                                forceSelection: true, 
                                allowBlank: true                         
                            },
                            {
                                xtype: 'datefield',
                                id : 'fechaFormalizacion2',
                                name: 'fechaFormalizacion2',
                                fieldLabel: 'al',
                                labelWidth: 55,
                                padding: 10,
                                width: 170,
                                emptyText: 'Hasta',
                                valueField: 'clave',
                                forceSelection: true, 
                                allowBlank: true
                            }
                        ]
                    }                      
                ]
            }
        ],
        buttons: [     
        {
            text: 'Buscar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarFechas();
                if(resultado != true){
                    Ext.MessageBox.alert('Aviso',resultado);
                }
                else{
                gridResultados.el.mask('Cargando...', 'x-mask-loading');
                gridStore.load({
                    informacion:  'vistoBueno',
                    params: Ext.apply(formBusqueda.getForm().getValues())
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                formBusqueda.getForm().reset();
            }
        }]      
    });


//----------*************  CONTENEDOR PRINCIPAL  *************----------//

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			formBusqueda,
			NE.util.getEspaciador(30),
                        gridResultados,
			NE.util.getEspaciador(20)
		]        
	});



}); 