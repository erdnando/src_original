<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.nafin.docgarantias.formalizacion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%@ include file="../../certificado.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    private static final Integer ESTATUS_RUBRICA_NAFIN       = 150;
    private static final Integer ESTATUS_OPERANDO            = 190;
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String icSolicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
String infoRegresar="";

log.info("informacion: "+informacion);

Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);
if (informacion.equals("docsFormalizacionNafin")){   //<------------------------------------------------------

    ArrayList<String> parametros = new ArrayList<>(); 
    String folioSolicitud = request.getParameter("folioSolicitud") == null ? "" : request.getParameter("folioSolicitud");
    String fechaSolicitud1 = request.getParameter("fechaSolicitud1") == null ? "" : request.getParameter("fechaSolicitud1");
    String fechaSolicitud2 = request.getParameter("fechaSolicitud2") == null ? "": request.getParameter("fechaSolicitud2");
    String usuarioSolicitante = request.getParameter("usuarioSolicitante") == null ? "" : request.getParameter("usuarioSolicitante");
    String usuarioEjecutivo = request.getParameter("usuarioEjecutivo") == null ? "" : request.getParameter("usuarioEjecutivo");
    String estatus = request.getParameter("estatus") == null ? "" : request.getParameter("estatus");
    String intermediario = request.getParameter("icIF") == null ? "0" : request.getParameter("icIF");
    Integer icIF = Integer.parseInt(intermediario);
    Map<String,String> params = new HashMap<>();
    params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF.toString());
    params.put(ConstantesFormalizacion.PARAM_IC_USUARIO, strLogin);
    ArrayList<DocValidacion> listado = formalizacionBean.getDocFormalizacionNAFIN(params);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("ACUSE")) {		
	Seguridad s = new Seguridad();
	String acusePki	= "";
	char generarAcuse = 'Y'; 
       	String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
        String usuario = null;
	try{
            String cadenaOriginal = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";
            String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";      
            String strPreacuse	= (request.getParameter("strPreacuse")==null)?"":request.getParameter("strPreacuse").trim(); 
            usuario = 	strLogin+ " - " +strNombreUsuario;
            String documento = request.getParameter("icDocumento") ;
            String archivo =  request.getParameter("icArchivo") ;
            String intermediario = request.getParameter("icIF");
            Integer icIF = Integer.parseInt(intermediario);
            Integer icDocumento = Integer.parseInt(documento);
            Integer icArchivo = Integer.parseInt(archivo);
            if (!formalizacionBean.usuarioNafinPuedeFirmarDocumento(icIF, icDocumento, strLogin)){
             log.info("InvalidUserSign: "+ strLogin + ","+icIF+","+icDocumento);
             throw new NafinException("El documento no se puede formalizar por el usuario");
            }
            if (!formalizacionBean.validaPrelacionDocumentoParaFirmar(icDocumento, icIF)){
                throw new NafinException("El documento no se puede formalizar debido a que no cumple con la prelacion establecida.");
            }
            
            if (!_serial.equals("") && cadenaOriginal!=null && pkcs7!=null) {
                System.out.println("_serial "+_serial);
                System.out.println("pkcs7 "+pkcs7);
                System.out.println("externContent "+cadenaOriginal);
                System.out.println("getReceipt "+generarAcuse);
                // Se AUTENTICA el texto firmado 
                if (s.autenticar(documento, _serial, pkcs7, cadenaOriginal, generarAcuse)){
                    acusePki = s.getAcuse(); // Este es el Folio del acuse del PKI			
                    log.info("ACUSE: "+ acusePki);
                    
                    HashMap<String, String> parametros = new HashMap<>();
                    parametros.put(ConstantesFormalizacion.PARAM_CADENA_FIRMADA, pkcs7);
                    parametros.put(ConstantesFormalizacion.PARAM_FOLIO_PKI, acusePki);
                    parametros.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento.toString());
                    parametros.put(ConstantesFormalizacion.PARAM_IC_USUARIO, strLogin);
                    parametros.put(ConstantesFormalizacion.PARAM_IC_IF, icIF.toString());
                    parametros.put(ConstantesFormalizacion.PARAM_TIPO_FIRMA, ConstantesFormalizacion.FIRMA_NAFIN+"");
                    formalizacionBean.guardarAcuse(parametros);
                    parametros.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo.toString());
                    
                    // AVANZAR DOCUMENTO DE ESTATUS 
                    FlujoDocumentos flujoDocumentos = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
                    flujoDocumentos.avanzarDocumentoFormalizacionNafin(icDocumento, strLogin, icIF, null);
                    
                    // Se agrega las cadenas de firma al PDF 
                    //String rutaDoc = formalizacionBean.FirmaDocumento(strDirectorioPublicacion, parametros);
                    //log.info("Ruta Documento Firmado: "+ rutaDoc);
                }
                else{
                    throw new NafinException("No se pudo autenticar el texto firmado");
                }
            }
	}catch(Exception e){
            e.printStackTrace();
            throw new NafinException("Error inesperado: "+ e.getMessage());
	}           
            
        JSONObject 	resultado	= new JSONObject();
        resultado.put("success", new Boolean(true));
        resultado.put("acuse", acusePki);
        resultado.put("fecha",fechaHoy);	
        resultado.put("hora",horaActual);	
        resultado.put("usuario",usuario.toString());
        resultado.put("folioDocumento",acusePki);
        infoRegresar = resultado.toString();
}
else if(informacion.equals("filesDoc")){
    String icDoc = request.getParameter("icDocumento") == null ? "0" : request.getParameter("icDocumento");
    Integer icDocumento = Integer.parseInt(icDoc);
    Map<String, ArrayList> mapaDocs = formalizacionBean.getFilesSoporteDocumento(icDocumento);
    ArrayList<ElementoCatalogo>listadoSoporte = mapaDocs.get("documentosSoporte");
    ArrayList<ElementoCatalogo>listadoFormalizacion = mapaDocs.get("documentosFormalizar");
    JSONArray jsObjArraySoporte = new JSONArray();
    jsObjArraySoporte = JSONArray.fromObject(listadoSoporte);
    
    JSONArray jsObjArrayFormalizacion = new JSONArray();
    jsObjArrayFormalizacion = JSONArray.fromObject(listadoFormalizacion);
    infoRegresar = "{\"success\": true,  \"documentosSoporte\": " + jsObjArraySoporte.toString() + ", \"documentosFormalizar\": "+jsObjArrayFormalizacion.toString() +"}";
}
else if (informacion.equals("getDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("getSignedDoc")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
    String sicDoc = request.getParameter("icDoc") == null ? "" : request.getParameter("icDoc");
    Map<String,String> params = new HashMap<>();
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);   
        Integer icIF = Integer.parseInt(sicIF);
        Integer icDocumento = Integer.parseInt(sicDoc);
        params.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo+"");
        params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF+"");
        params.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento+"");
        String nombreArchivo = formalizacionBean.FirmaDocumento(strDirectorioPublicacion, params);
        //ArchivoDocumento archivo = formalizacionBean.getArchivo(icArchivo, strDirectorioPublicacion);
        //jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        jsonObj.put("urlArchivo", nombreArchivo);		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa los parametros getSignedDoc");
        jsonObj.put("msg","Error al procesa los parametros icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("catDocumento")){
    ArrayList<ElementoCatalogo> listado = solicitudesBean.getCatDocumentos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("catFolio")){
    ArrayList<ElementoCatalogo> listado = formalizacionBean.getCatFolioDocumentoIF(null, ESTATUS_RUBRICA_NAFIN, ESTATUS_OPERANDO);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("catPortafolio")){
    ArrayList<ElementoCatalogo> listado = solicitudesBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("catProductoEmpresa")){
    ArrayList<ElementoCatalogo> listado = formalizacionBean.getCatNombreProductoEmpresa(ESTATUS_RUBRICA_NAFIN, null);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("catEstatus")){
    ArrayList<ElementoCatalogo> listado = formalizacionBean.getCatEstatusDocumento(ESTATUS_RUBRICA_NAFIN, ESTATUS_OPERANDO);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("catEjecutivo")){
    ArrayList<ElementoCatalogo> listado = formalizacionBean.getCatEjecutivos(ESTATUS_RUBRICA_NAFIN);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("catIntermediario")){
    ArrayList<ElementoCatalogo> listado = formalizacionBean.getCatIntermediarioDocFormalizacion();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}




log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>