<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.nafin.docgarantias.formalizacion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%@ include file="../../certificado.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%


    String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
    String icSolicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
    String infoRegresar="";
    
    log.info("informacion: "+informacion);
    SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);
    Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class); 

if (strPerfil.equals("ADMIN GESTGAR")){


    
    if (informacion.equals("vistoBueno")){   //<------------------------------------------------------
           
        ArrayList<String> parametros = new ArrayList<>(); 
        String folioSolicitud = request.getParameter("folioSolicitud") == null ? "" : request.getParameter("folioSolicitud");
        String fechaRecepcion1 = request.getParameter("fechaRecepcion1") == null ? null : request.getParameter("fechaRecepcion1");
        String fechaRecepcion2 = request.getParameter("fechaRecepcion2") == null ? null : request.getParameter("fechaRecepcion2");
        String intermediario = request.getParameter("intermediario") == null ? "0" : request.getParameter("intermediario");
        String usuarioSolicitante = request.getParameter("usuarioSolicitante") == null ? "" : request.getParameter("usuarioSolicitante");
        String usuarioEjecutivo = request.getParameter("usuarioEjecutivo") == null ? "" : request.getParameter("usuarioEjecutivo");
        String estatus = request.getParameter("estatus") == null ? "" : request.getParameter("estatus");
        
        
        Integer icIF = Integer.parseInt(iNoCliente);
        Map<String,String> params = new HashMap<>();
        params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF.toString());
        params.put(ConstantesFormalizacion.PARAM_IC_USUARIO, strLogin);
        params.put(ConstantesFormalizacion.PARAM_FECHA_LIBERACION_INICIO, fechaRecepcion1);
        params.put(ConstantesFormalizacion.PARAM_FECHA_LIBERACION_FIN, fechaRecepcion2);
        ArrayList<DocValidacion> listado = formalizacionBean.getDocVistoBuenoNAFIN(params);
        
        JSONArray jsObjArray = new JSONArray();
        jsObjArray = JSONArray.fromObject(listado);
        infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
    }
    else if(informacion.equals("ACUSE")) {		
            Seguridad s = new Seguridad();
            String acusePki	= "";
            char generarAcuse = 'Y'; 
            String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
            String usuario = null;
            try{
                String cadenaOriginal = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";
                String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";      
                String strPreacuse	= (request.getParameter("strPreacuse")==null)?"":request.getParameter("strPreacuse").trim(); 
                usuario = 	strLogin+" - "+strNombreUsuario;
                String documento = request.getParameter("icDocumento") ;
                String archivo =  request.getParameter("icArchivo") ;
                String intermediario = request.getParameter("icIF");
                Integer icIF = Integer.parseInt(intermediario);
                Integer icDocumento = Integer.parseInt(documento);
                Integer icArchivo = Integer.parseInt(archivo);
    
                if (!formalizacionBean.validaPrelacionDocumentoParaFirmar(icDocumento, icIF)){
                    throw new NafinException("El documento no se puede formalizar debido a que no cumple con la prelacion establecida.");
                }
                
                if (!_serial.equals("") && cadenaOriginal!=null && pkcs7!=null) {
                    System.out.println("_serial "+_serial);
                    System.out.println("pkcs7 "+pkcs7);
                    System.out.println("externContent "+cadenaOriginal);
                    System.out.println("getReceipt "+generarAcuse);
                    // Se AUTENTICA el texto firmado 
                    if (s.autenticar(documento, _serial, pkcs7, cadenaOriginal, generarAcuse)){
                        acusePki = s.getAcuse(); // Este es el Folio del acuse del PKI			
                        log.info("ACUSE: "+ acusePki);
                        
                        HashMap<String, String> parametros = new HashMap<>();
                        parametros.put(ConstantesFormalizacion.PARAM_CADENA_FIRMADA, pkcs7);
                        parametros.put(ConstantesFormalizacion.PARAM_FOLIO_PKI, acusePki);
                        parametros.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento.toString());
                        parametros.put(ConstantesFormalizacion.PARAM_IC_USUARIO, strLogin);
                        parametros.put(ConstantesFormalizacion.PARAM_IC_IF, icIF.toString());
                        parametros.put(ConstantesFormalizacion.PARAM_TIPO_FIRMA, ConstantesFormalizacion.FIRMA_VOBO_NAFIN+"");
                        if (formalizacionBean.guardarAcuse(parametros)){
                            parametros.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo.toString());
                            // AVANZAR DOCUMENTO DE ESTATUS 
                            FlujoDocumentos flujoDocumentos = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
                            flujoDocumentos.avanzarDocumentoIF(icDocumento, strLogin, icIF, null);
                            // Se agrega las cadenas de firma al PDF 
                            String rutaDoc = formalizacionBean.FirmaDocumento(strDirectorioPublicacion, parametros);
                            log.info("Ruta Documento Firmado: "+ rutaDoc);
                        }
                        else{
                            log.info("FIRMA DUPLICADA: "+ icDocumento+" "+icIF+" "+ strLogin);
                            throw new NafinException("GDOC-Formalizacion","No se pudo guardar el acuse");
                        }                        
                    }
                    else{
                        throw new NafinException("GDOC-Formalizacion","No se pudo autenticar el texto firmado");
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
                throw new NafinException("GDOC-Formalizacion","Error inesperado: "+ e.getMessage());
            }           
                
            JSONObject 	resultado	= new JSONObject();
            resultado.put("success", new Boolean(true));
            resultado.put("acuse", acusePki);
            resultado.put("fecha",fechaHoy);	
            resultado.put("hora",horaActual);	
            resultado.put("usuario",usuario.toString());
            resultado.put("folioDocumento",acusePki);
            infoRegresar = resultado.toString();
    }
    else if(informacion.equals("filesSol")){
        String icDoc = request.getParameter("icDocumento") == null ? "0" : request.getParameter("icDocumento");
        Integer icDocumento = Integer.parseInt(icDoc);
        Map<String, ArrayList> mapaDocs = formalizacionBean.getFilesSoporteDocumento(icDocumento);
        ArrayList<ElementoCatalogo>listadoSoporte = mapaDocs.get("documentosSoporte");
        ArrayList<ElementoCatalogo>listadoFormalizacion = mapaDocs.get("documentosFormalizar");
        JSONArray jsObjArraySoporte = new JSONArray();
        jsObjArraySoporte = JSONArray.fromObject(listadoSoporte);
        
        JSONArray jsObjArrayFormalizacion = new JSONArray();
        jsObjArrayFormalizacion = JSONArray.fromObject(listadoFormalizacion);
        infoRegresar = "{\"success\": true,  \"documentosSoporte\": " + jsObjArraySoporte.toString() + ", \"documentosFormalizar\": "+jsObjArrayFormalizacion.toString() +"}";
    }
    else if (informacion.equals("getDocFile")){
        boolean exito = false;
        Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
        JSONObject jsonObj   =  new JSONObject();
        String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
        try{
            Integer icArchivo = Integer.parseInt(sicArchivo);        
            ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
            jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
            exito = true;
        }catch(NumberFormatException  e){
            log.info("Error al procesa el parametro icArchivo");
            jsonObj.put("msg","Error al procesa el parametro icArchivo");
            e.printStackTrace();
        }finally{
            jsonObj.put("success", exito); 
            infoRegresar = jsonObj.toString();
        }
    }
    else if (informacion.equals("getCatApoderados")){
        String tipoDocumento = request.getParameter("icTipoDocumento") != null ? request.getParameter("icTipoDocumento") : "0";
        Integer icTipoDocumento = Integer.parseInt(tipoDocumento);
        ArrayList<ElementoCatalogo> listado = formalizacionBean.getCatFirmantesDocumento(icTipoDocumento);
        JSONArray jsObjArray = new JSONArray();
        jsObjArray = JSONArray.fromObject(listado);
        infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
    }
    
    else if (informacion.equals("guardaVistoBueno")){
        
        boolean exito = false;
        JSONObject jsonObj   =  new JSONObject();
        try{
            String sicDoc = request.getParameter("icDocumento") == null ? "" : request.getParameter("icDocumento");
            String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
            String firmanteUno= request.getParameter("wfirmante1") == null ? "" : request.getParameter("wfirmante1");
            String firmanteDos = request.getParameter("wfirmante2") == null ? "" : request.getParameter("wfirmante2");
            Integer icDocumento = Integer.parseInt(sicDoc);        
            Integer icIF = Integer.parseInt(sicIF);
            ArrayList<String> usuariosFirmantes = new ArrayList<>();
            usuariosFirmantes.add(firmanteUno);
            if (firmanteDos != null && !firmanteDos.isEmpty()){
                usuariosFirmantes.add(firmanteDos);    
            }
            exito = formalizacionBean.guardaVistoBuenoDoc(icDocumento, icIF, usuariosFirmantes, strLogin);
        }catch(NumberFormatException  e){
            log.info("Error al procesa los parametros guardaVistoBueno");
            jsonObj.put("msg","Error al procesa los parametros");
            e.printStackTrace();
        }finally{
            jsonObj.put("success", exito); 
            infoRegresar = jsonObj.toString();
        }
    }
    else if (informacion.equals("guardaRechazoVoBo")){
        boolean exito = false;
        JSONObject jsonObj   =  new JSONObject();
        try{
            String sicDoc = request.getParameter("icDocumento") == null ? "" : request.getParameter("icDocumento");
            String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
            String motivo = request.getParameter("motivo") == null ? "" : request.getParameter("motivo");
            Integer icDocumento = Integer.parseInt(sicDoc);        
            Integer icIF = Integer.parseInt(sicIF);
            exito = formalizacionBean.guardaRechazo(icDocumento, icIF, motivo, strLogin);
        }catch(NumberFormatException  e){
            log.info("Error al procesa los parametros guardaVistoBueno");
            jsonObj.put("msg","Error al procesar los parametros");
            e.printStackTrace();
        }finally{
            jsonObj.put("success", exito); 
            infoRegresar = jsonObj.toString();
        }
    }
    else if (informacion.equals("setProrroga")){
        boolean exito = false;
        JSONObject jsonObj   =  new JSONObject();
        try{
            String sicDoc = request.getParameter("icDocumento") == null ? "" : request.getParameter("icDocumento");
            String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
            String fechaProrroga = request.getParameter("newDate") == null ? "" : request.getParameter("newDate");
            Integer icDocumento = Integer.parseInt(sicDoc);        
            Integer icIF = Integer.parseInt(sicIF);
            exito = formalizacionBean.guardaFechaProrroga(fechaProrroga, icDocumento, icIF);
        }catch(NumberFormatException  e){
            log.info("Error al procesa los parametros guardaProrroga");
            jsonObj.put("msg","Error al procesar los parametros");
            e.printStackTrace();
        }finally{
            jsonObj.put("success", exito); 
            infoRegresar = jsonObj.toString();
        }
    }
    
    
    
    
    log.info("infoRegresar: "+infoRegresar);
    
    
} // end if es perfil admin gestfar

%>
<%=infoRegresar%>