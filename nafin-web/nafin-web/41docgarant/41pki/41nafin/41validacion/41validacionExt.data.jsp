<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
        java.util.Arrays,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.nafin.docgarantias.formalizacion.*,"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%@ include file="../../certificado.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    private static final Integer ESTATUS_VALIDACION_JURIDICO = 130;
    private static final Integer ESTATUS_OPERANDO            = 190;
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String icSolicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
String infoRegresar="";

log.info("informacion: "+informacion);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);
Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);

if (informacion.equals("DocValidacion")){   //<------------------------------------------------------
    ArrayList<String> paramNames = new ArrayList<String>(request.getParameterMap().keySet());
    System.out.println("PARAMETROS:"+paramNames);
    String folio = request.getParameter("folio") != null ? request.getParameter("folio").isEmpty() ? null : request.getParameter("folio") : null;
    String tipoDoc = request.getParameter("tipoDoc") != null ? request.getParameter("tipoDoc").isEmpty() ? null : request.getParameter("tipoDoc") : null;
    String nombreProducto = request.getParameter("nombreProducto") != null ? request.getParameter("nombreProducto").isEmpty() ? null : request.getParameter("nombreProducto") : null;
    String estatus = request.getParameter("estatus") != null ?  request.getParameter("estatus").isEmpty() ? null : request.getParameter("estatus") : null;
    String usuarioEjecutivo = request.getParameter("usuarioEjecutivo") != null ? request.getParameter("usuarioEjecutivo").isEmpty() ? null : request.getParameter("usuarioEjecutivo"): null;
    String intermediario = request.getParameter("intermediario")!= null ? request.getParameter("intermediario").isEmpty() ? null : request.getParameter("usuarioEjecutivo") : null;
    String fechaLiberacion1 = request.getParameter("fechaLiberacion1")!= null ? request.getParameter("fechaLiberacion1").isEmpty() ? null : request.getParameter("fechaLiberacion1") : null;
    String fechaLiberacion2 = request.getParameter("fechaLiberacion2") != null ? request.getParameter("fechaLiberacion2").isEmpty() ? null : request.getParameter("fechaLiberacion2") : null;
    
    Map<String,String> params = new HashMap<>();
    params.put(ConstantesFormalizacion.FILTRO_FOLIO, folio);
    params.put(ConstantesFormalizacion.FILTRO_TIPO_DOCUMENTO, tipoDoc);
    params.put(ConstantesFormalizacion.FILTRO_NOMBR_PRODUCTO, nombreProducto);
    params.put(ConstantesFormalizacion.FILTRO_ESTATUS, estatus);
    params.put(ConstantesFormalizacion.FILTRO_EJECUTIVO, usuarioEjecutivo);
    params.put(ConstantesFormalizacion.FILTRO_IF, intermediario);
    params.put(ConstantesFormalizacion.FILTRO_FECHA_LIBERACION_INICIO, fechaLiberacion1);
    params.put(ConstantesFormalizacion.FILTRO_FECHA_LIBERACION_FIN, fechaLiberacion2);
    ArrayList<DocValidacion> listado = solicitudesBean.getDocumentosValidacion(params);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("getDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if(informacion.equals("ACUSE")) {		
	Seguridad s = new Seguridad();
	String acusePki	= "";
	char generarAcuse = 'Y'; 
       	String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String	horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
        String usuario = null;
	try{
            String cadenaOriginal = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";
            String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";      
            //String strPreacuse	= (request.getParameter("strPreacuse")==null)?"":request.getParameter("strPreacuse").trim(); 
            usuario = 	strLogin+" - "+strNombreUsuario;
            String documento = request.getParameter("icDocumento") ;
            String archivo =  request.getParameter("icArchivo") ;
            String intermediario = request.getParameter("icIF");
            Integer icIF = Integer.parseInt(intermediario);
            Integer icDocumento = Integer.parseInt(documento);
            Integer icArchivo = Integer.parseInt(archivo);
            if (!_serial.equals("") && cadenaOriginal!=null && pkcs7!=null) {
                System.out.println("_serial "+_serial);
                System.out.println("pkcs7 "+pkcs7);
                System.out.println("externContent "+cadenaOriginal);
                System.out.println("getReceipt "+generarAcuse);
                // Se AUTENTICA el texto firmado 
                if (s.autenticar(documento, _serial, pkcs7, cadenaOriginal, generarAcuse)){
                    acusePki = s.getAcuse(); // Este es el Folio del acuse del PKI			
                    log.info("ACUSE: "+ acusePki);
                    
                    HashMap<String, String> parametros = new HashMap<>();
                    parametros.put(ConstantesFormalizacion.PARAM_CADENA_FIRMADA, pkcs7);
                    parametros.put(ConstantesFormalizacion.PARAM_FOLIO_PKI, acusePki);
                    parametros.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento.toString());
                    parametros.put(ConstantesFormalizacion.PARAM_IC_USUARIO, strLogin);
                    parametros.put(ConstantesFormalizacion.PARAM_IC_IF, icIF.toString());
                    parametros.put(ConstantesFormalizacion.PARAM_TIPO_FIRMA, ConstantesFormalizacion.FIRMA_JURIDICO+"");
                    if (formalizacionBean.guardarAcuse(parametros)){
                        parametros.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo.toString());
                        FlujoDocumentos flujoBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
                        flujoBean.avanzarDocumento(icDocumento, strLogin);
                    }
                    else{
                        log.info("FIRMA DUPLICADA: "+ icDocumento+" "+icIF+" "+ strLogin);
                        throw new NafinException("GDOC-Formalizacion","No se pudo guardar el acuse");
                    }                    
                }
            }
            else{
                log.error("Hubo un problema al firmar ");
            }
	}catch(Exception e){
            e.printStackTrace();
            throw new NafinException("GDOC-Formalizacion","Error inesperado: "+ e.getMessage());
	}           
            
        JSONObject 	resultado	= new JSONObject();
        resultado.put("success", new Boolean(true));
        resultado.put("acuse", acusePki);
        resultado.put("fecha",fechaHoy);	
        resultado.put("hora",horaActual);	
        resultado.put("usuario",usuario.toString());
        resultado.put("folioDocumento",acusePki);
        infoRegresar = resultado.toString();	
}
else if(informacion.equals("catEstatus")){
    ArrayList<ElementoCatalogo> listado = formalizacionBean.getCatEstatusDocumento(ESTATUS_VALIDACION_JURIDICO, ESTATUS_OPERANDO);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catDocumentos")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatDocumentos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";

}else if(informacion.equals("catPortafolios")){
    ArrayList<ElementoCatalogo> listado = solicitudesBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catProductos")){
    HashMap<String, String> parametros = new HashMap<>();
    Integer icIF = Integer.parseInt(iNoCliente);
    Integer icEstatus = Integer.parseInt("130");
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatProducto(icEstatus, null);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catFolios")){
    ArrayList<ElementoCatalogo>listado = formalizacionBean.getCatFolioDocumentoIF(null, ESTATUS_VALIDACION_JURIDICO, ESTATUS_OPERANDO);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";

}else if(informacion.equals("catEjecutivos")){
    ArrayList<ElementoCatalogo> listado = solicitudesBean.getCatEjecutivos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catIFs")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatIFformalizacion();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("rejectSol")){
    boolean exito = false;
    FlujoDocumentos flujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
    
    Integer icSol = Integer.parseInt(icSolicitud);
    String motivoRechazo =  request.getParameter("toRefuse");
    SolicitudDoc sol  = solicitudesBean.getSolicitud(icSol);
    exito = flujoDocumentosBean.rechazarDocumento(sol, iNoUsuario, motivoRechazo);
    JSONObject jsonObj   =  new JSONObject();
    if (exito){
        jsonObj.put("msg","Se ha rechazado la solicitud correctamente.");
    }
    else{
        jsonObj.put("msg","Error al rechazar la solicitud");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}

log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>