
Ext.onReady(function() {

    var ESTATUS_POR_SOLICITAR = 100;
    var ESTATUS_EN_PROCESO    = 110;
    var ESTATUS_EN_VALIDACION = 120;
    var VALIDACION_JURIDICO   = 130;
    var SIN_PARAMETRIZAR = "Sin parametrizar";


    /****************************** FIRMAR DOCUMENTO ****************************/
        var storeCifrasData = new Ext.data.ArrayStore({
              fields: [
                  {name: 'etiqueta'},
                  {name: 'informacion'}
              ]
	 });

        var gridCifrasControl = new Ext.grid.GridPanel({
            id: 'gridCifrasControl',
            store: storeCifrasData,
            margins: '20 0 0 0',
            style: 'margin:0 auto;',
            hideHeaders : true,
            hidden: true,
            align: 'center',
            columns: [
                {
                    header : 'Etiqueta',
                    dataIndex : 'etiqueta',
                    width : 200,
                    sortable : false
                },
                {
                    header : 'Informacion',
                    dataIndex : 'informacion',
                    width : 350,
                    sortable : false,
                    renderer:  function (causa, columna, registro){
                        columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
                        return causa;
                    }
                }
            ],
            stripeRows: true,
            columnLines : true,
            loadMask: true,
            width: 561,
            style: 'margin:0 auto;',
            autoHeight : true,
            //title: '',
            frame: true,
            scroll  : false
	});

	var fnProcConfirmarCallback = function(vpkcs7, vtextoFirmar, vrecord){

		if (Ext.isEmpty(vpkcs7)) {
			Ext.MessageBox.show({
			   title: 'Firma Electr�ica',
			   msg: '�Error al realizar la firma electr�nica con Pkcs7!',
			   buttons: Ext.MessageBox.OK,
			   fn: showResult,
			   icon: Ext.MessageBox.ERROR
		   });
			return;
		}
		pnl.mask('Firmando Electr�nicamente...', 'x-mask-loading');
		Ext.Ajax.request({
			url : '41validacionExt.data.jsp',
			params : {
				pkcs7: vpkcs7,
				textoFirmado: vtextoFirmar,
				icDocumento: vrecord.get('icDocumento'),
				icIF: vrecord.get('icIF'),
				icArchivo: vrecord.get('icArchivo'),
				informacion: 'ACUSE'
			},
			callback: procesarSuccessFailureGuardar
		});
	}

    function procesarConfirmar(record) {
            var folio = record.get('folioSolicitud');
            var nombre = record.get('nombreProducto');
            var nombreIF = record.get('nombreIF');
            var textoFirmar = nombre+ " Folio: "+ folio+ " para "+ nombreIF;

            NE.util.obtenerPKCS7(fnProcConfirmarCallback, textoFirmar, record);

	}

        var procesarSuccessFailureGuardar =  function(opts, success, response) {
                var jsondeAcuse = Ext.JSON.decode(response.responseText);
		if (success == true && jsondeAcuse.success == true) {
			if (jsondeAcuse != null){
				var acuseCifras = [
                                    ['N�mero de Acuse', jsondeAcuse.acuse],
                                    ['Folio del Documento ', jsondeAcuse.folioDocumento],
                                    ['Fecha ', jsondeAcuse.fecha],
                                    ['Hora ', jsondeAcuse.hora],
                                    ['Usuario ', jsondeAcuse.usuario]
				];
                                gridStore.reload();
				storeCifrasData.loadData(acuseCifras);
				gridCifrasControl.show();
                                pnl.unmask();
			}
		} else {
                        pnl.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}




    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};
            var forma  	= Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self';
			forma.submit();
            }else {
                NE.util.mostrarConnError(response,opts);
        }
        pnl.unmask();
    }


	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }
		]
	});



    var storeComboEjcutivos = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        proxy:              {
            type:            'ajax',
            url:             '41validacionExt.data.jsp',
            reader:          {
                type:         'json',
                root:         'registros'
            },
            extraParams:     {
                informacion:  'catEjecutivo'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }
    });





    function fnLiberarValidar(record){
        Ext.Msg.show({
            title: "Validar Documento",
            msg: '�Esta seguro que desea validar el documento con folio de solicitud: <strong>'+record.get('folioSolicitud')+'</strong>?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                    Ext.Ajax.request({
                        url: '41validacionExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'signDoc',
                            icSolicitud: record.get('icSolicitud')
                        }),
                        callback:  function(opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true ) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert('Enviar Solicitud', info.msg);
                            Ext.getCmp('contenedorPrincipal').unmask();
                        }
                    });

                }
            }
        });
    }


    function  fnEliminarRechazarSolicitud(record){
        if(record.get('estatus') == ESTATUS_EN_VALIDACION){
            fnRechazarSolicitud(record);
        }else{
            fnEliminarSolicitud(record);
        }
    }

    function fnRechazarSolicitud(record){
        Ext.MessageBox.show({
           title: 'Rechazar Documento',
           msg: '�Est� seguro de rechazar el documento con folio de solicitud: <strong>'+record.get('folioSolicitud')+'</strong>?\nCapture el motivo del rechazo:',
           width:400,
           buttons: Ext.MessageBox.YESNOCANCEL,
           icon: Ext.Msg.QUESTION,
           multiline: true,
           fn: function(btn, texto){
                if(btn == 'yes'){
                    var textoLimpio = texto.trim();
                    if (textoLimpio.length < 1){
                        Ext.MessageBox.show({
                           title: 'Rechazar Solicitud',
                           msg: '�Debe capturar la razon del rechazo!\nIntentelo nuevamente',
                           buttons: Ext.MessageBox.OK,
                           icon: Ext.MessageBox.ERROR
                       });
                    }
                    else{
                        Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                        Ext.Ajax.request({
                        url: '41validacionExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'rejectSol',
                            toRefuse:   texto,
                            icSolicitud: record.get('icSolicitud')
                        }),
                        callback:  function(opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true ) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert('Rechazar Solicitud', info.msg);
                            Ext.getCmp('contenedorPrincipal').unmask();
                            }
                        })
                    }
                }
           }
       });
    }

    function fnEliminarSolicitud(record){
        Ext.Msg.show({
            title: "Eliminar Solicitud",
            msg: '�Estas seguro de eliminar la solicitud con folio: <strong>'+record.get('folio')+'</strong>? Esta acci�n no podr� revertirse.',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                    Ext.Ajax.request({
                        url: '41validacionExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'soltrash',
                            icSolicitud: record.get('icSolicitud')
                        }),
                        callback:  function(opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true ) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert('Eliminar Solicitud', info.msg);
                            Ext.getCmp('contenedorPrincipal').unmask();
                        }
                    });
                }
            }
        });
    }

	 Ext.define('HomeSolicitudesDataModel', {
		extend: 'Ext.data.Model',
		fields: [
                   {name: 'icSolicitud', 	    mapping : 'icSolicitud' 	        },
                   {name: 'icIF', 	    mapping : 'icIF' 		},
		   {name: 'icArchivo',	    mapping : 'icArchivo'	},
                   {name: 'icEstatus',	            mapping : 'icEstatus'        	},
		   {name: 'fechaLiberacion', 	    mapping : 'fechaLiberacion'  	},
		   {name: 'nombre',          	    mapping : 'nombre' 	        },
                   {name: 'nombreIF',         mapping : 'nombreIF'          },
		   {name: 'folioSolicitud' ,                 mapping : 'folioSolicitud' 	                },
		   {name: 'nombreIntermediario',    mapping : 'nombreIntermediario'   },
		   {name: 'tipoDocumento', 	    mapping : 'tipoDocumento'	},
                   {name: 'nombreEjecutivoAtencion', 	    mapping : 'nombreEjecutivoAtencion' 	},
		   {name: 'nombreEstatus', 	    mapping : 'nombreEstatus' 		},
                   {name: 'version', 	    mapping : 'version' 		},
                   {name: 'nombreProducto', 	    mapping : 'nombreProducto' 		},
                   {name: 'firmasRequeridas', 	    mapping : 'firmasRequeridas' 		},
                   {name: 'icDocumento', 	    mapping : 'icDocumento' 		}
		]
	 });


    var procesarConsulta = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if(store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso','No existe informaci�n con los criterios determinados');
            }
        }
        storeComboEjcutivos.load();
        gridResultados.unmask();
    }


	var gridStore = Ext.create('Ext.data.Store', {
	   model: 'HomeSolicitudesDataModel',
            proxy:              {
                type:            'ajax',
                url:             '41validacionExt.data.jsp',
                reader:          {
                    type:         'json',
                    root:         'registros'
                },
                extraParams:     {
                    informacion:  'DocValidacion'
                }
            },
            autoLoad: true,
            listeners: {
                exception: NE.util.mostrarDataProxyError,
                load: procesarConsulta
            }
		});


    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    });

//********************************** GRID PRINCIPAL ************************************//

        var gridResultados = Ext.create('Ext.grid.Panel', {
           id                : 'gridId',
           store             : gridStore,
           stripeRows        : true,
           title             : 'Documentos a validar',
           width             : 944,
           height            : 400,
           collapsible       : false,
           plugins: [cellEditing],
           enableColumnMove  : true,
           enableColumnResize:true,
           autoScroll: true,
		   columns           :
		   [{
			  header: "Fecha de<br/>Recepci�n",
			  dataIndex: 'fechaLiberacion',
			  width:80,
			  sortable: true,
			  hideable: true
		   },{
			  header: "Folio de<br/>Solicitud",
			  dataIndex: 'folioSolicitud',
			  width: 70,
			  sortable: true,
			  hideable: true,
                          tdCls : 'cursorPointer',
                          listeners:{
                                click: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
                                var rec = gridStore.getAt(iRowIdx.index);
                                verSolicitud(rec.get("icSolicitud"));
                            }
                          }
		   }
			,{
			  header: "Intermediario<br/>Financiero",
			  dataIndex: 'nombreIF',
			  width: 150,
			  sortable: true,
			  hideable: true
		   }
			,{
			  header: "Tipo de<br/>Documento",
			  dataIndex: 'tipoDocumento',
			  align: 'center',
			  width: 150,
			  sortable: true,
			  hideable: true
		   }
			,{
			  header: "Ejecutivo",
			  dataIndex: 'nombreEjecutivoAtencion',
			  align: 'center',
			  width: 150,
			  sortable: true,
			  hideable: true
		   },
                    {
                    header: "Producto",
                    align: 'center',
                    dataIndex: 'nombreProducto',
                    hideable: true,
                    sortable: true,
                    width:120,
                    renderer: function(value) {
                         return '<span data-qtitle="Documentos" data-qwidth="200" '+
                             'data-qtip="'+value+'">'+
                             value+'<\/span>';
                     }
                }
                ,{
                  header: "Versi�n del<br/>documento",
                  dataIndex: 'version',
                  width: 80,
                  align: 'center',
                  sortable: true,
                  hideable: true,
                  renderer: function(value) {
                     return "V"+value;
                  }
            },
            {
                header: "Documentos a<br/> formalizar",
                menuText: "Documentos a<br/> formalizar",
                xtype:'actioncolumn',
                align: 'center',
                hideable: true,
                sortable: true,
                width:90,
                items: [{
                    iconCls: 'page_paintbrush',
                    tooltip: 'Ver Documento',
                    handler: function(grid, rowIndex, colIndex) {
                        pnl.mask("Descargando archivo...");
                        var record = grid.getStore().getAt(rowIndex);
                        Ext.Ajax.request({
                            url: '41validacionExt.data.jsp',
                            params: Ext.apply(  {
                                informacion: 'getDocFile',
                                icArchivo: record.get('icArchivo')
                            }),
                        callback: descargaArchivos
                        });
                    }
                }]
            }
            ,{
              header: "Firmantes IF<br/>requeridos",
              dataIndex: 'firmasRequeridas',
              align: 'center',
              width: 100,
              sortable: true,
              hideable: true,
              renderer: function(value) {
                return value == "0" ? SIN_PARAMETRIZAR : value;
              }
             }
            ,{
              header: "Acciones",
              menuText: "Acciones",
              xtype:'actioncolumn',
              dataIndex: 'icDocumento',
              align: 'center',
              width: 80,
              sortable: true,
              hideable: true,
                items: [{
                    iconCls: 'correcto',
                    tooltip: 'Validar Documento',
                    border: '10 10 10 10',
                    handler: function (grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        procesarConfirmar(record);
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        return (record.get('icEstatus') != VALIDACION_JURIDICO);
                    }
                }, {
                    iconCls: 'icoRechazar marginL15p',
                    tooltip: 'Rechazar Documento',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        fnRechazarSolicitud(rec);
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        return (record.get('icEstatus') != VALIDACION_JURIDICO);
                    }
                }]
             }
            ,{
              header: "Estatus",
              dataIndex: 'nombreEstatus',
              align: 'center',
              width: 130,
              sortable: true,
              hideable: true
             }
       ]
});


//----------************* CATALOGOS  *************----------//
	var  storeFolioSolicitud = Ext.create('Ext.data.Store', {
            model: 'ModelCatalogos',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: '41validacionExt.data.jsp',
                reader: {
                    type: 'json',
                    root: 'registros'
                },
                extraParams: {
                    informacion: 'catFolios'
                },
                listeners: {
                    exception: NE.util.mostrarProxyAjaxError
                }
            }
	});

    	var  storeCatDocumentos = Ext.create('Ext.data.Store', {
            model: 'ModelCatalogos',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: '41validacionExt.data.jsp',
                reader: {
                    type: 'json',
                    root: 'registros'
                },
                extraParams: {
                    informacion: 'catDocumentos'
                },
                listeners: {
                    exception: NE.util.mostrarProxyAjaxError
                }
            }
	});

    var  storeProducto = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41validacionExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catProductos'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var  storeCatEstatus = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41validacionExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEstatus'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });


    var  storeEjecutivos = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: false, // MODIFY
        proxy: {
            type: 'ajax',
            url: '41validacionExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEjecutivos'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var  storeIntermediarios = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41validacionExt.data.jsp',
            reader: {
                    type: 'json',
                    root: 'registros'
            },
            extraParams: {
                    informacion: 'catIFs'
            },
            listeners: {
                    exception: NE.util.mostrarProxyAjaxError
            }
        }
    });




var error1Fechas = "Debes capturar la fecha inicial y final";
var error2Fechas = "La fecha final debe ser posterior a la inicial";

var validarFechas =  function (val) {
    var fechaUno = Ext.getCmp('fechaLiberacion1');
    var fechaDos = Ext.getCmp('fechaLiberacion2');
    var fechaUnoVal = fechaUno.getValue();
    var fechaDosVal = fechaDos.getValue();
    if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)){
        fechaUno.markInvalid(error1Fechas);
        fechaDos.markInvalid(error1Fechas);
        return error1Fechas;
    }
    if(fechaUno != null && fechaUno > fechaDos){
        fechaUno.markInvalid(error2Fechas);
        fechaDos.markInvalid(error2Fechas);
        fechaDos.focus();
        return error2Fechas;
    }
    return true;
}

//----------*************  PANEL BUSQUEDA  *************----------//
var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        width: 943,
        height: 'auto',
        title: 'Validacion',
        layout: 'column',
        items: [
            {
                xtype: 'fieldset',
                height: 150,
                border:false,
                columnWidth: 0.5,
                items: [
                        {
                            xtype: 'combo',
                            name: 'folio',
                            id: 'folio',
                            fieldLabel: 'Folio de Solicitud',
                            padding: 5,
                            width: 400,
                            forceSelection: true,
                            queryMode: 'local',
                            allowBlank: true,
                            emptyText: 'Todos los Folios',
                            store: storeFolioSolicitud,
                            displayField: 'descripcion',
                            valueField: 'clave'
                        },
                        {
                            xtype: 'combo',
                            id: 'cbTipoDocumento',
                            name: 'cbTipoDocumento',
                            fieldLabel: 'Tipo de Documento',
                            padding: 5,
                            width: 400,
                            forceSelection: true,
                            allowBlank: true,
                            emptyText: 'Todos los Documentos',
                            store: storeCatDocumentos, displayField: 'descripcion', valueField: 'clave'
                        },
                        {
                            xtype: 'combo',
                            id: 'cbProducto',
                            name: 'cbProducto',
                            fieldLabel: 'Producto',
                            padding: 5,
                            width: 400,
                            forceSelection: true,
                            allowBlank: true,
                            emptyText: 'Todos los Productos/Empresas',
                            store: storeProducto, displayField: 'descripcion', valueField: 'clave'
                        },
                        {
                            xtype: 'combo',
                            id: 'estatus',
                            name: 'estatus',
                            fieldLabel: 'Estatus de la Solicitud:',
                            padding: 5,
                            width: 400,
                            store: storeCatEstatus,
                            emptyText: 'Todos los Estatus',
                            displayField: 'descripcion',
                            valueField: 'clave',
                            forceSelection: true,
                            allowBlank: true
                        }
                    ]
            },
            {
                xtype: 'fieldset',
                height: 150,
                border:false,
                columnWidth: 0.5,
                margins: '0 20 0 0',
			items: [
                    {
                        xtype: 'combo',
                        id: 'usuarioEjecutivo',
                        name: 'usuarioEjecutivo',
                        fieldLabel: 'Ejecutivo de Atenci�n',
                        padding: 5,
                        width: 440,
                        forceSelection: true,
                        queryMode: 'local',
                        allowBlank: true,
                        store: storeEjecutivos,
                        emptyText: 'Todos los Ejecutivos',
                        displayField: 'descripcion',
                        valueField: 'clave'
                    },
                    {
                        xtype: 'combo',
                        id: 'intermediario',
                        name: 'intermediario',
                        fieldLabel: 'Intermediariio Financiero:',
                        padding: 5,
                        width: 440,
                        store: storeIntermediarios,
                        forceSelection: true,
                        queryMode: 'local',
                        allowBlank: true,
                        emptyText: 'Todos los Intermediarios',
                        displayField: 'descripcion',
                        valueField: 'clave'
                    },
                        {
                        xtype:'fieldset',
                        collapsible:false,
                        border: false,
                        defaultType: 'textfield',
                        defaults: {anchor: '100%'},
                        padding: 0,
                        layout: 'hbox',
                        items :[
                                {
                                    xtype: 'datefield',
                                    id : 'fechaLiberacion1',
                                    name: 'fechaLiberacion1',
                                    fieldLabel: 'Fecha de Recepci�n',
                                    padding: 5,
                                    width: 215,
                                    emptyText: 'Desde',
                                    displayField: 'descripcion',
                                    valueField: 'clave',
                                    forceSelection: true,
                                    allowBlank: true
                                },
                                {
                                    xtype: 'datefield',
                                    id : 'fechaLiberacion2',
                                    name: 'fechaLiberacion2',
                                    fieldLabel: 'al',
                                    labelWidth: 55,
                                    padding: 10,
                                    width: 170,
                                    emptyText: 'Hasta',
                                    valueField: 'clave',
                                    forceSelection: true,
                                    allowBlank: true
                                }
                            ]
                        }
                ]
            }
        ],
        buttons: [
        {
            text: 'Buscar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarFechas();
                if(resultado != true){
                    Ext.MessageBox.alert('Aviso',resultado);
                }
                else{
                gridResultados.el.mask('Cargando...', 'x-mask-loading');
                gridStore.load({
                    informacion:  'HomeSolicitudes',
                    params: Ext.apply(formBusqueda.getForm().getValues())
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                Ext.getCmp('folio').setValue('');
                Ext.getCmp('cbTipoDocumento').setValue('');
                Ext.getCmp('cbProducto').setValue('');
                Ext.getCmp('estatus').setValue('');
                Ext.getCmp('usuarioEjecutivo').setValue('');
                Ext.getCmp('intermediario').setValue('');
                Ext.getCmp('fechaLiberacion1').setValue('');
                Ext.getCmp('fechaLiberacion2').setValue('');
            }
        }]
    });






//----------*************  CONTENEDOR PRINCIPAL  *************----------//

	var pnl = new Ext.Container({
            id: 'contenedorPrincipal',
            renderTo: 'areaContenido',
            width: 949,
            style: 'margin:0 auto;',
            items: [
                NE.util.getEspaciador(20),
                formBusqueda,
                NE.util.getEspaciador(30),
                gridCifrasControl,
                NE.util.getEspaciador(10),
                gridResultados,
                NE.util.getEspaciador(20)]
	});



});