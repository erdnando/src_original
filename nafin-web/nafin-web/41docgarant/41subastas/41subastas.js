Ext.onReady(function() {

    var POR_ASIGNAR = 'Por asignar';

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        Ext.getCmp('contenedorPrincipal').unmask();
        if (Ext.getCmp('winDoc')) {
            Ext.getCmp('winDoc').unmask();
        }
    }
    
      function descargarResultados(record){
        gridSeguimiento.mask("Descargando resultados...");
        Ext.Ajax.request({
            url: '41subastasExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getDocFile',
                icArchivo: record.get('idArchivoRes'),
                icIF:  record.get('idIntermediario'),
                icCov: record.get('icConvocatoria')
            }),
        callback: descargaArchivosRes
        });   
    }
    
     function descargaArchivosRes(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        gridSeguimiento.unmask();
    }
    
    function mostrarVentanaDocumentos(icConvocatoria, idIntermediario){
        storeArchivos.removeAll(true);        
        Ext.Ajax.request({
            url: '41subastasExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'fileSol',
                icConvocatoria: icConvocatoria,
                idIntermediario: idIntermediario
            }),
            callback:  function(opts, success, response) {
                var info = Ext.JSON.decode(response.responseText);
                if (success == true && info.success == true ) {
                    storeSeguimiento.reload();
                    var registros = info.registros;
                    for(i=0; i<registros.length; i++){
                        storeArchivos.add({'clave': registros[i].clave ,'descripcion': registros[i].descripcion});                   
                    }
                }
                procesarCargaNombresArchivos();
                Ext.getCmp('contenedorPrincipal').unmask();                            
            }
        });
    }
   

    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        }]
    });

    function procesarCargaNombresArchivos() {
                var winDoc = Ext.create('Ext.window.Window', {
                    title: 'Documentos de Posturas Recibidas',
                    id: 'winDoc',
                    height: 200,
                    width: 500,
                    layout: 'fit',
                    modal: true,
                    items: {
                        xtype: 'grid',
                        border: false,
                        enableColumnMove: false,
                        enableColumnResize: true,
                        enableColumnHide: false,
                        columns: [{
                            header: 'Nombre de archivo',
                            dataIndex: 'descripcion',
                            width: 288,
                            sortable: false
                        }, {
                            xtype: 'actioncolumn',
                            header: "Documento",
                            align: 'center',
                            sortable: false,
                            width: 200,
                            items: [{
                                iconCls: 'icoLupa',
                                tooltip: 'Ver Documento',
                                handler: function(grid, rowIndex, colIndex) {
                                    winDoc.mask("Descargando archivo...")
                                    var record = grid.getStore().getAt(rowIndex);
                                    Ext.Ajax.request({
                                        url: '41subastasExt.data.jsp',
                                        params: Ext.apply({
                                            informacion: 'getDocFile',
                                            icArchivo: record.get('clave')
                                        }),
                                        callback: descargaArchivos
                                    });
                                }
                            }]
                        }],
                        store: storeArchivos
                    }
                }).show();         
    }

    var storeArchivos = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        proxy: {
            type: 'ajax',
            url: '41subastasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'fileSol'
            }
        },
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarCargaNombresArchivos
        }
    });


    //----------*************  GRID  *************----------//
    Ext.define('ConsultaSubastasDataModel', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'icConvocatoria',
            mapping: 'icConvocatoria'
        }, {
            name: 'fechaAlta',
            mapping: 'fechaAlta'
        }, {
            name: 'nombreSubasta',
            mapping: 'nombreSubasta'
        }, {
            name: 'tipoSubasta',
            mapping: 'tipoSubasta'
        }, {
            name: 'fechaRecepResultados',
            mapping: 'fechaRecepResultados'
        }, {
            name: 'estatus',
            mapping: 'estatus'
        }, {
            name: 'posturasRecibidas',
            mapping: 'posturasRecibidas'
        },
        {
            name: 'idArchivo',
            mapping: 'idArchivo'
        } 
        ]
    });
    
    
     //----------*************  GRID  *************----------//
    Ext.define('ConsultaSeguimientoDataModel', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'icConvocatoria',
            mapping: 'icConvocatoria'
        }, {
            name: 'fechaAlta',
            mapping: 'fechaAlta'
        }, {
            name: 'intermediarioFin',
            mapping: 'intermediarioFin'
        }, {
            name: 'fechaRecepResultados',
            mapping: 'fechaRecepResultados'
        }, {
            name: 'posturasRecibidas',
            mapping: 'posturasRecibidas'
        }, {
            name: 'usuarioIntFin',
            mapping: 'usuarioIntFin'
        }, {
            name: 'fechaLimite',
            mapping: 'fechaLimite'
        }, {
            name: 'fechaEnvioRes',
            mapping: 'fechaEnvioRes'
        }, {
            name: 'estatus',
            mapping: 'estatus'
        },
        {
            name: 'nombreSubasta',
            mapping: 'nombreSubasta'
        },
         {name: 'icArchivoCa', mapping: 'icArchivoCa'},
         {name: 'idIntermediario', mapping: 'idIntermediario'},
         {name: 'idArchivoRes', mapping: 'idArchivoRes'},
         {name: 'idArchivo', mapping: 'idArchivo'}
        
        ]
    });

    var procesarConsultaSolicitudes = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'No existe informaci�n con los criterios determinados');
            }
        }
        gridSubastas.el.unmask();
    }
    
     var procesarConsultaSeguimiento = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'No existe informaci�n con los criterios determinados');
            }
        }
        if (gridSeguimiento.el){
            gridSeguimiento.el.unmask();
        }
    }
    
    var procesaCargaArchivo = function (opts, success, action) {
        Ext.MessageBox.alert('Carga Base de Operaciones', 'Se han guardado los cambios correctamente');
        Ext.getCmp('winFileUpload').close();
        storeSeguimiento.reload();
    }
    
     var procesaCargaArchivoSub = function (opts, success, action) {
        Ext.MessageBox.alert('Carga Base de Operaciones', 'Se han guardado los cambios correctamente');
        Ext.getCmp('winFileUpload').close();
        storeSubastas.reload();
    }
    
     var procesaCargaFechaAcla = function (opts, success, action) {
        Ext.MessageBox.alert('Carga de fecha de aclaracion', 'Se han guardado los cambios correctamente');
        Ext.getCmp('causaRec').close();
        storeSeguimiento.reload();        
    }
    
    var cargaArchivoBO = function (estado, respuesta, icConv, tipo) {
        var formaFile = Ext.getCmp('formFile');
        formaFile.getForm().submit({
            clientValidation: false,
            url: '41cargaSubExtArchivo.jsp',
            params: Ext.apply({
                informacion: 'loadfileIF',
                icIFFile : respuesta,
                icConv : icConv,
                tipo : tipo
            }),
            waitMsg: 'Enviando datos...',
            waitTitle: 'Cargando archivo',
            success: function (form, action) {
                procesaCargaArchivo(null, true, action);
            },
            failure: NE.util.mostrarSubmitError
        });
    };
    
     var cargaArchivoSub = function (estado, icArchivo, icConv) {
        var formaFile = Ext.getCmp('formFile');
        formaFile.getForm().submit({
            clientValidation: false,
            url: '41cargaSubExtArchivo.jsp',
            params: Ext.apply({
                informacion: 'loadfile',
                icArchivo : icArchivo,
                icConv : icConv
            }),
            waitMsg: 'Enviando datos...',
            waitTitle: 'Cargando archivo',
            success: function (form, action) {
                procesaCargaArchivoSub(null, true, action);
            },
            failure: NE.util.mostrarSubmitError
        });
    };
    
    
    var cargarArchivo = function (record) {
        var icArchivo = record.get('idArchivo');
        var icConv = record.get('icConvocatoria');
        var titulo = 'Cargar Archivo Subasta';
        Ext.create('Ext.window.Window', {
            title: titulo,
            id: 'winFileUpload',
            closable: true,
            closeAction: 'destroy',
            width: 450,
            minWidth: 250,
            height: 100,
            animCollapse: false,
            border: false,
            modal: true,
            layout: {
                type: 'border',
                padding: 0
            },
            items: [{
                xtype: 'form',
                id: 'formFile',
                items: [{
                    xtype: 'filefield',
                    padding: '20 20 20 20',
                    fieldLabel: 'Carga Archivo',
                    width: 398,
                    //vtype: 'archivoPdf',
                    listeners: {
                        change: function (fld, value) {
                            fld.setRawValue(value.substring(value.lastIndexOf("\\") + 1));
                            if (fld.isValid()) {
                                cargaArchivoSub('CARGAR_ARCHIVO', icArchivo,icConv);
                            }
                        }
                    }
                }]
            }]
        }).show();
    };    
    
    
    
     var cargarCa = function (record, tipo) {
        var icIF = record.get('idIntermediario');
        var icDoc = record.get('icDocumento');
        var icConv = record.get('icConvocatoria');
        var titulo = tipo=="P"?'Cargar Calificacion Postura':'Cargar Resultados';
        Ext.create('Ext.window.Window', {
            title: titulo,
            id: 'winFileUpload',
            closable: true,
            closeAction: 'destroy',
            width: 450,
            minWidth: 250,
            height: 100,
            animCollapse: false,
            border: false,
            modal: true,
            layout: {
                type: 'border',
                padding: 0
            },
            items: [{
                xtype: 'form',
                id: 'formFile',
                items: [{
                    xtype: 'filefield',
                    padding: '20 20 20 20',
                    fieldLabel: 'Carga Archivo',
                    width: 398,
                    //vtype: 'archivoPdf',
                    listeners: {
                        change: function (fld, value) {
                            fld.setRawValue(value.substring(value.lastIndexOf("\\") + 1));
                            if (fld.isValid()) {
                                cargaArchivoBO('CARGAR_ARCHIVO', icIF,icConv, tipo);
                            }
                        }
                    }
                }, {
                    xtype: 'hiddenfield',
                    id: 'icIF',
                    name: 'icIF',
                    value: icIF
                }, {
                    xtype: 'hiddenfield',
                    id: 'icDoc',
                    name: 'icDoc',
                    value: icDoc
                }]
            }]
        }).show();
    };    
    
    
    
     var cargarCanc = function (record) {
        var icIF = record.get('idIntermediario');
        var icConv = record.get('icConvocatoria');
        Ext.create('Ext.window.Window', {
            title: 'Causas de rechazo',
            id: 'causaRec',
            closable: true,
            closeAction: 'destroy',
            width: 450,
            minWidth: 250,
            height: 200,
            animCollapse: false,
            border: false,
            modal: true,
            layout: {
                type: 'border',
                padding: 0
            },
            items: [{
                xtype: 'form',
                id: 'formCausa',
                items: [{
                    xtype     : 'textareafield',
                    grow     : true,
                    name     : 'causa',
                    id       : 'causa',
                    width: 420,
                    fieldLabel: 'Indicar causa del rechazo',                    
                    anchor    : '100%',
                    allowBlank: false
                }, {
                    xtype: 'datefield',
                    id: 'fechaAcla',
                    name: 'fechaAcla',
                    fieldLabel: 'Fecha Limite para solventar aclaraciones: ',
                    padding: 5,
                    width: 250,
                    emptyText: 'De',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: false
                },
                {
                    xtype: 'hiddenfield',
                    id: 'icIF',
                    name: 'icIF',
                    value: icIF
                }, {
                    xtype: 'hiddenfield',
                    id: 'icConv',
                    name: 'icConv',
                    value: icConv
                }
                ],                
        buttons: [      
        {
            text: 'Guardar',
            id: 'btnGuardar',
            iconCls: 'icoGuardar',
            handler: function (boton, evento) {
                var panel = this.up('form');
                procesarGuardarfecha(panel);
            }
        }
        ]
            }]
        }).show();
    };
    
    
    
    function eliminarRes(record){
        var icIF = record.get('idIntermediario');
        var icConv = record.get('icConvocatoria');
        var icArchR = record.get('idArchivoRes');
        var nombreIF = record.get('intermediarioFin');
        Ext.Msg.confirm("Base de Operaciones", "�Est� seguro que desea eliminar el archivo de Resultados de <strong>"+ nombreIF+"</strong>?", function(btnText){
                if(btnText === "yes"){    
                    Ext.Ajax.request({
                        url: '41subastasExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'eliminaRes',
                            icIF:  icIF,
                            icConv: icConv,
                            icArchR: icArchR
                        }),
                    callback: function (opts, success, response) {
                        var info = Ext.JSON.decode(response.responseText);
                        if (success == true && info.success == true) {
                            Ext.MessageBox.alert('Eliminar', 'Se ha borrado el archivo correctamente');
                            storeSeguimiento.reload();
                        }
                    }
                    });   
                }
        });
    }
    
    
     function descargarConvocatoria(record){
        gridSubastas.mask("Descargando base...");
        Ext.Ajax.request({
            url: '41subastasExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getDocFile',
                icArchivo: record.get('idArchivo'),
                icIF:  record.get('idIntermediario'),
                icCov: record.get('icConvocatoria')
            }),
        callback: descargaArchivosRes
        });   
    }
    
     function descargaArchivosRes(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        gridSubastas.unmask();
    }

    
    
    var procesarGuardarfecha = function(panel) {
    var causa = Ext.getCmp('causa').getValue();
    var fechaAcla    = Ext.getCmp('fechaAcla').getValue();
    var fechaR = Ext.util.Format.date(fechaAcla, 'd/m/Y');
    var icIF = Ext.getCmp('fechaAcla').getValue();
    var icConv = Ext.getCmp('icConv').getValue();
    var formCausa = Ext.getCmp('formCausa');
    if (!Ext.getCmp('formCausa').isValid()){
        fieldNames = [];                
        fields = panel.getInvalidFields();
        for(var i=0; i <  fields.length; i++){
            field = fields[i];
            fieldNames.push(field.getName());
         }    
        Ext.Msg.alert('Guardar', '�Debe Capturar Capturar todos los campos marcados!');
        return;
    }
    
        formCausa.getForm().submit({
            clientValidation: false,
            url: '41subastasExt.data.jsp',
            params: Ext.apply({
                informacion: 'causaRec',
                causa : causa,
                fechaAcla : fechaR,
                icIF    : icIF,
                icConv  : icConv
            }),
            waitMsg: 'Enviando datos...',
            waitTitle: 'Cargando Causa',
            success: function (form, action) {
                procesaCargaFechaAcla(null, true, action);
            },
            failure: NE.util.mostrarSubmitError
        });
}

    
    var storeSubastas = Ext.create('Ext.data.Store', {
        model: 'ConsultaSubastasDataModel',
        proxy: {
            type: 'ajax',
            url: '41subastasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'consultaSubastas'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsultaSolicitudes
        }
    });


  var storeSeguimiento = Ext.create('Ext.data.Store', {
        model: 'ConsultaSeguimientoDataModel',
        groupField: 'nombreSubasta',
        proxy: {
            type: 'ajax',
            url: '41subastasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'consultaSeguimiento'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsultaSeguimiento
        }
    });




/* * * * * * * * * * * * * * * * * * GRID DE RESULTADOS SOLICITUDES * * * * * * * * * * * * * * * * * * * * * * * */
    var gridSubastas = Ext.create('Ext.grid.Panel', {
        id: 'd_gridId',
        store: storeSubastas,
        stripeRows: true,
        title: 'Home de Subastas',
        width: 944,
        height: 400,
        frame: true,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [
        {
                header: "Convocatoria",
                dataIndex: 'icConvocatoria',
                align: 'center',
                width: 80,
                sortable: true,
                hideable: true,
                tdCls: 'cursorPointer'
            },         
        {
                header: "Fecha de<br/>Solicitud",
                dataIndex: 'fechaAlta',
                align: 'center',
                width: 90,
                sortable: true,
                hideable: true
            }, {
                header: "Nombre<br/>de subasta",
                dataIndex: 'nombreSubasta',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Subasta" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }     
            }, 
            {
                header: "Tipo de<br/>Subasta",
                dataIndex: 'tipoSubasta',
                align: 'left',
                width: 170,
                sortable: true,
                hideable: true 
            },     
            {
                header: "Fecha l�mite<br/>recepci�n de<br/>posturas",
                dataIndex: 'fechaRecepResultados',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true
            },
          {
            header: "Acciones",
            menuText: "Acciones",
            dataIndex: 'icConvocatoria',
            xtype: 'actioncolumn',
            width: 100,
            align: 'center',
            sortable: true,
            hideable: true,
            items: [{
                iconCls: 'modificar marginR10p',
                tooltip: 'Modifica Subasta',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    verSubasta(record.get('icConvocatoria'));
                }
            }, {
                iconCls: 'borrar',
                tooltip: 'Borrar Subasta',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    borrarSubasta(record.get('icConvocatoria'));
                }
                }]
        },
            {
                header: "Bases<br/>Publicadas",
                menuText: "Bases<br/>publicadas",
                xtype: 'actioncolumn',
                align: 'center',
                dataIndex: 'icConvocatoria',
                hideable: true,
                sortable: true,
                width: 80,
                items: [ {
                iconCls: 'icoLupa',
                tooltip: 'Visualizar Convocatoria',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    descargarConvocatoria(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('idArchivo') == 0;
                }
               }
                ]
            },
            
            {
                header: "Carga de<br/>Documentos<br/>Soporte",
                menuText: "Carga<br/>Documentos",
                xtype: 'actioncolumn',
                align: 'center',
                dataIndex: 'icConvocatoria',
                hideable: true,
                sortable: true,
                width: 80,
                items: [{
                iconCls: 'icon-register-add',
                tooltip: 'Cargar documento de Soporte',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    Ext.Msg.confirm("Cargar base de operacion", "�Est� seguro que desea cargar o actualizar el archivo de Subastas?", function(btnText){
                    if(btnText === "yes"){    
                            cargarArchivo(record);   
                    }
                    }); 
                 }                
                }
                ]
            },
            
            {
                header: "Posturas Recibidas",
                dataIndex: 'posturasRecibidas',
                width: 90,
                align: 'center',
                sortable: true,
                hideable: true
            },
            {
                header: "Estatus",
                dataIndex: 'estatus',
                align: 'left',
                width: 80,
                sortable: true,
                hideable: true
            }
        ]
    });

    //----------************* CATALOGOS  *************----------//

  
    
    var sstoreSubastas = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41subastasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catSubastas'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError,
                'load': function(store, records, options) {
                    var record = new store.recordType({
                        id: 0,
                        title: '-- ALL --'
                    });
                    store.insert(0, record);
                    }
                
            }
        }
    });


  

    var sstoreIntermediario = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41subastasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catIF'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
    
 
    

   var catEstatus = new Ext.data.ArrayStore({
		id: 'catEstatus',
		fields : ['CLAVE', 'DESCRIPCION'],
    data:[['En Proceso','En Proceso'],['Concluida', 'Concluida']]
	});


   
    
    var verSubasta = function (icConvocatoria) {
        window.location = '41altaSubastasExt.jsp?idMenu=41ALTASOLICITUD&icConvocatoria=' + icConvocatoria;
    }
    
    var borrarSubasta = function (icConvocatoria) {
        window.location = '41altaSubastasExt.jsp?idMenu=41ALTASOLICITUD&icConvocatoria=' + icConvocatoria;
    }
    
    

    /**********************  PANEL BUSQUEDA Subastas *************************/
    var formBusquedaSub = new Ext.form.FormPanel({
        name: 'formBusquedaSub',
        id: 'formBusquedaSub',
        width: 943,
        height: 'auto',
        border: 0,
        frame: true,
        layout: {
        type: 'vbox',
        align: 'center',
        pack: 'center'
        },
        style: {
            "margin-left": "auto",
            "margin-right": "auto"
       }, 
        items: [{
            xtype: 'fieldset',
            id: 's_fieldsBusqueda',
            height: 180,
            border: false,
            columnWidth: 1,
            items: [ 
            {
                               
                    xtype: 'textfield',
                    id: 'nombreSubasta',
                    name: 'nombreSubasta',
                    fieldLabel: 'Nombre de </br>Subasta',
                    padding: 5,
                    width: 400,
                    forceSelection: true,
                    allowBlank: false,
                    labelStyle: 'width:200px'
                    //, labelWidth: '30%'            
                }, {
                    xtype: 'combo',
                    name: 'tipoSubasta',
                    id: 'tipoSubasta',
                    fieldLabel: 'Tipo Subasta',
                    padding: 5,
                    width: 400,
                    forceSelection: true,
                    queryMode: 'local',
                    allowBlank: true,
                    emptyText: 'Todos los Tipos',
                    store: sstoreSubastas,
                    displayField: 'descripcion',
                    valueField: 'clave',
                    triggerAction: 'all',
                    editable: true
                
            }, 
            {
                xtype: 'combo',
                name: 'estatusSubasta',
                id: 'estatusSubasta',
                fieldLabel: 'Estatus Subasta',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Estatus',
                store: catEstatus,
                displayField: 'DESCRIPCION',
                valueField: 'CLAVE',
                triggerAction: 'all',
                editable: true
            },            
            {
                xtype: 'datefield',
                id: 'fechaAlta',
                name: 'fechaAlta',
                fieldLabel: 'Fecha de Alta: ',
                padding: 5,
                width: 215,
                emptyText: 'De',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                allowBlank: true
            },            
            {
                xtype: 'datefield',
                id: 'fechaLimite',
                name: 'fechaLimite',
                fieldLabel: 'Fecha Limite para Recibir Posturas: ',
                padding: 5,
                width: 215,
                emptyText: 'De',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                allowBlank: true
            }            
        ]
        }],
        buttons: [
        {
            text: 'Nueva',
            id: 's_btnNueva',
            iconCls: 'icoAgregar',
            handler: function() {
                    window.location = '41altaSubastasExt.jsp?idMenu=41ALTASOLICITUD';
                }
        },        
        {
            text: 'Consultar',
            id: 's_btnBuscar',
            iconCls: 'icoBuscar',
            handler: function(boton, evento) {
                    gridSubastas.el.mask('Cargando...', 'x-mask-loading');
                    storeSubastas.load({
                        params: Ext.apply(formBusquedaSub.getForm().getValues())
                    });
                
            }
        }, {
            id: 's_btnLimpiar',
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function() {
                Ext.getCmp('formBusquedaSub').getForm().reset();
            }
        }]
    });



    var panelSolicitudes = new Ext.Container({
        id: 'panelSolicitudes',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            formBusquedaSub,
            NE.util.getEspaciador(30),
            gridSubastas           
        ]
    });
    
    
 
    /**********************  PANEL BUSQUEDA Seguimiento *************************/
    var formBusquedaSeg = new Ext.form.FormPanel({
        name: 'formBusquedaSeg',
        id: 'formBusquedaSeg',
        width: 943,
        height: 150,
        frame: true,
        border: 0,
        layout: 'column',
        items: [{
            xtype: 'fieldset',
            id: 's_fieldsSeguimiento',
            height: 150,
            border: false,
            columnWidth: 0.5,
            items: [ 
            {   
                    xtype: 'textfield',
                    id: 'nombreSubastaSeg',
                    name: 'nombreSubasta',
                    fieldLabel: 'Nombre de <\/br>Subasta',
                    padding: 5,
                    width: 400,
                    forceSelection: true,
                    allowBlank: false,
                    labelStyle: 'width:200px'
                    //, labelWidth: '30%'            
                }, {
                    xtype: 'combo',
                    name: 'tipoSubasta',
                    id: 'tipoSubastaSeg',
                    fieldLabel: 'Tipo Subasta',
                    padding: 5,
                    width: 400,
                    forceSelection: true,
                    queryMode: 'local',
                    allowBlank: true,
                    emptyText: 'Todos los Tipos',
                    store: sstoreSubastas,
                    displayField: 'descripcion',
                    valueField: 'clave',
                    triggerAction: 'all',
                    editable: false
                
            }
            ]
             },{
            xtype: 'fieldset',
            height: 150,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [
              {
                xtype: 'combo',
                id: 'd_intermediario',
                name: 'd_intermediario',
                fieldLabel: 'Intermediario Financiero:',
                padding: 5,
                width: 400,
                store: sstoreIntermediario,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Intermediarios',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                name: 'estatusSubasta',
                id: 'estatusSubastaSeg',
                fieldLabel: 'Estatus Subasta',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Estatus',
                store: catEstatus,
                displayField: 'DESCRIPCION',
                valueField: 'CLAVE',
                triggerAction: 'all',
                editable: false
            }            
                        
        ]
        }],
        buttons: [
        {
            text: 'Consultar',
            id: 's_btnBuscarSeg',
            iconCls: 'icoBuscar',
            handler: function(boton, evento) {
                    gridSeguimiento.el.mask('Cargando...', 'x-mask-loading');
                    storeSeguimiento.load({
                        params: Ext.apply(formBusquedaSeg.getForm().getValues())
                    });
                
            }
        }, {
            id: 's_btnLimpiarSeg',
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function() {
                Ext.getCmp('formBusquedaSeg').getForm().reset();
            }
        }]
    });

    /* * * * * * * * * * * * * * * * * * GRID DE RESULTADOS SOLICITUDES * * * * * * * * * * * * * * * * * * * * * * * */
    var gridSeguimiento = Ext.create('Ext.grid.Panel', {
        id: 'd_gridIdSeg',
        store: storeSeguimiento,
        stripeRows: true,
        title: 'Seguimiento',
        width: 944,
        height: 400,
        collapsible: false,
        frame: true,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [
        {
                header: "Fecha de</br>Envio de</br>Bases",
                dataIndex: 'fechaAlta',
                align: 'center',
                width: 80,
                sortable: true,
                hideable: true,
                tdCls: 'cursorPointer'
            },         
        {
                header: "Intermediario</br>Financiero",
                dataIndex: 'intermediarioFin',
                align: 'center',
                width: 90,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Intermediario" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }          
            }, {
                header: "Fecha limite<br/>para entregas</br>de Posturas",
                dataIndex: 'fechaRecepResultados',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true
            }, 
            {
            header: "Posturas</br>Recibidas",
            menuText: "Posturas",
            dataIndex: 'icConvocatoria',
            xtype: 'actioncolumn',
            width: 100,
            align: 'center',
            sortable: true,
            hideable: true,
            items: [
                
                {
                iconCls: 'icoLupa',
                tooltip: 'Visualizar Posturas',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    mostrarVentanaDocumentos(record.get('icConvocatoria'),record.get('idIntermediario'));
                    //descargarResultados(record);
                }
                /*,
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('idArchivoRes') == 0;
                }*/
               }
                
                ]
        },     
            {
                header: "Usuario<br/>Intermediario",
                dataIndex: 'usuarioIntFin',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Usuario Intermediario" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }          
            },
          {
            header: "Emisi�n<\/br>Calificacion<\/br>Posturas",
            menuText: "Acciones",
            dataIndex: 'icConvocatoria',
            xtype: 'actioncolumn',
            width: 100,
            align: 'center',
            sortable: true,
            hideable: true,
            items: [
                {
                iconCls: 'icon-register-add marginR10p',
                tooltip: 'Cargar calificacion Posturas',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    cargarCa(record, "P");
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('icArchivoCa') != 0) ; 
                }
                }  
                ,
                {
                    iconCls: 'icoRechazar',
                    tooltip: 'Rechazar Postura',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        cargarCanc(rec);
                    }
                }]
        },
        {
                header: "Fecha limite<br/>para solventar</br>observaciones",
                dataIndex: 'fechaLimite',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true
            },
            
            {
            header: "Carga</br>de Resultados",
            menuText: "resultados",
            dataIndex: 'icConvocatoria',
            xtype: 'actioncolumn',
            width: 100,
            align: 'center',
            sortable: true,
            hideable: true,
            items: [
            
            {
                iconCls: 'icon-register-add',
                tooltip: 'Cargar Resultados',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    cargarCa(record, "R");
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('idArchivoRes') != 0) ; 
                }
                }
            
            , {
                iconCls: 'borrar marginR10p',
                tooltip: 'Borrar Resultados',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    eliminarRes(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('idArchivoRes') == 0) ; 
                }
                },
                
                {
                iconCls: 'icoLupa marginR10p',
                tooltip: 'Visualizar Resultados',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    descargarResultados(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('idArchivoRes') == 0;
                }
               }
                
                ]
        },
        
        {
                header: "Fecha de Envi�<br/>de Resultados",
                dataIndex: 'fechaAlta',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true
            },
            
          {
                header: "Estatus",
                dataIndex: 'estatus',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true
            }
        ],
        features: [{ftype:'grouping'}]
    });
     
     var panelSeguimiento = new Ext.Container({
        id: 'panelSeguimiento',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            formBusquedaSeg,
            NE.util.getEspaciador(30),
            gridSeguimiento           
        ]
    });
   
    
    
    
    
/*************************************** CONTENEDOR PRINCIPAL  *******************************************************/      
    
    Ext.tip.QuickTipManager.init();

    Ext.create('Ext.tab.Panel', {
        width: 945,
        id: 'contenedorPrincipal',
        minHeight: 700,
        title: 'Proceso de Subastas',
        renderTo: 'areaContenido',
        items: [{
            id: 'tabSol',
            title: 'Consulta de Subastas',
            tooltip: 'Consulta de Subastas',
            items: [panelSolicitudes]
        }
        , {
            id: 'tapSeg',
            title: 'Seguimiento',
            tooltip: 'Seguimiento',
            items: [panelSeguimiento]
        }]
    });




});