<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
        com.nafin.docgarantias.*,
	com.nafin.docgarantias.subastas.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";

String infoRegresar="";

log.info("INFORMACION: "+informacion);
Subastas subastasBean = ServiceLocator.getInstance().lookup("SubastasEJB",Subastas.class);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);
String s_intermediario = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
    
if (informacion.equals("consultaSubastas")){   
    Map<String, String> filtros = new HashMap<>();
    
    String nombreSubasta	= request.getParameter("nombreSubasta");
    String tipoSubasta          = request.getParameter("tipoSubasta");
    String estatusSubasta       = request.getParameter("estatusSubasta");
    String fechaAlta            = request.getParameter("fechaAlta");
    String fechaLimite          = request.getParameter("fechaLimite");
    
    if (nombreSubasta   != null && nombreSubasta != null && !nombreSubasta.isEmpty()) filtros.put("nombreSubasta", nombreSubasta);
    if (tipoSubasta     != null && !tipoSubasta.isEmpty()) filtros.put("tipoSubasta", tipoSubasta);
    if (estatusSubasta  != null && !estatusSubasta.isEmpty()) filtros.put("estatusSubasta",estatusSubasta);
    if (fechaAlta       != null && !fechaAlta.isEmpty()) filtros.put("fechaAlta",fechaAlta);
    if (fechaLimite     != null && !fechaLimite.isEmpty()) filtros.put("fechaLimite",fechaLimite);
    
    ArrayList<InfoSubastas> listado = subastasBean.getInfoSubastas(filtros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}

else if (informacion.equals("consultaSeguimiento")){   
    Map<String, String> filtros = new HashMap<>();
    
    String nombreSubasta	= request.getParameter("nombreSubasta");
    String tipoSubasta          = request.getParameter("tipoSubasta");
    String estatusSubasta       = request.getParameter("estatusSubasta");
    String interFinanciero            = request.getParameter("d_intermediario");
    
    if (nombreSubasta   != null && nombreSubasta != null && !nombreSubasta.isEmpty()) filtros.put("nombreSubasta", nombreSubasta);
    if (tipoSubasta     != null && !tipoSubasta.isEmpty()) filtros.put("tipoSubasta", tipoSubasta);
    if (estatusSubasta  != null && !estatusSubasta.isEmpty()) filtros.put("estatusSubasta",estatusSubasta);
    if (interFinanciero       != null && !interFinanciero.isEmpty()) filtros.put("interFinanciero",interFinanciero);
    
    ArrayList<InfoSubastas> listado = subastasBean.getInfoSeguimiento(filtros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}

else if(informacion.equals("fileSol")){
    String icConvocatoria = request.getParameter("icConvocatoria") == null ? "" :request.getParameter("icConvocatoria");
    String icIF = request.getParameter("idIntermediario") == null ? "" :request.getParameter("idIntermediario");
    System.out.println("fileSOl "+icConvocatoria+" icIF "+icIF);
    ArrayList<ElementoCatalogo> listado = subastasBean.getPosturas(icConvocatoria, icIF);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + ", \"icConvocatoria\": "+icConvocatoria +"}";
    //infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("getDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("getDocFileIF")){
   /* boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    
    try{
        Integer icArchivo = consultaSolicitudesIfBean.getArchivoIF(s_intermediario);
        //Integer icArchivo = Integer.parseInt(sicArchivo);        
        if(icArchivo != 0){
            ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
            jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        }else{
             jsonObj.put("urlArchivo","0");
             jsonObj.put("msg","El Intermediario financiero no tiene ningun archivo asignado");
             //exito = false;
        }
       exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }*/
}
else if(informacion.equals("catEjecutivotos")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatEjecutivos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catSolicitante")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatSolicitantes();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catIF")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatSolicitudesIF();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catSubastas")){
    ArrayList<ElementoCatalogo>listado = subastasBean.getCatSubastas();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catPortafolio")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catProductoEmpresa")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatProducto(Integer.valueOf(0), null);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("causaRec")){
    JSONObject jsonObj   =  new JSONObject();
    boolean exito = false;
    String icConvocatoria = request.getParameter("icConv") == null ? "" :request.getParameter("icConv");
    String icIF = request.getParameter("icIF") == null ? "" :request.getParameter("icIF");
    String causa = request.getParameter("causa") == null ? "" :request.getParameter("causa");
    String fechaAcla = request.getParameter("fechaAcla") == null ? "" :request.getParameter("fechaAcla");
    
    System.out.println("fechaAcla "+fechaAcla);
    try{        
	InfoSubastas infoSubastas = new InfoSubastas();
        infoSubastas.setIdIntermediario(icIF);
        infoSubastas.setTagComentario(causa);
        infoSubastas.setIcConvocatoria(icConvocatoria);
        infoSubastas.setFechaSolventar(fechaAcla);
        String regreso = subastasBean.updatePostura(infoSubastas, "2");
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if(informacion.equals("eliminaRes")){
    JSONObject jsonObj   =  new JSONObject();
    boolean exito = false;
    String icConvocatoria = request.getParameter("icConv") == null ? "" :request.getParameter("icConv");
    String icIF = request.getParameter("icIF") == null ? "" :request.getParameter("icIF");
    String icArchR = request.getParameter("icArchR") == null ? "0" :request.getParameter("icArchR");
    
    System.out.println("icIF "+icIF);
    try{        
	InfoSubastas infoSubastas = new InfoSubastas();
        infoSubastas.setIdIntermediario(icIF);
        infoSubastas.setIcConvocatoria(icConvocatoria);
        infoSubastas.setIdArchivoRes(Integer.parseInt(icArchR));
        exito = subastasBean.borraResultado(infoSubastas);
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}

log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>