Ext.onReady(function() {

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();		
            }else {
                NE.util.mostrarConnError(response,opts);
        }
        Ext.getCmp('contenedorPrincipal').unmask();
    }

var dataFiles = new Array(); 

var fnArchivoPdfXlsxZip = function(nombArch){
    var myRegex = /^.+\.([Pp][dD][Ff])$/;
    var myRegexZip = /^.+\.([Zz][Ii][Pp])$/;
    var myRegexXlsx = /^.+\.([Xx][Ll][Ss][Xx])$/;
    return (myRegex.test(nombArch) || myRegexZip.test(nombArch) || myRegexXlsx.test(nombArch));
};
    
Ext.apply(Ext.form.VTypes, {
    archivoPdfXlsxZip: function(v) {
        return fnArchivoPdfXlsxZip(v);
    },
    archivoPdfXlsxZipText: 'Solo se permite cargar archivos en formato PDF, XLSX y ZIP Favor de verificarlo.'
});

 function eliminaArchivo(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var icArchivo = infoR.icArchivo;				
            if(dataFiles.length == 1){
                dataFiles.pop();
            }
            else{
                var newarray = new Array();
                // copia todos los elementos menos el icArchivo enviado
                for(i=0; i<dataFiles.length; i++){
                    if(dataFiles[i] != icArchivo){
                        newarray.push(dataFiles[i]);
                    }
                }
                dataFiles = newarray;            
            }
            // elimina los botones asociados al icArchivo
            var btnV = Ext.getCmp('btnV'+icArchivo);
            btnV.hide();
            btnV.destroy()
            var btnE = Ext.getCmp('btnE'+icArchivo);
            btnE.hide();
            btnE.destroy();
            icArc = "0";
            Ext.Msg.alert('Eliminar archivo', 'Se elimino el archivo de forma exitosa');          
        }
        Ext.getCmp('contenedorPrincipal').unmask();
    }
    
function eliminarArchivoC(icArchivo) {
            if(icArchivo!=null&&icArchivo != "0" ){
            if(dataFiles.length == 1){
                dataFiles.pop();
            }
            else{
                var newarray = new Array();
                // copia todos los elementos menos el icArchivo enviado
                for(i=0; i<dataFiles.length; i++){
                    if(dataFiles[i] != icArchivo){
                        newarray.push(dataFiles[i]);
                    }
                }
                dataFiles = newarray;            
            }
            var btnV = Ext.getCmp('btnV'+icArchivo);
            btnV.hide();
            btnV.destroy()
            var btnE = Ext.getCmp('btnE'+icArchivo);
            btnE.hide();
            btnE.destroy()
        }
}
    
    /*
function eliminaArchivosSalir(opts, success, response){
    eliminaArchivos(opts, success, response);
    window.location = '41HOMESUBASTASExt.jsp?idMenu=41HOMESUBASTAS';
}    */
    
 function eliminaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            for(i=0; i<dataFiles.length; i++){
                var icArchivo = dataFiles[i];
                var btnV = Ext.getCmp('btnV'+icArchivo);
                btnV.hide();
                btnV.destroy()
                var btnE = Ext.getCmp('btnE'+icArchivo);
                btnE.hide();
                btnE.destroy()            
            }
            Ext.Msg.alert('Eliminar archivo', 'Se elimino el archivo de forma exitosa');          
        }
        dataFiles = new Array();
        Ext.getCmp('contenedorPrincipal').unmask();
    }


    /* Funcion para agregar botones en el formulario **/
    function crearBotonesArchivo(icArchivo, nombreArchivo){
        var formaBotones= Ext.getCmp('formButtonsFile');
        icArc = icArchivo;
        var btnVer = Ext.create('Ext.Button', {
                id: 'btnV'+icArchivo,
                text: nombreArchivo,
                iconCls : 'iconoLupa',
                tooltip : 'Descargar '+nombreArchivo,
                handler: function() {
                    Ext.getCmp('contenedorPrincipal').mask("Cargando archivo...");
                    Ext.Ajax.request({
                        url: '41altaSubastasExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'getDocFile',
                            icArchivo: icArchivo
                        }),
                        callback: descargaArchivos
                    });                    
                }
            });
        if(!esSoloLectura){
            var btnEliminar = Ext.create('Ext.Button', {
                id: 'btnE'+icArchivo,
                text: nombreArchivo,
                iconCls : 'borrar',
                tooltip : 'Eliminar '+nombreArchivo,
                handler: function() {
                    Ext.Msg.show({
                        title:'�Eliminar Archivo?',
                        msg: '�Estas seguro que deseas eliminar el archivo:'+nombreArchivo+'?',
                        buttons: Ext.Msg.YESNOCANCEL,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                                Ext.Ajax.request({
                                    url: '41altaSubastasExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'delDocFile',
                                        icArchivo: icArchivo
                                    }),
                                    callback: eliminaArchivo
                                });                                  
                            } 
                        }
                    });                
                }
            });
            formaBotones.add(btnEliminar);   
        }
        dataFiles.push(icArchivo);
        formaBotones.add(btnVer);
         
    }


    var procesaCargaArchivo = function(opts, success, action) {
        crearBotonesArchivo(action.result.icArchivo, action.result.nombreArchivo);
    }


	var cargaArchivo = function(estado, respuesta){
            var formaFile = Ext.getCmp('formFile');
            formaFile.getForm().submit({
                clientValidation: false,
                url: '41cargaSubExtArchivo.jsp',
                params:	{
                    informacion:'loadfile',
                    icConv: icSol,
                    icArchivo:icArc
                },
                waitMsg: 'Enviando datos...',
                waitTitle:'Cargando archivo',
                success: function(form, action) {								
                        eliminarArchivoC(icArc);
                        procesaCargaArchivo(null,  true,  action );
                }
                ,failure: NE.util.mostrarSubmitError
            });
	};

var SI = 1;
var NO = 2;

var procesarPublicar = function(panel) {
    Ext.Ajax.request({
        url: '41altaSubastasExt.data.jsp',
        params: {
            informacion : 	"publicarSubasta",
            icConv : 	icSol
        },
        callback: respuestaPublicar
    });
}

function respuestaPublicar(opts, success, response) {
    if (success == true && Ext.JSON.decode(response.responseText).success == true) {
        var hoy = new Date();
      Ext.Msg.show({
          title: 'Borrar',
          msg: '<div style="text-align:center;">Se Publico la subasta correctamente <br/> '+hoy.toLocaleString()+'<br/>',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function(btn) {
                window.location = '41subastas.jsp?idMenu=41HOMESUBASTAS'
          }
          }); 
          
    } 
    else {
        NE.util.mostrarErrorResponse4(response,opts);
    }
};

var procesarBorrar = function(panel) {
   
    Ext.Ajax.request({
        url: '41altaSubastasExt.data.jsp',
        params: {
            informacion : 	"borrarSubasta",
            icConv : 	icSol
        },
        callback: respuestaBorrar
    });
}

function respuestaBorrar(opts, success, response) {
    if (success == true && Ext.JSON.decode(response.responseText).success == true) {
        var hoy = new Date();
      Ext.Msg.show({
          title: 'Borrar',
          msg: '<div style="text-align:center;">Se borro la subasta correctamente <br/> '+hoy.toLocaleString()+'<br/>',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function(btn) {
                window.location = '41subastas.jsp?idMenu=41HOMESUBASTAS'
          }
          }); 
          
    } 
    else {
        NE.util.mostrarErrorResponse4(response,opts);
    }
};

var procesarGuardarSolicitud = function(panel) {
    
    var nombreSubasta = Ext.getCmp('nombreSubasta').getValue();
    var tipoSubasta    = Ext.getCmp('tipoSubasta');
    
    if (Ext.getCmp('radio1').getValue()){
       tipoSubasta = '800';
    }
    else{
       tipoSubasta = '700'
    }
    var fechaRecepResultados = Ext.getCmp('fechaRecepResultados');
    var fechaR = Ext.util.Format.date(fechaRecepResultados.getValue(), 'd/m/Y');
    
    
    var tipoIntermediario = Ext.getCmp('chkSeleccionarIF');
    
    if (!Ext.getCmp('formAlta').isValid()){
        fieldNames = [];                
        fields = panel.getInvalidFields();
        for(var i=0; i <  fields.length; i++){
            field = fields[i];
            fieldNames.push(field.getName());
         }    
        Ext.Msg.alert('Guardar Solicitud', '�Debe Capturar Capturar todos los campos marcados!');
        return;
    }
    var jsonEncodeListFiles = '';
    if(icSol != null){        
        jsonEncodeListFiles = Ext.encode(dataFiles);
    }
    var jsonDataEncodeList = '';
    var records = Ext.getCmp('ifSeleccionados').toField.store.getRange();
    var datar = new Array();
    for (var i = 0; i < records.length; i++) {
        datar.push(records[i].data);
    }
    jsonDataEncodeList = Ext.encode(datar);
    var xkl = icSol != null ? "upSubastas":"subastas";
    Ext.Ajax.request({
        url: '41altaSubastasExt.data.jsp',
        params: {
            informacion : 	xkl,
            icConvocatoria : 	icSol,
            tipoSubasta :       tipoSubasta, 
            nombreSubasta:      nombreSubasta,
            fechaRecepResultados :	fechaR,
            tipoIntermediario:  Ext.getCmp('chkSeleccionarIF').getValue(),
            listaIF :		jsonDataEncodeList,
            listaFiles:         jsonEncodeListFiles
        },
        callback: respuestaGuardar
    });
}

function respuestaGuardar(opts, success, response) {
    if (success == true && Ext.JSON.decode(response.responseText).success == true) {
        var folioGenerado = Ext.JSON.decode(response.responseText).folio;
        var hoy = new Date();
      Ext.Msg.show({
          title: 'Guardar',
          msg: '<div style="text-align:center;">La carga de la operaci�n fue satisfactoria <br/> '+hoy.toLocaleString()+'<br/>'+
                'Folio: '+folioGenerado+'<br/><\/div><br/> '+
                '<small>Favor de tomar nota del folio para futuras aclaraciones y consultas.<\/small>',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function(btn) {
                window.location = '41altaSubastasExt.jsp?idMenu=41ALTASOLICITUD&icConvocatoria=' + folioGenerado;
          }
          }); 
          
    } 
    else {
        NE.util.mostrarErrorResponse4(response,opts);
    }
};


//----------************* CATALOGOS  *************----------//

	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }				
		]
	});

 
	
	var storeIF = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
                autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41altaSubastasExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catIF'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});      


    var selectorIF = Ext.create("Ext.ux.form.ItemSelector", {
        id: 'ifSeleccionados',
        name:'ifSeleccionados',
        autoScroll: true,
        padding: '10 15 15 15', 
        height: 200,
        width: 800,
        fieldLabel: '',
        imagePath: '../ux/images/',
        store: storeIF,
        displayField: 'descripcion', 
        valueField: 'clave',
        allowBlank: false,
        msgTarget: 'side',
        fromTitle: 'Intermediarios Financieros Disponibles',
        toTitle: 'Intermediarios Financieros Seleccionados',
        buttons: ['add', 'remove'],
        hidden : false,
        readOnly: true
    });


var cleanForm = function(){
    Ext.getCmp('idMovimiento').setValue('');
    Ext.getCmp('idPortafoio').setValue('');
    Ext.getCmp('tipoDocumento').setValue('');
    Ext.getCmp('ifSeleccionados').setValue('');
    Ext.getCmp('nombreProducto').setValue('');
    Ext.getCmp('descripcion').setValue('');
    Ext.getCmp('idDocumentoOrigen').setValue('');
}


    function fillComentarios(opts, success, response) {
        var infoR = Ext.JSON.decode(response.responseText);
        if (success == true && infoR.success == true) {
            var actual = Ext.getCmp('descripcion').getValue();
            var arrRegistros = infoR.registros;
            var full = "";
            for (indice = 0; indice < arrRegistros.length; indice++) {
                var c = arrRegistros[indice];
                full += "Fecha: " + c.fechaRegistro + " [" + c.nombreUsuario + "]\n" + c.commentario + "\n\n";
            }
            full += "\nSolicitud inicial:\n" + actual;
            Ext.getCmp('descripcion').setValue(full);
        }
        Ext.getCmp('descripcion').setReadOnly(true);
        Ext.getCmp('descripcionComentario').setVisible(true);
        Ext.getCmp('formAlta').unmask();
        Ext.getCmp('btnComments').disable();
    }

    function mostrarTodosComentarios() {
        Ext.getCmp('formAlta').mask("Actualizando...");
        Ext.Ajax.request({
            url: '41altaSubastasExt.data.jsp',
            params: Ext.apply({
                informacion: 'getComments',
                icSolicitud: icSol
            }),
            callback: fillComentarios
        });
    }
//----------*************  PANEL DE ALTA  *************----------//


var formAlta = new Ext.form.FormPanel({
        name: 'formAlta',
        id: 'formAlta',
        width: 943,
        height: 'auto',
        frame: true,
        title: 'Alta de Subastas',
        items: [
                {
                    xtype: 'textfield',
                    name : 'nombreSubasta',
                    id : 'nombreSubasta',
                    labelStyle: 'width:200px',
                    labelWidth: '85%',
                    width: 550, 
                    readOnly : false,
                    hidden: false,
                    forceSelection: true,
                    allowBlank: false,
                    fieldLabel: 'Nombre de Subasta',
                    padding: '10 15 15 15',
                    disabled: esSoloLectura
                },
                {
                    xtype: 'radiogroup',
                    id: 'radiosSeleccionar',
                    name: 'radiosSeleccionar',
                    fieldLabel: 'Tipo de Subasta',
                    padding: '10 15 15 15',
                    width: 550,
                    labelStyle: 'width:200px',
                    labelWidth: '100%',                    
                    forceSelection: true,
                    disabled: esSoloLectura,
                    columns: 2,
                    items: [
                        {boxLabel: 'primeras p�rdidas', name: 'seleccionarTipo', id: 'radio1', inputValue:SI},
                        {boxLabel: 'pari passu', name: 'seleccionarTipo', id: 'radio2', inputValue:NO, checked: true}
                    ]
                },
                {
                xtype: 'datefield',
                id: 'fechaRecepResultados',
                name: 'fechaRecepResultados',
                fieldLabel: 'Fecha M�xima de recepci�n<\/br>de Posturas',
                padding: '10',
                width: 750,
                anchor: '61%',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                emptyText: 'Seleccione la fecha',
                allowBlank: false,
                labelWidth: '260px',
                format: 'd/m/Y',
                submitFormat: 'd/m/Y'

                },
                 {
                xtype: 'checkboxgroup',
                id: 'chkSeleccionarIF',
                name: 'chkSeleccionarIF',
                fieldLabel: 'Tipo de Intermediario',
                padding: '10 15 15 15',
                width: 550,
                labelStyle: 'width:200px',
                labelWidth: '100%',
                forceSelection: true,
                columns: 2,
                items: [{
                    boxLabel: 'Intermediarios Financieros Bancarios',
                    name: 'chkIFB',
                    id: 'chkIFB',
                    inputValue: 1,
                    checked: true
                }, {
                    boxLabel: 'Intermediarios Financieros No Bancarios',
                    name: 'chkIFB2',
                    id: 'chkIFB2',
                    inputValue: 2,
                    checked: true
                }],
                listeners: {
				change: function(field, newValue, oldValue, eOpts){
                                
                                var valor = newValue;
                                if(Ext.getCmp("chkIFB").checked&&Ext.getCmp("chkIFB2").checked){
                                    valor = "";
                                }
                                  
                                adminpfstore = selectorIF.store; 
                                adminpfstore.load({
                                    params: {informacion : 'catIF',
                                             tipoIntermediario : valor}
                                });
                                selectorIF.bindStore(adminpfstore); 
                                }
			}
            },
                selectorIF,
                {
                    xtype: 'form',
                    id: 'formFile',
                    name: 'formFile',
                    frame: true,
                    border: 0,
                    items: [{
                        xtype: 'filefield',
                        id: 'archivo',
                        emptyText: 'Ruta del archivo de Registro',
                        fieldLabel: 'Ruta del archivo de Registro',
                        name: 'archivoSoporte',   
                        buttonText: 'Cargar archivo',
                        labelWidth: '45%',
                        disabled: esSoloLectura,
                        width: 800,	  
                        padding: '15 15 15 15',
                        buttonCfg: {
                            iconCls: 'upload-icon'
                        },
                        vtype: 'archivoPdfXlsxZip',
                        listeners: {
                            change: function(fld, value){
                                if(icSol != null ){
                                   fld.setRawValue(value.substring(value.lastIndexOf("\\")+1));                           
                                if(fld.isValid()){
                                    cargaArchivo('CARGAR_ARCHIVO',null);
                                }
                                }else{
                                   Ext.Msg.alert('Capture datos', 'Capture los datos y luego capture el archivo');
                                }
                                
                                
                            }
                            
                        }
                        
                    }]
                },
                {
                    xtype: 'form',
                    id: 'formButtonsFile',
                    frame:true,
                    name: 'formButtonsFile',
                    padding:'10 10 10 10',
                    layout: {
                        type: 'table',
                        columns: 2
                    },
                    defaults: {
                        bodyStyle: 'margin: 10px'
                    },                                      
                    border:0
                },                
                NE.util.getEspaciador(10)
        ],
        buttons: [
        {
            text: 'Publicar',
            id: 'btnPublicar',
            iconCls: 'icoBotonEnviarCorreo',
            disabled: esSoloLectura,
            handler: function (boton, evento) {
                var panel = this.up('form');
                if(icArc!=null&&icArc!=0){
                     procesarPublicar(panel);
                }else{
                    Ext.Msg.alert('Carga de archivo', 'Antes de publicar debe cargar un archivo');
                }
            }
        },
        {
            text: 'Borrar',
            id: 'btnBorrar',
            iconCls: 'borrar',
            disabled: esSoloLectura,
            handler: function (boton, evento) {
                var panel = this.up('form');
                 Ext.Msg.show({
                        title:'�Eliminar Subasta?',
                        msg: '�Estas seguro que desea borrar la subasta se borraran las posturas y resultados?.\n�Esta acci�n no podr� revertirse!',
                        buttons: Ext.Msg.YESNOCANCEL,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                procesarBorrar(panel);                                 
                            } 
                        }
                    }); 
                
                //procesarBorrar(panel);
            }
        },
        {
            text: 'Guardar',
            id: 'btnGuardar',
            iconCls: 'icoGuardar',
            disabled: esSoloLectura,
            handler: function (boton, evento) {
                var panel = this.up('form');
                procesarGuardarSolicitud(panel);
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            disabled: esSoloLectura,
            handler: function () {
                if(dataFiles.length>0){
                    Ext.Msg.show({
                        title:'�Eliminar Archivo?',
                        msg: '�Estas seguro que deseas eliminar los archivo cargados?.\n�Esta acci�n no podr� revertirse!',
                        buttons: Ext.Msg.YESNOCANCEL,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                cleanForm();
                                Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                                Ext.Ajax.request({
                                    url: '41altaSubastasExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'deleteDocFiles',
                                        icArchivos: Ext.encode(dataFiles)
                                    }),
                                    callback: eliminaArchivos
                                });                                  
                            } 
                        }
                    });                       
                }
                else{
                    cleanForm()
                }
            }           
        },
        {
            text:'Regresar',
            id:'btnHome',
            iconCls: 'icoRegresar',
            handler: function() {
                if(!esSoloLectura && dataFiles.length > 0){
                    Ext.Msg.show({
                        title:'�Salir?',
                        msg: '�Estas seguro que deseas salir de la pantalla?\n�Se perderan todos los cambios no guardados!',
                        buttons: Ext.Msg.YESNOCANCEL,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                window.location = '41subastas.jsp?idMenu=41HOMESUBASTAS';                                
                            } 
                        }
                    });                       
                }
                else{
                    window.location = '41subastas.jsp?idMenu=41HOMESUBASTAS';
                }                            
            }
        }],
        getInvalidFields: function() {
            var invalidFields = [];
            Ext.suspendLayouts();
            this.form.getFields().filterBy(function(field) {
                if (field.validate()) return;
                invalidFields.push(field);
            });
            Ext.resumeLayouts(true);
            return invalidFields;
        }        
    });


//----------*************  CONTENEDOR PRINCIPAL  *************----------//

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			formAlta
		]        
	});



/****************** CARGA INICIAL  ******************/
    var icSol =  null;
    icSol = eicSol;
    var icArc = null;
    if (icSol != null){    
        pnl.mask("Cargando...");
        var bntPublicar = Ext.getCmp("btnPublicar");
        bntPublicar.setVisible(true);
        var btnBorrar = Ext.getCmp("btnBorrar");
        btnBorrar.setVisible(true);
        Ext.getCmp('nombreSubasta').setValue(nombreSubasta+"");
       // Ext.getCmp("tipoSubasta").setValue(tipoSubasta+"");
        Ext.getCmp("fechaRecepResultados").setValue(fechaRecepResultados);
        Ext.getCmp("chkSeleccionarIF").setValue(chkSeleccionarIF);
        
        if(tipoSubasta == '800'){        
            Ext.getCmp("radio1").setValue(true);
            Ext.getCmp("radio2").setValue(false);
        }else if(tipoSubasta == '700'){
            Ext.getCmp("radio2").setValue(true);
            Ext.getCmp("radio1").setValue(false);
        }
        
        if(ifSeleccionados != null && ifSeleccionados.length > 0){
            Ext.getCmp('ifSeleccionados').setValue(ifSeleccionados);
            Ext.getCmp("ifSeleccionados").setVisible(true);
           
        }        
        for(var i=0; i<eFiles.length; i++){
            var file  = eFiles[i];
            crearBotonesArchivo(file.clave, file.descripcion);
        }
       
        pnl.unmask();
    }
    else{
        var bntPublicar = Ext.getCmp("btnPublicar");
        var btnBorrar = Ext.getCmp("btnBorrar");
        bntPublicar.setVisible(false);
        btnBorrar.setVisible(false);
        
    }
    

}); 
