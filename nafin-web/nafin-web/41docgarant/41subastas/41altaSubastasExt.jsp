<!DOCTYPE html>
<%@ page import="
		java.util.*,
                net.sf.json.JSONArray,
                org.apache.commons.logging.Log,
                com.nafin.docgarantias.subastas.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="41altaSubastasExt.js?&lt;%=session.getId()%>">
</script>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

boolean esSoloLectura = false;
 String icc = request.getParameter("icConvocatoria");
    String nfs = request.getParameter("nfs");
    Integer icSol = null;    
    InfoSubastas infoSubastas = null;
    String sIntermediarios = null;
    String folioDocOrigen = "";
    String sFiles =  null;
    if(icc != null){
        try{
            icSol = Integer.parseInt(icc);
            Subastas subastasBean = ServiceLocator.getInstance().lookup("SubastasEJB",Subastas.class);
            infoSubastas = subastasBean.getSubastas(icSol);
            JSONArray jsIFsArray = JSONArray.fromObject(infoSubastas.getSintermediarios());              
            sIntermediarios = jsIFsArray.toString();
            ArrayList<ElementoCatalogo> files = subastasBean.getFileSubasta(infoSubastas.getIdArchivo());
            JSONArray jsFilesArray = JSONArray.fromObject(files);
            sFiles  = jsFilesArray.toString();
            
           
        }
        catch(NumberFormatException  ne){
            log.info("No se entro un valor valido para el IC_SOLICITUD");            
        }       
    }
    else{
        infoSubastas = new InfoSubastas();
    }
%>
<script type="text/javascript">
    var eicSol = <%=icSol%>;
    var nombreSubasta = '<%=infoSubastas.getNombreSubasta()%>';
    var tipoSubasta = '<%=infoSubastas.getTipoSubasta()%>';
    var fechaRecepResultados = '<%=infoSubastas.getFechaMaximaRecep()%>';
    var chkSeleccionarIF = <%=infoSubastas.getTipoIntermediario()%>;
    var ifSeleccionados = <%=sIntermediarios%>;
    var eFiles = <%=sFiles%>;
    var esSoloLectura = <%=esSoloLectura%>;
    
</script>
<style>
.marginR10p{
    margin-right: 10px;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
            <br/>
            <br/>
        <div id="areaContenido"></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
        <form id='formAux' name="formAux" target='_new'></form>
</body>

</html>