Ext.onReady(function() {

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        Ext.getCmp('contenedorPrincipal').unmask();
        if (Ext.getCmp('winDoc')) {
            Ext.getCmp('winDoc').unmask();
        }
    }
    
      function descargarResultados(record){
        gridSeguimiento.mask("Descargando resultados...");
        Ext.Ajax.request({
            url: '41subastasIfExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getDocFile',
                icArchivo: record.get('idArchivoRes'),
                icIF:  record.get('idIntermediario'),
                icCov: record.get('icConvocatoria')
            }),
        callback: descargaArchivosRes
        });   
    }
    
    function descargarConvocatoria(record){
        gridSeguimiento.mask("Descargando convocatoria...");
        Ext.Ajax.request({
            url: '41subastasIfExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getDocFile',
                icArchivo: record.get('idArchivo'),
                icIF:  record.get('idIntermediario'),
                icCov: record.get('icConvocatoria')
            }),
        callback: descargaArchivosRes
        });   
    }
    
     function descargaArchivosRes(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        gridSeguimiento.unmask();
    }
    
     function fnEliminarPosturas(record){
        Ext.Msg.show({
            title: "Eliminar Posturas",
            msg: '�Estas seguro de eliminar las Posturas de la convocatoria? Esta acci�n no podr� revertirse.',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                    Ext.Ajax.request({
                        url: '41subastasIfExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'posturastrash',
                            icConvocatoria: record.get('icConvocatoria'),
                            icIf: record.get('idIntermediario')
                        }),
                        callback:  function(opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true ) {
                                storeSeguimiento.reload();
                            }
                            Ext.Msg.alert('Eliminar Posturas', info.msg);
                            Ext.getCmp('contenedorPrincipal').unmask();                            
                        }
                    });    
                } 
            }
        });  
    }
   

    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        }]
    });
    
    
     function mostrarVentanaDocumentos(icSolicitud){
        storeArchivos.removeAll(true);
        Ext.Ajax.request({
            url: '41monitorSolExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'fileSol',
                icSolicitud: icSolicitud
            }),
            callback:  function(opts, success, response) {
                var info = Ext.JSON.decode(response.responseText);
                if (success == true && info.success == true ) {
                    gridStore.reload();
                    var registros = info.registros;
                    var icDocumento = info.icDocumento;
                    for(i=0; i<registros.length; i++){
                        storeArchivos.add({'clave': registros[i].clave ,'descripcion': registros[i].descripcion});
                    }
                    if(icDocumento != 0){
                        storeArchivos.add({'clave': icDocumento, 'descripcion': 'Documento a Formalizar'});
                    }
                }
                procesarCargaNombresArchivos();
                Ext.getCmp('contenedorPrincipal').unmask();                            
            }
        });
    }
    
    

    var procesarCargaNombresArchivos = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'La Solicitud no tiene archivos cargados');
            } else {

                var winDoc = Ext.create('Ext.window.Window', {
                    title: 'Documentos Soporte de la Solicitud',
                    id: 'winDoc',
                    height: 200,
                    width: 500,
                    frame: true,
                    layout: 'fit',
                    modal: true,
                    items: {
                        xtype: 'grid',
                        border: false,
                        enableColumnMove: false,
                        enableColumnResize: true,
                        enableColumnHide: false,
                        columns: [{
                            header: 'Nombre de archivo',
                            dataIndex: 'descripcion',
                            width: 288,
                            sortable: false
                        }, {
                            xtype: 'actioncolumn',
                            header: "Documento",
                            align: 'center',
                            sortable: false,
                            width: 200,
                            items: [{
                                iconCls: 'icoLupa',
                                tooltip: 'Ver Documento',
                                handler: function(grid, rowIndex, colIndex) {
                                    winDoc.mask("Descargando archivo...")
                                    var record = grid.getStore().getAt(rowIndex);
                                    Ext.Ajax.request({
                                        url: '41subastasIfExt.data.jsp',
                                        params: Ext.apply({
                                            informacion: 'getDocFile',
                                            icArchivo: record.get('clave')
                                        }),
                                        callback: descargaArchivos
                                    });
                                }
                            }]
                        }],
                        store: storeArchivos
                    }
                }).show();
            }
        }
    }

    var storeArchivos = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        proxy: {
            type: 'ajax',
            url: '41subastasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'fileSol'
            }
        },
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarCargaNombresArchivos
        }
    });

    /** Carga los archivo Soporte de la solicitud */
    function mostrarVentanaSolicitudes(icSolicitud) {
        
        storeArchivos.removeAll(true);
        storeArchivos.load({
            informacion: 'fileSol',
            params: Ext.apply({
                icSolicitud: icSolicitud
            })
        });
    }
    
   
    
    
  

    //----------*************  GRID  *************----------//
    Ext.define('ConsultaSubastasDataModel', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'icConvocatoria',
            mapping: 'icConvocatoria'
        }, {
            name: 'fechaAlta',
            mapping: 'fechaAlta'
        }, {
            name: 'nombreSubasta',
            mapping: 'nombreSubasta'
        }, {
            name: 'tipoSubasta',
            mapping: 'tipoSubasta'
        }, {
            name: 'fechaRecepResultados',
            mapping: 'fechaRecepResultados'
        }, {
            name: 'fechaSolventar',
            mapping: 'fechaSolventar'
        }, 
        {
            name: 'estatus',
            mapping: 'estatus'
        }, {
            name: 'posturasRecibidas',
            mapping: 'posturasRecibidas'
        } 
        ]
    });
    
    
     //----------*************  GRID  *************----------//
    Ext.define('ConsultaSeguimientoDataModel', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'icConvocatoria',
            mapping: 'icConvocatoria'
        }, {
            name: 'fechaAlta',
            mapping: 'fechaAlta'
        }, {
            name: 'fechaRecepResultados',
            mapping: 'fechaRecepResultados'
        }, 
        {
            name: 'nombreSubasta',
            mapping: 'nombreSubasta'
        },
        {
            name: 'tipoSubasta',
            mapping: 'tipoSubasta'
        },        
        {
            name: 'usuarioIntFin',
            mapping: 'usuarioIntFin'
        }, 
        {
            name: 'fechaLimite',
            mapping: 'fechaLimite'
        }, 
        {
            name: 'idArchivo',
            mapping: 'idArchivo'
        }, 
        {
            name: 'estatus',
            mapping: 'estatus'
        },
         {name: 'idIntermediario', mapping: 'idIntermediario'},
         {name: 'icArchivoCalif', mapping: 'icArchivoCalif'},         
         {name: 'idArchivoRes', mapping: 'idArchivoRes'},
         {name: 'tagComentario', mapping: 'tagComentario'}
         
        ]
    });

    
     var procesarConsultaSeguimiento = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'No existe informaci�n con los criterios determinados');
            }
        }
        gridSeguimiento.el.unmask();
    }
    
    var procesaCargaArchivo = function (opts, success, action) {
        Ext.MessageBox.alert('Carga de Posturas', 'Se han guardado los cambios correctamente');
        Ext.getCmp('winFileUpload').close();
        storeSeguimiento.reload();
    }
    
     var procesaCargaFechaAcla = function (opts, success, action) {
        Ext.MessageBox.alert('Carga de fecha de aclaracion', 'Se han guardado los cambios correctamente');
        Ext.getCmp('causaRec').close();
        storeSeguimiento.reload();
    }
    
    var cargaArchivoBO = function (estado, respuesta, icConv, tipo) {
        var formaFile = Ext.getCmp('formFile');
        formaFile.getForm().submit({
            clientValidation: false,
            url: '../41cargaSubExtArchivo.jsp',
            params: Ext.apply({
                informacion: 'loadfilePos',
                icIFFile : respuesta,
                icConv : icConv,
                tipo : tipo
            }),
            waitMsg: 'Enviando datos...',
            waitTitle: 'Cargando archivo',
            success: function (form, action) {
                procesaCargaArchivo(null, true, action);
            },
            failure: NE.util.mostrarSubmitError
        });
    };
    
     var cargarCa = function (record, tipo) {
        var icIF = record.get('idIntermediario');
        var icDoc = record.get('icDocumento');
        var icConv = record.get('icConvocatoria');
        var titulo = tipo=="P"?'Cargar Postura':'Cargar Resultados';
        Ext.create('Ext.window.Window', {
            title: titulo,
            id: 'winFileUpload',
            closable: true,
            closeAction: 'destroy',
            width: 450,
            minWidth: 250,
            height: 100,
            frame: true,
            animCollapse: false,
            border: false,
            modal: true,
            layout: {
                type: 'border',
                padding: 0
            },
            items: [{
                xtype: 'form',
                id: 'formFile',
                frame: true,
                items: [{
                    xtype: 'filefield',
                    padding: '20 20 20 20',
                    fieldLabel: 'Carga Archivo',
                    width: 398,
                    //vtype: 'archivoPdf',
                    listeners: {
                        change: function (fld, value) {
                            fld.setRawValue(value.substring(value.lastIndexOf("\\") + 1));
                            if (fld.isValid()) {
                                cargaArchivoBO('CARGAR_ARCHIVO', icIF,icConv, tipo);
                            }
                        }
                    }
                }, {
                    xtype: 'hiddenfield',
                    id: 'icIF',
                    name: 'icIF',
                    value: icIF
                }, {
                    xtype: 'hiddenfield',
                    id: 'icDoc',
                    name: 'icDoc',
                    value: icDoc
                }]
            }]
        }).show();
    };    
    
    
    
     var cargarCanc = function (record) {
        var tagComentario = record.get('tagComentario');
        Ext.create('Ext.window.Window', {
            title: 'Causas de rechazo',
            id: 'causaRec',
            closable: true,
            closeAction: 'destroy',
            width: 450,
            minWidth: 250,
            height: 200,
            animCollapse: false,
            border: false,
            modal: true,
            layout: {
                type: 'border',
                padding: 0
            },
            items: [{
                xtype: 'form',
                id: 'formCausa',
                items: [{
                   xtype     : 'textareafield',
                    grow     : true,
                    name     : 'causa',
                    id       : 'causa',
                    readOnly:		true,
                    width: 420,
                    value : tagComentario,
                    fieldLabel: 'causa del rechazo'
                    ,                    anchor    : '100%'
                }
                ],                
        buttons: [      
        {
            text: 'Salir',
            id: 'btnSalir',
            iconCls: 'icoGuardar',
            handler: function (boton, evento) {
                Ext.getCmp('causaRec').close();
            }
        }
        ]
            }]
        }).show();
    };
    
    
    
    function eliminarRes(record){
        var icIF = record.get('idIntermediario');
        var icConv = record.get('icConvocatoria');
        var icArchR = record.get('idArchivoRes');
        var nombreIF = record.get('intermediarioFin');
        Ext.Msg.confirm("Base de Operaciones", "�Est� seguro que desea eliminar el archivo de Resultados de <strong>"+ nombreIF+"</strong>?", function(btnText){
                if(btnText === "yes"){    
                    Ext.Ajax.request({
                        url: '41subastasIfExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'eliminaRes',
                            icIF:  icIF,
                            icConv: icConv,
                            icArchR: icArchR
                        }),
                    callback: function (opts, success, response) {
                        var info = Ext.JSON.decode(response.responseText);
                        if (success == true && info.success == true) {
                            Ext.MessageBox.alert('Eliminar', 'Se ha borrado el archivo correctamente');
                            storeSeguimiento.reload();
                        }
                    }
                    });   
                }
        });
    } 
    
    
    var procesarGuardarfecha = function(panel) {
    
    var causa = Ext.getCmp('causa').getValue();
    var fechaAcla    = Ext.getCmp('fechaAcla').getValue();
    var fechaR = Ext.util.Format.date(fechaAcla, 'd/m/Y');
    var icIF = Ext.getCmp('fechaAcla').getValue();
    var icConv = Ext.getCmp('icConv').getValue();
   var formCausa = Ext.getCmp('formCausa');
        formCausa.getForm().submit({
            clientValidation: false,
            url: '41subastasIfExt.data.jsp',
            params: Ext.apply({
                informacion: 'causaRec',
                causa : causa,
                fechaAcla : fechaR,
                icIF    : icIF,
                icConv  : icConv
            }),
            waitMsg: 'Enviando datos...',
            waitTitle: 'Cargando Causa',
            success: function (form, action) {
                procesaCargaFechaAcla(null, true, action);
            },
            failure: NE.util.mostrarSubmitError
        });
}

  

  var storeSeguimiento = Ext.create('Ext.data.Store', {
        model: 'ConsultaSeguimientoDataModel',
        proxy: {
            type: 'ajax',
            url: '41subastasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'consultaSeguimiento'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsultaSeguimiento
        }
    });




    
    var sstoreSubastas = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41subastasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catSubastas'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError,
                'load': function(store, records, options) {
                    var record = new store.recordType({
                        id: 0,
                        title: '-- ALL --'
                    });
                    store.insert(0, record);
                    }
                
            }
        }
    });

/*
    var sstoreIntermediario = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41subastasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catIF'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    */

   var catEstatus = new Ext.data.ArrayStore({
		id: 'catEstatus',
		fields : ['CLAVE', 'DESCRIPCION'],
    data:[['En Proceso','En Proceso'],['Concluida', 'Concluida']]
	});


    var error1Fechas = "Debes capturar la fecha inicial y final";
    var error2Fechas = "La fecha final debe ser posterior a la inicial";

    var validarFechas = function(val) {
        var fechaUno = Ext.getCmp('d_fechaUno');
        var fechaDos = Ext.getCmp('d_fechaDos');
        var fechaUnoVal = fechaUno.getValue();
        var fechaDosVal = fechaDos.getValue();
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1Fechas);
            fechaDos.markInvalid(error1Fechas);
            return error1Fechas;
        }
        if (fechaUnoVal != null && fechaUnoVal > fechaDosVal) {
            fechaUno.markInvalid(error2Fechas);
            fechaDos.markInvalid(error2Fechas);
            fechaDos.focus();
            return error2Fechas;
        }
        return true;
    }
    
    
    
   
 
 
    /**********************  PANEL BUSQUEDA Seguimiento *************************/
    var formBusquedaSeg = new Ext.form.FormPanel({
        name: 'formBusquedaSeg',
        id: 'formBusquedaSeg',
        width: 943,
        height: 160,
        border: 0,
        frame: true,
        layout: 'column',
        items: [{
            xtype: 'fieldset',
            id: 's_fieldsSeguimiento',
            height: 130,
            border: false,
            columnWidth: 0.5,
            items: [
                {               
                    xtype: 'textfield',
                    id: 'nombreSubastaSeg',
                    name: 'nombreSubasta',
                    fieldLabel: 'Nombre de <br/>Subasta',
                    padding: 5,
                    width: 400,
                    forceSelection: true,
                    allowBlank: false,
                    labelStyle: 'width:200px'
                    //, labelWidth: '30%'            
                }, {
                    xtype: 'combo',
                    name: 'tipoSubasta',
                    id: 'tipoSubastaSeg',
                    fieldLabel: 'Tipo Subasta',
                    padding: 5,
                    width: 400,
                    forceSelection: true,
                    queryMode: 'local',
                    allowBlank: true,
                    emptyText: 'Todos los Tipos',
                    store: sstoreSubastas,
                    displayField: 'descripcion',
                    valueField: 'clave',
                    triggerAction: 'all',
                    editable: true
                
            },
            {
            xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'd_fechaUno',
                    name: 'd_fechaUno',
                    fieldLabel: 'Fecha de Recepci�n',
                    padding: 5,
                    width: 215,
                    emptyText: 'Desde',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 'd_fechaDos',
                    name: 'd_fechaDos',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            
        }
        ]
             },{
            xtype: 'fieldset',
            height: 200,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [
            {
                xtype: 'combo',
                name: 'estatusSubasta',
                id: 'estatusSubastaSeg',
                fieldLabel: 'Estatus Subasta',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Estatus',
                store: catEstatus,
                displayField: 'DESCRIPCION',
                valueField: 'CLAVE',
                triggerAction: 'all',
                editable: true
            }            
                        
        ]
        }],
        buttons: [
        {
            text: 'Consultar',
            id: 's_btnBuscarSeg',
            iconCls: 'icoBuscar',
            handler: function(boton, evento) {
                var resultado = validarFechas();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    gridSeguimiento.el.mask('Cargando...', 'x-mask-loading');
                    storeSeguimiento.load({
                        params: Ext.apply(formBusquedaSeg.getForm().getValues())
                    });
                }
            }
        }, {
            id: 's_btnLimpiarSeg',
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function() {
                Ext.getCmp('formBusquedaSeg').getForm().reset();
            }
        }]
    });

    /* * * * * * * * * * * * * * * * * * GRID DE RESULTADOS SOLICITUDES * * * * * * * * * * * * * * * * * * * * * * * */
    var gridSeguimiento = Ext.create('Ext.grid.Panel', {
        id: 'd_gridIdSeg',
        store: storeSeguimiento,
        stripeRows: true,
        title: 'Seguimiento',
        width: 944,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [
        {
                header: "Fecha de<br/>recepci�n",
                dataIndex: 'fechaAlta',
                align: 'center',
                width: 80,
                sortable: true,
                hideable: true,
                tdCls: 'cursorPointer'
            },         
           {
                header: "Tipo de subasta",
                dataIndex: 'tipoSubasta',
                align: 'left',
                width: 120,
                sortable: true,
                hideable: true,
                tdCls: 'cursorPointer',
                renderer: function(value) {
                    return '<span data-qtitle="Tipo de Subasta" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }      
            },           
            {
                header: "Nombre de subasta",
                dataIndex: 'nombreSubasta',
                align: 'left',
                width: 200,
                sortable: true,
                hideable: true,
                tdCls: 'cursorPointer',
                renderer: function(value) {
                    return '<span data-qtitle="Nombre de Subasta" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }    
            },
            {
                header: "Fecha l�mite para <br/>entrega de Postura",
                dataIndex: 'fechaLimite',
                align: 'center',
                width: 115,
                sortable: true,
                hideable: true
            },
            {
            header: "Convocatoria",
            menuText: "convocatoria",
            dataIndex: 'idArchivo',
            xtype: 'actioncolumn',
            width: 80,
            align: 'center',
            sortable: true,
            hideable: true,
            items: [
                {
                    iconCls: 'icoLupa',
                    tooltip: 'Visualizar Convocatoria',
                    border: '10 10 10 10',
                    handler: function (grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        descargarConvocatoria(record);
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        return record.get('idArchivo') == 0;
                    }
               }
                ]
            },
            {
            header: "Alta<br/>de Posturas",
            menuText: "Acciones",
            dataIndex: 'icConvocatoria',
            xtype: 'actioncolumn',
            width: 100,
            align: 'center',
            sortable: true,
            hideable: true,
            items: [
                {
                    iconCls: 'icon-register-add marginR10p',
                    tooltip: 'Cargar Postura',
                    border: '10 10 10 10',
                    handler: function (grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        cargarCa(record, "P");
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        var usuarioCarga = record.get("usuarioIntFin");
                        return usuarioCarga != "";
                    }                    
                },
                {
                    iconCls: 'icoLupa marginR10p',
                    tooltip: 'Ver Postura',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        fnEliminarPosturas(rec);
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        var usuarioCarga = record.get("usuarioIntFin");
                        return usuarioCarga == "";
                    }
                },        
                {
                    iconCls: 'icon-register-delete',
                    tooltip: 'Eliminar Postura',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        fnEliminarPosturas(rec);
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        var usuarioCarga = record.get("usuarioIntFin");
                        return usuarioCarga == "";
                    }
                }
                ]
        },
        {
                header: "Usuario que Carg� la Postura",
                dataIndex: 'usuarioIntFin',
                align: 'left',
                width: 200,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Usuario Carga Posturas" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }    
            }, 
            {
            header: "Observaciones",
            menuText: "resultados",
            dataIndex: 'icConvocatoria',
            xtype: 'actioncolumn',
            width: 100,
            align: 'center',
            sortable: true,
            hideable: true,
            items: [
            {
                iconCls: 'icoLupa marginR10p',
                tooltip: 'Visualizar Observaciones',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    cargarCanc(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('tagComentario') == "";
                }
           },
            {
                iconCls: 'icon-register-add',
                tooltip: 'Cargar Nueva Postura',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    cargarCa(record, "P");
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('tagComentario') == "";
                }                
             }  
                ]
        },
          {
                header: "Fecha limite<br/>para solventar<br/>observaciones",
                dataIndex: 'fechaSolventar',
                align: 'center',
                width: 100,
                sortable: true,
                hideable: true
            },  
            
            {
            header: "Resultados",
            menuText: "resultados",
            dataIndex: 'idArchivoRes',
            xtype: 'actioncolumn',
            width: 100,
            align: 'center',
            sortable: true,
            hideable: true,
            items: [
                {
                iconCls: 'icoLupa',
                tooltip: 'Visualizar Resultados',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    descargarResultados(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('idArchivoRes') == 0;
                }
               }
                
                ]
            },
            
             {
                header: "Fecha de Recepcion<br/>de Resultados",
                dataIndex: 'fechaRecepResultados',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true
            },
            
          {
                header: "Estatus",
                dataIndex: 'estatus',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true
            }
        ]
    });
     
    
    
/*************************************** CONTENEDOR PRINCIPAL  *******************************************************/      
    
    Ext.tip.QuickTipManager.init();
    Ext.create('Ext.panel.Panel', {
        width: 945,
        id: 'contenedorPrincipal',
        minHeight: 700,
        title: 'Consulta de Subastas',
        renderTo: 'areaContenido',
        items: [
        //     NE.util.getEspaciador(20),
            formBusquedaSeg,
            NE.util.getEspaciador(30),
            gridSeguimiento     
       ]
    });




});