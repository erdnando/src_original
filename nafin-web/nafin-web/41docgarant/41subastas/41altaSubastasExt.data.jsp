<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.nafin.docgarantias.subastas.*,
        com.nafin.docgarantias.nivelesdeservicio.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar="";
log.info("informacion: "+informacion);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);
Subastas subastasBean = ServiceLocator.getInstance().lookup("SubastasEJB",Subastas.class);

if (informacion.equals("catIF")){ //<------------------------------------------------------
    String tipoIntermediario = request.getParameter("tipoIntermediario")==null?"": request.getParameter("tipoIntermediario");
    
    Integer idTipoIF = null;
    if(!tipoIntermediario.equals("")){
        idTipoIF = Integer.parseInt(tipoIntermediario);
    }
    
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatIF(idTipoIF);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catPort")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";

}
else if(informacion.equals("subastas") || informacion.equals("upSubastas") ){
    ArrayList<String> parametros = new ArrayList<>(); 
    String icConvocatoria = request.getParameter("icConvocatoria");
    String fechaRecepResultados = request.getParameter("fechaRecepResultados") == null ? "" : request.getParameter("fechaRecepResultados");
    String nombreSubasta = request.getParameter("nombreSubasta") == null ? "" : request.getParameter("nombreSubasta");
    String tipoSubasta = request.getParameter("tipoSubasta") == null ? "" : request.getParameter("tipoSubasta");
    String estatus = request.getParameter("estatus") == null ? "" : request.getParameter("estatus");
    String tipoIntermediario = request.getParameter("tipoIntermediario")==null?"": request.getParameter("tipoIntermediario");
    String ifSeleccionados = request.getParameter("listaIF");
    String filesAdjuntados = request.getParameter("listaFiles");
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    JSONObject jsonObj = new JSONObject();
    Integer icDoc = null;
    Integer anio = null;
    Integer folio = null;
    ArrayList<Integer> listFiles = new ArrayList<>();
    if(informacion.equals("upSubastas")){        
        if (filesAdjuntados != null){
            JSONArray jsObjArray = JSONArray.fromObject(filesAdjuntados);        
            for(int indice=0; indice < jsObjArray.size(); indice++){
                Integer inte = jsObjArray.getInt(indice);
                listFiles.add(inte);
            }    
        }
        log.info(Arrays.asList(listFiles));
    }
    
    ArrayList<Integer> listIF = new ArrayList<>();
    if (ifSeleccionados != null){
        JSONArray jsObjArray = JSONArray.fromObject(ifSeleccionados);        
        for(int indice=0; indice < jsObjArray.size(); indice++){
            JSONObject auxiliar = jsObjArray.getJSONObject(indice);
            listIF.add(auxiliar.getInt("clave"));
        }    
    }
    InfoSubastas sol = new InfoSubastas();   
    sol.setNombreSubasta(nombreSubasta);
    sol.setFechaMaximaRecep(fechaRecepResultados);
    sol.setIcConvocatoria(icConvocatoria);
    sol.setEstatus(estatus);
    sol.setSintermediarios(listIF);
    sol.setTipoIntermediario(tipoIntermediario);
    sol.setTipoSubasta(tipoSubasta);
    if(informacion.equals("upSubastas")){
        sol.setIdArchivo(listFiles.get(0));
    }
    log.info(sol);
    Integer icDocumento = null;
    try{
        Boolean exito = false;
        String folioGenerado = null;
        if(icConvocatoria != null && informacion.equals("upSubastas")){
             Integer icSol = Integer.parseInt(icConvocatoria);
             sol.setIcConvocatoria(icConvocatoria);
             folioGenerado = subastasBean.updateSolicitud(sol);  
             
        }else{
            folioGenerado = subastasBean.altaSubastas(sol);  
        }
        if (folioGenerado != null){
            exito = true;
        }
        jsonObj.put("success", exito);
        jsonObj.put("guardar", exito);
        jsonObj.put("folio", folioGenerado);
    }
    catch(Exception e){
        log.error(e.getMessage());
        e.printStackTrace();
        jsonObj.put("success", new Boolean(false));
        jsonObj.put("guardar", new Boolean(false));    
        jsonObj.put("msg", e.getMessage());
        jsonObj.put("stackTrace",e.getStackTrace());
        jsonObj.put("causeStackTrace", e.getCause());
    }
    infoRegresar = jsonObj.toString(); 
}
else if (informacion.equals("borrarSubasta")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String icConvocatoria = request.getParameter("icConv") == null ? "0" : request.getParameter("icConv");
    ArchivoDocumento archivo = files2OnBaseBean.eliminaArchivoSConv((Integer.parseInt(icConvocatoria)));
    InfoSubastas sol = new InfoSubastas();   
    sol.setIcConvocatoria(icConvocatoria);    
    subastasBean.borrarSubastas(sol);
    
    if (archivo != null){
        exito = true;
        jsonObj.put("msg","Se ha eliminado la subasta correctamente.");        
    }
    else{
        jsonObj.put("msg","Error al eliminar el archivo");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();


}

else if (informacion.equals("publicarSubasta")){
    boolean exito = false;
    String icConvocatoria = request.getParameter("icConv") == null ? "0" : request.getParameter("icConv");
    
    NivelesServicio nivelesServicioBean = ServiceLocator.getInstance().lookup("NivelesServicio",NivelesServicio.class);
    nivelesServicioBean.enviarSubastas(1,Integer.parseInt(icConvocatoria));

    JSONObject jsonObj   =  new JSONObject();
    exito = true;
    jsonObj.put("msg","Se ha publicado la subasta correctamente.");        
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();


}

else if (informacion.equals("getDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("delDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    ArchivoDocumento archivo = files2OnBaseBean.eliminaArchivoConvocatoria(Integer.parseInt(sicArchivo));
    if (archivo != null){
        exito = true;
        jsonObj.put("msg","Se ha eliminado el archivo: "+archivo.getNombreArchivo()+ " correctamente.");
        jsonObj.put("icArchivo", archivo.getIcArchivo());
    }
    else{
        jsonObj.put("msg","Error al eliminar el archivo");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}else if (informacion.equals("deleteDocFiles")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String filesAdjuntados = request.getParameter("icArchivos");
    ArrayList<Integer> listFiles = new ArrayList<>();
    if (filesAdjuntados != null){
        JSONArray jsObjArray = JSONArray.fromObject(filesAdjuntados);        
        for(int indice=0; indice < jsObjArray.size(); indice++){
            Integer inte = jsObjArray.getInt(indice);
            listFiles.add(inte);
        }    
    }
    exito = files2OnBaseBean.eliminaArchivosSolicitud(listFiles);
    if (exito){
        jsonObj.put("msg","Se han eliminado los archivos correctamente.");
    }
    else{
        jsonObj.put("msg","Error al eliminar el archivo");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
 else if (informacion.equals("solData")){ 
    String icSolicitud = request.getParameter("icSolicitud");
    Integer icSol = null;
    JSONObject jsonObj   =  new JSONObject();
    try{
        icSol = Integer.parseInt(icSolicitud);
        SolicitudDoc sol = solicitudesBean.getSolicitud(icSol); 
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("sol",sol);
        infoRegresar = jsonObj.toString();
    }
    catch(NumberFormatException ne){
        jsonObj.put("msg","Error al procesa el parametro de la solicitud");
        jsonObj.put("success", new Boolean(false));
        infoRegresar = jsonObj.toString();
        log.info("No es un parametro vaido: ["+ icSolicitud+"]");
    }
}
 else if (informacion.equals("nfs2icFol")){ // <----- Obtener el numero IC_Documento del Folio de la Solicitud
    String folioSolicitud = request.getParameter("nfs");
    Integer icDoc = null;
    JSONObject jsonObj   =  new JSONObject();
    try{
        String paramsFolio[] = folioSolicitud.split("-");
        Integer anio = Integer.parseInt(paramsFolio[0]);
        Integer folio = Integer.parseInt(paramsFolio[1]);
        icDoc = solicitudesBean.getIcDocumentoFromFolioSolicitud(anio, folio); 
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("icDoc",icDoc);
        infoRegresar = jsonObj.toString();
    }
    catch(NumberFormatException ne){
        jsonObj.put("msg","Error al procesa el folio de la Soliciutd/Documento");
        jsonObj.put("success", new Boolean(false));
        infoRegresar = jsonObj.toString();
        log.info("No es un parametro vaido: ["+ folioSolicitud+"]");
    }
}
 else if (informacion.equals("getComments")){ 
        String icSolicitud = request.getParameter("icSolicitud");
        ArrayList<Comentario>listado = solicitudesBean.getComentariosSol(Integer.parseInt(icSolicitud));
        JSONArray jsObjArray = new JSONArray();
        jsObjArray = JSONArray.fromObject(listado);
        infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}


log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>