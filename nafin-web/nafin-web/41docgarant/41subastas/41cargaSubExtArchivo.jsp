<%@ page
	contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileUploadException,
		org.apache.commons.logging.Log,
		com.nafin.docgarantias.*,
                com.nafin.docgarantias.subastas.*,
		netropology.utilerias.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	private final int MAX_PLANTILLA_FILE_SIZE = 1024*1024*80; // 80 MB   
%>
<%
	ParametrosRequest req = null;
	String itemArchivo = "";
	String rutaArchivo = "";
	String mensajeError =""; 
	String PATH_FILE = strDirectorioTemp;
	String nombreArchivo= "";
	String rutaArchivoTemporal="";
	String cargaAcuse = "";
	String cargaFechaAcuse = "";
	String cargaHoraAcuse = "";
	Boolean exito = false;
	Integer icArchivo = null;
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType);
	request.setAttribute("myContentType", myContentType);
	String icIF = null;
        String picDocumento = null;
        String picSolicitud = null;
        String picConvocatoria = null;
        String informacion = null;
        String tipo = null;
        
	if (ServletFileUpload.isMultipartContent(request)) {
		DiskFileItemFactory factory = new DiskFileItemFactory();		
		factory.setRepository(new File(PATH_FILE));
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(MAX_PLANTILLA_FILE_SIZE);
		try {
                    req = new ParametrosRequest(upload.parseRequest(request));
                    informacion = req.getParameter("informacion");
                    icIF = req.getParameter("icIFFile");
                    picDocumento = req.getParameter("icDocumento");
                    picSolicitud = req.getParameter("icSolicitud");
                    picConvocatoria = req.getParameter("icConv");
                    tipo = req.getParameter("tipo");
                    FileItem fItem = (FileItem)req.getFiles().get(0);
                    itemArchivo		= (String)fItem.getName();
                    rutaArchivo = PATH_FILE+itemArchivo;
                    int tamanio			= (int)fItem.getSize();
                    nombreArchivo= itemArchivo;
                    rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
                    File archivoUpload  = new File(rutaArchivoTemporal);
                    if(archivoUpload.exists()){
                            archivoUpload.delete();
                    }
                    System.out.println(rutaArchivoTemporal);
                    fItem.write(new File(rutaArchivoTemporal));
                    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
                    
                    ArchivoDocumento archivoDoc = new ArchivoDocumento(nombreArchivo, 1);
                    if(informacion.equals("loadfileIF")){
                        archivoDoc = files2OnBaseBean.guardaArchivo(archivoDoc, PATH_FILE);
                        Subastas subastasBean = ServiceLocator.getInstance().lookup("SubastasEJB",Subastas.class);
                        InfoSubastas subastas = new InfoSubastas();
                        subastas.setIdArchivoCalif(archivoDoc.getIcArchivo());
                        subastas.setIdArchivo(archivoDoc.getIcArchivo());
                        subastas.setIcConvocatoria(picConvocatoria);
                        subastas.setIdIntermediario(icIF);                        
                        if(tipo.equals("P")){                        
                            subastasBean.updatePostura(subastas, "1");
                        }else if(tipo.equals("R")){
                            subastasBean.altaResultado(subastas);
                        }
                    } else if(informacion.equals("loadfilePos")){
                        archivoDoc = files2OnBaseBean.guardaArchivo(archivoDoc, PATH_FILE);
                        Subastas subastasBean = ServiceLocator.getInstance().lookup("SubastasEJB",Subastas.class);
                        InfoSubastas subastas = new InfoSubastas();
                        subastas.setIdArchivo(archivoDoc.getIcArchivo());
                        subastas.setIcConvocatoria(picConvocatoria);
                        subastas.setIdIntermediario(icIF);
                        subastas.setUsuarioIntFin(iNoUsuario);
                        subastasBean.altaPostura(subastas);
                  } else if(informacion.equals("loadfile")){
                        String sicArchivo = req.getParameter("icArchivo") == null ? "" : req.getParameter("icArchivo");
                        if(sicArchivo !=null && !sicArchivo.equals("")&& !sicArchivo.equals("0")){
                            ArchivoDocumento archivo = files2OnBaseBean.eliminaArchivoConvocatoria(Integer.parseInt(sicArchivo));
                        }
                        archivoDoc = files2OnBaseBean.guardaArchivo(archivoDoc, PATH_FILE);
                        Subastas subastasBean = ServiceLocator.getInstance().lookup("SubastasEJB",Subastas.class);
                        InfoSubastas subastas = new InfoSubastas();
                        subastas.setIdArchivo(archivoDoc.getIcArchivo());
                        subastas.setIcConvocatoria(picConvocatoria);
                        subastasBean.updateArchivoSubasta(subastas);
                  }           
                    else{
                        archivoDoc = files2OnBaseBean.guardaArchivo(archivoDoc, PATH_FILE);
                    }
                    icArchivo = archivoDoc.getIcArchivo();
                    exito  = true;
		} catch(Throwable e) {
                        if( e instanceof SizeLimitExceededException  ) {
                            log.error("CargaPlantilla.subirArchivo(Exception): El Archivo es muy Grande, excede el límite", e);
                            mensajeError = "El archivo que intenta cargar sobrepasa los "+ ( MAX_PLANTILLA_FILE_SIZE / 1048576 ) +" MB permitidos. Favor de verificarlo";
                        } else{
                            log.error(e.getStackTrace());
                            throw new AppException("Error al cargar el archivo a OnBase", e);
                        }
		}
	}
%>
{
	"success"       : <%=exito.toString()%>,
        "icArchivo"     : <%=icArchivo%>,
	"nombreArchivo" :'<%=nombreArchivo%>',
	"mensajeError"  :'<%=mensajeError%>'       
}