<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
        com.nafin.docgarantias.*,
	com.nafin.docgarantias.mantenimiento.*,
        com.nafin.docgarantias.formalizacion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar="";
Mantenimiento mantenimientoBean = ServiceLocator.getInstance().lookup("Mantenimiento",Mantenimiento.class);  
Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);  

if (informacion.equals("mantenimientoDocs")){   
    Map<String, String> filtros = new HashMap<>();
    String fechaUno		= request.getParameter("fechaFormalizacion1");
    String fechaDos           = request.getParameter("fechaFormalizacion2");
    String fechaFin1		= request.getParameter("fechaVersion1");
    String fechaFin2           = request.getParameter("fechaVersion2");    
    String folioSolicitud     = request.getParameter("folio");
    String intermediario      = request.getParameter("intermediario");
    String documento          = request.getParameter("tipoDocumento");
    String portafolio         = request.getParameter("portafolio");
    String productoEmpresa    = request.getParameter("producto");
    
    if (fechaUno           != null && fechaDos != null && !fechaUno.isEmpty() && !fechaDos.isEmpty()) {filtros.put("fechaFormalizacion1", fechaUno);filtros.put("fechaFormalizacion2", fechaDos);}
    if (fechaFin1           != null && fechaFin1 != null && !fechaFin1.isEmpty() && !fechaFin2.isEmpty()) {filtros.put("fechaVersion1", fechaFin1);filtros.put("fechaVersion2", fechaFin2);}
    if (folioSolicitud     != null && !folioSolicitud.isEmpty()) filtros.put("folio", folioSolicitud);
    if (intermediario      != null && !intermediario.isEmpty()) filtros.put("intermediario",intermediario);
    if (documento          != null && !documento.isEmpty()) filtros.put("tipoDocumento",documento);
    if (portafolio         != null && !portafolio.isEmpty()) filtros.put("portafolio",portafolio);
    if (productoEmpresa    != null && !productoEmpresa.isEmpty()) filtros.put("producto",productoEmpresa);
    
    ArrayList<MantenimientoDoc> listado = mantenimientoBean.getDocsMantenimiento(filtros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
} 
else if(informacion.equals("catIFs")){
    ArrayList<ElementoCatalogo>listado = mantenimientoBean.getCatIntermediarios();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catFolios")){
    ArrayList<ElementoCatalogo>listado = mantenimientoBean.getCatFolios();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catDocumentos")){
    ArrayList<ElementoCatalogo>listado = mantenimientoBean.getCatTipoDocumento();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catPortafolio")){
    ArrayList<ElementoCatalogo>listado = mantenimientoBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catProductos")){
    ArrayList<ElementoCatalogo>listado = mantenimientoBean.getCatNombreProducto();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("getSignedDoc")){
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
    String sicDoc = request.getParameter("icDoc") == null ? "" : request.getParameter("icDoc");
    Map<String,String> params = new HashMap<>();
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);   
        Integer icIF = Integer.parseInt(sicIF);
        Integer icDocumento = Integer.parseInt(sicDoc);
        params.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo+"");
        params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF+"");
        params.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento+"");
        String nombreArchivo = formalizacionBean.FirmaDocumento(strDirectorioPublicacion, params);	
        jsonObj.put("urlArchivo", nombreArchivo);		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa los parametros getSignedDoc");
        jsonObj.put("msg","Error al procesa los parametros icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}

log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>