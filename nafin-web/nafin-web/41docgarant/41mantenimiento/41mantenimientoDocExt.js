Ext.onReady(function () {


    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        pnl.unmask();
    }

    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        }]
    });



  

    Ext.define('HomeSolicitudesDataModel', {
        extend: 'Ext.data.Model',
        fields: [{name: 'icIF', mapping: 'icIF'},
            {name: 'icDocumento', mapping: 'icDocumento'},
            {name: 'folio', mapping: 'folio'},
            {name: 'tipoDocumento', mapping: 'tipoDocumento'},
            {name: 'portafolio', mapping: 'portafolio'},
            {name: 'nombreEmpresa', mapping: 'nombreEmpresa'},
            {name: 'intermediario', mapping: 'intermediario'},
            {name: 'icArchivo', mapping: 'icArchivo'},
            {name: 'version', mapping: 'version'},
            {name: 'fechaFormalizacion', mapping: 'fechaFormalizacion'},
            {name: 'fechaFinVersion', mapping: 'fechaFinVersion'}]
    });

    var procesarConsulta = function (store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'No existe información con los criterios determinados');
            }
        }
        gridResultados.unmask();
    }

    var gridStore = Ext.create('Ext.data.Store', {
        model: 'HomeSolicitudesDataModel',
        proxy: {
            type: 'ajax',
            url: '41mantenimientoDocExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'mantenimientoDocs'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsulta
        }
    });

    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    });

    //********************************** GRID PRINCIPAL ************************************//

    var gridResultados = Ext.create('Ext.grid.Panel', {
        id: 'gridId',
        store: gridStore,
        stripeRows: true,
        title: 'Consulta y Mantenimiento de Documentos',
        width: 944,
        height: 450,
        collapsible: false,
        plugins: [cellEditing],
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
            header: "Folio de<br/>Documento",
            dataIndex: 'folio',
            width: 70,
            sortable: true,
            hideable: true,
            tdCls: 'cursorPointer',
            listeners: {
                click: function (iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
                    var rec = gridStore.getAt(iRowIdx.index);
                    verSolicitud(rec.get("icSolicitud"));
                }
            }
        }, {
            header: "Tipo de Documento",
            dataIndex: 'tipoDocumento',
            width: 200,
            sortable: true,
            hideable: true
        }, {
            header: "Portafolio",
            dataIndex: 'portafolio',
            width: 130,
            sortable: true,
            hideable: true
        }, {
            header: "Producto/Empresa",
            dataIndex: 'nombreEmpresa',
            align: 'left',
            width: 250,
            sortable: true,
            hideable: true
        }, {
            header: "Intermediario Financiero",
            dataIndex: 'intermediario',
            align: 'left',
            width: 180,
            sortable: true,
            hideable: true
        }, {
            xtype:'actioncolumn',
            menuText: 'Documento',
            header: "Documento",
            align: 'center',
            dataIndex: 'icArchivo',
            hideable: true,
            width: 90,
                items: [{
                    iconCls: 'page_paintbrush',
                    tooltip: 'Ver Documento',
                    handler: function(grid, rowIndex, colIndex) {
                        pnl.mask("Descargando archivo...");
                        var record = grid.getStore().getAt(rowIndex); 
                        Ext.Ajax.request({
                            url: '41mantenimientoDocExt.data.jsp',
                            params: Ext.apply(  {
                                informacion: 'getSignedDoc',
                                icArchivo: record.get('icArchivo'),
                                icDoc: record.get('icDocumento'),
                                icIF: record.get('icIF')
                            }),
                        callback: descargaArchivos
                        });                                          
                    }
                }]
        }, {
            header: "Versión del<br/>Documento",
            dataIndex: 'version',
            width: 80,
            align: 'left',
            sortable: true,
            hideable: true
        }, {
            header: "Fecha de<br/>Formalizacion",
            dataIndex: "fechaFormalizacion",
            align: 'center',
            hideable: true,
            sortable: true,
            width: 100
        }, {
            header: "Fecha fin Version",
            dataIndex: 'fechaFinVersion',
            align: 'center',
            width: 100,
            sortable: true,
            hideable: true,
            renderer: function (value) {
                return  value;
            }
        }]
    });

    //----------************* CATALOGOS  *************----------//  
    var storeFolioSolicitud = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41mantenimientoDocExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catFolios'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var storeCatPortafolio = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41mantenimientoDocExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catPortafolio'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
    var storeCatDocumentos = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41mantenimientoDocExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catDocumentos'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var storeProducto = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41mantenimientoDocExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catProductos'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var storeIntermediarios = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41mantenimientoDocExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catIFs'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var error1Fechas = "Debes capturar la fecha inicial y final";
    var error2Fechas = "La fecha final debe ser posterior a la inicial";

    var validarFechas = function (val) {
        var fechaUno = Ext.getCmp('fechaFormalizacion1');
        var fechaDos = Ext.getCmp('fechaFormalizacion1');
        var fechaUnoVal = fechaUno.getValue();
        var fechaDosVal = fechaDos.getValue();
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1Fechas);
            fechaDos.markInvalid(error1Fechas);
            return error1Fechas;
        }
        if (fechaUno != null && fechaUno > fechaDos) {
            fechaUno.markInvalid(error2Fechas);
            fechaDos.markInvalid(error2Fechas);
            fechaDos.focus();
            return error2Fechas;
        }
        return true;
    }

    //----------*************  PANEL BUSQUEDA  *************----------//
    var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        width: 943,
        height: 'auto',
        frame: true,
        title: 'Consulta y Mantenimiento de Documentos',
        layout: 'column',
        items: [{
            xtype: 'fieldset',
            height: 150,
            border: false,
            columnWidth: 0.5,
            items: [{
                xtype: 'combo',
                name: 'folio',
                id: 'folio',
                fieldLabel: 'Folio de Documento',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Folios',
                store: storeFolioSolicitud,
                displayField: 'descripcion',
                valueField: 'clave'
            }, {
                xtype: 'combo',
                id: 'tipoDocumento',
                name: 'tipoDocumento',
                fieldLabel: 'Tipo de Documento',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Documentos',
                store: storeCatDocumentos,
                displayField: 'descripcion',
                valueField: 'clave'
            }, {
                xtype: 'combo',
                id: 'portafolio',
                name: 'portafolio',
                fieldLabel: 'Portafolio:',
                padding: 5,
                width: 400,
                store: storeCatPortafolio,
                emptyText: 'Todos los Portafolios',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                allowBlank: true
            }, {
                xtype: 'combo',
                id: 'producto',
                name: 'producto',
                fieldLabel: 'Producto',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Productos/Empresas',
                store: storeProducto,
                displayField: 'descripcion',
                valueField: 'clave'
            }]
        }, {
            xtype: 'fieldset',
            height: 150,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [{
                xtype: 'combo',
                id: 'intermediario',
                name: 'intermediario',
                fieldLabel: 'Intermediariio Financiero:',
                padding: 5,
                width: 400,
                store: storeIntermediarios,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Intermediarios',
                displayField: 'descripcion',
                valueField: 'clave'
            }, {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'fechaFormalizacion1',
                    name: 'fechaFormalizacion1',
                    fieldLabel: 'Fecha de Formalización',
                    padding: 5,
                    width: 215,
                    emptyText: 'Desde',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 'fechaFormalizacion2',
                    name: 'fechaFormalizacion2',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            }, {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'fechaVersion1',
                    name: 'fechaVersion1',
                    fieldLabel: 'Fecha Fin Versión',
                    padding: 5,
                    width: 215,
                    emptyText: 'Desde',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 'fechaVersion2',
                    name: 'fechaVersion2',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            }]
        }],
        buttons: [{
            text: 'Buscar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarFechas();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    gridResultados.el.mask('Cargando...', 'x-mask-loading');
                    gridStore.load({
                        informacion: 'HomeSolicitudes',
                        params: Ext.apply(formBusqueda.getForm().getValues())
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                formBusqueda.getForm().reset();
            }
        }]
    });

    //----------*************  CONTENEDOR PRINCIPAL  *************----------//

    var pnl = new Ext.Container({
        id: 'contenedorPrincipal',
        renderTo: 'areaContenido',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            formBusqueda,
            NE.util.getEspaciador(10),
            gridResultados,
            NE.util.getEspaciador(20)
        ]
    });

});
