
Ext.onReady(function() {

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();		
            }else {
                NE.util.mostrarConnError(response,opts);
        }
    }



//----------*************  GRID  *************----------//
	 Ext.define('MonitorLineasDataModel', {
		extend: 'Ext.data.Model',
		fields: [
		   {name: 'portafolio', 			mapping : 'nombrePortafolio' 			},
		   {name: 'producto', 				mapping : 'nombreProducto' 				},
		   {name: 'if', 					mapping : 'nombreIF' 					},
		   {name: 'linea_asignada', 		mapping : 'lineaProductoAsignada' 		},
		   {name: 'consumo_linea', 			mapping : 'consumoLineaProducto' 			},
		   {name: 'consumo_participacion',	mapping : 'consumoLineaIF'	},
		   {name: 'vigencia', 				mapping : 'fechaVigencia' 				},
		   {name: 'base_operacion', 		mapping : 'baseOperacion' 		}
		]
	 });
     
     
    var procesarConsulta = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if(store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso','No existe informaci�n con los criterios determinados');
                Ext.getCmp('btnPDF').setDisabled(true);
                Ext.getCmp('btnXLS').setDisabled(true);
            }
            else{
                Ext.getCmp('btnPDF').setDisabled(false);
                Ext.getCmp('btnXLS').setDisabled(false);
            }
        var total1 = 0;
        var total2 = 0;
        var total3 = 0;
        store.data.each(function() {
            total1 += this.data['linea_asignada']
            total2 += this.data['consumo_linea']
            total3 += this.data['consumo_participacion']
        });
        var totalEstor = Ext.getStore('totalStore');
        totalEstor.removeAll();
        totalEstor.add({'texto':'Totales', 'linea_asignada':Ext.util.Format.number(total1,'0,000.00'), 
            'consumo_linea':Ext.util.Format.number(total2,'0,000.00'), 
            'consumo_participacion': Ext.util.Format.number(total3,'0,000.00') });
        }
        gridResultados.el.unmask();
    }
        
    //  grid store
    var gridStore = Ext.create('Ext.data.Store', {
        model: 'MonitorLineasDataModel',
        proxy:{
                type:            'ajax',
                url:             '41monitorLineasExt.data.jsp',
                reader:          {
                type:         'json',
                root:         'registros'
            },
            extraParams:{
                informacion:  'MonitorLineasNafin'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsulta
        }            
    });        
        
        

		
        //  grid
        var gridResultados = Ext.create('Ext.grid.Panel', {
           id                : 'gridId',
           store             : gridStore,
           stripeRows        : true,
           title             : 'Monitor de L�neas',  
           width             : 944,
           height            : 400,
           collapsible       : false,             
           enableColumnMove  : true,
           enableColumnResize: true,
           enableColumnHide  : true,
           autoScroll: true,
		   columns           :
		   [{ 
			  header: "Nombre del<br/> Portafolio",
			  dataIndex: 'portafolio',	
			  id : 'name',    
			  width:130,
			  sortable: true,  
			  hideable: true  
		   },{
			  header: "Nombre del<br/> Producto", 
			  dataIndex: 'producto',
			  width: 120,
			  sortable: true,
			  hideable: true   
		   }
			,{
			  header: "Intermediario<br/> Financiero", 
			  dataIndex: 'if',
			  width: 120,
			  sortable: true,
			  hideable: true   
		   }               
			,{
			  header: "L�nea de Proyecto<br/> Asignada", 
			  dataIndex: 'linea_asignada',
			  align: 'right',
			  width: 140,
			  sortable: true,
                          renderer: Ext.util.Format.numberRenderer('0,000.00'),
			  hideable: false   
		   }  
			,{
			  header: "L�nea de Proyecto<br/>consumida", 
			  dataIndex: 'consumo_linea',
			  align: 'right',
			  width: 140,
			  sortable: true,
                          renderer: Ext.util.Format.numberRenderer('0,000.00'),
			  hideable: false  
		   }            
			,{
			  header: "L�nea Utilizada IF", 
			  dataIndex: 'consumo_participacion',
			  align: 'right',
			  width: 140,
			  sortable: true,
			  renderer: Ext.util.Format.numberRenderer('0,000.00'),
			  hideable: false  
		   }  
			,{
			  header: "% de Consumo<br/>L�nea de Proyecto", 
			  dataIndex: 'consumo_linea',
			  width: 100,
                          align: 'right',
			  sortable: true,
			  hideable: true,   
			  renderer :  function (value, metadata, record, rowIndex, colIndex, store) {
				  var calculado = (value/record.data['linea_asignada'])*100;
				  return Ext.util.Format.number(calculado, "000.00");
			  } 
		   }               
		   ,{
			  header: "% de Consumo IF",
			  dataIndex: 'consumo_participacion',
			  width: 100,
                          align: 'right',
			  sortable: true, 
			  renderer :  function (value, metadata, record, rowIndex, colIndex, store) {
				  var calculado = (value/record.data['linea_asignada'])*100;
				  return Ext.util.Format.number(calculado, "000.00");
			  },
			  hideable: true
		   }
			,{
			  header: "Fecha de<br/>Vigencia", 
			  dataIndex: 'vigencia',
			  align: 'center',
			  width: 100,
			  sortable: true,
			  hideable: true  
		   }
		   ,{
			  header: "Base(s) de<br/> Operaci�n", 
			  dataIndex: 'base_operacion',
			  align: 'right',
			  width: 80,
			  sortable: true,
			  hideable: true  
		   }
		   ],
		bbar: ['-','->',
                        {
                            xtype:'button',
                            text: 'Imprimir en PDF',
                            id: 'btnPDF',
                            iconCls: 'icoBotonPDF',
                            handler: function(boton, evento) {
                                Ext.Ajax.request({
                                    url: '41monitorLineasExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'GenerarArchivoPDF',
                                        portafolio: Ext.getCmp('portafolio').getValue(),
                                        intermediario: Ext.getCmp('intermediario').getValue(),
                                        fechaVigencia: Ext.getCmp('fechaVigencia').getValue(),
                                        nombreProducto : Ext.getCmp('nombreProducto').getValue(),
                                        baseOperacion:  Ext.getCmp('baseOperacion').getValue(),
                                        estatusLinea: Ext.getCmp('estatusLinea').getValue()  
                                    }),
                                callback: descargaArchivos
                                });
                            }   
                        },
                        {
                            xtype:'button',
                            text: 'Exportar a Excel',
                            id: 'btnXLS',
                            iconCls: 'icoBotonXLS',
                            handler: function(boton, evento) {
                                Ext.Ajax.request({
                                    url: '41monitorLineasExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'GenerarArchivoXSL',
                                        portafolio: Ext.getCmp('portafolio').getValue(),
                                        intermediario: Ext.getCmp('intermediario').getValue(),
                                        fechaVigencia: Ext.getCmp('fechaVigencia').getValue(),
                                        nombreProducto : Ext.getCmp('nombreProducto').getValue(),
                                        baseOperacion:  Ext.getCmp('baseOperacion').getValue(),
                                        estatusLinea: Ext.getCmp('estatusLinea').getValue() 
                                    }),
                                callback: descargaArchivos
                                });
                            }   
                        }                        
                        ]
    });




//************** GRID DE TOTALES ****************************//
	 Ext.define('TotalDataModel', {
		extend: 'Ext.data.Model',
		fields: [
                   {name: 'texto', 		mapping : 'texto' },
		   {name: 'linea_asignada', 		mapping : 'linea_asignada' },
		   {name: 'consumo_linea', 		mapping : 'consumo_linea' },
		   {name: 'consumo_participacion',	mapping : 'consumo_participacion' }
		]
	 });

    var totalStore = Ext.create('Ext.data.Store', {
        id: 'totalStore',
        model: 'TotalDataModel',
        autoLoad: false           
    });    
    

        var gridTotales = Ext.create('Ext.grid.Panel', {
           id                : 'gridTotales',
           store             : totalStore,
           stripeRows        : false,
           width             : 944,
           height            : 50,
           collapsible       : false,             
           enableColumnMove  : false,
           enableColumnResize: true,
           enableColumnHide  : false,
           autoScroll: true,
           columns           :
		   [
                    { 
			  header: "Concepto",
			  dataIndex: 'texto',	
			  id : 'ttexto',    
			  width: 250,
			  sortable: true,  
			  hideable: true  
		   },     
                   { 
			  header: "L�nea de Proyecto Asignada",
			  dataIndex: 'linea_asignada',	
			  id : 'tlinea_asignada',    
			  width:160,
                          align: 'right',
			  sortable: true,  
			  hideable: true  
		   },
                    { 
			  header: "L�nea de Proyecto Consumida",
			  dataIndex: 'consumo_linea',	
			  id : 'tconsumo_linea',    
			  width:170,
                          align: 'right',
			  sortable: true,  
			  hideable: true  
		   },
                    { 
			  header: "L�nea Utilizada IF",
			  dataIndex: 'consumo_participacion',	
			  id : 'tconsumo_participacion',    
			  width:160,
                          align: 'right',
			  sortable: true,  
			  hideable: true  
		   }                   
                   ]
        });
        


//----------************* CATALOGOS  *************----------//
    function actualizaCombos(combo, valor){
        var elPanel = Ext.getCmp('formBusqueda').mask('Actualizando...', 'x-mask-loading');
        var v_idPortafolio = Ext.getCmp("portafolio") == null ? "" : Ext.getCmp("portafolio").getValue();
        var v_nombreProducto =   Ext.getCmp('nombreProducto')== null ? "" : Ext.getCmp('nombreProducto').getValue();
        var v_intermediario =  Ext.getCmp('intermediario')== null ? "" : Ext.getCmp('intermediario').getValue();
        if (combo =='portafolio'){
            storeNombreProductos.load({
                params: {
                    informacion: 'catNombreProductos',
                    portafolio:  valor,
                    intermediario :  v_intermediario,
                    nombreProducto: v_nombreProducto
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('nombreProducto').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });
            storeCatIF.load({
                params: {
                    informacion: 'servGetCatalogoIF',
                    portafolio:  valor,
                    nombreProducto:   v_nombreProducto                
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('intermediario').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });   
            storeFechaVigencia.load({
                params: {
                    informacion: 'catFechaVigencia',
                    portafolio:  valor,
                    nombreProducto: v_nombreProducto,
                    intermediario :  v_intermediario
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('fechaVigencia').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });
            storeBaseOperaciones.load({
                params: {
                    informacion: 'baseOperaciones',
                    portafolio:  valor,
                    nombreProducto:  v_nombreProducto,
                    intermediario :  v_intermediario
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('baseOperacion').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });            
        }
        else if (combo == 'nombreProducto'){
            storeCatIF.load({
                params: {
                    informacion: 'servGetCatalogoIF',
                    portafolio:  v_idPortafolio,
                    nombreProducto:   valor
                    
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('intermediario').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });
            storeBaseOperaciones.load({
                params: {
                    informacion: 'baseOperaciones',
                    portafolio:  v_idPortafolio,
                    nombreProducto:   valor,
                    intermediario :  v_intermediario
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('fechaVigencia').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });              
        }
        else if(combo=='intermediario'){
            storeFechaVigencia.load({
                params: {
                    informacion: 'catFechaVigencia',
                    portafolio:  v_idPortafolio,
                    nombreProducto:   v_nombreProducto,
                    intermediario : valor
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('fechaVigencia').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            }); 
            storeBaseOperaciones.load({
                params: {
                    informacion: 'baseOperaciones',
                    portafolio:  v_idPortafolio,
                    nombreProducto:  v_nombreProducto,
                    intermediario : valor,
                    fechaVigencia : Ext.getCmp('fechaVigencia')== null ? "" : Ext.getCmp('fechaVigencia').getValue()
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('baseOperacion').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });             
        }
        else if(combo=='fechaVigencia'){
            storeBaseOperaciones.load({
                params: {
                    informacion: 'baseOperaciones',
                    portafolio:  v_idPortafolio,
                    nombreProducto:  v_nombreProducto,
                    intermediario : v_intermediario,
                    fechaVigencia : valor
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('baseOperacion').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });      
        }

    }// end actualizaCombos 



	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }				
		]
	});

	var  storeCatPortafolio = Ext.create('Ext.data.Store', {
            model: 'ModelCatalogos',
            autoLoad: false,
            proxy: {
                type: 'ajax',
                url: '41monitorLineasExt.data.jsp',
                reader: {
                        type: 'json',
                        root: 'registros'
                },
                extraParams: {
                        informacion: 'catPortafolio'
                },
                listeners: {
                        exception: NE.util.mostrarProxyAjaxError
                }
            }
	});

	var  storeCatEstatusEnLinea = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
		data:[
                    {clave: '', descripcion: 'Todos los estatus'},
                    {clave: '1', descripcion: 'Verde'},
                    {clave: '2', descripcion: 'Amarillo'},
                    {clave: '3', descripcion: 'Rojo'}            
		]
	});    


	var  storeCatIF = Ext.create('Ext.data.Store', {
            model: 'ModelCatalogos',     
            autoLoad: false,
            proxy: {
                type: 'ajax',
                url: '41monitorLineasExt.data.jsp',
                reader: {
                        type: 'json',
                        root: 'registros'
                },
                extraParams: {
                        informacion: 'servGetCatalogoIF',
                        portafolio: Ext.getCmp("portafolio") == null ? '': Ext.getCmp("portafolio").getValue()
                },
                listeners: {
                        exception: NE.util.mostrarProxyAjaxError
                }
            }
	});      
 

    
	var  storeFechaVigencia = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
                autoLoad: false,
		proxy: {
			type: 'ajax',
			url: '41monitorLineasExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catFechaVigencia',
                                portafolio: Ext.getCmp("portafolio") == null ? '': Ext.getCmp("portafolio").getValue()
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});    
    
    
	var  storeNombreProductos = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
                autoLoad: false,
		proxy: {
			type: 'ajax',
			url: '41monitorLineasExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catNombreProductos',
                                portafolio:  Ext.getCmp('portafolio') == null ? '' : Ext.getCmp('portafolio').getValue() 
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});    
    
    

	var  storeBaseOperaciones = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
                autoLoad: false,
		proxy: {
			type: 'ajax',
			url: '41monitorLineasExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'baseOperaciones',
                                portafolio: Ext.getCmp("portafolio") == null ? '': Ext.getCmp("portafolio").getValue()
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});      
        
    storeCatPortafolio.load();
    storeCatIF.load();
    storeFechaVigencia.load();
    storeNombreProductos.load();
    storeBaseOperaciones.load();
    

//----------*************  PANEL BUSQUEDA  *************----------//

var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        width: 943,
        height: 'auto',
        frame: true,
        title: 'Monitor de L�neas',
        layout: 'column',
        items: [
            {
                xtype: 'fieldset',
                id:'fieldsBusqueda1',
                height: 150,
                border:false,
                columnWidth: 0.5,
                items: [
                        {
                            xtype: 'combo',
                            name: 'portafolio',
                            id: 'portafolio',
                            fieldLabel: 'Portafolio',
                            padding: 5,
                            width: 400,
                            forceSelection: true, 
                            queryMode: 'local', 
                            allowBlank: false,
                            editable: false,
                            emptyText: 'Todos los Portafolios',
                            store: storeCatPortafolio, displayField: 'descripcion', valueField: 'clave',
                            listeners: {
                                select: function(combo, records, eOpts) {
                                    actualizaCombos('portafolio', records[0].get('clave'));
                                }
                            }
                        },
                        {
                            xtype: 'combo',
                            id : 'nombreProducto',
                            name: 'nombreProducto',
                            fieldLabel: 'Nombre del Producto',
                            padding: 5,
                            width: 400,
                            editable: false,
                            store: storeNombreProductos,
                            emptyText: 'Todos los Productos',
                            displayField: 'descripcion',
                            valueField: 'clave',
                            allowBlank: false,
                            forceSelection: true,
                            queryMode: 'local',
                            listeners: {
                                select: function(combo, records, eOpts) {
                                    actualizaCombos('nombreProducto', records[0].get('clave'));
                                }
                            }                            
                        },
                        {
                            xtype: 'combo',
                            id: 'intermediario',
                            name: 'intermediario',
                            fieldLabel: 'Intermediario Financiero',
                            padding: 5,
                            width: 400,
                            forceSelection: true, 
                            allowBlank: false,
                            queryMode: 'local',
                            editable: false,
                            emptyText: 'Todos los Intermediarios',
                            store: storeCatIF, displayField: 'descripcion', valueField: 'clave',
                            listeners: {
                                select: function(combo, records, eOpts) {
                                    actualizaCombos('intermediario', records[0].get('clave'));
                                }
                            }                              
                        },
                        {
                            xtype: 'combo',
                            id: 'fechaVigencia',
                            name: 'fechaVigencia',
                            fieldLabel: 'Vigencia',
                            padding: 5,
                            width: 400,
                            queryMode: 'local',
                            store: storeFechaVigencia,
                            emptyText: 'Todas las fechas',
                            displayField: 'descripcion', 
                            valueField: 'clave',
                            forceSelection: true,
                            editable: false,
                            allowBlank: false,
                            listeners: {
                                select: function(combo, records, eOpts) {
                                    actualizaCombos('fechaVigencia', records[0].get('clave'));
                                }
                            }                                          
                        }
                    ]                
            },
            {
                xtype: 'fieldset',
                height: 150,
                border:false,
                columnWidth: 0.5,
                margins: '0 20 0 0',
			items: [
                    {
                        xtype: 'combo',
                        id: 'estatusLinea',
                        name: 'estatusLinea',
                        fieldLabel: 'Estatus de L�nea',
                        padding: 5,
                        width: 300,
                        forceSelection: true, 
                        queryMode: 'local', 
                        allowBlank: false,
                        editable: false,
                        store: storeCatEstatusEnLinea,
                        emptyText: 'Todos los estatus',
                        displayField: 'descripcion', 
                        valueField: 'clave'                       
                    },
                    {
                        xtype: 'combo',
                        id: 'baseOperacion',
                        name: 'baseOperacion',
                        fieldLabel: 'Base de Operaci�n',
                        padding: 5,
                        width: 300,
                        store: storeBaseOperaciones,
                        forceSelection: true,
                        queryMode: 'local', 
                        allowBlank: false,
                        editable: false,
                        emptyText: 'Todas las bases de operaci�n',
                        displayField: 'descripcion', 
                        valueField: 'clave'                                                
                    }
                ]
            }
        ],
        buttons: [{
            text: 'Buscar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                gridResultados.el.mask('Cargando...', 'x-mask-loading');
                gridStore.load({
                    informacion:  'MonitorLineasNafin',
                    params: Ext.apply(formBusqueda.getForm().getValues())
                });
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                Ext.getCmp('portafolio').setValue('');
                Ext.getCmp('nombreProducto').setValue('');
                Ext.getCmp('intermediario').setValue('');
                Ext.getCmp('fechaVigencia').setValue('');
                Ext.getCmp('estatusLinea').setValue('');
                Ext.getCmp('baseOperacion').setValue('');
            }
        }]        
    });


//----------*************  CONTENEDOR PRINCIPAL  *************----------//

	var pnl = new Ext.Container({
            id: 'contenedorPrincipal',
            renderTo: 'areaContenido',
            width: 949,		
            style: 'margin:0 auto;',
            items: [
                NE.util.getEspaciador(10),	
                formBusqueda,
                NE.util.getEspaciador(10),
                gridResultados,
                gridTotales,
                NE.util.getEspaciador(20)
            ]        
	});


}); 