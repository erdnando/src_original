Ext.onReady(function() {

    function getIcIF(clavecombo){
        if (clavecombo != null && clavecombo != undefined && !(clavecombo instanceof Object)){
            var estor = Ext.getCmp('cb_institucion').getStore();
            var registroSeleccionado = estor.findRecord('descripcion', clavecombo);
            console.log(registroSeleccionado );
            if (registroSeleccionado != null){
                console.log("TIENE VALOR");
                console.log(registroSeleccionado.get('clave'));
            }
            return registroSeleccionado.get('clave');
        }
        return null;
    }

    function formatDate(date) {
        var monthNames = [
            "01", "02", "03",
            "04", "05", "06", "07",
            "08", "09", "10",
            "11", "12"
        ];

        var day = date.getDate();
        if (day < 10) {
            day = monthNames[day - 1];
        }

        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return year + '-' + monthNames[monthIndex] + '-' + String(day);
    }
    /*:::>MODELO<:::*/
    Ext.define('resultadoModel', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'fechaRegistro',
            mapping: 'fechaRegistro'
        }, {
            name: 'usuario',
            mapping: 'usuario'
        }, {
            name: 'nombreCompleto',
            mapping: 'nombreCompleto'
        }, {
            name: 'perfil',
            mapping: 'perfil'
        }, {
            name: 'institucion',
            mapping: 'institucion'
        }, {
            name: 'correoElectronico',
            mapping: 'correoElectronico'
        }, {
            name: 'documentos',
            mapping: 'documentos'
        }, {
            name: 'portafolio',
            mapping: 'portafolio'
        }, {
            name: 'historico',
            mapping: 'historico'
        }, {
            name: 'tipo',
            mapping: 'tipo'
        }, {
            name: 'esJuridico',
            mapping: 'esJuridico'
        }]
    });



    var gridStore = Ext.create('Ext.data.Store', {
        model: 'resultadoModel',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '41consultaparametrosusuarioExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });




    /************Resultados******************/
    var gridResultados = Ext.create('Ext.grid.Panel', {
        id: 'resultados',
        store: gridStore,
        stripeRows: true,
        title: 'Resultado busqueda',
        width: 944,
        height: 300,
        collapsible: false,
        enableColumnMove: false,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
                header: "Fecha del<br/>Registro",
                dataIndex: 'fechaRegistro',
                id: 'fechaRegistro',
                width: 90,
                sortable: true,
                hideable: true
            }, {
                header: "Usuario",
                dataIndex: 'usuario',
                minwith: 150,
                sortable: true,
                hideable: false
            }, {
                header: "Nombre<br/>Completo",
                dataIndex: 'nombreCompleto',
                width: 190,
                sortable: true,
                hideable: true
            }, {
                header: "Perfil",
                dataIndex: 'perfil',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value, metaData, record) {
                    var juridico = "";
                    if (record.get("esJuridico")) {
                        juridico = " [Juridico]";
                    }
                    return value + juridico;
                }
            }, {
                header: "Institución",
                dataIndex: 'institucion',
                align: 'left',
                width: 190,
                sortable: true,
                hideable: true
            }, {
                header: "Correo Electrónico",
                dataIndex: 'correoElectronico',
                width: 150,
                align: 'left',
                sortable: true,
                hideable: true
            }, {
                header: "Documentos",
                dataIndex: 'documentos',
                width: 190,
                align: 'left',
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Documentos" data-qwidth="200" ' +
                        'data-qtip="' + value + '">' +
                        value + '<\/span>';
                }
            }, {
                header: "Portafolio",
                dataIndex: 'portafolio',
                align: 'left',
                width: 180,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Portafolio" data-qwidth="200" ' +
                        'data-qtip="' + value + '">' +
                        value + '<\/span>';
                }
            },
            {
                xtype: 'actioncolumn',
                menuText: 'Histórico',
                header: 'Histórico',
                tooltip: 'Detalle',
                dataIndex: '',
                width: 70,
                align: 'center',
                items: [{
                    iconCls: 'icoLupa',
                    handler: muestraDetalle,
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        return !(record.get('historico') != null);
                    }
                }]
            }
        ]
    });


    function muestraDetalle(grid, rowIndex, colIndex, item, event) {


        var registro = grid.getStore().getAt(rowIndex);
        var usuario = registro.get('usuario');

        Ext.Ajax.request({
            url: '41consultaparametrosusuarioExt.data.jsp',
            params: {
                accion: 'getUserInfo',
                usuario: usuario,
                tipo: registro.get('tipo')
            },
            callback: function(opts, success, response) {
                if (success == true) {
                    var parametrizacionAnterior = JSON.parse(response.responseText);
                    var usuariosRelacionados = parametrizacionAnterior.parametros.usuariosRelacionados;
                    var contenido = '<table width="500" align="center">' +
                        '<table width="450">' +
                        '<tr>' +
                        '<td style="width:100px;"><span  >Usuario juridico:<\/span><\/td><td><span  >';
                    if (parametrizacionAnterior.parametros.juridico != undefined && parametrizacionAnterior.parametros.juridico != null) {
                        contenido += parametrizacionAnterior.parametros.juridico;
                    } else {
                        contenido += 'No';
                    }
                    contenido += '<\/span><\/td>' +
                        '<\/tr>' +
                        '<tr>' +
                        '<td><span  >Documentos:<\/span><\/td><td><span  >';
                    if (parametrizacionAnterior.parametros.documentos != undefined && parametrizacionAnterior.parametros.documentos != null) {
                        contenido += parametrizacionAnterior.parametros.documentos;
                    } else {
                        contenido += '-';
                    }
                    contenido += '<\/span><\/td>' +
                        '<\/tr>' +
                        '<tr>' +
                        '<td><span  >Portafolios:<\/span><\/td><td><span  >';
                    if (parametrizacionAnterior.parametros.portafolio != undefined && parametrizacionAnterior.parametros.portafolio != null) {
                        contenido += parametrizacionAnterior.parametros.portafolio;
                    } else {
                        contenido += '-';
                    }
                    contenido += '</span></td>' +
                        '</tr>' +
                        '</table>' +
                        '</br></br>';
                    contenido += '<table width="450" align="center">' +
                        '<tr>' +
                        '<td><span  >Tipo relación</span></td>' +
                        '<td><span  >Usuario</span></td>' +
                        '</tr>';
                    if (usuariosRelacionados != undefined && usuariosRelacionados != null && usuariosRelacionados.length > 0) {
                        for (var i = 0; i < usuariosRelacionados.length; i++) {
                            contenido += '<tr>' +
                                '<td><span  >' + usuariosRelacionados[i].descripcion + '<\/span><\/td>' +
                                '<td><span  >' + usuariosRelacionados[i].clave + '<\/span><\/td>' +
                                '<\/tr>';
                        }
                    } else {
                        contenido += '<tr>' +
                            '<td><span  >-<\/span><\/td>' +
                            '<td><span  >-<\/span><\/td>' +
                            '<\/tr>';
                    }
                    contenido += '<\/table>' +
                        '<\/table>';

                    new Ext.create('Ext.window.Window', {
                        title: 'Parametrización anterior',
                        modal: true,
                        resizable: false,
                        layout: 'form',
                        width: 500,
                        height: 300,
                        id: 'winDetalle',
                        preventBodyReset: true,
                        items: [{
                            xtype: 'displayfield',
                            value: contenido
                        }]
                    }).show();
                } else {
                    NE.util.mostrarConnError(response, opts);
                }
            }
        });

    }

    //----------************* CATALOGOS  *************----------//
    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
                name: 'clave',
                type: 'string'
            },
            {
                name: 'descripcion',
                type: 'string'
            }
        ]
    });

    var storePerfil = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultaparametrosusuarioExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                accion: 'loadPerfiles'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });



    var storeUsuario = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '41consultaparametrosusuarioExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var storeInstitucion = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultaparametrosusuarioExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                accion: 'loadInstitucion'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });



    //----------*************  PANEL BUSQUEDA  *************----------//
    var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        width: 943,
        frame: true,
        height: 'auto',
        title: 'Consulta de usuarios',
        layout: 'column',
        items: [{
            xtype: 'fieldset',
            id: 'consultaUsuariosBusqueda',
            height: 150,
            border: false,
            columnWidth: 0.5,
            items: [{
                xtype: 'combo',
                id: 'cb_institucion',
                name: 'cb_institucion',
                fieldLabel: 'Institución',
                padding: 15,
                width: 400,
                emptyText: 'Todas las instituciones',
                displayField: 'descripcion',
                valueField: 'descripcion',// Para filtrar busqueda en lugar de usar ic_if
                allowBlank: true,
                forceSelection: true,
                queryMode: 'local',
                store: storeInstitucion
            },{
                xtype: 'combo',
                name: 'cb_perfil',
                id: 'cb_perfil',
                fieldLabel: 'Perfil:',
                padding: 15,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Perfiles',
                displayField: 'descripcion',
                valueField: 'clave',
                store: storePerfil,
                listeners: {
                    'select': {
                        fn: function(combo, value) {
                            var perfil = combo.getValue();
                            Ext.getCmp('cb_usuario').clearValue();
                            storeUsuario.load({
                                params: {
                                    accion: 'loadUsuarios',
                                    perfil: perfil,
                                    icIF : getIcIF(Ext.getCmp('cb_institucion').getValue())
                                },
                                callback: function(records, operation, success) {
                                    Ext.getCmp('cb_usuario').bindStore(storeUsuario);
                                }
                            });

                        }
                    }
                }            
            }]
        }, {
            xtype: 'fieldset',
            height: 150,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [{
                xtype: 'combo',
                id: 'cb_usuario',
                name: 'cb_usuario',
                fieldLabel: 'Usuario:',
                padding: 15,
                width: 300,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los usuarios',
                displayField: 'descripcion',
                valueField: 'clave'
            }, {
                xtype: 'datefield',
                id: 'dt_fechaRegistro',
                name: 'dt_fechaRegistro',
                fieldLabel: 'Fecha de Registro:',
                padding: 15,
                width: 300,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todas las fechas',
                maxValue: new Date(),
                format: 'Y-m-d'

            }]
        }],
        buttons: [{
            text: 'Buscar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            //formBind: true,
            handler: function(boton, evento) {
                gridStore.clearFilter(true);

                gridStore.load({
                    params: {
                        accion: 'loadGrid',
                        formularioBusqueda: JSON.stringify(formBusqueda.getForm().getValues())
                    },
                    callback: function(records, operation, success) {
                        var perfil = Ext.getCmp('cb_perfil').getValue();
                        var usuario = Ext.getCmp('cb_usuario').getValue();
                        
                        var icIF = Ext.getCmp('cb_institucion').getValue();
                        var institucion = null;
                        if (icIF != null && icIF != undefined && !(icIF instanceof Object)){
                            var estor = Ext.getCmp('cb_institucion').getStore();
                            var registroSeleccionado = estor.findRecord('descripcion', icIF);
                            institucion = registroSeleccionado.get('descripcion');
                        }
                        
                        var fecha = Ext.getCmp('dt_fechaRegistro').getValue();
                        if (perfil != null) {
                            gridStore.filterBy(function(rec, id) {
                                if ((rec.get('perfil') == Ext.getCmp('cb_perfil').getValue())) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                        }

                        if (usuario != null && usuario != undefined && !(usuario instanceof Object)) {
                            gridStore.filterBy(function(rec, id) {
                                if (String(rec.get('usuario')) == String(usuario)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                        }

                        if (institucion != null && institucion != undefined && !(institucion instanceof Object)) {
                            gridStore.filterBy(function(rec, id) {
                                if (String(rec.get('institucion')) == String(institucion)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                        }

                        if (fecha != null) {
                            gridStore.filterBy(function(rec, id) {
                                if (rec.get('fechaRegistro') == formatDate(fecha)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                        }

                    }
                });


            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function() {
                Ext.getCmp('formBusqueda').getForm().reset();
                storeUsuario.removeAll();
                gridStore.clearFilter(true);
                gridStore.load({
                    params: {
                        accion: 'loadGrid',
                        formularioBusqueda: JSON.stringify(formBusqueda.getForm().getValues())
                    }
                });
            }
        }]
    });

    //----------*************  CONTENEDOR PRINCIPAL  *************----------//
    var pnl = new Ext.Container({
        id: 'contenedorPrincipal',
        renderTo: 'areaContenido',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            NE.util.getEspaciador(20),
            formBusqueda,
            NE.util.getEspaciador(30),
            gridResultados,
            NE.util.getEspaciador(20)
        ]
    });



});