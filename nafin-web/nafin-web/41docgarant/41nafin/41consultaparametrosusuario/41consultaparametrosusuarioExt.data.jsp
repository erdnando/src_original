<%@ page contentType="application/json;charset=UTF-8" import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.*,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.netro.seguridad.*,
        netropology.utilerias.usuarios.UtilUsr,
        netropology.utilerias.usuarios.Usuario,
        netropology.utilerias.ElementoCatalogo,
        com.nafin.docgarantias.parametrizacion.usuarios.*,
        com.nafin.docgarantias.parametrizacion.usuarios.model.GridConsultaVO," errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/41docgarant/41secsession.jspf"%>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    ConsUsuariosNafinSeg consUsuariosNafinSeg = new  ConsUsuariosNafinSeg();
    ParametrizacionUsuarios parametrizacionUsuarios = ServiceLocator.getInstance().lookup("ParametrizacionUsuariosBean", ParametrizacionUsuarios.class);
    
     private String usuariosByPerfil(String perfil, Integer icIF) throws Exception{    
        ArrayList<Usuario> listaUsuarios = null;
        if(perfil.equals("ADMIN IF GARANT")){
            if (icIF != null){
                listaUsuarios = parametrizacionUsuarios.getUsuariosIFPorPerfil(perfil, icIF);
            }
            else{
                listaUsuarios = new ArrayList<>();
            }
        }
        else{
            listaUsuarios = parametrizacionUsuarios.getUsuariosNafinPorPerfil(perfil);
        }
        JSONArray jsonArr = new JSONArray();        
        for (Usuario usuario :listaUsuarios){
            jsonArr.add(JSONObject.fromObject(new ElementoCatalogo(usuario.getLogin(), usuario.getNombreCompleto())));
        }
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";            
    }
    
    private String perfilByUsuario(String usuario) throws Exception{                   
        return consUsuariosNafinSeg.getPerfilByUsuario(usuario);          
    }
    
    private String getPerfilesParametrizar(){
        JSONArray jsonArr = new JSONArray();
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("ADMIN IF GARANT", "ADMIN IF GARANT")));
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("OPER GESTGAR", "OPER GESTGAR")));
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("AUTO GESTGAR", "AUTO GESTGAR")));
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
    }
    
    private String getInstituciones() throws Exception{
         ArrayList<ElementoCatalogo> listaInstituciones = parametrizacionUsuarios.getInstitucionesUsuarioParams();
        JSONArray jsObjArray = new JSONArray();
        jsObjArray = JSONArray.fromObject(listaInstituciones);
        return  "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
    }
%>
<%
       
    String accion = (request.getParameter("accion") != null) ? request.getParameter("accion") : "";    
    String infoRegresar="";    
    log.info(":::> accion: " + accion);
    JSONObject jsonObj   =  new JSONObject();    
    jsonObj.put("success", new Boolean(true));
    
    
    ArrayList<GridConsultaVO> resultado = new ArrayList<>();
    switch(accion){
        case "loadGrid":
            String formulario = (request.getParameter("formularioBusqueda") != null) ? request.getParameter("formularioBusqueda") : "{}";
            JSONObject jsonObject = JSONObject.fromObject(formulario);
            jsonObject = JSONObject.fromObject(formulario);
            Map mapOpciones = new HashMap();
        
          
            resultado = parametrizacionUsuarios.getGrid(mapOpciones);
            
            JSONObject jsonObjPerfil   =  new JSONObject();
            jsonObjPerfil.put("success", new Boolean(true));
            jsonObjPerfil.put("total", resultado.size());
            jsonObjPerfil.put("registros",resultado.toArray());
                
            infoRegresar = jsonObjPerfil.toString();
        break;
        case "loadPerfiles":
            infoRegresar = getPerfilesParametrizar();
        break; 
        case "loadUsuarios":
            String perfil =  request.getParameter("perfil");
            String paramIF = request.getParameter("icIF");
            Integer icIF = null;
            if  (paramIF != null && paramIF.length() > 0){
                    icIF = Integer.valueOf(paramIF);
            }
            infoRegresar = usuariosByPerfil(perfil, icIF);
        break; 
        case "loadInstitucion":
            infoRegresar = getInstituciones();
        break;
        case "getUserInfo":
            String usuario = (request.getParameter("usuario") != null) ? request.getParameter("usuario") : "{}";
            Map parametros = parametrizacionUsuarios.getParametrosUsuarioString(usuario);            
            for(ElementoCatalogo e : (ArrayList<ElementoCatalogo>)parametros.get("usuariosRelacionados")){
                UtilUsr oUtilUsr = new UtilUsr();
                e.setClave(oUtilUsr.getUsuario(e.getClave()).getNombreCompleto());
            }                        
            jsonObj.put("parametros", JSONObject.fromObject(parametros)); 
            infoRegresar = jsonObj.toString();
        break;
        }
    
        log.info("infoRegresar: "+infoRegresar);
%>
<%=infoRegresar%>