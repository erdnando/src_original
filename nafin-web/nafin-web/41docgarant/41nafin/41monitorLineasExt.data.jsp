<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String portafolio = request.getParameter("portafolio") == null ? "" : request.getParameter("portafolio");
String producto = request.getParameter("nombreProducto") == null ? "" : request.getParameter("nombreProducto");
String fechaVigencia = request.getParameter("fechaVigencia") == null ? "": request.getParameter("fechaVigencia");
String intermediario = request.getParameter("intermediario") == null ? "" : request.getParameter("intermediario");
String infoRegresar="";
Integer idIF  = null;
log.info("informacion: "+informacion);
DocGarantias docGarantias = ServiceLocator.getInstance().lookup("DocGarantias",DocGarantias.class);

if (informacion.equals("MonitorLineasNafin")){   //<------------------------------------------------------
    ArrayList<String> parametros = new ArrayList<>(); 

    String baseoperacion = request.getParameter("baseOperacion") == null ? "" : request.getParameter("baseOperacion");
    String estatusLinea = request.getParameter("estatusLinea") == null ? "" : request.getParameter("estatusLinea");


    parametros.add(portafolio);
    parametros.add(producto);
    parametros.add(intermediario);
    parametros.add(fechaVigencia);
    parametros.add(estatusLinea);
    parametros.add(baseoperacion);
	ArrayList<MovimientoLinea> listado = docGarantias.getMonitorLineas(parametros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}

else if (informacion.equals("servGetCatalogoIF")){ //<------------------------------------------------------
    Integer idProducto = null;
    if (!producto.isEmpty()){
        idProducto = Integer.parseInt(producto);
    }
    ArrayList<ElementoCatalogo>listado = docGarantias.getIFs(portafolio, idProducto);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catFechaVigencia")){
    Integer idProducto = null;
    if (!producto.trim().isEmpty()){
        idProducto = Integer.parseInt(producto);
    }
    if(!intermediario.isEmpty()){
        idIF = Integer.parseInt(intermediario);
    }
    ArrayList<ElementoCatalogo>listado = docGarantias.getFechaVigenciaMonitorLineas(idIF, portafolio, idProducto);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catNombreProductos")){
    ArrayList<ElementoCatalogo>listado = docGarantias.getNombreProductosMonitor(idIF, portafolio);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catPortafolio")){
	ArrayList<ElementoCatalogo>listado = docGarantias.getPortafolioMonitor(idIF);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("baseOperaciones")){
    Integer idProducto = null;
    if (!producto.trim().isEmpty()){
        idProducto = Integer.parseInt(producto);
    }
    if(!intermediario.trim().isEmpty()){
        idIF = Integer.parseInt(intermediario);
    }    
    ArrayList<ElementoCatalogo>listado = docGarantias.getBaseOperacionesMonitor(portafolio,idProducto, idIF, fechaVigencia);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";

}
else if (informacion.equals("GenerarArchivoPDF") || informacion.equals("GenerarArchivoXSL")){
    ArrayList<String> parametros = new ArrayList<>(); 
    String baseoperacion = request.getParameter("baseOperacion") == null ? "" : request.getParameter("baseOperacion");
    String estatusLinea = request.getParameter("estatusLinea") == null ? "" : request.getParameter("estatusLinea");
    
    parametros.add(portafolio);
    parametros.add(producto);
    parametros.add(intermediario);
    parametros.add(fechaVigencia);
    parametros.add(estatusLinea);
    parametros.add(baseoperacion);
    String tipoArchivo = informacion.equals("GenerarArchivoPDF") ? ConstantesMonitor.ARCHIVO_PDF : ConstantesMonitor.ARCHIVO_XLS;
    JSONObject jsonObj   =  new JSONObject();    
    String nombreArchivo = docGarantias.getMonitorPDF_XLS(request, strDirectorioPublicacion, parametros, false, tipoArchivo);
    jsonObj.put("success", new Boolean(true)); 
    jsonObj.put("urlArchivo", strDirecVirtualTemp +nombreArchivo);		
    infoRegresar = jsonObj.toString();
}

log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>