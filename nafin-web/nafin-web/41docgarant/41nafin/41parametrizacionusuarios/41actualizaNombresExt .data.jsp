<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.*,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.netro.seguridad.*,
        netropology.utilerias.usuarios.UtilUsr,
        netropology.utilerias.usuarios.Usuario,
        netropology.utilerias.ElementoCatalogo,
        com.nafin.docgarantias.parametrizacion.NombresUsuarios"
        
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
  
   
    private String getUsuarios() throws Exception{    
        NombresUsuarios o = new NombresUsuarios();
        ArrayList listaUsuarios = o.getUsuariosSinNombre();
        JSONArray jsonArr = new JSONArray();        
        Iterator it = listaUsuarios.iterator();        
        while(it.hasNext()) {
                Object obj = it.next();
                ElementoCatalogo ec = (ElementoCatalogo)obj;
                jsonArr.add(JSONObject.fromObject(ec));
        }        
        
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";            
    }
    
        private String updateUsuarios() throws Exception{    
        NombresUsuarios o = new NombresUsuarios();
        ArrayList listaUsuarios = o.actualizarNombresUsuarios();
        JSONArray jsonArr = new JSONArray();        
        Iterator it = listaUsuarios.iterator();        
        while(it.hasNext()) {
                Object obj = it.next();
                ElementoCatalogo ec = (ElementoCatalogo)obj;
                jsonArr.add(JSONObject.fromObject(ec));
        }        
        
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";            
    }

%>
<%
       
    String accion = (request.getParameter("accion") != null) ? request.getParameter("accion") : "";    
    String infoRegresar="";    
    log.info(":::> accion: " + accion);
    JSONObject jsonObj   =  new JSONObject();    
    jsonObj.put("success", new Boolean(true));
    if (accion.equals("loadUsers")){
            infoRegresar = getUsuarios();
        }
        else if(accion.equals("updateUsers")){
            infoRegresar = updateUsuarios();
        }
    
    log.info("infoRegresar: " + infoRegresar);

%>
<%=infoRegresar%>