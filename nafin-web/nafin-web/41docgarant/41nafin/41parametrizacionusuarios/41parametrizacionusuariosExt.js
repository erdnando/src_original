Ext.onReady(function() {

    var usuarioDocumentos = {};

    /*Carga de convenio autografo*/
    var icDoc = 0;

    var fnArchivoPdf = function(nombArch) {
        var myRegex = /^.+\.([Pp][dD][Ff])$/;
        return (myRegex.test(nombArch));
    };

    Ext.apply(Ext.form.VTypes, {
        archivoPdf: function(v) {
            return fnArchivoPdf(v);
        },
        archivoPdfText: 'Solo se permite cargar archivos en formato PDF, Favor de verificarlo.'
    });

    var cargaArchivo = function(estado, respuesta) {
        var formaFile = Ext.getCmp('formFile');
        formaFile.getForm().submit({
            clientValidation: false,
            url: '../../41solicitudes/41cargaDocExtArchivo.jsp',
            params: {
                informacion: 'loadfile'
            },
            waitMsg: 'Enviando datos...',
            waitTitle: 'Cargando archivo',
            success: function(form, action) {
                procesaCargaArchivo(null, true, action);
            },
            failure: NE.util.mostrarSubmitError
        });
    };

    var procesaCargaArchivo = function(opts, success, action) {
        var usuario = Ext.getCmp('txt_usuario').getValue();
        Ext.Ajax.request({
            url: '41parametrizacionusuariosExt.data.jsp',
            params: {
                accion: 'updateDocumento',
                icArchivo: action.result.icArchivo,
                usuario: usuario,
                opcion: 'agregar'
            }
        });
        crearBotonArchivo(action.result.icArchivo, action.result.nombreArchivo);
    }

    function crearBotonArchivo(icArchivo, nombreArchivo) {

        Ext.getCmp('archivo').buttonText = "Cambiar archivo";
        var formaBotones = Ext.getCmp('formButtonsFile');

        var btnVer = Ext.create('Ext.Button', {
            id: 'btnV' + icArchivo,
            text: nombreArchivo,
            iconCls: 'iconoLupa',
            handler: function() {
                Ext.getCmp('contenedorPrincipal').mask("Cargando archivo...");
                Ext.Ajax.request({
                    url: '../../41altadoctos/41altaDoctobExt.data.jsp',
                    params: Ext.apply({
                        informacion: 'getDocFile',
                        icArchivo: icArchivo
                    }),
                    callback: descargaArchivos
                });
            }
        });

        var btnEliminar = Ext.create('Ext.Button', {
            id: 'btnE' + icArchivo,
            text: nombreArchivo,
            iconCls: 'borrar',
            handler: function() {
                Ext.Msg.show({
                    title: '�Eliminar Archivo?',
                    msg: '�Estas seguro que deseas eliminar el archivo:' + nombreArchivo + '?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn) {
                        if (btn === 'yes') {
                            Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                            Ext.Ajax.request({
                                url: '../../41altadoctos/41altaDoctobExt.data.jsp',
                                params: Ext.apply({
                                    informacion: 'delDocFile',
                                    icArchivo: icArchivo
                                }),
                                callback: function(opts, seccess, response) {
                                    Ext.getCmp('icDocumento').setValue(0);
                                    formaBotones.remove(btnVer);
                                    formaBotones.remove(btnEliminar);
                                    var componente = Ext.getCmp('archivo');
                                    componente.setDisabled(false);
                                    Ext.getCmp('contenedorPrincipal').unmask();
                                    var usuario = Ext.getCmp('txt_usuario').getValue();
                                    Ext.Ajax.request({
                                        url: '41parametrizacionusuariosExt.data.jsp',
                                        params: {
                                            accion: 'updateDocumento',
                                            icArchivo: icArchivo,
                                            usuario: usuario,
                                            opcion: 'eliminar'
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        });

        formaBotones.add(btnVer);
        formaBotones.add(btnEliminar);
        Ext.getCmp('icDocumento').setValue(icArchivo);
        var componente = Ext.getCmp('archivo');
        componente.setDisabled(true);
    }




    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        Ext.getCmp('contenedorPrincipal').unmask();
    }




    // :::::> Utilidades <::::://

    function procesarInformacion(opts, success, response) {

        Ext.getCmp('perfilFormAutogestgarOptions').getForm().reset();
        Ext.getCmp('perfilFormAdminIfGestgarOptions').getForm().reset();
        Ext.getCmp('perfilFormOperGestGarOptions').getForm().reset();
        datos = JSON.parse(response.responseText);

        //console.log(datos);

        storeUsuarioManual.removeAll();
        storeUsuario.data.items.forEach(function(valor, indice, array) {
            if (Ext.getCmp('txt_usuario').getValue() != valor.data.clave) {
                var instance = Ext.create('ModelCatalogos', {
                    clave: valor.data.clave,
                    descripcion: valor.data.descripcion
                });
                storeUsuarioManual.add(instance);
            }


        });

        Ext.getCmp('formButtonsFile').removeAll();
        Ext.getCmp('archivo').setDisabled(false);

        Ext.getCmp('rb_autogestgar_firma_conjunta_si').setValue(0);
        Ext.getCmp('rb_autogestgar_firma_suplente_si').setValue(0);


        for (var k in datos.parametros) {
            var propiedad = String(k);
            //console.log('checkbos [inputValue='+datos.parametros[k]+']');
            if (propiedad.indexOf("DOC") >= 0) {
                var y = Ext.ComponentQuery.query('checkbox[inputValue=' + datos.parametros[k] + ']')
                y.forEach(function(valor, indice, array) {
                    Ext.getCmp(valor.id).setValue(true);
                });
            }

            if (propiedad.indexOf("doIF_") >= 0) {
                var arregloDocumentos = datos.parametros[k];
                for (var i = 0; i < arregloDocumentos.length; i++) {
                    var y = Ext.ComponentQuery.query('checkbox[inputValue=' + arregloDocumentos[i] + ']')
                    y.forEach(function(valor, indice, array) {
                        Ext.getCmp(valor.id).setValue(true);
                    });
                }
            }

            if (propiedad.indexOf("porIF_") >= 0) {
                var arregloPortafolios = datos.parametros[k];
                for (var i = 0; i < arregloPortafolios.length; i++) {
                    var y = Ext.ComponentQuery.query('checkbox[inputValue=' + arregloPortafolios[i] + ']')
                    y.forEach(function(valor, indice, array) {
                        Ext.getCmp(valor.id).setValue(true);
                    });
                }
            }
            if (propiedad.indexOf("PORTAFOLIO") >= 0) {
                var y = Ext.ComponentQuery.query('checkbox[inputValue=' + datos.parametros[k] + ']')
                y.forEach(function(valor, indice, array) {
                    Ext.getCmp(valor.id).setValue(true);
                });
            }

            if (propiedad.indexOf('usuario') >= 0) {
                relacion = datos.parametros[propiedad].split("_")[1];
                usuario = propiedad.split("_")[1];
                if (relacion == 1) {
                    Ext.getCmp('rb_autogestgar_firma_conjunta_si').setValue(1);
                    Ext.getCmp('sl_autogestgar_usuario_fc_relacionUsuario').setValue(usuario);

                }

                if (relacion == 2) {
                    Ext.getCmp('rb_autogestgar_firma_suplente_si').setValue(1);
                    Ext.getCmp('sl_autogestgar_usuario_suplente').setValue(usuario);

                }
            }

            if (propiedad.indexOf('ES_JURIDICO') >= 0) {
                if (datos.parametros[propiedad] == 1) {
                    Ext.getCmp('rb_autogestgar_isJuridico_si').setValue(1);
                } else {
                    Ext.getCmp('rb_autogestgar_isJuridico_no').setValue(0);
                }
            }

            if (propiedad.indexOf('mediariosFinancieros') >= 0) {
                var parametrizados = JSON.stringify(datos.parametros[propiedad].split(','));

                Ext.getCmp('itemSelectorIf').store.removeAll();
                Ext.getCmp('itemSelectorIf').reset();
                storeIF.load({
                    params: {
                        accion: 'getIF',
                        seleccionados: parametrizados,
                        tipo: 'B',
                        usuario: Ext.getCmp('txt_usuario').getValue()
                    },
                    callback: function(records, operation, success) {
                        Ext.getCmp('itemSelectorIf').setValue(JSON.parse(parametrizados));
                        Ext.getCmp('rb_opergestgar_isIfb_si').setValue(true);
                    }
                });
                Ext.getCmp('itemSelectorIf').bindStore(storeIF);
            }

            if (propiedad == 'usuarioSuperOperador') {
                var x = datos.parametros[propiedad];
                Ext.Ajax.request({
                    url: '41parametrizacionusuariosExt.data.jsp',
                    params: {
                        accion: 'getPerfilByUsuario',
                        usuarioSuplente: datos.parametros[propiedad]
                    },
                    callback: function(opts, success, response) {
                        if (success == true) {
                            Ext.getCmp('sl_opergestgar_perfil_supervisor').setValue( JSON.parse(response.responseText)['perfil']);
                            storeUsuarioSuplente.load({
                                params: {
                                    accion: 'usuariosByPerfil',
                                    perfil:  JSON.parse(response.responseText)['perfil']
                                },
                                callback: function(records, operation, success) {
                                    Ext.getCmp('sl_opergestgar_usuario_supervisor').setValue(x);
                                }

                            });
                        } else {
                            NE.util.mostrarConnError(response, opts);
                            perfilFormAutogestgarOptionsformValue.unmask();
                        }
                    }
                });
            }


            if (propiedad == 'firmasRequeridas') {
                if (Number(datos.parametros[propiedad]) > 0) {
                    Ext.getCmp('txt_numeroFirmas').setValue(datos.parametros[propiedad]);
                    Ext.getCmp('rb_adminIf_firmaMancomunada_si').setValue(1);
                } else {
                    Ext.getCmp('rb_adminIf_firmaMancomunada_no').setValue(0);
                }
            }

            if (propiedad == 'diasFirma') {
                Ext.getCmp('txt_diasHabiles').setValue(datos.parametros[propiedad]);
            }

            if (propiedad == 'archivo') {
                if (Number(datos.parametros[propiedad]) > 0) {
                    Ext.getCmp('icDocumento').setValue(datos.parametros[propiedad]);
                    crearBotonArchivo(datos.parametros[propiedad], datos.parametros['nombreArchivo'])
                }
            }

        }


        Ext.getCmp('perfilFormAutogestgarOptions').getEl().unmask();
        Ext.getCmp('perfilFormOperGestGarOptions').getEl().unmask();
    }

/*
    var windowUnexpectedError = new Ext.Window({
        id: 'windowUnexpectedError',
        title: 'Detalle del Error',
        autoScroll: true,
        width: 600,
        height: 300,
        y: 0,
        closeAction: 'hide',
        html: "algo se pondra aqui"
    });*/

    // :::::> Model <::::://
    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
                name: 'clave',
                type: 'string'
            },
            {
                name: 'descripcion',
                type: 'string'
            }
        ]
    });



    // :::::> Store Ejecutivo de atencion <::::://
    var storeEjecutivoAtencion = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });


    // :::::> Store Perfiles <::::://
    var storePerfil = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                accion: 'perfilParametrizar'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
        // :::::> Store Perfiles <::::://
    var storePerfilSupervisor = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                accion: 'perfilSupervisor'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    

    // :::::> Store Usuarios <::::://	
    var storeUsuario = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    // :::::> Store suplente <::::://	
    var storeUsuarioSuplente = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    // :::::> Store IF <::::://	
    var storeIF = Ext.create('Ext.data.ArrayStore', {
        model: 'ModelCatalogos',
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            autoLoad: false,
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    // :::::> Store Usuarios por intermediario <::::://	
    /*var storeUsuarioPorIF = Ext.create('Ext.data.ArrayStore', {
        model: 'ModelCatalogos',
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            autoLoad: false,
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });*/

    //StoreIf
    var storeIFTodos = Ext.create('Ext.data.ArrayStore', {
        model: 'ModelCatalogos',
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            autoLoad: false,
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    // :::::> Store Usuarios por intermediario <::::://	
    var storeUsuariosPorIF = Ext.create('Ext.data.ArrayStore', {
        model: 'ModelCatalogos',
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            autoLoad: false,
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    // storeUsuarioManual
    var storeUsuarioManual = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: false
    });

    var itemSelectorIf = Ext.create("Ext.ux.form.ItemSelector", {
        anchor: '100%',
        autoScroll: true,
        name: 'itemSelectorIf',
        id: 'itemSelectorIf',
        height: 300,
        fieldLabel: '',
        imagePath: '../ux/images/',
        store: storeIF,
        displayField: 'descripcion',
        valueField: 'clave',
        allowBlank: false,
        msgTarget: 'side',
        fromTitle: 'Disponibles',
        toTitle: 'Parametrizados',
        buttons: ['add', 'remove']
    });

    var ifPanel = Ext.create("Ext.form.Panel", {
        title: 'Intermediarios Financieros Asignados',
        width: 700,
        bodyPadding: 10,
        height: 350,
        items: [itemSelectorIf]
    });



    /*panel de lo ultimo */



    // :::::> Perfil form filter <::::://
    var perfilFormContainer = new Ext.form.FormPanel({
        name: 'perfilFormContainer',
        id: 'perfilFormContainer',
        width: 940,
        frame: true,
        title: 'Perfiles y usuarios',
        layout: 'column',
        listeners: {
            boxready: function(perfilFormContainer) {
                Ext.getCmp('perfilFormAutogestgarOptions').getEl().hide();
                Ext.getCmp('perfilFormOperGestGarOptions').getEl().hide();
                Ext.getCmp('perfilFormAdminIfGestgarOptions').getEl().hide();
                Ext.getCmp('fieldEjecutivoAtencion').getEl().hide();
                Ext.getCmp('vacio').getEl().hide();
            }
        },
        items: [{
                xtype: 'fieldset',
                id: 'fieldPerfil',
                height: 50,
                border: false,
                columnWidth: 0.5,
                items: [{
                        xtype: 'combo',
                        name: 'txt_perfil',
                        id: 'txt_perfil',
                        fieldLabel: 'Perfil a parametrizar:',
                        margin: '10 0 0 0',
                        padding: 5,
                        labelWidth: 170,
                        width: 400,
                        forceSelection: true,
                        queryMode: 'local',
                        allowBlank: false,
                        emptyText: 'Selecciona',
                        store: storePerfil,
                        displayField: 'descripcion',
                        valueField: 'clave',
                        listeners: {
                            'select': {
                                fn: function(combo, value) {
                                    Ext.getCmp('perfilFormAutogestgarOptions').getForm().reset();
                                    Ext.getCmp('perfilFormOperGestGarOptions').getForm().reset();
                                    var perfil = combo.getValue();

                                    switch (perfil) {
                                        case "AUTO GESTGAR":
                                            Ext.getCmp('perfilFormAutogestgarOptions').getEl().show();
                                            Ext.getCmp('perfilFormOperGestGarOptions').getEl().hide();
                                            Ext.getCmp('perfilFormAdminIfGestgarOptions').getEl().hide();
                                            Ext.getCmp('fieldEjecutivoAtencion').getEl().hide();
                                            Ext.getCmp('vacio').getEl().hide();
                                            Ext.getCmp('txt_usuario').setFieldLabel("Usuario:");
                                            Ext.getCmp('txt_usuario').clearValue();
                                            storeUsuario.load({
                                                params: {
                                                    accion: 'usuariosByPerfil',
                                                    perfil: perfil
                                                },
                                                callback: function(records, operation, success) {
                                                    Ext.getCmp('txt_usuario').bindStore(storeUsuario);
                                                }
                                            });
                                            break;
                                        case "OPER GESTGAR":
                                            Ext.getCmp('perfilFormOperGestGarOptions').getEl().show();
                                            Ext.getCmp('perfilFormAutogestgarOptions').getEl().hide();
                                            Ext.getCmp('perfilFormAdminIfGestgarOptions').getEl().hide();
                                            Ext.getCmp('fieldEjecutivoAtencion').getEl().hide();
                                            Ext.getCmp('vacio').getEl().hide();
                                            Ext.getCmp('rb_opergestgar_isIfb_si').setValue(true);
                                            Ext.getCmp('txt_usuario').setFieldLabel("Usuario:");
                                            /**/
                                            Ext.getCmp('txt_usuario').clearValue();
                                            storeUsuario.load({
                                                params: {
                                                    accion: 'usuariosByPerfil',
                                                    perfil: perfil
                                                },
                                                callback: function(records, operation, success) {
                                                    Ext.getCmp('txt_usuario').bindStore(storeUsuario);
                                                }
                                            });
                                            break;
                                        case "ADMIN IF GARANT":
                                            /**/
                                            Ext.getCmp('txt_usuario').clearValue();
                                            storeIFTodos.load({
                                                params: {
                                                    accion: 'getIF',
                                                    seleccionados: [],
                                                    tipo: 'todos'

                                                },
                                                callback: function(records, operation, success) {
                                                    Ext.getCmp('txt_usuario').bindStore(storeIFTodos);
                                                }
                                            });

                                            Ext.getCmp('perfilFormAdminIfGestgarOptions').getEl().show();
                                            Ext.getCmp('perfilFormAutogestgarOptions').getEl().hide();
                                            Ext.getCmp('perfilFormOperGestGarOptions').getEl().hide();
                                            Ext.getCmp('fieldEjecutivoAtencion').getEl().show();
                                            Ext.getCmp('rb_opergestgar_isIfb_si').getEl().show();
                                            Ext.getCmp('txt_usuario').setFieldLabel("Intermediario Financiero:");
                                            break;
                                    }

                                    /*
											
                                            Ext.getCmp('txt_usuario').clearValue();
                                           storeUsuario.load({
                                                    params:{
                                                        accion: 'usuariosByPerfil',
                                                        perfil:perfil
                                                    }
                                               });
											   
                                               */

                                }


                            }
                        }
                    }

                ]
            },
            {
                xtype: 'fieldset',
                id: 'fieldUsuario',
                border: false,
                columnWidth: 0.5,
                items: [{
                    xtype: 'combo',
                    name: 'txt_usuario',
                    id: 'txt_usuario',
                    fieldLabel: 'Usuario:',
                    margin: '10 0 0 0',
                    padding: 5,
                    width: 400,
                    forceSelection: true,
                    queryMode: 'local',
                    allowBlank: false,
                    emptyText: 'Selecciona',
                    store: storeUsuario,
                    displayField: 'descripcion',
                    valueField: 'clave',
                    listeners: {
                        select: {
                            fn: function(combo, value) {
                                var perfil = Ext.getCmp('txt_perfil').getValue();
                                var usuario = Ext.getCmp('txt_usuario').getValue();
                                switch (perfil) {
                                    case "AUTO GESTGAR":
                                        Ext.getCmp('perfilFormAutogestgarOptions').getEl().mask('Cargando...', 'mask-loading');
                                        Ext.Ajax.request({
                                            url: '41parametrizacionusuariosExt.data.jsp',
                                            params: {
                                                accion: 'getParametrosUsuario',
                                                usuario: usuario
                                            },
                                            callback: procesarInformacion
                                        });

                                        break;
                                    case "OPER GESTGAR":
                                        Ext.getCmp('perfilFormOperGestGarOptions').getEl().mask('Cargando...', 'mask-loading');
                                        Ext.Ajax.request({
                                            url: '41parametrizacionusuariosExt.data.jsp',
                                            params: {
                                                accion: 'getParametrosUsuario',
                                                usuario: usuario
                                            },
                                            callback: procesarInformacion
                                        });
                                        break;
                                    case "ADMIN IF GARANT":
                                        Ext.getCmp('perfilFormOperGestGarOptions').getEl().mask('Cargando...', 'mask-loading');
                                        Ext.Ajax.request({
                                            url: '41parametrizacionusuariosExt.data.jsp',
                                            params: {
                                                accion: 'getParametrosUsuario',
                                                usuario: usuario
                                            },
                                            callback: function(opts, success, response) {
                                                var datos = JSON.parse(response.responseText);
                                                var ejecutivo = datos.parametros.ejecutivoAtencion;
                                                storeEjecutivoAtencion.load({
                                                    params: {
                                                        accion: 'usuariosByPerfil',
                                                        perfil: 'SOL GESTGAR'
                                                    },
                                                    callback: function() {
                                                        Ext.getCmp('txt_ejecutivoAtencion').setValue(ejecutivo);
                                                    }
                                                });
                                                procesarInformacion(opts, success, response);
                                            }
                                        });

                                        storeUsuariosPorIF.load({
                                            params: {
                                                accion: 'usuariosPorintermediario',
                                                intermediario: usuario
                                            }
                                        });

                                        gridStoreDocumentos.load({
                                            params: {
                                                accion: 'documentosParametrizados',
                                                intermediario: Ext.getCmp('txt_usuario').getValue()
                                            },
                                            callback: function(records, operation, success) {
                                                usuarioDocumentos = {};
                                                for (var i = 0; i < records.length; i++) {
                                                    var registros = records[i].data.docid.replace(/,/g, "/");
                                                    registros = registros.replace(/ /g, '');
                                                    usuarioDocumentos[records[i].data.idUsuario] = registros.split('/');
                                                }
                                            }
                                        });

                                        break;
                                }

                            }
                        }
                    }
                }, ]
            },
            {
                xtype: 'fieldset',
                id: 'vacio',
                border: false,
                columnWidth: 0.5,
                items: [

                ]
            },
            {
                xtype: 'fieldset',
                id: 'fieldEjecutivoAtencion',
                margin: '0 0 0 0',
                border: false,
                columnWidth: 0.5,
                items: [{
                    xtype: 'combo',
                    name: 'txt_ejecutivoAtencion',
                    id: 'txt_ejecutivoAtencion',
                    fieldLabel: 'Ejecutivo atencion:',
                    padding: 5,
                    labelWidth: 170,
                    width: 400,
                    forceSelection: true,
                    queryMode: 'local',
                    allowBlank: false,
                    emptyText: 'Selecciona',
                    store: storeEjecutivoAtencion,
                    
                    displayField: 'descripcion',
                    valueField: 'clave'
                }, ]
            }
        ]
    });

    // :::::> Perfil form AUTOGESTGAR options <::::://
    var perfilFormAutogestgarOptions = new Ext.form.FormPanel({
            name: "perfilFormAutogestgarOptions",
            id: "perfilFormAutogestgarOptions",
            width: 940,
            frame: true,
            height: "auto",
            title: "El perfil de este usuario podra operar los siguientes documentos: ",
            layout: "column",
            items: [{
                    xtype: "fieldset",
                    height: 370,
                    border: false,
                    columnWidth: 0.5,
                    items: [{
                            xtype: "fieldcontainer",
                            fieldLabel: "Es usuario de jur�dico",
                            labelStyle: "margin-top: 15px;",
                            labelWidth: 170,
                            defaultType: "radiofield",
                            layout: {
                                type: "hbox"
                            },
                            items: [{
                                    xtype: "radiofield",
                                    boxLabel: "Si",
                                    name: "isUsuarioJuridico",
                                    inputValue: "1",
                                    id: "rb_autogestgar_isJuridico_si",
                                    margin: "10 20 0 40"
                                },
                                {
                                    xtype: "radiofield",
                                    boxLabel: "No",
                                    name: "isUsuarioJuridico",
                                    inputValue: "0",
                                    checked: false,
                                    id: "rb_autogestgar_isJuridico_no",
                                    margin: "10 20 0 40"
                                }
                            ]
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "T�rminos y Condiciones ",
                            name: "ch_autogestgar_terminos_condiciones_doc",
                            inputValue: "1001",
                            checked: false,
                            id: "ch_autogestgar_terminos_condiciones",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "fieldcontainer",
                            fieldLabel: "Se requiere firma conjunta",
                            labelStyle: "margin-top: 5px; margin-left:50px;",
                            labelWidth: 170,
                            defaultType: "radiofield",
                            layout: {
                                type: "hbox"
                            },
                            items: [{
                                    xtype: "radiofield",
                                    boxLabel: "Si",
                                    name: "firmaConjunta_bandera",
                                    inputValue: "1",
                                    id: "rb_autogestgar_firma_conjunta_si",
                                    margin: "10 20 0 40",
                                    handler: function(radio, checked) {
                                        if (checked) {
                                            Ext.getCmp('sl_autogestgar_usuario_fc_relacionUsuario').getEl().show();
                                        }
                                    }

                                },
                                {
                                    xtype: "radiofield",
                                    boxLabel: "No",
                                    name: "firmaConjunta_bandera",
                                    inputValue: "0",
                                    checked: false,
                                    id: "rb_autogestgar_firma_conjunta_no",
                                    margin: "10 20 0 40",
                                    handler: function(radio, checked) {
                                        if (checked) {
                                            Ext.getCmp('sl_autogestgar_usuario_fc_relacionUsuario').getEl().hide();
                                        }

                                    }
                                }
                            ]
                        },
                        {
                            xtype: "combo",
                            labelStyle: "margin-top: 5px; margin-left:50px;",
                            labelWidth: 170,
                            name: "sl_autogestgar_usuario_fc_relacionUsuario",
                            id: "sl_autogestgar_usuario_fc_relacionUsuario",
                            fieldLabel: "Selecciona usuario:",
                            padding: 5,
                            width: 300,
                            queryMode: "local",
                            allowBlank: false,
                            emptyText: "Selecciona",
                            store: storeUsuarioManual,
                            displayField: "descripcion",
                            valueField: "clave",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Reglamento Operativo de Garant�a Selectiva ",
                            name: "ch_autogestgar_rogs_doc",
                            inputValue: "1002",
                            checked: false,
                            id: "ch_autogestgar_rogs",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Reglamento Operativo de Garant�a Autom�tica",
                            name: "ch_autogestgar_roga_doc",
                            inputValue: "1003",
                            checked: false,
                            id: "ch_autogestgar_roga",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Ficha de Producto IF",
                            name: "ch_autogestgar_fpgs_doc",
                            inputValue: "1004",
                            checked: false,
                            id: "ch_autogestgar_fpgs",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Ficha de Producto de Garant�a Selectiva",
                            name: "ch_autogestgar_fpgs2_doc",
                            inputValue: "1007",
                            checked: false,
                            id: "ch_autogestgar_fpgs2",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Anexo de Intermediario Financiero",
                            name: "ch_autogestgar_anexo_doc",
                            inputValue: "1006",
                            checked: false,
                            id: "ch_autogestgar_anexo_doc",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Ficha de Producto Nafin",
                            name: "ch_autogestgar_fpga_doc",
                            inputValue: "1005",
                            checked: false,
                            id: "ch_autogestgar_fpga_doc",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Empresarial",
                            name: "ch_autogestgar_fpga_empresarial_portafolio",
                            inputValue: "2001",
                            checked: false,
                            id: "ch_autogestgar_fpga_empresarial",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Sectorial",
                            name: "ch_autogestgar_fpga_sectorial_portafolio",
                            inputValue: "2002",
                            checked: false,
                            id: "ch_autogestgar_fpga_sectorial",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Emergencia",
                            name: "ch_autogestgar_fpga_emergencia_portafolio",
                            inputValue: "2003",
                            checked: false,
                            id: "ch_autogestgar_fpga_emergencia",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Especiales",
                            name: "ch_autogestgar_fpga_emergenciaespecial_portafolio",
                            inputValue: "2004",
                            checked: false,
                            id: "ch_autogestgar_fpga_emergenciaespecial",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Subastas",
                            name: "ch_autogestgar_fpga_subasta_portafolio",
                            inputValue: "2005",
                            checked: false,
                            id: "ch_autogestgar_fpga_subasta",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        }
                    ]
                },
                {
                    xtype: "fieldcontainer",
                    fieldLabel: "Existe usuario suplente",
                    labelStyle: "margin-top: 15px;",
                    padding: 5,
                    labelWidth: 170,
                    columnWidth: 0.5,
                    defaultType: "radiofield",
                    layout: {
                        type: "hbox"
                    },
                    items: [{
                            xtype: "radiofield",
                            boxLabel: "Si",
                            name: "rb_autogestgar_firma_suplente_bandera",
                            inputValue: "1",
                            id: "rb_autogestgar_firma_suplente_si",
                            margin: "10 20 0 40",
                            handler: function(radio, checked) {
                                if (checked) {
                                    Ext.getCmp('sl_autogestgar_usuario_suplente').getEl().show();
                                }
                            }
                        },
                        {
                            xtype: "radiofield",
                            boxLabel: "No",
                            name: "rb_autogestgar_firma_suplente_bandera",
                            inputValue: "0",
                            checked: false,
                            id: "rb_autogestgar_firma_suplente_no",
                            margin: "10 20 0 40",
                            handler: function(radio, checked) {
                                if (checked) {
                                    Ext.getCmp('sl_autogestgar_usuario_suplente').getEl().hide();
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: "fieldset",
                    border: false,
                    columnWidth: 0.5,
                    items: [{
                        xtype: "combo",
                        name: "sl_autogestgar_usuario_suplente",
                        id: "sl_autogestgar_usuario_suplente",
                        fieldLabel: "Usuario Suplente:",
                        labelStyle: "margin-top: 5px; margin-left:0px;",
                        padding: 5,
                        width: 400,
                        forceSelection: false,
                        queryMode: "local",
                        allowBlank: false,
                        emptyText: "Selecciona",
                        store: storeUsuarioManual,
                        displayField: "descripcion",
                        valueField: "clave"
                    }]
                }
            ],
            buttons: [{
                text: 'Guardar',
                id: 'btnGuardar',
                iconCls: 'icoGuardar',
                //formBind: true,
                handler: function(boton, evento) {
                    var perfilFormAutogestgarOptionsformValue = perfilFormAutogestgarOptions.getEl();
                    perfilFormAutogestgarOptionsformValue.mask('Guardando...', 'mask-loading');


                    /*validaciones*/
                    if (Ext.getCmp('txt_usuario').getValue() == null || Ext.getCmp('txt_usuario').getValue() == undefined) {
                        Ext.Msg.alert('Error', 'Es necesario seleccionar un usuario');
                        perfilFormAutogestgarOptionsformValue.unmask();
                        return;
                    }

                    var juridico = Ext.ComponentQuery.query('checkbox[name="isUsuarioJuridico"]');
                    var unoSeleccionadoJurico = false;
                    juridico.forEach(function(valor, indice, array) {
                        if (Ext.getCmp(valor.id).getValue()) {
                            unoSeleccionadoJurico = true;
                        }

                    });




                    var documentos = Ext.ComponentQuery.query('checkbox[requerido="requerido"]')
                    var unoSeleccionadoDocumento = false;

                    documentos.forEach(function(valor, indice, array) {
                        if (Ext.getCmp(valor.id).getValue()) {
                            unoSeleccionadoDocumento = true;
                        }

                    });



                    if (!unoSeleccionadoJurico) {
                        Ext.Msg.alert('Error', 'Es necesario indicar si se usuario jur�dico.');
                        perfilFormAutogestgarOptionsformValue.unmask();
                        return;
                    }

                    if (!unoSeleccionadoDocumento) {
                        Ext.Msg.alert('Error', 'Es necesario seleccionar al menos un documento.');
                        perfilFormAutogestgarOptionsformValue.unmask();
                        return;
                    }



                    var firmaConjunta = Ext.ComponentQuery.query('checkbox[name="firmaConjunta_bandera"]');
                    var unoSeleccionadoFirmaConjunta = false;
                    var firmaSeleccionado = 0;
                    firmaConjunta.forEach(function(valor, indice, array) {
                        if (Ext.getCmp(valor.id).getValue()) {
                            unoSeleccionadoFirmaConjunta = true;
                            firmaSeleccionado = Ext.getCmp(valor.id).inputValue;
                        }

                    });

                    if (unoSeleccionadoFirmaConjunta) {
                        if (firmaSeleccionado == 1 && (Ext.getCmp('sl_autogestgar_usuario_fc_relacionUsuario').getValue() == null || Ext.getCmp('sl_autogestgar_usuario_fc_relacionUsuario').getValue() == undefined)) {
                            Ext.Msg.alert('Error', 'Seleccione usuario de firma conjunta.');
                            perfilFormAutogestgarOptionsformValue.unmask();
                            return;
                        }
                    } else {
                        Ext.Msg.alert('Error', 'Es necesario indicar si se requiere firma conjunta.');
                        perfilFormAutogestgarOptionsformValue.unmask();
                        return;
                    }

                    var suplentes = Ext.ComponentQuery.query('checkbox[name="rb_autogestgar_firma_suplente_bandera"]');
                    var unoSeleccionadoSuplentes = false;
                    var suplenteSeleccionado = 0;
                    suplentes.forEach(function(valor, indice, array) {
                        if (Ext.getCmp(valor.id).getValue()) {
                            unoSeleccionadoSuplentes = true;
                            suplenteSeleccionado = Ext.getCmp(valor.id).inputValue;
                        }

                    });




                    if (unoSeleccionadoSuplentes) {
                        if (suplenteSeleccionado == 1 && (Ext.getCmp('sl_autogestgar_usuario_suplente').getValue() == null || Ext.getCmp('sl_autogestgar_usuario_suplente').getValue() == undefined)) {
                            Ext.Msg.alert('Error', 'Seleccione usuario suplente.');
                            perfilFormAutogestgarOptionsformValue.unmask();
                            return;
                        }
                    } else {
                        Ext.Msg.alert('Error', 'Es necesario indicar si se requiere suplente.');
                        perfilFormAutogestgarOptionsformValue.unmask();
                        return;
                    }

                    /*Fin de validaciones */
                    Ext.Ajax.request({
                        url: '41parametrizacionusuariosExt.data.jsp',
                        params: Ext.apply(perfilFormAutogestgarOptions.getForm().getValues(), {
                            accion: 'guardarActualizar',
                            formulario: JSON.stringify(perfilFormContainer.getForm().getValues()),
                            opciones: JSON.stringify(perfilFormAutogestgarOptions.getForm().getValues())
                        }),
                        callback: function(opts, success, response) {
                            if (success == true && JSON.parse(response.responseText).success == true) {
                                var resp = JSON.parse(response.responseText);
                                Ext.Msg.alert('Status', resp.mensaje);
                                perfilFormAutogestgarOptionsformValue.unmask();
                            } else {
                                NE.util.mostrarConnError(response, opts);
                                perfilFormAutogestgarOptionsformValue.unmask();
                            }
                        }
                    }); /**/




                }
            }]
        }

    );

    // :::::> Perfil form perfilFormOperGestGarOptions options <::::   :// 
    var perfilFormOperGestGarOptions = new Ext.form.FormPanel({
        name: 'perfilFormOperGestGarOptions',
        id: 'perfilFormOperGestGarOptions',
        width: 940,
        frame: true,
        height: 'auto',
        title: 'El perfil de este usuario podra gestionar los siguientes documentos:',
        layout: 'column',
        items: [{
                xtype: "fieldset",
                fieldLabel: "",
                labelStyle: "margin-top: 15px;",
                padding: 5,
                labelWidth: 170,
                columnWidth: 0.5,
                border: false,
                items: [{
                        xtype: "checkboxfield",
                        boxLabel: "Terminos y condiciones",
                        name: "ch_opergestgar_tc_doc",
                        inputValue: "1001",
                        checked: false,
                        id: "ch_opergestgar_terminos_condiciones",
                        margin: "0 0 0 0",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Reglamento Operativo Garant�a Selectiva",
                        name: "ch_autogestgar_rogs_doc",
                        inputValue: "1002",
                        checked: false,
                        id: "ch_opergestgar_rogs",
                        margin: "0 0 0 0",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Reglamento Operativo Garant�a Autom�tica",
                        name: "ch_autogestgar_roga_doc",
                        inputValue: "1003",
                        checked: false,
                        id: "ch_opergestgar_roga",
                        margin: "0 0 0 0",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Ficha de Producto IF",
                        name: "ch_opergestgar_fpgs_doc",
                        inputValue: "1004",
                        checked: false,
                        id: "ch_opergestgar_fpgs",
                        margin: "0 0 0 0",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Ficha de Producto de Garant�a Selectiva",
                        name: "ch_opergestgar_fpgs2_doc",
                        inputValue: "1007",
                        checked: false,
                        id: "ch_opergestgar_fpgs2",
                        margin: "0 0 0 0",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Anexo de Intermediario Financiero",
                        name: "ch_opergestgar_anexo_doc",
                        inputValue: "1006",
                        checked: false,
                        id: "ch_opergestgar_anexo_doc",
                        margin: "0 0 0 0",
                        requerido: "requerido"
                    },                    
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Ficha de Producto Nafin",
                        name: "ch_opergestgar_fpga_doc",
                        inputValue: "1005",
                        checked: false,
                        id: "ch_opergestgar_fpga",
                        margin: "0 0 0 0",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Empresarial",
                        name: "ch_autogestgar_fpga_empresarial_portafolio",
                        inputValue: "2001",
                        checked: false,
                        id: "ch_opergestgar_fpga_empresarial",
                        margin: "0 0 0 50",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Sectorial",
                        name: "ch_autogestgar_fpga_sectorial_portafolio",
                        inputValue: "2002",
                        checked: false,
                        id: "ch_opergestgar_fpga_sectorial",
                        margin: "0 0 0 50",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Emergencia",
                        name: "ch_autogestgar_fpga_emergencia_portafolio",
                        inputValue: "2003",
                        checked: false,
                        id: "ch_opergestgar_fpga_emergencia",
                        margin: "0 0 0 50",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Especiales",
                        name: "ch_autogestgar_fpga_especia_portafolio",
                        inputValue: "2004",
                        checked: false,
                        id: "ch_opergestgar_fpga_emergenciaespecial",
                        margin: "0 0 0 50",
                        requerido: "requerido"
                    },
                    {
                        xtype: "checkboxfield",
                        boxLabel: "Subastas",
                        name: "ch_autogestgar_fpga_subasta_portafolio",
                        inputValue: "2005",
                        checked: false,
                        id: "ch_opergestgar_fpga_subasta",
                        margin: "0 0 0 50",
                        requerido: "requerido"
                    }

                ]
            },
            {
                xtype: "fieldset",
                border: false,
                columnWidth: 0.5,
                items: [{
                        xtype: "fieldcontainer",
                        fieldLabel: "Supervisor",
                        labelStyle: "margin-top: 15px;",
                        padding: 5,
                        labelWidth: 170,
                        columnWidth: 0.5,
                        defaultType: "radiofield"
                    },
                    {
                        xtype: "combo",
                        name: "sl_opergestgar_perfil_supervisor",
                        id: "sl_opergestgar_perfil_supervisor",
                        fieldLabel: "Perfil:",
                        labelStyle: "margin-top: 5px; margin-left:0px;",
                        padding: 5,
                        width: 400,
                        forceSelection: true,
                        queryMode: "local",
                        allowBlank: false,
                        editable: false,
                        emptyText: "Selecciona",
                        store: storePerfilSupervisor,
                        displayField: "descripcion",
                        valueField: "clave",
                        listeners: {
                            'select': {
                                fn: function(combo, value) {
                                    var perfil = combo.getValue();
                                    storeUsuarioSuplente.load({
                                        params: {
                                            accion: 'usuariosByPerfil',
                                            perfil: perfil
                                        }
                                    });

                                }


                            }
                        }
                    },
                    {
                        xtype: "combo",
                        name: "sl_opergestgar_usuario_supervisor",
                        id: "sl_opergestgar_usuario_supervisor",
                        fieldLabel: "Usuario:",
                        labelStyle: "margin-top: 5px; margin-left:0px;",
                        padding: 5,
                        width: 400,
                        forceSelection: true,
                        queryMode: "local",
                        editable: false,
                        allowBlank: false,
                        emptyText: "Selecciona",
                        store: storeUsuarioSuplente,
                        displayField: "descripcion",
                        valueField: "clave"
                    }
                ]
            },
            {
                xtype: "fieldcontainer",
                fieldLabel: "",
                labelStyle: "margin-top: 15px;",
                padding: 5,
                labelWidth: 170,
                columnWidth: 1,
                layout: {
                    type: 'hbox',
                    pack: 'center',
                    align: 'center'
                },
                items: [{
                        xtype: "radiofield",
                        boxLabel: "IF Bancario",
                        name: "ifb",
                        inputValue: "B",
                        id: "rb_opergestgar_isIfb_si",
                        margin: "10 20 0 40",
                        handler: function(radio, checked) {
                            if (checked) {
                                seleccionados = JSON.stringify(itemSelectorIf.getValue());
                                Ext.getCmp('itemSelectorIf').store.removeAll();
                                Ext.getCmp('itemSelectorIf').reset();
                                storeIF.load({
                                    params: {
                                        accion: 'getIF',
                                        seleccionados: seleccionados,
                                        tipo: 'B',
                                        usuario: Ext.getCmp('txt_usuario').getValue()
                                    },
                                    callback: function(records, operation, success) {
                                        Ext.getCmp('itemSelectorIf').setValue(JSON.parse(seleccionados));
                                    }
                                });
                                Ext.getCmp('itemSelectorIf').bindStore(storeIF);
                            }
                        }
                    },
                    {
                        xtype: "radiofield",
                        boxLabel: "IF no Bancario",
                        name: "ifb",
                        inputValue: "NB",
                        checked: false,
                        id: "rb_opergestgar_isIfb_no",
                        margin: "10 20 0 40",
                        handler: function(radio, checked) {
                            if (checked) {
                                seleccionados = JSON.stringify(itemSelectorIf.getValue());
                                Ext.getCmp('itemSelectorIf').store.removeAll();
                                Ext.getCmp('itemSelectorIf').reset();
                                storeIF.load({
                                    params: {
                                        accion: 'getIF',
                                        seleccionados: seleccionados,
                                        tipo: 'NB',
                                        usuario: Ext.getCmp('txt_usuario').getValue()
                                    },
                                    callback: function(records, operation, success) {
                                        Ext.getCmp('itemSelectorIf').setValue(JSON.parse(seleccionados));
                                    }
                                });
                                Ext.getCmp('itemSelectorIf').bindStore(storeIF);
                            }
                        }
                    }
                ]

            },
            {
                xtype: "fieldset",
                border: false,
                columnWidth: 1,
                layout: {
                    type: 'vbox',
                    pack: 'center',
                    align: 'center'
                },
                items: [
                    ifPanel
                ]
            }
        ],
        buttons: [{
            text: 'Guardar',
            id: 'btnGuardarOpergestgar',
            iconCls: 'icoGuardar',
            //formBind: true,
            handler: function(boton, evento) {
                var perfilFormOpergestgarOptionsformValue = perfilFormOperGestGarOptions.getEl();
                perfilFormOpergestgarOptionsformValue.mask('Guardando...', 'mask-loading');

                /*VALIDACIONES*/
                if (Ext.getCmp('txt_usuario').getValue() == null || Ext.getCmp('txt_usuario').getValue() == undefined) {
                    Ext.Msg.alert('Error', 'Es necesario seleccionar un usuario');
                    perfilFormOpergestgarOptionsformValue.unmask();
                    return;
                }

                var documentos = Ext.ComponentQuery.query('checkbox[requerido="requerido"]')
                var unoSeleccionadoDocumento = false;

                documentos.forEach(function(valor, indice, array) {
                    if (Ext.getCmp(valor.id).getValue()) {
                        unoSeleccionadoDocumento = true;
                    }

                });


                if (!unoSeleccionadoDocumento) {
                    Ext.Msg.alert('Error', 'Es necesario seleccionar al menos un documento.');
                    perfilFormOpergestgarOptionsformValue.unmask();
                    return;
                }

                /*FIN VALIDACIONES*/
                Ext.Ajax.request({
                    url: '41parametrizacionusuariosExt.data.jsp',
                    params: {
                        accion: 'guardarActualizar',
                        formulario: JSON.stringify(perfilFormContainer.getForm().getValues()),
                        opciones: JSON.stringify(perfilFormOperGestGarOptions.getForm().getValues())
                    },
                    callback: function(opts, success, response) {
                        if (success == true && JSON.parse(response.responseText).success == true) {
                            var resp = JSON.parse(response.responseText);
                            Ext.Msg.alert('Status', resp.mensaje);
                            perfilFormOpergestgarOptionsformValue.unmask();
                        } else {
                            NE.util.mostrarConnError(response, opts);
                            perfilFormOpergestgarOptionsformValue.unmask();
                        }
                    }
                }); /**/
            }
        }]
    });

    /*
    	Grid documentos
    */

    Ext.define('usuarioDocumentosModel', {
        extend: 'Ext.data.Model',
        fields: [{
                name: 'usuario',
                mapping: 'nombreCompleto'
            },
            {
                name: 'documento',
                mapping: 'documento'
            },
            {
                name: 'portafolio',
                mapping: 'portafolio'
            },
            {
                name: 'correo',
                mapping: 'email'
            },
            {
                name: 'edicion',
                mapping: 'edicion'
            },
            {
                name: 'idUsuario',
                mapping: 'icUsuario'
            },
            {
                name: 'docid',
                mapping: 'docId'
            }
        ]
    });


    var gridStoreDocumentos = Ext.create('Ext.data.Store', {
        model: 'usuarioDocumentosModel',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '41parametrizacionusuariosExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });


    var gridDocumentos = Ext.create('Ext.grid.Panel', {
        id: 'gridDocumentos',
        store: gridStoreDocumentos,
        stripeRows: true,
        title: 'Usuarios parametrizados',
        width: 900,
        height: 280,
        collapsible: false,
        enableColumnMove: false,
        frame:true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
                header: "Usuario",
                dataIndex: 'usuario',
                width: 220,
                sortable: false,
                hideable: false
            },
            {
                header: "Documentos",
                dataIndex: 'documento',
                width: 200,
                sortable: false,
                hideable: false,
                renderer: function(value) {
                    return '<span data-qtitle="Documentos" data-qwidth="200" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }
            },
            {
                header: "Portafolios",
                dataIndex: 'portafolio',
                width: 200,
                sortable: false,
                hideable: false,
                renderer: function(value) {
                    return '<span data-qtitle="Portafolios" data-qwidth="200" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }
            },
            {
                header: "Correo",
                dataIndex: 'correo',
                width: 200,
                sortable: false,
                hideable: false
            },
            {
                xtype: 'actioncolumn',
                header: 'Edici�n',
                tooltip: 'Eliminar',
                dataIndex: '',
                width: 50,
                align: 'center',
                items: [{
                    getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
                        if (registro.get('usuario') != '') {
                            this.items[0].tooltip = 'Eliminar';
                            return 'icoEliminar';
                        }
                    },
                    handler: function(grid, rowIndex, colIndex) {
                        Ext.MessageBox.confirm('Eliminar', '�Estas seguro que deseas quitar al usuario?', function(btn) {
                            if (btn === 'yes') {
                                usuarioDocumentos[grid.getStore().getAt(rowIndex).data.idUsuario] = [];
                                var estor = grid.getStore();
                                estor.removeAt(rowIndex);
                            }
                        });
                    }
                }]
            }
        ]
    });




    // :::::> Perfil form perfilFormAdminIfGestgarOptions options XXX <::::://
    var perfilFormAdminIfGestgarOptions = new Ext.form.FormPanel({
        name: 'perfilFormAdminIfGestgarOptions',
        id: 'perfilFormAdminIfGestgarOptions',
        width: 940,
        frame: true,
        height: 895,
        title: 'El perfil de este usuario podra operar los siguientes documentos:',
        layout: {
            align: 'center',
            pack: 'center'
        },
        items: [{
                xtype: 'fieldset',
                id: 'fieldDiasHabiles',
                height: 40,
                border: false,
                columnWidth: 0.5,
                layout: {
                    type: 'hbox'
                },
                items: [{
                    xtype: 'numberfield',
                    minValue: 1,
                    name: 'txt_diasHabiles',
                    id: 'txt_diasHabiles',
                    fieldLabel: 'D�as H�biles M�ximos para Firma de Documentos:',
                    labelWidth: 350,
                    padding: 5,
                    width: 500,
                    value: 1,
                    forceSelection: true,
                    queryMode: 'local',
                    editable:false,
                    allowBlank: false,
                    emptyText: '',
                    displayField: 'descripcion',
                    valueField: 'clave'
                    

                }]
            },
            {
                xtype: 'fieldset',
                id: 'opcionesIfUsuario',
                height: 480,
                border: false,
                columnWidth: 0.5,
                layout: {
                    type: 'hbox'
                },
                items: [{
                    xtype: 'panel',
                    title: 'Par�metros Para el Intermediario',
                    bodyPadding: 2,
                    width: 450,
                    height: 480,
                    frame: true,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [{
                            xtype: "fieldcontainer",
                            fieldLabel: "Se Requiere Firma Mancomunada por el IF",
                            labelStyle: "margin-top: 15px;",
                            labelWidth: 170,
                            defaultType: "radiofield",
                            layout: {
                                type: "hbox"
                            },
                            items: [{
                                    xtype: "radiofield",
                                    boxLabel: "Si",
                                    name: "firmaMancomunada",
                                    inputValue: "1",
                                    id: "rb_adminIf_firmaMancomunada_si",
                                    margin: "10 20 0 40",
                                    handler: function(radio, checked) {
                                        if (checked) {
                                            Ext.getCmp('txt_numeroFirmas').getEl().show();
                                        }
                                    }
                                },
                                {
                                    xtype: "radiofield",
                                    boxLabel: "No",
                                    name: "firmaMancomunada",
                                    inputValue: "0",
                                    checked: false,
                                    id: "rb_adminIf_firmaMancomunada_no",
                                    margin: "10 20 0 40",
                                    handler: function(radio, checked) {
                                        if (checked) {
                                            Ext.getCmp('txt_numeroFirmas').setValue(1);
                                            Ext.getCmp('txt_numeroFirmas').getEl().hide();
                                            
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'numberfield',
                            minValue: 1,
                            maxValue: 99,
                            name: 'txt_numeroFirmas',
                            id: 'txt_numeroFirmas',
                            fieldLabel: 'M�nimo de Firmas:',
                            labelWidth: 150,
                            padding: 5,
                            width: 200,
                            value: 1,
                            forceSelection: true,
                            queryMode: 'local',
                            allowBlank: true,
                            emptyText: '',
                            valueField: 'clave'
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Terminos y condiciones",
                            name: "ch_adminif_tc_doc",
                            inputValue: "1001",
                            checked: false,
                            id: "ch_adminif_terminos_condiciones",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Reglamento Operativo Garant�a Selectiva",
                            name: "ch_adminif_rogs_doc",
                            inputValue: "1002",
                            checked: false,
                            id: "ch_adminif_rogs",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Reglamento Operativo Garant�a Autom�tica",
                            name: "ch_adminif_roga_doc",
                            inputValue: "1003",
                            checked: false,
                            id: "ch_adminif_roga",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Ficha de Producto IF",
                            name: "ch_adminif_fpgs_doc",
                            inputValue: "1004",
                            checked: false,
                            id: "ch_adminif_fpgs",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Ficha de Producto de Garant�a Selectiva",
                            name: "ch_adminif_fpgs2_doc",
                            inputValue: "1007",
                            checked: false,
                            id: "ch_adminif_fpgs2",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Anexo de Intermediario Financiero",
                            name: "ch_adminif_anexo_doc",
                            inputValue: "1006",
                            checked: false,
                            id: "ch_adminif_anexo_doc",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },                        
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Ficha de Producto Nafin",
                            name: "ch_adminif_fpga_doc",
                            inputValue: "1005",
                            checked: false,
                            id: "ch_adminif_fpga",
                            margin: "0 0 0 0",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Empresarial",
                            name: "ch_adminif_fpga_empresarial_portafolio",
                            inputValue: "2001",
                            checked: false,
                            id: "ch_adminif_fpga_empresarial",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Sectorial",
                            name: "ch_adminif_fpga_sectorial_portafolio",
                            inputValue: "2002",
                            checked: false,
                            id: "ch_adminif_fpga_sectorial",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Emergencia",
                            name: "ch_adminif_fpga_emergencia_portafolio",
                            inputValue: "2003",
                            checked: false,
                            id: "ch_adminif_fpga_emergencia",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Especiales",
                            name: "ch_adminif_fpga_especia_portafolio",
                            inputValue: "2004",
                            checked: false,
                            id: "ch_adminif_fpga_emergenciaespecial",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Subastas",
                            name: "ch_adminif_fpga_subasta_portafolio",
                            inputValue: "2005",
                            checked: false,
                            id: "ch_adminif_fpga_subasta",
                            margin: "0 0 0 50",
                            requerido: "requerido"
                        },
                        {
                            xtype: 'form',
                            id: 'formFile',
                            name: 'formFile',
                            frame: true,
                            border: false,
                            items: [{
                                xtype: 'filefield',
                                id: 'archivo',
                                emptyText: 'Nombre del Archivo',
                                fieldLabel: 'Cargar contrato aut�grafo',
                                name: 'archivoSoporte',
                                buttonText: 'Cargar archivo',
                                labelWidth: '45%',
                                width: 400,
                                padding: '10 0 10 0',
                                buttonCfg: {
                                    iconCls: 'upload-icon'
                                },
                                vtype: 'archivoPdf',
                                listeners: {
                                    change: function(fld, value) {
                                        fld.setRawValue(value.substring(value.lastIndexOf("\\") + 1));
                                        if (fld.isValid()) {
                                            cargaArchivo('CARGAR_ARCHIVO', null);
                                        }
                                    }
                                }
                            }, {
                                xtype: 'hiddenfield',
                                id: 'icDocumento',
                                name: 'icDocumento',
                                value: icDoc
                            }]
                        },
                        {
                            xtype: 'form',
                            id: 'formButtonsFile',
                            name: 'formButtonsFile',
                            frame:true,
                            border: 0,
                            items: []
                        }

                    ]

                }, {
                    xtype: 'panel',
                    title: 'Par�metros para el Usuario',
                    bodyPadding: 2,
                    width: 450,
                    height: 480,
                    frame:true,
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [{
                            xtype: 'combo',
                            name: 'cb_adminifusuario',
                            id: 'cb_adminifusuario',
                            fieldLabel: 'Usuario:',
                            padding: 5,
                            width: 400,
                            forceSelection: true,
                            queryMode: 'local',
                            allowBlank: true,
                            emptyText: 'Selecciona',
                            store: storeUsuariosPorIF,
                            displayField: 'descripcion',
                            valueField: 'clave'

                        },
                        {
                            xtype: 'label',
                            text: 'Selecciona los documentos que puede formalizar este usuario:',
                            margin: '30 30 30 30'
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Terminos y condiciones",
                            name: "ch_adminifUsuario_tc_doc",
                            inputValue: "1001",
                            checked: false,
                            id: "ch_adminifUsuario_terminos_condiciones",
                            margin: "0 0 0 0",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Reglamento Operativo Garant�a Selectiva",
                            name: "ch_adminifUsuario_rogs_doc",
                            inputValue: "1002",
                            checked: false,
                            id: "ch_adminifUsuario_rogs",
                            margin: "0 0 0 0",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Reglamento Operativo Garant�a Autom�tica",
                            name: "ch_adminifUsuario_roga_doc",
                            inputValue: "1003",
                            checked: false,
                            id: "ch_adminifUsuario_roga",
                            margin: "0 0 0 0",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Ficha de Producto IF",
                            name: "ch_adminifUsuario_fpgs_doc",
                            inputValue: "1004",
                            checked: false,
                            id: "ch_adminifUsuario_fpgs",
                            margin: "0 0 0 0",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Ficha de Producto de Garant�a Selectiva",
                            name: "ch_adminifUsuario_fpgs2_doc",
                            inputValue: "1007",
                            checked: false,
                            id: "ch_adminifUsuario_fpgs2",
                            margin: "0 0 0 0",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Anexo de Intermediario Financiero",
                            name: "ch_adminifUsuario_anexo_doc",
                            inputValue: "1006",
                            checked: false,
                            id: "ch_adminifUsuario_anexo_doc",
                            margin: "0 0 0 0",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },                        
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Ficha de Producto Nafin",
                            name: "ch_adminifUsuario_fpga_doc",
                            inputValue: "1005",
                            checked: false,
                            id: "ch_adminifUsuario_fpga",
                            margin: "0 0 0 0",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Empresarial",
                            name: "ch_adminifUsuario_fpga_empresarial_portafolio",
                            inputValue: "2001",
                            checked: false,
                            id: "ch_adminifUsuario_fpga_empresarial",
                            margin: "0 0 0 50",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Sectorial",
                            name: "ch_adminifUsuario_fpga_sectorial_portafolio",
                            inputValue: "2002",
                            checked: false,
                            id: "ch_adminifUsuario_fpga_sectorial",
                            margin: "0 0 0 50",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Emergencia",
                            name: "ch_adminifUsuario_fpga_emergencia_portafolio",
                            inputValue: "2003",
                            checked: false,
                            id: "ch_adminifUsuario_fpga_emergencia",
                            margin: "0 0 0 50",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Especiales",
                            name: "ch_adminifUsuario_fpga_especia_portafolio",
                            inputValue: "2004",
                            checked: false,
                            id: "ch_adminifUsuario_fpga_emergenciaespecial",
                            margin: "0 0 0 50",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: "checkboxfield",
                            boxLabel: "Subastas",
                            name: "ch_adminifUsuario_fpga_subasta_portafolio",
                            inputValue: "2005",
                            checked: false,
                            id: "ch_adminifUsuario_fpga_subasta",
                            margin: "0 0 0 50",
                            requerido: "requeridoUsuario",
                            campousuario: "campousuario"
                        },
                        {
                            xtype: 'button',
                            width: 100,
                            text: 'Agregar',
                            handler: function() {
                                var documentosPortafolios = Ext.ComponentQuery.query('checkbox[campousuario="campousuario"]');
                                var usuario = Ext.getCmp("cb_adminifusuario").getValue();

                                if (usuario == null) {
                                    Ext.Msg.alert('Error', 'Es necesario seleccionar un usuario.');
                                    return;
                                }

                                var documentosUsuario = Ext.ComponentQuery.query('checkbox[requerido="requeridoUsuario"]')
                                var unoSeleccionadoDocumento = false;

                                documentosUsuario.forEach(function(valor, indice, array) {
                                    if (Ext.getCmp(valor.id).getValue()) {
                                        unoSeleccionadoDocumento = true;
                                    }

                                });

                                if (!unoSeleccionadoDocumento) {
                                    Ext.Msg.alert('Error', 'Es necesario seleccionar al menos un documento o portafolio.');
                                    perfilFormOpergestgarOptionsformValue.unmask();
                                    return;
                                }

                                var documentos = [];
                                var documentosif = "";
                                var documentosif2 = "";
                                var portafolioIF = "";
                                for (var i = 0; i < documentosPortafolios.length; i++) {
                                    if (documentosPortafolios[i].checked) {
                                        documentosif += documentosPortafolios[i].boxLabel + "/";
                                        documentos.push(documentosPortafolios[i].inputValue);
                                        
                                        
                                        var nombre = String(documentosPortafolios[i].getName());
                                        if(nombre.endsWith("_doc")){
                                            documentosif2 += documentosPortafolios[i].boxLabel+", ";
                                        }
                                        if(nombre.endsWith("_portafolio")){
                                            portafolioIF += documentosPortafolios[i].boxLabel+", ";
                                        }
                                    }
                                }
                                portafolioIF =  portafolioIF.substring(0, portafolioIF.length -2);
                                documentosif2 = documentosif2.substring(0, documentosif2.length -2);
                                usuarioDocumentos[usuario] = documentos;  // Esta variable es la que se envia para guardar
                                Ext.Ajax.request({
                                    url: '41parametrizacionusuariosExt.data.jsp',
                                    params: {
                                        accion: 'getCorreoUsuario',
                                        usuario: usuario
                                    },
                                    callback: function(opts, success, response) {
                                        if (success) {
                                            var existe = false;
                                            var correo = JSON.parse(response.responseText).correo;
                                            var parametrizacion = {
                                                'usuario': Ext.getCmp("cb_adminifusuario").rawValue,
                                                'documento': documentosif2,
                                                'portafolio': portafolioIF,
                                                'correo': correo,
                                                'edicion': 'edicion12'
                                            };
                                            Ext.getCmp("gridDocumentos").store.each(function(record) {
                                                for (var rd in record.data) {
                                                    var fName = rd;
                                                    var fValue = record.data[rd];

                                                    if (fName == 'usuario') {
                                                        if (fValue == Ext.getCmp("cb_adminifusuario").rawValue) {
                                                            existe = true;
                                                        }
                                                    }
                                                }

                                            }, this);

                                            if (!existe) {
                                                Ext.getCmp("gridDocumentos").store.insert(0, parametrizacion);
                                            } else {
                                                Ext.Msg.alert('Error', 'El usuario ' + Ext.getCmp("cb_adminifusuario").rawValue + ' ya se ha parametrizado');
                                            }
                                        }
                                    }
                                });

                                documentosUsuario.forEach(function(valor, indice, array) {
                                    Ext.getCmp(valor.id).setValue(false);

                                });

                            }
                        }
                    ]

                }]
            }, {
                xtype: 'fieldset',
                id: 'grid',
                height: 400,
                border: false,
                columnWidth: 0.5,
                layout: {
                    type: 'hbox'
                },
                items: [
                    gridDocumentos
                ]
            }
        ],
        buttons: [{
            text: 'Guardar',
            id: 'btnGuardarAdminGestGar',
            iconCls: 'icoGuardar',
            handler: function(boton, evento) {
                var perfilFormOpergestgarOptionsformValue = perfilFormAdminIfGestgarOptions.getEl();
                perfilFormOpergestgarOptionsformValue.mask('Guardando...', 'mask-loading');

                if (Ext.getCmp('txt_usuario').getValue() == null || Ext.getCmp('txt_usuario').getValue() == undefined) {
                    Ext.Msg.alert('Error', 'Es necesario seleccionar un intermediario financiero');
                    perfilFormOpergestgarOptionsformValue.unmask();
                    return;
                }

                if (Ext.getCmp('txt_ejecutivoAtencion').getValue() == null || Ext.getCmp('txt_ejecutivoAtencion').getValue() == undefined) {
                    Ext.Msg.alert('Error', 'Es necesario seleccionar un ejecutivo de atenci�n');
                    perfilFormOpergestgarOptionsformValue.unmask();
                    return;
                }

                if (Ext.getCmp('rb_adminIf_firmaMancomunada_si').value) {
                    if (Ext.getCmp("gridDocumentos").store.data.length < Number(Ext.getCmp('txt_numeroFirmas').getValue())) {
                        Ext.Msg.alert('Error', 'El numero de usuarios parametrizados debe ser igual o mayor al minimo de firmas requeridas');
                        perfilFormOpergestgarOptionsformValue.unmask();
                        return;
                    }
                }
                var documentos = Ext.ComponentQuery.query('checkbox[requerido="requerido"]')
                var unoSeleccionadoDocumento = false;

                documentos.forEach(function(valor, indice, array) {
                    if (Ext.getCmp(valor.id).getValue()) {
                        unoSeleccionadoDocumento = true;
                    }

                });


                if (!unoSeleccionadoDocumento) {
                    Ext.Msg.alert('Error', 'Es necesario seleccionar al menos un documento o portafolio.');
                    perfilFormOpergestgarOptionsformValue.unmask();
                    return;
                }
                Ext.Ajax.request({
                    url: '41parametrizacionusuariosExt.data.jsp',
                    params: Ext.apply(perfilFormAdminIfGestgarOptions.getForm().getValues(), {
                        accion: 'guardarActualizar',
                        formulario: JSON.stringify(perfilFormContainer.getForm().getValues()),
                        opciones: JSON.stringify(perfilFormAdminIfGestgarOptions.getForm().getValues()),
                        usuarioDocPor: JSON.stringify(usuarioDocumentos)
                    }),
                    callback: function(opts, success, response) {
                        if (success == true && JSON.parse(response.responseText).success == true) {
                            var resp = JSON.parse(response.responseText);
                            Ext.Msg.alert('Status', resp.mensaje);
                            perfilFormOpergestgarOptionsformValue.unmask();
                        } else {
                            NE.util.mostrarConnError(response, opts);
                            perfilFormOpergestgarOptionsformValue.unmask();
                        }
                    }
                });

            }
        }]
    });




    // :::::> CONTENEDOR PRINCIPAL <::::://
    var pnl = new Ext.Container({
        id: 'contenedorPrincipal',
        renderTo: 'areaContenido',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            NE.util.getEspaciador(20),
            perfilFormContainer,
            NE.util.getEspaciador(30),
            perfilFormAutogestgarOptions,
            perfilFormOperGestGarOptions,
            perfilFormAdminIfGestgarOptions,
            NE.util.getEspaciador(20),
        ]
    });
    storePerfil.load();


/** Implementacion de la funcion endWiths para navegadores que no lo soportan IE10*/
if (!String.prototype.endsWith) { 
    String.prototype.endsWith = function(searchString, position) { 
     var subjectString = this.toString(); 
     if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) { 
     position = subjectString.length; 
     } 
     position -= searchString.length; 
     var lastIndex = subjectString.lastIndexOf(searchString, position); 
     return lastIndex !== -1 && lastIndex === position; 
    }; 
} 

});