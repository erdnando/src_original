<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.*,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.netro.seguridad.*,
        netropology.utilerias.usuarios.UtilUsr,
        netropology.utilerias.usuarios.Usuario,
        netropology.utilerias.ElementoCatalogo,
        com.nafin.docgarantias.parametrizacion.usuarios.*,
        com.nafin.docgarantias.parametrizacion.usuarios.model.*,"
        
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    ConsUsuariosNafinSeg consUsuariosNafinSeg = new  ConsUsuariosNafinSeg();
    
    ParametrizacionUsuarios parametrizacionUsuarios = ServiceLocator.getInstance().lookup("ParametrizacionUsuariosBean", ParametrizacionUsuarios.class);
    
    private String usuariosByPerfil(String perfil, Integer icIF) throws Exception{    
        ArrayList<Usuario> listaUsuarios = null;
        if(perfil.equals("ADMIN IF GARANT")){
            listaUsuarios = parametrizacionUsuarios.getUsuariosIFPorPerfil(perfil, icIF);
        }
        else{
            listaUsuarios = parametrizacionUsuarios.getUsuariosNafinPorPerfil(perfil);
        }
        JSONArray jsonArr = new JSONArray();        
        Iterator it = listaUsuarios.iterator();        
        for (Usuario usuario : listaUsuarios){
            jsonArr.add(JSONObject.fromObject(new ElementoCatalogo(usuario.getLogin(), usuario.getNombreCompleto())));
        }    
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";            
    }
    
    private String usuariosByIntermediario(String intermediario) throws Exception{    
        ArrayList<ElementoCatalogo> listaUsuarios = parametrizacionUsuarios.getUsuariosPorIntermediario(intermediario);
        JSONArray jsonArr = new JSONArray();        
        Iterator it = listaUsuarios.iterator();    
        for(ElementoCatalogo e : listaUsuarios){
            jsonArr.add(e);   
        }
        
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";               
    }
    
    private String usuarioParametrizadosPorintermediario(String intermediario) throws Exception{    
        ArrayList<GridConsultaDocumentosParametrizadosVo> listaUsuariosParametrizados = parametrizacionUsuarios.getUsuariosParametrizadosPorIntermediario(intermediario);
        JSONArray jsonArr = new JSONArray();        
        Iterator it = listaUsuariosParametrizados.iterator();    
        for(GridConsultaDocumentosParametrizadosVo e : listaUsuariosParametrizados){
            jsonArr.add(e);   
        }        
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";               
    }
    
    private String perfilByUsuario(String usuario) throws Exception{                   
        return consUsuariosNafinSeg.getPerfilByUsuario(usuario);          
    }
    
    private String ifByTipo(String tipo,  ArrayList<String> seleccionados, String usuario) throws Exception{    
        ArrayList listaIF = parametrizacionUsuarios.getIFs(tipo, seleccionados, usuario);
        JSONArray jsonArr = new JSONArray();        
        Iterator it = listaIF.iterator();        
        while(it.hasNext()) {
                Object obj = it.next();
                ElementoCatalogo ec = (ElementoCatalogo)obj;
                jsonArr.add(JSONObject.fromObject(ec));
        }        
        
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";            
    }
    
    
    private String getPerfilesParametrizar(){
        JSONArray jsonArr = new JSONArray();
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("AUTO GESTGAR", "AUTO GESTGAR")));
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("OPER GESTGAR", "OPER GESTGAR")));
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("ADMIN IF GARANT", "ADMIN IF GARANT")));
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
    }
    
    private String getPerfilesSupervisores(){
        JSONArray jsonArr = new JSONArray();
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("AUTO GESTGAR", "AUTO GESTGAR")));
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("ADMIN GESTGAR", "ADMIN NAFIN GESTGAR")));
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("SOL GESTGAR", "SOL GESTGAR")));
        jsonArr.add(JSONObject.fromObject(new ElementoCatalogo("OPER GESTGAR", "OPER GESTGAR")));
        return "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";    
    }
%>
<%
       
    String accion = (request.getParameter("accion") != null) ? request.getParameter("accion") : "";    
    String infoRegresar="";    
    log.info(":::> accion: " + accion);
    JSONObject jsonObj   =  new JSONObject();    
    jsonObj.put("success", new Boolean(true));
    switch(accion){
        case "perfilParametrizar":
            infoRegresar = getPerfilesParametrizar();
        break;
        case "perfilSupervisor":
            infoRegresar = getPerfilesSupervisores();
        break;        
        case "usuariosByPerfil":
            String perfil = (request.getParameter("perfil") != null) ? request.getParameter("perfil") : "";
            Integer icIF = (request.getParameter("intermediario") != null) ? Integer.valueOf(request.getParameter("intermediario")) : null;
            infoRegresar = usuariosByPerfil(perfil, icIF);
        break;
        case "guardarActualizar": 
            Map parametrosUsuarios = new HashMap();
            Map parametrosUsuariosDocumentos = new HashMap();
            log.info("accion case: "+accion);
            String formulario = (request.getParameter("formulario") != null) ? request.getParameter("formulario") : "";
            JSONObject jsonObject = JSONObject.fromObject(formulario);
            Map mapUsuario = jsonObject;
            
            formulario = (request.getParameter("opciones") != null) ? request.getParameter("opciones") : "{}";
            jsonObject = JSONObject.fromObject(formulario);
            Map mapOpciones = jsonObject;
            
            if(mapUsuario.get("txt_perfil").toString().equals("ADMIN IF GARANT")){
                String usuarioDocumentos = (request.getParameter("usuarioDocPor") != null) ? request.getParameter("usuarioDocPor") : "";
                jsonObject = JSONObject.fromObject(usuarioDocumentos);
                parametrosUsuariosDocumentos = jsonObject;
                mapOpciones.put("usuarioDocPor", parametrosUsuariosDocumentos);
            }
            parametrosUsuarios.put("usuario", mapUsuario);
            parametrosUsuarios.put("parametros", mapOpciones);
            parametrizacionUsuarios.saveOrUpdateUser(parametrosUsuarios);
            
            jsonObj.put("mensaje", "Informacion actualizada correctamente"); 
            infoRegresar = jsonObj.toString();
        break;
        case "getParametrosUsuario":
            log.info("accion case: " + accion);
            String usuario = (request.getParameter("usuario") != null) ? request.getParameter("usuario") : "";
            Map parametros = parametrizacionUsuarios.getParametrosUsuario(usuario);
            jsonObj.put("parametros", JSONObject.fromObject(parametros)); 
            infoRegresar = jsonObj.toString();
            
        break;
        case "getPerfilByUsuario":
                String usuarioSuplente = (request.getParameter("usuarioSuplente") != null) ? request.getParameter("usuarioSuplente") : "";
                JSONObject jsonObjPerfil   =  new JSONObject();    
                jsonObjPerfil.put("success", new Boolean(true));
                 jsonObjPerfil.put("perfil", perfilByUsuario(usuarioSuplente));
                infoRegresar = jsonObjPerfil.toString();
            break;
        case "getIF":
                log.info("accion case: " + accion);
                String tipo = (request.getParameter("tipo") != null) ? request.getParameter("tipo") : "";  
                String seleccionados = (request.getParameter("seleccionados") != null) ? request.getParameter("seleccionados") : "[]";  
                String usuarioParametrizado = (request.getParameter("usuario") != null) ? request.getParameter("usuario") : "";
                
                JSONArray nameArray = (JSONArray) JSONSerializer.toJSON(seleccionados);              
                
                ArrayList<String> list = new ArrayList<String>();     
             
                if (nameArray != null) { 
                   int len = nameArray.size();
                   for (int i=0;i<len;i++){ 
                    list.add(nameArray.get(i).toString());
                   } 
                } 
                
                infoRegresar = ifByTipo(tipo , list, usuarioParametrizado);
            break;
        case "usuariosPorintermediario":
            String intermediario =  (request.getParameter("intermediario") != null) ? request.getParameter("intermediario") : "-";  
            infoRegresar = usuariosByIntermediario(intermediario);
            break;
        case "getCorreoUsuario":
            String usuarioID = (request.getParameter("usuario") != null) ? request.getParameter("usuario") : "";
            JSONObject jsonObjCorreoUsuario   =  new JSONObject();    
            jsonObjCorreoUsuario.put("success", new Boolean(true));
            jsonObjCorreoUsuario.put("correo", consUsuariosNafinSeg.getUsuarioById(usuarioID).getEmail());
            infoRegresar = jsonObjCorreoUsuario.toString();
        break;
        case "updateDocumento":
            String usuarioDocumento = (request.getParameter("usuario") != null) ? request.getParameter("usuario") : "";
            String icArchivo = (request.getParameter("icArchivo") != null) ? request.getParameter("icArchivo") : "";
            
            String opcion = (request.getParameter("opcion") != null) ? request.getParameter("opcion") : "";
            if(opcion.equals("eliminar")){
                icArchivo = "0";
            }
            
            parametrizacionUsuarios.updateDocumentoIf(usuarioDocumento, icArchivo);
            
            JSONObject jsonObjUpdateDocumento   =  new JSONObject();    
            jsonObjUpdateDocumento.put("success", new Boolean(true));            
            infoRegresar = jsonObjUpdateDocumento.toString();
            break;
        case "documentosParametrizados":
            String intermediarioConsulta = (request.getParameter("intermediario") != null) ? request.getParameter("intermediario") : "";
            infoRegresar = usuarioParametrizadosPorintermediario(intermediarioConsulta);
            break;        
        }
    
    log.info("infoRegresar: " + infoRegresar);

%>
<%=infoRegresar%>