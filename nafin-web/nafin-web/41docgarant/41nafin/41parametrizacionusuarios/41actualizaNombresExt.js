Ext.onReady(function() {
    
    Ext.define('resultadoModel', {
        extend: 'Ext.data.Model',
        fields: [
        {
            name: 'usuario',
            mapping: 'clave'
        }, {
            name: 'nombreCompleto',
            mapping: 'descripcion'
        }]
    });



    var gridStore = Ext.create('Ext.data.Store', {
        model: 'resultadoModel',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '41actualizaNombresExt .data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });


    var gridResultados = Ext.create('Ext.grid.Panel', {
        id: 'resultados',
        store: gridStore,
        stripeRows: true,
        hidden: true,
        title: 'Usuarios del M�dulo de Gesti�n',
        width: 500,
        height: 300,
        collapsible: false,
        enableColumnMove: false,
        enableColumnResize: true,
        autoScroll: true,
        style:   'margin:0 auto;',
        bodyStyle: 'padding: 6px',
        title: 'Actualizar Nombres de Usuarios',
        defaults: {
                msgTarget:    'side',
                anchor:       '-20'
        },        
        features: [{ftype: 'summary'}],        
        columns: [ 
            {
                header: "Usuario",
                dataIndex: 'usuario',
                width: 100,
                sortable: true,
                hideable: false,
                summaryType: 'count',
                summaryRenderer: function(value, summaryData, dataIndex) {
                    return '<strong>'+Ext.String.format('{0} usuario{1}', value, value !== 1 ? 's' : '')+'<\/strong>'; 
                }                
            }, {
                header: "Nombre Completo",
                dataIndex: 'nombreCompleto',
                width: 386,
                sortable: true,
                hideable: true
            }
        ]
    });




    //----------*************  PANEL BUSQUEDA  *************----------//
    var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        width: 500,
        frame: true,
        height: 'auto',
        style:   'margin:0 auto;',
        bodyStyle: 'padding: 10px',
        title: 'Actualizar Nombres de Usuarios',
        defaults: {
                msgTarget:    'side',
                anchor:       '-20'
        }, 
        items :[
            {
            xtype:'label',
            text: 'Buscar usuarios que no tengan su nombre registrado en la tabla del m�dulo de Gesti�n de Documentos de Garant�as.\n' +
            'Esta pantalla busca los usuarios pramtrizados y aquellos nuevos usuarios con los perfiles SOL GESTGAR, OPER GESTGAR'
            }
        ],
        buttons: [{
            text: 'Buscar Usuarios sin actualizar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            handler: function(boton, evento) {
                Ext.getCmp('resultados').show();
                gridStore.load({
                    params: {
                        accion: 'loadUsers'
                    },
                    callback: function(records, operation, success) {
                        if(success && records.length>0){
                            Ext.getCmp('btnUpdate').enable();
                        }
                        else{
                            Ext.getCmp('btnUpdate').disable();
                        }
                        
                    }
                });
            }
        }, {
            text: 'Actualizar',
            iconCls: 'icoActualizar',
            id: 'btnUpdate',
            disabled:true,
            handler: function() {
                gridStore.removeAll();
                gridStore.load({
                    params: {
                        accion: 'updateUsers'
                    }
                });
            }
        }]
    });

    var pnl = new Ext.Container({
        id: 'contenedorPrincipal',
        renderTo: 'areaContenido',
        width: 949,
        items: [
            NE.util.getEspaciador(40),
            formBusqueda,
            NE.util.getEspaciador(30),
            gridResultados
        ]
    });



});