<%@ page
	contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileUploadException,
		org.apache.commons.logging.Log,
		com.nafin.docgarantias.*,
                com.nafin.docgarantias.baseoperaciones.*,
		netropology.utilerias.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
	private final int MAX_PLANTILLA_FILE_SIZE = 1024*1024*80; // 80 MB   
%>
<%
	ParametrosRequest req = null;
	String itemArchivo = "";
	String rutaArchivo = "";
	String mensajeError =""; 
	String PATH_FILE = strDirectorioTemp;
	String nombreArchivo= "";
	String rutaArchivoTemporal="";
	String cargaAcuse = "";
	String cargaFechaAcuse = "";
	String cargaHoraAcuse = "";
	Boolean exito = false;
	Integer icArchivo = null;
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType);
	request.setAttribute("myContentType", myContentType);
	String picIF = null;
        String picDocumento = null;
	if (ServletFileUpload.isMultipartContent(request)) {
		DiskFileItemFactory factory = new DiskFileItemFactory();		
		factory.setRepository(new File(PATH_FILE));
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(MAX_PLANTILLA_FILE_SIZE);
		try {
                    req = new ParametrosRequest(upload.parseRequest(request));
                    picIF = req.getParameter("icIF");
                    picDocumento = req.getParameter("icDoc");
                    FileItem fItem = (FileItem)req.getFiles().get(0);
                    itemArchivo		= (String)fItem.getName();
                    rutaArchivo = PATH_FILE+itemArchivo;
                    int tamanio			= (int)fItem.getSize();
                    nombreArchivo= itemArchivo;
                    rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
                    File archivoUpload  = new File(rutaArchivoTemporal);
                    if(archivoUpload.exists()){
                        archivoUpload.delete();
                    }
                    fItem.write(new File(rutaArchivoTemporal));
                    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
                    ArchivoDocumento archivoDoc = new ArchivoDocumento(nombreArchivo, 3);
                    Integer icDocumento = null;
                    if (picDocumento != null){
                        try{
                            icDocumento = Integer.parseInt(picDocumento);
                            archivoDoc = files2OnBaseBean.guardaArchivoDoc(archivoDoc, PATH_FILE, icDocumento);
                        }
                        catch(NumberFormatException  e){
                            log.info("Se envio un parametro no valido, icDocumento:"+picDocumento);
                            throw new AppException("Parametro icDocumento no valido", e);
                        }
                    }
                    else{
                        archivoDoc = files2OnBaseBean.guardaArchivo(archivoDoc, PATH_FILE);
                    }
                    icArchivo = archivoDoc.getIcArchivo();
                    /* Actualizacion del registro de BO  */
                    BaseOperacion baseOperacionBean = ServiceLocator.getInstance().lookup("BaseOperacion",BaseOperacion.class);
                    HashMap<String, String> parametros = new HashMap<>();
                    parametros.put(BaseOperacionBean.PARAM_ESTATUS_BO, BaseOperacionBean.ESTATUSBO_SIN_BASE_OPERACION+"");
                    parametros.put(BaseOperacionBean.PARAM_IC_ARCHIVO, icArchivo+"");
                    parametros.put(BaseOperacionBean.PARAM_IC_DOC, icDocumento+"");
                    parametros.put(BaseOperacionBean.PARAM_IC_IF, picIF);
                    parametros.put(BaseOperacionBean.PARAM_IC_USUARIO, iNoUsuario);
                    exito = baseOperacionBean.insertarBO(parametros);
                    if (!exito){
                        log.info("Algo fallo... Eliminando archivo de OnBase: "+icArchivo+ ", "+archivoDoc.getNombreArchivo());
                        files2OnBaseBean.eliminaArchivoDocumento(icArchivo);
                    }
                    //exito  = true;
		} catch(Throwable e) {
                        if( e instanceof SizeLimitExceededException  ) {
                            log.error("CargaPlantilla.subirArchivo(Exception): El Archivo es muy Grande, excede el límite", e);
                            mensajeError = "El archivo que intenta cargar sobrepasa los "+ ( MAX_PLANTILLA_FILE_SIZE / 1048576 ) +" MB permitidos. Favor de verificarlo";
                        } else{
                            log.error(e.getStackTrace());
                            throw new AppException("Error al cargar el archivo a OnBase", e);
                        }
		}
	}
%>
{
    "success"       : <%=exito.toString()%>,
    "mensajeError"  :'<%=mensajeError%>',
}