
Ext.onReady(function () {

    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        }]
    });



    var storeEstatusBO = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41cargaBOExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEstatusBO'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        panelPrincipal.unmask();
    }

    function descargaArchivoFormalizado(record){
        panelPrincipal.mask("Descargando archivo...");
        Ext.Ajax.request({
            url: '41cargaBOExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getSignedDoc',
                icArchivo: record.get('icArchivo'),
                icIF:  record.get('icIF'),
                icDoc: record.get('icDocumento')
            }),
        callback: descargaArchivos
        });     
    }
    
    
    
    var procesaCargaArchivo = function (opts, success, action) {
        Ext.MessageBox.alert('Carga Base de Operaciones', 'Se han guardado los cambios correctamente');
        Ext.getCmp('winFileUpload').close();
        gridBOStore.reload();
    }

    var cargaArchivoBO = function (estado, respuesta) {
        var formaFile = Ext.getCmp('formFile');
        formaFile.getForm().submit({
            clientValidation: false,
            url: '41cargaFileBO.jsp',
            params: {
                informacion: 'loadfileBO'
            },
            waitMsg: 'Enviando datos...',
            waitTitle: 'Cargando archivo',
            success: function (form, action) {
                procesaCargaArchivo(null, true, action);
            },
            failure: NE.util.mostrarSubmitError
        });
    };

    var cargarBO = function (record) {
        var icIF = record.get('icIF');
        var icDoc = record.get('icDocumento');
        Ext.create('Ext.window.Window', {
            title: 'Cargar Base de Operaciones',
            id: 'winFileUpload',
            closable: true,
            closeAction: 'destroy',
            width: 450,
            minWidth: 250,
            height: 100,
            animCollapse: false,
            border: false,
            modal: true,
            layout: {
                type: 'border',
                padding: 0
            },
            items: [{
                xtype: 'form',
                id: 'formFile',
                items: [{
                    xtype: 'filefield',
                    padding: '20 20 20 20',
                    fieldLabel: 'Carga Archivo',
                    width: 398,
                    //vtype: 'archivoPdf',
                    listeners: {
                        change: function (fld, value) {
                            fld.setRawValue(value.substring(value.lastIndexOf("\\") + 1));
                            if (fld.isValid()) {
                                cargaArchivoBO('CARGAR_ARCHIVO', null);
                            }
                        }
                    }
                }, {
                    xtype: 'hiddenfield',
                    id: 'icIF',
                    name: 'icIF',
                    value: icIF
                }, {
                    xtype: 'hiddenfield',
                    id: 'icDoc',
                    name: 'icDoc',
                    value: icDoc
                }]
            }]
        }).show();
    };    
    

    
    function setearNoAplicaBO(record){
        var folio = record.get("folio");
        var nombreIF = record.get("nombreIF");
        Ext.Msg.confirm("Base de Operaciones", "�Est� seguro que para el documento con folio: <strong>"+ folio+"</strong> de <strong>"+ nombreIF+"</strong> no aplica la Base de Operaciones?", function(btnText){
                if(btnText === "yes"){
                    Ext.Ajax.request({
                        url: '41cargaBOExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'setNA',
                            icIF:  record.get('icIF'),
                            icDoc: record.get('icDocumento')
                        }),
                    callback: function (opts, success, response) {
                        var info = Ext.JSON.decode(response.responseText);
                        if (success == true && info.success == true) {
                            gridBOStore.reload();
                            Ext.MessageBox.alert('Guardar', 'Se han guardado los cambios correctamente');
                        }
                    }
                    });                       
                }
            }, this);    
    }
    
    
    function descargarBaseOperacion(record){
        panelPrincipal.mask("Descargando base de operaciones...");
        Ext.Ajax.request({
            url: '41cargaBOExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getBOFile',
                icArchivo: record.get('icArchivoBO'),
                icIF:  record.get('icIF'),
                icDoc: record.get('icDocumento')
            }),
        callback: descargaArchivos
        });   
    }
    
    function eliminarBO(record){
        panelPrincipal.mask("Procesando...");
        var folio = record.get("folio");
        var nombreIF = record.get("nombreIF");
        Ext.Msg.confirm("Base de Operaciones", "�Est� seguro que desea eliminarla base de operaciones del documento con folio: <strong>"+ folio+"</strong> de <strong>"+ nombreIF+"</strong>?", function(btnText){
                if(btnText === "yes"){    
                    Ext.Ajax.request({
                        url: '41cargaBOExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'eliminarBO',
                            icIF:  record.get('icIF'),
                            icDoc: record.get('icDocumento')
                        }),
                    callback: function (opts, success, response) {
                        var info = Ext.JSON.decode(response.responseText);
                        if (success == true && info.success == true) {
                            gridBOStore.reload();
                            Ext.MessageBox.alert('Guardar', 'Se han guardado los cambios correctamente');
                        }
                        panelPrincipal.unmask();
                    }
                    });   
                }
        });
    }

    function liberarBO(record){
        var folio = record.get("folio");
        var nombreIF = record.get("nombreIF");
        Ext.Msg.confirm("Base de Operaciones", "�Est� seguro que desea enviar la base de operaciones del documento con folo: <strong>"+ folio+"<\/strong> de <strong>"+ nombreIF+"<\/strong> a validaci�n?", function(btnText){    
                if(btnText === "yes"){    
                    panelPrincipal.mask("Procesando...");
                    Ext.Ajax.request({
                        url: '41cargaBOExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'liberarBO',
                            icIF:  record.get('icIF'),
                            icDoc: record.get('icDocumento')
                        }),
                    callback: function (opts, success, response) {
                        var info = Ext.JSON.decode(response.responseText);
                        if (success == true && info.success == true) {
                            gridBOStore.reload();
                            Ext.MessageBox.alert('Guardar', 'Se han guardado los cambios correctamente');
                        }
                        panelPrincipal.unmask();
                    }
                    });       
                }
            }
        );
    }

var formBusquedaBO = new Ext.form.FormPanel({
        name: 'boformBusqeda',
        id: 'boformBusqueda',
        title: 'Panel de Busqueda',
        width: 943,
        height: 200,frame: true,
        border: 0,
        layout: 'column',
        items: [
        {
            xtype: 'fieldset',
            id: 'bofieldsBusqueda1',
            height: 150,
            border: false,
            columnWidth: 0.5,
            items: [
            {
                xtype: 'combo',
                name: 'folio',
                id: 'folio',
                fieldLabel: 'Folio de Recepci�n',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Folios',
                //store: storeFolioSolicitud,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                name: 'tipoDocumento',
                id: 'tipoDocumento',
                fieldLabel: 'Tipo de Documento',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Folios',
                //store: storeFolioSolicitud,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },            
            {
                xtype: 'combo',
                id: 'portafolio',
                name: 'portafolio',
                fieldLabel: 'Portafolio',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Solicitantes',
                //store: storeSolicitantes,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                id: 'producto',
                name: 'producto',
                fieldLabel: 'Producto',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Solicitantes',
                //store: storeSolicitantes,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }
            ]
        }, {
            xtype: 'fieldset',
            height: 150,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [
            {
                xtype: 'combo',
                id: 'solicitante',
                name: 'solicitante',
                fieldLabel: 'Solicitante',
                padding: 5,
                width: 440,
                forceSelection: true,
                allowBlank: true,
                store: storeEstatusBO,
                emptyText: 'Todos los solicitantes',
                displayField: 'descripcion',
                editable: false,
                valueField: 'clave'
            },             
            {
                xtype: 'combo',
                id: 'boEstatus',
                name: 'boEstatus',
                fieldLabel: 'Estatus',
                padding: 5,
                width: 440,
                forceSelection: true,
                allowBlank: true,
                store: storeEstatusBO,
                emptyText: 'Todos los estatus',
                displayField: 'descripcion',
                editable: false,
                valueField: 'clave'
            }, 
            {
                xtype: 'combo',
                id: 'boEjecutivo',
                name: 'boEjecutivo',
                fieldLabel: 'Ejecutivo',
                padding: 5,
                width: 440,
                //store: storeSolicitudesIF,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Ejecutivos',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                id: 'bointermediario',
                name: 'bointermediario',
                fieldLabel: 'Intermediariio Financiero:',
                padding: 5,
                width: 440,
                //store: storeSolicitudesIF,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Intermediarios',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }            
            ]
        }],
        buttons: [{
            text: 'Buscar',
            id: 'bobtnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarFechas();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    gridResultados.el.mask('Cargando...', 'x-mask-loading');
                    gridStore.load({
                        params: Ext.apply(formBusqueda.getForm().getValues())
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                formBusquedaBO.getForm().reset();
            }
        }]
    });
    

    Ext.define('DocsBODataModel', {
        extend: 'Ext.data.Model',
        fields: 
        [{name: 'icDocumento', mapping: 'icDocumento'},
        {name: 'icArchivo', mapping: 'icArchivo'},
        {name: 'nombreUsuarioEjecutivo', mapping: 'nombreUsuarioEjecutivo'},
        {name: 'nombreUsuarioSolicito', mapping: 'nombreUsuarioSolicito'},
        {name: 'estatusBO', mapping: 'estatusBO'},
        {name: 'fechaLiberacion', mapping: 'fechaLiberacion'},
        {name: 'fechaFormalizacion', mapping: 'fechaFormalizacion'},
        {name: 'nombreIF', mapping: 'nombreIF'},
        {name: 'folio', mapping: 'folio'},
        {name: 'nombreProducto', mapping: 'nombreProducto'},
        {name: 'portafolio', mapping: 'portafolio'},
        {name: 'tipoDocumento', mapping: 'tipoDocumento'},
        {name: 'icIF', mapping: 'icIF'},
        {name: 'icArchivoBO', mapping: 'icArchivoBO'}]
    });    
    
    
    
    
    
    var gridBOStore = Ext.create('Ext.data.Store', {
        model: 'DocsBODataModel',
        proxy: {
            type: 'ajax',
            url: '41cargaBOExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'documentosBO'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }
    });
    
    
    
    
    
    
    var gridBO = Ext.create('Ext.grid.Panel', {
        id: 'gridBO',
        store: gridBOStore,
        stripeRows: true,
        title: 'Base de Operaciones',
        width: 944,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
            header: "Fecha de<br/>Recepci&oacute;n",
            menuText: "Fecha de Recepci&oacute;n",
            dataIndex: 'fechaFormalizacion',
            width: 90,
            sortable: true,
            hideable: true            
        }, {
            header: "Folio de<br/>Solicitud",
            dataIndex: 'folio',
            width: 80,
            sortable: true,
            hideable: true
        }, {
            header: "Tipo de Documento",
            dataIndex: 'tipoDocumento',
            width: 150,
            sortable: true,
            hideable: true
        }, {
            header: "Portafolio",
            dataIndex: 'portafolio',
            align: 'center',
            width: 120,
            sortable: true,
            hideable: true        
        }, {
            header: "Producto",
            dataIndex: 'nombreProducto',
            align: 'left',
            width: 250,
            sortable: true,
            hideable: true   
        },
        {
            header: "Solicitante",
            dataIndex: 'nombreUsuarioSolicito',
            align: 'left',
            width: 200,
            sortable: true,
            hideable: true   
        }, 
        {
            header: "Ejecutivo",
            dataIndex: 'nombreUsuarioEjecutivo',
            align: 'left',
            width: 200,
            sortable: true,
            hideable: true   
        },
        {
            header: "Intermediario Financiero",
            dataIndex: 'nombreIF',
            align: 'left',
            width: 200,
            sortable: true,
            hideable: true   
        },         
        {
            header: "Documento<br/>Formalizado",
            menuText: "Documento Formalizado",
            xtype: 'actioncolumn',
            align: 'center',
            dataIndex: 'icArchivo',
            hideable: true,
            sortable: true,
            width: 80,
            items: [{
                iconCls: 'icoFolderTable',
                tooltip: 'Mostrar Documentos',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    descargaArchivoFormalizado(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('icArchivo') == null;
                }
            }]
        }, 
        {
            xtype: 'actioncolumn',
            header: "Acciones",
            menuText: "Acciones",
            align: 'center',
            dataIndex: 'icDocumento',
            hideable: true,
            sortable: true,
            width: 110,
            items: [
                {
                iconCls: 'page_add',
                tooltip: 'Cargar Base de Operaci�n',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    cargarBO(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('icArchivoBO') != 0 || record.get('estatusBO') > 0) ; 
                }
            },
                {
                iconCls: 'icoRechazar marginL15p',
                tooltip: 'No aplica',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    setearNoAplicaBO(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatusBO') > 0 || record.get('icArchivoBO') != 0);
                }                
            }            
            ]
        },        
        {
            xtype: 'actioncolumn',
            header: "Base de Operaci&oacute;n",
            menuText: "Base de Operaci&oacute;n",
            align: 'center',
            dataIndex: 'icDocumento',
            hideable: true,
            sortable: true,
            width: 110,
            items: [{
                iconCls: 'icoLupa',
                tooltip: 'Visualizar Base de Operaci�n',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    descargarBaseOperacion(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('icArchivoBO') == 0;
                }
            },
            {
                iconCls: 'icoEliminar marginL15p',
                tooltip: 'Eliminar Base de Operaci�n',
                handler: function (grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                    eliminarBO(rec);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatusBO') > 0 || record.get('icArchivoBO') == 0);
                }    
            },
            {
                iconCls: 'icoValidar marginL15p',
                tooltip: 'Liberar Base de Operaci�n',
                handler: function (grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                    liberarBO(rec);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatusBO') > 0 || record.get('icArchivoBO') == 0);
                }  
            }            
            ]
        },
        {
            header: "Estatus",
            dataIndex: 'estatusBO',
            align: 'center',
            width: 180,
            sortable: true,
            hideable: true,
            renderer: function (value, metaData, record) {
                var estatus = record.get("estatusBO");
                var nombre = "";
                switch(estatus){
                    case "-1":
                        nombre = 'No Aplica';                        
                        break;                
                    case "0":
                        nombre = 'Sin Base de Operaci�n';
                        break;
                    case "1":
                        nombre = 'Por Validar Base de Operaci�n';
                        break;
                    case "2":
                        nombre = 'Base de Operaci�n Validada';
                        break;    
                    default:
                        nombre = 'Sin Base de Operaci�n';
                        break;
                }
                return nombre;
            }            
        }]
    });
    


    var panelPrincipal = new Ext.Container({
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(5),	
			formBusquedaBO,
			NE.util.getEspaciador(20),
                        gridBO,
			NE.util.getEspaciador(20)
		]        
	});


});
