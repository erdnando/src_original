<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.baseoperaciones.*,
        com.nafin.docgarantias.formalizacion.*,
        com.nafin.docgarantias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    private static final Integer ESTATUS_POR_VALIDAR_BO = 1;
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar="";
log.info("INFORMACION: "+informacion);
BaseOperacion baseOperacionBean = ServiceLocator.getInstance().lookup("BaseOperacion",BaseOperacion.class);

if(informacion.equals("catEstatusBO")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado.add(new ElementoCatalogo(null,"Todos los estatus"));
    listado.add(new ElementoCatalogo("910","Por validar bases"));
    listado.add(new ElementoCatalogo("920","Bases Liberadas"));
    listado.add(new ElementoCatalogo("930","Formalizado"));
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("documentosBO")){   
    ArrayList<String> parametros = new ArrayList<>(); 
    ArrayList<DocBaseOperacion> listado = baseOperacionBean.getDocsBaseOperacion();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("getSignedDoc")){   
    Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
    String sicDoc = request.getParameter("icDoc") == null ? "" : request.getParameter("icDoc");
    Map<String,String> params = new HashMap<>();
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);   
        Integer icIF = Integer.parseInt(sicIF);
        Integer icDocumento = Integer.parseInt(sicDoc);
        params.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo+"");
        params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF+"");
        params.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento+"");
        String nombreArchivo = formalizacionBean.FirmaDocumento(strDirectorioPublicacion, params);	
        jsonObj.put("urlArchivo", nombreArchivo);		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa los parametros getSignedDoc");
        jsonObj.put("msg","Error al procesa los parametros icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("setNA")){
    JSONObject jsonObj   =  new JSONObject();
    boolean exito =  false;
    try{
        String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
        String sicDoc = request.getParameter("icDoc") == null ? "" : request.getParameter("icDoc");
        Integer icIF = Integer.parseInt(sicIF);
        Integer icDocumento = Integer.parseInt(sicDoc);
        exito = baseOperacionBean.setNoAplicaBO(icDocumento, icIF, iNoUsuario);
    }catch(NumberFormatException  e){
        log.info("Error al procesa los parametros setNA");
        jsonObj.put("msg","Error al procesa los parametros icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }    
}
else if (informacion.equals("eliminarBO")){
    JSONObject jsonObj   =  new JSONObject();
    boolean exito =  false;
    try{
        String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
        String sicDoc = request.getParameter("icDoc") == null ? "" : request.getParameter("icDoc");
        Integer icIF = Integer.parseInt(sicIF);
        Integer icDocumento = Integer.parseInt(sicDoc);
        
        exito = baseOperacionBean.eliminarBO(icDocumento, icIF, iNoUsuario);
    }catch(NumberFormatException  e){
        log.info("Error al procesa los parametros eliminarBO");
        jsonObj.put("msg","Error al procesar los parametros");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }    
}
else if (informacion.equals("liberarBO")){
    JSONObject jsonObj   =  new JSONObject();
    boolean exito =  false;
    try{
        String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
        String sicDoc = request.getParameter("icDoc") == null ? "" : request.getParameter("icDoc");
        Integer icIF = Integer.parseInt(sicIF);
        Integer icDocumento = Integer.parseInt(sicDoc);
        exito = baseOperacionBean.avanzaEstatusDocto(icDocumento, icIF, iNoUsuario, ESTATUS_POR_VALIDAR_BO); 
    }catch(NumberFormatException  e){
        log.info("Error al procesa los parametros eliminarBO");
        jsonObj.put("msg","Error al procesar los parametros");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }    
}
else if(informacion.equals("getBOFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivoBO");
        jsonObj.put("msg","Error al procesa el parametro icArchivoBO");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>