<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String portafolio = request.getParameter("portafolio") == null ? "" : request.getParameter("portafolio");
String producto = request.getParameter("nombreProducto") == null ? "" : request.getParameter("nombreProducto");
String infoRegresar="";

log.info("informacion: "+informacion);
DocGarantias docGarantias = ServiceLocator.getInstance().lookup("DocGarantias",DocGarantias.class);
Integer if_siag = docGarantias.getClaveIF_SIAG(Integer.parseInt(iNoCliente));
if (informacion.equals("MonitorLineasNafin")){   //<------------------------------------------------------
    ArrayList<String> parametros = new ArrayList<>(); 
    String fechaVigencia = request.getParameter("fechaVigencia") == null ? "": request.getParameter("fechaVigencia");

    //Integer if_siag = docGarantias.getClaveIF_SIAG(Integer.parseInt(iNoCliente));
    
    parametros.add(portafolio);
    parametros.add(producto);
    parametros.add(if_siag+"");
    parametros.add(fechaVigencia);
    parametros.add("");
    parametros.add("");
	ArrayList<MovimientoLinea> listado = docGarantias.getMonitorLineas(parametros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catFechaVigencia")){
    Integer idProducto = null;
    if (!producto.isEmpty()){
        idProducto = Integer.parseInt(producto);
    }
    ArrayList<ElementoCatalogo>listado = docGarantias.getFechaVigenciaMonitorLineas(if_siag, portafolio, idProducto);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catNombreProductos")){

    ArrayList<ElementoCatalogo>listado = docGarantias.getNombreProductosMonitor(if_siag, portafolio);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catPortafolio")){
	ArrayList<ElementoCatalogo>listado = docGarantias.getPortafolioMonitor(if_siag);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}

else if (informacion.equals("GenerarArchivoPDF") || informacion.equals("GenerarArchivoXSL")){
    ArrayList<String> parametros = new ArrayList<>(); 

    String fechaVigencia = request.getParameter("fechaVigencia") == null ? "": request.getParameter("fechaVigencia");  
    
    parametros.add(portafolio);
    parametros.add(producto);
    parametros.add(if_siag+"");
    parametros.add(fechaVigencia);
    parametros.add("");
    parametros.add("");

    String tipoArchivo = informacion.equals("GenerarArchivoPDF") ? ConstantesMonitor.ARCHIVO_PDF : ConstantesMonitor.ARCHIVO_XLS;
    JSONObject jsonObj   =  new JSONObject();    
    String nombreArchivo = docGarantias.getMonitorPDF_XLS(request, strDirectorioPublicacion, parametros, true, tipoArchivo);
    jsonObj.put("success", new Boolean(true)); 
    jsonObj.put("urlArchivo", strDirecVirtualTemp +nombreArchivo);		
    infoRegresar = jsonObj.toString();
}

log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>