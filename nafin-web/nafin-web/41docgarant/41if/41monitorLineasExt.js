
Ext.onReady(function() {

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();		
            }else {
                NE.util.mostrarConnError(response,opts);
        }
    }
    
   function actualizaCombos(combo, valor){
        var elPanel = Ext.getCmp('formBusqueda').mask('Actualizando...', 'x-mask-loading');
        var v_idPortafolio = Ext.getCmp("portafolio") == null ? "" : Ext.getCmp("portafolio").getValue();
        var v_nombreProducto =   Ext.getCmp('nombreProducto')== null ? "" : Ext.getCmp('nombreProducto').getValue();
        if (combo =='portafolio'){
            storeNombreProductos.load({
                params: {
                    informacion: 'catNombreProductos',
                    portafolio:  valor,
                    nombreProducto: v_nombreProducto
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('nombreProducto').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });
            storeFechaVigencia.load({
                params: {
                    informacion: 'catFechaVigencia',
                    portafolio:  valor,
                    nombreProducto: v_nombreProducto
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('fechaVigencia').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });
         
        }
        else if (combo == 'nombreProducto'){
            storeFechaVigencia.load({
                params: {
                    informacion: 'catFechaVigencia',
                    portafolio:  v_idPortafolio,
                    nombreProducto: valor
                },
                callback: function(records, operation, success) {
                    Ext.getCmp('fechaVigencia').setValue('');
                    Ext.getCmp('formBusqueda').unmask();   
                },
                scope: this
            });          
        }
    }// end actualizaCombos 
    

//----------*************  GRID  *************----------//
	 Ext.define('MonitorLineasDataModel', {
		extend: 'Ext.data.Model',
		fields: [
		   {name: 'portafolio', 			mapping : 'nombrePortafolio', 	    },
		   {name: 'producto', 				mapping : 'nombreProducto', 	    },
                   {name: 'if', 					mapping : 'nombreIF', 	    },
		   {name: 'linea_asignada', 		mapping : 'lineaProductoAsignada', 	    },
		   {name: 'consumo_linea', 			mapping : 'consumoLineaProducto',   },
		   {name: 'consumo_participacion',	mapping : 'consumoLineaIF',	            },
		   {name: 'vigencia', 				mapping : 'fechaVigencia', 	    },
		   {name: 'base_operacion', 		mapping : 'baseOperacion', 		    }
		]
	 });
     
    var procesarConsulta = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if(store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso','No existe informaci�n con los criterios determinados');
                Ext.getCmp('btnPDF').setDisabled(true);
                Ext.getCmp('btnXLS').setDisabled(true);
            }
            else{
                Ext.getCmp('btnPDF').setDisabled(false);
                Ext.getCmp('btnXLS').setDisabled(false);
            }
        }
        gridResultados.el.unmask();
    }
    
		var gridStore = Ext.create('Ext.data.Store', {
		   model: 'MonitorLineasDataModel',
            proxy:              {
                type:            'ajax',
                url:             '41monitorLineasExt.data.jsp',
                reader:          {
                    type:         'json',
                    root:         'registros'
                },
                extraParams:     {
                    informacion:  'MonitorLineasNafin'
                }
            },
            autoLoad: true,
            listeners: {
                exception: NE.util.mostrarDataProxyError,
                load: procesarConsulta
            }             
		});        
        
        var gridResultados = Ext.create('Ext.grid.Panel', {
           id                : 'gridId',
           store             : gridStore,
           stripeRows        : true,
           title             : 'Monitor de L�neas',  
           width             : 944,
           height            : 400,
           collapsible       : false,             
           enableColumnMove  : true,
           enableColumnResize: true,
           enableColumnHide  : true,
           autoScroll: true,
		   columns           :
		   [
           { 
			  header: "Nombre del<br/> Portafolio",
			  dataIndex: 'portafolio',	
			  id : 'name',    
              width: 150,
			  sortable: true,  
			  hideable: true  
		   },{
			  header: "Nombre del<br/> Producto", 
			  dataIndex: 'producto',
              width: 150,
			  sortable: true,
			  hideable: true   
		   },
			{
			  header: "Intermediario<br/> Financiero", 
			  dataIndex: 'if',
			  width: 120,
			  sortable: true,
			  hideable: true   
		   }           
           ,{
			  header: "L�nea de Proyecto<br/> Asignada", 
			  dataIndex: 'linea_asignada',
			  align: 'right',
			  width: 110,
			  sortable: true,
                          hideable: true,
                          renderer: Ext.util.Format.numberRenderer('0,000.00')
		   }               
			,{
			  header: "L�nea de Proyecto<br/>consumida", 
			  dataIndex: 'consumo_linea',
			  align: 'right',
			  width: 110,
			  sortable: true,
                          renderer: Ext.util.Format.numberRenderer('0,000.00'),
			  hideable: true  
		   }
			,{
			  header: "L�nea Utilizada IF", 
			  dataIndex: 'consumo_participacion',
			  align: 'right',
			  width: 140,
			  sortable: true,
			  renderer: Ext.util.Format.numberRenderer('0,000.00'),
			  hideable: false  
		   }                    
			,{
			  header: "% de Consumo L�nea<br/>de Proyecto", 
			  dataIndex: 'consumo_linea',
			  width: 120,
                          align: 'right',
			  sortable: true,
			  hideable: true,   
			  renderer :  function (value, metadata, record, rowIndex, colIndex, store) {
				  var calculado = (value/record.data['linea_asignada'])*100;
				  return Ext.util.Format.number(calculado, "000.00");
			  }                  
		   }               
		   ,{
			  header: "% de Consumo IF",
			  dataIndex: 'consumo_participacion',
			  width: 110,
                          align: 'right',
			  sortable: true, 
			  renderer :  function (value, metadata, record, rowIndex, colIndex, store) {
				  var calculado = (value/record.data['linea_asignada'])*100;
				  return Ext.util.Format.number(calculado, "000.00");
			  },
			  hideable: true
		   }
			,{
			  header: "Fecha de<br/>Vigencia", 
			  dataIndex: 'vigencia',
			  align: 'center',
			  width: 100,
			  sortable: true,
			  hideable: true  
		   }
		   ,{
			  header: "Base(s) de<br/> Operaci�n", 
			  dataIndex: 'base_operacion',
			  align: 'right',
			  width: 80,
			  sortable: true,
			  hideable: true  
		   }
		   ],
        buttons: [{
            text: 'Imprimir en PDF',
            id: 'btnPDF',
            iconCls: 'icoBotonPDF',
            handler: function(boton, evento) {
                Ext.Ajax.request({
                    url: '41monitorLineasExt.data.jsp',
                    params: Ext.apply(  {
                        informacion: 'GenerarArchivoPDF',
                        portafolio: Ext.getCmp('portafolio').getValue(),
                        fechaVigencia: Ext.getCmp('fechaVigencia').getValue(),
                        nombreProducto : Ext.getCmp('nombreProducto').getValue(),
                    }),
                callback: descargaArchivos
                });
            }   
        },
        {
            text: 'Exportar a Excel',
            id: 'btnXLS',
            iconCls: 'icoBotonXLS',
            handler: function(boton, evento) {
                Ext.Ajax.request({
                    url: '41monitorLineasExt.data.jsp',
                    params: Ext.apply(  {
                        informacion: 'GenerarArchivoXSL',
                        portafolio: Ext.getCmp('portafolio').getValue(),
                        fechaVigencia: Ext.getCmp('fechaVigencia').getValue(),
                        nombreProducto : Ext.getCmp('nombreProducto').getValue()
                    }),
                callback: descargaArchivos
                });
            }   
        }         
        ]            
		});


//----------************* CATALOGOS  *************----------//
	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }				
		]
	});

	var  storeCatPortafolio = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorLineasExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catPortafolio'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});     
    
	var  storeFechaVigencia = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorLineasExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catFechaVigencia'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});    
    
    
	var  storeNombreProductos = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorLineasExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catNombreProductos'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});      
    
//----------*************  PANEL BUSQUEDA  *************----------//
var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        width: 943,
        height: 'auto',
        frame: true,
        title: 'Monitor de L�neas',
        layout: 'column',
        items: [
            {
                xtype: 'fieldset',
                id:'fieldsBusqueda1',
                height: 100,
                border:false,
                columnWidth: 0.5,
                margins: '0 20 0 0',
                items: [
                        {
                            xtype: 'combo',
                            name: 'portafolio',
                            id: 'portafolio',
                            fieldLabel: 'Portafolio',
                            padding: 5,
                            width: 400,
                            forceSelection: true, 
                            queryMode: 'local', 
                            allowBlank: true,
                            emptyText: 'Todos los Portafolios',
                            store: storeCatPortafolio, displayField: 'descripcion', valueField: 'clave',
                            listeners: {
                                select: function(combo, records, eOpts) {
                                    actualizaCombos('portafolio', records[0].get('clave'));
                                }
                            }                            
                        },
                        {
                            xtype: 'combo',
                            id : 'nombreProducto',
                            name: 'nombreProducto',
                            fieldLabel: 'Nombre del Producto',
                            padding: 5,
                            width: 400,
                            store: storeNombreProductos,
                            emptyText: 'Todos los Productos',
                            displayField: 'descripcion',
                            valueField: 'clave',
                            allowBlank: true,
                            forceSelection: true,
                            queryMode: 'local',
                            listeners: {
                                select: function(combo, records, eOpts) {
                                    actualizaCombos('nombreProducto', records[0].get('clave'));
                                }
                            }                            
                        }

                    ]                
            },
            {
                xtype: 'fieldset',
                height: 100,
                border:false,
                columnWidth: 0.5,
                margins: '0 20 0 0',
			items: [
                        {
                            xtype: 'combo',
                            id: 'fechaVigencia',
                            name: 'fechaVigencia',
                            fieldLabel: 'Vigencia',
                            padding: 5,
                            width: 400,
                            store: storeFechaVigencia,
                            emptyText: 'Todas las fechas',
                            displayField: 'descripcion', 
                            valueField: 'clave',
                            forceSelection: true,
                            allowBlank: true,
                            queryMode: 'local'
                        }
                ]
            }
        ],
        buttons: [{
            text: 'Buscar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                gridResultados.el.mask('Cargando...', 'x-mask-loading');
                gridStore.load({
                    informacion:  'MonitorLineasNafin',
                    params: Ext.apply(formBusqueda.getForm().getValues())
                });
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                Ext.getCmp('portafolio').setValue('');
				Ext.getCmp('nombreProducto').setValue('');
				Ext.getCmp('fechaVigencia').setValue('');;
            }
        }]        
    });


//----------*************  CONTENEDOR PRINCIPAL  *************----------//

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			formBusqueda,
			NE.util.getEspaciador(30),
            gridResultados,
			NE.util.getEspaciador(20)
		]        
	});



}); 