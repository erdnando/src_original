<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
        com.nafin.docgarantias.*,
	com.nafin.docgarantias.consultas.*,
        com.nafin.docgarantias.formalizacion.*,
        com.nafin.docgarantias.nivelesdeservicio.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";

String infoRegresar="";

log.info("INFORMACION: "+informacion);
ConsultaSolicitudes consultaSolicitudesBean = ServiceLocator.getInstance().lookup("ConsultaSolicitudes",ConsultaSolicitudes.class);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);
Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);
String tipoConsulta = (request.getParameter("tipoConsulta") != null) ? request.getParameter("tipoConsulta") : "";
  
if (informacion.equals("consultaSolicitudes")){   
    Map<String, String> filtros = new HashMap<>();
    
    String s_fechaUno		= request.getParameter("s_fechaUno");
    String s_fechaDos           = request.getParameter("s_fechaDos");
    String s_folioSolicitud     = request.getParameter("s_folioSolicitud");
    String s_usuarioSolicitante = request.getParameter("s_usuarioSolicitante");
    String s_tipoMovimiento     = request.getParameter("s_tipoMovimiento");
    String s_estatus            = request.getParameter("s_estatus");
    String s_ejecutivo          = request.getParameter("s_ejecutivo");
    String s_intermediario      = request.getParameter("s_intermediario");
    String s_documento          = request.getParameter("s_documento");
    String s_portafolio         = request.getParameter("s_portafolio");
    String s_productoEmpresa    = request.getParameter("s_productoEmpresaS");
    
    if (s_fechaUno           != null && s_fechaDos != null && !s_fechaUno.isEmpty() && !s_fechaDos.isEmpty()) {filtros.put("s_fechaUno", s_fechaUno);filtros.put("s_fechaDos", s_fechaDos);}
    if (s_folioSolicitud     != null && !s_folioSolicitud.isEmpty()) filtros.put("s_folioSolicitud", s_folioSolicitud);
    if (s_usuarioSolicitante != null && !s_usuarioSolicitante.isEmpty()) filtros.put("s_usuarioSolicitante",s_usuarioSolicitante);
    if (s_tipoMovimiento     != null && !s_tipoMovimiento.isEmpty()) filtros.put("s_tipoMovimiento",s_tipoMovimiento);
    if (s_estatus            != null && !s_estatus.isEmpty()) filtros.put("s_estatus",s_estatus);
    if (s_ejecutivo          != null && !s_ejecutivo.isEmpty()) filtros.put("s_ejecutivo",s_ejecutivo);
    if (s_intermediario      != null && !s_intermediario.isEmpty()) filtros.put("s_intermediario",s_intermediario);
    if (s_documento          != null && !s_documento.isEmpty()) filtros.put("s_documento",s_documento);
    if (s_portafolio         != null && !s_portafolio.isEmpty()) filtros.put("s_portafolio",s_portafolio);
    if (s_productoEmpresa    != null && !s_productoEmpresa.isEmpty()) filtros.put("s_productoEmpresa",s_productoEmpresa);
    
    ArrayList<InfoSolicitud> listado = consultaSolicitudesBean.getInfoSolicitudes(filtros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
} else if (informacion.equals("consultaDocumentos")){   
    Map<String, String> filtros = new HashMap<>();
    
    String s_fechaUno		= request.getParameter("d_fechaUno");
    String s_fechaDos           = request.getParameter("d_fechaDos");
    String s_folioSolicitud     = request.getParameter("d_folioSolicitud");
    String s_usuarioSolicitante = request.getParameter("d_usuarioSolicitante");
    
    String s_tipoMovimiento     = request.getParameter("d_tipoMovimiento");
    String s_estatus            = request.getParameter("d_estatus");
    String s_ejecutivo          = request.getParameter("d_ejecutivo");
    String s_documento          = request.getParameter("d_documento");
    String s_portafolio         = request.getParameter("d_portafolio");
    String s_productoEmpresa    = request.getParameter("d_productoEmpresa");
    String s_intermediario      = request.getParameter("d_intermediario");
    
    if (s_fechaUno           != null && s_fechaDos != null && !s_fechaUno.isEmpty() && !s_fechaDos.isEmpty()) {filtros.put("s_fechaUno", s_fechaUno);filtros.put("s_fechaDos", s_fechaDos);}
    if (s_folioSolicitud     != null && !s_folioSolicitud.isEmpty()) filtros.put("s_folioSolicitud", s_folioSolicitud);
    if (s_tipoMovimiento     != null && !s_tipoMovimiento.isEmpty()) filtros.put("s_tipoMovimiento",s_tipoMovimiento);
    if (s_estatus            != null && !s_estatus.isEmpty()) filtros.put("s_estatus",s_estatus);
    if (s_ejecutivo          != null && !s_ejecutivo.isEmpty()) filtros.put("s_ejecutivo",s_ejecutivo);
    if (s_intermediario      != null && !s_intermediario.isEmpty()) filtros.put("s_intermediario",s_intermediario.trim());
    if (s_documento          != null && !s_documento.isEmpty()) filtros.put("s_documento",s_documento);
    if (s_portafolio         != null && !s_portafolio.isEmpty()) filtros.put("s_portafolio",s_portafolio);
    if (s_productoEmpresa    != null && !s_productoEmpresa.isEmpty()) filtros.put("s_productoEmpresa",s_productoEmpresa);
   
    if (s_usuarioSolicitante    != null && !s_usuarioSolicitante.isEmpty()) filtros.put("s_usuarioSolicitante",s_usuarioSolicitante);
   
    
    ArrayList<InfoSolicitud> listado = consultaSolicitudesBean.getInfoDocumentos(filtros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}else if(informacion.equals("fileSol")){
    String icSolicitud = request.getParameter("icDocumento") == null ? "" :request.getParameter("icDocumento");
    String icInterFin        = request.getParameter("icInterFin") == null ? "" :request.getParameter("icInterFin");
    System.out.println("fileSOl "+icSolicitud);
    Integer icSol = Integer.parseInt(icSolicitud);
    Integer icIf  = Integer.parseInt(icInterFin);
    Map<String, ArrayList> mapaDocs = consultaSolicitudesBean.getFilesSoporteDocumento(icSol, icIf);
    ArrayList<ElementoCatalogo>listadoSoporte = mapaDocs.get("documentosSoporte");
    ArrayList<ElementoCatalogo>listadoFormalizacion = mapaDocs.get("documentosFormalizar");
    JSONArray jsObjArraySoporte = new JSONArray();
    jsObjArraySoporte = JSONArray.fromObject(listadoSoporte);
    
    JSONArray jsObjArrayFormalizacion = new JSONArray();
    jsObjArrayFormalizacion = JSONArray.fromObject(listadoFormalizacion);
    infoRegresar = "{\"success\": true,  \"documentosSoporte\": " + jsObjArraySoporte.toString() + ", \"documentosFormalizar\": "+jsObjArrayFormalizacion.toString() +"}";

}
else if (informacion.equals("getDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("docsFormalizacionNafin")){   //<------------------------------------------------------
    ArrayList<String> parametros = new ArrayList<>(); 
    String folioSolicitud = request.getParameter("folioSolicitud") == null ? "" : request.getParameter("folioSolicitud");
    String fechaSolicitud1 = request.getParameter("fechaSolicitud1") == null ? "" : request.getParameter("fechaSolicitud1");
    String fechaSolicitud2 = request.getParameter("fechaSolicitud2") == null ? "": request.getParameter("fechaSolicitud2");
    String usuarioSolicitante = request.getParameter("usuarioSolicitante") == null ? "" : request.getParameter("usuarioSolicitante");
    String usuarioEjecutivo = request.getParameter("usuarioEjecutivo") == null ? "" : request.getParameter("usuarioEjecutivo");
    String estatus = request.getParameter("estatus") == null ? "" : request.getParameter("estatus");
    String intermediario = request.getParameter("icIF") == null ? "0" : request.getParameter("icIF");
    Integer icIF = Integer.parseInt(intermediario);
    Map<String,String> params = new HashMap<>();
    params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF.toString());
    params.put(ConstantesFormalizacion.PARAM_IC_USUARIO, strLogin);
    ArrayList<DocValidacion> listado = formalizacionBean.getDocFormalizacionNAFIN(params);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}else if(informacion.equals("catFolios")){
    ArrayList<ElementoCatalogo>listado = consultaSolicitudesBean.getCatFolioSolicitudes(tipoConsulta);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEstatus")){
    ArrayList<ElementoCatalogo>listado = consultaSolicitudesBean.getCatEstatusSolicitud(tipoConsulta);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEjecutivotos")){
    ArrayList<ElementoCatalogo>listado = consultaSolicitudesBean.getCatEjecutivos(tipoConsulta);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catSolicitante")){
    ArrayList<ElementoCatalogo>listado = consultaSolicitudesBean.getCatSolicitantes(tipoConsulta);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catIF")){
    ArrayList<ElementoCatalogo>listado = consultaSolicitudesBean.getCatSolicitudesIF(tipoConsulta);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catTipoMovimiento")){
    ArrayList<ElementoCatalogo>listado = consultaSolicitudesBean.getCatTipoMovimiento(tipoConsulta);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catDocumento")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatDocumentos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catPortafolio")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catProductoEmpresa")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatProducto(Integer.valueOf(0), null);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("getSignedDoc")){
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
    String sicDoc = request.getParameter("icDoc") == null ? "" : request.getParameter("icDoc");
    Map<String,String> params = new HashMap<>();
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);   
        Integer icIF = Integer.parseInt(sicIF);
        Integer icDocumento = Integer.parseInt(sicDoc);
        params.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo+"");
        params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF+"");
        params.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento+"");
        String nombreArchivo = formalizacionBean.FirmaDocumento(strDirectorioPublicacion, params);	
        jsonObj.put("urlArchivo", nombreArchivo);		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa los parametros getSignedDoc");
        jsonObj.put("msg","Error al procesa los parametros icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}

log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>