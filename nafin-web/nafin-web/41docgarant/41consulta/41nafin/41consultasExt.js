Ext.onReady(function() {

    var POR_ASIGNAR = 'Por asignar';

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        Ext.getCmp('contenedorPrincipal').unmask();
        if (Ext.getCmp('winDoc')) {
            Ext.getCmp('winDoc').unmask();
        }
    }

  
    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        }]
    });
    
     Ext.define('ModelArchivo', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        },
         {
            name: 'version',
            type: 'string'
        }, {
            name: 'firmantes',
            mapping: 'firmantes'
        }, {
            name: 'icIF',
            mapping: 'icIF'
        }, {
            name: 'icDocumento',
            mapping: 'icDocumento'
        }]
    });
    
     function  procesarCargaNombresArchivos(record){
        var folio = record.get("folioSolicitud");
        var existeIf = record.get("icInterFin");
        var icDoc = record.get('icDocumento');
        var verCol = false;
        if(existeIf == 0){
            verCol = true;
        }
        var anchoColumnaNombreDoc  = 270;
        if (verCol){
            anchoColumnaNombreDoc = 538;
        }
        var winDoc = Ext.create('Ext.window.Window', {
            title: 'Documentos',
            id: 'winDoc',
            height: 230,
            width: 700,
            bodyStyle: 'background:#FFF;',
            modal: true,
            items: [{   // GRID DE DOCUMENTOS SOPORTE 
                xtype: 'grid',
                border: false,
                enableColumnMove  : false,
                enableColumnResize: true,
                enableColumnHide  : false,
                columns: [
                    {   
                        header: 'Documentos Soporte',
                        dataIndex: 'descripcion',
                        width: 548,
                        sortable: false
                    },
                    {
                        xtype:'actioncolumn',
                        header: "Ver Documento",
                        align: 'center',
                        sortable: false, 
                        width:140,
                        items: [{
                            iconCls: 'icoLupa',
                            tooltip: 'Ver Documento',
                            handler: function(grid, rowIndex, colIndex) {
                                winDoc.mask("Descargando archivo...");
                                var record = grid.getStore().getAt(rowIndex); 
                                Ext.Ajax.request({
                                    url: '41consultasExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'getDocFile',
                                        icArchivo: record.get('clave')
                                    }),
                                callback: descargaArchivos
                                });                                          
                            }
                        }]
                    }                        
                ],               
                store: storeArchivosSoporte
            },
            NE.util.getEspaciador(35),	
            {   // GRID DE DOCUMENTOS FORMALIZAR 
                xtype: 'grid',
                border: false,
                enableColumnMove  : false,
                enableColumnResize: true,
                enableColumnHide  : false,
                columns: [
                    {   
                        header: 'Documento a Formalizar',
                        dataIndex: 'descripcion',
                        width: anchoColumnaNombreDoc,
                        sortable: false
                    },
                    {   
                        header: 'Version',
                        dataIndex: 'version',
                        width: 50,
                        sortable: false
                    },
                    {   
                        header: 'Firmantes',
                        dataIndex: 'firmantes',
                        hidden: verCol,
                        width: 268,
                        sortable: false,
                        renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                            var fullUsuarios = record.get("firmantes");
                            var retorno = "";
                            for(var i =0; i< fullUsuarios.length; i++){
                                retorno += fullUsuarios[i].descripcion+"<\/br>";                                
                            }    
                            return '<span data-qtitle="Firmantes" data-qwidth="300" ' +
                                'data-qtip="' + retorno + '">' +
                                retorno + '</span>';
                        } 
                    },
                    {
                        xtype:'actioncolumn',
                        header: "Ver Documento",
                        align: 'center',
                        sortable: false, 
                        width:100,
                        items: [{
                            iconCls: 'icoLupa',
                            tooltip: 'Ver Documento',
                            handler: function(grid, rowIndex, colIndex) {
                                winDoc.mask("Descargando archivo...");
                                var record = grid.getStore().getAt(rowIndex); 
                                Ext.Ajax.request({
                                    url: '41consultasExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'getSignedDoc',
                                        icArchivo: record.get('clave'),
                                        icIF:   existeIf,
                                        icDoc: icDoc
                                    }),
                                callback: descargaArchivos
                                });                                          
                            }
                        }]
                    }                        
                ],               
                store: storeArchivoFormalizar
            }        
            ]
        }).show();
    }

    
     var storeArchivosSoporte = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }            
    });     
    
    var storeArchivoFormalizar = Ext.create('Ext.data.Store', {
       model: 'ModelArchivo',
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }            
    });      

    
    
     function mostrarVentanaDocumentos(record){
        var icDocumento = record.get("icDocumento");
        var icInterFin = record.get("icInterFin");
        storeArchivosSoporte.removeAll(true);
        storeArchivoFormalizar.removeAll(true);
        Ext.Ajax.request({
            url: '41consultasExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'fileSol',
                icDocumento: icDocumento,
                icInterFin: icInterFin
            }),
            callback:  function(opts, success, response) {
                var info = Ext.JSON.decode(response.responseText);
                if (success == true && info.success == true ) {
                    var documentosSoporte = info.documentosSoporte;
                    var documentosFormalizacion = info.documentosFormalizar;
                    for(i=0; i<documentosSoporte.length; i++){
                        storeArchivosSoporte.add({'clave': documentosSoporte[i].clave ,'descripcion': documentosSoporte[i].descripcion});
                    }
                    for(i=0; i<documentosFormalizacion.length; i++){
                        storeArchivoFormalizar.add({'clave': documentosFormalizacion[i].clave ,'descripcion': documentosFormalizacion[i].descripcion,'version': documentosFormalizacion[i].version,'firmantes': documentosFormalizacion[i].firmantes});
                    }
                }
                procesarCargaNombresArchivos(record);
                Ext.getCmp('contenedorPrincipal').unmask();                            
            }
        });
    }
    

    var verSolicitud = function(icSolicitud, folio) {
        window.location = '../../41solicitudes/41altaSolicitudExt.jsp?idMenu=41ALTASOLICITUD&icSol=' + icSolicitud + "&nfs=" + folio;
    }

    //----------*************  GRID  *************----------//
    Ext.define('ConsultaSolicitudesDataModel', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'fechaSolicitud',
            mapping: 'fechaSolicitud'
        }, {
            name: 'folioSolicitud',
            mapping: 'folioSolicitud'
        }, {
            name: 'usuarioSolicitante',
            mapping: 'usuarioSolicitante'
        }, {
            name: 'intermediarioFinanciero',
            mapping: 'intermediarioFinanciero'
        }, {
            name: 'usuarioEjecutivo',
            mapping: 'usuarioEjecutivo'
        }, {
            name: 'tipoMovimiento',
            mapping: 'tipoMovimiento'
        }, {
            name: 'tipoDocumento',
            mapping: 'tipoDocumento'
        }, {
            name: 'version',
            mapping: 'version'
        }, {
            name: 'portafolio',
            mapping: 'portafolio'
        }, {
            name: 'nombreProductoEmpresa',
            mapping: 'nombreProductoEmpresa'
        }, {
            name: 'icSolicitud',
            mapping: 'icSolicitud'
        },
        {
            name: 'icDocumento',
            mapping: 'icDocumento'
        },
        {
            name: 'icInterFin',
            mapping: 'icInterFin'
        },
        {
            name: 'estatus',
            mapping: 'estatus'
        }
        ]
    });

    var procesarConsultaSolicitudes = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'No existe informaci�n con los criterios determinados');
            }
        }
        gridSolicitudes.el.unmask();
    }

    
    var storeSolicitudes = Ext.create('Ext.data.Store', {
        model: 'ConsultaSolicitudesDataModel',
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'consultaSolicitudes'
            }
        },
        autoLoad: false, // true Modify
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsultaSolicitudes
        }
    });




/* * * * * * * * * * * * * * * * * * GRID DE RESULTADOS SOLICITUDES * * * * * * * * * * * * * * * * * * * * * * * */
    var gridSolicitudes = Ext.create('Ext.grid.Panel', {
        id: 'd_gridId',
        store: storeSolicitudes,
        stripeRows: true,
        title: 'Resultado de Solicitudes',
        width: 944,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
                header: "Fecha de<br/>Solicitud",
                dataIndex: 'fechaSolicitud',
                width: 90,
                sortable: true,
                hideable: true
            }, {
                header: "Folio de<br/>Solicitud",
                dataIndex: 'folioSolicitud',
                width: 80,
                sortable: true,
                hideable: true,
                tdCls: 'cursorPointer',
                listeners: {
                    click: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
                        var rec = storeSolicitudes.getAt(iRowIdx.index);
                        verSolicitud(rec.get("icSolicitud"), rec.get('folioSolicitud'));
                    }
                }
            }, {
                header: "Solicitante",
                dataIndex: 'usuarioSolicitante',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Solicitante" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }                
            }, {
                header: "Intermediario<br/>Financiero",
                dataIndex: 'intermediarioFinanciero',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Intermediario" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }                     
            }, {
                header: "Ejecutivo",
                dataIndex: 'usuarioEjecutivo',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Ejecutivo" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }     
            }, 
            {
                header: "Tipo de Movimiento",
                dataIndex: 'tipoMovimiento',
                align: 'left',
                width: 170,
                sortable: true,
                hideable: true 
            },     
            {
                header: "Tipo de Documento",
                dataIndex: 'tipoDocumento',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Tipo Documento" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }     
            },
            {
                header: "Versi�n",
                dataIndex: 'version',
                align: 'center',
                width: 80,
                sortable: true,
                hideable: true
            },            
            {
                header: "Portafolio",
                dataIndex: 'portafolio',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true
            },
            {
                header: "Nombre de Producto<br/>/Empresa",
                dataIndex: 'nombreProductoEmpresa',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Producto/Empresa" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }                     
            },
            {
                header: "Documentos<br/>Soporte",
                menuText: "Documentos<br/>Soporte",
                xtype: 'actioncolumn',
                align: 'center',
                dataIndex: 'icSolicitud',
                hideable: true,
                sortable: true,
                width: 80,
                items: [{
                    iconCls: 'icoFolderTable',
                    tooltip: 'Mostrar Documentos',
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        mostrarVentanaDocumentos(record);
                    }
                }]
            },
            {
                header: "Estatus",
                dataIndex: 'estatus',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Estatus" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }                    
            }
        ]
    });

    //----------************* CATALOGOS  *************----------//

    var sstoreFolioSolicitud = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catFolios',
                tipoConsulta: 'S'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
     var sstoreFolioDocumentos = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catFolios',
                tipoConsulta: 'D'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });



    var sstoreSolicitantes = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catSolicitante',
                tipoConsulta: 'S'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
     var sstoreSolicitantesDoc = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catSolicitante',
                tipoConsulta: 'D'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    

    var sstoreTipoMovimiento = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catTipoMovimiento',
                tipoConsulta: 'S'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

  var sstoreTipoMovimientoDoc = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catTipoMovimiento',
                tipoConsulta: 'D'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var sstoreIntermediario = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catIF',
                tipoConsulta: 'S'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
     var sstoreIntermediarioDoc = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catIF',
                tipoConsulta: 'D'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
    var storeTipoDocumento = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catDocumento'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
    var storeTipoPortafolio = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catPortafolio'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });    
    
    var storeProductoEmpresa = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catProductoEmpresa'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });    
    
        var storeProductoEmpresaDoc = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catProductoEmpresa'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });    
    

    var storeEstatus = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEstatus',
                tipoConsulta: 'S'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });    
    
     var storeEstatusDoc = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEstatus',
                tipoConsulta: 'D'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });    
    
    
     var storeEjecutivos = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEjecutivotos',
                tipoConsulta: 'S'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
    var storeEjecutivosDoc = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEjecutivotos',
                tipoConsulta: 'D'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });    
    
    

    var error1Fechas = "Debes capturar la fecha inicial y final";
    var error2Fechas = "La fecha final debe ser posterior a la inicial";

    var validarFechas = function(val) {
        var fechaUno = Ext.getCmp('s_fechaUno');
        var fechaDos = Ext.getCmp('s_fechaDos');
        var fechaUnoVal = fechaUno.getValue();
        var fechaDosVal = fechaDos.getValue();
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1Fechas);
            fechaDos.markInvalid(error1Fechas);
            return error1Fechas;
        }
        if (fechaUno != null && fechaUno > fechaDos) {
            fechaUno.markInvalid(error2Fechas);
            fechaDos.markInvalid(error2Fechas);
            fechaDos.focus();
            return error2Fechas;
        }
        return true;
    }
    
     var validarFechasD = function(val) {
        var fechaUno = Ext.getCmp('d_fechaUno');
        var fechaDos = Ext.getCmp('d_fechaDos');
        var fechaUnoVal = fechaUno.getValue();
        var fechaDosVal = fechaDos.getValue();
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1Fechas);
            fechaDos.markInvalid(error1Fechas);
            return error1Fechas;
        }
        if (fechaUno != null && fechaUno > fechaDos) {
            fechaUno.markInvalid(error2Fechas);
            fechaDos.markInvalid(error2Fechas);
            fechaDos.focus();
            return error2Fechas;
        }
        return true;
    }

    /**********************  PANEL BUSQUEDA Solicitudes *************************/
    var formBusquedaSol = new Ext.form.FormPanel({
        name: 'formBusquedaSol',
        id: 'formBusquedaSol',
        width: 943,
        height: 'auto',
        frame: true,
        border: 0,
        layout: 'column',
        items: [{
            xtype: 'fieldset',
            id: 's_fieldsBusqueda',
            height: 200,
            border: false,
            columnWidth: 0.5,
            items: [ 
            {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 's_fechaUno',
                    name: 's_fechaUno',
                    fieldLabel: 'Fecha de Solicitud',
                    padding: 5,
                    width: 215,
                    emptyText: 'Desde',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 's_fechaDos',
                    name: 's_fechaDos',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            }, 
            {
                xtype: 'combo',
                name: 's_folioSolicitud',
                id: 's_folioSolicitud',
                fieldLabel: 'Folio de Solicitud',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Folios',
                store: sstoreFolioSolicitud,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false,
                listeners:{
                // Fires before a request is made. op is an Ext.data.Operation object
                    beforeload:function(store,op){ 
                    // Set request parameters (without overriding other parameters)
                    op.params = Ext.apply(op.params||{},{
                         informacion: 'catFolios',
                         tipoConsulta: 'S'
                    });
                    store.reload();
                    }
                }

            },            
            {
                xtype: 'combo',
                id: 's_usuarioSolicitante',
                name: 's_usuarioSolicitante',
                fieldLabel: 'Solicitante',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Solicitantes',
                store: sstoreSolicitantes,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }, 
            {
                xtype: 'combo',
                id: 's_tipoMovimiento',
                name: 's_tipoMovimiento',
                fieldLabel: 'Tipo de movimiento',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los movimientos',
                store: sstoreTipoMovimiento,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },            
            {
                xtype: 'combo',
                id: 's_estatus',
                name: 's_estatus',
                fieldLabel: 'Estatus de la Solicitud:',
                padding: 5,
                width: 400,
                store: storeEstatus,
                emptyText: 'Todos los Estatus',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                editable: false,
                allowBlank: true
            }            
        ]
        }, {
            xtype: 'fieldset',
            height: 200,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [
            {
                xtype: 'combo',
                id: 's_ejecutivo',
                name: 's_ejecutivo',
                fieldLabel: 'Ejecutivo:',
                padding: 5,
                width: 440,
                store: storeEjecutivos,
                emptyText: 'Todos los Ejecutivos',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                editable: false,
                allowBlank: true
            },            
            {
                xtype: 'combo',
                id: 's_intermediario',
                name: 's_intermediario',
                fieldLabel: 'Intermediario Financiero:',
                padding: 5,
                width: 440,
                store: sstoreIntermediario,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Intermediarios',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            } ,
            {
                xtype: 'combo',
                id: 's_documento',
                name: 's_documento',
                fieldLabel: 'Tipo de Documento',
                padding: 5,
                width: 440,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: false,
                store: storeTipoDocumento,
                emptyText: 'Todos los documentos',
                displayField: 'descripcion',
                editable: false,
                valueField: 'clave'
            }, 
            {
                xtype: 'combo',
                id: 's_portafolio',
                name: 's_portafolio',
                fieldLabel: 'Portafolio:',
                padding: 5,
                width: 440,
                store: storeTipoPortafolio,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Portafolios',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                id: 's_productoEmpresaS',
                name: 's_productoEmpresaS',
                fieldLabel: 'Nombre Producto/Empresa:',
                padding: 5,
                width: 440,
                store: storeProductoEmpresa,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los productos/empresas',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }
            ]
        }],
        buttons: [{
            text: 'Buscar',
            id: 's_btnBuscar',
            iconCls: 'icoBuscar',
            handler: function(boton, evento) {
                var resultado = validarFechas();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    gridSolicitudes.el.mask('Cargando...', 'x-mask-loading');
                    storeSolicitudes.load({
                        params: Ext.apply(formBusquedaSol.getForm().getValues())
                    });
                }
            }
        }, {
            id: 's_btnLimpiar',
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function() {
                Ext.getCmp('formBusquedaSol').getForm().reset();
            }
        }]
    });



    var panelSolicitudes = new Ext.Container({
        id: 'panelSolicitudes',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            NE.util.getEspaciador(20),
            formBusquedaSol,
            NE.util.getEspaciador(30),
            gridSolicitudes,
            NE.util.getEspaciador(20)
        ]
    });
    
    
    
/*******************************************************************************************************************/
/* * * * * * * * * * * * * * * * * * * * * D O C U M E N T O S * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*******************************************************************************************************************/
    var procesarConsultaDocumentos = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'No existe informaci�n con los criterios determinados');
            }
        }
        gridDocumentos.el.unmask();

    }
    
      //----------*************  GRID  *************----------//
    Ext.define('ConsultaDocumentosDataModel', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'fechaSolicitud',
            mapping: 'fechaSolicitud'
        }, {
            name: 'folioSolicitud',
            mapping: 'folioSolicitud'
        }, {
            name: 'usuarioSolicitante',
            mapping: 'usuarioSolicitante'
        }, {
            name: 'intermediarioFinanciero',
            mapping: 'intermediarioFinanciero'
        }, {
            name: 'usuarioEjecutivo',
            mapping: 'usuarioEjecutivo'
        }, {
            name: 'tipoMovimiento',
            mapping: 'tipoMovimiento'
        }, {
            name: 'tipoDocumento',
            mapping: 'tipoDocumento'
        }, {
            name: 'version',
            mapping: 'version'
        }, {
            name: 'portafolio',
            mapping: 'portafolio'
        }, {
            name: 'nombreProductoEmpresa',
            mapping: 'nombreProductoEmpresa'
        }, 
        {
            name: 'fechaMaximaFormalizacion',
            mapping: 'fechaMaximaFormalizacion'
        },
        {
            name: 'numeroFirmantes',
            mapping: 'numeroFirmantes'
        },
        {
            name: 'firmantes',
            mapping: 'firmantes'
        },
        {
            name: 'numeroFirmantesNafin',
            mapping: 'numeroFirmantesNafin'
        },
        {
            name: 'firmantesNafin',
            mapping: 'firmantesNafin'
        },
        {
            name: 'fechaFormalizacion',
            mapping: 'fechaFormalizacion'
        },        
        {
            name: 'icSolicitud',
            mapping: 'icSolicitud'
        }, 
        {
            name: 'icDocumento',
            mapping: 'icDocumento'
        },
        {
            name: 'icInterFin',
            mapping: 'icInterFin'
        },
        {
            name: 'estatus',
            mapping: 'estatus'
        },{  name: 'icArchivoBO', 	    
             mapping : 'icArchivoBO' 	        
        }]
    });
    

    var storeDocumentos = Ext.create('Ext.data.Store', {
        model: 'ConsultaDocumentosDataModel',
        proxy: {
            type: 'ajax',
            url: '41consultasExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'consultaDocumentos',
                usuarioEjecutivo: '0'
            }
        },
        autoLoad: false, // modify
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsultaDocumentos
        }
    });

    function descargaArchivo(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        Ext.getCmp("tapDoc").unmask();
    }

    function getFileBO(icDocumento, icArchivoBO){
        Ext.getCmp("tapDoc").mask("Descargando base de operaci�n...");
        Ext.Ajax.request({
            url: '41consultasExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getDocFile',
                icArchivo: icArchivoBO
            }),
            callback: descargaArchivo
        });     
    }

/* * * * * * * * * * * * * * * * * * GRID DE RESULTADOS DOCUMENTOS * * * * * * * * * * * * * * * * * * * * * * * */
    var gridDocumentos = Ext.create('Ext.grid.Panel', {
        id: 'd_gridDocumentos',
        store: storeDocumentos,
        stripeRows: true,
        title: 'Resultado de Documentos',
        width: 944,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
                header: "Fecha <br/>Entrega",
                dataIndex: 'fechaSolicitud',
                width: 90,
                sortable: true,
                hideable: true
            }, {
                header: "Folio",
                dataIndex: 'folioSolicitud',
                width: 80,
                sortable: true,
                hideable: true,
                tdCls: 'cursorPointer',
                listeners: {
                    click: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
                        var rec = storeDocumentos.getAt(iRowIdx.index);
                        verSolicitud(rec.get("icSolicitud"), rec.get('folio'));
                    }
                }
            }, {
                header: "Solicitante",
                dataIndex: 'usuarioSolicitante',
                width: 150,
                sortable: true,
                hideable: true
            }, {
                header: "Intermediario<br/>Financiero",
                dataIndex: 'intermediarioFinanciero',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true
            }, {
                header: "Ejecutivo",
                dataIndex: 'usuarioEjecutivo',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value, meta, record) {
                    if (value == POR_ASIGNAR) {
                        return 'Por asignar <span class ="icoReportUser">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                    } else {
                        return value;
                    }
                }
            }, 
            {
                header: "Tipo de Movimiento",
                dataIndex: 'tipoMovimiento',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value, meta, record) {
                    if (value == POR_ASIGNAR) {
                        return 'Nuevo|Nueva Version| Adhesion IF';
                    } else {
                        return value;
                    }
                }
            },     
            {
                header: "Tipo de Documento",
                dataIndex: 'tipoDocumento',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value, meta, record) {
                    if (value == POR_ASIGNAR) {
                        return 'Por asignar <span class ="icoReportUser">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                    } else {
                        return value;
                    }
                }
            },
            {
                header: "Versi�n",
                dataIndex: 'version',
                align: 'center',
                width: 80,
                sortable: true,
                hideable: true,
                renderer: function(value, meta, record) {
                    if (value == POR_ASIGNAR) {
                        return 'Por asignar <span class ="icoReportUser">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                    } else {
                        return value;
                    }
                }
            },            
            {
                header: "Portafolio",
                dataIndex: 'portafolio',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value, meta, record) {
                    if (value == POR_ASIGNAR) {
                        return 'Por asignar <span class ="icoReportUser">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                    } else {
                        return value;
                    }
                }
            },
            {
                header: "Nombre de Producto/Empresa",
                dataIndex: 'nombreProductoEmpresa',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Nombre" data-qwidth="250" ' +
                        'data-qtip="' + value + '">' +
                        value + '<\/span>';
                }                 
            },
            {
                header: "Documentos Soporte<br>y Formalizado",
                menuText: "Documentos Soporte y Formalizado",
                xtype: 'actioncolumn',
                align: 'center',
                dataIndex: 'numeroArchivos',
                hideable: true,
                sortable: true,
                width: 100,
                items: [{
                    iconCls: 'icoFolderTable',
                    tooltip: 'Mostrar Documentos',
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        mostrarVentanaDocumentos(record);
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        return record.get('numeroArchivos') == 0;
                    }
                }]
            },
            {
                header: "Fecha M�xima de<br>Formalizaci�n IF",
                dataIndex: 'fechaMaximaFormalizacion',
                align: 'center',
                width: 100,
                sortable: true,
                hideable: true
            },
            {
                header: "M�nimo de Firmas IF",
                dataIndex: 'numeroFirmantes',
                align: 'center',
                width: 100,
                sortable: true,
                hideable: true
            },
            {
                header: "Firmantes IF",
                dataIndex: 'firmantes',
                align: 'center',
                width: 100,
                sortable: true,
                hideable: true,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    var fullUsuarios = record.get("firmantes");
                    var retorno = "";
                    for(var i =0; i< fullUsuarios.length; i++){
                           retorno += fullUsuarios[i].descripcion+"<\/br>";
                    }    
                    return '<span data-qtitle="Firmantes IF" data-qwidth="280" ' +
                        'data-qtip="' + retorno + '">' + retorno + '<\/span>';                    
                } 
            },
            {
                header: "N�mero de firmantes<br>NAFIN/1148",
                dataIndex: 'numeroFirmantesNafin',
                align: 'center',
                width: 120,
                sortable: true,
                hideable: true
            },
            {
                header: "Firmantes NAFIN/1148",
                dataIndex: 'firmantesNafin',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    var fullUsuarios = record.get("firmantesNafin");
                    var retorno = "";
                    for(var i =0; i< fullUsuarios.length; i++){
                           retorno += fullUsuarios[i].descripcion+"<\/br>";
                    }    
                    return '<span data-qtitle="Firmantes NAFIN/1148" data-qwidth="280" ' +
                        'data-qtip="' + retorno + '">' + retorno + '<\/span>';   
                }
            },
            {
                header: "Fecha de Formalizaci�n",
                dataIndex: 'fechaFormalizacion',
                align: 'center',
                width: 100,
                sortable: true,
                hideable: true
            },
            {
                header: "Estatus",
                dataIndex: 'estatus',
                align: 'left',
                width: 100,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Estatus" data-qwidth="250" ' +
                        'data-qtip="' + value + '">' +
                        value + '<\/span>';
                }                 
            },
            {
                xtype:'actioncolumn',
                header: "Base de Operaci�n",
                dataIndex: 'icArchivoBO',
                align: 'center',
                width: 120,
                sortable: true,
                hideable: true,
                items: [{
                    iconCls: 'bricks',
                    tooltip: 'Ver Base Operaci�n',
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex); 
                        getFileBO(record.get("icDocumento"), record.get("icArchivoBO"));
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        return record.get("icArchivoBO") == 0 ;
                    }
                }]                 
            }            
        ]
    });
    
    /**********************  PANEL BUSQUEDA DOCUMENTOS *************************/
    var formBusquedaDoc = new Ext.form.FormPanel({
        name: 'formBusquedaDoc',
        id: 'formBusquedaDoc',
        width: 943,
        frame: true,
        height: 'auto',
        border: 0,
        layout: 'column',
        items: [{
            xtype: 'fieldset',
            id: 'd_fieldsBusqueda',
            height: 270,
            border: false,
            columnWidth: 0.5,
            items: [ 
            {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'd_fechaUno',
                    name: 'd_fechaUno',
                    fieldLabel: 'Fecha de Entrega IF',
                    padding: 5,
                    width: 215,
                    emptyText: 'Desde',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 'd_fechaDos',
                    name: 'd_fechaDos',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            }, 
            {
                xtype: 'combobox',
                name: 'd_folioSolicitud',
                id: 'd_folioSolicitud',
                fieldLabel: 'Folio de Solicitud',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Folios',
                store: sstoreFolioDocumentos,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false,
                 listeners:{
                // Fires before a request is made. op is an Ext.data.Operation object
                    beforeload:function(store,op){ 
                    // Set request parameters (without overriding other parameters)
                    op.params = Ext.apply(op.params||{},{
                         informacion: 'catFolios',
                         tipoConsulta: 'D'
                    });
                    store.reload();
                    }
                }
            },            
            {
                xtype: 'combo',
                id: 'd_usuarioSolicitante',
                name: 'd_usuarioSolicitante',
                fieldLabel: 'Solicitante',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Solicitantes',
                store: sstoreSolicitantesDoc,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }, 
            {
                xtype: 'combo',
                id: 'd_tipoMovimiento',
                name: 'd_tipoMovimiento',
                fieldLabel: 'Tipo de movimiento',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los movimientos',
                store: sstoreTipoMovimientoDoc,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },            
            {
                xtype: 'combo',
                id: 'd_estatus',
                name: 'd_estatus',
                fieldLabel: 'Estatus de la Documento:',
                padding: 5,
                width: 400,
                store: storeEstatusDoc,
                emptyText: 'Todos los Estatus',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                editable: false,
                allowBlank: true
            },
            {
                xtype: 'combo',
                id: 'd_ejecutivo',
                name: 'd_ejecutivo',
                fieldLabel: 'Ejecutivo:',
                padding: 5,
                width: 400,
                store: storeEjecutivosDoc,
                emptyText: 'Todos los Ejecutivos',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                editable: false,
                allowBlank: true
            }           
        ]
        }, {
            xtype: 'fieldset',
            height: 200,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [
             {
                xtype: 'combo',
                id: 'd_intermediario',
                name: 'd_intermediario',
                fieldLabel: 'Intermediario Financiero:',
                padding: 5,
                width: 440,
                store: sstoreIntermediarioDoc,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Intermediarios',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                id: 'd_documento',
                name: 'd_documento',
                fieldLabel: 'Tipo de Documento',
                padding: 5,
                width: 440,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: false,
                store: storeTipoDocumento,
                emptyText: 'Todos los documentos',
                displayField: 'descripcion',
                editable: false,
                valueField: 'clave'
            }, 
            {
                xtype: 'combo',
                id: 'd_portafolio',
                name: 'd_portafolio',
                fieldLabel: 'Portafolio:',
                padding: 5,
                width: 440,
                store: storeTipoPortafolio,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Portafolios',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                id: 'd_productoEmpresa',
                name: 'd_productoEmpresa',
                fieldLabel: 'Nombre Producto/Empresa:',
                padding: 5,
                width: 440,
                store: storeProductoEmpresaDoc,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los productos/empresas',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                id: 'd_baseOperacion',
                name: 'd_baseOperacion',
                fieldLabel: 'Base de Operaci�n',
                padding: 5,
                width: 440,
                //store: storeEstatus,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todas las Bases',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'button',
                iconCls: 'icoPdf',
                text : 'Convenio Aut�grafo',
                hidden: true
            }            
            ]
        }],
        buttons: [{
            text: 'Buscar',
            id: 'd_btnBuscar',
            iconCls: 'icoBuscar',
            handler: function(boton, evento) {
                var resultado = validarFechasD();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    gridDocumentos.el.mask('Cargando...', 'x-mask-loading');
                    storeDocumentos.load({
                        params: Ext.apply(formBusquedaDoc.getForm().getValues())
                    });
                }
            }
        }, {
            id: 'd_btnLimpiar',
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function() {
                Ext.getCmp('formBusquedaDoc').getForm().reset();
            }
        }]
    });
    
    
    var panelDocumentos = new Ext.Container({
        id: 'panelDocumentos',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            NE.util.getEspaciador(20),
            formBusquedaDoc,
            NE.util.getEspaciador(30),
            gridDocumentos,
            NE.util.getEspaciador(20)
        ]
    });
    
    
    
/*************************************** CONTENEDOR PRINCIPAL  *******************************************************/      
    
    Ext.tip.QuickTipManager.init();

    Ext.create('Ext.tab.Panel', {
        width: 945,
        id: 'contenedorPrincipal',
        minHeight: 700,
        title: 'Consulta de Documentos y Solicitudes',
        renderTo: 'areaContenido',
        items: [{
            id: 'tabSol',
            title: 'Solicitudes',
            tooltip: 'Consulta de Solicitudes',
            items: [panelSolicitudes]
        }
        , {
            id: 'tapDoc',
            title: 'Documentos',
            tooltip: 'Consulta de Documentos',
            items: [panelDocumentos]
        }]
    });


});