<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
        com.nafin.docgarantias.*,
	com.nafin.docgarantias.consultas.*,
        com.nafin.docgarantias.formalizacion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";

String infoRegresar="";

log.info("INFORMACION: "+informacion);
ConsultaSolicitudesIf consultaSolicitudesIfBean = ServiceLocator.getInstance().lookup("ConsultaSolicitudesIf",ConsultaSolicitudesIf.class);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);
String s_intermediario = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
ConsultaSolicitudes consultaSolicitudesBean = ServiceLocator.getInstance().lookup("ConsultaSolicitudes",ConsultaSolicitudes.class);
    
if (informacion.equals("consultaSolicitudes")){   
    Map<String, String> filtros = new HashMap<>();
    
    String s_fechaUno		= request.getParameter("s_fechaUno");
    String s_fechaDos           = request.getParameter("s_fechaDos");
    String s_folioSolicitud     = request.getParameter("s_folioSolicitud");
    String s_tipoMovimiento     = request.getParameter("s_tipoMovimiento");
    String s_estatus            = request.getParameter("s_estatus");
    String s_ejecutivo          = request.getParameter("s_ejecutivo");
    String s_documento          = request.getParameter("s_documento");
    String s_portafolio         = request.getParameter("s_portafolio");
    String s_productoEmpresa    = request.getParameter("s_productoEmpresa");
    
    String s_fechaUnoMax	= request.getParameter("s_fechaUnoMax");
    String s_fechaDosMax        = request.getParameter("s_fechaDosMax");
    String s_fechaUnoFor	= request.getParameter("s_fechaUnoFor");
    String s_fechaDosFor        = request.getParameter("s_fechaDosFor");
    
    
    
    
    if (s_fechaUno           != null && s_fechaDos != null && !s_fechaUno.isEmpty() && !s_fechaDos.isEmpty()) {filtros.put("s_fechaUno", s_fechaUno);filtros.put("s_fechaDos", s_fechaDos);}
    if (s_folioSolicitud     != null && !s_folioSolicitud.isEmpty()) filtros.put("s_folioSolicitud", s_folioSolicitud);
    if (s_tipoMovimiento     != null && !s_tipoMovimiento.isEmpty()) filtros.put("s_tipoMovimiento",s_tipoMovimiento);
    if (s_estatus            != null && !s_estatus.isEmpty()) filtros.put("s_estatus",s_estatus);
    if (s_ejecutivo          != null && !s_ejecutivo.isEmpty()) filtros.put("s_ejecutivo",s_ejecutivo);
    if (s_intermediario      != null && !s_intermediario.isEmpty()) filtros.put("s_intermediario",s_intermediario);
    if (s_documento          != null && !s_documento.isEmpty()) filtros.put("s_documento",s_documento);
    if (s_portafolio         != null && !s_portafolio.isEmpty()) filtros.put("s_portafolio",s_portafolio);
    if (s_productoEmpresa    != null && !s_productoEmpresa.isEmpty()) filtros.put("s_productoEmpresa",s_productoEmpresa);
   
    if (s_fechaUnoMax      != null && !s_fechaUnoMax.isEmpty()) filtros.put("s_fechaUnoMax",s_fechaUnoMax);
    if (s_fechaDosMax      != null && !s_fechaDosMax.isEmpty()) filtros.put("s_fechaDosMax",s_fechaDosMax);
    if (s_fechaUnoFor      != null && !s_fechaUnoFor.isEmpty()) filtros.put("s_fechaUnoFor",s_fechaUnoFor);
    if (s_fechaDosFor      != null && !s_fechaDosFor.isEmpty()) filtros.put("s_fechaDosFor",s_fechaDosFor);
   
    
    
    ArrayList<InfoSolicitud> listado = consultaSolicitudesIfBean.getInfoSolicitudes(filtros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("fileSol")){
    String documento = request.getParameter("icDocumento") == null ? "" :request.getParameter("icDocumento");
    System.out.println("fileSOl "+documento);
    Integer icDocumento = Integer.parseInt(documento);
    Integer icIF = Integer.valueOf(s_intermediario);
    Map<String, ArrayList> mapaDocs = consultaSolicitudesBean.getFilesSoporteDocumento(icDocumento, icIF);
    ArrayList<ElementoCatalogo>listadoFormalizacion = mapaDocs.get("documentosFormalizar");
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listadoFormalizacion);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("getDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("getDocFileIF")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    
    try{
        Integer icArchivo = consultaSolicitudesIfBean.getArchivoIF(s_intermediario);
        //Integer icArchivo = Integer.parseInt(sicArchivo);        
        if(icArchivo != 0){
            ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
            jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        }else{
             jsonObj.put("urlArchivo","0");
             jsonObj.put("msg","El Intermediario financiero no tiene ningun archivo asignado");
             //exito = false;
        }
       exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if(informacion.equals("catFolios")){
    ArrayList<ElementoCatalogo>listado = consultaSolicitudesBean.getCatFolioSolicitudes(s_intermediario);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEstatus")){
    ArrayList<ElementoCatalogo>listado = consultaSolicitudesBean.getCatEstatusSolicitud(s_intermediario);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEjecutivotos")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatEjecutivos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catSolicitante")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatSolicitantes();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catIF")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatSolicitudesIF();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catTipoMovimiento")){
    ArrayList<ElementoCatalogo>listado = consultaSolicitudesBean.getCatTipoMovimiento(s_intermediario);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catDocumento")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatDocumentos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catPortafolio")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catProductoEmpresa")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatProducto(Integer.valueOf(0), null);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("getSignedDoc")){
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    String sicDoc = request.getParameter("icDoc") == null ? "" : request.getParameter("icDoc");
    Map<String,String> params = new HashMap<>();
    try{
        Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);
        Integer icArchivo = Integer.parseInt(sicArchivo);   
        Integer icIF = Integer.parseInt(s_intermediario);
        Integer icDocumento = Integer.parseInt(sicDoc);
        params.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo+"");
        params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF+"");
        params.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento+"");
        String nombreArchivo = formalizacionBean.FirmaDocumento(strDirectorioPublicacion, params);	
        
        jsonObj.put("urlArchivo", nombreArchivo);		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa los parametros getSignedDoc");
        jsonObj.put("msg","Error al procesa los parametros icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}

log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>