Ext.onReady(function() {


    var documentoEnObservacion = null;
    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        if (Ext.getCmp('winDoc')) {
            Ext.getCmp('winDoc').unmask();
        }
    }
    
     function descargaArchivosIF(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            if(archivo == "0"){
                var mensajeError = infoR.msg;
                Ext.MessageBox.alert('Warning', mensajeError);    
            }else{
                archivo = archivo.replace('/nafin', '');
                var params = {
                    nombreArchivo: archivo
                };
                var forma = Ext.getDom('formAux');
                forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
                forma.method = 'post';
                forma.target = '_self';
                forma.submit();
            }
        } else {
            NE.util.mostrarConnError(response, opts);
        }      
    }


    function descargaArchivo(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        Ext.getCmp("panelSolicitudes").unmask();
    }

    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        }]
    });

    var procesarCargaNombresArchivos = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'La Solicitud no tiene archivos cargados');
            } else {

                var winDoc = Ext.create('Ext.window.Window', {
                    title: 'Documento Formalizados',
                    id: 'winDoc',
                    height: 200,
                    width: 500,
                    layout: 'fit',
                    modal: true,
                    items: {
                        xtype: 'grid',
                        border: false,
                        enableColumnMove: false,
                        enableColumnResize: true,
                        enableColumnHide: false,
                        columns: [{
                            header: 'Nombre de archivo',
                            dataIndex: 'descripcion',
                            width: 288,
                            sortable: false
                        }, {
                            xtype: 'actioncolumn',
                            header: "Documento",
                            align: 'center',
                            sortable: false,
                            width: 200,
                            items: [{
                                iconCls: 'icoLupa',
                                tooltip: 'Ver Documento',
                                handler: function(grid, rowIndex, colIndex) {
                                    winDoc.mask("Descargando archivo...")
                                    var record = grid.getStore().getAt(rowIndex);
                                    Ext.Ajax.request({
                                        url: '41consultasIfExt.data.jsp',
                                        params: Ext.apply({
                                            informacion: 'getSignedDoc',
                                            icArchivo: record.get('clave'),
                                            icDoc: documentoEnObservacion
                                        }),
                                        callback: descargaArchivos
                                    });
                                }
                            }]
                        }],
                        store: storeArchivos
                    }
                }).show();
            }
        }
    }

    var storeArchivos = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        proxy: {
            type: 'ajax',
            url: '41consultasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'fileSol'
            }
        },
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarCargaNombresArchivos
        }
    });

    /** Carga los archivo Soporte de la solicitud */
    function mostrarVentanaSolicitudes(record) {
        //var icSolicitud =  record.get('icSolicitud');
        documentoEnObservacion = record.get('icDocumento');
        storeArchivos.removeAll(true);
        storeArchivos.load({
            informacion: 'fileSol',
            params: Ext.apply({
                icDocumento: documentoEnObservacion
            })
        });
    }

    
    
     var  botonContratoPDF = Ext.create('Ext.Button', {
        text: 'Ver Convenio Autógrafo',
        renderTo: Ext.getBody(),
        iconCls : 'icoPdf',
        handler: function() {
            Ext.Ajax.request({
            url: '41consultasIfExt.data.jsp',
            params: Ext.apply({
                informacion: 'getDocFileIF'
            }),
            callback: descargaArchivosIF
            });
        }
    });


    //----------*************  GRID  *************----------//
    Ext.define('ConsultaSolicitudesDataModel', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'fechaSolicitud',
            mapping: 'fechaSolicitud'
        }, {
            name: 'folioSolicitud',
            mapping: 'folioSolicitud'
        }, {
            name: 'usuarioSolicitante',
            mapping: 'usuarioSolicitante'
        }, {
            name: 'usuarioEjecutivo',
            mapping: 'usuarioEjecutivo'
        }, {
            name: 'tipoMovimiento',
            mapping: 'tipoMovimiento'
        }, {
            name: 'tipoDocumento',
            mapping: 'tipoDocumento'
        }, {
            name: 'version',
            mapping: 'version'
        }, {
            name: 'portafolio',
            mapping: 'portafolio'
        }, {
            name: 'nombreProductoEmpresa',
            mapping: 'nombreProductoEmpresa'
        }, {
            name: 'icSolicitud',
            mapping: 'icSolicitud'
        }, {
            name: 'estatus',
            mapping: 'estatus'
        },
        {
            name: 'firmantes',
            mapping: 'firmantes'
        },
         {
            name: 'fechaMaximaFormalizacion',
            mapping: 'fechaMaximaFormalizacion'
        },
        {
            name: 'numeroFirmantes',
            mapping: 'numeroFirmantes'
        },
        {
            name: 'icDocumento',
            mapping: 'icDocumento'
        },        
        {
            name: 'fechaFormalizacion',
            mapping: 'fechaFormalizacion'
        },
        {name: 'icArchivoBO', 	    mapping : 'icArchivoBO' 	        }
        ]
    });

    var procesarConsultaSolicitudes = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'No existe información con los criterios determinados');
            }
        }
        gridSolicitudes.el.unmask();
    }

    
    var storeSolicitudes = Ext.create('Ext.data.Store', {
        model: 'ConsultaSolicitudesDataModel',
        proxy: {
            type: 'ajax',
            url: '41consultasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'consultaSolicitudes'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsultaSolicitudes
        }
    });


    function getFileBO(icDocumento, icArchivoBO){
        Ext.getCmp("panelSolicitudes").mask("Descargando base de operación...");
        Ext.Ajax.request({
            url: '41consultasIfExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getDocFile',
                icArchivo: icArchivoBO
            }),
            callback: descargaArchivo
        });     
    }


/* * * * * * * * * * * * * * * * * * GRID DE RESULTADOS SOLICITUDES * * * * * * * * * * * * * * * * * * * * * * * */
    var gridSolicitudes = Ext.create('Ext.grid.Panel', {
        id: 'd_gridId',
        store: storeSolicitudes,
        stripeRows: true,
        title: 'Documentos',
        width: 944,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
                header: "Fecha de<br/>Recepción",
                dataIndex: 'fechaSolicitud',
                width: 90,
                sortable: true,
                hideable: true
            }, {
                header: "Folio de<br/>Solicitud",
                dataIndex: 'folioSolicitud',
                width: 80,
                sortable: true,
                hideable: true,
                tdCls: 'cursorPointer'
            }, {
                header: "Ejecutivo",
                dataIndex: 'usuarioEjecutivo',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Portafolio" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }     
            }, 
            {
                header: "Tipo de Movimiento",
                dataIndex: 'tipoMovimiento',
                align: 'center',
                width: 170,
                sortable: true,
                hideable: true 
            },     
            {
                header: "Tipo de Documento",
                dataIndex: 'tipoDocumento',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Tipo Documento" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }     
            },
            {
                header: "Versión",
                dataIndex: 'version',
                align: 'center',
                width: 80,
                sortable: true,
                hideable: true
            },            
            {
                header: "Portafolio",
                dataIndex: 'portafolio',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true
            },
            {
                header: "Nombre de Producto<br/>/Empresa",
                dataIndex: 'nombreProductoEmpresa',
                align: 'center',
                width: 180,
                sortable: true,
                hideable: true,
                renderer: function(value) {
                    return '<span data-qtitle="Producto/Empresa" data-qwidth="220" ' +
                        'data-qtip="' + value + '">' +
                        value + '</span>';
                }                     
            },
            {
                header: "Documento<br/>Generado",
                menuText: "Documentos<br/>Generado",
                xtype: 'actioncolumn',
                align: 'center',
                dataIndex: 'icSolicitud',
                hideable: true,
                sortable: true,
                width: 80,
                items: [{
                    iconCls: 'icoFolderTable',
                    tooltip: 'Mostrar Documentos',
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex);
                        mostrarVentanaSolicitudes(record);
                    }
                }]
            },
            
            {
                header: "Fecha Maxima de<br/>Formalización",
                dataIndex: 'fechaMaximaFormalizacion',
                width: 120,
                sortable: true,
                hideable: true
            },
            
            {
                header: "No de Firmantes<br/> IF",
                dataIndex: 'numeroFirmantes',
                align: 'center',
                width: 100,
                sortable: true,
                hideable: true
            }, 
              {
                header: "Firmantes IF",
                dataIndex: 'firmantes',
                align: 'center',
                width: 180,
                sortable: true,
                hideable: true,
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                var fullUsuarios = record.get("firmantes");
                var retorno = "";
                for(var i =0; i< fullUsuarios.length; i++){
                       retorno += fullUsuarios[i].descripcion+"<\/br>";
                }    
                 return '<span data-qtitle="Firmantes" data-qwidth="220" ' +
                        'data-qtip="' + retorno + '">' +
                        retorno + '</span>';
              } 
                
            }, 
            
             {
                header: "Fecha de<br/>Formalización",
                dataIndex: 'fechaFormalizacion',
                width: 90,
                sortable: true,
                hideable: true
            },
            
            {
                header: "Estatus",
                dataIndex: 'estatus',
                align: 'left',
                width: 150,
                sortable: true,
                hideable: true
            },
            
             {
                xtype:'actioncolumn',
                header: "Base de Operacion",
                dataIndex: 'icArchivoBO',
                align: 'center',
                width: 150,
                sortable: true,
                hideable: true,
                items: [{
                    iconCls: 'bricks',
                    tooltip: 'Ver Base Operación',
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex); 
                        getFileBO(record.get("icDocumento"), record.get("icArchivoBO"));
                    },
                    isDisabled: function (view, rowIndex, colIndex, item, record) {
                        return record.get("icArchivoBO") == 0;
                    }
                }]                  
            }
        ]
    });

    //----------************* CATALOGOS  *************----------//

    var sstoreFolioSolicitud = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catFolios'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError,
                'load': function(store, records, options) {
                    var record = new store.recordType({
                        id: 0,
                        title: '-- ALL --'
                    });
                    store.insert(0, record);
                    }
                
            }
        }
    });



    var sstoreTipoMovimiento = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catTipoMovimiento'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    
    var storeTipoDocumento = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catDocumento'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
    var storeTipoPortafolio = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catPortafolio'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });    
    
    var storeProductoEmpresa = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catProductoEmpresa'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });    
    

    var storeEstatus = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41consultasIfExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEstatus'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });    

    var error1Fechas = "Debes capturar la fecha inicial y final";
    var error2Fechas = "La fecha final debe ser posterior a la inicial";

    var validarFechas = function(val) {
        var fechaUno = Ext.getCmp('s_fechaUno');
        var fechaDos = Ext.getCmp('s_fechaDos');
        var fechaUnoVal = fechaUno.getValue();
        var fechaDosVal = fechaDos.getValue();
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1Fechas);
            fechaDos.markInvalid(error1Fechas);
            return error1Fechas;
        }
        if (fechaUnoVal != null && fechaUnoVal > fechaDosVal) {
            fechaUno.markInvalid(error2Fechas);
            fechaDos.markInvalid(error2Fechas);
            fechaDos.focus();
            return error2Fechas;
        }
        return true;
    }
    
    
    var error1FechasMax = "Debes capturar la fecha inicial y final";
    var error2FechasMax = "La fecha final debe ser posterior a la inicial";

    var validarFechasMax = function(val) {
        var fechaUno = Ext.getCmp('s_fechaUnoMax');
        var fechaDos = Ext.getCmp('s_fechaDosMax');
        var fechaUnoVal = fechaUno.getValue();
        var fechaDosVal = fechaDos.getValue();
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1FechasMax);
            fechaDos.markInvalid(error1FechasMax);
            return error1FechasMax;
        }
        if (fechaUnoVal != null && fechaUnoVal > fechaDosVal) {
            fechaUno.markInvalid(error2FechasMax);
            fechaDos.markInvalid(error2FechasMax);
            fechaDos.focus();
            return error2FechasMax;
        }
        return true;
    }
    
    var error1FechasForm = "Debes capturar la fecha inicial y final";
    var error2FechasForm = "La fecha final debe ser posterior a la inicial";

    var validarFechasForm = function(val) {
        var fechaUno = Ext.getCmp('s_fechaUnoFor');
        var fechaDos = Ext.getCmp('s_fechaDosFor');
        var fechaUnoVal = fechaUno.getValue();
        var fechaDosVal = fechaDos.getValue();
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1FechasForm);
            fechaDos.markInvalid(error1FechasForm);
            return error1FechasForm;
        }
        if (fechaUnoVal != null && fechaUnoVal > fechaDosVal) {
            fechaUno.markInvalid(error2FechasForm);
            fechaDos.markInvalid(error2FechasForm);
            fechaDos.focus();
            return error2FechasForm;
        }
        return true;
    }
    
    
    

    /**********************  PANEL BUSQUEDA Solicitudes *************************/
    var formBusquedaSol = new Ext.form.FormPanel({
        name: 'formBusquedaSol',
        id: 'formBusquedaSol',
        frame: true,
        width: 943,
        height: 'auto',
        border: 0,
        layout: 'column',
        items: [{
            xtype: 'fieldset',
            id: 's_fieldsBusqueda',
            height: 300,
            border: false,
            columnWidth: 0.5,
            items: [ 
            {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 's_fechaUno',
                    name: 's_fechaUno',
                    fieldLabel: 'Fecha de Recepción',
                    padding: 5,
                    width: 215,
                    emptyText: 'Desde',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 's_fechaDos',
                    name: 's_fechaDos',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            }, 
            {
                xtype: 'combo',
                name: 's_folioSolicitud',
                id: 's_folioSolicitud',
                fieldLabel: 'Folio de Solicitud',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Folios',
                store: sstoreFolioSolicitud,
                displayField: 'descripcion',
                valueField: 'clave',
                triggerAction: 'all',
                editable: true
            },            
            {
                xtype: 'combo',
                id: 's_tipoMovimiento',
                name: 's_tipoMovimiento',
                fieldLabel: 'Tipo de movimiento',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los movimientos',
                store: sstoreTipoMovimiento,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: true
            },            
            {
                xtype: 'combo',
                id: 's_estatus',
                name: 's_estatus',
                fieldLabel: 'Estatus de la Solicitud:',
                padding: 5,
                width: 400,
                store: storeEstatus,
                emptyText: 'Todos los Estatus',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                editable: false,
                allowBlank: true
            }            
        ]
        }, {
            xtype: 'fieldset',
            height: 300,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [
            {
                xtype: 'combo',
                id: 's_documento',
                name: 's_documento',
                fieldLabel: 'Tipo de Documento',
                padding: 5,
                width: 440,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: false,
                store: storeTipoDocumento,
                emptyText: 'Todos los documentos',
                displayField: 'descripcion',
                editable: false,
                valueField: 'clave'
            }, 
            {
                xtype: 'combo',
                id: 's_portafolio',
                name: 's_portafolio',
                fieldLabel: 'Portafolio:',
                padding: 5,
                width: 440,
                store: storeTipoPortafolio,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Portafolios',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                id: 's_productoEmpresa',
                name: 's_productoEmpresa',
                fieldLabel: 'Nombre Producto/Empresa:',
                padding: 5,
                width: 440,
                store: storeProductoEmpresa,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los productos/empresas',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
             {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 's_fechaUnoMax',
                    name: 's_fechaUnoMax',
                    fieldLabel: 'Fecha Max Formalización: ',
                    padding: 5,
                    width: 215,
                    emptyText: 'De',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 's_fechaDosMax',
                    name: 's_fechaDosMax',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            },
             {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 's_fechaUnoFor',
                    name: 's_fechaUnoFor',
                    fieldLabel: 'Fecha Formalización: ',
                    padding: 5,
                    width: 215,
                    emptyText: 'De',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 's_fechaDosFor',
                    name: 's_fechaDosFor',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            }
            
            ]
        }],
        buttons: [{
            text: 'Buscar',
            id: 's_btnBuscar',
            iconCls: 'icoBuscar',
            handler: function(boton, evento) {
                var resultado = validarFechas();
                var resultado2 = validarFechasMax();
                var resultado3 = validarFechasForm();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else if(resultado2!=true){
                    Ext.MessageBox.alert('Aviso', resultado2);
                } else if(resultado3!=true){
                     Ext.MessageBox.alert('Aviso', resultado3);
                } else {
                    gridSolicitudes.el.mask('Cargando...', 'x-mask-loading');
                    storeSolicitudes.load({
                        params: Ext.apply(formBusquedaSol.getForm().getValues())
                    });
                }
            }
        }, {
            id: 's_btnLimpiar',
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function() {
                Ext.getCmp('formBusquedaSol').getForm().reset();
            }
        }]
    });



    var panelSolicitudes = new Ext.Container({
        id: 'panelSolicitudes',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            NE.util.getEspaciador(20),
            formBusquedaSol,
            NE.util.getEspaciador(10),
            botonContratoPDF,
            NE.util.getEspaciador(30),
            gridSolicitudes           
        ]
    });
    
    
    
    
/*************************************** CONTENEDOR PRINCIPAL  *******************************************************/      
    
    Ext.tip.QuickTipManager.init();

    Ext.create('Ext.panel.Panel', {
        width: 945,
        id: 'panelTbs',
        minHeight: 700,
        title: 'Consulta de Documentos(IF)',
        renderTo: 'areaContenido',
        items: [
            panelSolicitudes       
       ]
    });


});