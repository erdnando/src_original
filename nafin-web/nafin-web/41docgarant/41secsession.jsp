<%-- No poner imports en esta seccion para evitar los warnings de imports repetidos --%>
<%
//El siguiente codigo debe ser lo primero en ejecutarse, el cual determina si la sesion esta activa. 
//De lo contrario en lugar de mostrar "Su sesion ha expirado..." lanzara un NullPointer Exception
String gsCveUsuario = (String) session.getAttribute("sesCveUsuario");
if(gsCveUsuario == null){
%>
	<script language="JavaScript">
		alert("Su sesi\u00F3n ha expirado, por favor vuelva a entrar.");
		top.location = "<%=request.getContextPath()%>/ssologout.jsp"
	</script>
<%
	return;
}
%>
<%@ include file="/41docgarant/41secsession_comun.jsp" %>
