<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String icSolicitud = request.getParameter("icSolicitud") == null ? "" : request.getParameter("icSolicitud");
String infoRegresar="";
log.info("informacion: "+informacion);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);

if (informacion.equals("catIF")){ //<------------------------------------------------------
    Integer tipoIF = null;
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatIF(tipoIF);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catDoc")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatDocumentos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";

}
else if(informacion.equals("catPort")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";

}
else if(informacion.equals("solicitud")){
    ArrayList<String> parametros = new ArrayList<>(); 
    
    String portafolio = request.getParameter("portafolio") == null ? "" : request.getParameter("portafolio");
    String producto = request.getParameter("producto") == null ? "" : request.getParameter("producto");
    String tipoDocumento = request.getParameter("tipoDocumento") == null ? "" : request.getParameter("tipoDocumento");
    String tipoSolicitud = request.getParameter("tipoSolicitud") == null ? "" : request.getParameter("tipoSolicitud");
    String descripcion = request.getParameter("descripcion") == null ? "" : request.getParameter("descripcion");
    String nombre = request.getParameter("nombreProducto") == null ? "" : request.getParameter("nombreProducto");
    String ifSeleccionados = request.getParameter("listaIF");
    String filesAdjuntados = request.getParameter("listaFiles");

    ArrayList<Integer> listFiles = new ArrayList<>();
    if (filesAdjuntados != null){
        JSONArray jsObjArray = JSONArray.fromObject(filesAdjuntados);        
        for(int indice=0; indice < jsObjArray.size(); indice++){
            Integer inte = jsObjArray.getInt(indice);
            listFiles.add(inte);
        }    
    }
    log.info(Arrays.asList(listFiles));
    ArrayList<Integer> listIF = new ArrayList<>();
    if (ifSeleccionados != null){
        JSONArray jsObjArray = JSONArray.fromObject(ifSeleccionados);        
        for(int indice=0; indice < jsObjArray.size(); indice++){
            JSONObject auxiliar = jsObjArray.getJSONObject(indice);
            listIF.add(auxiliar.getInt("clave"));
        }    
    }
    SolicitudDoc sol = new SolicitudDoc();
    sol.setPortafolio(Integer.parseInt(portafolio));
    sol.setIcTipoDocumento(Integer.parseInt(tipoDocumento));
    sol.setTipoSolicitud(Integer.parseInt(tipoSolicitud));
    sol.setUsuarioRegistro(iNoUsuario);
    sol.setNombre(nombre);
    sol.setDescripcion(descripcion);
    sol.setSIntermediarios(listIF);
    sol.setSArchivos(listFiles);
    sol.setNumeroArchivos(listFiles.size());
    log.info(sol);
    JSONObject jsonObj = new JSONObject();
    try{
        Boolean exito = false;
        String folioGenerado = solicitudesBean.altaSolicitud(sol);  
        if (folioGenerado != null){
            exito = true;
        }
        jsonObj.put("success", exito);
        jsonObj.put("guardar", exito);
        jsonObj.put("folio", folioGenerado);
    }
    catch(Exception e){
        log.error(e.getMessage());
        e.printStackTrace();
        jsonObj.put("success", new Boolean(false));
        jsonObj.put("guardar", new Boolean(false));    
        jsonObj.put("msg", e.getMessage());
        jsonObj.put("stackTrace",e.getStackTrace());
        jsonObj.put("causeStackTrace", e.getCause());
    }
    infoRegresar = jsonObj.toString(); 
}
else if (informacion.equals("getDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("getDocOrigen")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicDocto = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icDocto = Integer.parseInt(sicDocto);        
        Documento docto = solicitudesBean.getDocumentoByIcDocumento(icDocto);
        if(docto.getIcArchivo() == null || docto.getIcArchivo() == 0){
            throw new AppException("¡Error, el documento  "+docto.getIcDocumento()+"no tiene asignado ningun icArchivo!");
        }
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(docto.getIcArchivo(), strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("delDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    exito = files2OnBaseBean.eliminaArchivoDocumento(Integer.parseInt(sicArchivo));
    if (exito) { 
        jsonObj.put("msg","Se ha eliminado el archivo correctamente.");
        jsonObj.put("icArchivo", sicArchivo);
    }
    else{
        jsonObj.put("msg","Error al eliminar el archivo");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}else if (informacion.equals("deleteDocFiles")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String filesAdjuntados = request.getParameter("icArchivos");
    ArrayList<Integer> listFiles = new ArrayList<>();
    if (filesAdjuntados != null){
        JSONArray jsObjArray = JSONArray.fromObject(filesAdjuntados);        
        for(int indice=0; indice < jsObjArray.size(); indice++){
            Integer inte = jsObjArray.getInt(indice);
            listFiles.add(inte);
        }    
    }
    exito = files2OnBaseBean.eliminaArchivosSolicitud(listFiles);
    if (exito){
        jsonObj.put("msg","Se han eliminado los archivos correctamente.");
    }
    else{
        jsonObj.put("msg","Error al eliminar el archivo");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
   else if (informacion.equals("solData")){ //<------------------------------------------------------

        Integer icSol = null;
        JSONObject jsonObj   =  new JSONObject();
        try{
            icSol = Integer.parseInt(icSolicitud);
            SolicitudDoc sol = solicitudesBean.getSolicitud(icSol); 
            jsonObj.put("success", new Boolean(true));
            jsonObj.put("sol",sol);
            infoRegresar = jsonObj.toString();
        }
        catch(NumberFormatException ne){
            jsonObj.put("msg","Error al procesa el parametro de la solicitud");
            jsonObj.put("success", new Boolean(false));
            infoRegresar = jsonObj.toString();
            log.info("No es un parametro vaido: ["+ icSolicitud+"]");
        }
    }
else if (informacion.equals("removIcArchivoIF")){
    boolean exito = false;
    
    JSONObject jsonObj   =  new JSONObject();
    String picFile  = request.getParameter("icArchivo");
    String picIF  = request.getParameter("icIF");
    if(picFile != null && picFile.length() > 0){
        Integer icArchivo = Integer.parseInt(picFile);
        Integer icIF = Integer.parseInt(picIF);
        try{    
            exito = solicitudesBean.deleteIcArchivoIF(Integer.valueOf(icSolicitud), icIF, icArchivo);
        }
        catch(Exception e){
            exito = false;
            e.printStackTrace();
        }    
    }
    if (exito){
        jsonObj.put("msg","Se han eliminado los archivos correctamente.");
        jsonObj.put("icIF",picIF);
    }
    else{
        jsonObj.put("msg","Error al eliminar el archivo");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}    
    else if(informacion.equals("sadocto")){
        boolean exito = false;
        String nombre =  request.getParameter("nombreProducto");
        String icTipoDoc = request.getParameter("tipoDocumento");
        String idDoc = request.getParameter("icDocumento");
        Integer icDocumento = 0;
        if (idDoc != null){
            icDocumento = Integer.parseInt(idDoc);
        }
        String fileId = request.getParameter("file");
        String ifSeleccionados = request.getParameter("listaIF");
        ArrayList<Integer> icIFs = new ArrayList<>();
        JSONObject jsonObj = new JSONObject();
        try{
            if (ifSeleccionados != null){
                JSONArray jsObjArray = JSONArray.fromObject(ifSeleccionados);        
                for(int indice=0; indice < jsObjArray.size(); indice++){
                    JSONObject auxiliar = jsObjArray.getJSONObject(indice);
                    icIFs.add(auxiliar.getInt("clave"));
                }    
            }        
            Integer icTipoDocumento = Integer.parseInt(icTipoDoc);
            Integer file = Integer.parseInt(fileId);
            Integer ic_Solicitud = Integer.parseInt(icSolicitud);
            Documento docto = new Documento();
            docto.setIcSolicitud(ic_Solicitud);
            docto.setIcTipoDocumento(icTipoDocumento);
            docto.setIcArchivo(file);
            docto.setIcDocumento(icDocumento);
            docto.setIcIFs(icIFs);
            docto.setNombre(nombre);
            exito = solicitudesBean.guardarActualizarDocumento(docto);       
            jsonObj.put("success", exito);
            jsonObj.put("guardar", exito);
            jsonObj.put("msg", "Se guardaron los datos del documento correctamente.");
        }  
        catch(NumberFormatException ne){
            jsonObj.put("success", exito);
            jsonObj.put("guardar", exito);
            jsonObj.put("msg", "Error al guardar la informacion del documento.");
            jsonObj.put("stackTrace",ne.getStackTrace());
            jsonObj.put("causeStackTrace", ne.getCause());            
        }
        infoRegresar = jsonObj.toString(); 
    } 
    else if(informacion.equals("getComments")){
        ArrayList<Comentario>listado = solicitudesBean.getComentariosSol(Integer.parseInt(icSolicitud));
        JSONArray jsObjArray = new JSONArray();
        jsObjArray = JSONArray.fromObject(listado);
        infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
    } 
    else if(informacion.equals("libSol") ){
        boolean exito = false;
        FlujoDocumentos FlujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
        JSONObject jsonObj   =  new JSONObject();
        SolicitudDoc sol  = solicitudesBean.getSolicitud(Integer.parseInt(icSolicitud));
        exito = FlujoDocumentosBean.avanzarDocumento(sol, iNoUsuario);
        if (exito){
            jsonObj.put("msg","Operación realizada correctamente.");
        }
        else{
            jsonObj.put("msg","Error al cambiar el Estatus de la Solicitud");
        }
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
    else if(informacion.equals("getIcArchivoIFData") ){
        JSONObject jsonObj   =  new JSONObject();
        String sIF = request.getParameter("icIF");
        Integer icSol = Integer.valueOf(icSolicitud);
        Integer icIF = Integer.valueOf(sIF);
        Integer icArchivo = solicitudesBean.getIcArchivoIFData(icSol, icIF);
        jsonObj.put("icArchivo", icArchivo);
        jsonObj.put("success", true); 
        infoRegresar = jsonObj.toString();
    }    
    
    
log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>