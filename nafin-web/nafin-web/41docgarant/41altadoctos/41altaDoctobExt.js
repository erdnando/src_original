Ext.onReady(function () {

    var TERMINOS_CONDICIONES = 1001;
    var REGLAMENTO_GARANTIA_SELECTIVA = 1002;
    var REGLAMENTO_GARANTIA_AUTOMATICA = 1003;

    // dataFile almacena el icArchivo del documento a Formalizar
    

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        Ext.getCmp('contenedorPrincipal').unmask();
    }

    var fnArchivoPdf = function (nombArch) {
        var myRegex = /^.+\.([Pp][dD][Ff])$/;
        return (myRegex.test(nombArch));
    };

    Ext.apply(Ext.form.VTypes, {
        archivoPdf: function (v) {
            return fnArchivoPdf(v);
        },
        archivoPdfText: 'Solo se permite cargar archivos en formato PDF, Favor de verificarlo.'
    });

    function eliminaArchivo(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var icArchivo = infoR.icArchivo;
            Ext.getCmp('dataFile').setValue(0);
            // elimina los botones asociados al icArchivo
            var btnV = Ext.getCmp('btnV' + icArchivo);
            btnV.hide();
            btnV.destroy()
            var btnE = Ext.getCmp('btnE' + icArchivo);
            btnE.hide();
            btnE.destroy()
            Ext.getCmp('archivo').setDisabled(false);
            Ext.Msg.alert('Eliminar archivo', 'Se elimino el archivo de forma exitosa');

        }
        Ext.getCmp('contenedorPrincipal').unmask();
    }

    function eliminaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var icArchivo = Ext.getCmp('dataFile').getValue();
            var btnV = Ext.getCmp('btnV' + icArchivo);
            btnV.hide();
            btnV.destroy()
            var btnE = Ext.getCmp('btnE' + icArchivo);
            btnE.hide();
            btnE.destroy()
            Ext.Msg.alert('Eliminar archivo', 'Se elimino el archivo de forma exitosa');
        }
        Ext.getCmp('dataFile').getValue() = 0;
        Ext.getCmp('contenedorPrincipal').unmask();
    }





    var procesaCargaArchivo = function (opts, success, action) {
        crearBotonArchivo(action.result.icArchivo, action.result.nombreArchivo);
    }
    
    function crearBotonArchivoOrigen(icArchivo){
        var btn = crearBotonVer("DocumentoOrigen", icArchivo, 'getDocOrigen');
        var componenteBotonDocOrigen = Ext.getCmp('formDocumentoOrigen');
        componenteBotonDocOrigen.add(btn);
    }
    
    function crearBotonVer(nombreArchivo, icArchivo, action){
        var btnVer = Ext.create('Ext.Button', {
            id: 'btnV' + icArchivo,
            text: nombreArchivo,
            iconCls: 'iconoLupa',
            handler: function () {
                Ext.getCmp('contenedorPrincipal').mask("Cargando archivo...");
                Ext.Ajax.request({
                    url: '41altaDoctobExt.data.jsp',
                    params: Ext.apply({
                        informacion: action, 
                        icArchivo: icArchivo
                    }),
                    callback: descargaArchivos
                });
            }
        });
        return btnVer;
    }

    function crearBotonArchivo(icArchivo, nombreArchivo) {
        Ext.getCmp('archivo').buttonText = "Cambiar archivo";
        var formaBotones = Ext.getCmp('formButtonsFile');
        var btnVer = crearBotonVer(nombreArchivo, icArchivo, 'getDocFile');
        var btnEliminar = Ext.create('Ext.Button', {
            id: 'btnE' + icArchivo,
            text: nombreArchivo,
            iconCls: 'borrar',
            handler: function () {
                Ext.Msg.show({
                    title: '�Eliminar Archivo?',
                    msg: '�Estas seguro que deseas eliminar el archivo:' + nombreArchivo + '?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    icon: Ext.Msg.QUESTION,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                            Ext.Ajax.request({
                                url: '41altaDoctobExt.data.jsp',
                                params: Ext.apply({
                                    informacion: 'delDocFile',
                                    icArchivo: icArchivo
                                }),
                                callback: eliminaArchivo
                            });
                        }
                    }
                });
            }
        });
        formaBotones.add(btnVer);
        formaBotones.add(btnEliminar);
        Ext.getCmp('dataFile').setValue(icArchivo);
        var componente = Ext.getCmp('archivo');
        componente.setDisabled(true);
    }

    var cargaArchivo = function (estado, respuesta) {
        var formaFile = Ext.getCmp('formFile');
        formaFile.getForm().submit({
            clientValidation: false,
            url: '../41solicitudes/41cargaDocExtArchivo.jsp',
            params: {
                informacion: 'loadfile'
            },
            waitMsg: 'Enviando datos...',
            waitTitle: 'Cargando archivo',
            success: function (form, action) {
                procesaCargaArchivo(null, true, action);
            },
            failure: NE.util.mostrarSubmitError
        });
    };

    var procesarGuardarSolicitud = function () {
        if (!Ext.getCmp('formAlta').isValid()) {
            Ext.Msg.alert('Guardar Solicitud', '�Debe Capturar Capturar todos los campos marcados!');
            return;
        }
        // archivo a formalizar
        if (Ext.getCmp('dataFile').getValue() == 0) {
            Ext.Msg.alert('Guardar Solicitud', '�Debe Cargar un archivo para formalizar!');
            return
        }
        var jsonDataEncodeList = '';
        var records = Ext.getCmp('ifSeleccionados').toField.store.getRange();
        var datar = new Array();
        for (var i = 0; i < records.length; i++) {
            datar.push(records[i].data);
        }
        jsonDataEncodeList = Ext.encode(datar);

        Ext.Ajax.request({
            url: '41altaDoctobExt.data.jsp',
            params: {
                informacion: 'sadocto',
                icSolicitud: Ext.getCmp('icSolicitud').getValue(),
                nombreProducto: Ext.getCmp('nombreProducto').getValue(),
                file: Ext.getCmp('dataFile').getValue(),
                tipoDocumento: Ext.getCmp('tipoDocumento').getValue(),
                listaIF: jsonDataEncodeList,
                icDocumento: icDoc
                // FALTA guardar los archivos de los IFs
            },
            callback: respuestaGuardar
        });
    }

    function respuestaGuardar(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            Ext.Msg.show({
                title: 'Guardar',
                msg: 'Se guardaron los datos del docuento correctamente',
                modal: true,
                icon: Ext.Msg.INFO,
                buttons: Ext.Msg.OK
            });
        } else {
            NE.util.mostrarErrorResponse4(response, opts);
        }
    };

    //********************* CATALOGOS  ***********************//
    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        }]
    });

    var storeCatDoc = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41altaDoctobExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catDoc'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }        
    });

    var primeraVez = true;
    var storeIF = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41altaDoctobExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catIF'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
                /* load: function(store, records, success) {
                     console.log("Cargando store...");
                        if (primeraVez && icSol != null) {
                            primeraVez = false;
                            Ext.Ajax.request({
                                url: '41altaDoctobExt.data.jsp',
                                params: {
                                    informacion: 'solData',
                                    icSolicitud: icSol
                                },
                                callback: fillDataSol
                            });
                        }
                 }*/
            },
            afterRequest: function( request, success ){
                    if (primeraVez && icSol != null) {
                        primeraVez = false;
                        Ext.Ajax.request({
                            url: '41altaDoctobExt.data.jsp',
                            params: {
                                informacion: 'solData',
                                icSolicitud: icSol
                            },
                            callback: fillDataSol
                        });
                    }
            }
        }
    });

    /* * * * * * * * * * * * * * * * * * SELECTOR DE IFS  * * * * * * * * * * * * * * * * */

    var procesaCargaArchivoIF = function (opts, success, action) {
        Ext.getCmp('winFileIFupload').close();
        var grid = Ext.getCmp('gridFichaIF');
        storeFichaIF.each(function (record, idx) {
            val = record.get('icIF');
            if (val == action.result.icIF) {
                record.set('icArchivo', action.result.icArchivo);
                record.commit();
            }
        });
        grid.getView().refresh();
    }

    var cargaArchivoIF = function (estado, respuesta) {
        var formaFile = Ext.getCmp('formFileIF');
        formaFile.getForm().submit({
            clientValidation: false,
            url: '../41solicitudes/41cargaDocExtArchivo.jsp',
            params: {
                informacion: 'loadfile' // guardad archivo IF en gdocrel_documento_if
            },
            waitMsg: 'Enviando datos...',
            waitTitle: 'Cargando archivo',
            success: function (form, action) {
                procesaCargaArchivoIF(null, true, action);
            },
            failure: NE.util.mostrarSubmitError
        });
    };

    var fnCargarFichaIF = function (icIFFile) {
        Ext.create('Ext.window.Window', {
            title: 'Cargar Archivo',
            id: 'winFileIFupload',
            closable: true,
            closeAction: 'destroy',
            width: 450,
            minWidth: 250,
            height: 100,
            animCollapse: false,
            border: false,
            modal: true,
            layout: {
                type: 'border',
                padding: 0
            },
            items: [{
                xtype: 'form',
                frame: true,
                id: 'formFileIF',
                items: [{
                    xtype: 'filefield',
                    padding: '20 20 20 20',
                    fieldLabel: 'Carga Archivo',
                    width: 398,
                    vtype: 'archivoPdf',
                    listeners: {
                        change: function (fld, value) {
                            fld.setRawValue(value.substring(value.lastIndexOf("\\") + 1));
                            if (fld.isValid()) {
                                cargaArchivoIF('CARGAR_ARCHIVO', null);
                            }
                        }
                    }
                }, {
                    xtype: 'hiddenfield',
                    id: 'icIFFile',
                    name: 'icIFFile',
                    value: icIFFile
                }, {
                    xtype: 'hiddenfield',
                    id: 'icDocumento',
                    name: 'icDocumento',
                    value: icDoc
                }]
            }]
        }).show();
    };

    Ext.define('ModelIFsArchivos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'icIF',
            type: 'string'
        }, {
            name: 'nombreIF',
            type: 'string'
        }, {
            name: 'icArchivo',
            type: 'string'
        }, ]
    });

    var storeFichaIF = Ext.create('Ext.data.Store', {
        model: 'ModelIFsArchivos',
        autoLoad: true,
        data: []
    });



    function agregarIFalGrid(picIF, pnombreIF, picArchivo) {
        var record = storeFichaIF.findRecord( "icIF", picIF, 0, false, false, true );
        if (record == null){
            storeFichaIF.add({
                'icIF': picIF,
                'nombreIF': pnombreIF,
                'icArchivo': picArchivo
            });
        }
    }

    function eliminarIFalGrid(icIF, icArchivo) {
        pnl.mask("Actualizando...");
        eliminarArchivoIF(icArchivo, icIF);
    }


    function eliminarIFsinArchivo(icIF) {
        var grid = Ext.getCmp('gridFichaIF');
        var indiceR = 0;
        storeFichaIF.each(function (record, idx) {
            if (icIF == record.get('icIF')) {
                indiceR = idx;
            }
        });
        storeFichaIF.removeAt(indiceR);
        grid.getView().refresh();
    }


    function callBackQuitarArchivoIF(opts, success, response) {
        var rinfo = Ext.JSON.decode(response.responseText);
        var grid = Ext.getCmp('gridFichaIF');
        var val = rinfo.icIF;
        var message = rinfo.msg;
        storeFichaIF.each(function (record, idx) {
            if (val == record.get('icIF')) {
                record.set('icArchivo', null);
                record.commit();
            }
        });
        grid.getView().refresh();
        Ext.Msg.alert('Eliminar archivo', message);
        pnl.unmask();
    }

    function eliminarArchivoIF(icArchivo, icIF) {
        pnl.mask("Eliminando archivo ...");
        Ext.Ajax.request({
            url: '41altaDoctobExt.data.jsp',
            params: Ext.apply({
                informacion: 'removIcArchivoIF',
                icIF: icIF,
                icArchivo: icArchivo,
                icSolicitud: icSol
            }),
            callback: callBackQuitarArchivoIF
        });
    }

    var selectorIF = Ext.create("Ext.ux.form.ItemSelector", {
        id: 'ifSeleccionados',
        name: 'ifSeleccionados',
        autoScroll: true,
        padding: '10 15 15 15',
        height: 300,
        width: 800,
        fieldLabel: '',
        imagePath: '../ux/images/',
        store: storeIF,
        displayField: 'descripcion',
        valueField: 'clave',
        allowBlank: true,
        msgTarget: 'side',
        fromTitle: 'Intermediarios Financieros Disponibles',
        toTitle: 'Intermediarios Financieros Seleccionados',
        buttons: ['add', 'remove'],
        buttonsText: {
            add: 'Seleccionar',
            remove: 'Quitar'
        },
        listeners: {
            change: function (field, newValue, oldValue, eOpts) {
                var oldSize = !Ext.isDefined(oldValue) ? 0 : oldValue.length;
                if (newValue.length > oldSize) {
                    Ext.each(newValue, function (valor) {
                        var registro = storeIF.findRecord('clave', valor, 0, false, true, true);
                        if (conIFFiles && (oldSize == 0 || oldValue.indexOf(valor) == -1)) {
                            agregarIFalGrid(registro.data.clave, registro.data.descripcion, null);
                        }
                    });
                } else {
                    Ext.each(oldValue, function (valor) {
                        var registro = storeIF.findRecord('clave', valor, 0, false, true, true);
                        if (conIFFiles && newValue.indexOf(valor) == -1) {
                            if(registro.icArchivo != null){
                                eliminarIFalGrid(registro.data.clave, null);
                            }
                            eliminarIFsinArchivo(registro.data.clave);
                        }
                    });
                }
            },
            afterrender: function (fld, opts) {
                var view = fld.fromField.dropZone.view;
                view.on('beforedrop', function (targetNode, data, overRecord, currentPosition, dropHandlers) {
                    //Ext.Msg.alert('Quitar Intermediarios Financieros', 'Para quitar IF debes usar el boton correspondiente');
                    return false;
                })
            }
        },
        moveRec: function (add, recs) {
            var me = this,
                fromField = me.fromField,
                toField = me.toField,
                fromStore = add ? fromField.store : toField.store,
                toStore = add ? toField.store : fromField.store;
            if (!add) {
                Ext.Msg.show({
                    title: 'Quitar Intermediario Financiero',
                    msg: '�Estas seguro que deseas quitar el Intermediario Financiero?.\n',
                    buttons: Ext.Msg.YESNOCANCEL,
                    icon: Ext.Msg.QUESTION,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            fromStore.suspendEvents();
                            toStore.suspendEvents();
                            fromStore.remove(recs);
                            toStore.add(recs);
                            fromStore.resumeEvents();
                            toStore.resumeEvents();
                            fromField.boundList.refresh();
                            toField.boundList.refresh();
                            me.syncValue();
                        }
                    }
                });
            } else {
                fromStore.suspendEvents();
                toStore.suspendEvents();
                fromStore.remove(recs);
                toStore.add(recs);
                fromStore.resumeEvents();
                toStore.resumeEvents();
                fromField.boundList.refresh();
                toField.boundList.refresh();
                me.syncValue();
            }
        }
    });

    var cleanForm = function () {
        Ext.getCmp('tipoDocumento').setValue('');
        Ext.getCmp('ifSeleccionados').setValue('');
        Ext.getCmp('nombreProducto').setValue('');
    }

    //*********************  CARGAR INFORMACION DE LA SOLICITUD/DOCUMENTO  *********************//
    var fillDataSol = function (opts, success, response) {
        var jsonResponse = Ext.JSON.decode(response.responseText.trim());
        if (success == true && jsonResponse.success == true) {
            var laSolicitud = jsonResponse.sol;
            Ext.getCmp("lfolio").setText('Folio de la Solicitud: <strong>' + laSolicitud.folio + '<\/strong>', false);
            Ext.getCmp("lfechasolicitud").setText('Fecha de solicitud: <strong>' + laSolicitud.fechaSolicitud + '<\/strong>', false);
            Ext.getCmp("lsolicitante").setText('Solicitante: <strong>' + laSolicitud.nombreUsuarioRegistro + '<\/strong>', false);
            Ext.getCmp("descripcion").setValue(laSolicitud.descripcion);
            var splited1 = laSolicitud.fechaSolicitud.split('/');
            var splited2 = laSolicitud.fechaRegistroDoc.split('/');
            var fechaSol = new Date(splited1[1] + "/" + splited1[0] + "/" + splited1[2]);
            var fechaDoc = new Date(splited2[1] + "/" + splited2[0] + "/" + splited2[2]);

            if (fechaDoc > fechaSol) {
                Ext.getCmp('ifSeleccionados').setValue(laSolicitud.icIFs);
                Ext.getCmp('tipoDocumento').setValue(laSolicitud.icTipoDocumento + "");
                Ext.getCmp("nombreProducto").setValue(laSolicitud.nombre.trim());
                if(laSolicitud.icDocumentoOrigen != 0){
                    crearBotonArchivoOrigen(laSolicitud.icDocumentoOrigen);
                }
            } else {
                Ext.getCmp('ifSeleccionados').setValue(laSolicitud.SIntermediarios);
                Ext.getCmp('tipoDocumento').setValue(laSolicitud.icTipoDocumento + "");
                Ext.getCmp("nombreProducto").setValue(laSolicitud.nombre.trim());
            }
            Ext.getCmp('icSolicitud').setValue(laSolicitud.icSolicitud);
            Ext.getCmp('tipoDocumento').setValue(laSolicitud.icTipoDocumento + "");
            Ext.getCmp('dataFile').setValue(laSolicitud.itemNumArchivo);
            Ext.getCmp('icDocumento').setValue(laSolicitud.icDocumento);
            icDoc = laSolicitud.icDocumento;
            if (laSolicitud.icArchivo != 0) {
                crearBotonArchivo(laSolicitud.icArchivo, "DocumentoCargado");
            }
            if (laSolicitud.icTipoDocumento != TERMINOS_CONDICIONES &&
                laSolicitud.icTipoDocumento != REGLAMENTO_GARANTIA_SELECTIVA && laSolicitud.icTipoDocumento != REGLAMENTO_GARANTIA_AUTOMATICA) {
                Ext.getCmp('radioAddFichaIF').setVisible(true);
                conIFFiles = true; // SE MOSTRARA LA PARTE DEL GRID PARA CARGAR aRCHIVOS POR CADA if SELECCIONADO
                for(i=0; i<laSolicitud.SIntermediarios.length ; i++){
                    var icIF = laSolicitud.SIntermediarios[i];
                        Ext.Ajax.request({
                            url: '41altaDoctobExt.data.jsp',
                            params: {
                                informacion: 'getIcArchivoIFData',
                                icSolicitud: icSol,
                                icIF: icIF
                            },
                            callback: function (opts, success, response) {
                                var estor = Ext.getCmp('ifSeleccionados').getStore();
                                var registroIF = estor.findRecord('clave', icIF, 0, false, false, true);
                                var respuestaJson = JSON.parse(response.responseText);
                                //console.log("icArchivoIF: "+respuestaJson.icArchivo);
                                var icArchivoIF = respuestaJson.icArchivo != 0 ? respuestaJson.icArchivo : null
                                agregarIFalGrid(icIF, registroIF.get('descripcion') , icArchivoIF);
                            }
                        });
                }
            }
        } else {
            Ext.Msg.alert("Alerta", jsonResponse.msg);
        }
    }

    var conIFFiles = false;
    var icDoc = null;
    var icSol = null;
    icSol = Ext.get("icSol").dom.value;
    /*
    if (icSol != null) {
        Ext.Ajax.request({
            url: '41altaDoctobExt.data.jsp',
            params: {
                informacion: 'solData',
                icSolicitud: icSol
            },
            callback: fillDataSol
        });
    }
    */

    //********************  TABLA DE TITULOS DE LA SOLICITUD  ***********************//
    var tablaTitulos = Ext.create('Ext.panel.Panel', {
        width: 880,
        height: 63,
        border: 0,
        padding: '10 15 15 15',
        layout: {
            type: 'table',
            columns: 4
        },
        defaults: {
            bodyStyle: 'padding:10px',
            border: 0,
            frame: true
        },
        items: [{
            xtype: 'hiddenfield',
            id: 'icSolicitud',
            name: 'icSolicitud'
        }, {
            xtype: 'hiddenfield',
            id: 'dataFile',
            name: 'dataFile'
        }, {
            width: 210,
            items: [{
                xtype: 'label',
                id: 'lfolio'
            }]
        }, {
            width: 220,
            items: [{
                xtype: 'label',
                id: 'lfechasolicitud'
            }]
        }, {
            width: 420,
            items: [{
                xtype: 'label',
                id: 'lsolicitante'
            }]
        }]
    });

    var filtrarIF = function (tipoIFs, cmp) {
        if (tipoIFs == 0) {
            Ext.getCmp('chkIFB').setValue(true);
        } else {
            Ext.getCmp('contenedorPrincipal').mask();
            storeIF.load({
                informacion: 'catIF',
                params: {
                    tipoIF: tipoIFs
                },
                callback: function (records, operation, success) {
                    Ext.getCmp('contenedorPrincipal').unmask();
                }
            });
        }
    }

    function fillComentarios(opts, success, response) {
        var infoR = Ext.JSON.decode(response.responseText);
        if (success == true && infoR.success == true) {
            var actual = Ext.getCmp('descripcion').getValue();
            var arrRegistros = infoR.registros;
            var full = "";
            for (indice = 0; indice < arrRegistros.length; indice++) {
                var c = arrRegistros[indice];
                full += "Fecha: " + c.fechaRegistro + " [" + c.nombreUsuario + "]\n" + c.commentario + "\n\n";
            }
            full += "\nSolicitud inicial:\n" + actual;
            Ext.getCmp('descripcion').setValue(full);
        }
        Ext.getCmp('formAlta').unmask();
        Ext.getCmp('btnComments').disable();
    }

    function mostrarTodosComentarios() {
        Ext.getCmp('formAlta').mask("Actualizando...");
        Ext.Ajax.request({
            url: '41altaDoctobExt.data.jsp',
            params: Ext.apply({
                informacion: 'getComments',
                icSolicitud: Ext.getCmp('icSolicitud').getValue()
            }),
            callback: fillComentarios
        });
    }

    /*****************  PANEL DE ALTA  ************************/
    var gridFichaIFs = Ext.create('Ext.grid.Panel', {
        id: 'gridFichaIF',
        store: storeFichaIF,
        stripeRows: true,
        title: 'Fichas IF',
        width: 830,
        height: 300,
        padding: "15 15 15 15",
        collapsible: false,
        enableColumnMove: false,
        enableColumnResize: false,
        sortableColumns: false,
        enableColumnHide: false,
        autoScroll: true,
        hidden: true,
        columns: [{
            header: "Intermediario Financiero",
            dataIndex: 'nombreIF',
            width: 528
        }, {
            xtype: 'actioncolumn',
            header: "Carga Ficha IF",
            dataIndex: 'icArchivo',
            align: 'center',
            width: 90,
            items: [{
                iconCls: 'upload-icon',
                tooltip: 'Carga Ficha IF',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    fnCargarFichaIF(record.get("icIF"));
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    var icArchivo = record.get('icArchivo');
                    return !(icArchivo == null || icArchivo.length == 0);
                }
            }]
        }, {
            xtype: 'actioncolumn',
            header: "Previsualizaci�n",
            dataIndex: 'icArchivo',
            align: 'center',
            width: 90,
            items: [{
                iconCls: 'iconoLupa',
                tooltip: 'Ver Documento',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    Ext.getCmp('contenedorPrincipal').mask("Cargando archivo...");
                    Ext.Ajax.request({
                        url: '41altaDoctobExt.data.jsp',
                        params: Ext.apply({
                            informacion: 'getDocFile',
                            icArchivo: record.get('icArchivo')
                        }),
                        callback: descargaArchivos
                    });
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    var icArchivo = record.get('icArchivo');
                    return (icArchivo == null || icArchivo.length == 0);
                }
            }]
        }, {
            xtype: 'actioncolumn',
            align: 'center',
            header: "Eliminar<br/> Archivo",
            dataIndex: 'icArchivo',
            width: 90,
            items: [{
                iconCls: 'borrar',
                tooltip: 'Eliminar archivo',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    Ext.Msg.show({
                        title: 'Quitar Intermediario Financiero',
                        msg: '�Estas seguro que deseas eliminar el archivo del ' + record.get('nombreIF') + '?.\n',
                        buttons: Ext.Msg.YESNOCANCEL,
                        icon: Ext.Msg.QUESTION,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                eliminarArchivoIF(record.get("icArchivo"), record.get("icIF"));
                            }
                        }
                    });
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    var icArchivo = record.get('icArchivo');
                    return (icArchivo == null || icArchivo.length == 0);
                }
            }]
        }]
    });

    /*** * * * * * * * * * PANEL PRINCIPAL * * * * * * * * * * * * * * * * * * ***/
    var formAlta = new Ext.form.FormPanel({
        name: 'formAlta',
        id: 'formAlta',
        frame: true,
        width: 943,
        height: 'auto',
        title: 'Carga de Informaci�n',
        items: [
            tablaTitulos, {
                xtype: 'textareafield',
                id: 'descripcion',
                name: 'descripcion',
                fieldLabel: 'Descripci�n de la Solicitud',
                padding: '10 15 15 15',
                width: 800,
                height: 150,
                forceSelection: true,
                allowBlank: false,
                labelWidth: '45%',
                readOnly: true
            }, {
                xtype: 'button',
                id: 'btnComments',
                text: 'Comentarios:',
                iconCls: 'icoGuardar',
                margin: '0 15 0 15',
                padding: '5 5 5 5',
                handler: function (boton, evento) {
                    mostrarTodosComentarios();
                }
            }, 
            /******** FORM PARA EL BOTON DEL DOCUMENTO ORIGEN  *********/
                {
                xtype: 'form',
                id: 'formDocumentoOrigen',
                name: 'formDocumentoOrigen',
                frame: true,
                border: 0,
                padding: '20 25 25 25'
            },
            {
                xtype: 'combo',
                id: 'tipoDocumento',
                name: 'tipoDocumento',
                fieldLabel: 'Tipo de Documento:',
                padding: '10 15 15 15',
                width: 600,
                emptyText: 'Seleccione Tipo de Documento',
                forceSelection: true,
                allowBlank: false,
                labelWidth: '250px',
                store: storeCatDoc,
                queryMode: 'local',
                editable: false,
                displayField: 'descripcion',
                valueField: 'clave',
                listeners: {
                    change: function (cmp, newValue) {
                        if (newValue == REGLAMENTO_GARANTIA_SELECTIVA) {
                            Ext.getCmp('nombreProducto').setFieldLabel('Nombre de Empresa');
                        } else {
                            Ext.getCmp('nombreProducto').setFieldLabel('Nombre de Producto');
                        }
                    }
                }
            }, {
                xtype: 'textfield',
                id: 'nombreProducto',
                name: 'nombreProducto',
                fieldLabel: 'Nombre de Producto',
                padding: '10 15 15 15',
                width: 800,
                forceSelection: true,
                allowBlank: false,
                labelStyle: 'width:200px',
                labelWidth: '45%'
            }, {
                xtype: 'form',
                id: 'formFile',
                name: 'formFile',
                frame: true,
                border: 0,
                items: [{
                    xtype: 'filefield',
                    id: 'archivo',
                    emptyText: 'Nombre del Archivo',
                    fieldLabel: 'Nombre del Archivo',
                    name: 'archivoSoporte',
                    buttonText: 'Cargar archivo',
                    labelWidth: '45%',
                    width: 800,
                    padding: '15 15 15 15',
                    buttonCfg: {
                        iconCls: 'upload-icon'
                    },
                    vtype: 'archivoPdf',
                    listeners: {
                        change: function (fld, value) {
                            fld.setRawValue(value.substring(value.lastIndexOf("\\") + 1));
                            if (fld.isValid()) {
                                cargaArchivo('CARGAR_ARCHIVO', null);
                            }
                        }
                    }

                }, {
                    xtype: 'hiddenfield',
                    id: 'icDocumento',
                    name: 'icDocumento',
                    value: icDoc
                }]
            }, {
                xtype: 'form',
                id: 'formButtonsFile',
                name: 'formButtonsFile',
                frame: true,
                border: 0,
                padding: '0 0 0 15'
            }, {
                xtype: 'checkboxgroup',
                id: 'chkSeleccionarIF',
                name: 'chkSeleccionarIF',
                fieldLabel: 'Preseleccionar Intermediarios Financieros',
                padding: '10 15 15 15',
                width: 600,
                labelStyle: 'width:300px',
                labelWidth: '100%',
                forceSelection: true,
                columns: 2,
                items: [{
                    boxLabel: 'Intermediarios Bancarios',
                    name: 'chkIFB',
                    id: 'chkIFB',
                    inputValue: 1,
                    checked: true
                }, {
                    boxLabel: 'Intermediarios No Bancarios',
                    name: 'chkIFB2',
                    id: 'chkIFB2',
                    inputValue: 2,
                    checked: true
                }],
                listeners: {
                    change: function (cmp, newValue, oldValue, eOpts) {
                        var tipoIFs = Ext.getCmp('chkIFB').getValue() ? 1 : 0;
                        tipoIFs += Ext.getCmp('chkIFB2').getValue() ? 2 : 0;
                        filtrarIF(tipoIFs, cmp);
                    }
                }
            },
            selectorIF, {
                xtype: 'radiogroup',
                id: 'radioAddFichaIF',
                name: 'radioAddFichaIF',
                fieldLabel: 'Requiere Ficha de Intermediarios Financieros',
                padding: '10 15 15 15',
                width: 600,
                labelStyle: 'width:300px',
                labelWidth: '100%',
                forceSelection: true,
                columns: 2,
                hidden: true,
                items: [{
                    boxLabel: 'S�',
                    name: 'AddFichaIF',
                    id: 'radio1',
                    inputValue: 1
                }, {
                    boxLabel: 'No',
                    name: 'AddFichaIF',
                    id: 'radio2',
                    inputValue: 2,
                    checked: true
                }],
                listeners: {
                    change: function (cmp, newValue, oldValue, eOpts) {
                        gridFichaIFs.setVisible(Ext.getCmp('radio1').getValue());
                    }
                }
            },
            gridFichaIFs,
            NE.util.getEspaciador(10)
        ],
        buttons: [{
            text: 'Guardar',
            id: 'btnGuardar',
            iconCls: 'icoGuardar',
            handler: function (boton, evento) {
                procesarGuardarSolicitud();
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                if (Ext.getCmp('dataFile').getValue() != 0) {
                    Ext.Msg.show({
                        title: '�Eliminar Archivo?',
                        msg: '�Estas seguro que deseas eliminar los archivo cargados?.\n�Esta acci�n no podr� revertirse!',
                        buttons: Ext.Msg.YESNOCANCEL,
                        icon: Ext.Msg.QUESTION,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                cleanForm();
                                Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                                Ext.Ajax.request({
                                    url: '41altaDoctobExt.data.jsp',
                                    params: Ext.apply({
                                        informacion: 'deleteDocFiles',
                                        icArchivos: Ext.getCmp('dataFile').getValue()
                                    }),
                                    callback: eliminaArchivos
                                });
                            }
                        }
                    });
                } else {
                    cleanForm()
                }
            }
        }, {
            text: 'Liberar',
            iconCls: 'icoValidar',
            handler: function () {
                Ext.Msg.show({
                    title: '�Liberar Archivo?',
                    msg: '�Estas seguro que deseas liberar el documento?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    icon: Ext.Msg.QUESTION,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            Ext.getCmp('contenedorPrincipal').mask("Procesando...");
                            Ext.Ajax.request({
                                url: '41altaDoctobExt.data.jsp',
                                params: Ext.apply({
                                    informacion: 'libSol',
                                    icSolicitud: icSol
                                }),
                                callback: function (opts, success, response) {
                                    var rinfo = Ext.JSON.decode(response.responseText);
                                    if (success == true && rinfo.success == true) {
                                        Ext.Msg.show({
                                            title: 'Libeaci�n de Documento',
                                            msg: rinfo.msg,
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.INFO,
                                            fn: function (boton) {
                                                if (boton === 'ok') {
                                                    window.location = '/nafin/41docgarant/41solicitudes/41homeSolicitudesExt.jsp?idMenu=41HOMESOLICITUDES';
                                                }
                                            }
                                        });
                                    } else {
                                        Ext.Msg.alert('�Error!', rinfo.msg);
                                    }
                                    Ext.getCmp('contenedorPrincipal').unmask();
                                    
                                }
                            });
                        }
                    }
                });
            }
        }, {
            text: 'Cancelar',
            id: 'btnHome',
            iconCls: 'icoCancelar',
            handler: function () {
                Ext.Msg.show({
                    title: 'Cancelar',
                    msg: '�Estas seguro que deseas cancelar los cambios no guardados?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    icon: Ext.Msg.QUESTION,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            window.location.reload();
                        }
                    }
                });
            }
        }/*, {
            text: 'Imprimir y Descargar',
            iconCls: 'icoImprimir',
            handler: function () {
                window.print();
            }
        }*/
        ]
    });

    //**********************  CONTENEDOR PRINCIPAL  *********************//

    var pnl = new Ext.Container({
        id: 'contenedorPrincipal',
        renderTo: 'areaContenido',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            NE.util.getEspaciador(20),
            formAlta
        ]
    });

});
