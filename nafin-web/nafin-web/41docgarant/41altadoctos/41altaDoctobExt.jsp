<!DOCTYPE html>
<%@ page import="
		java.util.*,
                org.apache.commons.logging.Log,
                com.nafin.docgarantias.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>

<html>
<head>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    private static final int ESTATUS_EN_PROCESO = 110;
%>
<%
    String ics = request.getParameter("ics");
    String icMov = request.getParameter("accn");
    Integer icSol = 0;
    boolean esSoloLectura = false;
    boolean pantallaExtendida = false;
    if(ics != null){
        try{
            icSol = Integer.parseInt(ics);
            SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);
            boolean asignada = solicitudesBean.isSolicitudAsignadaEjecutivo(icSol, iNoUsuario);
            Integer icMovimiento = Integer.parseInt(icMov);
            
            if (!asignada){
                log.error("La solicitud que se intenta acceder ["+icSol+"] No esta asignada al usuario ["+iNoUsuario+"]");
                response.sendRedirect("/nafin/41docgarant/41solicitudes/41monitorSolExt.jsp?idMenu=41MONITORSOL");
            }
            Documento doc = solicitudesBean.getDocumentoByIcSolicitud(icSol);
            if (doc.getEstatus() > ESTATUS_EN_PROCESO){ 
                esSoloLectura = true;
            }
            if (doc.getIcMovimientoSeleccionado() == null){ 
                solicitudesBean.setTipoMovimientoDocumento(icMovimiento, doc.getIcDocumento());
                pantallaExtendida = true;
            }
                
        }
        catch(NumberFormatException  ne){
            log.error("No se entro un valor valido para el IC_SOLICITUD");
            response.sendRedirect("/nafin/41docgarant/41solicitudes/41monitorSolExt.jsp?idMenu=41MONITORSOL");
        }       
    }
%>
<script type="text/javascript">
    var pantallaExtendida = <%=pantallaExtendida%>;
    var esSoloLectura = <%=esSoloLectura%>;
</script>

<title>Nafinet</title>
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="../41altadoctos/41altaDoctobExt.js?&lt;%=session.getId()%>ession.getId()%>"></script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
        <div id="areaContenido"></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
        <input id='icSol' type="hidden" value='<%=icSol%>'/>
</body>

</html>