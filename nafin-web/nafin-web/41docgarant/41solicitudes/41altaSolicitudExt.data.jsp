<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar="";
log.info("informacion: "+informacion);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);

if (informacion.equals("catIF")){ //<------------------------------------------------------
    Integer idTipoIF = null;
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatIF(idTipoIF);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catDoc")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatDocumentos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";

}
else if(informacion.equals("catPort")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";

}
else if(informacion.equals("solicitud") || informacion.equals("upSolicitud") ){
    ArrayList<String> parametros = new ArrayList<>(); 
    String icSolicitud = request.getParameter("icSolicitud");
    String portafolio = request.getParameter("portafolio") == null ? "" : request.getParameter("portafolio");
    String producto = request.getParameter("producto") == null ? "" : request.getParameter("producto");
    String tipoDocumento = request.getParameter("tipoDocumento") == null ? "" : request.getParameter("tipoDocumento");
    String tipoSolicitud = request.getParameter("tipoSolicitud") == null ? "" : request.getParameter("tipoSolicitud");
    String descripcion = request.getParameter("descripcion") == null ? "" : request.getParameter("descripcion");
    String nuevoComentario = request.getParameter("descripcionComentario");
    String nombre = request.getParameter("nombreProducto") == null ? "" : request.getParameter("nombreProducto");
    String idDocumento = request.getParameter("icDocumento") == null ? "" : request.getParameter("icDocumento");
    String idDocOrigen = request.getParameter("icDocumentoOrigen") == null ? "" : request.getParameter("icDocumentoOrigen");
    String ifSeleccionados = request.getParameter("listaIF");
    
    String filesAdjuntados = request.getParameter("listaFiles");
    String folioEnviado = request.getParameter("folio");
    JSONObject jsonObj = new JSONObject();
    Integer icDoc = null;
    Integer anio = null;
    Integer folio = null;
    if (idDocOrigen.length() > 0){
        anio = Integer.parseInt(idDocOrigen.substring(0,4));
        folio = Integer.parseInt(idDocOrigen.substring(4,idDocOrigen.length()));    
        icDoc = solicitudesBean.getIcDocumentoFromFolioSolicitud(anio, folio); 
        if (idDocOrigen.trim().length()> 0 && icDoc == null){
            throw new AppException("¡El folio del Documento Origen no es correcto!\n Verifique los datos ingresados");
        }
    }        
    ArrayList<Integer> listFiles = new ArrayList<>();
    if (filesAdjuntados != null){
        JSONArray jsObjArray = JSONArray.fromObject(filesAdjuntados);        
        for(int indice=0; indice < jsObjArray.size(); indice++){
            Integer inte = jsObjArray.getInt(indice);
            listFiles.add(inte);
        }    
    }
    log.info(Arrays.asList(listFiles));
    ArrayList<Integer> listIF = new ArrayList<>();
    if (ifSeleccionados != null){
        JSONArray jsObjArray = JSONArray.fromObject(ifSeleccionados);        
        for(int indice=0; indice < jsObjArray.size(); indice++){
            JSONObject auxiliar = jsObjArray.getJSONObject(indice);
            listIF.add(auxiliar.getInt("clave"));
        }    
    }
    SolicitudDoc sol = new SolicitudDoc();   
    sol.setPortafolio(Integer.parseInt(portafolio));
    sol.setIcTipoDocumento(Integer.parseInt(tipoDocumento));
    sol.setTipoSolicitud(Integer.parseInt(tipoSolicitud));
    sol.setUsuarioRegistro(iNoUsuario);
    sol.setNombre(nombre);
    sol.setDescripcion(descripcion);
    sol.setSIntermediarios(listIF);
    sol.setSArchivos(listFiles);
    sol.setNumeroArchivos(listFiles.size());
    sol.setIcDocumentoOrigen(icDoc);
    log.info(sol);
    Integer icDocumento = null;
    try{
        Boolean exito = false;
        String folioGenerado = null;
        if(icSolicitud != null && informacion.equals("upSolicitud")){
             Integer icSol = Integer.parseInt(icSolicitud);
             sol.setIcSolicitud(icSol);
             sol.setFolio(folioEnviado);
             icDocumento = Integer.parseInt(idDocumento);
             sol.setIcDocumento(icDocumento);
             boolean esRechazo = false;
             if (nuevoComentario != null){
                esRechazo = true;
             }
             folioGenerado = solicitudesBean.updateSolicitud(sol, esRechazo, nuevoComentario, iNoUsuario);  
             
        }else{
            folioGenerado = solicitudesBean.altaSolicitud(sol);  
        }
        if (folioGenerado != null){
            exito = true;
        }
        jsonObj.put("success", exito);
        jsonObj.put("guardar", exito);
        jsonObj.put("folio", folioGenerado);
    }
    catch(Exception e){
        log.error(e.getMessage());
        e.printStackTrace();
        jsonObj.put("success", new Boolean(false));
        jsonObj.put("guardar", new Boolean(false));    
        jsonObj.put("msg", e.getMessage());
        jsonObj.put("stackTrace",e.getStackTrace());
        jsonObj.put("causeStackTrace", e.getCause());
    }
    infoRegresar = jsonObj.toString(); 
}
else if (informacion.equals("getDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);        
        ArchivoDocumento archivo = files2OnBaseBean.getArchivo(icArchivo, strDirectorioPublicacion);
        jsonObj.put("urlArchivo", strDirecVirtualTemp +archivo.getNombreArchivo());		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa el parametro icArchivo");
        jsonObj.put("msg","Error al procesa el parametro icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if (informacion.equals("delDocFile")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    ArchivoDocumento archivo = files2OnBaseBean.eliminaArchivoSolicitud(Integer.parseInt(sicArchivo));
    if (archivo != null){
        exito = true;
        jsonObj.put("msg","Se ha eliminado el archivo: "+archivo.getNombreArchivo()+ " correctamente.");
        jsonObj.put("icArchivo", archivo.getIcArchivo());
    }
    else{
        jsonObj.put("msg","Error al eliminar el archivo");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}else if (informacion.equals("deleteDocFiles")){
    boolean exito = false;
    Files2OnBase files2OnBaseBean = ServiceLocator.getInstance().lookup("Files2OnBase",Files2OnBase.class);
    JSONObject jsonObj   =  new JSONObject();
    String filesAdjuntados = request.getParameter("icArchivos");
    ArrayList<Integer> listFiles = new ArrayList<>();
    if (filesAdjuntados != null){
        JSONArray jsObjArray = JSONArray.fromObject(filesAdjuntados);        
        for(int indice=0; indice < jsObjArray.size(); indice++){
            Integer inte = jsObjArray.getInt(indice);
            listFiles.add(inte);
        }    
    }
    exito = files2OnBaseBean.eliminaArchivosSolicitud(listFiles);
    if (exito){
        jsonObj.put("msg","Se han eliminado los archivos correctamente.");
    }
    else{
        jsonObj.put("msg","Error al eliminar el archivo");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
 else if (informacion.equals("solData")){ 
    String icSolicitud = request.getParameter("icSolicitud");
    Integer icSol = null;
    JSONObject jsonObj   =  new JSONObject();
    try{
        icSol = Integer.parseInt(icSolicitud);
        SolicitudDoc sol = solicitudesBean.getSolicitud(icSol); 
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("sol",sol);
        infoRegresar = jsonObj.toString();
    }
    catch(NumberFormatException ne){
        jsonObj.put("msg","Error al procesa el parametro de la solicitud");
        jsonObj.put("success", new Boolean(false));
        infoRegresar = jsonObj.toString();
        log.info("No es un parametro vaido: ["+ icSolicitud+"]");
    }
}
 else if (informacion.equals("nfs2icFol")){ // <----- Obtener el numero IC_Documento del Folio de la Solicitud
    String folioSolicitud = request.getParameter("nfs");
    Integer icDoc = null;
    JSONObject jsonObj   =  new JSONObject();
    try{
        String paramsFolio[] = folioSolicitud.split("-");
        Integer anio = Integer.parseInt(paramsFolio[0]);
        Integer folio = Integer.parseInt(paramsFolio[1]);
        icDoc = solicitudesBean.getIcDocumentoFromFolioSolicitud(anio, folio); 
        jsonObj.put("success", new Boolean(true));
        jsonObj.put("icDoc",icDoc);
        infoRegresar = jsonObj.toString();
    }
    catch(NumberFormatException ne){
        jsonObj.put("msg","Error al procesa el folio de la Soliciutd/Documento");
        jsonObj.put("success", new Boolean(false));
        infoRegresar = jsonObj.toString();
        log.info("No es un parametro vaido: ["+ folioSolicitud+"]");
    }
}
 else if (informacion.equals("getComments")){ 
        String icSolicitud = request.getParameter("icSolicitud");
        ArrayList<Comentario>listado = solicitudesBean.getComentariosSol(Integer.parseInt(icSolicitud));
        JSONArray jsObjArray = new JSONArray();
        jsObjArray = JSONArray.fromObject(listado);
        infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}


log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>