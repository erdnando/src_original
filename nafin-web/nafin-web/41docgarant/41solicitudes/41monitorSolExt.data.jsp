<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String icSolicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
String infoRegresar="";

log.info("informacion: "+informacion);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);

if (informacion.equals("monitorSol")){   //<------------------------------------------------------
    ArrayList<String> parametros = new ArrayList<>(); 
    String folioSolicitud = request.getParameter("folioSolicitud") == null ? "" : request.getParameter("folioSolicitud");
    String fechaSolicitud1 = request.getParameter("fechaSolicitud1") == null ? "" : request.getParameter("fechaSolicitud1");
    String fechaSolicitud2 = request.getParameter("fechaSolicitud2") == null ? "": request.getParameter("fechaSolicitud2");
    String intermediario = request.getParameter("intermediario") == null ? "" : request.getParameter("intermediario");
    String usuarioSolicitante = request.getParameter("usuarioSolicitante") == null ? "" : request.getParameter("usuarioSolicitante");
    String usuarioEjecutivo = request.getParameter("usuarioEjecutivo") == null ? "" : request.getParameter("usuarioEjecutivo");
    String estatus = request.getParameter("estatus") == null ? "" : request.getParameter("estatus");
    
    
    parametros.add(folioSolicitud); // 0
    parametros.add(intermediario);
    if(usuarioSolicitante == ""){
        parametros.add(iNoUsuario);
    }
    else{
    parametros.add(usuarioSolicitante);
    }
    if (usuarioEjecutivo != null){
        parametros.add(usuarioEjecutivo.equals("0")? "" : usuarioEjecutivo);   // 3
    }
    else{
        parametros.add("");   // 3
    }    
    parametros.add(estatus);
    parametros.add(fechaSolicitud1);
    parametros.add(fechaSolicitud2);    
    parametros.add(intermediario);    //7 
    
	ArrayList<SolicitudDoc> listado = solicitudesBean.getSolicitudes(parametros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("fileSol")){

    Integer icSol = Integer.parseInt(icSolicitud);
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getFileSolicitud(icSol);
    Documento doc = solicitudesBean.getDocumentoByIcSolicitud(icSol);
    Integer icDocumento = 0;
    if(doc != null && doc.getIcArchivo() != null){
        icDocumento = doc.getIcArchivo();
    }
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + ", \"icDocumento\": "+icDocumento +"}";
}
else if(informacion.equals("catFolios")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatFolioSolicitudes();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEstatus")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatEstatusSolicitud();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEjecutivo")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getALLCatEjecutivos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEjecutivotoAssignSol")){	
    String intermediario = request.getParameter("intermediario") == null ? "" : request.getParameter("intermediario");
    Integer icIF = Integer.valueOf(intermediario);
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatEjecutivosPorIF(icIF);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("assignSolExe")){
    JSONObject jsonObj   =  new JSONObject();
    if (!strPerfil.equals("ADMIN GESTGAR")){
        log.error("El usuario no tiene el perfil requerido para realizar la acción");
        jsonObj.put("success", true); 
        jsonObj.put("msg", "No se ha podido procesar el procesas de la petición."); 
        infoRegresar = jsonObj.toString();
    }
    else{
        String usuarioEjecutivo = request.getParameter("icEjecutivo");
        if(usuarioEjecutivo != null ){
            Integer icSol = Integer.parseInt(icSolicitud);
            boolean exito = solicitudesBean.asignarSolicitudEjecutvo(icSol, usuarioEjecutivo);
            jsonObj.put("success", true); 
            jsonObj.put("msg", "Se asigno correctamente la solicitud"); 
            infoRegresar = jsonObj.toString();
        }
        else{        
            jsonObj.put("success", true); 
            jsonObj.put("msg", "No se ha podido procesar el parametro de la Solicitud."); 
            infoRegresar = jsonObj.toString();
        }
    }
}
else if(informacion.equals("catSolicitante")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatSolicitantes();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catSolicitudesIF")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatSolicitudesIF();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("libSol") || informacion.equals("checkSol")){
    boolean exito = false;
    FlujoDocumentos flujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
    JSONObject jsonObj   =  new JSONObject();
    SolicitudDoc sol  = solicitudesBean.getSolicitud(Integer.parseInt(icSolicitud));
    exito = flujoDocumentosBean.avanzarDocumento(sol, iNoUsuario);
    if (exito){
        jsonObj.put("msg","Operación realizada correctamente.");
    }
    else{
        jsonObj.put("msg","Error al cambiar el Estatus de la Solicitud");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("soltrash")){
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();
    Integer icSol = Integer.parseInt(icSolicitud);
    exito = solicitudesBean.eliminarSolicitud(icSol);
    if (exito){
        jsonObj.put("msg","Se ha eliminado la solicitud correctamente.");
    }
    else{
        jsonObj.put("msg","Error al eliminar la solicitud");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("rejectSol")){
    boolean exito = false;
    FlujoDocumentos flujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
    
    Integer icSol = Integer.parseInt(icSolicitud);
    String motivoRechazo =  request.getParameter("toRefuse");
    SolicitudDoc sol  = solicitudesBean.getSolicitud(icSol);
    exito = flujoDocumentosBean.rechazarDocumento(sol, iNoUsuario, motivoRechazo);
    JSONObject jsonObj   =  new JSONObject();
    if (exito){
        jsonObj.put("msg","Se ha rechazado la solicitud correctamente.");
    }
    else{
        jsonObj.put("msg","Error al rechazar la solicitud");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}else if (informacion.equals("solData")){
        Integer icSol = null;
        JSONObject jsonObj   =  new JSONObject();
        try{
            icSol = Integer.parseInt(icSolicitud);
            SolicitudDoc sol = solicitudesBean.getSolicitud(icSol); 
            jsonObj.put("success", new Boolean(true));
            jsonObj.put("sol",sol);
            infoRegresar = jsonObj.toString();
        }
        catch(NumberFormatException ne){
            jsonObj.put("msg","Error al procesa el parametro de la solicitud");
            jsonObj.put("success", new Boolean(false));
            infoRegresar = jsonObj.toString();
            log.info("No es un parametro vaido: ["+ icSolicitud+"]");
        }
    }

log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>