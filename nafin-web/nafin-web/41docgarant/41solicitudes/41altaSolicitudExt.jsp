<!DOCTYPE html>
<%@ page import="
		java.util.*,
                net.sf.json.JSONArray,
                org.apache.commons.logging.Log,
                com.nafin.docgarantias.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs4/ux/form/field/MaskField.js"></script>
<script type="text/javascript" src="41altaSolicitudExt.js?&lt;%=session.getId()%>"></script>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    private final String PERFIL_OPERADOR = "OPER GESTGAR";
%>

<%
    String ics = request.getParameter("icSol");
    String nfs = request.getParameter("nfs");
    Integer icSol = null;    
    SolicitudDoc solicitud = null;
    String sIntermediarios = null;
    String folioDocOrigen = "";
    String sFiles =  null;
    boolean esSoloLectura = false;
    if(ics != null){
        try{
            icSol = Integer.parseInt(ics);
            SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);
            solicitud = solicitudesBean.getSolicitud(icSol);
            JSONArray jsIFsArray = JSONArray.fromObject(solicitud.getSIntermediarios());              
            sIntermediarios = jsIFsArray.toString();
             ArrayList<ElementoCatalogo> files = solicitudesBean.getFileSolicitud(icSol);
            JSONArray jsFilesArray = JSONArray.fromObject(files);
            sFiles  = jsFilesArray.toString();
            esSoloLectura = solicitud.isSoloLectura();
            if (solicitud.getIcDocumentoOrigen() != null){
                folioDocOrigen = solicitudesBean.getFolioDocSolicitudOrigen(solicitud.getIcDocumentoOrigen());
            }
        }
        catch(NumberFormatException  ne){
            log.info("No se entro un valor valido para el IC_SOLICITUD");
            System.out.println("Perfil de Usuario: "+strPerfil);
            if (strPerfil.equals(PERFIL_OPERADOR)){
                response.sendRedirect("/nafin/41docgarant/41solicitudes/41homeSolicitudesExt.jsp?idMenu=41HOMESOL");
            }else{
                response.sendRedirect("/nafin/41docgarant/41solicitudes/41monitorSolExt.jsp?idMenu=41MONITORSOL");
            }
        }       
    }
    else{
        solicitud = new SolicitudDoc();
    }
%>
<script type="text/javascript">
    var eicSol = <%=icSol%>;
    var enfs = '<%=solicitud.getFolio()%>';
    var eidTipoSol = <%=solicitud.getTipoSolicitud()%>;
    var eidPortafolio = <%=solicitud.getPortafolio()%>;
    var etipoDoc = <%=solicitud.getIcTipoDocumento()%>;
    var eNombre  = '<%=solicitud.getNombre()%>';
    var eDescripcion = '<%=solicitud.getDescripcion()%>';
    var eIntermediarios = <%=sIntermediarios%>;
    var eFiles = <%=sFiles%>;
    var esSoloLectura = <%=esSoloLectura%>;
    var folioDocOrigen = '<%=folioDocOrigen%>';
    var icDocumento = <%=solicitud.getIcDocumento()%>;
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
        <%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
        <div id="areaContenido"></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>

</html>