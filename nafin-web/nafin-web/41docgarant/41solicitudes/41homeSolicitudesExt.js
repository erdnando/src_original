Ext.onReady(function () {

    var ESTATUSBO_POR_VALIDAR = 1;

    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        }]
    });



    var storeEstatusBO = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41homeSolicitudesExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEstatusBO'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });






var formBusquedaBO = new Ext.form.FormPanel({
        name: 'boformBusqeda',
        id: 'boformBusqueda',
        title: 'Busqueda Bases de Operaci�n',
        width: 943,
        height: 200,
        frame: true,
        border: 0,
        layout: 'column',
        items: [
        {
            xtype: 'fieldset',
            id: 'bofieldsBusqueda1',
            height: 150,
            border: false,
            columnWidth: 0.5,
            items: [{
                xtype: 'combo',
                name: 'bofolio',
                id: 'bofolio',
                fieldLabel: 'Folio de Recepci�n',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Folios',
                //store: storeFolioSolicitud,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }, {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'bofechaSolicitud1',
                    name: 'bofechaSolicitud1',
                    fieldLabel: 'Fecha de Solicitud',
                    padding: 5,
                    width: 215,
                    emptyText: 'Desde',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 'bofechaSolicitud2',
                    name: 'bofechaSolicitud2',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            }, 
            {
                xtype: 'combo',
                id: 'bousuarioSolicitante',
                name: 'usuarioSolicitante',
                fieldLabel: 'Solicitante',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Solicitantes',
                //store: storeSolicitantes,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }]
        }, {
            xtype: 'fieldset',
            height: 150,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [{
                xtype: 'combo',
                id: 'boEstatus',
                name: 'boEstatus',
                fieldLabel: 'Estatus',
                padding: 5,
                width: 440,
                forceSelection: true,
                allowBlank: true,
                store: storeEstatusBO,
                emptyText: 'Todos los estatus',
                displayField: 'descripcion',
                editable: false,
                valueField: 'clave'
            }, {
                xtype: 'combo',
                id: 'boEjecutivo',
                name: 'boEjecutivo',
                fieldLabel: 'Ejecutivo',
                padding: 5,
                width: 440,
                //store: storeSolicitudesIF,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Ejecutivos',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                id: 'bointermediario',
                name: 'bointermediario',
                fieldLabel: 'Intermediariio Financiero:',
                padding: 5,
                width: 440,
                //store: storeSolicitudesIF,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Intermediarios',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }            
            ]
        }],
        buttons: [{
            text: 'Buscar',
            id: 'bobtnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarFechas();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    tpanelBO.el.mask('Cargando...', 'x-mask-loading');
                    gridBOStore.load({
                        params: Ext.apply(formBusqueda.getForm().getValues())
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                formBusquedaBO.getForm().reset();
            }
        }]
    });
    

    Ext.define('DocsBODataModel', {
        extend: 'Ext.data.Model',
        fields: 
        [{name: 'icDocumento', mapping: 'icDocumento'},
        {name: 'icArchivo', mapping: 'icArchivo'},
        {name: 'icSolicitud', mapping: 'icSolicitud'},
        {name: 'nombreUsuarioEjecutivo', mapping: 'nombreUsuarioEjecutivo'},
        {name: 'nombreUsuarioSolicito', mapping: 'nombreUsuarioSolicito'},
        {name: 'estatusBO', mapping: 'estatusBO'},
        {name: 'fechaLiberacion', mapping: 'fechaLiberacion'},
        {name: 'fechaFormalizacion', mapping: 'fechaFormalizacion'},
        {name: 'nombreIF', mapping: 'nombreIF'},
        {name: 'icIF', mapping: 'icIF'},
        {name: 'folio', mapping: 'folio'}]
    });    
    
    
    var procesarConsultaBO = function (store, arrRegistros, opts) {
        //Ext.getCmp('tpanelBO').unmask();
    }    
    
    
    var gridBOStore = Ext.create('Ext.data.Store', {
        model: 'DocsBODataModel',
        proxy: {
            type: 'ajax',
            url: '41homeSolicitudesExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'documentosBO'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsultaBO
        }
    });
    
    
    
    function descargaArchivoFormalizado(record){
        Ext.getCmp('panelTbs').mask("Descargando archivo...");
        Ext.Ajax.request({
            url: '41homeSolicitudesExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'getSignedDoc',
                icArchivo: record.get('icArchivo'),
                icIF:  record.get('icIF'),
                icDoc: record.get('icDocumento')
            }),
        callback: descargaArchivos
        });     
    }
    
    
    function validarBaseOperaciones(record){
        Ext.MessageBox.show({
            title: 'Rechazar Base de Operaciones',
            msg: '�Est� seguro de validar la base de operaciones del documento con folio: <strong>' + record.get('folio') + '<\/strong>?',
            width: 400,
            buttons: Ext.MessageBox.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            //multiline: true,
            fn: function (btn, texto) {
                if (btn == 'yes') {
                    Ext.getCmp('panelTbs').mask("Espere un momento...");
                    Ext.Ajax.request({
                        url: '41homeSolicitudesExt.data.jsp',
                        params: Ext.apply({
                            informacion: 'validarBO',
                            icDoc: record.get('icDocumento'),
                            icIF: record.get('icIF')
                        }),
                        callback: function (opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true) {
                                gridBOStore.reload();
                            }
                            Ext.Msg.alert('Rechazar Base de Operaciones', info.msg);
                            Ext.getCmp('panelTbs').unmask();
                        }
                    });
                }
            }
        });            
    }
    
    function rechazarBaseOperaciones(record){
        Ext.MessageBox.show({
            title: 'Rechazar Base de Operaciones',
            msg: '�Est� seguro de rechazar la base de operaciones del documento con folio: <strong>' + record.get('folio') + '<\/strong>?<br/><br/>Por favor capture el motivo del rechazo:',
            width: 400,
            buttons: Ext.MessageBox.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            multiline: true,
            fn: function (btn, texto) {
                if (btn == 'yes') {
                    var textoLimpio = texto.trim();
                    if (textoLimpio.length < 1) {
                        Ext.MessageBox.show({
                            title: 'Rechazar Base de Operaciones',
                            msg: '�Debe capturar la razon del rechazo!\nIntentelo nuevamente',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    } else {
                        Ext.getCmp('panelTbs').mask("Espere un momento...");
                        Ext.Ajax.request({
                            url: '41homeSolicitudesExt.data.jsp',
                            params: Ext.apply({
                                informacion: 'rejectBO',
                                toRefuse: texto,
                                icDoc: record.get('icDocumento'),
                                icIF: record.get('icIF')
                            }),
                            callback: function (opts, success, response) {
                                var info = Ext.JSON.decode(response.responseText);
                                if (success == true && info.success == true) {
                                    gridBOStore.reload();
                                }
                                Ext.Msg.alert('Rechazar Base de Operaciones', info.msg);
                                Ext.getCmp('panelTbs').unmask();
                            }
                        })
                    }
                }
            }
        });        
    }
    
    
    var tpanelBO = Ext.create('Ext.grid.Panel', {
        id: 'gridBO',
        store: gridBOStore,
        stripeRows: true,
        title: 'Base de Operaciones',
        width: 944,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
            header: "Fecha de<br/>Recepci&oacute;n",
            dataIndex: 'fechaLiberacion',
            width: 90,
            sortable: true,
            hideable: true            
        }, {
            header: "Folio de<br/>Solicitud",
            dataIndex: 'folio',
            width: 80,
            sortable: true,
            hideable: true
        }, {
            header: "Solicitante",
            dataIndex: 'nombreUsuarioSolicito',
            width: 200,
            sortable: true,
            hideable: true
        }, {
            header: "Ejecutivo",
            dataIndex: 'nombreUsuarioEjecutivo',
            align: 'left',
            width: 200,
            sortable: true,
            hideable: true        
        }, {
            header: "Intermediario<br/>Financiero",
            dataIndex: 'nombreIF',
            align: 'left',
            width: 200,
            sortable: true,
            hideable: true   
        }, {
            header: "Documento<br/>Formalizado",
            menuText: "Documento Formalizado",
            xtype: 'actioncolumn',
            align: 'center',
            dataIndex: 'icArchivo',
            hideable: true,
            sortable: true,
            width: 80,
            items: [{
                iconCls: 'icoFolderTable',
                tooltip: 'Mostrar Documentos',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    descargaArchivoFormalizado(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('numeroArchivos') == 0;
                }
            }]
        }, 
        {
            header: "Fecha de<br/>Formalizacion",
            menuText: "Fecha de Formalizacion",
            dataIndex: 'fechaFormalizacion',
            width: 90,
            align: 'center',
            sortable: true,
            hideable: true
        },
        {
            xtype: 'actioncolumn',
            header: "Base de Operaci�n",
            menuText: "Base de Operaci�n",
            align: 'center',
            dataIndex: 'icDocumento',
            hideable: true,
            sortable: true,
            width: 110,
            items: [{
                iconCls: 'icoLupa',
                tooltip: 'Ver Base de Operaci�n',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    mostrarVentanaDocumentoFormalizar(record.get('icArchivo'));
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('estatusBO') != ESTATUSBO_POR_VALIDAR;
                }
            }
            ]
        },        
        {
            xtype: 'actioncolumn',
            header: "Validaci�n de<br/>Base de Operaci�n",
            menuText: "Validaci�n de Base de Operaci�n",
            align: 'center',
            dataIndex: 'icDocumento',
            hideable: true,
            sortable: true,
            width: 110,
            items: [{
                iconCls: 'correcto',
                tooltip: 'Liberar Base de Operaci�n',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    validarBaseOperaciones(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatusBO') != ESTATUSBO_POR_VALIDAR);
                }
            },
            {
                iconCls: 'icoRechazar marginL15p',
                tooltip: 'Rechazar Base de Operaci�n',
                handler: function (grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                    rechazarBaseOperaciones(rec);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatusBO') != ESTATUSBO_POR_VALIDAR);
                }          
            }
            ]
        },
        {
            header: "Estatus",
            dataIndex: 'estatusBO',
            align: 'center',
            width: 180,
            sortable: true,
            hideable: true,
            renderer: function (value, metaData, record) {
                var estatus = record.get("estatusBO");
                var nombre = "";
                switch(estatus){
                    case "-1":
                        nombre = 'No Aplica';                        
                        break;                
                    case "0":
                        nombre = 'Sin Base de Operaci�n';
                        break;
                    case "1":
                        nombre = 'Por Validar Base de Operaci�n';
                        break;
                    case "2":
                        nombre = 'Base de Operaci�n Validada';
                        break;    
                    default:
                        nombre = 'Sin Base de Operaci�n';
                        break;
                }
                return nombre;
            }              
        }]
    });
    


    
/********************************************************************************************************************/


/********************************************************************************************************************/





















    var ESTATUS_POR_SOLICITAR = 100;
    var ESTATUS_EN_VALIDACION = 120;
    var ESTATUS_EN_PROCESO = 110;
    
    var SIN_MOVIMIENTO = 0;
    var INCORPORACION_DOCUMENTO = 1;
    var NUEVA_VERSION = 2;
    var ADHESION_IF = 3;
    
    var TERMINOS_CONDICIONES = 1001;
    var REGLAMENTO_GARANTIA_SELECTIVA = 1002;
    var REGLAMENTO_GARANTIA_AUTOMATICA = 1003;

    var POR_ASIGNAR = 'Por asignar';

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;
            archivo = archivo.replace('/nafin', '');
            var params = {
                nombreArchivo: archivo
            };
            var forma = Ext.getDom('formAux');
            forma.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
            forma.method = 'post';
            forma.target = '_self';
            forma.submit();
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        Ext.getCmp('panelTbs').unmask();
        if (Ext.getCmp('winDoc')){
            Ext.getCmp('winDoc').unmask();
        }
    }

    var procesarCargaNombresArchivos = function (store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'La Solicitud no tiene archivos cargados');
            } else {

                var winDoc = Ext.create('Ext.window.Window', {
                    title: 'Documentos Soporte de la Solicitud',
                    id: 'winDoc',
                    height: 200,
                    width: 500,
                    layout: 'fit',
                    modal: true,
                    items: {
                        xtype: 'grid',
                        border: false,
                        enableColumnMove: false,
                        enableColumnResize: true,
                        enableColumnHide: false,
                        columns: [{
                            header: 'Nombre de archivo',
                            dataIndex: 'descripcion',
                            width: 288,
                            sortable: false
                        }, {
                            xtype: 'actioncolumn',
                            header: "Documento",
                            align: 'center',
                            sortable: false,
                            width: 200,
                            items: [{
                                iconCls: 'icoLupa',
                                tooltip: 'Ver Documento',
                                handler: function (grid, rowIndex, colIndex) {
                                    winDoc.mask("Descargando archivo...")
                                    var record = grid.getStore().getAt(rowIndex);
                                    Ext.Ajax.request({
                                        url: '41altaSolicitudExt.data.jsp',
                                        params: Ext.apply({
                                            informacion: 'getDocFile',
                                            icArchivo: record.get('clave')
                                        }),
                                        callback: descargaArchivos
                                    });
                                }
                            }]
                        }],
                        store: storeArchivos
                    }
                }).show();
            }
        }
    }

    var storeArchivos = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        proxy: {
            type: 'ajax',
            url: '41homeSolicitudesExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'fileSol'
            }
        },
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarCargaNombresArchivos
        }
    });

    /** Carga los archivo Soporte de la solicitud */
    function mostrarVentanaDocumentos(icSolicitud) {
        storeArchivos.removeAll(true);
        storeArchivos.load({
            informacion: 'fileSol',
            params: Ext.apply({
                icSolicitud: icSolicitud
            })
        });
    }

    /** desCarga el archivo del documento a Formalizar */
    function mostrarVentanaDocumentoFormalizar(icDocumento) {
        Ext.getCmp('panelTbs').mask("Descargando archivo...");
        Ext.Ajax.request({
            url: '41altaSolicitudExt.data.jsp',
            params: Ext.apply({
                informacion: 'getDocFile',
                icArchivo: icDocumento
            }),
            callback: descargaArchivos
        });
    }
    


    function fnLiberarValidar(record) {
        Ext.Msg.show({
            title: "Liberar Solicitud",
            msg: '�Esta seguro de Liberar la Solicitud con Folio: <strong>' + record.get('folio') + '</strong> y enviarla a Validaci�n?',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function (btn) {
                if (btn === 'yes') {
                    Ext.getCmp('panelTbs').mask("Espere un momento...");
                    Ext.Ajax.request({
                        url: '41homeSolicitudesExt.data.jsp',
                        params: Ext.apply({
                            informacion: 'libSol',
                            icSolicitud: record.get('icSolicitud')
                        }),
                        callback: function (opts, success, response) {

                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert('Enviar Solicitud', info.msg);
                            Ext.getCmp('panelTbs').unmask();
                        }
                    });

                }
            }
        });
    }

    function fnRechazarSolicitud(record) {
        Ext.MessageBox.show({
            title: 'Rechazar Solicitud',
            msg: '�Est� seguro de rechazar la solicitud con folio: <strong>' + record.get('folio') + '</strong>?\nCapture el motivo del rechazo:',
            width: 400,
            buttons: Ext.MessageBox.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            multiline: true,
            fn: function (btn, texto) {
                if (btn == 'yes') {
                    var textoLimpio = texto.trim();
                    if (textoLimpio.length < 1) {
                        Ext.MessageBox.show({
                            title: 'Rechazar Solicitud',
                            msg: '�Debe capturar la razon del rechazo!\nIntentelo nuevamente',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    } else {
                        Ext.getCmp('panelTbs').mask("Espere un momento...");
                        Ext.Ajax.request({
                            url: '41homeSolicitudesExt.data.jsp',
                            params: Ext.apply({
                                informacion: 'rejectSol',
                                toRefuse: texto,
                                icSolicitud: record.get('icSolicitud')
                            }),
                            callback: function (opts, success, response) {
                                var info = Ext.JSON.decode(response.responseText);
                                if (success == true && info.success == true) {
                                    gridStore.reload();
                                }
                                Ext.Msg.alert('Rechazar Solicitud', info.msg);
                                Ext.getCmp('panelTbs').unmask();
                            }
                        })
                    }
                }
            }
        });
    }
    
    
    var verSolicitud = function (icSolicitud, folio) {
        window.location = '41altaSolicitudExt.jsp?idMenu=41ALTASOLICITUD&icSol=' + icSolicitud+"&nfs="+folio;
    }

    //----------*************  GRID  *************----------//
    Ext.define('MonitorSolicitudesDataModel', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'icSolicitud',        mapping: 'icSolicitud'        }, {
            name: 'fechaSolicitud',     mapping: 'fechaSolicitud'        }, {
            name: 'nombre',             mapping: 'nombre'        }, {
            name: 'numeroArchivos',     mapping: 'numeroArchivos'        }, {
            name: 'folio',              mapping: 'folio'        }, {
            name: 'nombreIntermediario',mapping: 'nombreIntermediario'        }, {
            name: 'usuarioRegistro',    mapping: 'usuarioRegistro'        }, {
            name: 'usuarioEjecutivo',   mapping: 'usuarioEjecutivo'        }, {
            name: 'documentosSoporte',  mapping: 'documentosSoporte'        }, {
            name: 'estatus',            mapping: 'estatus'        }, {
            name: 'nombreEstatus',      mapping: 'nombreEstatus'        }, {
            name: 'icTipoDocumento',    mapping: 'icTipoDocumento'        },{
            name: 'icMovimientoSeleccionado',       mapping: 'icMovimientoSeleccionado'        },{            
            name: 'icArchivo',          mapping: 'icArchivo'},
            {name: 'esRechazo', 	            mapping : 'esRechazo' 		}]
    });

    var procesarConsulta = function (store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if (store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso', 'No existe informaci�n con los criterios determinados');
            }
        }
        gridResultados.el.unmask();
    }

    //  grid store
    var gridStore = Ext.create('Ext.data.Store', {
        model: 'MonitorSolicitudesDataModel',
        proxy: {
            type: 'ajax',
            url: '41homeSolicitudesExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'homeSolicitudes',
                usuarioEjecutivo: '0'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsulta
        }
    });

    /******************* R E C U P E R A C I O N    D E    D O C U M E N T O *********************************/
    function backRecuperacion() {
            var info = Ext.JSON.decode(response.responseText);
        if (success == true && info.success == true) {
            gridStore.reload();
        }
        Ext.Msg.alert('Enviar Solicitud', info.msg);
        gridId.reload();
        Ext.getCmp("panelTbs").unmask();
    }

    function ejecutaRecuperacion(record) {
        var titleRecuperacion = 'Recuperaci�n del Documento';
        Ext.MessageBox.show({
            title: titleRecuperacion,
            msg: '�Est� seguro de realizar la recuperaci�n del documento?<br/>' +
                'Se eliminar�n todos los registros y documentos que se hayan formalizado.<br/>' +
                '<br/><strong>�Esta acci�n no podr� ser revertida!<\/strong>',
            buttons: Ext.MessageBox.YESNOCANCEL,
            fn: function (boton, texto) {
                if (boton == 'yes') {
                    Ext.getCmp("panelTbs").mask('Procesando...', 'x-mask-loading');
                    Ext.Ajax.request({
                        url: '41homeSolicitudesExt.data.jsp',
                        params: Ext.apply({
                            informacion: 'retrievalDoc',
                            icSolicitud: record.get('icSolicitud')
                        }),
                        callback: function (opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert(titleRecuperacion, info.msg);
                            Ext.getCmp('panelTbs').unmask();
                        }
                    });
                }
            },
            icon: Ext.MessageBox.WARNING
        });
    }

    /* * * * * * * * * * * * * * * * * * * *  ASIGNAR SOLICITUD * * * * * * * * * * * * * * * * * * * */
    function backToAssing(opts, success, response) {
        var infoR = Ext.JSON.decode(response.responseText);
        if (success == true && infoR.success == true) {
            Ext.MessageBox.show({
                title: 'Aviso',
                msg: infoR.msg,
                buttons: Ext.MessageBox.OK
            });
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        gridStore.reload();
        Ext.getCmp("panelTbs").unmask();
    }



    function cargaInfo(tipoAccion, tipoDocumento, icSolicitud, alerta) {
        if (alerta){
             var txtAccion = tipoAccion == 1 ? "Incorporacion de Documento" : tipoAccion == 2 ? "Nueva Version" : "Adhesion de IF";
            Ext.MessageBox.show({
                title: "Accion sobre la Solicitud",
                msg: '�Est� seguro de seleccionar la acci�n <strong>'+txtAccion+'<\/strong> para este documento?',
                buttons: Ext.MessageBox.YESNOCANCEL,
                fn: function (boton, texto) {
                    if (boton == 'yes') {
                        window.location = '../41altadoctos/41altaDoctobExt.jsp?idMenu=41CARGADOCTOB&ics=' + icSolicitud + "&accn=" + tipoAccion;
                    }
                },
                icon: Ext.MessageBox.WARNING
            });  
        }
        else{
            window.location = '../41altadoctos/41altaDoctobExt.jsp?idMenu=41CARGADOCTOB&ics=' + icSolicitud + "&accn=" + tipoAccion;
        }
    }

    /* * * * * * * * * * * * * * * * * * * * GRID DE RESULTADOS * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    var gridResultados = Ext.create('Ext.grid.Panel', {
        id: 'gridId',
        store: gridStore,
        stripeRows: true,
        title: 'Home de Solicitudes',
        width: 944,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
            header: "Fecha de<br/>Solicitud",
            dataIndex: 'fechaSolicitud',
            width: 90,
            sortable: true,
            hideable: true,
            renderer: function (value, metaData, record) {
                if (record.get("esRechazo")){
                    metaData.tdCls  = 'redRow';
                }
                return value
            }            
        }, {
            header: "Folio de<br/>Solicitud",
            dataIndex: 'folio',
            width: 80,
            sortable: true,
            hideable: true,
            tdCls: 'cursorPointer',   
            renderer: function (value, metaData, record) {
                if (record.get("esRechazo")){
                    metaData.tdCls  = 'redRow';
                }
                return value
            },            
            listeners: {
                click: function (iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
                    var rec = gridStore.getAt(iRowIdx.index);
                    verSolicitud(rec.get("icSolicitud"), rec.get('folio'));
                }
            }
        }, {
            header: "Solicitante",
            dataIndex: 'usuarioRegistro',
            width: 150,
            sortable: true,
            hideable: true,
            renderer: function (value, metaData, record) {
                if (record.get("esRechazo")){
                    metaData.tdCls  = 'redRow';
                }
                return value
            }            
        }, {
            header: "Intermediario<br/>Financiero",
            dataIndex: 'nombreIntermediario',
            align: 'center',
            width: 150,
            sortable: true,
            hideable: true,
            renderer: function (value, metaData, record) {
                if (record.get("esRechazo")){
                    metaData.tdCls  = 'redRow';
                }
                return value
            }            
        }, {
            header: "Ejecutivo",
            dataIndex: 'usuarioEjecutivo',
            align: 'center',
            width: 150,
            sortable: true,
            hideable: true,
            renderer: function (value, meta, record) {
                if (record.get("esRechazo")){
                    meta.tdCls  = 'redRow';
                }            
                if (value == POR_ASIGNAR) {
                    return 'Por asignar <span class ="icoReportUser">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                } else {
                    return value;
                }
            },
            listeners: {
                click: function (iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) {
                    var rec = gridStore.getAt(iRowIdx.index);
                    if (rec.get('usuarioEjecutivo') == 'Por asignar') {
                        Ext.MessageBox.show({
                            title: 'Asignar Solicitud',
                            msg: '�Desea asignarse la solicitud <strong>' + rec.get('folio') + '</strong> para su atenci�n?',
                            buttons: Ext.MessageBox.YESNOCANCEL,
                            fn: function (boton, texto) {
                                if (boton == 'yes') {
                                    Ext.getCmp("panelTbs").mask('Procesando...', 'x-mask-loading');
                                    Ext.Ajax.request({
                                        url: '41homeSolicitudesExt.data.jsp',
                                        params: Ext.apply({
                                            informacion: 'toAssignSol',
                                            icSolicitud: rec.get('icSolicitud')
                                        }),
                                        callback: backToAssing
                                    });
                                }
                            },
                            icon: Ext.MessageBox.INFO
                        });
                    }
                }
            }
        }, {
            header: "Documentos<br/>Soporte",
            menuText: "Documentos<br/>Soporte",
            xtype: 'actioncolumn',
            align: 'center',
            dataIndex: 'numeroArchivos',
            hideable: true,
            sortable: true,
            width: 80,
            items: [{
                iconCls: 'icoFolderTable',
                tooltip: 'Mostrar Documentos',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    mostrarVentanaDocumentos(record.get('icSolicitud'));
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('numeroArchivos') == 0;
                }
            }]
        }, 
        {
            header: "Accion<br/>Ejecutivo",
            menuText: "Accion<br/>Ejecutivo",
            dataIndex: 'icSolicitud',
            xtype: 'actioncolumn',
            width: 100,
            align: 'center',
            sortable: true,
            hideable: true,
            items: [{
                iconCls: 'page_add',
                tooltip: 'Incorporaci�n Documento',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    cargaInfo(1, record.get('icTipoDocumento'), record.get('icSolicitud'), record.get('icMovimientoSeleccionado') == SIN_MOVIMIENTO);
                },
                isDisabled: function(view, rowIndex, colIndex, item, record) {
                    var movSel = record.get('icMovimientoSeleccionado');
                    return record.get('usuarioEjecutivo') == POR_ASIGNAR || (movSel > 0 && movSel != INCORPORACION_DOCUMENTO) || 
                        record.get('estatus') != ESTATUS_EN_PROCESO;
                }
            }, {
                iconCls: 'page_go marginL15p',
                tooltip: 'Nueva Versi�n',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    cargaInfo(2, record.get('icTipoDocumento'), record.get('icSolicitud'), record.get('icMovimientoSeleccionado') == SIN_MOVIMIENTO);
                },
                isDisabled: function(view, rowIndex, colIndex, item, record) {
                    var movSel = record.get('icMovimientoSeleccionado');
                    return record.get('usuarioEjecutivo') == POR_ASIGNAR ||  (movSel > 0 && movSel != NUEVA_VERSION) ||
                        record.get('estatus') != ESTATUS_EN_PROCESO;
                }
            }, {
                iconCls: 'building_add marginL15p',
                tooltip: 'Adhesi�n IF',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    cargaInfo(3, record.get('icTipoDocumento'), record.get('icSolicitud'), record.get('icMovimientoSeleccionado') == SIN_MOVIMIENTO);
                },
                isDisabled: function(view, rowIndex, colIndex, item, record) {
                    var movSel = record.get('icMovimientoSeleccionado');
                    return record.get('usuarioEjecutivo') == POR_ASIGNAR || (movSel > 0 && movSel != ADHESION_IF) ||
                            record.get('estatus') != ESTATUS_EN_PROCESO;
                }                
            }]
        },
        {
            xtype: 'actioncolumn',
            header: "Documentos a<br/>Formalizar",
            menuText: "Documentos a<br/>Formalizar",
            align: 'center',
            dataIndex: 'icArchivo',
            hideable: true,
            sortable: true,
            width: 90,
            items: [{
                iconCls: 'page_paintbrush',
                tooltip: 'Ver Documento',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    mostrarVentanaDocumentoFormalizar(record.get('icArchivo'));
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return record.get('icArchivo') == 0;
                }
            }]
        }, {
            xtype: 'actioncolumn',
            header: "Cambio de <br/>Estatus",
            menuText: "Cambio de <br/>Estatus",
            align: 'center',
            hideable: true,
            sortable: true,
            width: 80,
            items: [{
                iconCls: 'correcto',
                tooltip: 'Liberar Documento',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    fnLiberarValidar(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatus') != ESTATUS_EN_PROCESO);
                }
            }, {
                iconCls: 'icoRechazar marginL15p',
                tooltip: 'Rechazar Solicitud',
                handler: function (grid, rowIndex, colIndex) {
                    var rec = grid.getStore().getAt(rowIndex);
                    fnRechazarSolicitud(rec);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatus') != ESTATUS_EN_PROCESO);
                }
            }]
        }, {
            header: "Estatus",
            dataIndex: 'nombreEstatus',
            align: 'center',
            width: 100,
            sortable: true,
            hideable: true,
            renderer: function (value, metaData, record) {
                if (record.get("esRechazo")){
                    metaData.tdCls  = 'redRow';
                }
                return value
            }         
        }, {
            header: "Recuperar<br/>Documento",
            xtype: 'actioncolumn',
            menuText: "Recuperar Documento",
            align: 'center',
            hideable: true,
            sortable: true,
            width: 80,
            items: [{
                iconCls: 'icoArrowUndoRed',
                tooltip: 'Ejecutar Recuperaci�n',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    ejecutaRecuperacion(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                    return (record.get('estatus') <= ESTATUS_EN_PROCESO);
                }
            }]
        }]
    });

    //----------************* CATALOGOS  *************----------//
    var storeEstatus = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41homeSolicitudesExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEstatus'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var storeEjecutivos = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        data: [{
            clave: '0',
            descripcion: 'Mis Asignaciones'
        }, {
            clave: '1',
            descripcion: 'Mis Asignaciones y Sin Asignar'
        }]
    });

    var storeFolioSolicitud = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41homeSolicitudesExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catFolios'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var storeUsuarioSolicitante = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41homeSolicitudesExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catFolios'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var storeSolicitantes = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41homeSolicitudesExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catSolicitante'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var storeSolicitudesIF = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41homeSolicitudesExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catSolicitudesIF'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

    var error1Fechas = "Debes capturar la fecha inicial y final";
    var error2Fechas = "La fecha final debe ser posterior a la inicial";

    var validarFechas = function (val) {
        var fechaUno = Ext.getCmp('fechaSolicitud1');
        var fechaDos = Ext.getCmp('fechaSolicitud2');
        var fechaUnoVal = fechaUno.getValue();
        var fechaDosVal = fechaDos.getValue();
        if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)) {
            fechaUno.markInvalid(error1Fechas);
            fechaDos.markInvalid(error1Fechas);
            return error1Fechas;
        }
        if (fechaUno != null && fechaUno > fechaDos) {
            fechaUno.markInvalid(error2Fechas);
            fechaDos.markInvalid(error2Fechas);
            fechaDos.focus();
            return error2Fechas;
        }
        return true;
    }

    //----------*************  PANEL BUSQUEDA  *************----------//
    var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        width: 943,
        height: 'auto',
        border: 0,
        frame: true,
        layout: 'column',
        items: [{
            xtype: 'fieldset',
            id: 'fieldsBusqueda1',
            height: 150,
            border: false,
            columnWidth: 0.5,
            items: [{
                xtype: 'combo',
                name: 'folioSolicitud',
                id: 'folioSolicitud',
                fieldLabel: 'Folio de Solicitud',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Folios',
                store: storeFolioSolicitud,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }, {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'fechaSolicitud1',
                    name: 'fechaSolicitud1',
                    fieldLabel: 'Fecha de Solicitud',
                    padding: 5,
                    width: 215,
                    emptyText: 'Desde',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 'fechaSolicitud2',
                    name: 'fechaSolicitud2',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            }, {
                xtype: 'combo',
                id: 'usuarioSolicitante',
                name: 'usuarioSolicitante',
                fieldLabel: 'Solicitante',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Solicitantes',
                store: storeSolicitantes,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }, {
                xtype: 'combo',
                id: 'estatus',
                name: 'estatus',
                fieldLabel: 'Estatus de la Solicitud:',
                padding: 5,
                width: 400,
                store: storeEstatus,
                emptyText: 'Todos los Estatus',
                displayField: 'descripcion',
                valueField: 'clave',
                forceSelection: true,
                editable: false,
                allowBlank: true
            }]
        }, {
            xtype: 'fieldset',
            height: 150,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [{
                xtype: 'combo',
                id: 'usuarioEjecutivo',
                name: 'usuarioEjecutivo',
                fieldLabel: 'Ejecutivos',
                padding: 5,
                width: 440,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: false,
                store: storeEjecutivos,
                emptyText: 'Mis asignaciones',
                displayField: 'descripcion',
                editable: false,
                valueField: 'clave'
            }, {
                xtype: 'combo',
                id: 'intermediario',
                name: 'intermediario',
                fieldLabel: 'Intermediariio Financiero:',
                padding: 5,
                width: 440,
                store: storeSolicitudesIF,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Intermediarios',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }]
        }],
        buttons: [{
            text: 'Buscar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarFechas();
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    gridResultados.el.mask('Cargando...', 'x-mask-loading');
                    gridStore.load({
                        params: Ext.apply(formBusqueda.getForm().getValues())
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                Ext.getCmp('folioSolicitud').setValue('');
                Ext.getCmp('fechaSolicitud1').setValue('');
                Ext.getCmp('fechaSolicitud2').setValue('');
                Ext.getCmp('usuarioSolicitante').setValue('');
                Ext.getCmp('estatus').setValue('');
                Ext.getCmp('usuarioEjecutivo').setValue('');
                Ext.getCmp('intermediario').setValue('');
            }
        }]
    });

    //----------*************  CONTENEDOR PRINCIPAL  *************----------//

    var pnl = new Ext.Container({
        id: 'contenedorPrincipal',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            NE.util.getEspaciador(20),
            formBusqueda,
            NE.util.getEspaciador(30),
            gridResultados,
            NE.util.getEspaciador(20)
        ]
    });

    Ext.tip.QuickTipManager.init();

    var tituloBO = "Base de Operacion";
    

    Ext.create('Ext.tab.Panel', {
        width: 945,
        id: 'panelTbs',
        minHeight: 700,
        renderTo: 'areaContenido',
        items: [{
            id: 'tabSolicitudes',
            title: 'Solicitudes',
            tooltip: 'Atenci�n a Solicitudes',
            items: [pnl]
        }, {
            id: 'tapBO',
            title: 'Bar',
            tabConfig: {
                title: tituloBO,
                tooltip: 'Carga de Bases de Operaci�n'
            },
            items: [NE.util.getEspaciador(15), formBusquedaBO, NE.util.getEspaciador(10), tpanelBO]
        }]
    });

    // VALORES INICIALES DEL COMBO D EJECUTIVO // 
    Ext.getCmp('usuarioEjecutivo').setValue('0');







    
});
