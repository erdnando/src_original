<!DOCTYPE html>
<%@ page import="
		java.util.*,
                org.apache.commons.logging.Log,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<html>
<head>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    private final String PERFIL_OPERADOR = "OPER GESTGAR";
    private final String PERFIL_ADMINISTRADOR = "ADMIN GESTGAR";
%>
<%
    log.info("El Perfil: "+strPerfil+" intenta ingresar a HomeSolicitudes");
    if(!strPerfil.equals(PERFIL_OPERADOR) && !strPerfil.equals(PERFIL_ADMINISTRADOR)){
        response.sendRedirect("/nafin/41docgarant/41solicitudes/41monitorSolExt.jsp?idMenu=41MONITORSOL");
    }
%>
<title>Nafinet</title>
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="41homeSolicitudesExt.js?&lt;%=session.getId()%>"></script>
<style>
.cursorPointer{
    cursor: pointer;
}
.marginL15p{
    margin-left: 15px;
}
.redRow{
    color: red;
}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
            <br/>
            <br/>
        <div id="areaContenido"></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
        <form id='formAux' name="formAux" target='_new'></form>
</body>

</html>