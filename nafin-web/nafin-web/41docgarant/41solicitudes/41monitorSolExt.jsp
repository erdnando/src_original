<!DOCTYPE html>
<%@ page import="
		java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs4.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="41monitorSolExt.js?&lt;%=session.getId()%>"></script>
<style>
.cursorPointer{
    cursor: pointer;
}
.marginL15p{
    margin-right: 10px;
}
.redRow{
    color: red;
}
</style>
<%
 boolean esAdminNafinGestgar = false;
 System.out.println(strPerfil);
 if (strPerfil.equals("ADMIN GESTGAR")){
    esAdminNafinGestgar = true;
 }
%>
<script type="text/javascript">
    var esAdminNafinGestgar = <%=esAdminNafinGestgar%>;
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
        <div id="areaContenido"></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
</body>

</html>