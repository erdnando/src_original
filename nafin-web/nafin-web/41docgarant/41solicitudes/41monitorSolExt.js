
Ext.onReady(function() {

    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();		
            }else {
                NE.util.mostrarConnError(response,opts);
        }
        Ext.getCmp('winDoc').unmask();
    }

    var ESTATUS_POR_SOLICITAR = 100;
    var ESTATUS_EN_PROCESO =    110;
    var ESTATUS_EN_VALIDACION = 120; 
    
    var actualFolio = '';

	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }				
		]
	});
    
    function  procesarCargaNombresArchivos(){
        var winDoc = Ext.create('Ext.window.Window', {
            title: 'Documentos Soporte de la Solicitud: '+actualFolio,
            id: 'winDoc',
            height: 200,
            width: 500,
            layout: 'fit',
            modal: true,
            items: [{   // GRID DE DOCUMENTOS SOPORTE 
                xtype: 'grid',
                border: false,
                enableColumnMove  : false,
                enableColumnResize: true,
                enableColumnHide  : false,
                columns: [
                    {   
                        header: 'Nombre de archivo',
                        dataIndex: 'descripcion',
                        width: 348,
                        sortable: false
                    },
                    {
                        xtype:'actioncolumn',
                        header: "Documento",
                        align: 'center',
                        sortable: false, 
                        width:140,
                        items: [{
                            iconCls: 'icoLupa',
                            tooltip: 'Ver Documento',
                            handler: function(grid, rowIndex, colIndex) {
                                winDoc.mask("Descargando archivo...");
                                var record = grid.getStore().getAt(rowIndex); 
                                Ext.Ajax.request({
                                    url: '41altaSolicitudExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'getDocFile',
                                        icArchivo: record.get('clave')
                                    }),
                                callback: descargaArchivos
                                });                                          
                            }
                        }]
                    }                        
                ],               
                store: storeArchivos
            }]
        }).show();
    }



    var storeComboEjcutivos = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        proxy:              {
            type:            'ajax',
            url:             '41monitorSolExt.data.jsp',
            reader:          {
                type:         'json',
                root:         'registros'
            },
            extraParams:     {
                informacion:  'catEjecutivo'
            }
        },
        autoLoad: true,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }            
    });     


    var storeComboEjcutivosIF = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        proxy:              {
            type:            'ajax',
            url:             '41monitorSolExt.data.jsp',
            reader:          {
                type:         'json',
                root:         'registros'
            },
            extraParams:     {
                informacion:  'catEjecutivotoAssignSol',
                intermediario:  null
            }
        },
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError
        }            
    });     


    var storeArchivos = Ext.create('Ext.data.Store', {
       model: 'ModelCatalogos',
        proxy:              {
            type:            'ajax',
            url:             '41monitorSolExt.data.jsp',
            reader:          {
                type:         'json',
                root:         'registros'
            },
            extraParams:     {
                informacion:  'fileSol'
            }
        },
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarCargaNombresArchivos
        }            
    });     


    function mostrarVentanaDocumentos(icSolicitud){
        storeArchivos.removeAll(true);
        Ext.Ajax.request({
            url: '41monitorSolExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'fileSol',
                icSolicitud: icSolicitud
            }),
            callback:  function(opts, success, response) {
                var info = Ext.JSON.decode(response.responseText);
                if (success == true && info.success == true ) {
                    gridStore.reload();
                    var registros = info.registros;
                    var icDocumento = info.icDocumento;
                    for(i=0; i<registros.length; i++){
                        storeArchivos.add({'clave': registros[i].clave ,'descripcion': registros[i].descripcion});
                    }
                    if(icDocumento != 0){
                        storeArchivos.add({'clave': icDocumento, 'descripcion': 'Documento a Formalizar'});
                    }
                }
                procesarCargaNombresArchivos();
                Ext.getCmp('contenedorPrincipal').unmask();                            
            }
        });
    }
    

    function fnLiberarValidar(record){
        var titleWin    = "Liberar Solicitud";
        var msgquestion = '�Esta seguro de Liberar la Solicitud con Folio: <strong>'+record.get('folio')+'</strong> y enviarla al Ejecutivo?';
        var infoajx     = "libSol";
        
        if(record.get('estatus') == ESTATUS_EN_VALIDACION){
            titleWin    = "Validar Solicitud";
            msgquestion = '�Esta seguro que desea validar la solicitud con folio: <strong>'+record.get('folio')+'</strong>?';
            infoajx     = "checkSol";
        }
        Ext.Msg.show({
            title: titleWin,
            msg: msgquestion,
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                    Ext.Ajax.request({
                        url: '41monitorSolExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: infoajx,
                            icSolicitud: record.get('icSolicitud')
                        }),
                        callback:  function(opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true ) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert('Enviar Solicitud', info.msg);
                            Ext.getCmp('contenedorPrincipal').unmask();                            
                        }
                    });    
                    
                } 
            }
        });  
    }
    
    
    function  fnEliminarRechazarSolicitud(record){
        if(record.get('estatus') == ESTATUS_EN_VALIDACION){
            fnRechazarSolicitud(record);
        }else{
            fnEliminarSolicitud(record);
        }        
    }
    
    function fnRechazarSolicitud(record){
        Ext.MessageBox.show({
           title: 'Rechazar Solicitud',
           msg: '�Est� seguro de rechazar la solicitud con folio: <strong>'+record.get('folio')+'</strong>?\nCapture el motivo del rechazo:',
           width:400,
           buttons: Ext.MessageBox.YESNOCANCEL,
           icon: Ext.Msg.QUESTION,
           multiline: true,
           fn: function(btn, texto){
                if(btn == 'yes'){
                    var textoLimpio = texto.trim();
                    if (textoLimpio.length < 1){
                        Ext.MessageBox.show({
                           title: 'Rechazar Solicitud',
                           msg: '�Debe capturar la razon del rechazo!\nIntentelo nuevamente',
                           buttons: Ext.MessageBox.OK,
                           icon: Ext.MessageBox.ERROR
                       });
                    }
                    else{
                        Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                        Ext.Ajax.request({
                        url: '41monitorSolExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'rejectSol',
                            toRefuse:   texto,
                            icSolicitud: record.get('icSolicitud')
                        }),
                        callback:  function(opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true ) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert('Rechazar Solicitud', info.msg);
                            Ext.getCmp('contenedorPrincipal').unmask();                            
                            }
                        })                    
                    }
                }
           }
       });    
    }
    
    function fnEliminarSolicitud(record){
        Ext.Msg.show({
            title: "Eliminar Solicitud",
            msg: '�Estas seguro de eliminar la solicitud con folio: <strong>'+record.get('folio')+'</strong>? Esta acci�n no podr� revertirse.',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                    Ext.Ajax.request({
                        url: '41monitorSolExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'soltrash',
                            icSolicitud: record.get('icSolicitud')
                        }),
                        callback:  function(opts, success, response) {
                            var info = Ext.JSON.decode(response.responseText);
                            if (success == true && info.success == true ) {
                                gridStore.reload();
                            }
                            Ext.Msg.alert('Eliminar Solicitud', info.msg);
                            Ext.getCmp('contenedorPrincipal').unmask();                            
                        }
                    });    
                } 
            }
        });  
    }
    
    
    /************* VER DETALLES DE SOLICITUD ************************/    
    function fillWinSolDet(opts, success, response) {
        var info = Ext.JSON.decode(response.responseText);
        var solicitud = info.sol;
        if (success == true && info.success == true ) {
            Ext.create('Ext.window.Window', {
                title: 'Detalles de la Solicitudes',
                id: 'winSolDet',
                height: 300, width: 800,
                layout: { type: 'table',  columns: 2 },
                style: 'background-color:white;',
                defaults: { bodyStyle: 'padding:15px', width: 395, border: 0  }, 
                modal: true,
                items: [  
                    {id: 'wfolioSolicitud', html: 'Folio de Solicitud: '+solicitud.folio},
                    {id: 'wfechaSolicitud', html: 'Fecha de Solicitud: '+solicitud.fechaSolicitud},
                    {id: 'wsolicitante',    html: 'Solicitante: '+solicitud.nombreUsuarioRegistro   },
                    {id: 'wejecutivo'    ,  html: 'Ejecutivo: '+solicitud.nombreUsuarioEjecutivo     },                
                    {id: 'westatus',        html: 'Estatus de Solicitud: '+solicitud.nombreEstatus      },
                    {id: 'wIntermediario', html:  'Intermediario: '+solicitud.nombreIntermediario},
                    {id: 'wTipoDocumento', html:  'Tipo de Documento: '+solicitud.nombreTipoDocumento},
                    {id: 'wPortafolio', html:  'Portafolio: '+solicitud.nombrePortafolio},
                    {colspan: 2,width: 798, height: 130, id: 'wdescripcion', html: 'Descripcion: '+solicitud.descripcion  }  
                ]
            }).show();
        }
    }
    
    var verSolicitud = function(icSolicitud){
        Ext.Ajax.request({
            url: '../41altadoctos/41altaDoctobExt.data.jsp',
            params: Ext.apply(  {
                informacion: 'solData',
                icSolicitud: icSolicitud
            }),
        callback: fillWinSolDet
        });       
    }
    
    
//----------*************  GRID  *************----------//
	 Ext.define('HomeSolicitudesDataModel', {
		extend: 'Ext.data.Model',
		fields: [
                   {name: 'icSolicitud', 	    mapping : 'icSolicitud' 	        },
		   {name: 'fechaSolicitud', 	    mapping : 'fechaSolicitud'  	},
		   {name: 'nombre',          	    mapping : 'nombre' 	        },
                   {name: 'numeroArchivos',         mapping : 'numeroArchivos'          },
		   {name: 'folio' ,                 mapping : 'folio' 	                },
		   {name: 'nombreIntermediario',    mapping : 'nombreIntermediario'   },
		   {name: 'usuarioRegistro', 	    mapping : 'usuarioRegistro'	},
                   {name: 'usuarioEjecutivo', 	    mapping : 'usuarioEjecutivo' 	},
		   {name: 'documentosSoporte',	    mapping : 'documentosSoporte'	},
                   {name: 'estatus',	            mapping : 'estatus'        	},
		   {name: 'nombreEstatus', 	    mapping : 'nombreEstatus' 		},
                   {name: 'esRechazo', 	            mapping : 'esRechazo' 		}
                   
		]
	 });
     
     
    var procesarConsulta = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if(store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso','No existe informaci�n con los criterios determinados');
            }
        }
        storeComboEjcutivos.load();
        gridResultados.el.unmask();
    }
        
    function backToAssing(opts, success, response) {
        var infoR = Ext.JSON.decode(response.responseText);
        if (success == true && infoR.success == true) {
            Ext.MessageBox.show({
                title: 'Aviso',
                msg: infoR.msg,
                buttons: Ext.MessageBox.OK
            });
        } else {
            NE.util.mostrarConnError(response, opts);
        }
        gridStore.reload();
        pnl.unmask();
    }
    
        

	var gridStore = Ext.create('Ext.data.Store', {
	   model: 'HomeSolicitudesDataModel',
            proxy:              {
                type:            'ajax',
                url:             '41monitorSolExt.data.jsp',
                reader:          {
                    type:         'json',
                    root:         'registros'
                },
                extraParams:     {
                    informacion:  'monitorSol',
                    fechaSolicitud1: Ext.Date.format(Ext.Date.subtract(new Date(), Ext.Date.MONTH, 1), 'd/m/Y'),
                    fechaSolicitud2: Ext.Date.format(new Date(), 'd/m/Y')                    
                }
            },
            autoLoad: true,
            listeners: {
                exception: NE.util.mostrarDataProxyError,
                load: procesarConsulta
            }            
        });        
        
        
        function deshabilitarAsignarEjecutivo(){
            return !esAdminNafinGestgar;
        }
        
        
    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    });
		
        //  grid
        var gridResultados = Ext.create('Ext.grid.Panel', {
           id                : 'gridId',
           store             : gridStore,
           stripeRows        : true,
           title             : 'Solicitudes',  
           width             : 944,
           height            : 400,
           collapsible       : false,  
           plugins: [cellEditing],
           enableColumnMove  : true,
           enableColumnResize:true,
           autoScroll: true,
		   columns           :
		   [{ 
			  header: "Fecha de<br/>Solicitud",
			  dataIndex: 'fechaSolicitud',	
			  width:90,
			  sortable: true,  
			  hideable: true,
                            renderer: function (value, metaData, record) {
                                if (record.get("esRechazo")){
                                    metaData.tdCls  = 'redRow';
                                }
                                return value
                            }                             
		   },{
			  header: "Folio de<br/>Solicitud", 
			  dataIndex: 'folio',
			  width: 80,
			  sortable: true,
			  hideable: true,
                          tdCls : 'cursorPointer',
                            renderer: function (value, metaData, record) {
                                if (record.get("esRechazo")){
                                    metaData.tdCls  = 'redRow';
                                }
                                return value
                            } ,                          
                          listeners:{             
                                click: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) { 
                                var rec = gridStore.getAt(iRowIdx.index);
                                verSolicitud(rec.get("icSolicitud"));
                            }
                          }
		   }
			,{
			  header: "Solicitante", 
			  dataIndex: 'usuarioRegistro',
			  width: 180,
			  sortable: true,
			  hideable: true, 
                            renderer: function (value, metaData, record) {
                                if (record.get("esRechazo")){
                                    metaData.tdCls  = 'redRow';
                                }
                                return value
                            }                           
		   }               
			,{
			  header: "Intermediario<br/>Financiero", 
			  dataIndex: 'nombreIntermediario',
			  align: 'left',
			  width: 180,
			  sortable: true,
			  hideable: true,
                            renderer: function (value, metaData, record) {
                                if (record.get("esRechazo")){
                                    metaData.tdCls  = 'redRow';
                                }
                                return value
                            }                           
		   }  
			,{
			  header: "Ejecutivo", 
			  dataIndex: 'usuarioEjecutivo',
			  align: 'left',
			  width: 180,
			  sortable: true,
			  hideable: true,
                            renderer: function (value, metaData, record) {
                                if (record.get("esRechazo")){
                                    metaData.tdCls  = 'redRow';
                                }
                                return value
                            } ,                       
                          editor: {
                                xtype: 'combobox',
                                store: storeComboEjcutivos,
                                displayField: 'descripcion',
                                valueField: 'clave',
                                queryMode: 'local',
                                disabled : deshabilitarAsignarEjecutivo(),
                                listeners: {
                                    select: function(combo, record, index)  {
                                        var val = record[0].data;
                                        if (val.clave  != 0) {
                                            Ext.MessageBox.show({
                                                title: 'Asignar Solicitud',
                                                msg: '�Desea asignar la solicitud al usuario: '+val.descripcion+' para su atenci�n?',
                                                buttons: Ext.MessageBox.YESNOCANCEL,
                                                fn: function (boton, texto) {
                                                    if (boton == 'yes') {
                                                        pnl.mask('Procesando...', 'x-mask-loading');
                                                        var row = gridResultados.getSelectionModel().getSelection();
                                                        Ext.Ajax.request({
                                                            url: '41monitorSolExt.data.jsp',
                                                            params: Ext.apply({
                                                                informacion: 'assignSolExe',
                                                                icSolicitud: row[0].data.icSolicitud,
                                                                icEjecutivo: val
                                                            }),
                                                            callback: backToAssing
                                                        });
                                                    }
                                                },
                                                icon: Ext.MessageBox.INFO
                                            });
                                        }
                                    }
                                }                                
                            }                          
		   },
            {
                header: "Documentos<br/>Soporte",
                menuText:"Documentos<br/>Soporte",
                xtype:'actioncolumn',
                align: 'center',
                dataIndex: 'numeroArchivos',
                hideable: true,
                sortable: true, 
                width:80,
                items: [{
                    iconCls: 'icoFolderTable',
                    tooltip: 'Mostrar Documentos',                   
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex); 
                        actualFolio = record.get('folio');
                        mostrarVentanaDocumentos(record.get('icSolicitud'));
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        return record.get('numeroArchivos')==0;
                    }                    
                }]
            }
                ,{
                  header: "Accion<br/>Solicitante", 
                  dataIndex: 'icSolicitud',
                  width: 100,
                  align: 'center',
                  sortable: true,
                  hideable: true,   
                  tdCls : 'cursorPointer',
                  renderer :  function (value, metadata, record, rowIndex, colIndex, store) {
                        if (record.get("esRechazo")){
                            metadata.tdCls  = 'redRow';
                        }
                    if (record.get('estatus') ==  ESTATUS_POR_SOLICITAR){
                        return "Editar"
                    }
                    else if (record.get('estatus') ==  ESTATUS_EN_VALIDACION){
                        return "Validar"
                    }                  
                    else{
                        return "";
                    }
                  },
              listeners:{ 
                    click: function(iView, iCellEl, iColIdx, iStore, iRowEl, iRowIdx, iEvent) { 
                        var record = gridStore.getAt(iRowIdx.index);
                        if (record.get('estatus')==ESTATUS_POR_SOLICITAR){
                            Ext.Msg.show({
                                title:'�Editar Solicitud?',
                                msg: '�Esta seguro de Editar la Solicitud con Folio: <strong>'+record.get('folio')+'<\/strong>?',
                                buttons: Ext.Msg.YESNO,
                                icon: Ext.Msg.QUESTION,
                                fn: function(btn) {
                                    if (btn === 'yes') {
                                        window.location = '41altaSolicitudExt.jsp?idMenu=41ALTASOLICITUD&icSol='+record.get("icSolicitud")+'&nfs='+record.get('folio');
                                    }
                                }
                            });                         
                        }
                        else if (record.get('estatus')==ESTATUS_EN_VALIDACION){
                            Ext.Msg.show({
                                title:'�Validar Solicitud?',
                                msg: '�Esta seguro de Validar la Solicitud con Folio: <strong>'+record.get('folio')+'</strong>?',
                                buttons: Ext.Msg.YESNO,
                                icon: Ext.Msg.QUESTION,
                                fn: function(btn) {
                                    if (btn === 'yes') {
                                        console.log('Solicitud Validada');
                                        fnLiberarValidar(record);
                                    }
                                }
                            });
                        }                                                    
                    }           
              }              
           },
            {
                xtype:'actioncolumn',
                header: "Cambio de <br/>Estatus",
                menuText: "Cambio de <br/>Estatus",
                align: 'center',
                hideable: true,
                sortable: true, 
                width:90,
                items: [{
                    iconCls: 'correcto marginL15p',
                    tooltip: 'Liberar Solicitud',
                    border: '10 10 10 10',
                    handler: function(grid, rowIndex, colIndex) {
                        var record = grid.getStore().getAt(rowIndex); 
                        fnLiberarValidar(record);
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        return (record.get('estatus') != ESTATUS_POR_SOLICITAR && record.get('estatus') != ESTATUS_EN_VALIDACION);
                    }                    
                },
                {
                    iconCls: 'icoEliminar',
                    tooltip: 'Eliminar Solicitud',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        fnEliminarRechazarSolicitud(rec);
                    },
                    isDisabled: function(view, rowIndex, colIndex, item, record) {
                        return (record.get('estatus') != ESTATUS_POR_SOLICITAR && record.get('estatus') != ESTATUS_EN_VALIDACION);
                    }
                }],
                renderer: function (value, metaData, record) {
                    if(record.get('estatus') == ESTATUS_EN_VALIDACION){
                        this.items[0].tooltip = 'Validar Solicitud';
                        this.items[1].tooltip = 'Rechazar Solicitud';
                        this.items[1].iconCls = 'icoRechazar';
                    }else{
                        this.items[0].tooltip = 'Liberar Solicitud';
                        this.items[1].tooltip = 'Eliminar Solicitud';
                        this.items[1].iconCls = 'icoEliminar';                    
                    }
                }
            } 
            ,{
              header: "Estatus", 
              dataIndex: 'nombreEstatus',
              align: 'left',
              width: 160,
              sortable: true,
              hideable: true,
                renderer: function (value, metaData, record) {
                    if (record.get("esRechazo")){
                        metaData.tdCls  = 'redRow';
                    }
                    return value
                }               
       } ]
});


//----------************* CATALOGOS  *************----------//
	var  storeEstatus = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorSolExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catEstatus'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});    
  
    	var  storeEjecutivos = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorSolExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catEjecutivo'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});
    
    
	var  storeFolioSolicitud = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorSolExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catFolios'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});    
    
    /*
	var  storeUsuarioSolicitante = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorSolExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catFolios'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});  */


    var storeUsuarioEjecutivo = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorSolExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catFolios'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});      
    
    
  
    
	var  storeSolicitantes = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
        autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorSolExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catSolicitante'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});      
    
    
    
    	var  storeSolicitudesIF = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
                autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41monitorSolExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catSolicitudesIF'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});   

var error1Fechas = "Debes capturar la fecha inicial y final";
var error2Fechas = "La fecha final debe ser posterior a la inicial";

var validarFechas =  function (val) {
    var fechaUno = Ext.getCmp('fechaSolicitud1');
    var fechaDos = Ext.getCmp('fechaSolicitud2');
    var fechaUnoVal = fechaUno.getValue();
    var fechaDosVal = fechaDos.getValue();
    if ((fechaUnoVal == null && fechaDosVal != null) || (fechaUnoVal != null && fechaDosVal == null)){
        fechaUno.markInvalid(error1Fechas);
        fechaDos.markInvalid(error1Fechas);
        return error1Fechas;
    }
    if(fechaUno != null && fechaUno > fechaDos){
        fechaUno.markInvalid(error2Fechas);
        fechaDos.markInvalid(error2Fechas);
        fechaDos.focus();
        return error2Fechas;
    }
    return true;
}

//----------*************  PANEL BUSQUEDA  *************----------//
var formBusqueda = new Ext.form.FormPanel({
        name: 'formBusqeda',
        id: 'formBusqueda',
        frame: true,
        width: 943,
        height: 'auto',
        title: 'Monitor de Solicitudes',
        layout: 'column',
        items: [
            {
                xtype: 'fieldset',
                id:'fieldsBusqueda1',
                height: 150,
                border:false,
                columnWidth: 0.5,
                items: [
                        {
                            xtype: 'combo',
                            name: 'folioSolicitud',
                            id: 'folioSolicitud',
                            fieldLabel: 'Folio de Solicitud',
                            padding: 5,
                            width: 400,
                            forceSelection: true, 
                            queryMode: 'local', 
                            allowBlank: true,
                            emptyText: 'Todos los Folios',
                            store: storeFolioSolicitud, 
                            displayField: 'descripcion', 
                            valueField: 'clave'
                        },
                        {
                        xtype:'fieldset',
                        collapsible:false,
                        border: false,
                        defaultType: 'textfield',
                        defaults: {anchor: '100%'},
                        padding: 0,
                        layout: 'hbox',
                        items :[
                                {
                                    xtype: 'datefield',
                                    id : 'fechaSolicitud1',
                                    name: 'fechaSolicitud1',
                                    fieldLabel: 'Fecha de Solicitud',
                                    padding: 5,
                                    width: 215,
                                    emptyText: 'Desde',
                                    displayField: 'descripcion',
                                    valueField: 'clave',
                                    forceSelection: true, 
                                    allowBlank: true,
                                    value:  Ext.Date.subtract(new Date(), Ext.Date.MONTH, 1)
                                },
                                {
                                    xtype: 'datefield',
                                    id : 'fechaSolicitud2',
                                    name: 'fechaSolicitud2',
                                    fieldLabel: 'al',
                                    labelWidth: 55,
                                    padding: 10,
                                    width: 170,
                                    emptyText: 'Hasta',
                                    valueField: 'clave',
                                    forceSelection: true, 
                                    allowBlank: true,
                                    value: new Date()
                                }
                            ]
                        },                       
                        {
                            xtype: 'combo',
                            id: 'usuarioSolicitante',
                            name: 'usuarioSolicitante',
                            fieldLabel: 'Solicitante',
                            padding: 5,
                            width: 400,
                            forceSelection: true, 
                            allowBlank: true,
                            emptyText: 'Todos los Solicitantes',
                            store: storeSolicitantes, displayField: 'descripcion', valueField: 'clave'                             
                        },
                        {
                            xtype: 'combo',
                            id: 'estatus',
                            name: 'estatus',
                            fieldLabel: 'Estatus de la Solicitud:',
                            padding: 5,
                            width: 400,
                            store: storeEstatus,
                            emptyText: 'Todos los Estatus',
                            displayField: 'descripcion', 
                            valueField: 'clave',
                            forceSelection: true,
                            allowBlank: true
                        }
                    ]                
            },
            {
                xtype: 'fieldset',
                height: 150,
                border:false,
                columnWidth: 0.5,
                margins: '0 20 0 0',
			items: [
                    {
                        xtype: 'combo',
                        id: 'usuarioEjecutivo',
                        name: 'usuarioEjecutivo',
                        fieldLabel: 'Ejecutivos',
                        padding: 5,
                        width: 440,
                        forceSelection: true, 
                        queryMode: 'local', 
                        allowBlank: true,
                        store: storeEjecutivos,
                        emptyText: 'Todos los Ejecutivos',
                        displayField: 'descripcion', 
                        valueField: 'clave'
                    },
                    {
                        xtype: 'combo',
                        id: 'intermediario',
                        name: 'intermediario',
                        fieldLabel: 'Intermediariio Financiero:',
                        padding: 5,
                        width: 440,
                        store: storeSolicitudesIF,
                        forceSelection: true,
                        queryMode: 'local', 
                        allowBlank: true,
                        emptyText: 'Todos los Intermediarios',
                        displayField: 'descripcion', 
                        valueField: 'clave'
                    }
                ]
            }
        ],
        buttons: [
        {
            text: 'Nueva Solicitud',
            id: 'btnNuevaSol',
            iconCls: 'icoAgregar',
                handler: function() {
                    window.location = '41altaSolicitudExt.jsp?idMenu=41ALTASOLICITUD';
                }
        },        
        {
            text: 'Buscar',
            id: 'btnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                var resultado = validarFechas();
                if(resultado != true){
                    Ext.MessageBox.alert('Aviso',resultado);
                }
                else{
                gridResultados.el.mask('Cargando...', 'x-mask-loading');
                gridStore.load({
                    informacion:  'monitorSol',
                    params: Ext.apply(formBusqueda.getForm().getValues())
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                Ext.getCmp('folioSolicitud').setValue('');
				Ext.getCmp('fechaSolicitud1').setValue('');
				Ext.getCmp('fechaSolicitud2').setValue('');
				Ext.getCmp('usuarioSolicitante').setValue('');
                Ext.getCmp('estatus').setValue('');
                Ext.getCmp('usuarioEjecutivo').setValue('');
                Ext.getCmp('intermediario').setValue('');
            }
        }]      
    });


//----------*************  CONTENEDOR PRINCIPAL  *************----------//

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			formBusqueda,
			NE.util.getEspaciador(30),
                        gridResultados,
			NE.util.getEspaciador(20)
		]        
	});



}); 