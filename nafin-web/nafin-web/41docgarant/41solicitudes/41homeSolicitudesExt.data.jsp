<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.nafin.docgarantias.baseoperaciones.*,
        com.nafin.docgarantias.formalizacion.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
    private static final Integer ESTATUS_VALIDADO_NAFIN = 2;
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";

String infoRegresar="";

log.info("INFORMACION: "+informacion);
SolicitudesDoc solicitudesBean = ServiceLocator.getInstance().lookup("SolicitudesDoc",SolicitudesDoc.class);

if (informacion.equals("homeSolicitudes")){   //<------------------------------------------------------
    ArrayList<String> parametros = new ArrayList<>(); 
    String folioSolicitud = request.getParameter("folioSolicitud") == null ? "" : request.getParameter("folioSolicitud");
    String fechaSolicitud1 = request.getParameter("fechaSolicitud1") == null ? "" : request.getParameter("fechaSolicitud1");
    String fechaSolicitud2 = request.getParameter("fechaSolicitud2") == null ? "": request.getParameter("fechaSolicitud2");
    String intermediario = request.getParameter("intermediario") == null ? "" : request.getParameter("intermediario");
    String usuarioSolicitante = request.getParameter("usuarioSolicitante") == null ? "" : request.getParameter("usuarioSolicitante");
    String tipoUsuarioEjecutivo = request.getParameter("usuarioEjecutivo"); 
    //String estatus = "110";

    parametros.add(folioSolicitud); // 0
    parametros.add(intermediario);
    parametros.add(usuarioSolicitante);
    // mostrar  Solicitudes 0  = "ASIGNADOS AL EJECUTIVO", 1 = TODOS [ASIGNADOS AL EJECUTIVO O SIN ASIGNAR]
    if (tipoUsuarioEjecutivo != null){
        parametros.add(tipoUsuarioEjecutivo.equals("0")? iNoUsuario : "*"+iNoUsuario);   // 3
    }
    else{
        parametros.add(null);   // 3
    }
    parametros.add("");
    parametros.add(fechaSolicitud1);
    parametros.add(fechaSolicitud2);    // 6
    parametros.add("monitorSol"); // 7
    ArrayList<SolicitudDoc> listado = solicitudesBean.getSolicitudes(parametros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("fileSol")){
    String solicitud = request.getParameter("icSolicitud") == null ? "" :request.getParameter("icSolicitud");
    Integer icSolicitud = Integer.parseInt(solicitud);
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getFileSolicitud(icSolicitud);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catFolios")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatFolioSolicitudes();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEstatus")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatEstatusSolicitud();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEstatusBO")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado.add(new ElementoCatalogo(null,"Todos los estatus"));
    listado.add(new ElementoCatalogo("910","Por validar bases"));
    listado.add(new ElementoCatalogo("920","Bases Liberadas"));
    listado.add(new ElementoCatalogo("930","Formalizado"));
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEjecutivotoAssignSol")){	
    String intermediario = request.getParameter("intermediario") == null ? "" : request.getParameter("intermediario");
    Integer icIF = Integer.valueOf(intermediario);
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatEjecutivosPorIF(icIF);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catSolicitante")){
	ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatSolicitantes();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catSolicitudesIF")){
    ArrayList<ElementoCatalogo>listado = solicitudesBean.getCatSolicitudesIF();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("libSol") || informacion.equals("checkSol")){
    boolean exito = false;
    FlujoDocumentos FlujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
    JSONObject jsonObj   =  new JSONObject();
    String icSolicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
    SolicitudDoc sol  = solicitudesBean.getSolicitud(Integer.parseInt(icSolicitud));
    exito = FlujoDocumentosBean.avanzarDocumento(sol, iNoUsuario);
    if (exito){
        jsonObj.put("msg","Operación realizada correctamente.");
    }
    else{
        jsonObj.put("msg","Error al cambiar el Estatus de la Solicitud");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("rejectSol")){
    boolean exito = false;
    FlujoDocumentos flujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
    String solicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
    Integer icSolicitud = Integer.parseInt(solicitud);
    String motivoRechazo =  request.getParameter("toRefuse");
    SolicitudDoc sol  = solicitudesBean.getSolicitud(icSolicitud);
    exito = flujoDocumentosBean.rechazarDocumento(sol, iNoUsuario, motivoRechazo);
    JSONObject jsonObj   =  new JSONObject();
    if (exito){
        jsonObj.put("msg","Se ha rechazado la solicitud correctamente.");
    }
    else{
        jsonObj.put("msg","Error al rechazar la solicitud");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("retrievalDoc")){
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();    
    if (strPerfil.equals("OPER GESTGAR")){
        FlujoDocumentos flujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
        String solicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
        Integer icSolicitud = Integer.parseInt(solicitud);
        
        Documento doc  = solicitudesBean.getDocumentoByIcSolicitud(icSolicitud);
        exito = flujoDocumentosBean.recuperacionDocumento(doc, iNoUsuario);
    }
    if (exito){
        jsonObj.put("msg","Se ha recuperado la solicitud correctamente.");
    }
    else{
        jsonObj.put("msg","!Error¡, No se ha podido recuperar la solicitud");
        log.error("El usuario no tiene el perfil adecuado para realizar la recuperacion de la solicitud.");
    }
    jsonObj.put("success", true); 
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("toAssignSol")){ //<-------- AUTO ASIGNACION ----------
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();    
    if (!strPerfil.equals("OPER GESTGAR")){
        jsonObj.put("msg","No se puede asignar la solicitud");
        log.error("Se esta intentando asignar una solicitud a un usuario que es Ejecutivo, debe tener perfil: OPER GESTGAR, perfil actual: "+ strPerfil);
    }
    else{
        String solicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
        Integer icSolicitud = Integer.parseInt(solicitud);
        exito = solicitudesBean.asignarSolicitudEjecutvo(icSolicitud, iNoUsuario);
        if (exito){
            jsonObj.put("msg","Se ha asignado la solicitud correctamente.");
        }
        else{
            jsonObj.put("msg","Error al asignar la solicitud");
        }    
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("validarSolicitud")){
    boolean exito = false;
    FlujoDocumentos FlujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
    JSONObject jsonObj   =  new JSONObject();
    String icSolicitud = request.getParameter("icSolicitud") == null ? "0" : request.getParameter("icSolicitud");
    SolicitudDoc sol  = solicitudesBean.getSolicitud(Integer.parseInt(icSolicitud));
    exito = FlujoDocumentosBean.avanzarDocumento(sol, iNoUsuario);
    if (exito){
        jsonObj.put("msg","Se ha validado la Solicitud correctamente.");
    }
    else{
        jsonObj.put("msg","Error al validar la Solicitud");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
else if (informacion.equals("documentosBO")){   
    BaseOperacion baseOperacionBean = ServiceLocator.getInstance().lookup("BaseOperacion",BaseOperacion.class);
    ArrayList<String> parametros = new ArrayList<>(); 
    ArrayList<DocBaseOperacion> listado = baseOperacionBean.getDocsBaseOperacionPorValidar();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("getSignedDoc")){   
    Formalizacion formalizacionBean = ServiceLocator.getInstance().lookup("Formalizacion",Formalizacion.class);
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();
    String sicArchivo = request.getParameter("icArchivo") == null ? "" : request.getParameter("icArchivo");
    String sicIF = request.getParameter("icIF") == null ? "" : request.getParameter("icIF");
    String sicDoc = request.getParameter("icDoc") == null ? "" : request.getParameter("icDoc");
    Map<String,String> params = new HashMap<>();
    try{
        Integer icArchivo = Integer.parseInt(sicArchivo);   
        Integer icIF = Integer.parseInt(sicIF);
        Integer icDocumento = Integer.parseInt(sicDoc);
        params.put(ConstantesFormalizacion.PARAM_ICARCHIVO, icArchivo+"");
        params.put(ConstantesFormalizacion.PARAM_IC_IF, icIF+"");
        params.put(ConstantesFormalizacion.PARAM_IC_DOCUMENTO, icDocumento+"");
        String nombreArchivo = formalizacionBean.FirmaDocumento(strDirectorioPublicacion, params);	
        jsonObj.put("urlArchivo", nombreArchivo);		
        exito = true;
    }catch(NumberFormatException  e){
        log.info("Error al procesa los parametros getSignedDoc");
        jsonObj.put("msg","Error al procesa los parametros icArchivo");
        e.printStackTrace();
    }finally{
        jsonObj.put("success", exito); 
        infoRegresar = jsonObj.toString();
    }
}
else if(informacion.equals("rejectBO")){
    BaseOperacion baseOperacionBean = ServiceLocator.getInstance().lookup("BaseOperacion",BaseOperacion.class);
    boolean exito = false;
    FlujoDocumentos flujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
    String documento = request.getParameter("icDoc") == null ? "0" : request.getParameter("icDoc");
    String intermediario = request.getParameter("icIF") == null ? "0" : request.getParameter("icIF");
    Integer icDoc = Integer.parseInt(documento);
    Integer icIF = Integer.parseInt(intermediario);
    String motivoRechazo =  request.getParameter("toRefuse");
    exito = baseOperacionBean.rechazarBO(icDoc, icIF, iNoUsuario, motivoRechazo);
    JSONObject jsonObj   =  new JSONObject();
    if (exito){
        jsonObj.put("msg","Se ha rechazado la Base de Operaciones correctamente.");
    }
    else{
        jsonObj.put("msg","Error al rechazar la Base de Operaciones");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
else if(informacion.equals("validarBO")){
    BaseOperacion baseOperacionBean = ServiceLocator.getInstance().lookup("BaseOperacion",BaseOperacion.class);
    boolean exito = false;
    FlujoDocumentos flujoDocumentosBean = ServiceLocator.getInstance().lookup("FlujoDocumentos",FlujoDocumentos.class);
    String documento = request.getParameter("icDoc") == null ? "0" : request.getParameter("icDoc");
    String intermediario = request.getParameter("icIF") == null ? "0" : request.getParameter("icIF");
    Integer icDoc = Integer.parseInt(documento);
    Integer icIF = Integer.parseInt(intermediario);
    exito = baseOperacionBean.avanzaEstatusBO(icDoc, icIF, iNoUsuario, ESTATUS_VALIDADO_NAFIN);
    JSONObject jsonObj   =  new JSONObject();
    if (exito){
        jsonObj.put("msg","Se ha validado la base de operaciones correctamente.");
    }
    else{
        jsonObj.put("msg","Error al validar la base de operaciones");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}

log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>