
Ext.onReady(function() {

var GARANTIA_SELECTIVA  = 1002;
var GARANTIA_AUTOMATICA = 1003;

var MOVIMIENTO_NUEVA_VERSION = 2;
var MOVIMIENTO_ADHESION_IF   = 3;



    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();		
            }else {
                NE.util.mostrarConnError(response,opts);
        }
        Ext.getCmp('contenedorPrincipal').unmask();
    }

var dataFiles = new Array(); 

var fnArchivoPdfXlsxZip = function(nombArch){
    var myRegex = /^.+\.([Pp][dD][Ff])$/;
    var myRegexZip = /^.+\.([Zz][Ii][Pp])$/;
    var myRegexXlsx = /^.+\.([Xx][Ll][Ss][Xx])$/;
    return (myRegex.test(nombArch) || myRegexZip.test(nombArch) || myRegexXlsx.test(nombArch));
};
    
Ext.apply(Ext.form.VTypes, {
    archivoPdfXlsxZip: function(v) {
        return fnArchivoPdfXlsxZip(v);
    },
    archivoPdfXlsxZipText: 'Solo se permite cargar archivos en formato PDF, XLSX y ZIP Favor de verificarlo.'
});

 function eliminaArchivo(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var icArchivo = infoR.icArchivo;				
            if(dataFiles.length == 1){
                dataFiles.pop();
            }
            else{
                var newarray = new Array();
                // copia todos los elementos menos el icArchivo enviado
                for(i=0; i<dataFiles.length; i++){
                    if(dataFiles[i] != icArchivo){
                        newarray.push(dataFiles[i]);
                    }
                }
                dataFiles = newarray;            
            }
            // elimina los botones asociados al icArchivo
            var btnV = Ext.getCmp('btnV'+icArchivo);
            btnV.hide();
            btnV.destroy()
            var btnE = Ext.getCmp('btnE'+icArchivo);
            btnE.hide();
            btnE.destroy()
            Ext.Msg.alert('Eliminar archivo', 'Se elimino el archivo de forma exitosa');          
        }
        Ext.getCmp('contenedorPrincipal').unmask();
    }
    
    /*
function eliminaArchivosSalir(opts, success, response){
    eliminaArchivos(opts, success, response);
    window.location = '41homeSolicitudesExt.jsp?idMenu=41HOMESOLICITUDES';
}    */
    
 function eliminaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            for(i=0; i<dataFiles.length; i++){
                var icArchivo = dataFiles[i];
                var btnV = Ext.getCmp('btnV'+icArchivo);
                btnV.hide();
                btnV.destroy()
                var btnE = Ext.getCmp('btnE'+icArchivo);
                btnE.hide();
                btnE.destroy()            
            }
            Ext.Msg.alert('Eliminar archivo', 'Se elimino el archivo de forma exitosa');          
        }
        dataFiles = new Array();
        Ext.getCmp('contenedorPrincipal').unmask();
    }


    /* Funcion para agregar botones en el formulario **/
    function crearBotonesArchivo(icArchivo, nombreArchivo){
        var formaBotones= Ext.getCmp('formButtonsFile');
        var btnVer = Ext.create('Ext.Button', {
                id: 'btnV'+icArchivo,
                text: nombreArchivo,
                iconCls : 'iconoLupa',
                tooltip : 'Descargar '+nombreArchivo,
                handler: function() {
                    Ext.getCmp('contenedorPrincipal').mask("Cargando archivo...");
                    Ext.Ajax.request({
                        url: '41altaSolicitudExt.data.jsp',
                        params: Ext.apply(  {
                            informacion: 'getDocFile',
                            icArchivo: icArchivo
                        }),
                        callback: descargaArchivos
                    });                    
                }
            });
        if(!esSoloLectura){
            var btnEliminar = Ext.create('Ext.Button', {
                id: 'btnE'+icArchivo,
                text: nombreArchivo,
                iconCls : 'borrar',
                tooltip : 'Eliminar '+nombreArchivo,
                handler: function() {
                    Ext.Msg.show({
                        title:'�Eliminar Archivo?',
                        msg: '�Estas seguro que deseas eliminar el archivo:'+nombreArchivo+'?',
                        buttons: Ext.Msg.YESNOCANCEL,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                                Ext.Ajax.request({
                                    url: '41altaSolicitudExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'delDocFile',
                                        icArchivo: icArchivo
                                    }),
                                    callback: eliminaArchivo
                                });                                  
                            } 
                        }
                    });                
                }
            });
            formaBotones.add(btnEliminar);   
        }
        dataFiles.push(icArchivo);
        formaBotones.add(btnVer);
         
    }


    var procesaCargaArchivo = function(opts, success, action) {
        crearBotonesArchivo(action.result.icArchivo, action.result.nombreArchivo);
    }


	var cargaArchivo = function(estado, respuesta){
            var formaFile = Ext.getCmp('formFile');
            formaFile.getForm().submit({
                clientValidation: false,
                url: '41cargaDocExtArchivo.jsp',
                params:	{informacion:'loadfile'},
                waitMsg: 'Enviando datos...',
                waitTitle:'Cargando archivo',
                success: function(form, action) {								
                        procesaCargaArchivo(null,  true,  action );
                }
                ,failure: NE.util.mostrarSubmitError
            });
	};

var SI = 1;
var NO = 2;

var procesarGuardarSolicitud = function(panel) {
    var txtfolioOrigen = Ext.getCmp('idDocumentoOrigen');
     var folioDocOrigen = null;
    if(txtfolioOrigen.isVisible()){
        folioDocOrigen  = txtfolioOrigen.getValue().trim();
        if(folioDocOrigen.length==0){
            Ext.Msg.alert('Guardar Solicitud', 'Debe capturar un Numero de folio correcto.\n\n el formato es: aaaa-#; ejemplo 2019-79');
            return false;
        }
    }
    else{
        //txtfolioOrigen.allowBlank = true;
        Ext.apply(txtfolioOrigen, {allowBlank: true}, {});
    }
    
    if (Ext.getCmp('radio1').getValue()){
        Ext.apply(selectorIF, {allowBlank: false}, {});
    }
    else{
        Ext.apply(selectorIF, {allowBlank: true}, {});
    }
    var descripcionComentario = Ext.getCmp('descripcionComentario');
    if (!descripcionComentario.isVisible()){
        Ext.apply(descripcionComentario, {allowBlank: true}, {});
    }
    if (!Ext.getCmp('formAlta').isValid()){
        fieldNames = [];                
        fields = panel.getInvalidFields();
        for(var i=0; i <  fields.length; i++){
            field = fields[i];
            fieldNames.push(field.getName());
         }    
        Ext.Msg.alert('Guardar Solicitud', '�Debe Capturar Capturar todos los campos marcados!');
        return;
    }
    var jsonDataEncodeList = '';
    var records = Ext.getCmp('ifSeleccionados').toField.store.getRange();
    var datar = new Array();
    for (var i = 0; i < records.length; i++) {
        datar.push(records[i].data);
    }
    jsonDataEncodeList = Ext.encode(datar);
    var jsonEncodeListFiles = '';
    jsonEncodeListFiles = Ext.encode(dataFiles);
    var xkl = icSol != null ? "upSolicitud":"solicitud";
    var folioDocOrigenCapturado = null;
    if(Ext.getCmp('idDocumentoOrigen').isVisible()){
        folioDocOrigenCapturado  = txtfolioOrigen.getValue().trim();
    }
    Ext.Ajax.request({
        url: '41altaSolicitudExt.data.jsp',
        params: {
            informacion : 	xkl,
            icSolicitud : 	icSol,
            tipoSolicitud : Ext.getCmp('idMovimiento').getValue(), 
            portafolio 	: 	Ext.getCmp('idPortafoio').getValue(),
            tipoDocumento : Ext.getCmp('tipoDocumento').getValue(),
            nombreProducto	: 	Ext.getCmp('nombreProducto').getValue(),
            descripcion : 	Ext.getCmp('descripcion').getValue(),
            listaIF :		jsonDataEncodeList,
            listaFiles:         jsonEncodeListFiles,
            folio   :           enfs,
            icDocumento :       icDocumento,
            icDocumentoOrigen: folioDocOrigenCapturado,
            nuevoComentario: Ext.getCmp('descripcionComentario').getValue()
        },
        callback: respuestaGuardar
    });
}

function respuestaGuardar(opts, success, response) {
    if (success == true && Ext.JSON.decode(response.responseText).success == true) {
        var folioGenerado = Ext.JSON.decode(response.responseText).folio;
        var hoy = new Date();
      Ext.Msg.show({
          title: 'Guardar',
          msg: '<div style="text-align:center;">La carga de la operaci�n fue satisfactoria <br/> '+hoy.toLocaleString()+'<br/>'+
                'Folio: '+folioGenerado+'<br/></div><br/> '+
                '<small>Favor de tomar nota del folio para futuras aclaraciones y consultas.</small>',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function(btn) {
                window.location = '41homeSolicitudesExt.jsp?idMenu=41HOMESOLICITUDES'
          }
          }); 
          
    } 
    else {
        NE.util.mostrarErrorResponse4(response,opts);
    }
};


//----------************* CATALOGOS  *************----------//

	Ext.define('ModelCatalogos', 	{
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'clave', type: 'string' },
			{ name: 'descripcion', type: 'string' }				
		]
	});

 
	var  storeCatPortafolio = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
                autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41altaSolicitudExt.data.jsp',
			reader: {
                            type: 'json',
                            root: 'registros'
			},
			extraParams: {
                            informacion: 'catPort'
			},
			listeners: {
                            exception: NE.util.mostrarProxyAjaxError
			}
		}
	});  
        

        storeCatPortafolio.on('load',function (store, records, successful, eOpts ){
            if (icSol != null){
                Ext.getCmp('idPortafoio').setValue(eidPortafolio+'');
            }
        });    

	var  storeCatDoc = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
                autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41altaSolicitudExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catDoc'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});      

        storeCatDoc.on('load',function (store, records, successful, eOpts ){
            if (icSol != null){
                Ext.getCmp('tipoDocumento').setValue(etipoDoc+'');
            }
        });        

	var storeTipoMovimiento = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
                autoLoad: true,
                 data : [
                 {clave: '1', descripcion: 'Incorporaci�n de Documento'},
                 {clave: '2', descripcion: 'Nueva Versi�n'},
                 {clave: '3', descripcion: 'Adhesi�n de IF'}
                 ]
	});        
    
	var storeIF = Ext.create('Ext.data.Store', {
		model: 'ModelCatalogos',     
                autoLoad: true,
		proxy: {
			type: 'ajax',
			url: '41altaSolicitudExt.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'catIF'
			},
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		}
	});      


    var selectorIF = Ext.create("Ext.ux.form.ItemSelector", {
        id: 'ifSeleccionados',
        name:'ifSeleccionados',
        autoScroll: true,
        padding: '10 15 15 15', 
        height: 300,
        width: 800,
        fieldLabel: '',
        imagePath: '../ux/images/',
        store: storeIF,
        displayField: 'descripcion', 
        valueField: 'clave',
        allowBlank: true,
        msgTarget: 'side',
        fromTitle: 'Intermediarios Financieros Disponibles',
        toTitle: 'Intermediarios Financieros Seleccionados',
        buttons: ['add', 'remove'],
        hidden : true,
        readOnly: true
    });


var cleanForm = function(){
    Ext.getCmp('idMovimiento').setValue('');
    Ext.getCmp('idPortafoio').setValue('');
    Ext.getCmp('tipoDocumento').setValue('');
    Ext.getCmp('ifSeleccionados').setValue('');
    Ext.getCmp('nombreProducto').setValue('');
    Ext.getCmp('descripcion').setValue('');
    Ext.getCmp('idDocumentoOrigen').setValue('');
}


    function fillComentarios(opts, success, response) {
        var infoR = Ext.JSON.decode(response.responseText);
        if (success == true && infoR.success == true) {
            var actual = Ext.getCmp('descripcion').getValue();
            var arrRegistros = infoR.registros;
            var full = "";
            for (indice = 0; indice < arrRegistros.length; indice++) {
                var c = arrRegistros[indice];
                full += "Fecha: " + c.fechaRegistro + " [" + c.nombreUsuario + "]\n" + c.commentario + "\n\n";
            }
            full += "\nSolicitud inicial:\n" + actual;
            Ext.getCmp('descripcion').setValue(full);
        }
        Ext.getCmp('descripcion').setReadOnly(true);
        Ext.getCmp('descripcionComentario').setVisible(true);
        Ext.getCmp('formAlta').unmask();
        Ext.getCmp('btnComments').disable();
    }

    function mostrarTodosComentarios() {
        Ext.getCmp('formAlta').mask("Actualizando...");
        Ext.Ajax.request({
            url: '41altaSolicitudExt.data.jsp',
            params: Ext.apply({
                informacion: 'getComments',
                icSolicitud: icSol
            }),
            callback: fillComentarios
        });
    }
//----------*************  PANEL DE ALTA  *************----------//


var formAlta = new Ext.form.FormPanel({
        name: 'formAlta',
        id: 'formAlta',
        width: 943,
        height: 'auto',
        frame: true,
        title: 'Alta de Solicitudes',
        items: [
                {
                    xtype: 'textfield',
                    name : 'txtfolioSolicitud',
                    id : 'txtfolioSolicitud',
                    labelStyle: 'width:200px',
                    labelWidth: '85%',
                    width: 550, 
                    readOnly : true,
                    fieldStyle: 'background-color: #ddd; background-image: none;',
                    hidden: true,
                    fieldLabel: 'Folio de la Solicitud',
                    padding: '10 15 15 15',
                    disabled: esSoloLectura
                },
                {
                    xtype: 'combo',
                    name: 'idMovimiento',
                    id: 'idMovimiento',
                    fieldLabel: 'Tipo de Movimiento:',
                    padding: '10 15 15 15',
                    width: 550,
                    editable: false,
                    forceSelection: true, 
                    queryMode: 'local', 
                    allowBlank: false,
                    emptyText: 'Seleccione Tipo de Movimiento',
                    labelStyle: 'width:200px',
                    labelWidth: '85%',                    
                    store: storeTipoMovimiento, 
                    displayField: 'descripcion', 
                    valueField: 'clave',
                    disabled: esSoloLectura,
                    listeners: {
                        change: function (cmp, newValue) {
                            var mostrarCampo = newValue == MOVIMIENTO_NUEVA_VERSION || newValue == MOVIMIENTO_ADHESION_IF;
                            Ext.getCmp('idDocumentoOrigen').setVisible(mostrarCampo);
                            Ext.getCmp('idDocumentoOrigen').allowblank = !mostrarCampo;
                        }
                    }                      
                },     
                {
                    xtype: 'maskfield',
                    mask: '####-#####',
                    name: 'idDocumentoOrigen',
                    id: 'idDocumentoOrigen',
                    fieldLabel: 'Folio del documento origen:',
                    padding: '10 15 15 15',
                    width: 550,
                    allowBlank: false,
                    allowPartial: true,
                    labelStyle: 'width:200px',
                    labelWidth: '85%',                    
                    disabled: esSoloLectura,
                    hidden: true
                },                 
                {
                    xtype: 'combo',
                    id: 'idPortafoio',
                    name: 'idPortafoio',
                    fieldLabel: 'Portafolio:',
                    padding: '10 15 15 15',
                    width: 550,
                    editable: false,
                    forceSelection: true, 
                    allowBlank: false,
                    emptyText: 'Seleccione Portafolio',
                    labelStyle: 'width:200px',
                    labelWidth: '85%',
                    store: storeCatPortafolio, 
                    displayField: 'descripcion', 
                    queryMode: 'local',
                    valueField: 'clave',
                    disabled: esSoloLectura
                },
                {
                    xtype: 'combo',
                    id: 'tipoDocumento',
                    name: 'tipoDocumento',
                    fieldLabel: 'Tipo de Documento:',
                    padding: '10 15 15 15',
                    width: 550,
                    editable: false,
                    emptyText: 'Seleccione Tipo de Documento',
                    forceSelection: true,
                    queryMode: 'local',
                    allowBlank: false,
                    labelStyle: 'width:200px',
                    labelWidth: '85%',
                    store: storeCatDoc, 
                    displayField: 'descripcion', 
                    disabled: esSoloLectura,
                    valueField: 'clave',
                    listeners: {
                        change: function (cmp, newValue) {
                                if(newValue==GARANTIA_SELECTIVA){
                                    Ext.getCmp('nombreProducto').setFieldLabel('Nombre de Empresa');
                                }
                                else{
                                    Ext.getCmp('nombreProducto').setFieldLabel('Nombre de Producto');
                                }
                        }
                    }                    
                },
                {
                    xtype: 'radiogroup',
                    id: 'radiosSeleccionarIF',
                    name: 'radiosSeleccionarIF',
                    fieldLabel: 'Preseleccionar Intermediarios Financieros',
                    padding: '10 15 15 15',
                    width: 600,
                    labelStyle: 'width:300px',
                    labelWidth: '100%',                    
                    forceSelection: true,
                    disabled: esSoloLectura,
                    columns: 2,
                    items: [
                        {boxLabel: 'S�', name: 'seleccionarIF', id: 'radio1', inputValue:SI},
                        {boxLabel: 'No', name: 'seleccionarIF', id: 'radio2', inputValue:NO, checked: true}
                    ],
                    listeners: {
                        change: function (cmp, newValue) {
                                selectorIF.setVisible(newValue['seleccionarIF']==1);
                                selectorIF.setDisabled(esSoloLectura);
                        }
                    }
                }, 
                selectorIF,
                {
                    xtype: 'textfield',
                    id: 'nombreProducto',
                    name: 'nombreProducto',
                    fieldLabel: 'Nombre de Producto',
                    padding: '10 15 15 15',
                    width: 800,
                    forceSelection: true,
                    allowBlank: false,
                    labelStyle: 'width:200px',
                    labelWidth: '45%',
                    disabled: esSoloLectura
                },
                {
                    xtype: 'textareafield',
                    id: 'descripcion',
                    name: 'descripcion',
                    fieldLabel: 'Descripci�n de la Solicitud',
                    padding: '10 15 15 15',
                    width: 800,
                    height: 150,
                    forceSelection: true,
                    allowBlank: false,
                    labelWidth: '45%',
                    disabled: esSoloLectura
                },
                {
                    xtype: 'textareafield',
                    id: 'descripcionComentario',
                    name: 'descripcionComentario',
                    fieldLabel: 'Complementar la Solicitud',
                    padding: '10 15 15 15',
                    width: 800,
                    height: 100,
                    forceSelection: true,
                    allowBlank: false,
                    labelWidth: '45%',
                    hidden: true
                },                
                {
                    xtype: 'button',
                    id: 'btnComments',
                    text: 'Comentarios:',
                    iconCls: 'icoGuardar',
                    margin: '0 15 0 15',
                    padding: '5 5 5 5',
                    hidden: icSol == null,
                    handler: function (boton, evento) {
                        mostrarTodosComentarios();
                    }
                },                 
                {
                    xtype: 'form',
                    id: 'formFile',
                    name: 'formFile',
                    frame: true,
                    border: 0,
                    items: [{
                        xtype: 'filefield',
                        id: 'archivo',
                        emptyText: 'Nombre del archivo a cargar',
                        fieldLabel: 'Nombre del archivo a cargar',
                        name: 'archivoSoporte',   
                        buttonText: 'Cargar archivo',
                        labelWidth: '45%',
                        disabled: esSoloLectura,
                        width: 800,	  
                        padding: '15 15 15 15',
                        buttonCfg: {
                            iconCls: 'upload-icon'
                        },
                        vtype: 'archivoPdfXlsxZip',
                        listeners: {
                            change: function(fld, value){
                                fld.setRawValue(value.substring(value.lastIndexOf("\\")+1));                           
                                if(fld.isValid()){
                                    cargaArchivo('CARGAR_ARCHIVO',null);
                                }
                            }
                        }
                        
                    }]
                },
                {
                    xtype: 'form',
                    id: 'formButtonsFile',
                    frame: true,
                    name: 'formButtonsFile',
                    padding:'10 10 10 10',
                    layout: {
                        type: 'table',
                        columns: 2
                    },
                    defaults: {
                        bodyStyle: 'margin: 10px'
                    },                                      
                    border:0
                },                
                NE.util.getEspaciador(10),
                {
                    xtype: 'displayfield',
                    forId: 'nota01',
                    value:'Incluir los documentos m�nimos para proceder a la atenci�n de la solicitud que son: <br/>Presentaci�n y Acuerdo de Instancia de decisi�n y en su caso otros archivos que sirvan de soporte para atender la solicitud.',
                    height: 40,
                    margin: '10 10 10 15',
                    border: 1,
                    style: {
                        borderColor: 'black',
                        borderStyle: 'solid',
                        backgroundColor: 'lightgrey',   
                        padding:'10 10 10 10'
                    }
                },
                NE.util.getEspaciador(10)
                
        ],
        buttons: [      
        {
            text: 'Guardar',
            id: 'btnGuardar',
            iconCls: 'icoGuardar',
            disabled: esSoloLectura,
            handler: function (boton, evento) {
                var panel = this.up('form');
                procesarGuardarSolicitud(panel);
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            disabled: esSoloLectura,
            handler: function () {
                if(dataFiles.length>0){
                    Ext.Msg.show({
                        title:'�Eliminar Archivo?',
                        msg: '�Estas seguro que deseas eliminar los archivo cargados?.\n�Esta acci�n no podr� revertirse!',
                        buttons: Ext.Msg.YESNOCANCEL,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                cleanForm();
                                Ext.getCmp('contenedorPrincipal').mask("Espere un momento...");
                                Ext.Ajax.request({
                                    url: '41altaSolicitudExt.data.jsp',
                                    params: Ext.apply(  {
                                        informacion: 'deleteDocFiles',
                                        icArchivos: Ext.encode(dataFiles)
                                    }),
                                    callback: eliminaArchivos
                                });                                  
                            } 
                        }
                    });                       
                }
                else{
                    cleanForm()
                }
            }           
        },
        {
            text:'Regresar',
            id:'btnHome',
            iconCls: 'icoRegresar',
            handler: function() {
                if(!esSoloLectura && dataFiles.length > 0){
                    Ext.Msg.show({
                        title:'�Salir?',
                        msg: '�Estas seguro que deseas salir de la pantalla?\n�Se perderan todos los cambios no guardados!',
                        buttons: Ext.Msg.YESNOCANCEL,
                        icon: Ext.Msg.QUESTION,
                        fn: function(btn) {
                            if (btn === 'yes') {
                                window.location = '41homeSolicitudesExt.jsp?idMenu=41HOMESOLICITUDES';                                
                            } 
                        }
                    });                       
                }
                else{
                    window.location = '41homeSolicitudesExt.jsp?idMenu=41HOMESOLICITUDES';
                }                            
            }
        }],
        getInvalidFields: function() {
            var invalidFields = [];
            Ext.suspendLayouts();
            this.form.getFields().filterBy(function(field) {
                if (field.validate()) return;
                invalidFields.push(field);
            });
            Ext.resumeLayouts(true);
            return invalidFields;
        }        
    });


//----------*************  CONTENEDOR PRINCIPAL  *************----------//

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			formAlta
		]        
	});



/****************** CARGA INICIAL  ******************/
    var icSol =  null;
    icSol = eicSol;
    if (icSol != null){    
        pnl.mask("Cargando...");
        var txtFolioS = Ext.getCmp('txtfolioSolicitud');
        Ext.getCmp('idMovimiento').setValue(eidTipoSol+"");
        txtFolioS.setVisible(true);
        txtFolioS.setValue(enfs+'');
        Ext.getCmp("nombreProducto").setValue(eNombre.trim());
        Ext.getCmp("descripcion").setValue(eDescripcion.trim());        
        if(eIntermediarios != null && eIntermediarios.length > 0){
            Ext.getCmp('ifSeleccionados').setValue(eIntermediarios);
            Ext.getCmp("ifSeleccionados").setVisible(true);
            Ext.getCmp("radio1").setValue(true);
            Ext.getCmp("radio2").setValue(false);
        }        
        for(var i=0; i<eFiles.length; i++){
            var file  = eFiles[i];
            crearBotonesArchivo(file.clave, file.descripcion);
        }
        if(folioDocOrigen.length > 0){
            var replaceGuion = folioDocOrigen.replace('-','');
            Ext.getCmp("idDocumentoOrigen").setValue(replaceGuion);
        }
        Ext.getCmp("btnComments").setVisible(true);
        pnl.unmask();
    }
    

}); 