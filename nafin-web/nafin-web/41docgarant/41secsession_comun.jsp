<%
String strDirecCSS = "";
String strTipoUsuario = "", iNoUsuario = "", iNoCliente = "";
String strClase = "";
String strPerfil = "";
String iNoEPO = "";
String strDirectorioPublicacion = "";
String strDirectorioTemp = "";
String strDirecVirtualTemp = "";
String strDirTrabajo = "";
String strDirecVirtualTrabajo = "";
String strNombre ="", strNombreUsuario =""; 
strDirecCSS = "/nafin/41docgarant/css";
Long iNoNafinElectronico = null;

strTipoUsuario = (String)session.getAttribute("strTipoUsuario"); // POSIBLES VALORES NAFIN-EPO-PYME-IF
iNoUsuario = (String)session.getAttribute("iNoUsuario"); // 
iNoCliente = (String)session.getAttribute("iNoCliente");//NO DE CLIENTE DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
iNoEPO = (String)session.getAttribute("iNoEPO");//NO DE EPO al que pertenece la pyme en caso de que el tipo de afiliado sea  PYME
strClase = (String)session.getAttribute("strClase");//Clase css
strPerfil = (String)session.getAttribute("sesPerfil"); // Variable que contiene el perfil general del usuario
strDirectorioPublicacion = (String)application.getAttribute("strDirectorioPublicacion");

strDirectorioTemp = strDirectorioPublicacion+"00tmp/41docgarant/";
strDirecVirtualTemp = "/nafin/00tmp/41docgarant/";
strDirTrabajo = strDirectorioPublicacion+"00archivos/41docgarant/";
strDirecVirtualTrabajo = "/nafin/00archivos/41docgarant/";
strNombre = (session.getAttribute("strNombre")==null)?"":(String)session.getAttribute("strNombre");                  // NOMBRE COMERCIAL O DE LA PERSONA DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
strNombreUsuario = (session.getAttribute("strNombreUsuario")==null)?"":(String)session.getAttribute("strNombreUsuario"); // NOMBRE DEL USUARIO DE TABLA 
String strLogin = (session.getAttribute("sesCveUsuario")==null)?"":(String)session.getAttribute("sesCveUsuario");           // LOGIN DEL USUARIO QUE SE LOGUEA
String strLogo = (String)session.getAttribute("strLogo");//Logotipo
iNoNafinElectronico = (Long)session.getAttribute("iNoNafinElectronico");//NUMERO DE LA EPO A LA CUAL PERTENECE LA PYME

boolean esEsquemaExtJS = (session.getAttribute("version") != null && session.getAttribute("version").equals("2011"))?true:false;

if(esEsquemaExtJS) {
	//------------------------- BITACORA DE NAVEGACION DEL USUARIO -------------------------
	netropology.utilerias.Bitacora.grabarLog(request.getRequestURI(), 
			iNoUsuario, request.getParameter("informacion"), 
			session.getId());
	//--------------------------------------------------------------------------------------
}
%>