
Ext.onReady(function () {


    function descargaArchivos(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var infoR = Ext.JSON.decode(response.responseText);
            var archivo = infoR.urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma  	= Ext.getDom('formAux'); 
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method 		= 'post';
			forma.target 		= '_self'; 	
			forma.submit();		
            }else {
                NE.util.mostrarConnError(response,opts);
        }
    }

    Ext.define('ModelCatalogos', {
        extend: 'Ext.data.Model',
        fields: [{
            name: 'clave',
            type: 'string'
        }, {
            name: 'descripcion',
            type: 'string'
        }]
    });



    var storeEstatusBO = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41monitorNivelServExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEstatusBO'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });


    var storeTipoDocumento = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41monitorNivelServExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catTipoDocumento'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
        var storeTipoMovimiento = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41monitorNivelServExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catTipoMovimiento'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
        var storeProceso = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41monitorNivelServExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catProceso'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
        var storePortafolio = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41monitorNivelServExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catPortafolio'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
        var storeProducto = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41monitorNivelServExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catProducto'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });
    
    var storeEjecutivo = Ext.create('Ext.data.Store', {
        model: 'ModelCatalogos',
        autoLoad: true,
        proxy: {
            type: 'ajax',
            url: '41monitorNivelServExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'catEjecutivos'
            },
            listeners: {
                exception: NE.util.mostrarProxyAjaxError
            }
        }
    });

var formBusqueda = new Ext.form.FormPanel({
        name: 'boformBusqeda',
        id: 'boformBusqueda',
        title: 'Panel de Busqueda',
        width: 943,
        frame: true,
        height: 250,
        border: 0,
        layout: 'column',
        items: [
        {
            xtype: 'fieldset',
            id: 'bofieldsBusqueda1',
            height: 200,
            border: false,
            columnWidth: 0.5,
            items: [
            {
                xtype: 'combo',
                name: 'tipoDocumento',
                id: 'tipoDocumento',
                fieldLabel: 'Tipo de Documento',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los tipos',
                store: storeTipoDocumento,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'combo',
                name: 'tipoMovimiento',
                id: 'tipoMovimiento',
                fieldLabel: 'Tipo de Movimiento',
                padding: 5,
                width: 400,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los tipos',
                store: storeTipoMovimiento,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },            
            /*{
                xtype: 'combo',
                id: 'proceso',
                name: 'proceso',
                fieldLabel: 'Proceso',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Procesos',
                store: storeProceso,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },*/
            {
                xtype: 'combo',
                id: 'portafolio',
                name: 'portafolio',
                fieldLabel: 'Portafolio',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Portafolios',
                store: storePortafolio,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },            
            {
                xtype: 'combo',
                id: 'producto',
                name: 'producto',
                fieldLabel: 'Producto',
                padding: 5,
                width: 400,
                forceSelection: true,
                allowBlank: true,
                emptyText: 'Todos los Productos',
                store: storeProducto,
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            }
            ]
        }, {
            xtype: 'fieldset',
            height: 200,
            border: false,
            columnWidth: 0.5,
            margins: '0 20 0 0',
            items: [
            {
                xtype: 'combo',
                id: 'estatus',
                name: 'estatus',
                fieldLabel: 'Estatus',
                padding: 5,
                width: 440,
                forceSelection: true,
                allowBlank: true,
                store: storeEstatusBO,
                emptyText: 'Todos los estatus',
                displayField: 'descripcion',
                editable: false,
                valueField: 'clave'
            },             
            {
                xtype: 'combo',
                id: 'etapaProceso',
                name: 'etapaProceso',
                fieldLabel: 'Etapa del Proceso',
                padding: 5,
                width: 440,
                forceSelection: true,
                allowBlank: true,
                store: storeProceso,
                emptyText: 'Todas las etapas',
                displayField: 'descripcion',
                editable: false,
                valueField: 'clave'
            },            
            {
                xtype: 'combo',
                id: 'ejecutivo',
                name: 'ejecutivo',
                fieldLabel: 'Ejecutivo',
                padding: 5,
                width: 440,
                store: storeEjecutivo,
                forceSelection: true,
                queryMode: 'local',
                allowBlank: true,
                emptyText: 'Todos los Ejecutivos',
                displayField: 'descripcion',
                valueField: 'clave',
                editable: false
            },
            {
                xtype: 'fieldset',
                collapsible: false,
                border: false,
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                padding: 0,
                layout: 'hbox',
                items: [{
                    xtype: 'datefield',
                    id: 'fechaSolicitud1',
                    name: 'fechaSolicitud1',
                    fieldLabel: 'Fecha de Solicitud',
                    padding: 5,
                    width: 215,
                    emptyText: 'Desde',
                    displayField: 'descripcion',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }, {
                    xtype: 'datefield',
                    id: 'fechaSolicitud2',
                    name: 'fechaSolicitud2',
                    fieldLabel: 'al',
                    labelWidth: 55,
                    padding: 10,
                    width: 170,
                    emptyText: 'Hasta',
                    valueField: 'clave',
                    forceSelection: true,
                    allowBlank: true
                }]
            },
            {
                xtype: 'fieldcontainer',
                fieldLabel: 'Ultimo Estatus',
                defaultType: 'checkboxfield',
                id : 'boxUltimoEstatus',
                items: [
                    {
                        boxLabel  : 'Mostrar',
                        name      : 'ultimoEstatus',
                        inputValue: '1',
                        id        : 'ultimoEstatus',
                        checked   : true
                    }
                ]
            }          
            ]
        }],
        buttons: [{
            text: 'Buscar',
            id: 'bobtnBuscar',
            iconCls: 'icoBuscar',
            handler: function (boton, evento) {
                //var resultado = validarFechas();
                var resultado = true;
                if (resultado != true) {
                    Ext.MessageBox.alert('Aviso', resultado);
                } else {
                    grid.el.mask('Cargando...', 'x-mask-loading');
                    gridStore.load({
                        params: Ext.apply(formBusqueda.getForm().getValues())
                    });
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                formBusqueda.getForm().reset();
            }
        }]
    });
    

    Ext.define('DocsBODataModel', {
        extend: 'Ext.data.Model',
        fields: 
        [
       {name:'icProcesoBitacora',  mapping: 'icProcesoBitacora' },
       {name:'icEstatus',  mapping: 'icEstatus' },
       {name:'fechaSolicitud'    , mapping: 'fechaSolicitud'    },
       {name:'folio'             , mapping: 'folio'             },
       {name:'nombreIF'          , mapping: 'nombreIF'          },
       {name:'portafolio'        , mapping: 'portafolio'        },
       {name:'producto'          , mapping: 'producto'          },
       {name:'tipoDocumento'     , mapping: 'tipoDocumento'     },
       {name:'tipoMovimiento'    , mapping: 'tipoMovimiento'    },
       {name:'nombreEjecutivo'   , mapping: 'nombreEjecutivo'   },
       {name:'etapaProceso'      , mapping: 'etapaProceso'      },
       {name:'fechaInicioProceso', mapping: 'fechaInicioProceso'},
       {name:'fechaAtencion'     , mapping: 'fechaAtencion'     },
       {name:'nivelServicio'     , mapping: 'nivelServicio'     },
       {name:'diasAtencion'      , mapping: 'diasAtencion'      },
       {name:'estatusNivel'      , mapping: 'estatusNivel'      },
       {name:'comentarios'       , mapping: 'comentarios'       }        
        ]
    });    
    
        var procesarConsulta = function(store, arrRegistros, opts) {
        if (arrRegistros != null) {
            if(store.getTotalCount() <= 0) {
                Ext.MessageBox.alert('Aviso','No existe informaci�n con los criterios determinados');
            }
        }
        grid.unmask();
    }
    
    
    
    var gridStore = Ext.create('Ext.data.Store', {
        model: 'DocsBODataModel',
        proxy: {
            type: 'ajax',
            url: '41monitorNivelServExt.data.jsp',
            reader: {
                type: 'json',
                root: 'registros'
            },
            extraParams: {
                informacion: 'monitorNivelServicio'
            }
        },
        autoLoad: false,
        listeners: {
            exception: NE.util.mostrarDataProxyError,
            load: procesarConsulta
        }
    });
    
    
    
    
    
    function agregarComentario(record){
        var folio = record.get("folio");
        var icProcesoDocto = record.get("icProcesoBitacora");
        var icEstatus = record.get("icEstatus");
         Ext.create('Ext.window.Window', {
            title: 'Agregar comentario al proceso del documento '+folio,
            id: 'winGuardaComment',
            height: 180,
            width: 450,
            bodyStyle: 'background:#FFF;',
            frame:true,
            modal: true,
            items: [
                {
                    xtype: 'textareafield',
                    id : 'commentario',
                    name: 'commentario',
                    fieldLabel: 'Comentario:',
                    padding: "20 0 0 15",
                    width: 400,
                    labelWidth: 80,
                    emptyText: 'comentario',
                    allowBlank: false
                },
                NE.util.getEspaciador(20),                
                {
                    xtype: 'button',
                    text: 'Guardar',
                    style: {
                        marginLeft: '150px'
                    },
                    iconCls: 'icoGuardar',
                    handler: function() {
                    var campo = Ext.getCmp("commentario");
                        var valor = campo.getValue();
                        campo.setValue(valor.trim());
                        if(Ext.getCmp("commentario").isValid()){
                            Ext.Ajax.request({
                                url: '41monitorNivelServExt.data.jsp',
                                params: Ext.apply(  {
                                    informacion: 'saveComments',
                                    icProcesoDocto: icProcesoDocto,
                                    icEstatus: icEstatus,
                                    comentario: Ext.getCmp("commentario").getValue()
                                }),
                            callback: function(opts, success, response) {
                                var jsondeAcuse = Ext.JSON.decode(response.responseText);
                                if (success == true && jsondeAcuse.success == true) {	
                                        gridStore.reload();
                                        Ext.MessageBox.alert('Guardar comentario', 'Se ha guardado el comentario correctamente.');
                                } else {
                                        NE.util.mostrarConnError(response,opts);
                                }
                                var windowFecha = Ext.getCmp("winGuardaComment");
                                windowFecha.close();
                                }
                            });                          
                        }
                        }
                    }
                ,
                {
                    xtype: 'button',
                    text: 'Cancelar',
                    iconCls: 'icoCancelar',
                    style: {
                        marginLeft: '25px'
                    },
                    handler: function() {
                        var windowFecha = Ext.getCmp("winGuardaComment");
                        windowFecha.close();
                    }
                }        
            ]
        }).show();     
    }
    
    
    
    var grid = Ext.create('Ext.grid.Panel', {
        id: 'grid',
        store: gridStore,
        stripeRows: true,
        title: 'Base de Operaciones',
        width: 944,
        frame: true,
        height: 400,
        collapsible: false,
        enableColumnMove: true,
        enableColumnResize: true,
        autoScroll: true,
        columns: [{
            header: "Fecha de<br/>Solicitud",
            menuText: "Fecha de Solicitud",
            dataIndex: 'fechaSolicitud',
            width: 90,
            sortable: true,
            hideable: true            
        }, {
            header: "Folio de<br/>Solicitud",
            dataIndex: 'folio',
            width: 80,
            sortable: true,
            hideable: true
        }, {
            header: "Intermediario Financiero",
            dataIndex: 'nombreIF',
            width: 180,
            sortable: true,
            hideable: true
        }, {
            header: "Portafolio",
            dataIndex: 'portafolio',
            align: 'left',
            width: 150,
            sortable: true,
            hideable: true        
        }, {
            header: "Producto",
            dataIndex: 'producto',
            align: 'left',
            width: 250,
            sortable: true,
            hideable: true   
        },
        {
            header: "Tipo de Documento",
            dataIndex: 'tipoDocumento',
            align: 'left',
            width: 250,
            sortable: true,
            hideable: true   
        }, 
        {
            header: "Tipo de Movimiento",
            dataIndex: 'tipoMovimiento',
            align: 'left',
            width: 200,
            sortable: true,
            hideable: true   
        },
        {
            header: "Nombre del Ejecutivo",
            dataIndex: 'nombreEjecutivo',
            align: 'left',
            width: 250,
            sortable: true,
            hideable: true   
        },         
        {
            header: "Etapa del Proceso",
            align: 'left',
            dataIndex: 'etapaProceso',
            hideable: true,
            sortable: true,
            width: 180
        }, 
        {
            header: "Fecha de Inicio",
            align: 'center',
            dataIndex: 'fechaInicioProceso',
            hideable: true,
            sortable: true,
            width: 110
        },        
        {
            header: "Fecha de Atenci&oacute;n",
            align: 'center',
            dataIndex: 'fechaAtencion',
            hideable: true,
            sortable: true,
            width: 120,
            renderer: function (value, metaData, record) {
                if (value == ""){
                    return "Por atender";
                }
            return value;
            }            
        },
        {
            header: "Nivel de Servicio<br/>(d�as habiles)",
            align: 'center',
            dataIndex: 'nivelServicio',
            hideable: true,
            sortable: true,
            width: 100
        },        
        {
            header: "D�as h�biles<br/>de Atenci�n",
            align: 'center',
            dataIndex: 'diasAtencion',
            hideable: true,
            sortable: true,
            width: 90,
            renderer: function (value, metaData, record) {               
                return value;
            }
        },        
        {
            header: "Descripcion de Estatus<br/>(En tiempo, Con retraso)",
            align: 'center',
            dataIndex: 'estatusNivel',
            hideable: true,
            sortable: true,
            width: 150,
            renderer: function (value, metaData, record) {            
                var nivel = record.get("nivelServicio");
                var atencion = record.get("diasAtencion");
                if (atencion > nivel ){
                    return "<span style='color:red;'>Con retraso<\/span>";
                }
                else{
                    return  "<span style='color:green;'>En tiempo<\/span>"
                }
            }
        },         
        {
            header: "Observaciones",
            dataIndex: 'comentarios',
            align: 'left',
            width: 180,
            sortable: true,
            hideable: true,
            renderer: function(value) {
                return '<span data-qtitle="Observaciones" data-qwidth="250" ' +
                    'data-qtip="' + value + '">' +
                    value + '<\/span>';
            }            
        },
        {
            xtype: 'actioncolumn',
            header: "Acciones",
            menuText: "Acciones",
            align: 'center',
            dataIndex: 'icProcesoBitacora',
            hideable: true,
            sortable: true,
            width: 110,
            items: [{
                iconCls: 'icoGuardar',
                tooltip: 'Agregar Comentario',
                border: '10 10 10 10',
                handler: function (grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    agregarComentario(record);
                },
                isDisabled: function (view, rowIndex, colIndex, item, record) {
                        fechaAtencion = record.get("fechaAtencion");
                    return fechaAtencion != "";
                }
            }
            ]
        },        
        ],
        buttons: [
        {
            text: 'Exportar a Excel',
            id: 'btnXLS',
            iconCls: 'icoBotonXLS',
            handler: function(boton, evento) {
                Ext.Ajax.request({
                    url: '41monitorNivelServExt.data.jsp',
                    params: Ext.apply(  {
                        informacion: 'GenerarReporte',
                        tipoDocumento    : Ext.getCmp('tipoDocumento').getValue(),
                        tipoMovimiento   : Ext.getCmp('tipoMovimiento').getValue(),
                        portafolio       : Ext.getCmp('portafolio').getValue(),
                        producto         : Ext.getCmp('producto').getValue(),
                        estatus          : Ext.getCmp('estatus').getValue(),
                        etapaProceso     : Ext.getCmp('etapaProceso').getValue(),
                        ejecutivo        : Ext.getCmp('ejecutivo').getValue(),
                        fechaSolicitud1  : Ext.getCmp('fechaSolicitud1').getValue(),
                        fechaSolicitud2  : Ext.getCmp('fechaSolicitud2').getValue(),
                        ultimoEstatus    : Ext.getCmp('ultimoEstatus').getValue()
                    }),
                    callback: descargaArchivos
                })
            }   
        }]   
    });
    


    var panelPrincipal = new Ext.Container({
		id: 'contenedorPrincipal',
		renderTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(5),	
			formBusqueda,
			NE.util.getEspaciador(20),
                        grid,
			NE.util.getEspaciador(20)
		]        
	});


});
