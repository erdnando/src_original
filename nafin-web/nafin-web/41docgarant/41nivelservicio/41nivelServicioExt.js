Ext.onReady(function () {


    function guardaConfiguracion(){
        contenedorPrincipal.mask('Guardando Niveles de Servicio...', 'mask-loading');
                Ext.Ajax.request({
                url: '41nivelServicioExt.data.jsp',
                params:  {
                    informacion: 'guardarConfiguracion',
                    configuracion: JSON.stringify(formulario.getForm().getValues())
                },
                callback: function(opts, success, response) {
                    if (success == true && JSON.parse(response.responseText).success == true) {
                        var resp = JSON.parse(response.responseText);
                        Ext.Msg.alert('Guardar Niveles de Servicio', resp.msg);
                    } else {
                        NE.util.mostrarConnError(response, opts);
                    }
                    contenedorPrincipal.unmask();
                }
            });
    }

    /*** * * * * * * * * * PANEL PRINCIPAL * * * * * * * * * * * * * * * * * * ***/
    var formulario = new Ext.form.FormPanel({
        name: 'formulario',
        id: 'formulario',
        width: 943,
        height: 'auto',
        frame: true,	
        title: 'Captura de Niveles de Servicio',
        layout: 'column',
        items: [{
            xtype: 'fieldset',
            id: 'fieldsNiveles',
            height: 'auto',
            border: false,
            padding: 10,
            columnWidth: 1,
            items: [{
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                        xtype: 'label',
                        html: '<strong>Procesos<\/strong>',
                        padding: '0 5'
                    }, {
                        xtype: 'label',
                        html: '<strong>N�mero de d�as<\/strong>',
                        padding: '0 0 0 260'

                    }, {
                        xtype: 'label',
                        html: '<strong>Alerta previa a fecha de vencimiento<br>de nivel de servicio<\/strong>',
                        padding: '0 0 0 170'
                    }]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                        xtype: 'label',
                        text: 'T�rminos y Condiciones'
                    }]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numTCproceso01', name: 'numTCproceso01',
                            fieldLabel: 'En Proceso',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numTCproceso02', name: 'numTCproceso02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numTCproceso01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                            
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numTCvalidacion01', name: 'numTCvalidacion01',
                            fieldLabel: 'En Validaci�n',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numTCvalidacion02', name: 'numTCvalidacion02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numTCvalidacion01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                             
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numTCjuridico01', name: 'numTCjuridico01',
                            fieldLabel: 'Validaci�n Juridico',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },
                        {
                            xtype: 'numberfield',
                            id : 'numTCjuridico02', name: 'numTCjuridico02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numTCjuridico01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                             
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numTCformif01', name: 'numTCformif01',
                            fieldLabel: 'Formalizaci�n IF',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numTCformif02', name: 'numTCformif02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numTCformif01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                 
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numTCrubnafin01', name: 'numTCrubnafin01',
                            fieldLabel: 'Rubrica NAFIN',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numTCrubnafin02', name: 'numTCrubnafin02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numTCrubnafin01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                  
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numTCformnafin01', name: 'numTCformnafin01',
                            fieldLabel: 'Formalizaci�n NAFIN',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numTCformnafin02', name: 'numTCformnafin02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numTCformnafin01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                              
                        },
                    ]
                },
                /********************************* REGLAMENTO OPERATIVO ***************************/
                {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                        xtype: 'label',
                        text: 'Reglamento Operativo'
                    }]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numROproceso01', name: 'numROproceso01',
                            fieldLabel: 'En Proceso',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numROproceso02', name: 'numROproceso02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numROproceso01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                             
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numROvalidacion01', name: 'numROvalidacion01',
                            fieldLabel: 'En Validaci�n',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numROvalidacion02', name: 'numROvalidacion02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numROvalidacion01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                                           
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numROformif01', name: 'numROformif01',
                            fieldLabel: 'Formalizaci�n IF',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numROformif02', name: 'numROformif02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numROformif01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                                           
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numROrubnafin01', name: 'numROrubnafin01',
                            fieldLabel: 'Rubrica NAFIN',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numROrubnafin02', name: 'numROrubnafin02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numROrubnafin01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                   
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numROformnafin01', name: 'numROformnafin01',
                            fieldLabel: 'Formalizaci�n NAFIN',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numROformnafin02', name: 'numROformnafin02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numROformnafin01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                
                        },
                    ]
                },
                /********************************* FICHAS ***************************/
                {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                        xtype: 'label',
                        text: 'Fichas'
                    }]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numFIproceso01', name: 'numFIproceso01',
                            fieldLabel: 'En Proceso',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numFIproceso02', name: 'numFIproceso02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numFIproceso01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                             
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numFIvalidacion01', name: 'numFIvalidacion01',
                            fieldLabel: 'En Validaci�n',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numFIvalidacion02', name: 'numFIvalidacion02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numFIvalidacion01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                 
                        },
                    ]
                },  {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numFIformif01', name: 'numFIformif01',
                            fieldLabel: 'Formalizaci�n IF',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numFIformif02', name: 'numFIformif02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numFIformif01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                             
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numFIrubnafin01', name: 'numFIrubnafin01',
                            fieldLabel: 'Rubrica NAFIN',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numFIrubnafin02', name: 'numFIrubnafin02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numFIrubnafin01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                    
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numFIformnafin01', name: 'numFIformnafin01',
                            fieldLabel: 'Formalizaci�n NAFIN',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numFIformnafin02', name: 'numFIformnafin02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numFIformnafin01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                 
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                        xtype: 'label',
                        text: 'Fichas'
                    }]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numBOelaboracion01', name: 'numBOelaboracion01',
                            fieldLabel: 'En Elaboraci�n de Bases de Operaci�n',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numBOelaboracion02', name: 'numBOelaboracion02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numBOelaboracion01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                 
                        },
                    ]
                }, {
                    xtype: 'fieldset',
                    border: false,
                    padding: 0,
                    layout: 'hbox',
                    items: [{
                            xtype: 'numberfield',
                            id : 'numBOvalidacion01', name: 'numBOvalidacion01',
                            fieldLabel: 'En Validaci�n y Env�o de Bases de Operaci�n',
                            labelWidth: 300,
                            allowBlank: false,
                            padding: '0 20',
                            value: 1,
                            minValue: 1,
                            maxValue: 99
                        },

                        {
                            xtype: 'numberfield',
                            id : 'numBOvalidacion02', name: 'numBOvalidacion02',
                            labelWidth: 100,
                            fieldLabel: '',
                            allowBlank: false,
                            padding: '0 150',
                            value: 1,
                            minValue: 1,
                            maxValue: 99,
                            validator: function(val) {
                                var dias = Ext.getCmp("numBOvalidacion01").getValue();
                                if (val >=  dias ){
                                    return "Debe ser menor al numero de d�as"
                                }
                                return true;
                            }                                       
                        },
                    ]
                }
            ]
        }],
        buttons: [{
            text: 'Guardar',
            id: 'btnGuardar',
            iconCls: 'icoGuardar',
            handler: function () {
                if (Ext.getCmp('formulario').getForm().isValid()){
                    guardaConfiguracion();
                }
            }
        }, {
            text: 'Limpiar',
            iconCls: 'icoLimpiar',
            handler: function () {
                Ext.Msg.confirm("Limpiar", "�Estas seguro de limpiar todos los valores?", function(btnText){
                if(btnText === "yes"){
                    Ext.getCmp('formulario').getForm().reset();
                }
            }, this);
                
            }
        }]
    });

    //**********************  CONTENEDOR PRINCIPAL  *********************//
    var contenedorPrincipal = new Ext.Container({
        id: 'contenedorPrincipal',
        renderTo: 'areaContenido',
        width: 949,
        style: 'margin:0 auto;',
        items: [
            NE.util.getEspaciador(20),
            formulario
        ]
    });

    function getFielNameFromObj(obj){
        var nombre = "num";
        switch(obj.icProceso) {
          case 3100:
            nombre +="TC";
            break;
          case 3200:
            nombre +="RO";
            break;
          case 3300:
            if(obj.icEstatus == 175 || obj.icEstatus == 180){
                nombre += "BO";
            }else{
                nombre +="FI";
            }
            break;                  
          default:
            nombre +="";
        }
        switch(obj.icEstatus) {
          case 110:
            nombre +="proceso";
            break;
          case 120:
            nombre +="validacion";
            break;
          case 130:
            nombre +="juridico";
            break;
          case 140:
            nombre +="formif";
            break;
          case 150:
            nombre +="rubnafin";
            break;
          case 160:
            nombre +="formnafin";
            break;           
          case 175:
            nombre +="elaboracion";
            break;
          case 180:
            nombre +="validacion";
            break;            
          default:
            nombre +="";
        }        
        return nombre;
    }

    
    function FillDataFields(opts, success, response) {
        if (success == true && Ext.JSON.decode(response.responseText).success == true ) {
            var info = Ext.JSON.decode(response.responseText);
            var lista = info.registros;
            lista.forEach(function(obj){
                var nameField = getFielNameFromObj(obj);
                var fieldUno = Ext.getCmp(nameField+"01");
                var fieldDos = Ext.getCmp(nameField+"02");
                fieldUno.setValue(obj.diasAtencion);
                fieldDos.setValue(obj.diasAlerta);
            });
        }else {
            NE.util.mostrarConnError(response,opts);
        }
    }
    
    Ext.Ajax.request({
        url: '41nivelServicioExt.data.jsp',
        params: Ext.apply(  {
            informacion: 'getConfiguracion'
        }),
        callback: FillDataFields
    });

});
