<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.baseoperaciones.*,
        com.nafin.docgarantias.nivelesdeservicio.*,
        com.nafin.docgarantias.formalizacion.*,
        com.nafin.docgarantias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar="";
log.info("INFORMACION: "+informacion);
NivelesServicio nivelServicioBean = ServiceLocator.getInstance().lookup("NivelesServicio",NivelesServicio.class);


if (informacion.equals("monitorNivelServicio")){ 
    Map<String, String> filtros = new HashMap<>();
    String s_fechaUno            = request.getParameter("s_fechaUno");
    String s_fechaDos            = request.getParameter("s_fechaDos");
    String s_estatus            = request.getParameter("s_estatus");
    if (s_estatus != null && !s_estatus.isEmpty()) filtros.put("s_estatus",s_estatus);
    
    if (s_fechaUno != null && s_fechaDos != null){ 
        filtros.put("s_fechaUno", s_fechaUno);
        filtros.put("s_fechaDos", s_fechaDos);
    }
    ArrayList<String> parametros = new ArrayList<>(); 
    ArrayList<NivelServicioDocumento> listado = nivelServicioBean.getMonitorNivelServicio(filtros);
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("saveComments")){
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();
    String procesoBitacora = request.getParameter("icProcesoDocto") != null ? request.getParameter("icProcesoDocto") : "";
    String estatus = request.getParameter("icEstatus") != null ? request.getParameter("icEstatus") : "";
    String comentario = request.getParameter("comentario") != null ? request.getParameter("comentario") : "";
    Integer icProcesoDocto = Integer.parseInt(procesoBitacora);
    Integer icEstatus = Integer.parseInt(estatus);
    exito = nivelServicioBean.guardarComentarioBitacoraProceso(icProcesoDocto, icEstatus, comentario, iNoUsuario);
    if (exito) { 
        jsonObj.put("msg","Se ha guardado la configuración correctamente.");
    }
    else{
        jsonObj.put("msg","Error al guardar la configuración");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();    
}
else if(informacion.equals("catEstatusBO")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado.add(new ElementoCatalogo(null,"Todos los estatus"));
    listado.add(new ElementoCatalogo("910","Con Retraso"));
    listado.add(new ElementoCatalogo("920","En tiempo"));

    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catTipoDocumento")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado = nivelServicioBean.getCatTipoDocumento();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catTipoMovimiento")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado = nivelServicioBean.getCatTipoMovimiento();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catPortafolio")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado = nivelServicioBean.getCatPortafolio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catProceso")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado = nivelServicioBean.getCatProceso();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catTipoDocumento")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado = nivelServicioBean.getCatTipoDocumento();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catProducto")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado = nivelServicioBean.getCatProducto();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catTipoDocumento")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado = nivelServicioBean.getCatTipoDocumento();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if(informacion.equals("catEtapaProceso")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado = nivelServicioBean.getCatTipoDocumento();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}else if(informacion.equals("catEjecutivos")){
    ArrayList<ElementoCatalogo>listado = new ArrayList<>();
    listado = nivelServicioBean.getCatEjecutivos();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listado);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("GenerarReporte")){
    Map<String,String> filtros = new HashMap<>(); 
    String tipoDocumento   = request.getParameter("tipoDocumento");
    String tipoMovimiento  = request.getParameter("tipoMovimiento");
    String portafolio      = request.getParameter("portafolio");
    String producto        = request.getParameter("producto");
    String estatus         = request.getParameter("estatus");
    String etapaProceso    = request.getParameter("etapaProceso");
    String ejecutivo       = request.getParameter("ejecutivo");
    String fechaSolicitud1 = request.getParameter("fechaSolicitud1");
    String fechaSolicitud2 = request.getParameter("fechaSolicitud2");
    String ultimoEstatus   = request.getParameter("ultimoEstatus");
    
    if (tipoDocumento  != null && !tipoDocumento.isEmpty()) {filtros.put("tipoDocumento", tipoDocumento  );}
    if (tipoMovimiento != null && !tipoMovimiento.isEmpty()) {filtros.put("tipoMovimiento", tipoMovimiento );}
    if (portafolio     != null && !portafolio.isEmpty()) {filtros.put("portafolio", portafolio     );}
    if (producto       != null && !producto.isEmpty()) {filtros.put("producto", producto       );}
    if (estatus        != null && !estatus.isEmpty()) {filtros.put("estatus", estatus        );}
    if (etapaProceso   != null && !etapaProceso.isEmpty()) {filtros.put("etapaProceso", etapaProceso   );}
    if (ejecutivo      != null && !ejecutivo.isEmpty()) {filtros.put("ejecutivo", ejecutivo      );}
    if (fechaSolicitud1!= null && !fechaSolicitud1.isEmpty()) {filtros.put("fechaSolicitud1", fechaSolicitud1);}
    if (fechaSolicitud2!= null && !fechaSolicitud2.isEmpty()) {filtros.put("fechaSolicitud2", fechaSolicitud2);}
    if (ultimoEstatus  != null && !ultimoEstatus.isEmpty()) {filtros.put("ultimoEstatus", ultimoEstatus  );}

    JSONObject jsonObj   =  new JSONObject();    
    String nombreArchivo = nivelServicioBean.getReporteMonitorNiveles( strDirectorioPublicacion, filtros, strDirectorioPublicacion);
    jsonObj.put("success", new Boolean(true)); 
    jsonObj.put("urlArchivo", strDirecVirtualTemp +nombreArchivo);		
    infoRegresar = jsonObj.toString();
}


//log.info("infoRegresar: "+infoRegresar);

%>
<%=infoRegresar%>