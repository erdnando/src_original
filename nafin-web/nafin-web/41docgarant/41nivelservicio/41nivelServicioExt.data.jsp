<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	com.nafin.docgarantias.*,
        com.nafin.docgarantias.nivelesdeservicio.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/41docgarant/41secsession.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar="";
NivelesServicio nivelesBean = ServiceLocator.getInstance().lookup("NivelesServicio",NivelesServicio.class);
if(informacion.equals("getConfiguracion")){
    ArrayList<NivelServicio> listaNS = nivelesBean.getNivelesServicio();
    JSONArray jsObjArray = new JSONArray();
    jsObjArray = JSONArray.fromObject(listaNS);
    infoRegresar = "{\"success\": true, \"total\": \""+ jsObjArray.size()+"\" , \"registros\": " + jsObjArray.toString() + "}";
}
else if (informacion.equals("guardarConfiguracion")){
    boolean exito = false;
    JSONObject jsonObj   =  new JSONObject();
    String sformulario = request.getParameter("configuracion") == null ? "" : request.getParameter("configuracion");
    JSONObject jsonObject = JSONObject.fromObject(sformulario);
    Map<String, String> mapConfiguracionNiveles = jsonObject;
    exito = nivelesBean.guardarNivelesServicio(mapConfiguracionNiveles);
    if (exito) { 
        jsonObj.put("msg","Se ha guardado la configuración correctamente.");
    }
    else{
        jsonObj.put("msg","Error al guardar la configuración");
    }
    jsonObj.put("success", exito); 
    infoRegresar = jsonObj.toString();
}
    
log.info("infoRegresar: "+infoRegresar);
%>

<%=infoRegresar%>