Ext.onReady(function() { 
	var cveEpoOriginal= Ext.getDom('hidCboEpo').value;
	var comboEpoVacio= false;
	var cveIFtmp;
	var validaCder = true;
	
	var strUsr = Ext.getDom('hidStrUsr').value;
	var strUsuario = Ext.getDom('hidStrUsuario').value;			
	
	var Inicializacion = {
		catalogoMoneda : false,
		catalogoEpo: false
	};
	
	var objCalc={
		totalDescuento:0,
		totalMonto:0,		
		totalIntereses:0,
		totalRecibir:0,
		totalRecibirBene:0,
		totalNetoPyme:0,
		totaldocs:0,
		esBancoDeFondeoBANCOMEXT: null,
		tipoCambio: null,
		montoLimiteUtilizadoPorIFI: null,
		montoLimiteMaximoPorIFI: null,
		montoLimiteUtilizadoPorEPO: null,
		montoLimiteMaximoPorEPO: null,
		montoLimiteUtilizadoPorPYME: null,
		montoLimiteMaximoPorPYME: null,
		hayMontoLimiteParametrizadoPorPYME: null,
		hayMontoLimiteParametrizadoPorIFI: null,
		montoLimiteComprometidoPorPYME: null,
		montoLimiteComprometidoPorEPO: null
	};
	
	var objGral={
		inicial:'S',
		strTipoUsuario:null,
		pymeBloq:null,
		tipo_servicio:null,
		iNoEPO:null,
		urlCapturaDatos:null,
		fecDiaSigHabil:null,
		msgDiaSigHabil:null,
		msgOperFact24hrs:null,
		msgFecVencPyme:null,		
		valorTC: null,
		paramEpoPef: null,
		sLimiteActivo: null,
		numCamposDinamicos: null,
		mapNombres:null,
		operaNotasDeCredito: null,
		aplicarNotasDeCreditoAVariosDoctos: null,
		icDoctosNotas: null,
		icNotasAp: null,
		bancoDeFondeo: null,
		limites: null,
		sTipoLimite: null,
		monto: null,
		descuento: null,
		interes: null,
		recibir: null,
		valEPO: null,
		valIF: null,
		sFecVencLineaCred: 'N',
		msgError: null,
		horarioValidoCons:true,
		leyendaLegal:null,
		consultaIni:'S',
		operaMontosMenores:null
	};

//--------------------------------------------------------------------------------

var fnTransmitirDerechoCallback = function(vpkcs7, vtextoFirmar, vokResp){
	if (Ext.isEmpty(vpkcs7)) {
		//btn.enable();
		Ext.getCmp('btnTransmitir').enable();
		return;	//Error en la firma. Termina...
	}else{
		
		realizaConfirmacionNAFIN(vokResp, vpkcs7, vtextoFirmar);
	}
}

var fnTransmitirDerecho = function(btn, certificado){
		
		if((strUsuario==='PYME' || strUsuario==='NAFIN') && certificado!==true && validaCder){
					
			var win = new NE.cesion.WinCesionDerechos({
				getResultValid:resulValidCesion,
				cvePerfProt: 'PPYMAUTOR',
				cveFacultad: '13PYME13AUTORIZA',
				strUsuario: strUsr,
				tipoUsuario: strUsuario
			}).show();
			win.el.dom.scrollIntoView();
			
			
		}else{
			btn.disable();
			var infoTotal = '';
			storeTotalData.each(function(record) {
				infoTotal=infoTotal+
							record.data['EPO']+'|'+
							record.data['TOTALMONTO']+'|'+
							record.data['TOTALDESCTO']+'|'+
							record.data['TOTALINTERES']+'|'+
							record.data['TOTALIMPORT']+'|'+
							record.data['TOTALIMPORTBENEF']+'|'+
							record.data['TOTALNETOPYME']+'|\n';
							
							
			});
		
			var textoFirmar = "E P O-DATOS|Total Monto Documento|Total Monto Descuento|Total Monto de Inter�s|Total Importe a Recibir\n"+ infoTotal +
			"Al solicitar el factoraje electr�nico o descuento electr�nico del documento que selecciono e identifico, transmito los derechos"+ 
			" que sobre el mismo ejerzo, de acuerdo con lo establecido por los art�culos 427 de la ley general de t�tulos y operaciones de cr�dito,"+ 
			" 32 c del c�digo fiscal de la federaci�n y 2038 del c�digo civil federal, haci�ndome sabedor de su contenido y alcance, condicionado a"+
			" que se efect�e el descuento electr�nico o el factoraje electr�nico";
			
			
			NE.util.obtenerPKCS7(fnTransmitirDerechoCallback, textoFirmar, 'S');
			
		}
		
	};

var procesarSuccessValoresIni = function(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {

		    var objMsg = Ext.getCmp('mensajeDis1');
			objMsg.body.update('Cesi�n de derechos sujeta a la verificaci�n por la Empresa de Primer Orden');
			objMsg.show();
					    
					    
			var resp = 	Ext.util.JSON.decode(response.responseText);
			validaCder = resp.validaCder;
			objGral.inicial = 'N';
			objGral.icgrupo = resp.icgrupo;
			objGral.strTipoUsuario = resp.strTipoUsuario;
			objGral.pymeBloq = resp.pymeBloq;
			objGral.tipo_servicio = resp.tipo_servicio;
			objGral.urlCapturaDatos = resp.urlCapturaDatos;
			objGral.fecDiaSigHabil = resp.fecDiaSigHabil;
			objGral.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGral.msgOperFact24hrs = resp.msgOperFact24hrs;
			objGral.msgFecVencPyme = resp.msgFecVencPyme;
			objGral.iNoEPO = resp.iNoEPO;
			objGral.leyendaLegal = resp.leyendaLegal;
			
			if( !resp.bloqueoPymeEpo ){
				fp.show();
				if(resp.msgError===''){
								
				if(resp.operaDistribuido!=='N'){
				
					var valorInicialCboEpo = Ext.getDom("hidCboEpo").value;
					var cboEpo = Ext.getCmp('cboEpo1');
	
					if(cboEpo.getValue()==='' && valorInicialCboEpo === ''){
						if (storeCatEpoData.findExact("clave", objGral.iNoEPO) !== -1 ) {
							cboEpo.setValue(objGral.iNoEPO);
						}
						Inicializacion.catalogoEpo = true;
					} else if (valorInicialCboEpo !== ''){
						if (storeCatEpoData.findExact("clave", valorInicialCboEpo) !== -1 ) {
							cboEpo.setValue(valorInicialCboEpo);
						}
						Inicializacion.catalogoEpo = true;
						if(NE.util.allTrue(Inicializacion)) {
							//Si cuando llega aqui ya estan inicializados los valores iniciales necesarios,
							//realiza la consulta. Esta secci�n solo aplica cuando la consulta recibe parametros iniciales
							consultar();
						}
					}
										
					if(resp.msgError!==''){						
						objMsg.body.update(resp.msgError);
						objMsg.show();
						fp.hide();
					}else if(objGral.msgDiaSigHabil!==''){							
						objMsg.body.update(objGral.msgDiaSigHabil);
						objMsg.show();
						fp.show();
						Ext.getCmp('cfFechaVenc').doLayout();
						
					}else{
						fp.show();
						Ext.getCmp('cfFechaVenc').doLayout();
					}
										  
					    Ext.getCmp('icBeneficiario1').show();
		    			    catalogoBeneficiarioData.load({
						params:{				
						    iNoEPO:objGral.iNoEPO
						}
					    });	 
					
					
				}else  {
					objMsg.body.update('<center>La Pyme no Opera Factoraje Distribuido.</center>');
					objMsg.show();
					fp.hide();
				}
					
				}else{
					
					if(resp.msgError!==''){
						objMsg.body.update(resp.msgError);
					}else{
						objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
								'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
								'Cd. De M�xico 50-89-61-07. <br>'+
								'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
					}
										
					objMsg.show();
					fp.hide();
				}
			
			}else{
				
				objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
							'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
							'Cd. De M�xico 50-89-61-07. <br>'+
							'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
				objMsg.show();
				fp.hide();
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	//FUNCION: despues de generar el combo IF, se encarga de realizar la consulta
	//de los documentos seleccionados en base a los criterios seleccionados
	var procesarConsulta = function(store, arrRegistros, opts) {
		
		var cboIf = Ext.getCmp('cboIf1');
		var panelInterm = Ext.getCmp('intermediariosS1');
		panelInterm.show();
		cboIf.show();
		
		if(store.getTotalCount()>0 &&  Ext.isEmpty(arrRegistros[0].data.loadMsg)){
			pnl.el.unmask();
			pnl.el.mask('Consultando...', 'x-mask-loading');
			
			var cboEpo = Ext.getCmp('cboEpo1');
			var record;
			var valEPO;
			var storeIf = cboIf.getStore();
			
			if(comboEpoVacio){
				valEPO = cveEpoOriginal+'|'+'SI ALGO MAS';
			}else{
				record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
				valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
			}
			
			objGral.valEPO = valEPO;
			
			var valIF='';
			if(cboIf.getValue()===''){	
				
				storeIf.each(function(record) {
					if(record.data['lsIf']===cveIFtmp){
						valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
					}
				});				
				
			}else{
				record = cboIf.findRecord(cboIf.valueField, cboIf.getValue());
				if(record){
					valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
				}else{
					record = storeIf.getAt(0);
					valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
					
						cboIf.setValue(record.data['lsIf']);
				}
			}
			
			objGral.valIF = valIF;
			
			Ext.Ajax.request({
				url: '13formapd4ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'GeneraInfoCons',
					cboEpo: objGral.valEPO,
					cboIf: objGral.valIF
				}),
				callback: procesarSuccessValoresGrales
			});
			objGral.consultaIni = 'N';
		}else{
			cboIf.clearValue();
			pnl.el.unmask();
		}
		
	};
	
	var procesarSuccessDatCboIf = function(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var objMsg = Ext.getCmp('mensajes1');
			var panelInterm = Ext.getCmp('intermediariosS1');
			var panelTotales = Ext.getCmp('panelTotales1');
				
			if(resp.bloqueoPymeEpo !=='B'){
			
				if(resp.horarioValido){					
					var cboIf = Ext.getCmp('cboIf1');
					
					cboIf.emptyText = resp.msgCboIf;
					cveIFtmp = resp.cveIFtmp;
					storeCatIfData.loadData(resp);
				}else{						
					if(objMsg)
						objMsg.hide();
					if(panelInterm)
						panelInterm.hide();
					if(panelTotales)
						panelTotales.hide();					
					
					objMsg.body.update("Servicio No Disponible por el momento. Intente mas tarde");
					objMsg.show();
					fp.hide();
					Ext.getCmp('mensajeDis1').hide();
					pnl.el.unmask();
				}
		
			}else  {
				
				objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
								'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
								'Cd. De M�xico 50-89-61-07. <br>'+
								'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
				objMsg.show();
				fp.hide();				
				panelInterm.hide();
				panelTotales.hide();
				pnl.el.unmask();			
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var consultar = function(boton, evento) {
		var cboEpo = Ext.getCmp('cboEpo1');
		var txtFechaVencDe = Ext.getCmp('txtFechaVencDe');
		var txtFechaVenca = Ext.getCmp('txtFechaVenca');
		var cboMoneda1 = Ext.getCmp('cboMoneda1');
		var record; 
		var valEPO; 
		Ext.getCmp('panelMsgMotosMenos').hide();	

		if(Ext.isEmpty(cboEpo.getValue())){
		    cboEpo.markInvalid('Este campo es obligatorio');
		    cboEpo.focus();
		    return;
		}
			
		if(Ext.isEmpty(cboMoneda1.getValue())){
		    cboMoneda1.markInvalid('Este campo es obligatorio');
		    cboMoneda1.focus();
		    return;
		}
			
		if(txtFechaVencDe.getValue()!=='' || txtFechaVenca.getValue()!==''){
			if(txtFechaVencDe.getValue()===''){
				txtFechaVencDe.markInvalid('Ingrese fecha inicial');
				return;
			}else if (txtFechaVenca.getValue()===''){
				txtFechaVenca.markInvalid('Ingrese fecha final');
				return;
			}
		}
		
		pnl.el.mask('Consultando...', 'x-mask-loading');
				
		if(comboEpoVacio){
			valEPO = cveEpoOriginal+'|'+'SI ALGO MAS';
		}else{
			record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());		
			valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
		}
		
		Ext.Ajax.request({
			url: '13formapd4ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'iniciaConsulta',
				cboEpo: valEPO,
				cvePyme: Ext.getDom("cvePyme").value,
				cveEpoOriginal: cveEpoOriginal
			}),
			callback: procesarSuccessDatCboIf
		});  
	};
	
	var procesarSuccessValoresGrales = function(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			if(resp.bloqueoPymeEpoIF==='S') {
				Ext.MessageBox.alert("Mensaje","Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado- IF. Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes Cd. De M�xico 50-89-61-07. Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)",
				function(){ 
					Ext.getCmp('cboIf1').setValue(''); 
				});
			} 		
			
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				var panelMsgMotosMenos = Ext.getCmp('panelMsgMotosMenos');
				var cboIf = Ext.getCmp('cboIf1');				
				if (cboIf.getValue() !== ''){
					objGral.operaMontosMenores = resp.operaMontosMenores;
					
					if(objGral.operaMontosMenores==='N' ) {
						contenedorPrincipalCmp.add(panelMsgMotosMenos);		
						panelMsgMotosMenos.show();
					}else  if(objGral.operaMontosMenores==='S' ) {							
							panelMsgMotosMenos.hide();
					}
					contenedorPrincipalCmp.doLayout();						
				}
			
			objGral.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGral.strTipoUsuario = resp.strTipoUsuario;
			objGral.pymeBloq = resp.pymeBloq;
			objGral.tipo_servicio = resp.tipo_servicio;
			objGral.urlCapturaDatos = resp.urlCapturaDatos;
			objGral.fecDiaSigHabil = resp.fecDiaSigHabil;
			objGral.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGral.msgOperFact24hrs = resp.msgOperFact24hrs;
			objGral.msgFecVencPyme = resp.msgFecVencPyme;
			objGral.msgError = resp.msgError;
			objGral.horarioValidoCons = resp.horarioValidoCons;	
			objGral.sFecVencLineaCred = resp.sFecVencLineaCred;
			
			objGral.valorTC = resp.valorTC;
			objGral.paramEpoPef = resp.paramEpoPef;
			objGral.sLimiteActivo = resp.sLimiteActivo;
			objGral.numCamposDinamicos = resp.numCamposDinamicos;
			objGral.mapNombres = resp.mapNombres;
			objGral.operaNotasDeCredito = resp.operaNotasDeCredito;
			objGral.aplicarNotasDeCreditoAVariosDoctos = resp.aplicarNotasDeCreditoAVariosDoctos;
			objGral.bancoDeFondeo = resp.bancoDeFondeo;
			objGral.limites = resp.limites;
			objGral.sTipoLimite = resp.sTipoLimite;
			objGral.noTasasAceptada = resp.noTasasAceptada;
			
			//se asignan datos al objeto creado para el calculo de montos
			objCalc.totalMonto = 0;
			objCalc.totalDescuento = 0;
			objCalc.totalIntereses = 0;
			objCalc.totalRecibir = 0;
                        objCalc.totalRecibirBene=0;
                        objCalc.totalNetoPyme=0;
			objCalc.totaldocs = 0;
			objCalc.esBancoDeFondeoBANCOMEXT = (objGral.bancoDeFondeo==="BANCOMEXT" )?true:false;
			objCalc.tipoCambio = parseFloat(objGral.valorTC,10);
			
			var cboIf = Ext.getCmp('cboIf1');
			var objMsg = Ext.getCmp('mensajes1');
			if(cboIf.getValue()!==''){
				if(objGral.msgDiaSigHabil===''){					
					objMsg.hide();
				}else{					
					objMsg.body.update(objGral.msgDiaSigHabil);
					objMsg.show();
				}
			}
			storeDoctosData.loadData(resp);

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var procesarConsultaData = function(store, arrRegistros, opts) {		
		pnl.el.unmask();		
			
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridColumnMod = grid.getColumnModel();
		var strTipoUsuario = objGral.strTipoUsuario;
		
		var numCamposDinamicos = objGral.numCamposDinamicos;		
		var cboIf = Ext.getCmp('cboIf1');
		panelTotales.getForm().reset();		
				
			grid.show();
			panelTotales.show();
			if (arrRegistros !== null) {
				if (!grid.isVisible()) {
					contenedorPrincipalCmp.add(NE.util.getEspaciador(20));
					contenedorPrincipalCmp.add(grid);
					contenedorPrincipalCmp.doLayout();
				}
				
				var el = grid.getGridEl();
				if(store.getTotalCount() > 0) {
					Ext.getCmp('btnTerminar').enable();
					
					
					if(strTipoUsuario==='PYME'){//2017_003
					    var indice = 12;
					    for(var i=12;i<16;i++){
						gridColumnMod.setHidden(i,true);
					    }
						
					    for(var x=0; x<numCamposDinamicos; x++){				
						var nombreColumna = eval("objGral.mapNombres.CAMPO"+x);								
						gridColumnMod.setColumnHeader(indice,nombreColumna);
						gridColumnMod.setHidden(indice,false);
						indice = indice+1;							
					    }					    
					    
					}
						
					
					
					if(objGral.msgError!==''  && cboIf.getValue() !== ''){
					    Ext.getCmp('btnTerminar').disable();
					    var objMsg = Ext.getCmp('mensajes1');
					    objMsg.body.update(objGral.msgError);
					    objMsg.show();
					
					    if(!objGral.horarioValidoCons){
						Ext.MessageBox.alert('Mensaje','Fuera del horario de servicio para el Intermediario Financiero seleccionado, favor de seleccionar otro');
					    }						
					}
					
					el.unmask();
				}else {
					Ext.getCmp('btnTerminar').disable();
					if(objGral.msgError!=='' && cboIf.getValue() !== ''){
					    Ext.getCmp('btnTerminar').disable();							
						if(!objGral.horarioValidoCons){
						    Ext.MessageBox.alert('Mensaje','Fuera del horario de servicio para el Intermediario Financiero seleccionado, favor de seleccionar otro');
						}
						
						el.mask(objGral.msgError, 'x-mask');
					}else{
						if(objGral.noTasasAceptada){
							el.mask('No existen documentos para los criterios establecidos.', 'x-mask');
						}else{
							el.mask('No se encontr� ning�n registro', 'x-mask');
						}
					}
				
			}
		}	
	};
	
	function calcularLimite(cboIF, tipoC, monto)	{
		
		var record = cboIF.findRecord(cboIF.valueField, cboIF.getValue());		
		
		var valLimite = parseFloat(record.get('lsLimiteMax'));
		var valUtilizado = parseFloat(record.get('lsLimiteUt'));
		var valMontoComprometido = parseFloat(record.get('lsMontoComprometido'));
		var valor = parseFloat(monto,10)  + valUtilizado + valMontoComprometido;   
                return (valor <= valLimite);        
	};
	
	var recalcular = function(selModel,record, rowIndex, isSelect){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var cboIf = Ext.getCmp('cboIf1');
		var cboMoneda = Ext.getCmp('cboMoneda1');
		var notascred	= record.data['SELECNOTAS'];
		var montoMini = record.data['SELEMONTOMINI'];//f.aplicado;
		
		if (cboIf.getValue() !== ''){
			if ( montoMini  ==='S' ){
			
			if (!panelTotales.isVisible()) {
				var recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());
				panelTotales.setTitle('TOTALES - '+recordMoneda.get(cboMoneda.displayField));
				contenedorPrincipalCmp.add(panelTotales);
				contenedorPrincipalCmp.doLayout();
			}
                        
			if (isSelect){ //true --> Suma
				var totalDescto = 0;
				
				if (cboIf.getValue() === ''){
					if(notascred!==""){ 
					    return false; 
					}
					Ext.MessageBox.alert('Aviso','Debes seleccionar un Intermediario');
					selModel.deselectRow(rowIndex);
					return 'e';
				}else if(objGral.msgOperFact24hrs!==''){
					Ext.MessageBox.alert('Aviso',objGral.msgOperFact24hrs);
					selModel.deselectRow(rowIndex);
					return false;
				}
				
				totalDescto = parseFloat(objCalc.totalDescuento) + parseFloat(record.data['MONTODESC']);
				   
				if (calcularLimite(cboIf,  objCalc.tipoCambio, totalDescto)) {
					
					objCalc.totalMonto 		= parseFloat(objCalc.totalMonto) + parseFloat(record.data['MONTO']);
					objCalc.totalDescuento 	= parseFloat(objCalc.totalDescuento) + parseFloat(record.data['MONTODESC']);
					objCalc.totalIntereses 	= parseFloat(objCalc.totalIntereses) + parseFloat(record.data['IMPORTINT']);
					objCalc.totalRecibir 	= parseFloat(objCalc.totalRecibir) + parseFloat(record.data['IMPORTREC']); 
					objCalc.totalRecibirBene 	= parseFloat(objCalc.totalRecibirBene) + parseFloat(record.data['IMPORTEBENEFIC']); //2017_003
					objCalc.totalNetoPyme = parseFloat(objCalc.totalNetoPyme) + parseFloat(record.data['NETORECIBPYME']); //2017_003
					
					objCalc.totaldocs 		= parseInt(objCalc.totaldocs) + 1;
					var recordIF = cboIf.findRecord(cboIf.valueField, cboIf.getValue());
					record.data['SELECHID']=record.data['SELECHID']+'|'+recordIF.data['rs_tipo_lim']; 
                                       
				} else {  
                                        var mensaje 	= "Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.\n\n"+
							"Por favor comuniquese al Centro de Atencion a Clientes al telefono 50-89-61-07 o\n"+
							"del interior al 01-800-NAFINSA (01-800-6234672).";
					Ext.MessageBox.alert('Aviso',mensaje);
                                        record.data['SELECCION']='N';                                       
                                        selModel.deselectRow(rowIndex);
                                        isSelect = false;
					return false;
				}				
			}else{
				objCalc.totalMonto 		= parseFloat(objCalc.totalMonto) - parseFloat(record.data['MONTO']);
				objCalc.totalDescuento 	= parseFloat(objCalc.totalDescuento) - parseFloat(record.data['MONTODESC']);
				objCalc.totalIntereses 	= parseFloat(objCalc.totalIntereses) - parseFloat(record.data['IMPORTINT']);
				objCalc.totalRecibir 	= parseFloat(objCalc.totalRecibir) - parseFloat(record.data['IMPORTREC']);
				objCalc.totalRecibirBene 	= parseFloat(objCalc.totalRecibirBene) - parseFloat(record.data['IMPORTEBENEFIC']); //2017_003
				objCalc.totalNetoPyme 	= parseFloat(objCalc.totalNetoPyme) - parseFloat(record.data['NETORECIBPYME']); //2017_003
				
				objCalc.totaldocs = parseInt(objCalc.totaldocs) - 1;  
          
			}
	
			Ext.getCmp('totalDoctosDesp1').setValue(objCalc.totaldocs);
			Ext.getCmp('totalMontoDesp1').setValue('$'+formatoFlotante(objCalc.totalMonto,"aplicar"));
			Ext.getCmp('totalMontoDescDesp1').setValue('$'+formatoFlotante(objCalc.totalDescuento,"aplicar"));
			Ext.getCmp('totalImpIntDesp1').setValue('$'+formatoFlotante(objCalc.totalIntereses,"aplicar"));
			Ext.getCmp('totalImpRecibDesp1').setValue('$'+formatoFlotante(objCalc.totalRecibir,"aplicar"));  
			Ext.getCmp('totalImpRecibBenef1').setValue('$'+formatoFlotante(objCalc.totalRecibirBene,"aplicar"));  //2017_003
			Ext.getCmp('totalNetoPyme1').setValue('$'+formatoFlotante(objCalc.totalNetoPyme,"aplicar"));  //2017_003			
			
			}
			
		}else{
			if (isSelect){ //true --> Suma				
				Ext.MessageBox.alert('Aviso','Debes seleccionar un Intermediario');
				selModel.deselectRow(rowIndex);
				isSelect = false;
				return false;
			}
		}
		return true;
		
	};
	
	var terminar = function() {
		var cboIf = Ext.getCmp('cboIf1');
		var cboEpo = Ext.getCmp('cboEpo1');
		
		if (cboEpo.getValue() === ''){
			Ext.MessageBox.alert('Aviso','Debes seleccionar una Cadena');
			return;
		}else if (cboIf.getValue() !== ''){
			if (objCalc.totalMonto === 0) {
				Ext.MessageBox.alert('Aviso','No ha seleccionado documentos');
			} else {                        
                            //CODIGO PARA PREACUSE
                            generaPreAcuse();                            
			}
                        
		}else{
			Ext.MessageBox.alert('Aviso','Debes seleccionar un Intermediario');
			return;
		}
	};
	
	
	//FUNCION: genera la pantalla de Preacuse manipulando los objetos involucrados en el pantalla
	// y generando el grid de preacuse para verificar que los documentos son los seleccionados
	var generaPreAcuse = function() {
		var store = grid.getStore();		
		var noRegistros = false;		
		var registrosPreAcu = [];

		//se recorre grid principal para detectar los documentos seleccionados
		store.each(function(record) {
			
			if(record.data['SELECCION']==='S' && record.data['SELEMONTOMINI']=='S'  ){ 
				registrosPreAcu.push(record);
				noRegistros = true;
			}
		});
		
		//si existen doctos seleccionados, entonces se muestra el grid de preacuse
		if(noRegistros){
			var gridColumnMod = gridPreAcu.getColumnModel();
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var strTipoUsuario = objGral.strTipoUsuario;			
			var numCamposDinamicos = objGral.numCamposDinamicos;	
			grid.stopEditing();
			
			var ftotales = Ext.getCmp('panelTotales1');
			var objTotales = ftotales.getForm().getValues();
			var vcboEpo = Ext.getCmp('cboEpo1');
			var vrecord = vcboEpo.findRecord(vcboEpo.valueField, vcboEpo.getValue());
			
			var dataTotales = [
					[vrecord.get(vcboEpo.displayField), objTotales.totalMontoDesp, objTotales.totalMontoDescDesp, objTotales.totalImpIntDesp, objTotales.totalImpRecibDesp, objTotales.totalImpRecibBenef  , objTotales.totalNetoPyme   ]
				];
			
			
			//se le carga informacion al grid de preacuse y al grid de totales
			storePreAcuData.add(registrosPreAcu);
			storeTotalData.loadData(dataTotales);
			
			//se valida si el grid de preacuse ya es visible
			if (!gridPreAcu.isVisible()) {
				contenedorPrincipalCmp.findById('forma').hide();
				contenedorPrincipalCmp.findById('intermediariosS1').hide();
				contenedorPrincipalCmp.findById('gridDoctos').hide();
				contenedorPrincipalCmp.findById('panelTotales1').hide();
				
				if(contenedorPrincipalCmp.findById('gridDoctosPreAcu')){
					contenedorPrincipalCmp.findById('gridDoctosPreAcu').show();
					contenedorPrincipalCmp.findById('gridTotales1').show();
					contenedorPrincipalCmp.findById('pnmsgLegal1').show();
				}else{
					contenedorPrincipalCmp.insert(7,gridPreAcu);
					contenedorPrincipalCmp.insert(8,NE.util.getEspaciador(10));					
					contenedorPrincipalCmp.insert(9,gridTotales);
					contenedorPrincipalCmp.insert(10,NE.util.getEspaciador(20));
					contenedorPrincipalCmp.add(panelMsgLegal);
				}
				
				contenedorPrincipalCmp.doLayout();
				var objMsg = Ext.getCmp('pnmsgLegal1');
				objMsg.body.update(objGral.leyendaLegal);
				
			}
			
			
			if(strTipoUsuario==='PYME'){//2017_003
			
			    gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FECEMISION'), false);
			    gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FECVENC'), false);
			    gridColumnMod.setHidden(gridColumnMod.findColumnIndex('PLAZO'), false);
			    gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TASA'), false);			    
			
			    var indice = 14;
			    for(var i=14;i<18;i++){
				gridColumnMod.setHidden(i,true);
			    }
			    for(var x=0; x<numCamposDinamicos; x++){				
				var nombreColumna = eval("objGral.mapNombres.CAMPO"+x);								
				gridColumnMod.setColumnHeader(indice,nombreColumna);
				gridColumnMod.setHidden(indice,false);
				indice = indice+1;							
			    }
			    Ext.getCmp('mensajeDis1').hide();			   
			    var objMsgD = Ext.getCmp('mensajeDistribuido1');
			    objMsgD.body.update('Cesi�n de derechos sujeta a la verificaci�n por la Empresa de Primer Orden.');
			     objMsgD.show();
			    
			}			
		}
		
	}; //fin function generaPreAcuse
	
	
	//FUNCION:retorna de la pabtalla de Pre Acuse a la parte de seleccion de documentos 
	//para poder verificar la seleccion que se realizo
	var regresaAseleccion = function(){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		contenedorPrincipalCmp.findById('forma').show();
		contenedorPrincipalCmp.findById('intermediariosS1').show();
		contenedorPrincipalCmp.findById('gridDoctos').show();
		contenedorPrincipalCmp.findById('panelTotales1').show();
		
		contenedorPrincipalCmp.findById('gridDoctosPreAcu').hide();
		contenedorPrincipalCmp.findById('gridTotales1').hide();
		contenedorPrincipalCmp.findById('gridDoctosPreAcu').getStore().removeAll(true);
		contenedorPrincipalCmp.findById('gridTotales1').getStore().removeAll(true);
		
		contenedorPrincipalCmp.doLayout();
		
		var objMsg = Ext.getCmp('mensajeDis1');
		objMsg.body.update('Cesi�n de derechos sujeta a la verificaci�n por la Empresa de Primer Orden');
		objMsg.show();		
		Ext.getCmp('mensajeDistribuido1').hide();
					
	};
	
	var realizaConfirmacionNAFIN = function(okResp, pkcs7, textoFirmar){
	
		pnl.el.unmask();
		pnl.el.mask('Procesando...', 'x-mask-loading');


		var registrosEnviar = [];
		if(okResp==='S'){
			
			
			Ext.Ajax.request({
				url: '13formapd4ext.data.jsp',
				params: {
					informacion: 'validaHorario',
					cboIf: objGral.valIF,
					cboEpo: objGral.valEPO
				},
				callback: function(opts, success, response) {
					if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
						var resp = 	Ext.util.JSON.decode(response.responseText);
						
						if(resp.msgError!==''){
							Ext.MessageBox.alert('Mensaje',resp.msgError);
							Ext.getCmp('btnTransmitir').enable();
						}else{
							if(resp.msgDiaSigHabil!==''){
								Ext.Msg.confirm('Confirmaci�n', resp.msgDiaSigHabil, function(btn){
									if(btn==='yes'){
										if(resp.msgOperFact24hrs!==''){
											Ext.MessageBox.alert('Mensaje',resp.msgOperFact24hrs);
											Ext.getCmp('btnTransmitir').enable();
										}else{
											storePreAcuData.each(function(record){
												registrosEnviar.push(record.data);
											});
											
											Ext.getCmp('btnTransmitir').disable();
											Ext.Ajax.request({
												url: '13formapd4ext.data.jsp',
												params: {
													informacion: 'confirmaClaveCesion',
													registros: Ext.encode(registrosEnviar),													
													totalMonto: objCalc.totalMonto,
													totalDescuento: objCalc.totalDescuento,
													totalIntereses: objCalc.totalIntereses,
													totalRecibir: objCalc.totalRecibir,
													totalRecibirBene: objCalc.totalRecibirBene, //2017_003
													totalNetoPyme:objCalc.totalNetoPyme,  //2017_003
													sTipoLimite: objGral.sTipoLimite,
													svalorTC: objGral.valorTC,
													cboIf: objGral.valIF,
													cboEpo: objGral.valEPO,
													cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
													valCve:okResp,
													correo:'',
													email: '',
													Pkcs7: pkcs7,
													TextoFirmado: textoFirmar,
													strUsr:strUsr
												},
												callback: procesarSuccessConfirmaCesion
											});  
										}
									}else{
										Ext.getCmp('btnTransmitir').enable();
									}
								});
							}else{
							
								storePreAcuData.each(function(record){
									registrosEnviar.push(record.data);
								});
								
								Ext.getCmp('btnTransmitir').disable();
								
								Ext.Ajax.request({
									url: '13formapd4ext.data.jsp',
									params: {
										informacion: 'confirmaClaveCesion',
										registros: Ext.encode(registrosEnviar),										
										totalMonto: objCalc.totalMonto,
										totalDescuento: objCalc.totalDescuento,
										totalIntereses: objCalc.totalIntereses,
										totalRecibir: objCalc.totalRecibir,
										totalRecibirBene: objCalc.totalRecibirBene, //2017_003
										totalNetoPyme:objCalc.totalNetoPyme, //2017_003
										sTipoLimite: objGral.sTipoLimite,
										svalorTC: objGral.valorTC,
										cboIf: objGral.valIF,
										cboEpo: objGral.valEPO,
										cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
										valCve:okResp,
										strUsr:strUsr,
										correo:'',
										email: '',
										Pkcs7: pkcs7,
										TextoFirmado: textoFirmar
									},
									callback: procesarSuccessConfirmaCesion
								});  
							}
						}
					} else {
						NE.util.mostrarConnError(response,opts);
					}
				}
			});
		}
	};
	
	var resulValidCesion= function(okResp, siCorreo, email, errorMessage, vTipoUsuario){
		var registrosEnviar = [];
		
		pnl.el.unmask();
		pnl.el.mask('Procesando...', 'x-mask-loading');
		
		if(okResp==='S'){
			if(vTipoUsuario!=='NAFIN'){
				Ext.Ajax.request({
					url: '13forma1ext.data.jsp',
					params: {
						informacion: 'validaHorario',
						cboIf: objGral.valIF,
						cboEpo: objGral.valEPO
					},
					callback: function(opts, success, response) {
						if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
							var resp = 	Ext.util.JSON.decode(response.responseText);
							
							if(resp.msgError!==''){
								Ext.MessageBox.alert('Mensaje',resp.msgError);
							}else{
								if(resp.msgDiaSigHabil!==''){
									Ext.Msg.confirm('Confirmaci�n', resp.msgDiaSigHabil, function(btn){
										if(btn==='yes'){
											if(resp.msgOperFact24hrs!==''){
												Ext.MessageBox.alert('Mensaje',resp.msgOperFact24hrs);
											}else{
												storePreAcuData.each(function(record){
													registrosEnviar.push(record.data);
												});
												
												Ext.getCmp('btnTransmitir').disable();
												
												Ext.Ajax.request({
													url: '13formapd4ext.data.jsp',
													params: {
														informacion: 'confirmaClaveCesion',
														registros: Ext.encode(registrosEnviar),														
														totalMonto: objCalc.totalMonto,
														totalDescuento: objCalc.totalDescuento,
														totalIntereses: objCalc.totalIntereses,
														totalRecibir: objCalc.totalRecibir,
														totalRecibirBene: objCalc.totalRecibirBene, //2017_003
														totalNetoPyme:objCalc.totalNetoPyme, //2017_003
														sTipoLimite: objGral.sTipoLimite,
														svalorTC: objGral.valorTC,
														cboIf: objGral.valIF,
														cboEpo: objGral.valEPO,
														cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
														valCve:okResp,
														correo:siCorreo,
														email: email
													},
													callback: procesarSuccessConfirmaCesion
												});
											}
										}
									});
									
								}else{
									storePreAcuData.each(function(record){
										registrosEnviar.push(record.data);
									});
									
									Ext.getCmp('btnTransmitir').disable();
									Ext.Ajax.request({
										url: '13formapd4ext.data.jsp',
										params: {
											informacion: 'confirmaClaveCesion',
											registros: Ext.encode(registrosEnviar),											
											totalMonto: objCalc.totalMonto,
											totalDescuento: objCalc.totalDescuento,
											totalIntereses: objCalc.totalIntereses,
											totalRecibir: objCalc.totalRecibir,
											totalRecibirBene: objCalc.totalRecibirBene, //2017_003
											totalNetoPyme:objCalc.totalNetoPyme, //2017_003
											sTipoLimite: objGral.sTipoLimite,
											svalorTC: objGral.valorTC,
											cboIf: objGral.valIF,
											cboEpo: objGral.valEPO,
											cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
											valCve:okResp,
											correo:siCorreo,
											email: email
										},
										callback: procesarSuccessConfirmaCesion
									});
								}
							}
						} else {
							NE.util.mostrarConnError(response,opts);
						}
					}
				});
			}else{
				fnTransmitirDerecho(Ext.getCmp('btnTransmitir'), true);
			}
		}if(okResp==='R'){
			Ext.Msg.alert('Aviso', 'El folio de seguridad se envi� exitosamente al Sujeto de Apoyo', function(btn){			
			    window.location.href='13forma4ext.jsp?strUsr='+strUsr;
			});
		}
	};

	var procesarSuccessConfirmaCesion = function(opts, success, response) {
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			
			if(resp.msgError==='' && resp.esCveCorrecta){
				
				var acuseCifras = [
					['N�mero de Acuse', resp.objCifras.numAcuse],
					['Fecha de Carga', resp.objCifras.fecCarga],
					['Hora de Carga', resp.objCifras.horaCarga],
					['Usuario de Captura', resp.objCifras.captUser]
				];
				
				storeCifrasData.loadData(acuseCifras);
				
				//se muestran componentes para el acuse de la seleccion de doctos				
				
				if(strUsuario==='NAFIN'){
					panelMsgAcuse.html='<p align="center"><b>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br>Recibo: '+resp._acuse+'</b></p>';
					contenedorPrincipalCmp.insert(1,panelMsgAcuse);
				}
				contenedorPrincipalCmp.findById('gridDoctosPreAcu').setTitle('Acuse - Selecci�n Documentos');
				contenedorPrincipalCmp.remove(panelMsgLegal);
				
				contenedorPrincipalCmp.insert(2,NE.util.getEspaciador(20));
				contenedorPrincipalCmp.insert(3,gridCifrasCtrl);
				contenedorPrincipalCmp.insert(4,NE.util.getEspaciador(20));
				contenedorPrincipalCmp.insert(5, panelMsgCifrado);
				contenedorPrincipalCmp.insert(6, NE.util.getEspaciador(10));
				contenedorPrincipalCmp.doLayout();
				//se muestran botones para salir o generar acuse en pdf
				Ext.getCmp('btnRevisar').hide();
				Ext.getCmp('btnAbrirPDF').show();
				Ext.getCmp('btnTransmitir').hide();
				Ext.getCmp('btnSalir').show();
				Ext.getCmp('btnCancelar').hide();
				
				Ext.getCmp('btnAbrirPDF').setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = resp.urlArchivo;
					forma.submit();
				});

			}else if(resp.msgError!=='' && !resp.esCveCorrecta){
				Ext.MessageBox.alert('Aviso',resp.msgError);
			}
			
			pnl.el.unmask();
			

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

//------------------------------------------------------------------------------
	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'catalogoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13formapd4ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, option){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						if(objGral.inicial==='S'){
							iniRequest(store);							
						}
					}else if(store.getTotalCount()<1 ){
						cveEpoOriginal = Ext.getDom("hidCboEpo").value;	
                                                fp.show();                                                
                                                Ext.getCmp('cfFechaVenc').show();
                                                Ext.getCmp('icBeneficiario1').show();
                                                var objMsg = Ext.getCmp('mensajeDis1');
                                                objMsg.body.update('Cesi�n de derechos sujeta a la verificaci�n por la Empresa de Primer Orden');
                                                objMsg.show();
                                                
					}
			}
		}
	});
	
	var storeCatMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13formapd4ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store, records, oprion){
				if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
					var valorInicialCboMoneda = Ext.getDom("hidCboMoneda").value;
					var cboMoneda = Ext.getCmp('cboMoneda1');
					if(cboMoneda.getValue()==='' && valorInicialCboMoneda === ''){
						cboMoneda.setValue(records[0].data['clave']);
						Inicializacion.catalogoMoneda = true;
					} else if (valorInicialCboMoneda !== '	') {
						cboMoneda.setValue(valorInicialCboMoneda);
						Inicializacion.catalogoMoneda = true;
						if(NE.util.allTrue(Inicializacion)) {
							//Si cuando llega aqui ya estan inicializados los valores iniciales necesarios,
							//realiza la consulta. Esta secci�n solo aplica cuando la consulta recibe parametros iniciales
							consultar();							
						}
					}
				}
			}
		}
	});
	
	var storeCatIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['lsIf', 'lsNombreCuenta', 'lsTipoPiso','lsLimiteUt', 'lsLimiteMax', 'rs_tipo_lim', 'lsMontoComprometido', 'loadMsg'],		
		totalProperty : 'totalCount',
		autoLoad: false,
		listeners: {
			load: procesarConsulta,
			exception: NE.util.mostrarDataProxyError			
		}
	});
	
	var storeDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		fields: [
			{name: 'NUMDOCTO'},
			{name: 'FECEMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECVENC',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO'},
			{name: 'TASA'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORCDESC', type: 'float'},
			{name: 'MONTODESC', type: 'float'},
			{name: 'SELECCION'},  //--   aqui va el check de seleccion
			{name: 'SELECBOOL'},
			{name: 'SELECHID'},
			{name: 'APLICADO'},
			{name: 'IMPORTINT', type: 'float'},
			{name: 'NETORECIBPYME', type: 'float'},
			{name: 'IMPORTREC', type: 'float'},
			{name: 'BENEFICIARIO'},
			{name: 'PORCBENEFIC'},
			{name: 'IMPORTEBENEFIC'},
			{name: 'FLAGAUTCTASBANC'},
			{name: 'ENTIDAD_GOBIERNO'},			
			{name: 'CAMPO0'},  //2017_003
			{name: 'CAMPO1'}, //2017_003
			{name: 'CAMPO2'}, //2017_003
			{name: 'CAMPO3'},   //2017_003
			{name: 'CAMPO4'},  //2017_003
			{name: 'AUTOBENEIF'},  //2017_003
			{name: 'NUM_CONTRATO'},  //2018_02
			{name: 'CTA_AUTOIF'}, //2018_02
			{name: 'CONTRATO_AUTOIF'},//2018_02	
			{name: 'SELEMONTOMINI'} 
			
		    ],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});
	
	var storePreAcuData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMDOCTO'},
			{name: 'FECEMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECVENC',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO'},
			{name: 'TASA'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORCDESC', type: 'float'},
			{name: 'MONTODESC', type: 'float'},
			{name: 'SELECCION'},  //--   aqui va el check de seleccion
			{name: 'SELECBOOL'},
			{name: 'SELECHID'},
			{name: 'APLICADO'},
			{name: 'IMPORTINT', type: 'float'},
			{name: 'NETORECIBPYME', type: 'float'},
			{name: 'IMPORTREC', type: 'float'},
			{name: 'BENEFICIARIO'},
			{name: 'PORCBENEFIC'},
			{name: 'IMPORTEBENEFIC'},
			{name: 'FLAGAUTCTASBANC'},
			{name: 'ENTIDAD_GOBIERNO'},			
			{name: 'CAMPO0'}, //2017_003
			{name: 'CAMPO1'}, //2017_003
			{name: 'CAMPO2'},  //2017_003
			{name: 'CAMPO3'},  //2017_003
			{name: 'CAMPO4'}, //2017_003
			{name: 'AUTOBENEIF'}  //2017_003
		]
	});
	
	var storeTotalData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'EPO'},
			  {name: 'TOTALMONTO', type: 'float'},
			  {name: 'TOTALDESCTO', type: 'float'},
			  {name: 'TOTALINTERES', type: 'float'},
			  {name: 'TOTALIMPORT', type: 'float'},
			  {name: 'TOTALIMPORTBENEF', type: 'float'}, //2017_003
			  {name: 'TOTALNETOPYME', type: 'float'} //2017_003
			  
		  ]
	 });
	 
	 var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });


	var catalogoBeneficiarioData = new Ext.data.JsonStore({
		id: 'catalogoBeneficiarioDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13formapd4ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBeneficiario'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
//------------------------------------------------------------------------------	
	var elementosForma = [
		{
			xtype: 'displayfield',
			fieldLabel: 'No. Nafin Electr�nico Pyme',
			value: strUsuario==='NAFIN'?(Ext.getDom("strNePymeAsigna").value+' '+Ext.getDom("strNombrePymeAsigna").value):'',
			hidden: strUsuario==='NAFIN'?false:true
		},
		{
			xtype: 'combo',
			name: 'cboEpo',
			id: 'cboEpo1',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEpo',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEpoData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
			    select: function(combo, record, index){			    
					
				    Ext.getCmp('icBeneficiario1').reset();
				    catalogoBeneficiarioData.load({
					params:{				
					    iNoEPO:Ext.getCmp('cboEpo1').getValue()
					}
				    });		    
							
			    }
			}
		    },
		    {
			xtype: 'combo',
			name: 'cboMoneda',
			id: 'cboMoneda1',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMoneda',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'icBeneficiario',
			id: 'icBeneficiario1',
			fieldLabel: 'Nombre del Beneficiario',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'icBeneficiario',
			emptyText: 'Seleccione...',
			allowBlank: true,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			store : catalogoBeneficiarioData,
			tpl : NE.util.templateMensajeCargaCombo
		},	
		
		{
			xtype: 'compositefield',
			id: 'cfFechaVenc',
			fieldLabel: 'Fecha Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txtFechaVencDe',
					id: 'txtFechaVencDe',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaVenca',
					margins: '0 20 0 0',
					value: Ext.getDom('hidTxtFechaVencDe').value
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtFechaVenca',
					id: 'txtFechaVenca',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaVencDe',
					margins: '0 20 0 0',
					value: Ext.getDom('hidTxtFechaVenca').value
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	]; 
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		//hidden:true,
		style: 'margin:0 auto;',
		title: 'Selecci�n Documentos',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				id:'btnConsultar',
				formBind: true,
				handler: consultar
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {				    
				    Ext.getCmp('forma').getForm().reset();	
					
					
				}
			}
		]
	});	
	
	
	var selectModel = new Ext.grid.CheckboxSelectionModel({
        checkOnly: true,
		  renderer: function(v, p, record){
				if (record.data['SELECBOOL']){
					if(objGral.operaMontosMenores=='N' && parseFloat(record.data['IMPORTINT'])<0.01 ){
						record.data['SELEMONTOMINI']='N';
						return '<div>&#160;</div>';
					}else  {
						return '<div class="x-grid3-row-checker">&#160;</div>';
					}
				}else{
					return '<div>&#160;</div>';
				}
			},
		  listeners: {          
                        rowselect: function(selectModel, rowIndex, record) {
                          if(record.data['SELECCION']!=='S'   && recalcular(selectModel, record, rowIndex, true)===true){
										record.data['SELECCION']='S';
                              record.commit();
									}
                        },
                        //Para quitar lo selecccionado en el documentos ******************************************
                        rowdeselect: function(selectModel, rowIndex, record) {  
                            var cboIf = Ext.getCmp('cboIf1');
                                                         
                            if (cboIf.getValue() !== ''){
                                if(record.data['SELECBOOL'] && record.data['SELECCION']==='S'){   
                                    
                                  if(recalcular(selectModel, record, rowIndex, false)){ 
                                    record.data['SELECCION']='N';   
                                    record.commit();
                                    }
                                        
                                }else  {
                                    if(record.data['SELECCION']!=='N' && recalcular(selectModel, record, rowIndex, false)){ 
													record.data['SELECCION']='N';   
                                       record.commit();                                        
                                    }
                                }                             
                            }
                        },
                        //Para selecccionar los documentos ******************************************
                        beforerowselect: function( selectModel, rowIndex, keepExisting, record ){                              
                                
                                    var cboIf = Ext.getCmp('cboIf1');
                                    if (cboIf.getValue() !== ''){
					var mensaje = '';                                        
                  					
					if(record.data['SELECBOOL']    ){
						if( record.data['SELEMONTOMINI']==='S'   ){
							if(objGral.sLimiteActivo==='N'){
								mensaje 	=	"Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.\n\n"+
									"Por favor comuniquese al Centro de Atencion a Clientes al telefono 50-89-61-07 o\n"+
									"del interior al 01-800-NAFINSA (01-800-6234672).";
								
							}else if(objGral.msgOperFact24hrs!==''){
								mensaje = objGral.msgOperFact24hrs;
							}else if(record.data['FLAGAUTCTASBANC']==='N'){
								mensaje = "Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.C�digo 20.1<br>"+
											 "Por favor comun�quese al Centro de Atenci�n a Clientes al tel�fono 50-89-61-07 o del interior al 01-800-NAFINSA (01-800-6234672).";	
							}else  if(record.data['ENTIDAD_GOBIERNO']==='S'){
								mensaje  = 'No se puede operar el documento por que el Proveedor es Entidad de Gobierno';
												  
							}else  if(record.data['CTA_AUTOIF']==='N'){ //2018_02
								mensaje  = 'No se puede realizar el descuento del documento, ya que la cuenta bancaria del beneficiario no se encuentra Autorizada por el Intermediario Financiero.';
													
							}else  if(record.data['NUM_CONTRATO']===''){ //2018_02
								mensaje  = 'El documento no est� asociado a un Contrato. Favor de verificarlo.';
													
							}else  if(record.data['CONTRATO_AUTOIF']==='N'){  //2018_02
								mensaje  = 'No se puede realizar el descuento ya que el n�mero de contrato no ha sido autorizado por el Intermediario Financiero.';
							}
						}
					}else{
						mensaje  = 'No es posible seleccionar el documento';
					}					 
					
					if(mensaje!==''){
						Ext.MessageBox.alert('Aviso',mensaje);
						return false;
					}
                                        
                                    }else  {
                                      Ext.MessageBox.alert('Aviso','Debes seleccionar un Intermediario');  
                                      return false;
                                    }
				}
        }
    });



	var grid = new Ext.grid.EditorGridPanel({
	id: 'gridDoctos',
	store: storeDoctosData,
	margins: '20 0 0 0',
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }	,
	  getRowClass: function (record, rowIndex, rowParams, store) {
		if(record.get('SELEMONTOMINI') === 'N'){				
			return 'user-mci';
		}
	  }
   },
	sm: selectModel,
	columns: [
		selectModel,		
		{//1
			header: 'N�mero  <BR> Documento',
			tooltip: 'N�mero Documento',
			dataIndex: 'NUMDOCTO',
			sortable: true,
			width: 100,
			resizable: true,
			hidden: false
		},
		{//2
			header: 'Fecha  <BR> de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex: 'FECEMISION',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//3
			header: 'Fecha  <BR> de Vencimiento',
			tooltip: 'Fecha de Vencimiento',
			dataIndex: 'FECVENC',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//4
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO',
			sortable : true,
			width : 60,
			align: 'center'
		},
		{//5
			header : 'Tasa a Aplicar',
			tooltip: 'Tasa',
			dataIndex : 'TASA',
			width : 100,
			sortable : true,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00000%')
		},
		{//6
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			sortable : true
		},
		{//7
			header : '<center>Monto</center>',
			tooltip: 'Monto',
			dataIndex : 'MONTO',
			width : 80,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//8
			header : 'Porcentaje  <BR>de Descuento',
			tooltip: 'Porcentaje de Descuento',
			dataIndex : 'PORCDESC',
			sortable : true,
			align: 'center',
			width : 80,
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		},
		{//9
			header : '<center>Monto  <BR>a Descontar</center>',
			tooltip: 'Monto a Descontar',
			dataIndex : 'MONTODESC',
			sortable : true,
			width : 80,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//10
			header : '<center>Importe de <BR> Intereses</center>',
			tooltip: 'Importe de Intereses',
			dataIndex : 'IMPORTINT',
			width : 80,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : '<center>Importe <BR> a Recibir</center>',
			tooltip: 'Importe a Recibir',
			dataIndex : 'IMPORTREC',
			width : 80,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},		
		{//12 //2017_003
		    header : 'Campo1',
		    tooltip: 'Campo1',
		    dataIndex : 'CAMPO0',
		    width : 80,
		    sortable : true,
		    hidden: true,
		    align: 'center'			
		},
		{//13 //2017_003
		    header : 'Campo2',
		    tooltip: 'Campo2',
		    hidden: true,
		    dataIndex : 'CAMPO1',
		    width : 80,
		    sortable : true,
		    align: 'center'			
		},
		{//14 //2017_003
		    header : 'Campo3',
		    tooltip: 'Campo3',
		    hidden: true,
		    dataIndex : 'CAMPO2',
		    width : 80,
		    sortable : true,
		    align: 'center'			
		},
		{//15 //2017_003
		    header : 'Campo4',
		    tooltip: 'Campo4',
		    dataIndex : 'CAMPO3',
		    width : 80,
		    sortable : true,
		    hidden: true,
		    align: 'center'			
		},
		{//16 //2017_003
		    header : 'Campo5',
		    tooltip: 'Campo5',
		    dataIndex : 'CAMPO4',
		    width : 80,
		    sortable : true,
		    hidden: true,
		    align: 'center'			
		},		
		{//17
			header : 'Beneficiario',
			tooltip: 'Beneficiario',
			dataIndex : 'BENEFICIARIO',
			sortable : true,
			width : 100,
			align: 'left'
		},
		{//18
			header : '<center>% Beneficiario</center>',
			tooltip: '% Beneficiario',
			dataIndex : 'PORCBENEFIC',
			sortable : true,
			width : 80,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		},
		{//19
			header : 'Importe <BR> a Recibir Beneficiario',
			tooltip: 'Importe a Recibir Beneficiario',
			dataIndex : 'IMPORTEBENEFIC',
			width : 120,
			sortable : true,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//20
			header : '<center>Neto  <BR> a Recibir Pyme</center>',
			tooltip: 'Neto a Recibir Pyme',
			dataIndex : 'NETORECIBPYME',
			width : 100,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 900,
	style: 'margin:0 auto;',
	title: '',
	frame: true,	
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Terminar',
			id: 'btnTerminar',
			handler: terminar
			}
		]
	}
	});
	
	var panelTotales = new Ext.form.FormPanel({    
		name: 'panelTotales',
		id: 'panelTotales1',
		title:'-',
		width: 900,
		height: 90,
		style: 'margin:0 auto;',
		frame: true,
		layout:'absolute',
		defaultType: 'textfield',
      labelWidth: 0,  
		items:[
			{   
			  xtype:'label',   
			  y:5,    
			  x:12,   
			  text:'Total Documentos',   
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:132,   
			  text:'Monto',   
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:252,   
			  text:'Monto a Descontar',   
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:372,   
			  text:'Importe de Inter�s',   
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:492,   
			  text:'Importe a Recibir',   
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:612,   
			  text:'Imp a Recibir Beneficiario',    //2017_003
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:762,   
			  text:'Neto a Recibir Pyme',    //2017_003
			  style:'color:#777777'  
			}, 
			
			//************
			{   
			  name: 'totalDoctosDesp', // campo ID   
			  id:'totalDoctosDesp1',
			  x:12,   
			  y:25,   
			  style:'padding:4px 3px;',   
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			{   
			  name: 'totalMontoDesp', // campo ID   
			  id:'totalMontoDesp1',
			  x:132,   
			  y:25,   
			  style:'padding:4px 3px;',   
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {   
			  name: 'totalMontoDescDesp', // campo ID   
			  id:'totalMontoDescDesp1',
			  x:252,   
			  y:25,   
			  style:'padding:4px 3px;',   
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {   
			  name: 'totalImpIntDesp', // campo ID   
			  id:'totalImpIntDesp1',
			  x:372,   
			  y:25,   
			  style:'padding:4px 3px;',   
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {   
			  name: 'totalImpRecibDesp', // campo ID   
			  id:'totalImpRecibDesp1',
			  x:492,   
			  y:25,   
			  style:'padding:4px 3px;',      
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {   
			  name: 'totalImpRecibBenef', // 2017_003 
			  id:'totalImpRecibBenef1',
			  x:612,   
			  y:25,   
			  style:'padding:4px 3px;',      
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {   
			  name: 'totalNetoPyme', // 2017_003 
			  id:'totalNetoPyme1',
			  x:762,   
			  y:25,   
			  style:'padding:4px 3px;',      
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 }
		]
	});
	
	var gridPreAcu = new Ext.grid.GridPanel({
	id: 'gridDoctosPreAcu',
	store: storePreAcuData,
	margins: '20 0 0 0',
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [	
		{//1
			header: 'IF Seleccionado',
			tooltip: 'IF Seleccionado',
			dataIndex: 'IFSELECT',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false,
			align: 'left',
			renderer:  function (causa, columna, registro){
						var cboIFs = Ext.getCmp('cboIf1');
						var recordIF = cboIFs.findRecord(cboIFs.valueField, cboIFs.getValue());
						causa = recordIF.get(cboIFs.displayField);
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
		},
		{//2
			header: 'EPO',
			tooltip: 'EPO',
			dataIndex: 'EPOSELECT',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left',
			renderer:  function (causa, columna, registro){
						var cboEPO = Ext.getCmp('cboEpo1');
						var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
						causa = recordEPO.get(cboEPO.displayField);
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
			
		},
		{//3 -- se queda pendiente para la parte NAFIN
			header: 'PYME',
			tooltip: 'PYME',
			dataIndex: 'PYMESELECT',
			sortable: true,
			width: 150,
			resizable: true,
			align: 'left',
			//hidden: strUsuario=='NAFIN'?false:true, 
			hidden: true,
			renderer:  function (causa, columna, registro){
						if(strUsuario==='NAFIN'){
							causa = Ext.getDom("strNombrePymeAsigna").value;
						}
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
		},
		{//4
			header: 'N�mero <br>de Documento',
			tooltip: 'N�mero de Documento',
			dataIndex: 'NUMDOCTO',
			sortable: true,
			hideable: false,
			width: 100,			
			align: 'center'
		},
		
		{//5
			header: 'Fecha  <BR> de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex: 'FECEMISION',
			hidden: true,
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//6
			header: 'Fecha  <BR> de Vencimiento',
			tooltip: 'Fecha de Vencimiento',
			dataIndex: 'FECVENC',
			hidden: true,
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//7
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO',
			hidden: true,
			sortable : true,
			width : 60,
			align: 'center'
		},
		{//8
			header : 'Tasa a Aplicar',
			tooltip: 'Tasa',
			dataIndex : 'TASA',
			hidden: true,
			width : 100,
			sortable : true,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00000%')
		},
		{//9
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 100,
			sortable : true
		},
		{//10
			header : '<center>Monto</center>',
			tooltip: 'Monto',
			dataIndex : 'MONTO',
			width : 100,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : 'Porcentaje <BR>de Descuento',
			tooltip: 'Porcentaje de Descuento',
			dataIndex : 'PORCDESC',
			sortable : true,
			width : 100,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		},
		{//12
			header : '<center>Monto <BR> a Descontar</center>',
			tooltip: 'Monto a Descontar',
			dataIndex : 'MONTODESC',
			sortable : true,
			width : 100,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//13
			header : '<center>Importe Intereses</center>',
			tooltip: 'Importe de Intereses',
			dataIndex : 'IMPORTINT',
			width : 150,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//14
			header : '<center>Importe <BR>a Recibir</center>',
			tooltip: 'Importe a Recibir',
			dataIndex : 'IMPORTREC',
			width : 150,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//15 //2017_003
		    header : 'Campo1',
		    tooltip: 'Campo1',
		    dataIndex : 'CAMPO0',
		    width : 80,
		    sortable : true,
		    hidden: true,
		    align: 'center'			
		},
		{//16 //2017_003
		    header : 'Campo2',
		    tooltip: 'Campo2',
		    dataIndex : 'CAMPO1',
		    width : 80,
		    sortable : true,
		    hidden: true,
		    align: 'center'			
		},
		{//17 //2017_003
		    header : 'Campo3',
		    tooltip: 'Campo3',
		    dataIndex : 'CAMPO2',
		    width : 80,
		    sortable : true,
		    align: 'center',
		     hidden: true
		},
		{//18 //2017_003
		    header : 'Campo4',
		    tooltip: 'Campo4',
		    dataIndex : 'CAMPO3',
		    hidden: true,
		    width : 80,
		    sortable : true,
		    align: 'center'			
		},
		{//19
		    header : 'Campo5',
		    tooltip: 'Campo5',
		    dataIndex : 'CAMPO4',
		    width : 80,
		    sortable : true,
		    hidden: true,
		    align: 'center'			
		},
		{//20
			header : 'Beneficiario',
			tooltip: 'Beneficiario',
			dataIndex : 'BENEFICIARIO',			
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//21
			header : '% Beneficiario',
			tooltip: '% Beneficiario',
			dataIndex : 'PORCBENEFIC',
			//hidden: true,
			sortable : true,
			width : 100,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		},
		{//22
			header : '<center>Importe <br> a Recibir Beneficiario</center>',
			tooltip: 'Importe a Recibir Beneficiario',
			dataIndex : 'IMPORTEBENEFIC',
			//hidden: true,
			sortable : true,
			width : 120,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},		
		{//23
			header : '<center>Neto a <br> Recibir Pyme</center>',
			tooltip: 'Neto a Recibir Pyme',
			dataIndex : 'NETORECIBPYME',
			sortable : true,			
			width : 100,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	style: 'margin:0 auto;',
	width: 900,
	title: 'PRE ACUSE - SELECCI�N DE DOCUMENTOS',
	frame: true,
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Revisar',
			id: 'btnRevisar',
			handler: regresaAseleccion
			},
			{
			text: 'Generar PDF',
			id: 'btnGenerarPDF',
			hidden: true
			//handler: 
			},
			{
			text: 'Abrir PDF',
			id: 'btnAbrirPDF',
			hidden: true
			//handler: 
			},
			'-',
			{
			text: 'Transmitir Derecho',
			id: 'btnTransmitir',
			handler: fnTransmitirDerecho
			},
			{
			text: 'Salir',
			id: 'btnSalir',
			hidden: true,
			handler: function(){
			    if(strUsuario==='PYME'){
					window.location.href="13formapd4ext.jsp";
			    }else {				 
					window.location.href='13formapd4ext.jsp?strUsr='+strUsr+'&cveEpo='+cveEpoOriginal+'&cvePyme='+Ext.getDom("cvePyme").value+'&tipoFact=D';
			    }	
			}
			},
			'-',
			{
			text: 'Cancelar',
			id: 'btnCancelar',
			handler: function(){
					Ext.Msg.confirm('Confirmaci�n', '�Est� usted seguro de cancelar la operaci�n?', function(btn){
						if(btn==='yes'){							
						   regresaAseleccion();				
						}
					});
				}
			}
			
		]
	}
	});
	
	var gridTotales = new Ext.grid.GridPanel({
	id: 'gridTotales1',
	store: storeTotalData,
	margins: '20 0 0 0',
	columns: [
		{
			header : 'EPO',
			dataIndex : 'EPO',
			width : 200,
			sortable : true,
			renderer:  function (causa, columna, registro){
						var cboEPO = Ext.getCmp('cboEpo1');
						var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
						causa = recordEPO.get(cboEPO.displayField);
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
		},
		{
			header : 'PYME',
			dataIndex : 'PYME',
			width : 200,
			sortable : true,
			//hidden: strUsuario=='NAFIN'?false:true, 
			hidden: true, 
			renderer:  function (causa, columna, registro){
					if(strUsuario==='NAFIN'){
						causa = Ext.getDom("strNombrePymeAsigna").value;
					}
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
			}
		},
		{
			header : '<center>Total Monto Documento</center>',
			tooltip: 'Total Monto Documento',
			dataIndex : 'TOTALMONTO',
			width : 169,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : '<center>Total Monto Descuento</center>',
			tooltip: 'Total Monto Descuento',
			dataIndex : 'TOTALDESCTO',
			width : 166,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : '<center>Total Monto Inter�s</center>',
			tooltip: 'Total Monto Inter�s',
			dataIndex : 'TOTALINTERES',
			width : 166,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : '<center>Total Importe Recibir</center>',
			tooltip: 'Total Importe Recibir',
			dataIndex : 'TOTALIMPORT',
			width : 166,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : '<center>Total Importe a Recibir <BR> Beneficiario</center>', //2017_003
			tooltip: 'Total Importe a Recibir Beneficiario',
			dataIndex : 'TOTALIMPORTBENEF',
			width : 166,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : '<center>Total Neto a Recibir Pyme</center>', //2017_003
			tooltip: 'Total Neto a Recibir Pyme',
			dataIndex : 'TOTALNETOPYME',
			width : 166,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 120,
	width: 900,
	style: 'margin:0 auto;',
	title: ' ',
	frame: true
	});
	
	var gridCifrasCtrl = new Ext.grid.GridPanel({
	id: 'gridCifrasCtrl1',
	store: storeCifrasData,
	margins: '20 0 0 0',
	hideHeaders : true,
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 150,
			sortable : true
		},
		{
			header : 'Informacion',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'informacion',
			width : 230,
			sortable : true,
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 400,
	style: 'margin:0 auto;',
	autoHeight : true,
	title: 'Cifras de Control',
	frame: true
	});
	
	
	
	var panelMsgLegal = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'pnmsgLegal',
		id: 'pnmsgLegal1',
		width: 900,
		style: 'margin:0 auto;',
		frame: true,
		html:'Al solicitar el factoraje electr&oacute;nico o descuento electr&oacute;nico del documento que selecciono e identifico, '+
				'transmito los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art&iacute;culos 427 de la ley general de t&iacute;tulos y operaciones de cr&eacute;dito, '+
				'32 c del c&oacute;digo fiscal de la federaci&oacute;n y 2038 del c&oacute;digo civil federal, haci&eacute;ndome sabedor de su contenido y alcance, condicionado a que se efect&uacute;e '+
				'el descuento electr&oacute;nico o el factoraje electr&oacute;nico.'
	});
	
	var panelMsgCifrado = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'panelMsgCifrado',
		id: 'panelMsgCifrado1',
		width: 900,
		style: 'margin:0 auto;',
		frame: true,
		html:'Al solicitar el factoraje electr&oacute;nico o descuento electr&oacute;nico del documento que selecciono e identifico, '+
				'transmito los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art&iacute;culos 427 de la ley general de t&iacute;tulos y operaciones de cr&eacute;dito, '+
				'32 c del c&oacute;digo fiscal de la federaci&oacute;n y 2038 del c&oacute;digo civil federal, haci&eacute;ndome sabedor de su contenido y alcance, condicionado a que se efect&uacute;e '+
				'el descuento electr&oacute;nico o el factoraje electr&oacute;nico.'
	});
	
	var panelMsgAcuse = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'pnmsgAcuse',
		id: 'pnmsgAcuse1',
		width: 400,
		style: 'margin:0 auto;',
		frame: true
	});
	
	var panelMsgMotosMenos = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'panelMsgMotosMenos',
		id: 'panelMsgMotosMenos',
		width: 900,
		style: 'margin:0 auto;',
		frame: false,
		html:'<b>Nota:</b> Los documentos que se encuentran inhabilitados no podr�n ser operados por el IF.'
	});	
//-----------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			NE.util.getEspaciador(20),
			{//Panel para mostrar avisos  //2017_003
				xtype: 'panel',
				name: 'mensajeDistribuido',
				id: 'mensajeDistribuido1',
				width: 600,
				frame: true,
				style: 'margin:0 auto;',
				hidden: true
			},			
			{//Panel para mostrar avisos
				xtype: 'panel',
				name: 'mensajes',
				id: 'mensajes1',
				width: 600,
				frame: true,
				style: 'margin:0 auto;',
				hidden: true
			},			
			fp,
			NE.util.getEspaciador(10),
			{
				xtype: 'panel',
				name: 'intermediariosS',
				id: 'intermediariosS1',
				title:'Selecci�n de Intermediario Financiero',
				width: 600,
				style: 'margin:0 auto;',
				frame: true,
				items:[
					{
						xtype: 'combo',
						name: 'cboIf',
						id: 'cboIf1',
						hidden: true,
						fieldLabel: 'Intermediario',
						mode: 'local', 
						displayField : 'lsNombreCuenta',
						valueField : 'lsIf',
						hiddenName : 'cboIf',
						emptyText: 'Seleccionar',
						width: 580,
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store : storeCatIfData,
						listeners:{
							select : function(cboIf, record, index ) {
								pnl.el.unmask();
								pnl.el.mask('Consultando...', 'x-mask-loading');
								
								var cboEpo = Ext.getCmp('cboEpo1');								
								var valEPO;
								
								if(comboEpoVacio){
									valEPO = cveEpoOriginal+'|'+'SI ALFO MAS';
								}else{
									record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
									valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
								}
								
								var storeIf = cboIf.getStore();
								var valIF='';
								if(cboIf.getValue()===''){
									record = storeIf.getAt(0);
									valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
									
								}else{
									record = cboIf.findRecord(cboIf.valueField, cboIf.getValue());
									if(record){
										valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
									}else{
										record = storeIf.getAt(0);
										valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
										cboIf.setValue(record.data['lsIf']);
									}
								}
								
								objGral.valIF = valIF;
								Ext.Ajax.request({
									url: '13formapd4ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'GeneraInfoCons',
										cboEpo: valEPO,
										cboIf: valIF,
										icBeneficiario:Ext.getCmp('icBeneficiario1').getValue() //2017_003
									}),
									callback: procesarSuccessValoresGrales 
								});  
							}
						},
						tpl : NE.util.getTemplateMensajeCargaCombo('{lsNombreCuenta}')
					}
				],
				hidden: true
			},
			NE.util.getEspaciador(10),
			{//Panel para mostrar avisos  //2017_003
			    xtype: 'panel',
			    name: 'mensajeDis',
			    id: 'mensajeDis1',
			    width: 600,
			    frame: true,
			    style: 'margin:0 auto;',
			    hidden: true
			}
			
		    ]
	});
	
	fp.hide();
	storeCatEpoData.load();
	storeCatMonedaData.load();
	
	var iniRequest = function(storeEpo){
		var valorEpo = '';
		var cboEpo = Ext.getCmp('cboEpo1');

		if(strUsuario==='NAFIN'){
			var record = '';
			if(Ext.getDom("hidCboEpo").value!=='' || comboEpoVacio ){
				cveEpoOriginal = Ext.getDom("hidCboEpo").value;								
			
				if(comboEpoVacio){
					valorEpo = cveEpoOriginal+'|'+'IF';
				}else{
					if(storeEpo.findExact("clave", Ext.getDom("hidCboEpo").value) !== -1 ){
						cboEpo.setValue(Ext.getDom("hidCboEpo").value);
						record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());		
						valorEpo = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
					}else{
						record = storeEpo.getAt(0);
						cboEpo.setValue(record.data['clave']);
						valorEpo = cboEpo.getValue()+'|'+record.data['descripcion'];
					}
				}
			}else{
				record = storeEpo.getAt(0);
				cboEpo.setValue(record.data['clave']);
				valorEpo = cboEpo.getValue()+'|'+record.data['descripcion'];
			}
		}
		
		Ext.Ajax.request({
			url: '13formapd4ext.data.jsp',
			params: {
				informacion: 'valoresIniciales',
				cboEpo: valorEpo,
				cvePyme: Ext.getDom("cvePyme").value,
				cveEpoOriginal: cveEpoOriginal,
				strUsr: strUsr
				},
			callback: procesarSuccessValoresIni
		});
	};
	

});