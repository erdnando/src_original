<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		javax.naming.Context,
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.CatalogoIFDistribuidores,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";

log.debug("informacion = <"+informacion+">");

if (	informacion.equals("CatalogoIf") ) {
	
	CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setProducto("1");
	cat.setOrden("2");

	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("getParamsIf") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	ParametrosDescuento paramDsctoEJB = null;
	try {

		paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	}catch(Exception e){
		success		= false;
		log.error("AceptaCapura.Aceptar(Exception): Obtener instancia del EJB de ParametrosDescuento");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ParametrosDescuento.");

	}

	String sNiF						= (request.getParameter("clave_if")==null)?"":request.getParameter("clave_if");

	String sOperaFactoraje24	= new String(""),	sParamMoneda = new String(""), sParamEmail = new String(""), sParamConsulta = new String(""), sParamTasaFondeo = new String(""),
	sParamFisoCortes= new String("");
	String sNotificaDoctos = ""; //F000-2015 ABR
	String sMailNotificaDoctos = ""; //F000-2015 ABR

	Hashtable alParamIF = new Hashtable(); 
	String  operaFideiComiso= "";
	
	if(!sNiF.equals("")) {
		alParamIF = paramDsctoEJB.getParametrosIF(sNiF, 1);
		if (alParamIF!=null) {
			sOperaFactoraje24   = alParamIF.get("PUB_IF_FACT_24HRS")       ==null||alParamIF.get("PUB_IF_FACT_24HRS").equals("")       ?"":(String)alParamIF.get("PUB_IF_FACT_24HRS"); // FODEA 016 - 2010
			sParamMoneda        = alParamIF.get("TIPO_MONEDA")             ==null||alParamIF.get("TIPO_MONEDA").equals("")             ?"":(String)alParamIF.get("TIPO_MONEDA");
			sParamConsulta      = alParamIF.get("TIPO_CONSULTA")           ==null||alParamIF.get("TIPO_CONSULTA").equals("")           ?"":(String)alParamIF.get("TIPO_CONSULTA");
			sParamEmail         = alParamIF.get("CORREO_NOTIFICACION")     ==null||alParamIF.get("CORREO_NOTIFICACION").equals("")     ?"":(String)alParamIF.get("CORREO_NOTIFICACION");
			sParamTasaFondeo    = alParamIF.get("TASA_FONDEO")             ==null||alParamIF.get("TASA_FONDEO").equals("")             ?"":(String)alParamIF.get("TASA_FONDEO");
			sParamFisoCortes    = alParamIF.get("FISO_CORTES")             ==null||alParamIF.get("FISO_CORTES").equals("")             ?"N":(String)alParamIF.get("FISO_CORTES");
			sNotificaDoctos     = alParamIF.get("NOTIFICACION_DOCTOS")     ==null||alParamIF.get("NOTIFICACION_DOCTOS").equals("")     ?"N":(String)alParamIF.get("NOTIFICACION_DOCTOS");  //F000-2015 ABR
			sMailNotificaDoctos = alParamIF.get("MAIL_NOTIFICACION_DOCTOS")==null||alParamIF.get("MAIL_NOTIFICACION_DOCTOS").equals("")?"":(String)alParamIF.get("MAIL_NOTIFICACION_DOCTOS");  //F000-2015 ABR
		}
		
		operaFideiComiso =  paramDsctoEJB.getOperaFideicomiIF(sNiF);
		
	} //Fin de la obtencion de los parametros

	jsonObj.put("clave_if",				sNiF							);
	jsonObj.put("sOperaFactoraje24",	sOperaFactoraje24			);
	jsonObj.put("sParamMoneda",		sParamMoneda				);
	jsonObj.put("sParamConsulta",		sParamConsulta				);
	jsonObj.put("sParamEmail",			sParamEmail					);
	jsonObj.put("sParamTasaFondeo",			sParamTasaFondeo	);	
	jsonObj.put("operaFideiComiso",	operaFideiComiso		);
	jsonObj.put("sParamFisoCortes",	sParamFisoCortes		);
	jsonObj.put("sNotificaDoctos",     sNotificaDoctos      ); //F000-2015 ABR
	jsonObj.put("sMailNotificaDoctos", sMailNotificaDoctos  ); //F000-2015 ABR
	jsonObj.put("estadoSiguiente",     "RESPONSE_PARAMS_IF" );
	jsonObj.put("success",				new Boolean(success)		);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Save") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;
	String msg = "";

	ParametrosDescuento paramDsctoEJB = null;
	try {

		paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	}catch(Exception e){
		success		= false;
		log.error("AceptaCapura.Aceptar(Exception): Obtener instancia del EJB de ParametrosDescuento");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ParametrosDescuento.");

	}

	String	sNiF						= (request.getParameter("sNiF")						==null)?"":request.getParameter("sNiF");
	String	sOperaFactoraje24hrs = (request.getParameter("sOperaFactoraje24hrs")	==null)?"":request.getParameter("sOperaFactoraje24hrs");// FODEA 037 - 2009
	String	sMoneda 					= (request.getParameter("sMoneda")					== null)?"":request.getParameter("sMoneda");
	String sParamConsulta        = (request.getParameter("sParamConsulta")        ==null)?"":request.getParameter("sParamConsulta");// FODEA 014-2011
	String consulta              = (request.getParameter("consulta")              ==null)?"":request.getParameter("consulta");// FODEA 014-2011
	String Email                 = (request.getParameter("Email")                 ==null)?"":request.getParameter("Email");
	String tasaOperacion         = (request.getParameter("tasaOperacion")         ==null)?"":request.getParameter("tasaOperacion");
	String fide_cortes           = (request.getParameter("fide_cortes")           ==null)?"":request.getParameter("fide_cortes");
	String notifica_doctos       = (request.getParameter("notifica_doctos")       ==null)?"":request.getParameter("notifica_doctos");
	String email_notifica_doctos = (request.getParameter("email_notifica_doctos") == null)?"":request.getParameter("email_notifica_doctos");
	String correo			= Email.replace(';',',');
	String emailNotificaDoctos   = email_notifica_doctos.replaceAll(";",",");
	
	//System.out.println("fide_cortes  "+fide_cortes);
	//System.out.println("sNiF  "+sNiF); 
	//System.out.println("1.-" + notifica_doctos);
	//System.out.println("2.-" + emailNotificaDoctos);
	try{
		Hashtable alParamIF = new Hashtable();
		alParamIF.put("PUB_IF_FACT_24HRS",        sOperaFactoraje24hrs ); // FODEA 016 - 2010
		alParamIF.put("TIPO_MONEDA",              sMoneda              );
		alParamIF.put("TIPO_CONSULTA",            consulta             );
		alParamIF.put("CORREO_NOTIFICACION",      correo               );
		alParamIF.put("TASA_FONDEO",              tasaOperacion        );
		alParamIF.put("FISO_CORTES",              fide_cortes          );
		alParamIF.put("NOTIFICACION_DOCTOS",      notifica_doctos      );
		alParamIF.put("MAIL_NOTIFICACION_DOCTOS", emailNotificaDoctos  );
		
		paramDsctoEJB.setParametrosIF(sNiF, alParamIF, 1,iNoUsuario);
		msg = "Se guardo con éxito la parametrización del IF.";

	}catch(NafinException ne){
		ne.printStackTrace();
		msg = ne.getMsgError();
		success = false;
	}

	jsonObj.put("msg",					msg							);
	jsonObj.put("estadoSiguiente",	"RESPUESTA_GRABAR"		);
	jsonObj.put("success",				new Boolean(success)		);
	infoRegresar = jsonObj.toString();

}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>