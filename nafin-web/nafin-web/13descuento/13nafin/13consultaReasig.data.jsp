<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,	
	java.sql.*,	
   java.text.*, 
   javax.naming.*,
	com.netro.descuento.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>	

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String epo = (request.getParameter("epo")!=null)?request.getParameter("epo"):"";
String cg_no_electronico_prov = (request.getParameter("cg_no_electronico_prov")!=null)?request.getParameter("cg_no_electronico_prov"):"";
String cg_no_provedor = (request.getParameter("cg_no_provedor")!=null)?request.getParameter("cg_no_provedor"):"";
String cg_rfc = (request.getParameter("cg_rfc")!=null)?request.getParameter("cg_rfc"):"";
String cg_no_docto = (request.getParameter("cg_no_docto")!=null)?request.getParameter("cg_no_docto"):"";
String cg_usuario_reasigno = (request.getParameter("cg_usuario_reasigno")!=null)?request.getParameter("cg_usuario_reasigno"):"";
String df_reasignacion_i = (request.getParameter("df_reasignacion_i")!=null)?request.getParameter("df_reasignacion_i"):"";
String df_reasignacion_f = (request.getParameter("df_reasignacion_f")!=null)?request.getParameter("df_reasignacion_f"):"";

String infoRegresar  ="", consulta ="";
JSONObject   jsonObj = new JSONObject(); 
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();

IMantenimiento IMantenimientos = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);


if (informacion.equals("valoresIniciales") ) {
 
	SimpleDateFormat fecha_hoy = new SimpleDateFormat("dd/MM/yyyy");
	String fecha_Actual = fecha_hoy.format(new java.util.Date());
		
	 jsonObj.put("success",  new Boolean(true));
	 jsonObj.put("fecha_Actual", fecha_Actual);	
	 infoRegresar = jsonObj.toString();	

}else  if( informacion.equals("catalogoEPO") ){

	List catEPO = getCatalogoEP0();
	jsonObj.put("registros", catEPO);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();
	
	
}else  if( informacion.equals("Consultar") ){
	
	List vDoctosCargados = new ArrayList();
	List vd = null;
	vDoctosCargados = IMantenimientos.mostrarDocumentosReasignados(epo, cg_no_electronico_prov, cg_no_provedor, cg_rfc, cg_no_docto, cg_usuario_reasigno, df_reasignacion_i,df_reasignacion_f,strDirectorioTemp);
	for(int i=0;i<vDoctosCargados.size();i++){
		vd = (List)vDoctosCargados.get(i);
	
	   datos = new HashMap();
		datos.put("FECHA_REASIGNACION",(vd.get(0).toString()));		
		datos.put("NOMBRE_PYME",(vd.get(1).toString()));	
		datos.put("RFC_PYME",(vd.get(2).toString()));	
		datos.put("DIRECCION_PYME",(vd.get(3).toString()));	
		datos.put("COLONIA_PYME",(vd.get(4).toString()));	
		datos.put("TELEFONO_PYME",(vd.get(5).toString()));	
		datos.put("NOMBRE_EPO",(vd.get(6).toString()));	
		datos.put("NOMBRE_PYME_DES",(vd.get(7).toString()));	
		datos.put("RFC_PYME_DES",(vd.get(8).toString()));	
		datos.put("DIRECCION_PYME_DES",(vd.get(9).toString()));	
		datos.put("COLONIA_PYME_DES",(vd.get(10).toString()));	
		datos.put("TELEFONO_PYME_DES",(vd.get(11).toString()));	
		datos.put("NO_DOCUMENTO",(vd.get(12).toString()));	
		datos.put("FECHA_DOCUMENTO",(vd.get(13).toString()));	
		datos.put("FECHA_VENCIMIENTO",(vd.get(14).toString()));	
		datos.put("MONEDA",(vd.get(15).toString()));	
		datos.put("TIPO_FACTORAJE",(vd.get(16).toString()));	
		datos.put("MONTO_DOCUMENTO",(vd.get(17).toString()));	
		datos.put("CAUSA_RECHAZO",(vd.get(23).toString()));
		datos.put("NOMBRE_ARCHIVO",(vd.get(24).toString()));
		
		registros.add(datos);
	}
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";					
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();
}	

%>

<%=infoRegresar%>

<%!

public List  getCatalogoEP0( ){
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;	
	HashMap datos =  new HashMap();
	List registros  = new ArrayList();
	try{
		con.conexionDB();
		
		String SQL = " SELECT DISTINCT e.ic_epo AS ic_epo, e.cg_razon_social AS razon_social"   +
						"  FROM comcat_epo e, bit_reasignacion_doctos b"   +
						"  WHERE e.ic_epo = b.ic_epo "  ; 							
		rs = con.queryDB(SQL);			
		while(rs.next()) {			
			datos = new HashMap();			
			datos.put("clave", rs.getString(1));
			datos.put("descripcion", rs.getString(2));
			registros.add(datos);
		}
		rs.close();
		con.cierraStatement();
	
	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	} 
	return registros;
}
%>
