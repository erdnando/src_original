<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.text.*,
	java.util.Map,
	java.util.Set,
	java.io.*,
	javax.naming.*,
	com.netro.model.catalogos.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	com.netro.descuento.*,
	com.netro.afiliacion.*,
	com.netro.exception.*,
	com.netro.model.catalogos.CatalogoSimple,
	com.netro.model.catalogos.CatalogoEPONafin,
	netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>	
<%!
		private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String indiceEst= (request.getParameter("indiceEst") != null) ? request.getParameter("indiceEst") : "";
	String ic_ifnb = (String)(request.getParameter("ic_ifnb") == null?"":request.getParameter("ic_ifnb"));
	String infoRegresar ="", consulta="";
	JSONObject jsonObj = new JSONObject();
	HashMap datos = new HashMap();
	AutorizacionSolicitud autorizacionSolicitud = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);
	JSONArray registros= new JSONArray();
	if(informacion.equals("catIntermediarioFinanciero")){
	//Thread.sleep(5000);
	//int x =10/0;
		log.debug("DetalleClientesAction::inicializar(E)");
		try{
			List lista = autorizacionSolicitud.getListaDeIntermediariosFinancierosNoBancarios();
			List listaDeIntermediariosFinancierosNoBancarios=new ArrayList();
			for(int i=0;i<lista.size();i++){
				HashMap 	registro 	= (HashMap) lista.get(i);
				String 	clave				= (String)  registro.get("ID");
				String 	descripcion	= (String)  registro.get("DESCRIPCION");	
				datos.put("clave",clave);
				datos.put("descripcion",descripcion);
				registros.add(datos);
			}
			consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);
			
		}catch(Throwable e){
	
			throw new   AppException("Error al cargar datos ", e);
		 }
			infoRegresar = jsonObj.toString(); 
	}else if(informacion.equals("Consultar")){
		//Thread.sleep(5000);
		//int x =10/0;
		log.debug("DetalleClientesAction::consultar(E)");
		int numreg = 0;
		List registros1 = null;
		try{
			
			registros1 				= autorizacionSolicitud.getDetalleArchivosEnviados(ic_ifnb);
			if(registros1.size()>0){
				for(int i=0;i<registros1.size();i++){
					HashMap 	registro2 	= (HashMap) registros1.get(i);
					String 	no_prestamo				= (String)  registro2.get("NUMERO_DE_PRESTAMO");
					String 	ic_solic_portal	= (String)  registro2.get("IC_SOLIC_PORTAL");	
					String 	no_cliente	= (String)  registro2.get("NUMERO_DE_CLIENTES");
					String 	monto	= (String)  registro2.get("MONTO");
					String 	fecha_carga	= (String)  registro2.get("FECHA_DE_CARGA");
					datos.put("NUMERO_DE_PRESTAMO",no_prestamo);
					datos.put("IC_SOLIC_PORTAL",ic_solic_portal);
					datos.put("NUMERO_DE_CLIENTES",no_cliente);
					datos.put("MONTO",monto);
					datos.put("FECHA_DE_CARGA",fecha_carga);
					registros.add(datos);
				}
			}
			consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);
			 
		}catch(Throwable e){
	
			throw new   AppException("Error al consultar datos ", e);
		 }
			infoRegresar = jsonObj.toString(); 
	}
%>
<%=infoRegresar%>

