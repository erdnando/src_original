Ext.onReady(function(){


//-----------------------------------VARIABLES----------------------------------
	var jsonValoresIniciales = null;
//------------------------------------HANDLERS----------------------------------
	function procesaValoresIniciales(opts, success, response) {
			fp.el.mask();
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
					jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
					if(jsonValoresIniciales != null){
						fp.el.unmask();
						
						Ext.getCmp('strTipoUsuario').setValue(jsonValoresIniciales.strTipoUsuario);
						if(jsonValoresIniciales.strTipoUsuario=='NAFIN' && jsonValoresIniciales.esPromotorNafin=='N'){ 
							Ext.getCmp('fpBotones').show();
						}
						if(jsonValoresIniciales.strTipoUsuario=='NAFIN'){
							Ext.getCmp('ic_banco_fondeo01').show();
							Ext.getCmp('ic_epo1').show();
							Ext.getCmp('ic_epo2').show();
						}
					}
			} else {
					NE.util.mostrarConnError(response,opts);
			}
	}
	var procesarCatalogoAnio = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cboAnio = Ext.getCmp('cboAnio');
			if(cboAnio.getValue()==''){
				cboAnio.setValue(records[0].data['clave']);
			}
		}
	}	
	var procesarCatalogoMes = function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cboMes = Ext.getCmp('cboMes');
			if(cboMes.getValue()==''){
				cboMes.setValue(records[0].data['clave']);
			}
		}
	}
	var procesarResumenOperacion =  function(opts, success, response) {
		fp.el.unmask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var url = Ext.util.JSON.decode(response.responseText).urlArchivo;
			if(url != ""){
				var forma = Ext.getDom('formAux');
				forma.action = url;
				forma.submit();
			}else{
				Ext.Msg.alert('Mensaje error',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){  
					return;
				});				
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarPermisoConsulta =  function(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var hora = Ext.util.JSON.decode(response.responseText).HORA_REPORTE;
			if(Ext.util.JSON.decode(response.responseText).PERMISO_CONSULTA == "false"){
				Ext.Msg.alert('Mensaje informativo','El horario de consulta del reporte es a partir de las '+hora+'hrs. Por favor intente m�s tarde.',function(btn){
					return;
				});				
			}else{
				var strDirectorioTemp = jsonValoresIniciales.strDirectorioTemp;
				var strTipoUsuario = Ext.getCmp('strTipoUsuario');
				var epo = jsonValoresIniciales.epo;
				var dc_fecha_pubMin = Ext.getCmp('dc_fecha_pubMin');
				var dc_fecha_pubMax = Ext.getCmp('dc_fecha_pubMax');
				var fecha_publicacion_de = Ext.util.Format.date(dc_fecha_pubMin.getValue(),'d/m/Y');
				var fecha_publicacion_a = Ext.util.Format.date(dc_fecha_pubMax.getValue(),'d/m/Y');
				var rdoReporte1 = Ext.getCmp('rdoReporte1');
				var rdoReporte2 = Ext.getCmp('rdoReporte2');
				var rdoReporte3 = Ext.getCmp('rdoReporte3');
				if(strTipoUsuario.getValue()=='NAFIN') {
					epo = Ext.getCmp('ic_epo1').getValue();
				}
								
				if(rdoReporte1.getValue() == true){
				
					//document.forms[0].action = '/nafin/13descuento/13nafin/13REPORTEPYMESNAFINDescargarArchivo.do?directorio='+strDirectorioTemp+'&ic_epo='+epo+'&fecha_publicacion_de='+fecha_publicacion_de+'&fecha_publicacion_a='+fecha_publicacion_a;
					//document.forms[0].submit();
					fp.el.mask('Generando Reporte...','x-mask-loading');
					Ext.Ajax.request({
						url: '13consulta19aext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: "generaXlsResumenPymes",
							fecha_publicacion_de: fecha_publicacion_de,
							fecha_publicacion_a: fecha_publicacion_a,
							ic_epo:epo
						}),
						callback: procesarGeneraXLS
					});
					
				}else if(rdoReporte2.getValue() == true){
					//document.forms[0].action = '/nafin/13descuento/13nafin/13REPORTEANALITICONAFINDescargarArchivo.do?directorio='+strDirectorioTemp+'&ic_epo='+epo+'&fecha_publicacion_de='+fecha_publicacion_de+'&fecha_publicacion_a='+fecha_publicacion_a;
					//document.forms[0].submit();
					fp.el.mask('Generando Reporte...','x-mask-loading');
					Ext.Ajax.request({
						url: '13consulta19aext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: "generaXlsResumenAnalitico",
							fecha_publicacion_de: fecha_publicacion_de,
							fecha_publicacion_a: fecha_publicacion_a,
							ic_epo:epo
						}),
						callback: procesarGeneraXLS
					});
				}else if(rdoReporte3.getValue() == true){
					fp.el.mask('Enviando...','x-mask-loading');
					Ext.Ajax.request({
						url: '13consulta19aext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: "resumenOperacion",
							ic_epo:epo
						}),
						callback: procesarResumenOperacion
					});
				}
			}
		}
	}
	
	var procesarGeneraXLS = function procesarArchivoSuccess(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//-------------------------------------STORES-----------------------------------
	var catalogoMes = new Ext.data.JsonStore({
		id: 'catalogoMesStore',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '13consulta19aext.data.jsp',
		baseParams: {
			informacion: 'catalogoMes'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoMes,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoAnio = new Ext.data.JsonStore({
		id: 'catalogoAnioStore',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '13consulta19aext.data.jsp',
		baseParams: {
			informacion: 'catalogoAnio'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoAnio,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var CatalogoBanco = new Ext.data.JsonStore({
		id: 'CatalogoBanco',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta19aext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBanco'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
		}
	});
	
		
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta19aext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
		}
	});
		
//-----------------------------------COMPONENTES--------------------------------
	var elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '90%',
			items: [
				{
					xtype: 'radio',
					boxLabel: '<font size="2" face="Arial">Resumen de Pymes</font>',
					checked: true,
					name: 'rdoReporte',
					id: 'rdoReporte1',
					height:40,
					inputValue: 'S',
					handler: function(ctl, val) { 
						var boton = Ext.getCmp('btnGenerar');
						var rdoReporte1 = Ext.getCmp('rdoReporte1');
						var rdoReporte2 = Ext.getCmp('rdoReporte2');
						var rdoReporte3 = Ext.getCmp('rdoReporte3');
						var camposReportes = Ext.getCmp('camposReportes');
						var camposResumen = Ext.getCmp('camposResumen');
						var ic_banco_fondeo01 = Ext.getCmp('ic_banco_fondeo01');
						var ic_epo1 = Ext.getCmp('ic_epo1');
						if(rdoReporte1.getValue() == true){
							ic_banco_fondeo01.show();
							ic_epo1.show();	
						}
						
						if(rdoReporte1.getValue() == true || rdoReporte2.getValue() == true){
							boton.setIconClass('icoXls');
							camposReportes.show();
							camposResumen.hide();							
						}else{
							boton.setIconClass('icoPdf');
							camposReportes.hide();
							camposResumen.show();
						}
					} 
				},
				{
					xtype: 'radio',
					boxLabel: '<font size="2"  face="Arial">Reporte Anal�tico de Documentos</font>',
					checked: false,
					name: 'rdoReporte',
					id: 'rdoReporte2',
					height:40,
					inputValue: 'S',
					handler: function(ctl, val) { 
						var boton = Ext.getCmp('btnGenerar');
						var rdoReporte1 = Ext.getCmp('rdoReporte1');
						var rdoReporte2 = Ext.getCmp('rdoReporte2');
						var rdoReporte3 = Ext.getCmp('rdoReporte3');
						var camposReportes = Ext.getCmp('camposReportes');
						var camposResumen = Ext.getCmp('camposResumen');
						var ic_banco_fondeo01 = Ext.getCmp('ic_banco_fondeo01');
						var ic_epo1 = Ext.getCmp('ic_epo1');
						if(rdoReporte2.getValue() == true){
							ic_banco_fondeo01.show();
							ic_epo1.show();
						}
							
						if(rdoReporte1.getValue() == true || rdoReporte2.getValue() == true){
							boton.setIconClass('icoXls');
							camposReportes.show();
							camposResumen.hide();							
						}else{
							boton.setIconClass('icoPdf');
							camposReportes.hide();
							camposResumen.show();
						}
					} 
				},
				{
					xtype: 'radio',
					boxLabel: '<font size="2" face="Arial">Resumen de Operaci�n EPO</font>',
					checked: false,
					name: 'rdoReporte',
					id: 'rdoReporte3',
					height: 40,
					inputValue: 'S',
					handler: function(ctl, val) { 
						var boton = Ext.getCmp('btnGenerar');
						var rdoReporte1 = Ext.getCmp('rdoReporte1');
						var rdoReporte2 = Ext.getCmp('rdoReporte2');
						var rdoReporte3 = Ext.getCmp('rdoReporte3');
						var camposReportes = Ext.getCmp('camposReportes');
						var camposResumen = Ext.getCmp('camposResumen');
						var ic_banco_fondeo01 = Ext.getCmp('ic_banco_fondeo01');
						var ic_epo1 = Ext.getCmp('ic_epo1');
						
						if(rdoReporte3.getValue() == true){
							ic_banco_fondeo01.hide();
							ic_epo1.show();
						}	
						if(rdoReporte1.getValue() == true || rdoReporte2.getValue() == true){
							boton.setIconClass('icoXls');
							camposReportes.show();
							camposResumen.hide();			
							
						}else{
							boton.setIconClass('icoPdf');
							camposReportes.hide();
							camposResumen.show();
						}
					} 
				}
			]
		},
		{
			xtype: 'compositefield',
			id: 'ic_banco_fondeo01',
			fieldLabel: 'Banco de Fondeo',
			combineErrors: false,
			msgTarget: 'side',
			hidden: true,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_banco_fondeo',
					id: 'ic_banco_fondeo1',
					fieldLabel: 'Banco de Fondeo',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_banco_fondeo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store : CatalogoBanco,
					tpl : NE.util.templateMensajeCargaCombo,
					listeners: {
						select: {
							fn: function(combo) {
								var cmbEPO =  Ext.getCmp('ic_epo1');
								cmbEPO.setValue('');
								cmbEPO.store.load({
									params: {								
										ic_banco_fondeo:combo.getValue()
									}
								});														
							}
						}
					}
				}
			]
		},	
		{
			xtype: 'compositefield',
			fieldLabel: 'Nombre de la EPO',
			combineErrors: false,
			msgTarget: 'side',
			id: 'ic_epo2',
			hidden: true,
			width: 700,
			items: [		
				{
					xtype: 'combo',
					name: 'ic_epo',
					id: 'ic_epo1',
					fieldLabel: 'Nombre de la EPO',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_epo',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					hidden: true,
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoEPO,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 500				
				}
			]	
		},
		{
			xtype: 'compositefield',
			id: 'camposReportes',
			fieldLabel: 'Fecha de publicaci�n',
			combineErrors: false,
			msgTarget: 'side',		
			items: [				
				{
					xtype: 'datefield',
					name: 'df_fecha_pub_de',
					id: 'dc_fecha_pubMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecho: 'dc_fecha_pubMax',
					margins: '0 20 0 0' //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 24
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_pub_a',
					id: 'dc_fecha_pubMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'dc_fecha_pubMin',
					margins: '0 20 0 0' //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			id: 'camposResumen',
			fieldLabel: 'Fecha de Corte',
			combineErrors: false,
			msgTarget: 'side',			
			hidden: true,
			items: [			
				{
					xtype: 'combo',
					name: 'mes',
					id: 'cboMes',
					allowBlank: true,
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					hiddenName: 'mes',
					emptyText: 'Seleccione mes...',
					forceSelection: false, 
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					store: catalogoMes,
					tpl: NE.util.templateMensajeCargaCombo,
					anchor: '90%'
				},
				{
					xtype: 'combo',
					name: 'anio',
					id: 'cboAnio',
					allowBlank: true,
					mode: 'local',
					displayField: 'descripcion',
					valueField: 'clave',
					hiddenName: 'anio',
					emptyText: 'Seleccione a�o...',
					forceSelection: false, 
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					store: catalogoAnio,
					tpl: NE.util.templateMensajeCargaCombo,
					anchor: '90%',
					width: 120
				}
			]
		},
		{ 	xtype: 'textfield',  id: 'strTipoUsuario', hidden: true,	value: '' }
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 770,
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 120,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
						{
							text: 'Generar Reporte',
							id: 'btnGenerar',
							iconCls: 'icoXls',
							formBind: true,
							handler: function (boton, evento){
								var dc_fecha_pubMin = Ext.getCmp('dc_fecha_pubMin');
								var dc_fecha_pubMax = Ext.getCmp('dc_fecha_pubMax');
								var rdoReporte1 = Ext.getCmp('rdoReporte1');
								var rdoReporte2 = Ext.getCmp('rdoReporte2');
								var rdoReporte3 = Ext.getCmp('rdoReporte3');
								var strTipoUsuario = Ext.getCmp('strTipoUsuario');
								var ic_epo1 = Ext.getCmp('ic_epo1');
																
								if(rdoReporte1.getValue() == true || rdoReporte2.getValue() == true){
								
									if(strTipoUsuario.getValue()=='NAFIN')  {								
										if(Ext.isEmpty(ic_epo1.getValue())){
											ic_epo1.markInvalid("Seleccione una EPO.");
											ic_epo1.focus();
											fp.el.unmask();
											return;
										}
									}
															
									if(Ext.isEmpty(dc_fecha_pubMin.getValue())){
										dc_fecha_pubMin.markInvalid("Introduce el rango inicial de la Fecha de publicaci�n.");
										dc_fecha_pubMin.focus();
										fp.el.unmask();
										return;
									}
									if(Ext.isEmpty(dc_fecha_pubMax.getValue())){
										dc_fecha_pubMax.markInvalid("Introduce el rango final de la Fecha de publicaci�n.");
										dc_fecha_pubMax.focus();
										fp.el.unmask();
										return;
									}
									if(dc_fecha_pubMin.getValue()>dc_fecha_pubMax.getValue()){
										dc_fecha_pubMax.markInvalid("La fecha final no puede ser menor a la fecha inicial.");
										dc_fecha_pubMax.focus();
										fp.el.unmask();
										return;
									}
								}else if(rdoReporte3.getValue() == true){
									var cboMes = Ext.getCmp('cboMes');
									var cboAnio = Ext.getCmp('cboAnio');
									if(cboAnio.getValue() == jsonValoresIniciales.anioActual && cboMes.getValue() > jsonValoresIniciales.mesActual){
										Ext.Msg.alert('Mensaje informativo','Solo para el a�o en curso es necesario escoger el mes actual o anterior.',function(btn){  
											fp.el.unmask();
										});
										return;
									}
								}
							
								var dc_fecha_pubMin_ = Ext.util.Format.date(dc_fecha_pubMin.getValue(),'d/m/Y');
								var dc_fecha_pubMax_ = Ext.util.Format.date(dc_fecha_pubMax.getValue(),'d/m/Y');
								
								if(!Ext.isEmpty(dc_fecha_pubMin.getValue())){
									if(!isdate(dc_fecha_pubMin_)) { 
										dc_fecha_pubMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
										dc_fecha_pubMin.focus();
										return;
									}
								}
								if( !Ext.isEmpty(dc_fecha_pubMax.getValue())){
									if(!isdate(dc_fecha_pubMax_)) { 
										dc_fecha_pubMax.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
										dc_fecha_pubMax.focus();
										return;
									}
								}
								
								fp.el.mask('Enviando...','x-mask-loading');
								Ext.Ajax.request({
									url: '13consulta19aext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
										informacion: "permisoConsulta"
									}),
									callback: procesarPermisoConsulta
								});
							}
						}
		]
	});
	
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',	
      frame:     	false,
      border:    	false,
		hidden: true,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Consulta al Reporte SIAFF',			
				id: 'btnCons1',					
				handler: function() {
					window.location = '13consulta19ext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Captura del Reporte SIAFF',			
				id: 'btnCap',					
				handler: function() {
					window.location = '13forma19ext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<h1><b>Reportes para TOICs</b></h1>',			
				id: 'btnCons2',					
				handler: function() {
					window.location = '13consulta19aext.jsp';
				}
			}
		]
	};
	
//--------------------------------PRINCIPAL-------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20)
		]		
	});
	catalogoMes.load();
	catalogoAnio.load();
//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '13consulta19aext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
	CatalogoBanco.load();
	catalogoEPO.load();
	
});