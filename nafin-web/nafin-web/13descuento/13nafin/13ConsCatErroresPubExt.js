Ext.onReady(function() {

	
/*----------------------------------------- Hanlers -----------------------------------------*/
	var invalida = false;
	var procesarConsultaData = function(store, arrRegistros, opts) 
	{
		//pnl.el.unmask();
		var fp = Ext.getCmp('forma');
		
		fp.el.unmask();
		if (arrRegistros != null )	{
			if (!grid.isVisible()) 	{ 
				grid.show();
			}
			
			/*declaro botones y una variable para manipular el grid*/
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivo');
			var btnEliminar = Ext.getCmp('btnEliminarRegistro');
			var el = grid.getGridEl();
			btnBajarArchivo.hide();
			
			
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();			
				if (arrRegistros == '') {
					grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnGenerarArchivo.disable();
					btnEliminar.disable();
				}
				
				else if (arrRegistros != '')	{
					btnGenerarArchivo.enable();
					btnEliminar.enable();
				}
			}
		}	
	}
	
	var procesarSuccessFailureValidaFechas =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var res = Ext.util.JSON.decode(response.responseText).result
			if ( res == 'invalida') {
				invalida = true;
				Ext.Msg.alert('Eliminar','El rango de m�ximo de consulta es de 30 d�as.');
				return;
			} 
			else {
				var cmpForma = Ext.getCmp('forma');
				cmpForma.el.mask("Procesando", 'x-mask-loading');		
				
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{	
					operacion: 'Generar',
					start : 0,
					limit: 15
					})
				});
			}
		}
	}
	
	function procesaEliminar(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			infoR = Ext.util.JSON.decode(response.responseText);
	
			if (infoR.result != undefined && infoR.result == 'ok'){
				Ext.Msg.alert('Eliminar','Su informaci�n fue actualizada con �xito.');
				consultaData.reload();
				//grid.getSelectionModel().clearSelections();
			}else{
				Ext.Msg.alert('Eliminar','Su informaci�n no pudo ser actualizada.');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function leeRespuesta(){
		window.location = '13ConsCatErroresPubExt.jsp';
	}
	
	
	var procesarSuccessFailureGenerarCSV = function(opts, success, response) 
	{
		 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		 var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		 btnGenerarArchivo.setIconClass('');
		 btnBajarArchivo.setIconClass('icoXls');
		 
		 
		 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		 {
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler(function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		 } else {
			 btnGenerarArchivo.enable();
			 NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
/*--------------------------------- STORE'S -------------------------------*/
//-----------------------------------------------------------------------------//

	
	var catalogoEpo = new Ext.data.JsonStore({
	   id				: 'catEpo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13ConsCatErroresPubExt.data.jsp',
		baseParams	: { informacion: 'CatalogoEPO'},
		totalProperty : 'total',
		autoLoad		: true,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError 
		}
	});		
	
	
//-----------------------------------------------------------------------------//	
	/****** Store�s Grid�s *******/	
	var consultaData = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13ConsCatErroresPubExt.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'IC_ERROR_PUBLICACION'},
				{name: 'NOMBRE_EPO'},
				{name: 'NUMERO_PROVEEDOR'},
				{name: 'NUMERO_DOCTO'},
				{name: 'FECHA_PUBLICACION'},
				{name: 'FECHA_VENCIMIENTO'},					
				{name: 'NOMBRE_MONEDA'},
				{name: 'MONTO'},					
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'NOMBRE_USUARIO'},
				{name: 'EMAIL_USUARIO'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData,
				exception: NE.util.mostrarDataProxyError
			}
	});
	
	
	/****** End Store�s Grid�s *******/
	
	
	/*------------------------------------------ Grids ------------------------------------------*/

	var selectModel = new Ext.grid.CheckboxSelectionModel({
		width:	25,
		singleSelect: false,
      checkOnly: true,
		listeners: {
			 beforerowselect : function (sm, rowIndex, keep, rec) {
				if ( rec.data.CS_BORRADO == 'S'){
					return false;
				}
			 }
		}
    });

	var Columnas = new Ext.grid.ColumnModel([ 
			{
				dataIndex: 'IC_ERROR_PUBLICACION',	
				hidden : true
			},
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 250,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'N�mero de Proveedor',
				tooltip: 'N�mero de Proveedor',
				dataIndex : 'NUMERO_PROVEEDOR',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'No. de Documento',
				tooltip: 'No. de Documento',
				dataIndex : 'NUMERO_DOCTO',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center'
			},		
			
			{
										
				header : 'Fecha de Publicaci�n',
				tooltip: 'Fecha de Publicaci�n',
				dataIndex : 'FECHA_PUBLICACION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'
			},
			{
										
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center'
			},
			{
										
				header : 'Moneda',
				tooltip: 'Moneda ',
				dataIndex : 'NOMBRE_MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'
			},
			{
										
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
										
				header : 'Tipo de Factoraje',
				tooltip: 'Tipo de Factoraje',
				dataIndex : 'NOMBRE_TIPO_FACTORAJE',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center'
			},
			{
										
				header : 'Nombre de Usuario',
				tooltip: 'Nombre de Usuario',
				dataIndex : 'NOMBRE_USUARIO',
				sortable: true,
				width: 180,
				resizable: true,				
				align: 'left'
			},
			{										
				header : 'Email Usuario',
				tooltip: 'Email Usuario',
				dataIndex : 'EMAIL_USUARIO',
				sortable: true,
				width: 170,
				resizable: true,				
				align: 'center'
			},
			selectModel
	]); 
	
	
	var grid = new Ext.grid.EditorGridPanel({
		id: 'grid',				
		store: consultaData,	
		//view:gridView, 
		sm: selectModel,
		cm : Columnas,
		clicksToEdit: 1,
		stripeRows: true,
		columnLines : true,
		loadMask:false,
		height:420,
		width:900,	
		frame: true,
		hidden: true,
		header:true,
		clicksToEdit: 1,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		bbar: {
					xtype: 'paging',
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consultaData,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No se encontro ning�n registro",
					
					items: ['->','-',
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										totalProperty:'total',
										Ext.Ajax.request({
											url: '13ConsCatErroresPubExt.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'GenerarCSV'}),
											callback: procesarSuccessFailureGenerarCSV
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivo',
									hidden: true
								},
								'-',
								{
									xtype:	'button',
									text:		'Eliminar',
									id:		'btnEliminarRegistro',
									handler: function(boton, evento) {
											var sm = grid.getSelectionModel().getSelections();
											if (!Ext.isEmpty(sm)){
												var ic_error = [];
												for (i=0; i<=sm.length-1; i++) {
													ic_error.push(sm[i].get('IC_ERROR_PUBLICACION'));
												}
												Ext.Msg.confirm('Eliminar', '�Est� seguro de eliminar los registros seleccionados?',
															function(botonConf) {
																if (botonConf == 'ok' || botonConf == 'yes') {
																
																	Ext.getCmp('contenedorPrincipal').el.mask('Procesando. . . ','x-mask-loading');
				
																	Ext.Ajax.request({
																		url: '13ConsCatErroresPubExt.data.jsp',
																		params: Ext.apply({informacion: "EliminarArchivos",listRegistros: Ext.encode(ic_error)}),
																		callback: procesaEliminar
																	});
																}
															}
												);				
											}
											else{
												Ext.Msg.alert(boton.text,'Debe seleccionar al menos un registro para eliminar');
											}
					}
				},
				'-'
					]
				}
	});
	
	
/*******************Componentes*******************************/
	
	var elementosForma = [
		{
			xtype				: 'combo',
			id					: 'id_cmb_epo',
			name				: 'cmb_epo',
			hiddenName 		: 'cmb_epo',
			fieldLabel		: 'Nombre de la EPO',
			width				: 480,
			forceSelection	: true,
			triggerAction	: 'all',
			mode				: 'local',
			valueField		: 'clave',
			displayField	: 'descripcion',
			emptyText		: 'Seleccione EPO',
			store				: catalogoEpo,
			margins			: '0 20 0 0' ,
			listeners: {
				select: function(combo, record, index) {
					if (grid.show())
						grid.hide();
				}
			}
		},
		{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de Publicaci�n',
				combineErrors: false,
				msgTarget: 'side',
				items: [
					{
						xtype: 'datefield',
						name: 'txtFchReg1',
						id: 'txtFchReg1',
						allowBlank: false,
						startDay: 0,
						width: 130,
						msgTarget: 'side',
						maxLength : 10,
						vtype: 'rangofecha', 
						campoFinFecha: 'txtFchReg2',
						margins: '0 20 0 0' //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'a',
						width: 24
					},
					{
							xtype: 'datefield',
							name: 'txtFchReg2',
							id: 'txtFchReg2',
							maxLength : 10,
							allowBlank: false,
							startDay: 1,
							width: 130,
							msgTarget: 'side',
							vtype: 'rangofecha', 
							campoInicioFecha: 'txtFchReg1',
							margins: '0 20 0 0' //necesario para mostrar el icono de error
						}
				]
		}			
	];
	

	var fp = new Ext.form.FormPanel
	({
		id: 'forma',
		width: 680,
		style: 'margin:0 auto;',
		title: 'Bit�cora de Errores en la Publicaci�n',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		align			: 'center',
		layout		: 'form',
      defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();												
					var cmpForma = Ext.getCmp('forma');
					var params = (cmpForma)?cmpForma.getForm().getValues():{};				
							
					Ext.Ajax.request({
									url: '13ConsCatErroresPubExt.data.jsp',
									params: Ext.apply(params,{
										
											informacion: 'ValidaFechas'
									}),
									callback: procesarSuccessFailureValidaFechas
							});
				} //fin handler
			}, //fin boton Consultar
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					leeRespuesta();
				}
			}
		]
	});
	
	var pnl = new Ext.Container
	({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			
			fp,	NE.util.getEspaciador(20),
			grid,	
			NE.util.getEspaciador(10)	
		]
	});
	

	
});