<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		com.netro.model.catalogos.*,	
		com.netro.descuento.*,
		com.netro.pdf.*,
		java.math.*,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,
		com.netro.afiliacion.*,
		net.sf.json.JSONObject,
			netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
String informacion     = (request.getParameter("informacion")  != null)?request.getParameter("informacion"):"";
String icGrupo         = (request.getParameter("Hgrupo")       != null)?request.getParameter("Hgrupo"):"";
String icIF            = (request.getParameter("Hif")          != null)?request.getParameter("Hif"):"";

String banco           = (request.getParameter("Hbanco")       != null)?request.getParameter("Hbanco"):"";
String ic_epo          = (request.getParameter("HicEpo")       == null)?"":request.getParameter("HicEpo");
String documento       = (request.getParameter("documento")    == null)?"":request.getParameter("documento");
String naf_elec        = (request.getParameter("_txt_nafelec") == null)?"":request.getParameter("_txt_nafelec");
String ic_pyme         = (request.getParameter("hid_ic_pyme")  == null)?"":request.getParameter("hid_ic_pyme");
String fechaEm1        = (request.getParameter("fechaEm1")     == null)?"":request.getParameter("fechaEm1");
String fechaEm2        = (request.getParameter("fechaEm2")     == null)?"":request.getParameter("fechaEm2");
String infoProcesadaEn = (request.getParameter("stat")         != null)?request.getParameter("stat"):"";
String factoraje       = (request.getParameter("Hfactoraje")   != null)?request.getParameter("Hfactoraje"):"";
String fechaVenc1      = (request.getParameter("fechaVenc1")   != null)?request.getParameter("fechaVenc1"):"";
String fechaVenc2      = (request.getParameter("fechaVenc2")   != null)?request.getParameter("fechaVenc2"):"";
String cbMoneda        = (request.getParameter("HcbMoneda")    != null)?request.getParameter("HcbMoneda"):"";
String monto1          = (request.getParameter("monto1")       != null)?request.getParameter("monto1"):"";
String monto2          = (request.getParameter("monto2")       == null)?"":request.getParameter("monto2");
String Hestatus        = (request.getParameter("Hestatus")     == null)?"":request.getParameter("Hestatus");
String fechaIF1        = (request.getParameter("fechaIF1")     == null)?"":request.getParameter("fechaIF1");
String fechaIF2        = (request.getParameter("fechaIF2")     == null)?"":request.getParameter("fechaIF2");
String tasa1           = (request.getParameter("tasa1")        == null)?"":request.getParameter("tasa1");
String tasa2           = (request.getParameter("tasa2")        == null)?"":request.getParameter("tasa2");
String digito          = (request.getParameter("digito")       == null)?"":request.getParameter("digito");
String oIF             = (request.getParameter("oIF")          == null)?"":request.getParameter("oIF");
String oPYME           = (request.getParameter("oPYME")        == null)?"":request.getParameter("oPYME");
String oEpo            = (request.getParameter("oEpo")         == null)?"":request.getParameter("oEpo");
String oFecha          = (request.getParameter("oFecha")       == null)?"":request.getParameter("oFecha");
String rfc             = (request.getParameter("rfc")          == null)?"":request.getParameter("rfc");
String mandante        = (request.getParameter("mandante")     == null)?"":request.getParameter("mandante");
String duplicado       = (request.getParameter("duplicado")    == null)?"":request.getParameter("duplicado"); //Fodea 38-2014
String validaDoctoEPO       = (request.getParameter("validaDoctoEPO")    == null)?"":request.getParameter("validaDoctoEPO");
String contratoCons       = (request.getParameter("contrato")    == null)?"":request.getParameter("contrato");
String copadeCons      = (request.getParameter("copade")    == null)?"":request.getParameter("copade");


String operaFideicomiso="";
if(infoProcesadaEn.equals("0"))infoProcesadaEn="";
if(infoProcesadaEn.equals("S")){operaFideicomiso="S";infoProcesadaEn="";}
String infoRegresar ="";
HashMap datos;
HashMap datosDoc=new HashMap();
JSONArray registros = new JSONArray();
JSONObject jsonObj = new JSONObject();

if (informacion.equals("CatologoIF")  ) {
	
	CatalogoIFDescuento catalogo = new CatalogoIFDescuento();
	catalogo.setCampoClave("ic_if");
	catalogo.setClaveEpo(ic_epo);
	catalogo.setFideicomiso(BeanParamDscto.getOperaFideicomisoEpo(ic_epo));//BeanParamDscto.getOperaFideicomisoEpo
	catalogo.setProducto("true");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setOrden("i.cg_razon_social");
	
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("catalogoBanco")  ) {
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_banco_fondeo");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("comcat_banco_fondeo");		
	catalogo.setOrden("2");
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("catalogoEpo")  ) {

		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setBancofondeo(banco);
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveIf(iNoCliente);
		List elementos = catalogo.getListaElementosModalidad();
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar = "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

}else if (	informacion.equals("CatalogoBuscaAvanzada") ) {

	String num_pyme    = (request.getParameter("num_pyme")    ==null)?"":request.getParameter("num_pyme");
	String rfc_pyme    = (request.getParameter("rfc_pyme")    ==null)?"":request.getParameter("rfc_pyme");
	String nombre_pyme = (request.getParameter("nombre_pyme") ==null)?"":request.getParameter("nombre_pyme");
	String ic_pymes    = (request.getParameter("ic_pyme")     ==null)?"":request.getParameter("ic_pyme");

	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);

	Registros regis = afiliacion.getProveedores("",ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pymes,"","");

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (regis.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(regis.getString("IC_PYME"));
		elementoCatalogo.setDescripcion(regis.getString("IC_NAFIN_ELECTRONICO")+" "+regis.getString("CG_RAZON_SOCIAL"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenNombrePyme")) {



	String numeroDeProveedor	= (request.getParameter("numeroDeProveedor")!=null)?request.getParameter("numeroDeProveedor"):"";

	Registros regis			= new Registros();

	String txtCadenasPymes		= "";

	

		regis					= BeanParamDscto.getParametrosPymeNafinSeleccionIF(numeroDeProveedor);

		if(regis != null && regis.next()){
			ic_pyme					= (regis.getString("PYME")	== null)?"":regis.getString("PYME");
			txtCadenasPymes		= (regis.getString("NOMBRE")	== null)?"":regis.getString("NOMBRE");
		}else{
			jsonObj.put("muestraMensaje", new Boolean(true));
			jsonObj.put("textoMensaje", 	"El nafin electrónico no corresponde a una<br>PyME afiliada a la EPO o no existe.");
		
	}

	jsonObj.put("ic_pyme",					ic_pyme									);
	jsonObj.put("txt_nafelec",				numeroDeProveedor						);
	jsonObj.put("txtCadenasPymes",		txtCadenasPymes						);
	jsonObj.put("estadoSiguiente",		"RESPUESTA_OBTENER_NOMBRE_PYME"	);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(informacion.equals("catalogoCuenta")){
	
	 ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Registros reg=BeanParamDscto.getCtasPyme(ic_pyme);
	List elementos=new ArrayList();
		JSONArray jsonArr = new JSONArray();
			while(reg.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(reg.getString("CLAVE"));
				elementoCatalogo.setDescripcion(reg.getString("DESCRIPCION"));
				elementos.add(elementoCatalogo);
			}		
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
			
		infoRegresar = "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
}else if (informacion.equals("catalogoFactoraje")  ) {
	
	String tiposFactorajes="N,V,D,C";
	if(!ic_epo.equals(""))
		tiposFactorajes+=BeanParamDscto.getParametrosFactoraje(ic_epo);
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("CC_TIPO_FACTORAJE");
	catalogo.setCampoDescripcion("CG_NOMBRE");
	catalogo.setTabla("COMCAT_TIPO_FACTORAJE");	
	catalogo.setValoresCondicionIn(tiposFactorajes,String.class);
	boolean banderaMandate=tiposFactorajes.indexOf("M")>0?true:false;
	if(ic_epo.equals(""))
		banderaMandate=true;
	catalogo.setOrden("CC_TIPO_FACTORAJE");
	List elementos = catalogo.getListaElementos();
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar = "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+", \"mandante\":"+banderaMandate+"}";
	
}else if (informacion.equals("catalogoMoneda")) {
	
	CatalogoMoneda catalogo = new CatalogoMoneda();
	catalogo.setCampoClave("IC_MONEDA");
	catalogo.setCampoDescripcion("CD_NOMBRE");
	
	infoRegresar = catalogo.getJSONElementos();	
	
}else if (informacion.equals("catalogoEstatus")  ) {
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_ESTATUS_DOCTO");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("COMCAT_ESTATUS_DOCTO");		
	catalogo.setOrden("2");
	if(!infoProcesadaEn.equals("5")&&!infoProcesadaEn.equals("8")&&!infoProcesadaEn.equals("9")){
		if(operaFideicomiso.equals("S")){
			String In="3,4,11,12,24,33";
			catalogo.setValoresCondicionIn(In,Integer.class);
		}else{
		String notIn="8,13,14,15,17,18,19,20,22,25,27,32";
		catalogo.setValoresCondicionNotIn(notIn,Integer.class);
		}
	}else{
		String In="3,4,11,33";
		catalogo.setValoresCondicionIn(In,Integer.class);
	}
	infoRegresar = catalogo.getJSONElementos();	
	
}else if (informacion.equals("camposAdicionales")){

	List nombresCamposAdicionales = new ArrayList();
if(!ic_epo.equals("")){nombresCamposAdicionales = BeanParamDscto.getNombresCamposAdicionales(ic_epo);}
int i = 0;
for( i = 0; i < nombresCamposAdicionales.size(); i++){
	jsonObj.put("CAMPO"+(i+1),nombresCamposAdicionales.get(i));
}
//i++;
jsonObj.put("success",new Boolean(true));
jsonObj.put("TOTALES",i+"");
infoRegresar=jsonObj.toString();

} else if(informacion.equals("Consultar")||informacion.equals("ArchivoCSV") ||informacion.equals("GenerarPDF")){

	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

	boolean esNotaDeCreditoAplicada                     = false;
	boolean esDocumentoConNotaDeCreditoMultipleAplicada = false;

	boolean esNotaDeCreditoSimpleAplicada               = false;
	boolean esDocumentoConNotaDeCreditoSimpleAplicada   = false;
	String numeroDocumento="";

	int numeroCamposAdicionales = 0;
	if(!ic_epo.equals("")){numeroCamposAdicionales = BeanParamDscto.getNumeroCamposAdicionales(ic_epo);}

	int start = 0;
	int limit = 0;
	boolean estaHabilitadoNumeroSIAFF = true;
	if(!digito.equals("")){
		String claveEPO = getClaveEPO(digito);
		if(!BeanParamDscto.estaHabilitadoNumeroSIAFF(claveEPO)){
			estaHabilitadoNumeroSIAFF = false;
		} else{
			estaHabilitadoNumeroSIAFF = true;
		}
	}

	ConsDoctosNafinDE paginador =new  ConsDoctosNafinDE();
	paginador.setIc_epo(ic_epo);
	paginador.setIc_if(icIF);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIg_numero_docto(documento);
	paginador.setDf_fecha_docto_de(fechaEm1);
	paginador.setDf_fecha_docto_a(fechaEm2);
	paginador.setDf_fecha_venc_de(fechaVenc1);
	paginador.setDf_fecha_venc_a(fechaVenc2);
	paginador.setIc_moneda(cbMoneda);
	paginador.setFn_monto_de(monto1);
	paginador.setFn_monto_a(monto2);
	paginador.setIc_estatus_docto(Hestatus);
	paginador.setDf_fecha_seleccion_de(fechaIF1);
	paginador.setDf_fecha_seleccion_a(fechaIF2);
	paginador.setIn_tasa_aceptada_de(tasa1);
	paginador.setIn_tasa_aceptada_a(tasa2);
	paginador.setOrd_if(oIF);
	paginador.setOrd_pyme(oPYME);
	paginador.setOrd_epo(oEpo);
	paginador.setOrd_fec_venc(oFecha);
	paginador.setIc_banco_fondeo(banco);
	paginador.setTipoFactoraje(factoraje);
	paginador.setPerfil(strPerfil);
	paginador.setNumero_siaff (digito);
	paginador.setEstaHabilitadoNumeroSIAFF(estaHabilitadoNumeroSIAFF);
	paginador.setInfoProcesadaEn(infoProcesadaEn);
	paginador.setPerfil(strPerfil);
	paginador.setOperaFideicomiso(operaFideicomiso);
	paginador.setMandanteDoc(mandante);
	paginador.setStrPerfil(strPerfil);
	paginador.setValidaDoctoEPO(validaDoctoEPO);
	paginador.setCopade(copadeCons);
	paginador.setContrato(contratoCons);	

	
	if(duplicado.equals("Si")){
		duplicado = "S";
	} else if(duplicado.equals("No")){
		duplicado = "N";
	} else{
		duplicado = "";
	}
	paginador.setDuplicado(duplicado);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	if (informacion.equals("Consultar")) { //Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e){
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
			Registros registrosC = queryHelper.getPageResultSet(request,start,limit);
			String nombreEpo                 = null;
			String nombrePyme                = null;
			String ig_numero_docto           = null;
			String df_fecha_docto            = null;
			String df_fecha_venc             = null;
			String moneda                    = null;
			String fn_monto                  = null;
			String fn_monto_dscto            = null;
			String cd_descripcion            = null;
			String ic_moneda                 = null;
			String fn_porc_anticipo          = null; 
			String ic_estatus_docto          = null;
			String ic_documento              = null;
			String cs_cambio_importe         = null;
			String nombreIf                  = null;
			String cs_dscto_especial         = null;
			String beneficiario              = null;
			String fn_porc_beneficiario      = null; 
			String recibir_benef             = null;
			String df_alta                   = null;
			String cc_acuse                  = null;
			String icEpo                     = null;
			String df_entrega                = null;
			String cg_tipo_compra            = null;
			String cg_clave_presupuestaria   = null;
			String cg_periodo                = null;
			String plazo                     = null;
			String fecha_solicitud           = null;
			String neto_a_recibir_pyme       = null;
			String rs_fn_porc_docto_aplicado = null;
			String rs_in_importe_interes     = null;
			String rs_in_importe_recibir     = null;
			String rs_fn_monto_pago          = null;
			String in_importe_depositar_pyme = null;
			String fn_remanente              = null;
			String tipo_factoraje            = null;
			String nombremandante            = null;
			String IN_TASA_ACEPTADA          = null;
			String detalles                  = null;
			String ig_tipo_piso              = null;
			String numeroSIAFF               = null;
			String fideicomiso               = null;
			String duplicadoGrid             = null; //Fodea 38-2014
			String acuseDocto                = null;
			String muestraVisor              = null;
			double dblRecurso                = 0d;
			boolean siaff;
			String validacionJR              = "";

			while (registrosC.next()) {
				
					duplicadoGrid           = (registrosC.getString("DUPLICADO")               ==null)?"":registrosC.getString("DUPLICADO"); //Fodea 38-2014
					nombreEpo               = (registrosC.getString("nombreEpo")               ==null)?"":registrosC.getString("nombreEpo");
					nombrePyme              = (registrosC.getString("nombrePyme")              ==null)?"":registrosC.getString("nombrePyme");
					ig_numero_docto         = (registrosC.getString("ig_numero_docto")         ==null)?"":registrosC.getString("ig_numero_docto");
					df_fecha_docto          = (registrosC.getString("DF_FECHA_DOCTO")          ==null)?"":registrosC.getString("DF_FECHA_DOCTO").trim();
					df_fecha_venc           = (registrosC.getString("DF_FECHA_VENC")           ==null)?"":registrosC.getString("DF_FECHA_VENC").trim();
					moneda                  = (registrosC.getString("MONEDA")                  ==null)?"":registrosC.getString("MONEDA");
					fn_monto                = (registrosC.getString("fn_monto")                ==null)?"":registrosC.getString("fn_monto");

					fn_monto_dscto          = (registrosC.getString("fn_monto_dscto")          ==null)?"":registrosC.getString("fn_monto_dscto");
					cd_descripcion          = (registrosC.getString("cd_descripcion")          ==null)?"":registrosC.getString("cd_descripcion");
					ic_moneda               = (registrosC.getString("ic_moneda")               ==null)?"":registrosC.getString("ic_moneda");

					fn_porc_anticipo        = (registrosC.getString("fn_porc_anticipo")        ==null)?"":registrosC.getString("fn_porc_anticipo");
					ic_estatus_docto        = (registrosC.getString("ic_estatus_docto")        ==null)?"":registrosC.getString("ic_estatus_docto");
					ic_documento            = (registrosC.getString("ic_documento")            ==null)?"":registrosC.getString("ic_documento");
					cs_cambio_importe       = (registrosC.getString("cs_cambio_importe")       ==null)?"":registrosC.getString("cs_cambio_importe");
					nombreIf                = (registrosC.getString("nombreIf")                ==null)?"":registrosC.getString("nombreIf");
					cs_dscto_especial       = (registrosC.getString("CS_DSCTO_ESPECIAL")       ==null)?"":registrosC.getString("CS_DSCTO_ESPECIAL");
					beneficiario            = (registrosC.getString("beneficiario")            ==null)?"":registrosC.getString("beneficiario");
					fn_porc_beneficiario    = (registrosC.getString("fn_porc_beneficiario")    ==null)?"":registrosC.getString("fn_porc_beneficiario");
					recibir_benef           = (registrosC.getString("RECIBIR_BENEF")           ==null)?"":registrosC.getString("RECIBIR_BENEF");
					df_alta                 = (registrosC.getString("df_alta")                 ==null)?"":registrosC.getString("df_alta");
					cc_acuse                = (registrosC.getString("cc_acuse")                ==null)?"":registrosC.getString("cc_acuse");
					icEpo                   = (registrosC.getString("ic_epo")                  ==null)?"":registrosC.getString("ic_epo");
					df_entrega              = (registrosC.getString("DF_ENTREGA")              ==null)?"":registrosC.getString("DF_ENTREGA");
					cg_tipo_compra          = (registrosC.getString("CG_TIPO_COMPRA")          ==null)?"":registrosC.getString("CG_TIPO_COMPRA");
					cg_clave_presupuestaria = (registrosC.getString("CG_CLAVE_PRESUPUESTARIA") ==null)?"":registrosC.getString("CG_CLAVE_PRESUPUESTARIA");
					cg_periodo              = (registrosC.getString("CG_PERIODO")              ==null)?"":registrosC.getString("CG_PERIODO");
					fideicomiso             = (registrosC.getString("FIDEICOMISO")             ==null)?"":registrosC.getString("FIDEICOMISO");
					acuseDocto              = (registrosC.getString("ACUSE_DOCTO")             ==null)?"":registrosC.getString("ACUSE_DOCTO");
					muestraVisor            = (registrosC.getString("MUESTRA_VISOR")             ==null)?"":registrosC.getString("MUESTRA_VISOR");
					if (datosDoc.containsKey(icEpo)) {
						 siaff = ((Boolean)datosDoc.get(icEpo)).booleanValue();
	
					}else {
	
						siaff = BeanParamDscto.estaHabilitadoNumeroSIAFF(icEpo);
						datosDoc.put(icEpo,new Boolean(siaff));
					}
	
					if(siaff){
						numeroSIAFF = getNumeroSIAFF(icEpo,ic_documento);
					}else{
						numeroSIAFF = "";
					}
					if (Hestatus.equals("3") || Hestatus.equals("4") || Hestatus.equals("11") || Hestatus.equals("12") || Hestatus.equals("16") || Hestatus.equals("24") || Hestatus.equals("26"))//FODEA 005 - 2009 ACF
					{
					IN_TASA_ACEPTADA          = (registrosC.getString("IN_TASA_ACEPTADA")   == null) ? "0" : registrosC.getString("IN_TASA_ACEPTADA");
					ig_tipo_piso              = (registrosC.getString("IG_TIPO_PISO")       == null) ? ""  : registrosC.getString("IG_TIPO_PISO");
					plazo                     = (registrosC.getString("IG_PLAZO")           == null) ? ""  : registrosC.getString("IG_PLAZO");
					fecha_solicitud           = (registrosC.getString("DF_FECHA_SOLICITUD") == null) ? ""  : registrosC.getString("DF_FECHA_SOLICITUD");
					neto_a_recibir_pyme       = (registrosC.getString("NETO_REC_PYME")      == null) ? ""  : registrosC.getString("NETO_REC_PYME");
					dblRecurso                = new Double(fn_monto).doubleValue() - new Double(fn_monto_dscto).doubleValue();
					rs_fn_porc_docto_aplicado = (registrosC.getString("porcDoctoAplicado")  == null) ? "0" : registrosC.getString("porcDoctoAplicado");
					rs_in_importe_interes     = (registrosC.getString("IN_IMPORTE_INTERES") == null) ? "0" : registrosC.getString("IN_IMPORTE_INTERES");
					rs_in_importe_recibir     = (registrosC.getString("in_importe_recibir") == null) ? "0" : registrosC.getString("in_importe_recibir"); 
					rs_fn_monto_pago          = (registrosC.getString("fn_monto_pago")      == null) ? "0" : registrosC.getString("fn_monto_pago"); 
					in_importe_depositar_pyme = (Double.parseDouble(rs_in_importe_recibir) - Double.parseDouble(rs_fn_monto_pago)) +"";
					fn_remanente              = (registrosC.getString("fn_remanente")       == null) ? ""  : registrosC.getString("fn_remanente");
					}
					tipo_factoraje = (registrosC.getString("TIPO_FACTORAJE")==null)?"":registrosC.getString("TIPO_FACTORAJE");
					nombremandante = (registrosC.getString("NOMBREMANDANTE")==null)?"":registrosC.getString("NOMBREMANDANTE");
					
					if (ic_estatus_docto.equals("2") || ic_estatus_docto.equals("5") || ic_estatus_docto.equals("6") ||
							  ic_estatus_docto.equals("7") || ic_estatus_docto.equals("9") || ic_estatus_docto.equals("10")){
						fn_porc_anticipo = registrosC.getString(8);
						fn_monto_dscto = new Double(new Double(fn_monto).doubleValue() * (Double.parseDouble(fn_porc_anticipo) / 100)).toString();
					}
					
					if(ic_estatus_docto.equals("16") ){
								if(ig_tipo_piso.equals("1")){
									if(!fn_remanente.equals("")){
										in_importe_depositar_pyme=fn_remanente;
									}else{
										in_importe_depositar_pyme="";
						}
								}else{
									in_importe_depositar_pyme=in_importe_depositar_pyme;
							  }
						}else{
							in_importe_depositar_pyme=in_importe_depositar_pyme;
						}
					
					if(cs_dscto_especial.equals("V")) {
						fn_monto_dscto = fn_monto;
						fn_porc_anticipo = 100+"";
					}
					
					dblRecurso = new Double(fn_monto).doubleValue() - new Double(fn_monto_dscto).doubleValue();
					// Fodea 002 - 2010
					esNotaDeCreditoSimpleAplicada 					= ( cs_dscto_especial.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(String.valueOf(ic_documento)) )?true:false;
					esDocumentoConNotaDeCreditoSimpleAplicada		= ( !cs_dscto_especial.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(String.valueOf(ic_documento)))?true:false;
	
					esNotaDeCreditoAplicada 							= ( cs_dscto_especial.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(String.valueOf(ic_documento)) )?true:false;
					esDocumentoConNotaDeCreditoMultipleAplicada	= ( !cs_dscto_especial.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(String.valueOf(ic_documento)))?true:false;
					
					detalles="";
					
					if(esNotaDeCreditoSimpleAplicada){
						numeroDocumento									= getNumeroDoctoAsociado(String.valueOf(ic_documento));
					}else if(esDocumentoConNotaDeCreditoSimpleAplicada){
						numeroDocumento									= getNumeroNotaCreditoAsociada(String.valueOf(ic_documento));
					}
					if(esNotaDeCreditoAplicada || esDocumentoConNotaDeCreditoMultipleAplicada){
						detalles="SI";
					}else
					if(esNotaDeCreditoSimpleAplicada || esDocumentoConNotaDeCreditoSimpleAplicada){
						detalles=numeroDocumento;
					}else{
						//pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					
					validacionJR   = (registrosC.getString("VALIDACION_JUR") == null) ? ""  : registrosC.getString("VALIDACION_JUR");
					String copade  = (registrosC.getString("CG_NUM_COPADE")==null)?"":registrosC.getString("CG_NUM_COPADE");
					String contrato = (registrosC.getString("CG_NUM_CONTRATO")==null)?"":registrosC.getString("CG_NUM_CONTRATO");
							
					
					datos = new HashMap();
					datos.put("NOMBREEPO", nombreEpo);
					datos.put("NOMBREPYME", nombrePyme);
					datos.put("IG_NUMERO_DOCTO", ig_numero_docto);
					datos.put("DF_FECHA_DOCTO", df_fecha_docto);
					datos.put("DF_FECHA_VENC", df_fecha_venc);
					datos.put("MONEDA", moneda);
					datos.put("FN_MONTO", fn_monto);
					datos.put("FN_MONTO_DSCTO", fn_monto_dscto);
					datos.put("CD_DESCRIPCION", cd_descripcion);
					datos.put("IC_MONEDA", ic_moneda);
					datos.put("FN_PORC_ANTICIPO",fn_porc_anticipo);
					datos.put("IC_ESTATUS_DOCTO", ic_estatus_docto);
					datos.put("IC_DOCUMENTO", ic_documento);
					datos.put("CS_CAMBIO_IMPORTE", cs_cambio_importe.trim());
					datos.put("NOMBREIF", nombreIf);
					datos.put("CS_DSCTO_ESPECIAL", cs_dscto_especial);
					datos.put("BENEFICIARIO", beneficiario);
					datos.put("FN_PORC_BENEFICIARIO", fn_porc_beneficiario.equals("0")?"":fn_porc_beneficiario);
					datos.put("RECIBIR_BENEF", recibir_benef);
					datos.put("DF_ALTA", df_alta);
					datos.put("CC_ACUSE", cc_acuse);
					datos.put("ICEPO", icEpo);
					datos.put("DF_ENTREGA", df_entrega);
					datos.put("CG_TIPO_COMPRA", cg_tipo_compra);//
					datos.put("CG_CLAVE_PRESUPUESTARIA", cg_clave_presupuestaria);
					datos.put("CG_PERIODO", cg_periodo.equals("0")?"":cg_periodo);
					datos.put("TIPO_FACTORAJE", tipo_factoraje);
					datos.put("NOMBREMANDANTE", nombremandante);
					datos.put("NUMEROSIAFF",numeroSIAFF);
					datos.put("IG_PLAZO",plazo);
					datos.put("IN_IMPORTE_RECIBIR",rs_in_importe_recibir);
					datos.put("IN_TASA_ACEPTADA",IN_TASA_ACEPTADA);
					datos.put("FN_MONTO_DOC",fn_monto);
					datos.put("DF_FECHA_SOLICITUD", fecha_solicitud);
					datos.put("NETO_REC_PYME", neto_a_recibir_pyme);//
					datos.put("PORCDOCTOAPLICADO", rs_fn_porc_docto_aplicado);
					datos.put("FN_MONTO_PAGO", rs_fn_monto_pago);
					datos.put("FN_REMANENTE", tipo_factoraje);
					datos.put("DBL_RECURSO",dblRecurso+"");
					datos.put("IN_IMPORTE_INTERES",rs_in_importe_interes);
					datos.put("IN_IMPORTE_DEPOSITAR_PYME",in_importe_depositar_pyme);
					datos.put("DETALLES",detalles);
					datos.put("FIDEICOMISO",fideicomiso);
					datos.put("DUPLICADO",duplicadoGrid); //Fodea 38-2014
					datos.put("ACUSE_DOCTO", acuseDocto);
					datos.put("MUESTRA_VISOR", muestraVisor);
					datos.put("VALIDACION_JUR", validacionJR);
					datos.put("CONTRATO", contrato);
					datos.put("COPADE", copade);
					
					if(!ic_epo.equals("")){
					List datosCamposAdicionales = new ArrayList();
					if(!ic_epo.equals("")){datosCamposAdicionales = BeanParamDscto.getDatosCamposAdicionales(ic_documento, ic_epo, numeroCamposAdicionales);}
						for(int i = 0; i < datosCamposAdicionales.size(); i++){
								datos.put("CAMPO"+(i+1), datosCamposAdicionales.get(i));
							}
						}
					registros.add(datos);		
					}
				
				jsonObj=JSONObject.fromObject(infoRegresar);
				jsonObj.remove("registros");
				jsonObj.put("registros",registros);

				infoRegresar=jsonObj.toString();
				if(!estaHabilitadoNumeroSIAFF){
					
					infoRegresar = "{\"success\": true, \"total\": 0, \"registros\": [], \"noAplica\": true }";
	
				}
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
			}
		}else if(informacion.equals("ArchivoCSV")){
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
				infoRegresar = jsonObj.toString();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}			
				
		}else if(informacion.equals("GenerarPDF")){
			//start = Integer.parseInt(request.getParameter("start"));
			//limit = Integer.parseInt(request.getParameter("limit"));
			//String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit,strDirectorioTemp,"PDF");
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
}else if(informacion.equals("Totales")){
	
	CQueryHelperRegExtJS queryHelperE = new CQueryHelperRegExtJS(new ConsDoctosNafinDE()); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelperE.getJSONResultCount(request);
	
} else if(informacion.equals("modificaMontos")){
	String claveDocumento = (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto");

	if(!claveDocumento.equals("")){
		Registros regModifica = new Registros();
		regModifica = BeanParamDscto.getModificacionMontos("", claveDocumento);
		jsonObj.put("success", new Boolean(true));
		if (regModifica != null){
			jsonObj.put("datosModifica", regModifica.getJSONData() );
		}
		infoRegresar = jsonObj.toString();

	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
}else if (informacion.equals("obtenAcuse")) {

	Registros regDatosAcuse = new Registros();
	Registros regDatosAcuse2 = new Registros();
	Registros regDatosAcuseTotal = new Registros();
	String cc_acuse = (request.getParameter("ccAcuse")==null)?"":request.getParameter("ccAcuse");
	
	if(!cc_acuse.equals("")){
		regDatosAcuse = BeanParamDscto.getDatosAcuse(cc_acuse);
		regDatosAcuse2 = BeanParamDscto.getDatosAcuse2(cc_acuse);
		regDatosAcuseTotal = BeanParamDscto.getDatosTotalesAcuse(cc_acuse);

	
		jsonObj.put("success", new Boolean(true));
		if (regDatosAcuse != null){
			jsonObj.put("datosAcuse", regDatosAcuse.getJSONData() );
		}
		if (regDatosAcuse2 != null){
			jsonObj.put("datosAcuse2", regDatosAcuse2.getJSONData() );
		}
		if (regDatosAcuseTotal != null){
			jsonObj.put("datosAcuseTotal", regDatosAcuseTotal.getJSONData() );
		}

		infoRegresar = jsonObj.toString();

	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("obtenAplica")) {

	String claveDocumento = (request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
	if(!claveDocumento.equals("")){
		List nombreColumnas = new ArrayList();
		List dataColumnas = new ArrayList();
		nombreColumnas.add("IG_NUMERO_DOCTO");
		nombreColumnas.add("IN_IMPORTE_RECIBIR");
		nombreColumnas.add("IC_NOMBRE");
		nombreColumnas.add("NOMBRE_IF");
		nombreColumnas.add("DF_FECHA_SELECCION");
		nombreColumnas.add("DF_OPERACION");
		nombreColumnas.add("IC_MENSUALIDAD");
		nombreColumnas.add("NUMERO_CREDITO");
		nombreColumnas.add("CG_TIPO_CONTRATO");
		nombreColumnas.add("IG_NUMERO_PRESTAMO");
		nombreColumnas.add("IMPORTE_APLICADO_CREDITO");
		nombreColumnas.add("SALDO_CREDITO");
		nombreColumnas.add("IC_MONEDA");
		nombreColumnas.add("ORDEN_APLICA");
		ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
		Vector vDetalleDocumentoAplicado = BeanSeleccionDocumento.ovgetDetalleDocumentoAplicado("", claveDocumento);
		Registros  regAplica = new Registros();
		regAplica.setNombreColumnas(nombreColumnas);
		regAplica.setData(vDetalleDocumentoAplicado);

		jsonObj.put("success", new Boolean(true));
		if (regAplica != null){
			jsonObj.put("datosAplica", regAplica.getJSONData() );
		}
		infoRegresar = jsonObj.toString();

	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
}else if (informacion.equals("ConsultarDetalle")  ||  informacion.equals("ConsultarTotalDetalle")  
	|| informacion.equals("ImpCsvDetalle") || informacion.equals("ImpPDFDetalle")   )   {

	String clavePyme = (request.getParameter("clavePyme") != null) ? request.getParameter("clavePyme") : "";
	String claveDocumento = (request.getParameter("claveDocumento") != null) ? request.getParameter("claveDocumento") : "";
	String  consulta ="", numeroDocto = "", producto ="",  nombreIF ="", fechaSeleccion = "", fechaOperacion ="", 	numMensualidad = "", numCredito = "", 
	tipoContrato = "", numPrestamo = "",  	monedaClave ="", nombreArchivo = "";		 
	int ordenAplicacion =0, numRegistrosMN  =0, numRegistrosDL=0, numRegistros  =0;
	double dblTotalSaldoCreditoPesos = 0, dblTotalSaldoCreditoDolares = 0,  dblTotalAplicadoCreditoPesos = 0,  dblTotalAplicadoCreditoDolares = 0, 
	dblImporteRecibir = 0, dblSaldoCredito=0, dblImporteAplicadoCredito  =0, dblImporteDepositarPymePesos=0, dblImporteDepositarPymeDolares =0;
	
	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	
	StringBuffer contenidoArchivo = new StringBuffer();			
	CreaArchivo creaArchivo = new CreaArchivo();
	ComunesPDF pdfDoc = new ComunesPDF();
	 datos = new HashMap();	
	if( informacion.equals("ImpPDFDetalle") )   {
		
		

		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	
		pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
		pdfDoc.addText("Detalle de los créditos pagados con el documento  ","font4",ComunesPDF.LEFT);
		pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);	
	}


	try {
		Vector vDetalleDocumentoAplicado = BeanSeleccionDocumento.ovgetDetalleDocumentoAplicado(clavePyme, claveDocumento);

		for (int i = 0; i < vDetalleDocumentoAplicado.size(); i++) {
			numRegistros++;
			ordenAplicacion++;
			Vector vDatosDetalleDocumentoAplicado = (Vector) vDetalleDocumentoAplicado.get(i);

			numeroDocto = vDatosDetalleDocumentoAplicado.get(0).toString();
			dblImporteRecibir = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(1).toString());
			producto = vDatosDetalleDocumentoAplicado.get(2).toString();
			nombreIF = vDatosDetalleDocumentoAplicado.get(3).toString();
			fechaSeleccion = vDatosDetalleDocumentoAplicado.get(4).toString();
			fechaOperacion = vDatosDetalleDocumentoAplicado.get(5).toString();
			numMensualidad = vDatosDetalleDocumentoAplicado.get(6).toString();
			numCredito = vDatosDetalleDocumentoAplicado.get(7).toString();
			tipoContrato = vDatosDetalleDocumentoAplicado.get(8).toString();
			numPrestamo = vDatosDetalleDocumentoAplicado.get(9).toString();
			dblImporteAplicadoCredito = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(10).toString());
			dblSaldoCredito = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(11).toString());
			monedaClave = vDatosDetalleDocumentoAplicado.get(12).toString();
			
			if (informacion.equals("ConsultarDetalle"))  {
				datos = new HashMap();
				datos.put("PRODUCTO", producto);
				datos.put("NOMBRE_IF", nombreIF);
				datos.put("ORDER_APLICACION", Double.toString (ordenAplicacion));
				datos.put("FECHA_APLICACION", fechaSeleccion);
				datos.put("FECHA_OPERACION", fechaOperacion);
				datos.put("NUMERO_MENSUALIDAD", numMensualidad);
				datos.put("NUMERO_PEDIDO", numCredito);
				datos.put("TIPO_CONTRATO", tipoContrato);			
				datos.put("NUMERO_PRESTAMO", numPrestamo);
				datos.put("IMPORTE_APLICADO", Double.toString (dblImporteAplicadoCredito));
				datos.put("SALDO_CREDITO", Double.toString (dblSaldoCredito));
				registros.add(datos);	
			}
			
			if( informacion.equals("ImpPDFDetalle") )  {
			
				if(numRegistros == 1) { //Establece encabezados
					pdfDoc.setTable(11,100);				
					pdfDoc.setCell("Número de documento","forma",ComunesPDF.CENTER, 3);
					pdfDoc.setCell(numeroDocto,"forma",ComunesPDF.CENTER, 3);
					pdfDoc.setCell("Número de documento","forma",ComunesPDF.CENTER, 3);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblImporteRecibir,2),"formas",ComunesPDF.RIGHT,2);	
					
					pdfDoc.setCell("Producto","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Orden de aplicación","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de aplicación","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de operacion crédito","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Número de mensualidad","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Número de pedido/disposición","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo de contrato","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Número de prestamo NAFIN","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe aplicado a crédito","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Saldo del crédito","celda01",ComunesPDF.CENTER);				
				}
				
				pdfDoc.setCell(producto,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(nombreIF,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(Double.toString (ordenAplicacion),"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(fechaSeleccion,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(fechaOperacion,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(numMensualidad,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(numCredito,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(tipoContrato,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(numPrestamo,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(Comunes.formatoDecimal(dblImporteAplicadoCredito,2),"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(Comunes.formatoDecimal(dblSaldoCredito,2),"forma",ComunesPDF.CENTER);	
								
			}
			
			if( informacion.equals("ImpCsvDetalle") )  {
				if(numRegistros == 1) { //Establece encabezados
					contenidoArchivo.append("Número de documento,Monto aplicado\n");
					contenidoArchivo.append(numeroDocto + "," + dblImporteRecibir + "\n\n\n");
					contenidoArchivo.append("Producto,IF,Orden de aplicación,Fecha de aplicación," +
													"Fecha de operacion crédito,Número de mensualidad," +
													"Número de pedido/disposición,Tipo de contrato,Número de prestamo NAFIN," +
													"Importe aplicado a crédito,Saldo del crédito\n");						
				}
			
			contenidoArchivo.append(producto + "," + 
								nombreIF.replace(',',' ') + "," + 
								ordenAplicacion + "," + 
								fechaSeleccion + ","+
								fechaOperacion + "," + 
								numMensualidad + "," + 
								numCredito + "," + 
								tipoContrato + "," + 
								numPrestamo + "," + 
								Comunes.formatoDecimal(dblImporteAplicadoCredito,2,false) + "," + 
								Comunes.formatoDecimal(dblSaldoCredito,2,false) + "," + 
								"\n");				
			}		
			
			if ("1".equals(monedaClave)){	//moneda nacional
				numRegistrosMN++;			
				dblTotalAplicadoCreditoPesos += dblImporteAplicadoCredito;
				dblTotalSaldoCreditoPesos += dblSaldoCredito;
				
			} else if("54".equals(monedaClave) ) {	//dolar
				numRegistrosDL++;
				dblTotalAplicadoCreditoDolares += dblImporteAplicadoCredito;
				dblTotalSaldoCreditoDolares += dblSaldoCredito;
			}	
			
		}//FOR 
		
		dblImporteDepositarPymePesos = dblImporteRecibir - dblTotalAplicadoCreditoPesos;
		dblImporteDepositarPymeDolares = dblImporteRecibir - dblTotalAplicadoCreditoDolares;
		if (dblImporteDepositarPymePesos <= 0) {
			dblImporteDepositarPymePesos = 0;
		}
			
		if (informacion.equals("ConsultarDetalle"))  {
			consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("clavePyme", clavePyme);	
			jsonObj.put("claveDocumento", claveDocumento);	
			jsonObj.put("monto", "$"+Comunes.formatoDecimal(dblImporteRecibir, 2));
			
		}
			
		if( informacion.equals("ImpCsvDetalle") )  {
			if (!creaArchivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
				out.print("<--!Error al generar el archivo-->");
			else{
				nombreArchivo = creaArchivo.nombre;
			}
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		
		if( informacion.equals("ImpPDFDetalle") )  {			
			
			if(numRegistrosMN>0){
				pdfDoc.setCell("Total Moneda Nacional","celda01",ComunesPDF.LEFT,3);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalAplicadoCreditoPesos, 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalSaldoCreditoPesos, 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("Importe a depositar de la PYME","celda01",ComunesPDF.LEFT,3);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblImporteDepositarPymePesos, 2),"formas",ComunesPDF.LEFT,8);	
			}
			if(numRegistrosDL>0){
				pdfDoc.setCell("Total Dólares","celda01",ComunesPDF.LEFT,3);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalAplicadoCreditoDolares, 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalSaldoCreditoDolares, 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("Importe a depositar de la PYME","celda01",ComunesPDF.LEFT,3);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( dblImporteDepositarPymeDolares, 2),"formas",ComunesPDF.LEFT,8);
			}
			pdfDoc.addTable();					
			pdfDoc.endDocument();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		
		if( informacion.equals("ConsultarTotalDetalle") )  {
		
			for(int i=0; i<2; i++){
			
				datos = new HashMap();
				if(i==0 && numRegistrosMN>0  ){					
					datos.put("MONEDA", "Total Moneda Nacional");
					datos.put("IMPORTE_PYME", Double.toString (dblTotalAplicadoCreditoPesos)  );
					datos.put("IMPORTE_APLICADO", Double.toString (dblTotalSaldoCreditoPesos));
					datos.put("SALDO_CREDITO", Double.toString (dblImporteDepositarPymePesos));						
				}			
				if(i==1&& numRegistrosDL>0  ){				
					datos.put("MONEDA", "Total Dólares");
					datos.put("IMPORTE_PYME", Double.toString (dblTotalAplicadoCreditoDolares)  );
					datos.put("IMPORTE_APLICADO", Double.toString (dblTotalSaldoCreditoDolares));
					datos.put("SALDO_CREDITO", Double.toString ( dblImporteDepositarPymeDolares));						
				}
		
			registros.add(datos);
			}		
			consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
		}
		
		
		infoRegresar=jsonObj.toString();	
	} catch (Exception e) {
		e.printStackTrace();
	}
	
}

%>

<%!
	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);
	}

	public String getNumeroDeCuatroDigitos(String numero){

		String 			claveEPO 	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
      }
      resultado.append(claveEPO);

      return resultado.toString();
  }

  	public String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
      }
      resultado.append(claveDocto);

      return resultado.toString();
  }

  public String getClaveEPO(String numeroSIAFF){
		BigInteger	icEPO 		= new BigInteger("-1");
		if(numeroSIAFF != null && !numeroSIAFF.equals("") && numeroSIAFF.length() == 15){
			try{
			icEPO 		= new BigInteger(numeroSIAFF.substring(0,4));
			}catch(Exception e){
				
			}
		}
		return icEPO.toString();
	}

%>
<%-- Fodea 002 - 2010 --%>
<%!
	public String getNumeroDoctoAsociado(String ic_nota_credito)
		throws Exception{

			System.out.println("13consulta2.jsp::getNumeroDoctoAsociado(E)");

			if(ic_nota_credito == null || ic_nota_credito.equals("")){
				System.out.println("13consulta2.jsp::getNumeroDoctoAsociado(S)");
				return "";
			}

			AccesoDB 		con 				= new AccesoDB();
			StringBuffer 	qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";

			try {

				con.conexionDB();
				qrySentencia.append(
					"SELECT             "  +
					"  IG_NUMERO_DOCTO  "  +
					"FROM               "  +
					"COM_DOCUMENTO      "  +
					"WHERE              "  +
					"	IC_DOCUMENTO IN  "  +
					"		(             "  +
					"			SELECT IC_DOCTO_ASOCIADO FROM COM_DOCUMENTO WHERE IC_DOCUMENTO = ? "  +
					"     )             ");

				lVarBind.add(new Integer(ic_nota_credito));

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("IG_NUMERO_DOCTO");
					resultado = (resultado == null)?"":resultado;
				}

		}catch(Exception e){
			System.out.println("13consulta2.jsp::getNumeroDoctoAsociado(Exception)");
			System.out.println("13consulta2.jsp::getNumeroDoctoAsociado.ic_nota_credito = <"+ic_nota_credito+">");
			e.printStackTrace();
			throw new Exception("No se pudo consultar el documento especificado.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13consulta2.jsp::getNumeroDoctoAsociado(S)");
		}

		return resultado;
	}

	public String getNumeroNotaCreditoAsociada(String ic_documento)
		throws Exception{

			System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada(E)");

			if(ic_documento == null || ic_documento.equals("")){
				System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada(S)");
				return "";
			}

			AccesoDB 		con 				= new AccesoDB();
			StringBuffer 	qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";

			try {

				con.conexionDB();
				qrySentencia.append(
					"SELECT                  "  +
					"  IG_NUMERO_DOCTO       "  +
					"FROM                    "  +
					"  COM_DOCUMENTO         "  +
					"WHERE                   "  +
					"  IC_DOCTO_ASOCIADO = ? ");

				lVarBind.add(new Integer(ic_documento));

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("IG_NUMERO_DOCTO");
					resultado = (resultado == null)?"":resultado;
				}

		}catch(Exception e){
			System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada(Exception)");
			System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada.ic_documento = <"+ic_documento+">");
			e.printStackTrace();
			throw new Exception("No se pudo consultar el documento especificado.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada(S)");
		}

		return resultado;
	}
%>

<%=infoRegresar%>
