<!DOCTYPE html>
<%@ page import="
			java.util.*, 
			javax.naming.*, 
			com.netro.descuento.*, 
			com.netro.exception.*" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%@ include file="/appComun.jspf" %>
<%@ include file="/extjs.jspf" %>
<%@ page 	contentType	= "text/html;charset=windows-1252"%>

<% String version = (String)session.getAttribute("version"); %>


<html:html>
<HEAD>
<title>Nafin@net</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">	
<%if(version!=null){%>
		<%@ include file="/01principal/menu.jspf"%>
<%}%>
<script language="JavaScript1.2" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="13consulta04ext.js?<%=session.getId()%>"></script>	
</HEAD>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%if(version!=null){%>
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<%}%>
		<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
   </div>
</div>
<%if(version!=null){%>
<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>
</body>

</html:html>