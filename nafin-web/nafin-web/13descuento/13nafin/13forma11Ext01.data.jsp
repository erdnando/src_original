<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, 
    com.netro.model.catalogos.*,
    com.netro.descuento.ParametrosIf,
    netropology.utilerias.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject "
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
	String informacion 	= (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String infoRegresar = "";

 if(informacion.equals("catalogoif")) {
  
  JSONObject jsonObj  = new JSONObject();
	CatalogoIF	catIf 	= new CatalogoIF();
	catIf.setClave("ic_if");
	catIf.setDescripcion("cg_razon_social");
	catIf.setG_orden("2");
	List lis = catIf.getListaElementosGral();
	jsonObj.put("registros", lis);
	jsonObj.put("success",  new Boolean(true)); 
  infoRegresar = jsonObj.toString();  
  
} else if(informacion.equals("Consultar")){
 
  String intermediario	= (request.getParameter("ic_if") 	!= null) ? request.getParameter("ic_if"):"";
  ParametrosIf param = new ParametrosIf();
  JSONObject jsonObj  = new JSONObject();
  param.setIcIf(intermediario);
  Registros   layout = param.layout();
  jsonObj.put("registros", layout.getJSONData() );
  infoRegresar = jsonObj.toString();
  
} else if(informacion.equals("grabar")) {
 
  String   numReg	              = (request.getParameter("numR") 	!= null) ? request.getParameter("numR"):"";
	String[] clavesIF		          = request.getParameterValues("clvIF");
  String[] layoutsCompletoVenc	= request.getParameterValues("lCompletoVenc");
	String[] layoutsCompletoOper	= request.getParameterValues("lCompletoOper");
  String[] layoutsDoctosVenc	  = request.getParameterValues("lDoctosVenc");	
  
  List listIf 	= Arrays.asList(clavesIF);
	List listCV 	= Arrays.asList(layoutsCompletoVenc);
	List listCO 	= Arrays.asList(layoutsCompletoOper);
	List listDV 	= Arrays.asList(layoutsDoctosVenc);
  
  JSONObject jsonObj  = new JSONObject();
  ParametrosIf param = new ParametrosIf();
  
  param.guardarParametrosLayout(listIf,listCV,listCO,listDV);
  jsonObj.put("success", new Boolean (true));
	infoRegresar = jsonObj.toString();
}


%>
<%=infoRegresar%>
