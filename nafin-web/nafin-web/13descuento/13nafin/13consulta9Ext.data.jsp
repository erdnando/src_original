<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	com.netro.model.catalogos.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
		com.netro.descuento.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoEPONafin,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>	
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String icGrupo= (request.getParameter("Hgrupo")!=null)?request.getParameter("Hgrupo"):"";
String infoRegresar ="";
JSONObject jsonObj = new JSONObject();
String nombreArchivo="";
AutorizacionSolicitud autSolEJB = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);
//String claveIF	= iNoCliente;
if (informacion.equals("catalogoIF") ) {

	CatalogoIF_Desc cat = new CatalogoIF_Desc();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setOrden("cg_razon_social");
	cat.setG_producto("1");

	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoEstatus") ) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_estatus_solic");
	cat.setCampoClave("ic_estatus_solic");
	cat.setCampoDescripcion("cd_descripcion");
	//cat.setOrden("2");
	cat.setValoresCondicionIn("1,2,3,4,12", Integer.class);
	
	infoRegresar = cat.getJSONElementos();
}
else if (informacion.equals("Consulta") ||informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")){

	int start = 0;
	int limit = 0;

	String ic_if           = (request.getParameter("ic_if")                !=null)?request.getParameter("ic_if"):"";
	String fechaOper       = (request.getParameter("df_fecha_operacion_de")!=null)?request.getParameter("df_fecha_operacion_de"):"";
	String noSirac         = (request.getParameter("ic_numero_sirac")      !=null)?request.getParameter("ic_numero_sirac"):"";
	String acuse           = (request.getParameter("ic_numero_acuse")      !=null)?request.getParameter("ic_numero_acuse"):"";
	String noPrestamo      = (request.getParameter("ic_numero_prestamo")   !=null)?request.getParameter("ic_numero_prestamo"):"";
	String fechaOperFin    = (request.getParameter("df_fecha_operacion_a") !=null)?request.getParameter("df_fecha_operacion_a"):"";
	String fechaEnvioIf    = (request.getParameter("df_fecha_envio_if_de") !=null)?request.getParameter("df_fecha_envio_if_de"):"";
	String fechaEnvioIfFin = (request.getParameter("df_fecha_envio_if_a")  !=null)?request.getParameter("df_fecha_envio_if_a"):"";
	String estatus         = (request.getParameter("ic_estatus")           !=null)?request.getParameter("ic_estatus"):"";

	OperCredEleNafinDE paginador =new  OperCredEleNafinDE();
	paginador.setIc_if(ic_if);
	paginador.setFechaOper(fechaOper);
	paginador.setNoSirac(noSirac);
	paginador.setAcuse(acuse);
	paginador.setNoPrestamo(noPrestamo);
	paginador.setFechaOperFin(fechaOperFin);
	paginador.setFechaEnvioIf(fechaEnvioIf);
	paginador.setFechaEnvioIfFin(fechaEnvioIfFin);
	paginador.setEstatus(estatus);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	queryHelper.setMultiplesPaginadoresXPagina(false);
	if (informacion.equals("Consulta")) {	//Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request);
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	} else if(informacion.equals("ArchivoCSV")){
		nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	} else if (informacion.equals("ArchivoPDF")) {
		//nombreArchivo=queryHelper.getCreatePageCustomFile(request,start,limit, strDirectorioTemp , "PDF");
		nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	}
} else if (informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion
} else if (informacion.equals("ArchivoDetPDF") ) {
	String claveSolicitud = (request.getParameter("claveSolicitud") == null)?"":request.getParameter("claveSolicitud");
	try{
		nombreArchivo = autSolEJB.getOperCredElecPDF(strDirectorioTemp,strDirectorioTemp,claveSolicitud);	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();
}

%>

<%=infoRegresar%>
