<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.CatalogoEPO,
	com.netro.model.catalogos.CatalogoPYME,
	com.netro.model.catalogos.CatalogoMoneda,
	com.netro.model.catalogos.CatalogoSimple,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%System.out.println("13consulta10.data.jsp (E)"); %>  
<%
String informacion         = (request.getParameter("informacion")         == null)?"" :request.getParameter("informacion");
String df_operacion_inicio = (request.getParameter("df_operacion_inicio") == null)?"" :request.getParameter("df_operacion_inicio");
String df_operacion_fin    = (request.getParameter("df_operacion_fin")    == null)?"" :request.getParameter("df_operacion_fin");
String operacion           = (request.getParameter("operacion")           == null)?"" :request.getParameter("operacion");
String paginaNo            = (request.getParameter("paginaNo")            == null)?"1":request.getParameter("paginaNo");
String nomArchivo          = (request.getParameter("nomArchivo")          == null)?"" :request.getParameter("nomArchivo");
String ic_banco_fondeo     = (request.getParameter("ic_banco_fondeo")     == null)?"" :request.getParameter("ic_banco_fondeo");  //FODEA 007 - 2009

if(ic_banco_fondeo.equals("")){ic_banco_fondeo = iTipoPerfil.equals("8")?"2":"1";} //FODEA 007 - 2009
String infoRegresar = "";
int start = 0;
int limit = 0;	
System.out.println("df_operacion_inicio---->"+df_operacion_inicio);
System.out.println("df_operacion_fin---->"+df_operacion_fin);
System.out.println("ic_banco_fondeo---->"+ic_banco_fondeo);
System.out.println("informacion---->"+informacion);

if (informacion.equals("CatalogoBanco")) {
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_BANCO_FONDEO");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("COMCAT_BANCO_FONDEO");
	catalogo.setOrden("IC_BANCO_FONDEO");
	infoRegresar = catalogo.getJSONElementos();

} else if (informacion.equals("Consulta")) { //Datos para la Consulta con Paginacion
	//_________________________ INICIO DEL PROGRAMA _________________________
	operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
		
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	System.out.println("start---->"+start);
	System.out.println("limit---->"+limit);

	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new com.netro.descuento.ConsRechDescNafinDE());

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}

		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		System.out.println("infoRegresar---->"+infoRegresar);
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}

} else if (informacion.equals("ResumenTotales")) {//Datos para el Resumen de Totales
	//Debe ser llamado despues de Consulta
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

} else if (informacion.equals("GENERA_PDF")){

	JSONObject jsonObj = new JSONObject();
	ConsRechDescNafinDE paginador = new ConsRechDescNafinDE();
	paginador.setIcBancoFondeo(ic_banco_fondeo);
	paginador.setDfOperacionInicio(df_operacion_inicio);
	paginador.setDfOperacionFin(df_operacion_fin);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo PDF. " + e);
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>

 <%System.out.println("13consulta10.data.jsp (S)"); %>
 