//******************************************************************************
function cambioSinRecurso(check, rowIndex, colIds){
	
  var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
    
    if (colIds === 1 && rowIndex === 0 ){
      reg.set('CS_LAYOUT_COMPLETO_VENC','S');
    }
    if (colIds === 1 && rowIndex === 1 ){
      reg.set('CS_LAYOUT_COMPLETO_OPER','S');
    }
    if (colIds === 5 ){
      reg.set('CS_LAYOUT_DOCTOS_VENC','S');
    }

	store.commitChanges();	
};

function cambioConRecurso(check, rowIndex, colIds){
	var  gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	
  if (colIds === 2 && rowIndex === 0  ){
      reg.set('CS_LAYOUT_COMPLETO_VENC','N');
    }
    if (colIds === 2 && rowIndex === 1  ){
      reg.set('CS_LAYOUT_COMPLETO_OPER','N');
    }
    if (colIds === 6 ){
      reg.set('CS_LAYOUT_DOCTOS_VENC','N');
    }
	store.commitChanges();	
};

//******************************************************************************


Ext.onReady(function() {

//---------------------------descargaInfo---------------------------------------
function descargaInfo(opts, success, response) {

  if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
      var infoR = Ext.util.JSON.decode(response.responseText);
        Ext.Msg.show({
          title: 'Grabar',
          msg: 'Par�metros guardados con �xito.',
          modal: true,
          icon: Ext.Msg.INFO,
          buttons: Ext.Msg.OK,
          fn: function (){ consultaData.load(); }
          });

  } else {
       NE.util.mostrarConnError(response, opts);
  }
};
//---------------------------fin descargaInfo-----------------------------------

//-----------------------------procesarConsultaData-----------------------------
var procesarConsultaData = function(store, arrRegistros, opts) 	{
  
var fp = Ext.getCmp('forma');
  fp.el.unmask();							
  var gridConsulta = Ext.getCmp('grid');	
  var el = gridConsulta.getGridEl();		
  if (arrRegistros != null) {
    if (!gridConsulta.isVisible()) {
      gridConsulta.show();
      }				
    if(store.getTotalCount() > 0) {		
      el.unmask();
    } else {		
      el.mask('No se encontr� ning�n registro', 'x-mask');				
    }
  }
};
//--------------------------Fin procesarConsultaData----------------------------


//---------------------------grabar---------------------------------------------
function grabar() {
  
  var gridConsulta = Ext.getCmp('grid');	
	var store = gridConsulta.getStore();
  
  var clavesIF = new Array();
  var layoutsCompletoVenc = new Array();
  var layoutsCompletoOper = new Array();
  var layoutsDoctosVenc = new Array();
  var numReg =0;
  
  var uno = "";
   var dos = "";
  
  store.each(function(record) {
 //  clavesIF.push(record.data['IC_IF']);
 uno = uno + record.data['CS_LAYOUT_COMPLETO_VENC'];
 dos = dos + record.data['CS_LAYOUT_COMPLETO_OPER'];
//   layoutsCompletoVenc.push(record.data['CS_LAYOUT_COMPLETO_VENC']);
  // layoutsCompletoOper.push(record.data['CS_LAYOUT_COMPLETO_OPER']);
//   layoutsDoctosVenc.push(record.data['CS_LAYOUT_DOCTOS_VENC']);
//   numReg++;
  });
 
  Ext.Ajax.request({
  url: '13forma12Ext01.data.jsp',
  params: {
      informacion: 'grabar',
      numR: numReg,
  //    clvIF: clavesIF,
      lCompletoVenc: uno,
      lCompletoOper: dos
    //  lDoctosVenc: layoutsDoctosVenc
  },
  callback: descargaInfo
  });


};
//---------------------------fin grabar-----------------------------------------
	
//-------------------------------ConsultaData-----------------------------------
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13forma12Ext01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
		//	{	name: 'IC_IF'},
			{	name: 'DESCRIPCION'},
			{	name: 'CS_LAYOUT_COMPLETO_VENC'},
			{	name: 'CS_LAYOUT_COMPLETO_OPER'},
//			{	name: 'CS_LAYOUT_DOCTOS_VENC'},
      { name: 'LCVESTATUS'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
//----------------------------Fin ConsultaData----------------------------------

//------------------------------------If----------------------------------------
var If = Ext.data.Record.create([ 
    {name: "clave", type: "string"}, 
    {name: "descripcion", type: "string"}, 
    {name: "loadMsg", type: "string"}
	]);
//---------------------------------Fin If---------------------------------------

//---------------------------- procesarIf---------------------------------------
	var procesarIf = function(store, arrRegistros, opts) {
		store.insert(0,new If({ 
      clave: "", 
      descripcion: "Todos los IF�s", 
      loadMsg: ""
    })); 
    
    Ext.getCmp('ic_if1').setValue("");
		store.commitChanges(); 
	}	
//----------------------------Fin procesarIf------------------------------------

//------------------------------Catalogo IF-------------------------------------
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '13forma11Ext01.data.jsp',
		baseParams: {
			informacion: 'catalogoif'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {	
			load: procesarIf,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
//------------------------------Fin Catalogo IF---------------------------------

//-------------------------------elementosForma---------------------------------	
	var elementosForma =  [	
		{
			xtype: 'combo',
			fieldLabel: 'IF',
			name: 'ic_if',
			id: 'ic_if1',		
			mode: 'local',			
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_if',
			autoLoad: false,
			forceSelection: true,
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoIF,
			tpl: NE.util.templateMensajeCargaCombo,
			anchor: '100%',
			width: 120
}];
//-----------------------------Fin elementosForma-------------------------------

//------------------------grupo-------------------------------------------------
var grupo = new Ext.ux.grid.ColumnHeaderGroup({
  rows: [
    [
      {header: 'Descripcion', colspan: 1, align: 'center'},
      {header: 'Desplegar archivo de vencimiento <br>con todos los campos de Sirac', colspan: 2, align: 'center'},
      {header: 'Desplegar archivo de operados <br>con todos los campos de Sirac', colspan: 2, align: 'center'}
     // {header: 'Desplegar numero de <br>documentos en vencimiento', colspan: 2, align: 'center'} 
    ]
  ]
});
//------------------------ fin grupo--------------------------------------------

//--------------------------------gridConsulta----------------------------------	
var gridConsulta = new Ext.grid.EditorGridPanel({	
		store				:consultaData,
		id					:'grid',
		margins			:'20 0 0 0',		
		style				:'margin:0 auto;',
		title				:'<center>Pagos de Cartera - Informaci�n Operativa </center>',	
    //plugins: grupo,
		hidden			:true,
		columns			:[
              {
              header:'',
              dataIndex : 'DESCRIPCION',
              width : 380,
              align: 'left',
              sortable: false
              },{
              header:'Si',
              dataIndex : 'LCVESTATUS',// 'CS_LAYOUT_COMPLETO_VENC',
              width : 100,
              align: 'center',
              sortable: false,
              renderer: function(value, metadata, record, rowIndex, colIndex, store){	
              	
                if(record.data['LCVESTATUS'] == ''){
                    if(record.data['CS_LAYOUT_COMPLETO_VENC']=='S' ){//  if(record.data['SINRECURSO']=='checked' ){
                   
                      return '<input  id="chkPRODUCTO1"   name="desco'+rowIndex+'"  type="Radio" checked  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }/*else{
                      return '<input  id="chkPRODUCTO1"  name="desco'+rowIndex+'"   type="Radio"  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }*/
                   else if(record.data['CS_LAYOUT_COMPLETO_OPER']=='S' ){//  if(record.data['SINRECURSO']=='checked' ){
                      return '<input  id="chkPRODUCTO1"   name="desco'+rowIndex+'"  type="Radio" checked  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }else{
                      return '<input  id="chkPRODUCTO1"  name="desco'+rowIndex+'"   type="Radio"  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }					
                }else{
                  if(record.data['LCVESTATUS'] == 'S'){
                    return '<input  id="chkPRODUCTO1"   name="desco'+rowIndex+'"  type="Radio" checked  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                  }else{
                    return '<input  id="chkPRODUCTO1"  name="desco'+rowIndex+'"   type="Radio"  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                  }
                }
      
              }
            },	{
              header:'No',
              dataIndex : 'CS_LAYOUT_COMPLETO_VENC', 
              width : 100,
              align: 'center',
              sortable: false,
              renderer: function(value, metadata, record, rowIndex, colIndex, store){				
                if(record.data['LCVESTATUS'] == ''){
                    if(record.data['CS_LAYOUT_COMPLETO_VENC']=='N' ){
                      return '<input  id="chkPRODUCTO4" type="Radio" name="desco'+rowIndex+'" checked  onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }/*else{
                      return '<input  id="chkPRODUCTO4" type="Radio"  name="desco'+rowIndex+'" onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }*/ 
                   else if(record.data['CS_LAYOUT_COMPLETO_OPER']=='N' ){
                      return '<input  id="chkPRODUCTO4" type="Radio" name="desco'+rowIndex+'" checked  onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }else{
                      return '<input  id="chkPRODUCTO4" type="Radio"  name="desco'+rowIndex+'" onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }			
                }else{
                  if(record.data['LCVESTATUS'] == 'S'){
                    return '<input  id="chkPRODUCTO4" type="Radio" name="desco'+rowIndex+'" checked  onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                  }else{
                    return '<input  id="chkPRODUCTO4" type="Radio"  name="desco'+rowIndex+'" onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                  }
                }
              }
            }/*,{
              header:'Si',
             // dataIndex : 'CS_LAYOUT_COMPLETO_OPER',
              width : 100,
              align: 'center',
              sortable: false,
              renderer: function(value, metadata, record, rowIndex, colIndex, store){					
                if(record.data['LCVESTATUS'] == ''){
                    if(record.data['CS_LAYOUT_COMPLETO_OPER']=='S' ){//  if(record.data['SINRECURSO']=='checked' ){
                      return '<input  id="chkOper"   name="op'+rowIndex+'"  type="Radio" checked  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }else{
                      return '<input  id="chkOper"  name="op'+rowIndex+'"   type="Radio"  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }					
                }else{
                  if(record.data['LCVESTATUS'] == 'S'){
                    return '<input  id="chkOper"   name="op'+rowIndex+'"  type="Radio" checked  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                  }else{
                    return '<input  id="chkOper"  name="op'+rowIndex+'"   type="Radio"  onclick="cambioSinRecurso(this, '+rowIndex +','+colIndex+');" />';
                  }
                }
      
              }
              },{
              header:'No',
            //  dataIndex : 'CS_LAYOUT_COMPLETO_OPER', 
              width : 100,
              align: 'center',
              sortable: false/*,
              renderer: function(value, metadata, record, rowIndex, colIndex, store){				
                if(record.data['LCVESTATUS'] == ''){
                    if(record.data['CS_LAYOUT_COMPLETO_OPER']=='N' ){
                      return '<input  id="noper" type="Radio" name="op'+rowIndex+'" checked  onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }else{
                      return '<input  id="noper" type="Radio"  name="op'+rowIndex+'" onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                    }		
                }else{
                  if(record.data['LCVESTATUS'] == 'S'){
                    return '<input  id="noper" type="Radio" name="op'+rowIndex+'" checked  onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                  }else{
                    return '<input  id="noper" type="Radio"  name="op'+rowIndex+'" onclick="cambioConRecurso(this, '+rowIndex +','+colIndex+');" />';
                  }
                }
              }
            }*/
		],			
		displayInfo		: true,		
		emptyMsg			: "No hay registros.",		
		loadMask			: true,
		stripeRows		: true,
		height			: 200,
    width				: 600,
		align				: 'center',
		frame				: true,
		bbar				: {
			store			: consultaData,
				emptyMsg		: "No hay registros.",
				items: ['->','-',
        {
        xtype: 'button',
					text: 'Grabar',					
					tooltip:	'Grabar',
					iconCls: 'icoGuardar',
					id: 'btnGrabar', 
					handler: function(boton, evento) {
          //Ext.getCmp('grid').hide();
            grabar();
          }
        } ,'-'
			]
		}
	});	
//-----------------------------Fin gridConsulta---------------------------------

//-------------------------------Panel Consulta---------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: '<center>Par�metros por IF</center>',
		frame: true,	
    hidden: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 50,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsulta',
				iconCls: 'icoBuscar',
				handler: function (boton, evento){
					fp.el.mask('Cargando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});
				}
			}
		]
	});
//------------------------------Fin Panel Consulta------------------------------

//----------------------------Contenedor Principal------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),	
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});	
//-----------------------------Fin Contenedor Principal-------------------------

//catalogoIF.load();
consultaData.load();

});//-----------------------------------------------Fin Ext.onReady(function(){}