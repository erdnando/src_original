<%@ page contentType="application/json;charset=UTF-8" errorPage="/00utils/error_extjs.jsp"%>
<%@ page import="java.sql.*,net.sf.json.JSONObject"%>
<%@ page import="java.math.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="netropology.utilerias.*"%>
<%@ page import="com.netro.descuento.*"%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%!public static final boolean SIN_COMAS = false;%>
<%
	String infoRegresar="";
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
%>
<%

String icGrupo= (request.getParameter("Hgrupo")!=null)?request.getParameter("Hgrupo"):"";
String icIF= (request.getParameter("Hif")!=null)?request.getParameter("Hif"):"";

String banco= (request.getParameter("Hbanco")!=null)?request.getParameter("Hbanco"):"";
String ic_epo									= (request.getParameter("HicEpo")==null)?"":request.getParameter("HicEpo");
String documento										= (request.getParameter("documento")==null)?"":request.getParameter("documento");
String naf_elec								= (request.getParameter("_txt_nafelec")==null)?"":request.getParameter("_txt_nafelec");
String ic_pyme								= (request.getParameter("hid_ic_pyme")==null)?"":request.getParameter("hid_ic_pyme");
String fechaEm1							= (request.getParameter("fechaEm1")==null)?"":request.getParameter("fechaEm1");
String fechaEm2							= (request.getParameter("fechaEm2")==null)?"":request.getParameter("fechaEm2");
String infoProcesadaEn= (request.getParameter("stat")!=null)?request.getParameter("stat"):"";
String factoraje= (request.getParameter("Hfactoraje")!=null)?request.getParameter("Hfactoraje"):"";
String fechaVenc1= (request.getParameter("fechaVenc1")!=null)?request.getParameter("fechaVenc1"):"";
String fechaVenc2= (request.getParameter("fechaVenc2")!=null)?request.getParameter("fechaVenc2"):"";
String cbMoneda= (request.getParameter("HcbMoneda")!=null)?request.getParameter("HcbMoneda"):"";
String monto1= (request.getParameter("monto1")!=null)?request.getParameter("monto1"):"";
String monto2							= (request.getParameter("monto2")==null)?"":request.getParameter("monto2");
String Hestatus							= (request.getParameter("Hestatus")==null)?"":request.getParameter("Hestatus");
String fechaIF1								= (request.getParameter("fechaIF1")==null)?"":request.getParameter("fechaIF1");
String fechaIF2						= (request.getParameter("fechaIF2")==null)?"":request.getParameter("fechaIF2");
String tasa1							= (request.getParameter("tasa1")==null)?"":request.getParameter("tasa1");
String tasa2							= (request.getParameter("tasa2")==null)?"":request.getParameter("tasa2");
String digito								= (request.getParameter("digito")==null)?"":request.getParameter("digito");
String oIF						= (request.getParameter("oIF")==null)?"":request.getParameter("oIF");
String oPYME							= (request.getParameter("oPYME")==null)?"":request.getParameter("oPYME");
String oEpo							= (request.getParameter("oEpo")==null)?"":request.getParameter("oEpo");
String oFecha						= (request.getParameter("oFecha")==null)?"":request.getParameter("oFecha");
String rfc										= (request.getParameter("rfc")==null)?"":request.getParameter("rfc");
String operaFideicomiso="";
if(infoProcesadaEn.equals("0"))infoProcesadaEn="";
if(infoProcesadaEn.equals("S")){operaFideicomiso="S";infoProcesadaEn="";}

boolean estaHabilitadoNumeroSIAFF = true;
	if(!digito.equals("")){
	String claveEPO = getClaveEPO(digito);
	if(!BeanParamDscto.estaHabilitadoNumeroSIAFF(claveEPO)){
		estaHabilitadoNumeroSIAFF = false;
	}else{
		estaHabilitadoNumeroSIAFF = true;
	}
}
ConsDoctosNafinDE paginador =new  ConsDoctosNafinDE();
	paginador.setIc_epo(ic_epo);
	paginador.setIc_if(icIF);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIg_numero_docto(documento);
	paginador.setDf_fecha_docto_de(fechaEm1);
	paginador.setDf_fecha_docto_a(fechaEm2);
	paginador.setDf_fecha_venc_de(fechaVenc1);
	paginador.setDf_fecha_venc_a(fechaVenc2);
	paginador.setIc_moneda(cbMoneda);
	paginador.setFn_monto_de(monto1);
	paginador.setFn_monto_a(monto2);
	paginador.setIc_estatus_docto(Hestatus);
	paginador.setDf_fecha_seleccion_de(fechaIF1);
	paginador.setDf_fecha_seleccion_a(fechaIF2);
	paginador.setIn_tasa_aceptada_de(tasa1);
	paginador.setIn_tasa_aceptada_a(tasa2);
	paginador.setOrd_if(oIF);
	paginador.setOrd_pyme(oPYME);
	paginador.setOrd_epo(oEpo);
	paginador.setOrd_fec_venc(oFecha);
	paginador.setIc_banco_fondeo(banco);
	paginador.setTipoFactoraje(factoraje);
	paginador.setPerfil(strPerfil);
	paginador.setNumero_siaff (digito);
	paginador.setEstaHabilitadoNumeroSIAFF(estaHabilitadoNumeroSIAFF);
	paginador.setInfoProcesadaEn(infoProcesadaEn);
	paginador.setPerfil(strPerfil);
	paginador.setOperaFideicomiso(operaFideicomiso);

String strGenerarArchivo = (request.getParameter("txtGeneraArchivo") == null) ? "" : request.getParameter("txtGeneraArchivo");
String numeroSIAFF	= (request.getParameter("digito")==null)?"":request.getParameter("digito");

//String banderaTablaDoctos	= (request.getParameter("banderaTablaDoctos")==null)?"":request.getParameter("banderaTablaDoctos");
String nombresDoctos = "";
strGenerarArchivo="GeneraArchivo";
//if(banderaTablaDoctos.equals("1")){
	nombresDoctos = ",Fecha de Recepción de Bienes y Servicios, Tipo de Compra (procedimiento), Clasificador por Objeto del Gasto, Plazo Máximo";
//}

//============================================================================>> FODEA 002 - 2009 (I)
List nombresCamposAdicionales = new ArrayList();
int numeroCamposAdicionales = 0;
String strCamposAdicionales = "";
if(!ic_epo.equals("")){numeroCamposAdicionales = BeanParamDscto.getNumeroCamposAdicionales(ic_epo);}
if(!ic_epo.equals("")){nombresCamposAdicionales = BeanParamDscto.getNombresCamposAdicionales(ic_epo);}
for(int i = 0; i < nombresCamposAdicionales.size(); i++){strCamposAdicionales += "," + nombresCamposAdicionales.get(i);}
//============================================================================>> FODEA 002 - 2009 (F)
//Variables de uso local
Registros rs = null;
int registros = 0, numRegistros = 0, numRegistrosMN = 0, numRegistrosDL = 0;
BigDecimal montoTotalMN = new BigDecimal("0.00");
BigDecimal montoTotalDL = new BigDecimal("0.00");
BigDecimal montoTotalDsctoMN = new BigDecimal("0.00");
BigDecimal montoTotalDsctoDL = new BigDecimal("0.00");

CreaArchivo archivo = new CreaArchivo();
String linea = "", nombreArchivo = "";
StringBuffer contenidoArchivo = new StringBuffer();
OutputStreamWriter writer = null;
BufferedWriter buffer = null;
/*
if(strPerfil.equals("PROMO NAFIN")){
	contenidoArchivo.append("Nombre EPO,PYME,IF,R.F.C,Num. Documento,Fecha emision,Fecha vencimiento,Moneda,Tipo Factoraje,Monto,Monto Descontar,Estatus,Porcentaje Descuento,Recurso Garantia,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario,Fecha para Efectuar Descuento,Fecha Alta Docto.,Digito Identificador"+nombresDoctos+strCamposAdicionales+"\n");
}else{
	contenidoArchivo.append("Nombre EPO,PYME,IF,Num. Documento,Fecha emision,Fecha vencimiento,Moneda,Tipo Factoraje,Monto,Monto Descontar,Estatus,Porcentaje Descuento,Recurso Garantia,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario,Fecha para Efectuar Descuento,Fecha Alta Docto.,Digito Identificador"+nombresDoctos+strCamposAdicionales+"\n");
}
*/
int vtmn = 0, vesp = 0, vnumd = 0;

String NOM_EPO="", NOM_PYME="", NOM_IF="", IG_NUMERO_DOCTO="", DF_FECHA_DOCTO="", DF_FECHA_VENC="", RFC="";
String NOM_MONEDA="", FN_MONTO="", FN_MONTO_DSCTO="", IN_TASA_ACEPTADA="", ESTATUS_DOCTO="";
String DF_ALTA="";
String IC_EPO="";
String CC_ACUSE="";
double dblPorcentaje = 0, dblRecurso = 0; int IC_MONEDA;
double montoPago = 0;
String FECHA_SOLICITUD="", strEstatus = "", IC_DOCUMENTO = "", CS_CAMBIO_IMPORTE="", Parametros = "";
String nombreTipoFactoraje="";
String tipoFactorajeDesc ="";
String tasa = "";
String plazo = "";
String porc_docto_aplicado = "";

String beneficiario = "";
double importe_recibir = 0;
double importe_depositar_pyme = 0;
double importe_interes = 0;

int porciento_beneficiario = 0;
double importe_a_recibir_beneficiario = 0;
double neto_a_recibir_pyme = 0;
String cc_acuse="";
String numeroPedido = "";
String nomArchivo = "";
String FECHA_ENTREGA = "", TIPO_COMPRA = "", CLAVE_PRESUPUESTARIA = "", PERIODO = "";
String fideicomiso = "";
HashMap datos = new HashMap();
boolean siaff = false;

// Fodea 002 - 2010
boolean esNotaDeCreditoAplicada						= false;
boolean esDocumentoConNotaDeCreditoMultipleAplicada	= false;

boolean esNotaDeCreditoSimpleAplicada						= false;
boolean esDocumentoConNotaDeCreditoSimpleAplicada		= false;
String  numeroDocumento											= "";

String fecha_registro_operacion = "";//FODEA 005 - 2009 ACF

AccesoDB con = new AccesoDB();
CQueryHelperRegExtJS queryHelper = null;
try{
	con.conexionDB();
	queryHelper = new CQueryHelperRegExtJS(paginador);
	if("GeneraArchivo".equals(strGenerarArchivo)){
		rs = queryHelper.doSearch();
	}
	else{
		con.cierraConexionDB();
	}
	
//Fodea 057-Mejoras III
String NombreMandante = "";
	nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
	writer = new OutputStreamWriter(new FileOutputStream(strDirectorioTemp + nombreArchivo, true),"ISO-8859-1");
	buffer = new BufferedWriter(writer);
	/* 											No Negociable 			Negociable						Eliminacion EPO					Descuento Fisico			Pagado Anticipado				Vencido sin Operar				Pagado sin Operar					Bloqueado                                                                       */
	if(Hestatus.equals("") || Hestatus.equals("1") || Hestatus.equals("2") || Hestatus.equals("5") || Hestatus.equals("6") || Hestatus.equals("7") || Hestatus.equals("9") || Hestatus.equals("10") || Hestatus.equals("21")|| Hestatus.equals("23")|| Hestatus.equals("28") || Hestatus.equals("29")|| Hestatus.equals("30")|| Hestatus.equals("31"))	{//FODEA 005 - 2009 ACF
		//rs = con.queryDB(qrySentencia);
		if(strPerfil.equals("PROMO NAFIN")){
			contenidoArchivo.append("Nombre EPO,PYME,IF,FIDEICOMISO,R.F.C,Num. Documento,Fecha emision,Fecha vencimiento,Moneda,Tipo Factoraje,Monto,Monto Descontar,Estatus,Porcentaje Descuento,Recurso Garantia,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario,Fecha para Efectuar Descuento,Fecha Alta Docto.,Digito Identificador"+nombresDoctos+strCamposAdicionales+"\n");
		}else{
		contenidoArchivo.append("Nombre EPO,PYME,IF,FIDEICOMISO,Num. Documento,Fecha emision,Fecha vencimiento,Moneda,Tipo Factoraje,Monto,Monto Descontar,Estatus,Porcentaje Descuento,Recurso Garantia,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario,Fecha para Efectuar Descuento,Fecha Alta Docto.,Digito Identificador"+nombresDoctos+strCamposAdicionales+",");
		contenidoArchivo.append("Mandante"+"\n");
		}

			
		String df_programacion = "";
		while(rs.next()){
			NOM_EPO = (rs.getString(1)==null)?"":rs.getString(1);
			NOM_PYME = (rs.getString(2) == null)?"":rs.getString(2);
			IG_NUMERO_DOCTO = (rs.getString(3)==null)?"":rs.getString(3);
			DF_FECHA_DOCTO = (rs.getString(4)==null)?"":rs.getString(4);
			DF_FECHA_VENC = (rs.getString(5)==null)?"":rs.getString(5);
			NOM_MONEDA = (rs.getString(6)==null)?"":rs.getString(6);
			IC_MONEDA = Integer.parseInt(rs.getString(11));
			FN_MONTO = (rs.getString(7)==null)?"0.00":rs.getString(7);
			dblPorcentaje = Double.parseDouble(rs.getString(12));
			strEstatus = (rs.getString(13)==null)?"":rs.getString(13);
			FN_MONTO_DSCTO = (rs.getString(9)==null)?"0.00":rs.getString(9);
			ESTATUS_DOCTO = (rs.getString(10)==null)?"":rs.getString(10);
			IC_DOCUMENTO = (rs.getString(14)==null)?"":rs.getString(14);
			CS_CAMBIO_IMPORTE = (rs.getString(15)==null)?"":rs.getString(15);
			NOM_IF = (rs.getString(16)==null)?"":rs.getString(16).trim();
			RFC = (rs.getString(23)==null)?"":rs.getString(23).trim();
			DF_ALTA 		= (rs.getString("DF_ALTA")==null)?"":rs.getString("DF_ALTA").trim();
			IC_EPO 		= (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO").trim();
			CC_ACUSE 	= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE").trim();
			NombreMandante = (rs.getString("NOMBREMANDANTE")==null)?"":rs.getString("NOMBREMANDANTE").trim();
			fideicomiso	= (rs.getString("FIDEICOMISO")==null)?"":rs.getString("FIDEICOMISO").trim();
			//si hay bandera de documentos se recuperan los valores
//			if(banderaTablaDoctos.equals("1")){
				FECHA_ENTREGA 					= (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
				TIPO_COMPRA 						= (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
				CLAVE_PRESUPUESTARIA 		= (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
				PERIODO 								= (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
//			}

					////*****1	
					
				if (datos.containsKey(IC_EPO)) {				 
					 siaff = ((Boolean)datos.get(IC_EPO)).booleanValue();
				
				}else {	
										
					siaff = BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO);
					datos.put(IC_EPO,new Boolean(siaff));
				}
				  
				if(siaff){
					numeroSIAFF = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);											
				}else{
					numeroSIAFF = "";
				}
				
				/*
				if(BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO)){				
					numeroSIAFF = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);				
				}else{
					numeroSIAFF = "";
					}
				*/
			
				
		
			beneficiario = (rs.getString(18)==null)?"":rs.getString(18).trim();
			porciento_beneficiario =Integer.parseInt(rs.getString(19));
			importe_a_recibir_beneficiario = Double.parseDouble(rs.getString(20));

			if(strEstatus.equals("2") || strEstatus.equals("5") || strEstatus.equals("6") ||
				strEstatus.equals("7") || strEstatus.equals("9") || strEstatus.equals("10")){
					dblPorcentaje = Double.parseDouble(rs.getString(8));
					FN_MONTO_DSCTO = new Double(new Double(FN_MONTO).doubleValue() * (dblPorcentaje / 100)).toString();
			}
			
			tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
			nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
			if(nombreTipoFactoraje.equals("V")) {
				FN_MONTO_DSCTO = FN_MONTO;
				dblPorcentaje = 100;
			}
			
/*FODEA 005 - 2009 ACF
			if (strEstatus.equals("26")){//26 = Programado foda 025-2005
				df_programacion = (rs.getString("df_programacion")==null)?"":rs.getString("df_programacion").trim();
				dblPorcentaje = rs.getDouble(8);
				FN_MONTO_DSCTO = new Double(new Double(FN_MONTO).doubleValue() * (dblPorcentaje / 100)).toString();
			}
*/
//			nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
			tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
			
			/*
			if("N".equals(nombreTipoFactoraje))
				nombreTipoFactoraje = "Normal";
			else if("V".equals(nombreTipoFactoraje))
				nombreTipoFactoraje = "Vencido";
			else if("D".equals(nombreTipoFactoraje))
				nombreTipoFactoraje = "Distribuido";
			else if("C".equals(nombreTipoFactoraje))
				nombreTipoFactoraje = "Nota de Crédito";
			*/
			
			dblRecurso = new Double(FN_MONTO).doubleValue() - new Double(FN_MONTO_DSCTO).doubleValue();
			if(IC_MONEDA == 54) {//Dólar
				montoTotalDL = montoTotalDL.add(new BigDecimal(FN_MONTO));
				montoTotalDsctoDL = montoTotalDsctoDL.add(new BigDecimal(FN_MONTO_DSCTO));
				numRegistrosDL++;
			} else if (IC_MONEDA == 1) {
				montoTotalMN = montoTotalMN.add(new BigDecimal(FN_MONTO));
				montoTotalDsctoMN = montoTotalDsctoMN.add(new BigDecimal(FN_MONTO_DSCTO));
				numRegistrosMN++;
			}
			registros++;
			/*GENERAMOS EL ARCHIVO*/
      if(strPerfil.equals("PROMO NAFIN")){
			contenidoArchivo.append( "\""+NOM_EPO.replace(',',' ')+"\",\""+NOM_PYME.replace(',',' ')+"\",\""+NOM_IF.replace(',',' ')+"\",\""+fideicomiso.replaceAll(",","")+"\",\""+RFC+"\","+IG_NUMERO_DOCTO+","
				+DF_FECHA_DOCTO+","+DF_FECHA_VENC+","+NOM_MONEDA+","+tipoFactorajeDesc+","+FN_MONTO+","
				+FN_MONTO_DSCTO+","+ESTATUS_DOCTO+","+Comunes.formatoDecimal(dblPorcentaje,0,false)+","+dblRecurso);
			}else{
      contenidoArchivo.append("\""+NOM_EPO.replace(',',' ')+"\",\""+NOM_PYME.replace(',',' ')+"\",\""+NOM_IF.replace(',',' ')+"\",\""+fideicomiso.replaceAll(",","")+"\","+IG_NUMERO_DOCTO+","
				+DF_FECHA_DOCTO+","+DF_FECHA_VENC+","+NOM_MONEDA+","+tipoFactorajeDesc+","+FN_MONTO+","
				+FN_MONTO_DSCTO+","+ESTATUS_DOCTO+","+Comunes.formatoDecimal(dblPorcentaje,0,false)+","+dblRecurso);
      }

			if(!"".equals(beneficiario)) contenidoArchivo.append(","+beneficiario.replace(',',' ')); else contenidoArchivo.append(",");
			if(0 != porciento_beneficiario) contenidoArchivo.append(","+porciento_beneficiario); else contenidoArchivo.append(",");
			if(0 != importe_a_recibir_beneficiario) contenidoArchivo.append(","+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false)); else contenidoArchivo.append(",");

			contenidoArchivo.append("," + df_programacion + ","+DF_ALTA+","+numeroSIAFF);

			//si hay bandera de documentos se recuperan los valores
//			if(banderaTablaDoctos.equals("1")){
				PERIODO=PERIODO.equals("0")?"":PERIODO;
				contenidoArchivo.append("," + FECHA_ENTREGA + "," + TIPO_COMPRA + "," + CLAVE_PRESUPUESTARIA + "," + PERIODO);
//			}
//============================================================================>> FODEA 002 - 2009 (I)
			if(!ic_epo.equals("")){
				List datosCamposAdicionales = new ArrayList();
				if(!ic_epo.equals("")){datosCamposAdicionales = BeanParamDscto.getDatosCamposAdicionales(IC_DOCUMENTO, ic_epo, numeroCamposAdicionales);}
				for(int i = 0; i < numeroCamposAdicionales; i++){
					contenidoArchivo.append("," + datosCamposAdicionales.get(i));
				}
			}
			
			contenidoArchivo.append(","+NombreMandante);
			
//============================================================================>> FODEA 002 - 2009 (F)		
		  contenidoArchivo.append("\n");
			
			buffer.write(contenidoArchivo.toString());	
		contenidoArchivo = new StringBuffer();//Limpio
			//contenidoArchivo = contenidoArchivo+linea;
			//System.out.println(registros);
		} // while

		
	} //if estatus="",2,5,6,7,9,10

	/*			Seleccionada Pyme					Operada						Operada Pagada				Operada Pendiente de Pago        Aplicado a Credito            En Proceso de Autorización IF		Programado Pyme*/
	if(Hestatus.equals("3") || Hestatus.equals("4") || Hestatus.equals("11") || Hestatus.equals("12") || Hestatus.equals("16") || Hestatus.equals("24") || Hestatus.equals("26")){//FODEA 005 - 2009 ACF
		//rs = con.queryDB(qrySentencia);
    System.out.println("***************************************************************************************"+strPerfil);
    if(strPerfil.equals("PROMO NAFIN")){
    		contenidoArchivo.append("Nombre EPO,PYME,IF,FIDEICOMISO,R.F.C,Num. Documento,Num. Acuse," +
				"Fecha emision,Fecha vencimiento,Plazo,Moneda,Tipo Factoraje," +
				"Monto,Porcentaje descuento,Recurso en garantia, Monto Descontar," +
				"Intereses del documento,Monto a recibir,Tasa,Estatus," +
				"Importe aplicado a credito,Porcentaje del documento aplicado," +
				"Importe a depositar a la Pyme,");
    }else{
        contenidoArchivo.append("Nombre EPO,PYME,IF,FIDEICOMISO,Num. Documento,Num. Acuse," +
				"Fecha emision,Fecha vencimiento,Plazo,Moneda,Tipo Factoraje," +
				"Monto,Porcentaje descuento,Recurso en garantia, Monto Descontar," +
				"Intereses del documento,Monto a recibir,Tasa,Estatus," +
				"Importe aplicado a credito,Porcentaje del documento aplicado," +
				"Importe a depositar a la Pyme,");
    }
//============================================================================>> FODEA 002 - 2009 (I)
		if (Hestatus.equals("4") || Hestatus.equals("11") || Hestatus.equals("12")){
			contenidoArchivo.append("Fecha Aut. IF,");
		}
		contenidoArchivo.append("Neto a Recibir,Doctos Aplicados a Nota de Credito,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario,Fecha Alta Docto.,Digito Identificador"+nombresDoctos+"");
		contenidoArchivo.append(strCamposAdicionales);
		if(Hestatus.equals("26")){contenidoArchivo.append(",Fecha Registro Operación");}//FODEA 005 - 2009 ACF
		contenidoArchivo.append( "\n");
//============================================================================>> FODEA 002 - 2009 (F)		
		String ig_tipo_piso="",fn_remanente="";
		while (rs.next()){
			NOM_EPO = (rs.getString("NOMBREEPO") == null) ? "" : rs.getString("NOMBREEPO");
			NOM_PYME = (rs.getString("NOMBREPYME") == null) ? "" : rs.getString("NOMBREPYME");
			NOM_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
			IG_NUMERO_DOCTO = (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
			cc_acuse = rs.getString("CC_ACUSE");
			DF_FECHA_DOCTO = (rs.getString("DF_FECHA_DOCTO") == null) ? "" : rs.getString("DF_FECHA_DOCTO");
			DF_FECHA_VENC = (rs.getString("DF_FECHA_VENC") == null) ? "" : rs.getString("DF_FECHA_VENC");
			plazo = (rs.getString("IG_PLAZO") == null) ? "" : rs.getString("IG_PLAZO");
			NOM_MONEDA = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
			IC_MONEDA = Integer.parseInt(rs.getString("IC_MONEDA"));
			FN_MONTO = (rs.getString("FN_MONTO") == null) ? "0.00" : rs.getString("FN_MONTO");
			dblPorcentaje =Double.parseDouble(rs.getString("FN_PORC_ANTICIPO"));
			FN_MONTO_DSCTO = (rs.getString("FN_MONTO_DSCTO")==null)?"0.00":rs.getString("FN_MONTO_DSCTO");
			importe_interes =Double.parseDouble(rs.getString("IN_IMPORTE_INTERES")); 
			importe_recibir =Double.parseDouble(rs.getString("IN_IMPORTE_RECIBIR")); 
			IN_TASA_ACEPTADA = (rs.getString("IN_TASA_ACEPTADA") == null) ? "" : rs.getString("IN_TASA_ACEPTADA");
			ESTATUS_DOCTO = (rs.getString("CD_DESCRIPCION") == null) ? "" : rs.getString("CD_DESCRIPCION");
			FECHA_SOLICITUD = (rs.getString("DF_FECHA_SOLICITUD") == null) ? "" : rs.getString("DF_FECHA_SOLICITUD");
			IC_DOCUMENTO = (rs.getString("IC_DOCUMENTO") == null) ? "" : rs.getString("IC_DOCUMENTO");
			CS_CAMBIO_IMPORTE = (rs.getString("CS_CAMBIO_IMPORTE") == null) ? "" : rs.getString("CS_CAMBIO_IMPORTE");
			porc_docto_aplicado	= (rs.getString("PORCDOCTOAPLICADO") == null) ? "" : rs.getString("porcDoctoAplicado");
			montoPago =Double.parseDouble(rs.getString("FN_MONTO_PAGO")); 
			beneficiario = (rs.getString("BENEFICIARIO")==null)?"":rs.getString("BENEFICIARIO").trim();
			porciento_beneficiario =Integer.parseInt(rs.getString("FN_PORC_BENEFICIARIO"));  
			importe_a_recibir_beneficiario =Double.parseDouble(rs.getString("RECIBIR_BENEF"));
			neto_a_recibir_pyme =Double.parseDouble(rs.getString("NETO_REC_PYME")); 
			DF_ALTA = (rs.getString("DF_ALTA")==null)?"":rs.getString("DF_ALTA").trim();
      RFC = (rs.getString("RFC")==null)?"":rs.getString("RFC").trim();
			ig_tipo_piso = (rs.getString("ig_tipo_piso")==null)?"":rs.getString("ig_tipo_piso");
			fn_remanente = (rs.getString("fn_remanente")==null)?"":rs.getString("fn_remanente");
			// Factoraje Vencido
			tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
			nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
			IC_EPO 		= (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO").trim();
			fecha_registro_operacion = rs.getString("fecha_registro_operacion")==null?"":rs.getString("fecha_registro_operacion");//FODEA 005 - 2009 ACF
			///*******2
			fideicomiso	= (rs.getString("FIDEICOMISO")==null)?"":rs.getString("FIDEICOMISO").trim();

			if(porc_docto_aplicado.equals("0")){
				porc_docto_aplicado="";
			}
			
				if (datos.containsKey(IC_EPO)) {				 
					 siaff = ((Boolean)datos.get(IC_EPO)).booleanValue();
				
				}else {	
										
					siaff = BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO);
					datos.put(IC_EPO,new Boolean(siaff));
				}
				
				if(siaff){
					numeroSIAFF = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);						
				}else{
					numeroSIAFF = "";
				}
				
				/*	
				if(BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO)){				
					numeroSIAFF = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);						
				}else{
						numeroSIAFF = "";						
				}
				*/

			//si hay bandera de documentos se recuperan los valores
//			if(banderaTablaDoctos.equals("1")){
				FECHA_ENTREGA 					= (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
				TIPO_COMPRA 						= (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
				CLAVE_PRESUPUESTARIA 		= (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
				PERIODO 								= (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
//			}
				PERIODO=PERIODO.equals("0")?"":PERIODO;
/*			if(nombreTipoFactoraje.equals("V")) {
				FN_MONTO_DSCTO = FN_MONTO;
				dblPorcentaje = 100;
			}
*/
//			nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
			tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
			
			/*
			if("N".equals(nombreTipoFactoraje))
				nombreTipoFactoraje = "Normal";
			else if("V".equals(nombreTipoFactoraje))
				nombreTipoFactoraje = "Vencido";
			else if("D".equals(nombreTipoFactoraje))
				nombreTipoFactoraje = "Distribuido";
			else if("C".equals(nombreTipoFactoraje))
				nombreTipoFactoraje = "Nota de Crédito";
			*/
			
			dblRecurso = new Double(FN_MONTO).doubleValue() - new Double(FN_MONTO_DSCTO).doubleValue();
			importe_depositar_pyme = importe_recibir - montoPago;

			// Fodea 002 - 2010
			//esNotaDeCreditoSimpleAplicada						= ( nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(String.valueOf(IC_DOCUMENTO)) )?true:false;
			//esDocumentoConNotaDeCreditoSimpleAplicada		= ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;
				
			//esNotaDeCreditoAplicada 							= ( nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(String.valueOf(IC_DOCUMENTO)))?true:false;
			//esDocumentoConNotaDeCreditoMultipleAplicada	= ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;

			esNotaDeCreditoSimpleAplicada = "true".equals(rs.getString("existeDocumentoAsociado"))?true:false;
			esDocumentoConNotaDeCreditoSimpleAplicada = "true".equals(rs.getString("esDocConNotaDeCredSimpleApl"))?true:false;
				
			esNotaDeCreditoAplicada = "true".equals(rs.getString("existeRefEnComrelNotaDocto"))?true:false;
			esDocumentoConNotaDeCreditoMultipleAplicada	= "true".equals(rs.getString("esDocConNotaDeCredMultApl"))?true:false;

			
			if(esNotaDeCreditoSimpleAplicada){
				numeroDocumento									= getNumeroDoctoAsociado(String.valueOf(IC_DOCUMENTO)); 
			}else if(esDocumentoConNotaDeCreditoSimpleAplicada){
				numeroDocumento									= getNumeroNotaCreditoAsociada(String.valueOf(IC_DOCUMENTO));
			}
			
			registros++;
			/*GENERAMOS EL ARCHIVO*/
      if(strPerfil.equals("PROMO NAFIN")){
			contenidoArchivo.append("\"" + NOM_EPO.replace(',',' ') + "\",\"" + NOM_PYME.replace(',',' ') +
					"\",\"" + NOM_IF.replace(',',' ') +"\",\""+fideicomiso.replaceAll(",","")+ "\",\"" + RFC.replace(',',' ') + "\"," + IG_NUMERO_DOCTO +
					"," + cc_acuse+"," + DF_FECHA_DOCTO + "," + DF_FECHA_VENC +
					"," + plazo + "," +  NOM_MONEDA + "," + tipoFactorajeDesc +
					"," + FN_MONTO + "," + dblPorcentaje + "," + dblRecurso +
					"," + FN_MONTO_DSCTO +
					"," + Comunes.formatoDecimal(importe_interes,2,SIN_COMAS) +
					"," + Comunes.formatoDecimal(importe_recibir,2,SIN_COMAS) +
					"," + IN_TASA_ACEPTADA + "," + ESTATUS_DOCTO +
					"," + Comunes.formatoDecimal(montoPago,2,SIN_COMAS) +
					"," + porc_docto_aplicado) ;
      }else{
      contenidoArchivo.append("\"" + NOM_EPO.replace(',',' ') + "\",\"" + NOM_PYME.replace(',',' ') +
					"\",\"" + NOM_IF.replace(',',' ') +"\",\""+fideicomiso.replaceAll(",","")+ "\"," + IG_NUMERO_DOCTO +
					"," + cc_acuse+"," + DF_FECHA_DOCTO + "," + DF_FECHA_VENC +
					"," + plazo + "," +  NOM_MONEDA + "," + tipoFactorajeDesc +
					"," + FN_MONTO + "," + dblPorcentaje + "," + dblRecurso +
					"," + FN_MONTO_DSCTO +
					"," + Comunes.formatoDecimal(importe_interes,2,SIN_COMAS) +
					"," + Comunes.formatoDecimal(importe_recibir,2,SIN_COMAS) +
					"," + IN_TASA_ACEPTADA + "," + ESTATUS_DOCTO +
					"," + Comunes.formatoDecimal(montoPago,2,SIN_COMAS) +
					"," + porc_docto_aplicado) ;
      }

			if(Hestatus.equals("16") ){
				if(ig_tipo_piso.equals("1")){
					if(!fn_remanente.equals("")){
						contenidoArchivo.append("," + Comunes.formatoDecimal(Double.parseDouble(fn_remanente),2,SIN_COMAS));
					}else{
						contenidoArchivo.append("," + " ");
					}
				}else{
					contenidoArchivo.append("," + Comunes.formatoDecimal(importe_depositar_pyme,2,SIN_COMAS));
				}
			}else{
				contenidoArchivo.append("," + Comunes.formatoDecimal(importe_depositar_pyme,2,SIN_COMAS));
			}

			if (Hestatus.equals("4") || Hestatus.equals("11") || Hestatus.equals("12")) {
				contenidoArchivo.append("," + FECHA_SOLICITUD);
			}
			contenidoArchivo.append("," + Comunes.formatoDecimal(neto_a_recibir_pyme,2,SIN_COMAS));
			
			// Fodea 002 - 2010.
			contenidoArchivo.append(",");
			if(esNotaDeCreditoAplicada || esDocumentoConNotaDeCreditoMultipleAplicada){
				contenidoArchivo.append("Si");	
			}
			if(esNotaDeCreditoSimpleAplicada || esDocumentoConNotaDeCreditoSimpleAplicada){
				contenidoArchivo.append("\""+numeroDocumento+"\"");	
			}
			
			if(!"".equals(beneficiario)) contenidoArchivo.append(","+beneficiario.replace(',',' ')); else contenidoArchivo.append(",");
			if(0 != porciento_beneficiario) contenidoArchivo.append(","+porciento_beneficiario); else contenidoArchivo.append(",");
			if(0 != importe_a_recibir_beneficiario) contenidoArchivo.append(","+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,SIN_COMAS));  else contenidoArchivo.append(",");

			contenidoArchivo.append(","+DF_ALTA+","+numeroSIAFF);

			//si hay bandera de documentos se recuperan los valores
//			if(banderaTablaDoctos.equals("1")){
				contenidoArchivo.append("," + FECHA_ENTREGA + "," + TIPO_COMPRA + "," + CLAVE_PRESUPUESTARIA + "," + PERIODO);
//			}
//============================================================================>> FODEA 002 - 2009 (I)
			if(!ic_epo.equals("")){
				List datosCamposAdicionales = new ArrayList();
				if(!ic_epo.equals("")){datosCamposAdicionales = BeanParamDscto.getDatosCamposAdicionales(IC_DOCUMENTO, ic_epo, numeroCamposAdicionales);}
				for(int i = 0; i < numeroCamposAdicionales; i++){
					contenidoArchivo.append("," + datosCamposAdicionales.get(i));
				}
			}
			if(Hestatus.equals("26")){contenidoArchivo.append(","+fecha_registro_operacion);}//FODEA 005 - 2009 ACF
			contenidoArchivo.append("\n");
//============================================================================>> FODEA 002 - 2009 (F)
			//contenidoArchivo = contenidoArchivo+linea;
			buffer.write(contenidoArchivo.toString());	
			contenidoArchivo = new StringBuffer();//Limpio
		} // while
	} //if estatus=3,4,11,12

	
	buffer.close();
	if(registros >= 1){
		if(Hestatus.equals("") || Hestatus.equals("2") || Hestatus.equals("5") ||
			Hestatus.equals("6") || Hestatus.equals("7") || Hestatus.equals("9") || Hestatus.equals("10") || Hestatus.equals("21")|| Hestatus.equals("28") || Hestatus.equals("29")|| Hestatus.equals("30")|| Hestatus.equals("31")) {
				vtmn=8; vesp=4; vnumd=9;
		}
		if(Hestatus.equals("3") || Hestatus.equals("4") || Hestatus.equals("11") || Hestatus.equals("12")|| Hestatus.equals("16") || Hestatus.equals("24") || Hestatus.equals("26")) {//FDOEA 005 - 2009 ACF
			vtmn=9; vesp=8; vnumd=12;

		}
			
			
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
		} //Existen registros
		if(registros==0) {

	} //sin registros
	 //if Generar Consulta. 
}catch(Exception e){
	System.out.println("13consulta2c.jsp(Exception)");
	out.println(e);
	e.printStackTrace();
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
}
%>
<%!
	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);
	}

	public String getNumeroDeCuatroDigitos(String numero){

		String 			claveEPO 	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
      }
      resultado.append(claveEPO);

      return resultado.toString();
  }

  public String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
      }
      resultado.append(claveDocto);

      return resultado.toString();
  }

   public String getClaveEPO(String numeroSIAFF){
		BigInteger	icEPO 		= new BigInteger("-1");
		if(numeroSIAFF != null && !numeroSIAFF.equals("") && numeroSIAFF.length() == 15){
			icEPO 		= new BigInteger(numeroSIAFF.substring(0,4));
		}
		return icEPO.toString();
	}
%>
<%-- Fodea 002 - 2010 --%>
<%!
	public String getNumeroDoctoAsociado(String ic_nota_credito)
		throws Exception{
		
			System.out.println("13consulta2c.jsp::getNumeroDoctoAsociado(E)");
		
			if(ic_nota_credito == null || ic_nota_credito.equals("")){
				System.out.println("13consulta2c.jsp::getNumeroDoctoAsociado(S)");
				return "";
			}
		
			AccesoDB 		con 				= new AccesoDB();
			StringBuffer 	qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";
			
			try {	
				
				con.conexionDB();
				qrySentencia.append( 
					"SELECT             "  + 
					"  IG_NUMERO_DOCTO  "  +
					"FROM               "  + 
					"COM_DOCUMENTO      "  + 
					"WHERE              "  + 
					"	IC_DOCUMENTO IN  "  +
					"		(             "  +
					"			SELECT IC_DOCTO_ASOCIADO FROM COM_DOCUMENTO WHERE IC_DOCUMENTO = ? "  +
					"     )             ");			
		
				lVarBind.add(new Integer(ic_nota_credito));
				
				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("IG_NUMERO_DOCTO");
					resultado = (resultado == null)?"":resultado;
				}
				
		}catch(Exception e){
			System.out.println("13consulta2c.jsp::getNumeroDoctoAsociado(Exception)");
			System.out.println("13consulta2c.jsp::getNumeroDoctoAsociado.ic_nota_credito = <"+ic_nota_credito+">");
			e.printStackTrace();
			throw new Exception("No se pudo consultar el documento especificado.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13consulta2c.jsp::getNumeroDoctoAsociado(S)");
		}

		return resultado;	
	}
	
	public String getNumeroNotaCreditoAsociada(String ic_documento)
		throws Exception{
		
			System.out.println("13consulta2c.jsp::getNumeroNotaCreditoAsociada(E)");
		
			if(ic_documento == null || ic_documento.equals("")){
				System.out.println("13consulta2c.jsp::getNumeroNotaCreditoAsociada(S)");
				return "";
			}
		
			AccesoDB 		con 				= new AccesoDB();
			StringBuffer 	qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";
			
			try {	
				
				con.conexionDB();
				qrySentencia.append( 
					"SELECT                  "  + 
					"  IG_NUMERO_DOCTO       "  +
					"FROM                    "  +
					"  COM_DOCUMENTO         "  +
					"WHERE                   "  +
					"  IC_DOCTO_ASOCIADO = ? ");			
		
				lVarBind.add(new Integer(ic_documento));
				
				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("IG_NUMERO_DOCTO");
					resultado = (resultado == null)?"":resultado;
				}
				
		}catch(Exception e){
			System.out.println("13consulta2c.jsp::getNumeroNotaCreditoAsociada(Exception)");
			System.out.println("13consulta2c.jsp::getNumeroNotaCreditoAsociada.ic_documento = <"+ic_documento+">");
			e.printStackTrace();
			throw new Exception("No se pudo consultar el documento especificado.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13consulta2c.jsp::getNumeroNotaCreditoAsociada(S)");
		}

		return resultado;	
	}
%><%=infoRegresar%>