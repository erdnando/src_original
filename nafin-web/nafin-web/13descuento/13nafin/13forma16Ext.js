Ext.onReady(function() {

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - 

	var procesaAccion = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){

		if(  estadoSiguiente == "ACEPTAR" 										){

			Ext.getCmp('lblMensaje').hide();
			Ext.getCmp('forma').el.mask('Enviando...', 'x-mask-loading');

			Ext.Ajax.request({
				url: '13forma16.data.jsp',
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion:	'Aceptar'
							}
				),
				callback: procesaAccion
			});
		
		} else if(	estadoSiguiente == "RESPUESTA_ACEPTAR"					){

			Ext.getCmp('_idUser').setValue(respuesta.idUser);
			Ext.getCmp('_ic_nafin_electronico').setValue(respuesta.ic_nafin_electronico);
			Ext.getCmp('_cg_razon_social').setValue(respuesta.cg_razon_social);
			Ext.getCmp('_cg_rfc').setValue(respuesta.cg_rfc);
			Ext.getCmp('_email').setValue(respuesta.email);
			Ext.getCmp('_cs_estatus_mail').setValue(respuesta.cs_estatus_mail	);
			(respuesta.cs_estatus_mail == "S") ?Ext.getCmp('_cs_estatus_mail').setValue("Si") : Ext.getCmp('_cs_estatus_mail').setValue("No")
			Ext.getCmp('forma').hide();
			Ext.getCmp('formaConfirma').show();

		} else if(	estadoSiguiente == "DESBLOQUEAR"							){

			Ext.getCmp('formaConfirma').el.mask('Procesando...', 'x-mask-loading');

			Ext.Ajax.request({
				url: '13forma16.data.jsp',
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion:	'Save'
							}
				),
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA_DESBLOQUEAR"			){

			Ext.getCmp('lblMensaje').show();
			Ext.getCmp('forma').show();
			Ext.getCmp('formaConfirma').hide();

		} else if(	estadoSiguiente == "FIN"									){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "13forma16Ext.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}

//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

	var fpConfirma = new Ext.form.FormPanel({
		id:				'formaConfirma',
		title:			'<div align="center">Confirmaci�n de Datos de la PyME</div>',
		width:			600,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			true,
		hidden:			true,
		labelWidth:		230,
		bodyStyle:		'padding: 6px',
		items:			[
			{
				xtype:		'displayfield',
				id:			'_idUser',
				fieldLabel:	'Clave de usuario',
				value:		''
			},{
				xtype:		'displayfield',
				id:			'_ic_nafin_electronico',
				fieldLabel:	'Nafin Electr�nico',
				value:		''
			},{
				xtype:		'displayfield',
				id:			'_cg_razon_social',
				fieldLabel:	'Nombre',
				value:		''
			},{
				xtype:		'displayfield',
				id:			'_cg_rfc',
				fieldLabel:	'RFC',
				value:		''
			},{
				xtype:		'displayfield',
				id:			'_email',
				fieldLabel:	'Correo Electr�nico',
				value:		''
			},{
				xtype:		'displayfield',
				id:			'_cs_estatus_mail',
				fieldLabel:	'Recibe acuse de confirmaci�n v�a correo',
				value:		''
			}
		],
		monitorValid:	true,
		buttons: [
			{
				text:		'Desbloquear',
				id:		'btnDesbloq',
				iconCls:	'icoAceptar',
				width:	100,
				formBind:true,
				handler: function(boton, evento) {
								Ext.Msg.confirm("Mensaje de p�gina web",
									"�Esta seguro de desbloquear la clave?",
									function(btn){
										if(btn == "ok" || btn == "yes"){
											accionConsulta("DESBLOQUEAR",null);
										}
									}
								);

				} //fin handler
			}
		]
	});
	
	var fp = new Ext.form.FormPanel({
		id:				'forma',
		title:			undefined,
		width:			350,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			true,
		labelWidth:		100,
		bodyStyle:		'padding-left:40px;,padding-top:10px;,text-align:left',
		items:			[
			{
				xtype:	'label',
				id:		'lblMensaje',
				cls:		'x-form-item',
				hidden:	true,
				style: 	'font-weight:bold;text-align:center;margin:14px;',
				text:  	'La clave de cesi�n de derechos ha sido desbloqueada'
			},{
				xtype:		'textfield',
				id:			'_cg_login',
				name:			'cg_login',
				fieldLabel:	'Clave de usuario',
				maxLength:	10,
				msgTarget:	'side',
				anchor:		'75%',
				allowBlank:	false
			}
		],
		monitorValid:	true,
		buttons: [
			{
				text:		'Aceptar',
				id:		'btnAceptar',
				iconCls:	'icoAceptar',
				width:	100,
				formBind:true,
				handler: function(boton, evento) {
								accionConsulta("ACEPTAR",null);

				} //fin handler
			}
		]
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		renderHidden:	true,
		height: 	'auto',
		items: 	[NE.util.getEspaciador(20),fp,fpConfirma],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

         element = Ext.getCmp("formaConfirma").getEl();
			if(Ext.getCmp("formaConfirma").isVisible()){
				if( element.isMasked()){
					element.unmask();
				}
			}

			// Derefenrencia ultimo elemento...
			element = null;

		}

	});

});