<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		java.sql.*,
		java.text.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";


if(informacion.equals("descargaUltimoArchivo")){
	JSONObject jsonObj = new JSONObject();
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	String lContArch = null;
	String diaMes = new SimpleDateFormat("ddMMyyyy").format(new java.util.Date());
	String nombreArchivo = "s"+diaMes;
	int registros = 0;
	
	CreaArchivo archivo = new CreaArchivo();	
	try{
		con.conexionDB();
		
		String strSQL =	" SELECT cg_email_pub_vig " + 
										"  FROM com_email_pub_vig " + 
										"   WHERE ic_email_pub_vig = (SELECT MAX (ic_email_pub_vig) " + 
										"                               FROM com_email_pub_vig)";
		System.out.println("strSQL: " + strSQL);
		
		ps = con.queryPrecompilado(strSQL);
		rs = ps.executeQuery();
		
		
		if(rs.next()){
			lContArch = (rs.getString(1)==null)?"":rs.getString(1);
			registros++;
		}
		
		System.out.println("registros: " + registros);
		
		rs.close();
		con.cierraStatement();
	
		if(registros>=1){
			archivo.make(lContArch.toString(), strDirectorioTemp,nombreArchivo,".txt");
		}
		
		nombreArchivo =  strDirecVirtualTemp+nombreArchivo+".txt";
		String fec_Actual = fHora.format(new java.util.Date());
	
	} catch (Exception e){
		out.println(e);
		e.printStackTrace();	
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}	
	
	jsonObj.put("urlArchivo", nombreArchivo);
	jsonObj.put("registros", String.valueOf(registros));
	jsonObj.put("success", new Boolean(true));
	
	infoRegresar = jsonObj.toString();

}

System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>