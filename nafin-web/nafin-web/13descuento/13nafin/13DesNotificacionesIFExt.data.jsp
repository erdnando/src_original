<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.text.*,
	javax.naming.*,
	com.netro.model.catalogos.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
		com.netro.descuento.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoEPONafin,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>	
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String indiceEst= (request.getParameter("indiceEst") != null) ? request.getParameter("indiceEst") : "";
SimpleDateFormat formatoHora2 = new SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a");
String infoRegresar ="", consulta="";
JSONObject jsonObj = new JSONObject();


if (informacion.equals("valoresIniciales")  ) {  

	jsonObj.put("success",new Boolean(true));
	jsonObj.put("strNombreUsuario", strNombreUsuario);
	jsonObj.put("fechaHora",formatoHora2.format(new java.util.Date()));
	infoRegresar = jsonObj.toString();
 
}
else if (informacion.equals("CatalogoIF") ) {
		
		CatalogoIF cat = new CatalogoIF();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setOrden("cg_razon_social");		
		List elementos = cat.getListaElementos();
		Iterator it = elementos.iterator();
		JSONArray jsonArr = new JSONArray();
		
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";	
}
else if (informacion.equals("DescargaArchivo") ) {
			String intermediario   = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
			String notificacion   = (request.getParameter("notificacion")!=null)?request.getParameter("notificacion"):"";
						
			CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);

			String	correos  = CargaDocumentos.ultimoArchNotIF(intermediario, notificacion);
			
			String nombreArchivo="correosaux";
			CreaArchivo archivo = new CreaArchivo();	
			if(!correos.equals("")){
				archivo.make(correos, strDirectorioTemp,nombreArchivo,".txt");
				jsonObj.put("correos","OK");
			}else
			{
				jsonObj.put("correos","NO");
			}
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo+".txt");
			jsonObj.put("success",new Boolean(true));
						
			infoRegresar = jsonObj.toString();

}
else if (informacion.equals("Eliminar") ) {
		
			String intermediario   = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";
			String notificacion   = (request.getParameter("notificacion")!=null)?request.getParameter("notificacion"):"";
			
			CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);

			String	elimino  = CargaDocumentos.EliminarNotIF(intermediario, notificacion);
			jsonObj.put("elimino",elimino);
			jsonObj.put("success",new Boolean(true));
			infoRegresar = jsonObj.toString();			
}
%>

<%=infoRegresar%>

