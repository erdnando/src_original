 <%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.descuento.*, 
		
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray, net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion  = (request.getParameter("informacion") !=null)  ? request.getParameter("informacion"):"";
	String operacion    = (request.getParameter("operacion")   != null) ? request.getParameter("operacion")  :"";
	String infoRegresar = "";
	int start = 0; 
	int limit = 0;
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();

	if (informacion.equals("CatalogoEPO") ) {

		String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String ic_banco_fondeo = (request.getParameter("fondeo")==null)?"":request.getParameter("fondeo");

		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("2");
		cat.setBancofondeo(ic_banco_fondeo);
		if(!ic_if.equals(""))
			cat.setClaveIf(ic_if);

		List elementos = cat.getListaElementos();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsObjArray.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
	
	} else if (informacion.equals("CatalogoBanco")) {
		List list = null;

		if(iTipoPerfil.equals("8")) {
			HashMap hash = new HashMap();
			list = new ArrayList();
			hash.put("clave","2");
			hash.put("descripcion","BANCOMEXT");
			list.add(hash);
		}
		else {
			HashMap hash = new HashMap();
			list = new ArrayList();
			hash.put("clave","1");
			hash.put("descripcion","NAFIN");
			list.add(hash);
			hash = new HashMap();
			hash.put("clave","2");
			hash.put("descripcion","BANCOMEXT");
			list.add(hash);
		}

		jsObjArray = JSONArray.fromObject(list);
		infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
	} else if(informacion.equals("CatalogoIF")) {
		String ic_banco_fondeo = (request.getParameter("fondeo")==null)?"":request.getParameter("fondeo");
		
		CatalogoIF_Desc cat = new CatalogoIF_Desc();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setG_orden("2");
		cat.setG_fondeo(ic_banco_fondeo);
		cat.setG_producto("1");

		List elementos = cat.getListaElementos(); 
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsObjArray.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";

	} else if (informacion.equals("VerificaLimites")) {
		String sNoEPO = (request.getParameter("noEpo")==null)?"":request.getParameter("noEpo");
		ConsLimitesPorEPO paginador = new ConsLimitesPorEPO();
		String res = "";
		JSONObject resultado = new JSONObject();
		if(!sNoEPO.equals(""))
			res = paginador.verificaLimitesEPO(sNoEPO);
			resultado.put("success", new Boolean(true));
			resultado.put("limitesEPO", res);
			infoRegresar = resultado.toString();
	} else if (informacion.equals("CatalogoProveedor")) {
		try {
			String sNoEPO = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
			String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
			
			List listVerifica = new ArrayList();
			ConsLimitesPorEPO paginador = new ConsLimitesPorEPO();
			if(!sNoEPO.equals("")) 
				listVerifica = paginador.limitesPyme(sNoEPO,ic_if);
			
			jsObjArray = JSONArray.fromObject(listVerifica);
			infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";

		} catch(Exception e) { 
			throw new AppException("Error en la recuperación de datos", e);
		}
	}
	if(informacion.equals("ConsultaGrid") || informacion.equals("ConsultaGridLimites") || informacion.equals("GenerarPDF") || informacion.equals("GenerarCSV")){
		String ic_banco     = (request.getParameter("bco_fondeo")       ==null)?"":request.getParameter("bco_fondeo");
		String ic_if        = (request.getParameter("nombre_if")        ==null)?"":request.getParameter("nombre_if");
		String ic_epo       = (request.getParameter("cmb_epo")          ==null)?"":request.getParameter("cmb_epo");
		String ic_proveedor = (request.getParameter("nombre_proveedor") ==null)?"":request.getParameter("nombre_proveedor");
		String limite_pyme  = (request.getParameter("limite")           ==null)?"":request.getParameter("limite");
		String tipoConsulta = (request.getParameter("limites")          ==null)?"":request.getParameter("limites");
		ConsLimitesPorEPO paginador = new ConsLimitesPorEPO();
		paginador.setBancoFondeo(ic_banco);
		paginador.setClaveIF(ic_if);
		paginador.setEpo(ic_epo);
		paginador.setProveedor(ic_proveedor);
		paginador.setLimitePyme(limite_pyme);
		if(informacion.equals("ConsultaGrid"))
			paginador.setTipoConsulta(1);
		else if(informacion.equals("ConsultaGridLimites"))
			paginador.setTipoConsulta(2);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

		try{
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
			
		} catch(Exception e){
			System.out.println("Error en parametros de paginación");
		}
		try {
			if(informacion.equals("ConsultaGrid") || informacion.equals("ConsultaGridLimites")){
				if(operacion.equals("Generar"))
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				String consultar = queryHelper.getJSONPageResultSet(request,start,limit);
				jsonObj = JSONObject.fromObject(consultar);
				infoRegresar=jsonObj.toString();
			} else if (informacion.equals("GenerarPDF")){
				if(tipoConsulta.equals("N"))
					paginador.setTipoConsulta(1);
				else
					paginador.setTipoConsulta(2);
				//String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "PDF");
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			} else if (informacion.equals("GenerarCSV")){
				if(tipoConsulta.equals("N"))
					paginador.setTipoConsulta(1);
				else
					paginador.setTipoConsulta(2);
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
		} catch(Exception e){
			throw new AppException("Error en la recuperacion de datos!", e);
		}
}

//System.out.println("infoRegresar============"+infoRegresar); 
%>
<%=infoRegresar%>