
Ext.onReady(function() {

	function verEmail(nomObj){
	
		var email = Ext.getCmp('_email');
		
		if (nomObj.length>0){
			if ( nomObj.indexOf("@")==-1 ){				
				email.markInvalid('"Correo no correcto\n ejemplo: nombre@empresa.com');
				email.focus();
				return;
			}
			if (nomObj.indexOf(".")==-1 ){
				email.markInvalid('"Correo no correcto\n ejemplo: nombre@empresa.com');
				email.focus();
				return;
			}
			//return true;
		}
	}

	/**********     Valida el campo Correo(s) de Notificaci�n .     **********/
	function validaEmail(){
		var errores = 0;
		expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var correo = Ext.getCmp('email_notifica_doctos_id').getValue();
		// Verifico el tama�o del campo
		if(correo.length == 0){
			Ext.getCmp('email_notifica_doctos_id').markInvalid('El campo es obligatorio');
			return false;
		}
		if(correo.length > 400){
			Ext.getCmp('email_notifica_doctos_id').markInvalid('El tama�o m�ximo es de 400');
			return false;
		}
		// Verifico el formato de los correos
		var vecCorreos = correo.split(',');
		for(var i = 0; i < vecCorreos.length; i++){
			if (!expr.test(vecCorreos[i])){
				errores++;
			}
		}
		if(errores > 0){
			Ext.getCmp('email_notifica_doctos_id').markInvalid('Este campo debe ser una direcci�n de correo electr�nico con el formato usuario@dominio.com,usuario2@dominio.com');
			return false;
		}
		return true;
	}

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

	var catalogoIfData = new Ext.data.JsonStore({
		id:				'catalogoIfDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'13paramIfExt.data.jsp',
		baseParams:		{	informacion:	'CatalogoIf'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - 

	var procesaAccion = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);
			
				Ext.getCmp('operaFideiComiso').setValue(resp.operaFideiComiso);
				
				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){

		if(  estadoSiguiente == "GET_PARAMAS_IF" 										){

			var forma = Ext.getCmp("forma");
			forma.el.mask('Procesando...','x-mask-loading');

			Ext.Ajax.request({
				url: '13paramIfExt.data.jsp',
				params:	{
								informacion:	'getParamsIf',
								clave_if:		respuesta.clave_if
				},
				callback: procesaAccion
			});			

		}else if(  estadoSiguiente == "RESPONSE_PARAMS_IF" 						){

			var operaFac_s	= Ext.getCmp('operaFac_s');
			var operaFac_n = Ext.getCmp('operaFac_n');
			var _mn			= Ext.getCmp('_mn');
			var _da			= Ext.getCmp('_da');
			var __a			= Ext.getCmp('__a');
			var _email		= Ext.getCmp('_email');
			var consultaG	= Ext.getCmp('consultaG');
			var consultaD	= Ext.getCmp('consultaD');
			var id_tasaOperacion	= Ext.getCmp('id_tasaOperacion');
			var fide_cortes_s	= Ext.getCmp('fide_cortes_s');
			var fide_cortes_n = Ext.getCmp('fide_cortes_n'); 
			

			Ext.getCmp('_ic_if').setValue(respuesta.clave_if);
			_email.setValue(respuesta.sParamEmail);
			respuesta.sOperaFactoraje24=="S"	?	operaFac_s.setValue(true)	:operaFac_s.setValue(false);
			respuesta.sOperaFactoraje24=="N"	?	operaFac_n.setValue(true)	:operaFac_n.setValue(false);
			respuesta.sParamMoneda		=="MN"?	_mn.setValue(true)			:_mn.setValue(false);
			respuesta.sParamMoneda		=="DA"?	_da.setValue(true)			:_da.setValue(false);
			respuesta.sParamMoneda		=="A"	?	__a.setValue(true)			:__a.setValue(false);
			respuesta.sParamConsulta	=="G"	?	consultaG.setValue(true)	:consultaG.setValue(false);
			respuesta.sParamConsulta	=="D"	?	consultaD.setValue(true)	:consultaD.setValue(false);	
			respuesta.sParamFisoCortes	=="S"	?	fide_cortes_s.setValue(true)	:fide_cortes_n.setValue(false);
			respuesta.sParamFisoCortes	=="N"	?	fide_cortes_n.setValue(true)	:fide_cortes_n.setValue(false);	
			id_tasaOperacion.setValue(respuesta.sParamTasaFondeo);

		// INI F000-2015
		if(respuesta.sNotificaDoctos == 'N'){
			Ext.getCmp('notifica_doctos_id').setValue('N');
			Ext.getCmp('email_notifica_doctos_id').disable();
			Ext.getCmp('email_notifica_doctos_id').setValue('');
		} else if(respuesta.sNotificaDoctos == 'S'){
			Ext.getCmp('notifica_doctos_id').setValue('S');
			Ext.getCmp('email_notifica_doctos_id').enable();
			Ext.getCmp('email_notifica_doctos_id').setValue(respuesta.sMailNotificaDoctos);
		}
		// FIN F000-2015

		}else if(  estadoSiguiente == "GRABAR" 										){
			
			//INI F000-2015
			var notifica_doctos = (Ext.getCmp('notifica_doctos_id')).getValue();
			if(notifica_doctos.getGroupValue() == 'S'){
				if(!validaEmail()){
					return;
				}
			}
			//FIN F000-2015

			var operaFideiComiso =  Ext.getCmp('operaFideiComiso');
			var id_tasaOperacion  =  Ext.getCmp('id_tasaOperacion');
			if(operaFideiComiso.getValue()=='S' && id_tasaOperacion.getValue() =='') {
				id_tasaOperacion.markInvalid('Es un campo obligatorio' );	
				return;
			}

			// INI F000-2015
			var notifica_doctos =(Ext.getCmp('notifica_doctos_id')).getValue();
			if(notifica_doctos != null || notifica_doctos != ''){
				if( notifica_doctos.getGroupValue() == 'S' && Ext.getCmp('email_notifica_doctos_id').getValue() == '' ){
					Ext.getCmp('email_notifica_doctos_id').markInvalid('El campo es obligatorio');
				return;
			}
			}
			// FIN F000-2015

			Ext.Ajax.request({
				url: '13paramIfExt.data.jsp',
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion:	'Save'
							}
				),
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "FIN"											){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "13paramIfExt.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

	var elementosForma = [
		{ 	xtype: 'textfield',  hidden: true,  id: 'operaFideiComiso', 	value: '' },	
		{
			xtype:			'box',
			height:			20
		},{
			xtype:			'combo',
			id:				'_ic_if',
			name:				'sNiF',
			hiddenName:		'sNiF',
			fieldLabel:		'Nombre del IF',
			emptyText:		'Seleccione IF',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		false,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoIfData,
			tpl:				NE.util.templateMensajeCargaCombo,
			listeners:{
				'select':function(cbo){
								if(!Ext.isEmpty(cbo.getValue())){
									var respuesta = new Object();
									respuesta["clave_if"] = cbo.getValue();
									
									//Ext.getCmp('id_tasaOperacion').setValue('');
									
									accionConsulta("GET_PARAMAS_IF",	respuesta);
								}
							}
			}
		},{
			xtype:			'box',
			height:			20
		},{
			xtype:			'panel',
			title:			'<div align="center">Par�metros por IF</div>',
			layout:			'table',
			anchor:			'65%',
			frame:			false,
			border:true,
			style:			'margin:0 auto;',
			layoutConfig:	{ columns: 3 },
			defaults:		{frame:true,	border:true,	height:40,	bodyStyle:'padding:5px'},
			items:[
				{html:'<div align="center">1</div>'},{html:'Opera Factoraje 24hrs',width:200},
				{
					layout:		'form',
					labelWidth:	1,
					width:		300,
					items:[
						{
							xtype:	'radiogroup',
							columns:	[50, 50],
							items:[
								{boxLabel: 'SI',	id:'operaFac_s',	name:'sOperaFactoraje24hrs',	inputValue:'S'},
								{boxLabel: 'NO',	id:'operaFac_n',	name:'sOperaFactoraje24hrs',	inputValue:'N'}
							]
						}
					]
				},
				{html:'<div align="center">2</div>',height:85},{html:'Moneda (WebService)',width:200,height:85},
				{
					width:	300,
					height:	85,
					items:[
						{
							xtype:	'radiogroup',
							columns:	1,
							items:[
								{boxLabel: 'Moneda Nacional',		id:'_mn',	name:"sMoneda",	inputValue:"MN"},
								{boxLabel: 'D�lares Americanos',	id:'_da',	name:"sMoneda",	inputValue:"DA"},
								{boxLabel: 'Ambas',					id:'__a',	name:"sMoneda",	inputValue:"A"}
							]
						}
					]
				},
				{html:'<div align="center">2.1</div>'},{html:'Correo de Notificaci�n',width:200},
				{
					width:		300,
					layout:		'form',
					labelWidth:	1,
					items:[
						{
							xtype:		'textfield',
							id:			'_email',
							name:			"Email",
							maxLength:	"400",
							anchor:		'85%',
							msgTarget:	'side',
							listeners: {
								blur: {
									fn: function(objTxt) {
										verEmail(objTxt.getValue());
									}
								}
							}
							//regex:		/^([\w\-\"'"\-]+)(\.[\w-\"'"\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/,
							//regexText:	'Correo no correcto\<br> ejemplo: nombre@empresa.com'
						}
					]
				},
				{html:'<div align="center">3</div>',height:60},{html:'Tipo de Consulta <br>(L�neas de Fondeo SIRAC)',width:200,height:60},
				{
					width:	300,
					height:	60,
					items:[
						{
							xtype:	'radiogroup',
							columns:	1,
							items:[
								{boxLabel: 'Global',		id:'consultaG',	name:"consulta", inputValue:"G"},
								{boxLabel: 'Detallada',	id:'consultaD',	name:"consulta", inputValue:"D"}
							]
						}
					]
				},
				{html:'<div align="center">4</div>',height:60 },{html:'Tasa por FONDEO de Fideicomiso para Desarrollo de Proveedores ',width:200, height:60},
				{
					width:		300,
					height:	60,
					layout:		'form',
					labelWidth:	1,
					items:[
						{	
							width: 40,	
							xtype:'bigdecimal',		
							maxText:'El tama�o m�ximo del campos es de 3 enteros 5 decimales',				
							maxValue: '999.99999', 
							allowDecimals: true,
							allowNegative: false,							
							hidden: 			false,	
							allowBlank: true,
							msgTarget: 		'side',
							anchor:			'-20',						
							format:			'000.00000',
							id:'id_tasaOperacion',	
							name:'tasaOperacion'	
						}
					]
				},
				{html:'<div align="center">5</div>',height:60},{html:'IF Fideicomiso (Cortes Diarios)',width:200,height:60},				
				{
					layout:		'form',
					labelWidth:	1,
					width:		300,
					height:60,
					items:[
						{
							xtype:	'radiogroup',
							columns:	[50, 50],
							items:[
								{boxLabel: 'SI',	id:'fide_cortes_s',	name:'fide_cortes',	inputValue:'S'},
								{boxLabel: 'NO',	id:'fide_cortes_n',	name:'fide_cortes',	inputValue:'N'}
							]
						}
					]
				},
				//INI F000-2015
				{
					html:               '<div align="center">6</div>',
					height:             60
				},{
					html:               'Activar Notificaci�n doctos.<br>"Seleccionada PyME" por e-mail',
					width:              200,
					height:             60
				},{
					layout:             'form',
					labelWidth:         1,
					width:              300,
					height:             60,
					items:[{
						xtype:          'radiogroup',
						id:             'notifica_doctos_id',
						name:           'notifica_doctos',
						columns:        [50, 50],
						items:[{
							boxLabel:   'SI',
							name:       'notifica_doctos',
							inputValue: 'S'
						},{
							boxLabel:   'NO',
							name:       'notifica_doctos',
							inputValue: 'N'
						}],
						listeners:     {
							change:     {
								fn:      function(){
									var notifica_doctos = (Ext.getCmp('notifica_doctos_id')).getValue();
									if(notifica_doctos.getGroupValue() == 'S'){
										Ext.getCmp('email_notifica_doctos_id').enable();
									} else if(notifica_doctos.getGroupValue() == 'N'){
										Ext.getCmp('email_notifica_doctos_id').setValue('');
										Ext.getCmp('email_notifica_doctos_id').disable();
									}
								}
				}
						}
					}]
				},{
					html:             '<div align="center">6.1</div>',
					height:           90
				},{
					html:             'Correo(s) de Notificaci�n',
					width:            200,
					height:           90
				},{
					width:            300,
					height:           90,
					labelWidth:       1,
					layout:           'form',
					items:[{
						xtype:         'textarea',
						id:            'email_notifica_doctos_id',
						name:          'email_notifica_doctos',
						anchor:        '85%',
						msgTarget:     'side',
						disabled:      true,
						listeners:     {
							blur:       {
								fn: function(objTxt) {
									if(!validaEmail()){
										return;
									}
								}
							}
						}
					}]
				}
				//F000-2015 FIN
			]
		},	
		{
			xtype:			'box',
			height:			20
		}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		title:			undefined,
		width:			830,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			false,
		bodyStyle:		'padding: 10px',
		labelWidth:		100,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		monitorValid:	true,
		buttonAlign:	'center',
		buttons: [
			{
				text:		'Grabar',
				id:		'btnGrabar',
				iconCls:	'autorizar',
				width:	100,
				formBind:true,
				handler: function(boton, evento) {
								accionConsulta("GRABAR",null);

				} //fin handler
			}
		]
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		renderHidden:	true,
		height: 	'auto',
		items: 	[fp],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Derefenrencia ultimo elemento...
			element = null;

		}

	});

	Ext.StoreMgr.key('catalogoIfDataStore').load();

});