Ext.onReady(function() {
 			
	//------------------------ NUEVOS COMPONENTES DE EXTJS -------------------------
	
	//------------------------------ VALIDACIONES ----------------------------------

	//-------------------------------- HANDLERS ------------------------------------
	
	//--------------------------------- STORES -------------------------------------		
	
	// Hooray!!! no stores here!!!
	
	//--------------------------- MAQUINA DE ESTADO --------------------------------
	
	var procesaGeneraCedula = function(opts, success, response){
		
		// Ocultar mascaras segun se requiera
      Ext.getCmp('contenedorPrincipal').ocultaMascaras(opts);
        	
		if( success == true && Ext.util.JSON.decode(response.responseText).success == true ){
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						generaCedula(resp.estadoSiguiente,resp);
					}
				);
			} else {
				generaCedula(resp.estadoSiguiente,resp);
			}
			
		} else {
 
			// Suprimir m�scara de la forma "G�nera C�dula"
			var element = Ext.getCmp("formaGeneraCedula").getEl();
			if( element.isMasked()){
				element.unmask();
			}
						
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
 
		}
		
	}
	
	var generaCedula = function(estadoSiguiente, respuesta){
		
		if( 			estadoSiguiente == "INICIALIZACION" ){
			
			// Inicializar 
			
			// 1. Ocultar Campo: Meses con Publicacion
			Ext.getCmp("labelMesesConPublicacion").hide();
			Ext.getCmp("mesesConPublicacion").hide();
			
			// 2. Ocultar Campo: Meses a Evaluar
			Ext.getCmp("labelMesesAEvaluar").hide();
			Ext.getCmp("mesesAEvaluar").hide();
				
			// 3. Cargar Combo Nombre de la EPO
			var catalogoEPOData = Ext.StoreMgr.key('catalogoEPODataStore');
			catalogoEPOData.load();
			
			// 4. Cargar combo Nombre de la PYME
			var catalogoPYMEData = Ext.StoreMgr.key('catalogoPYMEDataStore');
			catalogoPYMEData.load({
				params: {
					claveEPO: Ext.getCmp("comboEPO").getValue()	
				}
			});
 
			// 5. Cargar catalogo EPO's relacionadas
			var catalogoEPOSRelacionadasData = Ext.StoreMgr.key('catalogoEPOSRelacionadasDataStore');
			catalogoEPOSRelacionadasData.load({
				params: {
					clavePYME: Ext.getCmp("comboPYME").getValue()	
				}
			});
 
			// 6. Cargar Catalogo de Monedas
			var catalogoMonedaData = Ext.StoreMgr.key('catalogoMonedaDataStore');
			catalogoMonedaData.load();
			
			// 7. Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13consulta11ext.data.jsp',
				params: 	{
					informacion:		'GeneraCedula.inicializacion'
				},
				callback: 				procesaGeneraCedula
			});
		
		} else if( estadoSiguiente == "ACTUALIZAR_FECHA_VALUACION"    ){

			var fechaDeValuacionInicial = Ext.getCmp("fechaDeValuacionInicial");
			var fechaDeValuacionFinal   = Ext.getCmp("fechaDeValuacionFinal");
			
			// ACTUALIZAR FECHAS
			fechaDeValuacionInicial.setValue(respuesta.fechaDeValuacionInicial);
			fechaDeValuacionInicial.originalValue = respuesta.fechaDeValuacionInicial;
			
			fechaDeValuacionFinal.setValue(respuesta.fechaDeValuacionFinal);
			fechaDeValuacionFinal.originalValue   = respuesta.fechaDeValuacionFinal;
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13consulta11ext.data.jsp',
				params: 	{
					informacion:		'GeneraCedula.actualizarFechaValuacion'
				},
				callback: 				procesaGeneraCedula
			});
			
		} else if( estadoSiguiente == "ESPERAR_DECISION"    			  ){
			
			// Suprimir m�scara de la forma "G�nera C�dula"
			var element = Ext.getCmp("formaGeneraCedula").getEl();
			if( element.isMasked()){
				element.unmask();
			}
						
		} else if( estadoSiguiente == "GENERAR_CEDULA"      ){
			
			var formaGeneraCedula = Ext.getCmp("formaGeneraCedula");
			
			// Agregar mascara: "Generando C�dula"
			formaGeneraCedula.getEl().mask('Generando C�dula...','x-mask-loading');
			
			// Realizar accion
			Ext.Ajax.request({
				url: 						'13consulta11ext.data.jsp',
				params: 	Ext.apply(
					formaGeneraCedula.getForm().getValues(),
					{
						informacion:	'GeneraCedula.generarCedula',
						esCedula8x12:	respuesta.esCedula8x12,
						hidAction812:	respuesta.hidAction812,
						hidAction:		respuesta.hidAction
					}
				),
				callback: 				procesaGeneraCedula
			});
			
		} else if( estadoSiguiente == "MOSTRAR_CEDULA" ){
			
			var formaGeneraCedula       = Ext.getCmp("formaGeneraCedula");
			var contenedorDetalleCedula = Ext.getCmp("contenedorDetalleCedula");
			
			// GUARDAR UNA COPIA DE LOS �LTIMO PARAMETROS DE LA CONSULTA
			contenedorDetalleCedula.formParams = respuesta.params;
			
			// ACTUALIZAR CONTENIDOS
			Ext.getCmp("fechaEmisionCedula.DETALLE_OPERACIONES").setValue('Fecha de Emisi�n: <b>'+respuesta.fechaEmision+'</b>');
			Ext.getCmp("detalleEPOProveedor.NOMBRE_PROVEEDOR").setValue('<b>'+respuesta.nombreProveedor+'</b>');
			Ext.getCmp("detalleEPOProveedor.RFC").setValue('<b>'+respuesta.rfc+'</b>');
			Ext.getCmp("detalleEPOProveedor.TELEFONOS").setValue(respuesta.telefonos);
			Ext.getCmp("detalleEPOProveedor.NOMBRE_EPO").setValue('<b>'+respuesta.nombreEpo+'</b>');
			
			Ext.getCmp("fechasDetalleCedula.FECHA_INGRESO_CADENA").setValue('<b><u>Fecha de Ingreso a la Cadena</u></b><br>&nbsp;<br><b>'+respuesta.fechaIngresoCadena+'</b>');
			Ext.getCmp("fechasDetalleCedula.FECHA_INICIO_OPERACIONES").setValue('<b><u>Fecha de Inicio de Operaciones</u></b><br>&nbsp;<br><b>'+respuesta.fechaInicioOperaciones+'</b>');
			Ext.getCmp("fechasDetalleCedula.NUMERO_MESES_OPERANDO").setValue('<b><u>N�m. Meses Operando</u></b><br>&nbsp;<br><b>'+respuesta.numeroMesesOperando+'</b>');
			
			Ext.StoreMgr.key('detalleOperacionesDataStore').loadData(respuesta.detalleOperaciones);
			Ext.StoreMgr.key('detalleTotalOperacionesDataStore').loadData(respuesta.totalDetalleOperaciones);
			
			Ext.getCmp("pieDetalleCedula.MONTO_CREDITO_OTORGAR").setValue('Monto M�ximo del Cr�dito a Otorgar por esta Cadena:&nbsp;<b>'+respuesta.montoCreditoOtorgar+'</b>');
			Ext.getCmp("pieDetalleCedula.SUSCEPTIBLE").setValue('Susceptible:&nbsp;<b>'+respuesta.susceptible+'</b>');
			Ext.getCmp("pieDetalleCedula.SOLICITO").setValue('Solicit�:&nbsp;<b>'+respuesta.solicito+'</b>');
 
			// ACTUALIZAR FECHAS
			Ext.getCmp("fechaDeValuacionInicial").setValue(respuesta.fechaDeValuacionInicial);
			Ext.getCmp("fechaDeValuacionFinal").setValue(respuesta.fechaDeValuacionFinal);
 
			// ACTUALIZAR ESTATUS DE LOS BOTONES
			if(respuesta.params.esCedula8x12 === true){
				Ext.getCmp("labelMesesConPublicacion").show();
				Ext.getCmp("mesesConPublicacion").show();
				Ext.getCmp("labelMesesAEvaluar").show();
				Ext.getCmp("mesesAEvaluar").show();
				Ext.getCmp("botonGenerarCedula8x12").show();
			} else {
				Ext.getCmp("labelMesesConPublicacion").hide();
				Ext.getCmp("mesesConPublicacion").hide();
				Ext.getCmp("labelMesesAEvaluar").hide();
				Ext.getCmp("mesesAEvaluar").hide();
				Ext.getCmp("botonGenerarCedula8x12").hide();
			}
			
			if(respuesta.showBotonGenerarCedula8x12pie === true){
				Ext.getCmp("botonGenerarCedula8x12pie").show();
			} else {
				Ext.getCmp("botonGenerarCedula8x12pie").hide();
			}
 
			Ext.getCmp("btnGeneraPDF").enable();
			Ext.getCmp("btnGeneraCSV").enable();
			Ext.getCmp("btnBajaPDF").hide();
			Ext.getCmp("btnBajaCSV").hide();
				
			// SUPRIMIR M�SCARA DE LA FORMA "G�NERA C�DULA"
			var element = Ext.getCmp("formaGeneraCedula").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Mostrar Detalle de la C�dula
			contenedorDetalleCedula.show();
        
		}
		
	}
		
	//--------------------------- DETALLE DE LA CEDULA: LABELS PIE -----------------------------
	
	var pieDetalleCedula	  = new Ext.Container({
		layout: 'table',
    	style:  'padding-top:20;',
    	defaults: {
        // applied to each contained panel
        style:'padding-top:5;padding-left:0;padding-right:0;padding-bottom:15;text-align:left;border: 0;'
   	},
    	layoutConfig: {
        tableAttrs: {
            style: {
               width: '100%'
            }
        },
        // The total column count must be specified here
        columns: 2
    	},
   	items: [
    		{
    			xtype:	'displayfield',
    			id: 		'pieDetalleCedula.MONTO_CREDITO_OTORGAR',
				html:    'Monto M�ximo del Cr�dito a Otorgar por esta Cadena:&nbsp;<b>000000.00</b>',
				cellCls:	'x-form-item',
				width:   600
			},
			{
				xtype:	'displayfield',
				id: 		'pieDetalleCedula.SUSCEPTIBLE',
				html:    'Susceptible:&nbsp;<b>No</b>',
				cellCls:	'x-form-item'
			},
			{
				xtype:	'displayfield',
    			id: 		'pieDetalleCedula.SOLICITO',
				html:    'Solicit�:&nbsp;<b>Nombre Ap. Paterno Ap. Materno.</b>',
				cellCls:	'x-form-item',
				width:   600
			},
			{
				xtype:	'button',
				id:		'botonGenerarCedula8x12pie',
				text: 	'Generar C�dula 8 por 12',
				hidden: 	false,
				iconCls: 'icoGenerarCedula',
				handler: function() {
					
					var mesesConPublicacion = Ext.getCmp("mesesConPublicacion");
					mesesConPublicacion.setValue(8);
					
					var mesesAEvaluar = Ext.getCmp("mesesAEvaluar");
					mesesAEvaluar.setValue(12);
					
					// VALIDAR FORMA GENERA CEDULA
					var esCedula8x12 = true;
					if( !validaFormaGeneraCedula(esCedula8x12)){
						return;
					}
					
					// GENERAR CEDULA
					var respuesta = new Object();
					respuesta.esCedula8x12 	= esCedula8x12;
					respuesta.hidAction812 	= "G"; // SE ENV�A HID ACTION CON VALOR
					respuesta.hidAction 		= "";
					generaCedula( "GENERAR_CEDULA", respuesta );
					
				}
			}
		]
   });
   
   //---------------- DETALLE DE LA CEDULA: TOTALES GRID DETALLE OPERACIONES ----------------
   
   var procesarConsultaDetalleTotalOperaciones = function(store, registros, opts){

		var gridDetalleTotalOperaciones = Ext.getCmp('gridDetalleTotalOperaciones');
		var el 								  = gridDetalleTotalOperaciones.getGridEl();
		el.unmask();
		
		if (registros != null) {
			
			/*
			if (!gridDetalleTotalOperaciones.isVisible()) {
				gridDetalleTotalOperaciones.show();
			}		
			*/
			
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
  
			// Cargar totales
			// store.reader.jsonData.summaryData
		
		}
 
	}
	
	var detalleTotalOperacionesData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'detalleTotalOperacionesDataStore',
		url: 		'13consulta11ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetalleTotalOperaciones'
		},
		idIndex: 		1,
		fields: [			
			{ name:'MES_ANIO',				type:"string" },
			{ name:'NUMERO_PUBLICADOS',	type:"int"    },
			{ name:'MONTO_PUBLICADOS',		type:"float", convert: function(value, record){ return Number(value.replace(/[\$,]/g,"")); } },
			{ name:'NUMERO_OPERADOS',		type:"int"    },
			{ name:'MONTO_OPERADOS',		type:"float", convert: function(value, record){ return Number(value.replace(/[\$,]/g,"")); } },
			{ name:'NUMERO_SIN_OPERAR',	type:"int"    },
			{ name:'MONTO_SIN_OPERAR',		type:"float", convert: function(value, record){ return Number(value.replace(/[\$,]/g,"")); } }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
				}
			},
			load: 	procesarConsultaDetalleTotalOperaciones,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDetalleTotalOperaciones(null, null, null);						
				}
			}
		}
		
	});
	   
	var mesAnioRenderer01 = function( value, metadata, record, rowIndex, colIndex, store){
   	
   	metadata.attr = 'style="color:#333; background: #f1f2f4; text-align:right;"';
		return value;
		
	}
	
   var numeroRenderer01 = function( value, metadata, record, rowIndex, colIndex, store){
 
		var dataIndex = Ext.getCmp("gridDetalleOperaciones").getColumnModel().getDataIndex(colIndex);
   	metadata.attr = 'style="text-align:right;"';
		return record.json[dataIndex];
		
	}
	
   var montoRenderer01 = function( value, metadata, record, rowIndex, colIndex, store){
 
		var dataIndex = Ext.getCmp("gridDetalleOperaciones").getColumnModel().getDataIndex(colIndex);
   	metadata.attr = 'style="text-align:right;"';
		return record.json[dataIndex];
		
	}
		
	var gridDetalleTotalOperaciones = new Ext.grid.GridPanel({
		store: 	detalleTotalOperacionesData,
		id:		'gridDetalleTotalOperaciones',
		hidden: 	false,
		margins: '20 0 0 0',
		style: 	'border-width: 1;',
		hideHeaders: true,
		columns: [
			{
				header: 		'Mes / A�o',
				tooltip: 	'Mes / A�o',
				dataIndex: 	'MES_ANIO',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				hidden: 		false,
				align:		'center',
				renderer:	mesAnioRenderer01
			},
			{
				header: 		'N�mero',
				tooltip: 	'N�mero',
				dataIndex: 	'NUMERO_PUBLICADOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroRenderer01
			},
			{
				header: 		'Monto',
				tooltip: 	'Monto',
				dataIndex: 	'MONTO_PUBLICADOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		135,
				hidden: 		false,
				renderer:	montoRenderer01
			},
			{
				header: 		'N�mero',
				tooltip: 	'N�mero',
				dataIndex: 	'NUMERO_OPERADOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroRenderer01
			},
			{
				header: 		'Monto',
				tooltip: 	'Monto',
				dataIndex: 	'MONTO_OPERADOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		135,
				hidden: 		false,
				renderer:	montoRenderer01
			},
			{
				header: 		'N�mero',
				tooltip: 	'N�mero',
				dataIndex: 	'NUMERO_SIN_OPERAR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroRenderer01
			},
			{
				header: 		'Monto',
				tooltip: 	'Monto',
				dataIndex: 	'MONTO_SIN_OPERAR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		135,
				hidden: 		false,
				renderer:	montoRenderer01
			}
		],
		stripeRows: true,
		loadMask: 	true,
		height: 		44,		
		frame: 		false,
		flex:			7
	});
	
   //-------------------- DETALLE DE LA CEDULA: GRID DETALLE OPERACIONES -------------------- 
   
	var procesarConsultaDetalleOperaciones = function(store, registros, opts){

		var gridDetalleOperaciones = Ext.getCmp('gridDetalleOperaciones');
		var el 								 = gridDetalleOperaciones.getGridEl();
		el.unmask();
		
		if (registros != null) {
			
			/*
			if (!gridDetalleOperaciones.isVisible()) {
				gridDetalleOperaciones.show();
			}		
			*/
			
			if(store.getTotalCount() == 0) {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
  
			// Cargar totales
			/*
			var detalleTotalOperacionesData = Ext.StoreMgr.key('detalleTotalOperacionesDataStore');
			detalleTotalOperacionesData.loadData(store.reader.jsonData.summaryData);
			*/
		}
 
	}
	
	var detalleOperacionesData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'detalleOperacionesDataStore',
		url: 		'13consulta11ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetalleOperaciones'
		},
		idIndex: 		1,
		fields: [			
			{ name:'MES_ANIO',				type:"string" },
			{ name:'NUMERO_PUBLICADOS',	type:"int"    },
			{ name:'MONTO_PUBLICADOS',		type:"float", convert: function(value, record){ return Number(value.replace(/[\$, ]/g,"")); } },
			{ name:'NUMERO_OPERADOS',		type:"int"    },
			{ name:'MONTO_OPERADOS',		type:"float", convert: function(value, record){ return Number(value.replace(/[\$, ]/g,"")); } },
			{ name:'NUMERO_SIN_OPERAR',	type:"int"    },
			{ name:'MONTO_SIN_OPERAR',		type:"float", convert: function(value, record){ return Number(value.replace(/[\$, ]/g,"")); } }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload: {
				fn: function( store, opts ) {
				}
			},
			load: 	procesarConsultaDetalleOperaciones,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDetalleOperaciones(null, null, null);						
				}
			}
		}
		
	});
	
	var gridDetalleOperacionesColumnHeaderGroup = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
        	  [
        	  	  { header: '&nbsp;',             		colspan: 1, align: 'center' },
        	  	  { header: 'Documentos Publicados',	colspan: 2, align: 'center' },
        	  	  { header: 'Documentos Operados', 		colspan: 2, align: 'center' },
        	  	  { header: 'Documentos Sin Operar',	colspan: 2, align: 'center' }
        	  ]
        ]
   });
   
   var numeroRenderer = function( value, metadata, record, rowIndex, colIndex, store){
   	
   	var dataIndex = Ext.getCmp("gridDetalleOperaciones").getColumnModel().getDataIndex(colIndex);
   	metadata.attr = 'style="text-align:right;"';
		return record.json[dataIndex];
		
	}
	
   var montoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		var dataIndex = Ext.getCmp("gridDetalleOperaciones").getColumnModel().getDataIndex(colIndex);
   	metadata.attr = 'style="text-align:right;"';
		return record.json[dataIndex];
		
	}
		
	var gridDetalleOperaciones = new Ext.grid.GridPanel({
		store: 	detalleOperacionesData,
		id:		'gridDetalleOperaciones',
		hidden: 	false,
		margins: '20 0 0 0',
		style: 'border-width: 1;',
		columns: [
			{
				header: 		'Mes / A�o',
				tooltip: 	'Mes / A�o',
				dataIndex: 	'MES_ANIO',
				sortable: 	true,
				resizable: 	true,
				width: 		116,
				hidden: 		false,
				align:		'center'
			},
			{
				header: 		'N�mero',
				tooltip: 	'N�mero',
				dataIndex: 	'NUMERO_PUBLICADOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false, 
				renderer:	numeroRenderer
			},
			{
				header: 		'Monto',
				tooltip: 	'Monto',
				dataIndex: 	'MONTO_PUBLICADOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		135,
				hidden: 		false,
				renderer:	montoRenderer
			},
			{
				header: 		'N�mero',
				tooltip: 	'N�mero',
				dataIndex: 	'NUMERO_OPERADOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroRenderer
			},
			{
				header: 		'Monto',
				tooltip: 	'Monto',
				dataIndex: 	'MONTO_OPERADOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		135,
				hidden: 		false,
				renderer:	montoRenderer
			},
			{
				header: 		'N�mero',
				tooltip: 	'N�mero',
				dataIndex: 	'NUMERO_SIN_OPERAR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	numeroRenderer
			},
			{
				header: 		'Monto',
				tooltip: 	'Monto',
				dataIndex: 	'MONTO_SIN_OPERAR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		135,
				hidden: 		false,
				renderer:	montoRenderer
			}
		],
		stripeRows: true,
		loadMask: 	true,
		height: 		301,		
		frame: 		false,
		flex:			7,
		plugins:		gridDetalleOperacionesColumnHeaderGroup
	});
	
	//-------------------- DETALLE DE LA CEDULA: LABEL DETALLE OPERACIONES --------------------
	
	var detalleOperacionesCedula = {
		xtype:'displayfield',
		id:	'detalleOperacionesCedula',
		html: 'Detalle de Operaciones',
		cls:	'x-form-item',
		style:'padding-bottom: 5;'
	};
 
	//-------------------- DETALLE DE LA CEDULA: LABEL FECHAS --------------------
	
	var fechasDetalleCedula	  = new Ext.Container({
		layout: 'table',
   	width:  600,
    	style:  'padding-left:64;padding-right:64;',
    	defaults: {
        // applied to each contained panel
        bodyStyle:'padding:5;text-align:center;border: 0;'
   	},
    	layoutConfig: {
        tableAttrs: {
            style: {
               width: '100%'
            }
        },
        // The total column count must be specified here
        columns: 3
    	},
   	items: [
    		{
    			xtype:	'displayfield',
    			id:	 	'fechasDetalleCedula.FECHA_INGRESO_CADENA',
				html: 	'<b><u>Fecha de Ingreso a la Cadena</u></b><br>&nbsp;<br>00/00/0000',
				cellCls:	'x-form-item',
				style:	'text-align: center;'
			},
			{
				xtype:	'displayfield',
				id:	 	'fechasDetalleCedula.FECHA_INICIO_OPERACIONES',
				html: 	'<b><u>Fecha de Inicio de Operaciones</u></b><br>&nbsp;<br>00/00/0000',
				cellCls:	'x-form-item',
				style:	'text-align: center;'
			},
			{
				xtype:	'displayfield',
				id:	 	'fechasDetalleCedula.NUMERO_MESES_OPERANDO',
				html: 	'<b><u>N�m. Meses Operando</u></b><br>&nbsp;<br>00/00/0000',
				cellCls:	'x-form-item',
				style:	'text-align: center;'
			}
		]
   });
	
   //-------------------- DETALLE DE LA CEDULA: LABELS DETALLE EPO PROVEEDOR --------------------
    
	var detalleEPOProveedor = new Ext.Container({
		layout: 'form',
		hidden: false,
		flex:   0,
		style: 'padding-left:128;padding-right:128;',
		labelWidth:	140,
		items:[
			NE.util.getEspaciador(25),
			{
				xtype: 		'displayfield',
				id:	 		'detalleEPOProveedor.NOMBRE_PROVEEDOR',
				fieldLabel: 'Nombre del Proveedor'
			},
			{
				xtype: 		'displayfield',
				id:	 		'detalleEPOProveedor.RFC',
				fieldLabel: 'R.F.C.'
			},
			{
				xtype: 		'displayfield',
				id:	 		'detalleEPOProveedor.TELEFONOS',
				fieldLabel: 'Tel�fonos'
			},
			NE.util.getEspaciador(25),
			{
				xtype: 		'displayfield',
				id:	 		'detalleEPOProveedor.NOMBRE_EPO',
				fieldLabel: 'Nombre de la EPO'
			},
			NE.util.getEspaciador(25)
		]
	});
	
	//-------------------- DETALLE DE LA CEDULA: LABEL FECHA EMISION --------------------
	 
	var fechaEmisionCedula = {
		xtype:	'displayfield',
		id:		'fechaEmisionCedula.DETALLE_OPERACIONES',
		html: 	'Fecha de Emisi�n: <b>00/00/0000</b>',
		cls:		'x-form-item',
		style: 	'text-align:right;'
	};
	
	//-------------------- DETALLE DE LA CEDULA: CONTENEDOR PRINCIPAL --------------------
	 
	var procesarSuccessFailureGeneraPDF =  function(opts, success, response) {
		
		var btnGeneraPDF = Ext.getCmp('btnGeneraPDF');
		btnGeneraPDF.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaPDF = Ext.getCmp('btnBajaPDF');
			btnBajaPDF.show();
			btnBajaPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaPDF.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
			
		} else {
			
			btnGeneraPDF.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var procesarSuccessFailureGeneraCSV =  function(opts, success, response) {
		
		var btnGeneraCSV = Ext.getCmp('btnGeneraCSV');
		btnGeneraCSV.setIconClass('icoGenerarDocumento');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var btnBajaCSV = Ext.getCmp('btnBajaCSV');
			btnBajaCSV.show();
			btnBajaCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajaCSV.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
			
		} else {
			
			btnGeneraCSV.enable();
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var contenedorDetalleCedula = {
		xtype: 	'panel',
		id:	 	'contenedorDetalleCedula',
		frame:	true,
		hidden:	true,
		width:	800,
		style: 	'margin: 0 auto',
		title: 	'C�dula Evaluaci�n',
		height: 	803,
		layout: {
			type:		'vbox',
			padding:	'10',
			align:	'stretch'
		},
		defaults:{
			margins:'0 0 0 0'
		},
		formParams: {},
		items: [ 
			fechaEmisionCedula,
			detalleEPOProveedor,
			fechasDetalleCedula,
			detalleOperacionesCedula,
			gridDetalleOperaciones,
			gridDetalleTotalOperaciones,
			pieDetalleCedula
		],
		buttons: [
			{
				xtype: 	'button',
				text: 	'Generar PDF',
				id: 		'btnGeneraPDF',
				iconCls: 'icoGenerarDocumento',
				disabled: false,
				handler: function(boton, evento) {
						
					boton.disable();
					boton.setIconClass('loading-indicator');
						
					// Obtener copia del contenedor
					var contenedorDetalleCedula = Ext.getCmp("contenedorDetalleCedula");
						
					// Copiar ultimos parametros de la consulta
					var formParams = Ext.apply( {}, contenedorDetalleCedula.formParams );
					
					// Generar Archivo PDF
					Ext.Ajax.request({
						url: 		'13consulta11ext.data.jsp',
						params: 	Ext.apply(
							formParams,
							{
								informacion: 'GenerarArchivo',
								tipo: 		 'PDF'
							}
						),
						callback: procesarSuccessFailureGeneraPDF
					});
						
				}
			},
			{
				xtype: 	'button',
				text: 	'Bajar PDF',
				id: 		'btnBajaPDF',
				iconCls:	'icoBotonPDF',
				hidden: 	true
			},
			{
				xtype: 	'button',
				text: 	'Generar Archivo',
				id: 		'btnGeneraCSV',
				iconCls:	'icoGenerarDocumento',
				disabled: false,
				handler: function(boton, evento){
								
					boton.disable();
					boton.setIconClass('loading-indicator');
							
					// Obtener copia del contenedor
					var contenedorDetalleCedula = Ext.getCmp("contenedorDetalleCedula");
						
					// Copiar ultimos parametros de la consulta
					var formParams = Ext.apply( {}, contenedorDetalleCedula.formParams );
						
					// Generar Archivo CSV
					Ext.Ajax.request({
						url: 		'13consulta11ext.data.jsp',
						params: 	Ext.apply(
							formParams, 
							{
								informacion: 'GenerarArchivo',
								tipo: 		 'CSV'
							}
						),
						callback: procesarSuccessFailureGeneraCSV
					});
							
				}
			},
			{
				xtype: 	'button',
				text: 	'Bajar Archivo',
				id: 		'btnBajaCSV',
				iconCls:	'icoBotonXLS',
				hidden: 	true
			}
		]
	};
	
	//---------------------------------- FORMA -------------------------------------
	
	var validaFormaGeneraCedula = function(esCedula8x12){
		
		var invalidForm = false;
		if( !Ext.getCmp("formaGeneraCedula").getForm().isValid() ){
			invalidForm = true;
		}
		
		// VALIDAR QUE SE HAYA SELECCIONADO UNA EPO
		var comboEPO = Ext.getCmp("comboEPO");
		if( comboEPO.isValid() && Ext.isEmpty(comboEPO.getValue()) ){
			invalidForm = true;
			comboEPO.markInvalid("Este campo es obligatorio"); 
		}

		// VALIDA QUE SE HAYA SELECCIONADO UNA PYME
		var comboPYME = Ext.getCmp("comboPYME");
		if( comboPYME.isValid() && Ext.isEmpty(comboPYME.getValue()) ){
			invalidForm = true;
			comboPYME.markInvalid("Este campo es obligatorio"); 
		}
		
		// VALIDA QUE SE HAYA SELECCIONADO UNA EPO RELACIONADA
		var comboEPOSRelacionadas = Ext.getCmp("comboEPOSRelacionadas");
		if( comboEPOSRelacionadas.isValid() && Ext.isEmpty(comboEPOSRelacionadas.getValue()) ){
			invalidForm = true;
			comboEPOSRelacionadas.markInvalid("Este campo es obligatorio"); 
		}

		// VALIDA QUE SE HAYA SELECCIONADO UNA MONEDA
		var comboMoneda = Ext.getCmp("comboMoneda");
		if( comboMoneda.isValid() && Ext.isEmpty(comboMoneda.getValue()) ){
			invalidForm = true;
			comboMoneda.markInvalid("Este campo es obligatorio");
		}
		
		// VALIDA QUE SE HAYA SELECCIONADO UN FACTOR DE CREDICADENAS
		var factorCredicadenas = Ext.getCmp("factorCredicadenas");
		if( factorCredicadenas.isValid() && Ext.isEmpty(factorCredicadenas.getValue()) ){
			invalidForm = true;
			factorCredicadenas.markInvalid("Este campo es obligatorio"); 
		}
		
		// VALIDA QUE SE HAYAN CAPTURADO LOS ULTIMOS MESES CONSECUTIVOS
		var ultimosMesesConsecutivos = Ext.getCmp("ultimosMesesConsecutivos");
		if( ultimosMesesConsecutivos.isValid() && Ext.isEmpty(ultimosMesesConsecutivos.getValue()) ){
			invalidForm = true;
			ultimosMesesConsecutivos.markInvalid("Este campo es obligatorio"); 
		}
		
		// VALIDA QUE SE HAYAN CAPTURADO LOS MESES A PROMEDIAR
		var mesesAPromediar = Ext.getCmp("mesesAPromediar");
		if( mesesAPromediar.isValid() && Ext.isEmpty(mesesAPromediar.getValue()) ){
			invalidForm = true;
			mesesAPromediar.markInvalid("Este campo es obligatorio"); 
		} else if(  ultimosMesesConsecutivos.isValid() && ultimosMesesConsecutivos.getValue() > mesesAPromediar.getValue() ){
			invalidForm = true;
			mesesAPromediar.markInvalid("Meses a Promediar debe ser mayor o igual a Ultimos Meses Consecutivos."); 
		}

		// VALIDA QUE SE HAYA CAPTURADO EL PERIODO DE INCIO 		
		var fechaDeValuacionInicial = Ext.getCmp("fechaDeValuacionInicial");
		if( !fechaDeValuacionInicial.isValid() ){
			invalidForm = true;
		}
		
		// VALIDA QUE SE HAYA CAPTURADO EL PERIODO FINAL DE EVALUACION
		var fechaDeValuacionFinal = Ext.getCmp("fechaDeValuacionFinal");
		if( !fechaDeValuacionFinal.isValid() ){
			invalidForm = true;
		}
		
		// VALIDA QUE EL PERIODO DE INICIO NO SEA MAYOR AL PERIODO FINAL
		//  "La fecha 'Periodo de evaluaci�n a' no puede ser menor\nque la fecha 'Periodo de evaluaci�n de'"
		// Nota: Esta validacion no es necesaria, debido a que ya se realiza implicitamente en el componente

		// VALIDAR PAR�METROS CEDULA 8X12
		if( esCedula8x12 ){
			
			// VALIDA QUE SE HAYAN CAPTURADO LOS MESES CON PUBLICACION
			var mesesConPublicacion = Ext.getCmp("mesesConPublicacion");
			if( mesesConPublicacion.isValid() && Ext.isEmpty(mesesConPublicacion.getValue()) ){
				invalidForm = true;
				mesesConPublicacion.markInvalid("Este campo es obligatorio"); 
			}
			
			// VALIDA QUE SE HAYAN CAPTURADO LOS MESES A EVALUAR
			var mesesAEvaluar = Ext.getCmp("mesesAEvaluar");
			if( mesesAEvaluar.isValid() && Ext.isEmpty(mesesAEvaluar.getValue()) ){
				invalidForm = true;
				mesesAEvaluar.markInvalid("Este campo es obligatorio"); 
			}
			
		}
		
		return !invalidForm;
		
	}
	
	var procesaGenerarCedula8x12 = function( boton, evento ) {
					
		// VALIDAR FORMA GENERA CEDULA
		var esCedula8x12 = true;
		if( !validaFormaGeneraCedula(esCedula8x12)){
			return;
		}
		
		// GENERAR CEDULA
		var respuesta = new Object();
		respuesta.esCedula8x12 	= esCedula8x12;
		respuesta.hidAction 		= "";
		respuesta.hidAction812 	= "G"; // SE ENV�A HID ACTION CON VALOR
		generaCedula( "GENERAR_CEDULA", respuesta );
					
	}
	
	var procesaGenerarCedula = function() {
			
		// VALIDAR FORMA GENERA CEDULA
		var esCedula8x12 = false;
		if( !validaFormaGeneraCedula(esCedula8x12)){
			return;
		}
		
		// GENERAR CEDULA
		var respuesta = new Object();
		respuesta.esCedula8x12 	= esCedula8x12;
		respuesta.hidAction		= "G";
		respuesta.hidAction812	= "";  // SE ENVIA HID ACTION 812 COMO VAC�O
		generaCedula( "GENERAR_CEDULA", respuesta );
					
	}
	
	var procesaLimpiarForma = function() {
 
		var catalogoPYMEData 				= Ext.StoreMgr.key('catalogoPYMEDataStore');
		var catalogoEPOSRelacionadasData = Ext.StoreMgr.key('catalogoEPOSRelacionadasDataStore');
		
		catalogoPYMEData.removeAll();
		catalogoEPOSRelacionadasData.removeAll();
		Ext.getCmp("formaGeneraCedula").getForm().reset();
		
		Ext.getCmp("comboPYME").setNewEmptyText("Seleccione una EPO");
		Ext.getCmp("comboEPOSRelacionadas").setNewEmptyText("Seleccione una PYME");
		Ext.getCmp("fechaDeValuacionInicial").setMaxValue('');
		Ext.getCmp("fechaDeValuacionFinal").setMinValue('');
		
		Ext.getCmp("botonGenerarCedula8x12").hide();
		Ext.getCmp("labelMesesConPublicacion").hide();
		Ext.getCmp("mesesConPublicacion").hide();
		Ext.getCmp("labelMesesAEvaluar").hide();
		Ext.getCmp("mesesAEvaluar").hide();
 
	}
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 			'catalogoEPODataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13consulta11ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
					
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String( options.params.defaultValue );
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboEPO").setValue(defaultValue);
						Ext.getCmp("comboEPO").originalValue = defaultValue;
					}
					//Ext.getCmp("comboEPO").setReadOnly(true);
								
				} 
									
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPYMEData = new Ext.data.JsonStore({
		id: 			'catalogoPYMEDataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13consulta11ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
									
				// Determinar el mensaje de seleccion que se mostrar� en el combo
				if( !Ext.isDefined(options) || !Ext.isDefined(options.params) || Ext.isEmpty(options.params.claveEPO)){
					Ext.getCmp("comboPYME").setNewEmptyText('Seleccione una EPO');
					return;
				}else{
					Ext.getCmp("comboPYME").setNewEmptyText('Seleccionar...');
				}
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String( options.params.defaultValue );
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboPYME").setValue(defaultValue);
						Ext.getCmp("comboPYME").originalValue = defaultValue;
					}
					//Ext.getCmp("comboPYME").setReadOnly(true);
 
				}
 
				// Disparar evento de seleccion de componente
				var comboPYME = Ext.getCmp("comboPYME");
				comboPYME.fireEvent('select',comboPYME);
				
 
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPOSRelacionadasData = new Ext.data.JsonStore({
		id: 			'catalogoEPOSRelacionadasDataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13consulta11ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPOSRelacionadas'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
					
				// Determinar el mensaje de seleccion que se mostrar� en el combo
				if( !Ext.isDefined(options) || !Ext.isDefined(options.params) || Ext.isEmpty(options.params.clavePYME) ){
					Ext.getCmp("comboEPOSRelacionadas").setNewEmptyText('Seleccione una PYME');
					return;
				}else{
					Ext.getCmp("comboEPOSRelacionadas").setNewEmptyText('Seleccionar...');
				}

				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String( options.params.defaultValue );
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboEPOSRelacionadas").setValue(defaultValue);
						Ext.getCmp("comboEPOSRelacionadas").originalValue = defaultValue;
					}
					//Ext.getCmp("comboEPOSRelacionadas").setReadOnly(true);
								
				} 
									
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
 
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 			'catalogoMonedaDataStore',
		root: 		'registros',
		fields: 		['clave', 'descripcion', 'loadMsg'],
		url: 			'13consulta11ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboMoneda").setValue(defaultValue);
					}
					// Ext.getCmp("comboMoneda").setReadOnly(true);
									
				} 
				
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementosFormaGeneraCedula = [
		// COMBO: Nombre de la EPO
		{
			xtype: 				'combo',
			name: 				'comboEPO',
			id: 					'comboEPO',
			fieldLabel: 		'Nombre de la EPO',
			allowBlank: 		true,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			emptyText: 			'Seleccionar...', 
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			//tpl: 					NE.util.templateMensajeCargaCombo,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word; ">{descripcion}</div>' +
									'</div></tpl></tpl>',
			anchor:				'-220',
			listeners: {
				select: function(combo, record, index ) { 

					// Actualizar combo PYME
					var catalogoPYMEData = Ext.StoreMgr.key('catalogoPYMEDataStore');
					catalogoPYMEData.load({
						params: {
							claveEPO: 		combo.getValue(),
							defaultValue:	Ext.getCmp("comboPYME").getValue()
						}
					});
 
				}
			}
		},
		// COMBO: Nombre de la PYME
		{
			xtype: 				'combo',
			name: 				'comboPYME',
			id: 					'comboPYME',
			fieldLabel: 		'Nombre de la PYME',
			allowBlank: 		true,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'clavePYME',
			emptyText: 			'Seleccione una EPO', 
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoPYMEData,
			//tpl: 					NE.util.templateMensajeCargaCombo,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word; ">{descripcion}</div>' +
									'</div></tpl></tpl>',
			anchor:				'-220',
			listeners: {
				select: function(combo, record, index ) { 
					
					var catalogoEPOSRelacionadasData = Ext.StoreMgr.key('catalogoEPOSRelacionadasDataStore');
					catalogoEPOSRelacionadasData.load({
						params: {
							clavePYME: combo.getValue(),
							defaultValue:	Ext.getCmp("comboEPOSRelacionadas").getValue()
						}
					});
				}
			},
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// COMBO: EPO's Relacionadas
		{
			xtype: 				'combo',
			name: 				'EPOSRelacionadas',
			id: 					'comboEPOSRelacionadas',
			fieldLabel: 		'EPO\'s Relacionadas',
			allowBlank: 		true,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPOSRelacionadas',
			emptyText: 			'Seleccione una PYME', 
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOSRelacionadasData,
			//tpl: 				NE.util.templateMensajeCargaCombo,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word; ">{descripcion}</div>' +
									'</div></tpl></tpl>',
			anchor:				'-220',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			}
		},
		// COMBO MONEDA
		{
			xtype: 				'combo',
			name: 				'comboMoneda',
			id: 					'comboMoneda',
			fieldLabel: 		'Moneda',
			allowBlank: 		true,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveMoneda',
			emptyText: 			'Seleccionar...',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoMonedaData,
			//tpl: 					NE.util.templateMensajeCargaCombo,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word; ">{descripcion}</div>' +
									'</div></tpl></tpl>',
			anchor:				'-420'
		},
		// INPUT NUMBER: Factor Credicadenas
		{
			xtype: 				'compositefield',
			fieldLabel: 		'Factor Credicadenas',			
			msgTarget: 			'side',
			id: 					'compositefield01',
			combineErrors: 	false,
			items: [
				{ 
					xtype: 				'numberfield',
					name: 				'factorCredicadenas',
					id: 					'factorCredicadenas',
					readOnly: 			false,
					allowBlank: 		false, 
					hidden: 				false,
					maxLength: 			5,	  
					width:				48,
					msgTarget: 			'side',
					anchor:				'-20',
					margins: 			'0 20 0 0', //necesario para mostrar el icono de error
					value:				6,
					format: 				'0.00',
               decimalPrecision: 2,
               allowDecimals:		true,
               minValue: 			-99.99, 
               maxValue: 			99.99,
               style:				{
               	textAlign:	'right'
               },
               beforeBlur: function() {
					  var v = this.parseValue(this.getRawValue());
								  
					  if (!Ext.isEmpty(v)) {
						  this.setValue(
							  Ext.util.Format.number(v, this.format)
						  );
					  }
					}
				}
			]
		},
		// INPUT NUMBER: �ltimos Meses Consecutivos
		{
			xtype: 				'compositefield',
			fieldLabel: 		'&Uacute;ltimos Meses Consecutivos',			
			msgTarget: 			'side',
			id: 					'compositefield02',
			combineErrors: 	false,
			items: [
				{ 
					xtype: 				'numberfield',
					name: 				'ultimosMesesConsecutivos',
					id: 					'ultimosMesesConsecutivos',
					allowBlank: 		true,
					hidden: 				false,
					maxLength: 			2,	
					msgTarget: 			'side',
					width:				28,
					margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
					value:				6,
					format: 				'0', 
               decimalPrecision: 0,
               allowDecimals:		false,
               allowNegative:		false,
               minValue: 			0, 
               maxValue: 			99,
               style:				{
               	textAlign:	'right'
               },
               beforeBlur: function() {
					  var v = this.parseValue(this.getRawValue());
								  
					  if (!Ext.isEmpty(v)) {
						  this.setValue(
							  Ext.util.Format.number(v, this.format)
						  );
					  }
					}
				},
				{
					xtype: 		'displayfield',
					id: 			'labelMesesConPublicacion',
					value: 		'Meses con Publicaci&oacute;n:',
					width:		124,
					hidden:		false
				},
				{ 
					xtype: 				'numberfield',
					name: 				'mesesConPublicacion',
					id: 					'mesesConPublicacion',
					allowBlank: 		true,
					hidden: 				false,
					maxLength: 			2,	
					msgTarget: 			'side',
					width:				28,
					margins: 			'0 20 0 0',  // necesario para mostrar el icono de error
					//value:				8,
					format: 				'0', 
               decimalPrecision: 0,
               allowDecimals:		false,
               allowNegative:		false,
               minValue: 			0, 
               maxValue: 			99,
               style:				{
               	textAlign:	'right'
               },
               beforeBlur: function() {
					  var v = this.parseValue(this.getRawValue());
								  
					  if (!Ext.isEmpty(v)) {
						  this.setValue(
							  Ext.util.Format.number(v, this.format)
						  );
					  }
					}
				}
			]
		},
		// INPUT NUMBER: Meses a Promediar
		{
			xtype: 				'compositefield',
			fieldLabel: 		'Meses a Promediar',			
			msgTarget: 			'side',
			id: 					'compositefield03',
			combineErrors: 	false,
			items: [
				{ 
					xtype: 				'numberfield',
					name: 				'mesesAPromediar',
					id: 					'mesesAPromediar',
					allowBlank: 		true,
					hidden: 				false,
					maxLength: 			2,
					msgTarget: 			'side',
					width:				28,
					margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
					value:				6,
					format: 				'0',
               decimalPrecision: 0,
               allowDecimals:		false,
               allowNegative:		false,
               minValue: 			0, 
               maxValue: 			99,
               style:				{
               	textAlign:	'right'
               },
               beforeBlur: function() {
					  var v = this.parseValue(this.getRawValue());
								  
					  if (!Ext.isEmpty(v)) {
						  this.setValue(
							  Ext.util.Format.number(v, this.format)
						  );
					  }
					}
				},
				{
					xtype: 				'displayfield',
					value: 				'Meses a Evaluar:',
					width:				124,
					id:					'labelMesesAEvaluar',
					hidden:				false
				},
				{ 
					xtype: 				'numberfield',
					name: 				'mesesAEvaluar',
					id: 					'mesesAEvaluar',
					allowBlank: 		true,
					hidden: 				false,
					maxLength: 			2,	
					msgTarget: 			'side',
					width:				28,
					margins: 			'0 20 0 0',  //necesario para mostrar el icono de error
					//value:				12,
					format: 				'0',
               decimalPrecision: 0,
               allowDecimals:		false,
               allowNegative:		false,
               minValue: 			0, 
               maxValue: 			99,
               style:				{
               	textAlign:	'right'
               },
               beforeBlur: function() {
					  var v = this.parseValue(this.getRawValue());
								  
					  if (!Ext.isEmpty(v)) {
						  this.setValue(
							  Ext.util.Format.number(v, this.format)
						  );
					  }
					}
				}
			]
		},
		// DATE RANGE: Fecha de Valuaci�n
		{
			xtype: 						'compositefield',
			fieldLabel: 				'Fecha de Valuaci�n',
			msgTarget: 					'side',
			id: 							'compositefield04',
			combineErrors: 			false,
			items: [
				{
					xtype: 				'datefield',
					name: 				'fechaDeValuacionInicial',
					id: 					'fechaDeValuacionInicial',
					allowBlank: 		true,
					startDay: 			0,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoFinFecha: 	'fechaDeValuacionFinal',
					margins: 			'0 20 0 0',
					allowBlank:			false,
					readOnly:			true,
					style: 				'background: gainsboro;'
				},{
					xtype: 				'displayfield',
					value: 				'a',
					width: 				20
				},{
					xtype: 				'datefield',
					name: 				'fechaDeValuacionFinal',
					id: 					'fechaDeValuacionFinal',
					allowBlank: 		true,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoInicioFecha: 'fechaDeValuacionInicial',
					margins: 			'0 20 0 0',
					allowBlank:			false,
					readOnly:			true,
					style: 				'background: gainsboro;'
				},{
					xtype: 				'displayfield',
					value: 				'(dd/mm/aaaa)',
					width: 				64
				}
			]
		}
	];
	
	
	var formaGeneraCedula = {
		xtype:				'form',
		id: 					'formaGeneraCedula',
		hidden:				false,
		width: 				800,
		title: 				'Par�metros',
		frame: 				true,
		collapsible: 		true,
		titleCollapse: 	true,
		style: 				'margin: 0 auto',
		bodyStyle:			'padding:10px',
		trackResetOnLoad: true,
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		labelWidth: 		164,
		labelAlign: 		'left',
		defaultType: 		'textfield',
		items: 				elementosFormaGeneraCedula,
		monitorValid: 		false,
		buttons: [
			// BOTON GENERAR CEDULA 8 POR 12
			{
				id:				'botonGenerarCedula8x12',
				text: 			'Generar C�dula 8 por 12',
				hidden: 			true,
				iconCls: 		'icoGenerarCedula',
				handler: 		procesaGenerarCedula8x12
			},
			// BOTON GENERAR CEDULA
			{
				id:				'botonGenerarCedula',
				text: 			'Generar C�dula',
				hidden: 			false,
				iconCls: 		'icoGenerarCedula',
				handler: 		procesaGenerarCedula
			},
			// BOTON LIMPIAR
			{
				text: 			'Limpiar',
				hidden: 			false,
				iconCls: 		'icoLimpiar',
				handler: 		procesaLimpiarForma
			}
		]
	};	
	
	//-------------------------------- PRINCIPAL -----------------------------------
	
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		disabled: false,
		items: 	[
			formaGeneraCedula,
			contenedorDetalleCedula
		],
		ocultaMascaras: function(opts){
					
			return;
			
			// Suprimir mascara del Panel de Tasa Base ( Puntos Limite Adicionales )
			var element = Ext.getCmp("formaPuntosAdicionales").getGridEl();
			if( element.isMasked() ){
				element.unmask();
			}
 
			// Derefenrenciar ultimo elemento...
			element = null;
			
		}
	});
	
	//------------------------ ACCIONES DE INICIALIZACION --------------------------
	
	//Peticion para realizar la inicializacion de la pantalla.
	var respuesta 						= new Object();
	generaCedula("INICIALIZACION", respuesta);
	
});