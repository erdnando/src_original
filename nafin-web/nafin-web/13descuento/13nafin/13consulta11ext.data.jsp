<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.exception.*, 
		javax.naming.Context,
		java.math.BigDecimal,
		com.netro.descuento.ConsultaCedulaEvaluacionPYME,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoPYME,
		com.netro.model.catalogos.CatalogoMoneda"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("GeneraCedula.inicializacion")	){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	String		meses_prom			= (request.getParameter("mesesAPromediar") 			== null) ? "6": request.getParameter("mesesAPromediar");
	String		meses_eval			= (request.getParameter("mesesAEvaluar") 				== null) ?"12": request.getParameter("mesesAEvaluar");
	String		periodo_inicio 	= (request.getParameter("fechaDeValuacionInicial") == null) ?  "": request.getParameter("fechaDeValuacionInicial");
	String		periodo_fin 		= (request.getParameter("fechaDeValuacionFinal") 	== null) ?  "": request.getParameter("fechaDeValuacionFinal");
	String		hidAction 			= (request.getParameter("hidAction") 					== null) ?  "": request.getParameter("hidAction");
	String		hidAction812		= (request.getParameter("hidAction812") 				== null) ?  "": request.getParameter("hidAction812");

	String		dia_					= "";
	String		mes_					= "";
	String		anyo_					= "";
		
	// Calcular Fecha de Valudacion Final y Fecha de Valuacion Inicial
	String 	rs_fechaHoy				= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	Calendar fechaVal 				= new GregorianCalendar();
		
	dia_	= rs_fechaHoy.substring(0, 2);
	mes_	= rs_fechaHoy.substring(3, 5);
	anyo_	= rs_fechaHoy.substring(6, 10);
		
	fechaVal.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));
	fechaVal.add(Calendar.MONTH, -1);
	fechaVal.set(Calendar.DAY_OF_MONTH, fechaVal.getActualMaximum(Calendar.DAY_OF_MONTH));
	periodo_fin = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaVal.getTime());
	if( !"".equals(meses_prom) && "".equals(hidAction812) ) {
			
		dia_				= periodo_fin.substring(0, 2);
		mes_				= periodo_fin.substring(3, 5);
		anyo_				= periodo_fin.substring(6, 10);
		fechaVal.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));	
		fechaVal.add(Calendar.MONTH, -(Integer.parseInt(meses_prom)-1));
		fechaVal.set(Calendar.DAY_OF_MONTH, fechaVal.getActualMinimum(Calendar.DAY_OF_MONTH));
		periodo_inicio = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaVal.getTime());
			
	} else {
			
		if(!"".equals(hidAction812)) {
			dia_				= periodo_fin.substring(0, 2);
			mes_				= periodo_fin.substring(3, 5);
			anyo_				= periodo_fin.substring(6, 10);
			fechaVal.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));	
			fechaVal.add(Calendar.MONTH, -(Integer.parseInt(meses_eval)-1));
			fechaVal.set(Calendar.DAY_OF_MONTH, fechaVal.getActualMinimum(Calendar.DAY_OF_MONTH));
			periodo_inicio = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaVal.getTime());
		}
			
	}
		
	resultado.put("fechaDeValuacionInicial",	periodo_inicio	);
	resultado.put("fechaDeValuacionFinal",		periodo_fin		);
	
	// 1. Definir el estado siguiente
	estadoSiguiente = "ACTUALIZAR_FECHA_VALUACION";
	
	// 2. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if (        informacion.equals("GeneraCedula.actualizarFechaValuacion")	){
 
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// 1. Definir el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
	
	// 2. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CatalogoEPO")			){
		
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setValoresCondicionNotIn("231",String.class);
	catalogo.setOrden("2");
	infoRegresar = catalogo.getJSONElementos();	
	
} else if(  informacion.equals("CatalogoPYME")			){
	
	String claveEPO = (request.getParameter("claveEPO") == null)?"":request.getParameter("claveEPO");
	
	if( "".equals(claveEPO) ){
		
		infoRegresar = "{\"success\": true, \"total\": \"0\", \"registros\": [] }";
		
	} else {
		
		CatalogoPYME catalogo = new CatalogoPYME();
		catalogo.setCampoClave("ic_pyme");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveEpo(claveEPO);
		catalogo.setConDocumentosPublicados(true);
		catalogo.setOrden("cg_razon_social");
		infoRegresar = catalogo.getJSONElementos();

	}
	
} else if(  informacion.equals("CatalogoEPOSRelacionadas")		 ){
	
	String clavePYME = (request.getParameter("clavePYME") == null)?"":request.getParameter("clavePYME");
	
	if( "".equals(clavePYME) ){
		
		infoRegresar = "{\"success\": true, \"total\": \"0\", \"registros\": [] }";
		
	} else {
		
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("ic_epo");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setPyme(clavePYME);
		catalogo.setValoresCondicionNotIn("231",String.class);
		catalogo.setOrden("2");
		
		List listaElementos 	= catalogo.getListaElementosCedulaEvaluacionPYME();	
		infoRegresar 			= catalogo.getJSONElementos(listaElementos);
 			
	}
	
} else if (	informacion.equals("CatalogoMoneda")			){
	
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	cat.setOrden("1");
	
	infoRegresar = cat.getJSONElementos();
	
} else if (	informacion.equals("GeneraCedula.generarCedula")			){

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// 1. Generar Cedula
	HashMap 		parametros 			= new HashMap();
	parametros.put("claveEPO", 						request.getParameter("claveEPO")							);
	parametros.put("clavePYME", 						request.getParameter("clavePYME")						);
	parametros.put("claveEPOSRelacionadas", 		request.getParameter("claveEPOSRelacionadas")		);
	parametros.put("claveMoneda", 					request.getParameter("claveMoneda")						);
	parametros.put("factorCredicadenas", 			request.getParameter("factorCredicadenas")			);
	parametros.put("ultimosMesesConsecutivos",	request.getParameter("ultimosMesesConsecutivos")	);
	parametros.put("mesesConPublicacion", 			request.getParameter("mesesConPublicacion")			);
	parametros.put("mesesAPromediar", 				request.getParameter("mesesAPromediar")				);
	parametros.put("mesesAEvaluar", 					request.getParameter("mesesAEvaluar")					);
	parametros.put("fechaDeValuacionInicial", 	request.getParameter("fechaDeValuacionInicial")		);
	parametros.put("fechaDeValuacionFinal", 		request.getParameter("fechaDeValuacionFinal")		);
	parametros.put("hidAction", 						request.getParameter("hidAction")						);
	parametros.put("hidAction812", 					request.getParameter("hidAction812")					);
	parametros.put("nombreUsuario", 					session.getAttribute("strNombreUsuario")				);
	parametros.put("esCedula8x12",					new Boolean(request.getParameter("esCedula8x12"))	);
	
	// 2. Preparar resultados
	ConsultaCedulaEvaluacionPYME consultaCedulaEvaluacionPYME = new ConsultaCedulaEvaluacionPYME();
	HashMap cedula = consultaCedulaEvaluacionPYME.generaCedulaEvaluacion(parametros);
 
	// 3. Enviar parametros de la consulta	
	resultado.put("params", 				JSONObject.fromObject(parametros)	);
	
	// 4. Copiar resultados a la respuesta
	resultado.put("fechaEmision",							(String) 	cedula.get("fechaEmision")							);
	resultado.put("nombreProveedor",						(String) 	cedula.get("nombreProveedor")						);
	resultado.put("rfc",										(String) 	cedula.get("rfc")										);
	resultado.put("telefonos",								(String) 	cedula.get("telefonos")								);
	resultado.put("nombreEpo",								(String) 	cedula.get("nombreEpo")								);
	resultado.put("fechaIngresoCadena",					(String) 	cedula.get("fechaIngresoCadena")					);
	resultado.put("fechaInicioOperaciones",			(String) 	cedula.get("fechaInicioOperaciones")			);
	resultado.put("numeroMesesOperando",				(String) 	cedula.get("numeroMesesOperando")				);
	resultado.put("montoCreditoOtorgar",				(String) 	cedula.get("montoCreditoOtorgar")				);
	resultado.put("susceptible",							(String) 	cedula.get("susceptible")							);
	resultado.put("solicito",								(String) 	cedula.get("solicito")								);
	resultado.put("showBotonGenerarCedula8x12pie",	(Boolean)	cedula.get("showBotonGenerarCedula8x12pie")	);
	
	// Actualizar fechas
	resultado.put("fechaDeValuacionInicial",	(String) cedula.get("fechaDeValuacionInicial")	);
	resultado.put("fechaDeValuacionFinal",		(String) cedula.get("fechaDeValuacionFinal")		);
		
	// 5. Enviar el detalle de las Operaciones
	
	JSONObject	detalleOperaciones		= 	new JSONObject();	
	List			listaOperaciones 			= (List) cedula.get("detalleOperaciones");
	// Convertir respuesta a formato JSON
	JSONArray 	registros01 = new JSONArray();
	for(int i=0;i<listaOperaciones.size();i++){
		
		HashMap registro = (HashMap) listaOperaciones.get(i);
		registros01.add(
			JSONObject.fromObject(registro)
		);
		
	}
	// Enviar resultado de la operacion
	detalleOperaciones.put("success", 	new Boolean(true)						);
	detalleOperaciones.put("total",    	new Integer(registros01.size()) 	);
	detalleOperaciones.put("registros",	registros01			 					);
	// Agregar detalle de las operaciones a la respuesta
	resultado.put("detalleOperaciones",detalleOperaciones);

	// 6. Enviar el detalle de las Operaciones
	
	JSONObject	totalDetalleOperaciones				= 	new JSONObject();	
	List			listaTotalOperaciones 				= (List) cedula.get("totalDetalleOperaciones");
	// Convertir respuesta a formato JSON
	JSONArray 	registros02 = new JSONArray();
	for(int i=0;i<listaTotalOperaciones.size();i++){
		
		HashMap registro = (HashMap) listaTotalOperaciones.get(i);
		registros02.add(
			JSONObject.fromObject(registro)
		);
		
	}
	// Enviar resultado de la operacion
	totalDetalleOperaciones.put("success", 	new Boolean(true)							);
	totalDetalleOperaciones.put("total",    	new Integer(registros02.size()) 		);
	totalDetalleOperaciones.put("registros",	registros02			 						);
	// Agregar total del detalle de las operaciones a la respuesta
	resultado.put("totalDetalleOperaciones",totalDetalleOperaciones);
	
	// 7. Definir el estado siguiente
	estadoSiguiente = "MOSTRAR_CEDULA";
	
	// 8. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("GenerarArchivo") ){
	
	JSONObject	resultado		= new JSONObject();
	boolean		success			= true;
	String 		msg 				= "";
	String      urlArchivo		= "";
	
	String 		nombreArchivo	= "";
	
	// Leer parametros
	String 		tipo 				= (request.getParameter("tipo") == null)?"":request.getParameter("tipo");
	
	// Preparar clases adicionales
	ConsultaCedulaEvaluacionPYME consulta = new ConsultaCedulaEvaluacionPYME();
	// Generar Cedula
	HashMap 		parametros 			= new HashMap();
	parametros.put("claveEPO", 						request.getParameter("claveEPO")							);
	parametros.put("clavePYME", 						request.getParameter("clavePYME")						);
	parametros.put("claveEPOSRelacionadas", 		request.getParameter("claveEPOSRelacionadas")		);
	parametros.put("claveMoneda", 					request.getParameter("claveMoneda")						);
	parametros.put("factorCredicadenas", 			request.getParameter("factorCredicadenas")			);
	parametros.put("ultimosMesesConsecutivos",	request.getParameter("ultimosMesesConsecutivos")	);
	parametros.put("mesesConPublicacion", 			request.getParameter("mesesConPublicacion")			);
	parametros.put("mesesAPromediar", 				request.getParameter("mesesAPromediar")				);
	parametros.put("mesesAEvaluar", 					request.getParameter("mesesAEvaluar")					);
	parametros.put("fechaDeValuacionInicial", 	request.getParameter("fechaDeValuacionInicial")		);
	parametros.put("fechaDeValuacionFinal", 		request.getParameter("fechaDeValuacionFinal")		);
	parametros.put("hidAction", 						request.getParameter("hidAction")						);
	parametros.put("hidAction812", 					request.getParameter("hidAction812")					);
	parametros.put("nombreUsuario", 					session.getAttribute("strNombreUsuario")				);
	parametros.put("esCedula8x12",					new Boolean(request.getParameter("esCedula8x12"))	);
	HashMap cedula = consulta.generaCedulaEvaluacion(parametros);

	// HttpSession session
	String			directorioTemporal		= strDirectorioTemp; 
	String 			directorioPublicacion 	= strDirectorioPublicacion;
	
	// Crear archivo
	if(        "PDF".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoPDF( directorioTemporal, directorioPublicacion, cedula, session );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	} else if( "CSV".equals(tipo) ){
		nombreArchivo = consulta.generaArchivoCSV( directorioTemporal, cedula );
		urlArchivo 	  = strDirecVirtualTemp + nombreArchivo;
	}
 
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg								);
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo 						); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)			);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("fin")                        	){

} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>