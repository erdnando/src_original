<%@ page import=
		"java.util.*, 
		netropology.utilerias.*,
		com.netro.descuento.*, 
		com.netro.pdf.*, 
		java.sql.*,
		java.math.*,		
		net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>

<%
System.out.println("13consulta10exti (E)");
String df_operacion_inicio = (request.getParameter("df_operacion_inicio")==null)?"":request.getParameter("df_operacion_inicio");
String df_operacion_fin=(request.getParameter("df_operacion_fin")==null)?"":request.getParameter("df_operacion_fin");
String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")==null)?"":request.getParameter("ic_banco_fondeo");  //FODEA 007 - 2009
if(ic_banco_fondeo.equals("")){ic_banco_fondeo = iTipoPerfil.equals("8")?"2":"1";} //FODEA 007 - 2009
String start1 = (request.getParameter("start")!=null)?request.getParameter("start"):"0";
String limit1 = (request.getParameter("limit")!=null)?request.getParameter("limit"):"15";

String 	NOM_EPO = "",	NOM_PYME = "", 	NOM_IF = "", 	NUM_DOCTO = "", FECHA_EMISION ="",FECHA_VENC = "",
				MONEDA = "", TIPODESC ="", MONTO_DOCTO = "", 	PORC_DESCTO = "", MONTO_DESCONTAR = "", TIPO_DSCTOAUTO = "",
				DIAS_MINIMOS = "", LIMITE_IFEPO = "", CAUSA_RECHAZO = "", FECHA_OPERACION = "";

System.out.println("start1---->"+start1);
System.out.println("limit1---->"+limit1);
System.out.println("df_operacion_inicio---->"+df_operacion_inicio);
System.out.println("df_operacion_fin---->"+df_operacion_fin);
System.out.println("ic_banco_fondeo---->"+ic_banco_fondeo);


JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

try {
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new com.netro.descuento.ConsRechDescNafinDE());

	int start = 0;
	int limit = 0;
	int nRow = 0;
	Registros rs = new Registros();
	Registros rst = new Registros();
	
	try {
		start = Integer.parseInt(start1);
		limit = Integer.parseInt(limit1);
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}	
	rs = queryHelper.getPageResultSet(request,start,limit);
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	

	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
	pdfDoc.addText("Rechazo en Descuento Automatico","formas",ComunesPDF.CENTER);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);	
	
	pdfDoc.setTable(16, 100);
	pdfDoc.setCell("Nombre de EPO","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Num. Documento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Emisión","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo de Descuento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("% Descuento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo de Dscto. Automático","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Días Mínimos Cadena","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Límite IF-EPO","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Causa de Rechazo","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha y Hora Descto","celda01",ComunesPDF.CENTER);
		
	while (rs.next()) {
	 	 NOM_EPO = (rs.getString(1)==null)?"":rs.getString(1);
			NOM_PYME = (rs.getString(2) == null)?"":rs.getString(2);
			NOM_IF = (rs.getString(3) == null)?"":rs.getString(3);
			NUM_DOCTO = (rs.getString(4)==null)?"":rs.getString(4);
			FECHA_EMISION = (rs.getString(5)==null)?"":rs.getString(5);
			FECHA_VENC = (rs.getString(6)==null)?"":rs.getString(6);
			MONEDA = (rs.getString(7)==null)?"":rs.getString(7);
			TIPODESC = (rs.getString(8)==null)?"":rs.getString(8);
			MONTO_DOCTO = (rs.getString(9)==null)?"":rs.getString(9);
			PORC_DESCTO = (rs.getString(10)==null)?"":rs.getString(10);
			MONTO_DESCONTAR = (rs.getString(11)==null)?"":rs.getString(11);
			TIPO_DSCTOAUTO = (rs.getString(12)==null)?"":rs.getString(12);
			DIAS_MINIMOS = (rs.getString(13)==null)?"":rs.getString(13);
			LIMITE_IFEPO = (rs.getString(14)==null)?"":rs.getString(14);
			CAUSA_RECHAZO = (rs.getString(15)==null)?"":rs.getString(15);
			FECHA_OPERACION = (rs.getString(16)==null)?"":rs.getString(16);
						
			pdfDoc.setCell(NOM_EPO,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(NOM_PYME,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(NOM_IF,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(NUM_DOCTO,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(FECHA_EMISION,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(FECHA_VENC,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(MONEDA,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(TIPODESC,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(MONTO_DOCTO,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(PORC_DESCTO+"%","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(MONTO_DESCONTAR,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(TIPO_DSCTOAUTO,"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(DIAS_MINIMOS,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(LIMITE_IFEPO,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(CAUSA_RECHAZO,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(FECHA_OPERACION,"formas",ComunesPDF.CENTER);		
	}
	pdfDoc.addTable();
	
	
	//para los totales 
	queryHelper = new CQueryHelperExtJS(new com.netro.descuento.ConsRechDescNafinDE());
	List vecTotales = queryHelper.getResultCount(request);
	List vecColumnas =null;
	
	if(vecTotales.size()>0){
		pdfDoc.setTable(3, 50);
		pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("NUMERO DOCUMENTOS","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("MONTO","celda01",ComunesPDF.CENTER);
		
		for(int i=0; i<vecTotales.size(); i++){	
		vecColumnas= 	(ArrayList)vecTotales.get(i);		
			pdfDoc.setCell(vecColumnas.get(1).toString(),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(vecColumnas.get(2).toString(),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(vecColumnas.get(3).toString(),2) ,"formas",ComunesPDF.RIGHT);
		}
		pdfDoc.addTable();
	}
	
		
	pdfDoc.endDocument();

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch(Exception e) {
	throw new AppException("Error al generar el archivo", e);
}
%>
<%=jsonObj%>

<% System.out.println("jsonObj"+jsonObj); %>
<% System.out.println("13consulta10exti (S)"); %>