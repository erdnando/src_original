<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,		
		java.util.Vector,
		netropology.utilerias.*,
		com.netro.descuento.*, 
		com.netro.exception.*,
		javax.naming.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoSimple"
	errorPage="/00utils/error_extjs.jsp"%>		
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>

<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_banco_fondeo = request.getParameter("ic_banco_fondeo") == null?"2":(String)request.getParameter("ic_banco_fondeo");
String ic_epo = request.getParameter("ic_epo")== null?"0":(String)request.getParameter("ic_epo");
if(ic_epo.equals("")){ ic_epo = "0"; }

String infoRegresar = "";
/*System.out.println("ic_banco_fondeo   "+ic_banco_fondeo);
System.out.println("ic_epo   "+ic_epo);
System.out.println("informacion   "+informacion);
*/
%>
<%
if (informacion.equals("CatalogoBanco")) {
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("IC_BANCO_FONDEO");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("COMCAT_BANCO_FONDEO");
	catalogo.setOrden("IC_BANCO_FONDEO");
	infoRegresar = catalogo.getJSONElementos();
	
}else if (informacion.equals("CatalogoEPO")) {
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveBancoFondeo(ic_banco_fondeo);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();	
}

if (informacion.equals("Consulta")) {

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	ArrayList alListadeEPO = null;   
		
	infoRegresar = BeanParamDscto.getParamPorEPOEXTJS(ic_epo, ic_banco_fondeo);
	
}

%>

<%=infoRegresar%>
