<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		org.apache.commons.logging.Log,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,		
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType);
	request.setAttribute("myContentType", myContentType);
	JSONObject resultado = new JSONObject();
	boolean success = true;
	String fileName = "";
	String errorTam = "";
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
	} catch(Exception e) {
		success = false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 2097152) {
			errorTam = "El archivo es muy grande, excede el Límite que es de 2 MB.";
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 2 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
	}
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest = myUpload.getRequest();
	// Guardar Archivo en Disco
	int numFiles = 0;
	try {
		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," "));
		fileName = myFile.getFileName().replaceAll("\\s+"," ");
	}catch(Exception e){
		success = false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
%>
{
	"success": '<%=success%>',
	"archivo": '<%=fileName%>',
	"error_tam":'<%=errorTam%>'	
}



