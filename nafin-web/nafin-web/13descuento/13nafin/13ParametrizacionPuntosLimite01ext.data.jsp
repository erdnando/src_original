<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.afiliacion.*,
		com.netro.exception.*, 
		javax.naming.Context,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.descuento.*,
		java.math.BigDecimal"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("ParametrizacionTasas.inicializacion") 			){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// 1. Definir el estado siguiente
	estadoSiguiente = "CARGAR_PUNTOS_ADICIONALES";
	
	// 2. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",					msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("ParametrizacionTasas.cargarPuntosAdicionales") ){	
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// 1. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("ParametrizacionTasas.inicializacion(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
		
	// 2. Obtener parametrizacion de los puntos limite
	HashMap parametrizacion 		= autorizacionTasas.obtenerParametrizacionPuntosLimite();
	String  valorPuntosLimite		= (String) parametrizacion.get("valorPuntosLimite");
	String  usuarioModificacion	= (String) parametrizacion.get("usuarioModificacion");
	String  fechaModificacion		= (String) parametrizacion.get("fechaModificacion");
	
	valorPuntosLimite 				= "".equals(valorPuntosLimite)  ?" ":valorPuntosLimite;
	usuarioModificacion				= "".equals(usuarioModificacion)?" ":usuarioModificacion;
	fechaModificacion					= "".equals(fechaModificacion)  ?" ":fechaModificacion;
	
	JSONObject parametrizacionPuntosLimite = new JSONObject();
	parametrizacionPuntosLimite.put("Puntos Adicionales Límite" , valorPuntosLimite   );
	parametrizacionPuntosLimite.put("Última Modificación"			, fechaModificacion   );
	parametrizacionPuntosLimite.put("Usuario"							, usuarioModificacion );
	resultado.put("parametrizacionPuntosLimite",parametrizacionPuntosLimite);
 
	// 3. Definir el estado siguiente
	estadoSiguiente = "MOSTRAR_PUNTOS_ADICIONALES";
	
	// 4. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",					msg						);
	infoRegresar = resultado.toString();
	
} else if( informacion.equals("ParametrizacionTasas.guardarPuntosAdicionales") ){

	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= "CANCELAR_GUARDAR_PUNTOS_ADICIONALES";
	String		msg					= null;
	boolean 		hayError				= false;
	
	// 1. Leer parametros
	String cgUsuario      = (String)request.getSession().getAttribute("strNombreUsuario");
	String fnPuntosLimite = (request.getParameter("fnPuntosLimite") == null)?"":request.getParameter("fnPuntosLimite");

	// 2. Validar los parametros recibidos
	if (fnPuntosLimite != null && !fnPuntosLimite.equals("")) {
		
		if (fnPuntosLimite.indexOf(",") != -1) {
			fnPuntosLimite = fnPuntosLimite.replaceAll(",", "");
		}
		
		if (fnPuntosLimite.indexOf(".") != -1) {			
			String valorDecimal = fnPuntosLimite.substring(fnPuntosLimite.indexOf(".") + 1, fnPuntosLimite.length());
			if (valorDecimal.length() > 5) {
				msg    = "El valor de los Puntos Adicionales Límite no es válido. Introduzca máximo 2 entero(s) y 5 decimal(es).";
				hayError = true;
			}
		}
		
		if (!Comunes.esDecimalPositivo(fnPuntosLimite)) {
			msg    = "El valor de los Puntos Adicionales Límite no es válido. Introduzca máximo 2 entero(s) y 5 decimal(es).";
			hayError = true;
		}else{
			BigDecimal bdFnPuntosLimite = new BigDecimal(fnPuntosLimite).setScale(2, BigDecimal.ROUND_HALF_UP);
			if (bdFnPuntosLimite.compareTo(new BigDecimal("99.99999")) == 1) {
				msg    = "El valor de los Puntos Adicionales Límite no es válido. Introduzca máximo 2 entero(s) y 5 decimal(es).";
				hayError = true;
			}
		}
		
	} else {

		msg    = "El valor de los Puntos Adicionales Límite es requerido.";
		hayError = true;
		
	}
			
	// 2. Guardar Puntos Adicionales al Límite
	if(!hayError){

		// 2.1 Obtener instancia del EJB de Autorizacion
		AutorizacionTasas autorizacionTasas = null;
		try {
					
			autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
	
		} catch(Exception e) {
		 
			log.error("ParametrizacionTasas.inicializacion(Exception): Obtener instancia del EJB de  Autorización Tasas");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
			
		}
				
		// 2.2 Guardar Parametrizacion
		HashMap parametrosPuntosLimite = new HashMap();
		parametrosPuntosLimite.put("fnPuntosLimite", fnPuntosLimite);
		parametrosPuntosLimite.put("cgUsuario", 		cgUsuario);
		autorizacionTasas.guardarParametrizacionPuntosLimite(parametrosPuntosLimite);
			
		HashMap parametrizacionPuntosLimite = autorizacionTasas.obtenerParametrizacionPuntosLimite();
		if (!parametrizacionPuntosLimite.isEmpty()) {
			msg 					= "La captura del valor de los puntos adicionales límite se realizó con éxito.";
			estadoSiguiente 	= "CARGAR_PUNTOS_ADICIONALES";
		} else {
			msg 					= "No se pudo guardar la parametrización.";
		}
	 
	} 
		
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CatalogoEPO")                			 ) {
	
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();	
		
} else if(  informacion.equals("ConsultaParametrizacionEPO") 			) {

	JSONObject	resultado			= new JSONObject();
	boolean		success				= true;
	
	// 1. Leer parametros de la consulta
	String claveEpo 					= (request.getParameter("claveEPO")				== null)?"":request.getParameter("claveEPO"); 
	String csParametroLimite 		= (request.getParameter("utilizaParametroLimite")	== null)?"":request.getParameter("utilizaParametroLimite");

	// 2. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
					
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
	
	} catch(Exception e) {
		 
		log.error("ParametrizacionTasas.consultaParametrizacionEpo(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
			
	}
 
	// 3. Realizar consulta
	HashMap parametrizacionPuntosLimitePorEpo = autorizacionTasas.obtenerParametrizacionPuntosLimitePorEpo(claveEpo, csParametroLimite);

	// 4. Construir respuesta
	JSONArray registros	 		= new JSONArray();
	int 		 numeroRegistros 	= Integer.valueOf( (String) parametrizacionPuntosLimitePorEpo.get("numeroRegistros") ).intValue();
	for(int i=0;i<numeroRegistros;i++){
		
		HashMap puntosLimitePorEpo 		= (HashMap) parametrizacionPuntosLimitePorEpo.get("puntosLimitePorEpo"+i);
		String  idEpo 							= (String) puntosLimitePorEpo.get("claveEpo");
		String  nombreEpo 					= (String) puntosLimitePorEpo.get("nombreEpo");
		String  utilizaParametroLimite 	= (String) puntosLimitePorEpo.get("csPuntosLimite");
		
		JSONObject	registro = new JSONObject();
		registro.put("ID_EPO",							idEpo);
		registro.put("NOMBRE_EPO",					nombreEpo);
		registro.put("UTILIZA_PARAMETRO_LIMITE",	utilizaParametroLimite);
		registros.add(registro);
		
	}

	// 4. Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)					);
	resultado.put("total",    	String.valueOf(registros.size()) );
	resultado.put("registros",	registros			 					);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("ParametrizacionTasas.cambiarDefinicionPuntosAdicionales") ) {
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= "MOSTRAR_DEFINICION_PUNTOS_ADICIONALES";
	String		msg					= null;
	boolean 		hayError				= false;
	
	// 1. Leer parametros
	String claveEpoModificacion 				= (request.getParameter("idEpo")							== null)?"":request.getParameter("idEpo");
	String csParametroLimiteModificacion 	= (request.getParameter("utilizaParametroLimite")	== null)?"":request.getParameter("utilizaParametroLimite");
 
	csParametroLimiteModificacion = csParametroLimiteModificacion.equals("")?"SI":csParametroLimiteModificacion;
	// 2. Se realiza el cambio de parametrizacion, si viene el valor SI se cambia a N y si viene NO se cambia a S
	csParametroLimiteModificacion = csParametroLimiteModificacion.equals("SI")?"N":"S";
			
	// 3. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
					
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
	
	} catch(Exception e) {
		 
		log.error("ParametrizacionTasas.consultaParametrizacionEpo(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
			
	}
		
	// 4. Se realiza el cambio de la parametrización	
	autorizacionTasas.cambiarParametrizacionPuntosLimite(claveEpoModificacion, csParametroLimiteModificacion);

	// 5. Consultar parametrización.
	// Nota: En la version original se motraba una precision de 4 decimales, por lo que se adapto para que se usen 5.
	if( "S".equals( csParametroLimiteModificacion ) ){
		HashMap parametrizacionPuntosLimite = autorizacionTasas.obtenerParametrizacionPuntosLimite();
		String  valorPuntosLimite           = parametrizacionPuntosLimite.get("valorPuntosLimite")==null?"":Comunes.formatoDecimal(parametrizacionPuntosLimite.get("valorPuntosLimite"), 5);
		resultado.put("valorPuntosLimite", valorPuntosLimite );
	}
	resultado.put("utilizaParametroLimite", csParametroLimiteModificacion	);
			
	// 6. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("fin")                        			) {
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>