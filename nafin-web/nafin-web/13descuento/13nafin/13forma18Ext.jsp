<% String version = (String)session.getAttribute("version"); %>
<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
	String tabActiva = (request.getParameter("tabActiva") != null) ? request.getParameter("tabActiva") : "";
%>
<html>
	<head>
		<title>Nafinet</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<%@ include file="/extjs.jspf" %>
		<%@ include file="/01principal/menu.jspf"%>
    <style type="text/css">
        .x2{color: black;}
    </style>
		<link rel="stylesheet" type="text/css" href="<%=appWebContextRoot%>/00utils/extjs/ux/FileUploadField.css" />
		<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/FileUploadField.js"></script>
		<script type="text/javascript" src="13forma18Ext.js?<%=session.getId()%>"></script>
	</head>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<%}%>
		<div id="_menuApp"></div>
		
		<div id="Contcentral">
	<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
   <%}%>
		<div id="areaContenido"><div style="height:230px"></div></div>
		</div>
		
	<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/pie.jspf"%>
   <%}%>
		<form id='formAux' name="formAux" target='_new'></form>

		<form id='formParametros' name="formParametros">
			<input type="hidden" id="tabActiva" name="tabActiva" value="<%=tabActiva%>"/>	
		</form>

	</body>
</html>