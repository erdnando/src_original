Ext.onReady(function() 
{
//--------------------------------- HANDLERS -------------------------------
  var strPerfil   	=  Ext.getDom('strPerfil').value;	
  var strTipoUsuario =  Ext.getDom('strTipoUsuario').value;	
  
  var jsonValoresIniciales = null;
	function procesaValoresIniciales(opts,success,response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var VisorAcusesDeCarga = function(rec){
		var ic_acuse = rec.get('CC_ACUSE');
		if(ic_acuse != ''){
			var win = new NE.AcusesDeCarga.VisorAcusesDeCarga(ic_acuse);
		}
	}

	var procesarCatalogoBancoData = function(store, records, opts) {
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var cbBancoFondeo = Ext.getCmp('bancoFondeo');
			if(cbBancoFondeo.getValue()==''){
				cbBancoFondeo.setValue(records[0].data['clave']);
			}
			if(strTipoUsuario == 'NAFIN') {
				if(store.getTotalCount() > 1){
					catalogoEPO.load({params: {cbBancoFondeo: '1' }});
				} else{
					catalogoEPO.load({params: {cbBancoFondeo: '2' }});
				}
			}
		}
	}

  var procesarCatalogoEPOData = function(store, records, oprion){
   if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
    var cbIcEPO = Ext.getCmp('icEPO');
	 
	 if(cbIcEPO.getValue()=='' && jsonValoresIniciales.strTipoUsuario =='EPO'){
	  cbIcEPO.setValue(records[0].data['clave']);
	  cbIcEPO.hide();
	 }
   }
  }  
  
  var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
				gridGeneral.show();
			}
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		//var btnBajarPDF = Ext.getCmp('btnBajarPDF');

		var jsonData = store.reader.jsonData;		

		//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas 
		var cm = gridGeneral.getColumnModel();
		var el = gridGeneral.getGridEl();	
				
		if(jsonData.mostrarCampos=='S'){
			gridGeneral.getColumnModel().setHidden(cm.findColumnIndex('DF_ENTREGA'), false);	
			gridGeneral.getColumnModel().setHidden(cm.findColumnIndex('CG_TIPO_COMPRA'), false);	
			gridGeneral.getColumnModel().setHidden(cm.findColumnIndex('CG_CLAVE_PRESUPUESTARIA'), false);	
			gridGeneral.getColumnModel().setHidden(cm.findColumnIndex('CG_PERIODO'), false);	
		}		
  
			
		if(store.getTotalCount() > 0) {
			btnGenerarPDF.show();
			btnGenerarPDF.enable();
			//btnBajarPDF.hide();
			el.unmask();
		} else {
			btnGenerarPDF.disable();
			//btnBajarPDF.hide();
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
	}
  }

	/***** Descargar archivos PDF *****/
	var procesarSuccessFailureGenerarPDF = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var resp = Ext.util.JSON.decode(response.responseText);
			if(resp.estatusArchivo == 'FINAL'){
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
				Ext.getCmp('btnGenerarPDF').setText('Generar Todo');
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
			} else if(resp.estatusArchivo == 'ERROR'){
				Ext.Msg.alert('Error','No se gener� el archivo',function(btn){
					Ext.getCmp('btnGenerarPDF').setText('Generar Todo');
					Ext.getCmp('btnGenerarPDF').enable();
					Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf')
				});
			} else{
				Ext.getCmp('btnGenerarPDF').setIconClass('loading-indicator');
				Ext.getCmp('btnGenerarPDF').setText(resp.porcentaje);
				Ext.Ajax.request({
					url: '13consulta17ext.data.jsp',
					params: Ext.apply (fp.getForm().getValues(),{
						estatusArchivo: 'PROCESANDO',
						informacion: 'ArchivoPDF'
					}),
					callback: procesarSuccessFailureGenerarPDF
				});
			}
		} else{
			Ext.getCmp('btnGenerarPDF').setText('Generar Todo');
			Ext.getCmp('btnGenerarPDF').enable();
			Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf')
			NE.util.mostrarConnError(response,opts);
		}
	}

/*
	var procesarSuccessFailureGenerarPDF = function (opts, success, response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDF.setHandler(function (boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}
		else 
		{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
  }
*/
	function procesaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

  function procesarSuccesUsuario(opts, success, response){
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var infoR = Ext.util.JSON.decode(response.responseText);
		Ext.getCmp('disClave').setValue('');
		Ext.getCmp('disNombre').setValue('');
		Ext.getCmp('disApat').setValue('');
		Ext.getCmp('disAmat').setValue('');
		Ext.getCmp('disCorreo').setValue('');

		if (infoR != undefined){
			Ext.getCmp('disClave').setValue(infoR.claveUsuario);
			Ext.getCmp('disNombre').setValue(infoR.nombre);
			Ext.getCmp('disApat').setValue(infoR.apellidoPaterno);
			Ext.getCmp('disAmat').setValue(infoR.apellidoMaterno);
		   Ext.getCmp('disCorreo').setValue(infoR.email);
			
		}
		var win = Ext.getCmp('winUsuario');
		if (win){
			win.show();
		}else{
			var winUsuario = new Ext.Window ({
				id:	'winUsuario',
				height: 220,
				width: 400,
				modal: true,
				closeAction: 'hide',
				title:'Datos del Usuario',
				items:[fpUsuario],
				buttons:[
					{
						text:'Cerrar',
						handler: function() {	Ext.getCmp('winUsuario').hide();	}
					}
				]
			}).show();
		}
	}else{
		NE.util.mostrarConnError(response,opts);
	}
  }
  
  var procesarDetalle = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var ccAcuse = registro.get('CC_ACUSE');
		var _icEPO = registro.get('IC_EPO');
		Ext.Ajax.request({url: '13consulta17ext.data.jsp',params: Ext.apply({informacion: "ArchivoTXT",ccAcuse: ccAcuse, _icEPO: _icEPO}),callback: procesaArchivo});
	}
	
	var procesarUsuario =function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var idUsuario = registro.get('IC_USUARIO');
		Ext.Ajax.request({url: '13consulta17ext.data.jsp',params: Ext.apply({informacion: "InfoUsuario",idUsuario: idUsuario}),callback:procesarSuccesUsuario});
	}

//-------------------------------- STORES -----------------------------------

/*var informacionUsuario = new Ext.data.JsonStore({
	id: 'informacionUsuarioStore',
	root: 'registros',
	fields: [''],


});*/

  var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catologoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta17ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoMonedaDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var catalogoUsuario = new Ext.data.JsonStore
  ({
	   id: 'catologoUsuarioDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta17ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoUsuarioDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var catalogoBanco = new Ext.data.JsonStore
  ({
	   id: 'catologoBancoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta17ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catologoBancoDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 load: procesarCatalogoBancoData,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var catalogoEPO = new Ext.data.JsonStore
  ({
	   id: 'catologoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta17ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEPODist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners:
		{
		 load: procesarCatalogoEPOData,
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
  var consultaData = new Ext.data.JsonStore
  ({     
			root: 'registros',
			url: '13consulta17ext.data.jsp',
			baseParams: {
				informacion: 'Consulta'		
			},
			fields: [
			         {name: 'IG_NUMERO_DOCTO'},
						{name: 'FN_MONTO'},
						{name: 'MONEDA'},
						{name: 'IC_USUARIO'},
						{name: 'DF_FECHAHORA_CARGA'},
						{name: 'CC_ACUSE'},
						{name: 'IC_EPO'},
						{name: 'MUESTRA_VISOR'},
						{name: 'DF_ENTREGA'},
						{name: 'CG_TIPO_COMPRA'},
						{name: 'CG_CLAVE_PRESUPUESTARIA'},
						{name: 'CG_PERIODO'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
					load: procesarConsultaData,
					exception: {
								fn: function(proxy,type,action,optionsRequest,response,args){
										NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
										procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
								}
					}
			}
  });

//---------------------------------COMPONENTES-------------------------------
	var elementosUsuario = [
		{
			id:'disClave',
			fieldLabel:'Clave del Usuario',
			value:''
		},{
			id:'disNombre',
			fieldLabel: 'Nombre',
			value:''
		},{
			id:'disApat',
			fieldLabel: 'Apellido Paterno',
			value:''
		},{
			id:'disAmat',
			fieldLabel: 'Apellido Materno',
			value:''
		},{
			id:'disCorreo',
			fieldLabel: 'E-mail',
			value:''
		}
	];

	var fpUsuario = new Ext.form.FormPanel({
		id: 'fpUsuario',
		height: 'auto',
		
		width: 'auto',
		style: 'margin:10 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'displayfield',
		frame: false,
		items: elementosUsuario
	});

  var gridGeneral = new Ext.grid.GridPanel
  ({
		store: consultaData,
		hidden: true,
      id: 'gridGeneral',
		columns: [		
			{
				header: 'No. de Documento',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'center',
				hidden: false,
				dataIndex: 'IG_NUMERO_DOCTO'
			},
			{
				header: 'Monto',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'right',
				hidden: false,
				dataIndex: 'FN_MONTO',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')

			},
			{
				header: 'Moneda',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'left',
				hidden: false,
				dataIndex: 'MONEDA'
			},
			{
				xtype: 'actioncolumn',
				header: 'Usuario',
				sortable: true,
				resizable: true,
			   width: 250,	
				align: 'center',
				hidden: false,
				dataIndex: 'IC_USUARIO',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return value+'&nbsp;&nbsp;';
							 },
				
				items: [
					     {
						   getClass: function(value, metadata, registro, rowIndex, colIndex, store) {								
							 this.items[0].tooltip = 'Ver';								
							 return 'iconoLupa';											
						   },
						   handler: procesarUsuario
					     }
				       ]	
			},
			{
				header: 'Fecha de Publicaci�n',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'center',
				hidden: false,
				dataIndex: 'DF_FECHAHORA_CARGA'
			},
			{
				xtype:     'actioncolumn',
				header: 'Acuse',
				sortable: true,
				resizable: true,
				width:     180,	
				align: 'center',
				hidden: false,
				dataIndex: 'CC_ACUSE',
				renderer: function(value, metadata, record, rowindex, colindex, store){
					return (record.get('CC_ACUSE'));
				},
				items: [{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store){
						if(registro.get('CC_ACUSE') != '' && registro.get('MUESTRA_VISOR') == 'S'){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						} else{
							return value;
						}
					},
					handler: function(gridGeneral, rowIndex, colIndex){
						var rec = consultaData.getAt(rowIndex);
						VisorAcusesDeCarga(rec);
					}
				}]
			},
			{
			   xtype: 'actioncolumn',
				header: 'Archivo',
				tooltip: 'Ver Archivo',
				dataIndex: '',
            width: 100,
				align: 'center',
				items: [
					     {
						   getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {								
							 this.items[0].tooltip = 'Ver';								
							 return 'iconoLupa';											
						   },
						   handler: procesarDetalle
					     }
				       ]	
			},
			{
				header: 'Fecha de Recepci�n de Bienes y Servicios',
				sortable: true,
				resizable: true,
				width: 250,	
				align: 'center',
				hidden: true,
				dataIndex: 'DF_ENTREGA'
			},
			{
				header: 'Tipo de Compra (procedimiento)',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: true,
				dataIndex: 'CG_TIPO_COMPRA'
			},
			{
				header: 'Clasificador por Objeto del Gasto',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: true,
				dataIndex: 'CG_CLAVE_PRESUPUESTARIA'
			},
			{
				header: 'Plazo M�ximo',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: true,
				dataIndex: 'CG_PERIODO',
				renderer	: function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('CG_PERIODO') == '0')
						return '';
					else
						return value;
					
				}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true,
			bbar: 
	      {
			 xtype: 'paging',
			 pageSize: 15,
			 buttonAlign: 'left',
			 id: 'barraPaginacion',
			 displayInfo: true,
			 store: consultaData,
			 displayMsg: '{0} - {1} de {2}',
			 emptyMsg: 'No hay registros.',
			 items: ['->','-',{
				xtype:   'button',
				text:    'Generar Todo',
				tooltip: 'Imprime los registros en formato PDF.',
				iconCls: 'icoPdf',
				id:      'btnGenerarPDF',
				handler: function(boton, evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta17ext.data.jsp',
						params: Ext.apply (fp.getForm().getValues(),{
							estatusArchivo: 'INICIO',
							informacion: 'ArchivoPDF'
						}),
						callback: procesarSuccessFailureGenerarPDF
					});
				}
			}
/*
					 {
						xtype: 'button',
						text: 'Bajar PDF',
						id: 'btnBajarPDF',
						hidden: true
					 },
					 '-'
*/
		]}
	});

	 var elementosForma = 
	[
	{
	 //Seleccionar Banco de Fondeo
	 xtype: 'combo',
	 fieldLabel: ' Banco de Fondeo',
	 displayField: 'descripcion',
	 valueField: 'clave',
	 triggerAction: 'all',
	 typeAhead: true,
	 minChars: 1,
	 store: catalogoBanco,
	 tpl: NE.util.templateMensajeCargaCombo,
	 name:'cbBancoFondeo',
	 id: 'bancoFondeo',
	 mode: 'local',
	 forceSelection : true,
	 allowBlank: true,
	 hidden:true,
	 hiddenName: 'cbBancoFondeo',
	 listeners: {
						select: function(combo) {
							if(strTipoUsuario == 'NAFIN') {
								Ext.getCmp('usuario').reset();
								Ext.getCmp('icEPO').reset();
								catalogoEPO.load({params: {cbBancoFondeo: combo.getValue()}});
							}
						}
					}
	},
	{
	 //Seleccionar EPO
	 xtype: 'combo',
	 fieldLabel: 'Nombre de la EPO',
	 displayField: 'descripcion',
	 valueField: 'clave',
	 triggerAction: 'all',
	 typeAhead: true,
	 minChars: 1,
	 store: catalogoEPO,
	 tpl: NE.util.templateMensajeCargaCombo,
	 name:'cbIcEPO',
	 id: 'icEPO',
	 mode: 'local',
	 forceSelection : true,
	 allowBlank: true,
	 hiddenName: 'cbIcEPO',
	 hidden: true,
	 emptyText: 'Seleccionar una EPO',
	 listeners: 
	 {
			select: function(combo,record, index) {
				Ext.getCmp('usuario').reset();
				catalogoUsuario.load({
								params : Ext.apply({
									icEPO : record.json.clave
								})
				});
			}
		}
	},
	
	{
	 //USUARIO
	 xtype: 'combo',
	 fieldLabel: 'Usuario',
	 emptyText: 'Seleccionar',
	 displayField: 'descripcion',
	 valueField: 'clave',
	 triggerAction: 'all',
	 typeAhead: true,
	 minChars: 1,
	 store: catalogoUsuario,
	 tpl: NE.util.templateMensajeCargaCombo,
	 name:'cbUsuario',
	 id: 'usuario',
	 mode: 'local',
	 hiddenName: 'cbUsuario',
	 forceSelection: true
   },
	{
	 //No. DOCUMENTO
	 xtype: 'textfield',
	 fieldLabel: 'No. de Documento',
	 inputType: 'text',
	 maxLength: 15,
	 name: 'txtNoDocumento',
	 id: 'noDocumento',
	 allowBlank: true
	},
	{
	 //No. ACUSE
	 xtype: 'textfield',
	 fieldLabel: 'No. de Acuse',
	 inputType: 'text',
	 maxLength: 22,
	 name: 'txtNoAcuse',
	 id: 'noAcuse',
	 allowBlank: true
	},
	{
	 xtype: 'compositefield',
	 fieldLabel: '* Fecha de publicaci�n',
	 combineErrors: false,
	 msgTarget: 'side',
	 items: [
            {
			    // Fecha Inicio
			    xtype: 'datefield',
             name: 'dFechaRegIni',
             id: 'dFechaRegIni',
             vtype: 'rangofecha',
             campoFinFecho: 'dFechaRegFin',
				 allowBlank: false,
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 minValue: '01/01/1901',
				 margins: '0 20 0 0'
            },
			   {
				 xtype: 'displayfield',
				 value: 'a',
				 width: 20
			   },
		      {
			    // Fecha Final
			    xtype: 'datefield',
             name: 'dFechaRegFin',
             id: 'dFechaRegFin',
             vtype: 'rangofecha',
             campoInicioFecha: 'dFechaRegIni',
				 allowBlank: false,
				 msgTarget: 'side',
				 width: 100,
				 startDay: 0,
				 width: 100,
				 minValue: '01/01/1901',
				 margins: '0 20 0 0'
             },
				 {
				 xtype: 'displayfield',
				 value: 'dd/mm/aaaa',
				 width: 20
			    }
            ]
	 },
	 {
	 xtype: 'compositefield',
	 fieldLabel: 'Monto',
	 combineErrors: false,
	 msgTarget: 'side',
	 items: [
            {
			    //MONTO INICIO
	           xtype: 'numberfield',
	           inputType: 'text',
	           maxLength: 15,
	           name: 'txtMontoInicio',
	           id: 'txtMontoInicio',
	           allowBlank: true,
				  width: 150,
				  margins: '0 20 0 0',
				  vtype: 'rangoValor',
				  campoFinValor: 'txtMontoFin',
				  msgTarget: 'side'
            },
			   {
				 xtype: 'displayfield',
				 value: 'a',
				 width: 20
			   },
		      {
			     //MONTO FIN
	           xtype: 'numberfield',
	           inputType: 'text',
				  width: 150,
	           maxLength: 15,
	           name: 'txtMontoFin',
	           id: 'txtMontoFin',
	           allowBlank: true,
				  margins: '0 20 0 0',
				  vtype: 'rangoValor',
				  campoInicioValor: 'txtMontoInicio',
				  msgTarget: 'side'
            }
            ]
	 },
	 {
	  //MONEDA
	  xtype: 'combo',
	  fieldLabel: 'Moneda',
	  emptyText: 'Seleccionar',
	  displayField: 'descripcion',
	  valueField: 'clave',
	  triggerAction: 'all',
	  typeAhead: true,
	  minChars: 1,
	  store: catalogoMoneda,
	  tpl: NE.util.templateMensajeCargaCombo,
	  name:'cbMoneda',
	  id: 'moneda',
	  mode: 'local',
	  hiddenName: 'cbMoneda',
	  forceSelection: true
    }
	];
  
  var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Consultar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
			  {	
					if(strPerfil == 'ADMIN EPO') {
					 var variable = Ext.getCmp('bancoFondeo');
					  if(Ext.isEmpty(variable.getValue())){
							variable.markInvalid('Seleccione un banco de fondeo.');
							variable.focus();
							return;
					  }
					}
					//pnl.el.mask('Enviando...', 'x-mask-loading');
					//var fp = Ext.getCmp('forma');		
		fp.el.mask('Enviando...', 'x-mask-loading');
		
					consultaData.load({ params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar', //Generar datos para la consulta
									start: 0,
									limit: 15})
							         });
			  }
			 },
			 {
			  //Bot�n Limpiar
			  xtype: 'button',
			  text: 'Limpiar',
			  name: 'btnLimpiar',
			  hidden: false,
			  //formBind: true,
			  iconCls: 'icoLimpiar',
			  handler: function(boton, evento) 
			  {
				window.location = '13consulta17ext.jsp'
			  }
			 }
		  ]
  });


//-------------------------------- PRINCIPAL -----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridGeneral
		]
	});

	if(strTipoUsuario == 'NAFIN') 
		Ext.getCmp('icEPO').setVisible(true);

	Ext.Ajax.request({
		url: '13consulta17ext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});

	catalogoBanco.load();
	catalogoMoneda.load();
	catalogoUsuario.load();

});
