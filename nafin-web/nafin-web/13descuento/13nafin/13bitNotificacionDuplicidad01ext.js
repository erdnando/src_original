Ext.onReady(function(){

	/********** Variables globales **********/
	var recordType = Ext.data.Record.create([{name:'clave'}, {name:'descripcion'}]);

	/***** Obtiene el nombre de la pyme a partir del numero de nafin electronico *****/
	function obtenerNombrePyme(){
		Ext.Ajax.request({
			url: '13bitNotificacionDuplicidad01ext.data.jsp',
			params: {
				informacion : 'Obtiene_Nombre_Pyme',
				ne_pyme     : Ext.getCmp('ne_pyme_id').getValue() 
			},
			callback: procesaNombrePyme
		});
	}

	/***** Muestra en el campo corrspondiente el nombre de la pyme buscado *****/
	var procesaNombrePyme = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.getCmp('nombre_pyme_id').setValue(Ext.util.JSON.decode(response.responseText).nombre);
			Ext.getCmp('ic_pyme_id').setValue(Ext.util.JSON.decode(response.responseText).ic_pyme);
		} else{
			Ext.Msg.alert('Mensaje...', Ext.util.JSON.decode(response.responseText).mensaje);
		}
	};

	/***** Descargar archivos CSV *****/
	function descargaArchivo(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			formaPrincipal.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			formaPrincipal.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarCSV').setIconClass('icoXls');
		Ext.getCmp('btnGenerarCSV').enable();
	}

	/***** Descargar archivos PDF *****/
	function descargaArchivoPDFTodo(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){

			var resp = Ext.util.JSON.decode(response.responseText);
			if(resp.estatusArchivo == 'FINAL'){
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				formaPrincipal.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				formaPrincipal.getForm().getEl().dom.submit();
				Ext.getCmp('btnImprimir').setText('Generar Todo');
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnImprimir').setIconClass('icoPdf');
			} else if(resp.estatusArchivo == 'ERROR'){
				Ext.Msg.alert('Error','No se gener� el archivo',function(btn){
					Ext.getCmp('btnImprimir').setText('Generar Todo');
					Ext.getCmp('btnImprimir').enable();
					Ext.getCmp('btnImprimir').setIconClass('icoPdf')
				});
			} else{
				Ext.getCmp('btnImprimir').setIconClass('loading-indicator');
				Ext.getCmp('btnImprimir').setText(resp.porcentaje);
				Ext.Ajax.request({
					url: '13bitNotificacionDuplicidad01ext.data.jsp',
					params: Ext.apply({
						informacion    : 'Imprimir_PDF',
						estatusArchivo : 'PROCESANDO',
						epo            : Ext.getCmp('epo_id').getValue(),
						ic_pyme        : Ext.getCmp('ic_pyme_id').getValue(),
						num_docto      : Ext.getCmp('num_docto_id').getValue(),
						fecha_inicio   : Ext.getCmp('fecha_inicio').getValue(),
						fecha_final    : Ext.getCmp('fecha_final').getValue()
					}),
					callback: descargaArchivoPDFTodo
				});
			}

		} else{
			Ext.getCmp('btnImprimir').setText('Generar Todo');
			Ext.getCmp('btnImprimir').enable();
			Ext.getCmp('btnImprimir').setIconClass('icoPdf')
			NE.util.mostrarConnError(response,opts);
		}
	}

	/********** Regresa la pantalla a su condici�n inicial **********/
	function procesoSalir(){
		Ext.getCmp('formaPrincipal').getForm().reset();
		Ext.getCmp('formaPrincipal').show();
		Ext.getCmp('nombre_pyme_id').disable();
		Ext.getCmp('ne_pyme_id').disable();
		Ext.getCmp('gridConsulta').hide();
	}

	/********** Se abre la ventana emergente de B�squeda Avanzada **********/
	function abrirBusquedaAvanzada(){

		if(Ext.getCmp('epo_id').getValue() == ''){
			Ext.getCmp('epo_id').markInvalid('Debe seleccionar una Cadena.');
			return null;
		}

		var ventana = Ext.getCmp('ventanaBusquedaAv');
		if (ventana){
			Ext.getCmp('formaEmergenteSup').getForm().reset();
			Ext.getCmp('formaEmergenteInf').getForm().reset();

			Ext.getCmp('cmb_pyme_id').setValue();
			Ext.getCmp('cmb_pyme_id').store.removeAll();
			Ext.getCmp('cmb_pyme_id').reset();
			ventana.show();
		} else{
			var ventanaBusquedaAv = new Ext.Window ({
				x:           300,
				y:           100,
				width:       550,
				id:          'ventanaBusquedaAv',
				title:       'B�squeda Avanzada',
				closeAction: 'hide',
				modal:       true,
				autoHeight:  true,
				items:[formaEmergenteSup, formaEmergenteInf]
			}).show();
		}
	}

	/********** Se cierran los forms para la B�squeda Avanzada **********/
	function cerrarBusquedaAvanzada(){
		Ext.getCmp('ventanaBusquedaAv').hide();
	}

	/********** Realiza la B�squeda Avanzada **********/
	function procesoBusquedaAvanzada(){
		if(Ext.getCmp('nombre_id').getValue() == '' && Ext.getCmp('rfc_id').getValue() == '' && Ext.getCmp('nafin_electronico_id').getValue() == ''){
			Ext.Msg.alert('Mensaje...', 'Debe seleccionar al menos un par�metro.');
			return;
		}
		if( Ext.getCmp('formaEmergenteSup').getForm().isValid()){
			Ext.getCmp('btnBusquedaEmergente').setIconClass('loading-indicator');
			catalogoPyme.load({
				params: Ext.apply({
					epo      : Ext.getCmp('epo_id').getValue(),
					nombre   : Ext.getCmp('nombre_id').getValue(),
					rfc      : Ext.getCmp('rfc_id').getValue(),
					num_pyme : Ext.getCmp('nafin_electronico_id').getValue()
				})
			});
		}
	}

	/********** Proceso para llenar el combo Pyme (Combo del form Busqueda Avanzada) **********/
	var procesarCatalogoPyme = function(store, arrRegistros, opts){
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == 'excede'){
				Ext.getCmp('btnBusquedaEmergente').setIconClass('icoBuscar');
				Ext.Msg.alert('Buscar...','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('formaEmergenteSup').getForm().reset();
				Ext.getCmp('formaEmergenteInf').getForm().reset();
				return;
			}else if (jsonData.total == '0'){
				Ext.getCmp('btnBusquedaEmergente').setIconClass('icoBuscar');
				Ext.Msg.alert('Buscar...','No existe informaci�n con los criterios determinados');
				Ext.getCmp('formaEmergenteSup').getForm().reset();
				Ext.getCmp('formaEmergenteInf').getForm().reset();
				return;
			} else{
				Ext.getCmp('btnBusquedaEmergente').setIconClass('icoBuscar');
				Ext.getCmp('btnAceptarEmergente').enable();
				var pyme = Ext.getCmp('cmb_pyme_id');
				Ext.getCmp('cmb_pyme_id').setValue('');
				if(pyme.getValue() == ''){
					var newRecord = new recordType({
						clave: '',
						descripcion: 'Seleccionar ....'
					});
					store.insert(0, newRecord);
					store.commitChanges();
					pyme.setValue('');
				}
			}
			Ext.getCmp('cmb_pyme_id').focus();
		}
	};

	/********** Proceso Aceptar Busqueda Avanzada **********/
	function aceptarBusquedaAvanzada(){
		if (!Ext.isEmpty(Ext.getCmp('cmb_pyme_id').getValue())){
			var disp = Ext.getCmp('cmb_pyme_id').lastSelectionText;
			var cveP = disp.substr(0,disp.indexOf(' '));
			var desc = disp.slice(disp.indexOf(' ')+1);
			Ext.getCmp('ic_pyme_id').setValue(Ext.getCmp('cmb_pyme_id').getValue());
			Ext.getCmp('ne_pyme_id').setValue(cveP);
			Ext.getCmp('nombre_pyme_id').setValue(desc);
			Ext.getCmp('ne_pyme_id').enable();
			Ext.getCmp('nombre_pyme_id').enable();
			Ext.getCmp('btnAceptarEmergente').disable();
			cerrarBusquedaAvanzada();
		}
	}

	/********** Proceso para llenar el combo Epo **********/
	var procesarCatalogoEpo = function(store, records, oprion){
		if(store.getTotalCount()>0 && Ext.isEmpty(records[0].data.loadMsg)){
			var epo = Ext.getCmp('epo_id');
			Ext.getCmp('epo_id').setValue('');
			if(epo.getValue() == ''){
				var newRecord = new recordType({
					clave: '',
					descripcion: 'Seleccionar ....'
				});
				store.insert(0, newRecord);
				store.commitChanges();
				epo.setValue('');
			}
		}
	};

	/***** Realiza la consulta para llenar el grid *****/
	function procesoConsultarDatos(){
		if(Ext.getCmp('epo_id').getValue() == ''){
			Ext.getCmp('epo_id').markInvalid('El campo es obligatorio');
			return;
		}
		if((Ext.getCmp('fecha_inicio').getValue() == '' && Ext.getCmp('fecha_final').getValue() != '') || 
			(Ext.getCmp('fecha_inicio').getValue() != '' && Ext.getCmp('fecha_final').getValue() == '')){
			Ext.getCmp('fecha_inicio').markInvalid('Debe ingresar ambas fechas');
			Ext.getCmp('fecha_final').markInvalid('Debe ingresar ambas fechas');
			return;
		}
		if( Ext.getCmp('formaPrincipal').getForm().isValid()){
			formaPrincipal.el.mask('Procesando...', 'x-mask-loading');
			consultaData.load({
				params: Ext.apply({
					operacion    : 'generar',
					epo          : Ext.getCmp('epo_id').getValue(),
					ic_pyme      : Ext.getCmp('ic_pyme_id').getValue(),
					num_docto    : Ext.getCmp('num_docto_id').getValue(),
					fecha_inicio : Ext.getCmp('fecha_inicio').getValue(),
					fecha_final  : Ext.getCmp('fecha_final').getValue(),
					start        : 0,
					limit        : 15
				})
			});	
		}
	}

	/********** Proceso y consulta para llenar el grid **********/
	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('formaPrincipal');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();
		if (arrRegistros != null){
			Ext.getCmp('gridConsulta').show();
			var jsonData = store.reader.jsonData;
			if(store.getTotalCount() > 0){
				el.unmask();
				Ext.getCmp('btnImprimir').enable();
				Ext.getCmp('btnGenerarCSV').enable();
			} else{
				Ext.getCmp('btnImprimir').disable();
				Ext.getCmp('btnGenerarCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	};

	/********** Se crea el store para el combo Pyme (Combo del form Busqueda Avanzada) **********/
	var catalogoPyme = new Ext.data.JsonStore({
		id:     'catalogoPyme',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '13bitNotificacionDuplicidad01ext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Pyme'
		},
		totalProperty:  'total',
		autoLoad:       false,
		listeners: {
			load:        procesarCatalogoPyme,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/********** Se crea el store para el combo Epo **********/
	var catalogoEpo = new Ext.data.JsonStore({
		id:     'catalogoEpo',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '13bitNotificacionDuplicidad01ext.data.jsp',
		baseParams: {
			informacion: 'Catalogo_Epo'
		},
		totalProperty:  'total',
		autoLoad:       true,
		listeners: {
			load:        procesarCatalogoEpo,
			exception:   NE.util.mostrarDataProxyError,
			beforeload:  NE.util.initMensajeCargaCombo
		}
	});

	/********** Se crea el store para el Grid **********/
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url:  '13bitNotificacionDuplicidad01ext.data.jsp',
		baseParams: {
			informacion: 'Consultar_Datos'
		},
		fields: [
			{ name: 'EPO'                },
			{ name: 'PYME'               },
			{ name: 'NO_DOCTO'           },
			{ name: 'FECHA_EMISION'      },
			{ name: 'MONEDA'             },
			{ name: 'MONTO'              },
			{ name: 'USUARIO_PUBLICO'    },
			{ name: 'NOMBRE_USUARIO'     },
			{ name: 'FECHA_ALERTA'       },
			{ name: 'ESTATUS_PUBLICACION'}
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	/********** Se crea el grid de consulta**********/
	var gridConsulta = new Ext.grid.EditorGridPanel({
		width:        900,
		height:       420,
		clicksToEdit: 1,
		store:        consultaData,
		id:           'gridConsulta',
		title:        'Bit�cora de Notificaciones por Duplicidad',
		margins:      '20 0 0 0',
		style:        'margin:0 auto;',
		align:        'center',
		hidden:       true,
		displayInfo:  true,
		loadMask:     true,
		stripeRows:   true,
		frame:        false,
		columns: [
			{width: 200, header: 'EPO',                    dataIndex: 'EPO',                 align: 'left',   sortable: true, resizable: true},
			{width: 200, header: 'PYME',                   dataIndex: 'PYME',                align: 'left',   sortable: true, resizable: true},
			{width: 100, header: 'No. Docto',              dataIndex: 'NO_DOCTO',            align: 'center', sortable: true, resizable: true},
			{width: 120, header: 'Fecha de emisi�n',       dataIndex: 'FECHA_EMISION',       align: 'center', sortable: true, resizable: true},
			{width: 150, header: 'Moneda',                 dataIndex: 'MONEDA',              align: 'center', sortable: true, resizable: true},
			{width: 150, header: 'Monto',                  dataIndex: 'MONTO',               align: 'center', sortable: true, resizable: true, renderer: Ext.util.Format.numberRenderer('$0,000.00')},
			{width: 100, header: 'Usuario p�blico',        dataIndex: 'USUARIO_PUBLICO',     align: 'center', sortable: true, resizable: true},
			{width: 200, header: 'Nombre usuario',         dataIndex: 'NOMBRE_USUARIO',      align: 'left',   sortable: true, resizable: true},
			{width: 150, header: 'Fecha de alerta',        dataIndex: 'FECHA_ALERTA',        align: 'center', sortable: true, resizable: true},
			{width: 130, header: 'Estatus de publicaci�n', dataIndex: 'ESTATUS_PUBLICACION', align: 'center', sortable: true, resizable: true}
		],
		bbar: {
			xtype:       'paging',
			id:          'barraPaginacion',
			displayMsg:  '{0} - {1} de {2}',
			emptyMsg:    'No hay registros.',
			buttonAlign: 'left',
			displayInfo: true,
			pageSize:    15,
			store:       consultaData,
			items: ['->','-',{
				xtype:   'button',
				id:      'btnGenerarCSV',
				text:    'Generar Archivo',
				tooltip: 'Imprime los registros en formato CSV.',
				iconCls: 'icoXls',
				handler: function(boton, evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13bitNotificacionDuplicidad01ext.data.jsp',
						params: Ext.apply({
							informacion  : 'Imprimir_CSV',
							operacion    : 'generar',
							epo          : Ext.getCmp('epo_id').getValue(),
							ic_pyme      : Ext.getCmp('ic_pyme_id').getValue(),
							num_docto    : Ext.getCmp('num_docto_id').getValue(),
							fecha_inicio : Ext.getCmp('fecha_inicio').getValue(),
							fecha_final  : Ext.getCmp('fecha_final').getValue()
						}),
						callback: descargaArchivo
					});
				}
			},'-',{
				xtype:   'button',
				id:      'btnImprimir',
				text:    'Generar Todo',
				tooltip: 'Imprime los registros en formato PDF.',
				iconCls: 'icoPdf',
				handler: function(boton, evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13bitNotificacionDuplicidad01ext.data.jsp',
						params: Ext.apply({
							informacion    : 'Imprimir_PDF',
							estatusArchivo : 'INICIO',
							epo            : Ext.getCmp('epo_id').getValue(),
							ic_pyme        : Ext.getCmp('ic_pyme_id').getValue(),
							num_docto      : Ext.getCmp('num_docto_id').getValue(),
							fecha_inicio   : Ext.getCmp('fecha_inicio').getValue(),
							fecha_final    : Ext.getCmp('fecha_final').getValue()
						}),
						callback: descargaArchivoPDFTodo
					});
				}
			}/*{
				xtype:   'button',
				id:      'btnImprimir',
				text:    'Generar PDF',
				iconCls: 'icoPdf',
				handler: function(boton, evento){
					var barraPaginacionA = Ext.getCmp('barraPaginacion');
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13bitNotificacionDuplicidad01ext.data.jsp',
						params: Ext.apply({
							informacion  : 'Imprimir_PDF',
							epo          : Ext.getCmp('epo_id').getValue(),
							ic_pyme      : Ext.getCmp('ic_pyme_id').getValue(),
							num_docto    : Ext.getCmp('num_docto_id').getValue(),
							fecha_inicio : Ext.getCmp('fecha_inicio').getValue(),
							fecha_final  : Ext.getCmp('fecha_final').getValue(),
							start        : barraPaginacionA.cursor,
							limit        : barraPaginacionA.pageSize
						}),
						callback: descargaArchivo
					});
				}
			}*/]
		}
	});

	/********** Form Panel Emergente Superior para buscar el n�mero de Nafin electr�nico por criterios m�s espec�ficos **********/
	var formaEmergenteSup = new Ext.form.FormPanel({
		width:         532,
		labelWidth:    70,
		id:            'formaEmergenteSup',
		layout:        'form',
		style:         'margin: 0 auto;',
		hidden:        false,
		frame:         true,
		border:        false,
		autoHeight:    true,
		items: [{
			xtype:      'displayfield',
			anchor:     '95%',
			value:      '&nbsp;&nbsp; Utilice el * para b�squeda gen�rica'
		},{
			xtype:      'textfield',
			id:         'nombre_id',
			name:       'nombre',
			fieldLabel: 'Nombre',
			msgTarget:  'side',
			margins:    '0 20 0 0',
			anchor:     '95%',
			maxLength:  80
		},{
			xtype:      'textfield',
			id:         'rfc_id',
			name:       'rfc',
			fieldLabel: 'RFC',
			//vtype:      'validaRFC',
			msgTarget:  'side',
			margins:    '0 20 0 0',
			anchor:     '95%',
			maxLength:  16
		},{
			xtype:      'textfield',
			id:         'nafin_electronico_id',
			name:       'nafin_electronico',
			fieldLabel: 'No. Nafin Electr�nico',
			msgTarget:  'side',
			margins:    '0 20 0 0',
			anchor:     '95%',
			maxLength:  25
		}],
		buttons: [{
			xtype:      'button',
			text:       'Buscar',
			id:         'btnBusquedaEmergente',
			iconCls:    'icoBuscar',
			handler:    procesoBusquedaAvanzada
		},{
			xtype:      'button',
			text:       'Cancelar',
			id:         'btnCancelarEmergente',
			iconCls:    'icoCancelar',
			handler:    cerrarBusquedaAvanzada
		}]
	});

	/********** Form Panel Emergente Inferior para buscar el n�mero de Nafin electr�nico por criterios m�s espec�ficos **********/
	var formaEmergenteInf = new Ext.form.FormPanel({
		width:             532,
		labelWidth:        70,
		id:                'formaEmergenteInf',
		layout:            'form',
		style:             'margin: 0 auto;',
		hidden:            false,
		frame:             true,
		border:            false,
		autoHeight:        true,
		items: [{
			xtype:          'combo',
			id:             'cmb_pyme_id',
			name:           'cmb_pyme',
			fieldLabel:     'Nombre',
			emptyText:      'Seleccione...',
			displayField:   'descripcion',
			valueField:     'clave',
			msgTarget:      'side',
			mode:           'local',
			triggerAction:  'all',
			anchor:         '95%',
			forceSelection: true,
			typeAhead:      true,
			allowBlank:     true,
			store:          catalogoPyme
		}],
		buttons: [{
			xtype:          'button',
			text:           'Aceptar',
			id:             'btnAceptarEmergente',
			iconCls:        'icoAceptar',
			disabled:       true,
			handler:        aceptarBusquedaAvanzada
		},{
			xtype:          'button',
			text:           'Cancelar',
			id:             'btnCancelarEmergente1',
			iconCls:        'icoCancelar',
			handler:        cerrarBusquedaAvanzada
		}]
	});

	/********** Form Panel Principal **********/
	var formaPrincipal = new Ext.form.FormPanel({
		width:                  600,
		labelWidth:             100,
		id:                     'formaPrincipal',
		title:                  'Criterios de B�squeda',
		layout:                 'form',
		style:                  'margin: 0 auto;',
		frame:                  true,
		hidden:                 false,
		border:                 true,
		autoHeight:             true,
		items: [{
			xtype:               'textfield',
			id:                  'ic_pyme_id',
			name:                'ic_pyme',
			fieldLabel:          '&nbsp;&nbsp; IC_PYME',
			hidden:              true
		},{
			xtype:               'combo',
			id:                  'epo_id',
			name:                'epo',
			fieldLabel:          '&nbsp;&nbsp; EPO',
			emptyText:           'Seleccione una EPO...',
			displayField:        'descripcion',
			valueField:          'clave',
			msgTarget:           'side',
			mode:                'local',
			triggerAction:       'all',
			anchor:              '95%',
			forceSelection:      true,
			typeAhead:           true,
			allowBlank:          false,
			store:               catalogoEpo,
			listeners: {
				select: {
					fn: function(combo){
						var campo = combo.getValue();
						if(campo != ''){
							Ext.getCmp('ne_pyme_id').enable();
							Ext.getCmp('nombre_pyme_id').enable();
						}
					}
				}
			}
		},{
			xtype:               'compositefield',
			fieldLabel:          '&nbsp;&nbsp; N@E Pyme',
			msgTarget:           'side',
			combineErrors:       false,
			items: [{
				width:            150,
				xtype:            'numberfield',
				id:               'ne_pyme_id',
				name:             'ne_pyme',
				msgTarget:        'side',
				margins:          '0 20 0 0',
				disabled:         true,
				maskRe:           /[0-9]/,
				maxLength:        25,
				listeners: {
					'change': function(field){
						if(!Ext.isEmpty(field.getValue())&&field.isValid()){
							obtenerNombrePyme();
						}
					}
				}
			},{
				width:            283,
				xtype:            'textfield',
				id:               'nombre_pyme_id',
				name:             'nombre_pyme',
				msgTarget:        'side',
				margins:          '0 20 0 0',
				//maxLength:        24,
				disabled:         true
			}]
		},{
			xtype:               'compositefield',
			items: [{
				width:            10,
				xtype:            'displayfield',
				value:            '&nbsp;'
			},{
				xtype:            'button',
				text:             'B�squeda Avanzada',
				id:               'btnBuscarAvanzada',
				iconCls:          'icoBuscar',
				handler:          abrirBusquedaAvanzada
			}]
		},{
			xtype:               'textfield',
			id:                  'num_docto_id',
			name:                'num_docto',
			fieldLabel:          '&nbsp;&nbsp; No. documento',
			msgTarget:           'side',
			margins:             '0 20 0 0',
			anchor:              '95%',
			maxLength:           15
		},{
			xtype:               'compositefield',
			fieldLabel:          '&nbsp;&nbsp; Fecha de alerta',
			msgTarget:           'side',
			combineErrors:       false,
			items: [{
				width:            205,
				xtype:            'datefield',
				id:               'fecha_inicio',
				name:             'fecha_inicio',
				msgTarget:        'side',
				vtype:            'rangofecha',
				minValue:         '01/01/1901',
				campoFinFecha:    'fecha_final',
				margins:          '0 20 0 0',
				value:            new Date(),
				allowBlank:       true,
				startDay:         0
			},{
				width:            20,
				xtype:            'displayfield',
				value:            'a'
			},{
				width:            205,
				xtype:            'datefield',
				id:               'fecha_final',
				name:             'fecha_final',
				msgTarget:        'side',
				vtype:            'rangofecha',
				minValue:         '01/01/1901',
				campoInicioFecha: 'fecha_inicio',
				margins:          '0 20 0 0',
				value:            new Date(),
				allowBlank:       true,
				startDay:         1
			}]
		}],
		buttons: [{
				xtype:            'button',
				text:             'Buscar',
				id:               'btnBuscar',
				iconCls:          'icoBuscar',
				handler:          procesoConsultarDatos
			},{
				xtype:            'button',
				text:             'Limpiar',
				id:               'btnLimpiar',
				iconCls:          'icoLimpiar',
				handler:          procesoSalir
		}]
	});

	/********** Contenedor Principal **********/
	var pnl = new Ext.Container({
		width:   949,
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin: 0 auto;',
		items: [
			NE.util.getEspaciador(20),
			formaPrincipal,
			NE.util.getEspaciador(10),
			gridConsulta
		]
	});

});