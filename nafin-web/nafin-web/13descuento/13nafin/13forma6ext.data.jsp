<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*,
				java.io.*,
				java.sql.*,
				com.netro.descuento.*, 
				netropology.utilerias.*,   
				com.netro.model.catalogos.CatalogoSimple,   
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%  
String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
String tipoProceso = (request.getParameter("tipo_proceso")==null)?"":request.getParameter("tipo_proceso");
String idProceso = (request.getParameter("id_proceso")==null)?"":request.getParameter("id_proceso");
String fechaDetalle = (request.getParameter("fecha_detalle")==null)?"":request.getParameter("fecha_detalle");
String idsProcesos[]	= request.getParameterValues("ids_procesos");
String tipoIF = (request.getParameter("tipo_if")==null)?"":request.getParameter("tipo_if");
String numIF = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
String icMoneda = (request.getParameter("moneda")==null)?"":request.getParameter("moneda");
String fechaCorte = (request.getParameter("FechaCorte")==null)?"":request.getParameter("FechaCorte");
String fechaDe	= (request.getParameter("fecha_ejec_de")==null)?"":request.getParameter("fecha_ejec_de");
String fechaA	= (request.getParameter("fecha_ejec_a")==null)?"":request.getParameter("fecha_ejec_a");
String fechaProcDe	= (request.getParameter("fecha_proc_de")==null)?"":request.getParameter("fecha_proc_de");
String fechaProcA	= (request.getParameter("fecha_proc_a")==null)?"":request.getParameter("fecha_proc_a");
String infoRegresar = "";

String numRegEliminar = "10000"; //-- Indica el numero de registros a eliminar por cada bloque

if(informacion.equals("consultaProcesos") || 
			informacion.equals("cancelarProcesos") ||
			informacion.equals("reprocesarProcesos") ||
			informacion.equals("consultaDetalleProcesos") ||
			informacion.equals("ConsultarTotales") ||
			informacion.equals("ArchivoCSV") ||
			informacion.equals("descargarArchivo")
			
			){
			
	PagosIFNB beanProd = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
	
	if(informacion.equals("consultaProcesos")){
		//Llenar grid productos
		JSONArray jsObjArray = new JSONArray();
		List lista = new ArrayList();
		List vecFilas = beanProd.consultarProcesos(fechaDe, fechaA, fechaProcDe, fechaProcA, tipoProceso, tipoIF);		
		for (int i=0; i < vecFilas.size(); i++){
			List vecColumnas = (List) vecFilas.get(i);
			HashMap mapa = new HashMap();
			mapa.put("ID_PROCEDIMIENTO", (String)vecColumnas.get(2));
			mapa.put("FECHA_EJECUCION", (String)vecColumnas.get(0));
			mapa.put("HORA_EJECUCION", (String)vecColumnas.get(1));
			mapa.put("FECHA_FIN_MES", (String)vecColumnas.get(5));
			mapa.put("FECHA_PROB_PAGO", (String)vecColumnas.get(5));
			mapa.put("FECHA_OPERACION", (String)vecColumnas.get(5));
			mapa.put("NUMREG_MN", (String)vecColumnas.get(6));
			mapa.put("NUMREG_DLS", (String)vecColumnas.get(7));
			mapa.put("NUM_INTERM", (String)vecColumnas.get(4));
			mapa.put("CG_ARCHIVO", (String)vecColumnas.get(8));
			lista.add(mapa);
		}		
		jsObjArray = JSONArray.fromObject(lista);
		infoRegresar	=		"{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
		
	}else if(informacion.equals("cancelarProcesos")){
		List listaProcesos = new ArrayList(Arrays.asList(idsProcesos));
		beanProd.cancelarProceso(listaProcesos, tipoProceso, numRegEliminar ); //-- Se agrego el numero de registros a eliminar por bloque --//

	}else if(informacion.equals("reprocesarProcesos")){
		String mensaje = "";
		List listaProcesos = new ArrayList(Arrays.asList(idsProcesos));
		beanProd.cancelarProceso(listaProcesos, tipoProceso, numRegEliminar ); //-- Se agrego el numero de registros a eliminar por bloque --//
		if (tipoProceso.equals("V")){
			mensaje = beanProd.obtenerVencimientosSirac();
		}else if (tipoProceso.equals("O")){
			mensaje = beanProd.obtenerOperadosSirac();
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("textoMensaje", mensaje);
		infoRegresar = jsonObj.toString();
		System.out.println(infoRegresar);
	
	}else if(informacion.equals("ArchivoCSV")){			
		String nombreArchivo = "";
		List registros = beanProd.consultarDetalleProceso(idProceso, tipoProceso, tipoIF, fechaDetalle);
		nombreArchivo = beanProd.crearCustomFile(registros, strDirectorioTemp, "CVS");	
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("consultaDetalleProcesos") || informacion.equals("ConsultarTotales")){
		String nombArchivo = "";
		int numRegistrosMN = 0;
		int numRegistrosDLS = 0;
		//String ruta = strDirectorioPublicacion + "16archivos/descuento/procesos/";
		String fecha = fechaDetalle.substring(0,2) + 
		"_" + fechaDetalle.substring(3,5) +
		"_" + fechaDetalle.substring(6,10);
		int Ls = 0;
		//String archzip = "";
		if (tipoProceso.equals("E")) {
			nombArchivo = "archedocuenta_"+fecha+"_";
			//ruta += "edocuenta/";
		} else if (tipoProceso.equals("V")) {
			nombArchivo = "archvenc_"+fecha+"_";
			//ruta += "vencimiento/";
		} else if (tipoProceso.equals("O")) {
			nombArchivo = "archoper_"+fecha+"_";
			//ruta += "operado/";
		}
		JSONArray jsObjArray = new JSONArray();
		List lista = new ArrayList();
		List vecFilas = beanProd.consultarDetalleProceso(idProceso, tipoProceso, tipoIF, fechaDetalle);
		for (int i=0; i < vecFilas.size(); i++){
			List vecColumnas = (List) vecFilas.get(i);
			HashMap mapa = new HashMap();
			mapa.put("IC_FINANCIERA", (String)vecColumnas.get(0));
			mapa.put("IC_IF", (String)vecColumnas.get(1));
			mapa.put("CG_RAZON_SOCIAL", (String)vecColumnas.get(2));
			mapa.put("TOTAL_MN", (String)vecColumnas.get(3));
			mapa.put("TOTAL_DLS", (String)vecColumnas.get(4));
			mapa.put("FECHA_CORTE", (String)vecColumnas.get(5));
			mapa.put("IC_MONEDA", (String)vecColumnas.get(6));			
			/*
			archzip = ruta + nombArchivo + (String)vecColumnas.get(1) + "_S.zip";
			File zf = new File(archzip);
			if(!zf.exists()){
				Ls = 0;
				archzip = ruta + nombArchivo + (String)vecColumnas.get(1) + "_N.zip";
				zf = new File(archzip);
				if(zf.exists()){
					Ls = 1;
				}else{
					Ls = 0;
				}
			}else{
				Ls = 1;
			}
			if(Ls != 0){
				mapa.put("ARCHIVO_ZIP", "SI");
			}else{
				mapa.put("ARCHIVO_ZIP", "NO");
			}
			*/
			
			nombArchivo += (String)vecColumnas.get(1) + "_S.zip";
			
			if(beanProd.existeArchivosZip(nombArchivo)){
				mapa.put("ARCHIVO_ZIP", "SI");
			}else{
				mapa.put("ARCHIVO_ZIP", "NO");
			}
			String auxMN = (String)vecColumnas.get(3);
			numRegistrosMN += Integer.parseInt(auxMN);
			String auxDLS = (String)vecColumnas.get(4);
			numRegistrosDLS += Integer.parseInt(auxDLS);
			lista.add(mapa);
		}
		if(informacion.equals("consultaDetalleProcesos")){
			jsObjArray = JSONArray.fromObject(lista);
			infoRegresar	=		"{\"success\": true, \"total\": \""	+	jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+ "}";
			
		}else if(informacion.equals("ConsultarTotales")){		
			JSONArray registros = new JSONArray();
			HashMap datos = new HashMap();
			datos.put("TOTALES_MN", String.valueOf(numRegistrosMN));
			datos.put("TOTALES_DLS", String.valueOf(numRegistrosDLS));
			registros.add(datos);
			infoRegresar	=		"{\"success\": true, \"total\": \""	+	registros.size() + "\", \"registros\": " + registros.toString()+ "}";
		}
	
	}else if(informacion.equals("descargarArchivo")){
		CQueryHelper queryHelper = null;	
		ResultSet rs;
		GenerarContenidoArchivos conten = new GenerarContenidoArchivos();
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = null;
		String contArchivo = "";
		JSONObject jsonObj = new JSONObject();
		
		if(tipoProceso.equals("E")){
			AccesoDB con = new AccesoDB();
			con.conexionDB();
			queryHelper = new CQueryHelper(new EstCuentaIfDE());
			rs = queryHelper.getCreateFile(request,con);				
			contArchivo = conten.llenarContenido(rs, "S", 3,"","");
			if (con.hayConexionAbierta() == true)
				con.cierraConexionDB();
			}
		else if(tipoProceso.equals("V")){
			AccesoDB con = new AccesoDB();
			con.conexionDB();
			queryHelper = new CQueryHelper(new VencIfDE());
			rs = queryHelper.getCreateFile(request,con);				
			contArchivo = conten.llenarContenido(rs, "S", 1,"","");
			if (con.hayConexionAbierta() == true)
				con.cierraConexionDB();
			}
		else if(tipoProceso.equals("O")){
			AccesoDB con = new AccesoDB();
			con.conexionDB();
			queryHelper = new CQueryHelper(new OperIfDE());		
			rs = queryHelper.getCreateFile(request,con);	
			contArchivo = conten.llenarContenido(rs, "S", 2,"","");
			if (con.hayConexionAbierta() == true)
				con.cierraConexionDB();
			}
		if(!archivo.make(contArchivo, strDirectorioTemp, ".txt"))
			out.print("<--!Error al generar el archivo-->");
		else
			nombreArchivo = archivo.nombre;
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
		infoRegresar = jsonObj.toString();
	}
	
}%>
<%=  infoRegresar%>
