var verTasas;

var texto = ['Habilitada para operar Factoraje Vencido con pago de intereses por cuenta de la empresa de Primer Orden'];						

Ext.onReady(function() {

var nombreEPO;
var strPerfil =  Ext.getDom('strPerfil').value;	
var strTipoUsuario = Ext.getDom('strTipoUsuario').value;
	
Ext.QuickTips.init();
		
//_--------------------------------- HANDLERS -------------------------------

var procesarVerTasasData = function(store, arrRegistros, opts) {
	var fp = Ext.getCmp('forma');
		fp.el.unmask();		
			
		var gridVerTasas = Ext.getCmp('gridVerTasas');
		if (arrRegistros != null ) {
			if (!gridVerTasas.isVisible()) {
				gridVerTasas.show();
			}						
		}
		var el = gridVerTasas.getGridEl();
		if (arrRegistros == '') {
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}else if (arrRegistros != null){
			el.unmask();
		}
		
	}			

	//HANDLERS  Historico 	
	var procesarHistoricoData = function(store, arrRegistros, opts) {
	var fp = Ext.getCmp('forma');	
		fp.el.unmask();			
		if (arrRegistros != null) {
			if (!gridHistorico.isVisible()) {
				gridHistorico.show();
			}				
		}
		var el = gridHistorico.getGridEl();
		if (arrRegistros == '') {
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}else if (arrRegistros != null){
			el.unmask();
		}		
	}	
	
	//HANDLERS GENERAL	
	var procesarGeneralData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		var valor = jsonData.VALOR;
		nombreEPO = jsonData.NOMBREEPO;
		
		var fp = Ext.getCmp('forma');	
		fp.el.unmask();		
		
		if(valor =='S'){	
			fpT.show();			
		}else{
			fpT.hide();
		}
		
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
					gridGeneral.show();
			}				
		}		
		var el = gridGeneral.getGridEl();		
		if (arrRegistros == '') {
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}else if (arrRegistros != null){
			el.unmask();
		}
	}	
	
//-------------------------------- STORES -----------------------------------	
var CatalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta06.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});

	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta06.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	
	var catalogoBancoFondeo = new Ext.data.JsonStore({
		id: 'catalogoBancoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta06.data.jsp',
		baseParams: {
			informacion: 'CatalogoBanco'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	function verTasas1(no_epo, fecha) {
	var no_if = Ext.getCmp("cmbIF").getValue();
	
	var ventana = Ext.getCmp('winVerTasas');
	if (ventana) {		
		ventana.destroy();		
	} 
		
		consultaVERTASASData.load({
			params: {no_epo :no_epo, fechaAnterior: fecha , no_if: no_if}	
		});
						
		new Ext.Window({
				width: 870,
				height: 400,
				id: 'winVerTasas',
				closeAction: 'hide',
				//plain: true,
				modal: true,
				items: [
				gridVerTasas
				],
				title: nombreEPO
				}).show();				
		
		if (!Ext.fly('winVerTasas')) { 
			consultaVERTASASData.load({
			params: {no_epo :no_epo, fechaAnterior: fecha , no_if: no_if}	
		});
		
		new Ext.Window({
				//layout: 'fit',
				width: 870,
				height: 400,				
				id: 'winVerTasas',
				closeAction: 'hide',				
				items: [
				gridVerTasas
				],
				title: 'Ver Tasas'
				});
		}	
	}	
	
	verTasas = verTasas1;
	
		var CatalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta06.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	// ELEMENTOS DE LA FORMA 										
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_banco_fondeo',
			id: 'ic_banco_fondeo2',
			fieldLabel: 'Banco de Fondeo',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_banco_fondeo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			allowBlank: true,
			store : catalogoBancoFondeo,
			tpl : NE.util.templateMensajeCargaCombo,
				listeners: {
				select: {
					fn: function(combo) {
						var epoComboCmp = Ext.getCmp('cmbEPO');
						epoComboCmp.setValue('');
						epoComboCmp.setDisabled(false);
						epoComboCmp.store.load({
								params: {
									ic_banco_fondeo:combo.getValue()
								}
						});
					}
				}
			}
		},
			{
			xtype: 'combo',
			name: 'no_epo',
			id:'cmbEPO',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'no_epo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					
					fn: function(combo) {
						Ext.getCmp('idLimpiar').setVisible(true);
						Ext.getCmp('idConsultar').setVisible(true);
						
						var ifComboCmp = Ext.getCmp('cmbIF');
						ifComboCmp.setValue('');
						ifComboCmp.setDisabled(false);
						ifComboCmp.store.load({
								params: {
									no_epo: combo.getValue()
								}
						});
					}
				}
			}			
		},
		{
			xtype: 'combo',
			name: 'no_if',
			id: 'cmbIF',
			fieldLabel: 'Intermediario Financiero',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'no_if',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : CatalogoIFData,
			tpl : NE.util.templateMensajeCargaCombo
		}		
		
		];

var consultaVERTASASData   = new Ext.data.GroupingStore({	 
		root : 'registros',
		url : '13consulta06.data.jsp',
		baseParams: {
			informacion: 'VerTasas'	
		},		
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [	
		  {name:'NOMIFV'},
			{name:'MONEDAV'},
			{name:'TASABASEV'},
			{name:'PLAZOV'},
			{name:'VALORV'},
			{name:'RETMATV'},
			{name:'PTOSV'},	
			{name:'TASA_APLICARV'}	
		]
		}),
		groupField: 'NOMIFV',
		sortInfo:{field: 'NOMIFV', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarVerTasasData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarVerTasasData(null, null, null);
				}
			}
		}		
	});
		
	//CONSULTA PARA LOS DEL GRID Historico de Tasas
	var consultaHistoricoData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta06.data.jsp',
		baseParams: {
			informacion: 'Historico'
		},
		fields: [				  
			{name: 'EPO'},
			{name: 'FECHA'}					
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarHistoricoData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarHistoricoData(null, null, null);
				}
			}
		}		
	});
	
	
		//CONSULTA PARA LOS DEL GRID GENERAL
	var consultaGeneralData   = new Ext.data.GroupingStore({	 
		root : 'registros',
		url : '13consulta06.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
			{name: 'NOMBREIF'},
			{name: 'NOMBREMONEDA'},					
			{name: 'NOMBRETASA'},					
			{name: 'RANGOPLAZO'},					
			{name: 'VALOR'},					
			{name: 'RELMAT'},					
			{name: 'PTOS'},					
			{name: 'TASA_APLICAR'}								
			]
		}),
		groupField: 'NOMBREIF',
		sortInfo:{field: 'NOMBREIF', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarGeneralData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarGeneralData(null, null, null);
				}
			}
		}		
	});
	// -------------------------GRID	
	
	//CREA EL GRID PARA LA OPCION DE VER TASAS 
		var gridVerTasas = {
		xtype: 'grid',		
		store: consultaVERTASASData,
		id: 'gridVerTasas',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		columns: [
		
		{
				header: 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex: 'NOMIFV',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDAV',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'left'	
			},			
			{
				header: 'Tasa Base',
				tooltip: 'Tasa Base',
				dataIndex: 'TASABASEV',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'center'
			},
			
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZOV',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'center'
			},				
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'VALORV',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			},						
			{
				header: 'Rel. Mat.',
				tooltip: 'Rel. Mat.',
				dataIndex: 'RETMATV',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'			
			},			
			{
				header: 'Puntos Adicionales',
				tooltip: 'Puntos Adicionales',
				dataIndex: 'PTOSV',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'			
			},				
			{
				header: 'Tasa a Aplicar',
				tooltip: 'Tasa a Aplicar',
				dataIndex: 'TASA_APLICARV',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			}
		],
		view: new Ext.grid.GroupingView({
			forceFit:false,
  		 groupTextTpl: '{text}'
    }),				
		stripeRows: true,
		loadMask: true,
		height: 350,
		width: 858,
		title: '',
		frame: true			
	};	
	
	
	//CREA EL GRID PARA LA OPCION DE VER TASAS 
		var gridGeneral = new Ext.grid.GridPanel({
		store: consultaGeneralData,
		id: 'gridGeneral',
		hidden: true,
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		columns: [		
			{
				header: 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex: 'NOMBREIF',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'	
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'NOMBREMONEDA',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false, 	
				align: 'left'	
			},						
			{
				header: 'Tasa Base',
				tooltip: 'Tasa Base',
				dataIndex: 'NOMBRETASA',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'center'	
			},			
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'RANGOPLAZO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'center'	
			},				
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			},						
			{
				header: 'Rel. Mat.',
				tooltip: 'Rel. Mat.',
				dataIndex: 'RELMAT',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'			
			},			
			{
				header: 'Puntos Adicionales',
				tooltip: 'Puntos Adicionales',
				dataIndex: 'PTOS',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},				
			{
				header: 'Tasa a Aplicar',
				tooltip: 'Tasa a Aplicar',
				dataIndex: 'TASA_APLICAR',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			}
		],	
			view: new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{text}'					 
        }),
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false		
	});	
	
	// create the Grid HISTORICO
	var gridHistorico = new Ext.grid.GridPanel({
		store: consultaHistoricoData,
		margins: '2 0 0 0',
		hidden: true,
		style: 'margin:0 auto;',
		columns: [
			{
				header: '',
				tooltip: '',
				dataIndex: 'EPO',
				sortable: true,
				width: 10,
				resizable: true,
				hidden: true
			},
			{
				header: '',
				tooltip: '',
				dataIndex: 'FECHA',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},
			{
	       xtype: 'actioncolumn',
				 header: '',
				 tooltip: '',
         width: 130,
				 align: 'center',	
				 renderer: 	function (valor, metadata, registro) {
				 var valor= registro.get('EPO');
					var FECHANTERIOR= registro.get('FECHA');							
					if(valor>0) {  	
						return String.format('<a href="javascript:verTasas(\'{0}\',\'{1}\');" style="color: #0000FF;text-decoration:none;">Ver Tasas</a>',valor,FECHANTERIOR);
					}	
				}						
			}			
		],		
		stripeRows: true,
		loadMask: true,
		height: 300,
		width: 300,
		title: 'Hist�rico de Tasas',
		frame: true			
	});	
	
	//-------------------------------- PRINCIPAL -----------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Tasas',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		style: 'margin:0 auto;',
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'idConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				hidden : true,
				handler: function(boton, evento) {	
					
				var ic_banco_fondeo = Ext.getCmp("ic_banco_fondeo2");
				if (Ext.isEmpty(ic_banco_fondeo.getValue()) && strTipoUsuario != 'NAFIN') {
						ic_banco_fondeo.markInvalid('Por favor, especifique el Banco de Fondeo');
						return;
				}		
		
				var no_epo = Ext.getCmp("cmbEPO");
				if (Ext.isEmpty(no_epo.getValue()) ) {
						no_epo.markInvalid('Por favor, especifique la Epo');
						return;
				}											
				
				fp.el.mask('Enviando...', 'x-mask-loading');		
				var ventana = Ext.getCmp('winVerTasas');
				if (ventana) {					
					ventana.destroy();
				}		
			
					consultaGeneralData.load({					
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'GenerarC' //Generar datos para la consulta							
						})
					});	
					
					consultaHistoricoData.load({					
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar' //Generar datos  Historicos							
						})
					});
					
				}
			},
			{
				text: 'Limpiar',
				id: 'idLimpiar',
				iconCls: 'icoLimpiar',
				hidden : true,
				handler: function() {	
					gridGeneral.hide();
					gridHistorico.hide();				
					fpT.hide();					
					var ventana = Ext.getCmp('winVerTasas');
					if (ventana) {					
					ventana.destroy();
					}		
					fp.getForm().reset();
				}
			}					
		]		
	});
	
	var fpT = new Ext.form.FormPanel({		
			id: 'forma1',
			width: 600,			
			frame: true,		
			titleCollapse: false,
			hidden: true,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',
			html: texto.join('')			
		});
		
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',		
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridGeneral,
			NE.util.getEspaciador(20),
			fpT,
			NE.util.getEspaciador(20),
			gridHistorico	
			
		]
	});

if(strPerfil == 'ADMIN BANCOMEXT' )
	Ext.getCmp('ic_banco_fondeo2').setVisible(false);

catalogoEPOData.load();	
CatalogoIFData.load();

catalogoBancoFondeo.load();
});