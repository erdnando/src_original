Ext.onReady(function() {
	var tipobusqueda = "A";
	var iEstatus = 0;
/*----------------------------------------- Hanlers -----------------------------------------*/
	var renTasa = function(value, metadata, record, rowindex, colindex, store) {
		var valor = "";
		if(record.data.TASAACEPTADA == "N/A")
			valor = 'N/A';
		else{
			valor = Ext.util.Format.number(record.data.TASAACEPTADA,'0.00%');;
		}	
		return valor;		
	}
	
	
	var renMonto = function(value, metadata, record, rowindex, colindex, store) {
		var valor = "";
		var num = record.data.IMPORTERECIBIR;
		if(record.data.IMPORTERECIBIR == "N/A")
			valor = '<center>N/A</center>';
		else{
			valor = Ext.util.Format.number(num,'$0,0.00');
		}	
		
		return valor; 
	}
	
	
	var renInteres = function(value, metadata, record, rowindex, colindex, store) {
		var valor = "";
		if(record.data.IMPORTEINTERES == "N/A")
			return '<center>N/A</center>';
		else{
			valor = Ext.util.Format.number(record.data.IMPORTEINTERES,'$0,0.00'); ;
			return valor; 
		}
	}	
	
		
	function procesaValoresIniciales(opts, success, response){
		//fp.el.mask();
		fp.el.unmask();
		;
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if(jsonValoresIniciales != null){	
				var strNombreUsuario = Ext.getCmp('strNombreUsuario');
				var fechaHora = Ext.getCmp('fechaHora');
				strNombreUsuario.getEl().update(jsonValoresIniciales.strNombreUsuario);
				fechaHora.getEl().update(jsonValoresIniciales.fechaHora);
	
				
				//catalogoEstatus.load();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}
	
	var procesarConsultaData2 = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		grid2.getGridEl().unmask();
		if (arrRegistros != null )	{
			if (!grid2.isVisible()) 	{ 
				grid2.show();
				//esconde todos
				grid7.hide(); 	grid8.hide();	grid28.hide();	grid29.hide();	grid32.hide();	grid40.hide();
			}
			
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF2'); 			
			var el = grid2.getGridEl();
			btnImprimirPDF.enable();									
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();							
				
				if (arrRegistros == '') {
					grid2.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnImprimirPDF.disable();
				}
				
				else if (arrRegistros != '')	{
					consultaDataTotales.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultarTotales'})  });
					btnImprimirPDF.enable();
				}
			}
		}	
	}
	
	var procesarConsultaData7 = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		grid7.getGridEl().unmask();
		
		if (arrRegistros != null )	{
			if (!grid7.isVisible()) 	{ 
				grid7.show();
				//esconde todos
				grid2.hide(); 	grid8.hide();	grid28.hide();	grid29.hide();	grid32.hide();	grid40.hide();
			}
			
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF7'); 
			var el = grid7.getGridEl();
		//	var el = grid2.getGridEl();
			
			btnImprimirPDF.enable();
			
				
			if (store.getTotalCount() > 0 ) 	{	
				if (tipobusqueda == 'N' ){
						consultaDataTotales7Normal.load({ params: 
									Ext.apply(fp.getForm().getValues(), {
										informacion: 'ConsultarTotales',
										TipoBusqueda: 'N'})  });
						gridTotales7Normal.show();					
						gridTotales7Fideicomiso.hide();
						gridTotales.hide();
					}else if(tipobusqueda == 'F'){
						consultaDataTotales7Fideicomiso.load({ params: 
									Ext.apply(fp.getForm().getValues(), {
										informacion: 'ConsultarTotales',
										TipoBusqueda: 'F'})  });		
						gridTotales7Fideicomiso.show();					
						gridTotales7Normal.hide();
						gridTotales.hide();
					}else{
						consultaDataTotales7Normal.load({ params: 
									Ext.apply(fp.getForm().getValues(), {
										informacion: 'ConsultarTotales',
										TipoBusqueda: 'N'})  });					
						consultaDataTotales7Fideicomiso.load({ params: 
									Ext.apply(fp.getForm().getValues(), {
										informacion: 'ConsultarTotales',
										TipoBusqueda: 'F'})  });	
					
						gridTotales.hide();
						gridTotales7Fideicomiso.show();					
						gridTotales7Normal.show();					
					}
	
					//consultaDataTotales7Normal.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultarTotales'})  });
					btnImprimirPDF.enable();	
					el.unmask();	
			}else {
				Ext.getCmp('gridTotales7Fideicomiso').hide();
				Ext.getCmp('gridTotales7Normal').hide();
				Ext.getCmp('gridTotales').hide();
				btnImprimirPDF.disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');			
			}
		}	
	}
	
	var procesarConsultaData8 = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		grid8.getGridEl().unmask();
		if (arrRegistros != null )	{
			if (!grid8.isVisible()) 	{ 
				grid8.show();
				//esconde todos
				grid2.hide(); 	grid7.hide();	grid28.hide();	grid29.hide();	grid32.hide();	grid40.hide();
			}
			
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF8'); 
			var el = grid8.getGridEl();
			btnImprimirPDF.enable();
						
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();			
								
				if (arrRegistros == '') {
					grid8.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnImprimirPDF.disable();
				}
				
				else if (arrRegistros != '')	{
					consultaDataTotales.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultarTotales'})  });
					btnImprimirPDF.enable();
				}
			}
		}	
	}
	
	
	var procesarConsultaData28 = function(store, arrRegistros, opts)  	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		grid28.getGridEl().unmask();
		if (arrRegistros != null )	{
			if (!grid28.isVisible()) 	{ 
				grid28.show();
				//esconde todos
				grid2.hide(); 	grid7.hide();	grid8.hide();	grid29.hide();	grid32.hide();	grid40.hide();
			}
			
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF28'); 
			var el = grid28.getGridEl();
			btnImprimirPDF.enable();
						
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();		
				
				if (arrRegistros == '') {
					grid28.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnImprimirPDF.disable();
				}
				
				else if (arrRegistros != '')	{
					consultaDataTotales.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultarTotales'})  });
					btnImprimirPDF.enable();
				}
			}
		}	
	}
	
	var procesarConsultaData29 = function(store, arrRegistros, opts)  	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		grid29.getGridEl().unmask();
		if (arrRegistros != null )	{
			if (!grid29.isVisible()) 	{ 
				grid29.show();
				//esconde todos
				grid2.hide(); 	grid7.hide();	grid8.hide();	grid28.hide();	grid32.hide();	grid40.hide();
			}
			
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF29'); 
			var el = grid29.getGridEl();
			btnImprimirPDF.enable();
						
			if (store.getTotalCount() >= 0 ) 	{
				el.unmask();			
								
				if (arrRegistros == '') {
					grid29.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnImprimirPDF.disable();
				}
				
				else if (arrRegistros != '')	{
					consultaDataTotales.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultarTotales'})  });
					btnImprimirPDF.enable();
				}
			}
		}	
	}
	
	var procesarConsultaData32 = function(store, arrRegistros, opts)  {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		grid32.getGridEl().unmask();
		if (arrRegistros != null )	{
			if (!grid32.isVisible()) 	{ 
				grid32.show();
				//esconde todos
				grid2.hide(); 	grid7.hide();	grid8.hide();	grid28.hide();	grid29.hide();	grid40.hide();
			}
			
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF32'); 
			var el = grid32.getGridEl();
			btnImprimirPDF.enable();
						
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();			
				
				if (arrRegistros == '') {
					grid32.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnImprimirPDF.disable();
				}
				
				else if (arrRegistros != '')	{
					consultaDataTotales.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultarTotales'})  });
					btnImprimirPDF.enable();
				}
			}
		}	
	}
	
	
	var procesarConsultaData40 = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		grid40.getGridEl().unmask();
		if (arrRegistros != null )	{
			if (!grid40.isVisible()) 	{ 
				grid40.show();
				//esconde todos
				grid2.hide(); 	grid7.hide();	grid8.hide();	grid28.hide();	grid29.hide();	grid32.hide();
			}
			
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF40'); 
			var el = grid40.getGridEl();
			btnImprimirPDF.enable();
						
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();			

				if (arrRegistros == '') {
					el.mask('No se encontr� ning�n registro', 'x-mask');
					btnImprimirPDF.disable();
				}
				
				else if (arrRegistros != '')	{
					consultaDataTotales.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultarTotales'})  });
					btnImprimirPDF.enable();
				}
			}
		}	
	}
	
	var ocultar = function(){
		Ext.getCmp('cfBusqueda').hide();
		Ext.getCmp('radioAmbas').setValue(true);	
		tipoBusqueda = 'A';
	}
	
	var procesarTotalesData = function(store, arrRegistros, opts) 	{
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();
		if (arrRegistros != null) {
			if (!gridTotales.isVisible() && iEstatus != 7 ) {
				gridTotales.show();
			}						
			
			if(store.getTotalCount() >= 0) {	
				el.unmask();	
				var cm = gridTotales.getColumnModel();			
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DOCTO'), false);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DSCTO'), false);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DSCTO2'), true);					
				if(iEstatus == 2 || iEstatus == 7)	{	
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), false);							
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPERAR'), false);
					
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_ANT'), true);							
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('RECURSO_GARANTIA'), true);	
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('PORC_DSCTO'), true);			
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_NUEVO'), true);
				}
				else if(iEstatus == 8 )	{
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_ANT'), false);							
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('RECURSO_GARANTIA'), false);	
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DSCTO2'), false);
					
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DSCTO'), true);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), true);												
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPERAR'), true);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('PORC_DSCTO'), true);			
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_NUEVO'), true);
				}
				else if(iEstatus == 28 )	{
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_ANT'), false);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_NUEVO'), false);
					
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DOCTO'), true);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DSCTO'), true);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), true);												
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPERAR'), true);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('RECURSO_GARANTIA'), true);	
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('PORC_DSCTO'), true);			
				}
				else if(iEstatus == 29 )	{
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), true);												
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPERAR'), true);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_ANT'), true);							
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('RECURSO_GARANTIA'), true);	
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('PORC_DSCTO'), true);			
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_NUEVO'), true);				
				}
				else if(iEstatus == 32 )	{
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), false);		
					
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPERAR'), true);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_ANT'), true);							
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('RECURSO_GARANTIA'), true);	
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('PORC_DSCTO'), true);			
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_NUEVO'), true);				
				}
				else if(iEstatus == 40 )	{
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('PORC_DSCTO'), false);			
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DSCTO2'), false);
					
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DSCTO'), true);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), true);												
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPERAR'), true);
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_ANT'), true);							
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('RECURSO_GARANTIA'), true);	
					gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_NUEVO'), true);	
				}				
				
			} 
			else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
		
	}
	
	function procesarSuccessFailureGenerarPDF(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
/*--------------------------------- STORE'S -------------------------------*/
//-----------------------------------------------------------------------------//

	
	var catalogoEstatus = new Ext.data.JsonStore({
	   id				: 'catEpo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13reporte2ext.data.jsp',
		baseParams	: { informacion: 'CatalogoEstatus'},
		totalProperty : 'total',
		autoLoad		: true,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError 
		}
	});		
	
	
//-----------------------------------------------------------------------------//	
	/****** Store�s Grid�s *******/	
	var consultaData2 = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13reporte2ext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'NOMBREPYME'},
				{name: 'NOMBREEPO'},
				{name: 'NOMBREIF'},
				{name: 'NUMERODOCTO'},
				{name: 'NOMBREMONEDA'},
				
				{name: 'NOMBRE_TIPO_FACTORAJE'},					
				{name: 'MONTODOCTO'},
				{name: 'PORCENTAJE'},					
				{name: 'MONTODSCTO'},
				{name: 'TASAACEPTADA'},
				
				{name: 'IMPORTEINTERES'},					
				{name: 'IMPORTERECIBIR'},
				{name: 'NUMEROACUSE'},					
				{name: 'TIPOFACTORAJE'},
				{name: 'NETORECIBIR'},
				
				{name: 'BENEFICIARIO'},					
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEARECIBIRBENEFICIARIO'},
				{name: 'CLAVEMONEDA'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData2,
				exception: NE.util.mostrarDataProxyError
			}
	});
	
	var consultaData7 = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13reporte2ext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid7'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'NOMBREPYME'},
				{name: 'NOMBREEPO'},
				{name: 'NOMBREIF'},
				{name: 'NUMEROACUSE'},
				{name: 'NUMERODOCTO'},
				
				{name: 'NOMBREMONEDA'},					
				{name: 'CLAVEMONEDA'},
				{name: 'TIPOFACTORAJE'},					
				{name: 'MONTODOCTO'},
				{name: 'BENEFICIARIO'},
				
				{name: 'PORCBENEFICIARIO'},					
				{name: 'IMPORTEARECIBIRBENEFICIARIO'},
				{name: 'PORCENTAJE'},					
				{name: 'MONTODSCTO'},
				{name: 'TASAACEPTADA'},
				
				{name: 'IMPORTEINTERES'},					
				{name: 'IMPORTERECIBIR'},
				{name: 'NETORECIBIR'},
				{name: 'NUMEROSOLICITUD'},
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				
				{name: 'NOMBREFIDEICOMISO'},
				{name: 'IMPORTERECIBIRIF'},
				{name: 'TASAACEPTADAIF'},
				{name: 'IMPORTEINTERESIF'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData7,
				exception: NE.util.mostrarDataProxyError
			}
	});
	var consultaData8 = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13reporte2ext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'NOMBREPYME'},
				{name: 'NOMBREEPO'},
				{name: 'NOMBREIF'},
				{name: 'NUMEROACUSE'},
				{name: 'NUMERODOCTO'},
				
				{name: 'NOMBREMONEDA'},					
				{name: 'CLAVEMONEDA'},
				{name: 'TIPOFACTORAJE'},					
				{name: 'MONTODOCTO'},
				{name: 'BENEFICIARIO'},
				
				{name: 'PORCBENEFICIARIO'},					
				{name: 'IMPORTEARECIBIRBENEFICIARIO'},
				{name: 'PORCENTAJE'},					
				{name: 'MONTODSCTO'},
				{name: 'FECHADOCUMENTO'},
				
				{name: 'FECHAVENCIMIENTO'},					
				{name: 'MONTOANTERIOR'},
				{name: 'CAUSA'},					
				{name: 'FECHAVENCANT'},
				{name: 'NOMBRE_TIPO_FACTORAJE'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData8,
				exception: NE.util.mostrarDataProxyError
			}
	});
	var consultaData28 = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13reporte2ext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'NOMBREEPO'},
				{name: 'NUMEROACUSE'},
				{name: 'NUMERODOCTO'},
				{name: 'NOMBREMONEDA'},
				{name: 'CLAVEMONEDA'},		
				
				{name: 'TIPOFACTORAJE'},					
				{name: 'FECHADOCUMENTO'},
				{name: 'FECHAVENCIMIENTO'},				
				{name: 'MONTOANTERIOR'},					
				{name: 'CAUSA'},
				
				{name: 'MONTONUEVO'},					
				{name: 'NOMBRE_TIPO_FACTORAJE'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData28,
				exception: NE.util.mostrarDataProxyError
			}
	});
	
	var consultaData29 = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13reporte2ext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'NOMBREPYME'},
				{name: 'NOMBREEPO'},
				{name: 'NOMBREIF'},
				{name: 'NUMERODOCTO'},
				
				{name: 'FECHADOCUMENTO'},
				{name: 'FECHAVENCIMIENTO'},					
				{name: 'NOMBREMONEDA'},
				{name: 'NOMBRE_TIPO_FACTORAJE'},		
				
				{name: 'MONTODOCTO'},
				{name: 'PORCENTAJE'},
				{name: 'MONTODSCTO'},
				{name: 'NUMEROACUSE'},
				{name: 'CAUSA'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData29,
				exception: NE.util.mostrarDataProxyError
			}
	});
	
	var consultaData32 = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13reporte2ext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NOMBREIF'},
				{name: 'NUMEROACUSE'},
				{name: 'NUMERODOCTO'},
				
				{name: 'NOMBREMONEDA'},					
				{name: 'CLAVEMONEDA'},
				{name: 'TIPOFACTORAJE'},					
				{name: 'MONTODOCTO'},
				{name: 'BENEFICIARIO'},
				
				
				{name: 'PORCBENEFICIARIO'},					
				{name: 'IMPORTEARECIBIRBENEFICIARIO'},
				{name: 'PORCENTAJE'},					
				{name: 'MONTODSCTO'},
				{name: 'TASAACEPTADA'},
				
				{name: 'IMPORTEINTERES'},					
				{name: 'IMPORTERECIBIR'},
				{name: 'NETORECIBIR'},					
				{name: 'NOMBRE_TIPO_FACTORAJE'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData32,
				exception: NE.util.mostrarDataProxyError
			}
	});
	var consultaData40 = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13reporte2ext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NUMEROACUSE'},
				{name: 'NUMERODOCTO'},
				
				{name: 'NOMBREMONEDA'},					
				{name: 'CLAVEMONEDA'},
				{name: 'TIPOFACTORAJE'},					
				{name: 'MONTODOCTO'},
				{name: 'BENEFICIARIO'},
				
				
				{name: 'PORCBENEFICIARIO'},					
				{name: 'IMPORTE_A_RECIBIR'},
				{name: 'DF_FECHA_VENC_PYME'},					
				{name: 'PORCENTAJE'},
				{name: 'MONTODSCTO'},
				
				{name: 'FECHADOCUMENTO'},					
				{name: 'FECHAVENCIMIENTO'},
				{name: 'MONTOANTERIOR'},					
				{name: 'CAUSA'},
				{name: 'NOMBRE_TIPO_FACTORAJE'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData40,
				exception: NE.util.mostrarDataProxyError
			}
	});
	/****** End Store�s Grid�s *******/
	
	//----**------> Store Totales <-----**-----//

	var consultaDataTotales = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte2ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'		
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'MONTO_DOCTO', type: 'float', mapping: 'MONTO_DOCTO'},			
			{name: 'MONTO_DSCTO', type: 'float', mapping: 'MONTO_DSCTO'},
			{name: 'MONTO_INT', type: 'float', mapping: 'MONTO_INT'},	
			{name: 'MONTO_OPERAR', type: 'float', mapping: 'MONTO_OPERAR'},	
			{name: 'MONTO_ANT', type: 'float', mapping: 'MONTO_ANT'},	
			{name: 'RECURSO_GARANTIA', type: 'float', mapping: 'RECURSO_GARANTIA'},	
			{name: 'PORC_DSCTO', type: 'float', mapping: 'PORC_DSCTO'},
			{name: 'MONTO_NUEVO', type: 'float', mapping: 'MONTO_NUEVO'},
			{name: 'MONTO_DSCTO2', type: 'float', mapping: 'MONTO_DSCTO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}		
	});
	
	var consultaDataTotales7Normal = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte2ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'		
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'MONTO_DOCTO', type: 'float', mapping: 'MONTO_DOCTO'},			
			{name: 'MONTO_DSCTO', type: 'float', mapping: 'MONTO_DSCTO'},
			{name: 'MONTO_INT', type: 'float', mapping: 'MONTO_INT'},	
			{name: 'MONTO_OPERAR', type: 'float', mapping: 'MONTO_OPERAR'},	
			{name: 'MONTO_ANT', type: 'float', mapping: 'MONTO_ANT'},	
			{name: 'RECURSO_GARANTIA', type: 'float', mapping: 'RECURSO_GARANTIA'},	
			{name: 'PORC_DSCTO', type: 'float', mapping: 'PORC_DSCTO'},
			{name: 'MONTO_NUEVO', type: 'float', mapping: 'MONTO_NUEVO'},
			{name: 'MONTO_DSCTO2', type: 'float', mapping: 'MONTO_DSCTO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}		
	});	
	
	var consultaDataTotales7Fideicomiso = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte2ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'		
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'MONTO_DOCTO', type: 'float', mapping: 'MONTO_DOCTO'},			
			{name: 'MONTO_DSCTO', type: 'float', mapping: 'MONTO_DSCTO'},
			{name: 'MONTO_INT', type: 'float', mapping: 'MONTO_INT'},	
			{name: 'MONTO_OPERAR', type: 'float', mapping: 'MONTO_OPERAR'},	
			{name: 'MONTO_INT_FIDE', type: 'float', mapping: 'MONTO_INT_FIDE'},	
			{name: 'MONTO_OPERAR_FIDE', type: 'float', mapping: 'MONTO_OPERAR_FIDE'},			
			{name: 'MONTO_ANT', type: 'float', mapping: 'MONTO_ANT'},	
			{name: 'RECURSO_GARANTIA', type: 'float', mapping: 'RECURSO_GARANTIA'},	
			{name: 'PORC_DSCTO', type: 'float', mapping: 'PORC_DSCTO'},
			{name: 'MONTO_NUEVO', type: 'float', mapping: 'MONTO_NUEVO'},
			{name: 'MONTO_DSCTO2', type: 'float', mapping: 'MONTO_DSCTO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}		
	});	
	
	
	
	/*------------------------------------------ Grid's ------------------------------------------*/

	var grid2 = new Ext.grid.GridPanel
	({
		title:'Seleccionada PYME a Negociable', store: consultaData2, margins: '20 0 0 0',	stripeRows: true,	loadMask:false, /*autoHeight:true,*/ height:250, width:900, frame:true, hidden:true, header:true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',align:'center',
		columns: [			
			{	header:'Nombre PYME', tooltip: 'Nombre PYME', dataIndex:'NOMBREPYME',	sortable:true, resizable:true, width:180, align:'left'	},
			{	header:'Nombre EPO',  tooltip:'Nombre EPO', dataIndex:'NOMBREEPO',	sortable:true,	resizable:true, width:180,	align : 'left',
					renderer:function(value, metadata, record, rowindex, colindex, store) 
					{	metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"'; return value;	}
			},
			{	header:'Nombre IF', tooltip: 'Nombre IF',	dataIndex:'NOMBREIF', sortable:true, width:150, align:'left'	},
			{	header:'N�mero de Documento', tooltip:'N�mero de Documento', dataIndex:'NUMERODOCTO', sortable:true, width:100, align:'center' },
			{	header:'Moneda',	tooltip:'Moneda',	dataIndex:'NOMBREMONEDA',	sortable:true,	width:120,	align:'center'/*renderer:Ext.util.Format.dateRenderer('d/m/Y')*/	},
			{	header:'Tipo Factoraje', tooltip:'Tipo Factoraje', dataIndex:'NOMBRE_TIPO_FACTORAJE', sortable:true,	width:90,	align:'center'	},
			{	header:'Monto Documento', tooltip:'Monto Documento',	dataIndex:'MONTODOCTO',	sortable:true,	width:100, align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')  },
			{	header:'Porcentaje de Descuento', tooltip:'Porcentaje de Descuento',	dataIndex:'PORCENTAJE', sortable:true,	width:80, align:'center',renderer: Ext.util.Format.numberRenderer('0%') 	},
			{	header:'Recurso en Garant�a',	tooltip:'Recurso en Garant�a', dataIndex:'', sortable:true, width:90,	align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('MONTODOCTO') != 0 || record.get('MONTODOCTO')!='' ) { 
						var dblRecurso = '';
						if(rowindex == 0)
							dblRecurso = record.data.MONTODOCTO; 
						else {
							var reg = store.getAt(rowindex-1);
							var dMontoDocto = record.data.MONTODOCTO; 
							var dMontoDscto = reg.get('MONTODSCTO');
							dblRecurso = dMontoDocto - dMontoDscto;
						}
						return '$ '+ Ext.util.Format.number(dblRecurso,'0,000.00');
					}
					else{	return '';}	
				}
			},
			{	header:'Monto a Descontar', tooltip:'Monto a Descontar', dataIndex:'MONTODSCTO',	sortable:true,	width:90,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header:'Tasa',	tooltip:'Tasa', dataIndex:'TASAACEPTADA', sortable:true, width:80,	align:'center',renderer: Ext.util.Format.numberRenderer('0.00%') },
			{	header:'Monto int.',	tooltip:'Monto int.', dataIndex:'IMPORTEINTERES', sortable:true, width:80,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header:'Monto a Operar', tooltip:'Monto a Operar',	dataIndex:'IMPORTERECIBIR', sortable:true, width:80, align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header:'N�mero Acuse IF', tooltip:'N�mero Acuse IF',	dataIndex:'NUMEROACUSE', sortable:true,	width:100,	align:'center' },
			
			{	header:'Neto a Recibir PYME', tooltip:'Neto a Recibir PYME', dataIndex:'NETORECIBIR', sortable:true, width:80, align:'right', 
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('NETORECIBIR');
						return '$ '+ Ext.util.Format.number(value,'0,000.00');
					}
					else{	return '';}	
				}
			},
			{	header:'Beneficiario', tooltip:'Beneficiario', dataIndex:'BENEFICIARIO', sortable:true, width:150, align:'center'	,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('BENEFICIARIO');
						return value;	
					}
					else{	return '';}	
				}
			},
			{	header:'% Beneficiario', tooltip:'% Beneficiario',	dataIndex:'',	sortable:true,	width:80, align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('PORCBENEFICIARIO');
						return Ext.util.Format.number(value,'0%');
					}
					else{	return '';}	
				}
			},
			{	header:'Importe a Recibir Beneficiario', tooltip:'Importe a Recibir Beneficiario',	dataIndex:'',	sortable:true,	width:80, align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('IMPORTEARECIBIRBENEFICIARIO');
						return '$ '+ Ext.util.Format.number(value,'0,000.00');						
					}
					else{	return '';}	
				}
			}
		],
		
		bbar: {
			items: [
				'->',	'-',
				{	
					xtype: 'button',	text: 'Imprimir PDF',
					id: 'btnImprimirPDF2',
					handler: function(boton, evento) {
					
						Ext.Ajax.request({ url: '13reporte2ext.data.jsp', params: Ext.apply (fp.getForm().getValues(),{	informacion: 'GeneraPDF' /* parametro que indique que tipo de consulta es!!!*/	}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}				
				
		]}
	});

	
	var gruposConsulta = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 10, align: 'center'},
				{	header: 'Intermediario Financiero', colspan: 4, align: 'center'},	
				{	header: 'Fideicomiso ', colspan: 4, align: 'center'},
				{	header: ' ', colspan: 5, align: 'center'}
			]
		]
	});	
	
	
	var grid7 = new Ext.grid.GridPanel
	({
		title:'Operada - Operada Pendiente de pago', store: consultaData7, margins: '20 0 0 0',	stripeRows: true,	loadMask:false, height:250,	width:900, frame:true, hidden:true, header:true,
		plugins: gruposConsulta,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		columns: [			
			{	header:'Nombre EPO',  tooltip:'Nombre EPO', dataIndex:'NOMBREPYME',	sortable:true,	resizable:true, width:180,	align : 'left',
					renderer:function(value, metadata, record, rowindex, colindex, store) 
					{	metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"'; return value;	}
			},
			{	header:'Nombre PYME', tooltip: 'Nombre PYME', dataIndex:'NOMBREEPO',	sortable:true, resizable:true, width:180, align:'left'	},
			{	header:'N�mero Solicitud', tooltip:'N�mero Solicitud', dataIndex:'NUMEROSOLICITUD', sortable:true, width:100, align:'center' },
			{	header:'N�mero de Documento', tooltip:'N�mero de Documento', dataIndex:'NUMERODOCTO', sortable:true, width:100, align:'center' },
			
			{	header:'Moneda',	tooltip:'Moneda',	dataIndex:'NOMBREMONEDA',	sortable:true,	width:120,	align:'center'/*renderer:Ext.util.Format.dateRenderer('d/m/Y')*/	},
			{	header:'Tipo Factoraje', tooltip:'Tipo Factoraje', dataIndex:'NOMBRE_TIPO_FACTORAJE', sortable:true,	width:90,	align:'center'	},
			{	header:'Monto Documento', tooltip:'Monto Documento',	dataIndex:'MONTODOCTO',	sortable:true,	width:100, align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')  },
			{	header:'Porcentaje de Descuento', tooltip:'Porcentaje de Descuento',	dataIndex:'PORCENTAJE', sortable:true,	width:80, align:'center',renderer: Ext.util.Format.numberRenderer('0.00%') 	},
			
			{	header:'Recurso en Garant�a',	tooltip:'Recurso en Garant�a', dataIndex:'', sortable:true, width:90,	align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('MONTODOCTO') != 0 || record.get('MONTODOCTO')!='' ) { 
						var dblRecurso = '';
						if(rowindex == 0)
							dblRecurso = record.data.MONTODOCTO; 
						else {
							var reg = store.getAt(rowindex-1);
							var dMontoDocto = record.data.MONTODOCTO; 
							var dMontoDscto = reg.get('MONTODSCTO');
							dblRecurso = dMontoDocto - dMontoDscto;
						}
						return '$ '+ Ext.util.Format.number(dblRecurso,'0,000.00');
					}
					else{	return '';}	
				}
			},
			{	header:'Monto a Descontar', tooltip:'Monto a Descontar', dataIndex:'MONTODSCTO',	sortable:true,	width:90,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			
			{	header:'Nombre', tooltip: 'Nombre',	dataIndex:'NOMBREIF', sortable:true, width:150, align:'left'	},					
			{	header:'Tasa',	tooltip:'Tasa', dataIndex:'TASAACEPTADAIF', sortable:true, width:80,	align:'center',renderer: Ext.util.Format.numberRenderer('0.00%') },
			{	header:'Monto int.',	tooltip:'Monto int.', dataIndex:'IMPORTEINTERESIF', sortable:true, width:80,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header:'Monto a Operar', tooltip:'Monto a Operar',	dataIndex:'IMPORTERECIBIRIF', sortable:true, width:80, align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			
			{	header:'Nombre', tooltip: 'Nombre',	dataIndex:'NOMBREFIDEICOMISO', sortable:true, width:150, align:'left'	},					
			{	header:'Tasa',	tooltip:'Tasa', dataIndex:'TASAACEPTADA', sortable:true, width:80,	align:'center',renderer: renTasa },
			{	header:'Monto int.',	tooltip:'Monto int.', dataIndex:'IMPORTEINTERES', sortable:true, width:80,	align:'right',renderer: renInteres },
			{	header:'Monto a Operar', tooltip:'Monto a Operar',	dataIndex:'IMPORTERECIBIR', sortable:true, width:80, align:'right',renderer: renMonto },
						
			
			{	header:'N�mero Acuse IF', tooltip:'N�mero Acuse IF',	dataIndex:'NUMEROACUSE', sortable:true,	width:100,	align:'center' },
			
			{	header:'Neto a Recibir PYME', tooltip:'Neto a Recibir PYME', dataIndex:'NETORECIBIR', sortable:true, width:80, align:'right', 
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('NETORECIBIR');
						return '$ '+ Ext.util.Format.number(value,'0,000.00');
					}
					else{	return '';}	
				}
			},
			{	header:'Beneficiario', tooltip:'Beneficiario', dataIndex:'BENEFICIARIO', sortable:true, width:150, align:'center'	,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('BENEFICIARIO');
						return value;	
					}
					else{	return '';}	
				}
			},
			{	header:'% Beneficiario', tooltip:'% Beneficiario',	dataIndex:'',	sortable:true,	width:80, align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('PORCBENEFICIARIO');
						return Ext.util.Format.number(value,'0%');
					}
					else{	return '';}	
				}
			},
			{	header:'Importe a Recibir Beneficiario', tooltip:'Importe a Recibir Beneficiario',	dataIndex:'',	sortable:true,	width:80, align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('IMPORTEARECIBIRBENEFICIARIO');
						return '$ '+ Ext.util.Format.number(value,'0,000.00');						
					}
					else{	return '';}	
				}
			}
		],
		
		bbar: {
			items: [
				'->',	'-',
				{	
					xtype: 'button',	text: 'Imprimir PDF',
					id: 'btnImprimirPDF7',
					handler: function(boton, evento) {
						
						var TipoBusqueda = 'A';						
						if(Ext.getCmp('radioNormal').getValue()==true)  {  TipoBusqueda = 'N';  }
						if(Ext.getCmp('radioFideicomiso').getValue()==true)  {  TipoBusqueda = 'F';  }
						if(Ext.getCmp('radioAmbas').getValue()==true)  {  TipoBusqueda = 'A';  }
						
						Ext.Ajax.request({
							url: '13reporte2ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GeneraPDF',
							TipoBusqueda:TipoBusqueda
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
						
					}
				}
				
		]}
	});

	var grid8 = new Ext.grid.GridPanel
	({
		title:'Modificaci�n de Montos y/o Fechas de Vencimiento', store: consultaData8, margins: '20 0 0 0',	stripeRows: true,	loadMask:false,  height:250,	width:900,  frame:true, hidden:true, header:true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		columns: [			
			{	header:'Nombre EPO',  tooltip:'Nombre EPO', dataIndex:'NOMBREEPO',	sortable:true,	resizable:true, width:180,	align : 'left',
					renderer:function(value, metadata, record, rowindex, colindex, store) 
					{	metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"'; return value;	}
			},
			{	header:'Nombre PYME', tooltip: 'Nombre PYME', dataIndex:'NOMBREPYME',	sortable:true, resizable:true, width:180, align:'left'	},
			{	header:'Nombre IF', tooltip: 'Nombre IF',	dataIndex:'NOMBREIF', sortable:true, width:150, align:'left'	},
			{	header:'N�mero de Documento', tooltip:'N�mero de Documento', dataIndex:'NUMERODOCTO', sortable:true, width:100, align:'center' },
			{	header:'Fecha Documento', tooltip:'Fecha Documento', dataIndex:'FECHADOCUMENTO', sortable:true, width:100, align:'center' },
			
			{	header:'Fecha Vencimiento', tooltip:'Fecha Vencimiento', dataIndex:'FECHAVENCIMIENTO', sortable:true, width:100, align:'center' },
			{	header:'Moneda',	tooltip:'Moneda',	dataIndex:'NOMBREMONEDA',	sortable:true,	width:120,	align:'center'/*renderer:Ext.util.Format.dateRenderer('d/m/Y')*/	},
			{	header:'Tipo Factoraje', tooltip:'Tipo Factoraje', dataIndex:'NOMBRE_TIPO_FACTORAJE', sortable:true,	width:90,	align:'center'	},
			{	header:'Monto', tooltip:'Monto',	dataIndex:'MONTODOCTO',	sortable:true,	width:90, align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')  },
			{	header:'Monto Anterior', tooltip:'Monto Anterior',	dataIndex:'MONTOANTERIOR',	sortable:true,	width:90, align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')  },
			
			{	header:'Fecha de Vencimiento Anterior', tooltip:'Fecha de Vencimiento Anterior',	dataIndex:'FECHAVENCANT',	sortable:true,	width:100, align:'center'  },
			{	header:'Porcentaje de Descuento', tooltip:'Porcentaje de Descuento',	dataIndex:'PORCENTAJE', sortable:true,	width:80, align:'center',renderer: Ext.util.Format.numberRenderer('0.00%') 	},
			{	header:'Recurso en Garant�a',	tooltip:'Recurso en Garant�a', dataIndex:'', sortable:true, width:90,	align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('MONTODOCTO') != 0 || record.get('MONTODOCTO')!='' ) { 
						var dblRecurso = '';
						if(rowindex == 0)
							dblRecurso = record.data.MONTODOCTO; 
						else {
							var reg = store.getAt(rowindex-1);
							var dMontoDocto = record.data.MONTODOCTO; 
							var dMontoDscto = reg.get('MONTODSCTO');
							dblRecurso = dMontoDocto - dMontoDscto;
						}
						return '$ '+ Ext.util.Format.number(dblRecurso,'0,000.00');
					}
					else{	return '';}	
				}
			},
			{	header:'Monto a Descontar', tooltip:'Monto a Descontar', dataIndex:'MONTODSCTO',	sortable:true,	width:90,	
				align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					var montoDesc = record.get('MONTODSCTO')
					if (record.get('TIPOFACTORAJE') == 'V' || record.get('TIPOFACTORAJE')=='D'   || record.get('TIPOFACTORAJE')=='I') { 
						 montoDesc = record.get('MONTODOCTO')
					}
					return '$ '+ Ext.util.Format.number(montoDesc,'0,000.00');
				}
			},
				
			{	header:'N�mero de Acuse', tooltip:'N�mero de Acuse',	dataIndex:'NUMEROACUSE', sortable:true,	width:100,	align:'center' },
			
			{	header:'Causa',	tooltip:'Causa', dataIndex:'CAUSA', sortable:true, width:150,	align:'center' },
			{	header:'Beneficiario', tooltip:'Beneficiario', dataIndex:'BENEFICIARIO', sortable:true, width:150, align:'center'	,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('BENEFICIARIO');
						return value;	
					}
					else{	return '';}	
				}
			},
			{	header:'% Beneficiario', tooltip:'% Beneficiario',	dataIndex:'',	sortable:true,	width:100, align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('PORCBENEFICIARIO');
						return Ext.util.Format.number(value,'0%');
					}
					else{	return '';}	
				}
			},
			{	header:'Importe a Recibir Beneficiario', tooltip:'Importe a Recibir Beneficiario',	dataIndex:'',	sortable:true,	width:80, align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' || record.get('TIPOFACTORAJE')=='I' ) { 
						value = record.get('IMPORTEARECIBIRBENEFICIARIO');
						return '$ '+ Ext.util.Format.number(value,'0,000.00');						
					}
					else{	return '';}	
				}
			}	
		],
		
		bbar: {
			items: [
				'->',	'-',
				{	
					xtype: 'button',	text: 'Imprimir PDF',
					id: 'btnImprimirPDF8',
					handler: function(boton, evento) {
															
						Ext.Ajax.request({ url: '13reporte2ext.data.jsp', params: Ext.apply (fp.getForm().getValues(),{	informacion: 'GeneraPDF' /* parametro que indique que tipo de consulta es!!!*/	}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}
				
		]}
	});

	var grid28 = new Ext.grid.GridPanel
	({
		title:'Aplicaci�n de Nota de Cr�dito', store: consultaData28, margins: '20 0 0 0',	stripeRows: true,	loadMask:false, height:250,	width:900, frame:true, hidden:true, header:true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		columns: [			
			{	header:'Nombre de la EPO',  tooltip:'Nombre de la EPO', dataIndex:'NOMBREEPO',	sortable:true,	resizable:true, width:250,	align : 'left'	},
			{	header:'N�mero Documento', tooltip:'N�mero Documento', dataIndex:'NUMERODOCTO', sortable:true, width:100, align:'center' },
			{	header:'N�mero de Acuse', tooltip:'N�mero de Acuse',	dataIndex:'NUMEROACUSE', sortable:true,	width:100,	align:'center' },
			{	header:'Fecha Emisi�n',	tooltip:'Fecha Emisi�n', dataIndex:'FECHADOCUMENTO', sortable:true, width:100,	align:'center' },
			{	header:'Fecha Vencimiento',	tooltip:'Fecha Vencimiento', dataIndex:'FECHAVENCIMIENTO', sortable:true, width:100,	align:'center' },			
			{	header:'Tipo Factoraje', tooltip:'Tipo Factoraje', dataIndex:'NOMBRE_TIPO_FACTORAJE', sortable:true,	width:100,	align:'center'	},
			{	header:'Moneda',	tooltip:'Moneda',	dataIndex:'NOMBREMONEDA',	sortable:true,	width:120,	align:'center'/*renderer:Ext.util.Format.dateRenderer('d/m/Y')*/	},
			{	header:'Monto Anterior',	tooltip:'Monto Anterior', dataIndex:'MONTOANTERIOR', sortable:true, width:80,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header:'Monto Nuevo',	tooltip:'Monto Nuevo', dataIndex:'MONTONUEVO', sortable:true, width:80,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header:'Causa',	tooltip:'Causa', dataIndex:'CAUSA', sortable:true, width:150,	align:'center' }
		],
		
		bbar: {
			items: [
				'->',	'-',
				{	
					xtype: 'button',	text: 'Imprimir PDF',
					id: 'btnImprimirPDF28',
					handler: function(boton, evento) {
																
						Ext.Ajax.request({ url: '13reporte2ext.data.jsp', params: Ext.apply (fp.getForm().getValues(),{	informacion: 'GeneraPDF' /* parametro que indique que tipo de consulta es!!!*/	}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}			
				
		]}
	});

	
	var grid29 = new Ext.grid.GridPanel
	({
		title:'Programado a Negociable', store: consultaData29, margins: '20 0 0 0',	stripeRows: true,	loadMask:false, height:250,	width:900, frame:true, hidden:true, header:true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		columns: [			
			{	header:'Nombre PYME', tooltip: 'Nombre PYME', dataIndex:'NOMBREPYME',	sortable:true, resizable:true, width:180, align:'left'	},
			{	header:'Nombre EPO',  tooltip:'Nombre EPO', dataIndex:'NOMBREEPO',	sortable:true,	resizable:true, width:180,	align : 'left' },
			{	header:'Nombre IF', tooltip: 'Nombre IF',	dataIndex:'NOMBREIF', sortable:true, width:150, align:'left'	},
			{	header:'N�mero de Documento', tooltip:'N�mero de Documento', dataIndex:'NUMERODOCTO', sortable:true, width:100, align:'center' },
			{	header:'Fecha Emisi�n', tooltip:'Fecha Emisi�n', dataIndex:'FECHADOCUMENTO', sortable:true, width:100, align:'center' },
			{	header:'Fecha Vencimiento', tooltip:'Fecha Vencimiento', dataIndex:'FECHAVENCIMIENTO', sortable:true, width:100, align:'center' },
			
			{	header:'Moneda',	tooltip:'Moneda',	dataIndex:'NOMBREMONEDA',	sortable:true,	width:120,	align:'center'/*renderer:Ext.util.Format.dateRenderer('d/m/Y')*/	},
			{	header:'Tipo Factoraje', tooltip:'Tipo Factoraje', dataIndex:'NOMBRE_TIPO_FACTORAJE',sortable:true,	width:90,	align:'center'	},
			{	header:'Monto Documento', tooltip:'Monto Documento',	dataIndex:'MONTODOCTO',	sortable:true,	width:90, align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')  },
			{	header:'Porcentaje de Descuento', tooltip:'Porcentaje de Descuento',	dataIndex:'PORCENTAJE',	sortable:true,	width:80, align:'center',renderer: Ext.util.Format.numberRenderer('0%') },
			{	header:'Monto a Descontar',	tooltip:'Monto a Descontar', dataIndex:'MONTODSCTO', sortable:true, width:90,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header:'N�mero Acuse IF', tooltip:'N�mero Acuse IF',	dataIndex:'NUMEROACUSE', sortable:true,	width:100,	align:'center' },
			{	header:'Causa', tooltip:'Causa', dataIndex:'CAUSA', sortable:true, width:150, align:'center' }
		],
		
		bbar: {
			items: [
				'->',	'-',
				{	
					xtype: 'button',	text: 'Imprimir PDF',
					id: 'btnImprimirPDF29',
					handler: function(boton, evento) {
						
						Ext.Ajax.request({ url: '13reporte2ext.data.jsp', params: Ext.apply (fp.getForm().getValues(),{	informacion: 'GeneraPDF' /* parametro que indique que tipo de consulta es!!!*/	}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}
				
		]}
	});

	var grid32 = new Ext.grid.GridPanel
	({
		title:'Programado Pyme a Seleccionado Pyme', store: consultaData32, margins: '20 0 0 0',	stripeRows: true,	loadMask:false, height:250,	width:900, frame:true, hidden:true, header:true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		columns: [			
			{	header:'Nombre PYME', tooltip: 'Nombre PYME', dataIndex:'NOMBREPYME',	sortable:true, resizable:true, width:180, align:'left'	},
			{	header:'Nombre EPO',  tooltip:'Nombre EPO', dataIndex:'NOMBREEPO',	sortable:true,	resizable:true, width:180,	align : 'left' },
			{	header:'Nombre IF', tooltip: 'Nombre IF',	dataIndex:'NOMBREIF', sortable:true, width:150, align:'left'	},
			{	header:'N�mero de Documento', tooltip:'N�mero de Documento', dataIndex:'NUMERODOCTO', sortable:true, width:100, align:'center' },
			{	header:'Moneda',	tooltip:'Moneda',	dataIndex:'NOMBREMONEDA',	sortable:true,	width:120,	align:'center'/*renderer:Ext.util.Format.dateRenderer('d/m/Y')*/	},
			{	header:'Tipo Factoraje', tooltip:'Tipo Factoraje', dataIndex:'NOMBRE_TIPO_FACTORAJE',sortable:true,	width:90,	align:'center'	},
			{	header:'Monto Documento', tooltip:'Monto Documento',	dataIndex:'MONTODOCTO',	sortable:true,	width:90, align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')  },
			{	header:'Porcentaje de Descuento', tooltip:'Porcentaje de Descuento',	dataIndex:'PORCENTAJE',	sortable:true,	width:80, align:'center',renderer: Ext.util.Format.numberRenderer('0.00%') },
			{	header:'Recurso en Garant�a',	tooltip:'Recurso en Garant�a', dataIndex:'', sortable:true, width:90,	align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('MONTODOCTO') != 0 || record.get('MONTODOCTO')!='' ) { 
						var dblRecurso = '';
						if(rowindex == 0)
							dblRecurso = record.data.MONTODOCTO; 
						else {
							var reg = store.getAt(rowindex-1);
							var dMontoDocto = record.data.MONTODOCTO; 
							var dMontoDscto = reg.get('MONTODSCTO');
							dblRecurso = dMontoDocto - dMontoDscto;
						}
						return '$ '+ Ext.util.Format.number(dblRecurso,'0,000.00');
					}
					else{	return '';}	
				}
			},
			
			{	header:'Monto a Descontar', tooltip:'Monto a Descontar', dataIndex:'MONTODSCTO',	sortable:true,	width:90,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header:'Tasa',	tooltip:'Tasa', dataIndex:'TASAACEPTADA', sortable:true, width:80,	align:'center',renderer: Ext.util.Format.numberRenderer('0.00%') },
			{	header:'Monto int.',	tooltip:'Monto int.', dataIndex:'IMPORTEINTERES', sortable:true, width:80,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header:'Neto a Recibir PYME', tooltip:'Neto a Recibir PYME', dataIndex:'NETORECIBIR', sortable:true, width:80, align:'right', 
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D') { 
						value = record.get('NETORECIBIR');
						return '$ '+ Ext.util.Format.number(value,'0,000.00');
					}
					else{	return '';}	
				}
			},
			{	header:'Beneficiario', tooltip:'Beneficiario', dataIndex:'BENEFICIARIO', sortable:true, width:150, align:'center'	,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' ) { 
						value = record.get('BENEFICIARIO');
						return value;	
					}
					else{	return '';}	
				}
			},
			{	header:'% Beneficiario', tooltip:'% Beneficiario',	dataIndex:'',	sortable:true,	width:80, align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D') { 
						value = record.get('PORCBENEFICIARIO');
						return Ext.util.Format.number(value,'0%');
					}
					else{	return '';}	
				}
			},
			{	header:'Importe a Recibir Beneficiario', tooltip:'Importe a Recibir Beneficiario',	dataIndex:'',	sortable:true,	width:80, align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' ) { 
						value = record.get('IMPORTEARECIBIRBENEFICIARIO');
						return '$ '+ Ext.util.Format.number(value,'0,000.00');						
					}
					else{	return '';}	
				}
			}
		],
		
		bbar: {
			items: [
				'->',	'-',
				{	
					xtype: 'button',	text: 'Imprimir PDF',
					id: 'btnImprimirPDF32',
					handler: function(boton, evento) {
						
						Ext.Ajax.request({ url: '13reporte2ext.data.jsp', params: Ext.apply (fp.getForm().getValues(),{	informacion: 'GeneraPDF' /* parametro que indique que tipo de consulta es!!!*/	}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}
				
		]}
	});
	
	
	/////////////////////////////////////////////////////////////////
	var grid40 = new Ext.grid.GridPanel
	({
		title:'Pre Negociable a Negociable', store: consultaData40, margins: '20 0 0 0',	stripeRows: true,	loadMask:false, height:250,	width:900, frame:true, hidden:true, header:true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		columns: [			
			{	header:'Nombre PYME', tooltip: 'Nombre PYME', dataIndex:'NOMBREPYME',	sortable:true, resizable:true, width:180, align:'left'	},
			{	header:'Nombre EPO',  tooltip:'Nombre EPO', dataIndex:'NOMBREEPO',	sortable:true,	resizable:true, width:180,	align : 'left'	},
			
			{	header:'Nombre IF', tooltip: 'Nombre IF',	dataIndex:'', sortable:true, width:90, align:'left'	},
			{	header:'Fecha Emisi�n',	tooltip:'Fecha Emisi�n',	dataIndex:'FECHADOCUMENTO',	sortable:true,	width:100,	align:'center'/*renderer:Ext.util.Format.dateRenderer('d/m/Y')*/	},
			{	header:'Fecha Vencimiento',	tooltip:'Fecha Vencimiento',	dataIndex:'FECHAVENCIMIENTO',	sortable:true,	width:100,	align:'center'/*renderer:Ext.util.Format.dateRenderer('d/m/Y')*/	},
			{	header:'N�mero Documento', tooltip:'N�mero Documento', dataIndex:'NUMERODOCTO', sortable:true, width:100, align:'center' },
			{	header:'Moneda',	tooltip:'Moneda',	dataIndex:'NOMBREMONEDA',	sortable:true,	width:120,	align:'center'/*renderer:Ext.util.Format.dateRenderer('d/m/Y')*/	},
			
			{	header:'Tipo Factoraje', tooltip:'Tipo Factoraje', dataIndex:'NOMBRE_TIPO_FACTORAJE', sortable:true,	width:100,	align:'center'	},
			{	header:'Monto Documento', tooltip:'Monto Documento',	dataIndex:'MONTODOCTO',	sortable:true,	width:100, align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')  },
			{	header:'Porcentaje de Descuento', tooltip:'Porcentaje de Descuento',	dataIndex:'PORCENTAJE', sortable:true,	width:80, align:'center',renderer: Ext.util.Format.numberRenderer('0.00%') 	},
			
			{	header:'Monto a Descontar', tooltip:'Monto a Descontar', dataIndex:'MONTODSCTO',	sortable:true,	width:90,	align:'right',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			
			{	header:'Beneficiario', tooltip:'Beneficiario', dataIndex:'BENEFICIARIO', sortable:true, width:150, align:'center'	,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D' ) { 
						value = record.get('BENEFICIARIO');
						return value;	
					}
					else{	return '';}	
				}
			},
			{	header:'% Beneficiario', tooltip:'% Beneficiario',	dataIndex:'',	sortable:true,	width:80, align:'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D'  ) { 
						value = record.get('PORCBENEFICIARIO');
						return Ext.util.Format.number(value,'0%');
					}
					else{	return '';}	
				}
			},
			{	header:'Importe a Recibir Beneficiario', tooltip:'Importe a Recibir Beneficiario',	dataIndex:'',	sortable:true,	width:80, align:'right',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					if (record.get('TIPOFACTORAJE') == 'D') { 
						value = record.get('IMPORTEARECIBIRBENEFICIARIO');
						return '$ '+ Ext.util.Format.number(value,'0,000.00');						
					}
					else{	return '';}	
				}
			}
		],
		
		bbar: {
			items: [
				'->',	'-',
				{	
					xtype: 'button',	text: 'Imprimir PDF',
					id: 'btnImprimirPDF40',
					handler: function(boton, evento) {
						
						Ext.Ajax.request({ url: '13reporte2ext.data.jsp', params: Ext.apply (fp.getForm().getValues(),{	informacion: 'GeneraPDF' /* parametro que indique que tipo de consulta es!!!*/	}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}
		]}
	});
	
	//-------------------------------------------------------------------------------
		
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales', store: consultaDataTotales,	style: 'margin:0 auto;', 
		title:'', hidden: true,height: 100,width: 900, frame: false,		
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		columns: [	
			
			// ------------------------------------------------------------------------------------------------------------------------------------ //
			{ 	header: 'MONEDA', dataIndex: 'MONEDA', width: 150, align: 'left' },		
			{	header: 'Monto Documento', dataIndex: 'MONTO_DOCTO', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto a Descontar', dataIndex: 'MONTO_DSCTO', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{	header: 'Monto Int.', dataIndex: 'MONTO_INT', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto a Operar', dataIndex: 'MONTO_OPERAR', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto Anterior', dataIndex: 'MONTO_ANT', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto Nuevo', dataIndex: 'MONTO_NUEVO', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Recurso en Garant�a', dataIndex: 'RECURSO_GARANTIA', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Porcentaje de Descuento', dataIndex: 'PORC_DSCTO', width: 150, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')	},
			{	header: 'Monto a Descontar', dataIndex: 'MONTO_DSCTO2', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00')}
			
		]
	});
	
		
	var gridTotales7Normal = new Ext.grid.GridPanel({
		id: 'gridTotales7Normal', store: consultaDataTotales7Normal,	style: 'margin:0 auto;', title:'', hidden: true,height: 115,width: 900,title: 'Totales Operaci�n Normal', frame: false,		
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		columns: [	
			
			// ------------------------------------------------------------------------------------------------------------------------------------ //
			{ 	header: 'MONEDA', dataIndex: 'MONEDA', width: 150, align: 'left' },		
			{	header: 'Monto Documento', dataIndex: 'MONTO_DOCTO', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto a Descontar', dataIndex: 'MONTO_DSCTO', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{	header: 'Monto Int.', dataIndex: 'MONTO_INT', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto a Operar', dataIndex: 'MONTO_OPERAR', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto Anterior', dataIndex: 'MONTO_ANT', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto Nuevo', dataIndex: 'MONTO_NUEVO', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Recurso en Garant�a', dataIndex: 'RECURSO_GARANTIA', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Porcentaje de Descuento', dataIndex: 'PORC_DSCTO', width: 150, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')	},
			{	header: 'Monto a Descontar', dataIndex: 'MONTO_DSCTO2', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00')}
			
		]
	});
	
	
	var grupo = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 3, align: 'center'},
					{header: 'Intermediario Financiero', colspan: 2, align: 'center'},
					{header: 'Fideicomiso', colspan: 2, align: 'center'},
					{header: '', colspan: 5, align: 'center'}
				]
			]
		});
	
	var gridTotales7Fideicomiso = new Ext.grid.GridPanel({
		id: 'gridTotales7Fideicomiso', store: consultaDataTotales7Fideicomiso,	
		plugins: grupo,
		style: 'margin:0 auto;', title:'Totales Operaci�n Fideicomiso', hidden: true,height: 130,width: 900, frame: false,		
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		columns: [	
			
			// ------------------------------------------------------------------------------------------------------------------------------------ //
			{ 	header: 'MONEDA', dataIndex: 'MONEDA', width: 150, align: 'left' },		
			{	header: 'Monto Documento', dataIndex: 'MONTO_DOCTO', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto a Descontar', dataIndex: 'MONTO_DSCTO', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{	header: 'Monto Int.', dataIndex: 'MONTO_INT', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto a Operar', dataIndex: 'MONTO_OPERAR', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto Int.', dataIndex: 'MONTO_INT_FIDE', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto a Operar', dataIndex: 'MONTO_OPERAR_FIDE', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },						
			{	header: 'Monto Anterior', dataIndex: 'MONTO_ANT', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Monto Nuevo', dataIndex: 'MONTO_NUEVO', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Recurso en Garant�a', dataIndex: 'RECURSO_GARANTIA', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00') },
			{	header: 'Porcentaje de Descuento', dataIndex: 'PORC_DSCTO', width: 150, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')	},
			{	header: 'Monto a Descontar', dataIndex: 'MONTO_DSCTO2', width: 150, align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00')}
			
		]
	});	
/*******************Componentes*******************************/
	
	var elementosForma = [
		{
			xtype: 'compositefield',
			id: 'cfBusqueda',
			fieldLabel:'Tipo de B�squeda',
			hidden:true,
			msgTarget: 'side',
			items: [

				{ 
					xtype: 'radio',
					name: 'tipo_busqueda',
					id: 'radioNormal',
					value: true,
					listeners:{
						check:	function(radio){
										if (radio.checked){
											tipobusqueda = "N";
											consultaData7.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid7', 
													TipoBusqueda: tipobusqueda													
												})
											});
											
										}
									}
					}	
				},	{
					xtype: 'displayfield',
					value: 'Operaci�n Normal',
					width: 150
				}, 				{ 
					xtype: 'radio',
					name: 'tipo_busqueda',
					id: 'radioFideicomiso',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											tipobusqueda = "F";
											
											consultaData7.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid7', 
													TipoBusqueda: tipobusqueda												
												})
											});
										}
									}
					}					
				},{
					xtype: 'displayfield',
					value: 'Operaci�n Fideicomiso ',
					width: 150
				},	{ 
					xtype: 'radio',
					name: 'tipo_busqueda',
					checked:true,
					id: 'radioAmbas',
					listeners:{
						check:	function(radio){
										if (radio.checked){
											tipobusqueda = "A";
											consultaData7.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid7', 
													TipoBusqueda: tipobusqueda
												})
											});		
										}
									}
					}					
				},{
					xtype: 'displayfield',
					value: 'Ambas',
					width: 150
				}
			]
		},	
		{
			xtype: 'panel',
			labelWidth: 140,
			layout: 'form',
			items: [ 		
					{
						xtype				: 'combo',
						id					: 'id_cambio_estatus',
						name				: 'ic_cambio_estatus',
						hiddenName 		: 'ic_cambio_estatus',
						fieldLabel		: 'Tipo de Cambio Estatus',
						width				: 280,
						forceSelection	: true,
						triggerAction	: 'all',
						mode				: 'local',
						valueField		: 'clave',
						displayField	: 'descripcion',
						emptyText		: 'Seleccione Estatus',
						store				: catalogoEstatus,
						listeners: {
							select: function(combo, record, index) {
								iEstatus = record.json.clave;
								gridTotales7Normal.hide();
								gridTotales7Fideicomiso.hide();
								gridTotales.hide();
								
								fp.el.mask('Enviando...', 'x-mask-loading');	
								
								if (record.json.clave == '2') {
									ocultar();
									grid2.hide();grid7.hide();grid8.hide();grid28.hide();grid29.hide();grid32.hide();grid40.hide();
									consultaData2.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid',
													ic_cambio_estatus: record.json.clave
												})
									});
								}
								else if (record.json.clave == '7'){
									grid2.hide();grid7.hide();grid8.hide();grid28.hide();grid29.hide();grid32.hide();grid40.hide();
									Ext.getCmp('cfBusqueda').show();
									consultaData7.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid7', //??
													TipoBusqueda: tipobusqueda,
													ic_cambio_estatus: record.json.clave
												})
									});
								}
								else if (record.json.clave == '8'){
									ocultar();
									grid2.hide();grid7.hide();grid8.hide();grid28.hide();grid29.hide();grid32.hide();grid40.hide();
									consultaData8.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid',
													ic_cambio_estatus: record.json.clave
												})
									});
								}
								else if (record.json.clave == '28'){
									ocultar();
									grid2.hide();grid7.hide();grid8.hide();grid28.hide();grid29.hide();grid32.hide();grid40.hide();
									consultaData28.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid',
													ic_cambio_estatus: record.json.clave
												})
									});
								}
								else if (record.json.clave == '29'){
									ocultar();
									grid2.hide();grid7.hide();grid8.hide();grid28.hide();grid29.hide();grid32.hide();grid40.hide();
									consultaData29.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid',
													ic_cambio_estatus: record.json.clave
												})
									});
								}
								else if (record.json.clave == '32'){
									ocultar();
									grid2.hide();grid7.hide();grid8.hide();grid28.hide();grid29.hide();grid32.hide();grid40.hide();
									consultaData32.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid',
													ic_cambio_estatus: record.json.clave
												})
									});
								}
								else if (record.json.clave == '40'){
									ocultar();
									grid2.hide();grid7.hide();grid8.hide();grid28.hide();grid29.hide();grid32.hide();grid40.hide();
									consultaData40.load({
												params: Ext.apply(fp.getForm().getValues(),{	
													operacion: 'ConsultaGrid',
													ic_cambio_estatus: record.json.clave
												})
									});
								}							
								
							}//select
						}//listener
					}
				]
		},
		{
			xtype : 'displayfield',
			value :''
		},
		{
			xtype: 'label',
			id: 'strNombreUsuario',
			style: 'font-weight:bold;color:#006699;text-align:right;display:block;',
			fieldlabel: 'Usuario',
			text: '--'
		},
		{
			xtype: 'label',
			id: 'fechaHora',
			style: 'font-weight:bold;color:#006699;text-align:right;display:block;',
			fieldlabel: 'Fecha',
			text: '--'
		}];
	

	var fp = new Ext.form.FormPanel
	({
		id: 'forma',
		width: 680,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},		style: ' margin:0 auto;',
		title: 'Cambio de Estatus',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 140,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true
	});
	
	var pnl = new Ext.Container
	({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			
			fp,	NE.util.getEspaciador(20),
			grid2,
			grid7,
			grid8,
			grid28,
			grid29,
			grid32,
			grid40,NE.util.getEspaciador(10),
			gridTotales,
			gridTotales7Normal, NE.util.getEspaciador(10),
			gridTotales7Fideicomiso
		]
	});
	
	
	var valIniciales = function(){
		
		Ext.Ajax.request({
			url: '13reporte2ext.data.jsp',
			params: {
						informacion: "valoresIniciales"
			},
			callback: procesaValoresIniciales
		});
	}
	
	catalogoEstatus.load();
	valIniciales();
	
});