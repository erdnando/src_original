 <%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		java.text.SimpleDateFormat,
		com.netro.descuento.*, 
		
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray, net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion     =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	String infoRegresar  = "";
	int start=0; int limit=0;   
	SimpleDateFormat formatoHora2 = new SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a");
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	
	if (informacion.equals("valoresIniciales")  ) {  
	
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("strNombreUsuario", strNombreUsuario);
		jsonObj.put("fechaHora",formatoHora2.format(new java.util.Date()));
		infoRegresar = jsonObj.toString();
	}
	
	else if (informacion.equals("CatalogoEstatus") ) 
	{
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("ic_cambio_estatus");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setTabla("comcat_cambio_estatus");
		cat.setValoresCondicionIn("2,7,8,28,29,32,40",Integer.class);
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();
		
	}
	else if(informacion.equals("ConsultaGrid7")){
				String busqueda = (request.getParameter("TipoBusqueda") != null) ?   request.getParameter("TipoBusqueda")  :"";
				RepCEstatusNafinDEbean cons = new RepCEstatusNafinDEbean();
				cons.setTipoBusqueda(busqueda);
				Registros reg	=	cons.getQueryPorCambioEstatus7();
				String consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
				jsonObj.put("success", new Boolean(true));
				jsonObj = JSONObject.fromObject(consulta);
				infoRegresar = jsonObj.toString();
	} 
	
	else if(informacion.equals("ConsultaGrid") ||  informacion.equals("GeneraPDF") || informacion.equals("ConsultarTotales")  )
	{
		String ic_cambio_estatus  		= (request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String busqueda = (request.getParameter("TipoBusqueda") != null) ?   request.getParameter("TipoBusqueda")  :"";							
			
		
		//RepCEstatusNafinExt paginador = new RepCEstatusNafinExt();
		RepCEstatusNafinDEbean paginador = new RepCEstatusNafinDEbean();
		
		paginador.setNoCambioEstatus(Integer.parseInt(ic_cambio_estatus));
		
		if(!busqueda.equals("A"))
			paginador.setTipoBusqueda(busqueda); 
	
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
		try 
		{
			if(informacion.equals("ConsultaGrid"))
			{
				Registros reg = queryHelper.doSearch();
				infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
			} 
			else if (informacion.equals("GeneraPDF"))
			{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();		
			}
			else if(informacion.equals("ConsultarTotales"))
			{
				Registros reg = queryHelper.doSearch();
				
				int iNoMoneda = 0;
				double nacional = 0;
				double nacional_desc = 0;
				double nacional_int = 0;
				double nacional_descIF = 0;
				double nacional_int_fide = 0;
				double nacional_descIF_fide = 0;
							
				double dolares = 0;
				double dolares_desc = 0;
				double dolares_int = 0;
				double dolares_descIF = 0;	
				double dolares_int_fide = 0;
				double dolares_descIF_fide = 0;			

				double dMontoDscto = 0;
				double dImporteInteres = 0;
				double dImporteRecibir = 0;
				double dImporteInteresFide = 0;
				double dImporteRecibirFide = 0;				
				double dTasaAceptada = 0;
				double dMontoDocto = 0;
				double dPorcentaje = 0;
				double dblRecurso = 0;
				String sTipoFactoraje = "";
				boolean dolaresDocto = false;
				boolean nacionalDocto = false;
			  // Declaración e inicialización de variables
				double total_mn = 0.0;
				double total_dolares = 0.0;
				double dblTotMontoDescuentoMN = 0.0;
				double dblTotMontoDescuentoDL = 0.0;
				dMontoDscto = 0;
				double dblTotRecursoMN = 0.0;
				double dblTotRecursoDL = 0.0;
				double df_total_ant_mn = 0.0;
				double df_total_ant_dol = 0.0;
				
				String sMontoDocto = "";
				String sMontoDscto = "";
				String sImporteInteres = "";
				String sImporteRecibir = "";
				String sImporteInteresFide = "";
				String sImporteRecibirFide = "";				
				String sMontoAnterior = "";
				String sMontoNuevo = "";
				String sNoMoneda = "";
				HashMap registrosTotNacional = new HashMap();	
				HashMap registrosTotDolares = new HashMap();	
				JSONArray registrosTotales= new JSONArray();
				
				// Grid 2 y Grid 7
				if(ic_cambio_estatus.equals("2") || ic_cambio_estatus.equals("7")) {
					
					while (reg.next()) {
						sMontoDocto = (reg.getString("montoDocto") == null) ? "" : reg.getString("montoDocto"); 
						sMontoDscto = (reg.getString("montoDscto") == null) ? "" : reg.getString("montoDscto");
						if(ic_cambio_estatus.equals("7") ){
						sImporteInteres = (reg.getString("importeInteresIf") == null) ? "" : reg.getString("importeInteresIf"); 
						sImporteRecibir = (reg.getString("importeRecibirIf") == null) ? "" : reg.getString("importeRecibirIf"); 
						}else  {
							sImporteInteres = (reg.getString("importeInteres") == null) ? "" : reg.getString("importeInteres"); 
							sImporteRecibir = (reg.getString("importeRecibir") == null) ? "" : reg.getString("importeRecibir"); 
						}
						
						
						sImporteInteresFide = (reg.getString("importeInteres") == null) ? "" : reg.getString("importeInteres"); 
						sImporteRecibirFide = (reg.getString("importeRecibir") == null) ? "" : reg.getString("importeRecibir"); 
						
						sNoMoneda	= (reg.getString("claveMoneda") == null) ? "" : reg.getString("claveMoneda");
						
						dMontoDocto 	 = Double.parseDouble(sMontoDocto);
						dMontoDscto 	 = Double.parseDouble(sMontoDscto);
						
						if(!"N/A".equals(sImporteInteres) && !"".equals(sImporteInteres))
							dImporteInteres = Double.parseDouble(sImporteInteres);
						else
							dImporteInteres = 0;
						
						if(!"N/A".equals(sImporteRecibir) && !"".equals(sImporteRecibir))
							dImporteRecibir = Double.parseDouble(sImporteRecibir);						
						else 
							dImporteRecibir = 0;
							
						if(!"N/A".equals(sImporteInteresFide) && !"".equals(sImporteInteresFide))
							dImporteInteresFide = Double.parseDouble(sImporteInteresFide);
						else
							dImporteInteresFide = 0;
						
						if(!"N/A".equals(sImporteRecibirFide) && !"".equals(sImporteRecibirFide))
							dImporteRecibirFide = Double.parseDouble(sImporteRecibirFide);						
						else 
							dImporteRecibirFide = 0;						
						
						
						iNoMoneda	= Integer.parseInt(sNoMoneda);
						sTipoFactoraje = (reg.getString("tipoFactoraje")==null)?"":reg.getString("tipoFactoraje").trim();
					
						if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
						  dMontoDscto = dMontoDocto;
						  dPorcentaje = 100;
						}
						if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
							nacional_int_fide += dImporteInteresFide;
							nacional_descIF_fide += dImporteRecibirFide;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
							dolares_int_fide += dImporteInteresFide;
							dolares_descIF_fide += dImporteRecibirFide; 
						}
					
						if (nacionalDocto) { 				
							registrosTotNacional = new HashMap();		
							registrosTotNacional.put("MONEDA", "Total MN");
							registrosTotNacional.put("MONTO_DOCTO",   Comunes.formatoMoneda2(String.valueOf(nacional),false) );
							registrosTotNacional.put("MONTO_DSCTO",    Comunes.formatoMoneda2(String.valueOf(nacional_desc),false) );
							registrosTotNacional.put("MONTO_INT",   Comunes.formatoMoneda2(String.valueOf(nacional_int),false) );
							registrosTotNacional.put("MONTO_OPERAR",   Comunes.formatoMoneda2(String.valueOf(nacional_descIF),false) );
							registrosTotNacional.put("MONTO_INT_FIDE",   Comunes.formatoMoneda2(String.valueOf(nacional_int_fide),false) );
							registrosTotNacional.put("MONTO_OPERAR_FIDE",   Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fide),false) );
												
						}
					
						if (dolaresDocto){ 			
							registrosTotDolares = new HashMap();
							registrosTotDolares.put("MONEDA", "Total USD");
							registrosTotDolares.put("MONTO_DOCTO",   Comunes.formatoMoneda2(String.valueOf(dolares),false) );
							registrosTotDolares.put("MONTO_DSCTO",    Comunes.formatoMoneda2(String.valueOf(dolares_desc),false) );
							registrosTotDolares.put("MONTO_INT",   Comunes.formatoMoneda2(String.valueOf(dolares_int),false) );
							registrosTotDolares.put("MONTO_OPERAR",   Comunes.formatoMoneda2(String.valueOf(dolares_descIF),false) );
							registrosTotDolares.put("MONTO_INT_FIDE",   Comunes.formatoMoneda2(String.valueOf(dolares_int_fide),false) );
							registrosTotDolares.put("MONTO_OPERAR_FIDE",   Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fide),false) );
											
						}
					}	
				}
				// Grid 8
				if(ic_cambio_estatus.equals("8")) {
					
					while (reg.next()) {
						sMontoDocto = (reg.getString("montoDocto") == null) ? "" : reg.getString("montoDocto"); 
						sMontoDscto = (reg.getString("montoDscto") == null) ? "" : reg.getString("montoDscto");
						sMontoAnterior = (reg.getString("MontoAnterior") == null) ? "" : reg.getString("MontoAnterior"); 
						sNoMoneda	= (reg.getString("claveMoneda") == null) ? "" : reg.getString("claveMoneda");
						
						dMontoDocto 	 = Double.parseDouble(sMontoDocto);
						dblRecurso = dMontoDocto - dMontoDscto;
						dMontoDscto 	 = Double.parseDouble(sMontoDscto);
						
						double dMontoAnterior = Double.parseDouble(sMontoAnterior);
						iNoMoneda	= Integer.parseInt(sNoMoneda);
						sTipoFactoraje = (reg.getString("tipoFactoraje")==null)?"":reg.getString("tipoFactoraje").trim();
					
						if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
							dMontoDscto = dMontoDocto;
							dPorcentaje = 100;
						}
						if (iNoMoneda == 1) {
							total_mn += dMontoDocto;
							df_total_ant_mn += dMontoAnterior;
							dblTotRecursoMN += dblRecurso;
							dblTotMontoDescuentoMN += dMontoDscto;
							nacionalDocto = true;
						}
						  if (iNoMoneda == 54) {
							total_dolares += dMontoDocto;
							df_total_ant_dol += dMontoAnterior;
							dblTotRecursoDL += dblRecurso;
							dblTotMontoDescuentoDL += dMontoDscto;
							dolaresDocto = true;
						}
					
						if (nacionalDocto) { 				
							registrosTotNacional = new HashMap();		
							registrosTotNacional.put("MONEDA", "Total MN");
							registrosTotNacional.put("MONTO_DOCTO",   			Comunes.formatoMoneda2(String.valueOf(total_mn),true) );
							registrosTotNacional.put("MONTO_ANT",    		Comunes.formatoMoneda2(String.valueOf(df_total_ant_mn),true) );
							registrosTotNacional.put("RECURSO_GARANTIA", Comunes.formatoMoneda2(String.valueOf(dblTotRecursoMN),true) );
							registrosTotNacional.put("MONTO_DSCTO",   	Comunes.formatoMoneda2(String.valueOf(dblTotMontoDescuentoMN),true) );
						}
					
						if (dolaresDocto){ 			
							registrosTotDolares = new HashMap();
							registrosTotDolares.put("MONEDA", "Total USD");
							registrosTotDolares.put("MONTO_DOCTO",   				Comunes.formatoMoneda2(String.valueOf(total_dolares),true) );
							registrosTotDolares.put("MONTO_ANT",    		Comunes.formatoMoneda2(String.valueOf(df_total_ant_dol),true) );
							registrosTotDolares.put("RECURSO_GARANTIA",  Comunes.formatoMoneda2(String.valueOf(dblTotRecursoDL),true) );
							registrosTotDolares.put("MONTO_DSCTO",   		Comunes.formatoMoneda2(String.valueOf(dblTotMontoDescuentoDL),true) );
						}
					}	
				}		
				// Grid 28
				if(ic_cambio_estatus.equals("28")) {
					
					while (reg.next()) {
						sMontoAnterior = (reg.getString("montoAnterior") == null) ? "" : reg.getString("MontoAnterior"); 
						sMontoNuevo = (reg.getString("montoNuevo") == null) ? "" : reg.getString("montoNuevo"); 
						sNoMoneda	= (reg.getString("claveMoneda") == null) ? "" : reg.getString("claveMoneda");
						
						double dMontoAnterior = Double.parseDouble(sMontoAnterior);
						double dMontoNuevo    = Double.parseDouble(sMontoNuevo);
						iNoMoneda	= Integer.parseInt(sNoMoneda);
						double dblTotalMontoPesos = 0;
						double dblTotalMontoDolares = 0;
	
						if (iNoMoneda == 1) {
							df_total_ant_mn += dMontoAnterior;
							total_mn += dMontoNuevo;
							dblTotalMontoPesos += dMontoDscto;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							df_total_ant_dol += dMontoAnterior;
							total_dolares += dMontoNuevo;
							dblTotalMontoDolares += dMontoDscto;
							dolaresDocto = true;
						}
					
						if (nacionalDocto) { 				
							registrosTotNacional = new HashMap();		
							registrosTotNacional.put("MONEDA", "Total MN");
							registrosTotNacional.put("MONTO_ANT",   Comunes.formatoMoneda2(String.valueOf(df_total_ant_mn),true) );
							registrosTotNacional.put("MONTO_NUEVO", Comunes.formatoMoneda2(String.valueOf(total_mn),true) );
						}
					
						if (dolaresDocto){ 			
							registrosTotDolares = new HashMap();
							registrosTotDolares.put("MONEDA", "Total USD");
							registrosTotDolares.put("MONTO_ANT",    Comunes.formatoMoneda2(String.valueOf(df_total_ant_dol),true));
							registrosTotDolares.put("MONTO_DOCTO",  Comunes.formatoMoneda2(String.valueOf(total_dolares),true) );
						}
					}	
				}
				// Grid 29
				if(ic_cambio_estatus.equals("29")) {
					
					while (reg.next()) {
						sMontoDocto = (reg.getString("montoDocto") == null) ? "" : reg.getString("montoDocto"); 
						sMontoDscto = (reg.getString("montoDscto") == null) ? "" : reg.getString("montoDscto");
						sNoMoneda	= (reg.getString("claveMoneda") == null) ? "" : reg.getString("claveMoneda");
						
						dMontoDocto 	 = Double.parseDouble(sMontoDocto);
						dMontoDscto 	 = Double.parseDouble(sMontoDscto);
						iNoMoneda	= Integer.parseInt(sNoMoneda);
						// Para Factoraje Vencido y Distribuido.
						sTipoFactoraje = (reg.getString("tipoFactoraje")==null)?"":reg.getString("tipoFactoraje").trim();
						if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
						  dMontoDscto = dMontoDocto;
						  dPorcentaje = 100;
					   }
				
						if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolaresDocto = true;
						}	
					
						if (nacionalDocto) { 				
							registrosTotNacional = new HashMap();		
							registrosTotNacional.put("MONEDA", "Total MN");
							registrosTotNacional.put("MONTO_DOCTO",   Comunes.formatoMoneda2(String.valueOf(nacional),false) );
							registrosTotNacional.put("MONTO_DSCTO", Comunes.formatoMoneda2(String.valueOf(nacional_desc),false) );
						}
					
						if (dolaresDocto){ 			
							registrosTotDolares = new HashMap();
							registrosTotDolares.put("MONEDA", "Total USD");
							registrosTotDolares.put("MONTO_DOCTO",    Comunes.formatoMoneda2(String.valueOf(dolares),false));
							registrosTotDolares.put("MONTO_DSCTO",  Comunes.formatoMoneda2(String.valueOf(dolares_desc),false) );
						}
					}	
				}					
				// Grid 32
				if(ic_cambio_estatus.equals("32")) {
					
					while (reg.next()) {
						sMontoDocto 		= (reg.getString("montoDocto") == null) ? "" : reg.getString("montoDocto"); 
						sMontoDscto 		= (reg.getString("montoDscto") == null) ? "" : reg.getString("montoDscto");
						sImporteInteres   = (reg.getString("importeInteres") == null) ? "" : reg.getString("importeInteres");
						sImporteRecibir 	= (reg.getString("importeRecibir") == null) ? "" : reg.getString("importeRecibir");
						sNoMoneda	= (reg.getString("claveMoneda") == null) ? "" : reg.getString("claveMoneda");
						
						dMontoDocto 	 = Double.parseDouble(sMontoDocto);
						dMontoDscto 	 = Double.parseDouble(sMontoDscto);
						dImporteInteres = Double.parseDouble(sImporteInteres);
						dImporteRecibir = Double.parseDouble(sImporteRecibir);
						iNoMoneda	= Integer.parseInt(sNoMoneda);
						 if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						 }
						 if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						 }
									
						if (nacionalDocto) { 				
							registrosTotNacional = new HashMap();		
							registrosTotNacional.put("MONEDA", "Total MN");
							registrosTotNacional.put("MONTO_DOCTO",   Comunes.formatoMoneda2(String.valueOf(nacional),false) );
							registrosTotNacional.put("MONTO_DSCTO",   Comunes.formatoMoneda2(String.valueOf(nacional_desc),false) );
							registrosTotNacional.put("MONTO_INT",   Comunes.formatoMoneda2(String.valueOf(nacional_int),false) );
						}
					
						if (dolaresDocto){ 			
							registrosTotDolares = new HashMap();
							registrosTotDolares.put("MONEDA", "Total USD");
							registrosTotDolares.put("MONTO_DOCTO",   Comunes.formatoMoneda2(String.valueOf(dolares),false) );
							registrosTotDolares.put("MONTO_DSCTO",   Comunes.formatoMoneda2(String.valueOf(dolares_desc),false) );
							registrosTotDolares.put("MONTO_INT",   Comunes.formatoMoneda2(String.valueOf(dolares_int),false) );
						}
						
					}	
				}			
				// Grid 40
				if(ic_cambio_estatus.equals("40")) {
					
					while (reg.next()) {
						sMontoDocto = (reg.getString("montoDocto") == null) ? "" : reg.getString("montoDocto"); //
						sMontoDscto = (reg.getString("montoDscto") == null) ? "" : reg.getString("montoDscto");//
						sNoMoneda	= (reg.getString("claveMoneda") == null) ? "" : reg.getString("claveMoneda");
						
						dMontoDocto = Double.parseDouble(sMontoDocto);
						dMontoDscto = Double.parseDouble(sMontoDscto);
						iNoMoneda	= Integer.parseInt(sNoMoneda);
						
					  if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						}
					
						if (nacionalDocto) { 				
							registrosTotNacional = new HashMap();		
							registrosTotNacional.put("MONEDA", "Total MN");
							registrosTotNacional.put("MONTO_DOCTO",   Comunes.formatoMoneda2(String.valueOf(nacional),false) );
							registrosTotNacional.put("PORC_DSCTO",    Comunes.formatoMoneda2(String.valueOf(nacional_desc),false) );
							registrosTotNacional.put("MONTO_DSCTO",   Comunes.formatoMoneda2(String.valueOf(nacional_int),false) );
						}
					
						if (dolaresDocto){ 			
							registrosTotDolares = new HashMap();
							registrosTotDolares.put("MONEDA", "Total USD");
							registrosTotDolares.put("MONTO_DOCTO",   Comunes.formatoMoneda2(String.valueOf(dolares),false) );
							registrosTotDolares.put("PORC_DSCTO",    Comunes.formatoMoneda2(String.valueOf(dolares_desc),false) );
							registrosTotDolares.put("MONTO_DSCTO",   Comunes.formatoMoneda2(String.valueOf(dolares_int),false) );
						}
					}	
				}					
				if (nacionalDocto)
					registrosTotales.add(registrosTotNacional);		
				if (dolaresDocto)
					registrosTotales.add(registrosTotDolares);	
				infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";					
				
			
			}
		} catch(Exception e) 
			{
				throw new AppException("Error en la recuperacion de datos!", e);
			}
		
   }//System.out.println("infoRegresar============"+infoRegresar); 	


%>
<%=infoRegresar%> 