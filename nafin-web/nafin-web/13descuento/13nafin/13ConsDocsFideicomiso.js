Ext.onReady(function(){

	function procesarSuccessFailureNombreNae(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var nom_Nae = infoR.nom_Nae;
			var num_pyme = infoR.num_pyme;
			Ext.getCmp('_nom_prov').setValue(nom_Nae);
			Ext.getCmp('_ic_pyme').setValue(num_pyme);
		}else {
			Ext.Msg.alert('Aviso','El No. Nafin Electr�nico no corresponde a una PyME afiliada o no existe.');
		}	
	}

	var procesarConsultaRegistros = function(store, registros, opts){
		var forma = Ext.getCmp('forma');
		forma.el.unmask();
		if (registros != null) {
			if(Ext.getCmp('rb2').getValue()){
				var grid  = Ext.getCmp('gridConsulta');
				if (!grid.isVisible()) {
					grid.show();
				}
				Ext.getCmp('barraPaginacion').show();
				var el = grid.getGridEl();

				if(store.getTotalCount() > 0) {
					var registroi = store.getAt(0);
					var registrof = store.getAt(store.getTotalCount()-1);
					Ext.getCmp('btnGenerarPDF').enable();
					Ext.getCmp('btnArchivoCSV').enable();
					el.unmask();
					totalesConsultadosData.load({
											url: '13ConsDocsFideicomiso.data.jsp',
											params:	Ext.apply(fp.getForm().getValues(),
														{
															informacion:'ConsultarTotales'
														}
											)
										});
					
					var gr = Ext.getCmp('gridTotalesConsulta');
					gr.show();
					gr.setTitle('Fecha de Operaci�n: '+registroi.data['FEC_OPE']+' al '+registrof.data['FEC_OPE']);
				} else {
					Ext.getCmp('btnGenerarPDF').disable();
					Ext.getCmp('btnArchivoCSV').disable();
					el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
				}
				// Actualizar los parametros base con los parametros de la consulta exitosa
				store.baseParams = opts.params;
				// Resetear el campo operacion para evitar que cuando se utilicen los botones
				// de paginacion del pagingtoolbar se tengan que regenerar la llaves
				Ext.apply(
					store.baseParams,
					{
						operacion: ''
					}
				);	
			}else{
				var grid  = Ext.getCmp('gridConsulta2');
				if (!grid.isVisible()) {
					grid.show();
				}
				Ext.getCmp('barraPaginacion2').show();
				var el = grid.getGridEl();

				if(store.getTotalCount() > 0) {
					var registroi = store.getAt(0);
					var registrof = store.getAt(store.getTotalCount()-1);
					Ext.getCmp('btnGenerarPDF2').enable();
					Ext.getCmp('btnArchivoCSV2').enable();
					el.unmask();
					totalesConsultadosData.load({
											url: '13ConsDocsFideicomiso.data.jsp',
											params:	Ext.apply(fp.getForm().getValues(),
														{
															informacion:'ConsultarTotales'
														}
											)
										});
					
					var gr = Ext.getCmp('gridTotalesConsulta');
					gr.show();
					gr.setTitle('Fecha de Operaci�n: '+registroi.data['FEC_OPE']+' al '+registrof.data['FEC_OPE']);
				} else {
					Ext.getCmp('btnGenerarPDF2').disable();
					Ext.getCmp('btnArchivoCSV2').disable();
					el.mask('No se encontr� ning�n registro, intente de nuevo con otro criterio de b�squeda', 'x-mask');
				}
				// Actualizar los parametros base con los parametros de la consulta exitosa
				store.baseParams = opts.params;
				// Resetear el campo operacion para evitar que cuando se utilicen los botones
				// de paginacion del pagingtoolbar se tengan que regenerar la llaves
				Ext.apply(
					store.baseParams,
					{
						operacion: ''
					}
				);	
			}
		}
	}
	
	function procesarSuccessFailureArchivoCSV(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		boton = Ext.getCmp('btnArchivoCSV');
		boton.enable();
		boton.setIconClass('icoXls');
	}

	function procesarSuccessFailureGenerarPDF(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		boton = Ext.getCmp('btnGenerarPDF');
		boton.setIconClass('icoPdf');
		boton.enable();
	}

	function procesarSuccessFailureArchivoCSV2(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		boton = Ext.getCmp('btnArchivoCSV2');
		boton.setIconClass('icoXls');
		boton.enable();
	}
	
	function procesarSuccessFailureGenerarPDF2(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		boton = Ext.getCmp('btnGenerarPDF2');
		boton.setIconClass('icoPdf');
		boton.enable();
	}
	
/*----------------------------- Maquina de Edos. ----------------------------*/
		var accionConsulta = function(estadoSiguiente, respuesta){

		 if(  estadoSiguiente == "CONSULTAR" ){
			bandera = true;
			var fechaIniNot = Ext.getCmp('_fecha_noti_ini');
			var fechaFinNot = Ext.getCmp('_fecha_noti_fin');
			var fechaIniVen = Ext.getCmp('_fecha_ven_ini');
			var fechaFinVen = Ext.getCmp('_fecha_ven_fin');
			var fechaIniOpe = Ext.getCmp('_fecha_ope_ini');
			var fechaFinOpe = Ext.getCmp('_fecha_ope_fin');
			var monto_i = Ext.getCmp('_monto_ini');
			var monto_f = Ext.getCmp('_monto_fin');
			if(fechaIniNot.getValue()!=""){
				var fechaDe = Ext.util.Format.date(fechaIniNot.getValue(),'d/m/Y');
				var fechaA = Ext.util.Format.date(fechaFinNot.getValue(),'d/m/Y');
				if(!isdate(fechaDe)){
					fechaIniNot.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}else if(!isdate(fechaA)){
					fechaFinNot.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}
			}
			if(parseInt(monto_i.getValue()) > parseInt(monto_f.getValue())){
				monto_f.markInvalid("El monto final debe ser mayor al inicial");
				bandera = false;
			}
			if(fechaIniVen.getValue()!=""){
				var fechaDe = Ext.util.Format.date(fechaIniVen.getValue(),'d/m/Y');
				var fechaA = Ext.util.Format.date(fechaFinVen.getValue(),'d/m/Y');
				if(!isdate(fechaDe)){
					fechaIniVen.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}else if(!isdate(fechaA)){
					fechaFinVen.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}
			}
			if(fechaIniOpe.getValue()!=""){
				var fechaDe = Ext.util.Format.date(fechaIniOpe.getValue(),'d/m/Y');
				var fechaA = Ext.util.Format.date(fechaFinOpe.getValue(),'d/m/Y');
				if(!isdate(fechaDe)){
					fechaIniOpe.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}else if(!isdate(fechaA)){
					fechaFinOpe.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
					bandera = false;
				}
			}
				
			//if(bandera){
				var forma = Ext.getCmp("forma");
				forma.el.mask('Buscando...','x-mask-loading');
				if(Ext.getCmp('rb2').getValue()){
					var estatus = Ext.getCmp('_estatus').getValue();
					Ext.StoreMgr.key('registrosConsultadosDataStore').load({
							params:	Ext.apply(fp.getForm().getValues(),
										{
											informacion: 'consultaDatos',
											estatus: estatus,
											operacion: 'Generar',
											tipo: 'D',
											start: 0,
											limit: 15
										}
								)
					});
				}else{
					var estatus = Ext.getCmp('_estatus').getValue();
					Ext.StoreMgr.key('registrosConsultadosDataStore').load({
							params:	Ext.apply(fp.getForm().getValues(),
										{
											informacion: 'consultaDatos',
											estatus: estatus,
											operacion: 'Generar',
											tipo: 'C',
											start: 0,
											limit: 15
										}
								)
					});
				}
			//}
		} else if(	estadoSiguiente == "LIMPIAR"){
			if(Ext.getCmp('rb2').getValue())
				Ext.getCmp('forma').getForm().reset();
			else{
				Ext.getCmp('_cmb_epo').reset();
				Ext.getCmp('_nae').reset();
				Ext.getCmp('_nom_prov').reset();
				Ext.getCmp('_ic_pyme').reset();
				Ext.getCmp('_cmb_moneda').reset();
				Ext.getCmp('_estatus').reset();
				Ext.getCmp('_fecha_ven_ini').reset();
				Ext.getCmp('_fecha_ven_fin').reset();
				Ext.getCmp('_fecha_ope_ini').reset();
				Ext.getCmp('_fecha_ope_fin').reset();
			}
			grid.hide();
			grid2.hide();
			gridTot.hide();
		}
	}
	
/*---------------------------------- Store's --------------------------------*/

	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var registrosConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'registrosConsultadosDataStore',
			url: 			'13ConsDocsFideicomiso.data.jsp',
			baseParams: {	informacion:	'ConsultaRegistros'	},
			fields: [
				{ name: 'NOM_EPO' },
				{ name: 'NOM_PYME'},
				{ name: 'NUM_DOC'},
				{ name: 'RFC'},
				{ name: 'TOT_DOC_CON'},
				{ name: 'FEC_NOTI'},
				{ name: 'FEC_VEN'},
				{ name: 'FEC_OPE'},
				{ name: 'PLAZO'},
				{ name: 'ESTATUS'},
				{ name: 'MONEDA'},
				{ name: 'MONTO_DOC'},
				{ name: 'REC_GARAN'},
				{ name: 'MONTO_DESC'},
				{ name: 'NOM_FIDE'},
				{ name: 'TASA_PYME'},
				{ name: 'MONTO_INT_PYME'},
				{ name: 'MONTO_OPE_PYME'},
				{ name: 'NOM_IF'},
				{ name: 'TASA_IF'},
				{ name: 'MONTO_INT_IF'},
				{ name: 'MONTO_OPE_IF'},
				{ name: 'TASA_IF_ING'},
				{ name: 'ING_NETO_IF'},
				{ name: 'DIF_TASAS'},
				{ name: 'ING_NETO_FISO'},
				{ name: 'TASA_FONDEO_IF'},
				{ name: 'ING_FONDEO'},
				{ name: 'PORC_CONTRA_EPO'},
				{ name: 'CONTRA_EPO'},
				{ name: 'ING_NETO_NAFIN'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				beforeload:	{
					fn: function(store, options){
					
					}
				},
				load: 	procesarConsultaRegistros,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaRegistros(null, null, null);
					}
				}
			}
	});
	
	//Este store es para los datos que seran cargados en el grid despues de realizar la consulta
		var totalesConsultadosData = new Ext.data.JsonStore({
			root: 		'registros',
			id:			'totalesConsultadosDataStore',
			url: 			'13ConsDocsFideicomiso.data.jsp',
			baseParams: {	informacion:	'ConsultarTotales'	},
			fields: [
				{ name: 'CVE_MONEDA' },
				{ name: 'REGISTROS'},
				{ name: 'MONTO_DOC_TOT'},
				{ name: 'MONTO_REC_GARAN_TOT'},
				{ name: 'MONTO_DESC_TOT'},
				{ name: 'MONTO_INT_TOT_PYME'},
				{ name: 'MONTO_OPE_TOT_PYME'},
				{ name: 'MONTO_INT_TOT_IF'},
				{ name: 'MONTO_OPE_TOT_IF'},
				{ name: 'ING_NETO_TOT_IF'},
				{ name: 'ING_NETO_TOT_FISO'},
				{ name: 'ING_NETO_TOT_NAFIN'}
			],
			totalProperty: 	'total',
			messageProperty: 	'msg',
			autoLoad: 			false,
			listeners: {
				//load: 	procesarTotales,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					}
				}
			}
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
      id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13ConsDocsFideicomiso.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13ConsDocsFideicomiso.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoEPOs = new Ext.data.JsonStore({
		id: 'catalogoEPOs',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13ConsDocsFideicomiso.data.jsp',
		baseParams: {
			informacion: 'catalogoCadenas'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13ConsDocsFideicomiso.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	
	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13ConsDocsFideicomiso.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			//exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	/*-------------------------------- Componentes ------------------------------*/
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 12, align: 'center'},
					{header: 'OPERACI�N PYME', colspan: 4, align: 'center'},
					{header: 'OPERACI�N INTERMEDIARIO FINANCIERO', colspan: 4, align: 'center'},
					{header: 'INGRESO IF', colspan: 2, align: 'center'},
					{header: 'INGRESO FIDEICOMISO', colspan: 2, align: 'center'},
					{header: 'INGRESO NAFIN', colspan: 5, align: 'center'}
				]
			]
	});
	
	//Elementos del grid de la consulta
		var grid = new Ext.grid.GridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			title:		undefined,
			//view:			new Ext.grid.GridView({forceFit:	true}),
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			width: 		940,
			plugins:		grupos,
			frame: 		false,
			columns: [
				//.toUpperCase();
				{
					header: 		'Nombre de EPO',
					tooltip: 	'Nombre de la EPO',
					dataIndex: 	'NOM_EPO',
					sortable: 	true,
					align:		'left',
					resizable: 	true,
					width: 		180,
					hideable:	false
				},{
					header: 		'Nombre PYME',
					tooltip: 	'Nombre de la PyME',
					dataIndex: 	'NOM_PYME',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		165,
					hideable:	false
				},{
					header: 		'N�mero de Documento',
					tooltip: 	'N�mero del documento',
					dataIndex: 	'NUM_DOC',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		122,
					hideable:	false
				},{
					header: 		'Fecha de Notificaci�n',
					tooltip: 	'Fecha de notificaci�n',
					dataIndex: 	'FEC_NOTI',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hideable:	false
				},{
					header: 		'Fecha de Vencimiento',
					tooltip: 	'Fecha de vencimiento',
					dataIndex: 	'FEC_VEN',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hideable:	false
				},{
					header: 		'Fecha de Operaci�n',
					tooltip: 	'Fecha de operaci�n',
					dataIndex: 	'FEC_OPE',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hideable:	false
				},{
					header: 		'Plazo',
					tooltip: 	'Plazo',
					dataIndex: 	'PLAZO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		92
				},{
					header: 		'Estatus',
					tooltip: 	'Estatus',
					dataIndex: 	'ESTATUS',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Moneda',
					tooltip: 	'Moneda',
					dataIndex: 	'MONEDA',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		130
				},{
					header: 		'Monto del Documento',
					tooltip: 	'Monto del documento',
					dataIndex: 	'MONTO_DOC',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Recurso en Garant�a',
					tooltip: 	'Recurso en Garant�a',
					dataIndex: 	'REC_GARAN',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto a Descontar',
					tooltip: 	'Monto a Descontar',
					dataIndex: 	'MONTO_DESC',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Nombre Fideicomiso',
					tooltip: 	'Nombre del fideicomiso',
					dataIndex: 	'NOM_FIDE',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		140
				},{
					header: 		'Tasa',
					tooltip: 	'Tasa',
					dataIndex: 	'TASA_PYME',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					renderer: Ext.util.Format.numberRenderer('0.0000%')
				},{
					header: 		'Monto Int.',
					tooltip: 	'Monto Int.',
					dataIndex: 	'MONTO_INT_PYME',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto a Operar',
					tooltip: 	'Monto a operar',
					dataIndex: 	'MONTO_OPE_PYME',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Nombre IF',
					tooltip: 	'Nombre del IF',
					dataIndex: 	'NOM_IF',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		135
				},{
					header: 		'Tasa',
					tooltip: 	'Tasa',
					dataIndex: 	'TASA_IF',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					renderer: Ext.util.Format.numberRenderer('0.0000%')
				},{
					header: 		'Monto Int.',
					tooltip: 	'Monto Int.',
					dataIndex: 	'MONTO_INT_IF',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto a Operar',
					tooltip: 	'Monto a operar',
					dataIndex: 	'MONTO_OPE_IF',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Tasa IF',
					tooltip: 	'Tasa IF',
					dataIndex: 	'TASA_IF_ING',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		115,
					renderer: Ext.util.Format.numberRenderer('0.0000%')
				},{
					header: 		'Ingreso Neto IF',
					tooltip: 	'Ingreso Neto IF',
					dataIndex: 	'ING_NETO_IF',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Diferencial de Tasas',
					tooltip: 	'Diferencial de tasas',
					dataIndex: 	'DIF_TASAS',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('0.0000%')
				},{
					header: 		'Ingreso Neto FISO',
					tooltip: 	'Ingreso Neto FISO',
					dataIndex: 	'ING_NETO_FISO',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Tasa de Fondeo al IF',
					tooltip: 	'Tasa de Fondeo al IF',
					dataIndex: 	'TASA_FONDEO_IF',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('0.0000%')
				},{
					header: 		'Ingreso por FONDEO',
					tooltip: 	'Ingreso por FONDEO',
					dataIndex: 	'ING_FONDEO',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'% Contraprestaci�n a EPO',
					tooltip: 	'Porcentaje Contraprestaci�n a EPO',
					dataIndex: 	'PORC_CONTRA_EPO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		140,
					renderer: Ext.util.Format.numberRenderer('0.0000%')
				},{
					header: 		'Contraprestaci�n a EPO',
					tooltip: 	'Contraprestaci�n a EPO',
					dataIndex: 	'CONTRA_EPO',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		140,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Ingreso Neto NAFIN',
					tooltip: 	'Ingreso Neto NAFIN',
					dataIndex: 	'ING_NETO_NAFIN',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		140,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				}
			],
			bbar: {
				xtype: 			'paging',
				pageSize: 		15,
				buttonAlign: 	'left',
				id: 				'barraPaginacion',
				displayInfo: 	true,
				store: 			registrosConsultadosData,
				displayMsg: 	'{0} - {1} de {2}',
				emptyMsg: 		'No hay registros.',
				items: [
					'->','-',
/*					{
						xtype:	'button',
						text:		'Imprimir PDF',
						id: 		'btnGenerarPDF',
						iconCls:	'icoPdf',
						handler: function(boton, evento) {
										var estatus = Ext.getCmp('_estatus').getValue();
										boton.setIconClass('loading-indicator');
										var barraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '13ConsDocsFideicomiso.data.jsp',
											params:	Ext.apply(fp.getForm().getValues(),
												{
													informacion:'ArchivoXPDF',
													start: barraPaginacion.cursor,
													limit: barraPaginacion.pageSize,
													estatus: estatus,
													tipo: 'D'
												}
											),
											callback: procesarSuccessFailureGenerarPDF
										});
						}
					},*/
					{
						xtype:   'button',
						iconCls: 'icoXls',
						text:    'Generar Archivo',
						tooltip: 'Imprime los registros en formato CSV.',
						id:      'btnArchivoCSV',
						handler: function(boton, evento){
							boton.disable();
							boton.setIconClass('loading-indicator');
							var estatus = Ext.getCmp('_estatus').getValue();
							Ext.Ajax.request({
								url: '13ConsDocsFideicomiso.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion:'ArchivoCSV',
									estatus: estatus,
									tipo: 'D'
								}),
								callback: procesarSuccessFailureArchivoCSV
							});
						}
					},{
						xtype:   'button',
						text:    'Generar Todo',
						tooltip: 'Imprime los registros en formato PDF.',
						iconCls: 'icoPdf',
						id:      'btnGenerarPDF',
						handler: function(boton, evento) {
							var estatus = Ext.getCmp('_estatus').getValue();
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '13ConsDocsFideicomiso.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion:'ArchivoXPDF',
									estatus: estatus,
									tipo: 'D'
								}),
								callback: procesarSuccessFailureGenerarPDF
							});
						}
					}
				]
			}
	});
	
	var grupos2 = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 11, align: 'center'},
					{header: 'OPERACI�N PYME', colspan: 2, align: 'center'},
					{header: 'OPERACI�N INTERMEDIARIO FINANCIERO', colspan: 2, align: 'center'},
					{header: '', colspan: 2, align: 'center'},
					{header: 'INGRESO NAFIN', colspan: 3, align: 'center'}
				]
			]
	});
	
	//Elementos del grid de la consulta CONSOLIDADO
		var grid2 = new Ext.grid.GridPanel({
			store: 		registrosConsultadosData,
			id:			'gridConsulta2',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			title:		undefined,
			stripeRows: true,
			loadMask: 	true,
			height: 		400,
			width: 		940,
			plugins:		grupos2,
			frame: 		false,
			columns: [
				{
					header: 		'RFC',
					tooltip: 	'RFC',
					dataIndex: 	'RFC',
					sortable: 	true,
					align:		'left',
					resizable: 	true,
					width: 		120,
					hideable:	false
				},{
					header: 		'Nombre de PYME',
					tooltip: 	'Nombre de la PyME',
					dataIndex: 	'NOM_PYME',
					align:		'left',
					sortable: 	true,
					resizable: 	true,
					width: 		165,
					hideable:	false
				},{
					header: 		'EPO',
					tooltip: 	'EPO',
					dataIndex: 	'NOM_EPO',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		140,
					hideable:	false
				},{
					header: 		'Total Doctos.',
					tooltip: 	'Total de documentos',
					dataIndex: 	'TOT_DOC_CON',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hideable:	false
				},{
					header: 		'Fecha de Vencimiento',
					tooltip: 	'Fecha de vencimiento',
					dataIndex: 	'FEC_VEN',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hideable:	false
				},{
					header: 		'Fecha de Operaci�n',
					tooltip: 	'Fecha de operaci�n',
					dataIndex: 	'FEC_OPE',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					hideable:	false
				},{
					header: 		'Estatus',
					tooltip: 	'Estatus',
					dataIndex: 	'ESTATUS',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		150
				},{
					header: 		'Moneda',
					tooltip: 	'Moneda',
					dataIndex: 	'MONEDA',
					align:		'center',
					sortable: 	true,
					resizable: 	true,
					width: 		130
				},{
					header: 		'Monto Documentos',
					tooltip: 	'Monto de los documentos',
					dataIndex: 	'MONTO_DOC',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Recurso en Garant�a',
					tooltip: 	'Recurso en Garant�a',
					dataIndex: 	'REC_GARAN',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto Descuento',
					tooltip: 	'Monto del descuento',
					dataIndex: 	'MONTO_DESC',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto Int.',
					tooltip: 	'Monto Int.',
					dataIndex: 	'MONTO_INT_PYME',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto a Operar',
					tooltip: 	'Monto a operar',
					dataIndex: 	'MONTO_OPE_PYME',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto Int.',
					tooltip: 	'Monto Int.',
					dataIndex: 	'MONTO_INT_IF',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto a Operar',
					tooltip: 	'Monto a operar',
					dataIndex: 	'MONTO_OPE_IF',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Ingreso Neto IF',
					tooltip: 	'Ingreso Neto IF',
					dataIndex: 	'ING_NETO_IF',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		120,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Ingreso Neto FISO',
					tooltip: 	'Ingreso Neto FISO',
					dataIndex: 	'ING_NETO_FISO',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Ingreso por FONDEO',
					tooltip: 	'Ingreso por FONDEO',
					dataIndex: 	'ING_FONDEO',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		130,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Contraprestaci�n a EPO',
					tooltip: 	'Contraprestaci�n a EPO',
					dataIndex: 	'CONTRA_EPO',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		140,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Ingreso Neto NAFIN',
					tooltip: 	'Ingreso Neto NAFIN',
					dataIndex: 	'ING_NETO_NAFIN',
					align:		'right',
					sortable: 	true,
					resizable: 	true,
					width: 		140,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				}
			],
			bbar: {
				xtype: 			'paging',
				pageSize: 		15,
				buttonAlign: 	'left',
				id: 				'barraPaginacion2',
				displayInfo: 	true,
				store: 			registrosConsultadosData,
				displayMsg: 	'{0} - {1} de {2}',
				emptyMsg: 		"No hay registros.",
				items: [
					'->','-',
/*					{
						xtype:	'button',
						text:		'Imprimir PDF',
						id: 		'btnGenerarPDF2',
						iconCls:	'icoPdf',
						handler: function(boton, evento) {
										var estatus = Ext.getCmp('_estatus').getValue();
										boton.setIconClass('loading-indicator');
										var barraPaginacion = Ext.getCmp("barraPaginacion2");
										Ext.Ajax.request({
											url: '13ConsDocsFideicomiso.data.jsp',
											params:	Ext.apply(fp.getForm().getValues(),
												{
													informacion:'ArchivoXPDF',
													start: barraPaginacion.cursor,
													limit: barraPaginacion.pageSize,
													estatus: estatus,
													tipo: 'C'
												}
											),
											callback: procesarSuccessFailureGenerarPDF2
										});
						}
					},*/
					{
						xtype:   'button',
						iconCls: 'icoXls',
						text:    'Generar Archivo',
						id:      'btnArchivoCSV2',
						tooltip: 'Imprime los registros en formato CSV.',
						handler: function(boton, evento) {
							boton.disable();
							boton.setIconClass('loading-indicator');
							var estatus = Ext.getCmp('_estatus').getValue();
							Ext.Ajax.request({
								url: '13ConsDocsFideicomiso.data.jsp',
								params:	Ext.apply(fp.getForm().getValues(),{
									informacion:'ArchivoCSV',
									estatus: estatus,
									tipo: 'C'
								}),
								callback: procesarSuccessFailureArchivoCSV2
							});
						}
					},{
						xtype:   'button',
						text:    'Generar Todo',
						tooltip: 'Imprime los registros en formato PDF.',
						iconCls: 'icoPdf',
						id:      'btnGenerarPDF2',
						handler: function(boton, evento) {
							var estatus = Ext.getCmp('_estatus').getValue();
							boton.disable();
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
								url: '13ConsDocsFideicomiso.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion:'ArchivoXPDF',
									estatus: estatus,
									tipo: 'C'
								}),
								callback: procesarSuccessFailureGenerarPDF2
							});
						}
					}
				]
			}
	});
	
	var gruposTot = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 5, align: 'center'},
					{header: 'OPERACI�N PYME', colspan: 2, align: 'center'},
					{header: 'OPERACI�N INTERMEDIARIO FINANCIERO', colspan: 2, align: 'center'},
					{header: '', colspan: 3, align: 'center'}
				]
			]
	});
	
		var gridTot = new Ext.grid.GridPanel({
			store: 		totalesConsultadosData,
			id:			'gridTotalesConsulta',
			style: 		'margin: 0 auto; padding-top:10px;',
			hidden: 		true,
			margins:		'20 0 0 0',
			title:		'Aqui va la Fecha',
			//view:			new Ext.grid.GridView({forceFit:	true}),
			stripeRows: true,
			loadMask: 	true,
			height: 		143,
			width: 		940,
			plugins:		gruposTot,
			frame: 		false,
			columns: [
				{
					header: 		'Totales',
					tooltip: 	'Totales',
					dataIndex: 	'CVE_MONEDA',
					sortable: 	false,
					align:		'left',
					resizable: 	true,
					width: 		138,
					hideable:	false
				},{
					header: 		'Registros',
					tooltip: 	'Registros',
					dataIndex: 	'REGISTROS',
					sortable: 	false,
					align:		'center',
					resizable: 	true,
					width: 		85
				},{
					header: 		'Monto Documentos',
					tooltip: 	'Monto de los documentos',
					dataIndex: 	'MONTO_DOC_TOT',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		130,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto Recurso en Garant�a',
					tooltip: 	'Monto Recurso en Garant�a',
					dataIndex: 	'MONTO_REC_GARAN_TOT',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		148,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto Descuento',
					tooltip: 	'Monto del descuento',
					dataIndex: 	'MONTO_DESC_TOT',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		120,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto Int.',
					tooltip: 	'Monto Int.',
					dataIndex: 	'MONTO_INT_TOT_PYME',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		105,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto a Operar',
					tooltip: 	'Monto a operar',
					dataIndex: 	'MONTO_OPE_TOT_PYME',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		110,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto Int.',
					tooltip: 	'Monto Int.',
					dataIndex: 	'MONTO_INT_TOT_IF',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		110,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Monto a Operar',
					tooltip: 	'Monto a Operar',
					dataIndex: 	'MONTO_OPE_TOT_IF',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		120,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Ingreso Neto IF',
					tooltip: 	'Ingreso Neto IF',
					dataIndex: 	'ING_NETO_TOT_IF',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		120,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Ingreso Neto FISO',
					tooltip: 	'Ingreso Neto FISO',
					dataIndex: 	'ING_NETO_TOT_FISO',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		120,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				},{
					header: 		'Ingreso Neto NAFIN',
					tooltip: 	'Ingreso Neto NAFIN',
					dataIndex: 	'ING_NETO_TOT_NAFIN',
					sortable: 	true,
					align:		'right',
					resizable: 	true,
					width: 		127,
					hideable:	false,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				}
			]
		});
	
	var afiliacion = {
		xtype		: 'panel',
		id 		: 'afiliacion',
		items		: 
		[{
			layout		: 'form',
			width			:700,
			labelWidth	: 115,
			border		: false,
			items			:[{
					xtype: 'radiogroup',
					fieldLabel	: 'Tipo de Reporte',
					labelWidth	: 125,
					columns: 4,
					id: '_radio_selec',
					vertical: false,
					items: [
						{ boxLabel: 'Consolidado', name: 'rb', id: 'rb1', inputValue: 'B' },
						{ boxLabel: 'Detalle', name: 'rb', id: 'rb2', inputValue: 'NB', checked: true }
					],
					listeners:{
						//change: cambioRadio
						change:	function() {
								Ext.getCmp('_cmb_epo').reset();
								Ext.getCmp('_cmb_nom_if').reset();
								Ext.getCmp('_nae').reset();
								Ext.getCmp('_nom_prov').reset();
								Ext.getCmp('_ic_pyme').reset();
								Ext.getCmp('_num_doc').reset();
								Ext.getCmp('_fecha_noti_ini').reset();
								Ext.getCmp('_fecha_noti_fin').reset();
								Ext.getCmp('_fecha_ven_ini').reset();
								Ext.getCmp('_fecha_ven_fin').reset();
								Ext.getCmp('_fecha_ope_ini').reset();
								Ext.getCmp('_fecha_ope_fin').reset();
								Ext.getCmp('_monto_ini').reset();
								Ext.getCmp('_monto_fin').reset();
								Ext.getCmp('_cmb_moneda').reset();
								Ext.getCmp('_estatus').reset();
								if(Ext.getCmp('rb1').getValue()){
									Ext.getCmp('_cmb_nom_if').hide();
									Ext.getCmp('_num_doc').hide();
									Ext.getCmp('cmpst_fec_noti').hide();
									Ext.getCmp('cmpst_monto').hide();
									grid.hide();
									grid2.hide();
									gridTot.hide();
								}
								else{
									Ext.getCmp('_cmb_nom_if').show();
									Ext.getCmp('_num_doc').show();
									Ext.getCmp('cmpst_fec_noti').show();
									Ext.getCmp('cmpst_monto').show();
									grid.hide();
									grid2.hide();
									gridTot.hide();
								}
							}
					} 
				},{
					xtype				: 'combo',
					id					: '_cmb_epo',
					hiddenName 		:'cmb_epo', 
					fieldLabel		: 'Nombre de la EPO',
					emptyText: 'Seleccionar...',
					width				: 450,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	:'descripcion',
					store				: catalogoEPOs,
					listeners: {
									select: function(combo){
										if(Ext.getCmp('rb2').getValue()){
											Ext.getCmp('_cmb_nom_if').reset();
											var ic_epo = combo.getValue();
											catalogoIF.load({
												params: {
														ic_epo: ic_epo
												}
											});
										}
									}
					}
					
				}]
			}
	]};
	
	var datos_izq = {
		xtype			: 'fieldset',
		id 			: 'datos_izq',
		border		: false,
		labelWidth	: 105,
		items			: 
		[
			{
				xtype			: 'textfield',
				id          : '_nae',
				name	: 'nae',
				fieldLabel  : 'N@E',
				maskRe:		/[0-9]/,
				maxLength	: 20,
				margins		: '0 20 0 0',
				width			: 240,
				listeners:{
							'change':function(field){
									if ( !Ext.isEmpty(field.getValue()) ) {
										var num_naf = field.getValue();
										Ext.Ajax.request({
														url: '13ConsDocsFideicomiso.data.jsp',
														params: {
																informacion: 'CargaNom_Prov',
																num_Nae: num_naf
														},
														callback: procesarSuccessFailureNombreNae
										});
									} else{
										Ext.getCmp('_nom_prov').setValue('');
										Ext.getCmp('_ic_pyme').setValue('');
									}
							}
						}
			},
			{
				xtype			: 'textfield',
				id          : '_nom_prov',
				hiddenName	: 'nom_prov',
				fieldLabel  : 'Nombre de Proveedor',
				disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
				disabled:		true,
				maxLength	: 100,
				width			: 240,
				margins		: '0 20 0 0'
			},{
				xtype:'hidden',
				id:	'_ic_pyme',
				hiddenName	: 'ic_pyme',
				value:''
			},
			{
				xtype: 'compositefield',
				combineErrors: false,
				id: 'busqava',
				msgTarget: 'side',
				items: [
					{
						xtype:'displayfield',
						id:'disEspacio',
						text:''
					},
					{
						xtype:	'button',
						id:		'btnBuscaA',
						iconCls:	'icoBuscar',
						text:		'B�squeda Avanzada',
						handler: function(boton, evento) {
											var cveAfiliado = Ext.getCmp('_cmb_epo');
											var num_naf = Ext.getCmp('_nae').getValue();
											Ext.getCmp('_nae').reset();
											Ext.getCmp('_nom_prov').reset();
											var winVen = Ext.getCmp('winBuscaA');
											if (winVen){
												Ext.getCmp('fpWinBusca').getForm().reset();
												Ext.getCmp('fpWinBuscaB').getForm().reset();
												Ext.getCmp('cmb_num_ne').setValue();
												Ext.getCmp('cmb_num_ne').store.removeAll();
												Ext.getCmp('cmb_num_ne').reset();
												winVen.show();
											}else{
												var winBuscaA = new Ext.Window ({
													id:'winBuscaA',
													height: 364,
													x: 300,
													y: 100,
													width: 550,
													modal: true,
													closeAction: 'hide',
													title: 'B�squeda Avanzada',
													items:[
														{
															xtype:'form',
															id:'fpWinBusca',
															frame: true,
															border: false,
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 130,
															items:[
																{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:'Utilice el * para b�squeda gen�rica'
																},{
																	xtype:'displayfield',
																	frame:true,
																	border: false,
																	value:''
																},{
																	xtype:'hidden',
																	id:	'HicEpo',
																	value:''
																},{
																	xtype: 'textfield',
																	name: 'nombre_pyme',
																	id:	'txtNombre',
																	fieldLabel:'Nombre',
																	maxLength:	100
																},{
																	xtype: 'textfield',
																	name: 'rfc_prov',
																	id:	'_rfc_prov',
																	fieldLabel:'RFC',
																	maxLength:	20
																},{
																	xtype: 'textfield',
																	name: 'num_pyme',
																	id:	'txtNe',
																	fieldLabel:'N�mero Proveedor',
																	maxLength:	25
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Buscar',
																	iconCls:'icoBuscar',
																	handler: function(boton) {
																		Ext.getCmp('HicEpo').setValue(Ext.getCmp('_cmb_epo').getValue());
																		catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues())
																		});
																	}
	
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {
																					Ext.getCmp('winBuscaA').hide();
																					Ext.getCmp('winBuscaA').destroy();
																				}
																}
															]
														},{
															xtype:'form',
															frame: true,
															id:'fpWinBuscaB',
															style: 'margin: 0 auto',
															bodyStyle:'padding:10px',
															monitorValid: true,
															defaults: {
																msgTarget: 'side',
																anchor: '-20'
															},
															labelWidth: 80,
															items:[
																{
																	xtype: 'combo',
																	id:	'cmb_num_ne',
																	name: 'ic_pyme',
																	hiddenName : 'ic_pyme',
																	fieldLabel: 'Nombre',
																	emptyText: 'Seleccione. . .',
																	displayField: 'descripcion',
																	valueField: 'clave',
																	triggerAction : 'all',
																	forceSelection:true,
																	allowBlank: false,
																	typeAhead: true,
																	mode: 'local',
																	minChars : 1,
																	store: catalogoNombreProvData,
																	tpl : NE.util.templateMensajeCargaCombo
																}
															],
															buttonAlign:'center',
															buttons:[
																{
																	text:'Aceptar',
																	iconCls:'aceptar',
																	formBind:true,
																	handler: function() {
																				if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																					var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																					var cveP = disp.substr(0,disp.indexOf(" "));
																					var desc = disp.slice(disp.indexOf(" ")+1);
																					Ext.getCmp('_nae').setValue(cveP);
																					Ext.getCmp('_nom_prov').setValue(desc);
																					Ext.getCmp('winBuscaA').hide();

																					//Se obtiene el ic_pyme
																					Ext.Ajax.request({
																						url: '13ConsDocsFideicomiso.data.jsp',
																						params: {
																							informacion: 'CargaNom_Prov',
																							num_Nae: Ext.getCmp('_nae').getValue()
																						},
																						callback: procesarSuccessFailureNombreNae
																					});

																				}
																			}
																},{
																	text:'Cancelar',
																	iconCls: 'icoRechazar',
																	handler: function() {	
																		Ext.getCmp('winBuscaA').hide();	
																		Ext.getCmp('winBuscaA').destroy();
																	}
																}
															]
														}
													]
												}).show();
											}
										
									}
					}
				]
			},
			{
				xtype			: 'combo',
				id          : '_cmb_nom_if',
				hiddenName			: 'cmb_nom_if',
				fieldLabel  : 'Nombre del IF',
				forceSelection	: true,
				triggerAction	: 'all',
				emptyText: 'Seleccionar...',
				mode				: 'local',
				valueField		: 'clave',
				displayField	:'descripcion',
				width			: 240,
				margins		: '0 20 0 0',
				store: catalogoIF
			},
			{
				xtype			: 'textfield',
				id          : '_num_doc',
				name			: 'num_doc',
				fieldLabel  : 'N�mero de Documento',
				maxLength	: 16,
				maskRe:		/[0-9]/,
				width			: 160,
				margins		: '0 20 0 0'
			}
		]
	};
		
	var datos_der = {
		xtype			: 'fieldset',
		id 			: 'datos_der',
		border		: false,
		labelWidth	: 126,
		items			: 
		[
			{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de Notificaci�n',
				id: 'cmpst_fec_noti',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'datefield',
						name: 'fecha_noti_ini',
						id: '_fecha_noti_ini',
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoFinFecha: '_fecha_noti_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'al',
						width: 20
					},
					{
						xtype: 'datefield',
						name: 'fecha_noti_fin',
						id: '_fecha_noti_fin',
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoInicioFecha: '_fecha_noti_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			},
			{
					xtype				: 'combo',
					id					: '_cmb_moneda',
					hiddenName 		:'cmb_moneda', 
					fieldLabel		: 'Moneda',
					emptyText: 'Seleccionar...',
					width				: 275,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					margins: '0 20 0 0',
					displayField	:'descripcion',
					store: catalogoMoneda
			},{
				xtype: 'compositefield',
				fieldLabel: 'Monto',
				id: 'cmpst_monto',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'textfield',
						name: 'monto_ini',
						id: '_monto_ini',
						msgTarget: 'side',
						width: 100,
						maxLength	: 12,
						maskRe:		/[0-9]/,
						margins: '0 20 0 0'
					},
					{
						xtype: 'displayfield',
						value: 'a',
						width: 20
					},
					{
						xtype: 'textfield',
						name: 'monto_fin',
						id: '_monto_fin',
						msgTarget: 'side',
						maxLength	: 12,
						maskRe:		/[0-9]/,
						width: 100,
						margins: '0 20 0 0'
					}
				]
			},
			{
				xtype			: 'combo',
				id          : '_estatus',
				//name			: 'estatus',
				fieldLabel  : 'Estatus',
				emptyText: 'Seleccionar...',
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	:'descripcion',
				width			: 275,
				margins		: '0 20 0 0',
				store: catalogoEstatus
			},{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de Vencimiento',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'datefield',
						name: 'fecha_ven_ini',
						id: '_fecha_ven_ini',
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoFinFecha: '_fecha_ven_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'al',
						width: 20
					},
					{
						xtype: 'datefield',
						name: 'fecha_ven_fin',
						id: '_fecha_ven_fin',
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoInicioFecha: '_fecha_ven_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			},{
				xtype: 'compositefield',
				fieldLabel: 'Fecha de Operaci�n',
				combineErrors: false,
				msgTarget: 'side',
				//anchor:'100%',
				items: [
					{
						xtype: 'datefield',
						name: 'fecha_ope_ini',
						id: '_fecha_ope_ini',
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoFinFecha: '_fecha_ope_fin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'al',
						width: 20
					},
					{
						xtype: 'datefield',
						name: 'fecha_ope_fin',
						id: '_fecha_ope_fin',
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoInicioFecha: '_fecha_ope_ini',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			}
		]
	};
	
	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			840,
		style:			'margin:0 auto;',
		titleCollapse:	true,
		title: 'Reporte Operaciones con Fideicomiso',
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:[
			{
				layout	: 'hbox',
				title		: '',
				width		: 900,
				items		: [afiliacion ]
			},
			{
				layout	: 'hbox',
				id			: 'prueba',
				title 	: '',
				padding	:	'10 0 0 0',
				width		: 940,
				items		: [datos_izq	, datos_der	]
			}
		],
		monitorValid:	true,
		buttons: [
			{
				text:		'Consultar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				handler: function(boton, evento) {
								// El siguiente IF es solo para validar el formato de las fechas
								if(!Ext.getCmp('forma').getForm().isValid()){
									return;
								}
								var gr = Ext.getCmp('gridTotalesConsulta');
								var fechaIniNot = Ext.getCmp('_fecha_noti_ini');
								var fechaFinNot = Ext.getCmp('_fecha_noti_fin');
								var fechaIniVen = Ext.getCmp('_fecha_ven_ini');
								var fechaFinVen = Ext.getCmp('_fecha_ven_fin');
								var fechaIniOpe = Ext.getCmp('_fecha_ope_ini');
								var fechaFinOpe = Ext.getCmp('_fecha_ope_fin');
								var monto_i = Ext.getCmp('_monto_ini');
								var monto_f = Ext.getCmp('_monto_fin');
								if (gr.isVisible()) {
									gr.hide();
								}
								if(!Ext.isEmpty(fechaIniNot.getValue()) || !Ext.isEmpty(fechaFinNot.getValue())){
										if(Ext.isEmpty(fechaIniNot.getValue())){
											fechaIniNot.markInvalid('Debe capturar ambas fechas');
											return;
										}
										if(Ext.isEmpty(fechaFinNot.getValue())){
											fechaFinNot.markInvalid('Debe capturar ambas fechas');
											return;
										}
								}
								if(!Ext.isEmpty(monto_i.getValue()) || !Ext.isEmpty(monto_f.getValue())){
										if(Ext.isEmpty(monto_i.getValue())){
											monto_i.markInvalid('Debe capturar ambos montos');
											return;
										}
										if(Ext.isEmpty(monto_f.getValue())){
											monto_f.markInvalid('Debe capturar ambos montos');
											return;
										}
								}
								if(!Ext.isEmpty(fechaIniVen.getValue()) || !Ext.isEmpty(fechaFinVen.getValue())){
										if(Ext.isEmpty(fechaIniVen.getValue())){
											fechaIniVen.markInvalid('Debe capturar ambas fechas');
											return;
										}
										if(Ext.isEmpty(fechaFinVen.getValue())){
											fechaFinVen.markInvalid('Debe capturar ambas fechas');
											return;
										}
								}
								if(!Ext.isEmpty(fechaIniOpe.getValue()) || !Ext.isEmpty(fechaFinOpe.getValue())){
										if(Ext.isEmpty(fechaIniOpe.getValue())){
											fechaIniOpe.markInvalid('Debe capturar ambas fechas');
											return;
										}
										if(Ext.isEmpty(fechaFinOpe.getValue())){
											fechaFinOpe.markInvalid('Debe capturar ambas fechas');
											return;
										}
								}
								accionConsulta("CONSULTAR", null);
				} //fin handler
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR", null);
							}
			}
		]
	});
	
	
	//----------------------------------- CONTENEDOR -------------------------------------

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(5),
			grid,
			grid2,
			NE.util.getEspaciador(5),
			gridTot
		]
	});

	catalogoEPOs.load();
	catalogoMoneda.load();
	catalogoEstatus.load();

});