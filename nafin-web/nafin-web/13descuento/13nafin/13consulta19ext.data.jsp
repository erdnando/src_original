<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		java.math.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp" 
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String infoRegresar = "";
	InformacionTOIC informacionTOICBean = ServiceLocator.getInstance().lookup("InformacionTOICEJB", InformacionTOIC.class);
	JSONObject jsonObj = new JSONObject();
	if(informacion.equals("valoresIniciales")){
		String mensaje = "";
		if(strTipoUsuario.equals("EPO") && !informacionTOICBean.getExisteParametrizacionEpo(iNoCliente)){
			mensaje = "Estimado usuario usted no tiene parametrización. Es necesario parametrize para entrar a esta pantalla";
		}else{
			mensaje = "";
		}
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("mensaje", mensaje);
		jsonObj.put("strTipoUsuario", strTipoUsuario);
		infoRegresar = jsonObj.toString();
	}
	else if(informacion.equals("Consulta")){
		List docSIAFF = new ArrayList();
		String fini = (request.getParameter("df_fecha_carga_de") != null)?request.getParameter("df_fecha_carga_de"):"";
		String ffin = (request.getParameter("df_fecha_carga_a") != null)?request.getParameter("df_fecha_carga_a"):"";
		
		if(strTipoUsuario.equals("NAFIN")){
			docSIAFF = informacionTOICBean.getCargaSIAFF(fini, ffin);
		}else if(strTipoUsuario.equals("EPO")){
			docSIAFF = informacionTOICBean.getCargaSIAFFxEpo(fini, ffin, iNoCliente);
													
			/***codigo para fromar lsuat general***/
			if(docSIAFF != null && docSIAFF.size() > 0 ){
				String auxCarga = "";
				String auxArch = "";
				String auxUsua = "";
				String auxsiaff = "";
				List regAuxSiaff = new ArrayList();
				List regFinSiaff = new ArrayList();
				for(int x = 0; x < docSIAFF.size(); x++){
					List regSIAFFtmp = (List)docSIAFF.get(x);
	
					if(x == 0){
						regAuxSiaff.add((String)regSIAFFtmp.get(0));
						regAuxSiaff.add((String)regSIAFFtmp.get(1));
						regAuxSiaff.add((String)regSIAFFtmp.get(2));
						regAuxSiaff.add((String)regSIAFFtmp.get(3));
						regAuxSiaff.add((String)regSIAFFtmp.get(4));
						regAuxSiaff.add((String)regSIAFFtmp.get(5));
	
						if(x == docSIAFF.size() - 1){
							if(regAuxSiaff.size() == 6){
								if(((String)regAuxSiaff.get(4)).equals("N")){
									regAuxSiaff.add("S");
								}else{
									regAuxSiaff.add("N");
								}
								regAuxSiaff.add("0");
								regAuxSiaff.add((String)regSIAFFtmp.get(6));
							}
							regFinSiaff.add(regAuxSiaff);
						}
					}else{
						if(regAuxSiaff.size() == 6  && auxCarga.equals((String)regSIAFFtmp.get(1)) && auxArch.equals((String)regSIAFFtmp.get(2)) && auxUsua.equals((String)regSIAFFtmp.get(3))){
							regAuxSiaff.add((String)regSIAFFtmp.get(4));
							regAuxSiaff.add((String)regSIAFFtmp.get(5));
							regAuxSiaff.add(auxsiaff);
													
							regFinSiaff.add(regAuxSiaff);
							regAuxSiaff = new ArrayList();
						}else{
							if(regAuxSiaff.size() == 6){
								if(((String)regAuxSiaff.get(4)).equals("N")){
									regAuxSiaff.add("S");
								}else{
									regAuxSiaff.add("N");
								}
									regAuxSiaff.add("0");
									regAuxSiaff.add(auxsiaff);
									regFinSiaff.add(regAuxSiaff);
									regAuxSiaff = new ArrayList();
							}
							regAuxSiaff.add((String)regSIAFFtmp.get(0));
							regAuxSiaff.add((String)regSIAFFtmp.get(1));
							regAuxSiaff.add((String)regSIAFFtmp.get(2));
							regAuxSiaff.add((String)regSIAFFtmp.get(3));
							regAuxSiaff.add((String)regSIAFFtmp.get(4));
							regAuxSiaff.add((String)regSIAFFtmp.get(5));
	
							if(x == docSIAFF.size() - 1){
								if(regAuxSiaff.size() == 6){
									if(((String)regAuxSiaff.get(4)).equals("N")){
										regAuxSiaff.add("S");
									}else{
										regAuxSiaff.add("N");
									}
									regAuxSiaff.add("0");
									regAuxSiaff.add((String)regSIAFFtmp.get(6));
								}
								regFinSiaff.add(regAuxSiaff);
							}
						}
					}
					auxCarga = (String)regSIAFFtmp.get(1);
					auxArch = (String)regSIAFFtmp.get(2);
					auxUsua = (String)regSIAFFtmp.get(3);
					auxsiaff = (String)regSIAFFtmp.get(6);
				}//end for para recorrer resultado de consulta
				docSIAFF = regFinSiaff;
			}//end if si la regresa registros la consulta
			/*************************************/
		}//end if condicion si es EPO	
		List regSIAFF = new ArrayList();
		HashMap datos = new HashMap();
		List registros  = new ArrayList();
		
		if(docSIAFF != null && docSIAFF.size()>0 ){
			for(int x=0; x < docSIAFF.size(); x++){
				regSIAFF = (List)docSIAFF.get(x);
		
				datos = new HashMap();
				datos.put("TIPO_PROCESO", regSIAFF.get(0));
				datos.put("FECHA_CARGA", regSIAFF.get(1));
				datos.put("NOMBRE_ARCHIVO", regSIAFF.get(2));
				datos.put("NOMBRE_USUARIO", regSIAFF.get(3));
				
				if(!strTipoUsuario.equals("EPO")){
					datos.put("TIPO_CARGA", regSIAFF.get(9));
					datos.put("ICSIAFF", regSIAFF.get(8));
					datos.put("REGISTROS_CARGADOS", regSIAFF.get(4));
					
					datos.put("REGISTROS_ENCONTRADOS", regSIAFF.get(5));
					datos.put("REGISTROS_NO_ENCONTRADOS", regSIAFF.get(6));
					
					datos.put("REGISTRO_ERRORES", regSIAFF.get(7));					
					datos.put("ARCHIVO","NA");					
					
				}else  {
					
					int registrosCargados	= Integer.parseInt((String)regSIAFF.get(5)) + Integer.parseInt((String)regSIAFF.get(7));
					datos.put("TIPO_CARGA","");
					datos.put("ICSIAFF", regSIAFF.get(8));						
					datos.put("REGISTROS_CARGADOS", String.valueOf(registrosCargados) );
							
					if(((String)regSIAFF.get(4)).equals("N")){					
						datos.put("REGISTROS_ENCONTRADOS", regSIAFF.get(7));
						datos.put("REGISTROS_NO_ENCONTRADOS", regSIAFF.get(5));
					}else {
						datos.put("REGISTROS_ENCONTRADOS", regSIAFF.get(5));	
						datos.put("REGISTROS_NO_ENCONTRADOS", regSIAFF.get(7));	
					}											
				}			
			
				datos.put("ICSIAFF", (regSIAFF.get(8))!=null?(String)regSIAFF.get(8):"");
				datos.put("AUXSIAFF", regSIAFF.get(6));				
				
				registros.add(datos);
			}
		}
		
		
		jsonObj = new JSONObject();
		jsonObj.put("registros", registros);
		jsonObj.put("success",  new Boolean(true));
		jsonObj.put("strTipoUsuario", strTipoUsuario);
		infoRegresar = jsonObj.toString();
	

	}	else if(informacion.equals("ArchivoPlano")){
		try{
			String fini = (request.getParameter("df_fecha_carga_de") != null)?request.getParameter("df_fecha_carga_de"):"";
			String ffin = (request.getParameter("df_fecha_carga_a") != null)?request.getParameter("df_fecha_carga_a"):"";
			String cargaSiaff = (request.getParameter("cargaSiaff") != null)?request.getParameter("cargaSiaff"):"";
			String flag = (request.getParameter("flag") != null)?request.getParameter("flag"):"";
			String contenidoArchivo = informacionTOICBean.getArchivoPlanoSIAFF(strTipoUsuario,iNoCliente,fini,ffin,cargaSiaff,flag);
			
			CreaArchivo archivo 					= new CreaArchivo();
			String nombreArchivo					= null;
			
			if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")){
				infoRegresar = "{\"success\": true, \"urlArchivo\": \"\"}";	
			}else{
				nombreArchivo = archivo.nombre;
				infoRegresar = "{\"success\": true, \"urlArchivo\": \"" + strDirecVirtualTemp + nombreArchivo + "\"}";
			}
		}catch(Throwable e){
				throw new AppException("Error al intentar generar el archivo plano.", e);
		}
	}else if(informacion.equals("EliminarCarga")){
	
		String cargaSiaff = (request.getParameter("cargaSiaff") != null)?request.getParameter("cargaSiaff"):"";
		if(!cargaSiaff.equals("")){
			//informacionTOICBean.eliminarCargaSIAFF(Integer.parseInt(cargaSiaff));
		}
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));		
		infoRegresar = jsonObj.toString();
	}
%>
<%=infoRegresar%>