<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.io.*, 		
		javax.naming.*,
		java.text.*, 
		java.util.*,
		java.sql.*,
		com.netro.descuento.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		javax.naming.Context,		
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";

JSONObject jsonObj = new JSONObject();

InformacionTOIC informacionTOICBean = ServiceLocator.getInstance().lookup("InformacionTOICEJB", InformacionTOIC.class);
		

if(informacion.equals("ValidaCargaArchivo")) {
	
	String archivo = (request.getParameter("archivo") != null)?request.getParameter("archivo"):"";
	String tipo_carga = "M", 	boton_procesar= "", con_registro = "", sin_registro ="", duplicados = "", errores ="", total_registros  ="";
	int numeroProceso =0;

	String rutaArchivo = strDirectorioTemp + iNoUsuario + "." + archivo;
	//se lee el archivo
	String respuesta = informacionTOICBean.leeArchivo(rutaArchivo);
	
	if(respuesta.equals("S")) {
	
		numeroProceso = informacionTOICBean.getNumeroProcesoCargaSIAFF();//FODEA 034 - 2009 ACF
		
		informacionTOICBean.setCargaTmpMasivaSIAFF(rutaArchivo, numeroProceso, tipo_carga); 
				
		HashMap registros = new HashMap();
		registros= informacionTOICBean.validaArchivo(numeroProceso);
		
		boton_procesar  = registros.get("BOTON_PROCESAR").toString();
		con_registro  = registros.get("CON_REGISTRO").toString();
		sin_registro  = registros.get("SIN_REGISTRO").toString();
		duplicados  = registros.get("DUPLICADOS").toString();
		errores  = registros.get("ERRORES").toString();
		total_registros  = registros.get("TOTAL_REGISTROS").toString();
	
	}
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("boton_procesar", boton_procesar);
	jsonObj.put("con_registro", con_registro);
	jsonObj.put("sin_registro", sin_registro);
	jsonObj.put("duplicados", duplicados);
	jsonObj.put("errores", errores);
	jsonObj.put("total_registros", total_registros);	
	jsonObj.put("numeroProceso", String.valueOf(numeroProceso));
	jsonObj.put("respuesta", respuesta);
	jsonObj.put("nombreArchivo", archivo);
	infoRegresar = jsonObj.toString();	

}else if(informacion.equals("ProcesarCargaArchivo")) {
	String respuesta ="", mensaje="";
	String nombreArchivo = (request.getParameter("nombreArchivo") != null)?request.getParameter("nombreArchivo"):"";
	String numeroProceso = (request.getParameter("numeroProceso") != null)?request.getParameter("numeroProceso"):"";
	
	try{
		
		int numero_carga_siaff = informacionTOICBean.getNumeroCargaSIAFF();//FODEA 034 - 2009 ACF
		informacionTOICBean.procesarCargaSIAFF(Integer.parseInt(numeroProceso), numero_carga_siaff, iNoUsuario, nombreArchivo);
		respuesta = "Proceso de Carga del Archivo "+nombreArchivo+" con Exito";
		mensaje="Información cargada con éxito.";
	}catch(Exception e){
		e.printStackTrace();
		System.out.println("Error al procesar archivo:::"+e.getMessage());
		respuesta= "Error al Procesar el Achivo"+nombreArchivo;	
		mensaje="Error en la carga del archivo.";
	}	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta", respuesta);
	jsonObj.put("mensaje", mensaje);
	infoRegresar = jsonObj.toString();	

}

%>
<%=infoRegresar%>
