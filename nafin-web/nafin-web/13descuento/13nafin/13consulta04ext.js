Ext.onReady(function() {

	function procesarDescargaArchivos(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnArchivoPDF').setIconClass('icoPdf');
		Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');
	}

	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {	
				
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13consulta04.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [	
		   {	name: 'MONEDA' },
			{	name: 'NO_DOCUMENTOS'},	
			{	name: 'MONTO_TOTAL'},				
			{	name: 'MONT_DESCONTAR_TOTAL'}	
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);					
				}
			}
		}		
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Totales',	
		align: 'left',
		hidden: true,
		columns: [	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},		
			{
				header: 'N�mero de documentos ',
				tooltip: 'N�mero de documentos ',
				dataIndex: 'NO_DOCUMENTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},				
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO_TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONT_DESCONTAR_TOTAL',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
				header: '',
				tooltip: '',				
				sortable: true,
				width: 280,			
				resizable: true,				
				align: 'left'	
			}	
		],	
		stripeRows: true,
		loadMask: true,
		height: 150,
		width: 900,		
		frame: true	
	});
	
	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var gridConsulta = Ext.getCmp('gridConsulta');
		var el = gridConsulta.getGridEl();

		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}

			var jsonData = store.reader.jsonData;

			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();

				consultaTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotales'
					})
				});

				el.unmask();
			} else {
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaData   = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta04.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'NOMBRE_IF'},
			{	name: 'NOMBRE_EPO'},	
			{	name: 'NOMBRE_PROVEEDOR'},	
			{	name: 'NUM_SOLICITUD'},	
			{	name: 'MONEDA'},	
			{	name: 'MONTO'},			
			{	name: 'PORCE_DESC'},
			{  name: 'RECURSO_GARAN' },
			{	name: 'MONTO_DESCONTAR'},
			{	name: 'FECHA_REG'},
			{	name: 'TIPO_CAMBIO'},
			{	name: 'FECHA_CAMBIO'},
			{	name: 'CAUSA'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	

	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Proveedor',
				tooltip: 'Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'N�m. de Solicitud',
				tooltip: 'Num. de Solicitud',
				dataIndex: 'NUM_SOLICITUD',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORCE_DESC',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'RECURSO_GARAN',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Fecha Reg. Sol.',
				tooltip: 'Fecha Reg. Sol.',
				dataIndex: 'FECHA_REG',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Tipo Cambio de Estatus',
				tooltip: 'Tipo Cambio de Estatus',
				dataIndex: 'TIPO_CAMBIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'				
			},
			{
				header: 'Fecha Cambio de Estatus',
				tooltip: 'Fecha Cambio de Estatus.',
				dataIndex: 'FECHA_CAMBIO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},
			{
				header: 'Causa',
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'				
			}			
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: 'No hay registros.',
			items: [
				'->','-',
					{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime todos los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento){
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '13consulta04.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV'
							}),
							callback: procesarDescargaArchivos
						});
					}
				},{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime todos los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento){
							boton.setIconClass('loading-indicator');
							Ext.Ajax.request({
							url: '13consulta04.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								start: 0,
								limit: 0
							}),
							callback: procesarDescargaArchivos
						});
					}
				}
/*
	Fodea 011-2015
	La opci�n de generar el PDF por paginaci�n queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el m�todo por si se requiere nuevamente 
	Fecha: 29/06/2015
	BY: Agust�n Bautista Ruiz
*/
/*
				{
					xtype: 'button',
					text: 'Imprimir',
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacion");
							Ext.Ajax.request({
							url: '13consulta04.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								start: barraPaginacionA.cursor,
								limit: barraPaginacionA.pageSize
							}),
							callback: procesarDescargaArchivos
						});
					}
				}
*/
			]
		}
	});

	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04.data.jsp',
			baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
		}
	});
	

	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04.data.jsp',
			baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04.data.jsp',
			baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var catalogoPYME = new Ext.data.JsonStore({
		id: 'catalogoPYME',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04.data.jsp',
			baseParams: {
			informacion: 'catalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
		}
	});
	var catalogoEPO = new Ext.data.JsonStore({
			id: 'catalogoEPO',
			root : 'registros',
			fields : ['clave', 'descripcion', 'loadMsg'],
			url : '13consulta04.data.jsp',
			baseParams: {
				informacion: 'catalogoEPO'
			},
			totalProperty : 'total',
			autoLoad: false,
			listeners: {				
				exception: NE.util.mostrarDataProxyError
			}
		});

	var procesarBancoFondeo= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_banco_fondeo = Ext.getCmp('ic_banco_fondeo1');
			if(ic_banco_fondeo.getValue()==''){
				ic_banco_fondeo.setValue(records[1].data['clave']);
			}
		}
	 }
	 
	var catalogoBancoFondeo = new Ext.data.JsonStore({
		id: 'catalogoBancoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04.data.jsp',
		baseParams: {
			informacion: 'CatalogoBanco'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarBancoFondeo,
			exception: NE.util.mostrarDataProxyError
		}
	});
		
	var  elementosForma  = [
		{
			xtype: 'combo',
			name: 'ic_banco_fondeo',
			id: 'ic_banco_fondeo1',
			fieldLabel: 'Banco de Fondeo',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_banco_fondeo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			store : catalogoBancoFondeo,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
					
						var cmbEPO =  Ext.getCmp('ic_epo1');
						cmbEPO.setValue('');
						cmbEPO.store.load({
							params: {								
								ic_banco_fondeo:combo.getValue()
							}
						});
						
						var cmbIF = Ext.getCmp('ic_if1');
						cmbIF.setValue('');
						cmbIF.store.load({
							params: {								
								ic_banco_fondeo:combo.getValue()
							}
						});
						
					}
				}
			}
			
		},
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var cmbPYME= Ext.getCmp('ic_pyme1');
						cmbPYME.setValue('');
						cmbPYME.store.load({
							params: {								
								ic_epo:combo.getValue()
							}
						});
					}
				}
			}
		},
		{
			xtype: 'combo',
			name: 'ic_pyme',
			id: 'ic_pyme1',
			fieldLabel: 'Proveedor',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_pyme',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoPYME,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'ic_if1',
			fieldLabel: 'IF',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_if',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoIF,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'N�m. solicitud',		
			name: 'ic_folio',
			id: 'ic_folio1',
			allowBlank: true,
			maxLength: 11,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo de',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'numberfield',
					name: 'ig_plazoMin',
					id: 'ig_plazoMin',
					allowBlank: true,
					maxLength: 3,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangoValor',
					campoFinValor: 'ig_plazoMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'numberfield',
					name: 'ig_plazoMax',
					id: 'ig_plazoMax',
					allowBlank: true,
					maxLength: 3,
					width: 110,
					msgTarget: 'side',
					vtype: 'rangoValor',
					campoInicioValor: 'ig_plazoMin',
					margins: '0 20 0 0'	  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'combo',
			name: 'ic_moneda',
			id: 'ic_moneda1',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_moneda',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoMoneda,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'ic_cambio_estatus',
			id: 'ic_cambio_estatus1',
			fieldLabel: 'Tipo de estatus',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_cambio_estatus',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatus,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto de',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'numberfield',
					name: 'fn_montoMin',
					id: 'fn_montoMin',
					allowBlank: true,
					maxLength: 9,
					width: 110,
					msgTarget: 'side',
					vtype: 'rangoValor',
					campoFinValor: 'fn_montoMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'numberfield',
					name: 'fn_montoMax',
					id: 'fn_montoMax',
					allowBlank: true,
					maxLength: 9,
					width: 110,
					msgTarget: 'side',
					vtype: 'rangoValor',
					campoInicioValor: 'fn_montoMin',
					margins: '0 20 0 0'	  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de solicitud de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_solicitudMin',
					id: 'df_fecha_solicitudMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_solicitudMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_solicitudMax',
					id: 'df_fecha_solicitudMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_solicitudMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de cambio de estatus',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'dc_fecha_cambioMin',
					id: 'dc_fecha_cambioMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'dc_fecha_cambioMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'dc_fecha_cambioMax',
					id: 'dc_fecha_cambioMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'dc_fecha_cambioMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {

					if(!Ext.getCmp('forma').getForm().isValid()){
						return;
					}

					var ig_plazoMin = Ext.getCmp('ig_plazoMin');
					var ig_plazoMax = Ext.getCmp('ig_plazoMax');
				
					if(!Ext.isEmpty(ig_plazoMin.getValue()) || !Ext.isEmpty(ig_plazoMax.getValue())){
						if(Ext.isEmpty(ig_plazoMin.getValue())){
							ig_plazoMin.markInvalid('Debe capturar ambos plazos o dejarlos en blanco');
							ig_plazoMin.focus();
							return;
						}
						if(Ext.isEmpty(ig_plazoMax.getValue())){
							ig_plazoMax.markInvalid('Debe capturar ambos plazos o dejarlos en blanco');
							ig_plazoMax.focus();
							return;
						}
					}
					
					var fn_montoMin = Ext.getCmp('fn_montoMin');
					var fn_montoMax = Ext.getCmp('fn_montoMax');
					
					if(!Ext.isEmpty(fn_montoMin.getValue()) || !Ext.isEmpty(fn_montoMax.getValue())){
						if(Ext.isEmpty(fn_montoMin.getValue())){
							fn_montoMin.markInvalid('Debe capturar ambos Montos  o dejarlos en blanco');
							fn_montoMin.focus();
							return;
						}
						if(Ext.isEmpty(fn_montoMax.getValue())){
							fn_montoMax.markInvalid('Debe capturar ambos Montos o dejarlos en blanco');
							fn_montoMax.focus();
							return;
						}
					}					
					
					var df_fecha_solicitudMin = Ext.getCmp('df_fecha_solicitudMin');
					var df_fecha_solicitudMax = Ext.getCmp('df_fecha_solicitudMax');
					
					if(!Ext.isEmpty(df_fecha_solicitudMin.getValue()) || !Ext.isEmpty(df_fecha_solicitudMax.getValue())){
						if(Ext.isEmpty(df_fecha_solicitudMin.getValue())){
							df_fecha_solicitudMin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_solicitudMin.focus();
							return;
						}
						if(Ext.isEmpty(df_fecha_solicitudMax.getValue())){
							df_fecha_solicitudMax.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_solicitudMax.focus();
							return;
						}
					}
										
					var dc_fecha_cambioMin = Ext.getCmp('dc_fecha_cambioMin');
					var dc_fecha_cambioMax = Ext.getCmp('dc_fecha_cambioMax');
					
					if(!Ext.isEmpty(dc_fecha_cambioMin.getValue()) || !Ext.isEmpty(dc_fecha_cambioMax.getValue())){
						if(Ext.isEmpty(dc_fecha_cambioMin.getValue())){
							dc_fecha_cambioMin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							dc_fecha_cambioMin.focus();
							return;
						}
						if(Ext.isEmpty(dc_fecha_cambioMax.getValue())){
							dc_fecha_cambioMax.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							dc_fecha_cambioMax.focus();
							return;
						}
					}
					
					var df_fecha_solicitudMin_ = Ext.util.Format.date(df_fecha_solicitudMin.getValue(),'d/m/Y');
					var df_fecha_solicitudMax_ = Ext.util.Format.date(df_fecha_solicitudMax.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(df_fecha_solicitudMin.getValue())){
						if(!isdate(df_fecha_solicitudMin_)) { 
							df_fecha_solicitudMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_fecha_solicitudMin.focus();
							return;
						}
					}
					if( !Ext.isEmpty(df_fecha_solicitudMax.getValue())){
						if(!isdate(df_fecha_solicitudMax_)) { 
							df_fecha_solicitudMax.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_fecha_solicitudMax.focus();
							return;
						}
					}

					var dc_fecha_cambioMin_ = Ext.util.Format.date(dc_fecha_cambioMin.getValue(),'d/m/Y');
					var dc_fecha_cambioMax_ = Ext.util.Format.date(dc_fecha_cambioMax.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(dc_fecha_cambioMin.getValue())){
						if(!isdate(dc_fecha_cambioMin_)) { 
							dc_fecha_cambioMin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							dc_fecha_cambioMin.focus();
							return;
						}
					}
					if( !Ext.isEmpty(dc_fecha_cambioMax.getValue())){
						if(!isdate(dc_fecha_cambioMax_)) { 
							dc_fecha_cambioMax.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							dc_fecha_cambioMax.focus();
							return;
						}
					}
					fp.el.mask('Enviando...', 'x-mask-loading');
					Ext.getCmp('gridTotales').hide();
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15
						})
					});
				}
			},{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta04ext.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20)			
		]
	});
	
	catalogoBancoFondeo.load();
	catalogoEPO.load();
	catalogoIF.load();
	catalogoMoneda.load();
	catalogoEstatus.load();
	  
});	
