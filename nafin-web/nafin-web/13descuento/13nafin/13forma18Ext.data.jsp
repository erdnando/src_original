<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		javax.naming.Context,
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoIfContratoElectronico,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";

log.debug("informacion = <"+informacion+">");

if (	informacion.equals("CatalogoIf") ) {
	
	CatalogoIF cat = new CatalogoIF();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setG_producto("1");
	if(strPerfil.equalsIgnoreCase("ADMIN NAFIN")){
		cat.setG_fondeo("1");
	}
	if(iTipoPerfil.equalsIgnoreCase("8")){
		cat.setG_fondeo("2");
	}
	cat.setOrden("2");

	infoRegresar = cat.getJSONElementos();

}else if (	informacion.equals("CatalogoIfConsulta") ) {
	
	CatalogoIfContratoElectronico cat = new CatalogoIfContratoElectronico();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	if(strPerfil.equalsIgnoreCase("ADMIN NAFIN")){
		cat.setBancoFondeo("1");
	} if(iTipoPerfil.equalsIgnoreCase("8")){
	  cat.setBancoFondeo("2");
	}
	cat.setOrden("2");

	infoRegresar = cat.getJSONElementos();

} else if (        	informacion.equals("AceptaCapura.subirArchivo") 				)	{
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);

	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;

	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {

		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(4194304);
		myUpload.upload();

	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 4194304) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 4 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
	}

	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 	= myUpload.getRequest();
	String tipoRegistro 							= (myRequest.getParameter("tipoRegistro")	== null)?"":myRequest.getParameter("tipoRegistro");
	String claveIf									= (myRequest.getParameter("ic_if")==null)?"":myRequest.getParameter("ic_if");

	com.jspsmart.upload.File archivo 		= null;
	String extension								= null;
	String nombreArchivo							= null;
	String imagen									= null;
	String doctoActualizado						= "N";	
	String consecutivo							= null;

	if ("ACTUALIZAR".equals(tipoRegistro)) {
		consecutivo			= (myRequest.getParameter("ic_consecutivo")==null)?"":myRequest.getParameter("ic_consecutivo");
		doctoActualizado	= (myRequest.getParameter("doctoActualizado")==null)?"N":myRequest.getParameter("doctoActualizado");
	}

	try {

		if ( "ACTUALIZAR".equals(tipoRegistro) && doctoActualizado.equals("N") ) { //Dado que no hay archivo nuevo se mantiene el actual (Se obtiene de BD)
			ContratoIFArchivo contratoArchivo = new ContratoIFArchivo();
			contratoArchivo.setClaveIF(claveIf);
			contratoArchivo.setConsecutivo(consecutivo);
			nombreArchivo = contratoArchivo.getArchivoContratoIF(strDirectorioTemp);
			extension = nombreArchivo.substring(nombreArchivo.length()-3).toLowerCase();
		} else {
			archivo			= myUpload.getFiles().getFile(0);
			//Nombre del Archivo temporal
			extension		= archivo.getFileExt().toLowerCase();
			nombreArchivo	= Comunes.cadenaAleatoria(8) + "." + extension;
			archivo.saveAs(strDirectorioTemp + nombreArchivo);
		}

	}catch(Exception e){

		success		= false;
		log.error("AceptaCapura.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}

	resultado.put("clausulado_preview",	strDirecVirtualTemp+nombreArchivo	);
	resultado.put("consecutivo",			consecutivo									);
	resultado.put("doctoActualizado",	doctoActualizado							);
	resultado.put("extension",				extension									);
	resultado.put("tipoRegistro",			tipoRegistro								);
	resultado.put("nombreArchivo",		nombreArchivo								);

	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 	"" 	);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)		);

	infoRegresar = resultado.toString();

} else if (        	informacion.equals("AceptaCapura.Aceptar") 				)	{

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	ParametrosDescuento paramDsctoEJB = null;
	try {

		paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	}catch(Exception e){
		success		= false;
		log.error("AceptaCapura.Aceptar(Exception): Obtener instancia del EJB de ParametrosDescuento");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ParametrosDescuento.");

	}

	String tipoRegistro		= (request.getParameter("tipoRegistro")==null)?"":request.getParameter("tipoRegistro");
	
	String consecutivo		= null;
	String doctoActualizado	= null;

	if ("ACTUALIZAR".equals(tipoRegistro)) {
		consecutivo				= request.getParameter("ic_consecutivo");
		doctoActualizado		= request.getParameter("doctoActualizado");
	}

	String claveIf				= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
	String tituloAviso		= (request.getParameter("cg_titulo_aviso") == null)?"":request.getParameter("cg_titulo_aviso");
	String contenidoAviso	= (request.getParameter("cg_contenido_aviso") == null)?"":request.getParameter("cg_contenido_aviso");
	String botonAviso			= (request.getParameter("cg_boton_aviso")==null)?"":request.getParameter("cg_boton_aviso");
	String textoAceptacion	= (request.getParameter("cg_texto_aceptacion")==null)?"":request.getParameter("cg_texto_aceptacion");
	String botonAceptacion	= (request.getParameter("cg_boton_aceptacion") == null)?"":request.getParameter("cg_boton_aceptacion");
	String mostrar				= (request.getParameter("cs_mostrar")==null)?"N":request.getParameter("cs_mostrar");
	mostrar						= (mostrar.equals("on"))?"S":"N";
	String nombreArchivo		= (request.getParameter("nombreArchivo")==null)?"N":request.getParameter("nombreArchivo");
	String rutaArchivo		= strDirectorioTemp + nombreArchivo;

	ContratoIF contrato = new ContratoIF();
	contrato.setClaveIF(claveIf);

	if ("ACTUALIZAR".equals(tipoRegistro)) {
		contrato.setConsecutivo(consecutivo);
	}

	contrato.setTituloAviso(tituloAviso);
	contrato.setContenidoAviso(contenidoAviso);
	contrato.setBotonAviso(botonAviso);
	contrato.setTextoAceptacion(textoAceptacion);
	contrato.setBotonAceptacion(botonAceptacion);
	contrato.setMostrar(mostrar);

	if ("ACTUALIZAR".equals(tipoRegistro)) {
		//Se envía null en la ruta del archivo si este no fue modificado
		paramDsctoEJB.actualizarClausuladoIF( contrato, ((doctoActualizado.equals("S"))?rutaArchivo:null) );
	} else if ("NUEVO".equals(tipoRegistro)) {
		consecutivo = paramDsctoEJB.guardarClausuladoIF(contrato, rutaArchivo);
	}

	List registros = paramDsctoEJB.getDatosClausuladoIF(claveIf, consecutivo);
	//solo trae un registro la lista.
	ContratoIF contratoGuardado = (ContratoIF) registros.get(0);

	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");

	List regs = new ArrayList();
	HashMap hash = new HashMap();
	hash.put("NOMBRE_IF",		contratoGuardado.getNombreIF()					);
	hash.put("DOCUMENTO",		strDirecVirtualTemp+nombreArchivo				);
	hash.put("FECHA_ALTA",		sdf.format(contratoGuardado.getFechaAlta())	);
	hash.put("ACTIVA",			contratoGuardado.getMostrar()						);
	regs.add(hash);

	jsonObj.put("registrosCapturadosData",	regs										);

	jsonObj.put("estadoSiguiente",	"RESPUESTA_ACEPTA_CAPTURA"					);
	jsonObj.put("success",				new Boolean(success)							);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Consultar") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	ParametrosDescuento paramDsctoEJB = null;
	try {

		paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	}catch(Exception e){
		success		= false;
		log.error("Consultar(Exception): Obtener instancia del EJB de ParametrosDescuento");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ParametrosDescuento.");

	}

	String claveIf			= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
	String consecutivo	= null;

	List registros = paramDsctoEJB.getDatosClausuladoIF(claveIf, consecutivo);
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");

	List regs = new ArrayList();

	Iterator it = registros.iterator();
	while (it.hasNext()) {
		ContratoIF contrato = (ContratoIF)it.next();
		HashMap hash = new HashMap();
		hash.put("CONSECUTIVO",		contrato.getConsecutivo()					);
		hash.put("CLAVE_IF",			contrato.getClaveIF()						);
		hash.put("NOMBRE_IF",		contrato.getNombreIF()						);
		hash.put("FECHA_ALTA",		sdf.format(contrato.getFechaAlta())		);
		hash.put("ACTIVA",			contrato.getMostrar()						);
		hash.put("MODIFICAR",		new Boolean(contrato.esUltimo())			);
		regs.add(hash);
	} //fin while

	jsonObj.put("registrosConsultadosData",	regs								);
	jsonObj.put("estadoSiguiente",				"RESPUESTA_CONSULTA"			);
	jsonObj.put("success",							new Boolean(success));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Accion.modificar") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	ParametrosDescuento paramDsctoEJB = null;
	try {

		paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	}catch(Exception e){
		success		= false;
		log.error("Consultar(Exception): Obtener instancia del EJB de ParametrosDescuento");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ParametrosDescuento.");

	}

	String claveIf			= (request.getParameter("clave_if")==null)?"":request.getParameter("clave_if");
	String consecutivo	= (request.getParameter("consecutivo")==null)?"":request.getParameter("consecutivo");

	List registros			= paramDsctoEJB.getDatosClausuladoIF(claveIf, consecutivo);
	ContratoIF contratoGuardado = (ContratoIF) registros.get(0);
	
	
	jsonObj.put("tipoRegistro",			"ACTUALIZAR"									);
	jsonObj.put("ic_if",						contratoGuardado.getClaveIF()				);
	jsonObj.put("ic_consecutivo",			contratoGuardado.getConsecutivo()		);
	jsonObj.put("doctoActualizado",		"N"												);
	jsonObj.put("nombreIf",					contratoGuardado.getNombreIF()			);
	jsonObj.put("cg_titulo_aviso",		contratoGuardado.getTituloAviso()		);
	jsonObj.put("cg_contenido_aviso",	contratoGuardado.getContenidoAviso()	);
	jsonObj.put("cg_boton_aviso",			contratoGuardado.getBotonAviso()			);
	jsonObj.put("cg_texto_aceptacion",	contratoGuardado.getTextoAceptacion()	);
	jsonObj.put("cg_boton_aceptacion",	contratoGuardado.getBotonAceptacion()	);
	jsonObj.put("cs_mostrar",				contratoGuardado.getMostrar()				);

	jsonObj.put("estadoSiguiente",		"RESPUESTA.ACCION.MODIFICAR"				);
	jsonObj.put("success",					new Boolean(success)							);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Accion.abrir_clausulado") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	String claveIF		= (request.getParameter("clave_if")==null)?"":request.getParameter("clave_if");
	String consecutivo	= (request.getParameter("consecutivo")==null)?"":request.getParameter("consecutivo");

	ContratoIFArchivo contratoArchivo = new ContratoIFArchivo();
	contratoArchivo.setClaveIF(claveIF);
	contratoArchivo.setConsecutivo(consecutivo);
	String nombreArchivo = contratoArchivo.getArchivoContratoIF(strDirectorioTemp);

	jsonObj.put("nombreArchivo",		strDirecVirtualTemp + nombreArchivo		);
	jsonObj.put("estadoSiguiente",	"RESPUESTA.ACCION.ABRIR_CLAUSULADO"		);
	jsonObj.put("success",				new Boolean(success)							);
	infoRegresar = jsonObj.toString();

}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>