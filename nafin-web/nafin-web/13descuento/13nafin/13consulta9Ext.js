Ext.onReady(function(){

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -
function numDias(d,m,a){
  m = (m + 9) % 12;
  a = a - Math.floor(m/10);
  return 365*a+Math.floor(a/4)-Math.floor(a/100)+Math.floor(a/400)
            +Math.floor((m*306+5)/10)+d-1 
}
function difDias(d1,m1,a1,d2,m2,a2){
   return numDias(d2,m2,a2) - numDias(d1,m1,a1)
}
	var procesarSuccessFailureGenerarPDF = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}
function procesarArchivoSuccess(opts, success, response) {
var btnArchivoCSV = Ext.getCmp('btnGenerarArchivo');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			Ext.getCmp('btnGenerarArchivo').enable();
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		btnArchivoCSV.setIconClass('icoXls');
	}
var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		var boton = Ext.getCmp('btnGenerarArchivo');
		var botonPDF = Ext.getCmp('btnGenerarPDF');
		
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
				boton.enable();
				botonPDF.enable();
				resumenTotalesData.load();
				var totalesCmp = Ext.getCmp('gridTotalesE');
				totalesCmp.show();
				
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				boton.disable();
				botonPDF.disable();
				
			}
		}
	}
function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}

function procesarArchivos(opts, success, response) {
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var infoR = Ext.util.JSON.decode(response.responseText);
		var archivo = infoR.urlArchivo;				
		archivo = archivo.replace('/nafin','');
		var params = {nombreArchivo: archivo};				
		fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
		fp.getForm().getEl().dom.submit();
	}else {
		NE.util.mostrarConnError(response,opts);
	}
}

var verDetalle = function (grid,rowIndex,colIndex,item){

		var registro = grid.getStore().getAt(rowIndex);
		var claveSolicitud = registro.get('NUMFOLIO');

		Ext.Ajax.request({
			url: '13consulta9Ext.data.jsp',
			params: {
				informacion: 'ArchivoDetPDF',
				claveSolicitud:claveSolicitud
			},
			callback: procesarArchivos
		});
	}

/////////////////////////////////////////////////////////////////////////////////77
var accionConsulta = function(estadoSiguiente, respuesta){
 if(  estadoSiguiente == "CONSULTAR" 											){

			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
			
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("grid").hide();
			Ext.getCmp("gridTotalesE").hide();
						consultaGrid.load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consulta',
								operacion: 'Generar',
								start: 0,
								limit: 15
							}
				)
			});

		} else if(	estadoSiguiente == "LIMPIAR"														){

			Ext.getCmp('forma').getForm().reset();
			grid.hide();
			gridTotalesE.hide();

		}
		else if(	estadoSiguiente == "FIN"															){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "13consulta9Ext.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}
//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

	var catalogoIF = new Ext.data.JsonStore({
		id:				'catalogoIFDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'13consulta9Ext.data.jsp',
		baseParams:		{	informacion: 'catalogoIF'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatus = new Ext.data.JsonStore({
		id:				'catalogoEstatusDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'13consulta9Ext.data.jsp',
		baseParams:		{	informacion:	'CatalogoEstatus'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
var consultaGrid = new Ext.data.JsonStore({
	root: 'registros',
	url:'13consulta9Ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
		
	},
	fields: [
				{name: 'NOMBREPYME'},
				{name: 'CLAVESIRAC'},
				{name: 'NUMDOCTO'},
				{name: 'FECHAETC'},
				{name: 'FECHADSCTO'},
				{name: 'NOMBREMONEDA'},
				{name: 'DESCTIPOCRED'},
				{name: 'IMPORTEDOCTO'},
				{name: 'IMPORTEDSCTO'},
				{name: 'NUMFOLIO'},
				{name: 'FECHAOPERACION'},
				{name: 'NUMPRESTAMO'},
				{name: 'NUMACUSE'},
				{name: 'NOMBREIF'},
				{name: 'PERIODICIDAD_PAGO'},
				{name: 'FECHAENVIOIF'},
				{name: 'ERRORPROCESO'},
				{name: 'CODIGOAPLICACION'},
				{name: 'ERRORDESCRIPCION'},
				{name: 'TIPOCRED'}
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
	
beforeLoad:	{fn: function(store, options){
						//Ext.apply(options.params, {Ext.apply(  fp.getForm().getValues())});
						options.params=Ext.apply(options.params,fp.getForm().getValues());
						}	},
	
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							//procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});
var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta9Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'OPERCREDELENAFINDE',							mapping: 'OPERCREDELENAFINDE'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
///////////////////////////////////////////////////////////////
var gridTotalesE =  new Ext.grid.GridPanel({
		store:resumenTotalesData,	id:'gridTotalesE',	
		view:new Ext.grid.GridView({forceFit:true}),	
		width:930,	height:75,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'Total de Solicitudes:',	dataIndex: 'OPERCREDELENAFINDE',	align: 'left', menuDisabled:true
				}		
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	});

	var grid = new Ext.grid.GridPanel({
		id:           'grid',
		hidden:       true,
		store:        consultaGrid,
		style:        'margin:0 auto;',
		columns:[{
			header:    'Nombre del Cliente',
			tooltip:   'Nombre del Cliente',
			sortable:  true,
			dataIndex: 'NOMBREPYME',
			width:     150,
			align:     'left'
		},{
			header:    'N�mero de Sirac',
			tooltip:   'N�mero de Sirac',
			sortable:  true,
			dataIndex: 'CLAVESIRAC',
			width:     150,
			align:     'center'
		},{
			header:    'N�mero de Documento',
			tooltip:   'N�mero de Documento',
			sortable:  true,
			dataIndex: 'NUMDOCTO',
			width:     130,
			align:     'center',
			renderer:  function (value, metadata, registro) {
				var valor= registro.get('NUMDOCTO');
				if(valor>0) {
					return value;
				} else {
					return '';
				}
			}
		},{
			align:     'center',
			header:    'Periodicidad de Pago de Capital',
			tooltip:   'Periodicidad de Pago de Capital',
			sortable:  true,
			dataIndex: 'PERIODICIDAD_PAGO',
			width:     130
		},{
			header:    'Fecha Emisi�n T�tulo de Cr�dito',
			tooltip:   'Fecha Emisi�n T�tulo de Cr�dito',
			sortable:  true,
			dataIndex: 'FECHAETC',
			align:     'center',
			width:     130
		},{
			header:    'Fecha Vencimiento Descuento',
			tooltip:   'Fecha Vencimiento Descuento',
			sortable:  true,
			dataIndex: 'FECHADSCTO',
			width:     150,
			align:     'center'
		},{
			header:    'Moneda',
			tooltip:   'Moneda',
			sortable:  true,
			dataIndex: 'NOMBREMONEDA',
			width:     130,
			align:     'left'
		},{
			header:    'Tipo de Cr�dito',
			tooltip:   'Tipo de Cr�dito',
			sortable:  true,
			dataIndex: 'DESCTIPOCRED',
			width:     130,
			align:     'left'
		},{
			header:    'Monto del Documento',
			tooltip:   'Monto del Documento',
			sortable:  true,
			dataIndex: 'IMPORTEDOCTO',
			width:     150,
			align:     'right',
			renderer:  function (value, metadata, registro) {
				var valor= registro.get('IMPORTEDOCTO');
				if(valor>0) {
					return Ext.util.Format.number(value, '$0,0.00');
				} else {
					return '';
				}
			}
		},{
			header:    'Monto del Descuento',
			tooltip:   'Monto del Descuento',
			sortable:  true,
			dataIndex: 'IMPORTEDSCTO',
			width:     150,
			align:     'right',
			renderer:  Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header:    'N�mero de Folio',
			tooltip:   'N�mero de Folio',
			sortable:  true,
			dataIndex: 'NUMFOLIO',
			align:     'center',
			width:     150
		},{
			header:    'Fecha de Operaci�n',
			tooltip:   'Fecha de Operaci�n',
			sortable:  true,
			align:     'center',
			dataIndex: 'FECHAOPERACION',
			width:     150
		},{
			header:    'N�mero de Pr�stamo',
			tooltip:   'N�mero de Pr�stamo',
			sortable:  true,
			dataIndex: 'NUMPRESTAMO',
			align:     'center',
			width:     150,
			renderer:  function (value, metaData, record, rowIndex, colIndex, store){
				if(value=='0') {
					return '';
				}
				return value;
			}
		},{
			header:    'N�mero de Acuse',
			tooltip:   'N�mero de Acuse',
			sortable:  true,
			dataIndex: 'NUMACUSE',
			align:     'center',
			width:     150
		},{
			header:    'Fecha de Env�o del IF',
			tooltip:   'Fecha de Env�o del IF',
			sortable:  true,
			dataIndex: 'FECHAENVIOIF',
			align:     'center',
			width:     150
		},{
			header:    'C�digo de Error',
			tooltip:   'C�digo de Error',
			sortable:  true,
			dataIndex: 'CODIGOAPLICACION',//ERRORPROCESO
			align:     'center',
			width:     150,
			renderer:  function (value, metaData, record, rowIndex, colIndex, store){
				if(value!='') {
					return value+record.data.ERRORPROCESO;
				}
				return value;
			}
		},{
			header:    'Descripci�n',
			tooltip:   'Descripci�n',
			sortable:  true,
			align:     'left',
			dataIndex: 'ERRORDESCRIPCION',
			width:     150
		},{
			header:    'Intermediario',
			tooltip:   'Intermediario',
			sortable:  true,
			align:     'left',
			dataIndex: 'NOMBREIF',
			width:     150
		},{
			xtype:     'actioncolumn',
			header:    'Detalle de Operaci�n',
			tooltip:   'Detalle de Operaci�n',
			dataIndex: 'TIPOCRED',
			sortable:  true,
			width:     150,
			resizable: true,
			align:     'center',
			hidden:    false,
			renderer:  function(value,metaData,registro,rowIndex,colIndex,store){
				return value;
			},
			items: [{
				getClass: function(value,metadata,registro,rowIndex,colIndex,store){
					this.items[0].tooltip = 'Ver';
					return 'icoPdf';
				}
				,handler: verDetalle
			}]
		}],
		stripeRows:     true,
		loadMask:       true,
		height:         400,
		width:          940,
		style:          'margin:0 auto;',
		frame:          true,
		bbar: {
			xtype:       'paging',
			autoScroll:  true,
			//height:    30,
			pageSize:    15,
			buttonAlign: 'left',
			id:          'barraPaginacion',
			displayInfo: true,
			store:       consultaGrid,
			displayMsg:  '{0} - {1} de {2}',
			emptyMsg:    'No hay registros.',
			items: ['->','-',{
				xtype:    'button',
				text:     'Generar Archivo',
				id:       'btnGenerarArchivo',
				iconCls:  'icoXls',
				handler:  function(boton, evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta9Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'
						}), callback: procesarArchivoSuccess
					});
				}
			},'-',{
				xtype:    'button',
				text:     'Generar Todo',
				tooltip:  'Imprime todos los registros en formato PDF.',
				id:       'btnGenerarPDF',
				iconCls:  'icoPdf',
				handler:  function(boton, evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta9Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoPDF',
							start: 0,
							limit: 0
						}), callback: procesarSuccessFailureGenerarPDF
					});
				}
			}
/*
	Fodea 011-2015
	La opci�n de generar el PDF por paginaci�n queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el m�todo por si se requiere nuevamente 
	Fecha: 29/06/2015
	BY: Agust�n Bautista Ruiz
*/
/*
			{
				xtype:    'button',
				text:     'Generar PDF',
				id:       'btnGenerarPDF',
				iconCls:  'icoPdf',
				handler:  function(boton, evento){
					boton.disable();
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13consulta9Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoXPDF',
							start: barraPaginacion.cursor,
							limit: barraPaginacion.pageSize
						}), callback: procesarSuccessFailureGenerarPDF
					});
				}
			}
*/
		]}
	});

//-------------------------------------------
var elementosForma = [
		{
			xtype:			'combo',
			id:				'id_ic_if',
			name:				'ic_if',
			hiddenName:		'ic_if',
			fieldLabel:		'*Intermediario Financiero',
			emptyText:		'Seleccionar. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			typeAhead:		true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoIF,
			tpl:				NE.util.templateMensajeCargaCombo
			
		},{
			xtype: 'compositefield',
			fieldLabel: '*Fecha Env�o de IF',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype:			'datefield',
					name:				'df_fecha_envio_if_de',
					id:				'_df_fecha_envio_if_de',
					allowBlank:		false,
					startDay:		0,
					width:			100,
					msgTarget:		'side',
					vtype:			'rangofecha', 
					campoFinFecha:	'_df_fecha_envio_if_a',
					margins:			'0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 25
				},
				{
					xtype:				'datefield',
					name:					'df_fecha_envio_if_a',
					id:					'_df_fecha_envio_if_a',
					allowBlank:			false,
					startDay:			1,
					width:				100,
					msgTarget:			'side',
					vtype:				'rangofecha', 
					campoInicioFecha:	'_df_fecha_envio_if_de',
					margins:				'0 20 0 0'  //necesario para mostrar el icono de error
					}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype:			'datefield',
					name:				'df_fecha_operacion_de',
					id:				'_df_fecha_operacion_de',
					allowBlank:		true,
					startDay:		0,
					width:			100,
					msgTarget:		'side',
					vtype:			'rangofecha', 
					campoFinFecha:	'_df_fecha_operacion_a',
					margins:			'0 20 0 0'  //necesario para mostrar el icono de error
					},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 25
				},
				{
					xtype:				'datefield',
					name:					'df_fecha_operacion_a',
					id:					'_df_fecha_operacion_a',
					allowBlank:			true,
					startDay:			1,
					width:				100,
					msgTarget:			'side',
					vtype:				'rangofecha', 
					campoInicioFecha:	'_df_fecha_operacion_de',
					margins:				'0 20 0 0'  //necesario para mostrar el icono de error
					}
			]
		},
		{
			xtype: 'numberfield',
			id: 'id_ic_numero_sirac',
			name: 'ic_numero_sirac',
			fieldLabel: "N�mero Sirac",
			maxLength: 12
		},
		{
			xtype: 'textfield',
			id: 'id_ic_numero_acuse',
			name: 'ic_numero_acuse',
			fieldLabel: "N�mero de Acuse",
			maxLength: 22
		},
		{
			xtype: 'numberfield',
			id: 'id_ic_numero_prestamo',
			name: 'ic_numero_prestamo',
			fieldLabel: "N�mero de Pr�stamo",
			maxLength: 12
		},				
		{
			xtype:			'combo',
			id:				'id_ic_estatus',
			name:				'ic_estatus',
			hiddenName:		'ic_estatus',
			fieldLabel:		'Estatus',
			emptyText:		'Seleccionar. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:true,
			typeAhead:		true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoEstatus,
			tpl:				NE.util.templateMensajeCargaCombo
		}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		title:			'Criterios de b�squeda',
		width:			610,
		style:			'margin:0 auto;',
		collapsible:	true,
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		//monitorValid:	true,
		buttons: [
			{
				text:		'Consultar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:true,
				handler: function(boton, evento) {

								if(!Ext.getCmp('forma').getForm().isValid()){
									return;
								}
								if(Ext.getCmp('id_ic_if').getValue()=='')
								{
								Ext.getCmp('id_ic_if').markInvalid('El IF es obligatorio');
									//Ext.MessageBox.alert("error",
									//"El IF es obligatorio");
								return;

								}
								var fechMinEnvIF= Ext.getCmp('_df_fecha_envio_if_de').getValue();
								var fechMaxEnvIF= Ext.getCmp('_df_fecha_envio_if_a').getValue();
								var fechMinOper= Ext.getCmp('_df_fecha_operacion_de').getValue();
								var fechMaxOper= Ext.getCmp('_df_fecha_operacion_a').getValue();

							if(!NE.util.validatorDate(Ext.getCmp('_df_fecha_envio_if_de'),Ext.getCmp('_df_fecha_envio_if_a'),true)){
								Ext.getCmp('_df_fecha_envio_if_de').markInvalid('El rango de Fechas de Envio no debe ser mayor para el segundo campo');
								Ext.getCmp('_df_fecha_envio_if_de').focus();
								return;
							}

							if(!NE.util.validatorDate(Ext.getCmp('_df_fecha_operacion_de'),Ext.getCmp('_df_fecha_operacion_a'),false)){
								Ext.getCmp('_df_fecha_operacion_de').markInvalid('El rango de Fechas de Envio no debe ser mayor para el segundo campo');
								Ext.getCmp('_df_fecha_operacion_de').focus();
								return;
							}

							var fechMinEnvIF_ = Ext.util.Format.date(fechMinEnvIF,'d/m/Y');
							var fechMaxEnvIF_ = Ext.util.Format.date(fechMaxEnvIF,'d/m/Y');

							if(!Ext.isEmpty(fechMinEnvIF)){
								if(!isdate(fechMinEnvIF_)) {
									Ext.getCmp('_df_fecha_envio_if_de').markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
									Ext.getCmp('_df_fecha_envio_if_de').focus();
								return;
								}
							}
							if( !Ext.isEmpty(fechMaxEnvIF)){
								if(!isdate(fechMaxEnvIF_)) { 
									Ext.getCmp('_df_fecha_envio_if_a').markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
									Ext.getCmp('_df_fecha_envio_if_a').focus();
									return;
								}
							}

							var fechMinOper_= Ext.util.Format.date(fechMinOper,'d/m/Y');
							var fechMaxOper_= Ext.util.Format.date(fechMaxOper,'d/m/Y');
						
							if(!Ext.isEmpty(fechMinOper)){
								if(!isdate(fechMinOper_)) { 
									Ext.getCmp('_df_fecha_operacion_de').markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
									Ext.getCmp('_df_fecha_operacion_de').focus();
								return;
								}
							}
							
							if( !Ext.isEmpty(fechMaxOper)){
								if(!isdate(fechMaxOper_)) { 
									Ext.getCmp('_df_fecha_operacion_a').markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
									Ext.getCmp('_df_fecha_operacion_a').focus();
									return;
								}
							}

							if (difDias(fechMinEnvIF.getDate(),fechMinEnvIF.getMonth()+1,fechMinEnvIF.getFullYear(),
											fechMaxEnvIF.getDate(),fechMaxEnvIF.getMonth()+1,fechMaxEnvIF.getFullYear())>7)
								{
									Ext.getCmp('_df_fecha_envio_if_de').markInvalid('El rango de Fechas de Envio no debe ser mayor a 7 dias');
									Ext.getCmp('_df_fecha_envio_if_de').focus();
									return;
								}
								if(fechMinOper!="" && fechMaxOper!="" )
								{
									if (difDias(fechMinOper.getDate(),fechMinOper.getMonth()+1,fechMinOper.getFullYear(),
												fechMaxOper.getDate(),fechMaxOper.getMonth()+1,fechMaxOper.getFullYear())>7)
									{		
											Ext.getCmp('_df_fecha_operacion_de').markInvalid('El rango de Fechas de Envio no debe ser mayor a 7 dias');
											Ext.getCmp('_df_fecha_operacion_de').focus();
											return;
									}
								}
								accionConsulta("CONSULTAR",null);

				} //fin handler
			},{
				text:    'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					accionConsulta("LIMPIAR",null);
				}
			},{
				text:    'Cancelar',
				id:      'btnCancelar',
				iconCls: 'borrar',
				hidden: true,
				handler: function(){
					//accionConsulta("FIN",null);
				}
			}
		]
	});

//----------------Principal------------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 930,
	  height: 'auto',
	  items: 
	    [  
			fp ,
			NE.util.getEspaciador(10),
			grid,NE.util.getEspaciador(10),
			gridTotalesE
		 ]
  });
		
		catalogoIF.load();
		catalogoEstatus.load();
		
});