<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.math.*,
	netropology.utilerias.*,		
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	 com.netro.descuento.*,		
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_if = (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") : "";
String ic_epo = (request.getParameter("ic_epo") != null) ? request.getParameter("ic_epo") : "";
String ic_folio = (request.getParameter("ic_folio") != null) ? request.getParameter("ic_folio") : "";
String ic_moneda = (request.getParameter("ic_moneda") != null) ? request.getParameter("ic_moneda") : "";
String ic_estatus_solic = (request.getParameter("ic_estatus_solic") != null) ? request.getParameter("ic_estatus_solic") : "";
String cs_tipo_solicitud = (request.getParameter("cs_tipo_solicitud") != null) ? request.getParameter("cs_tipo_solicitud") : "";
String infoRegresar ="", consulta ="";
String noSolicitudes = (request.getParameter("noSolicitudes") != null) ? request.getParameter("noSolicitudes") : "";
	
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

ConsAutorizaSolic paginador = new ConsAutorizaSolic();

AutorizacionSolicitud BeanAutSolicitud = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);


if (informacion.equals("catalogoIF")){

	CatalogoIF catalogo = new CatalogoIF();
	catalogo.setClave("ic_if");
	catalogo.setDescripcion("cg_razon_social");		
	catalogo.setOrden("cg_razon_social");	
	List resultCatIF	= catalogo.getListaElementosGral();
	if(resultCatIF.size() >0 ){	
		for (int i= 0; i < resultCatIF.size(); i++) {			
			String registro =  resultCatIF.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
	consulta =  "{\"success\": true, \"total\": \"" + resultCatIF.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	


}else  if (informacion.equals("catalogoEPO")){

	CatalogoEPO catalogo = new CatalogoEPO();	
	List resultCatEpo = new ArrayList();	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");	
	resultCatEpo	= catalogo.getListaElementos();
	if(resultCatEpo.size() >0 ){	
		for (int i= 0; i < resultCatEpo.size(); i++) {			
			String registro =  resultCatEpo.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
	consulta =  "{\"success\": true, \"total\": \"" + resultCatEpo.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("catalogoMONEDA")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("catalogoESTATUS")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_estatus_solic");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_estatus_solic");		
	catalogo.setOrden("ic_estatus_solic");
	catalogo.setValoresCondicionIn("1,2", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("catalogoEstatusAsignar")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_estatus_solic");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_estatus_solic");		
	catalogo.setOrden("ic_estatus_solic");
	catalogo.setValoresCondicionIn("3, 4", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
	
}else if (informacion.equals("catalogoCausaRechazo")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("cd_descripcion");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_causa_rechazo");		
	catalogo.setOrden("cd_descripcion");
	infoRegresar = catalogo.getJSONElementos();	  
	

}else if (informacion.equals("Consultar")  ||  informacion.equals("ConsultarTotales")){
	
	paginador.setIc_if(ic_if);
	paginador.setIc_epo(ic_epo);
	paginador.setIc_folio(ic_folio);
	paginador.setIc_moneda(ic_moneda);
	paginador.setIc_estatus_solic(ic_estatus_solic);
	paginador.setCs_tipo_solicitud(cs_tipo_solicitud);	
	paginador.setPantalla("Consulta");	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
	if (informacion.equals("Consultar") ){	
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
				throw new AppException("Error en la paginación", e);
		}	
		
		jsonObj.put("ic_estatus_solic", ic_estatus_solic);
		
	}else if (informacion.equals("ConsultarTotales") ){ 
		
		int numRegistrosMN =0, numRegistrosDL=0;
		BigDecimal montoTotalMN = new BigDecimal("0.00");
		BigDecimal montoTotalDL = new BigDecimal("0.00");
		Registros reg	=	queryHelper.doSearch();
		
		while (reg.next()) {
			String claveMoneda = reg.getString("IC_MONEDA");
			String montoOperar = reg.getString("MONTO_OPERAR");  			
			if (claveMoneda.equals("1")) { //moneda nacional
				numRegistrosMN++;
				if (!montoOperar.equals("")) {
					montoTotalMN = montoTotalMN.add(new BigDecimal(montoOperar));
				}				
			} else if(claveMoneda.equals("54")) {	//dolar
				numRegistrosDL++;
				if (!montoOperar.equals("")) {
					montoTotalDL = montoTotalDL.add(new BigDecimal(montoOperar));
				}
			}
		}// while
		
		
		if (numRegistrosMN >0) {
			datos = new HashMap();
			datos.put("MONEDA", "MONEDA NACIONAL");
			datos.put("NUM_DOCTOS", String.valueOf(numRegistrosMN));
			datos.put("TOTAL_MONTOS", montoTotalMN.toPlainString());
			registros.add(datos);
		} 
		if (numRegistrosDL >0) {
			datos = new HashMap();
			datos.put("MONEDA", "DOLAR AMERICANO ");
			datos.put("NUM_DOCTOS", String.valueOf(numRegistrosDL));
			datos.put("TOTAL_MONTOS", montoTotalDL.toPlainString());
			registros.add(datos);
		}			
			
		String consulta2 =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta2);		
	
	}
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CambioEstatus_solic")){

	String noSolicitud[] = request.getParameterValues("noSolicitud");
	String estatusAsignar[] = request.getParameterValues("estatusAsignar");
	String causaRechazo[] = request.getParameterValues("causaRechazo");
	String numPrestamo[] = request.getParameterValues("numPrestamo");
	String fechaOperacion[] = request.getParameterValues("fechaOperacion");
	String numRegistros = (request.getParameter("numRegistros") != null) ? request.getParameter("numRegistros") : "";
	try {
		noSolicitudes=  paginador.cambioEstatus_solic(Integer.parseInt(numRegistros),  noSolicitud , estatusAsignar, causaRechazo, numPrestamo , fechaOperacion);
	} catch(Exception e) {
		e.printStackTrace();
	}
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("noSolicitudes", noSolicitudes);
	infoRegresar = jsonObj.toString();


}else if (informacion.equals("ConsultaAcuse")){

	paginador.setPantalla("Acuse");	
	paginador.setNoSolicitudes(noSolicitudes);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);	
	try {		
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
				throw new AppException("Error en la paginación", e);
		}	
		
	infoRegresar = jsonObj.toString();
	
	
}else if (informacion.equals("GenerarArchivoAcuse")){

	paginador.setPantalla("Acuse");	
	paginador.setNoSolicitudes(noSolicitudes);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
	String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	
	jsonObj.put("success", new Boolean(true)); 
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("MODIFICAR_ESTATUS")){

	String noSolicitud[] = request.getParameterValues("noSolicitud");
	String numRegistros = (request.getParameter("numRegistros") != null) ? request.getParameter("numRegistros") : "";
	StringBuffer noSolicitudes2 = new StringBuffer("");
	for (int i=0;i<Integer.parseInt(numRegistros);i++)  {
		noSolicitudes2.append(noSolicitud[i]+",");		
	}
		
	noSolicitudes2.deleteCharAt(noSolicitudes2.length()-1);
	
	BeanAutSolicitud.procesarSolicitudes(noSolicitudes2.toString()); // Metodo que modifica el estaus 	
	
	ic_folio  = noSolicitudes2.substring(0,noSolicitudes2.toString().indexOf(','));
	
	jsonObj.put("success", new Boolean(true)); 
	jsonObj.put("noSolicitudes",noSolicitudes2.toString());
	jsonObj.put("ic_folio",ic_folio);
	jsonObj.put("ic_estatus_solic","2"); 
	
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ImprimirReporte")){


	Vector registrosProcesados = BeanAutSolicitud.getSolicProcesadas(noSolicitudes);	
	String nombreArchivo  = paginador.impReporteOperacionesPDF (request, registrosProcesados, strDirectorioTemp);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("DescargaArchivo")){

	Vector registrosProcesados = BeanAutSolicitud.getSolicProcesadas(noSolicitudes);
	
	String nombreArchivo  = paginador.impDescargaArchivo(request, registrosProcesados, strDirectorioTemp);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
		
}	



%>
<%=infoRegresar%>