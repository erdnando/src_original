<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<% String version = (String)session.getAttribute("version"); %>

<html>
<head>
<title>Nafi@net - Autorizaciones de Solicitudes</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(version!=null){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
 <script type="text/javascript" src="13forma01ext.js?<%=session.getId()%>"></script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%if(version!=null){%>
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<%}%>
		<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
   </div>
</div>
<%if(version!=null){%>
<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>