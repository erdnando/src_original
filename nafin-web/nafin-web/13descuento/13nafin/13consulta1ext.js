Ext.onReady(function() {

	var VisorAcusesDeCarga = function(rec){
		var ic_acuse = rec.get('IC_ACUSE');
		if(ic_acuse != ''){
			var win = new NE.AcusesDeCarga.VisorAcusesDeCarga(ic_acuse);
		}
	}

	var renTasa = function(value, metadata, record, rowindex, colindex, store) {
		var valor = "";
		if(record.data.IN_TASA_ACEPTADA == "N/A")
			valor = 'N/A';
		else{
			valor = Ext.util.Format.number(record.data.IN_TASA_ACEPTADA,'0,0.00000%');;
		}	
		return valor;		
	}
	//IN_IMPORTE_RECIBIR
	var renMonto = function(value, metadata, record, rowindex, colindex, store) {
		var valor = "";
		var num = record.data.IN_IMPORTE_RECIBIR;
		if(record.data.IN_IMPORTE_RECIBIR == "N/A")
			valor = '<center>N/A</center>';
		else{
			valor = Ext.util.Format.number(num,'$0,0.00');
		}	
		
		return valor; 
	}
	
	//IN_IMPORTE_INTERES
	var renInteres = function(value, metadata, record, rowindex, colindex, store) {
		var valor = "";
		if(record.data.IN_IMPORTE_INTERES == "N/A")
			return '<center>N/A</center>';
		else{
			valor = Ext.util.Format.number(record.data.IN_IMPORTE_INTERES,'$0,0.00'); ;
			return valor; 
		}
	}	
	var ValidaTamEnetroDec=function(valor){
		var valor=replaceAll(valor,',','');
		var enteros=valor.indexOf('.');
		var decimales=valor.length-1-enteros;
		if(enteros<=13 && decimales==2) {
			return true;
		}else
			return false;
	};
	
						 
	var replaceAll= function ( text, busca, reemplaza ){
	  while (text.toString().indexOf(busca) != -1)
			text = text.toString().replace(busca,reemplaza);
	  return text;
	}

	
	var  ConsultarAmbosTotales =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var json = Ext.decode(response.responseText);
				if(Ext.getCmp('ic_busqueda1').getValue() != 'F'){
					consultaDataTotales.loadData(json);
					gridTotales.show();								
				}else{
					gridTotales.hide();
				}
				consultaDataTotalesC.loadData(json);
				gridTotalesC.show();			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//ConsultarT2
	var  ConsultarT2 =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var json = Ext.decode(response.responseText);
			if(json.total > 0) {
				consultaDataTotales2.loadData(json);
				gridTotales2.show();	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function mostrarArchivoPDF(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			if(resp.estatusArchivo == 'FINAL'){
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
				Ext.getCmp('btnGenerarArchivoPDF').setText('Generar Todo');
				Ext.getCmp('btnGenerarArchivoPDF').enable();
				Ext.getCmp('btnGenerarArchivoPDF').setIconClass('icoPdf');
			} else if(resp.estatusArchivo == 'ERROR'){
				Ext.Msg.alert('Error','No se gener� el archivo',function(btn){
					Ext.getCmp('btnGenerarArchivoPDF').setText('Generar Todo');
					Ext.getCmp('btnGenerarArchivoPDF').enable();
					Ext.getCmp('btnGenerarArchivoPDF').setIconClass('icoPdf')
				});
			} else{
				Ext.getCmp('btnGenerarArchivoPDF').setIconClass('loading-indicator');
				Ext.getCmp('btnGenerarArchivoPDF').setText(resp.porcentaje);
				Ext.Ajax.request({
					url:'13consulta1ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion:'GenerarArchivoPDF',
						tipo:'PDF',
						estatusArchivo: 'PROCESANDO',
						start: 0,
						limit:0
					}),
					callback: mostrarArchivoPDF
				});
			}
		} else{
			Ext.getCmp('btnGenerarArchivoPDF').setText('Generar Todo');
			Ext.getCmp('btnGenerarArchivoPDF').enable();
			Ext.getCmp('btnGenerarArchivoPDF').setIconClass('icoPdf')
			NE.util.mostrarConnError(response,opts);
		}

	}

	function mostrarArchivoCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivoCSV').enable();
		Ext.getCmp('btnGenerarArchivoCSV').setIconClass('icoXls');
	}

	var consultaDataTotalesC = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotalesC'
		},
		hidden: true,
		fields: [	
			{name: 'CVE_MONEDA'},
			{name: 'MONTO_DESC_RECIB'},
			{name: 'MONTO_DESC_OPER'},
			{name: 'MONTO_DESC_RECH'},
			{name: 'TOTAL_SOLIC_RECIBIDAS'},
			{name: 'TOTAL_SOLIC_OPER'},
			{name: 'TOTAL_SOLIC_RECH'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},
		hidden: true,
		fields: [	
			{name: 'CVE_MONEDA'},
			{name: 'TOTAL_DOCTOS'},
			{name: 'MONTO_DOCTOS'},
			{name: 'MONTO_DESC_DOCTOS'},
			{name: 'MONTO_INTERES'}								
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	
	var consultaDataTotales2 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},
		hidden: true,
		fields: [	
			{name: 'CVE_MONEDA'},
			{name: 'TOTAL_DOCTOS'},
			{name: 'MONTO_DOCTOS'},
			{name: 'MONTO_DESC_DOCTOS'},
			{name: 'MONTO_INTERES'},								
			{name: 'MONTO_INTERES_IF'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});


	
	var gruposTotales = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 1, align: 'center'},
				{	header: 'Recibidos', colspan: 2, align: 'center'},	
				{	header: 'Operados ', colspan: 2, align: 'center'},
				{	header: 'Rechazados ', colspan: 2, align: 'center'}
			]
		]
	});
	
	var gridTotalesC = new Ext.grid.GridPanel({
		id: 'gridTotalesC',				
		store: consultaDataTotalesC,	
		style: 'margin:0 auto;',
		title:'',
		plugins: gruposTotales,
		hidden: true,
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'CVE_MONEDA',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'left',
				align: 'center',renderer:function(value,metadata,registro,rowIndex){		
								if (registro.get('CVE_MONEDA')==1){	
								  return '<span>MONEDA NACIONAL</span>';				
							 }else{
								  return '<span>DOLAR AMERICANO</span>';
							 }	
				}
			},		
			{							
				header : 'Total de solicitudes ',
				tooltip: 'Total de solicitudes ',
				dataIndex : 'TOTAL_SOLIC_RECIBIDAS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Total Montos ',
				tooltip: 'Total Montos ',
				dataIndex : 'MONTO_DESC_RECIB',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},		
			{							
				header : 'Total de solicitudes ',
				tooltip: 'Total de solicitudes ',
				dataIndex : 'TOTAL_SOLIC_OPER',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'							
			},	
			{							
				header : 'Total Montos ',
				tooltip: 'Total Montos ',
				dataIndex : 'MONTO_DESC_OPER',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},		
			{							
				header : 'Total de solicitudes ',
				tooltip: 'Total de solicitudes ',
				dataIndex : 'TOTAL_SOLIC_RECH',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'				
			},
			{							
				header : 'Total Montos ',
				tooltip: 'Total Montos ',
				dataIndex : 'MONTO_DESC_RECH',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			}	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 150,
		width: 920,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var gruposT = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 3, align: 'center'},
				{	header: 'Intermediario Financiero', colspan: 2, align: 'center'}	
				//{	header: 'Fideicomiso ', colspan: 4, align: 'center'},
				//{	header: ' ', colspan: 2, align: 'center'}
			]
		]
	});	
	
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		plugins: gruposT,
		title:'',
		hidden: true,
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'CVE_MONEDA',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro,rowIndex){		
								if (registro.get('CVE_MONEDA')==1){	
								  return '<span>MONEDA NACIONAL</span>';				
							 }else{
								  return '<span>DOLAR AMERICANO</span>';
							 }	
				}
			},		
			{							
				header : 'N�mero de Documentos',
				tooltip: 'N�mero de Documentos',
				dataIndex : 'TOTAL_DOCTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'Total Monto Documento',
				tooltip: 'Total Monto Documento',
				dataIndex : 'MONTO_DOCTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Total Monto a Descontar',
				tooltip: 'Total Monto a Descontar',
				dataIndex : 'MONTO_DESC_DOCTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Total Monto est. int',
				tooltip: 'Total Monto est. int',
				dataIndex : 'MONTO_INTERES',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 100,
		width: 920,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var gruposT2 = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 4, align: 'center'}, //sera de 3
				{	header: 'Intermediario Financiero', colspan: 1, align: 'center'},	
				{	header: 'Fideicomiso ', colspan: 1, align: 'center'}
				//{	header: ' ', colspan: 2, align: 'center'}
			]
		]
	});	
		
	
	var gridTotales2 = new Ext.grid.GridPanel({
		id: 'gridTotales2',				
		store: consultaDataTotales2,	
		style: 'margin:0 auto;',
		plugins: gruposT2,
		title:'',
		hidden: true,//cambiar
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'CVE_MONEDA',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro,rowIndex){		
								if (registro.get('CVE_MONEDA')==1){	
								  return '<span>MONEDA NACIONAL</span>';				
							 }else{
								  return '<span>DOLAR AMERICANO</span>';
							 }	
				}
			},		
			{							
				header : 'N�mero de Documentos',
				tooltip: 'N�mero de Documentos',
				dataIndex : 'TOTAL_DOCTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'Total Monto Documento',
				tooltip: 'Total Monto Documento',
				dataIndex : 'MONTO_DOCTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Total Monto a Descontar',
				tooltip: 'Total Monto a Descontar',
				dataIndex : 'MONTO_DESC_DOCTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Total Monto est. int',
				tooltip: 'Total Monto est. int',
				dataIndex : 'MONTO_INTERES_IF',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Total Monto est. int',
				tooltip: 'Total Monto est. int',
				dataIndex : 'MONTO_INTERES',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 100,
		width: 920,
		style: 'margin:0 auto;',		
		frame: false
	});
		
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid= Ext.getCmp('gridConsulta');
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		if (arrRegistros != null) {
			Ext.getCmp('btnConsultar').enable();
			grid.el.unmask();
			if (!grid.isVisible()) {
				grid.show();
			}		
			var btnGenerarArchivoPDF = Ext.getCmp('btnGenerarArchivoPDF');
			var btnGenerarArchivoCSV = Ext.getCmp('btnGenerarArchivoCSV');
			btnGenerarArchivoCSV.enable();
			btnGenerarArchivoPDF.enable();
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var jsonData = store.reader.jsonData;	
			var cm = grid.getColumnModel();			
			var el = grid.getGridEl();	
				
			if(jsonData.esBANCOMEXT=='S') {
				grid.getColumnModel().setHidden(cm.findColumnIndex('NOM_BANCO_FONDEO'), true);	
			}else  if(jsonData.esBANCOMEXT=='N') {
				grid.getColumnModel().setHidden(cm.findColumnIndex('NOM_BANCO_FONDEO'), false );				
			}
			
			if(store.getTotalCount() > 0) {
				el.unmask();	
				Ext.Ajax.request({
					url:'13consulta1ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{					
						informacion:'ConsultarTotales'	
					}),
					callback: ConsultarAmbosTotales
				});		
				
				//if(Ext.getCmp('ic_busqueda1').getValue() == 'F'){
					Ext.Ajax.request({
						url:'13consulta1ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{					
							informacion:'ConsultarT2'
							//esTotal2: 'S'
						}),
						callback: ConsultarT2
					});				
				//}
			
			} else {
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
				gridTotales.hide();	
				gridTotalesC.hide();
				gridTotales2.hide();
			}
		}
	};
	
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url:  '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [
			{name: 'TIPO'                   },
			{name: 'NOMBREIF'               },
			{name: 'NOMBREFIDEICOMISO'      },
			{name: 'NOMBREEPO'              },
			{name: 'NOMBREPYME'             },
			{name: 'IC_FOLIO'               },
			{name: 'DF_FECHA_SOLICITUD'     },
			{name: 'CD_NOMBRE'              },
			{name: 'IC_MONEDA'              },
			{name: 'FN_MONTO'               },
			{name: 'FN_PORC_ANTICIPO'       },
			{name: 'FN_MONTO_DSCTO'         },
			{name: 'IN_IMPORTE_RECIBIR'     },
			{name: 'IN_IMPORTE_RECIBIR_IF'  },
			{name: 'IN_TASA_ACEPTADA'       },
			{name: 'IN_TASA_ACEPTADA_IF'    },
			{name: 'IN_IMPORTE_INTERES'     },
			{name: 'IN_IMPORTE_INTERES_IF'  },
			{name: 'CD_DESCRIPCION'         },
			{name: 'IC_ESTATUS_SOLIC'       },
			{name: 'PROCESO'                },
			{name: 'IG_NUMERO_PRESTAMO'     },
			{name: 'NUMERO_PRESTAMO'        },
			{name: 'DF_OPERACION'           },
			{name: 'TASA_FONDEO'            },
			{name: 'NUM_PRESTAMO'           },
			{name: 'FECHA_OPERACION'        },
			{name: 'TASA_FONDEO'            },
			{name: 'FECHA_VENCIMIENTO'      },
			{name: 'IN_NUMERO_SIRAC'        },
			{name: 'BENEFICIARIO'           },
			{name: 'FN_PORC_BENEFICIARIO'   },
			{name: 'RECIBIR_BENEF'          },
			{name: 'NETO_REC_PYME'          },
			{name: 'NOM_BANCO_FONDEO'       },
			{name: 'DF_ENTREGA'             },
			{name: 'CG_TIPO_COMPRA'         },
			{name: 'CG_CLAVE_PRESUPUESTARIA'},
			{name: 'CG_PERIODO'             },	
			{name: 'TIPOTASA'               },
			{name: 'CS_DSCTO_ESPECIAL'      },
			{name: 'IC_ACUSE'               },
			{name: 'MUESTRA_VISOR'          }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: procesarConsultaData,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args){
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);
					}
				}
			}
	});

	var gruposConsulta = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 7, align: 'center'},
				{	header: 'Intermediario Financiero', colspan: 4, align: 'center'},	
				{	header: 'Fideicomiso ', colspan: 4, align: 'center'},
				{	header: ' ', colspan: 21, align: 'center'}
			]
		]
	});	

	var grid = new Ext.grid.GridPanel({
		height:  400,
		width:   920,
		store:   consultaData,
		plugins: gruposConsulta,
		id:      'gridConsulta',
		style:   'margin:0 auto;',
		hidden:  true,
		frame:   true,
		columns: [{
			header:'Origen',
			tooltip:'Origen',
			dataIndex:'TIPO',
			hidden: false,
			width: 100,
			align: 'center'
		},{
			header:'<center>EPO</center>',
			tooltip:'EPO',
			dataIndex:'NOMBREEPO',
			hidden: false,
			width: 150,
			align: 'left'
		},{
			header:'<center>PYME</center>',
			tooltip:'PYME',
			dataIndex:'NOMBREPYME',
			hidden: false,
			width: 150,
			align: 'left'
		},{
			header: '<center>Monto Documento</center>',
			tooltip: 'Monto Documento',
			dataIndex: 'FN_MONTO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header: 'Porcentaje de Descuento',
			tooltip: 'Porcentaje de Descuento',
			dataIndex: 'FN_PORC_ANTICIPO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0%')
		},{
			header: '<center>Recurso en Garant�a</center>',
			tooltip: 'Recurso en Garant�a',
			dataIndex: 'FN_PORC_ANTICIPO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'right',
			renderer:function(value,metadata,registro,rowIndex){
				recurso= (registro.get('FN_MONTO'))-(registro.get('FN_MONTO_DSCTO'))
				return Ext.util.Format.number(recurso,'$0,0.00');
			}
		},{
			header: '<center>Monto Descuento</center>',
			tooltip: 'Monto Descuento',
			dataIndex: 'FN_MONTO_DSCTO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header:'<center>Nombre</center>',
			tooltip:'Nombre',
			dataIndex:'NOMBREIF',
			hidden: false,
			width: 150,
			align: 'left'
		},{
			header: '<center>Monto a Operar</center>',
			tooltip: 'Monto a Operar',
			dataIndex: 'IN_IMPORTE_RECIBIR_IF',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header: 'Tasa PYME',
			tooltip: 'Tasa PYME',
			dataIndex: 'IN_TASA_ACEPTADA_IF',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00000%')
		},{
			header: '<center>Monto est. int.</center>',
			tooltip: 'Monto est. int.',
			dataIndex: 'IN_IMPORTE_INTERES_IF',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},{
			header:'<center>Nombre</center>',
			tooltip:'Intermediario',
			dataIndex:'NOMBREFIDEICOMISO',
			hidden: false,
			width: 150,
			align: 'left'
		},{
			header: '<center>Monto a Operar</center>',
			tooltip: 'Monto a Operar',
			dataIndex: 'IN_IMPORTE_RECIBIR',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'right',
			renderer: renMonto
		},{
			header: 'Tasa PYME',
			tooltip: 'Tasa PYME',
			dataIndex: 'IN_TASA_ACEPTADA',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center',
			renderer: renTasa
		},{
			header: '<center>Monto est. int.</center>',
			tooltip: 'Monto est. int.',
			dataIndex: 'IN_IMPORTE_INTERES',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'right',
			renderer: renInteres
		},{
			header:'Num. Solicitud',
			tooltip:'Num. Solicitud',
			dataIndex:'IC_FOLIO',
			hidden: false,
			width: 150,
			align: 'center'
		},{
			width:     180,
			xtype:     'actioncolumn',
			header:    'No. Acuse Autorizaci�n',
			tooltip:   'No. Acuse Autorizaci�n',
			dataIndex: 'IC_ACUSE',
			align:     'center',
			hidden:    false,
			renderer: function(value, metadata, record, rowindex, colindex, store){
				return (record.get('IC_ACUSE'));
			},
			items: [{
				getClass: function(value, metadata, registro, rowIndex, colIndex, store){
					if(registro.get('IC_ACUSE') != ''  && registro.get('MUESTRA_VISOR') == 'S'){
						this.items[0].tooltip = 'Ver';
						return 'iconoLupa';
					} else{
						return value;
					}
				},
				handler: function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					VisorAcusesDeCarga(rec);
				}
			}]
		},{
			header:'Fecha Valor',
			tooltip:'Fecha Valor',
			dataIndex:'DF_FECHA_SOLICITUD',
			hidden: false,
			width: 150,
			align: 'center'
		},{
			header:'Moneda',
			tooltip:'Moneda',
			dataIndex:'CD_NOMBRE',
			hidden: false,
			width: 150,
			align: 'center'
		},{
			header: 'Estatus',
			tooltip: 'Estatus',
			dataIndex: 'CD_DESCRIPCION',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center'
		},{
			header: 'N�mero de Pr�stamo',
			tooltip: 'N�mero de Pr�stamo',
			dataIndex: 'NUMERO_PRESTAMO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center'
		},{
			header: 'Fecha de Operaci�n',
			tooltip: 'Fecha de Operaci�n',
			dataIndex: 'DF_OPERACION',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center'
		},{
			header: 'Tasa de Fondeo',
			tooltip: 'Tasa de Fondeo',
			dataIndex: 'TASA_FONDEO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center',
			renderer:function(value,metadata,registro,rowIndex){
				if (registro.get('TASA_FONDEO')=="0" ){
					 return "";
				}else{
					return value;
				}
			}
		},{
			header: 'Fecha de Vencimiento',
			tooltip: 'Fecha de Vencimiento',
			dataIndex: 'FECHA_VENCIMIENTO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center'
		},{
			header: 'N�mero Cliente SIRAC',
			tooltip: 'N�mero Cliente SIRAC',
			dataIndex: 'IN_NUMERO_SIRAC',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center'
		},{
			header: 'N�mero de Cr�dito Aplicado',
			tooltip: 'N�mero de Cr�dito Aplicado',
			dataIndex: 'IG_NUMERO_PRESTAMO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center',
			renderer:function(value,metadata,registro,rowIndex){
				if (registro.get('IG_NUMERO_PRESTAMO')=="0" ){
					return "";
				}else{
					return value;
				}
			}
		},{
			header: 'M�todo de Operaci�n',
			tooltip: 'M�todo de Operaci�n',
			dataIndex: 'PROCESO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'left'
		},{
			header: 'Neto a recibir PYME',
			tooltip: 'Neto a recibir PYME',
			dataIndex: '',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'right',
			renderer:function(value,metadata,registro,rowIndex){
				if (registro.get('CS_DSCTO_ESPECIAL')=="D" || registro.get('CS_DSCTO_ESPECIAL')=="I"){
					return Ext.util.Format.number(registro.get('NETO_REC_PYME'),'$0,0.00');
				}else{
					return "";
				}
			}
		},{
			header: '<center>Beneficiario</center>',
			tooltip: 'Beneficiario',
			dataIndex: 'BENEFICIARIO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'left'
		},{
			header: '% Beneficiario',
			tooltip: '% Beneficiario',
			dataIndex: 'FN_PORC_BENEFICIARIO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center',
			renderer:function(value,metadata,registro,rowIndex){
				if (registro.get('CS_DSCTO_ESPECIAL')=="D" || registro.get('CS_DSCTO_ESPECIAL')=="I"){
					return Ext.util.Format.number(registro.get('FN_PORC_BENEFICIARIO'),'0.0000%');
				}else{
					return "";
				}
			}
		},{
			header: 'Importe a recibir Beneficiario',
			tooltip: 'Importe a recibir Beneficiario',
			dataIndex: '',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'right',
			renderer:function(value,metadata,registro,rowIndex){
				if (registro.get('CS_DSCTO_ESPECIAL')=="D" || registro.get('CS_DSCTO_ESPECIAL')=="I"){
					return Ext.util.Format.number(registro.get('RECIBIR_BENEF'),'$0,0.00');
				}else{
					return "";
				}
			}
		},{
			header: 'Banco de Fondeo',
			tooltip: 'Banco de Fondeo',
			dataIndex: 'NOM_BANCO_FONDEO',
			id: 'ic_banco_fondeoCol',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center'
		},{
			header: 'Fecha de Recepci�n de Bienes y Servicios',
			tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
			dataIndex: 'DF_ENTREGA',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center'
		},{
			header: 'Tipo de Compra (procedimiento)',
			tooltip: 'Tipo de Compra (procedimiento)',
			dataIndex: 'CG_TIPO_COMPRA',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'left'
		},{
			header: 'Clasificador por Objeto del Gasto',
			tooltip: 'Clasificador por Objeto del Gasto',
			dataIndex: 'CG_CLAVE_PRESUPUESTARIA',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'left'
		},{
			header: 'Plazo M�ximo',
			tooltip: 'Plazo M�ximo',
			dataIndex: 'CG_PERIODO',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'center',
			renderer:function(value,metadata,registro,rowIndex){
				if (registro.get('CG_PERIODO')=="0" ){
					return "";
				}else{
					return value;
				}
			}
		},{
			header: 'Referencia Tasa',
			tooltip: 'Referencia Tasa',
			dataIndex: 'TIPOTASA',
			sortable: true,
			resizable: true,
			width: 130,
			align: 'left',
			renderer:function(value,metadata,registro,rowIndex){
				if (registro.get('IC_ESTATUS_SOLIC')==1 || registro.get('IC_ESTATUS_SOLIC')==3 || registro.get('IC_ESTATUS_SOLIC')==5){
					return '<span style="color:black;">'+value+'</span>';
				}else{
					return '<span>N/A</span>';
				}
			}
		}],
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-','->',
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnGenerarArchivoCSV',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var fn_montoMin2=replaceAll(Ext.getCmp('fn_montoMin1').getValue(),',','');
						var fn_montoMax2=replaceAll(Ext.getCmp('fn_montoMax1').getValue(),',','');
						var fp = Ext.getCmp('forma');
							Ext.Ajax.request({
								url:'13consulta1ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
								fn_montoMin2: fn_montoMin2,
								fn_montoMax2: fn_montoMax2,
								informacion:'GenerarArchivoCSV',
								tipo:'CSV'
							}),
							callback: mostrarArchivoCSV
						});
					}
				},{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime todos los registros en formato PDF.',
					id: 'btnGenerarArchivoPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var fn_montoMin2=replaceAll(Ext.getCmp('fn_montoMin1').getValue(),',','');
						var fn_montoMax2=replaceAll(Ext.getCmp('fn_montoMax1').getValue(),',','');
						var fp = Ext.getCmp('forma');
						Ext.Ajax.request({
							url:'13consulta1ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								fn_montoMin2: fn_montoMin2,
								fn_montoMax2: fn_montoMax2,
								informacion:'GenerarArchivoPDF',
								tipo:'PDF',
								estatusArchivo: 'INICIO',
								start: 0,
								limit:0
							}),
							callback: mostrarArchivoPDF
						});
					}
				}
/*
				{
					xtype: 'button',
					text: 'Descargar PDF',
					id: 'btnGenerarArchivoPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						var fp = Ext.getCmp('forma');
						Ext.Ajax.request({
							url:'13consulta1ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'GenerarArchivoPDF',
								tipo:'PDF',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize	
							}),
							callback: mostrarArchivoPDF
						});
					}
				}
*/
			]
		}
	});


	//********************Componentes de la forma de busqueda **************************************
	
	
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				if(jsonData.esBANCOMEXT=='S') {					
					Ext.getCmp("ic_banco_fondeoC").hide();
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	 catalogoTipoTasa = new Ext.data.ArrayStore({
			  fields: ['clave',	'descripcion'],
			  data: [['N', 'Negociada'], ['O', 'Oferta de Tasas'],['P', 'Preferencial'],['B', 'Tasa Base']]
		 });
	catalogoTipoBusqueda= new Ext.data.ArrayStore({
			  fields: ['clave',	'descripcion'],
			  data: [['', 'Todas'], ['A', 'Autom�tica'],
			  ['M', 'Manual'], ['F','Operaci�n Fideicomiso']]
		 });
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoEstado = new Ext.data.JsonStore({
		id: 'catalogoEstado',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEstado'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	
	});
	
	var catalogoPYME = new Ext.data.JsonStore({
		id: 'catalogoPYME',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	
	});
	
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	
	});
	
	
	var  elementosForma  = [	
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			listeners: {
				select: {
					fn: function(combo) {
						
						var cmbIF = Ext.getCmp('ic_if1');
						cmbIF.setValue('');
						cmbIF.store.load({
							params: {
								ic_epo:combo.getValue()
							}
						});
						
						var cmbPYME = Ext.getCmp('ic_pyme1');
						cmbPYME.setValue('');
						cmbPYME.store.load({
							params: {
								ic_epo:combo.getValue()
							}
						});				
					}
				}
			}			
		},
		{
			xtype: 'combo',
			fieldLabel: 'Nombre del Intermediario',
			name: 'ic_if',
			id: 'ic_if1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_if',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIF						
		},	
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la PYME',
			name: 'ic_pyme',
			id: 'ic_pyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_pyme',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoPYME
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Acuse',
			combineErrors: true,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			combineErrors: false,
			minChars : 1,
			width: 500,
			items: [				
				{
					xtype: 'textfield',
					name: 'cc_acuse',
					id: 'cc_acuse1',
					allowBlank: true,
					maxLength: 22,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				}
			]	
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto',
			combineErrors: false,
			msgTarget: 'side',
			width: 300,
			items: [		
				{
					xtype: 'textfield',
					name: 'fn_montoMin',
					id: 'fn_montoMin1',
					fieldLabel: 'Monto entre',					
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					vtype:	'rangoBigamount',
					xtype: 'bigamount'
				},
				{
					xtype: 'displayfield',
					value: ' a',					 
					width: 15
				},				
				{
					xtype: 'textfield',
					name: 'fn_montoMax',
					id: 'fn_montoMax1',
					fieldLabel: 'Monto entre',				
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					vtype:	'rangoBigamount',
					xtype: 'bigamount'
				}	
				
			]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha valor',
			combineErrors: false,
			msgTarget: 'side',
			items: [							
				{
					xtype: 'datefield',
					name: 'df_fecha_solicitudMin',
					id: 'df_fecha_solicitudMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_solicitudMax',
					margins: '0 20 0 0' ,
					minValue: '01/01/1901'				
					},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_solicitudMax',
					id: 'df_fecha_solicitudMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoInicioFecha: 'df_fecha_solicitudMin',
					margins: '0 20 0 0'					
				}					
			]	
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Solicitud',
			combineErrors: true,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			combineErrors: false,
			minChars : 1,
			width: 700,
			items: [
				{
					xtype: 'textfield',
					fieldLabel: 'N�mero de Solicitud',
					name: 'ic_folioMin',
					id: 'ic_folioMin1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',			
					valueField: 'clave',
					hiddenName : 'ic_folioMin',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 170,
					maxLength: 11,
					minChars : 1,
					maskRe:/[0-9]/
				},
				{
					xtype: 'displayfield',
					value: 'Estado:',
					width: 81
				},				
				{
					xtype: 'combo',
					fieldLabel: 'Estado',
					name: 'ic_estado',
					id: 'ic_estado1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_estado',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoEstado
				}
			]	
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Moneda',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoMoneda						
				},
				{
					xtype: 'displayfield',
					value: 'Estatus:',
					width: 80
				},				
				{
					xtype: 'combo',
					fieldLabel: 'Estatus',
					name: 'ic_estatus_solic',
					id: 'ic_estatus_solic1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_estatus_solic',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoEstatus
				}
			]	
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de B�squeda',
			combineErrors: true,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Tipo de B�squeda',
					name: 'ic_busqueda',
					id: 'ic_busqueda1',
					mode: 'local',
					value:'',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_busqueda',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoTipoBusqueda						
				},
				{
					xtype: 'displayfield',
					value: 'Tipo de Tasa:',
					width: 80
				},				
				{
					xtype: 'combo',
					fieldLabel: 'Tipo de Tasa',
					name: 'ic_tasa',
					id: 'ic_tasa1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_tasa',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoTipoTasa,
					listeners: 	{
						select:function(combo, record, index) {
							Ext.getCmp("suceptibleFloating").hide();
							Ext.getCmp('aplicaFloating2').setValue(false);
							Ext.getCmp('aplicaFloating1').setValue(false);
							if(combo.value==='P'){
								Ext.getCmp("suceptibleFloating").show();								
							}
						}
					}
				}
			]	
		},
		{
			xtype: 'compositefield',	
			name: 'suceptibleFloating',   
			id: 'suceptibleFloating', 
			combineErrors: true,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			hidden: true,
			items: [
				{
					xtype: 'displayfield',					
					width: 175
				},	
				{
					xtype: 'displayfield',
					value: 'Aplica Floating:',
					width: 100
				},	
				{  
					xtype:  'radiogroup', 					 
					align: 'center', 
					width: 100,		   					
					items: [						
						{ 
							boxLabel:    "Si",             
							name:        'aplicaFloating', 
							id: 'aplicaFloating1',
							inputValue:  "S" ,
							value: 'S',							
							width: 15
						}, 				
						{           
							boxLabel: "No",             
							name: 'aplicaFloating', 
							id: 'aplicaFloating2',
							inputValue:  "N", 
							value: 'N',							
							width: 15						
						} 				
					]
				}
			]			
		},		
		{
		  xtype: 'label',
		  id: 'lblCriterios',
		  fieldLabel: 'Criterios ordenamiento',
		  text: ''
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			items: [			
				{
					xtype: 'numberfield',
					name: 'ordenaIF',
					id: 'ordenaIF1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'IF',
					width: 80
				},
				{
					xtype: 'numberfield',
					name: 'ordenaPyme',
					id: 'ordenaPyme1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'PYME',
					width: 80
				},
				{
					xtype: 'numberfield',
					name: 'ordenaEPO',
					id: 'ordenaEPO1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'EPO',
					width: 130
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			items: [		
				{
					xtype: 'numberfield',
					name: 'ordenaFecha',
					id: 'ordenaFecha1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'Fecha de Valor',
					width: 80
				},
				{
					xtype: 'numberfield',
					name: 'ordenaEstatus',
					id: 'ordenaEstatus1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'Estatus',
					width: 80
				},
				{
					xtype: 'numberfield',
					name: 'ordenaFolio',
					id: 'ordenaFolio1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'Folio',
					width: 80
				}		
			]
		}	
	];


	var fp  = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		title: 'Informaci�n de Solicitudes',
		frame: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
			monitorValid: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Buscar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var fn_montoMin=Ext.getCmp('fn_montoMin1');
					var fn_montoMax=Ext.getCmp('fn_montoMax1');

					var df_fecha_solicitudMin=Ext.getCmp('df_fecha_solicitudMin');
					var df_fecha_solicitudMax=Ext.getCmp('df_fecha_solicitudMax');

					var fn_montoMin2=replaceAll(Ext.getCmp('fn_montoMin1').getValue(),',','');
					var fn_montoMax2=replaceAll(Ext.getCmp('fn_montoMax1').getValue(),',','');

					if(!Ext.isEmpty(df_fecha_solicitudMin.getValue()) || !Ext.isEmpty(df_fecha_solicitudMax.getValue())){
						if(Ext.isEmpty(df_fecha_solicitudMin.getValue())){
							df_fecha_solicitudMin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_solicitudMin.focus();
							return;
						}
						if(Ext.isEmpty(df_fecha_solicitudMax.getValue())){
							df_fecha_solicitudMax.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_solicitudMax.focus();
							return;
						}
					}

					if (!Ext.isEmpty(fn_montoMin.getValue())   && Ext.isEmpty(fn_montoMax.getValue())){
						fn_montoMax.markInvalid('Debe capturar el monto final.');
						return;
					}
					if (!Ext.isEmpty(fn_montoMin.getValue())   && !Ext.isEmpty(fn_montoMax.getValue())){

						if(parseFloat(fn_montoMin2,10)>parseFloat(fn_montoMax2,10)){
							fn_montoMax.markInvalid('El monto final debe ser mayor o igual al inicial.');
							return;
						}
					}

					if( ( !Ext.isEmpty(fn_montoMin.getValue()) ||  !Ext.isEmpty(fn_montoMax.getValue()) )){
						if(!ValidaTamEnetroDec(Ext.getCmp('fn_montoMin1').getValue())&&ValidaTamEnetroDec(Ext.getCmp('fn_montoMax1').getValue())){
							Ext.MessageBox.alert("Mensaje","El monto debe de ser de una longitud m�xima de 13 Enteros y 2 Decimales"	);
							return;
						}
					}

					fp.el.mask('Enviando...', 'x-mask-loading');
					boton.disable();
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							fn_montoMin2:fn_montoMin2,
							fn_montoMax2:fn_montoMax2,
							start:0,
							limit:15
						})
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
				window.location = '13consulta1ext.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid,
			NE.util.getEspaciador(20),
			gridTotales,
			NE.util.getEspaciador(20),
			gridTotales2,
			NE.util.getEspaciador(20),
			gridTotalesC
		]
	});

	catalogoEPO.load();
	catalogoIF.load();
	catalogoEstado.load();
	catalogoEstatus.load();
	catalogoMoneda.load();

	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '13consulta1ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});
	
});