 <%@ page contentType="application/json;charset=UTF-8"
	import=" 
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		java.text.SimpleDateFormat,
		com.netro.descuento.*, 
		
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray, net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion     =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	String infoRegresar  = "";
	int start=0; int limit=0;  
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	
	String tipoDescuento = (request.getParameter("descuento")==null)?"":request.getParameter("descuento");
	String modalidad  	= (request.getParameter("modalidad")==null)?"":request.getParameter("modalidad");
	String ic_proceso  	= (request.getParameter("ic_proceso")==null)?"":request.getParameter("ic_proceso");
	String ic_epo  	   = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String numRegistro  	   = (request.getParameter("numRegistro")==null)?"":request.getParameter("numRegistro");
	String numTotal  	   = (request.getParameter("numTotal")==null)?"":request.getParameter("numTotal");
	int numReg = 0;
	if(!numRegistro.equals("")) {
		numReg = Integer.parseInt(numRegistro)+1;
	}	
			
	if (informacion.equals("CorreProceso")) {
		try 
		{	
			ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
			
			BeanSeleccionDocumento. vRealizarDescuentoAutomatico(ic_epo, "",null,0, tipoDescuento, modalidad, strDirectorioTemp, strDirectorioPublicacion );      
				
		} catch(Exception e) 
			{
				throw new AppException("Error en la ejecución del proceso!", e);
			}
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("numRegistro", String.valueOf(numReg));
			jsonObj.put("numTotales", String.valueOf(numTotal));			
			infoRegresar = jsonObj.toString();
	}
	
	else if(informacion.equals("ConsultaGrid") || informacion.equals("ConsultaDetalle") ||informacion.equals("GeneraCSV")  )
	{
		try 
		{
			ConsDsctoAutomaticoEPO paginador = new ConsDsctoAutomaticoEPO();
			paginador.setTipoDescuento(tipoDescuento);
			paginador.setModalidad(modalidad);
			paginador.setProceso(ic_proceso);
			paginador.setNoEpo(ic_epo);
				
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
			try
			{
				 start = Integer.parseInt(request.getParameter("start"));
				 limit = Integer.parseInt(request.getParameter("limit"));	
				
			} catch(Exception e)
			  { System.out.println("Error en parametros de paginación"); }
			  
			/* 1.- Consulta del Grid General 						*/
			if (informacion.equals("ConsultaGrid")) {
				Registros reg = queryHelper.doSearch();
				infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
			}
						
			/* 2.- Consulta del Grid de Detalle	de Doctos. 		*/
			else if (informacion.equals("ConsultaDetalle")) 
			{			
				if (tipoDescuento.equals("II") || tipoDescuento.equals("N19")) //Si el tipo de Descuento es Instruccion Irrevocable o Automatico Edirlo toma como Normal
					paginador.setTipoDescuento("N");
				
					if(operacion.equals("Generar")) 						
						queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
					
					String  consultar = queryHelper.getJSONPageResultSet(request,start,limit);	
					jsonObj = JSONObject.fromObject(consultar);
					infoRegresar=jsonObj.toString();	
			}
			
			/* 3.- Genera Archivo Xls del Detalle	de Doctos.	*/
			else if (informacion.equals("GeneraCSV")) 
			{
				if (tipoDescuento.equals("II") || tipoDescuento.equals("N19")) //Si el tipo de Descuento es Instruccion Irrevocable o Automatico Edirlo toma como Normal
					paginador.setTipoDescuento("N");
				
				paginador.setConsultaDetalle("S");
														
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "CSV");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
			
		} catch(Exception e) 
			{
				throw new AppException("Error en la recuperacion de datos!", e);
			}
		
   }

//System.out.println("infoRegresar============"+infoRegresar); 
%>
<%=infoRegresar%> 