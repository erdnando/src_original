Ext.onReady(function() {
	/*--------------------------------- HANDLERS -------------------------------*/	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var el = grid.getGridEl();	
		var jsonData = store.reader.jsonData;	
		var cm = grid.getColumnModel();
		if (arrRegistros != null) {
			
			if (!grid.isVisible()) {
				grid.show();
			}	
			var consultar = Ext.getCmp('btnConsultar');
			var limpiar = Ext.getCmp('btnLimpiar');
			var depurar = Ext.getCmp('btnDepurar');
			if(store.getTotalCount() > 0) {
				consultar.enable();
				limpiar.enable();
				depurar.enable();
				el.unmask();
			} else {	
				depurar.disable();
				el.mask('No se encontraron registros.', 'x-mask');
			}
		}
	}
	function procesaEliminar(opts, success, response) {
		var btnDepurar = Ext.getCmp('btnDepurar');
		btnDepurar.disable();
		btnDepurar.setIconClass('icoEliminar');
		var inter = (Ext.getCmp("cmbIntermediario")).getValue();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
						consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:"Consultar",
							intermediario : inter
							})
					});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarDepurar(){
		var  grid = Ext.getCmp('grid');
		var store = grid.getStore();
		var inter = (Ext.getCmp("cmbIntermediario")).getValue();
		var icSolicitud = '';
		var coma= 0;
		store.each(function(record) {
		var numDocumentos = store.indexOf(record);
			if(record.data['DEPURAR']=='true' ||  record.data['DEPURAR']==true ){	
				if(coma == 0){
					icSolicitud += record.data['icSolicitud'];
				}else{
					icSolicitud += "," + record.data['icSolicitud'];    
				}
				coma++;	
			}
			intermediario = record.data['intermediario'];
		});
		if(icSolicitud =='') {
			Ext.MessageBox.alert('Mensaje','Seleccione al menos un registro.');
			return false;	
		}else  {	
			var btnDepurar = Ext.getCmp('btnDepurar');
			btnDepurar.disable();
			btnDepurar.setIconClass('loading-indicator');
			Ext.Ajax.request({
				url: '13DescargaTablasEmergentes01ext.data.jsp',
				params:Ext.apply (fp.getForm().getValues(),{
					informacion: 'Depurar',
					intermediario:inter,
					icSolicitud:icSolicitud									
				})
				,callback: procesaEliminar
			});
		}	
	}
	
	function leeRespuesta(){
		window.location = '13DescargaTablasEmergentes01ext.jsp';
	}
	function procesarDescargaArchivos(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var desArchivo = function (grid,rowIndex,colIndex,item,event){
		
		var registro = grid.getStore().getAt(rowIndex);
		var icSolicitud = registro.get('icSolicitud');
		Ext.Ajax.request({
			url: '13DescargaTablasEmergentes01ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{				
				informacion:'Descargar',
				 extension:'csv',				
				 icSolicitud: icSolicitud
			}),
			callback: procesarDescargaArchivos
		});		
	}
	//-------------------------------- STORES -----------------------------------
	var catalogoInF = new Ext.data.JsonStore({
	
		id: 'catInF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13DescargaTablasEmergentes01ext.data.jsp',
		baseParams: {
			informacion: 'catIntermediarioFinanciero'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	var consultaData   = new Ext.data.JsonStore({ 
		root:'registros',	
		url : '13DescargaTablasEmergentes01ext.data.jsp',
		baseParams:{
			informacion:'Consultar'
		},	
		fields: [	
			{name:'numeroDocumentos'},
			{name: 'montoDocumentos' },	
			{name: 'montoDescuentos' },	
			{name: 'programa'  },
			{name: 'usuario'  },
			{name: 'nombreUsuario'  },
			{name: 'fechaEnvio'  },
			{name: 'icSolicitud' },
			{name: 'DEPURAR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: 
		{
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}
	
	});
	/*********** Grid�s *************/
	var grid = new Ext.grid.EditorGridPanel({
		title:'',
		id: 'grid',
		store: consultaData,
		stripeRows: true,
		loadMask:false,
		hidden: true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		style: ' margin:0 auto;',
		height:400,
		width:900,			
		frame:true, 
		header:true,		
		columns: [
			{
				header:		'N�mero de Tablas',
				tooltip:		'N�mero de Tablas',			
				dataIndex:	'numeroDocumentos',	
				sortable:true,	
				resizable:true,	
				width:100,
				align:'center'
			},
			{
				header:		'Monto Total de Documentos',
				tooltip:		'Monto Total de Documentos',
				dataIndex:	'montoDocumentos',	
				sortable:true,	
				resizable:true,	
				width:150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header:		'Monto Total de Descuento',
				tooltip: 	'Monto Total de Descuento',
				dataIndex:	'montoDescuentos',
				sortable:true,
				resizable:true,	
				width:150, 
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header:		'<center>Programa</center>', 
				tooltip: 	'Programa',	
				dataIndex: 	'programa',	
				sortable:true, 
				resizable:true,
				width:100, 	
				align:'center'
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',
				dataIndex: 'icSolicitud',
				width: 100,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('detalle') ==0 ) {
						return 'N/A';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
						if(registro.get('detalle') !=0 ) {
							this.items[0].tooltip = 'Descargar';
							return 'icoXls';		
							}								
						},handler: desArchivo
						
					}
				]				
			},
			{
				header:		'Usuario',
				tooltip:		'Usuario',
				dataIndex:	'usuario',		
				sortable:true, 
				resizable:true,
				width:100, 
				align:'center'
			},
			{
				header:		'<center>Nombre de Usuario</center>',
				tooltip:		'Nombre de Usuario',
				dataIndex:	'nombreUsuario',		
				sortable:true, 
				resizable:true,
				width:150, 
				align:'center'
				
			},
			{
				header:		'Fecha de Env�o',
				tooltip:		'Fecha de Env�o',
				dataIndex:	'fechaEnvio',		
				sortable:true, 
				resizable:true,
				width:100, 
				align:'center'
				
			},
			{
				xtype:'checkcolumn',
				header:		'Depurar',
				tooltip:		'Depurar',
				dataIndex:	'DEPURAR',
				sortable:true, 
				resizable:true,
				width:50
			}	
		],	
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,	
		bbar: {
			items: [
				'->','-',
				{
				xtype: 'button',
					text: 'Depurar',					
					tooltip:	'Depurar',
					iconCls: 'icoEliminar',
					id: 'btnDepurar',
					formBind: true	,
				handler: function(boton,evento) {
					
					procesarDepurar();
				}
			}
			]
		}
	});
	//-------------------------------- COMPONENTES -----------------------------------	
	var elementosForma = [
	{
			xtype: 'combo',
			name: 'intermediario',
			id: 'cmbIntermediario',
			hiddenName : 'intermediario',
			fieldLabel: 'Intermediario Financiero',
			mode: 'local', 
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',		
			emptyText: 'Seleccionar...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,			
			store : catalogoInF,
			tpl : NE.util.templateMensajeCargaCombo
	}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		title: ' ',
		frame:true,
		collapsible: true,
		titleCollapse: false,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items:	elementosForma,
		defaultType: 'textfield',
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true	,
				handler: function(boton, evento) {	
					var intermadiario = Ext.getCmp('cmbIntermediario');
					if(Ext.isEmpty(intermadiario.getValue())){
						intermadiario.markInvalid('Seleccione un Intermediario');
						return;
					}
					fp.el.mask('Consultando...', 'x-mask-loading');
					Ext.getCmp('grid').hide();
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion : 'Consultar'
						})
					}); 
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				formBind: true	,
				handler: function() {
					fp.el.mask('Limpiando...', 'x-mask-loading');
					leeRespuesta();
				}
			}
		]					
	}); 

	//-------------------------------- PRINCIPAL -----------------------------------	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 950,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});
	catalogoInF.load();
});
