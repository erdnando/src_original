<%@ page contentType="application/json;charset=UTF-8" 
	import="   java.util.*,
				java.io.*,
				java.sql.*,
				java.math.*,
				java.text.*,
				com.netro.descuento.*, 
				netropology.utilerias.*,   
				com.netro.model.catalogos.CatalogoSimple,   
				net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/15cadenas/015secsession_extjs.jspf"%>
<%  
	String informacion = (request.getParameter("informacion") !=null)?request.getParameter("informacion"):"";
	String radio = (request.getParameter("cs_tipo")!=null)?request.getParameter("cs_tipo"):"";
	String numIntermediario = (request.getParameter("numIntermediario")!=null)?request.getParameter("numIntermediario"):""; 
	String cmbIF = (request.getParameter("intermediarioFinanciero")!=null)?request.getParameter("intermediarioFinanciero"):""; 
	String fechaVencimientoIni = (request.getParameter("fechaVencimientoIni")!=null)?request.getParameter("fechaVencimientoIni"):""; 
	String fechaVencimientoFin = (request.getParameter("fechaVencimientoFin")!=null)?request.getParameter("fechaVencimientoFin"):""; 
	String fechaProbaPagoIni = (request.getParameter("fechaProbaPagoIni")!=null)?request.getParameter("fechaProbaPagoIni"):""; 
	String fechaProbaPagoFin = (request.getParameter("fechaProbaPagoFin")!=null)?request.getParameter("fechaProbaPagoFin"):""; 
	String fechaPagoIni = (request.getParameter("fechaPagoIni")!=null)?request.getParameter("fechaPagoIni"):""; 
	String fechaPagoFin = (request.getParameter("fechaPagoFin")!=null)?request.getParameter("fechaPagoFin"):""; 
	String fechaRegistroIni = (request.getParameter("fechaRegistroIni")!=null)?request.getParameter("fechaRegistroIni"):""; 
	String fechaRegistroFin = (request.getParameter("fechaRegistroFin")!=null)?request.getParameter("fechaRegistroFin"):""; 
	PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
	ConsulPagosEnviados pag = new ConsulPagosEnviados();
	JSONObject jsonObj = new JSONObject();
	String resultado			= null;
	String qrySentencia		= "";
	String condicion 			= "";  
	int start					= 0;
	int limit 					= 2;
	AccesoDB con 				= null;
	PreparedStatement ps 	= null;
	ResultSet rs 				= null;
	String infoRegresar = "", consulta = "";
	DecimalFormat convertir = new DecimalFormat("$0.00");
	//convertir.setMaximumFractionDigits(3);
	if(informacion.equals("catalogoIntermediario")){
		 try {
			con = new AccesoDB();
         con.conexionDB();
			 qrySentencia =
				"  SELECT          /*+       use_nl(E,I)       index(E CP_COM_ENCABEZADO_PAGO_PK)       index(I CP_COMCAT_IF_PK)       */    "+
				"  DISTINCT e.ic_if, i.cg_razon_social, i.ic_financiera  "+
				"  FROM com_encabezado_pago e, comcat_if i "+
				"	WHERE e.ic_if = i.ic_if AND i.cs_tipo = '"+radio+"'	"+
				"  ORDER BY i.ic_financiera ";
			rs = con.queryDB(qrySentencia);
			HashMap datos;
         List reg = new ArrayList();
			while(rs.next()) {            
            datos = new HashMap();
            datos.put("clave",rs.getString(1));
            datos.put("descripcion",rs.getString(3)+" "+rs.getString(2));
            reg.add(datos);
			}
			rs.close();
		   con.cierraStatement();
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			infoRegresar = jsonObj.toString();
		 } catch(Exception e) {
			out.println(e.getMessage()); 
			e.printStackTrace();
		} finally {	
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB();	
		}
	}else  if (informacion.equals("Consultar")){
	
		pag.setFechaVencimientoIni(fechaVencimientoIni);
		pag.setFechaVencimientoFin(fechaVencimientoFin);
		pag.setFechaProbaPagoIni(fechaProbaPagoIni);
		pag.setFechaProbaPagoFin(fechaProbaPagoFin);
		pag.setFechaPagoIni(fechaPagoIni);
		pag.setFechaPagoFin(fechaPagoFin);
		pag.setFechaRegistroIni(fechaRegistroIni);
		pag.setfechaRegistroFin(fechaRegistroFin);
		pag.setNumIntermediario(numIntermediario);
		pag.setSopcion(radio);
		pag.setIc_if(cmbIF);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( pag);
		Registros reg	=	queryHelper.doSearch();	
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);	
		infoRegresar = jsonObj.toString();  
	}else  if (informacion.equals("consultaProcesosDetalle") || informacion.equals("GenerarArchivoCSV")|| informacion.equals("ArchivoPDF")){


		String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";//C2,C3
		String tipoVer = (request.getParameter("tipoVer")!=null)?request.getParameter("tipoVer"):"";//E,T
	   String sEncabezadoPago = (request.getParameter("sEncabezadoPago")!=null)?request.getParameter("sEncabezadoPago"):""; //clave para busqueda
		pag.setSopcion(opcion);
		pag.setEncabezadoPago(sEncabezadoPago);
		pag.setCs_tipo(radio);
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( pag);
		HashMap datos = new HashMap();
		List registros1  = new ArrayList();   
		if(informacion.equals("GenerarArchivoCSV")){
		
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}if(informacion.equals("ArchivoPDF")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}else if (informacion.equals("consultaProcesosDetalle")){
		
			HashMap  encabezado=	pag.getEncabezado(sEncabezadoPago,opcion );
			List registros  = new ArrayList();
			int numReg = 0;
			if(radio.equals("NB")){
				datos = new HashMap();
				if(!opcion.equals("C3")){
					datos = new HashMap();
					if(encabezado.size()>0){
						datos.put("ETIQUETA","E");
						datos.put("CLAVE_IF",encabezado.get("IC_FINANCIERA")==null?"":encabezado.get("IC_FINANCIERA"));
						datos.put("CLAVE_ESTATAL",encabezado.get("IC_OF_CONTROLADORA")==null?"":encabezado.get("IC_OF_CONTROLADORA"));
						datos.put("CLAVE_MONEDA",encabezado.get("IC_MONEDA")==null?"":encabezado.get("IC_MONEDA"));
						datos.put("BANCO",encabezado.get("BANCO")==null?"":encabezado.get("BANCO"));
						datos.put("DF_VENCIMIENTO",encabezado.get("DF_PERIODO_FIN")==null?"":encabezado.get("DF_PERIODO_FIN"));
						datos.put("DF_PAGO",encabezado.get("DF_PROBABLE_PAGO")==null?"":encabezado.get("DF_PROBABLE_PAGO"));
						datos.put("DF_DEPOSITO",encabezado.get("DF_DEPOSITO")==null?"":encabezado.get("DF_DEPOSITO"));
						double importeDeposi= Double.parseDouble(String.valueOf(encabezado.get("IG_IMPORTE_DEPOSITO")==null?"0":encabezado.get("IG_IMPORTE_DEPOSITO")));
						datos.put("IMPORTE",convertir.format(importeDeposi).toString());///
						datos.put("REFERENCIA_BANCO",encabezado.get("CG_REFERENCIA_BANCO")==null?"":encabezado.get("CG_REFERENCIA_BANCO"));
						datos.put("REFERENCIA_INTERMEDIARIO",encabezado.get("REFERENCIA_INTER")==null?"":encabezado.get("REFERENCIA_INTER"));
						numReg++;
						registros1.add(datos);
					}
				}else{//FILA
					datos = new HashMap();
					if(encabezado.size()>0){
						datos.put("ETIQUETA","E");
						datos.put("CLAVE_IF",encabezado.get("IC_FINANCIERA")==null?"":encabezado.get("IC_FINANCIERA"));
						datos.put("CLAVE_ESTATAL",encabezado.get("IC_OF_CONTROLADORA")==null?"":encabezado.get("IC_OF_CONTROLADORA"));
						datos.put("CLAVE_MONEDA",encabezado.get("IC_MONEDA")==null?"":encabezado.get("IC_MONEDA"));
						datos.put("BANCO",encabezado.get("BANCO")==null?"":encabezado.get("BANCO"));
						datos.put("DF_VENCIMIENTO",encabezado.get("DF_PERIODO_FIN")==null?"":encabezado.get("DF_PERIODO_FIN"));
						datos.put("DF_PAGO",encabezado.get("DF_PROBABLE_PAGO")==null?"":encabezado.get("DF_PROBABLE_PAGO"));
						datos.put("DF_DEPOSITO",encabezado.get("DF_DEPOSITO")==null?"":encabezado.get("DF_DEPOSITO"));
						double importeDeposi= Double.parseDouble(String.valueOf(encabezado.get("IG_IMPORTE_DEPOSITO")==null?"0":encabezado.get("IG_IMPORTE_DEPOSITO")));

						datos.put("IMPORTE",convertir.format(importeDeposi).toString());///
						datos.put("REFERENCIA_BANCO",encabezado.get("CG_REFERENCIA_BANCO")==null?"":encabezado.get("CG_REFERENCIA_BANCO"));
						datos.put("REFERENCIA_INTERMEDIARIO",encabezado.get("REFERENCIA_INTER")==null?"":encabezado.get("REFERENCIA_INTER"));
						numReg++;
						registros1.add(datos);
					}
				}
			}else{
				
				datos = new HashMap();
				if(!opcion.equals("C3")){
					datos = new HashMap();
					if(encabezado.size()>0){
						datos.put("ETIQUETA","E");
						datos.put("CLAVE_IF",encabezado.get("IC_FINANCIERA")==null?"":encabezado.get("IC_FINANCIERA"));
						datos.put("CLAVE_ESTATAL",encabezado.get("IC_OF_CONTROLADORA")==null?"":encabezado.get("IC_OF_CONTROLADORA"));
						datos.put("CLAVE_MONEDA",encabezado.get("IC_MONEDA")==null?"":encabezado.get("IC_MONEDA"));
						datos.put("BANCO",encabezado.get("BANCO")==null?"":encabezado.get("BANCO"));
						datos.put("DF_PAGO",encabezado.get("DF_PROBABLE_PAGO")==null?"":encabezado.get("DF_PROBABLE_PAGO"));
						datos.put("DF_DEPOSITO",encabezado.get("DF_DEPOSITO")==null?"":encabezado.get("DF_DEPOSITO"));
						double importeDeposi= Double.parseDouble(String.valueOf(encabezado.get("IG_IMPORTE_DEPOSITO")==null?"0":encabezado.get("IG_IMPORTE_DEPOSITO")));
						datos.put("IMPORTE",convertir.format(importeDeposi).toString());
						datos.put("REFERENCIA_BANCO",encabezado.get("CG_REFERENCIA_BANCO")==null?"":encabezado.get("CG_REFERENCIA_BANCO"));
						datos.put("REFERENCIA_INTERMEDIARIO",encabezado.get("REFERENCIA_INTER")==null?"":encabezado.get("REFERENCIA_INTER"));
						numReg++;
						registros1.add(datos);
					}
				}else{//FILA
					datos = new HashMap();
					
					if(encabezado.size()>0){
						datos.put("ETIQUETA","E");
						datos.put("CLAVE_IF",encabezado.get("IC_FINANCIERA")==null?"":encabezado.get("IC_FINANCIERA"));
						datos.put("CLAVE_ESTATAL",encabezado.get("IC_OF_CONTROLADORA")==null?"":encabezado.get("IC_OF_CONTROLADORA"));
						datos.put("CLAVE_MONEDA",encabezado.get("IC_MONEDA")==null?"":encabezado.get("IC_MONEDA"));
						datos.put("BANCO",encabezado.get("BANCO")==null?"":encabezado.get("BANCO"));
						datos.put("DF_PAGO",encabezado.get("DF_PROBABLE_PAGO")==null?"":encabezado.get("DF_PROBABLE_PAGO"));
						datos.put("DF_DEPOSITO",encabezado.get("DF_DEPOSITO")==null?"":encabezado.get("DF_DEPOSITO"));
						double importeDeposi= Double.parseDouble(String.valueOf(encabezado.get("IG_IMPORTE_DEPOSITO")==null?"0":encabezado.get("IG_IMPORTE_DEPOSITO")));
						datos.put("IMPORTE",convertir.format(importeDeposi).toString());
						datos.put("REFERENCIA_BANCO",encabezado.get("CG_REFERENCIA_BANCO")==null?"":encabezado.get("CG_REFERENCIA_BANCO"));
						datos.put("REFERENCIA_INTERMEDIARIO",encabezado.get("REFERENCIA_INTER")==null?"":encabezado.get("REFERENCIA_INTER"));
						numReg++;
						registros1.add(datos);
					}
				}
			}
			   
			if(!tipoVer.equals("E")){
				Registros reg	=	queryHelper.doSearch();	
				while(reg.next()){
					datos = new HashMap();
					datos.put("ETIQUETA","D");
					if(radio.equals("NB")){
						datos.put("CLAVE_IF",reg.getString("IG_SUBAPLICACION")==null?"":reg.getString("IG_SUBAPLICACION"));
						datos.put("CLAVE_ESTATAL",reg.getString("IG_PRESTAMO")==null?"":reg.getString("IG_PRESTAMO"));
						datos.put("CLAVE_MONEDA",reg.getString("IG_CLIENTE")==null?"":reg.getString("IG_CLIENTE"));
						double capital= Double.parseDouble(String.valueOf(reg.getString("FG_AMORTIZACION")==null?"":reg.getString("FG_AMORTIZACION")));
						double interes= Double.parseDouble(String.valueOf(reg.getString("FG_INTERES")==null?"":reg.getString("FG_INTERES")));
						double mora= Double.parseDouble(String.valueOf(reg.getString("FG_INTERES_MORA")==null?"":reg.getString("FG_INTERES_MORA")));
						double subsidio= Double.parseDouble(String.valueOf(reg.getString("FG_SUBSIDIO")==null?"":reg.getString("FG_SUBSIDIO")));
						double comision= Double.parseDouble(String.valueOf(reg.getString("FG_COMISION")==null?"":reg.getString("FG_COMISION")));
						double iva= Double.parseDouble(String.valueOf(reg.getString("FG_IVA")==null?"":reg.getString("FG_IVA")));
						double totalExigible= Double.parseDouble(String.valueOf(reg.getString("FG_TOTALEXIGIBLE")==null?"":reg.getString("FG_TOTALEXIGIBLE")));
						datos.put("BANCO",convertir.format(capital).toString());
						datos.put("DF_VENCIMIENTO",convertir.format(interes).toString());  
						datos.put("DF_PAGO",convertir.format(mora).toString());
						datos.put("DF_DEPOSITO",convertir.format(subsidio).toString());
						datos.put("IMPORTE",convertir.format(comision).toString());
						datos.put("REFERENCIA_BANCO",convertir.format(iva).toString());
						datos.put("REFERENCIA_INTERMEDIARIO",convertir.format(totalExigible).toString());
						datos.put("CG_CONCEPTO_PAGO",reg.getString("CG_CONCEPTO_PAGO")==null?"":reg.getString("CG_CONCEPTO_PAGO"));
						datos.put("CG_ORIGEN_PAGO",reg.getString("CG_ORIGEN_PAGO")==null?"":reg.getString("CG_ORIGEN_PAGO"));
					}else{
						datos.put("CLAVE_IF",reg.getString("IG_SUCURSAL").equals("0")?"":reg.getString("IG_SUCURSAL"));
						datos.put("CLAVE_ESTATAL",reg.getString("IG_SUBAPLICACION")==null?"":reg.getString("IG_SUBAPLICACION"));
						datos.put("CLAVE_MONEDA",reg.getString("IG_PRESTAMO")==null?"":reg.getString("IG_PRESTAMO"));
						datos.put("BANCO",reg.getString("CG_ACREDITADO")==null?"":reg.getString("CG_ACREDITADO"));
						datos.put("DF_PAGO",reg.getString("DF_OPERACION")==null?"":reg.getString("DF_OPERACION"));
						datos.put("DF_DEPOSITO",reg.getString("DF_VENCIMIENTO")==null?"":reg.getString("DF_VENCIMIENTO"));
						datos.put("IMPORTE",reg.getString("DF_PAGO")==null?"":reg.getString("DF_PAGO"));
						double saldoInsoluto= Double.parseDouble(String.valueOf(reg.getString("FN_SALDO_INSOLUTO")==null?"":reg.getString("FN_SALDO_INSOLUTO")));
						double tasa= Double.parseDouble(String.valueOf(reg.getString("IG_TASA")==null?"":reg.getString("IG_TASA")));
						datos.put("REFERENCIA_BANCO",convertir.format(saldoInsoluto).toString());
						datos.put("REFERENCIA_INTERMEDIARIO",String.valueOf(tasa));
						double capital= Double.parseDouble(String.valueOf(reg.getString("FG_AMORTIZACION")==null?"":reg.getString("FG_AMORTIZACION")));
						double interes= Double.parseDouble(String.valueOf(reg.getString("FG_INTERES")==null?"":reg.getString("FG_INTERES")));
						double comision= Double.parseDouble(String.valueOf(reg.getString("FG_COMISION")==null?"":reg.getString("FG_COMISION")));
						double iva= Double.parseDouble(String.valueOf(reg.getString("FG_IVA")==null?"":reg.getString("FG_IVA")));
						double totalExigible= Double.parseDouble(String.valueOf(reg.getString("FG_TOTALEXIGIBLE")==null?"":reg.getString("FG_TOTALEXIGIBLE")));
						datos.put("FG_AMORTIZACION",convertir.format(capital).toString());
						datos.put("IG_DIAS",reg.getString("IG_DIAS").equals("0")?"":reg.getString("IG_DIAS"));
						datos.put("FG_INTERES",convertir.format(interes).toString());
						datos.put("FG_COMISION",convertir.format(comision).toString());
						datos.put("FG_IVA",convertir.format(iva).toString());
						datos.put("FG_TOTALEXIGIBLE",convertir.format(totalExigible).toString());
					}
					numReg++;
					registros1.add(datos);
				}
			}
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("total", new Integer(registros1.size()));
			jsonObj.put("opcion", opcion);
			jsonObj.put("registros", registros1);   
			infoRegresar = jsonObj.toString();     
			System.out.println("resultados --- "+infoRegresar);
		}
	}else if(informacion.equals("Eliminar")){
		String numRegistros  =(request.getParameter("numRegistros")!=null) ? request.getParameter("numRegistros"):"";
		try{
			int i_numReg = Integer.parseInt(numRegistros);
			String icEncabezadoPago[] = request.getParameterValues("icEncabezadoPago");
			for(int i =0; i<i_numReg;i++){
				bean.eliminaPagos(icEncabezadoPago[i]);
			}
			jsonObj.put("success", new Boolean(true));
		} catch(Throwable e){
			throw new   AppException("Error al consultar los datos", e);
		}
		infoRegresar = jsonObj.toString();  
	}else if(informacion.equals("ConsultarTotales")){
			String sEncabezadoPago = (request.getParameter("sEncabezadoPago")!=null)?request.getParameter("sEncabezadoPago"):""; //clave para busqueda
			try{
					JSONArray totales =pag.getTotales(sEncabezadoPago);
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("total", new Integer(totales.size()));
					jsonObj.put("registros", totales);
					infoRegresar= jsonObj.toString();      
				} catch(Exception e){
					throw new AppException("Error al consultar los totales", e);   
				}
		
	
			
		}  
%>
<%=  infoRegresar%>
