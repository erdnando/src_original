Ext.onReady(function() {
	
/*--------------------------------- HANDLERS -------------------------------*/
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var grid = Ext.getCmp('grid');	
		var el = grid.getGridEl();	
		var jsonData = store.reader.jsonData;	
		var cm = grid.getColumnModel();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
				
			}
			if(store.getTotalCount() > 0) {
				el.unmask();
				
			} else {	
				el.mask('No se encontraron registros.', 'x-mask');	
			}
		}
		
	}
	function leeRespuesta(){
		window.location = '13detalleclientes01ext.jsp';
	}
	//-------------------------------- STORES -----------------------------------
	
	var catalogoInterFinanciero = new Ext.data.JsonStore({
	   id				: 'catIntermediario',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url : '13detalleclientes01ext.data.jsp',
		baseParams	: { informacion: 'catIntermediarioFinanciero'},
		totalProperty : 'total',
		autoLoad		: false,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaData   = new Ext.data.JsonStore({ 
		root:'registros',	
		url : '13detalleclientes01ext.data.jsp',
		baseParams:{
			informacion:'Consultar'
		},	
		fields: [	
			{name:'NUMERO_DE_PRESTAMO'},
			{name: 'MONTO' },	
			{name: 'IC_SOLIC_PORTAL' },	
			{name: 'NUMERO_DE_CLIENTES'  },
			{name: 'FECHA_DE_CARGA'  }
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: 
		{
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}
	
	});
	
	
	/*********** Grid�s *************/
 
	var grid = new Ext.grid.EditorGridPanel({
		title:'',
		id: 'grid',
		store: consultaData,
		stripeRows: true,
	//	margins: '100 200 200 200',
		loadMask:false,
		hidden: true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		style: ' margin:0 auto;',
		height:400,
		width:950,			
		frame:true, 
		//header:true,
		columns: [
			{
				header:		'N&uacute;mero&nbsp;de Pr&eacute;stamo',
				tooltip:		'N&uacute;mero&nbsp;de Pr&eacute;stamo',			
				dataIndex:	'NUMERO_DE_PRESTAMO',	
				sortable:true,	
				resizable:true,	
				width:150,
				align:'center'
			},
			{
				header:		'Monto (M.N.)',
				tooltip:		'Monto (M.N.)',
				dataIndex:	'MONTO',	
				sortable:true,	
				resizable:true,	
				width:100,
				align:'right'
			},
			{
				header:		'N&uacute;mero&nbsp;de Clientes',
				tooltip: 	'N&uacute;mero&nbsp;de Clientes',
				dataIndex:	'NUMERO_DE_CLIENTES',
				sortable:true,
				resizable:true,	
				width:100, 
				align:'center'				
			},
			{
				header:		'Fecha&nbsp;de Carga',
				tooltip:		'Fecha&nbsp;de Carga',
				dataIndex:	'FECHA_DE_CARGA',		
				sortable:true, 
				resizable:true,
				width:100, 
				align:'center'
			}
		],	
		height: 450,
		width: 450,
		frame: false,
		align: 'center',
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true
	});
			

//-------------------------------- COMPONENTES -----------------------------------	
	
	var elementosForma = [
	{
		xtype				: 'combo',
		id: 'cmbInter',
		name				: 'ic_ifnb',
		hiddenName 		: 'ic_ifnb',
		fieldLabel  	: 'Intermediario Financiero No Bancario',
		forceSelection	: true,
		allowBlank		: true,
		//msgTarget	: 'side',
		triggerAction	: 'all',
		mode				: 'local',
		emptyText		:'Seleccionar...',
		valueField		: 'clave',
		displayField	: 'descripcion',
		width				: 230,
		autoLoad: false,
		typeAhead: true,
		minChars : 1,
		store				: catalogoInterFinanciero,
		tpl : NE.util.templateMensajeCargaCombo
		
	}
	];
			

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		autoHeight	: true,
		labelWidth: 140,
		bodyStyle: 'padding: 20px',
		frame:true,
		collapsible: true,
		style: ' margin:0 auto;',
		title: '   ',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:	elementosForma,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true	,
				handler: function(boton, evento) {	
					var noBancario = Ext.getCmp('cmbInter');
					if(Ext.isEmpty(noBancario.getValue())){
						noBancario.markInvalid('Debe especificar un Intermediario Financiero');
						return;
					}
					Ext.getCmp('grid').hide();
					fp.el.mask('Consultando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion : 'Consultar'
						})
					}); 
				}
				
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				formBind: true	,
				handler: function() {
					fp.el.mask('Limpiando...', 'x-mask-loading');
					leeRespuesta();
				}
			}
		]							
	}); 

	//-------------------------------- PRINCIPAL -----------------------------------	
	var pnl = new Ext.Container({
		
		applyTo: 'areaContenido',
		width: 890,
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),			
			fp,
			NE.util.getEspaciador(20),
			grid,
			NE.util.getEspaciador(20)		
		]
	});
	
	catalogoInterFinanciero.load();
	
});//Fin de funcion principal
