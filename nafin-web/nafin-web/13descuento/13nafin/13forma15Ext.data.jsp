<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		javax.naming.Context,
		org.apache.commons.logging.Log,
		com.netro.model.catalogos.CatalogoEPONafin,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";

log.debug("informacion = <"+informacion+">");

if (	informacion.equals("CatalogoEpo") ) {
	
	CatalogoEPONafin cat = new CatalogoEPONafin();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setOrden("2");

	infoRegresar = cat.getJSONElementos();

}else if (	informacion.equals("CatalogoEpoConsulta") ) {
	
	CatalogoEPONafin cat = new CatalogoEPONafin();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setHabilitado("");
	cat.setContratoElectronico("S");
	cat.setOrden("2");

	infoRegresar = cat.getJSONElementos();

} else if (        	informacion.equals("AceptaCapura.subirArchivo") 				)	{	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);

	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;

	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {

		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(4194304);
		myUpload.upload();

	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 4194304) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 4 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
	}

	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 	= myUpload.getRequest();
	String tipoRegistro 							= (myRequest.getParameter("tipoRegistro")	== null)?"":myRequest.getParameter("tipoRegistro");
	String claveEpo 								= (myRequest.getParameter("ic_epo")			== null)?"":myRequest.getParameter("ic_epo");
	

	com.jspsmart.upload.File archivo 		= null;
	String extension								= null;
	String nombreArchivo							= null;
	String imagen									= null;
	String doctoActualizado						= "N";	
	String consecutivo							= null;

	if ("ACTUALIZAR".equals(tipoRegistro)) {
		consecutivo			= (myRequest.getParameter("ic_consecutivo")==null)?"":myRequest.getParameter("ic_consecutivo");
		doctoActualizado	= (myRequest.getParameter("doctoActualizado")==null)?"N":myRequest.getParameter("doctoActualizado");
	}

	try {

		if ( "ACTUALIZAR".equals(tipoRegistro) && doctoActualizado.equals("N") ) { //Dado que no hay archivo nuevo se mantiene el actual (Se obtiene de BD)
			ContratoEpoArchivo contratoArchivo = new ContratoEpoArchivo();
			contratoArchivo.setClaveEpo(claveEpo);
			contratoArchivo.setConsecutivo(consecutivo);
			nombreArchivo	= contratoArchivo.getArchivoContratoEpo(strDirectorioTemp);
			extension		= nombreArchivo.substring(nombreArchivo.length()-3).toLowerCase();
		} else { 
			archivo			= myUpload.getFiles().getFile(0);
			//Nombre del Archivo temporal
			extension		= archivo.getFileExt().toLowerCase();
			nombreArchivo	= Comunes.cadenaAleatoria(8) + "." + extension;
			archivo.saveAs(strDirectorioTemp + nombreArchivo);
		}

	}catch(Exception e){

		success		= false;
		log.error("AceptaCapura.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}

	resultado.put("clausulado_preview",	strDirecVirtualTemp+nombreArchivo	);
	resultado.put("consecutivo",			consecutivo									);
	resultado.put("doctoActualizado",	doctoActualizado							);
	resultado.put("extension",				extension									);
	resultado.put("tipoRegistro",			tipoRegistro								);
	resultado.put("nombreArchivo",		nombreArchivo								);

	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 	"" 	);
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)		);

	infoRegresar = resultado.toString();

} else if (        	informacion.equals("AceptaCapura.Aceptar") 				)	{

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	ParametrosDescuento paramDsctoEJB = null;
	try {

		paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	}catch(Exception e){
		success		= false;
		log.error("AceptaCapura.Aceptar(Exception): Obtener instancia del EJB de ParametrosDescuento");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ParametrosDescuento.");

	}

	String tipoRegistro		= (request.getParameter("tipoRegistro")==null)?"":request.getParameter("tipoRegistro");
	
	String consecutivo		= null;
	String doctoActualizado	= null;

	if ("ACTUALIZAR".equals(tipoRegistro)) {
		consecutivo				= request.getParameter("ic_consecutivo");
		doctoActualizado		= request.getParameter("doctoActualizado");
	}

	String claveEpo			= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String tituloAviso		= (request.getParameter("cg_titulo_aviso") == null)?"":request.getParameter("cg_titulo_aviso");
	String contenidoAviso	= (request.getParameter("cg_contenido_aviso") == null)?"":request.getParameter("cg_contenido_aviso");
	String botonAviso			= (request.getParameter("cg_boton_aviso")==null)?"":request.getParameter("cg_boton_aviso");
	String textoAceptacion	= (request.getParameter("cg_texto_aceptacion")==null)?"":request.getParameter("cg_texto_aceptacion");
	String botonAceptacion	= (request.getParameter("cg_boton_aceptacion") == null)?"":request.getParameter("cg_boton_aceptacion");
	String mostrar				= (request.getParameter("cs_mostrar")==null)?"N":request.getParameter("cs_mostrar");
	mostrar						= (mostrar.equals("on"))?"S":"N";
	String nombreArchivo		= (request.getParameter("nombreArchivo")==null)?"N":request.getParameter("nombreArchivo");
	String rutaArchivo		= strDirectorioTemp + nombreArchivo;

	ContratoEpo contrato		= new ContratoEpo();
	contrato.setClaveEpo(claveEpo);

	if ("ACTUALIZAR".equals(tipoRegistro)) {
		contrato.setConsecutivo(consecutivo);
	}

	contrato.setTituloAviso(tituloAviso);
	contrato.setContenidoAviso(contenidoAviso);
	contrato.setBotonAviso(botonAviso);
	contrato.setTextoAceptacion(textoAceptacion);
	contrato.setBotonAceptacion(botonAceptacion);
	contrato.setMostrar(mostrar);

	if ("ACTUALIZAR".equals(tipoRegistro)) {
		//Se envía null en la ruta del archivo si este no fue modificado
		paramDsctoEJB.actualizarClausulado( contrato, ((doctoActualizado.equals("S"))?rutaArchivo:null) );
	} else if ("NUEVO".equals(tipoRegistro)) {
		consecutivo = paramDsctoEJB.guardarClausulado(contrato, rutaArchivo);
	}

	List registros = paramDsctoEJB.getDatosClausulado(claveEpo, consecutivo);
	//solo trae un registro la lista.
	ContratoEpo contratoGuardado = (ContratoEpo) registros.get(0);

	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");

	List regs = new ArrayList();
	HashMap hash = new HashMap();
	hash.put("NOMBRE_EPO",		contratoGuardado.getNombreEpo()					);
	hash.put("DOCUMENTO",		strDirecVirtualTemp+nombreArchivo				);
	hash.put("FECHA_ALTA",		sdf.format(contratoGuardado.getFechaAlta())	);
	hash.put("ACTIVA",			contratoGuardado.getMostrar()						);
	regs.add(hash);

	jsonObj.put("registrosCapturadosData",	regs										);

	jsonObj.put("estadoSiguiente",	"RESPUESTA_ACEPTA_CAPTURA");
	jsonObj.put("success",				new Boolean(success));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Consultar") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	ParametrosDescuento paramDsctoEJB = null;
	try {

		paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	}catch(Exception e){
		success		= false;
		log.error("Consultar(Exception): Obtener instancia del EJB de ParametrosDescuento");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ParametrosDescuento.");

	}

	String claveEpo		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String consecutivo	= null;

	List registros = paramDsctoEJB.getDatosClausulado(claveEpo, consecutivo);
	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");

	List regs = new ArrayList();

	Iterator it = registros.iterator();
	while (it.hasNext()) {
		ContratoEpo contrato = (ContratoEpo)it.next();
		HashMap hash = new HashMap();
		hash.put("CONSECUTIVO",		contrato.getConsecutivo()					);
		hash.put("CLAVE_EPO",		contrato.getClaveEpo()						);
		hash.put("NOMBRE_EPO",		contrato.getNombreEpo()						);
		hash.put("FECHA_ALTA",		sdf.format(contrato.getFechaAlta())		);
		hash.put("ACTIVA",			contrato.getMostrar()						);
		hash.put("MODIFICAR",		new Boolean(contrato.esUltimo())			);
		regs.add(hash);
	} //fin while

	jsonObj.put("registrosConsultadosData",	regs								);
	jsonObj.put("estadoSiguiente",				"RESPUESTA_CONSULTA"			);
	jsonObj.put("success",							new Boolean(success));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Accion.modificar") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	ParametrosDescuento paramDsctoEJB = null;
	try {

		paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	}catch(Exception e){
		success		= false;
		log.error("Consultar(Exception): Obtener instancia del EJB de ParametrosDescuento");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ParametrosDescuento.");

	}

	String claveEpo		= (request.getParameter("clave_epo")==null)?"":request.getParameter("clave_epo");
	String consecutivo	= (request.getParameter("consecutivo")==null)?"":request.getParameter("consecutivo");

	List registros = paramDsctoEJB.getDatosClausulado(claveEpo, consecutivo);
	ContratoEpo contrato = (ContratoEpo)registros.get(0);
	
	
	jsonObj.put("tipoRegistro",			"ACTUALIZAR"						);
	jsonObj.put("ic_epo",					contrato.getClaveEpo()			);
	jsonObj.put("ic_consecutivo",			contrato.getConsecutivo()		);
	jsonObj.put("doctoActualizado",		"N"									);
	jsonObj.put("nombreEpo",				contrato.getNombreEpo()			);
	jsonObj.put("cg_titulo_aviso",		contrato.getTituloAviso()		);
	jsonObj.put("cg_contenido_aviso",	contrato.getContenidoAviso()	);
	jsonObj.put("cg_boton_aviso",			contrato.getBotonAviso()		);
	jsonObj.put("cg_texto_aceptacion",	contrato.getTextoAceptacion()	);
	jsonObj.put("cg_boton_aceptacion",	contrato.getBotonAceptacion()	);
	jsonObj.put("cs_mostrar",				contrato.getMostrar()			);

	jsonObj.put("estadoSiguiente",				"RESPUESTA.ACCION.MODIFICAR"			);
	jsonObj.put("success",							new Boolean(success));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Accion.abrir_clausulado") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	String claveEpo		= (request.getParameter("clave_epo")==null)?"":request.getParameter("clave_epo");
	String consecutivo	= (request.getParameter("consecutivo")==null)?"":request.getParameter("consecutivo");

	ContratoEpoArchivo contratoArchivo = new ContratoEpoArchivo();
	contratoArchivo.setClaveEpo(claveEpo);
	contratoArchivo.setConsecutivo(consecutivo);
	String nombreArchivo = contratoArchivo.getArchivoContratoEpo(strDirectorioTemp);

	jsonObj.put("nombreArchivo",		strDirecVirtualTemp + nombreArchivo		);
	jsonObj.put("estadoSiguiente",	"RESPUESTA.ACCION.ABRIR_CLAUSULADO"		);
	jsonObj.put("success",				new Boolean(success)							);
	infoRegresar = jsonObj.toString();

}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>