<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.text.*,
	com.netro.model.catalogos.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
		com.netro.descuento.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoEPONafin,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
	<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>	
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String indiceEst= (request.getParameter("indiceEst") != null) ? request.getParameter("indiceEst") : "";
String tipoOper= (request.getParameter("tipoOperacion") != null) ? request.getParameter("tipoOperacion") : "";
String banderaFISO= (request.getParameter("banderaFISO") != null) ? request.getParameter("banderaFISO") : "";



SimpleDateFormat formatoHora2 = new SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a");
String infoRegresar ="", consulta="";
JSONObject jsonObj = new JSONObject();
String nombreArchivo="";
String NoEstatusDocto[]={"2","3","24","","","","","11","26"};
String NoEstatusSolic[]={"","","","1","2","3","10","",""};
//String claveIF	= iNoCliente;
if (informacion.equals("valoresIniciales")  ) {  

	jsonObj.put("success",new Boolean(true));
	jsonObj.put("strNombreUsuario", strNombreUsuario);
	jsonObj.put("fechaHora",formatoHora2.format(new java.util.Date()));
	infoRegresar = jsonObj.toString();
 
}
else if (informacion.equals("CatalogoEstatus") ) {
		
		JSONObject jsonObjRet= new JSONObject();
		JSONArray jsonArrEst=new JSONArray();
		
		jsonArrEst.add(nuevoNodoEstatusCat("1","Negociables"));
		jsonArrEst.add(nuevoNodoEstatusCat("2","Seleccionada PYME"));
		jsonArrEst.add(nuevoNodoEstatusCat("3","En Proceso de Autorización IF"));
		jsonArrEst.add(nuevoNodoEstatusCat("4","Seleccionada IF"));
		jsonArrEst.add(nuevoNodoEstatusCat("5","En Proceso"));
		jsonArrEst.add(nuevoNodoEstatusCat("6","Operada"));
		jsonArrEst.add(nuevoNodoEstatusCat("11","Operado con Fondeo Propio"));
		jsonArrEst.add(nuevoNodoEstatusCat("7","Operada Pagada"));
		jsonArrEst.add(nuevoNodoEstatusCat("10","Programado PyME"));
			
		jsonObjRet.put("success", new Boolean(true));
		jsonObjRet.put("registros", jsonArrEst );
		infoRegresar =	jsonObjRet.toString();	
}

else  if (informacion.equals("Consultar")  ||  informacion.equals("ResumenTotales")  ||  informacion.equals("ResumenTotales_FISO")   ) {
	
	RepDoctoSolicxEstatusDEbean ejb=new RepDoctoSolicxEstatusDEbean();
	int i=Integer.parseInt(indiceEst);
	
	if(NoEstatusDocto[i].equals("") && !NoEstatusSolic[i].equals(""))
	{
		ejb.setNoEstatusSolic(Integer.parseInt(NoEstatusSolic[i]));	
	}
	else if(!NoEstatusDocto[i].equals("") && NoEstatusSolic[i].equals(""))
	{
		ejb.setNoEstatusDocto(Integer.parseInt(NoEstatusDocto[i]));
	}
		
	ConsDoctosSolicXEstatusNafin paginador = new ConsDoctosSolicXEstatusNafin(ejb);
	paginador.setEstatus(indiceEst);
	paginador.setTipoOper(tipoOper);
	paginador.setBanderaFISO(banderaFISO); 
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if (operacion.equals("Generar") ){
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}	
	} else if (operacion.equals("ArchivoPDF")) {
		try {
			 nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}				
	} else if (operacion.equals("ArchivoCSV")) {
		try {
			 nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}		
	}
	
	 if (informacion.equals("ResumenTotales")   ||   informacion.equals("ResumenTotales_FISO")   ) {		//Datos para el Resumen de Totales
		
		Registros reg	=	paginador.getDocumentQueryFile_Con();		
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("estatus1", indiceEst);	
		infoRegresar = jsonObj.toString();  		
	
	}
	
/*
	if ( informacion.equals("ResumenTotales_") ) {
	
		Registros reg	=	paginador.getDocumentQueryFile_Con();
	
		boolean dolaresDocto = false;
		boolean nacionalDocto = false;
		
		boolean dolaresDocto_fid = false;
		boolean nacionalDocto_fid = false;
		
		double nacional = 0,nacionalNC = 0,nacional_descNC = 0,dblMontoDescuento = 0,fn_monto = 0,
		dblPorcentaje = 0,dblTotalMontoDescuentoDolares = 0,dblTotalMontoDescuentoPesos=0
		,fn_monto_dscto=0,nacional_desc = 0,nacional_int = 0,nacional_descIF = 0,
		dolares_desc = 0, dolares_int = 0, dolares_descIF = 0,in_importe_interes=0,in_importe_recibir=0;

		double nacional_fid = 0,nacionalNC_fid = 0,nacional_descNC_fid = 0,dblMontoDescuento_fid = 0,fn_monto_fid = 0,
		dblPorcentaje_fid = 0,dblTotalMontoDescuentoDolares_fid = 0,dblTotalMontoDescuentoPesos_fid=0
		,fn_monto_dscto_fid=0,nacional_desc_fid = 0,nacional_int_fid = 0,nacional_descIF_fid = 0,
		dolares_desc_fid = 0, dolares_int_fid = 0, dolares_descIF_fid = 0,in_importe_interes_fid=0,in_importe_recibir_fid=0,
		nacional_int_fiso_s=0,nacional_descIF_fiso_s=0,dolares_descIF_fiso_s=0,dolares_int_fiso_s = 0;
		
		int  doc_nacional =0,doc_dolares =0,dolaresNC=0,dolares_descNC=0,dolares = 0;
		int  doc_nacional_fid =0,doc_dolares_fid =0,dolaresNC_fid=0,dolares_descNC_fid=0,dolares_fid = 0;	
		
		HashMap registrosTot = new HashMap();	
		HashMap registrosTot_fid = new HashMap();
		JSONArray registrosTotales = new JSONArray();
		JSONArray registrosTotales_fid = new JSONArray();
		String tipoFactoraje ="",ic_moneda="",fn_montoS="",dblPorcentajeS=""
		,fn_monto_dsctoS="",tipo_ope="";;
		while (reg.next()) {
			
			ic_moneda = (reg.getString("NUMEROMONEDA") == null) ? "" : reg.getString("NUMEROMONEDA");
			fn_montoS =(reg.getString("MONTODOCTO") == null) ? "0" : reg.getString("MONTODOCTO");
			fn_monto = Double.parseDouble(fn_montoS);
			
			System.out.println(" indiceEst   " +indiceEst);
			System.out.println(" tipo_ope   " +tipo_ope);
				
		if(indiceEst.equals("0"))
		{
				 tipoFactoraje = (reg.getString("TIPOFACTORAJE")==null)?"":reg.getString("TIPOFACTORAJE");
				 dblPorcentajeS =(reg.getString("AFORO") == null) ? "0" : reg.getString("AFORO");
							
				dblPorcentaje=Double.parseDouble(dblPorcentajeS);
				dblMontoDescuento = fn_monto * (dblPorcentaje / 100);			
				
				
			if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I") ) {
				dblMontoDescuento = fn_monto;
				}
			
			if (ic_moneda.equals("1")) {
				if(tipoFactoraje.equals("C")) {
					nacionalNC += fn_monto;
					nacional_descNC += dblMontoDescuento;
				} else {
					dblTotalMontoDescuentoPesos += dblMontoDescuento;
					nacional+=fn_monto;
					nacionalDocto = true;
					doc_nacional++;
				}
			}
			if (ic_moneda.equals("54")) {
				if(tipoFactoraje.equals("C")) {
					dolaresNC += fn_monto;
					dolares_descNC += dblMontoDescuento;
				} else {
					dblTotalMontoDescuentoDolares += dblMontoDescuento;
					dolares+=fn_monto;
					dolaresDocto = true;
					doc_dolares++;
				}
			}
		}
			if(indiceEst.equals("1")||indiceEst.equals("2")||indiceEst.equals("3")
			||indiceEst.equals("4")||indiceEst.equals("5")||indiceEst.equals("6")
			||indiceEst.equals("7")||indiceEst.equals("8")){
			fn_monto_dsctoS= (reg.getString("MONTODSCTO") == null) ? "0" : reg.getString("MONTODSCTO");
			tipo_ope=(reg.getString("BANDERA_FISO") == null) ? "" : reg.getString("BANDERA_FISO");
			if(!indiceEst.equals("7")  )
			{
				if(!indiceEst.equals("5"))
				in_importe_interes =(reg.getString("IMPORTEINTERES") == null) ? 0 : Double.parseDouble(reg.getString("IMPORTEINTERES"));
				in_importe_recibir =(reg.getString("IMPORTERECIBIR") == null) ? 0 : Double.parseDouble(reg.getString("IMPORTERECIBIR"));
				
				if( tipo_ope.equals("S"))
				{
					if(!indiceEst.equals("5"))
					in_importe_interes_fid =(reg.getString("IMPORTEINTERES_FID") == null) ? 0 : Double.parseDouble(reg.getString("IMPORTEINTERES_FID"));
					in_importe_recibir_fid =(reg.getString("IMPORTERECIBIR_FID") == null) ? 0 : Double.parseDouble(reg.getString("IMPORTERECIBIR_FID"));
				}
			}
			fn_monto_dscto=Double.parseDouble(fn_monto_dsctoS);
			if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
				fn_monto_dscto = fn_monto;
				dblPorcentaje = 100;
			}		
				if((!tipoFactoraje.equals("C") && (indiceEst.equals("8"))) ||
					(indiceEst.equals("1")||indiceEst.equals("2")|| indiceEst.equals("3")
					||indiceEst.equals("4")||indiceEst.equals("5")
					||indiceEst.equals("6")||indiceEst.equals("7"))) { // No se toma en cuenta para la suma de los montos las Notas de Crédito.
					if (ic_moneda.equals("1")) {						
						
						if(tipo_ope.equals("N")|| (indiceEst.equals("8")))
						{
							nacionalDocto = true;
							nacional+=fn_monto;
							nacional_desc+=fn_monto_dscto;
							nacional_int+=in_importe_interes;
							nacional_descIF+=in_importe_recibir;
							doc_nacional++;
						}
						else if( tipo_ope.equals("S") && !(indiceEst.equals("8")))
						{
							nacionalDocto_fid = true;
							nacional_fid+=fn_monto;
							nacional_desc_fid+=fn_monto_dscto;
							nacional_int_fid+=in_importe_interes;
							nacional_descIF_fid+=in_importe_recibir;
							doc_nacional_fid++;
							nacional_int_fiso_s+=in_importe_interes_fid;
							nacional_descIF_fiso_s+=in_importe_recibir_fid;
						}
					}
					if(ic_moneda.equals("54")) {
						if(tipo_ope.equals("N")|| (indiceEst.equals("8")))
						{
							dolares+=fn_monto;
							dolares_desc+=fn_monto_dscto;
							dolares_int+=in_importe_interes;
							dolares_descIF+=in_importe_recibir;
							dolaresDocto = true;
							doc_dolares++;
						}else if( tipo_ope.equals("S"))
						{
							dolares_fid+=fn_monto;
							dolares_desc_fid+=fn_monto_dscto;
							dolares_int_fid+=in_importe_interes;
							dolares_descIF_fid+=in_importe_recibir;
							dolaresDocto_fid = true;
							doc_dolares_fid++;
							dolares_int_fiso_s+=in_importe_interes_fid;
							dolares_descIF_fiso_s+=in_importe_recibir_fid;
						}
					}
				}
			}
		}		
			registrosTot = new HashMap();		
			registrosTot_fid = new HashMap();		
			if (nacionalDocto) { 				
				registrosTot.put("MONEDA", "Total MN");
				registrosTot.put("NUM_DOCTOS", Double.toString(doc_nacional));		
				registrosTot.put("TOTAL_MONTO_DOCTO",  Comunes.formatoMoneda2(String.valueOf(nacional-nacionalNC),false) );
				registrosTot.put("TOTAL_REC_GAR",Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoPesos-nacional_descNC),false));					
				if(!indiceEst.equals("0"))
				registrosTot.put("MONTO_DESC",Comunes.formatoMoneda2(String.valueOf(nacional_desc),false));
				else
				registrosTot.put("MONTO_DESC",Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoPesos-nacional_descNC),false));					
				
				registrosTot.put("MONTO_INT",Comunes.formatoMoneda2(String.valueOf(nacional_int),false));	
				registrosTot.put("MONTO_OPER",Comunes.formatoMoneda2(String.valueOf(nacional_descIF),false));
				
				registrosTotales.add(registrosTot);	
			}
			if (nacionalDocto_fid) { 				
				registrosTot_fid.put("MONEDA", "Total MN");
				registrosTot_fid.put("NUM_DOCTOS", Double.toString(doc_nacional_fid));		
				registrosTot_fid.put("TOTAL_MONTO_DOCTO",  Comunes.formatoMoneda2(String.valueOf(nacional_fid-nacionalNC_fid),false) );
				registrosTot_fid.put("TOTAL_REC_GAR",Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoPesos_fid-nacional_descNC_fid),false));					
				if(!indiceEst.equals("0"))
				registrosTot_fid.put("MONTO_DESC",Comunes.formatoMoneda2(String.valueOf(nacional_desc_fid),false));
				else
				registrosTot_fid.put("MONTO_DESC",Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoPesos_fid-nacional_descNC_fid),false));					
				
				registrosTot_fid.put("MONTO_INT",Comunes.formatoMoneda2(String.valueOf(nacional_int_fid),false));	
				registrosTot_fid.put("MONTO_OPER",Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fid),false));
				
				registrosTot_fid.put("MONTO_INT_FISO",Comunes.formatoMoneda2(String.valueOf(nacional_int_fiso_s),false));	
				registrosTot_fid.put("MONTO_OPER_FISO",Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fiso_s),false));
				registrosTotales_fid.add(registrosTot_fid);	
			}
			registrosTot = new HashMap();
			registrosTot_fid = new HashMap();		
				if (dolaresDocto){ 			
				registrosTot.put("MONEDA", "Total USD");
				registrosTot.put("NUM_DOCTOS", Double.toString(doc_dolares));		
				registrosTot.put("TOTAL_MONTO_DOCTO",  Comunes.formatoMoneda2(String.valueOf(dolares-dolaresNC),false) );
				registrosTot.put("TOTAL_REC_GAR",Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoDolares-dolares_descNC),false));					
				if(!indiceEst.equals("0"))
				registrosTot.put("MONTO_DESC",Comunes.formatoMoneda2(String.valueOf(dolares_desc),false));					
				else
				registrosTot.put("MONTO_DESC",Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoDolares-dolares_descNC),false));					
				
				registrosTot.put("MONTO_INT",Comunes.formatoMoneda2(String.valueOf(dolares_int),false));	
				registrosTot.put("MONTO_OPER",Comunes.formatoMoneda2(String.valueOf(dolares_descIF),false));
		
				registrosTotales.add(registrosTot);	
			}
			if (dolaresDocto_fid){ 			
				registrosTot_fid.put("MONEDA", "Total USD");
				registrosTot_fid.put("NUM_DOCTOS", Double.toString(doc_dolares_fid));		
				registrosTot_fid.put("TOTAL_MONTO_DOCTO",  Comunes.formatoMoneda2(String.valueOf(dolares_fid-dolaresNC_fid),false) );
				registrosTot_fid.put("TOTAL_REC_GAR",Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoDolares_fid-dolares_descNC_fid),false));					
				if(!indiceEst.equals("0"))
				registrosTot_fid.put("MONTO_DESC",Comunes.formatoMoneda2(String.valueOf(dolares_desc_fid),false));					
				else
				registrosTot_fid.put("MONTO_DESC",Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoDolares_fid-dolares_descNC_fid),false));					
				
				registrosTot_fid.put("MONTO_INT",Comunes.formatoMoneda2(String.valueOf(dolares_int_fid),false));	
				registrosTot_fid.put("MONTO_OPER",Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fid),false));
				
				registrosTot_fid.put("MONTO_INT_FISO",Comunes.formatoMoneda2(String.valueOf(dolares_int_fiso_s),false));	
				registrosTot_fid.put("MONTO_OPER_FISO",Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fiso_s),false));
		
				registrosTotales_fid.add(registrosTot_fid);	
			}
			
			infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+
			", \"total_fid\": \""+registrosTotales_fid.size() + "\", \"registros_fid\": " + registrosTotales_fid.toString()+"}";					
	}
	
	*/
	if (informacion.equals("Consultar")){
	/*	jsonObj.put("bTipoFactoraje",bTipoFactoraje);
		jsonObj.put("bOperaFactorajeDist",bOperaFactorajeDist);
		jsonObj.put("bOperaFactVencimientoInfonavit",bOperaFactVencimientoInfonavit);
		jsonObj.put("bOperaFactAutomatico",bOperaFactAutomatico);
		jsonObj.put("sOperaFactConMandato",sOperaFactConMandato);*/	
		jsonObj.put("index_estatus",indiceEst);	
		infoRegresar	=	jsonObj.toString();	
	}		
}


%>
<%!
private JSONObject nuevoNodoEstatusCat(String clave,String descrip)
{	
	JSONObject jsonObjReg= new JSONObject();jsonObjReg.put("clave",clave);
	jsonObjReg.put("descripcion",descrip);
	return jsonObjReg;
}
%>

<%=infoRegresar%>

