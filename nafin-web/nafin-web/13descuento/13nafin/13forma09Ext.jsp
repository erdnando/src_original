<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<script language="JavaScript" src="../../00utils/valida.js?<%=session.getId()%>"></script>
<%@ include file="/appComun.jspf" %>
<%@ include file="../13secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>    
<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS) {%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
<script type="text/javascript" src="13forma09Ext.js?<%=session.getId()%>"></script>
	 
</head>
<%if(esEsquemaExtJS) {%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<div id="_menuApp"></div>
	<div id="Contcentral">	
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
		<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
   </div>
</div>
<%@ include file="/01principal/01nafin/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
<%}else  if(!esEsquemaExtJS) {%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id="areaContenido" align="center"></div> <!-- dentro puede ir un style="margin-left: 3px; margin-top: 3px;" -->
<form id='formAux' name="formAux" target='_new'></form>
</body>
<%}%>


</html>