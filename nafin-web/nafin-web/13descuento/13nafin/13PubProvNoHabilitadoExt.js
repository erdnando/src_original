Ext.onReady(function() {

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

	var procesarSuccessFailureArchivoCSV = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnArchivoCSV').enable();
		Ext.getCmp('btnArchivoCSV').setIconClass('icoXls');
	}
/*
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureArchivoCSV =  function(opts, success, response) {
		var btnArchivoCSV = Ext.getCmp('btnArchivoCSV');
		btnArchivoCSV.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();			
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.el.dom.scrollIntoView();
			btnBajarCSV.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnArchivoCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/

	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}

	var procesarConsultaRegistros = function(store, registros, opts){

		var forma = Ext.getCmp('forma');
		forma.el.unmask();

		if (registros != null) {

			var grid  = Ext.getCmp('gridConsulta');

			if (!grid.isVisible()) {
				grid.show();
			}

			Ext.getCmp('btnBajarPDF').hide();
			Ext.getCmp('btnBajarCSV').hide();
			Ext.getCmp('barraPaginacion').show();
			var el = grid.getGridEl();

			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				el.unmask();
			} else {
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}

			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
		}
	}

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

	var catalogoBancoFondeoData = new Ext.data.JsonStore({
		id:				'catalogoBancoFondeoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'13PubProvNoHabilitado.data.jsp',
		baseParams:		{	informacion: 'catalogoBancoFondeo'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners:	{
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEpoData = new Ext.data.JsonStore({
		id:				'catalogoEpoDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'13PubProvNoHabilitado.data.jsp',
		baseParams:		{	informacion:	'CatalogoEpo'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13PubProvNoHabilitado.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var registrosConsultadosData = new Ext.data.JsonStore({
		root: 		'registros',
		id:			'registrosConsultadosDataStore',
		url: 			'13PubProvNoHabilitado.data.jsp',
		baseParams: {	informacion:	'ConsultaRegistros'	},
		fields: [
			{ name: 'NOMBREPO', 						type: 'string' },
			{ name: 'PYME', 							type: 'string' },
			{ name: 'NAFINELECTRONICO', 			type: 'string' },
			{ name: 'IC_PYME', 						type: 'string' },
			{ name: 'RFC', 							type: 'string' },
			{ name: 'TELEFONO',						type: 'string' },
			{ name: 'RAZONSOCIALPROVEEDOR',		type: 'string' },
			{ name: 'NUMERODOCUMENTOS',			type: 'string' },
			{ name: 'TOTAL',							type: 'float' },
			{ name: 'MONEDA',							type: 'string' }
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload:	{
				fn: function(store, options){
					/*
					console.log("options.params(beforeload)");
					console.dir(options.params);
					*/
				}
			},
			load: 	procesarConsultaRegistros,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistros(null, null, null);
				}
			}
		}
	});

//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - 

	var procesaConsulta = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){

		if(  estadoSiguiente == "OBTENER_NOMBRE_PYME" 										){
		
			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '13PubProvNoHabilitado.data.jsp',	
				params: {
							numeroDeProveedor:	respuesta.noNafinElec,
							ic_banco_fondeo:		respuesta.ic_banco_fondeo,
							ic_epo:					respuesta.ic_epo,
							informacion: 			"obtenNombrePyme" 
				},
				callback: procesaConsulta
			});

		}else if(  estadoSiguiente == "RESPUESTA_OBTENER_NOMBRE_PYME" 										){
		
			Ext.getCmp('hid_ic_pyme').setValue(respuesta.ic_pyme)
			Ext.getCmp('_txt_nombre').setValue(respuesta.txtCadenasPymes);

			if (respuesta.muestraMensaje != undefined && respuesta.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	respuesta.textoMensaje);
				grid.hide();
			}

		} else if(  estadoSiguiente == "CONSULTAR" 											){

			var forma = Ext.getCmp("forma");
			forma.el.mask('Buscando...','x-mask-loading');
			
			// Ocultar gridPanel con el resultado anterior para evitar que el usuario realice mas de una accion
			Ext.getCmp("gridConsulta").hide();
			Ext.StoreMgr.key('registrosConsultadosDataStore').load({
				params:	Ext.apply(fp.getForm().getValues(),
							{
								informacion: 'Consultar',
								operacion: 'Generar',
								start: 0,
								limit: 15
							}
				)
			});

		} else if(	estadoSiguiente == "LIMPIAR"														){

			Ext.getCmp('forma').getForm().reset();
			grid.hide();

		} else if(	estadoSiguiente == "FIN"															){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "13PubProvNoHabilitadoExt.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 6, align: 'center'},
				{header: 'Documentos negociables', colspan: 2, align: 'center'},
				{header: '', colspan: 1, align: 'center'}
			]
		]
	});

	// Grid con las cuentas registradas
	var grid = new Ext.grid.GridPanel({
		store: 		registrosConsultadosData,
		id:			'gridConsulta',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		margins:		'20 0 0 0',
		title:		undefined,
		//view:			new Ext.grid.GridView({forceFit:	true}),
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		width: 		850,
		frame: 		false,
		plugins:		grupos,
		columns: [
			//.toUpperCase();
			{
				header: 		'EPO',
				tooltip: 	'EPO',
				dataIndex: 	'NOMBREPO',
				sortable: 	true,
				resizable: 	true,
				width: 		180,
				hidden: 		false,
				hideable:	false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header: 		'No Proveedor',
				tooltip: 	'No Proveedor',
				dataIndex: 	'PYME',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				hideable:	false
			},{
				header: 		'No. Nafin Electr�nico',
				tooltip: 	'No. Nafin Electr�nico',
				dataIndex: 	'NAFINELECTRONICO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				hideable:	false
			},{
				header: 		'RFC',
				tooltip: 	'RFC',
				dataIndex: 	'RFC',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				hideable:	false
			},{
				header: 		'Tel�fono Contacto',
				tooltip: 	'Tel�fono Contacto',
				dataIndex: 	'TELEFONO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				hideable:	false
			},{
				header: 		'Nombre � Razon Social',
				tooltip: 	'Nombre � Razon Social',
				dataIndex: 	'RAZONSOCIALPROVEEDOR',
				align:		'left',
				sortable: 	true,
				resizable: 	true,
				width: 		180,
				hidden: 		false,
				hideable:	false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header: 		'Total',
				tooltip: 	'Total',
				dataIndex: 	'NUMERODOCUMENTOS',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				hideable:	false
			},{
				header: 		'Monto',
				tooltip: 	'Monto',
				dataIndex: 	'TOTAL',
				align:		'right',
				sortable: 	true,
				resizable: 	true,
				width: 		120,
				hidden: 		false,
				hideable:	false,
				renderer:	Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header: 		'Moneda',
				tooltip: 	'Moneda',
				dataIndex: 	'MONEDA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		180,
				hidden: 		false,
				hideable:	false
         }
		],
		bbar: {
			xtype: 			'paging',
			pageSize: 		15,
			buttonAlign: 	'left',
			id: 				'barraPaginacion',
			displayInfo: 	true,
			store: 			registrosConsultadosData,
			displayMsg: 	'{0} - {1} de {2}',
			emptyMsg: 		'No hay registros.',
			items: [
				'->','-',
				{
					xtype:	'button',
					text:    'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					id:      'btnGenerarPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13PubProvNoHabilitado.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoXPDF'
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},{
					xtype:  'button',
					text:   'Bajar PDF',
					id:     'btnBajarPDF',
					hidden: true
				},'-',{
					xtype:	 'button',
					iconCls:  'icoXls',
					tooltip:  'Imprime los registros en formato CSV.',
					text:     'Generar Archivo',
					id:       'btnArchivoCSV',
					disabled: true,
					handler:  function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13PubProvNoHabilitado.data.jsp',
							params:	Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoCSV'
							}),
							callback: procesarSuccessFailureArchivoCSV
						});
					}
				},{
					xtype:  'button',
					text:   'Bajar Archivo',
					id:     'btnBajarCSV',
					hidden: true
				}
			]
		}
	});

	var elementosForma = [
		{
			xtype:			'combo',
			id:				'_ic_banco_fondeo',
			name:				'ic_banco_fondeo',
			hiddenName:		'ic_banco_fondeo',
			fieldLabel:		'Banco de Fondeo',
			emptyText:		'Seleccionar. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:false,
			typeAhead:		true,
			hidden: true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoBancoFondeoData,
			tpl:				NE.util.templateMensajeCargaCombo,
			listeners:{
				'select':function(cbo){
								if(!Ext.isEmpty(cbo.getValue())){
										var comboEpo = Ext.getCmp('ic_epo');
										comboEpo.setValue('');
										comboEpo.store.removeAll();
										comboEpo.store.reload({	params: {ic_banco_fondeo: cbo.getValue()}	});
								}
							}
			}
		},{
			xtype:			'combo',
			id:				'ic_epo',
			name:				'_ic_epo',
			hiddenName:		'_ic_epo',
			fieldLabel:		'Nombre de la EPO',
			emptyText:		'Seleccionar. . .',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		true,
			mode:				'local',
			minChars			:1,
			store:			catalogoEpoData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:'hidden',
			id:	'hid_nombre',
			value:''
		},{
			xtype:'hidden',
			id:	'hid_ic_pyme',
			value:''
		},{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			fieldLabel: 'Proveedor',
			items: [
				{
					xtype:		'textfield',
					name:			'txt_nafelec',
					id:			'_txt_nafelec',
					maskRe:		/[0-9]/,
					allowBlank:	true,
					maxLength:	25,
					listeners:	{
						'change':function(field){
										if ( !Ext.isEmpty(field.getValue()) ) {

											var respuesta = new Object();
											respuesta["noNafinElec"]		= field.getValue();
											respuesta["ic_banco_fondeo"]	= Ext.getCmp('_ic_banco_fondeo').getValue();
											respuesta["ic_epo"]				= Ext.getCmp('ic_epo').getValue();
											
											accionConsulta("OBTENER_NOMBRE_PYME",respuesta);

										}
									}
					}
				},{
					xtype:			'textfield',
					name:				'txt_nombre',
					id:				'_txt_nombre',
					width:			270,
					disabledClass:	"x2",	//Para cambiar el estilo de la clase deshabilitada
					disabled:		true,
					allowBlank:		true,
					maxLength:		100
				}
			]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					id:'disEspacio',
					text:''
				},
				{
					xtype:	'button',
					id:		'btnBuscaA',
					iconCls:	'icoBuscar',
					text:		'B�squeda Avanzada',
					handler: function(boton, evento) {
								var winVen = Ext.getCmp('winBuscaA');
									if (winVen){
										Ext.getCmp('fpWinBusca').getForm().reset();
										Ext.getCmp('fpWinBuscaB').getForm().reset();
										
										Ext.getCmp('cmb_num_ne').setValue();
										Ext.getCmp('cmb_num_ne').store.removeAll();
										Ext.getCmp('cmb_num_ne').reset();
										winVen.show();
									}else{
										var winBuscaA = new Ext.Window ({
											id:'winBuscaA',
											height: 320,
											x: 300,
											y: 100,
											width: 550,
											heigth: 100,
											modal: true,
											closeAction: 'hide',
											title: 'B�squeda Avanzada',
											items:[
												{
													xtype:'form',
													id:'fpWinBusca',
													frame: true,
													border: false,
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													labelWidth: 130,
													items:[
														{
															xtype:'displayfield',
															frame:true,
															border: false,
															value:'Utilice el * para b�squeda gen�rica'
														},{
															xtype:'displayfield',
															frame:true,
															border: false,
															value:''
														},{
															xtype: 'textfield',
															name: 'nombre_pyme',
															id:	'txtNombre',
															fieldLabel:'Nombre',
															maxLength:	100
														},{
															xtype: 'textfield',
															name: 'rfc_pyme',
															id:	'txtRfc',
															fieldLabel:'RFC',
															maxLength:	20
														},{
															xtype: 'textfield',
															name: 'num_pyme',
															id:	'txtNe',
															fieldLabel:'N�mero Proveedor',
															maxLength:	25
														}
													],
													buttonAlign:'center',
													buttons:[
														{
															text:'Buscar',
															iconCls:'icoBuscar',
															handler: function(boton) {
																			catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoRechazar',
															handler: function() {
																			Ext.getCmp('winBuscaA').hide();
																		}
														}
													]
												},{
													xtype:'form',
													frame: true,
													id:'fpWinBuscaB',
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													monitorValid: true,
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													labelWidth: 80,
													items:[
														{
															xtype: 'combo',
															id:	'cmb_num_ne',
															name: 'ic_pyme',
															hiddenName : 'ic_pyme',
															fieldLabel: 'Nombre',
															emptyText: 'Seleccione Proveedor. . .',
															displayField: 'descripcion',
															valueField: 'clave',
															triggerAction : 'all',
															forceSelection:true,
															allowBlank: false,
															typeAhead: true,
															mode: 'local',
															minChars : 1,
															store: catalogoNombreProvData,
															tpl : NE.util.templateMensajeCargaCombo
														}
													],
													buttonAlign:'center',
													buttons:[
														{
															text:'Aceptar',
															iconCls:'aceptar',
															formBind:true,
															//hidden:true,
															handler: function() {
																			if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																				var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																				var cveP = disp.substr(0,disp.indexOf(" "));
																				var desc = disp.slice(disp.indexOf(" ")+1);
																				Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('cmb_num_ne').getValue())
																				Ext.getCmp('_txt_nafelec').setValue(cveP);
																				Ext.getCmp('_txt_nombre').setValue(desc);
																				Ext.getCmp('winBuscaA').hide();
																			}
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoRechazar',
															handler: function() {	Ext.getCmp('winBuscaA').hide();	}
														}
													]
												}
											]
										}).show();
									}
								}
				}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Publicaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype:			'datefield',
					name:				'df_fecha_publicacion_de',
					id:				'_df_fecha_publicacion_de',
					allowBlank:		false,
					startDay:		0,
					width:			100,
					msgTarget:		'side',
					vtype:			'rangofecha', 
					campoFinFecha:	'_df_fecha_publicacion_a',
					margins:			'0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 25
				},
				{
					xtype:				'datefield',
					name:					'df_fecha_publicacion_a',
					id:					'_df_fecha_publicacion_a',
					allowBlank:			false,
					startDay:			1,
					width:				100,
					msgTarget:			'side',
					vtype:				'rangofecha', 
					campoInicioFecha:	'_df_fecha_publicacion_de',
					margins:				'0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		title:			'Criterios de b�squeda',
		width:			610,
		style:			'margin:0 auto;',
		collapsible:	true,
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		monitorValid:	true,
		buttons: [
			{
				text:		'Buscar',
				id:		'btnBuscar',
				iconCls:	'icoBuscar',
				formBind:true,
				handler: function(boton, evento) {

								if(!Ext.getCmp('forma').getForm().isValid()){
									return;
								}

								var fechaMin = Ext.getCmp('_df_fecha_publicacion_de');
								var fechaMax = Ext.getCmp('_df_fecha_publicacion_a');
								if (!Ext.isEmpty(fechaMin.getValue()) || !Ext.isEmpty(fechaMax.getValue()) ) {
									if(Ext.isEmpty(fechaMin.getValue()))	{
										fechaMin.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
										fechaMin.focus();
										return;
									}else if (Ext.isEmpty(fechaMax.getValue())){
										fechaMax.markInvalid('Debe capturar ambas fechas de publicaci�n o dejarlas en blanco');
										fechaMax.focus();
										return;
									}
								}

								accionConsulta("CONSULTAR",null);

				} //fin handler
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
								accionConsulta("LIMPIAR",null);
							}
			},{
				text:		'Cancelar',
				id:		'btnCancelar',
				iconCls:	'borrar',
				hidden:	true,
				handler:	function(){
								accionConsulta("FIN",null);
							}
			}
		]
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(5),
			grid
		],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("forma").getEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Ocultar mascara del panel de resumen de carga de plantillas
			element = Ext.getCmp('gridConsulta').getGridEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Derefenrencia ultimo elemento...
			element = null;

		}

	});
	
	Ext.StoreMgr.key('catalogoBancoFondeoDataStore').load();
	Ext.StoreMgr.key('catalogoEpoDataStore').load();

});