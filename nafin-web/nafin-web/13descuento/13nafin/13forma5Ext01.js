Ext.onReady(function(){
	var tipoVer = "";
	var opcionG = "";
	var radio = "";
	var encabezado = "";
	function inicializa(){
		tipoVer = "";
	}
	function radioButton (){
		radio = "";
		opcionG = "";
		encabezado = "";
	}
//--------------------------------HANDLERS-----------------------------------
	function procesaEliminar(opts, success, response) {
		Ext.getCmp('btnEliminar').enable();
		var btnEliminar = Ext.getCmp('btnEliminar');
		btnEliminar.setIconClass('icoEliminar');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	//checar esta parte 
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar'												
						})
					});
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesarDepurar(){
		var  grid = Ext.getCmp('gridPrincipal');
		var store = grid.getStore();
		var icEncabezadoPago = [];
		var coma= 0;
		store.each(function(record) {
		var numDocumentos = store.indexOf(record);
			if( record.data['ELIMINAR']==true ){	
				icEncabezadoPago[coma]=record.data['IC_ENCABEZADO_PAGO'];
				coma++;	
			}
		});
		if(icEncabezadoPago =='') {
			Ext.MessageBox.alert('Mensaje','Debes seleccionar al menos un elemento para eliminar.');
			return false;	
		}else  {	
			Ext.getCmp('btnEliminar').enable();
			var btnDepurar = Ext.getCmp('btnEliminar');
			btnDepurar.setHandler(
				function(boton, evento) {	
					Ext.Ajax.request({
						url: '13forma5Ext01.data.jsp',
						params:Ext.apply (fp.getForm().getValues(),{
							informacion: 'Eliminar',
							icEncabezadoPago:icEncabezadoPago,
							numRegistros:coma
						})
						,callback: procesaEliminar
					});
				}
			);		
		}	
	}
	function mostrarArchivoCSV(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
			var btnImprimirCSV = Ext.getCmp('btnGenerarArchivo');   
		   btnImprimirCSV.setIconClass('icoXls');
			Ext.getCmp('btnGenerarArchivo').enable();
			
			var btnImprimirCSV = Ext.getCmp('btnGenerarArchivo_b');   
		   btnImprimirCSV.setIconClass('icoXls');
			Ext.getCmp('btnGenerarArchivo_b').enable();
		}else {
			Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
			 Ext.getCmp('btnGenerarArchivo').enable();
			 
			 Ext.getCmp('btnGenerarArchivo_b').setIconClass('icoXls');
			 Ext.getCmp('btnGenerarArchivo_b').enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	function mostrarArchivoPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
			var boton=Ext.getCmp('btnImprimir');
			boton.setIconClass('icoPdf');
			boton.enable();
			
			var boton=Ext.getCmp('btnImprimir_b');
			boton.setIconClass('icoPdf');
			boton.enable();
		}else {
			 Ext.getCmp('btnImprimir').setIconClass('icoPdf');
			 Ext.getCmp('btnImprimir').enable();
			 
			  Ext.getCmp('btnImprimir_b').setIconClass('icoPdf');
			 Ext.getCmp('btnImprimir_b').enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//FUNCION PARA LAS VALIDACIONES Y CONSULTA 
	var realizarConsulta = function(){
		var fp = Ext.getCmp('forma');
		var noInter = Ext.getCmp('tfNumIntermediario');
		var cmbInter = Ext.getCmp('cmbIntermediarioFinanciero');
		
		var fechaVencimientosDe = Ext.getCmp('dfFechaVencimientoIni');
		var fechaVencimientosA = Ext.getCmp('dfFechaVencimientoFin');
		
		var fechaProbableDe = Ext.getCmp('dfFechaProbaPagoIni');
		var fechaProbableA = Ext.getCmp('dfFechaProbaPagoFin');
		
		var fechaPagoDe = Ext.getCmp('dfFechaPagoIni');
		var fechaPagoA = Ext.getCmp('dfFechaPagoFin');
		
		var fechaRegistroDe = Ext.getCmp('dfFechaRegistroIni');
		var fechaRegistroA = Ext.getCmp('dfFechaRegistroFin');
		
		if(!Ext.isEmpty(fechaVencimientosDe.getValue()) || !Ext.isEmpty(fechaVencimientosA.getValue())){
			if(Ext.isEmpty(fechaVencimientosDe.getValue())){
				fechaVencimientosDe.markInvalid('El valor de la fecha de vencimiento inicial es requerido');
				fechaVencimientosDe.focus();
				return;
			}
			if(Ext.isEmpty(fechaVencimientosA.getValue())){
				fechaVencimientosA.markInvalid('El valor de la fecha de Vencimiento Final  es requerido');
				fechaVencimientosA.focus();
				return;
			}
		}
		if(!Ext.isEmpty(fechaProbableDe.getValue()) || !Ext.isEmpty(fechaProbableA.getValue())){
			if(Ext.isEmpty(fechaProbableDe.getValue())){
				fechaProbableDe.markInvalid('El valor de la fecha Probable de Pago inicial es requerido');
				fechaProbableDe.focus();
				return;
			}
			if(Ext.isEmpty(fechaProbableA.getValue())){
				fechaProbableA.markInvalid('El valor de la fecha Probable de Pago Final  es requerido');
				fechaProbableA.focus();
				return;
			}
		}
		if(!Ext.isEmpty(fechaPagoDe.getValue()) || !Ext.isEmpty(fechaPagoA.getValue())){
			if(Ext.isEmpty(fechaPagoDe.getValue())){
				fechaPagoDe.markInvalid('El valor de la fecha de pago inicial es requerido');
				fechaPagoDe.focus();
				return;
			}
			if(Ext.isEmpty(fechaPagoA.getValue())){
				fechaPagoA.markInvalid('El valor de la fecha de pago Final  es requerido');
				fechaPagoA.focus();
				return;
			}
		}
		if(!Ext.isEmpty(fechaRegistroDe.getValue()) || !Ext.isEmpty(fechaRegistroA.getValue())){
			if(Ext.isEmpty(fechaRegistroDe.getValue())){
				fechaRegistroDe.markInvalid('El valor de la fecha de registro inicial es requerido');
				fechaRegistroDe.focus();
				return;
			}
			if(Ext.isEmpty(fechaRegistroA.getValue())){
				fechaRegistroA.markInvalid('El valor de la fecha de registro Final  es requerido');
				fechaRegistroA.focus();
				return;
			}
		}
		if((!Ext.isEmpty(fechaVencimientosDe.getValue()) || !Ext.isEmpty(fechaVencimientosA.getValue()))&&(!Ext.isEmpty(fechaProbableDe.getValue()) || !Ext.isEmpty(fechaProbableA.getValue()))){			
			alert("Los criterios de busqueda son excluyentes. S�lo se puede realizar por Fecha de Vencimiento, Fecha Probable de Pago o Fecha de Pago");
			return;
		}else	if((!Ext.isEmpty(fechaVencimientosDe.getValue()) || !Ext.isEmpty(fechaVencimientosA.getValue()))&&(!Ext.isEmpty(fechaPagoDe.getValue()) || !Ext.isEmpty(fechaPagoA.getValue()))){			
			alert("Los criterios de busqueda son excluyentes. S�lo se puede realizar por Fecha de Vencimiento, Fecha Probable de Pago o Fecha de Pago");
			return;
		}else if((!Ext.isEmpty(fechaProbableDe.getValue()) || !Ext.isEmpty(fechaProbableA.getValue()))&&(!Ext.isEmpty(fechaPagoDe.getValue()) || !Ext.isEmpty(fechaPagoA.getValue()))){
			alert("Los criterios de busqueda son excluyentes. S�lo se puede realizar por Fecha de Vencimiento, Fecha Probable de Pago o Fecha de Pago");
			return;
		}else  if((!Ext.isEmpty(fechaRegistroDe.getValue()) || !Ext.isEmpty(fechaRegistroA.getValue()))&&(!Ext.isEmpty(fechaVencimientosDe.getValue()) || !Ext.isEmpty(fechaVencimientosA.getValue()))){
			alert("Los criterios de busqueda son excluyentes. S�lo se puede realizar por Fecha de Vencimiento, Fecha Probable de Pago o Fecha de Pago");
			return;
		}else if((!Ext.isEmpty(fechaRegistroDe.getValue()) || !Ext.isEmpty(fechaRegistroA.getValue()))&&(!Ext.isEmpty(fechaProbableDe.getValue()) || !Ext.isEmpty(fechaProbableA.getValue()))){
			alert("Los criterios de busqueda son excluyentes. S�lo se puede realizar por Fecha de Vencimiento, Fecha Probable de Pago o Fecha de Pago");
			return;
		}else if((!Ext.isEmpty(fechaRegistroDe.getValue()) || !Ext.isEmpty(fechaRegistroA.getValue()))&&(!Ext.isEmpty(fechaPagoDe.getValue()) || !Ext.isEmpty(fechaPagoA.getValue()))){
			alert("Los criterios de busqueda son excluyentes. S�lo se puede realizar por Fecha de Vencimiento, Fecha Probable de Pago o Fecha de Pago");
			return;
		}
		 if(Ext.isEmpty(cmbInter.getValue())&&Ext.isEmpty(noInter.getValue())){   
			alert("Debe capturar el Num. de Intermediario Financiero o Seleccionarlo");
			Ext.getCmp('btnConsultar').enable();
			return;
		}
		
		if(datecomp(fechaVencimientosDe.getValue(),fechaVencimientosA.getValue()) == "1"){
			alert("La fecha de fin no puede ser menor a la fecha de inicio");
			f.sFechaVencFin.focus();
			return;
		}  		
		 if(datecomp(fechaProbableDe.getValue(),fechaProbableA.getValue()) == "1"){
			alert("La fecha de fin no puede ser menor a la fecha de inicio");
			f.sFechaProbFin.focus();
			return;
		}  		   
		if(datecomp(fechaPagoDe.getValue(),fechaPagoA.getValue()) == "1"){
			alert("La fecha de fin no puede ser menor a la fecha de inicio");
			f.sFechaPagoFin.focus();
			return;
		}  		
		if(datecomp(fechaRegistroDe.getValue(),fechaRegistroA.getValue()) == "1"){
			alert("La fecha de fin no puede ser menor a la fecha de inicio");
			f.sFechaRegFin.focus();
			return;
		}  	
		
		fp.el.mask('Consultando...', 'x-mask-loading');
		consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar'												
						})
		});
	}
	//FUNCION QUE VALIDA QUE LA CONSULTA SE REALICE EXITOSAMENTE
	var procesarConsultaProcesosData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		Ext.getCmp('btnConsultar').enable();
		var grid = Ext.getCmp('gridPrincipal');
		var btnEliminar = Ext.getCmp('btnEliminar');
		grid.el.unmask();
		if (arrRegistros != null) {	
			if(!gridPrincipal.isVisible()){
				gridPrincipal.show();
			}
			if(store.getTotalCount() > 0) {
				grid.el.unmask();
				btnEliminar.enable();
				
			} else {	
				btnEliminar.disable();
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var mostrarVentanaDetallesEncabezado = function(grid, rowIndex, colIndex, item, event){
		inicializa();
		tipoVer = "E";
		var radioIF ="";
		var obRadio= Ext.getCmp('radioGp');
		if(obRadio.getValue()!=null){
			radioIF = Ext.getCmp('radioGp').getValue().getRawValue();
		}
		var registro = gridPrincipal.getStore().getAt(rowIndex);
		var sEncabezadoPago = registro.get('IC_ENCABEZADO_PAGO');
		radioButton();
		radio = radioIF;
		opcionG = "C3";
		encabezado= sEncabezadoPago;
	
		consultaDataDetalleEncabezado.load({
			params: {
				informacion: 'consultaProcesosDetalle',
				opcion: "C3",
				cs_tipo:radioIF,
				tipoVer:tipoVer, 
				sEncabezadoPago: sEncabezadoPago
			}
		});
		Ext.getCmp('winDetalles').show();
		
	}
	var mostrarVentanaDetalles = function(grid, rowIndex, colIndex, item, event) 	{
		inicializa();
		tipoVer ="T";
		var radioIF ="";
		var obRadio= Ext.getCmp('radioGp');
		if(obRadio.getValue()!=null){
		 radioIF = Ext.getCmp('radioGp').getValue().getRawValue();
		}
		var registro = gridPrincipal.getStore().getAt(rowIndex);
		var sEncabezadoPago = registro.get('IC_ENCABEZADO_PAGO');
		radioButton();
		radio = radioIF;
		opcionG = "C2";
		encabezado= sEncabezadoPago;
		consultaDataDetalle.load({
			params: {
				informacion: 'consultaProcesosDetalle',
				opcion: "C2",
				cs_tipo:radioIF,
				tipoVer:tipoVer, 
				sEncabezadoPago: sEncabezadoPago
			}
		});
		Ext.getCmp('winDetalles').show();
	}
	var procesarConsultaDataDetalle = function(store, arrRegistros, opts) 	{
		var grid = Ext.getCmp('gridDetalles');
		var el = grid.getGridEl();	
		if (arrRegistros != null) {	
			if(!grid.isVisible()){
				Ext.getCmp('gridDetallesEncabezado').hide();
				grid.show();
			}   
			var jsonData = store.reader.jsonData;
			if((store.getTotalCount() <=0)) {
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnImprimir').disable();
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			} else {
				var cm = grid.getColumnModel();   
				var jsonData = store.reader.jsonData;
			if(radio=="NB"){
				if(opcionG!="C3"){
					grid.getColumnModel().setHidden(cm.findColumnIndex('ETIQUETA'), jsonData.ETIQUETA);
					grid.getColumnModel().setColumnHeader(1, 'Subaplicaci�n');
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_IF'), jsonData.CLAVE_IF);
					grid.getColumnModel().setColumnHeader(2, 'Pr�stamo');
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_ESTATAL'), jsonData.CLAVE_ESTATAL);
					grid.getColumnModel().setColumnHeader(3, 'Clave SIRAC Cliente');
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_MONEDA'), jsonData.CLAVE_MONEDA);
					grid.getColumnModel().setColumnHeader(4, 'Capital');
					grid.getColumnModel().setHidden(cm.findColumnIndex('BANCO'), jsonData.BANCO);
					grid.getColumnModel().setColumnHeader(5, 'Interes');
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_VENCIMIENTO'), jsonData.DF_VENCIMIENTO);
					grid.getColumnModel().setColumnHeader(6, 'Moratorios');
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_PAGO'), jsonData.DF_PAGO);
					grid.getColumnModel().setColumnHeader(7, 'Subsidio');
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_DEPOSITO'), jsonData.DF_DEPOSITO);
					grid.getColumnModel().setColumnHeader(8, 'Comisi�n');
					grid.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE'), jsonData.IMPORTE);
					grid.getColumnModel().setColumnHeader(9, 'IVA');
					grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_BANCO'), jsonData.REFERENCIA_BANCO);
					grid.getColumnModel().setColumnHeader(10, 'Importe Pago');
					grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_INTERMEDIARIO'), jsonData.REFERENCIA_INTERMEDIARIO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_TOTALEXIGIBLE'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_SUBSIDIO'), true);
					grid.getColumnModel().setColumnHeader(13, 'Concepto Pago');
					grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CONCEPTO_PAGO'), jsonData.CG_CONCEPTO_PAGO);
					grid.getColumnModel().setColumnHeader(14, 'Origen Pago');
					grid.getColumnModel().setHidden(cm.findColumnIndex('CG_ORIGEN_PAGO'), jsonData.CG_ORIGEN_PAGO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_AMORTIZACION'),true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('IG_DIAS'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_INTERES'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_COMISION'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_IVA'), true);
					consultaTotalesData.load({
						params: {
							informacion: 'ConsultarTotales',
							opcion: "C2",
							sEncabezadoPago: encabezado   
						}
				});
				}
			}else{
				if(opcionG!="C3"){
					grid.getColumnModel().setHidden(cm.findColumnIndex('ETIQUETA'), jsonData.ETIQUETA);
					grid.getColumnModel().setColumnHeader(1, 'Sucursal');
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_IF'), jsonData.CLAVE_IF);
					grid.getColumnModel().setColumnHeader(2, 'Subaplicaci�n');
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_ESTATAL'), jsonData.CLAVE_ESTATAL);
					grid.getColumnModel().setColumnHeader(3, 'Pr�stamo');
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_MONEDA'), jsonData.CLAVE_MONEDA);
					grid.getColumnModel().setColumnHeader(4, 'Acreditado');
					grid.getColumnModel().setHidden(cm.findColumnIndex('BANCO'), jsonData.BANCO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_VENCIMIENTO'), true);
					grid.getColumnModel().setColumnHeader(6, 'Fecha Operaci�n');
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_PAGO'), jsonData.DF_PAGO);
					grid.getColumnModel().setColumnHeader(7, 'Fecha Vencimiento');
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_DEPOSITO'), jsonData.DF_DEPOSITO);
					grid.getColumnModel().setColumnHeader(8, 'Fecha Pago');
					grid.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE'), jsonData.IMPORTE);
					grid.getColumnModel().setColumnHeader(9, 'Saldo Insoluto');
					grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_BANCO'), jsonData.REFERENCIA_BANCO);
					grid.getColumnModel().setColumnHeader(10, 'Tasa');
					grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_INTERMEDIARIO'), jsonData.REFERENCIA_INTERMEDIARIO);
					grid.getColumnModel().setColumnHeader(11, 'Capital');
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_AMORTIZACION'), jsonData.FG_AMORTIZACION);
					grid.getColumnModel().setColumnHeader(12, 'D�as');
					grid.getColumnModel().setHidden(cm.findColumnIndex('IG_DIAS'), jsonData.IG_DIAS);
					grid.getColumnModel().setColumnHeader(13, 'Intereses');
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_INTERES'), jsonData.FG_INTERES);
					grid.getColumnModel().setColumnHeader(14, 'Comisi�n');
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_COMISION'), jsonData.FG_COMISION);
					grid.getColumnModel().setColumnHeader(15, 'IVA');
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_IVA'), jsonData.FG_IVA);
					grid.getColumnModel().setColumnHeader(16, 'Importe Pago');
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_TOTALEXIGIBLE'), jsonData.FG_TOTALEXIGIBLE);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_SUBSIDIO'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CONCEPTO_PAGO'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CG_ORIGEN_PAGO'), true);
					consultaTotalesData.load({
						params: {
							informacion: 'ConsultarTotales',
							opcion: "C2",
							sEncabezadoPago: encabezado
						}
					
					});
				}
			}
			Ext.getCmp('btnGenerarArchivo').enable();
			Ext.getCmp('btnImprimir').enable();
			grid.el.unmask();
			}
		}
	}
	var procesarConsultaDataDetalleEncabezado = function(store, arrRegistros, opts) 	{
		var grid = Ext.getCmp('gridDetallesEncabezado');
		var el = grid.getGridEl();	
			
		if (arrRegistros != null) {	
			if(!grid.isVisible()){
				Ext.getCmp('gridDetalles').hide();
				Ext.getCmp('gridTotales').hide();
				grid.show();
				
			}   
			var jsonData = store.reader.jsonData;
			if((store.getTotalCount() <= 0)) {
				Ext.getCmp('btnGenerarArchivo_b').disable();
				Ext.getCmp('btnImprimir_b').disable();
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			} else {
				var cm = grid.getColumnModel();
				var jsonData = store.reader.jsonData;
			if(radio=="NB"){
					
					grid.getColumnModel().setHidden(cm.findColumnIndex('ETIQUETA'), jsonData.ETIQUETA);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_IF'), jsonData.CLAVE_IF);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_ESTATAL'), jsonData.CLAVE_ESTATAL);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_MONEDA'), jsonData.CLAVE_MONEDA);
					grid.getColumnModel().setHidden(cm.findColumnIndex('BANCO'), jsonData.BANCO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_VENCIMIENTO'), jsonData.DF_VENCIMIENTO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_PAGO'), jsonData.DF_PAGO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_DEPOSITO'), jsonData.DF_DEPOSITO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE'), jsonData.IMPORTE);
					grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_BANCO'), jsonData.REFERENCIA_BANCO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_INTERMEDIARIO'), jsonData.REFERENCIA_INTERMEDIARIO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_AMORTIZACION'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('IG_DIAS'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_INTERES'),true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_COMISION'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_IVA'),true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_TOTALEXIGIBLE'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_SUBSIDIO'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CONCEPTO_PAGO'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CG_ORIGEN_PAGO'), true);
					
			}else{
					grid.getColumnModel().setHidden(cm.findColumnIndex('ETIQUETA'), jsonData.ETIQUETA);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_IF'), jsonData.CLAVE_IF);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_ESTATAL'), jsonData.CLAVE_ESTATAL);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CLAVE_MONEDA'), jsonData.CLAVE_MONEDA);
					grid.getColumnModel().setHidden(cm.findColumnIndex('BANCO'), jsonData.BANCO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_VENCIMIENTO'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_PAGO'), jsonData.DF_PAGO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('DF_DEPOSITO'), jsonData.DF_DEPOSITO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE'), jsonData.IMPORTE);
					grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_BANCO'), jsonData.REFERENCIA_BANCO);
					grid.getColumnModel().setHidden(cm.findColumnIndex('REFERENCIA_INTERMEDIARIO'), jsonData.REFERENCIA_INTERMEDIARIO);
					
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_AMORTIZACION'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('IG_DIAS'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_INTERES'),true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_COMISION'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_IVA'),true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_TOTALEXIGIBLE'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('FG_SUBSIDIO'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CG_CONCEPTO_PAGO'), true);
					grid.getColumnModel().setHidden(cm.findColumnIndex('CG_ORIGEN_PAGO'), true);
					
			}
			Ext.getCmp('btnGenerarArchivo_b').enable();
			Ext.getCmp('btnImprimir_b').enable();
			grid.el.unmask();
			}
		}
	}
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var grid = Ext.getCmp('gridTotales');
		if (arrRegistros != null) {	
			if(!grid.isVisible()){
				grid.show();
			}
			if(store.getTotalCount() <= 0) {
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			} else {
				grid.el.unmask();
			}
		}
	}


//---------------------------------STORES------------------------------------
	
	//STORE PARA LLENAR EL COMBO DE TIPO DE PROCESO
	var storeComboIF = new Ext.data.JsonStore({
		id: 'storeComboIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma5Ext01.data.jsp',
		baseParams: {
			informacion: 'catalogoIntermediario'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			//load: procesarCatIntermediarioData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	//STORE PARA LLENAR EL GRID DE DETALLE
	var consultaDataDetalle = new Ext.data.JsonStore({
		root: 'registros',
		url: '13forma5Ext01.data.jsp',
		baseParams: {
			informacion: 'consultaProcesosDetalle'
		},
		fields: [
			{name: 'ETIQUETA'},
			{name: 'CLAVE_IF'},
			{name: 'CLAVE_ESTATAL'},
			{name: 'CLAVE_MONEDA'},
			{name: 'BANCO'},
			{name: 'DF_VENCIMIENTO'},
			{name: 'DF_PAGO'},
			{name: 'DF_DEPOSITO'},
			{name: 'IMPORTE'},
			{name: 'REFERENCIA_BANCO'},
			{name: 'REFERENCIA_INTERMEDIARIO'},
			{name: 'FG_AMORTIZACION'},
			{name: 'IG_DIAS'},
			{name: 'FG_INTERES'},
			{name: 'FG_COMISION'},
			{name: 'FG_IVA'},
			{name: 'FG_TOTALEXIGIBLE'},
			{name: 'FG_SUBSIDIO'},
			{name: 'CG_CONCEPTO_PAGO'},
			{name: 'CG_ORIGEN_PAGO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataDetalle,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaDataDetalle(null, null, null);
				}
			}
		}
	});
	var consultaDataDetalleEncabezado = new Ext.data.JsonStore({
		root: 'registros',
		url: '13forma5Ext01.data.jsp',
		baseParams: {
			informacion: 'consultaProcesosDetalle'
		},
		fields: [
			{name: 'ETIQUETA'},
			{name: 'CLAVE_IF'},
			{name: 'CLAVE_ESTATAL'},
			{name: 'CLAVE_MONEDA'},
			{name: 'BANCO'},
			{name: 'DF_VENCIMIENTO'},
			{name: 'DF_PAGO'},
			{name: 'DF_DEPOSITO'},
			{name: 'IMPORTE'},
			{name: 'REFERENCIA_BANCO'},
			{name: 'REFERENCIA_INTERMEDIARIO'},
			{name: 'FG_AMORTIZACION'},
			{name: 'IG_DIAS'},
			{name: 'FG_INTERES'},
			{name: 'FG_COMISION'},
			{name: 'FG_IVA'},
			{name: 'FG_TOTALEXIGIBLE'},
			{name: 'FG_SUBSIDIO'},
			{name: 'CG_CONCEPTO_PAGO'},
			{name: 'CG_ORIGEN_PAGO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataDetalleEncabezado,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaDataDetalleEncabezado(null, null, null);
				}
			}
		}
	});

	//STORE PARA LLENAR EL GRID DE PROCESOS (gridPrincipal)
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '13forma5Ext01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'IC_ENCABEZADO_PAGO'},
			{name: 'IC_FINANCIERA'},
			{name: 'IC_MONEDA'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'FECHA_PROB'},
			{name: 'FECHA_PAGO'},
			{name: 'FECHA_REG'},
			{name: 'CG_NOMBRE_ARCHIVO'},
			{name: 'ELIMINAR'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaProcesosData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaProcesosData(null, null, null);
				}
			}
		}
	});
	
	var consultaTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13forma5Ext01.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [	
		   {	name: 'REGISTROS' },
			{	name: 'TOTAL_PAGOS' }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);					
				}
			}
		}		
	});
//------------------------------COMPONENTES----------------------------------
	//ELEMENTOS QUE SE PRESENTAN EN EL FORM
	var elementosForma = [
		{
			xtype:'radiogroup',
			id:	'radioGp',
			columns: 2,
			allowBlank	: true,
			fieldLabel  : '',
			hidden: false,
			items: [
				{ 
				  xtype      : 'radio',
				  fieldLabel : "",
				  boxLabel: 'IF', 
				  id:'cs_tipo_B', 
				  name: 'cs_tipo', 
				  inputValue: 'B', 
				  handler		:function(rb,value){
						if(value){
							Ext.getCmp('cmbIntermediarioFinanciero').setValue('');
							Ext.getCmp('cmbIntermediarioFinanciero').getStore().removeAll(true);
							Ext.getCmp('gridPrincipal').hide();
							storeComboIF.load({
								params : Ext.apply({
									cs_tipo : rb.getRawValue()
								})
							});	
					
						}
					}
				},
				{ 
				  xtype : 'radio',
				  boxLabel: 'IFNB', 
				  id:'cs_tipo_NB', 
				  name: 'cs_tipo', 
				  inputValue: 'NB', 
					handler		:function(rb,value){
						if(value){
							Ext.getCmp('cmbIntermediarioFinanciero').setValue('');
							Ext.getCmp('cmbIntermediarioFinanciero').getStore().removeAll(true);
							Ext.getCmp('gridPrincipal').hide();
							storeComboIF.load({
								params : Ext.apply({
									cs_tipo : rb.getRawValue()
								})
							});	
					
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Intermediario',
			combineErrors: false,
			msgTarget: 'side',
			items: [				
				{
					xtype: 'numberfield',
					name: 'numIntermediario',
					id: 'tfNumIntermediario',
					allowBlank: true,
					maxLength: 10,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: 
							{
								blur: {
									fn: function() {
										Ext.getCmp('cmbIntermediarioFinanciero').setValue('');										
									}
								}
							}
				}
			]
		},
		{
			xtype: 'combo',
			name: 'intermediarioFinanciero',
			id: 'cmbIntermediarioFinanciero',
			fieldLabel:'Intermediario Financiero',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'intermediarioFinanciero',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,	
			allowBlank: true,	
			minChars : 1,		
			store: storeComboIF,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
									'</div></tpl></tpl>',
			setNewEmptyText: function(emptyTextMsg){
				this.emptyText = emptyTextMsg;
				this.setValue('');
				this.applyEmptyText();
				this.clearInvalid();
			},
			listeners: {
				select: function(combo) {
					Ext.getCmp('tfNumIntermediario').setValue('');
				}
			}
			
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '95%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Fecha de Vencimiento',
					name: 'fechaVencimientoIni',
					id: 'dfFechaVencimientoIni',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'dfFechaVencimientoFin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'fechaVencimientoFin',
					id: 'dfFechaVencimientoFin',
					allowBlank: true,
					vtype: 'rangofecha',
					campoInicioFecha: 'dfFechaVencimientoIni',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '100%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Fecha Probable de Pago',
					name: 'fechaProbaPagoIni',
					id: 'dfFechaProbaPagoIni',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'dfFechaProbaPagoFin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'fechaProbaPagoFin',
					id: 'dfFechaProbaPagoFin',
					allowBlank: true,
					vtype: 'rangofecha',
					campoInicioFecha: 'dfFechaProbaPagoIni',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '100%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Fecha de Pago',
					name: 'fechaPagoIni',
					id: 'dfFechaPagoIni',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'dfFechaPagoFin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'fechaPagoFin',
					id: 'dfFechaPagoFin',
					allowBlank: true,
					vtype: 'rangofecha',
					campoInicioFecha: 'dfFechaPagoIni',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '100%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Fecha de Registro',
					name: 'fechaRegistroIni',
					id: 'dfFechaRegistroIni',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'dfFechaRegistroFin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'fechaRegistroFin',
					id: 'dfFechaRegistroFin',
					allowBlank: true,
					vtype: 'rangofecha',
					campoInicioFecha: 'dfFechaRegistroIni',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 440,
		style: 'margin:0 auto;',
		title: 'Criterios de B�squeda',
		collapsible: false,
		titleCollapse:	false,
		defaultType: 'textfield',
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaults: {	
			msgTarget: 'side',
			anchor:	'-20'	
		},
		items: elementosForma,
		buttons: [		
			{    
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,	
				handler: function(boton, evento){
					realizarConsulta();					
				}
			},
			{				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('forma').getForm().reset();
					Ext.getCmp('gridPrincipal').hide();
					Ext.getCmp('winDetalles').hide();
				}
			}
		]
	});

	//GRID EN EL QUE SE CARGAN LOS PROCESOS ENCONTRADOS EN LA BUSQUEDA
	var gridPrincipal = new Ext.grid.GridPanel({
		id: 'gridPrincipal',
		store: consultaData,
		height: 350,
		width:520,
		loadMask: true,
		frame: true,
		hidden: true,
		margins: '20 0 0 0',
		align: 'center',
		style: 'margin:0 auto',
		columns:[
			{
				header: 'Nombre del Archivo ',
				tooltip: 'Nombre del Archivo',
		    	width: 150,
				dataIndex: 'CG_NOMBRE_ARCHIVO',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Fecha de Registro',
				tooltip: 'Fecha de Registro',
				dataIndex: 'FECHA_REG',
				sortable: true,
				width: 100,
				resizable: true,
				align: 'center'
			},
			{
				xtype: 'actioncolumn',
				header: 'Ver Encabezado',
				tooltip: 'Ver Encabezado',
				width: 100,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Encabezado';
							return 'iconoLupa';										
						},
						handler: mostrarVentanaDetallesEncabezado
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'Ver Todo',
				tooltip: 'Ver Todo',
				width: 100,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Todo';
							return 'iconoLupa';										
						},
						handler: mostrarVentanaDetalles
					}
				]				
			},
			{
				xtype:'checkcolumn',
				header: 'Eliminar',
				tooltip:		'Eliminar',
				dataIndex : 'ELIMINAR',
				width : 50,
				sortable:true, 
				resizable:true,
				width:50
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Eliminar',
					id: 'btnEliminar',
					iconCls: 'icoEliminar',
					handler: function(boton,evento) {
						
						var  grid = Ext.getCmp('gridPrincipal');
						var store = grid.getStore();
						var icEncabezadoPago = [];
						var coma= 0;
						store.each(function(record) {
							var numDocumentos = store.indexOf(record);
							if( record.data['ELIMINAR']==true ){	
								icEncabezadoPago[coma]=record.data['IC_ENCABEZADO_PAGO'];
								coma++;	
							}
						});
						if(icEncabezadoPago =='') {
							Ext.MessageBox.alert('Mensaje','Debes seleccionar al menos un elemento para eliminar.');
							
							return false;	
						}else  {	
							boton.disable();
							boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
										url: '13forma5Ext01.data.jsp',
										params:Ext.apply (fp.getForm().getValues(),{
											informacion: 'Eliminar',
											icEncabezadoPago:icEncabezadoPago,
											numRegistros:coma
										})
									,callback: procesaEliminar
								});
						}	
					}
				}
			]
		}
	}); 
	var gruposHeader = new Ext.ux.grid.ColumnHeaderGroup({
      rows: [
         [
            {header: 'E', colspan: 1, align: 'center'},
            {header: 'Clave del I.F.', colspan: 1, align: 'center'},
				{header: 'Clave<br>direcci�n estatal', colspan: 1, align: 'center'},
				{header: 'Clave moneda', colspan: 1, align: 'center'},
				{header: 'Banco de servicio', colspan: 1, align: 'center'},
				{header: 'Fecha<br>de vencimiento', colspan: 1, align: 'center'},
				{header: 'Fecha<br>probable de pago', colspan: 1, align: 'center'},
				{header: 'Fecha<br>de dep�sito', colspan: 1, align: 'center'},
				{header: 'Importe<br>de dep�sito', colspan: 1, align: 'center'},
				{header: 'Referencia Banco', colspan: 1, align: 'center'},
				{header: 'Referencia<br>Intermediario', colspan: 1, align: 'center'},
				{header: '&nbsp;', colspan: 1, align: 'center'},
				{header: '&nbsp;', colspan: 1, align: 'center'},
				{header: '&nbsp;', colspan: 1, align: 'center'},
				{header: '&nbsp;', colspan: 1, align: 'center'},
				{header: '&nbsp;', colspan: 1, align: 'center'},
				{header: '&nbsp;', colspan: 1, align: 'center'},
				{header: '&nbsp;', colspan: 1, align: 'center'},
				{header: '&nbsp;', colspan: 1, align: 'center'},
				{header: '&nbsp;', colspan: 1, align: 'center'}
         ]
      ]
	});

	//GRID EN EL QUE SE CARGAN LOS DETALLES DE PROCESOS
	var gridDetalles = new Ext.grid.GridPanel({
		id: 'gridDetalles',
		store: consultaDataDetalle,
		style: 'margin:0 auto',
		hidden: true,
		 plugins: gruposHeader,
		margins: '20 0 0 0',
		clicksToEdit:1,
		stripeRows: true,
		loadMask: true,
		height: 350,
		width: 750,
		frame: 		true,
		columns:[
			{
				header: 'D',
				tooltip: 'D',
				dataIndex: 'ETIQUETA',
				sortable: true,
				hidden:true,
				width: 100,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Subaplicaci�n',
				tooltip: 'Supaplicaci�n',
				dataIndex: 'CLAVE_IF',
				sortable: true,
				width: 100,
				hidden:true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Pr�stamo',
				tooltip: 'Pr�stamo',
				dataIndex: 'CLAVE_ESTATAL',
				sortable: true,
				hidden:true,
				width: 100,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Clave SIRAC Cliente',
				tooltip: 'Clave SIRAC Cliente',
				dataIndex: 'CLAVE_MONEDA',
				hidden:true,
				sortable: true,
				resizable: true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Capital',
				tooltip: 'Capital',
				dataIndex: 'BANCO',
				sortable: true,
				hidden:true,
				width: 100,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Interes',
				tooltip: 'Interes',
				dataIndex: 'DF_VENCIMIENTO',
				sortable: true,
				hidden:true,
				resizable: true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Moratorios',
				tooltip: 'Moratorios',
				dataIndex: 'DF_PAGO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Subsidio',
				tooltip: 'Subsidio',
				dataIndex: 'DF_DEPOSITO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Comisi�n',
				tooltip: 'Comisi�n',
				dataIndex: 'IMPORTE',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: 'IVA',
				tooltip: 'IVA',
				dataIndex: 'REFERENCIA_BANCO',
				sortable: true,
				hidden:true,
				resizable: true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Importe Pago',
				tooltip: 'Importe Pago',
				dataIndex: 'REFERENCIA_INTERMEDIARIO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
				
			}
			
			
			
			,
			{
				header: '',
				dataIndex: 'FG_AMORTIZACION',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
				
			},
			{
				header: '',
				dataIndex: 'IG_DIAS',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			},
			{
				header: '',
				dataIndex: 'FG_INTERES',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: '',
				dataIndex: 'FG_COMISION',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: '',
				dataIndex: 'FG_IVA',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: '',
				dataIndex: 'FG_TOTALEXIGIBLE',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: '',
				dataIndex: 'FG_SUBSIDIO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: 'Concepto Pago',
				dataIndex: 'CG_CONCEPTO_PAGO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Origen Pago',
				dataIndex: 'CG_ORIGEN_PAGO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url:'13forma5Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{					
								informacion:'GenerarArchivoCSV',
								tipo:'CSV',
								cs_tipo:Ext.getCmp('radioGp').getValue().getRawValue(),
								sEncabezadoPago:encabezado,
								opcion : opcionG
							}),
							callback: mostrarArchivoCSV
						});		
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',
					id: 'btnImprimir',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url:'13forma5Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{					
								informacion:'ArchivoPDF',
								tipo:'PDF',
								cs_tipo:Ext.getCmp('radioGp').getValue().getRawValue(),
								sEncabezadoPago:encabezado,
								opcion : opcionG
							}),
							callback: mostrarArchivoPDF
						});		
					}
				}
			]
		}
	});
	var gridDetallesEncabezado = new Ext.grid.GridPanel({
		id: 'gridDetallesEncabezado',
		store: consultaDataDetalleEncabezado,
		style: 'margin:0 auto',
		hidden: true,
		margins: '20 0 0 0',
		clicksToEdit:1,
		stripeRows: true,
		loadMask: true,
		height: 350,
		width: 750,
		frame: 		true,
		columns:[
			{
				header: 'E',
				tooltip: 'E',
				dataIndex: 'ETIQUETA',
				sortable: true,
				hidden:true,
				width: 100,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Clave del I.F.',
				tooltip: 'Clave del I.F.',
				dataIndex: 'CLAVE_IF',
				sortable: true,
				width: 100,
				hidden:true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Clave<br>direcci�n estatal',
				tooltip: 'Clave direcci�n estatal',
				dataIndex: 'CLAVE_ESTATAL',
				sortable: true,
				hidden:true,
				width: 100,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Clave moneda',
				tooltip: 'Clave moneda',
				dataIndex: 'CLAVE_MONEDA',
				hidden:true,
				sortable: true,
				resizable: true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Banco de servicio',
				tooltip: 'Banco de servicio',
				dataIndex: 'BANCO',
				sortable: true,
				hidden:true,
				width: 100,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Fecha<br>de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex: 'DF_VENCIMIENTO',
				sortable: true,
				hidden:true,
				resizable: true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Fecha<br>probable de pago',
				tooltip: 'Fecha probable de pago',
				dataIndex: 'DF_PAGO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Fecha<br>de dep�sito',
				tooltip: 'Fecha de dep�sito',
				dataIndex: 'DF_DEPOSITO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			},
			{
				header: '<center>Importe<br>de dep�sito</center>',
				tooltip: 'Importe de dep�sito',
				dataIndex: 'IMPORTE',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: 'Referencia Banco',
				tooltip: 'Referencia Banco',
				dataIndex: 'REFERENCIA_BANCO',
				sortable: true,
				hidden:true,
				resizable: true,
				width: 100,
				align: 'center'
			},
			{
				header: '<center>Referencia<br>Intermadiario</center>',
				tooltip: 'Referencia Intermadiario',
				dataIndex: 'REFERENCIA_INTERMEDIARIO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
				
			}
			
			
			
			,
			{
				header: '',
				dataIndex: 'FG_AMORTIZACION',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
				
			},
			{
				header: '',
				dataIndex: 'IG_DIAS',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			},
			{
				header: '',
				dataIndex: 'FG_INTERES',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: '',
				dataIndex: 'FG_COMISION',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: '',
				dataIndex: 'FG_IVA',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: '',
				dataIndex: 'FG_TOTALEXIGIBLE',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: '',
				dataIndex: 'FG_SUBSIDIO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'right'
			},
			{
				header: 'Concepto Pago',
				dataIndex: 'CG_CONCEPTO_PAGO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			},
			{
				header: 'Origen Pago',
				dataIndex: 'CG_ORIGEN_PAGO',
				sortable: true,
				resizable: true,
				hidden:true,
				width: 100,
				align: 'center'
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo_b',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url:'13forma5Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{					
								informacion:'GenerarArchivoCSV',
								tipo:'CSV',
								cs_tipo:Ext.getCmp('radioGp').getValue().getRawValue(),
								sEncabezadoPago:encabezado,
								opcion : opcionG
							}),
							callback: mostrarArchivoCSV
						});		
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',
					id: 'btnImprimir_b',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url:'13forma5Ext01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{					
								informacion:'ArchivoPDF',
								tipo:'PDF',
								cs_tipo:Ext.getCmp('radioGp').getValue().getRawValue(),
								sEncabezadoPago:encabezado,
								opcion : opcionG
							}),
							callback: mostrarArchivoPDF
						});		
					}
				}
			]
		}
	});
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'TOTAL',	
		align: 'left',
		hidden: true,
		columns: [	
			{
				header: ' ',
				tooltip: '',
				dataIndex: 'REGISTROS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},		
			{
				header: 'Total Pagos',
				tooltip: 'Total Pagos',
				dataIndex: 'TOTAL_PAGOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$ 0,0.00')//
				
			}
		],	
		stripeRows: true,
		loadMask: true,
		height: 90,
		width: 320,
		frame: true	
	});
	
	var ventanaDetalles = new Ext.Window({
		id: 'winDetalles',
		modal: true,
		width: 760,
		closeAction: 'hide',
		resizable: true,
		constrain: false,
		closable:true,
		title: 'DETALLE',
		align:'center',
		items:[ 
			gridDetallesEncabezado,
			gridDetalles,
			gridTotales
		]
	});
//-------------------------COMPONENTE PRINCIPAL------------------------------
	//PANEL PRINCIPAL
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		heigth: 'auto',
		items: [
		NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(10),
			gridPrincipal
		]
	});
});