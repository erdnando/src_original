Ext.onReady(function() {

//_--------------------------------- HANDLERS -------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');

			var btnTotales = Ext.getCmp('btnTotales');
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnBajarPDF.hide();
				if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		 btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//-------------------------------- STORES -----------------------------------

 var procesarBancoFondeo= function(store, records, oprion){
   if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
    var ic_banco_fondeo = Ext.getCmp('ic_banco_fondeo2');
	 if(ic_banco_fondeo.getValue()==''){
		ic_banco_fondeo.setValue(records[0].data['clave']);
	 }
   }
  }
  
var catalogoBancoFondeo = new Ext.data.JsonStore({
		id: 'catalogoBancoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta10.data.jsp',
		baseParams: {
			informacion: 'CatalogoBanco'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			load: procesarBancoFondeo,
			exception: NE.util.mostrarDataProxyError
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta10.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [		
			{name: 'EPO'},
			{name: 'PYME'},
			{name: 'CIF'},			
			{name: 'DOCUMENTO'},
			{name: 'FECHADOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHAVEN', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'MONEDA'},
			{name: 'TIPODESCUENTO'}	,
			{name: 'MONTO',  type: 'float'}	,
			{name: 'PORDESC'},	
			{name: 'MONTODES',type: 'float' },	
			{name: 'DESAUTO'}	,
			{name: 'DIASMINIMOS'}	,
			{name: 'LIMITEEPO',type: 'float'}	,
			{name: 'CAUSARECHAZO'},
			{name: 'FECHADESC'}		
			],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}		
	});

	
	var resumenTotalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13consulta10.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'			
		},
		fields: [
			{name: 'MONEDA', mapping: 'col1'},
			{name: 'NUMERO_DOCUMENTOS', type: 'float', mapping: 'col2'},
			{name: 'TOTAL_IMPORTE', type: 'float', mapping: 'col3'}			
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_banco_fondeo',
			id: 'ic_banco_fondeo2',
			fieldLabel: 'Banco de Fondeo',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_banco_fondeo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			hidden:true,
			minChars : 1,
			store : catalogoBancoFondeo,
			tpl : NE.util.templateMensajeCargaCombo
		},	
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_operacion_inicio',
					id: 'df_operacion_inicio',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_operacion_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_operacion_fin',
					id: 'df_operacion_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_operacion_inicio',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}		
	];
	
	var gridTotales ={
		xtype: 'grid',
		store: resumenTotalesData,
		id: 'gridTotales',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},		
			{
				header: 'N�MERO DE DOCUMENTOS ',
				dataIndex: 'NUMERO_DOCUMENTOS',
				width: 150,
				align: 'center'				
			},
			{
				header: 'TOTAL MONEDA',
				dataIndex: 'TOTAL_IMPORTE',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 200,
		title: '',
		frame: false
	};
	
    // create the Grid
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		margins: '20 0 0 0',
		hidden: true,
		columns: [
		{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'EPO',
				sortable: true,
				width: 250,
				resizable: true,
				align: 'left',
				hidden: false
			},
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'PYME',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'left'				
			},			
			{
				header: 'IF',
				tooltip: 'IF',
				dataIndex: 'CIF',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'left'				
			},			
			{
				header: 'Num. Documento',
				tooltip: 'Num. Documento',
				dataIndex: 'DOCUMENTO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},						
			{	header: 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHADOCTO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},					
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHAVEN',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},
			{
				header: 'Tipo de Descuento',
				tooltip: 'Tipo de Descuento',
				dataIndex: 'TIPODESCUENTO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'			
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',				
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: '% de Descuento',
				tooltip: '% de Descuento',
				dataIndex: 'PORDESC',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTODES',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo de Descto. Autom�tico',
				tooltip: 'Tipo de Descto. Autom�tico',
				dataIndex: 'DESAUTO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},
			{
				header: 'D�as M�nimos  cadena',
				tooltip: 'D�as M�nimos  cadena',
				dataIndex: 'DIASMINIMOS',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},		
			
			{
				header: 'L�mite IF_EPO',
				tooltip: 'L�mite IF_EPO',
				dataIndex: 'LIMITEEPO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Causa del Rechazo',
				tooltip: 'Causa del Rechazo',
				dataIndex: 'CAUSARECHAZO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},
			{
				header: 'Fecha y hora del Descto',
				tooltip: 'Fecha y hora del Descto.',
				dataIndex: 'FECHADESC',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'			
			}			
		],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 400,
		width: 885,
		title: '',
		style: 'margin:0 auto;',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: 'No hay registros.',
			items: [
			'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales');
						if (ventana) {
							ventana.show();
						} else {							
							resumenTotalesData.load();
							new Ext.Window({
								layout: 'fit',
								width: 600,
								height: 105,
								id: 'winTotales',
								closeAction: 'hide',
								items: [
									gridTotales
								],								
								title: 'Totales'
							}).show().alignTo(grid.el);
						}
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime todos los registros en formato CSV.',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta10exta.jsp',
							params: fp.getForm().getValues(),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime todos los registros en formato PDF.',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta10.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GENERA_PDF'
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
/*
	Fodea 011-2015
	La opci�n de generar el PDF por paginaci�n queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el m�todo por si se requiere nuevamente 
	Fecha: 30/06/2015
	BY: Agust�n Bautista Ruiz
*/
/*
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '13consulta10exti.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							start: cmpBarraPaginacion.cursor,
							limit: cmpBarraPaginacion.pageSize	
							}),						
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
*/
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
	});


//-------------------------------- PRINCIPAL -----------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Rechazo en Descuento Autom�tico.',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',		
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					
					
					var ventana = Ext.getCmp('winTotales');
					if (ventana) {
						ventana.destroy();
					}		
									
					var ic_banco_fondeo = Ext.getCmp("ic_banco_fondeo2");
					if (Ext.isEmpty(ic_banco_fondeo.getValue()) ) {
						ic_banco_fondeo.markInvalid('Por favor, especifique el Banco de Fondeo');
						return;
					}
					
					var df_operacion_inicio = Ext.getCmp("df_operacion_inicio");
					var df_operacion_fin = Ext.getCmp("df_operacion_fin");
					if (!Ext.isEmpty(df_operacion_inicio.getValue()) && Ext.isEmpty(df_operacion_fin.getValue()) ) {
						df_operacion_fin.markInvalid('Por favor, especifique la fecha de cambio final');
						return;
					}
						
					
					var df_operacion_inicio_ = Ext.util.Format.date(df_operacion_inicio.getValue(),'d/m/Y');
					var df_operacion_fin_ = Ext.util.Format.date(df_operacion_fin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(df_operacion_inicio.getValue())){
						if(!isdate(df_operacion_inicio_)) { 
							df_operacion_inicio.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_operacion_inicio.focus();
							return;
						}
					}
					
					if( !Ext.isEmpty(df_operacion_fin.getValue())){
						if(!isdate(df_operacion_fin_)) { 
							df_operacion_fin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_operacion_fin.focus();
							return;
						}
					}
												
					fp.el.mask('Enviando...', 'x-mask-loading');	
						consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar', //Generar datos para la consulta
							start: 0,
							limit: 15
						})
					});
					Ext.getCmp('btnGenerarArchivo').disable();
					Ext.getCmp('btnTotales').disable();
					Ext.getCmp('btnBajarArchivo').hide();
					Ext.getCmp('btnBajarPDF').hide();
				
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fp.getForm().reset();Ext.getCmp("ic_banco_fondeo2").setValue(1);
					grid.hide();	
				}
				
			}
		]
	});

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',		
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	}); 

//-------------------------------- ----------------- -----------------------------------
catalogoBancoFondeo.load();

});