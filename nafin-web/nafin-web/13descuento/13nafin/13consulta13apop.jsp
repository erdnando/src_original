<%@ page import="
			java.util.*, 
			netropology.utilerias.*,
			com.netro.descuento.*, 
			com.netro.exception.*" %>
<%@ include file="../13secsession.jspf" %>
<%
try {
	String sNoEPO 							= (request.getParameter("sNoEPO")==null)?"":request.getParameter("sNoEPO");
	String sNombreEPO 					= (request.getParameter("sNombreEPO")==null)?"":request.getParameter("sNombreEPO");
	String sParamOperNotasCred 		= (request.getParameter("sParamOperNotasCred ")==null)?"":request.getParameter("sParamOperNotasCred "); //FODA 021-2005 SMJ/
	String sParamDsctoObligatorio 	= (request.getParameter("sParamDsctoObligatorio")==null)?"":request.getParameter("sParamDsctoObligatorio"); //FODA 021-2005 SMJ/
	String sParam_fn_monto_minimo 	= (request.getParameter("sParam_fn_monto_minimo")==null)?"":request.getParameter("sParam_fn_monto_minimo");
	String sParam_dscto_dinam 			= (request.getParameter("sParam_dscto_dinam ")==null)?"":request.getParameter("sParam_dscto_dinam ");
	String sParam_limite_pyme 			= (request.getParameter("sParam_limite_pyme  ")==null)?"":request.getParameter("sParam_limite_pyme");
	String sParam_firma_manc 			= (request.getParameter("sParam_firma_manc")==null)?"":request.getParameter("sParam_firma_manc");
	String sParam_criterio_hash 		= (request.getParameter("sParam_criterio_hash  ")==null)?"":request.getParameter("sParam_criterio_hash");
	String sParam_fecha_venc_pyme 	= (request.getParameter("sParam_criterio_hash  ")==null)?"":request.getParameter("sParam_criterio_hash");
	String sParam_pub_docto_venc 		= (request.getParameter("sParam_pub_docto_venc  ")==null)?"":request.getParameter("sParam_pub_docto_venc");	
	String sdiasMaximosAd 	 			= (request.getParameter("sdiasMaximosAd")==null)?"":request.getParameter("sdiasMaximosAd");	
	String sPlazoMaxFechasVePyDc 		= (request.getParameter("sPlazoMaxFechasVePyDc")==null)?"":request.getParameter("sPlazoMaxFechasVePyDc");	
	String sPlazoMaxFechasVenPyDm 	= (request.getParameter("sPlazoMaxFechasVenPyDm")==null)?"":request.getParameter("sPlazoMaxFechasVenPyDm");	
	String sParamPreafiliacion 		= (request.getParameter("sParamPreafiliacion")==null)?"":request.getParameter("sParamPreafiliacion");
	String sPermitirDescAutomatico	= (request.getParameter("sPermitirDescAutomatico")==null)?"":request.getParameter("sPermitirDescAutomatico");
	String sPubEPOPEF						= (request.getParameter("sPubEPOPEF")==null)?"":request.getParameter("sPubEPOPEF");
	
	String sPubEPOPEFSIAFFval			= (request.getParameter("sPubEPOPEFSIAFFval")==null)?"":request.getParameter("sPubEPOPEFSIAFFval");
	String sPubEPOPEFRecepcion			= (request.getParameter("sPubEPOPEFRecepcion")==null)?"":request.getParameter("sPubEPOPEFRecepcion");
	String sPubEPOPEFEntrega			= (request.getParameter("sPubEPOPEFEntrega")==null)?"":request.getParameter("sPubEPOPEFEntrega");
	String sPubEPOPEFTipoCompra		= (request.getParameter("sPubEPOPEFTipoCompra")==null)?"":request.getParameter("sPubEPOPEFTipoCompra");
	String sPubEPOPEFPresupuestal		= (request.getParameter("sPubEPOPEFPresupuestal")==null)?"":request.getParameter("sPubEPOPEFPresupuestal");
	String sPubEPOPEFPeriodo			= (request.getParameter("sPubEPOPEFPeriodo")==null)?"":request.getParameter("sPubEPOPEFPeriodo");
	String sOperaFactConMandato		= (request.getParameter("sOperaFactConMandato")==null)?"":request.getParameter("sOperaFactConMandato");
	String sOperaFactVencimiento		= (request.getParameter("sOperaFactVencimiento")==null)?"":request.getParameter("sOperaFactVencimiento");	
	  
	String sOperaCesionDerechos = ""; //Fodea 018-2010
	String sOperaFacVencido = ""; //Fodea 018-2010
	String sOperaDesAutoFacVencido = ""; //Fodea 018-2010
	String sOperposicionje24hrs = "";//Fodea 018-2010
  
	String sOperaDsctoAutUltimoDia	= "";//FODEA 024-2010 
	String sOperaFactorajeDist			= "";//FODEA 032-2010 
	String sAutorizacionCuentasBanc	= "";//FODEA 032-2010 
	
	String sParamOperaTasasEspeciales	= "";//Fodea 036-2010
   String sParamTipoTasa					= "";//Fodea 036-2010
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
%>
<html>
<head>
<title>Nafinet</title>
<link rel="stylesheet" href="/nafin/14seguridad/Seguridad/css/nafin.css"> 
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div align="center">
<%
	StringBuffer sbArchivo = new StringBuffer();
	sbArchivo.append("Nombre de la EPO:,"+sNombreEPO+"\r\n\r\n");
	sbArchivo.append("Parametros por EPO\r\n");
	
	//ArrayList alParamEPO = null;   
	Hashtable alParamEPO = new Hashtable(); 
	alParamEPO = BeanParamDscto.getParametrosEPO(sNoEPO, 1);
	if (alParamEPO!=null) {
	
	sParamOperNotasCred 		= alParamEPO.get("OPERA_NOTAS_CRED")==null?"":(String)alParamEPO.get("OPERA_NOTAS_CRED");
			sParamDsctoObligatorio 	= alParamEPO.get("DSCTO_AUTO_OBLIGATORIO")==null?"":(String)alParamEPO.get("DSCTO_AUTO_OBLIGATORIO");	
			sParam_fn_monto_minimo 	= alParamEPO.get("MONTO_MINIMO")==null?"":(String)alParamEPO.get("MONTO_MINIMO");
			sParam_dscto_dinam 		= alParamEPO.get("DESCTO_DINAM")==null?"":(String)alParamEPO.get("DESCTO_DINAM");
			sParam_limite_pyme 		= alParamEPO.get("LIMITE_PYME")==null?"":(String)alParamEPO.get("LIMITE_PYME");
			sParam_firma_manc 		= alParamEPO.get("PUB_FIRMA_MANC")==null?"":(String)alParamEPO.get("PUB_FIRMA_MANC");
			sParam_criterio_hash 	= alParamEPO.get("PUB_HASH")==null?"":(String)alParamEPO.get("PUB_HASH");
			sParam_fecha_venc_pyme 	= alParamEPO.get("FECHA_VENC_PYME")==null?"":(String)alParamEPO.get("FECHA_VENC_PYME");
			sParam_pub_docto_venc   = alParamEPO.get("PUB_DOCTO_VENC")==null?"":(String)alParamEPO.get("PUB_DOCTO_VENC");
			
			sdiasMaximosAd		    	= alParamEPO.get("PLAZO_PAGO_FVP")==null?"":(String)alParamEPO.get("PLAZO_PAGO_FVP");
			sPlazoMaxFechasVePyDc   = alParamEPO.get("PLAZO_MAX1_FVP")==null?"":(String)alParamEPO.get("PLAZO_MAX1_FVP");
			sPlazoMaxFechasVenPyDm  = alParamEPO.get("PLAZO_MAX2_FVP")==null?"":(String)alParamEPO.get("PLAZO_MAX2_FVP");
			sParamPreafiliacion  	= alParamEPO.get("PREAFILIACION")==null?"":(String)alParamEPO.get("PREAFILIACION");
			sPermitirDescAutomatico	= alParamEPO.get("DESC_AUTO_PYME")==null?"":(String)alParamEPO.get("DESC_AUTO_PYME");
			sPubEPOPEF					= alParamEPO.get("PUBLICACION_EPO_PEF")==null?"":(String)alParamEPO.get("PUBLICACION_EPO_PEF");
			sPubEPOPEFSIAFFval		= (alParamEPO.get("PUB_EPO_PEF_USA_SIAFF")==null||alParamEPO.get("PUB_EPO_PEF_USA_SIAFF").equals(""))?"N":(String)alParamEPO.get("PUB_EPO_PEF_USA_SIAFF");
			
			sPubEPOPEFRecepcion		= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION");		// 13. PUB_EPO_PEF_FECHA_RECEPCION 
			sPubEPOPEFEntrega			= alParamEPO.get("PUB_EPO_PEF_FECHA_ENTREGA")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_FECHA_ENTREGA");				// 13.1 PUB_EPO_PEF_FECHA_ENTREGA
			sPubEPOPEFTipoCompra		= alParamEPO.get("PUB_EPO_PEF_TIPO_COMPRA")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_TIPO_COMPRA");					// 13.2 PUB_EPO_PEF_TIPO_COMPRA
			sPubEPOPEFPresupuestal	= alParamEPO.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL");// 13.3 PUB_EPO_PEF_CLAVE_PRESUPUESTAL
			sPubEPOPEFPeriodo			= alParamEPO.get("PUB_EPO_PEF_PERIODO")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_PERIODO");								// 13.4 PUB_EPO_PEF_PERIODO
			sOperaFactConMandato    = alParamEPO.get("PUB_EPO_OPERA_MANDATO")==null||alParamEPO.get("PUB_EPO_OPERA_MANDATO").equals("")?"N":(String)alParamEPO.get("PUB_EPO_OPERA_MANDATO");	// 14. PUB_EPO_OPERA_MANDATO
			sOperaFactVencimiento	= alParamEPO.get("PUB_EPO_VENC_INFONAVIT")==null||alParamEPO.get("PUB_EPO_VENC_INFONAVIT").equals("")?"N":(String)alParamEPO.get("PUB_EPO_VENC_INFONAVIT");	// 15. PUB_EPO_VENC_INFONAVIT  -FODEA 042 - Agosto/2009
	
      sOperaCesionDerechos = alParamEPO.get("OPER_SOLIC_CONS_CDER")==null||alParamEPO.get("OPER_SOLIC_CONS_CDER").equals("")?"N":(String)alParamEPO.get("OPER_SOLIC_CONS_CDER");	 //Fodea 018-2010
      sOperaFacVencido = alParamEPO.get("cs_factoraje_vencido")==null||alParamEPO.get("cs_factoraje_vencido").equals("")?"N":(String)alParamEPO.get("cs_factoraje_vencido");	 //Fodea 018-2010
      sOperaDesAutoFacVencido = alParamEPO.get("PUB_EPO_DESCAUTO_FACTVENCIDO")==null||alParamEPO.get("PUB_EPO_DESCAUTO_FACTVENCIDO").equals("")?"N":(String)alParamEPO.get("PUB_EPO_DESCAUTO_FACTVENCIDO");	 //Fodea 018-2010
      sOperposicionje24hrs = alParamEPO.get("PUB_EPO_FACT_24HRS")==null||alParamEPO.get("PUB_EPO_FACT_24HRS").equals("")?"N":(String)alParamEPO.get("PUB_EPO_FACT_24HRS");	 //Fodea 018-2010
		
		sOperaDsctoAutUltimoDia = alParamEPO.get("DESC_AUT_ULTIMO_DIA")==null||alParamEPO.get("DESC_AUT_ULTIMO_DIA").equals("")?"N":(String)alParamEPO.get("DESC_AUT_ULTIMO_DIA");	 //Fodea 018-2010
		
		sOperaFactorajeDist = alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO")==null||alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").equals("")?"N":(String)alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO");	 //FODEA  032-2010 FVR
		sAutorizacionCuentasBanc = alParamEPO.get("AUTORIZA_CTAS_BANC_ENTIDADES")==null||alParamEPO.get("AUTORIZA_CTAS_BANC_ENTIDADES").equals("")?"N":(String)alParamEPO.get("AUTORIZA_CTAS_BANC_ENTIDADES");	 //FODEA  032-2010 FVR

		sParamOperaTasasEspeciales	= alParamEPO.get("OPER_TASAS_ESPECIALES") == null || alParamEPO.get("OPER_TASAS_ESPECIALES").equals("") ?"N":(String)alParamEPO.get("OPER_TASAS_ESPECIALES");	 //FODEA  036-2010
		sParamTipoTasa					= alParamEPO.get("TIPO_TASA")             == null || alParamEPO.get("TIPO_TASA").equals("")             ?"N":(String)alParamEPO.get("TIPO_TASA");	 //FODEA  036-2010
      
	/*
		sParamOperNotasCred 		= (alParamEPO.get(0)==null)?"":alParamEPO.get(0).toString();
		sParamDsctoObligatorio 	= (alParamEPO.get(1)==null)?"":alParamEPO.get(1).toString();
		sParam_fn_monto_minimo 	= (alParamEPO.get(2)==null)?"":alParamEPO.get(2).toString();
		sParam_dscto_dinam 		= (alParamEPO.get(3)==null)?"":alParamEPO.get(3).toString();
		sParam_limite_pyme 		= (alParamEPO.get(4)==null)?"":alParamEPO.get(4).toString();
		sParam_firma_manc 		= (alParamEPO.get(5)==null)?"":alParamEPO.get(5).toString();
		sParam_criterio_hash 	= (alParamEPO.get(6)==null)?"":alParamEPO.get(6).toString();
		sParam_fecha_venc_pyme 	= (alParamEPO.get(7)==null)?"":alParamEPO.get(7).toString();
		sParam_pub_docto_venc   = (alParamEPO.get(8)==null)?"":alParamEPO.get(8).toString();
		sdiasMaximosAd		    	= (alParamEPO.get(9)==null)?"":alParamEPO.get(9).toString();
		sPlazoMaxFechasVePyDc   = (alParamEPO.get(10)==null)?"":alParamEPO.get(10).toString();
		sPlazoMaxFechasVenPyDm  = (alParamEPO.get(11)==null)?"":alParamEPO.get(11).toString();
		sParamPreafiliacion  	= (alParamEPO.get(12)==null)?"":alParamEPO.get(12).toString();
		sPermitirDescAutomatico	= (alParamEPO.get(13)==null)?"":alParamEPO.get(13).toString();
		sPubEPOPEF					= (alParamEPO.get(14)==null)?"":alParamEPO.get(14).toString();
		*/
	}
	//String sParamOperNotasCred = BeanParamDscto.getOperaNotasCredito(sNoEPO, 1);
	sbArchivo.append("1,Opera Notas de Cr�dito,"+(sParamOperNotasCred.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	sbArchivo.append("2,Descuento Obligatorio,"+(sParamDsctoObligatorio.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	sbArchivo.append("3,Monto m�nimo resultante para la aplicaci�n de Notas de Cr�dito"+ ","+ sParam_fn_monto_minimo +"\r\n");
	sbArchivo.append("4,Descuento Automatico Dinamico,"+(sParam_dscto_dinam.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	sbArchivo.append("5,Opera L�mite de Linea por PyME,"+(sParam_limite_pyme.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	sbArchivo.append("6,Publicaci�n con Firma Mancomunada,"+(sParam_firma_manc.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	sbArchivo.append("7,Publicaci�n con Criterio Hash,"+(sParam_criterio_hash.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	sbArchivo.append("8,Operaci�n con Fecha Vencimiento Proveedor,"+(sParam_fecha_venc_pyme.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	
	sbArchivo.append("8.1,Plazo para determinar d�as m�ximos adicionales a la fecha de vencimiento del proveedor"+ ","+ sdiasMaximosAd +"\r\n");
	sbArchivo.append("8.2,Plazo m�ximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento; cuyo plazo de pago al proveedor a partir de la publicaci�n sea menor al par�metro 8.1"+ ","+ sPlazoMaxFechasVePyDc +"\r\n");
	sbArchivo.append("8.3,Plazo m�ximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento; cuyo plazo de pago al proveedor a partir de la publicaci�n sea mayor  al par�metro 8.1"+ ","+ sPlazoMaxFechasVenPyDm +"\r\n");
	
	sbArchivo.append("9,Publicacion Documentos Vencidos,"+(sParam_pub_docto_venc.equalsIgnoreCase("S")?"SI":(sParam_pub_docto_venc.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	sbArchivo.append("10,Opera Pre-Afiliaci�n a Descuento PYMES,"+(sParamPreafiliacion.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	sbArchivo.append("11,Permitir Descuento Autom�tico a PYMES,"+(sPermitirDescAutomatico.equalsIgnoreCase("S")?"SI":(sPermitirDescAutomatico.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	sbArchivo.append("12,Publicaci�n EPO PEF (Clave SIAFF),"+(sPubEPOPEF.equalsIgnoreCase("S")?"SI":(sPubEPOPEF.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	if(sPubEPOPEF.equalsIgnoreCase("S")){
	sbArchivo.append("12.1, Utiliza SIAFF,"+(sPubEPOPEFSIAFFval.equalsIgnoreCase("S")?"SI":(sPubEPOPEFSIAFFval.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	}
	sbArchivo.append("13,Publicaci�n EPO PEF,"+(sPubEPOPEFRecepcion.equalsIgnoreCase("S")?"SI":(sPubEPOPEFRecepcion.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	if(sPubEPOPEFRecepcion.equalsIgnoreCase("S")){
	sbArchivo.append("13.1,Fecha de Recepci�n de Bienes y Servicios,"+(sPubEPOPEFEntrega.equalsIgnoreCase("S")?"SI":(sPubEPOPEFEntrega.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	sbArchivo.append("13.2,Tipo de Compra (procedimiento),"+(sPubEPOPEFTipoCompra.equalsIgnoreCase("S")?"SI":(sPubEPOPEFTipoCompra.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	sbArchivo.append("13.3,Clasificador por Objeto del Gasto,"+(sPubEPOPEFPresupuestal.equalsIgnoreCase("S")?"SI":(sPubEPOPEFPresupuestal.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	sbArchivo.append("13.4,Plazo M�ximo,"+(sPubEPOPEFPeriodo.equalsIgnoreCase("S")?"SI":(sPubEPOPEFPeriodo.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	} 
	sbArchivo.append("14,Opera Factoraje con Mandato,"+(sOperaFactConMandato.equalsIgnoreCase("S")?"SI":(sOperaFactConMandato.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	sbArchivo.append("15,Opera Factoraje Vencimiento Infonavit,"+(sOperaFactVencimiento.equalsIgnoreCase("S")?"SI":(sOperaFactVencimiento.equalsIgnoreCase("N")?"NO":""))+"\r\n");
  
  sbArchivo.append("16,Opera Solicitud de Consentimiento de Cesi�n de Derechos,"+(sOperaCesionDerechos.equalsIgnoreCase("S")?"SI":(sOperaCesionDerechos.equalsIgnoreCase("N")?"NO":""))+"\r\n");
  sbArchivo.append("17,Opera Factoraje 24hrs,"+(sOperposicionje24hrs.equalsIgnoreCase("S")?"SI":(sOperposicionje24hrs.equalsIgnoreCase("N")?"NO":""))+"\r\n"); //Fodea 018-2010  
  sbArchivo.append("18,Opera Factoraje Vencido,"+(sOperaFacVencido.equalsIgnoreCase("S")?"SI":(sOperaFacVencido.equalsIgnoreCase("N")?"NO":""))+"\r\n"); //Fodea 018-2010
  sbArchivo.append("18.1,Opera Descuento Autom�tico � Factoraje Vencido,"+(sOperaDesAutoFacVencido.equalsIgnoreCase("S")?"SI":(sOperaDesAutoFacVencido.equalsIgnoreCase("N")?"NO":""))+"\r\n"); //Fodea 018-2010
  
  sbArchivo.append("19,Opera Descuento Autom�tico � �ltimo d�a antes del vencimiento,"+(sOperaDsctoAutUltimoDia.equalsIgnoreCase("S")?"SI":(sOperaDsctoAutUltimoDia.equalsIgnoreCase("N")?"NO":""))+"\r\n"); //FODEA 024-2010
  
  sbArchivo.append("20,Factoraje Distribuido,"+(sOperaFactorajeDist.equalsIgnoreCase("S")?"SI":(sOperaFactorajeDist.equalsIgnoreCase("N")?"NO":""))+"\r\n"); //FODEA 032-2010 FVR
  sbArchivo.append("20.1,Autorizaci�n cuentas bancarias de entidades,"+(sAutorizacionCuentasBanc.equalsIgnoreCase("S")?"SI":(sAutorizacionCuentasBanc.equalsIgnoreCase("N")?"NO":""))+"\r\n"); //FODEA 032-2010 FVR
	  
  sbArchivo.append("21,Opera Tasas Especiales,"+(sParamOperaTasasEspeciales.equalsIgnoreCase("S")?"SI":(sParamOperaTasasEspeciales.equalsIgnoreCase("N")?"NO":""))+"\r\n"); //FODEA 036-2010
  sbArchivo.append("21.1,Tipo Tasa,"+(sParamTipoTasa.equalsIgnoreCase("P")?"SI":(sParamOperaTasasEspeciales.equalsIgnoreCase("N")?"NO":"NO")) +",Preferencial\r\n"); //FODEA 036-2010
  sbArchivo.append(",,"+(sParamTipoTasa.equalsIgnoreCase("NG")?"SI":(sParamOperaTasasEspeciales.equalsIgnoreCase("N")?"NO":"NO")) +",Negociada\r\n"); //FODEA 036-2010
  sbArchivo.append(",,"+(sParamTipoTasa.equalsIgnoreCase("A")?"SI":(sParamOperaTasasEspeciales.equalsIgnoreCase("N")?"NO":"NO")) +",Ambas\r\n"); //FODEA 036-2010
	  
	String nombreArchivo = "";
	CreaArchivo archivo = new CreaArchivo();
	if (archivo.make(sbArchivo.toString(), strDirectorioTemp, ".csv")) {
		nombreArchivo = archivo.nombre;		%>
<table width="150" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td class="formas" align="center"><br><br>
	<table cellpadding="1" cellspacing="2" border="0" class="formas">
	<tr>
		<td class="celda02" height="25" width="70" align="center"><a href="<%=strDirecVirtualTemp+nombreArchivo%>" onmouseout="window.status=''; return true;" onmouseover ="window.status='Descargar'; return true;">Descargar</a></td>
		<td class="celda02" height="25" width="60" align="center"><a href="javascript:window.close();" onmouseout="window.status=''; return true;" onmouseover ="window.status='Cerrar'; return true;">Cerrar</a></td>
	</tr>
	</table>
	</td>
</tr>
</table>
<%
	} // Genera Archivo
//} catch(NafinException ne) {	%>
<!--table width="550" border="0" cellspacing="0" cellpadding="2">
<tr>
	<td class="formas" height="20" align="center" colspan="2">
		<%//=ne.getMsgError()%>
	</td>
</tr>
</table-->
<%
} catch(Exception e) { 	%>
<table width="550" border="0" cellspacing="0" cellpadding="2">
<tr> 
	<td class="nota" height="20" align="center" valign="middle" colspan="2">
		<%=e.getMessage()%>
	</td>
</tr>
</table>
<%
}
%>
</div>
</html>
<%
/*	Created by HUGORO. Fecha: 10/12/2004.
	Foda 99 "Notas de Credito Factoraje Bimbo".	*/
%>