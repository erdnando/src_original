<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		java.sql.*,
		java.util.regex.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
	JSONObject archivoCSV = new JSONObject();
	JSONObject archivoPDF = new JSONObject();
	JSONObject registroAcuse = new JSONObject();
	JSONArray arrAcuse = new JSONArray();
	boolean flag = true;
	boolean flagExt = true;
	boolean flagReg = true;
	ParametrosRequest req = null;
	String rutaArchivo = "";
	String PATH_FILE	=	strDirectorioTemp;
	String itemArchivo = "";
	String extension = "";
	String folio = "";
	String fechaHora = "";
	String numProceso = "";
	String numTotal = "";
	String totalMonto = "";
	String msgError = "";
	int totalRegistros = 0;
	int tamanio = 0;
	String fechaHoy	= "";
	String horaActual	= "";
	
	String valida = "";
	String nombreArch = "";
	String errores = "";
	String err = "";
	int registros = 0;
	
	String sregistro = "", linea = "";
	
	BigDecimal montoTotal = new BigDecimal("0.0");
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		extension = (req.getParameter("hidExtension") == null)? "" : req.getParameter("hidExtension");

		try{
			int tamaPer = 0;
			if (extension.equals("txt")){
				upload.setSizeMax(200 * 1024);	//700 Kb
				tamaPer = (200 * 1024);
			}

			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			InputStream archivoZip = fItem.getInputStream();
			tamanio			= (int)fItem.getSize();

			rutaArchivo = PATH_FILE+itemArchivo;
			
			File file = new File(rutaArchivo);
			fItem.write(file);
			
			System.out.println("tamanio=="+tamanio);
			System.out.println("tamaPer=="+tamaPer);
			System.out.println("itemArchivo=="+itemArchivo);
			
			if (tamanio > tamaPer){
				flag = false;
				msgError = "El tama�o del archivo TXT es mayor al permitido";
			}
			if (flag){

				fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				try{

					//String PATH_FILE = strDirectorioTemp;
					//String nombreArch = (request.getParameter("nombreArch")==null)?"":request.getParameter("nombreArch");
					//String nombreArch = itemArchivo;
					nombreArch = itemArchivo;
					System.out.println("ARCHIVO:: "+nombreArch);
					//String tipoArchivo = (request.getParameter("tipoArchivo")==null)?"":request.getParameter("tipoArchivo");
					String nombreUsuario = strNombreUsuario;
					String login = strLogin;
					boolean resultado = true;
					int reg = 0;
					ArrayList lresultados = new ArrayList();
					
					
					lresultados = leerArchivoaTmpDest(PATH_FILE,nombreArch);		
					if(((Boolean)lresultados.get(0)).booleanValue()){
						resultado = true;
						lresultados = insertarDatosDest(PATH_FILE, nombreArch, login, nombreUsuario);
						if(((Boolean)lresultados.get(0)).booleanValue()){
							reg = ((Integer)lresultados.get(1)).intValue();
							resultado = true;
							fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
							horaActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
						}else{ // if(insertarDatosVenc(nombreArch))
							resultado = false;
							err +="Ocurrieron errores al guardar los datos.";
						}
					}else{ // if(((Boolean)lresultados.get(0)).booleanValue())
						resultado = false;
						err += "El archivo no se pudo cargar. ";
						err += lresultados.get(1).toString();
					}	
					System.out.println("resultado: " + resultado);
					System.out.println("err: " + err);
					
					if(resultado){
						valida = "ok";
						//nombreArch = nombreArch;
						errores = err;
						registros = reg;
					}else{
						
						valida = "er";
						errores = err;
						
						nombreArch = nombreArch.substring(0,(nombreArch.length()-4));
						java.io.File ferr = new java.io.File(strDirectorioTemp+"logerr"+nombreArch+".txt");
						if(ferr.exists()){
							BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(ferr)));
							while ((sregistro=br.readLine())!=null){
								linea += sregistro+"\n";
							}
							br.close();
						}
					}
					

				}catch (Exception e){
						throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			
			}
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>	


<%!
private ArrayList leerArchivoaTmpDest(String ruta, String nombreArch) throws IOException{
	System.out.println("leerArchivoaTmpDest(E)");
	System.out.println("ruta:" + ruta);
	System.out.println("nombreArch:" + nombreArch);
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	String qrySentencia = "", mensaje = "";
	boolean exito = true, validar = true;
	Vector vecdat = null;
	VectorTokenizer vt = null;
	VectorTokenizer vtcampos = new VectorTokenizer("cg_email_pub_vig",",");
	Vector vecCampos = vtcampos.getValuesVector();
	int i = 0, contador = 1, j = 0, pos = 0, k = 0, cont = 0, ilinea = 1;
	final int TOTAL_CAMPOS = 1;
	ArrayList lretorno = new ArrayList();		
	try{		
		con.conexionDB();
		StringBuffer strSQL = new StringBuffer();		

		// ABRIR ARCHIVO CARGADO
		java.io.File lofArchivo = new java.io.File(ruta + nombreArch);
		BufferedReader 	br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo)));
	
		String str;
		while ((str = br.readLine()) != null){			
			vt = new VectorTokenizer(str,"|");				
			vecdat = vt.getValuesVector();			
			for(k=0;k<vecdat.size();k++){
				String valor = vecdat.get(k).toString().trim();	
				//System.out.println("valor:" + valor);
				if(!valor.equals("")){
					if (!isEmail(valor)){
						mensaje = "Error en la linea: "+ilinea+", el campo ["+valor+"] no es un email valido.";
						exito = false;
					}
				}
				if(!exito)
					break;
			}//for(k=0;k<vecdat.size();k++)				
			if(!exito)
				break;
				ilinea++;
		}// fin while((str = br.readLine())!=null)
		br.close();
	}catch(IOException ioe){
		exito = false;
		System.out.println("leerArchivoaTmpDest:::IOException:..pos:: "+pos);
		ioe.printStackTrace();
	}catch(Exception e){
		exito = false;
		System.out.println("leerArchivoaTmpDest:::Exception:..pos:: "+pos);
		e.printStackTrace();
	}finally{
		System.out.println("leerArchivoaTmpDest(S)");
		if (con.hayConexionAbierta()){
			con.terminaTransaccion(exito);
			con.cierraConexionDB();
		}
	}
	lretorno.add(new Boolean(exito));
	lretorno.add(mensaje);
	return lretorno;
}

private ArrayList insertarDatosDest(String ruta, String nombreArch, String login, String nombreUsuario) throws SQLException{
	System.out.println("insertarDatosDest(E)");
	AccesoDB con = new AccesoDB();
	boolean exito = true;
	int registros = 0;
	ArrayList lretorno = new ArrayList();
	Vector vecdat = null;
	VectorTokenizer vt = null;
	try{
		con.conexionDB();

		// ABRIR ARCHIVO CARGADO
		java.io.File lofArchivo = new java.io.File(ruta + nombreArch);
		BufferedReader 	br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo)));
	
		String str;
		while ((str = br.readLine()) != null){
			vt = new VectorTokenizer(str,"|");				
			vecdat = vt.getValuesVector();
			registros = vecdat.size();
			
			String strSQL =	" INSERT INTO COM_EMAIL_PUB_VIG (ic_email_pub_vig, cg_email_pub_vig, cg_usuario, df_alta) " + 
											" select seq_com_email_pub_vig.nextval, '"+ str +"', " + 
											"'" + login + " - " + nombreUsuario + "', SYSDATE from dual";
			System.out.println("strSQL: " + strSQL);		
			con.ejecutaSQL(strSQL);		
			con.cierraStatement();
		}
	}catch(SQLException sqle){
		exito = false;
		System.out.println("error:::insertarDatosDest::: "+sqle);
	}catch(Exception e){
		exito = false;
		System.out.println("error:::insertarDatosDest:::"+e);
		e.printStackTrace();
	}finally{
		System.out.println("insertarDatosDest(S)");
		if (con.hayConexionAbierta()){
			con.terminaTransaccion(exito);
			con.cierraConexionDB();
		}
	}
	lretorno.add(new Boolean(exito));
	lretorno.add(new Integer(registros));
	return lretorno;
}

public boolean isEmail(String correo){
	Pattern pat = null;        
	Matcher mat = null;                
	pat = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	mat = pat.matcher(correo);        
	
	if (mat.find()) {            
		System.out.println("[" + mat.group() + "]");            
		return true;
	}else{
		return false;
	}            
}
%>

{
	"success": true,
	"msgError":	'<%=msgError%>',
	"valida": '<%=valida%>',
	"nombreArch": '<%=nombreArch%>',
	"errores": '<%=errores%>',
	"registros": '<%=registros%>',
	"fechaHoy": '<%=fechaHoy%>',
	"horaActual": '<%=horaActual%>',
	"usuario": '<%=strNombreUsuario%>',
	"strLogin": '<%=strLogin%>',
	"linea": '<%=linea%>'
	
}
