Ext.onReady(function() {

	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});

	var noSolicitud = [];
	var estatusAsignar = [];
	var causaRechazo = [];
	var numPrestamo = [];
	var fechaOperacion = [];
	var regSeleccioandos =[];
	
	var cancelar =  function() { 
		noSolicitud = [];
		estatusAsignar = [];
		causaRechazo = [];
		numPrestamo = [];
		fechaOperacion = [];	
		regSeleccioandos =[];
	}
	
	
		
	var procesarCausaRechazo= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var causa_rechazo1 = Ext.getCmp('causa_rechazo1');
			if(causa_rechazo1.getValue()==''){
				causa_rechazo1.setValue(records[0].data['clave']);
			}
		}
	}
	
	var catalogoCausaRechazo = new Ext.data.JsonStore({
		id: 'catalogoCausaRechazo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma01.data.jsp',
		baseParams: {
			informacion: 'catalogoCausaRechazo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	
		load: procesarCausaRechazo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	function causaRechazoWindos(value,metadata,registro,rowIndex,colIndex,store){
		
		var fpCausa = Ext.getCmp('fpCausa');		
		var ventana = Ext.getCmp('VerCausaRechazo');
		fpCausa.getForm().reset();		
		Ext.getCmp('indice').setValue(rowIndex);
		
		if (ventana) {
			ventana.show();			
			
		} else {
			new Ext.Window({				
				width: 500,						
				id:'VerCausaRechazo',
				closeAction: 'close',				
				closable:false,	
				modal:true,				
				items: [						
					fpCausa
				],
				title: 'Causa Rechazo',
				buttons:[
					{
						xtype:	'button',
						text:		'Aplicar causa de error',
						id:		'btnRechazo',
						iconCls:	'icoAceptar',
						handler:	function(boton, evento) {						
						
							var causa_rechazo1 = Ext.getCmp('causa_rechazo1');
							if(Ext.isEmpty(causa_rechazo1.getValue())){
								causa_rechazo1.markInvalid('Debe seleccionar la causa de rechazo');
								causa_rechazo1.focus();
								return;
							}											
						
							var causa_rechazo = Ext.getCmp('causa_rechazo1').getValue() +"\n"+Ext.getCmp('cg_causa_rechazo1').getValue();
							
							var indice = Ext.getCmp('indice').getValue();
							
							var  gridConsulta = Ext.getCmp('gridConsulta');	
							var reg = gridConsulta.getStore().getAt(indice);
							reg.set('CAUSA_RECHAZO', causa_rechazo);				
							store.commitChanges();
							
							Ext.getCmp('VerCausaRechazo').hide();														
					}	
				}	
			]
			}).show();
		}
	}
	


	var fpCausa = new Ext.form.FormPanel({
		id:'fpCausa',	
		width: 500,
		title: '',
		frame: true,	
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',		
		defaultType: 'textfield',
		labelWidth: 10,	
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},	
		items: [	
			{ 	xtype: 'textfield', hidden: true,  id: 'indice', 	value: '' },
			{
				xtype: 'combo',
				fieldLabel: '',
				name: 'causa_rechazo',
				id: 'causa_rechazo1',
				mode: 'local',
				autoLoad: false,
				displayField: 'descripcion',
				emptyText: 'Seleccionar ...',			
				valueField: 'clave',	
				hiddenName : 'causa_rechazo',
				forceSelection : true,			
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,			
				store: catalogoCausaRechazo					
			},
			
			{
				xtype: 'textarea',
				name: 'cg_causa_rechazo',
				id: 'cg_causa_rechazo1',
				fieldLabel: '',
				allowBlank: true,
				maxLength: 400,	
				hidden: false,		
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			}		
		]					
	});
	
	
	
		//*****************Descargar Archivos *************************************

	function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				id: 'btnImprimirReporte',
				text: 'Imprimir Reporte ',
				iconCls: 'icoPdf',	
				handler: function(boton, evento) {
					Ext.Ajax.request({
						url: '13forma01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ImprimirReporte',
							noSolicitudes: Ext.getCmp('noSolicitudes').getValue()
						}),
						callback: procesarArchivos
					});
				}				
			},
			{
					xtype: 'displayfield',
					value: '     ',
					width: 20
			},
			{
				xtype: 'button',
				id: 'btnDescargaArchivo',
				text: 'Descarga Archivo',
				iconCls: 'icoXls',	
				handler: function(boton, evento) {
					Ext.Ajax.request({
						url: '13forma01.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'DescargaArchivo',
							noSolicitudes: Ext.getCmp('noSolicitudes').getValue()
						}),
						callback: procesarArchivos
					});
				}				
			}			
		]
	});
	
	//*****************************Funci�n para Procesar *************************************

	function transmiteModificar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);			
			
			Ext.getCmp('noSolicitudes').setValue(info.noSolicitudes);
			Ext.getCmp('ic_folio1').setValue(info.ic_folio);
			Ext.getCmp('ic_estatus_solic1').setValue(info.ic_estatus_solic);
			
			Ext.getCmp('fpBotones').show();
			fp.el.mask('Enviando...', 'x-mask-loading');			
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Consultar',
					ic_folio:info.noSolicitudes
				})
			});	
			
			consultaDataTotales.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'ConsultarTotales',
					ic_folio:info.noSolicitudes
				})
			});
					
		} else {
				NE.util.mostrarConnError(response,opts);							
		}
	}
	
	var procesar2 = function() {
	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var numRegistros =0;
		noSolicitud = [];
	
		store.each(function(record) {		
			noSolicitud.push(record.data['NUM_SOLICITUD']);	
			numRegistros++;
		});
	
		Ext.Ajax.request({
			url: '13forma01.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'MODIFICAR_ESTATUS',
				noSolicitud:noSolicitud,
				numRegistros:numRegistros
			}),
			callback: transmiteModificar
		});		
	}
	
	
	//*****************************Funci�n para Procesar Operar  � Rechazar*************************************
	function transmiteCambioEstatus(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);			
			
			Ext.getCmp('fpBotones').hide();
			
			if(info.noSolicitudes !='') {			
				Ext.getCmp('forma').hide();
				Ext.getCmp('gridConsulta').hide();				
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('noSolicitudes').setValue(info.noSolicitudes);
				
				consultaAcuseData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultaAcuse',
						noSolicitudes:info.noSolicitudes						
					})
				});
				
			}else  {
				Ext.MessageBox.alert('Mensaje','Error en el cambio de estatus ' );
				window.location = '13forma01ext.jsp';
			}
		} else {
				NE.util.mostrarConnError(response,opts);							
		}
	}
	
	 
	var procesarOperar_Rechazar = function() {
		
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var columnModelGrid = gridConsulta.getColumnModel();
		
		var hayErrores=0;
		var numRegistros =0;
		var numReg =0;
		
		cancelar();
		store.each(function(record) {		
			numReg = store.indexOf(record);
			
			if(record.data['ESTATUS_ASIGNAR']==3){	
				if(record.data['NUMERO_PRESTAMO'] ==''){									
					Ext.MessageBox.alert('Error de validaci�n','Debe de capturar N�mero de Prestamo',
						function(){
							gridConsulta.startEditing(numReg, columnModelGrid.findColumnIndex('NUMERO_PRESTAMO'));
						}
					);
					hayErrores++;
					return false;	
				}
			}
			if(record.data['ESTATUS_ASIGNAR']==4){			
				if(record.data['CAUSA_RECHAZO'] ==''){								
					Ext.MessageBox.alert('Error de validaci�n','Debe de capturar Causa Rechazo',
						function(){
							gridConsulta.startEditing(numReg, columnModelGrid.findColumnIndex('CAUSA_RECHAZO'));
						}
					);
					hayErrores++;
					return false;	
				}
			}		
			if(hayErrores==0){
				noSolicitud.push(record.data['NUM_SOLICITUD']);
				estatusAsignar.push(record.data['ESTATUS_ASIGNAR']);
				causaRechazo.push(record.data['CAUSA_RECHAZO']);
				numPrestamo.push(record.data['NUMERO_PRESTAMO']);
				fechaOperacion.push( Ext.util.Format.date(record.data['FECHA_OPERACION'],'d/m/Y') ); 								
				numRegistros++;			
			}			
		});
		
		if(hayErrores==0){
			Ext.Msg.show({
				title:	"!Importante!",
				msg:		'Este proceso cambia el estatus de la solicitud',
				buttons:	Ext.Msg.OKCANCEL,
				fn: function resultMsj(btn){
					if(btn =='ok'){	
						Ext.Ajax.request({
							url: '13forma01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'CambioEstatus_solic',
								noSolicitud:noSolicitud,
								estatusAsignar:estatusAsignar,
								causaRechazo:causaRechazo,
								numPrestamo:numPrestamo,
								fechaOperacion:fechaOperacion,
								numRegistros:numRegistros
							}),
							callback: transmiteCambioEstatus
						});
					}
				}
			});
		}
	}
	
	//*****************consulta Acuse *************************************
	
	var procesarConsultaAcuseData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridAcuse = Ext.getCmp('gridAcuse');	
		var el = gridAcuse.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridAcuse.isVisible()) {
				gridAcuse.show();
			}					
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
				
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {								
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse'
		},
		hidden: true,
		fields: [				
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_PYME'},
			{name: 'NUM_SOLICITUD'},
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_OPERAR'},
			{name: 'ESTATUS_ACTUAL'},
			{name: 'CAUSA_RECHAZO'},
			{name: 'NETO_RECIBIR'},
			{name: 'BENEFICIARIO'},
			{name: 'PORCEN_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR'},
			{name: 'IC_ESTATUS'}		
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaAcuseData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaAcuseData(null, null, null);						
					}
				}
			}			
	});
	
	var gridAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridAcuse',
		hidden: true,
		title:'Autorizaci�n de Solicitudes Acuse ',
		store: consultaAcuseData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		
		columns: [
			{							
				header : 'IF',
				tooltip: 'IF',
				dataIndex : 'NOMBRE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'PYME',
				tooltip: 'PYME',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'N�m. de Solicitud',
				tooltip: 'N�m. de Solicitud',
				dataIndex : 'NUM_SOLICITUD',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Monto a operar',
				tooltip: 'Monto a operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Estatus Actual',
				tooltip: 'Estatus Actual',
				dataIndex : 'ESTATUS_ACTUAL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},									
			{
				header : 'Causa Rechazo', 
				tooltip: 'Causa Rechazo',
				dataIndex : 'CAUSA_RECHAZO', 
				fixed:true,
				sortable : false,	
				width : 300,
				align: 'left',
				hiddeable: false		
			},		
			{							
				header : 'Netro a Recibir',
				tooltip: 'Netro a Recibir',
				dataIndex : 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex : 'BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},
			{							
				header : '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex : 'PORCEN_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Importe a Recibir',
				tooltip: 'Importe a Recibir',
				dataIndex : 'IMPORTE_RECIBIR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],					
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		displayInfo: true,		
		clicksToEdit: 1, 
		bbar: {			
			items: [					
				'->',
				'-',
				{
					xtype: 'button',
					id: 'btnSalir',
					text: 'Salir',
					iconCls: 'icoLimpiar',
					handler: function() {
						window.location = '13forma01ext.jsp';					
					}
				},
				{
					xtype: 'button',
					id: 'btnImprimir',
					text: 'Imprimir',
					iconCls: 'icoPdf',	
					handler: function(boton, evento) {
						Ext.Ajax.request({
							url: '13forma01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'GenerarArchivoAcuse',
								noSolicitudes:Ext.getCmp('noSolicitudes').getValue()
							}),
							callback: procesarArchivos
						});
					}				
				}
			]
		}
	});
	

	


	//*******************GRID DE CONSULTA PRINICIPAL *******************************
		

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();			
			var el = gridConsulta.getGridEl();	
				
			if(jsonData.ic_estatus_solic==1) { 
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAUSA_RECHAZO'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUMERO_PRESTAMO'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_OPERACION'), true);	
				
			}else  if(jsonData.ic_estatus_solic==2) { 
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAUSA_RECHAZO'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUMERO_PRESTAMO'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_OPERACION'), false);	
			}						
			
			if(store.getTotalCount() > 0) {				
				gridTotales.show();
				if(jsonData.ic_estatus_solic==1) { 
					Ext.getCmp('btnOperar_Rechazar').hide();	
					Ext.getCmp('btnProcesar').show();
					Ext.getCmp('btnProcesar').enable();
				}else if(jsonData.ic_estatus_solic==2) { 
					
					Ext.getCmp('btnProcesar').hide();	
					Ext.getCmp('btnOperar_Rechazar').show();
					Ext.getCmp('btnOperar_Rechazar').enable();				
				}				
				el.unmask();					
			} else {	
				gridTotales.hide();
				Ext.getCmp('btnOperar_Rechazar').hide();
				Ext.getCmp('btnProcesar').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},
		hidden: true,
		fields: [	
			{name: 'MONEDA'},
			{name: 'NUM_DOCTOS'},
			{name: 'TOTAL_MONTOS'}											
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'',
		hidden: true,
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'N�mero de Documentos',
				tooltip: 'N�mero de Documentos',
				dataIndex : 'NUM_DOCTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'Total Monto',
				tooltip: 'Total Monto',
				dataIndex : 'TOTAL_MONTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 100,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [				
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_PYME'},
			{name: 'NUM_SOLICITUD'},
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_OPERAR'},
			{name: 'ESTATUS_ACTUAL'},
			{name: 'ESTATUS_ASIGNAR'},
			{name: 'ORIGEN'},
			{name: 'CAUSA_RECHAZO'},
			{name: 'NUMERO_PRESTAMO'},
			{name: 'FECHA_OPERACION'},
			{name: 'NETO_RECIBIR'},
			{name: 'BENEFICIARIO'},
			{name: 'PORCEN_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR'},
			{name: 'IC_ESTATUS_ACTUAL'}				
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	
	//*****************Para el combo del Grid *************************************
	var catalogoEstatusAsignar = new Ext.data.JsonStore({
		id: 'catalogoEstatusAsignar',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],		
		url : '13forma01.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatusAsignar'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var comboEstatusAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catalogoEstatusAsignar
	});
	
	
	Ext.util.Format.comboRenderer = function(comboEstatusAsignar){	
	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var valor = registro.data['ESTATUS_ASIGNAR'];
			var  estatusActualfp =  Ext.getCmp('ic_estatus_solic1').getValue();
			var  statusActualgrid =  registro.data['IC_ESTATUS_ACTUAL'];
				
			if(estatusActualfp==2) { //En Proceso	
				if(valor =='4' &&  registro.data['CAUSA_RECHAZO']==''  ){ // Rechazada Nafin
					causaRechazoWindos(value,metadata,registro,rowIndex,colIndex,store);
				}
				if(valor =='3' &&  registro.data['CAUSA_RECHAZO']!=''  ){ 
					var  gridConsulta = Ext.getCmp('gridConsulta');	
					var reg = gridConsulta.getStore().getAt(rowIndex);
					reg.set('CAUSA_RECHAZO','');				
					store.commitChanges();				
				}
				
				if(valor !=''){
					var record = comboEstatusAsignar.findRecord(comboEstatusAsignar.valueField, value);
					return record ? record.get(comboEstatusAsignar.displayField) : comboEstatusAsignar.valueNotFoundText;
				} 	
			}else if(estatusActualfp==1) { // Seleccionada IF
            return 'En proceso';
         }			
		}
	}	


	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		hidden: true,
		title:'Autorizaci�n de Solicitudes',
		store: consultaData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		
		columns: [
			{							
				header : 'IF',
				tooltip: 'IF',
				dataIndex : 'NOMBRE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'PYME',
				tooltip: 'PYME',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'N�m. de Solicitud',
				tooltip: 'N�m. de Solicitud',
				dataIndex : 'NUM_SOLICITUD',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Monto a operar',
				tooltip: 'Monto a operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Estatus Actual',
				tooltip: 'Estatus Actual',
				dataIndex : 'ESTATUS_ACTUAL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Estatus Asignar',
				tooltip: 'Estatus Asignar',
				dataIndex: 'ESTATUS_ASIGNAR',
				sortable: true,
				resizable: true,
				width: 200,				
				align: 'left',
				editor: comboEstatusAsignar,
				renderer: Ext.util.Format.comboRenderer(comboEstatusAsignar)
			},	
			{							
				header : 'Origen',
				tooltip: 'Origen',
				dataIndex : 'ORIGEN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'
			},			
			{
				header : 'Causa Rechazo', 
				tooltip: 'Causa Rechazo',
				dataIndex : 'CAUSA_RECHAZO', 
				fixed:true,
				sortable : false,	
				width : 300,
				align: 'left',
				hiddeable: false,							
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},
			{
				header: 'N�mero de Pr�stamo', 
				tooltip: 'N�mero de Pr�stamo',
				dataIndex: 'NUMERO_PRESTAMO',
				sortable: true,
				resizable: true,
				width: 130,
				hidden: false,
				align: 'center',
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				},
				renderer:function(value,metadata,registro){
					return NE.util.colorCampoEdit(value,metadata,registro);
				}
			},
			{				
				header : 'Fecha de operaci�n', // es es editable 
				tooltip: 'Fecha de operaci�n',
				dataIndex : 'FECHA_OPERACION',
				width : 150,
				sortable : false,
				hidden: false,
				align: 'center',
				editor: campoFechas,
				renderer:function(value,metadata,registro){
					 return NE.util.colorCampoEdit(Ext.util.Format.date(value,'d/m/Y'),metadata,registro);
				}
			},
			{							
				header : 'Netro a Recibir',
				tooltip: 'Netro a Recibir',
				dataIndex : 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex : 'BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},
			{							
				header : '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex : 'PORCEN_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Importe a Recibir',
				tooltip: 'Importe a Recibir',
				dataIndex : 'IMPORTE_RECIBIR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],					
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		displayInfo: true,		
		clicksToEdit: 1, 
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;					
				if(campo == 'ESTATUS_ASIGNAR'  && e.record.data['IC_ESTATUS_ACTUAL']==2){							
					return true; // es editable 					
				}else  if(campo == 'ESTATUS_ASIGNAR'  && e.record.data['IC_ESTATUS_ACTUAL']!=2){
					return false; // no es editable 
				}
				
				if(campo == 'CAUSA_RECHAZO'){
				//	return false; // no es editable 
				}					
				
			}
		},
		bbar: {			
			items: [					
				'->',
				'-',
				{
					xtype: 'button',
					id: 'btnOperar_Rechazar',
					text: 'Operar/Rechazar',
					iconCls: 'icoAceptar',
					handler: procesarOperar_Rechazar
				},
					{
					xtype: 'button',
					id: 'btnProcesar',
					text: 'Procesar',
					hidden: true,
					iconCls: 'icoAceptar',
					handler: procesar2
				}
			]
		}
	});
	
//*********************** Criterios de Busqueda ********************************
		
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma01.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma01.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoMONEDA = new Ext.data.JsonStore({
		id: 'catalogoMONEDA',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma01.data.jsp',
		baseParams: {
			informacion: 'catalogoMONEDA'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var procesarCatalogoESTATUS= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_estatus_solic1 = Ext.getCmp('ic_estatus_solic1');
			if(ic_estatus_solic1.getValue()==''){
				ic_estatus_solic1.setValue(records[0].data['clave']);
			}
		}
	}
	
	var catalogoESTATUS = new Ext.data.JsonStore({
		id: 'catalogoESTATUS',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma01.data.jsp',
		baseParams: {
			informacion: 'catalogoESTATUS'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	
		load: procesarCatalogoESTATUS,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		//Panel de valores iniciales
	var catalogoORIGEN = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['C','Internas'],
			['E','Externas']
		]
	});
	
	
	var elementosForma =[
		{
			xtype: 'combo',
			fieldLabel: 'Nombre del Intermediario',
			name: 'ic_if',
			id: 'ic_if1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_if',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,			
			store: catalogoIF,
			tpl : NE.util.templateMensajeCargaCombo				
		},
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_epo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,			
			store: catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {		
						Ext.getCmp('cs_tipo_solicitud1').setValue('C');					
					}
				}
			}
		},
		{
			xtype: 'textfield',
			name: 'ic_folio',
			id: 'ic_folio1',
			fieldLabel: 'N�mero de la Solicitud',
			allowBlank: true,
			maxLength: 11,	
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0' 
		},
		{
			xtype: 'combo',
			fieldLabel: 'Moneda',
			name: 'ic_moneda',
			id: 'ic_moneda1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_moneda',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,			
			store: catalogoMONEDA,
			tpl : NE.util.templateMensajeCargaCombo			
		},	
		{
			xtype: 'combo',
			fieldLabel: 'Estado Solicitud',
			name: 'ic_estatus_solic',
			id: 'ic_estatus_solic1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'ic_estatus_solic',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,			
			store: catalogoESTATUS,
			tpl : NE.util.templateMensajeCargaCombo			
		},
		{
			xtype: 'combo',
			fieldLabel: 'Origen',
			name: 'cs_tipo_solicitud',
			id: 'cs_tipo_solicitud1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'cs_tipo_solicitud',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,			
			store: catalogoORIGEN					
		},
		{ 	xtype: 'textfield', hidden: true, id: 'noSolicitudes', 	value: '' }
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {			
								
					var ic_epo1 = Ext.getCmp('ic_epo1');
					if(!Ext.isEmpty(ic_epo1.getValue())){
						Ext.getCmp('cs_tipo_solicitud1').setValue('C');			
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'							
						})
					});
					
					consultaDataTotales.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ConsultarTotales'	
						})
					});
				
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13forma01ext.jsp';					
				}
			}
		]
	});
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),			
			fp,
			NE.util.getEspaciador(20),
			fpBotones,			
			gridAcuse,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});

	
	catalogoIF.load();
	catalogoEPO.load();
	catalogoMONEDA.load();
	catalogoESTATUS.load();
	catalogoEstatusAsignar.load();
	catalogoCausaRechazo.load();

	
	
});