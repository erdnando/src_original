
Ext.onReady(function() {
//_--------------------------------- HANDLERS -------------------------------

	var procesarConsultaData = function(store, arrRegistros, opts) {
	var fp = Ext.getCmp('forma');	
		fp.el.unmask();			
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}	
					
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var el = grid.getGridEl();
			
			if (arrRegistros == '') {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				btnGenerarArchivo.disable();
				btnImprimirPDF.disable();	
			}else if (arrRegistros != null){
				el.unmask();
				btnGenerarArchivo.enable();
				btnImprimirPDF.enable();		
			}
		}
	}	
	
	
	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarArchivoPDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			btnImprimirPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}		
//GENERAR ARCHIVO DE GRID GENERAL
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		  btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
//-------------------------------- STORES -----------------------------------
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta18.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var catalogoBancoFondeoData = new Ext.data.JsonStore({
		id: 'catalogoBancoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta18.data.jsp',
		baseParams: {
			informacion: 'CatalogoBanco'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	//CONSULTA PARA LOS DEL GRID GENERAL
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta18.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [				  
			{name: 'nombre'},
			{name: 'dmin'},
			{name: 'dmax'},			
			{name: 'fecha_aforo'},
			{name: 'fecha_aforo_dol'},
			{name: 'fact_venc'},
			{name: 'fact_distr'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}		
	});
	
													
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_banco_fondeo',
			id: 'ic_banco_fondeo2',
			fieldLabel: 'Banco de Fondeo',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_banco_fondeo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			allowBlank: true,
			store : catalogoBancoFondeoData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var epoComboCmp = Ext.getCmp('cmbEPO');
						epoComboCmp.setValue('');
						epoComboCmp.setDisabled(false);
						epoComboCmp.store.load({
								params: {
									ic_banco_fondeo: combo.getValue()
								}
						});
					}
				}
			}
			
		},			
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'cmbEPO',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Todas...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo
		}		
		];
				
		
// create the Grid GENERAL
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		margins: '20 0 0 0',
		hidden: true,
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'nombre',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header: 'D�as M�n. para Descuento',
				tooltip: 'D�as M�n. para Descuento',
				dataIndex: 'dmin',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},			
			{
				header: 'D�as M�x. para Descuento',
				tooltip: 'D�as M�x. para Descuento',
				dataIndex: 'dmax',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},			
			{
				header: 'Porcentaje de Descuento M.N',
				tooltip: 'Porcentaje de Descuento M.N',
				dataIndex: 'fecha_aforo',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},						
			{	header: 'Porcentaje de Descuento Dol.',
				tooltip: 'Porcentaje de Descuento Dol.',
				dataIndex: 'fecha_aforo_dol',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},					
			{
				header: 'Opera Factoraje Vencido',
				tooltip: 'Opera Factoraje Vencido',
				dataIndex: 'fact_venc',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			}	,	
			{
				header: 'Opera Factoraje Distribuido',
				tooltip: 'Opera Factoraje Distribuido',
				dataIndex: 'fact_distr',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			}		
		],		
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 400,
		width: 885,
		title: '',
		frame: true,		
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta18exta.jsp',
							params: fp.getForm().getValues(),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},			
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',				
				{
					xtype: 'button',
					text: 'Imprimir PDF',
					id: 'btnImprimirPDF',
					align: 'right',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta18exti.jsp',
							params: fp.getForm().getValues(),
							callback: procesarSuccessFailureGenerarArchivoPDF
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}		
			]
		}
	});	
	
//-------------------------------- PRINCIPAL -----------------------------------

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Otros Parametros por EPO',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
				
				var ic_banco_fondeo = Ext.getCmp("ic_banco_fondeo2");
				if (Ext.isEmpty(ic_banco_fondeo.getValue()) ) {
						ic_banco_fondeo.markInvalid('Por favor, especifique el Banco de Fondeo');
						return;
				}
					
					//fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({					
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar' //Generar datos para la consulta							
						})
					});
					Ext.getCmp('btnGenerarArchivo').disable();
					Ext.getCmp('btnBajarArchivo').hide();
					Ext.getCmp('btnImprimirPDF').disable();
					Ext.getCmp('btnBajarPDF').hide();					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {										
					fp.getForm().reset();
					grid.hide();
				}
			}				
			
		]
		
	});

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',		
		width: 890,	
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});

//-------------------------------- ----------------- -----------------------------------
catalogoEPOData.load();
catalogoBancoFondeoData.load();


});