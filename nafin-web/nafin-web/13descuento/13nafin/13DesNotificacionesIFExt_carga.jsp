<%@ page contentType="text/html; charset=UTF-8" import="java.util.List,java.util.Iterator,
java.io.File,
org.apache.commons.fileupload.servlet.ServletFileUpload,
org.apache.commons.fileupload.disk.DiskFileItemFactory,
org.apache.commons.fileupload.*,
java.io.*,
javax.naming.*,
netropology.utilerias.*,
com.netro.descuento.*,
java.util.*,
java.math.BigDecimal,
net.sf.json.JSONArray,
net.sf.json.JSONObject"
       errorPage="/00utils/error_extjs_fileupload.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param archivo
	 */
	 private String leerArchivo(InputStream archivo) throws AppException   {
    InputStreamReader isR = null;
    BufferedReader bis    = null;
    String lineaArchivo   = "";
    String archivoS       = "";
    try
    {
      isR = new InputStreamReader(archivo, "ISO-8859-1");
      bis = new BufferedReader(isR);
      lineaArchivo = bis.readLine();
      while(lineaArchivo!=null)
      {
        archivoS+=lineaArchivo+"\n";  
        lineaArchivo = bis.readLine();  
      }
    }catch(Exception e)
    {
      throw new AppException("Error Inesperado", e);
    }
    return archivoS;
  } 
%>
<%
		boolean result = true;
		FileItem receivedFile;
		String fileName="";		
		FileItem item =null;
		File savedFile=null;
		InputStream zipStream=null;
		String ic_if="",notificacion="";
		List datos=null;
		String usuario = strLogin+" "+strNombreUsuario;
		String cgError="";
		String s_archivoCar="",s_totalC="",s_fechaC="",s_horaC="",s_usuario="";
				
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List items = upload.parseRequest(request);

				Iterator iter = items.iterator();
				while (iter.hasNext()) {
					 item = (FileItem)iter.next();

					if (item.isFormField()) {
						if (item.getFieldName().equalsIgnoreCase("ic_if")) {
							ic_if=!item.getString().equals("")?item.getString():"";
						}
						else if (item.getFieldName().equalsIgnoreCase("notificacion")) {
							 notificacion=item.getString();			
						}
						
						
					} else {
						
						try {
								String nombreArchivo = item.getName();
								CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
								cgError = CargaDocumentos.archivoCorreosNotiIF(leerArchivo(item.getInputStream()));
									
								if(cgError.equals("")){
										datos  = CargaDocumentos.cargaCorreosNotiIF(ic_if, notificacion, usuario, leerArchivo(item.getInputStream()), nombreArchivo);
								}
								if(datos !=null){ 
								 s_archivoCar = (String)datos.get(0); 
								 s_totalC = (String)datos.get(1); 
								 s_fechaC = (String)datos.get(2); 
								 s_horaC = (String)datos.get(3); 
								 s_usuario = (String)datos.get(4); 
								}
								
								cgError= cgError.length()>0?cgError.substring(0,cgError.length()-1):"";
								cgError = cgError.replaceAll("(\r\n|\n)", "--");
							} catch (Exception e) {
								e.printStackTrace();
							}				
					}
				}
								
			} catch (Exception e) {
									
					e.printStackTrace();						
				}		
		}
			
	%>
	
{"success": true,
 "erroresm":"<%= cgError %>",
"archivoCar":"<%=s_archivoCar%>",
"totalC":"<%=s_totalC%>",
"fechaC":"<%=s_fechaC%>",
"horaC":"<%=s_horaC%>",
"usuario":"<%=s_usuario%>"}