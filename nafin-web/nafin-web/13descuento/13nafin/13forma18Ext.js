Ext.onReady(function() {

	var tabActiva = Ext.get('tabActiva').getValue();
	var varGlobal = {_if:null,	sequence:null}

//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 

	var catalogoIfData = new Ext.data.JsonStore({
		id:				'catalogoIfDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'13forma18Ext.data.jsp',
		baseParams:		{	informacion:	'CatalogoIf'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIfConsultaData = new Ext.data.JsonStore({
		id:				'catalogoIfConsultaDataStore',
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'13forma18Ext.data.jsp',
		baseParams:		{	informacion:	'CatalogoIfConsulta'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var registrosConsultadosData = new Ext.data.JsonStore({
		fields: [
			{name: 'CONSECUTIVO'	},
			{name: 'CLAVE_IF'	},
			{name: 'NOMBRE_IF'	},
			{name: 'FECHA_ALTA'	},
			{name: 'MODIFICAR'	},		//, 	convert: NE.util.string2boolean 
			{name: 'ACTIVA'		}
		],
		data:	[
			{'NOMBRE_IF':'','DOCUMENTO':'','FECHA_ALTA':''}
		],
		autoLoad:	true,
		listeners:	{exception: NE.util.mostrarDataProxyError}
	});

	var registrosCapturadosData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMBRE_IF'},
			{name: 'DOCUMENTO'},
			{name: 'FECHA_ALTA'},
			{name: 'ACTIVA'}
		],
		data:	[
			{'NOMBRE_IF':'','DOCUMENTO':'','FECHA_ALTA':''}
		],
		autoLoad:	true,
		listeners:	{exception: NE.util.mostrarDataProxyError}
	});

//- - - - - - - - - - - - MAQUINA DE ESTADO (-SIMULACI�N-) - - - - - - - - - - 

	var procesaAccion = function(opts, success, response) {
		Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					Ext.Msg.alert(
						'Mensaje',
						resp.msg,
						function(btn, text){
							accionConsulta(resp.estadoSiguiente,resp);
						}
					);
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}

	var accionConsulta = function(estadoSiguiente, respuesta){

		if(  estadoSiguiente == "ACEPTAR_FORMA_CAPTURAR" 										){

			Ext.getCmp("formaCapturar").getForm().submit({
				clientValidation: 	true,
				url: 						'13forma18Ext.data.jsp?informacion=AceptaCapura.subirArchivo',
				waitMsg:   				'Subiendo archivo...',
				waitTitle: 				'Por favor espere',
				success: function(form, action) {
					 //procesaCargaArchivo(null,  true,  action.response );
					var resp = 	Ext.util.JSON.decode(action.response.responseText);
					tp.hideTabStripItem(1);
					Ext.getCmp('formaCapturar').hide();
					Ext.getCmp('_nombreArchivo').setValue(resp.nombreArchivo);
					Ext.getCmp('_tipoRegistro').setValue(resp.tipoRegistro);
					Ext.getCmp('_doctoActualizado').setValue(resp.doctoActualizado);
					Ext.getCmp('_ic_consecutivo').setValue(resp.consecutivo);

					var imagen = "";
					if (resp.extension == "doc") {
						imagen = "icoDoc";
					} else if (resp.extension == "ppt" || resp.extension == "pps") {
						imagen = "icoPpt";
					} else if (resp.extension == "pdf") {
						imagen = "icoPdf";
					}else{
						imagen = "vacio";
					}

					var elementosAceptar = [
						{
							xtype:		'panel',
							style:		'margin:0 auto;',
							bodyStyle:	'padding: 6px',
							border:		false,
							layout:		'form',
							html:			'<div class="formas" align="center"><b><font color="#FF0000">' + Ext.getCmp('_cg_titulo_aviso').getValue() + '</font></b></div>'	+
											'<div class="formas" align="left">' + Ext.getCmp('_cg_contenido_aviso').getValue() + '</div>',
							buttonAlign:'center',
							buttons: [
								{
									text:			Ext.getCmp('_cg_boton_aviso').getValue(),
									iconAlign:	'right',
									iconCls:		imagen,
									urlArchivo:	resp.clausulado_preview,
									handler: function(boton, event) {
													var forma 		= Ext.getDom('formAux');
													forma.action 	= boton.urlArchivo;
													forma.submit();
												}
								}
							]
						},{
							xtype:		'panel',
							style:		'margin:0 auto;',
							bodyStyle:	'padding: 6px',
							border:		false,
							layout:		'form',
							html:			'<div class="formas" align="left"><img src="/nafin/00utils/gif/chk_h.gif" border="0">' + Ext.getCmp('_cg_texto_aceptacion').getValue() +'</div>'
						},{
							xtype:		'panel',
							style:		'margin:0 auto;',
							layout:		'form',
							border:		false,
							buttonAlign:'center',
							buttons: [
								{
									text:	Ext.getCmp('_cg_boton_aceptacion').getValue()
								}
							]
						}
					];

					Ext.getCmp('formaAceptar').add(elementosAceptar);
					Ext.getCmp('formaAceptar').doLayout();
					Ext.getCmp('formaAceptar').show();

				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaAccion(null,  false, action.response );
				}
			});

		}else if(  estadoSiguiente == "CANCELA_CAPTURA" 										){

			window.location = "13forma18Ext.jsp";

		}else if(  estadoSiguiente == "REGRESA_CAPTURA" 										){

			Ext.getCmp('formaAceptar').hide();
			Ext.getCmp('formaAceptar').removeAll();
			Ext.getCmp('formaAceptar').doLayout();
			var win = Ext.getCmp('winPopup');
			if(win){
				win.destroy();
			}
			Ext.getCmp('formaCapturar').show();
			Ext.getCmp('_bi_documento').reset();
			var tipoReg = Ext.getCmp('_tipoRegistro').getValue();
			if (tipoReg == "NUEVO"){
				tp.unhideTabStripItem(1);
			}

		}else if(  estadoSiguiente == "ACEPTA_CAPTURA" 											){

			Ext.getCmp('formaAceptar').el.mask('Procesando...', 'x-mask-loading');

			Ext.Ajax.request({
				url: '13forma18Ext.data.jsp',
				params:	Ext.apply(fpCapturar.getForm().getValues(),
							{
								informacion:	'AceptaCapura.Aceptar'
							}
				),
				callback: procesaAccion
			});

		}else if(  estadoSiguiente == "RESPUESTA_ACEPTA_CAPTURA" 							){

			registrosCapturadosData.loadData(respuesta.registrosCapturadosData);
			Ext.getCmp('formaAceptar').hide();
			gridCaptura.show();

		} else if(  estadoSiguiente == "CONSULTA_FORMA_CONSULTAR" 							){

			var forma = Ext.getCmp("formaConsultar");
			forma.el.mask('Buscando...','x-mask-loading');
			registrosConsultadosData.removeAll();
			varGlobal._if		=null;
			varGlobal.sequence=null;

			Ext.Ajax.request({
				url: '13forma18Ext.data.jsp',
				params:	Ext.apply(forma.getForm().getValues(),
							{
								informacion:	'Consultar'
							}
				),
				callback: procesaAccion
			});

		}else if(  estadoSiguiente == "RESPUESTA_CONSULTA" 									){

			registrosConsultadosData.loadData(respuesta.registrosConsultadosData);
			var gridConsulta = Ext.getCmp('gridConsulta');
			if(	!gridConsulta.isVisible()	){
				gridConsulta.show();
			}

		} else if(	estadoSiguiente == "ACCION.MODIFICAR"										){

			//var forma = Ext.getCmp("formaConsultar");
			tp.el.mask('Procesando...','x-mask-loading');

			Ext.Ajax.request({
				url: '13forma18Ext.data.jsp',
				params:	{
								informacion:	'Accion.modificar',
								clave_if:		respuesta.claveIf,
								consecutivo:	respuesta.consecutivo
							},
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA.ACCION.MODIFICAR"							){

			tp.setActiveTab(0);
			tp.hideTabStripItem(0);
			tp.hideTabStripItem(1);
			Ext.getCmp('_ic_if').setValue(respuesta.ic_if);
			Ext.getCmp('_ic_if').hide();
			Ext.getCmp("labelIf").setText(respuesta.nombreIf,false);
			Ext.getCmp("labelIf").show();
			Ext.getCmp('_cg_titulo_aviso').setValue(respuesta.cg_titulo_aviso);
			Ext.getCmp('_cg_contenido_aviso').setValue(respuesta.cg_contenido_aviso);
			Ext.getCmp('_cg_boton_aviso').setValue(respuesta.cg_boton_aviso);
			//Ext.getCmp('_bi_documento').setValue(respuesta.bi_documento);
			Ext.getCmp('_cg_texto_aceptacion').setValue(respuesta.cg_texto_aceptacion);
			Ext.getCmp('_cg_boton_aceptacion').setValue(respuesta.cg_boton_aceptacion);
			if(respuesta.cs_mostrar == "S"){
				Ext.getCmp('_cs_mostrar').setValue('on');
				Ext.getCmp('_cs_mostrar').checked = true;
			}
			//Ext.getCmp('_nombreArchivo').setValue(respuesta.nombreArchivo);
			Ext.getCmp('_tipoRegistro').setValue(respuesta.tipoRegistro);
			Ext.getCmp('_doctoActualizado').setValue(respuesta.doctoActualizado);
			Ext.getCmp('_ic_consecutivo').setValue(respuesta.ic_consecutivo);
			Ext.getCmp('_bi_documento').allowBlank = true;
			Ext.getCmp('btnLimpiar').hide();
			Ext.getCmp('btnCancelar').show();

		} else if(	estadoSiguiente == "ACCION.ABRIR_CLAUSULADO"										){

			varGlobal._if			= respuesta.claveIf;
			varGlobal.sequence	= respuesta.consecutivo;

			tp.el.mask('Procesando...','x-mask-loading');

			Ext.Ajax.request({
				url: '13forma18Ext.data.jsp',
				params:	{
								informacion:	'Accion.abrir_clausulado',
								clave_if:		respuesta.claveIf,
								consecutivo:	respuesta.consecutivo
							},
				callback: procesaAccion
			});

		} else if(	estadoSiguiente == "RESPUESTA.ACCION.ABRIR_CLAUSULADO"							){

			Ext.getCmp('btnAux').setHandler(
				function(btn, event){
					var forma 		= Ext.getDom('formAux');
					forma.action 	= respuesta.nombreArchivo;
					forma.submit();
				}
			);
			
			Ext.getCmp("btnAux").handler.call(Ext.getCmp("btnAux").scope);

		} else if(	estadoSiguiente == "LIMPIAR_CAPTURA"										){

			Ext.getCmp('formaCapturar').getForm().reset();

		} else if(	estadoSiguiente == "FIN"														){

			// Finalizar pantalla
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "13forma18Ext.jsp";
			forma.target	= "_self";
			forma.submit();
		}
	}
//- - - - - - -  - - - - - COMPONENTES - - - - - - - - - - - - -

	// Grid con las cuentas registradas
	var gridConsulta = new Ext.grid.GridPanel({
		store: 		registrosConsultadosData,
		id:			'gridConsulta',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		margins:		'20 0 0 0',
		title:		undefined,
		view:			new Ext.grid.GridView({forceFit:	true}),
		clicktoEdit:	1,
		stripeRows: true,
		loadMask: 	true,
		height: 		300,
		width: 		810,
		frame: 		true,
		columns: [
			{
				header: 		'IF',
				tooltip: 	'IF',
				dataIndex: 	'NOMBRE_IF',
				sortable: 	true,
				resizable: 	true,
				width: 		400,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				xtype:	'actioncolumn',
				header:	'Documento', tooltip: 'Documento',
				menuDisabled:true,
				dataIndex: '',	align: 'center',	sortable: true,	resizable: true,	width: 100,	hideable: false, hidden: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						},
						handler:	function(grid, rowIndex, colIndex, item, event)	{
										var record = registrosConsultadosData.getAt(rowIndex);
										var respuesta				=	new Object();
										respuesta["claveIf"]		=	record.json['CLAVE_IF'];
										respuesta["consecutivo"]=	record.json['CONSECUTIVO'];

										if(	(varGlobal._if == respuesta.claveIf) && (varGlobal.sequence == respuesta.consecutivo)	){

											Ext.getCmp("btnAux").handler.call(Ext.getCmp("btnAux").scope);

										}else{

											accionConsulta("ACCION.ABRIR_CLAUSULADO",	respuesta);

										}
									}
					}
				]
			},{
				header: 		'Fecha Alta',
				tooltip: 	'Fecha Alta',
				dataIndex: 	'FECHA_ALTA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		250,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true
         },{
				header: 		'Activa',
				tooltip: 	'Activa',
				dataIndex: 	'ACTIVA',
				align:		'center',
				sortable: 	false,
				resizable: 	false,
				width: 		100,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value=="S"){
									return "<img src='/nafin/00utils/gif/paloma.gif' border='0'>";
								}else{
									return "";
								}
							}
         },{
				xtype:	'actioncolumn',
				header:	undefined,
				menuDisabled:true,
				dataIndex: '',	align: 'center',	sortable: true,	resizable: true,	width: 100,	hideable: false, hidden: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(	registro.get('MODIFICAR')	)	{
								this.items[0].tooltip = 'Modificar';
								return 'modificar';
							}else{
								return "";
							}
						},
						handler:	function(grid, rowIndex, colIndex, item, event)	{
										var record = registrosConsultadosData.getAt(rowIndex);
										var respuesta				=	new Object();
										respuesta["claveIf"]	=	record.json['CLAVE_IF'];
										respuesta["consecutivo"]=	record.json['CONSECUTIVO'];
										accionConsulta("ACCION.MODIFICAR",	respuesta);
									}
					}
				]
			}
		]
	});

	var fpConsultar = new Ext.form.FormPanel({
		id:				'formaConsultar',
		title:			'<div align="center">Clausulado Electr�nico</div>',
		width:			810,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			false,
		bodyStyle:		'padding: 10px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			[
			{
				xtype:			'combo',
				id:				'_ic_ifC',
				name:				'ic_if',
				hiddenName:		'ic_if',
				fieldLabel:		'IF',
				emptyText:		'Seleccione IF',
				displayField:	'descripcion',
				valueField:		'clave',
				triggerAction:	'all',
				forceSelection:true,
				typeAhead:		true,
				allowBlank:		false,
				mode:				'local',
				minChars:		1,
				store:			catalogoIfConsultaData,
				tpl:				NE.util.templateMensajeCargaCombo
			}
		],
		monitorValid:	true,
		buttonAlign:	'center',
		buttons: [
			{
				text:		'Consultar',
				id:		'btnConsultar',
				iconCls:	'icoBuscar',
				width:	100,
				formBind:true,
				handler: function(boton, evento) {

								if(!Ext.getCmp('formaConsultar').getForm().isValid()){
									return;
								}

								accionConsulta("CONSULTA_FORMA_CONSULTAR",null);

				} //fin handler
			},{
				text:			'Aux...',
				id:			'btnAux',
				urlArchivo:	'',
				hidden:		'true'
			}
		]
	});

	var gridCaptura = new Ext.grid.GridPanel({
		store: 		registrosCapturadosData,
		id:			'gridCaptura',
		style: 		'margin: 0 auto; padding-top:10px;',
		hidden: 		true,
		margins:		'20 0 0 0',
		title:		undefined,
		view:			new Ext.grid.GridView({forceFit:	true}),
		stripeRows: true,
		loadMask: 	true,
		height: 		120,
		width: 		810,
		frame: 		true,
		buttonAlign:'center',
		columns: [
			{
				header: 		'IF',
				tooltip: 	'IF',
				dataIndex: 	'NOMBRE_IF',
				sortable: 	true,
				resizable: 	true,
				width: 		400,
				hidden: 		false,
				hideable:	false,
				menuDisabled:true,
				renderer:	function( value, metadata, record, rowIndex, colIndex, store){
									metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
									return value;
								}
			},{
				xtype:	'actioncolumn',
				header:	'Documento', tooltip: 'Documento',
				menuDisabled:true,
				dataIndex: 'DOCUMENTO',	align: 'center',	sortable: true,	resizable: true,	width: 100,	hideable: false, hidden: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						},
						handler:	function(grid, rowIndex, colIndex) {

										var record = registrosCapturadosData.getAt(rowIndex);
										Ext.getCmp('btnAux').setHandler(
											function(btn, event){
												var forma 		= Ext.getDom('formAux');
												forma.action 	= record.json['DOCUMENTO'];
												forma.submit();
											}
										);
										Ext.getCmp("btnAux").handler.call(Ext.getCmp("btnAux").scope);
									}
					}
				]
			},{
				header: 		'Fecha Alta',
				tooltip: 	'Fecha Alta',
				dataIndex: 	'FECHA_ALTA',
				align:		'center',
				menuDisabled:true,
				sortable: 	true,
				resizable: 	true,
				width: 		250,
				hidden: 		false,
				hideable:	false
         },{
				header: 		'Activa',
				tooltip: 	'Activa',
				dataIndex: 	'ACTIVA',
				align:		'center',
				sortable: 	false,
				resizable: 	false,
				menuDisabled:true,
				width: 		100,
				hidden: 		false,
				hideable:	false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value=="S"){
									return "<img src='/nafin/00utils/gif/paloma.gif' border='0'>";
								}else{
									return "";
								}
							}
			}
		],
		buttons:[
			{
				text:		'Regresar',
				iconCls:	'x-tbar-page-prev',
				width:	100,
				handler:	function(){
								accionConsulta("FIN",null);
							}
			}
		]
	});

	var fpAceptar = new Ext.form.FormPanel({
		id:				'formaAceptar',
		title:			undefined,
		width:			610,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		frame:			false,
		hidden:			true,
		labelWidth:		1,
		items:			[],
		tbar:{
			items:[
				{
					xtype:	'displayfield',
					html:		'<b>&nbsp;El clausulado electr�nico se muestra a continuaci�n. Por favor,<br> confirme los cambios.</b>'
				},'->',' ',{
					xtype:	'button',
					text:		'Aceptar',
					iconCls:	'icoAceptar',
					handler:	function(btn, event){
									accionConsulta("ACEPTA_CAPTURA",null);
								}
				},' ','-',' ',{
					xtype:	'button',
					text:		'Regresar',
					iconCls:	'x-tbar-page-prev',
					handler:	function(btn, event){
									accionConsulta("REGRESA_CAPTURA",null);
								}
				},' '
			]
		}
	});

	var elementosForma = [
		{
			xtype:			'combo',
			id:				'_ic_if',
			name:				'ic_if',
			hiddenName:		'ic_if',
			fieldLabel:		'IF',
			emptyText:		'Seleccione IF',
			displayField:	'descripcion',
			valueField:		'clave',
			triggerAction:	'all',
			forceSelection:true,
			typeAhead:		false,
			allowBlank:		false,
			style:			{boder: '0px'},
			mode:				'local',
			minChars:		1,
			store:			catalogoIfData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'label',
			id:				'labelIf',
			cls:				'x-form-item',
			fieldLabel:		'IF',
			hidden:			true
		},{
			xtype:			'textfield',
			fieldLabel:		'T�tulo Aviso',
			name:				'cg_titulo_aviso',
			id:				'_cg_titulo_aviso',
			allowBlank:		false,
			maxLength:		255
		},{
			xtype:			'textarea',
			fieldLabel:		'Contenido Aviso',
			name:				'cg_contenido_aviso',
			id:				'_cg_contenido_aviso',
			allowBlank:		false,
			maxLengthText:	'La longitud m�xima es de 4000 caracteres.',
			height:			100,
			maxLength:		4000
		},{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			fieldLabel: 'Bot�n Aviso',
			items: [
				{
					xtype:			'displayfield',
					value:			'Ir a '
				},{
					xtype:			'textfield',
					name:				'cg_boton_aviso',
					id:				'_cg_boton_aviso',
					width:			588,
					allowBlank:		false,
					maxLength:		50
				}
			]
		},{
			xtype:		'fileuploadfield',
			id:			'_bi_documento',
			allowBlank:	false,
			blankText:	'Debe seleccionar una ruta de archivo.',
			emptyText:	'Seleccione un archivo',
			fieldLabel:	'Documento',
			name:			'bi_documento',
			regex:		/^.+\.(?:(?:[dD][oO][cC][xX]?)|(?:[pP][pP][tT][xX]?)|(?:[pP][pP][sS][xX]?)|(?:[pP][dD][fF]))$/,
			regexText:	'El formato del archivo no es v�lido. Favor de verificar.',
			buttonText: 'Examinar...'
		},{
			xtype:			'textfield',
			fieldLabel:		'Texto de aceptaci�n',
			name:				'cg_texto_aceptacion',
			id:				'_cg_texto_aceptacion',
			allowBlank:		false,
			maxLength:		400
		},{
			xtype:			'textfield',
			fieldLabel:		'Bot�n de aceptaci�n',
			name:				'cg_boton_aceptacion',
			id:				'_cg_boton_aceptacion',
			allowBlank:		false,
			maxLength:		50
		},{
			xtype:			'checkbox',
			fieldLabel:		'Mostrar clausulado electr�nico',
			name:				'cs_mostrar',
			id:				'_cs_mostrar',
			value:			'S',
			boxLabel:		undefined
		},{
			xtype:			'hidden',
			id:				'_tipoRegistro',
			name:				'tipoRegistro',
			value:			'NUEVO'
		},{
			xtype:			'hidden',
			id:				'_ic_consecutivo',
			name:				'ic_consecutivo',
			value:			null		
		},{
			xtype:			'hidden',
			id:				'_doctoActualizado',
			name:				'doctoActualizado',
			value:			'N'
		},{
			xtype:			'hidden',
			id:				'_nombreArchivo',
			name:				'nombreArchivo',
			value:			null
		}
	];

	var fpCapturar = new Ext.form.FormPanel({
		id:				'formaCapturar',
		title:			'<div align="center">Clausulado Electr�nico</div>',
		width:			810,
		style:			'margin:0 auto;',
		collapsible:	false,
		titleCollapse:	false,
		fileUpload:		true,
		frame:			false,
		//bodyStyle:		'padding: 10px',
		labelWidth:		150,
		bodyStyle:		'padding-left:40px;,padding-top:10px;,text-align:left',
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		monitorValid:	true,
		buttonAlign:	'center',
		buttons: [
			{
				text:		'Aceptar',
				id:		'btnAceptar',
				iconCls:	'icoContinuar',
				width:	100,
				formBind:true,
				handler: function(boton, evento) {

								if(!Ext.getCmp('formaCapturar').getForm().isValid()){
									return;
								}
								if(	Ext.getCmp('_tipoRegistro').getValue() == "ACTUALIZAR"	){
									if(	!Ext.isEmpty(	Ext.getCmp('_bi_documento').getValue()	)	){
										Ext.getCmp('_doctoActualizado').setValue("S");
									}else{
										Ext.getCmp('_doctoActualizado').setValue("N");
									}
								}

								accionConsulta("ACEPTAR_FORMA_CAPTURAR",null);

				} //fin handler
			},{
				text:		'Limpiar',
				id:		'btnLimpiar',
				iconCls:	'icoLimpiar',
				width:	100,
				handler:	function() {
								accionConsulta("LIMPIAR_CAPTURA",null);
							}
			},{
				text:		'Cancelar',
				id:		'btnCancelar',
				iconCls:	'icoRechazar',
				width:	100,
				hidden:	true,
				handler: function(boton, evento) {
								window.location = "13forma18Ext.jsp?tabActiva=1";
				} //fin handler
			}
		]
	});

	var tp = new Ext.TabPanel({
		id:			'tp',
		activeTab:	(Ext.isEmpty(tabActiva))?0:1,
		width:		940,
		plain:		true,
		defaults:	{autoHeight: true},
		items:[
			{
				title:	'Capturar',
				id:		'tabCapturar',
				//style:	'margin:0 auto;',
				items: [
					NE.util.getEspaciador(10),
					fpCapturar,fpAceptar,
					NE.util.getEspaciador(10),
					gridCaptura,
					NE.util.getEspaciador(20)
				]
			},{
				title:	'Consultar',
				id:		'tabConsultar',
				style:	'margin:0 auto;',
				items: [
					NE.util.getEspaciador(10),
					fpConsultar,
					NE.util.getEspaciador(10),
					gridConsulta,
					NE.util.getEspaciador(10)
				]
			}
		],
		listeners:{
			tabchange:	function(tab, panel){
								if(Ext.getCmp('_tipoRegistro').getValue() == "NUEVO"	){
									if (panel.getItemId() == 'tabCapturar') {
											accionConsulta("LIMPIAR_CAPTURA",null);
									}else if (panel.getItemId() == 'tabConsultar') {
										Ext.getCmp('formaConsultar').getForm().reset();
										gridConsulta.hide();
									}
								}
							}
		}
	});

//----------------------------------- CONTENEDOR -------------------------------------	

	var contenedorPrincipal = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	940,
		renderHidden:	true,
		height: 	'auto',
		items: 	[tp],
		ocultaMascaras: function(){

			var element = null;

         // Ocultar mascara del panel de la forma de carga de plantillas
         element = Ext.getCmp("formaCapturar").getEl();
			if(Ext.getCmp("formaCapturar").isVisible()){
				if( element.isMasked()){
					element.unmask();
				}
			}

         element = Ext.getCmp('formaAceptar').getEl();
			if(Ext.getCmp("formaAceptar").isVisible()){
				if( element.isMasked()){
					element.unmask();
				}
			}

         element = Ext.getCmp("formaConsultar").getEl();
			if(Ext.getCmp("formaConsultar").isVisible()){
				if( element.isMasked()){
					element.unmask();
				}
			}

			element = Ext.getCmp("tp").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			// Derefenrencia ultimo elemento...
			element = null;

		}

	});

	Ext.StoreMgr.key('catalogoIfDataStore').load();
	Ext.StoreMgr.key('catalogoIfConsultaDataStore').load();


});