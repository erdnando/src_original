Ext.onReady(function(){


//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});
	
//------------------------Info-Arryas de grids----------------------------------
var listaTitulos=["Todos","Negociables","Seleccionada PYME","En Proceso de Autorizaci�n IF"
,"Seleccionada IF","En Proceso","Operada","Operado con Fondeo Propio",
"Operada Pagada","Programado PyME"];

var arrayEstatusSelect=0;
var listaStores=new Array(9);
var listaGrids=new Array(9);
//-----FUNCION RENDERER DE PORCENTAJES O N/A--------------------------------------------
var renderPorcentajeNA= function (value, metaData, record, rowIndex, colIndex, store)
{
	if(value=='N/A')
	return value;
	var res=Ext.util.Format.numberRenderer('0.00000%');
	return res(value);
};
var renderPesosNA= function (value, metaData, record, rowIndex, colIndex, store)
{
	if(value=='N/A')
	return value;
	var res= Ext.util.Format.numberRenderer('$0,0.00');
	return res(value);
};
//------------------------funcion generadora de Stores para grids---------------------------

var nuevoStoreGrid =  function(campos ,procesarConsultaData ) {
 return new Ext.data.JsonStore({
	root: 'registros',
	url:'13reporte1Ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'		
	},
	fields:campos,
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
	
		beforeLoad:	{fn: function(store, options){
						options.params=Ext.apply(options.params,fp.getForm().getValues());
						}},
	
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							
					}
		}
	}
});
}
///////////////////////////////////////////////////////////////////////////////
//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -
	function procesaValoresIniciales(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if(jsonValoresIniciales != null){	
				var strNombreUsuario = Ext.getCmp('strNombreUsuario');
				var fechaHora = Ext.getCmp('fechaHora');
				strNombreUsuario.getEl().update('Usuario: '+jsonValoresIniciales.strNombreUsuario);
				fechaHora.getEl().update('Fecha: '+jsonValoresIniciales.fechaHora);	
				fp.el.unmask();
				catalogoEstatus.load();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}
	
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var botonPDF = Ext.getCmp('btnGenerarPDF'+arrayEstatusSelect);	
			if (botonPDF)
				botonPDF.enable();
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
		} else {
			//btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
function procesarArchivoSuccess(opts, success, response) {

		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var boton = Ext.getCmp('btnGenerarArchivo'+arrayEstatusSelect);
			if(boton)
			boton.enable();
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarConsultaData = function(store, arrRegistros, opts) {
		fp.el.unmask();
		
		var indexSel=store.reader.jsonData.index_estatus;
		var grid=listaGrids[indexSel];
		 var boton = Ext.getCmp('btnGenerarArchivo'+indexSel);
		 var botonPDF = Ext.getCmp('btnGenerarPDF'+indexSel);
		 
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var el = grid.getGridEl();
			el.unmask();
			if(store.getTotalCount() > 0) {
				
				/////////////////////////////////////////////////////////////////////
				consuTotalesData.load({
					params:	Ext.apply(fp.getForm().getValues(), 
					{
						informacion: 'ResumenTotales',
						indiceEst:arrayEstatusSelect,
						banderaFISO:'N'
					}
				)});
				
				
				if (boton)
				boton.enable();
				if (botonPDF)
				botonPDF.enable();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				if (boton)
				boton.disable();
				if (botonPDF)
				botonPDF.disable();
				
			}
		}
	}

var peticionExcel=function(){

var boton = Ext.getCmp('btnGenerarArchivo'+arrayEstatusSelect);
if(boton)
boton.disable();
	
		 
	Ext.Ajax.request({
		url: '13reporte1Ext.data.jsp',
		params: Ext.apply(fp.getForm().getValues(),{
		informacion: 'Consultar',
		indiceEst:arrayEstatusSelect,
		operacion:'ArchivoCSV'
		})
		, callback: procesarArchivoSuccess
		});
}
var peticionPdf=function(){
	var botonPDF = Ext.getCmp('btnGenerarPDF'+arrayEstatusSelect);	
	if (botonPDF)
	botonPDF.disable();
	
	Ext.Ajax.request({
		url: '13reporte1Ext.data.jsp',
		params: Ext.apply(fp.getForm().getValues(),{
			informacion: 'Consultar',
			indiceEst:arrayEstatusSelect,
			operacion:'ArchivoPDF'
		}),
		callback: procesarSuccessFailureGenerarPDF											  
	});
}
/////////////////////////////////////////////////////////////////////////////////77

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 
	var catalogoEstatus = new Ext.data.JsonStore({
		root:				'registros',
		fields:			['clave', 'descripcion', 'loadMsg'],
		url:				'13reporte1Ext.data.jsp',
		baseParams:		{	informacion:	'CatalogoEstatus'	},
		totalProperty:	'total',
		autoLoad:		false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
/////////////////////////////////////////////////////////////////////////////
//////////STORES DE TODOS LOS GRIDS//////////////////////////////////////////
var negociablesStoreGridData =nuevoStoreGrid(
				[{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NUMERODOCTO'},
				{name: 'ACUSE1'},
				{name: 'FECHADOCTO'},
				{name: 'FECHAVENC'},
				{name: 'NOMBREMONEDA'},
				{name: 'NUMEROMONEDA'},
				{name: 'MONTODOCTO'},
				{name: 'AFORO'},
				{name: 'TIPOFACTORAJE'},
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'NOMBREIF'},
				{name: 'BENEFICIARIO'},
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEBENEFICIARIO'}]
				,procesarConsultaData);
			listaStores[0]=negociablesStoreGridData;
var seleccionadaPYMEStoreGridData=nuevoStoreGrid(
				[{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NUMERODOCTO'},
				{name: 'FECHADOCTO'},
				{name: 'FECHAVENC'},
				{name: 'NOMBREIF'},
				{name: 'TASAACEPTADA'},
				{name: 'PLAZO'},
				{name: 'NOMBREMONEDA'},
				{name: 'NUMEROMONEDA'},
				{name: 'MONTODOCTO'},
				{name: 'PORCANTICIPO'},
				{name: 'MONTODSCTO'},
				{name: 'IMPORTEINTERES'},
				{name: 'IMPORTERECIBIR'},				
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'TIPOFACTORAJE'},
				{name: 'DF_PROGRAMACION'},
				{name: 'TIPOTASA'},
				{name: 'IMPORTENETO'},
				{name: 'BENEFICIARIO'},
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEBENEFICIARIO'},
				{name: 'NOMBRE_FID'},
				{name: 'TASAACEPTADA_FID'},
				{name: 'IMPORTEINTERES_FID'},
				{name: 'IMPORTERECIBIR_FID'}]
				,procesarConsultaData);
			listaStores[1]=seleccionadaPYMEStoreGridData;
var enProcesoAutorizacionIFStoreGridData=nuevoStoreGrid(
				[{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NUMERODOCTO'},
				{name: 'FECHADOCTO'},
				{name: 'FECHAVENC'},
				{name: 'NOMBREIF'},
				{name: 'TASAACEPTADA'},
				{name: 'PLAZO'},
				{name: 'NOMBREMONEDA'},
				{name: 'NUMEROMONEDA'},
				{name: 'MONTODOCTO'},
				{name: 'PORCANTICIPO'},
				{name: 'MONTODSCTO'},
				{name: 'IMPORTEINTERES'},
				{name: 'IMPORTERECIBIR'},				
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'TIPOFACTORAJE'},
				{name: 'DF_PROGRAMACION'},
				{name: 'TIPOTASA'},
				{name: 'IMPORTENETO'},
				{name: 'BENEFICIARIO'},
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEBENEFICIARIO'},
				{name: 'NOMBRE_FID'},
				{name: 'TASAACEPTADA_FID'},
				{name: 'IMPORTEINTERES_FID'},
				{name: 'IMPORTERECIBIR_FID'}]
				,procesarConsultaData);
			listaStores[2]=enProcesoAutorizacionIFStoreGridData;
var seleccionadaIFStoreGridData=nuevoStoreGrid(
				[{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NOMBREIF'},
				{name: 'NUMERODOCTO'},
				{name: 'NUMEROSOLICITUD'},
				{name: 'NOMBREMONEDA'},
				{name: 'NUMEROMONEDA'},
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'MONTODOCTO'},
				{name: 'PORCANTICIPO'},				
				{name: 'MONTODSCTO'},				
				{name: 'TASAACEPTADA'},				
				{name: 'IMPORTEINTERES'},
				{name: 'IMPORTERECIBIR'},				
				{name: 'TIPOFACTORAJE'},				
				{name: 'ACUSE3'},
				{name: 'TIPOTASA'},
				{name: 'IMPORTENETO'},
				{name: 'BENEFICIARIO'},
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEBENEFICIARIO'},
				{name: 'NOMBRE_FID'},
				{name: 'TASAACEPTADA_FID'},
				{name: 'IMPORTEINTERES_FID'},
				{name: 'IMPORTERECIBIR_FID'}]
				,procesarConsultaData);
			listaStores[3]=seleccionadaIFStoreGridData;
var enProcesoStoreGridData=nuevoStoreGrid(
				[{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NOMBREIF'},
				{name: 'NUMERODOCTO'},
				{name: 'NUMEROSOLICITUD'},
				{name: 'NOMBREMONEDA'},
				{name: 'NUMEROMONEDA'},
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'MONTODOCTO'},
				{name: 'PORCANTICIPO'},				
				{name: 'MONTODSCTO'},				
				{name: 'TASAACEPTADA'},				
				{name: 'IMPORTEINTERES'},
				{name: 'IMPORTERECIBIR'},				
				{name: 'TIPOFACTORAJE'},				
				{name: 'ACUSE3'},
				{name: 'TIPOTASA'},
				{name: 'IMPORTENETO'},
				{name: 'BENEFICIARIO'},
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEBENEFICIARIO'},
				{name: 'NOMBRE_FID'},
				{name: 'TASAACEPTADA_FID'},
				{name: 'IMPORTEINTERES_FID'},
				{name: 'IMPORTERECIBIR_FID'}]
				,procesarConsultaData);
			listaStores[4]=enProcesoStoreGridData;
var operadaStoreGridData=nuevoStoreGrid(
				[{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NOMBREIF'},
				{name: 'NUMERODOCTO'},
				{name: 'NUMEROSOLICITUD'},
				{name: 'NOMBREMONEDA'},
				{name: 'NUMEROMONEDA'},
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'MONTODOCTO'},
				{name: 'PORCANTICIPO'},				
				{name: 'MONTODSCTO'},				
				{name: 'TASAACEPTADA'},				
				{name: 'IMPORTEINTERES'},
				{name: 'IMPORTERECIBIR'},				
				{name: 'TIPOFACTORAJE'},				
				{name: 'ACUSE3'},
				{name: 'TIPOTASA'},
				{name: 'IMPORTENETO'},
				{name: 'BENEFICIARIO'},
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEBENEFICIARIO'},
				{name: 'NOMBRE_FID'},
				{name: 'TASAACEPTADA_FID'},
				{name: 'IMPORTEINTERES_FID'},
				{name: 'IMPORTERECIBIR_FID'}]
				,procesarConsultaData);
			listaStores[5]=operadaStoreGridData;
var operadoFondeoPropioStoreGridData=nuevoStoreGrid(
				[{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NOMBREIF'},
				{name: 'NUMERODOCTO'},
				{name: 'NUMEROSOLICITUD'},
				{name: 'NOMBREMONEDA'},
				{name: 'NUMEROMONEDA'},
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'MONTODOCTO'},
				{name: 'PORCANTICIPO'},				
				{name: 'MONTODSCTO'},				
				{name: 'TASAACEPTADA'},				
				{name: 'IMPORTEINTERES'},
				{name: 'IMPORTERECIBIR'},				
				{name: 'TIPOFACTORAJE'},				
				{name: 'ACUSE3'},
				{name: 'TIPOTASA'},
				{name: 'IMPORTENETO'},
				{name: 'BENEFICIARIO'},
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEBENEFICIARIO'}]
				,procesarConsultaData);
			listaStores[6]=operadoFondeoPropioStoreGridData;
var operadaPagadaStoreGridData=nuevoStoreGrid(
				[{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NOMBREIF'},
				{name: 'NUMERODOCTO'},
				{name: 'NUMEROSOLICITUD'},
				{name: 'NOMBREMONEDA'},
				{name: 'NUMEROMONEDA'},
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'MONTODOCTO'},
				{name: 'PORCANTICIPO'},				
				{name: 'MONTODSCTO'},				
				{name: 'TASAACEPTADA'},				
				{name: 'IMPORTEINTERES'},
				{name: 'IMPORTERECIBIR'},				
				{name: 'TIPOFACTORAJE'},				
				{name: 'FECHAVENC'},
				{name: 'ACUSE3'},
				{name: 'TIPOTASA'},
				{name: 'IMPORTENETO'},
				{name: 'BENEFICIARIO'},
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEBENEFICIARIO'},
				{name: 'NOMBRE_FID'},
				{name: 'TASAACEPTADA_FID'},
				{name: 'IMPORTEINTERES_FID'},
				{name: 'IMPORTERECIBIR_FID'}]
				,procesarConsultaData);
			listaStores[7]=operadaPagadaStoreGridData;
var programadoPyMEStoreGridData=nuevoStoreGrid(
				[{name: 'NOMBREEPO'},
				{name: 'NOMBREPYME'},
				{name: 'NUMERODOCTO'},
				{name: 'FECHADOCTO'},
				{name: 'FECHAVENC'},
				{name: 'NOMBREIF'},
				{name: 'TASAACEPTADA'},
				{name: 'PLAZO'},
				{name: 'NOMBREMONEDA'},
				{name: 'NUMEROMONEDA'},
				{name: 'MONTODOCTO'},
				{name: 'PORCANTICIPO'},
				{name: 'MONTODSCTO'},
				{name: 'IMPORTEINTERES'},
				{name: 'IMPORTERECIBIR'},				
				{name: 'NOMBRE_TIPO_FACTORAJE'},
				{name: 'TIPOFACTORAJE'},
				{name: 'DF_PROGRAMACION'},
				{name: 'TIPOTASA'},
				{name: 'IMPORTENETO'},
				{name: 'BENEFICIARIO'},
				{name: 'PORCBENEFICIARIO'},
				{name: 'IMPORTEBENEFICIARIO'}]
				,procesarConsultaData);
			listaStores[8]=programadoPyMEStoreGridData;


	
	//------Totales Normales  ------------------------
	var procesarTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}	  
			
			var jsonData = store.reader.jsonData;	
			var cm = gridTotales.getColumnModel();  			
			var estatus1 =jsonData.estatus1;		
					
					
			if(estatus1 ==0  ){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), true);							
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESC'), false);	
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPER'), true);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_REC_GAR'), true);
			}
			
			if(estatus1 ==1  ){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), false);							
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESC'), false);	
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPER'), false);
			}			
			if( estatus1 ==2 ||estatus1 ==3 ||estatus1 ==4  || estatus1 ==6){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), false);							
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESC'), false);	
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPER'), false);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_REC_GAR'), true);										
			}
			if( estatus1 ==5 ){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), true);							
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESC'), false);	
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPER'), false);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_REC_GAR'), true);										
			}			
			if( estatus1 ==7 ){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INT'), true);							
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESC'), false);	
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPER'), true);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_REC_GAR'), true);									
			}			
			
			if( estatus1 ==1  ||  estatus1 ==2  ||  estatus1 ==3   ||  estatus1 ==4    ||  estatus1 ==5  ||   estatus1 ==7 )  {				
				consuTotalesDataFid.load({
					params:	Ext.apply(fp.getForm().getValues(), 
					{
						informacion: 'ResumenTotales_FISO',
						indiceEst:arrayEstatusSelect,
						banderaFISO:'S'
					}
				)});
			}					
			
			if(store.getTotalCount() > 0) {			
				el.unmask();					
			} else {	
				gridTotales.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	
	var consuTotalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte1Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'		
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'NUM_DOCTOS', type: 'float', mapping: 'NUM_DOCTOS'},			
			{name: 'TOTAL_MONTO_DOCTO', type: 'float', mapping: 'TOTAL_MONTO_DOCTO'},	
			{name: 'TOTAL_REC_GAR', type: 'float', mapping: 'TOTAL_REC_GAR'},
			{name: 'MONTO_INT', type: 'float', mapping: 'MONTO_INT'},
			{name: 'MONTO_DESC', type: 'float', mapping: 'MONTO_DESC'},
			{name: 'MONTO_OPER', type: 'float', mapping: 'MONTO_OPER'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}		
	});
	
	
		//------Totales Normales  ------------------------
	var procesarTotalesFISOData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridTotalesFid = Ext.getCmp('gridTotalesFid');	
		var el = gridTotalesFid.getGridEl();	
		if (arrRegistros != null) {
			if (!gridTotalesFid.isVisible()) {
				gridTotalesFid.show();
			}	  
			
			var jsonData = store.reader.jsonData;	
			var cmFid = gridTotalesFid.getColumnModel();  
			
			var estatus1 =jsonData.estatus1;		
					
			if(estatus1 ==5 ) 	{
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_INT_FISO'), true);							
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_INT'), true);					
				}	else	if(estatus1 ==7 )	{
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_INT_FISO'), true);							
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_INT'), true);
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_OPER'), true);							
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_OPER_FISO'), true);					
				}else	{
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_INT_FISO'), false);							
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_INT'), false);	
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_OPER'), false);							
					gridTotalesFid.getColumnModel().setHidden(cmFid.findColumnIndex('MONTO_OPER_FISO'),false);					
				}
			}
			
			
			if(store.getTotalCount() > 0) {			
				el.unmask();					
			} else {		
				gridTotalesFid.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	
		
	//totales fideicomiso
	
	var consuTotalesDataFid = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte1Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales_FISO'		
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'NUM_DOCTOS', type: 'float', mapping: 'NUM_DOCTOS'},			
			{name: 'TOTAL_MONTO_DOCTO', type: 'float', mapping: 'TOTAL_MONTO_DOCTO'},	
			{name: 'TOTAL_REC_GAR', type: 'float', mapping: 'TOTAL_REC_GAR'},
			{name: 'MONTO_INT', type: 'float', mapping: 'MONTO_INT'},
			{name: 'MONTO_DESC', type: 'float', mapping: 'MONTO_DESC'},
			{name: 'MONTO_OPER', type: 'float', mapping: 'MONTO_OPER'},
			{name: 'MONTO_INT_FISO', type: 'float', mapping: 'MONTO_INT_FISO'},
			{name: 'MONTO_OPER_FISO', type: 'float', mapping: 'MONTO_OPER_FISO'}		
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesFISOData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesFISOData(null, null, null);						
					}
				}
			}		
	});
	
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consuTotalesData,	
		style: 'margin:0 auto;',
		title:'Totales Operaci�n Normal',
		hidden: true,		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'			
			},						
			{
				header: 'Num. Documentos',
				dataIndex: 'NUM_DOCTOS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Documento',
				dataIndex: 'TOTAL_MONTO_DOCTO',
				width: 150,
				align: 'right',
				renderer: renderPesosNA
			},
			{
				header: 'Recurso en Garant�a',
				dataIndex: 'TOTAL_REC_GAR',
				width: 150,
				align: 'right',
					hidden: true,	
				renderer: renderPesosNA
			},
			{
				header: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				width: 150,
				align: 'right',
				renderer:renderPesosNA
			},
			{
				header: 'Monto Int.',
				dataIndex: 'MONTO_INT',
				width: 150,
				align: 'right',
				renderer:renderPesosNA
			},
			{
				header: 'Monto a Operar',
				dataIndex: 'MONTO_OPER',
				width: 150,
				align: 'right',
				renderer: renderPesosNA
			}
		],
		height: 150,
		frame: true
	});

//////////////////TOTALES PARA EL FIDEICOMISO/////////////////////////
function  creaGrupoHeaderTotales (tipo){
var rows;

rows= [
			[
				{header: '', colspan: 5, align: 'center'},
				{header: 'INTERMEDIARIO FINANCIERO', colspan: 2, align: 'center'},
				{header: 'FIDEICOMISO', colspan: 2, align: 'center'}
			]
		];		

return new Ext.ux.grid.ColumnHeaderGroup({
		rows:rows
	})
};
var groupHeaderTotal=creaGrupoHeaderTotales("TODO");

var gridTotalesFid = new Ext.grid.GridPanel({
		id: 'gridTotalesFid',				
		store: consuTotalesDataFid,	
		style: 'margin:0 auto;',
		title:'Totales Operacion Fideicomiso',
		plugins:groupHeaderTotal,
		hidden: true,		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'			
			},						
			{
				header: 'Num. Documentos',
				dataIndex: 'NUM_DOCTOS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Documento',
				dataIndex: 'TOTAL_MONTO_DOCTO',
				width: 150,
				align: 'right',
				renderer:renderPesosNA
			},
			{
				header: 'Recurso en Garant�a',
				dataIndex: 'TOTAL_REC_GAR',
				width: 150,
				align: 'right',
					hidden: true,	
				renderer: renderPesosNA
			},
			{
				header: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				width: 150,
				align: 'right',
				renderer: renderPesosNA
			},
			{
				header: 'Monto Int.',
				dataIndex: 'MONTO_INT',
				width: 150,
				align: 'right',
				renderer: renderPesosNA
			},
			{
				header: 'Monto a Operar',
				dataIndex: 'MONTO_OPER',
				width: 150,
				align: 'right',
				renderer: renderPesosNA
			},
			{
				header: 'Monto Int.',
				dataIndex: 'MONTO_INT_FISO',
				width: 150,
				align: 'right',
				renderer: renderPesosNA
			},
			{
				header: 'Monto a Operar',
				dataIndex: 'MONTO_OPER_FISO',
				width: 150,
				align: 'right',
				renderer:renderPesosNA
			}
		],
		height: 150,
		//width: 610,
		frame: true
	});

///////////////////////////////////////////////////////////////

var negociablesGrid = new Ext.grid.GridPanel({
				id: 'negociables_grid',
				hidden: true,
				store: negociablesStoreGridData,
				style: 'margin:0 auto;',
				title:'negociablesGrid',
				columns:[
				//CAMPOS DEL GRID
						{
						header: 'Nombre de EPO',
						tooltip: 'Nombre de EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Nombre Proveedor',
						tooltip: 'Nombre Proveedor',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 150,
						align: 'left'
						},
						{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMERODOCTO',
						width: 130,
						align: 'center'						
						},
						{
						align:'center',
						header: 'Fecha Documento',
						tooltip: 'Fecha Documento',
						sortable: true,
						dataIndex: 'FECHADOCTO',
						width: 130
						},{
						align:'center',
						header:'Fecha Vencimiento',
						tooltip: 'Fecha Vencimiento',
						sortable: true,
						dataIndex: 'FECHAVENC',
						width: 130						
						},
						{						
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 150,
						align: 'center'
						},
						{
						header: 'Tipo Factoraje',
						tooltip: 'Tipo Factoraje',
						sortable: true,
						dataIndex: 'NOMBRE_TIPO_FACTORAJE',
						width: 130,
						align: 'center'
						},{
						header: 'Monto Documento',
						tooltip: 'Monto Documento',
						sortable: true,
						dataIndex: 'MONTODOCTO',
						width: 130,
						align: 'right',
						renderer:renderPesosNA
						},
						{
						header: 'Porcentaje de Descuento',
						tooltip: 'Porcentaje de Descuento',
						sortable: true,
						dataIndex: 'AFORO',
						width: 150,
						align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
							var por=record.data.AFORO;
							var tipoFactoraje=record.data.TIPOFACTORAJE;
							if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
							tipoFactoraje=="C" || tipoFactoraje=="I" ) {
								por = 100;
							}
							return por +'%';
						}
						},
						{
						header: 'Recurso en Garant�a',
						tooltip: 'Recurso en Garant�a',
						sortable: true,
						//dataIndex: 'IMPORTEDSCTO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODOCTO * (record.data.AFORO / 100);
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="C" || tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(record.data.MONTODOCTO-dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto a Descontar',
						tooltip: 'Monto a Descontar',
						sortable: true,
						align: 'right',
						//dataIndex: 'NUMFOLIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODOCTO * (record.data.AFORO / 100);
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="C" || tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'N�mero de Acuse',
						tooltip: 'N�mero de Acuse',
						sortable: true,
						dataIndex: 'ACUSE1',
						width: 150,
						align: 'center'
						},
						{
						header: 'Nombre IF',
						tooltip: 'Nombre IF',
						sortable: true,
						align: 'left',
						dataIndex: 'NOMBREIF',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var nomIF = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="C" || tipoFactoraje=="I" || tipoFactoraje=="A") {
									 nomIF = record.data.NOMBREIF;
								}
								return nomIF;
							}
						},
						{
						header: 'Beneficiario',
						tooltip: 'Beneficiario',
						sortable: true,
						dataIndex: 'BENEFICIARIO',
						width: 150,
						align: 'center'
						},
						{
						header: '% Beneficiario',
						tooltip: '% Beneficiario',
						sortable: true,
						align: 'center',
						//dataIndex: 'PORCBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.PORCBENEFICIARIO;
								}
								return porcentajeb;
							}
						
						},
						{
						header: 'Importe a Recibir Beneficiario',
						tooltip: 'Importe a Recibir Beneficiario',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTEBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var imporb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 imporb = record.data.IMPORTEBENEFICIARIO;
									 imporb='$ '+ Ext.util.Format.number(imporb,'0,000.00');
								}
								return  imporb;
							}
						}				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {
				buttonAlign: 'left',
					items: [	'->','-',								
								{
									xtype:	'button',
									text:		'Generar PDF',
									id: 		'btnGenerarPDF0',
									iconCls:	'icoPdf',
									handler: function(boton, evento) {
										peticionPdf();
									}
								}
						]
				}
		});
		
		listaGrids[0]=negociablesGrid;
///////////////////////////////////////////////////////////////////////////////////		

function  creaGrupoHeaderPymeSelec (tipo){
var rows;

rows= [
			[
				{header: '', colspan: 12, align: 'center'},
				{header: 'INTERMEDIARIO FINANCIERO', colspan: 4, align: 'center',id:'ifG'},
				{header: 'FIDEICOMISO', colspan: 4, align: 'center'},
				{header: '', colspan: 6, align: 'center'}
			]
		];		

return new Ext.ux.grid.ColumnHeaderGroup({
		rows:rows
	})
};
function creaGridPymeSel(tipo)
{
	var ifCol=new Array({},{},{},{});fidCol=new Array({},{},{},{});
	if(tipo=="IF" || tipo=="TODO" )
	{
		ifCol=new Array(
			{						
						header: 'Intermediario Financiero',
						tooltip: 'Intermediario Financiero',
						sortable: true,
						dataIndex: 'NOMBREIF',
						id:'py_NOMBREIF',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa',
						tooltip: 'Tasa',
						sortable: true,
						dataIndex: 'TASAACEPTADA',
						id:'py_TASAACEPTADA',
						width: 130,
						align: 'center',
						renderer: renderPorcentajeNA
						},
						{
						header: 'Monto Int.',
						tooltip: 'Monto Int.',
						sortable: true,
						dataIndex: 'IMPORTEINTERES',
						id: 'py_IMPORTEINTERES',
						width: 150,
						align: 'right',
						renderer: renderPesosNA
						}
						,
						{
						header: 'Monto a Operar',
						tooltip: 'Monto a Operar',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR',
						id: 'py_IMPORTERECIBIR',
						width: 150,
						align: 'right',
						renderer:renderPesosNA
						});
	}
	if(tipo=="FID" || tipo=="TODO")
	{
		fidCol=new Array(
						{						
						header: 'Nombre FIDEICOMISO',
						tooltip: 'Nombre FIDEICOMISO',
						sortable: true,
						dataIndex: 'NOMBRE_FID',
						width: 150,
						align: 'left',
						id:'py_NOMBRE_FID'
						},
						{
						header: 'Tasa FIDEICOMISO',
						tooltip: 'Tasa FIDEICOMISO',
						sortable: true,
						dataIndex: 'TASAACEPTADA_FID',
						id:'py_TASAACEPTADA_FID',
						width: 130,
						align: 'center',
						renderer:renderPorcentajeNA//(value, metaData, record, rowIndex, colIndex, store)
						//renderer: Ext.util.Format.numberRenderer('0.00000%')
						},
						{
						header: 'Monto Int. FIDEICOMISO',
						tooltip: 'Monto Int. FIDEICOMISO',
						sortable: true,
						dataIndex: 'IMPORTEINTERES_FID',
						id: 'py_IMPORTEINTERES_FID',
						width: 150,
						align: 'right',
						renderer:renderPesosNA
						}
						,
						{
						header: 'Monto a Operar FIDEICOMISO',
						tooltip: 'Monto a Operar FIDEICOMISO',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR_FID',
						id: 'py_IMPORTERECIBIR_FID',
						width: 150,
						align: 'right',
						renderer:renderPesosNA
						});
	}
	
	return new Ext.grid.GridPanel({
				id: 'seleccionadaPYME_grid',
				hidden: true,
				title:'SeleccionadaPYMEGrid',
				store: seleccionadaPYMEStoreGridData,
				style: 'margin:0 auto;',
				plugins: gruposPymeSelec,
				columns:[
				//CAMPOS DEL GRID
						{
						header: 'Nombre de EPO',
						tooltip: 'Nombre de EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Nombre PYME',
						tooltip: 'Nombre PYME',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 150,
						align: 'left'
						},
						{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMERODOCTO',
						width: 130,
						align: 'center'						
						},
						{
						align:'center',
						header: 'Fecha Documento',
						tooltip: 'Fecha Documento',
						sortable: true,
						dataIndex: 'FECHADOCTO',
						width: 130
						},{
						align:'center',
						header:'Fecha Vencimiento',
						tooltip: 'Fecha Vencimiento',
						sortable: true,
						dataIndex: 'FECHAVENC',
						width: 130						
						},{
						header: 'Plazo (d�as)',
						tooltip: 'Plazo (d�as)',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 130,
						align: 'center'
						},{
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},
						{
						header: 'Tipo Factoraje',
						tooltip: 'Tipo Factoraje',
						sortable: true,
						dataIndex: 'NOMBRE_TIPO_FACTORAJE',
						width: 130,
						align: 'center'
						},{
						header: 'Monto Documento',
						tooltip: 'Monto Documento',
						sortable: true,
						dataIndex: 'MONTODOCTO',
						width: 130,
						align: 'right',
						renderer: renderPesosNA
						},
						{
						header: 'Porcentaje de Descuento',
						tooltip: 'Porcentaje de Descuento',
						sortable: true,align: 'center',
						dataIndex: 'PORCANTICIPO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
							var por=record.data.PORCANTICIPO;
							var tipoFactoraje=record.data.TIPOFACTORAJE;
							if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
							 tipoFactoraje=="I" ) {
								por = 100;
							}
							return por +'%';
						}
						},
						{
						header: 'Recurso en Garant�a',
						tooltip: 'Recurso en Garant�a',
						sortable: true,
						//dataIndex: 'IMPORTEDSCTO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(record.data.MONTODOCTO-dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto a Descontar',
						tooltip: 'Monto a Descontar',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO ;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(dblMontoDescuento,'0,000.00');
							}
						},ifCol[0]?ifCol[0]:null,ifCol[1]?ifCol[1]:null,ifCol[2]?ifCol[2]:null,ifCol[3]?ifCol[3]:null,
						fidCol[0]?fidCol[0]:null,fidCol[1]?fidCol[1]:null,fidCol[2]?fidCol[2]:null,fidCol[3]?fidCol[3]:null,
						
						////////////////////FIDEICOMISO
						
						////////FIN FIDEICOMISO
						
						{
						header: 'Neto a Recibir PYME',
						tooltip: 'Neto a Recibir PYME',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var neto ='';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if( tipoFactoraje=="D" ||
								 tipoFactoraje=="I" ) {
									neto = record.data.IMPORTENETO;
									neto='$ '+ Ext.util.Format.number(neto,'0,000.00');
								}
								return neto;
							}
						},
						
						{
						header: 'Beneficiario',
						tooltip: 'Beneficiario',
						sortable: true,
						dataIndex: 'BENEFICIARIO',
						width: 150,
						align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.BENEFICIARIO;
								}
								return porcentajeb;
							}
						},
						{
						header: '% Beneficiario',
						tooltip: '% Beneficiario',
						sortable: true,
						align: 'center',
						//dataIndex: 'PORCBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.PORCBENEFICIARIO;
								}
								return porcentajeb;
							}
						
						},
						{
						header: 'Importe a Recibir Beneficiario',
						tooltip: 'Importe a Recibir Beneficiario',
						sortable: true,
						dataIndex: 'IMPORTEBENEFICIARIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var imporb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 imporb = record.data.IMPORTEBENEFICIARIO;
									 imporb='$ '+ Ext.util.Format.number(imporb,'0,000.00');
								}
								return  imporb;
							}
						},						
						{
						header: 'Fecha Registro Operaci�n',
						tooltip: 'Fecha Registro Operaci�n',
						sortable: true,
						dataIndex: 'DF_PROGRAMACION',
						width: 150,
						align: 'center'
						},						
						{
						header: 'Referencia Tasa',
						tooltip: 'Referencia Tasa',
						sortable: true,
						dataIndex: 'TIPOTASA',
						width: 150,
						align: 'center'
						}				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {					
					items: ['->','-',								
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo1',
									iconCls: 'icoXls',
									handler: function(boton, evento){
										peticionExcel();
									}
								},
								'-',
								{
									xtype:	'button',
									text:		'Generar PDF',
									id: 		'btnGenerarPDF1',
									iconCls:	'icoPdf',
									handler: function(boton, evento) {
										peticionPdf();
									}
								}
						]
				}
		});

}

var gruposPymeSelec = creaGrupoHeaderPymeSelec("TODO");
//Ext.getCmp('ifG').hide();
var SeleccionadaPYMEGrid =creaGridPymeSel("TODO"); 
		
			listaGrids[1]=SeleccionadaPYMEGrid;
/////////////////////////////////////////////////////////////////////////////

function  creaGrupoHeaderProcAutorizacionIF (tipo){
var rows;

rows= [
			[
				{header: '', colspan: 12, align: 'center'},
				{header: 'INTERMEDIARIO FINANCIERO', colspan: 4, align: 'center'},
				{header: 'FIDEICOMISO', colspan: 4, align: 'center'},
				{header: '', colspan: 5, align: 'center'}
			]
		];		

return new Ext.ux.grid.ColumnHeaderGroup({
		rows:rows
	})
};
var gruposProcAutorizacionIF = creaGrupoHeaderProcAutorizacionIF("TODO");
	var enProcesoAutorizacionIFGrid = new Ext.grid.GridPanel({
				id: 'enProcesoAutorizacionIF_grid',
				hidden: true,
				store: enProcesoAutorizacionIFStoreGridData,
				plugins: gruposProcAutorizacionIF,
				style: 'margin:0 auto;',
				title:'enProcesoAutorizacionIFGrid',
				columns:[
				//CAMPOS DEL GRID
						{
						header: 'Nombre de EPO',
						tooltip: 'Nombre de EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Nombre PYME',
						tooltip: 'Nombre PYME',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 150,
						align: 'left'
						},
						{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMERODOCTO',
						width: 130,
						align: 'center'						
						},
						{
						align:'center',
						header: 'Fecha Documento',
						tooltip: 'Fecha Documento',
						sortable: true,
						dataIndex: 'FECHADOCTO',
						width: 130
						},{
						
						header:'Fecha Vencimiento',
						tooltip: 'Fecha Vencimiento',
						sortable: true,
						dataIndex: 'FECHAVENC',
						width: 130	,
						align: 'center'
						},
						{
						header: 'Plazo (d�as)',
						tooltip: 'Plazo (d�as)',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 130,
						align: 'center'
						},{
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},{
						header: 'Tipo Factoraje',
						tooltip: 'Tipo Factoraje',
						sortable: true,
						dataIndex: 'NOMBRE_TIPO_FACTORAJE',
						width: 130,
						align: 'center'
						},{
						header: 'Monto Documento',
						tooltip: 'Monto Documento',
						sortable: true,
						dataIndex: 'MONTODOCTO',
						width: 130,
						align: 'right',
						renderer: renderPesosNA
						},
						{
						header: 'Porcentaje de Descuento',
						tooltip: 'Porcentaje de Descuento',
						sortable: true,
						align: 'center',
						dataIndex: 'PORCANTICIPO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
							var por=record.data.PORCANTICIPO;
							var tipoFactoraje=record.data.TIPOFACTORAJE;
							if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
							 tipoFactoraje=="I" ) {
								por = 100;
							}
							return por +'%';
						}
						},
						{
						header: 'Recurso en Garant�a',
						tooltip: 'Recurso en Garant�a',
						sortable: true,
						//dataIndex: 'IMPORTEDSCTO',
						align: 'right',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(record.data.MONTODOCTO-dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto a Descontar',
						tooltip: 'Monto a Descontar',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						align: 'right',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO ;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(dblMontoDescuento,'0,000.00');
							}
						},
						{						
						header: 'Intermediario Financiero',
						tooltip: 'Intermediario Financiero',
						sortable: true,
						dataIndex: 'NOMBREIF',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa IF',
						tooltip: 'Tasa IF',
						sortable: true,
						dataIndex: 'TASAACEPTADA',
						width: 130,
						align: 'center',
						renderer: renderPorcentajeNA
						},
						{
						header: 'Monto Int.IF',
						tooltip: 'Monto Int. IF',
						sortable: true,
						dataIndex: 'IMPORTEINTERES',
						width: 150,
						align: 'right',
						renderer:renderPesosNA
						}
						,
						{
						header: 'Monto a Operar IF',
						tooltip: 'Monto a OperarIF',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR',
						width: 150,
						align: 'right',
						renderer: renderPesosNA
						},
						{						
						header: 'Nombre FIDEICOMISO',
						tooltip: 'Intermediario FIDEICOMISO',
						sortable: true,
						dataIndex: 'NOMBRE_FID',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa FIDEICOMISO',
						tooltip: 'Tasa FIDEICOMISO',
						sortable: true,
						dataIndex: 'TASAACEPTADA_FID',
						width: 130,
						align: 'center',
						renderer:renderPorcentajeNA
						},
						{
						header: 'Monto Int.FIDEICOMISO',
						tooltip: 'Monto Int. FIDEICOMISO',
						sortable: true,
						dataIndex: 'IMPORTEINTERES_FID',
						width: 150,
						align: 'right',
						renderer: renderPesosNA
						}
						,
						{
						header: 'Monto a Operar FIDEICOMISO',
						tooltip: 'Monto a Operar FIDEICOMISO',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR_FID',
						width: 150,
						align: 'right',
						renderer:renderPesosNA
						},
						{
						header: 'Neto a Recibir PYME',
						tooltip: 'Neto a Recibir PYME',
						sortable: true,
						align: 'right',
						//dataIndex: 'NUMFOLIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var neto ='';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if( tipoFactoraje=="D" ||
								 tipoFactoraje=="I" ) {
									neto = record.data.IMPORTENETO;
									neto='$ '+ Ext.util.Format.number(neto,'0,000.00');
								}
								return neto;
							}
						},
						
						{
						header: 'Beneficiario',
						tooltip: 'Beneficiario',
						sortable: true,
						dataIndex: 'BENEFICIARIO',
						width: 150,
						align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.BENEFICIARIO;
								}
								return porcentajeb;
							}
						},
						{
						header: '% Beneficiario',
						tooltip: '% Beneficiario',
						sortable: true,
						align: 'center',
						//dataIndex: 'PORCBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.PORCBENEFICIARIO;
								}
								return porcentajeb;
							}
						
						},
						{
						header: 'Importe a Recibir Beneficiario',
						tooltip: 'Importe a Recibir Beneficiario',
						sortable: true,
						dataIndex: 'IMPORTEBENEFICIARIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var imporb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 imporb = record.data.IMPORTEBENEFICIARIO;
									 imporb='$ '+ Ext.util.Format.number(imporb,'0,000.00');
								}
								return  imporb;
							}
						},						
						{
						header: 'Referencia Tasa',
						tooltip: 'Referencia Tasa',
						sortable: true,
						dataIndex: 'TIPOTASA',
						width: 150,
						align: 'center'
						}				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {					
					items: ['->','-',								
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo2',
									iconCls: 'icoXls',
									handler: function(boton, evento){
										peticionExcel();
									}
								},
								'-',
								{
									xtype:	'button',
									text:		'Generar PDF',
									id: 		'btnGenerarPDF2',
									iconCls:	'icoPdf',
									handler: function(boton, evento) {
										peticionPdf();
									}
								}
						]
				}
		});
		
			listaGrids[2]=enProcesoAutorizacionIFGrid;
/////////////////////////////////////////////////////////////////////
function  creaGrupoHeaderSeleccionadaIFGrid(tipo){
var rows;

rows= [
			[
				{header: '', colspan: 10, align: 'center'},
				{header: 'INTERMEDIARIO FINANCIERO', colspan: 4, align: 'center'},
				{header: 'FIDEICOMISO', colspan: 4, align: 'center'},
				{header: '', colspan: 7, align: 'center'}
			]
		];		

return new Ext.ux.grid.ColumnHeaderGroup({
		rows:rows
	})
};
var gruposSeleccionadaIFG = creaGrupoHeaderSeleccionadaIFGrid("TODO");
	var seleccionadaIFGrid = new Ext.grid.GridPanel({
				id: 'seleccionadaIF_grid',
				hidden: true,
				store: seleccionadaIFStoreGridData,
				plugins:gruposSeleccionadaIFG,
				style: 'margin:0 auto;',
				title:'seleccionadaIFGrid',
				columns:[
				//CAMPOS DEL GRID
						{
						header: 'Nombre de EPO',
						tooltip: 'Nombre de EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Nombre PYME',
						tooltip: 'Nombre PYME',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 150,
						align: 'left'
						},{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMERODOCTO',
						width: 130,
						align: 'center'						
						},
						{
						header: 'N�mero de Solicitud',
						tooltip: 'N�mero de Solicitud',
						sortable: true,
						dataIndex: 'NUMEROSOLICITUD',
						width: 130,
						align: 'center'						
						},{
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},{
						header: 'Tipo Factoraje',
						tooltip: 'Tipo Factoraje',
						sortable: true,
						dataIndex: 'NOMBRE_TIPO_FACTORAJE',
						width: 130,
						align: 'center'
						},
						{
						header: 'Monto Documento',
						tooltip: 'Monto Documento',
						sortable: true,
						dataIndex: 'MONTODOCTO',
						width: 130,
						align: 'right',
						renderer:renderPesosNA
						},
						{
						header: 'Porcentaje de Descuento',
						tooltip: 'Porcentaje de Descuento',
						sortable: true,
						dataIndex: 'PORCANTICIPO',
						width: 150,
						align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
							var por=record.data.PORCANTICIPO;
							var tipoFactoraje=record.data.TIPOFACTORAJE;
							if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
							 tipoFactoraje=="I" ) {
								por = 100;
							}
							return por +'%';
						}
						},
						{
						header: 'Recurso en Garant�a',
						tooltip: 'Recurso en Garant�a',
						sortable: true,
						//dataIndex: 'IMPORTEDSCTO',
						align: 'right',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(record.data.MONTODOCTO-dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto a Descontar',
						tooltip: 'Monto a Descontar',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO ;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(dblMontoDescuento,'0,000.00');
							}
						},
						{						
						header: 'Nombre IF',
						tooltip: 'Nombre IF',
						sortable: true,
						dataIndex: 'NOMBREIF',
						width: 150,
						align: 'left'
						},
						
						{
						header: 'Tasa IF',
						tooltip: 'Tasa IF',
						sortable: true,
						dataIndex: 'TASAACEPTADA',
						width: 130,
						align: 'center',
						renderer: renderPorcentajeNA
						},
						{
						header: 'Monto Int. IF',
						tooltip: 'Monto Int. IF',
						sortable: true,
						dataIndex: 'IMPORTEINTERES',
						width: 150,
						align: 'right',
						renderer:renderPesosNA
						}
						,
						{
						header: 'Monto a Operar IF',
						tooltip: 'Monto a Operar IF',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR',
						width: 150,
						align: 'right',
						renderer: renderPesosNA
						},
						{						
						header: 'Nombre FIDEICOMISO',
						tooltip: 'Nombre FIDEICOMISO',
						sortable: true,
						dataIndex: 'NOMBRE_FID',
						width: 150,
						align: 'left'
						},
						
						{
						header: 'Tasa FIDEICOMISO',
						tooltip: 'Tasa FIDEICOMISO',
						sortable: true,
						dataIndex: 'TASAACEPTADA_FID',
						width: 130,
						align: 'center',
						renderer: renderPorcentajeNA
						},
						{
						header: 'Monto Int. FIDEICOMISO',
						tooltip: 'Monto Int. FIDEICOMISO',
						sortable: true,
						dataIndex: 'IMPORTEINTERES_FID',
						width: 150,
						align: 'right',
						renderer: renderPesosNA
						}
						,
						{
						header: 'Monto a Operar FIDEICOMISO',
						tooltip: 'Monto a Operar FIDEICOMISO',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR_FID',
						width: 150,
						align: 'right',
						renderer:renderPesosNA
						},
						{
						header: 'Origen',
						tooltip: 'Origen',
						sortable: true,
						dataIndex: '',
						width: 130,
						align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								
								return 'Comunidades';
							}
						},{
						header: 'N�mero Acuse IF',
						tooltip: 'N�mero Acuse IF',
						sortable: true,
						dataIndex: 'ACUSE3',
						width: 130,
						align: 'center'
						},
						{
						header: 'Neto a Recibir PYME',
						tooltip: 'Neto a Recibir PYME',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						align: 'right',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var neto ='';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if( tipoFactoraje=="D" ||
								 tipoFactoraje=="I" ) {
									neto = record.data.IMPORTENETO;
									neto='$ '+ Ext.util.Format.number(neto,'0,000.00');
								}
								return neto;
							}
						},
						
						{
						header: 'Beneficiario',
						tooltip: 'Beneficiario',
						sortable: true,
						dataIndex: 'BENEFICIARIO',
						align: 'center',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.BENEFICIARIO;
								}
								return porcentajeb;
							}
						},
						{
						header: '% Beneficiario',
						tooltip: '% Beneficiario',
						sortable: true,
						align: 'center',
						//dataIndex: 'PORCBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.PORCBENEFICIARIO;
								}
								return porcentajeb;
							}
						
						},
						{
						header: 'Importe a Recibir Beneficiario',
						tooltip: 'Importe a Recibir Beneficiario',
						sortable: true,
						dataIndex: 'IMPORTEBENEFICIARIO',
						align: 'right',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var imporb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 imporb = record.data.IMPORTEBENEFICIARIO;
									 imporb='$ '+ Ext.util.Format.number(imporb,'0,000.00');
								}
								return  imporb;
							}
						},						
						{
						header: 'Referencia Tasa',
						tooltip: 'Referencia Tasa',
						sortable: true,
						dataIndex: 'TIPOTASA',
						width: 150,
						align: 'center'
						}				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {					
					items: ['->','-',								
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo3',
									iconCls: 'icoXls',
									handler: function(boton, evento){
										peticionExcel();
									}
								},
								'-',
								{
									xtype:	'button',
									text:		'Generar PDF',
									id: 		'btnGenerarPDF3',
									iconCls:	'icoPdf',
									handler: function(boton, evento) {
										peticionPdf();
									}
								}
						]
				}
		});
		
			listaGrids[3]=seleccionadaIFGrid;
//////////////////////////////////////////////////////////////////////////////
function  creaGrupoHeaderEnProcesoGrid(tipo){
var rows;

rows= [
			[
				{header: '', colspan: 10, align: 'center'},
				{header: 'INTERMEDIARIO FINANCIERO', colspan: 4, align: 'center'},
				{header: 'FIDEICOMISO', colspan: 4, align: 'center'},
				{header: '', colspan: 4, align: 'center'}
			]
		];		

return new Ext.ux.grid.ColumnHeaderGroup({
		rows:rows
	})
};
var gruposEnProceso = creaGrupoHeaderEnProcesoGrid("TODO");
var enProcesoGrid = new Ext.grid.GridPanel({
				id: 'enProceso_grid',
				hidden: true,
				store: enProcesoStoreGridData,
				plugins:gruposEnProceso,
				style: 'margin:0 auto;',
				title:'enProcesoGrid',
				columns:[
				//CAMPOS DEL GRID
						{
						header: 'Nombre de EPO',
						tooltip: 'Nombre de EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Nombre PYME',
						tooltip: 'Nombre PYME',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 150,
						align: 'left'
						},{
						header: 'N�mero de Solicitud',
						tooltip: 'N�mero de Solicitud',
						sortable: true,
						dataIndex: 'NUMEROSOLICITUD',
						width: 130,
						align: 'center'						
						},
						{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMERODOCTO',
						width: 130,
						align: 'center'						
						},
						{
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},{
						header: 'Tipo Factoraje',
						tooltip: 'Tipo Factoraje',
						sortable: true,
						dataIndex: 'NOMBRE_TIPO_FACTORAJE',
						width: 130,
						align: 'center'
						},
						{
						header: 'Monto Documento',
						tooltip: 'Monto Documento',
						sortable: true,
						dataIndex: 'MONTODOCTO',
						width: 130,
						align: 'right',
						renderer: renderPesosNA
						},
						{
						header: 'Porcentaje de Descuento',
						tooltip: 'Porcentaje de Descuento',
						sortable: true,
						align: 'center',
						dataIndex: 'PORCANTICIPO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
							var por=record.data.PORCANTICIPO;
							var tipoFactoraje=record.data.TIPOFACTORAJE;
							if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
							 tipoFactoraje=="I" ) {
								por = 100;
							}
							return por +'%';
						}
						},
						{
						header: 'Recurso en Garant�a',
						tooltip: 'Recurso en Garant�a',
						sortable: true,
						align: 'right',
						//dataIndex: 'IMPORTEDSCTO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(record.data.MONTODOCTO-dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto a Descontar',
						tooltip: 'Monto a Descontar',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO ;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(dblMontoDescuento,'0,000.00');
							}
						},
						{						
						header: 'Nombre IF',
						tooltip: 'Nombre IF',
						sortable: true,
						dataIndex: 'NOMBREIF',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa IF',
						tooltip: 'Tasa IF',
						sortable: true,
						dataIndex: 'TASAACEPTADA',
						width: 130,
						align: 'center',
						renderer: renderPorcentajeNA
						},
						{
						header: 'Monto Int. IF',
						tooltip: 'Monto Int. IF',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTEINTERES',
						width: 150,
						renderer: renderPesosNA
						}
						,
						{
						header: 'Monto a Operar IF',
						tooltip: 'Monto a Operar IF',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR',
						width: 150,
						align: 'right',
						renderer:renderPesosNA
						},
						{						
						header: 'Nombre FIDEICOMISO',
						tooltip: 'Nombre FIDEICOMISO',
						sortable: true,
						dataIndex: 'NOMBRE_FID',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa FIDEICOMISO',
						tooltip: 'Tasa FIDEICOMISO',
						sortable: true,
						dataIndex: 'TASAACEPTADA_FID',
						width: 130,
						align: 'center',
						renderer:renderPorcentajeNA
						},
						{
						header: 'Monto Int. FIDEICOMISO',
						tooltip: 'Monto Int. FIDEICOMISO',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTEINTERES_FID',
						width: 150,
						renderer:renderPesosNA
						}
						,
						{
						header: 'Monto a Operar FIDEICOMISO',
						tooltip: 'Monto a Operar FIDEICOMISO',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR_FID',
						width: 150,
						align: 'right',
						renderer: renderPesosNA
						},
						{
						header: 'Neto a Recibir PYME',
						tooltip: 'Neto a Recibir PYME',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var neto ='';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if( tipoFactoraje=="D" ||
								 tipoFactoraje=="I" ) {
									neto = record.data.IMPORTENETO;
									neto='$ '+ Ext.util.Format.number(neto,'0,000.00');
								}
								return neto;
							}
						},
						
						{
						header: 'Beneficiario',
						tooltip: 'Beneficiario',
						sortable: true,
						dataIndex: 'BENEFICIARIO',
						align: 'center',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.BENEFICIARIO;
								}
								return porcentajeb;
							}
						},
						{
						header: '% Beneficiario',
						tooltip: '% Beneficiario',
						sortable: true,
						align: 'center',
						//dataIndex: 'PORCBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.PORCBENEFICIARIO;
								}
								return porcentajeb;
							}
						
						},
						{
						header: 'Importe a Recibir Beneficiario',
						tooltip: 'Importe a Recibir Beneficiario',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTEBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var imporb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 imporb = record.data.IMPORTEBENEFICIARIO;
									 imporb='$ '+ Ext.util.Format.number(imporb,'0,000.00');
								}
								return  imporb;
							}
						}				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {					
					items: ['->',
								'-',
								{
									xtype:	'button',
									text:		'Generar PDF',
									id: 		'btnGenerarPDF4',
									iconCls:	'icoPdf',
									handler: function(boton, evento) {
										peticionPdf();
									}
								}
						]
				}
		});
		
			listaGrids[4]=enProcesoGrid;
////////////////////////////////////////////////////////////////////////////////
function  creaGrupoHeaderOperadaGrid(tipo){
var rows;

rows= [
			[
				{header: '', colspan: 10, align: 'center'},
				{header: 'INTERMEDIARIO FINANCIERO', colspan: 3, align: 'center'},
				{header: 'FIDEICOMISO', colspan: 3, align: 'center'},
				{header: '', colspan: 5, align: 'center'}
			]
		];		

return new Ext.ux.grid.ColumnHeaderGroup({
		rows:rows
	})
};
var gruposOperada = creaGrupoHeaderOperadaGrid("TODO");
var operadaGrid = new Ext.grid.GridPanel({
				id: 'operada_grid',
				hidden: true,
				store: operadaStoreGridData,
				plugins:gruposOperada,
				style: 'margin:0 auto;',
				title:'operadaGrid',
				columns:[
				//CAMPOS DEL GRID
						{
						header: 'Nombre de EPO',
						tooltip: 'Nombre de EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Nombre PYME',
						tooltip: 'Nombre PYME',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 150,
						align: 'left'
						},{
						header: 'N�mero de Solicitud',
						tooltip: 'N�mero de Solicitud',
						sortable: true,
						dataIndex: 'NUMEROSOLICITUD',
						width: 130,
						align: 'center'						
						},
						{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMERODOCTO',
						width: 130,
						align: 'center'						
						},
						{
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},{
						header: 'Tipo Factoraje',
						tooltip: 'Tipo Factoraje',
						sortable: true,
						dataIndex: 'NOMBRE_TIPO_FACTORAJE',
						width: 130,
						align: 'center'
						},
						{
						header: 'Monto Documento',
						tooltip: 'Monto Documento',
						sortable: true,
						dataIndex: 'MONTODOCTO',
						width: 130,
						align: 'right',
						renderer: renderPesosNA
						},
						{
						header: 'Porcentaje de Descuento',
						tooltip: 'Porcentaje de Descuento',
						sortable: true,
						dataIndex: 'PORCANTICIPO',
						width: 150,
						align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
							var por=record.data.PORCANTICIPO;
							var tipoFactoraje=record.data.TIPOFACTORAJE;
							if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
							 tipoFactoraje=="I" ) {
								por = 100;
							}
							return por +'%';
						}
						},
						{
						header: 'Recurso en Garant�a',
						tooltip: 'Recurso en Garant�a',
						sortable: true,
						align: 'right',
						//dataIndex: 'IMPORTEDSCTO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(record.data.MONTODOCTO-dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto a Descontar',
						tooltip: 'Monto a Descontar',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						align: 'right',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO ;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(dblMontoDescuento,'0,000.00');
							}
						},
						{						
						header: 'Nombre IF',
						tooltip: 'Nombre IF',
						sortable: true,
						dataIndex: 'NOMBREIF',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa IF',
						tooltip: 'Tasa IF',
						sortable: true,
						dataIndex: 'TASAACEPTADA',
						width: 130,
						align: 'center',
						renderer: renderPorcentajeNA
						},
						{
						header: 'Monto a Operar IF',
						tooltip: 'Monto a Operar IF',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR',
						align: 'right',
						width: 150,
						renderer:renderPesosNA
						},{						
						header: 'Nombre FIDEICOMISO',
						tooltip: 'Nombre FIDEICOMISO',
						sortable: true,
						dataIndex: 'NOMBRE_FID',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa FIDEICOMISO',
						tooltip: 'Tasa FIDEICOMISO',
						sortable: true,
						dataIndex: 'TASAACEPTADA_FID',
						width: 130,
						align: 'center',
						renderer:renderPorcentajeNA
						},
						{
						header: 'Monto a Operar FIDEICOMISO',
						tooltip: 'Monto a Operar FIDEICOMISO',
						sortable: true,
						dataIndex: 'IMPORTERECIBIR_FID',
						align: 'right',
						width: 150,
						renderer: renderPesosNA
						},
						{
						header: 'Neto a Recibir PYME',
						tooltip: 'Neto a Recibir PYME',
						sortable: true,
						align: 'right',
						//dataIndex: 'NUMFOLIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var neto ='';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if( tipoFactoraje=="D" ||
								 tipoFactoraje=="I" ) {
									neto = record.data.IMPORTENETO;
									neto='$ '+ Ext.util.Format.number(neto,'0,000.00');
								}
								return neto;
							}
						},
						
						{
						header: 'Beneficiario',
						tooltip: 'Beneficiario',
						sortable: true,
						dataIndex: 'BENEFICIARIO',
						width: 150,align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.BENEFICIARIO;
								}
								return porcentajeb;
							}
						},
						{
						header: '% Beneficiario',
						tooltip: '% Beneficiario',
						sortable: true,
						align: 'center',
						//dataIndex: 'PORCBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.PORCBENEFICIARIO;
								}
								return porcentajeb;
							}
						
						},
						{
						header: 'Importe a Recibir Beneficiario',
						tooltip: 'Importe a Recibir Beneficiario',
						sortable: true,
						dataIndex: 'IMPORTEBENEFICIARIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var imporb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 imporb = record.data.IMPORTEBENEFICIARIO;
									 imporb='$ '+ Ext.util.Format.number(imporb,'0,000.00');
								}
								return  imporb;
							}
						},
						{
						header: 'Referencia Tasa',
						tooltip: 'Referencia Tasa',
						sortable: true,
						dataIndex: 'TIPOTASA',
						width: 150,
						align: 'center'
						}	
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {					
					items: ['->','-',								
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo5',
									iconCls: 'icoXls',
									handler: function(boton, evento){
										peticionExcel();
									}
								},
								'-',
								{
									xtype:	'button',
									text:		'Generar PDF',
									id: 		'btnGenerarPDF5',
									iconCls:	'icoPdf',
									handler: function(boton, evento) {
										peticionPdf();
									}
								}
						]
				}
		});
		
			listaGrids[5]=operadaGrid;
/////////////////////////////////////////////////////////////////////////////77
var operadoFondeoPropioGrid = new Ext.grid.GridPanel({
				id: 'operadoFondeoPropio_grid',
				hidden: true,
				store: operadoFondeoPropioStoreGridData,
				style: 'margin:0 auto;',
				title:'operadoFondeoPropioGrid',
				columns:[
				//CAMPOS DEL GRID
						{
						header: 'Nombre de EPO',
						tooltip: 'Nombre de EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Nombre PYME',
						tooltip: 'Nombre PYME',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 150,
						align: 'left'
						},
						{						
						header: 'Nombre IF',
						tooltip: 'Nombre IF',
						sortable: true,
						dataIndex: 'NOMBREIF',
						width: 150,
						align: 'left'
						},
						{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMERODOCTO',
						width: 130,
						align: 'center'						
						},
						{
						header: 'N�mero de Solicitud',
						tooltip: 'N�mero de Solicitud',
						sortable: true,
						dataIndex: 'NUMEROSOLICITUD',
						width: 130,
						align: 'center'						
						},{
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},{
						header: 'Tipo Factoraje',
						tooltip: 'Tipo Factoraje',
						sortable: true,
						dataIndex: 'NOMBRE_TIPO_FACTORAJE',
						width: 130,
						align: 'center'
						},
						{
						header: 'Monto Documento',
						tooltip: 'Monto Documento',
						sortable: true,
						dataIndex: 'MONTODOCTO',
						width: 130,
						align: 'right',
						renderer: renderPesosNA
						},
						{
						header: 'Porcentaje de Descuento',
						tooltip: 'Porcentaje de Descuento',
						sortable: true,
						dataIndex: 'PORCANTICIPO',
						width: 150,
						align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
							var por=record.data.PORCANTICIPO;
							var tipoFactoraje=record.data.TIPOFACTORAJE;
							if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
							 tipoFactoraje=="I" ) {
								por = 100;
							}
							return por +'%';
						}
						},
						{
						header: 'Recurso en Garant�a',
						tooltip: 'Recurso en Garant�a',
						sortable: true,
						//dataIndex: 'IMPORTEDSCTO',
						align: 'right',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(record.data.MONTODOCTO-dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto a Descontar',
						tooltip: 'Monto a Descontar',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						align: 'right',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO ;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Tasa',
						tooltip: 'Tasa',
						sortable: true,
						dataIndex: 'TASAACEPTADA',
						width: 130,
						align: 'center',
						renderer: renderPorcentajeNA
						},
						{
						header: 'Monto Int.',
						tooltip: 'Monto Int.',
						sortable: true,
						dataIndex: 'IMPORTEINTERES',
						width: 150,
						align: 'right',
						renderer:renderPesosNA
						}
						,
						{
						header: 'Monto a Operar',
						tooltip: 'Monto a Operar',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTERECIBIR',
						width: 150,
						renderer:renderPesosNA
						},{
						header: 'Origen',
						tooltip: 'Origen',
						sortable: true,
						dataIndex: '',
						width: 130,
						align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								
								return 'Comunidades';
							}
						},{
						header: 'N�mero Acuse IF',
						tooltip: 'N�mero Acuse IF',
						sortable: true,
						dataIndex: 'ACUSE3',
						width: 130,
						align: 'center'
						},
						{
						header: 'Neto a Recibir PYME',
						tooltip: 'Neto a Recibir PYME',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						align: 'right',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var neto ='';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if( tipoFactoraje=="D" ||
								 tipoFactoraje=="I" ) {
									neto = record.data.IMPORTENETO;
									neto='$ '+ Ext.util.Format.number(neto,'0,000.00');
								}
								return neto;
							}
						},
						
						{
						header: 'Beneficiario',
						tooltip: 'Beneficiario',
						sortable: true,
						dataIndex: 'BENEFICIARIO',
						width: 150,align: 'center',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.BENEFICIARIO;
								}
								return porcentajeb;
							}
						},
						{
						header: '% Beneficiario',
						tooltip: '% Beneficiario',
						sortable: true,align: 'center',
						//dataIndex: 'PORCBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.PORCBENEFICIARIO;
								}
								return porcentajeb;
							}
						
						},
						{
						header: 'Importe a Recibir Beneficiario',
						tooltip: 'Importe a Recibir Beneficiario',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTEBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var imporb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 imporb = record.data.IMPORTEBENEFICIARIO;
									 imporb='$ '+ Ext.util.Format.number(imporb,'0,000.00');
								}
								return  imporb;
							}
						},						
						{
						header: 'Referencia Tasa',
						tooltip: 'Referencia Tasa',
						sortable: true,
						dataIndex: 'TIPOTASA',
						width: 150
						}				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {					
					items: ['->','-',								
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo6',
									iconCls: 'icoXls',
									handler: function(boton, evento){
										peticionExcel();
									}
								},
								'-',
								{
									xtype:	'button',
									text:		'Generar PDF',
									id: 		'btnGenerarPDF6',
									iconCls:	'icoPdf',
									handler: function(boton, evento) {
										peticionPdf();
									}
								}
						]
				}
		});
		listaGrids[6]=operadoFondeoPropioGrid;
////////////////////////////////////////////////////////////////////////////////
////////////////////////Operada Pagada//////////////////////////////////////////
function  creaGrupoHeaderOperadaPagadaGrid(tipo){
var rows;

rows= [
			[
				{header: '', colspan: 10, align: 'center'},
				{header: 'INTERMEDIARIO FINANCIERO', colspan: 2, align: 'center'},
				{header: 'FIDEICOMISO', colspan: 2, align: 'center'},
				{header: '', colspan: 7, align: 'center'}
			]
		];		

return new Ext.ux.grid.ColumnHeaderGroup({
		rows:rows
	})
};
var gruposOperadaPagada = creaGrupoHeaderOperadaPagadaGrid("TODO");
var operadaPagadaGrid = new Ext.grid.GridPanel({
				id: 'operadaPagada_grid',
				hidden: true,
				store: operadaPagadaStoreGridData,//operadaStoreGridData,
				style: 'margin:0 auto;',
				plugins:gruposOperadaPagada,
				title:'operadaPagadaGrid',
				columns:[
				//CAMPOS DEL GRID
						{
						header: 'Nombre de EPO',
						tooltip: 'Nombre de EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Nombre PYME',
						tooltip: 'Nombre PYME',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 150,
						align: 'left'
						},{
						header: 'N�mero de Solicitud',
						tooltip: 'N�mero de Solicitud',
						sortable: true,
						dataIndex: 'NUMEROSOLICITUD',
						width: 130,
						align: 'center'						
						},
						{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMERODOCTO',
						width: 130,
						align: 'center'						
						},
						{
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},{
						header: 'Tipo Factoraje',
						tooltip: 'Tipo Factoraje',
						sortable: true,
						dataIndex: 'NOMBRE_TIPO_FACTORAJE',
						width: 130,
						align: 'center'
						},
						{
						header: 'Monto Documento',
						tooltip: 'Monto Documento',
						sortable: true,
						dataIndex: 'MONTODOCTO',
						width: 130,
						align: 'right',
						renderer: renderPesosNA
						},
						{
						header: 'Porcentaje de Descuento',
						tooltip: 'Porcentaje de Descuento',
						sortable: true,align: 'center',
						dataIndex: 'PORCANTICIPO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
							var por=record.data.PORCANTICIPO;
							var tipoFactoraje=record.data.TIPOFACTORAJE;
							if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
							 tipoFactoraje=="I" ) {
								por = 100;
							}
							return por +'%';
						}
						},
						{
						header: 'Recurso en Garant�a',
						tooltip: 'Recurso en Garant�a',
						sortable: true,
						//dataIndex: 'IMPORTEDSCTO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(record.data.MONTODOCTO-dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto a Descontar',
						tooltip: 'Monto a Descontar',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO ;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(dblMontoDescuento,'0,000.00');
							}
						},
						{						
						header: 'Nombre IF',
						tooltip: 'Nombre IF',
						sortable: true,
						dataIndex: 'NOMBREIF',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa IF',
						tooltip: 'Tasa IF',
						sortable: true,
						dataIndex: 'TASAACEPTADA',
						width: 130,
						align: 'center',
						renderer:renderPorcentajeNA
						},{						
						header: 'Nombre FIDEICOMISO',
						tooltip: 'Nombre FIDEICOMISO',
						sortable: true,
						dataIndex: 'NOMBRE_FID',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa FIDEICOMISO',
						tooltip: 'Tasa FIDEICOMISO',
						sortable: true,
						dataIndex: 'TASAACEPTADA_FID',
						width: 130,
						align: 'center',
						renderer:renderPorcentajeNA
						},{
						align: 'center',
						header:'Fecha Vencimiento',
						tooltip: 'Fecha Vencimiento',
						sortable: true,
						dataIndex: 'FECHAVENC',
						width: 130						
						},{
						align: 'center',
						header:'Fecha Pago',
						tooltip: 'Fecha Pago',
						sortable: true,
						dataIndex: 'FECHAVENC',
						width: 130						
						},
						{
						header: 'Neto a Recibir PYME',
						tooltip: 'Neto a Recibir PYME',
						sortable: true,
						//dataIndex: 'NUMFOLIO',
						width: 150,
						align: 'right',
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var neto ='';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if( tipoFactoraje=="D" ||
								 tipoFactoraje=="I" ) {
									neto = record.data.IMPORTENETO;
									neto='$ '+ Ext.util.Format.number(neto,'0,000.00');
								}
								return neto;
							}
						},
						
						{
						header: 'Beneficiario',
						tooltip: 'Beneficiario',
						sortable: true,align: 'center',
						dataIndex: 'BENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.BENEFICIARIO;
								}
								return porcentajeb;
							}
						},
						{
						header: '% Beneficiario',
						tooltip: '% Beneficiario',
						sortable: true,align: 'center',
						//dataIndex: 'PORCBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.PORCBENEFICIARIO;
								}
								return porcentajeb;
							}
						
						},
						{
						header: 'Importe a Recibir Beneficiario',
						tooltip: 'Importe a Recibir Beneficiario',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTEBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var imporb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 imporb = record.data.IMPORTEBENEFICIARIO;
									 imporb='$ '+ Ext.util.Format.number(imporb,'0,000.00');
								}
								return  imporb;
							}
						},
						{
						header: 'Referencia Tasa',
						tooltip: 'Referencia Tasa',
						sortable: true,align: 'center',
						dataIndex: 'TIPOTASA',
						width: 150
						}
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {					
					items: ['->','-',								
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo7',
									iconCls: 'icoXls',
									handler: function(boton, evento){
										peticionExcel();
									}
								},
								'-',
								{
									xtype:	'button',
									text:		'Generar PDF',
									id: 		'btnGenerarPDF7',
									iconCls:	'icoPdf',
									handler: function(boton, evento) {
										peticionPdf();
									}
								}
						]
				}
		});
		
			listaGrids[7]=operadaPagadaGrid;
//////////////////////////////////////////////////////////////////////////////
//programadoPyMEStoreGridData
var programadoPyMEGrid = new Ext.grid.GridPanel({
				id: 'programadoPyME_grid',
				hidden: true,
				store: programadoPyMEStoreGridData,
				style: 'margin:0 auto;',
				title:'programadoPyMEGrid',
				columns:[
				//CAMPOS DEL GRID
						{
						header: 'Nombre de EPO',
						tooltip: 'Nombre de EPO',
						sortable: true,
						dataIndex: 'NOMBREEPO',
						width: 150,
						align: 'left'
						},
						{
						header: 'Nombre PYME',
						tooltip: 'Nombre PYME',
						sortable: true,
						dataIndex: 'NOMBREPYME',
						width: 150,
						align: 'left'
						},
						{
						header: 'N�mero de Documento',
						tooltip: 'N�mero de Documento',
						sortable: true,
						dataIndex: 'NUMERODOCTO',
						width: 130,
						align: 'center'						
						},
						{
						align:'center',
						header: 'Fecha Documento',
						tooltip: 'Fecha Documento',
						sortable: true,
						dataIndex: 'FECHADOCTO',
						width: 130
						},{
						align: 'center',
						header:'Fecha Vencimiento',
						tooltip: 'Fecha Vencimiento',
						sortable: true,
						dataIndex: 'FECHAVENC',
						width: 130						
						},
						{						
						header: 'Intermediario Financiero',
						tooltip: 'Intermediario Financiero',
						sortable: true,
						dataIndex: 'NOMBREIF',
						width: 150,
						align: 'left'
						},
						{
						header: 'Tasa',
						tooltip: 'Tasa',
						sortable: true,
						dataIndex: 'TASAACEPTADA',
						width: 130,
						align: 'center'
						},{
						header: 'Plazo (d�as)',
						tooltip: 'Plazo (d�as)',
						sortable: true,
						dataIndex: 'PLAZO',
						width: 130,
						align: 'center'
						},{
						header: 'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'NOMBREMONEDA',
						width: 130,
						align: 'center'
						},{
						header: 'Tipo Factoraje',
						tooltip: 'Tipo Factoraje',
						sortable: true,
						dataIndex: 'NOMBRE_TIPO_FACTORAJE',
						width: 130,
						align: 'center'
						},{
						header: 'Monto Documento',
						tooltip: 'Monto Documento',
						sortable: true,
						dataIndex: 'MONTODOCTO',
						width: 130,
						align: 'right',
						renderer: renderPesosNA
						},
						{
						header: 'Porcentaje de Descuento',
						tooltip: 'Porcentaje de Descuento',
						sortable: true,align: 'center',
						dataIndex: 'PORCANTICIPO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
							var por=record.data.PORCANTICIPO;
							var tipoFactoraje=record.data.TIPOFACTORAJE;
							if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
							 tipoFactoraje=="I" ) {
								por = 100;
							}
							return por +'%';
						}
						},
						{
						header: 'Recurso en Garant�a',
						tooltip: 'Recurso en Garant�a',
						sortable: true,
						align: 'right',
						//dataIndex: 'IMPORTEDSCTO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(record.data.MONTODOCTO-dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto a Descontar',
						tooltip: 'Monto a Descontar',
						sortable: true,
						align: 'right',
						//dataIndex: 'NUMFOLIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var dblMontoDescuento = record.data.MONTODSCTO ;
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="V" || tipoFactoraje=="D" ||
								tipoFactoraje=="I" ) {
									dblMontoDescuento = record.data.MONTODOCTO;
								}
								return '$ '+ Ext.util.Format.number(dblMontoDescuento,'0,000.00');
							}
						},
						{
						header: 'Monto Int.',
						tooltip: 'Monto Int.',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTEINTERES',
						width: 150,
						renderer: renderPesosNA
						}
						,
						{
						header: 'Monto a Operar',
						tooltip: 'Monto a Operar',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTERECIBIR',
						width: 150,
						renderer: renderPesosNA
						},
						{
						header: 'Neto a Recibir PYME',
						tooltip: 'Neto a Recibir PYME',
						sortable: true,
						align: 'right',
						//dataIndex: 'NUMFOLIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var neto ='';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if( tipoFactoraje=="D" ||
								 tipoFactoraje=="I" ) {
									neto = record.data.IMPORTENETO;
									neto='$ '+ Ext.util.Format.number(neto,'0,000.00');
								}
								return neto;
							}
						},
						
						{
						header: 'Beneficiario',
						tooltip: 'Beneficiario',
						sortable: true,align: 'center',
						dataIndex: 'BENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.BENEFICIARIO;
								}
								return porcentajeb;
							}
						},
						{
						header: '% Beneficiario',
						tooltip: '% Beneficiario',
						sortable: true,align: 'center',
						//dataIndex: 'PORCBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var porcentajeb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 porcentajeb = record.data.PORCBENEFICIARIO;
								}
								return porcentajeb;
							}
						
						},
						{
						header: 'Importe a Recibir Beneficiario',
						tooltip: 'Importe a Recibir Beneficiario',
						sortable: true,
						align: 'right',
						dataIndex: 'IMPORTEBENEFICIARIO',
						width: 150,
						renderer:function (value, metaData, record, rowIndex, colIndex, store){
								var imporb = '';
								var tipoFactoraje=record.data.TIPOFACTORAJE;
								if(tipoFactoraje=="D" || tipoFactoraje=="I" ) {
									 imporb = record.data.IMPORTEBENEFICIARIO;
									 imporb='$ '+ Ext.util.Format.number(imporb,'0,000.00');
								}
								return  imporb;
							}
						},						
						{
						header: 'Fecha Registro Operaci�n',
						tooltip: 'Fecha Registro Operaci�n',
						sortable: true,align: 'center',
						dataIndex: 'DF_PROGRAMACION',
						width: 150
						},						
						{
						header: 'Referencia Tasa',
						tooltip: 'Referencia Tasa',
						sortable: true,align: 'center',
						dataIndex: 'TIPOTASA',
						width: 150
						}				
				],
				stripeRows: true,
				loadMask: true,
				height: 400,
				width: 940,
				style: 'margin:0 auto;',
				frame: true,
				bbar: {					
					items: ['->','-',								
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivo8',
									iconCls: 'icoXls',
									handler: function(boton, evento){
										peticionExcel();
									}
								},
								'-',
								{
									xtype:	'button',
									text:		'Generar PDF',
									id: 		'btnGenerarPDF8',
									iconCls:	'icoPdf',
									handler: function(boton, evento) {
										peticionPdf();
									}
								}
						]
				}
		});
		
			listaGrids[8]=programadoPyMEGrid;
//-------------------------------------------
var elementosForma = [
		{
			xtype: 'label',
			id: 'strNombreUsuario',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Usuario',
			text: '--'
		},
		{
			xtype: 'label',
			id: 'fechaHora',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Fecha',
			text: '--'
		},NE.util.getEspaciador(5),
		{
            xtype: 'radiogroup',
				id:'radioTipoOper',
				hidden:true,
            fieldLabel: '',
				items: [
                {boxLabel: 'Operaci�n Normal', name: 'tipoOperacion',inputValue: 'ON'},
                {boxLabel: 'Operaci�n Fideicomiso', name: 'tipoOperacion',inputValue: 'FID'},
					 {boxLabel: 'Ambas', name: 'tipoOperacion',inputValue: 'AM',checked: true}
            ],
				listeners:{
				'change':function( elem,cheked){
								
								var cbo=Ext.getCmp('id_ic_estatus');
								var v = cbo.getValue();
								var record = cbo.findRecord(cbo.valueField || cbo.displayField, v);
								var index = cbo.store.indexOf(record);
								if(!Ext.isEmpty(cbo.getValue())){
										
										arrayEstatusSelect=index;
										Ext.getCmp('gridTotalesFid').hide();
										Ext.getCmp('gridTotales').hide();
										Ext.getCmp('seleccionadaPYME_grid').hide();
										Ext.getCmp('negociables_grid').hide();
										Ext.getCmp('enProcesoAutorizacionIF_grid').hide();
										Ext.getCmp('seleccionadaIF_grid').hide();
										Ext.getCmp('enProceso_grid').hide();	
										Ext.getCmp('operada_grid').hide();	
										Ext.getCmp('operadoFondeoPropio_grid').hide();									
										Ext.getCmp('programadoPyME_grid').hide();									
										Ext.getCmp('operadaPagada_grid').hide();									
										
										
										listaGrids[index].setTitle(record.get('descripcion'));
										listaStores[index].load({
												params:	Ext.apply(fp.getForm().getValues(),
															{
																informacion: 'Consultar',
																indiceEst:arrayEstatusSelect,
																operacion: 'Generar'	
															}
												)
											});
									
										
										}
							}
			}
      },
		{
			xtype:			'combo',
			id:				'id_ic_estatus',
			name:				'ic_estatus',
			hiddenName:		'ic_estatus',
			fieldLabel:		'Estatus',
			emptyText:		'Seleccionar. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:false,
			typeAhead:		true,
			triggerAction:	'all',
			minChars:		1,
			store:			catalogoEstatus,
			tpl:				NE.util.templateMensajeCargaCombo
			,listeners:{
				'select':function( cbo, record, index){
								var fp = Ext.getCmp('forma');
								var grupoRad=Ext.getCmp('radioTipoOper');
								grupoRad.hide();
								if(!Ext.isEmpty(cbo.getValue())){
										fp.el.mask('Procesando...', 'x-mask-loading');
										
										if(index==1 || index==2|| index==3 ||index==4||index==5||index==7)
										grupoRad.show();
										arrayEstatusSelect=index;
										Ext.getCmp('gridTotalesFid').hide();
										Ext.getCmp('gridTotales').hide();
										Ext.getCmp('seleccionadaPYME_grid').hide();
										Ext.getCmp('negociables_grid').hide();
										Ext.getCmp('enProcesoAutorizacionIF_grid').hide();
										Ext.getCmp('seleccionadaIF_grid').hide();
										Ext.getCmp('enProceso_grid').hide();	
										Ext.getCmp('operada_grid').hide();	
										Ext.getCmp('operadoFondeoPropio_grid').hide();									
										Ext.getCmp('programadoPyME_grid').hide();									
										Ext.getCmp('operadaPagada_grid').hide();									
										
										
										listaGrids[index].setTitle(record.get('descripcion'));
										listaStores[index].load({
												params:	Ext.apply(fp.getForm().getValues(),
															{
																informacion: 'Consultar',
																indiceEst:arrayEstatusSelect,
																operacion: 'Generar'	
															}
												)
											});
									
										
										}
							}
			}
		}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			610,
		style:			'margin:0 auto;',
		collapsible:	true,
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		monitorValid:	true
	});

//----------------Principal------------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 930,
	  height: 'auto',
	  items: 
	    [  
			fp ,
			NE.util.getEspaciador(10),negociablesGrid,SeleccionadaPYMEGrid,
			enProcesoAutorizacionIFGrid,seleccionadaIFGrid,
			enProcesoGrid,operadaGrid,operadoFondeoPropioGrid,
			operadaPagadaGrid,programadoPyMEGrid,
			NE.util.getEspaciador(10),gridTotales	,gridTotalesFid	
		 ]
  });
		
		
		var valIniciales = function(){
		Ext.Ajax.request({
			url: '13reporte1Ext.data.jsp',
			params: {
						informacion: "valoresIniciales"
			},
			callback: procesaValoresIniciales
		});
	}
	
	valIniciales();
	

		
		
});