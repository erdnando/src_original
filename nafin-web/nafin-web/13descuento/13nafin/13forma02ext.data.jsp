<%@ page 
	contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		com.netro.exception.*, 
		java.math.BigDecimal,
		com.netro.exception.NafinException,
		com.netro.model.catalogos.CatalogoEPO,
		javax.naming.Context"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion	= (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CapturaTasas.inicializacion") 				) {
 
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// 1. Definir el estado siguiente
	estadoSiguiente = "ESPERAR_DECISION";
	
	// 2. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",					msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CatalogoEPO")                				) {
	
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();	

} else if(  informacion.equals("CapturaTasas.consultaTasaBasePorEPO")	) {
 
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	// 1. Leer parametros de la consulta
	String 		claveEPO				= (request.getParameter("claveEPO") == null)?"":request.getParameter("claveEPO");
	
	// 2. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("CapturaTasas.consultaTasaBasePorEPO(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
	
	// 3. Obtener numero de Tasas
	int numRegistros 	= autorizacionTasas.getTasaBase(claveEPO).size();
					
	// 4. Determinar el estado siguiente
	if (numRegistros != 0){

		// Consultar IFs asociados
		Vector      vIfs 		   = autorizacionTasas.getIFAsociados(claveEPO);
		
		JSONArray   registros 	= new JSONArray();
		JSONObject  registro	   = new JSONObject();
		registro.put("clave",		"TODOS");
		registro.put("descripcion","TODOS");
		registros.add(registro);
		
		for (int n = 0; n < vIfs.size(); n++) {
			Vector 		vRegistro = (Vector)vIfs.get(n);
			registro	 				 = new JSONObject();
			registro.put("clave",		(String) vRegistro.get(0));
			registro.put("descripcion",(String) vRegistro.get(1));
			registros.add(registro);
		}
		
		JSONObject 	catalogoIfsAsociados = new JSONObject();
		catalogoIfsAsociados.put("registros",	registros								);
		catalogoIfsAsociados.put("total",		String.valueOf(registros.size())	);
		resultado.put("catalogoIfsAsociados",catalogoIfsAsociados);
 
		estadoSiguiente = "CARGAR_CATALOGO_IFS_ASOCIADOS";

	} else {
		estadoSiguiente = "AGREGAR_NUEVA_TASA_BASE";
	}
 
	// 4. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.cargarCatalogoIfsAsociados") 	){
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	String 		estadoSiguiente 	= null;
	String		msg					= null;
	
	estadoSiguiente = "CONSULTA_DETALLE_TASAS_POR_EPO_DATA";
	
	// 4. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	infoRegresar = resultado.toString();
				
} else if(  informacion.equals("ConsultaDetalleTasasPorEpoData") 			) {
 
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	
	// 1. Leer parametros
	String ifAsociados 		= (request.getParameter("ifAsociados")		== null)?"[]":request.getParameter("ifAsociados");
	String ifSeleccionado 	= (request.getParameter("ifSeleccionado")	== null)?"[]":request.getParameter("ifSeleccionado");
	String claveIF 			= (request.getParameter("claveIF")			== null)?"":request.getParameter("claveIF");
	String claveEPO 			= (request.getParameter("claveEPO")			== null)?"":request.getParameter("claveEPO");
	
	// 2. Adaptar parametros
	JSONArray vIfs = null;
	if( "TODOS".equals(claveIF)){
		vIfs = JSONArray.fromObject(ifAsociados);
	} else {
		vIfs = JSONArray.fromObject(ifSeleccionado);
	}
	
	// 3. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("ConsultaDetalleTasasPorEpoData(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
	
	// 4. Realizar consulta
	JSONArray 	detalleTasasPorEpoData = new JSONArray();
	JSONObject 	detalle					  = null;
	String 		monedaAnterior 		  = "";
	int 			groupId					  = 0;
	for (int a = 0; a < vIfs.size(); a++) {
		
		JSONObject 	lic_ifs 			= vIfs.getJSONObject(a);
		
		for (int m = 0; m < 2; m++) {
								
			String ic_moneda = (m==0)?"1":"54";
 
			log.debug("////////////////////////VARIABLES PARA EL QUERY////////////////////////");
			log.debug("ic_epo::"+claveEPO);
			log.debug("lic_ifs.getString(\"clave\")::"+lic_ifs.getString("clave"));
			log.debug("ic_moneda::"+ic_moneda);
			log.debug("////////////////////////FIN DE VARIABLES PARA EL QUERY////////////////////////");
			
			Vector registros = autorizacionTasas.getTasaAutIF(claveEPO, lic_ifs.getString("clave"), ic_moneda, "");
			
			// Determinar el plazo con rango máximo de días. F031-2014 by JSHD
			String 	icPlazoRangoMaximo 	= null;
			
			if (registros.size() > 0) {
				
				int 		plazoDiasInicio  		= 0;
				
				for (int i = 0; i < registros.size(); i++) {		
					
					Vector registro 				= (Vector) registros.get(i);
					
					String icPlazo01				= registro.get(13).toString(); // ic_plazo
					String plazoDiasInicio01	= registro.get(14).toString(); // plazo_dias_inicio
					
					int 	 dias						= plazoDiasInicio01.matches("^\\s*$")?0:Integer.parseInt(plazoDiasInicio01);
					
					if( dias > plazoDiasInicio ){
						icPlazoRangoMaximo = icPlazo01;
						plazoDiasInicio	 = dias;	
					}
					
				}
				
				// Asegurarse que efectivamente icPlazoRangoMaximo tenga valor
				icPlazoRangoMaximo = icPlazoRangoMaximo == null || icPlazoRangoMaximo.matches("^\\s*$")?null:icPlazoRangoMaximo;
				
			}
			
			// Agregar detalle de las tasas
			if (registros.size() > 0) {
									
				String intermediarioFinanciero	= lic_ifs.getString("descripcion");
				//String nombreMoneda					= ((Vector)registros.get(0)).get(6).toString();
									
				for (int i = 0; i < registros.size(); i++) {
										
					Vector	registro		= (Vector) registros.get(i);
 
					String	nombreIF 		= registro.get(0).toString();
					String	voboEpo 			= registro.get(1).toString();
					String	voboIf 			= registro.get(2).toString();
					String	voboNafin 		= registro.get(3).toString();
					String	fecha1 			= registro.get(4).toString();
					String	nombreTasa 		= registro.get(5).toString();
					String	nombreMoneda 	= registro.get(6).toString();
					String	rango_plazo 	= registro.get(8).toString();
					String	relmat_tasa 	= registro.get(9).toString();
					String	puntos_tasa 	= registro.get(10).toString();
					log.debug("::::::::PUNTOS TASA:::::"+registro.get(10).toString());
					String	valor_tasa 		= registro.get(11).toString();
					String	fecha_tmp 		= registro.get(12).toString();
					String	icPlazo 			= registro.get(13).toString();
					String	plazoDiasInicio= registro.get(14).toString();
					String	plazoDiasFin 	= registro.get(15).toString();
					String	plazoEspecifico= registro.get(16).toString();
					String 	tasa_aplicar 	= "";
				
					/* Nota: no se ocupa
					if (!voboNafin.equals("S")) {
						esPosibleCrearNuevaTasa = false;
					}
					*/
					if(relmat_tasa.equals("+")) {
						tasa_aplicar = (new BigDecimal(valor_tasa).add(new BigDecimal(puntos_tasa))).toPlainString();
					}
					if(relmat_tasa.equals("-")) {
						tasa_aplicar = (new BigDecimal(valor_tasa).subtract(new BigDecimal(puntos_tasa))).toPlainString();
					}
					if(relmat_tasa.equals("/")) {
						if (!puntos_tasa.equals("0")) {
							tasa_aplicar = (new BigDecimal(valor_tasa).divide(new BigDecimal(puntos_tasa),5)).toPlainString();
						}
					}
					if(relmat_tasa.equals("*")) {
						tasa_aplicar = (new BigDecimal(valor_tasa).multiply(new BigDecimal(puntos_tasa))).toPlainString();
					}
										
					String tasaBase				= nombreTasa;
					String plazo					= rango_plazo;
					String valor					= valor_tasa;
					String relacionMatematica	= relmat_tasa;
					String puntosAdicionales	= puntos_tasa;
					String tasaAAplicar			= Comunes.formatoDecimal(tasa_aplicar, 5);
					
					String icIF 					= lic_ifs.getString("clave");
					if( !monedaAnterior.equals(ic_moneda) ){
						monedaAnterior = ic_moneda;
						groupId++;
					} 
					
					// Determinar si el plazo en cuestión corresponde al rango máximo de días. F031-2014 by JSHD
					Boolean esRangoMaximo 		= icPlazoRangoMaximo != null && icPlazoRangoMaximo.equals(icPlazo)?Boolean.TRUE:Boolean.FALSE;
					
					detalle = new JSONObject();
					detalle.put("GROUP_ID",							new Integer(groupId)		);
					detalle.put("IC_IF",								new Integer(icIF)			);
					detalle.put("IC_MONEDA",						new Integer(ic_moneda)	);
					detalle.put("INTERMEDIARIO_FINANCIERO",	intermediarioFinanciero	);
					detalle.put("MONEDA",							nombreMoneda				);
					detalle.put("TASA_BASE",						tasaBase						);
					detalle.put("PLAZO",								plazo							);
					detalle.put("VALOR_TASA",						valor							);
					detalle.put("RELACION_MATEMATICA",			relacionMatematica		);
					detalle.put("PUNTOS_ADICIONALES",			puntosAdicionales			);
					detalle.put("TASA_A_APLICAR",					tasaAAplicar				);

					// PARAMETROS PARA MODIFICAR LA TASA...
					
					//'nombreTasa' 									--> "TASA_BASE"
					//'nombreIF'   									--> "INTERMEDIARIO_FINANCIERO"
					detalle.put("FECHA_TASA_BASE",						fecha1		);
					//ic_if 												--> IC_IF
					detalle.put("IC_EPO",							claveEPO		);
					detalle.put("VOBO_EPO",							voboEpo		);
					detalle.put("VOBO_IF",							voboIf		);
					detalle.put("VOBO_NAFIN",						voboNafin	);
					//icmoneda 											--> IC_MONEDA
					detalle.put("RANGO_PLAZO",						rango_plazo	);
					//valor_tasa    									--> VALOR
					//relmat_tasa   									--> RELACION_MATEMATICA
					//puntos_tasa  									--> PUNTOS_ADICIONALES
					
					// Enviar datos del plazo, así como bandera para inidicar si se trata del rango máximo. F031-2014 by JSHD
					detalle.put("IC_PLAZO",							icPlazo				);
					detalle.put("PLAZO_DIAS_INICIO",				plazoDiasInicio	);
					detalle.put("PLAZO_DIAS_FIN",					plazoDiasFin		);
					detalle.put("PLAZO_ESPECIFICO",				plazoEspecifico	);
					detalle.put("ES_RANGO_MAXIMO",				esRangoMaximo		);
					
					
					detalleTasasPorEpoData.add(detalle);
					
				}
			}
		}
	}
	
	JSONArray registros = new JSONArray();
	
	// 4. Enviar resultado de la operacion
	resultado.put("success", 	new Boolean(success)	 								);
	resultado.put("registros",	detalleTasasPorEpoData								);
	resultado.put("total",		String.valueOf(detalleTasasPorEpoData.size()));
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.modificarTasaBase") 			) {
	
	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
	
	// 1. Leer parametros
	String 	tasaBase 					= (request.getParameter("TASA_BASE")						== null)?"":request.getParameter("TASA_BASE"); // 'nombre_tbase'
	String 	intermediarioFinanciero = (request.getParameter("INTERMEDIARIO_FINANCIERO")	== null)?"":request.getParameter("INTERMEDIARIO_FINANCIERO"); // 'nombre_if'
	String 	fechaTasaBase 				= (request.getParameter("FECHA_TASA_BASE")				== null)?"":request.getParameter("FECHA_TASA_BASE"); // 'fecha_tbase'
	String 	icIf 							= (request.getParameter("IC_IF")								== null)?"":request.getParameter("IC_IF"); // 'n_if'
	String 	icEpo 						= (request.getParameter("IC_EPO")							== null)?"":request.getParameter("IC_EPO"); // 'n_epo'
	String 	voboEpo 						= (request.getParameter("VOBO_EPO")							== null)?"":request.getParameter("VOBO_EPO"); // 'voboEpo'
	String 	voboIf 						= (request.getParameter("VOBO_IF")							== null)?"":request.getParameter("VOBO_IF"); // 'voboIf'
	String 	voboNafin 					= (request.getParameter("VOBO_NAFIN")						== null)?"":request.getParameter("VOBO_NAFIN"); // 'voboNafin'
	String 	icMoneda 					= (request.getParameter("IC_MONEDA")						== null)?"":request.getParameter("IC_MONEDA"); // 'icmoneda'
	String 	rangoPlazo 					= (request.getParameter("RANGO_PLAZO")						== null)?"":request.getParameter("RANGO_PLAZO"); // 'rplazo'
	String 	plazoEspecifico 			= (request.getParameter("PLAZO_ESPECIFICO")				== null)?"":request.getParameter("PLAZO_ESPECIFICO"); // '???'
	String 	valorTasa 					= (request.getParameter("VALOR_TASA")						== null)?"":request.getParameter("VALOR_TASA"); // 'vtasa'
	String 	relacionMatematica 		= (request.getParameter("RELACION_MATEMATICA")			== null)?"":request.getParameter("RELACION_MATEMATICA"); // 'rmtasa'
	String 	puntosAdicionales 		= (request.getParameter("PUNTOS_ADICIONALES")			== null)?"":request.getParameter("PUNTOS_ADICIONALES"); // 'ptasa'
	String 	icPlazo 						= (request.getParameter("IC_PLAZO")							== null)?"":request.getParameter("IC_PLAZO"); // '???'
	String 	plazoDiasInicio 			= (request.getParameter("PLAZO_DIAS_INICIO")				== null)?"":request.getParameter("PLAZO_DIAS_INICIO"); // '???'
	String 	plazoDiasFin 				= (request.getParameter("PLAZO_DIAS_FIN")					== null)?"":request.getParameter("PLAZO_DIAS_FIN"); // '???'
	String 	esRangoMaximo 				= (request.getParameter("ES_RANGO_MAXIMO")				== null)?"":request.getParameter("ES_RANGO_MAXIMO"); // '???'
	
	// 2. Calcular los parametros segun se requiera.
	String 	nombreMoneda 				= icMoneda.equals("1")?"MONEDA NACIONAL":"DOLAR AMERICANO";
	Boolean 	autoriza						= voboEpo.equals("S") && voboIf.equals("S") && voboNafin.equals("S")?new Boolean(true):new Boolean(false);
	// 3. Calcular Tasa a Aplicar
	String 	tasaAAplicar 				= "";
	if(relacionMatematica.equals("+")) {
		tasaAAplicar = (new BigDecimal(valorTasa).add(new BigDecimal(puntosAdicionales))).toPlainString();
	}
	if(relacionMatematica.equals("-")) {
		tasaAAplicar = (new BigDecimal(valorTasa).subtract(new BigDecimal(puntosAdicionales))).toPlainString();
	}
	if(relacionMatematica.equals("/")) {
		if (!puntosAdicionales.equals("0")) {
			tasaAAplicar = (new BigDecimal(valorTasa).divide(new BigDecimal(puntosAdicionales),5)).toPlainString();
		}
	}
	if(relacionMatematica.equals("*")) {
		tasaAAplicar = (new BigDecimal(valorTasa).multiply(new BigDecimal(puntosAdicionales))).toPlainString();
	}	
	tasaAAplicar = Comunes.formatoDecimal(tasaAAplicar, 5);
	
	// 4. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("ConsultaDetalleTasasPorEpoData(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}

	// 5. Calcular puntos límite 
	String csPuntosLimite = "";
	String fnPuntosLimite = "";
	if (!icEpo.equals("")) {
		HashMap valorPuntosLim 		= autorizacionTasas.obtenerParametrizacionPuntosLimite();
		HashMap paramPuntosLimEpo 	= autorizacionTasas.obtenerParametrizacionPuntosLimitePorEpo(icEpo, "");
		fnPuntosLimite 				= valorPuntosLim.get("valorPuntosLimite")==null?"0.0000":(String)valorPuntosLim.get("valorPuntosLimite");
		HashMap puntosLimiteEpo 	= paramPuntosLimEpo.get("puntosLimitePorEpo0")==null?new HashMap():(HashMap)paramPuntosLimEpo.get("puntosLimitePorEpo0");
		csPuntosLimite 				= puntosLimiteEpo.get("csPuntosLimite")==null?"S":(puntosLimiteEpo.get("csPuntosLimite").toString().equals("SI")?"S":"N");
	}
	
	// 6. Enviar respuesta
	resultado.put("TASA_BASE",						tasaBase						); // 'nombre_tbase'
	resultado.put("INTERMEDIARIO_FINANCIERO",	intermediarioFinanciero	); // 'nombre_if'
	resultado.put("FECHA_TASA_BASE",				fechaTasaBase				); // 'fecha_tbase'
	resultado.put("IC_IF",							icIf							); // 'n_if'
	resultado.put("IC_EPO",							icEpo							); // 'n_epo'
	resultado.put("VOBO_EPO",						voboEpo						); // 'voboEpo'
	resultado.put("VOBO_IF",						voboIf						); // 'voboIf'
	resultado.put("VOBO_NAFIN",					voboNafin					); // 'voboNafin'
	resultado.put("IC_MONEDA",						icMoneda						); // 'icmoneda'
	resultado.put("RANGO_PLAZO",					rangoPlazo					); // 'rplazo'
	resultado.put("PLAZO_ESPECIFICO",			plazoEspecifico			); // '???'
	resultado.put("VALOR_TASA",					valorTasa					); // 'vtasa'
	resultado.put("RELACION_MATEMATICA",		relacionMatematica		); // 'rmtasa'
	resultado.put("PUNTOS_ADICIONALES",			puntosAdicionales			); // 'ptasa'
	resultado.put("CS_PUNTOS_LIMITE",			csPuntosLimite				);
	resultado.put("FN_PUNTOS_LIMITE",			fnPuntosLimite				);
	resultado.put("NOMBRE_MONEDA",				nombreMoneda				);
	resultado.put("AUTORIZA",						autoriza						);
	resultado.put("TASA_A_APLICAR",				tasaAAplicar				);
	resultado.put("IC_PLAZO",						icPlazo						); // '???'
	resultado.put("PLAZO_DIAS_INICIO",			plazoDiasInicio			); // '???'
	resultado.put("PLAZO_DIAS_FIN",				plazoDiasFin				); // '???'
	resultado.put("ES_RANGO_MAXIMO",				esRangoMaximo				); // '???'
	
	// 7. Definir el estado siguiente
	estadoSiguiente = "MOSTRAR_WINDOW_MODIFICAR_TASA_BASE";
	
	// 8. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",					msg						);
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.guardarModificacionTasaBase")  ) {

	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
	boolean 		hayError 				= false;
	
	// 1. Leer parametros
	String 		fechaTasaBase			= (request.getParameter("fechaTasaBase")			== null)?"":request.getParameter("fechaTasaBase");
	String 		icEpo 					= (request.getParameter("icEpo")						== null)?"":request.getParameter("icEpo");			
	String 		icIf						= (request.getParameter("icIf")						== null)?"":request.getParameter("icIf");
	String 		relacionMatematica 	= (request.getParameter("relacionMatematica")	== null)?"":request.getParameter("relacionMatematica");
	String 		puntosAdicionales 	= (request.getParameter("puntosAdicionales")		== null)?"":request.getParameter("puntosAdicionales");
	String 		autoriza 				= (request.getParameter("autoriza")					== null)?"N":request.getParameter("autoriza");
	String 		plazoEspecifico 		= (request.getParameter("plazoEspecifico") 		== null)?"":request.getParameter("plazoEspecifico");
		
	// 2. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("CapturaTasas.guardarModificacionTasaBase(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
	
	try {
		
		if(fechaTasaBase.equals("") && icEpo.equals("") && icIf.equals("") /*&& aut_epo.equals("") && aut_if.equals("") && aut_nafin.equals("")*/) {
			msg 					= "Error al leer los parámetros.";
			estadoSiguiente 	= "ESPERAR_DECISION";
		} else {
			autorizacionTasas.setEstatusTasaAutorizadaIF(
				icIf, 
				icEpo, 
				fechaTasaBase,
				autoriza, 
				autoriza, 
				autoriza,
				relacionMatematica,
				puntosAdicionales,
				plazoEspecifico
			);
		}
		
	} catch(NafinException ne) {
		
		hayError 			= true;
		msg      			= ne.getMsgError();
		estadoSiguiente 	= "ESPERAR_DECISION";
		
	} catch (Exception e) {
		
		hayError = true;
		throw e;
		
	} 
	
	if(!hayError) {
		
		msg 					= "La modificación se realizo con éxito.";
		estadoSiguiente 	= "OCULTAR_WINDOW_MODIFICAR_TASA_BASE";
		resultado.put("ACTUALIZAR_DETALLE_TASA_POR_EPO",new Boolean(true));
		
	}
		
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.cargarGridDetalleIFRelacionar")	) { // ESTADO GRID_DETALLE_IF_RELACIONAR

	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
	
	// 1. Leer parametros de la consulta
	String 		claveEPO					= (request.getParameter("claveEPO") == null)?"":request.getParameter("claveEPO");
	
	// 2. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("CapturaTasas.cargarGridDetalleIFRelacionar(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
	
	// 3. Consultar IFs asociados
	Vector ifsPorRelacionar = autorizacionTasas.getIFXRelacionar(claveEPO);
	int numRegistros = ifsPorRelacionar.size();	
	if(numRegistros > 0){
		
		JSONArray   registros 	= new JSONArray();
		JSONObject  registro	   = null;
			
		for(int n=0; n<numRegistros; n++) { 
			Vector ifPorRelacionar 		= (Vector) ifsPorRelacionar.get(n);
			registro	 				= new JSONObject();
			registro.put("clave",		(String) ifPorRelacionar.get(0));
			registro.put("descripcion",(String) ifPorRelacionar.get(1));
			registros.add(registro);
		}
			
		JSONObject catalogoIfsRelacionados = new JSONObject();
		catalogoIfsRelacionados.put("registros",	registros								);
		catalogoIfsRelacionados.put("total",		String.valueOf(registros.size())	);
		resultado.put("catalogoIfsRelacionados",catalogoIfsRelacionados);
		
		estadoSiguiente = "CARGAR_CATALOGO_IFS_RELACIONADOS";
		
 	} else {
 	
		estadoSiguiente = "SIN_DETALLE_IF_RELACIONAR";
		
	}
 	
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.cargarCatalogoIfsRelacionados")	) { // ESTADO GRID_DETALLE_IF_RELACIONAR

	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
	
	estadoSiguiente = "MOSTRAR_GRID_DETALLE_IF_RELACIONAR";
	
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.consultarTasaBaseIfRelacionar")  ) { // ESTADO GRID_DETALLE_IF_RELACIONAR
	
	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
	
	String      gridMsg					= "";
	
	// 1. Leer parametros de la consulta
	String 		claveIF 				= (request.getParameter("claveIF")				== null)?"":request.getParameter("claveIF");
	String 		claveEPO 			= (request.getParameter("claveEPO")				== null)?"":request.getParameter("claveEPO");
	String 		ifSeleccionadoAux = (request.getParameter("ifSeleccionado")		== null)?"":request.getParameter("ifSeleccionado");
 
	JSONObject 	ifSeleccionado 	= JSONObject.fromObject(ifSeleccionadoAux);
	
	// 2. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("CapturaTasas.consultarTasaBaseIfRelacionar(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
 
	try {
	
		// Enviar la clave de la EPO
		resultado.put("claveEPO",	claveEPO);
		
		// 3. Consultar parametrizacion de los puntos límite
		String csPuntosLimite = "";
		String fnPuntosLimite = "";
		if (!claveEPO.equals("")) {
			HashMap valorPuntosLim 		= autorizacionTasas.obtenerParametrizacionPuntosLimite();
			HashMap paramPuntosLimEpo 	= autorizacionTasas.obtenerParametrizacionPuntosLimitePorEpo(claveEPO, "");
			fnPuntosLimite 				= valorPuntosLim.get("valorPuntosLimite")==null?"0.0000":(String)valorPuntosLim.get("valorPuntosLimite");
			HashMap puntosLimiteEpo 	= paramPuntosLimEpo.get("puntosLimitePorEpo0")==null?new HashMap():(HashMap)paramPuntosLimEpo.get("puntosLimitePorEpo0");
			csPuntosLimite 				= puntosLimiteEpo.get("csPuntosLimite")==null?"S":(puntosLimiteEpo.get("csPuntosLimite").toString().equals("SI")?"S":"N");
		}
		resultado.put("csPuntosLimite",csPuntosLimite);
		resultado.put("fnPuntosLimite",fnPuntosLimite);
		
		// 4. Consultar Detalle de las Tasas
		JSONArray  	detalleIFRelacionarData	= new JSONArray();
		JSONObject 	detalle				 	 	= null;
		JSONArray  	comboGrupoData				= new JSONArray();
		JSONObject 	detalleGrupo				= null;
	
		int 			m								= 0;
		int 			numRegistrosTasaBase 	= 0;
		int 			groupId						= 0;
		Vector 		vmonedasif 					= autorizacionTasas.getMonedaRelacionadaIF(claveEPO, claveIF);
		String  		intermediarioFinanciero	= ifSeleccionado.getString("descripcion");
		
		for(m=0,groupId = 0; m < vmonedasif.size(); m++,groupId++) {
			
			String ic_moneda 			 = vmonedasif.get(m).toString();
			Vector registrosTasaBase = autorizacionTasas.getTasasDisponibles(claveEPO, ic_moneda);
			
			if(registrosTasaBase.size() > 0) {
				
				detalleGrupo = new JSONObject();
				detalleGrupo.put("clave", 			new Integer(groupId) 																							 );
				detalleGrupo.put("descripcion",	intermediarioFinanciero + " ( " + ((Vector)registrosTasaBase.get(0)).get(3).toString() + " ) " );
				comboGrupoData.add(detalleGrupo);
				
				numRegistrosTasaBase += registrosTasaBase.size();
				/*
				
					// -- CAMPOS DE LA CABECERA DEL GRUPO --
					
					// "regMoneda"+ic_moneda = registrosTasaBase.size()
					// "Moneda:" = ((Vector)registrosTasaBase.get(0)).get(3).toString()
					// "rel_tasa"+ic_moneda
					// "puntos"+ic_moneda
				
				*/
				int 		i 			= 0;
				for (i = 0; i < registrosTasaBase.size(); i++) {
					
					Vector registro		= (Vector) registrosTasaBase.get(i);
					
					String nombreTasa 	= registro.get(0).toString();
					String valorTasa 		= registro.get(1).toString();
					String claveTasa		= registro.get(2).toString();
					String nombreMoneda	= registro.get(3).toString();
					String rangoPlazo		= registro.get(4).toString();
					String rs_plazo		= registro.get(5).toString();
					
					/*
					
					// -- CAMPOS DE LA TABLA --
					
					nombreTasa // Tipo Tasa
					rangoPlazo // Plazo
					Comunes.formatoDecimal(valorTasa,5) // Valor
					"+" // "rel_tasa" + ic_moneda // Relación Matemática
					""  // Puntos Adicionales // "puntos"     + ic_moneda + "_" + i // validaNumeroFlotante(this.form.name,this.name,0,5,5); recalcular(ic_moneda,i);
					""  // Tasa a Aplicar     // "valor_oper" + ic_moneda + "_" + i
 
					// -- CAMPOS HIDDEN --
					
					"valor_tasa" + ic_moneda +"_"+i = Comunes.formatoDecimal(valorTasa,5)
					"cve_tasa"  + ic_moneda  +"_"+i = claveTasa
					"cve_plazo" + ic_moneda  +"_"+i = rs_plazo
					
					CAMPO UNICO "MN" // ic_moneda == "1", valor="i"
						ó "Dolar" // ic_moneda == "54" valor="i"
					
					"noMoneda"+m = ic_moneda
					
					*/
					
					detalle = new JSONObject();
					
					detalle.put("GROUP_ID",						new Integer(groupId)												);
					detalle.put("INTERMEDIARIO_FINANCIERO",intermediarioFinanciero											);
					detalle.put("MONEDA",						((Vector)registrosTasaBase.get(0)).get(3).toString()	);
					detalle.put("TIPO_TASA",					nombreTasa															);
					detalle.put("PLAZO",							rangoPlazo															);
					detalle.put("VALOR_TASA",					Comunes.formatoDecimal(valorTasa,5)							);
					detalle.put("RELACION_MATEMATICA",		"+"																	);
					detalle.put("PUNTOS_ADICIONALES",		""																		);
					detalle.put("TASA_A_APLICAR",				""																		);
					detalle.put("IC_IF",							claveIF																);
					detalle.put("IC_MONEDA",					ic_moneda															);
					detalle.put("IC_EPO",						claveEPO																);
					detalle.put("CLAVE_TASA",					claveTasa															);
					detalleIFRelacionarData.add(detalle);
					
				}
			}
		}
		if(numRegistrosTasaBase > 0) { 
			
			JSONObject  detalleIFRelacionarDataAux = new JSONObject();
			detalleIFRelacionarDataAux.put("registros",	detalleIFRelacionarData								);
			detalleIFRelacionarDataAux.put("total",   	new Integer(detalleIFRelacionarData.size()) 	);
			resultado.put("detalleIFRelacionarData",		detalleIFRelacionarDataAux							);
 
			JSONObject  comboGrupoDataAux 			= new JSONObject();
			comboGrupoDataAux.put("registros",	comboGrupoData								);
			comboGrupoDataAux.put("total",   	new Integer(comboGrupoData.size()) 	);
			resultado.put("comboGrupoData",		comboGrupoDataAux							);
			
			resultado.put("cargarDetalle",		new Boolean(true)							);
			
		} else if(numRegistrosTasaBase==0) {
			
			gridMsg = "Debe de parametrizar monedas para poder autorizar tasas.";
			resultado.put("gridMsg",			gridMsg				);
			resultado.put("mostrarGridMsg",	new Boolean(true)	);
			
		}
		
	} catch (NafinException ne){
		
		gridMsg = ne.getMsgError();
		resultado.put("gridMsg",			gridMsg				);
		resultado.put("mostrarGridMsg",	new Boolean(true)	);
		
	} catch (Exception e){
		throw e;
	}
	
	// 4. Determinar el estado siguiente
	estadoSiguiente = "MOSTRAR_CONSULTA_TASA_BASE_IF_RELACIONAR";
	
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.guardarTasaBaseIfRelacionar")  	) { // ESTADO GRID_DETALLE_IF_RELACIONAR

	JSONObject	resultado 							= new JSONObject();
	boolean		success	 							= true;
	String 		estadoSiguiente 					= null;
	String		msg									= null;

	// 1. Leer parámetros
	String 		detalleIFRelacionarDataAux 	= (request.getParameter("detalleIFRelacionarData")		== null)?"[]"	:request.getParameter("detalleIFRelacionarData");
	String 		claveIF 								= (request.getParameter("claveIF")							== null)?""		:request.getParameter("claveIF");
	String 		claveEPO 							= (request.getParameter("claveEPO")							== null)?""		:request.getParameter("claveEPO");
	
	JSONArray 	detalleIFRelacionarData  		= JSONArray.fromObject(detalleIFRelacionarDataAux);

	// 2. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("CapturaTasas.consultarTasaBaseIfRelacionar(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
	
	try {
		
		// Obtener la fecha de las tasas
		Vector   registros 		= autorizacionTasas.getTasaBase(claveEPO);
		int      numRegistros 	= registros.size();
		String[] fechaTasas 		= new String[numRegistros];
		if (numRegistros > 0) {
			for (int b = 0; b < numRegistros; b++) {
				Vector vdatos = (Vector)registros.get(b);
				fechaTasas[b] = vdatos.get(6)+"|"+vdatos.get(5);
			}
		} else {
			throw new AppException("EL parámetro fechaTasas no puede venir vacío.");
		}
		
		// Leer los campos que fueron modificados		
		List vDatosTasaBase = new ArrayList();
		for(int i=0;i<detalleIFRelacionarData.size();i++){
			
			JSONObject registro = detalleIFRelacionarData.getJSONObject(i);
			
			String[] registroTasaBase = new String[3];
			registroTasaBase[0] = registro.getString("CLAVE_TASA"); 			  //claveTasa
			registroTasaBase[1] = registro.getString("RELACION_MATEMATICA"); //relMatTasaBase
			registroTasaBase[2] = registro.getString("PUNTOS_ADICIONALES");  //puntosTasaBase
			vDatosTasaBase.add(registroTasaBase);
			
		}
		
		// Asignar Nueva Relacion IF
		autorizacionTasas.setRelacion(claveEPO, claveIF, fechaTasas, vDatosTasaBase);
			
		// 4. Determinar el estado siguiente
		resultado.put("ACTUALIZAR_DETALLE_TASA_POR_EPO",new Boolean(success));
		msg 					= "Se agregó la relación del IF con éxito.";
		estadoSiguiente 	= "OCULTAR_GRID_DETALLE_IF_RELACIONAR";
	
	} catch(NafinException ne){
		msg = ne.getMsgError();
		estadoSiguiente = "ESPERAR_DECISION";
	} catch(Exception e){
		throw e;
	}
 
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();

} else if(  informacion.equals("CapturaTasas.cargarGridDetalleNuevaTasaBase")	) { // ESTADO GRID_DETALLE_NUEVA_TASA_BASE

	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
	
	// 1. Leer parametros de la consulta
	String 		claveEPO					= (request.getParameter("claveEPO") == null)?"":request.getParameter("claveEPO");
	
	// 2. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("CapturaTasas.cargarGridDetalleNuevaTasaBase(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
	
	// 3. Consultar IFs asociados
	Vector 		ifsAsociados	= autorizacionTasas.getIFAsociados(claveEPO);
	int 			numRegistros	= ifsAsociados.size();	
 
	JSONArray   registros 		= new JSONArray();
	JSONObject  registro	   	= null;
		
	registro	 						= new JSONObject();
	registro.put("clave",		"TODOS");
	registro.put("descripcion","TODOS");
	registros.add(registro);
			
	for(int n=0; n<numRegistros; n++) { 
		Vector ifPorRelacionar 		= (Vector) ifsAsociados.get(n);
		registro	 						= new JSONObject();
		registro.put("clave",		(String) ifPorRelacionar.get(0));
		registro.put("descripcion",(String) ifPorRelacionar.get(1));
		registros.add(registro);
	}
			
	JSONObject catalogoIfsAsociados = new JSONObject();
	catalogoIfsAsociados.put("registros",	registros								);
	catalogoIfsAsociados.put("total",		String.valueOf(registros.size())	);
	resultado.put("catalogoIfsAsociados",catalogoIfsAsociados);
		
	estadoSiguiente = "CARGAR_CATALOGO_IFS_NUEVA_TASA_BASE";
 
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.cargarCatalogoIfsNuevaTasaBase")	) { // ESTADO GRID_DETALLE_NUEVA_TASA_BASE

	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
	
	estadoSiguiente = "MOSTRAR_GRID_DETALLE_NUEVA_TASA_BASE";
	
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.mostrarGridDetalleNuevaTasaBase")   ) { // ESTADO GRID_DETALLE_NUEVA_TASA_BASE
	
	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
	
	estadoSiguiente = "CONSULTAR_TASA_BASE_NUEVA";
	resultado.put("consultarTodos",new Boolean(true));
	
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
 
} else if(  informacion.equals("CapturaTasas.consultarTasaBaseNueva")  				) { // ESTADO GRID_DETALLE_NUEVA_TASA_BASE
	
	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;
	
	String      gridMsg					= "";
	int 			numRegistrosTasaBase = 0;
	
	// 1. Leer parametros de la consulta
	String 		claveIF 					= (request.getParameter("claveIF")				== null)?"":request.getParameter("claveIF");
	String 		claveEPO 				= (request.getParameter("claveEPO")				== null)?"":request.getParameter("claveEPO");
	String 		ifSeleccionadosAux 	= (request.getParameter("ifSeleccionados")	== null)?"":request.getParameter("ifSeleccionados");
 
	JSONArray 	ifSeleccionados 		= JSONArray.fromObject(ifSeleccionadosAux);
	
	// 2. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("CapturaTasas.consultarTasaBaseNueva(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
	
	// Consultar Detalle de las Tasas
	try {
		
		// 3. Enviar la clave de la EPO
		resultado.put("claveEPO",	claveEPO);
		
		// 4. Consultar parametrizacion de los puntos límite
		String csPuntosLimite = "";
		String fnPuntosLimite = "";
		if (!claveEPO.equals("")) {
			HashMap valorPuntosLim 		= autorizacionTasas.obtenerParametrizacionPuntosLimite();
			HashMap paramPuntosLimEpo 	= autorizacionTasas.obtenerParametrizacionPuntosLimitePorEpo(claveEPO, "");
			fnPuntosLimite 				= valorPuntosLim.get("valorPuntosLimite")==null?"0.0000":(String)valorPuntosLim.get("valorPuntosLimite");
			HashMap puntosLimiteEpo 	= paramPuntosLimEpo.get("puntosLimitePorEpo0")==null?new HashMap():(HashMap)paramPuntosLimEpo.get("puntosLimitePorEpo0");
			csPuntosLimite 				= puntosLimiteEpo.get("csPuntosLimite")==null?"S":(puntosLimiteEpo.get("csPuntosLimite").toString().equals("SI")?"S":"N");
		}
		resultado.put("csPuntosLimite",csPuntosLimite);
		resultado.put("fnPuntosLimite",fnPuntosLimite);
		
		// 5. Consultar Detalle de las Tasas
		JSONArray  	detalleNuevaTasaBaseData	= new JSONArray();
		JSONObject 	detalle				 	 		= null;
		JSONArray  	comboGrupoData					= new JSONArray();
		JSONObject 	detalleGrupo					= null;
		int 			groupId							= 0;
		
		for(int a=0;a<ifSeleccionados.size();a++){
			
			JSONObject 	ifSeleccionado = ifSeleccionados.getJSONObject(a);
			
			String 		iefe 							= ifSeleccionado.getString("clave");
			String 		intermediarioFinanciero = ifSeleccionado.getString("descripcion");
			Vector 		vmonedasif 					= autorizacionTasas.getMonedaRelacionadaIF(claveEPO, iefe);
			
			for(int m=0; m < vmonedasif.size(); m++,groupId++) {
				
				String 	ic_moneda 			= vmonedasif.get(m).toString();
				Vector 	registrosTasaBase = autorizacionTasas.getTasasDisponibles(claveEPO, ic_moneda);
				
				if(registrosTasaBase.size() > 0) {
					
					detalleGrupo = new JSONObject();
					detalleGrupo.put("clave", 			new Integer(groupId) 																							 );
					detalleGrupo.put("descripcion",	intermediarioFinanciero + " ( " + ((Vector)registrosTasaBase.get(0)).get(3).toString() + " ) " );
					comboGrupoData.add(detalleGrupo);
				
					numRegistrosTasaBase += registrosTasaBase.size();
					
					
					String moneda						 = ((Vector)registrosTasaBase.get(0)).get(3).toString();
 
					for (int i = 0; i < registrosTasaBase.size(); i++) {
						
						Vector registro 	= (Vector) registrosTasaBase.get(i);
						
						String nombreTasa 		= registro.get(0).toString();
						String valorTasa 			= registro.get(1).toString();
						String claveTasa 			= registro.get(2).toString();
						String nombreMoneda 		= registro.get(3).toString();
						String rangoPlazo 		= registro.get(4).toString();
						String rs_plazo 			= registro.get(5).toString();
						
						/*
						
							nombreTasa 									// Tipo Tasa
							rangoPlazo 									// Plazo
							Comunes.formatoDecimal(valorTasa,5) // Valor
							"+"											// relacionMatematica
							"" 											// Puntos Adicionales
							""    										// Tasa a Aplicar
							
							Comunes.formatoDecimal(valorTasa,5) // valorTasa 
							claveTasa									// claveTasa
							rs_plazo										// cve_plazo
							ic_moneda									// ic_moneda
							ic_if											// iefe
						
						*/
						
						detalle = new JSONObject();
						
						detalle.put("GROUP_ID",							new Integer(groupId)						);
						detalle.put("INTERMEDIARIO_FINANCIERO",	intermediarioFinanciero					);
						detalle.put("MONEDA",							moneda										);
						detalle.put("TIPO_TASA",						nombreTasa									);
						detalle.put("PLAZO",								rangoPlazo									);
						detalle.put("VALOR_TASA",						Comunes.formatoDecimal(valorTasa,5) );
						detalle.put("RELACION_MATEMATICA",			"+"											);
						detalle.put("PUNTOS_ADICIONALES",			""												);
						detalle.put("TASA_A_APLICAR",					""												);
						detalle.put("IC_MONEDA",						ic_moneda									);
						detalle.put("IC_IF",								iefe											);
						detalle.put("IC_EPO",							claveEPO										);
						detalle.put("CLAVE_TASA",						claveTasa									);
						detalle.put("CLAVE_PLAZO",						rs_plazo										);
						detalleNuevaTasaBaseData.add(detalle);
						
					}
				}
				
			}
		}
 
		// 6. Obtener número de registros tipificados.
		Vector registrosTipificados 		= autorizacionTasas.getTasasPorClasificacion(claveEPO);
		int 	 numRegistrosTipificados 	= registrosTipificados.size();
		resultado.put("numRegistrosTipificados",new Integer(numRegistrosTipificados));
	 
		if(	     numRegistrosTasaBase >  0 ){
			
			// Habilitar Boton Autorizar Tasas
			// Habilitar Boton Cancelar
			JSONObject  detalleNuevaTasaBaseDataAux = new JSONObject();
			detalleNuevaTasaBaseDataAux.put("registros",	detalleNuevaTasaBaseData								);
			detalleNuevaTasaBaseDataAux.put("total",   	new Integer(detalleNuevaTasaBaseData.size()) 	);
			resultado.put("detalleNuevaTasaBaseData",		detalleNuevaTasaBaseDataAux							);
 
			JSONObject  comboGrupoDataAux 			= new JSONObject();
			comboGrupoDataAux.put("registros",	comboGrupoData								);
			comboGrupoDataAux.put("total",   	new Integer(comboGrupoData.size()) 	);
			resultado.put("comboGrupoData",		comboGrupoDataAux							);
			
			resultado.put("cargarDetalle",		new Boolean(true)							);
			
		} else if( numRegistrosTasaBase == 0 ){
			
			// Deshabilitar Boton Autorizar Tasas
			// Habilitar Boton Cancelar
			gridMsg = "Debe de parametrizar monedas para poder autorizar tasas.";
			resultado.put("gridMsg",			gridMsg				);
			resultado.put("mostrarGridMsg",	new Boolean(true)	);
			
		} 
	
	} catch (NafinException ne){
		
		gridMsg = ne.getMsgError();
		resultado.put("gridMsg",			gridMsg				);
		resultado.put("mostrarGridMsg",	new Boolean(true)	);
		
	} catch (Exception e){
		
		throw e;
		
	}
	
	// 7. Determinar el estado siguiente
	estadoSiguiente = "MOSTRAR_CONSULTA_TASA_BASE_NUEVA";
	
	// 8. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
		
} else if(  informacion.equals("CapturaTasas.guardarNuevasTasasBase")  	) { // ESTADO GRID_DETALLE_NUEVA_TASA_BASE

	JSONObject	resultado 							= new JSONObject();
	boolean		success	 							= true;
	String 		estadoSiguiente 					= null;
	String		msg									= null;

	// 1. Leer parámetros
	String 		detalleNuevaTasaBaseDataAux 	= (request.getParameter("detalleNuevaTasaBaseData")	== null)?"[]"	:request.getParameter("detalleNuevaTasaBaseData");
	String 		claveEPO 							= (request.getParameter("claveEPO")							== null)?""		:request.getParameter("claveEPO");
	String 		numRegistrosTipificadosAux		= (request.getParameter("numRegistrosTipificados")		== null)?"":request.getParameter("numRegistrosTipificados");
	
	JSONArray 	detalleNuevaTasaBaseData  		= JSONArray.fromObject(detalleNuevaTasaBaseDataAux);
	
	// 2. Obtener instancia del EJB de Autorizacion
	AutorizacionTasas autorizacionTasas = null;
	try {
				
		autorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	} catch(Exception e) {
	 
		log.error("CapturaTasas.guardarNuevasTasasBase(Exception): Obtener instancia del EJB de  Autorización Tasas");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Autorización Tasas.");
		
	}
	
	try {
 
		Vector 	vClavesSelIF 				= new Vector();
		Vector 	vDatosTasaBase 			= new Vector();
		Vector 	vDatosTipificados 		= new Vector();
		Vector 	vDatosTasaIf 				= new Vector();
	
		int 		numRegistrosTipificados = Integer.parseInt(numRegistrosTipificadosAux);
	
		//GEAG. Se cambia el esquema para llenar la lista de vDatosTasaBase, debido a que si el primer IF no maneja las dos monedas, solo se van a procesar aquellas que tenga para toooodos los registros.
		
		// 1. Obtener clave del Primer IF
		String clavePrimerIF = "";
		for(int i=0;i<detalleNuevaTasaBaseData.size();i++){
			
			JSONObject registro	= detalleNuevaTasaBaseData.getJSONObject(i);
			clavePrimerIF 			= registro.getString("IC_IF");
			break;
			
		}
		
		// 2. Llenar Datos Tasa Base para Moneda Nacional
		String 	MONEDA_NACIONAL 			= "1";
		boolean 	tasasBasesObtenidasMN 	= false;
		for(int i=0;i<detalleNuevaTasaBaseData.size();i++){
			
			JSONObject 	registro 			= detalleNuevaTasaBaseData.getJSONObject(i);
 
			String 		claveIF				= registro.getString("IC_IF");
			String 		claveMoneda			= registro.getString("IC_MONEDA");
			
			if( claveIF.equals(clavePrimerIF) && claveMoneda.equals(MONEDA_NACIONAL) ){
			
				String[] 	registroTasaBase 	= new String[4];
				registroTasaBase[0] = registro.getString("CLAVE_TASA"); //claveTasa
				registroTasaBase[1] = "+"; //request.getParameter("rel_tasa"+iefe[a]+"_1"); //OBSOLETO.... NO IMPORTA EL VALOR
				registroTasaBase[2] = "0"; //request.getParameter("puntos"+iefe[a]+"_1"); //OBSOLETO.... NO IMPORTA EL VALOR
				registroTasaBase[3] = registro.getString("CLAVE_PLAZO"); //cve_plazo
				vDatosTasaBase.add(registroTasaBase);
				
				tasasBasesObtenidasMN = true;
		
			}
			
		}
		
		// 3. Llenar Datos Tasa Base para Dólar Americano
		String 	DOLAR_AMERICANO 			= "54";
		boolean 	tasasBasesObtenidasDL 	= false;
		for(int i=0;i<detalleNuevaTasaBaseData.size();i++){
			
			JSONObject 	registro 			= detalleNuevaTasaBaseData.getJSONObject(i);
 
			String 		claveIF				= registro.getString("IC_IF");
			String 		claveMoneda			= registro.getString("IC_MONEDA");
			
			if( claveIF.equals(clavePrimerIF) && claveMoneda.equals(DOLAR_AMERICANO) ){
			
				String[] 	registroTasaBase 	= new String[4];
				registroTasaBase[0] = registro.getString("CLAVE_TASA"); //claveTasa
				registroTasaBase[1] = "+"; //request.getParameter("rel_tasa"+iefe[a]+"_1"); //OBSOLETO.... NO IMPORTA EL VALOR
				registroTasaBase[2] = "0"; //request.getParameter("puntos"+iefe[a]+"_1"); //OBSOLETO.... NO IMPORTA EL VALOR
				registroTasaBase[3] = registro.getString("CLAVE_PLAZO"); //cve_plazo
				vDatosTasaBase.add(registroTasaBase);
				
				tasasBasesObtenidasDL = true;
				
			}
			
		}
		
		
		//4. llenando datos para if
		//Moneda Nacional	
		boolean 	existenTasasMN	 = false;
		for(int i=0;i<detalleNuevaTasaBaseData.size();i++){
			
			JSONObject 	registro 			= detalleNuevaTasaBaseData.getJSONObject(i);
 
			String 		claveIF				= registro.getString("IC_IF");
			String 		claveMoneda			= registro.getString("IC_MONEDA");
			
			if( claveMoneda.equals(MONEDA_NACIONAL) ){
			
				String[] registroTasaIF = new String[4];
				registroTasaIF[0] = claveIF;
				registroTasaIF[1] = registro.getString("CLAVE_TASA"); //claveTasa
				registroTasaIF[2] = registro.getString("RELACION_MATEMATICA"); //relMatTasaBase
				registroTasaIF[3] = registro.getString("PUNTOS_ADICIONALES"); //puntosTasaBase
				vDatosTasaIf.add(registroTasaIF);
				
				existenTasasMN	 = true;
				
			}
			
		}
		
		//5. llenando datos para if
		//Dolar.
		boolean 	existenTasasDL 			= false;
		for(int i=0;i<detalleNuevaTasaBaseData.size();i++){
			
			JSONObject 	registro 			= detalleNuevaTasaBaseData.getJSONObject(i);
 
			String 		claveIF				= registro.getString("IC_IF");
			String 		claveMoneda			= registro.getString("IC_MONEDA");
			
			if( claveMoneda.equals(DOLAR_AMERICANO) ){
			
				String[] registroTasaIF = new String[4];
				registroTasaIF[0] = claveIF;
				registroTasaIF[1] = registro.getString("CLAVE_TASA"); //claveTasa
				registroTasaIF[2] = registro.getString("RELACION_MATEMATICA"); //relMatTasaBase
				registroTasaIF[3] = registro.getString("PUNTOS_ADICIONALES"); //puntosTasaBase
				vDatosTasaIf.add(registroTasaIF);
				
				existenTasasDL = true;
				
			}
			
		}
 
		// 6. Agregar Nuevas Tasas
		autorizacionTasas.setTasas(claveEPO, vDatosTasaBase, vDatosTasaIf);
	
		// 7. Determinar el estado siguiente
		resultado.put("claveEPO",claveEPO);
		resultado.put("actualizarDetalleTasaPorEpo", new Boolean(success)	);
		msg 					= "La captura de los valores de las tasas se realizó con éxito.";
		estadoSiguiente 	= "OCULTAR_GRID_DETALLE_NUEVA_TASA_BASE";
	
	} catch(NafinException ne){
		msg = ne.getMsgError();
		estadoSiguiente = "ESPERAR_DECISION";
	} catch(Exception e){
		throw e;
	}
 
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();
	
} else if(  informacion.equals("CapturaTasas.ocultarGridDetalleNuevaTasaBase")	) { 

	JSONObject	resultado 				= new JSONObject();
	boolean		success	 				= true;
	String 		estadoSiguiente 		= null;
	String		msg						= null;

	String		claveEPO 						 = (request.getParameter("claveEPO") == null)?"":request.getParameter("claveEPO");
	boolean 		actualizarDetalleTasaPorEpo = "true".equals(request.getParameter("actualizarDetalleTasaPorEpo"))?true:false;
	
	if( actualizarDetalleTasaPorEpo ){
		resultado.put("claveEPO",			claveEPO);
		estadoSiguiente = "CONSULTA_TASA_BASE_POR_EPO";
	} else {
		estadoSiguiente = "ESPERAR_DECISION";
	}
	
	// 3. Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("msg",						msg						);
	
	infoRegresar = resultado.toString();

} else if(  informacion.equals("fin")                        						) {
	
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}

//log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>