<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.sql.*, 
		java.math.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		com.netro.parametrosgrales.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.xls.ComunesXLS,
                org.apache.commons.codec.binary.Base64,
		com.netro.promoepo.webservice.edocuenta.EdoCuentaWSInfo,
		com.netro.promoepo.webservice.edocuenta.EdoCuentaWSClient"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%!private String getDiaMesyAnio(String strFecha){
		String []fecha = strFecha.split("/");
		String dia = fecha[0];
		String mes  = "DESCONOCIDO";
		String anio = fecha[2];
		
		int intMes = Integer.parseInt(fecha[1]);
		switch(intMes){
			case 1:  mes = "ENERO"; 		break;
			case 2:  mes = "FEBRERO"; 		break;
			case 3:  mes = "MARZO"; 		break;
			case 4:  mes = "ABRIL"; 		break;
			case 5:  mes = "MAYO"; 			break;
			case 6:  mes = "JUNIO"; 		break;
			case 7:  mes = "JULIO"; 		break;
			case 8:  mes = "AGOSTO"; 		break;
			case 9:  mes = "SEPTIEMBRE"; 	break;
			case 10: mes = "OCTUBRE"; 		break;
			case 11: mes = "NOVIEMBRE"; 	break;
			case 12: mes = "DICIEMBRE"; 	break;
		}
		return dia + " DE " + mes + " DE " + anio;
		
	}
	
	private String getNumeroDeCuatroDigitos(String numero){
		String 			claveEPO 	= (numero == null )?"":numero.trim();
		StringBuffer 	resultado 	= new StringBuffer();

		for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
		}
		resultado.append(claveEPO);

		return resultado.toString();
	}

  	private String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
		StringBuffer 	resultado 	= new StringBuffer();

		for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
		}
		resultado.append(claveDocto);
		return resultado.toString();
  }
%>
<%
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo") != null)?request.getParameter("ic_banco_fondeo"):"";
	String ic_epo = (request.getParameter("ic_epo") != null)?request.getParameter("ic_epo"):"";
	
	
	String infoRegresar = "";
	String fechaActual   = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String mesActual  = fechaActual.substring(3,5);
	String anioActual = fechaActual.substring(6);
	InformacionTOIC informacionTOICBean = ServiceLocator.getInstance().lookup("InformacionTOICEJB", InformacionTOIC.class);
	String esPromotorNafin = ("NAFIN".equals(strTipoUsuario) && "PROMO NAFIN".equals(strPerfil))?"S":"N";
	
	if(informacion.equals("valoresIniciales")){
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("strDirectorioTemp", strDirectorioTemp);
		jsonObj.put("epo",iNoCliente);
		jsonObj.put("mesActual",new Integer(mesActual));
		jsonObj.put("anioActual",new Integer(anioActual));
		jsonObj.put("strTipoUsuario",strTipoUsuario);
		jsonObj.put("esPromotorNafin",esPromotorNafin);
		
		infoRegresar = jsonObj.toString();
	}else  if(informacion.equals("CatalogoBanco")) {

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_banco_fondeo");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_banco_fondeo");	
	if("ADMIN BANCOMEXT".equals(strPerfil)) {
		catalogo.setValoresCondicionIn("2", Integer.class);
	}else {
		catalogo.setValoresCondicionIn("1,2", Integer.class);
	}
	infoRegresar = catalogo.getJSONElementos();
	
}else  if(informacion.equals("catalogoEPO")) {

	CatalogoEPO catalogo = new CatalogoEPO(); 
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setClaveBancoFondeo(ic_banco_fondeo);
	
	infoRegresar = catalogo.getJSONElementos();

	}else if(informacion.equals("permisoConsulta")){
		try{
			ParametrosGrales parametrosbean = ServiceLocator.getInstance().lookup("ParametrosGralesEJB", ParametrosGrales.class);
			HashMap 	permisoConsultaReportesEPO = parametrosbean.getPermisoConsultaReportesEPO();
			String permisoConsulta = (String)permisoConsultaReportesEPO.get("PERMISO_CONSULTA");
			String horaReporte = (String)permisoConsultaReportesEPO.get("HORA_REPORTE");
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("PERMISO_CONSULTA",permisoConsulta);
			jsonObj.put("HORA_REPORTE",horaReporte);
			infoRegresar = jsonObj.toString();
		}catch(Throwable e){
			throw new AppException("Ocurrió un error al inicializar la pantalla", e);
		}
	}
	else if(informacion.equals("catalogoMes")){
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		JSONArray jsonArr = new JSONArray();
		JSONObject jsonObj = new JSONObject();
		for(int imes=1;imes<=12;imes++){
			jsonObj.put("descripcion",meses[imes-1]);
			jsonObj.put("clave",Integer.toString(imes));
			jsonArr.add(jsonObj);
		}
		infoRegresar = "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}
	else if(informacion.equals("catalogoAnio")){
		JSONArray jsonArr = new JSONArray();
		JSONObject jsonObj = new JSONObject();
		for(int ianio=2008;ianio<=Integer.parseInt(anioActual);ianio++){
			jsonObj.put("descripcion",Integer.toString(ianio));
			jsonObj.put("clave",Integer.toString(ianio));
			jsonArr.add(jsonObj);
		}
		infoRegresar = "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}
	else if(informacion.equals("resumenOperacion")){
		String mesReporte  = (request.getParameter("mes")  == null)?"1"    : request.getParameter("mes");
		String anioReporte = (request.getParameter("anio") == null)?"9999" : request.getParameter("anio");
		try{
			CreaArchivo        archivo            = null;
			String             nombreArchivo      = null;

			HashMap            reporteEdoCuenta   = new HashMap();

			String             fechaCorte         = null;
			String             codigoEjecucion    = null;
			EdoCuentaWSInfo    info               = null;
			EdoCuentaWSClient edoCuentaWSCliente = null;
			
			try {
				archivo = new CreaArchivo();
				nombreArchivo = archivo.nombreArchivo()+".pdf";
				// 1. Obtener nombre del archivo consultado
				fechaCorte = mesReporte + "/" + anioReporte;
				// 2. Consultar reporte
				edoCuentaWSCliente = new EdoCuentaWSClient();	
				if(strTipoUsuario.equals("NAFIN")){
					info = edoCuentaWSCliente.consultarEstadoCuenta(ic_epo, fechaCorte);
				}else {
					info = edoCuentaWSCliente.consultarEstadoCuenta(iNoCliente, fechaCorte);	
				}
			// 3. Crear archivo
				codigoEjecucion = info.getCodigoEjecucion();
				
				if( codigoEjecucion.equals(EdoCuentaWSInfo.SIN_ERROR) ){
					FileOutputStream fos = null;
					try {
						fos = new FileOutputStream(strDirectorioTemp+nombreArchivo);
						byte[] contenidoArchivoPDF = Base64.decodeBase64(info.getEdoCuenta().getBytes());
						fos.write(contenidoArchivoPDF);
						fos.flush();
					}catch(Exception e){
						reporteEdoCuenta.put("ERROR","true");
						reporteEdoCuenta.put("MENSAJE_ERROR","Ocurrió un error al crear el archivo con el reporte.");
					}finally{
						if(fos != null ){ fos.close(); }
					}
				}else if( codigoEjecucion.equals(EdoCuentaWSInfo.CON_ERROR) ){
					String[] datosMensaje = info.getResumenEjecucion().split("\\|");
					String 	mensajeError = datosMensaje.length > 1?datosMensaje[1]:"Ocurrió un error al consultar el reporte, por favor contacte al administrador.";
					
					reporteEdoCuenta.put("ERROR",				"true");
					reporteEdoCuenta.put("MENSAJE_ERROR",	mensajeError);					
				}
				reporteEdoCuenta.put("NOMBRE_ARCHIVO",nombreArchivo);
				
			}catch(Exception e){
				reporteEdoCuenta.put("ERROR","true");
				reporteEdoCuenta.put("MENSAJE_ERROR","Ocurrió un error al consultar el reporte, por favor contacte al administrador.");
				if( reporteEdoCuenta.get("NOMBRE_ARCHIVO") == null){
					nombreArchivo = nombreArchivo == null || nombreArchivo.trim().equals("")?"Reporte con Error.pdf":nombreArchivo;	
					reporteEdoCuenta.put("NOMBRE_ARCHIVO",nombreArchivo);
				}				
			}
			boolean	hayError	= "true".equals((String) reporteEdoCuenta.get("ERROR"))?true:false;
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			if(!hayError){
				nombreArchivo = (String) reporteEdoCuenta.get("NOMBRE_ARCHIVO");
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				jsonObj.put("mensaje", "");
			}else{
				String mensajeError 	= (String) reporteEdoCuenta.get("MENSAJE_ERROR");
				mensajeError  			= mensajeError == null?"Mensaje no encontrado":mensajeError.replaceAll("\"","\\\"");
				jsonObj.put("urlArchivo", "");
				jsonObj.put("mensaje", mensajeError);
			}
			infoRegresar = jsonObj.toString();
		}catch(Exception e){
			throw new AppException("Error al generar el PDF", e);	
		}
	}else if(informacion.equals("generaXlsResumenPymes")){
		String nombreArchivo = "";
		HashMap resumenPyme = null;
		List resumenMonedaNacional = null;
		List resumenDolar = null;
		HashMap totalMN = null;
		HashMap totalDolar = null;
		String fecha_publicacion_de = (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		String fecha_publicacion_a = (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		
		CreaArchivo archivo = new CreaArchivo();
		
		resumenPyme = informacionTOICBean.getResumenPymes(ic_epo, fecha_publicacion_de, fecha_publicacion_a);
		resumenMonedaNacional = (ArrayList)resumenPyme.get("MONEDANACIONAL");
		resumenDolar = (ArrayList)resumenPyme.get("DOLAR");
		totalMN = (HashMap)resumenPyme.get("TOTALMONEDANACIONAL");
		totalDolar = (HashMap)resumenPyme.get("TOTALDOLAR");
		
		nombreArchivo = archivo.nombreArchivo()+".xls";
		ComunesXLS xlsDoc = new ComunesXLS(strDirectorioTemp+nombreArchivo, "REPORTE ANALITICO DE DOCUMENTOS");
		
		//ENCABEZADO GENERAL
		xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 18);
		xlsDoc.setCelda("OPERACION DE " + resumenPyme.get("EPO") + " DURANTE EL PERIODO DE " + getDiaMesyAnio(fecha_publicacion_de) + " AL " + getDiaMesyAnio(fecha_publicacion_a), "formast", ComunesXLS.LEFT, 18);
		//ENCABEZADO DE LA TABLA
		xlsDoc.setTabla(9);
		xlsDoc.setCelda("NUM. IC_PYME","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("RAZON SOCIAL EMPRESA","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("HABILITADA","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("DOCUMENTOS PUBLICADOS","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("MONTO PUBLICADO","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("DOCUMENTOS OPERADOS","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("MONTO DOCUMENTOS OPERADOS","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("INTERESES","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("MONTO OPERADO EN FACTORAJE","formasb", ComunesXLS.CENTER);
		
		//REPORTE MONEDA NACIONAL
		if(resumenMonedaNacional.size()>0){
			for(int i=0; i<resumenMonedaNacional.size(); i++){
				HashMap pyme = (HashMap)resumenMonedaNacional.get(i);					
				xlsDoc.setCelda(pyme.get("NUM_PYME"));
				xlsDoc.setCelda(pyme.get("RAZON_SOCIAL"));
				xlsDoc.setCelda(pyme.get("HABILITADA"));
				xlsDoc.setCelda(pyme.get("DOCUMENTOS_PUBLICADOS"));
				xlsDoc.setCelda(pyme.get("MONTO_PUBLICADO"));
				xlsDoc.setCelda(pyme.get("DOCUMENTOS"));
				xlsDoc.setCelda(pyme.get("MONTO_OPERADOS"));
				xlsDoc.setCelda(pyme.get("INTERESES"));
				xlsDoc.setCelda(pyme.get("MONTO_FACTORAJE"));
			}
		}
		
		//TOTAL MONEDA NACIONAL
		xlsDoc.setCelda("");
		xlsDoc.setCelda("TOTAL M.N ", "formasb", ComunesXLS.LEFT, 2);
		xlsDoc.setCelda(totalMN.get("DOCUMENTOS"));
		xlsDoc.setCelda(totalMN.get("MONTO"));
		xlsDoc.setCelda(totalMN.get("DOCUMENTOSOPE"));
		xlsDoc.setCelda(totalMN.get("MONTO_OPERADOS"));
		xlsDoc.setCelda(totalMN.get("INTERESES"));
		xlsDoc.setCelda(totalMN.get("MONTO_FACTORAJE"));
		
		xlsDoc.setCelda("", "formasb", ComunesXLS.LEFT, 18);
		
		//REPORTE DOLAR
		if(resumenDolar.size()>0){
			for(int i=0; i<resumenDolar.size(); i++){
				HashMap pyme = (HashMap)resumenDolar.get(i);					
				xlsDoc.setCelda(pyme.get("NUM_PYME"));
				xlsDoc.setCelda(pyme.get("RAZON_SOCIAL"));
				xlsDoc.setCelda(pyme.get("HABILITADA"));
				xlsDoc.setCelda(pyme.get("DOCUMENTOS_PUBLICADOS"));
				xlsDoc.setCelda(pyme.get("MONTO_PUBLICADO"));
				xlsDoc.setCelda(pyme.get("DOCUMENTOS"));
				xlsDoc.setCelda(pyme.get("MONTO_OPERADOS"));
				xlsDoc.setCelda(pyme.get("INTERESES"));
				xlsDoc.setCelda(pyme.get("MONTO_FACTORAJE"));
			}
		}
		
		
		//TOTALDOLAR
		xlsDoc.setCelda("");
		xlsDoc.setCelda("TOTAL DOLAR ", "formasb", ComunesXLS.LEFT, 2);
		xlsDoc.setCelda(totalDolar.get("DOCUMENTOS"));
		xlsDoc.setCelda(totalDolar.get("MONTO"));
		xlsDoc.setCelda(totalDolar.get("DOCUMENTOSOPE"));
		xlsDoc.setCelda(totalDolar.get("MONTO_OPERADOS"));
		xlsDoc.setCelda(totalDolar.get("INTERESES"));
		xlsDoc.setCelda(totalDolar.get("MONTO_FACTORAJE"));
		
		xlsDoc.cierraTabla();
		xlsDoc.cierraXLS();
		///////////////////////////////////////////////////////////////////////XLS/////////////////////////////////////////////////////////////////////////////////
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("generaXlsResumenAnalitico")){
		String fecha_publicacion_de = (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		String fecha_publicacion_a = (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		String nombreArchivo = "";
		HashMap resumenDocto = null;
		List resumenMonedaNacional = null;
		List resumenDolar = null;
		HashMap totalMN = null;
		HashMap totalDolar = null;
		CreaArchivo archivo = new CreaArchivo();
		
		resumenDocto = informacionTOICBean.getReporteAnaliticoDoctos(ic_epo, fecha_publicacion_de, fecha_publicacion_a);
		nombreArchivo = archivo.nombreArchivo()+".xls";
		ComunesXLS xlsDoc = new ComunesXLS(strDirectorioTemp+nombreArchivo, "REPORTE ANALITICO DE DOCUMENTOS");
			
		resumenMonedaNacional = (ArrayList)resumenDocto.get("MONEDANACIONAL");
		resumenDolar = (ArrayList)resumenDocto.get("DOLAR");
		totalMN = (HashMap)resumenDocto.get("TOTALMONEDANACIONAL");
		totalDolar = (HashMap)resumenDocto.get("TOTALDOLAR");
		
		//ENCABEZADO GENERAL
		xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 19);
		xlsDoc.setCelda("DOCUMENTOS PUBLICADOS Y OPERADOS EN " + resumenDocto.get("EPO") + " DURANTE EL PERIODO DE " + getDiaMesyAnio(fecha_publicacion_de) + " AL " + getDiaMesyAnio(fecha_publicacion_a), "formast", ComunesXLS.LEFT, 19);
		//ENCABEZADO DE LA TABLA
		xlsDoc.setTabla(19);
		xlsDoc.setCelda("NUM. IC_PYME","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("RAZON SOCIAL EMPRESA","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("NUMERO DE DOCUMENTO","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("HABILITADA","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("FECHA DE EMISION DEL DOCUMENTO","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("FECHA DE PUBLICACION","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("FECHA DE VENCIMIENTO","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("PLAZO PROMEDIO DE REGISTRO","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("PLAZO PROMEDIO DE PAGO","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("MONEDA","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("FECHA DE ENTREGA","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("TIPO DE COMPRA","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("CLAVE PRESUPUESTAL","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("PERIODO","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("DIGITO IDENTIFICADOR","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("MONTO PUBLICADO","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("INTERESES","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("MONTO OPERADO EN FACTORAJE","formasb", ComunesXLS.CENTER);
		xlsDoc.setCelda("FECHA AUTORIZACION IF","formasb", ComunesXLS.CENTER); // Fodea 040 - 2011
		
		//REPORTE MONEDA NACIONAL
		if(resumenMonedaNacional.size()>0){
			for(int i=0; i<resumenMonedaNacional.size(); i++){
				HashMap pyme = (HashMap)resumenMonedaNacional.get(i);					
				xlsDoc.setCelda(pyme.get("NUM_PYME"));
				xlsDoc.setCelda(pyme.get("RAZON_SOCIAL"));
				xlsDoc.setCelda(pyme.get("NUMERODOC"));
				xlsDoc.setCelda(pyme.get("HABILITADA"));
				xlsDoc.setCelda(pyme.get("DFEMISION"));
				xlsDoc.setCelda(pyme.get("DFPUBLICACION"));
				xlsDoc.setCelda(pyme.get("DFVENCIMIENTO"));
				xlsDoc.setCelda(pyme.get("PLAZOREGISTRO"));
				xlsDoc.setCelda(pyme.get("PLAZOPAGO"));
				xlsDoc.setCelda(pyme.get("MONEDA"));
				xlsDoc.setCelda(pyme.get("FECHAENTREGA"));
				xlsDoc.setCelda(pyme.get("TIPOCOMPRA"));
				xlsDoc.setCelda(pyme.get("CLAVEPRESUPUESTARIA"));
				xlsDoc.setCelda(pyme.get("PERIODO"));
				xlsDoc.setCelda(getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(pyme.get("ICDOCTO").toString()));
				xlsDoc.setCelda(pyme.get("MONTOPUBLICADO"));
				xlsDoc.setCelda(pyme.get("INTERESES"));
				xlsDoc.setCelda(pyme.get("MONTOOPERADO"));
				xlsDoc.setCelda(pyme.get("FECHAAUTORIZACIONIF")); // Fodea 040 - 2011
			}
		}
		
		//TOTAL MONEDA NACIONAL
		xlsDoc.setCelda("");
		xlsDoc.setCelda("TOTAL MONEDA NACIONAL", "formasb", ComunesXLS.LEFT, 6);
		xlsDoc.setCelda(totalMN.get("PLAZOREGISTRO"));
		xlsDoc.setCelda(totalMN.get("PLAZOPAGO"));
		xlsDoc.setCelda("", "formasb", ComunesXLS.LEFT, 6);
		xlsDoc.setCelda(totalMN.get("MONTOPUBLICADO"));
		xlsDoc.setCelda(totalMN.get("INTERESES"));
		xlsDoc.setCelda(totalMN.get("MONTOOPERADO"));
		xlsDoc.setCelda("");
		
		xlsDoc.setCelda("", "formasb", ComunesXLS.LEFT, 19);
		
		//REPORTE DOLAR
		if(resumenDolar.size()>0){
			for(int i=0; i<resumenDolar.size(); i++){
				HashMap pyme = (HashMap)resumenDolar.get(i);					
				xlsDoc.setCelda(pyme.get("NUM_PYME"));
				xlsDoc.setCelda(pyme.get("RAZON_SOCIAL"));
				xlsDoc.setCelda(pyme.get("NUMERODOC"));
				xlsDoc.setCelda(pyme.get("HABILITADA"));
				xlsDoc.setCelda(pyme.get("DFEMISION"));
				xlsDoc.setCelda(pyme.get("DFPUBLICACION"));
				xlsDoc.setCelda(pyme.get("DFVENCIMIENTO"));
				xlsDoc.setCelda(pyme.get("PLAZOREGISTRO"));
				xlsDoc.setCelda(pyme.get("PLAZOPAGO"));
				xlsDoc.setCelda(pyme.get("MONEDA"));
				xlsDoc.setCelda(pyme.get("FECHAENTREGA"));
				xlsDoc.setCelda(pyme.get("TIPOCOMPRA"));
				xlsDoc.setCelda(pyme.get("CLAVEPRESUPUESTARIA"));
				xlsDoc.setCelda(pyme.get("PERIODO"));
				xlsDoc.setCelda(getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(pyme.get("ICDOCTO").toString()));
				xlsDoc.setCelda(pyme.get("MONTOPUBLICADO"));
				xlsDoc.setCelda(pyme.get("INTERESES"));
				xlsDoc.setCelda(pyme.get("MONTOOPERADO"));
				xlsDoc.setCelda(pyme.get("FECHAAUTORIZACIONIF")); // Fodea 040 - 2011
			}
		}
		
		//TOTALDOLAR
		xlsDoc.setCelda("");
		xlsDoc.setCelda("TOTAL DOLAR", "formasb", ComunesXLS.LEFT, 6);
		xlsDoc.setCelda(totalDolar.get("PLAZOREGISTRO"));
		xlsDoc.setCelda(totalDolar.get("PLAZOPAGO"));
		xlsDoc.setCelda("", "formasb", ComunesXLS.LEFT, 6);
		xlsDoc.setCelda(totalDolar.get("MONTOPUBLICADO"));
		xlsDoc.setCelda(totalDolar.get("INTERESES"));
		xlsDoc.setCelda(totalDolar.get("MONTOOPERADO"));
		xlsDoc.setCelda("");
		
		xlsDoc.cierraTabla();
		xlsDoc.cierraXLS();
		///////////////////////////////////////////////////////////////////////XLS/////////////////////////////////////////////////////////////////////////////////
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		
		infoRegresar = jsonObj.toString();
		
	}
%>
<%=infoRegresar%>