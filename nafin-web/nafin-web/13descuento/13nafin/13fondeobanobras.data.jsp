<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONArray,	
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String df_vencimiento_inicio = (request.getParameter("df_vencimiento_inicio")!=null)?request.getParameter("df_vencimiento_inicio"):"";
String df_vencimiento_fin = (request.getParameter("df_vencimiento_fin")!=null)?request.getParameter("df_vencimiento_fin"):"";
String df_operacion_inicio = (request.getParameter("df_operacion_inicio")!=null)?request.getParameter("df_operacion_inicio"):"";
String df_operacion_fin = (request.getParameter("df_operacion_fin")!=null)?request.getParameter("df_operacion_fin"):"";
String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";

String infoRegresar = "", consulta ="";
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

if(informacion.equals("catalogoMoneda")) {

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");	
	catalogo.setValoresCondicionIn("1,54", Integer.class);	
	infoRegresar = catalogo.getJSONElementos();


}else if (informacion.equals("Consultar") || informacion.equals("ConsultarTotales") 
		|| informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF") ){
	
	ConsFondeoBanobras paginador = new ConsFondeoBanobras();
	paginador.setDf_vencimiento_inicio(df_vencimiento_inicio);
	paginador.setDf_vencimiento_fin(df_vencimiento_fin);
	paginador.setDf_operacion_inicio(df_operacion_inicio);
	paginador.setDf_operacion_fin(df_operacion_fin);
	paginador.setIc_moneda(ic_moneda);
		
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
 
	if (informacion.equals("Consultar") ){ 
		try {
			Registros reg	=	queryHelper.doSearch();
				
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj.put("success", new Boolean(true));
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
	}else if (informacion.equals("ConsultarTotales") ){ 
		
		int numRegistrosMN =0, numRegistrosDL=0;
		BigDecimal montoTotal = new BigDecimal("0.00");
		Registros reg	=	queryHelper.doSearch();
		int totalDoc= 0;
		
		while (reg.next()) {			
			String totalDocto = reg.getString("TOTAL_DOCUMENTOS");
			String monto = reg.getString("MONTO_FONDEO");  
			montoTotal = montoTotal.add(new BigDecimal(monto));
			totalDoc +=Integer.parseInt(totalDocto);			
		}// while
		
		datos = new HashMap();
		datos.put("TOTAL_DOCTOS", String.valueOf(totalDoc));			
		datos.put("TOTAL_MONTOS", montoTotal.toPlainString());
		registros.add(datos);		
			
		String consulta2 =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta2);		
	
	}else  if(informacion.equals("ArchivoCSV") )  {	
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else  if(informacion.equals("ArchivoPDF") )  {	
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}
	
	infoRegresar = jsonObj.toString();	
}
%>
<%=infoRegresar%>