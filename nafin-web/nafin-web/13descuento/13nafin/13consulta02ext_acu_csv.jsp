<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

String cc_acuse = (request.getParameter("ccAcuse") == null) ? "" : request.getParameter("ccAcuse");
JSONObject jsonObj = new JSONObject();

try {
	if (!cc_acuse.equals(""))	{
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".csv";
		Registros regDatosAcuse = new Registros();
		Registros regDatosAcuse2 = new Registros();
		Registros regDatosAcuseTotal = new Registros();
		regDatosAcuse = BeanParamDscto.getDatosAcuse(cc_acuse);
		regDatosAcuse2 = BeanParamDscto.getDatosAcuse2(cc_acuse);
		regDatosAcuseTotal = BeanParamDscto.getDatosTotalesAcuse(cc_acuse);		

		String nombreIF			= "";
		String nombreEPO			= "";
		String numeroDocto		= "";
		String nombreMoneda		= "";
		String montoDocto			= "";
		String porcDesc			= "";
		String montoDescontar	= "";
		String montoInteres		= "";
		String montoRecibir		= "";

	/*******************
	Cabecera del archivo
	********************/			
		contenidoArchivo.append("IF Seleccionado,EPO,Número de Documento,Moneda,Monto Documento,Porcentaje de Descuento,Monto a Descontar,Monto Intereses,Monto a Recibir \n");
		if (regDatosAcuse != null)	{
			while (regDatosAcuse.next()){
				nombreIF			= (regDatosAcuse.getString("NOMBRE_IF")==null)?"":regDatosAcuse.getString("NOMBRE_IF");
				nombreEPO		= (regDatosAcuse.getString("NOMBRE_EPO")==null)?"":regDatosAcuse.getString("NOMBRE_EPO");
				numeroDocto		= (regDatosAcuse.getString("IG_NUMERO_DOCTO")==null)?"":regDatosAcuse.getString("IG_NUMERO_DOCTO");
				nombreMoneda	= (regDatosAcuse.getString("CD_NOMBRE")==null)?"":regDatosAcuse.getString("CD_NOMBRE");
				montoDocto		= (regDatosAcuse.getString("FN_MONTO")==null)?"":regDatosAcuse.getString("FN_MONTO");
				porcDesc			= (regDatosAcuse.getString("FN_PORC_ANTICIPO")==null)?"":regDatosAcuse.getString("FN_PORC_ANTICIPO");
				montoDescontar	= (regDatosAcuse.getString("FN_MONTO_DSCTO")==null)?"":regDatosAcuse.getString("FN_MONTO_DSCTO");
				montoInteres	= (regDatosAcuse.getString("IN_IMPORTE_INTERES")==null)?"":regDatosAcuse.getString("IN_IMPORTE_INTERES");
				montoRecibir	= (regDatosAcuse.getString("IN_IMPORTE_RECIBIR")==null)?"":regDatosAcuse.getString("IN_IMPORTE_RECIBIR");
				contenidoArchivo.append(nombreIF.replace(',',' ')+","+nombreEPO.replace(',',' ')+","+numeroDocto+","+nombreMoneda+","+Comunes.formatoDecimal(montoDocto,2,false)+","+porcDesc+"%,"+Comunes.formatoDecimal(montoDescontar,2,false)+","+Comunes.formatoDecimal(montoInteres,2,false)+","+Comunes.formatoDecimal(montoRecibir,2,false)+"\n");
			}
		}
		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else {
			nombreArchivo = archivo.nombre;
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {}
%>
<%=jsonObj%>