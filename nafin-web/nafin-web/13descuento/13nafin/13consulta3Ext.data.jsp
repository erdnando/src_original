<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		com.netro.model.catalogos.*,	
		com.netro.descuento.*,
		com.netro.pdf.*,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
			netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

String banco      = (request.getParameter("Hbanco")    !=null)?request.getParameter("Hbanco"):"";
String ic_epo     = (request.getParameter("HicEpo")    ==null)?"":request.getParameter("HicEpo");
String documento  = (request.getParameter("documento") ==null)?"":request.getParameter("documento");
String fechaEm1   = (request.getParameter("fechaEm1")  ==null)?"":request.getParameter("fechaEm1");
String fechaEm2   = (request.getParameter("fechaEm2")  ==null)?"":request.getParameter("fechaEm2");
String proveedor  = (request.getParameter("Hproveedor")!=null)?request.getParameter("Hproveedor"):"";
String factoraje  = (request.getParameter("Hfactoraje")!=null)?request.getParameter("Hfactoraje"):"";
String fechaVenc1 = (request.getParameter("fechaVenc1")!=null)?request.getParameter("fechaVenc1"):"";
String fechaVenc2 = (request.getParameter("fechaVenc2")!=null)?request.getParameter("fechaVenc2"):"";
String cbMoneda   = (request.getParameter("HcbMoneda") !=null)?request.getParameter("HcbMoneda"):"";
String monto1     = (request.getParameter("monto1")    !=null)?request.getParameter("monto1"):"";
String monto2     = (request.getParameter("monto2")    ==null)?"":request.getParameter("monto2");
String Hestatus   = (request.getParameter("Hestatus")  ==null)?"":request.getParameter("Hestatus");
String fechaIF1   = (request.getParameter("fechaIF1")  ==null)?"":request.getParameter("fechaIF1");
String fechaIF2   = (request.getParameter("fechaIF2")  ==null)?"":request.getParameter("fechaIF2");

String infoRegresar = "";
HashMap datos;
JSONArray registros = new JSONArray();
JSONObject jsonObj = new JSONObject();

 if (informacion.equals("catalogoProveedores")  ) {
	
	CatalogoPYME catalogo = new CatalogoPYME();
	catalogo.setClaveEpo(ic_epo);
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setCampoClave("ic_pyme");
	catalogo.setCondicionesAdicionales();
	infoRegresar = catalogo.getJSONElementos();	
	
		
}else if (informacion.equals("catalogoBanco")  ) {
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_banco_fondeo");
	catalogo.setCampoDescripcion("CD_DESCRIPCION");
	catalogo.setTabla("comcat_banco_fondeo");		
	catalogo.setOrden("2");
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("catalogoEpo")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setTabla("comcat_epo");		
	catalogo.setOrden("2");
	infoRegresar = catalogo.getJSONElementos();	

}else  if (informacion.equals("catalogoFactoraje")  ) {
	
	String tiposFactorajes="N,V,D,C";
	if(!ic_epo.equals(""))
		tiposFactorajes+=BeanParamDscto.getParametrosFactoraje(ic_epo);
	tiposFactorajes+=",M";
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("CC_TIPO_FACTORAJE");
	catalogo.setCampoDescripcion("CG_NOMBRE");
	catalogo.setTabla("COMCAT_TIPO_FACTORAJE");	
	catalogo.setValoresCondicionIn(tiposFactorajes,String.class);
	catalogo.setOrden("CC_TIPO_FACTORAJE");
	infoRegresar = catalogo.getJSONElementos();	
	
} else if(informacion.equals("catalogoMoneda")){

	CatalogoMoneda catalogo = new CatalogoMoneda();
	catalogo.setCampoClave("IC_MONEDA");
	catalogo.setCampoDescripcion("CD_NOMBRE");

	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("catalogoEstatus")){

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_cambio_estatus");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_cambio_estatus");
	catalogo.setOrden("2");
	String estatus = "";
	if(!ic_epo.equals("")){
		String firmaMancomunada = BeanParamDscto.getOperaFirmaMancomunada(ic_epo, 1);
		if(firmaMancomunada.equals("S")){
			estatus = " 2,4,5,6,8,23,24,28,29,31,32,37,38,39,40,45,46";
		} else if(firmaMancomunada.equals("N")){
			estatus = " 2,4,5,6,8,23,24,28,29,31,32,45,46";
		} else if(firmaMancomunada.equals("")){
			estatus = " 2,4,5,6,8,23,24,28,29,31,32,37,38,39,40,45,46";
		}
		catalogo.setValoresCondicionIn(estatus,String.class);
	}
	infoRegresar = catalogo.getJSONElementos();

} else if(informacion.equals("firmaMancomunada")){
	String firmaMancomunada="";
	if(!ic_epo.equals("")){
		 firmaMancomunada= BeanParamDscto.getOperaFirmaMancomunada(ic_epo, 1);
	}
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("firmaMancomunada",firmaMancomunada);
			infoRegresar = jsonObj.toString();
} else if (informacion.equals("Consultar") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")){

	String firmaMancomunada="";
	if(!ic_epo.equals("")){
		 firmaMancomunada= BeanParamDscto.getOperaFirmaMancomunada(ic_epo, 1);
	}
	int start = 0;
	int limit = 0;
	CambioEstatusNafinDE paginador =new  CambioEstatusNafinDE();
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(proveedor);
	paginador.setIg_numero_docto(documento);
	paginador.setIc_moneda(cbMoneda);
	paginador.setTipoFactoraje(factoraje);
	paginador.setDf_fecha_vencMin(fechaVenc1);
	paginador.setDf_fecha_vencMax(fechaVenc2);
	paginador.setFn_montoMin(monto1);
	paginador.setFn_montoMax(monto2);
	paginador.setIc_cambio_estatus(Hestatus);
	paginador.setDf_fecha_doctoMin(fechaEm1);
	paginador.setDf_fecha_doctoMax(fechaEm2);
	paginador.setDc_fecha_cambioMin(fechaIF1);
	paginador.setDc_fecha_cambioMax(fechaIF2);
	paginador.setFirmaMancomunada(firmaMancomunada);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	if(informacion.equals("Consultar")){ //Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try{
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e){
			throw new AppException("Error en los parametros recibidos", e);
		}
		try{
			if (operacion.equals("Generar")){ //Nueva consulta
				queryHelper.executePKQuery(request);
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e){
			throw new AppException("Error en la paginacion", e);
		}
	} else if(informacion.equals("ArchivoCSV")){
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
	} else if(informacion.equals("ArchivoPDF")){
		//start = Integer.parseInt(request.getParameter("start"));
		//limit = Integer.parseInt(request.getParameter("limit"));
		//String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit,strDirectorioTemp,"PDF");
		String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
		jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
	}

}else if(informacion.equals("Totales")){
	
	CQueryHelperRegExtJS queryHelperE = new CQueryHelperRegExtJS(new ConsDoctosNafinDE()); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelperE.getJSONResultCount(request);
	
} 

%>

<%=infoRegresar%>
