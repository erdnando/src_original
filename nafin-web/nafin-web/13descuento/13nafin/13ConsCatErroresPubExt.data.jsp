 <%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		java.text.SimpleDateFormat,
		com.netro.descuento.*, 
		
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray, net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion     =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	String infoRegresar  = "";
	int start=0; int limit=0;  
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	
	if (informacion.equals("CatalogoEPO") ) {
		
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("2");
		
		List elementos = cat.getListaElementos(); 
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsObjArray.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
	
	}
	else if (informacion.equals("EliminarArchivos")){
	
		String vReg = (request.getParameter("listRegistros")!=null)?request.getParameter("listRegistros"):"";
		JSONArray jsonArr = JSONArray.fromObject(vReg);
		ArrayList listaRegistros = new ArrayList();
		
		for (int x = 0;	x< jsonArr.size();	x++){
			listaRegistros.add(jsonArr.get(x).toString());
		}
		
		CargaDocumento cargaDocto = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
		if (listaRegistros != null && listaRegistros.size() > 0){
			cargaDocto.borraDoctosBitacoraErrorPublicacion(listaRegistros);
			jsonObj.put("result", "ok");
		}
	
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	
	}
	
	else if (informacion.equals("ValidaFechas")) {
		String txtFch1 = (request.getParameter("txtFchReg1")!=null)?request.getParameter("txtFchReg1"):"";
		String txtFch2 = (request.getParameter("txtFchReg2")!=null)?request.getParameter("txtFchReg2"):"";
		if( Fecha.restaFechas(txtFch1, txtFch2) > 30 )
			jsonObj.put("result", "invalida");
		
		else 
			jsonObj.put("result", "valida");
		
		jsonObj.put("success", new Boolean(true));	
		infoRegresar = jsonObj.toString();		
	}
	
	else if(informacion.equals("ConsultaGrid") || informacion.equals("GenerarCSV")  )
	{
		String ic_epo  		= (request.getParameter("cmb_epo")==null)?"":request.getParameter("cmb_epo");
		String fch_pub_1  	= (request.getParameter("txtFchReg1")==null)?"":request.getParameter("txtFchReg1");
		String fch_pub_2  	= (request.getParameter("txtFchReg2")==null)?"":request.getParameter("txtFchReg2");
		
		ConsBitacoraErrorPublicacion paginador = new ConsBitacoraErrorPublicacion();

		paginador.setClave_epo(ic_epo);
		paginador.setFecha_publicacion_ini(fch_pub_1);
		paginador.setFecha_publicacion_fin(fch_pub_2);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
		try
		{
			 start = Integer.parseInt(request.getParameter("start"));
			 limit = Integer.parseInt(request.getParameter("limit"));	
			
		} catch(Exception e)
		  { System.out.println("Error en parametros de paginación"); }
		  
		try 
		{
			if(informacion.equals("ConsultaGrid"))
			{
				if(operacion.equals("Generar")) 
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				
				String  consultar = queryHelper.getJSONPageResultSet(request,start,limit);	
				jsonObj = JSONObject.fromObject(consultar);
				infoRegresar=jsonObj.toString();				
			}
			
			else if (informacion.equals("GenerarCSV"))
			{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
		} catch(Exception e) 
			{
				throw new AppException("Error en la recuperacion de datos!", e);
			}
		
   }

System.out.println("infoRegresar============"+infoRegresar); 
%>
<%=infoRegresar%> 