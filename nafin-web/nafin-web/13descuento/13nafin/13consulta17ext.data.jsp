<%@ page contentType="application/json;charset=UTF-8" import="
   java.util.*, java.sql.*,
	com.netro.exception.*,
	javax.naming.*,
	java.io.*,
	com.netro.threads.*,
	com.netro.model.catalogos.*,
	com.netro.model.catalogos.CatalogoMoneda,
	com.netro.model.catalogos.CatalogoEPO,
	netropology.utilerias.*,
	netropology.utilerias.usuarios.*,
	net.sf.json.JSONObject,
	com.netro.descuento.*,
	net.sf.json.JSONArray"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/13descuento/13secsession_extjs.jspf"%>
<%
String fechaRegIni            = request.getParameter("dFechaRegIni");         if (fechaRegIni            == null) { fechaRegIni           = ""; }
String fechaRegFin            = request.getParameter("dFechaRegFin");         if (fechaRegFin            == null) { fechaRegFin           = ""; }
String bancoFondeo            = request.getParameter("cbBancoFondeo");        if (bancoFondeo            == null) { bancoFondeo           = ""; } 
String icEPO                  = request.getParameter("cbIcEPO");              if (icEPO                  == null) { icEPO                 = ""; }                                                                               
String moneda                 = request.getParameter("cbMoneda");             if (moneda                 == null) { moneda                = ""; }
String usuario                = request.getParameter("cbUsuario");            if (usuario                == null) { usuario               = ""; }
String noDocumento            = request.getParameter("txtNoDocumento");       if (noDocumento            == null) { noDocumento           = ""; }
String noAcuse                = request.getParameter("txtNoAcuse");           if (noAcuse                == null) { noAcuse               = ""; }
String montoInicio            = request.getParameter("txtMontoInicio");       if (montoInicio            == null) { montoInicio           = ""; }
String montoFin               = request.getParameter("txtMontoFin");          if (montoFin               == null) { montoFin              = ""; }
/*if (strTipoUsuario.equals("NAFIN")) {
	icEPO 							= request.getParameter("icEPO");          		if (icEPO               	== null) { icEPO              = ""; }
} 
else */if (strTipoUsuario.equals("EPO")) {
	icEPO  = iNoCliente;
}
CreaArchivo archivo           = new CreaArchivo();
String contenidoArchivo       = "";
BufferedReader	brt            =  null;
String nombreArchivo          = null;
boolean mostrarParametros     = false;

int start                     = 0;
int limit                     = 0;
JSONObject resultado 	      = new JSONObject();
String informacion            =(request.getParameter("informacion")    != null) ?   request.getParameter("informacion") :"";
String operacion              =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
String infoRegresar           = "";

	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	String mostrarCampos ="N";	
	//boolean  campos=	BeanSeleccionDocumento.bgetExisteParamDocs(iNoCliente);
	if(strTipoUsuario.equals("EPO")) {
		boolean  campos=	BeanSeleccionDocumento.bgetExisteParamDocs(icEPO);
		if(campos ==true )  mostrarCampos = "S";
	} 
	else if(strTipoUsuario.equals("NAFIN")){
		 mostrarCampos = "S";
	}
	
if(informacion.equals("valoresIniciales")){
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("strTipoUsuario",strTipoUsuario);
		infoRegresar = jsonObj.toString();
}
else if (informacion.equals("catologoMonedaDist")) {
 CatalogoMoneda cat = new CatalogoMoneda();
 cat.setCampoClave("ic_moneda");
 cat.setCampoDescripcion("cd_nombre"); 
 cat.setOrden("CLAVE");
 
 infoRegresar = cat.getJSONElementos();
}
else if(informacion.equals("catologoUsuarioDist")){
	
   UtilUsr util = new UtilUsr();
	List clavesUsuarios = util.getUsuariosxAfiliado(icEPO, "E");
	JSONArray jsonArr = new JSONArray();
	
	ElementoCatalogo eleCat = new ElementoCatalogo();
	eleCat.setClave("enlac_aut");
	eleCat.setDescripcion("enlac_aut");
	jsonArr.add(JSONObject.fromObject(eleCat));
	
	Iterator it = clavesUsuarios.iterator();
	while (it.hasNext()) {
		String clave = (String)it.next();
		Usuario usr = util.getUsuario(clave);
		String descripcion = clave + " " + usr.getApellidoPaterno() + " " + usr.getApellidoMaterno() + " " + usr.getNombre();
		ElementoCatalogo _eleCat = new ElementoCatalogo();
		_eleCat.setClave(clave);
		_eleCat.setDescripcion(descripcion);
		
		jsonArr.add(JSONObject.fromObject(_eleCat));	
	} 
	
	infoRegresar = "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
}
else if(informacion.equals("catologoBancoDist")){

	List list = null;
	JSONArray jsObjArray = new JSONArray();
	
		if(iTipoPerfil.equals("8")) {
			HashMap hash = new HashMap();
			list = new ArrayList();
			hash.put("clave","2");
			hash.put("descripcion","BANCOMEXT");
			list.add(hash);
		}
		else {
			HashMap hash = new HashMap();
			list = new ArrayList();
			hash.put("clave","1");
			hash.put("descripcion","NAFIN");
			list.add(hash);
			hash = new HashMap();
			hash.put("clave","2");
			hash.put("descripcion","BANCOMEXT");
			list.add(hash);
		}

		jsObjArray = JSONArray.fromObject(list);
		infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";

} else if(informacion.equals("catalogoEPODist")){

	if(strTipoUsuario.equals("EPO")){
		JSONArray jsonArr = new JSONArray();
		ElementoCatalogo eleCat = new ElementoCatalogo();
		eleCat.setClave(iNoCliente);
		eleCat.setDescripcion(iNoCliente);
		jsonArr.add(JSONObject.fromObject(eleCat));
		infoRegresar = "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	} else if(strTipoUsuario.equals("NAFIN")){
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setBancofondeo(bancoFondeo);
		cat.setOrden("2");
		infoRegresar = cat.getJSONElementos();
	}

} else if (informacion.equals("Consulta") || informacion.equals("ArchivoPDF")) {
	com.netro.descuento.ConsBitPublicacionDE paginador = new com.netro.descuento.ConsBitPublicacionDE();
	paginador.setFechaRegIni(fechaRegIni);
	paginador.setFechaRegFin(fechaRegFin);
	paginador.setIcBancoFondeo(bancoFondeo);
	paginador.setMoneda(moneda);
	paginador.setIcEPO(icEPO);
	paginador.setNoDocumento(noDocumento);
	paginador.setNoAcuse(noAcuse);
	paginador.setUsuario(usuario);
	paginador.setMontoInicio(montoInicio);
	paginador.setMontoFin(montoFin);
	paginador.setMostrarCampos(mostrarCampos);

	//CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	CQueryHelperThreadRegExtJS queryHelper = new CQueryHelperThreadRegExtJS(paginador);
	String consulta = "";

	if(informacion.equals("Consulta")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) { throw new AppException("Error en los parametros recibidos", e);}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

		resultado = JSONObject.fromObject(consulta);
		resultado.put("mostrarCampos", mostrarCampos);
		infoRegresar = resultado.toString();

	} else if(informacion.equals("ArchivoPDF")){

		JSONObject jsonObj = new JSONObject();
		try {
			//nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "PDF"); // Este método usa paginación
			/**
			 ***** El siguiente bloque de código genera el PDF implementando la interfaz IQueryGeneratorRegExtJS *****
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			*/
			String estatusArchivo = request.getParameter("estatusArchivo");
			String porcentaje = "";
			if(estatusArchivo.equals("INICIO")){
				session.removeAttribute("13consulta17extThreadCreateFiles");
				ThreadCreateFiles threadCreateFiles = queryHelper.getThreadCreateCustomFile(request, strDirectorioTemp, "PDF");
				session.setAttribute("13consulta17extThreadCreateFiles", threadCreateFiles);
				estatusArchivo = "PROCESANDO";
			} else if(estatusArchivo.equals("PROCESANDO")){
				ThreadCreateFiles threadCreateFiles = (ThreadCreateFiles)session.getAttribute("13consulta17extThreadCreateFiles");
				System.out.println("queryHelper.getPercent() = "+ threadCreateFiles.getPercent());
				porcentaje = String.valueOf(threadCreateFiles.getPercent()) + " %";
				if(!threadCreateFiles.isRunning() && !threadCreateFiles.hasError()){
					nombreArchivo = threadCreateFiles.getNombreArchivo();
					estatusArchivo = "FINAL";
				}else if(threadCreateFiles.hasError()){
					estatusArchivo = "ERROR";
				}
			}

			if(porcentaje.equals("") || porcentaje.equals("0") || porcentaje.equals("0 %")){
				porcentaje = "Procesando...";
			}
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			jsonObj.put("estatusArchivo", estatusArchivo);
			jsonObj.put("porcentaje", porcentaje);

		} catch(Throwable e){
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();

	}

} else if (informacion.equals("ArchivoTXT")){
	String ccAcuse = request.getParameter("ccAcuse")==null?"":request.getParameter("ccAcuse");
	String _icEPO  = request.getParameter("_icEPO")==null?"":request.getParameter("_icEPO");
	String rutaNombreArchivo = "";
	String linea = "";

	CargaDocumento cargaDocumentoEJB = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);

	if (strTipoUsuario.equals("NAFIN") || (strTipoUsuario.equals("EPO")&&BeanSeleccionDocumento.bgetExisteParamDocs(_icEPO))) {
		mostrarParametros = true;
	}

	//SE LLAMA AL NUEVO METODO Y SE INDICA SI MOSTRARA LA PARAMETRIZACION
	contenidoArchivo = cargaDocumentoEJB.generarArchivoOrigenconParam(_icEPO, ccAcuse, mostrarParametros);

	if(!archivo.make(contenidoArchivo, strDirectorioTemp, ".txt")){
		resultado.put("success", new Boolean(false));
		resultado.put("msg", "Error al generar el archivo");
	}else{
		nombreArchivo = archivo.nombre;
		resultado.put("success", new Boolean(true));
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
	infoRegresar = resultado.toString();
} else if(informacion.equals("InfoUsuario")){
	

	String claveUsuario = request.getParameter("idUsuario")==null?"":request.getParameter("idUsuario");
	
	if(claveUsuario!=null&&claveUsuario!=""){
		UtilUsr util = new UtilUsr();
		Usuario usr = util.getUsuario(claveUsuario);
		resultado.put("success",new Boolean(true));
		resultado.put("claveUsuario",claveUsuario);
		resultado.put("nombre",usr.getNombre());
		resultado.put("apellidoPaterno",usr.getApellidoPaterno());
		resultado.put("apellidoMaterno",usr.getApellidoMaterno());
		resultado.put("email",usr.getEmail());
		//System.out.print("It's work");
		infoRegresar=resultado.toString();
	}
}
%>
<%=infoRegresar%>

