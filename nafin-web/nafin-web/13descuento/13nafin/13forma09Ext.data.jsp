 <%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.pdf.*,
		com.netro.descuento.*, 
		com.netro.model.catalogos.CatalogoEPONafin,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray, net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar  = "";


if (informacion.equals("CatalogoEPO") ) {

		CatalogoEPONafin cat = new CatalogoEPONafin();
		cat.setCampoClave ("e.ic_epo");
		cat.setCampoDescripcion("e.cg_razon_social"); 
		cat.setProducto("1");
		//cat.setAceptacion("H");
		//
		List elementos = cat.getListaElementos(); 
		
		//necesitamos una cadena JSON por lo que convertimos el List
		//ArrayList alListadeEPO = null;   
		 JSONArray jsonArr = new JSONArray();
		 
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";

}

else if(informacion.equals("ConsultaParametrosEPO")){
	
	try {
		String sNoEPO = (request.getParameter("sNoEPO")==null)?"":request.getParameter("sNoEPO");
		
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		Hashtable alParamEPO = new Hashtable(); 
		System.out.println("sNoEPO :: "+sNoEPO);
		if(!sNoEPO.equals("")) {
			alParamEPO = BeanParamDscto.getParametrosEPO(sNoEPO, 1);
		}
		
		ArrayList lista_res = new ArrayList();
		lista_res.add(alParamEPO);
		JSONArray jsObjArray = JSONArray.fromObject(lista_res);
		infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
	} 	catch(Exception e) { 
				throw new AppException("Error en la recuperación de datos", e);
			}
}

else if(informacion.equals("ModificaParametrosEPO")){
	
	try {
		
//-----------------*******************************Recuperación de datos*****************************--------------//		
	String sNoEPO            = (request.getParameter("sNoEPO")             ==null)?"":request.getParameter("sNoEPO"),
			sOperaNotasCredito = (request.getParameter("sOperaNotasCredito") ==null)?"":request.getParameter("sOperaNotasCredito"),
			sDsctoObligatorio  = (request.getParameter("sDsctoObligatorio")  ==null)?"":request.getParameter("sDsctoObligatorio"),
			sDsctoDinam        = (request.getParameter("sDsctoDinam")        ==null)?"":request.getParameter("sDsctoDinam"),
			fn_monto_minimo    = (request.getParameter("fn_monto_minimo")    ==null)?"":request.getParameter("fn_monto_minimo"), //FODA 53-2005 SMJ AGREGO FN_MONTO_MINIMO 08/08/2005
			sAccion            = (request.getParameter("sAccion")            ==null)?"":request.getParameter("sAccion"),
			sLimitePyme        = (request.getParameter("sLimitePyme")        ==null)?"N":request.getParameter("sLimitePyme"),
			sFimaManc          = (request.getParameter("sFimaManc")          ==null)?"":request.getParameter("sFimaManc"),
			sCriterioHash      = (request.getParameter("sCriterioHash")      ==null)?"":request.getParameter("sCriterioHash"),
			sFechaVencPyme     = (request.getParameter("sFechaVencPyme")     ==null)?"":request.getParameter("sFechaVencPyme"),

			diasMaximosAd                = (request.getParameter("diasMaximosAd")                ==null)?"": request.getParameter("diasMaximosAd"),
			PlazoMaxFechasVePyDc         = (request.getParameter("PlazoMaxFechasVePyDc")         ==null)?"": request.getParameter("PlazoMaxFechasVePyDc"),
			PlazoMaxFechasVenPyDm        = (request.getParameter("PlazoMaxFechasVenPyDm")        ==null)?"": request.getParameter("PlazoMaxFechasVenPyDm"),
			sPubDoctoVenc                = (request.getParameter("sPubDoctoVenc")                ==null)?"N":request.getParameter("sPubDoctoVenc"),
			sOperPreafilia               = (request.getParameter("sOperPreafilia")               ==null)?"N":request.getParameter("sOperPreafilia"),
			sPermitirDescuentoAutomatico = (request.getParameter("sPermitirDescuentoAutomatico") ==null)?"N":request.getParameter("sPermitirDescuentoAutomatico"),
			sPublicacionEPOPEF           = (request.getParameter("sPublicacionEPOPEF")           ==null)?"N":request.getParameter("sPublicacionEPOPEF"),

			//estos son los valores que se van a actualizar en el bean
			sEPOPEFFlag   = (request.getParameter("sPubEPOPEFFlag")  ==null)?"N":request.getParameter("sPubEPOPEFFlag"),
			sEPOPEFDocto1 = (request.getParameter("sPubEPOPEFDocto1")==null)?"": request.getParameter("sPubEPOPEFDocto1"),
			sEPOPEFDocto2 = (request.getParameter("sPubEPOPEFDocto2")==null)?"": request.getParameter("sPubEPOPEFDocto2"),
			sEPOPEFDocto3 = (request.getParameter("sPubEPOPEFDocto3")==null)?"": request.getParameter("sPubEPOPEFDocto3"),
			sEPOPEFDocto4 = (request.getParameter("sPubEPOPEFDocto4")==null)?"": request.getParameter("sPubEPOPEFDocto4"),
			//MODIFICACION F049-2008 FVR
			sPubEPOPEFSIAFF               = (request.getParameter("sPubEPOPEFSIAFF")              ==null)?"" :request.getParameter("sPubEPOPEFSIAFF"),
			sOperaFactorajeConMandato     = (request.getParameter("sOperaFactorajeConMandato")    ==null)?"N":request.getParameter("sOperaFactorajeConMandato"),
			sOperaFactorajeVencimiento    = (request.getParameter("sOperaFactorajeVencimiento")   ==null)?"N":request.getParameter("sOperaFactorajeVencimiento"),   //FODEA 042 - Agosto/2009
			sOperaSolicitudCesionDerechos = (request.getParameter("sOperaSolicitudCesionDerechos")==null)?"N":request.getParameter("sOperaSolicitudCesionDerechos"),// FODEA 037 - 2009   
			// Modificacion F002 - 2010 JSHD
			sAplicarNotasCredito = (request.getParameter("sAplicarNotasCredito")==null)?"":request.getParameter("sAplicarNotasCredito"),// FODEA 002 - 2010
			//FODEA 016-2010
			sOperaFactoraje24hrs = (request.getParameter("sOperaFactoraje24hrs")==null)?"":request.getParameter("sOperaFactoraje24hrs");

			sOperaSolicitudCesionDerechos = (request.getParameter("sOperaSolicitudCesionDerechos")==null)?"":request.getParameter("sOperaSolicitudCesionDerechos");// FODEA 037 - 2009

			String sOperaFactorajeVencido = (request.getParameter("sOperaFactorajeVencido")==null)?"N":request.getParameter("sOperaFactorajeVencido");// FODEA 018 - 2010
			String sDesAutoFactVenci = (request.getParameter("sDesAutoFactVenci")          ==null)?"": request.getParameter("sDesAutoFactVenci");// FODEA 018 - 2010
			String sDesAutoVencido   = (request.getParameter("sDesAutoFactVenci")          ==null)?"": request.getParameter("sDesAutoFactVenci"); // FODEA 018 - 2010
			String sFactorajeVencido = (request.getParameter("sOperaFactorajeVencido")     ==null)?"": request.getParameter("sOperaFactorajeVencido"); // FODEA 018 - 2010

			String sOperaDsctoAutUltimoDia  = (request.getParameter("sOperaDsctoAutUltimoDia") ==null)?"": request.getParameter("sOperaDsctoAutUltimoDia");// FODEA 024 - 2010
			String sFactorajeDist           = (request.getParameter("sFactorajeDist")          ==null)?"N":request.getParameter("sFactorajeDist");// FODEA 032 - 2010 FVR
			String sAutorizacionCuentasBanc = (request.getParameter("sAutorizacionCuentasBanc")==null)?"": request.getParameter("sAutorizacionCuentasBanc");// FODEA 032 - 2010 FVR

			// Modificacion F036 - 2010 JSHD
			String sOperaTasasEspeciales = (request.getParameter("sOperaTasasEspeciales") == null)?"":request.getParameter("sOperaTasasEspeciales");// FODEA 036 - 2010
			String sTipoTasa             = (request.getParameter("sTipoTasa")             == null)?"":request.getParameter("sTipoTasa");            // FODEA 036 - 2010

			String sHoraEnvOpIf = (request.getParameter("sHoraEnvOpIf") == null)?"":request.getParameter("sHoraEnvOpIf"); // FODEA 036 - 2010

			// Modificacion F040 - 2011 By JSHD 
			String sMostrarFechaAutorizacionIFInfoDoctos = (request.getParameter("sMostrarFechaAutorizacionIFInfoDoctos") == null)?"": request.getParameter("sMostrarFechaAutorizacionIFInfoDoctos");// FODEA 40 - 2011
			// lo recupero F026 -2012
			String sOperaFactorajeIF = (request.getParameter("sOperaFactorajeIF") == null)?"":request.getParameter("sOperaFactorajeIF");   // FODEA 026 - 2012
			String cUnicoIF          = (request.getParameter("cUnicoIF")          == null)?"":request.getParameter("cUnicoIF");            // FODEA 026 - 2012
			String FideDePro         = (request.getParameter("FideDePro")         == null)?"":request.getParameter("FideDePro");           // FODEA 026 - 2012
			String tasaOperacion     = (request.getParameter("tasaOperacion")     == null)?"":request.getParameter("tasaOperacion");

			String valida_duplic      = (request.getParameter("valida_duplic")      == null)?"N":request.getParameter("valida_duplic");      //Fodea 38-2014
			String email_notificacion = (request.getParameter("email_notificacion") == null)?"": request.getParameter("email_notificacion"); //Fodea 38-2014

//-----------------**************************Fin Recuperación de datos**********************************--------------//

			System.out.println("******************************************************************   ");
			System.out.println("sPermitirDescuentoAutomatico   "+sPermitirDescuentoAutomatico);
			System.out.println("sLimitePyme   "+sLimitePyme);
			System.out.println("******************************************************************   ");
			
			
			/**Arma Hash**/
			Hashtable alParamEPO = new Hashtable(); 
			alParamEPO.put("OPERA_NOTAS_CRED",sOperaNotasCredito); 			//add(0,sOperaNotasCredito);
			alParamEPO.put("DSCTO_AUTO_OBLIGATORIO",sDsctoObligatorio); 	//add(1,sDsctoObligatorio);
			alParamEPO.put("DESCTO_DINAM",sDsctoDinam); 							//add(3,sDsctoDinam);
			alParamEPO.put("LIMITE_PYME",sLimitePyme); 							//add(4,sLimitePyme);
			alParamEPO.put("PUB_FIRMA_MANC",sFimaManc); 							//add(5,sFimaManc) ;
			alParamEPO.put("PUB_HASH",sCriterioHash); 							//add(6,sCriterioHash);
			alParamEPO.put("FECHA_VENC_PYME",sFechaVencPyme); 					//add(7,sFechaVencPyme);
			alParamEPO.put("PUB_DOCTO_VENC",sPubDoctoVenc); 					//add(8,sPubDoctoVenc);
			alParamEPO.put("PREAFILIACION",sOperPreafilia); 					//add(12,sOperPreafilia);
			alParamEPO.put("DESC_AUTO_PYME",sPermitirDescuentoAutomatico); //add(13,sPermitirDescuentoAutomatico);
			alParamEPO.put("PUBLICACION_EPO_PEF",sPublicacionEPOPEF); 		//add(14,sPublicacionEPOPEF);
			alParamEPO.put("MONTO_MINIMO",fn_monto_minimo); 					//add(2,fn_monto_minimo);
			alParamEPO.put("PLAZO_PAGO_FVP",diasMaximosAd); 					//add(9,diasMaximosAd);
			alParamEPO.put("PLAZO_MAX1_FVP",PlazoMaxFechasVePyDc); 		//add(10,PlazoMaxFechasVePyDc);
			alParamEPO.put("PLAZO_MAX2_FVP",PlazoMaxFechasVenPyDm); 		//add(11,PlazoMaxFechasVenPyDm);
		
			alParamEPO.put("PUB_EPO_PEF_FECHA_RECEPCION",sEPOPEFFlag); 	//add(15,sEPOPEFFlag);
			alParamEPO.put("PUB_EPO_PEF_FECHA_ENTREGA",sEPOPEFDocto1); 		//add(16,sEPOPEFDocto1);
			alParamEPO.put("PUB_EPO_PEF_TIPO_COMPRA",sEPOPEFDocto2); 	//add(17,sEPOPEFDocto2);
			alParamEPO.put("PUB_EPO_PEF_CLAVE_PRESUPUESTAL",sEPOPEFDocto3); 				//add(18,sEPOPEFDocto3);
			alParamEPO.put("PUB_EPO_PEF_PERIODO",sEPOPEFDocto4); 		//add(19,sEPOPEFDocto4);
		 
			alParamEPO.put("PUB_EPO_PEF_USA_SIAFF",sPubEPOPEFSIAFF);    		//add(20,sPubEPOPEFSIAFF);//MODIFICACION F049-2008
			alParamEPO.put("PUB_EPO_OPERA_MANDATO",sOperaFactorajeConMandato); 		
			alParamEPO.put("PUB_EPO_VENC_INFONAVIT",sOperaFactorajeVencimiento); // FODEA 042 - Agosto/2009
			alParamEPO.put("OPER_SOLIC_CONS_CDER",sOperaSolicitudCesionDerechos); // FODEA 037 - 2009
			alParamEPO.put("PUB_EPO_FACT_24HRS",sOperaFactoraje24hrs); // FODEA 016 - 2010
				
			//System.out.println("sOperaFactoraje24hrs>>>>>>>>>>>>>>"+sOperaFactoraje24hrs);
			// Fodea 002 - 2010
			sAplicarNotasCredito = (sOperaNotasCredito != null && sOperaNotasCredito.equals("S"))?sAplicarNotasCredito:"N";
			alParamEPO.put("CS_APLICAR_NOTAS_CRED",sAplicarNotasCredito);
		 
			 alParamEPO.put("PUB_EPO_DESCAUTO_FACTVENCIDO",sDesAutoVencido); // FODEA 018 - 2010
			 alParamEPO.put("cs_factoraje_vencido",sFactorajeVencido); 		// FODEA 018 - 2010
			 
			 alParamEPO.put("DESC_AUT_ULTIMO_DIA",sOperaDsctoAutUltimoDia); // FODEA 024 - 2010
			 alParamEPO.put("PUB_EPO_FACTORAJE_DISTRIBUIDO",sFactorajeDist); // FODEA 032 - 2010 FVR
			 alParamEPO.put("AUTORIZA_CTAS_BANC_ENTIDADES",sAutorizacionCuentasBanc); // FODEA 032 - 2010 FVR
			 
			 // Fodea 036 - 2010
			 System.err.println("---> sOperaTasasEspeciales = <"+sOperaTasasEspeciales+">");
			 System.err.println("---> sTipoTasa             = <"+sTipoTasa+">");
			 alParamEPO.put("OPER_TASAS_ESPECIALES",	sOperaTasasEspeciales); 
			 alParamEPO.put("TIPO_TASA"				,  ((sOperaTasasEspeciales == null || "N".equals(sOperaTasasEspeciales))?"N":sTipoTasa)); 
			 
			 //FODEA 026-2011
			 alParamEPO.put("HORA_ENV_OPE_IF",  sHoraEnvOpIf); 
			 
			 //FODEA 040 - 2011
			 System.err.println("---> sMostrarFechaAutorizacionIFInfoDoctos = <"+sMostrarFechaAutorizacionIFInfoDoctos+">");
			 alParamEPO.put("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS",	sMostrarFechaAutorizacionIFInfoDoctos);
			 
			 alParamEPO.put("CS_FACTORAJE_IF",sOperaFactorajeIF);
			 alParamEPO.put("CS_CONVENIO_UNICO",cUnicoIF);
			 alParamEPO.put("CS_OPERA_FIDEICOMISO",FideDePro);//Fodea 017-2013
			 alParamEPO.put("TASA_OPERACION",tasaOperacion);//Fodea 017-2013

			 alParamEPO.put("VALIDA_DUPLIC", valida_duplic); //Fodea 38-2014
			 alParamEPO.put("EMAIL_DUPLIC", email_notificacion); //Fodea 38-2014
		 


		/*Invoca al método del Bean*/

		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		BeanParamDscto.setParametrosEPO(sNoEPO, alParamEPO,1,iNoUsuario);
		
		JSONObject resultado = new JSONObject();
		resultado.put("success", new Boolean(true));
		infoRegresar = resultado.toString();	
	} 	catch(Exception e) { 
				throw new AppException("Error en la recuperación de datos", e);
			}
}

System.out.println("infoRegresar============"+infoRegresar); 
%>
<%=infoRegresar%>