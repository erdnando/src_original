Ext.onReady(function() {

	//----------------------------- NUEVOS COMPONENTES DE EXTJS --------------------------
	
		// Custom Action Column Component
		Ext.grid.CustomizedActionColumn = Ext.extend(Ext.grid.ActionColumn, {
			constructor: function(cfg) {
				  var me = this,
						items = cfg.items || (me.items = [me]),
						l = items.length,
						i,
						item;
		
				  Ext.grid.CustomizedActionColumn.superclass.constructor.call(me, cfg);
		
				  // Renderer closure iterates through items creating an <img> element for each and tagging with an identifying 
				  // class name x-action-col-{n}
				  me.renderer = function(v, meta) {
						// Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
						v = Ext.isFunction(cfg.renderer) ? cfg.renderer.apply(this, arguments)||'' : '';
		
						meta.css += ' x-action-col-cell';
						for (i = 0; i < l; i++) {
							 item = items[i];
							 
							 // Check for icon position
							 var iconPosition =  !Ext.isEmpty(item.iconPosition) && "right" == item.iconPosition?"right":"left";
							
							 // Render icon to left side
							 if( "right" == iconPosition ){
								 
								 v += Ext.isEmpty(item.text)?'':'<a class="x-action-col-icon x-action-col-' + String(i) + '" style="color:#0000FF;text-decoration:underline;" >'+item.text+'</a>&nbsp;';
								 v += '<img alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
										'" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
										' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
										((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />'; 
								 
							 // Render icon to right side
							 } else if (  "left" == iconPosition  ){
								 
								 v += '<img alt="' + (item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
										'" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
										' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
										((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />'; 
								 v += Ext.isEmpty(item.text)?'':'&nbsp;<a class="x-action-col-icon x-action-col-' + String(i) + '" style="color:#0000FF;text-decoration:underline;" >'+item.text+'</a>';
								 //v += Ext.isEmpty(item.text)?"":"&nbsp;"+item.text;
							 }
							 
						}
						
						return v;
				  };
			 }
		});
		Ext.apply(Ext.grid.Column.types, { customizedactioncolumn: Ext.grid.CustomizedActionColumn }  );
	
	//------------------------------- VALIDACIONES ---------------------------------

	//--------------------------------- HANDLERS -----------------------------------
	
	//---------------------------------- STORES ------------------------------------
	
	//----------------------------- MAQUINA DE ESTADO ------------------------------
	
	var procesaParametrizacionTasas = function(opts, success, response) {
		
		// Ocultar mascaras segun se requiera
      Ext.getCmp('contenedorPrincipal').ocultaMascaras(opts);
        	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						parametrizacionTasas(resp.estadoSiguiente,resp);
					}
				);
			} else {
				parametrizacionTasas(resp.estadoSiguiente,resp);
			}
			
		}else{
 
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
 
		}
		
	}
	
	var parametrizacionTasas = function(estadoSiguiente, respuesta){
		
		if(         estadoSiguiente == "INICIALIZACION" 										){
			
			// Inicializar Forma de Puntos Adicionales
			var formaPuntosAdicionales = Ext.getCmp("formaPuntosAdicionales");
			formaPuntosAdicionales.getGridEl().mask('Cargando...','x-mask-loading');
			
			// Cargar COMBO de EPOs
			var catalogoEPOData        = Ext.StoreMgr.key('catalogoEPODataStore');
			catalogoEPOData.load();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13ParametrizacionPuntosLimite01ext.data.jsp',
				params: 	{
					informacion:		'ParametrizacionTasas.inicializacion'
				},
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "CARGAR_PUNTOS_ADICIONALES" 						){	
			
			// Inicializar Forma de Puntos Adicionales
			var formaPuntosAdicionales = Ext.getCmp("formaPuntosAdicionales");
			formaPuntosAdicionales.getGridEl().mask('Cargando...','x-mask-loading');
			
			// Deshabilitar los botones de la forma
			Ext.getCmp("botonGuardar").disable();
			Ext.getCmp("botonCancelar").disable();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13ParametrizacionPuntosLimite01ext.data.jsp',
				params: 	{
					informacion:		'ParametrizacionTasas.cargarPuntosAdicionales'
				},
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "MOSTRAR_PUNTOS_ADICIONALES" 						){	
 
			var formaPuntosAdicionales = Ext.getCmp("formaPuntosAdicionales");
			formaPuntosAdicionales.setSource(
				respuesta.parametrizacionPuntosLimite
			);
 
			Ext.getCmp("botonGuardar").enable();
			Ext.getCmp("botonCancelar").enable();
			
		} else if(  estadoSiguiente == "GUARDAR_PUNTOS_ADICIONALES" 						){
			
			// Inicializar Forma de Puntos Adicionales
			var formaPuntosAdicionales = Ext.getCmp("formaPuntosAdicionales");
			formaPuntosAdicionales.getGridEl().mask('Guardando...','x-mask-loading');
			
			// Deshabilitar los botones de la forma
			Ext.getCmp("botonGuardar").disable();
			Ext.getCmp("botonCancelar").disable();
 
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13ParametrizacionPuntosLimite01ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'ParametrizacionTasas.guardarPuntosAdicionales'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "CANCELAR_GUARDAR_PUNTOS_ADICIONALES"  			){
			
			// Habilitar los botones de la forma
			Ext.getCmp("botonGuardar").enable();
			Ext.getCmp("botonCancelar").enable();
			
		} else if(  estadoSiguiente == "ESPERAR_DECISION"  									){
 		
		} else if(  estadoSiguiente == "CAMBIAR_DEFINICION_PUNTOS_ADICIONALES"        ){
			
			// Inicializar Forma de Puntos Adicionales
			var gridParametrizacionEPO = Ext.getCmp("gridParametrizacionEPO");
			gridParametrizacionEPO.getGridEl().mask('Espere un momento...','x-mask-loading');
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13ParametrizacionPuntosLimite01ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'ParametrizacionTasas.cambiarDefinicionPuntosAdicionales'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "MOSTRAR_DEFINICION_PUNTOS_ADICIONALES"        ){
			
			if(        respuesta.utilizaParametroLimite == "S" ){
				showWindowEpoConPuntosAdicionales(respuesta.valorPuntosLimite);
			} else if( respuesta.utilizaParametroLimite == "N" ) {
				showWindowEpoSinPuntosAdicionales();
			}
 	
		} else if(  estadoSiguiente == "FIN"                 									){
			
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"13ParametrizacionPuntosLimite01ext.data.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			forma.submit();
			
		}
		
	}
	
	//-------------------------- PANEL EPO SIN PUNTOS ADICIONALES ------------------------------

	var elementosEpoSinPuntosAdicionales = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'justify',
				paddingBottom: '15px',
				fontWeight:    'bold'
			},
			html: 	'La EPO ha dejado de operar con los Puntos Adicionales L�mite.'
		},
		{
			xtype: 	'button',
			text:  	'Aceptar',    // Nota: Version Original: 'Regresar', 
			iconCls: 'icoAceptar', // Nota: Version Original: 'icoRegresar', 
			width:	100,
			handler: function(){
				hideWindowEPOSinPuntosAdicionales();
				// Realizar consulta
				var parametrizacionEPOData  = Ext.StoreMgr.key('parametrizacionEPODataStore');
				parametrizacionEPOData.reload();
				// Nota: en vez de un reload se podr�a modificar directamente
				// el registro, lo que beneficiar�a al usuario.
				// se podr�a usar: getScrollState() y restoreScroll()
			}
		}
	];
	
	var panelEpoSinPuntosAdicionales = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelEpoSinPuntosAdicionales',
		frame: 			false,
		width:			450,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		bodyCfg:       { tag:'center' },
		items: 			elementosEpoSinPuntosAdicionales,
		//plain:        true,
		//frame:        false,		
		border:       false
	});
	
	var showWindowEpoSinPuntosAdicionales = function(){
		
		var ventana = Ext.getCmp('windowEPOSinPuntosAdicionales');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Definici�n de Puntos Adicionales',
				layout: 			'fit',
				width:			450,
				height:			125,
				resizable:		false,
				id: 				'windowEPOSinPuntosAdicionales',
				modal:			true,
				closable: 		false,
				items: [
					Ext.getCmp("panelEpoSinPuntosAdicionales")
				]
			}).show();
			
		}
		
	}	

	var hideWindowEPOSinPuntosAdicionales = function(){
		
		var ventanaEPOSinPuntosAdicionales = Ext.getCmp('windowEPOSinPuntosAdicionales');
		
		if(ventanaEPOSinPuntosAdicionales && ventanaEPOSinPuntosAdicionales.isVisible() ){	
			ventanaEPOSinPuntosAdicionales.hide();
		}
      
	}
	
	//-------------------------- PANEL EPO CON PUNTOS ADICIONALES ------------------------------
	
	var elementosEpoConPuntosAdicionales = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'justify',
				paddingBottom: '15px',
				fontWeight:    'bold'
			},
			html: 	'La EPO ha sido parametrizada con los siguientes Puntos Adicionales al L�mite.'
		},
		{
			xtype:      'container',
			layout:     'form',
			labelWidth: 160,
			width:		350,
			style: {
				paddingBottom: '15px'
			},
			items: [
				{
					xtype: 	'label',	
					cls: 		'x-form-item',
					style: {
						paddingLeft:   '3pt',
						textAlign: 		'left',
						fontWeight:    'bold'
					},
					html: 	'MONEDA NACIONAL PESO MEXICANO'
				},
				{
					xtype: 		'textfield',
					fieldLabel: 'Puntos Adicionales al L�mite',
					id:    		'puntosAdicionalesLimiteMXN',
					width:		75,
					value: 		'00.00000',
					readOnly:	true,
					style: {
						textAlign: 'right'
					}
				},
				{
					xtype: 	'label',	
					cls: 		'x-form-item',
					style: {
						paddingLeft:   '3pt',
						textAlign: 		'left',
						fontWeight:    'bold'
					},
					html: 	'DOLAR AMERICANO'
				},
				{
					xtype: 		'textfield',
					fieldLabel: 'Puntos Adicionales al L�mite',
					id:    		'puntosAdicionalesLimiteDL',
					width:		75,
					value: 		'00.00000',
					readOnly:	true,
					style: {
						textAlign: 'right'
					}
				}
			]
		},
		{
			xtype: 	'button',
			text:    'Aceptar',    // Nota: Version Original: 'Regresar', 
			iconCls: 'icoAceptar', // Nota: Version Original: 'icoRegresar',
			width:	100,
			handler: function(){
				hideWindowEPOConPuntosAdicionales();
				// Realizar consulta
				var parametrizacionEPOData  = Ext.StoreMgr.key('parametrizacionEPODataStore');
				parametrizacionEPOData.reload();
				// Nota: en vez de un reload se podr�a modificar directamente
				// el registro, lo que beneficiar�a al usuario.
				// se podr�a usar: getScrollState() y restoreScroll()
			}
		}
	];
	
	var panelEpoConPuntosAdicionales = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelEpoConPuntosAdicionales',
		width:			600,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		bodyCfg:       { tag:'center' },
		items: 			elementosEpoConPuntosAdicionales,
		//plain:        true,
		//frame:        false,		
		border:       false
	});
	
	var showWindowEpoConPuntosAdicionales = function(valorPuntosLimite){
		
		Ext.getCmp("puntosAdicionalesLimiteMXN").setValue(valorPuntosLimite);
		Ext.getCmp("puntosAdicionalesLimiteDL").setValue(valorPuntosLimite);
		
		var ventana = Ext.getCmp('windowEPOConPuntosAdicionales');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Definici�n de Puntos Adicionales',
				layout: 			'fit',
				width:			600,
				height:			245,
				resizable:		false,
				id: 				'windowEPOConPuntosAdicionales',
				modal:			true,
				closable: 		false,
				items: [
					Ext.getCmp("panelEpoConPuntosAdicionales")
				]
			}).show();
			
		}
		
	}	

	var hideWindowEPOConPuntosAdicionales = function(){
		
		var ventanaEPOConPuntosAdicionales = Ext.getCmp('windowEPOConPuntosAdicionales');
		
		if(ventanaEPOConPuntosAdicionales && ventanaEPOConPuntosAdicionales.isVisible() ){	
			ventanaEPOConPuntosAdicionales.hide();
		}
      
	}
	
	//--------------------- GRID PANEL DETALLE PARAMETRIZACION EPO ---------------------------
	
	var procesarConsultaParametrizacionEPO = function(store, registros, opts){
 
		var panelFormaParametrosEPO = Ext.getCmp('panelFormaParametrosEPO');
		panelFormaParametrosEPO.el.unmask();
		
		var gridParametrizacionEPO  = Ext.getCmp('gridParametrizacionEPO');
 
		if (registros != null) {
			
			if (!gridParametrizacionEPO.isVisible()) {
				gridParametrizacionEPO.show();
			}			

			var el = gridParametrizacionEPO.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
			// Actulizar los parametros base con los parametros de la consulta exitosa
			store.baseParams = opts.params;
			// Resetear el campo operacion para evitar que cuando se utilicen los botones
			// de paginacion del pagingtoolbar se tengan que regenerar la llaves
			Ext.apply( 
				store.baseParams,
				{ 
					operacion: '' 
				}
			);
 
		}
		
	}
	
	var parametrizacionEPOData = new Ext.data.JsonStore({
		root: 	'registros',
		id:		'parametrizacionEPODataStore',
		url: 		'13ParametrizacionPuntosLimite01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaParametrizacionEPO'
		},
		fields: [
			{ name: 'ID_EPO', 							type: 'int' 	}, 
			{ name: 'NOMBRE_EPO', 						type: 'string' }, 
			{ name: 'UTILIZA_PARAMETRO_LIMITE', 	type: 'string' } 
		],
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,
		listeners: {
			beforeload:	{
				fn: function(store, options){
					Ext.getCmp('gridParametrizacionEPO').getGridEl().unmask();
				}
			}, 
			load: 	procesarConsultaParametrizacionEPO,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaParametrizacionEPO(null, null, null);						
				}
			}
		}
		
	});
 
	var nombreEpoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
 
	var utilizaParametroLimiteRenderer  = function( value, metadata, record, rowIndex, colIndex, store){
		
		if( value == 'SI' ){
			value = "S�";
		} else if( value == 'NO' ){
			value = "No";
		}
		return value;
		
	}
	
	// Grid con las cuentas registradas
	var gridParametrizacionEPO = new Ext.grid.GridPanel({
		store: 	parametrizacionEPOData,
		id:		'gridParametrizacionEPO',
		style: 	'margin: 0 auto;',
		hidden: 	true,
		margins: '20 0 0 0',
		title: 	undefined,
		autoExpandColumn: 'EPO',
		stateful:false,
		columns: [
			{
				header: 		'Clave EPO',
				tooltip: 	'Id de la EPO',
				dataIndex: 	'ID_EPO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		200,
				hidden: 		true
			},
			{
				id:			'EPO',
				header: 		'EPO<br>&nbsp;',
				tooltip: 	'Nombre de la EPO',
				dataIndex: 	'NOMBRE_EPO',
				align:		'left',
				sortable: 	true,
				resizable: 	true,
				hidden: 		false,
				renderer:	nombreEpoRenderer
			},
			{
				header: 		'Utiliza Par�metro<br>L�mite',
				tooltip: 	'Utiliza Par�metro L�mite',
				dataIndex: 	'UTILIZA_PARAMETRO_LIMITE',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		100,
				hidden: 		false,
				renderer:	utilizaParametroLimiteRenderer
			},
			{
            xtype: 		'customizedactioncolumn',
            header: 		'Definici�n de Puntos<br>Adicionales',
				tooltip: 	'Cambiar la Definici�n de Puntos Adicionales de la EPO',
            width: 		125,
            align:		'center',
            hidden: 		false,
            items: [ 
            	 // ACCION CAMBIAR PARAMETRIZACION
            	 {
                    getClass: function(value, metadata, record) { 
                    	  	 
                        if (        record.json['UTILIZA_PARAMETRO_LIMITE'] == "SI" ) {
                            this.items[0].tooltip 		 = 'Cambiar Parametrizaci�n de Puntos Adicionales de la EPO';
                            this.items[0].text	 		 = 'Cambiar';
									 this.items[0].iconPosition = 'left';
                            return 'icoInvalidar';
                        } else if ( record.json['UTILIZA_PARAMETRO_LIMITE'] == "NO" ) {
                            this.items[0].tooltip      = 'Cambiar Parametrizaci�n de Puntos Adicionales de la EPO';
                            this.items[0].text	 		 = 'Cambiar';
									 this.items[0].iconPosition = 'left';
                            return 'icoValidar';
                        } else {
                            this.items[0].tooltip = '';
                            return 'icoTransparente';
                        }
                        
                    },
                    handler: function(grid, rowIndex, colIndex) {
                    	  
                        var record = Ext.StoreMgr.key('parametrizacionEPODataStore').getAt(rowIndex);
                        
                        if (       record.json['UTILIZA_PARAMETRO_LIMITE']  == "SI"  ) {
                        	
                        	Ext.MessageBox.confirm(
										'Confirm', 
										"�Est� seguro de que desea cambiar la parametrizaci�n de la EPO?", 
										function(confirmBoton){   
											if( confirmBoton == "yes" ){
												var respuesta = new Object();
												respuesta['idEpo']   					= record.json['ID_EPO'];
												respuesta['utilizaParametroLimite'] = record.json['UTILIZA_PARAMETRO_LIMITE'];
												parametrizacionTasas("CAMBIAR_DEFINICION_PUNTOS_ADICIONALES",respuesta); 
											}
										}
									);
 
                        } else if ( record.json['UTILIZA_PARAMETRO_LIMITE'] == "NO"  ) {
                        	    
                        	Ext.MessageBox.confirm(
										'Confirm', 
										"�Est� seguro de que desea cambiar la parametrizaci�n de la EPO?", 
										function(confirmBoton){   
											if( confirmBoton == "yes" ){
												var respuesta = new Object();
												respuesta['idEpo']   					= record.json['ID_EPO'];
												respuesta['utilizaParametroLimite'] = record.json['UTILIZA_PARAMETRO_LIMITE'];
												parametrizacionTasas("CAMBIAR_DEFINICION_PUNTOS_ADICIONALES",respuesta); 
											}
										}
									);
 
                        } 
                        
                    }
                }
            ]
         }
		],
		stripeRows: true,
		loadMask: 	true,
		height: 		480,
		width: 		500,		
		frame: 		true
	});
	
	//------------------------ FORMA PARAMETRIZACION EPOS --------------------------
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 			'catalogoEPODataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13ParametrizacionPuntosLimite01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
					
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboEPO").setValue(defaultValue);
						Ext.getCmp("comboEPO").originalValue = defaultValue;
					}
					//Ext.getCmp("comboEPO").setReadOnly(true);
								
				} 
									
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
 
	var elementosPanelFormaParametrosEPO = [
		// COMBO EPO
		{
			xtype: 				'combo',
			name: 				'epo',
			id: 					'comboEPO',
			fieldLabel: 		'EPO',
			allowBlank: 		true,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			emptyText: 			'Seleccionar...', 
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			//tpl: 					NE.util.templateMensajeCargaCombo,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word; ">{descripcion}</div>' +
									'</div></tpl></tpl>',
			anchor:				'-20'
		},
		// RADIO: UTILIZA PARAMETRO LIMITE
		{
        xtype: 				'radiogroup',
        fieldLabel: 			'Utiliza Par�metro L�mite',
        items: [
        	  { boxLabel: 'S�',  name: 'utilizaParametroLimite', inputValue: 'S', checked: true },
        	  { boxLabel: 'No',  name: 'utilizaParametroLimite', inputValue: 'N' },
        	  { xtype:    'box'  },{ xtype:    'box'  },{ xtype:    'box'  },{ xtype:    'box'  } 
   	  ]
      }
	];                                                  
	
	var panelFormaParametrosEPO = {
		xtype:			'form',
		id: 				'panelFormaParametrosEPO',
		hidden:			false,
		width: 			500,
		title: 			'Consultar Parametrizaci�n por EPO',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	135,
		labelAlign: 	'right',
		defaultType: 	'textfield',
		items: 			elementosPanelFormaParametrosEPO,
		monitorValid: 	false,
		buttons: [
			// BOTON BUSCAR
			{
				id:				'botonConsultar',
				text: 			'Consultar',
				hidden: 			false,
				iconCls: 		'icoBuscar',
				handler: 		function() {
					
					// Si la forma no es invalida, suspender operaci�n
					var panelFormaParametrosEPO = Ext.getCmp("panelFormaParametrosEPO");
					// Si hay error en la forma, suspender la consulta
					if( !panelFormaParametrosEPO.getForm().isValid() ){
						return;
					}
										
					// Agregar mascara de b�squeda
					panelFormaParametrosEPO.getEl().mask('Buscando...','x-mask-loading');
		
					// Realizar consulta
					var parametrizacionEPOData  = Ext.StoreMgr.key('parametrizacionEPODataStore');
					parametrizacionEPOData.load({
						params: panelFormaParametrosEPO.getForm().getValues()
					});
			
			
				}
			},
			// BOTON LIMPIAR
			{
				text: 		'Limpiar',
				hidden: 		false,
				iconCls: 	'icoLimpiar',
				handler: 	function() {
					Ext.getCmp("panelFormaParametrosEPO").getForm().reset();
					Ext.getCmp("gridParametrizacionEPO").hide();
				}
			}
		]
	};	
 
	//------------------------- FORMA LIMITE PUNTOS ADICIONALES ---------------------------
	var formaPuntosAdicionales = new Ext.grid.PropertyGrid({
		id:				'formaPuntosAdicionales',
		xtype:			'propertygrid',
		title: 			'Tasa Base',
		frame:			true,
		width: 			500,
		height: 			194,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		stateful:		false,
		tbar:			[
			{
				xtype:	'button',
				id:		'botonGuardar',
				text:  	'Guardar',
				iconCls:	'icoGuardar',
				handler: function(button,event){
					
					var formaPuntosAdicionales 	= Ext.getCmp("formaPuntosAdicionales");
					
					var respuesta 						= new Object();
					respuesta.fnPuntosLimite      = formaPuntosAdicionales.getSource()['Puntos Adicionales L�mite'];
					parametrizacionTasas("GUARDAR_PUNTOS_ADICIONALES", respuesta);
 
				},
				disabled: true
			},
			{
				xtype:	'button',
				id:		'botonCancelar',
				text:		'Cancelar',
				iconCls:	'icoDeshacer',
				handler:	function(button,event){
					var respuesta 					= new Object();
					var formaPuntosAdicionales	= Ext.getCmp('formaPuntosAdicionales');
					formaPuntosAdicionales.customEditors['Puntos Adicionales L�mite'].cancelEdit(true);
					parametrizacionTasas("CARGAR_PUNTOS_ADICIONALES", respuesta);
				},
				disabled: true
			}
		],
		viewConfig: {
            //forceFit: true,
            scrollOffset: 2 // => No mostrar scrollbar
      },
      customEditors: {
      	'Puntos Adicionales L�mite'	: new Ext.grid.GridEditor(
      		new Ext.form.NumberField({
      			selectOnFocus:		true, 
      			style: 'text-align: left;' + (
            		  Ext.isIE7?'top: 0px; padding-bottom: 2px;':
            		( Ext.isIE8?'top: 0px; padding-bottom: 2px;':
            		'top: 1px; padding-bottom: 2px;')
            	),
      			decimalPrecision: 5,
      			minValue:			0.00000, // Debido a que no se aceptan numeros negativos
      			maxValue: 			99.99999,
      			listeners: {
						invalid: function(e){
							Ext.getCmp("botonGuardar").disable();
						},
						valid: function(e){
							Ext.getCmp("botonGuardar").enable();
						}
					}
      		}),
      		{
      			revertInvalid: false // Evita que se resetee el valor capturado cuando este es invalido y se guarde sin cambios.
      		}
      	)
      },
      customRenderers: {
      	'Puntos Adicionales L�mite'	: function(value,meta,record){
      		// meta.style += "font-weight:bold;background-color: #ffc;"; 
      		// meta.style += "font-weight:bold; border:1px solid gray; "; 
      		meta.attr  = 'style="border: 1px solid gray;"';
      		return Ext.util.Format.number(value,'0.00000');
      	},
      	'�ltima Modificaci�n'			: function(value,meta,record){
      		//meta.style += "color:#262626;";
      		//meta.style += "background-color: 'gainsboro'; "; 
      		return value; // value.dateFormat('d/m/Y');
      	},
			'Usuario'							: function(value,meta,record){
      		//meta.style += "color:#262626;"; 
      		//meta.style += "background-color: 'gainsboro'; ";
      		return value;
      	}
      },
      listeners: {
			beforeedit: function(e) {
				if(        e.record.id == '�ltima Modificaci�n') {
					e.cancel = true;
				} else if( e.record.id == 'Usuario'){
					e.cancel = true;
				}
			}
    	},
		bbar: [
			{
				xtype: 'label',
				html:  '<div style="padding:3pt">Los puntos adicionales aplicar�n para los tipos de moneda nacional y d�lar americano.</div>'
			}
		]
   });
   // Remove default sorting
   delete formaPuntosAdicionales.getStore().sortInfo;
   formaPuntosAdicionales.getColumnModel().getColumnById('name').sortable = false;
   formaPuntosAdicionales.getColumnModel().getColumnById('name').width    = 152;
   formaPuntosAdicionales.getColumnModel().getColumnById('value').width   = 330;
   // Now load data
   /*
   formaPuntosAdicionales.setSource(
   	{
   		'Puntos Adicionales L�mite'	: "0.00000",
			'�ltima Modificaci�n'			: new Date(Date.parse('01/01/1970')),
			'Usuario'							: "No disponible"
		}
	);
	*/
   
	//------------------------ PANEL MODO PANTALLA --------------------------
	var panelModoPantalla = {
      xtype:		'panel',
      frame:		false,
      border:		false,
      bodyStyle:	'background:transparent;',
      layout: 		'hbox',
      style: 		'margin: 0 auto',
      width:		500,
      height: 		75,
      layoutConfig: {
        align:	'middle',
        pack:	'center'
      },
      items: [
		  {	
			  xtype: 		'button',
			  text: 	  	 	'Parametrizaci�n',
			  width:			126,
			  pressed:  	true,
			  id: 			'botonPantallaParametrizacion'
		  },
		  {
			  xtype: 		'box',
			  width:			8,
			  autoWidth: 	false
		  },
		  {
			  xtype: 	'button',
			  text: 	   'Captura',
			  width:			126,
			  id: 		'botonPantallaCaptura',
			  handler: function(){
			  	  var respuesta = new Object();
			  	  respuesta['destino']   			= "13forma02ext.jsp";
			  	  parametrizacionTasas("FIN",respuesta); 
			  }
		  }
     ]
   };
  
	//-------------------------------- PRINCIPAL -----------------------------------
   
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		disabled: false,
		items: 	[
			panelModoPantalla,
			formaPuntosAdicionales,
			NE.util.getEspaciador(10),
			panelFormaParametrosEPO,
			NE.util.getEspaciador(10),
			gridParametrizacionEPO
		],
		ocultaMascaras: function(opts){
			
			// NOTA: CON OPTS. SE PODR�A AGREGAR UN PARAMETRO ORIGEN, 
			// PARA QUE OCULTA MASCARAS VAYA FOCALIZADO.
			
			// Suprimir mascara del Panel de Tasa Base ( Puntos Limite Adicionales )
			var element = Ext.getCmp("formaPuntosAdicionales").getGridEl();
			if( element.isMasked() ){
				element.unmask();
			}
			
			// Sacar m�scara del panel de "Consultar Parametrizaci�n EPO"
			/*element = Ext.getCmp("panelFormaParametrosEPO").getEl();
			if( element.isMasked()){
				element.unmask();
			}*/
			
			// Derefenrenciar ultimo elemento...
			element = null;
			
		}
	});
 			
	//------------------------ ACCIONES DE INICIALIZACION --------------------------
	
	//Peticion para realizar la inicializacion de la pantalla.
	var respuesta 						= new Object();
	parametrizacionTasas("INICIALIZACION", respuesta);
	
});