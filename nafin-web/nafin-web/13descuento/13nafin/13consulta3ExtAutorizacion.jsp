<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
		com.netro.model.catalogos.*,	
		com.netro.pdf.*,
		java.sql.*,
		java.io.*,
		netropology.utilerias.usuarios.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
			netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>



<%
String infoRegresar="";
String icDocumento = request.getParameter("icDocumento") == null?"":request.getParameter("icDocumento");

AccesoDB con = new AccesoDB();
PreparedStatement ps = null;
ResultSet rs = null;
StringBuffer strSQL = new StringBuffer();
List varBind = new ArrayList();
File file = null;
FileOutputStream fileOutputStream = null;
String pathname = "";

try{
	con.conexionDB();
	
	strSQL.append(" SELECT bi_autorizacion, cg_extension FROM comhis_cambio_estatus WHERE ic_documento = ?");
	varBind.add(new Long(icDocumento));
	
	System.out.println("..:: strSQL : "+strSQL.toString());
	System.out.println("..:: varBind : "+varBind);
	
	ps = con.queryPrecompilado(strSQL.toString(), varBind);
	rs = ps.executeQuery();
	
	if(rs.next()){
		pathname = strDirectorioTemp + Comunes.cadenaAleatoria(10) + "." + rs.getString("cg_extension").toLowerCase();
		file = new File(pathname);
		fileOutputStream = new FileOutputStream(file);
		Blob blob = rs.getBlob("bi_autorizacion");
		InputStream inStream = blob.getBinaryStream();
		int size = (int)blob.length();
		byte[] buffer = new byte[size];
		int length = -1;
		
		while ((length = inStream.read(buffer)) != -1){
			fileOutputStream.write(buffer, 0, length);
		}
		
		inStream.close();
		fileOutputStream.close();		
	}
	System.out.println("..:: ruta : "+strDirecVirtualTemp + file.getName());
	//response.setHeader("Content-disposition", "attachment; filename=" + file.getName());
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success",new Boolean(true));
	jsonObj.put("urlArchivo",strDirecVirtualTemp + file.getName());
	infoRegresar = jsonObj.toString();
}catch(Exception e){
	e.printStackTrace();
	throw new Exception("Error al realizar la consulta del documento.", e);
	
}finally{
	if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	}
}%>
<%=infoRegresar%>