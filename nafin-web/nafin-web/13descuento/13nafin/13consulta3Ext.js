Ext.onReady(function(){
var firmaMancomunada;
//-------------------Handlers-----------------------
function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}

function comparaFechas(fecha1,fecha2){
	var fec1=Ext.getCmp(fecha1);
	var fec2=Ext.getCmp(fecha2);
	if (fec2.getValue()==''||fec1.getValue()==''){
		return true;
		
	}
	if(fec2.getValue()>=fec1.getValue()){
		return false;
	}else{
		return true;
	}
	
	
}
var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var botonPDF = Ext.getCmp('btnGenerarPDF');
		var botonCSV = Ext.getCmp('btnGenerarArchivo');
		
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}			
			
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var el = grid.getGridEl();						
			if(store.getTotalCount() > 0) {
			botonPDF.enable();
			botonCSV.enable();
			
			var montoTotalMN=0;
			var montoTotalDsctoMN=0;
			var montoTotalDL=0;
			var montoTotalDsctoDL=0;
			var numRegistrosMN=0;
			var numRegistrosDL=0;
			var icCambioEstatus;
			var firmaManco;
			var claveMoneda;
			var nomIF='';
			var value='';
			var valorN;
			var ic_estatus = Ext.getCmp('estatus').getValue();
			for(var i=0;i<arrRegistros.length;i++){
					nomIF='';
					registro=store.getAt(i);
					value=registro.get('NOMBREIF');
					var dblPorcentaje='';
				
					if (registro.get('IC_CAMBIO_ESTATUS')	==	28) {
										valorN=registro.get('FN_MONTO_NUEVO')*registro.get('FN_AFORO')/100;
										//alert(valorN);
										registro.set('FN_MONTO_DSCTO',valorN);	
						}
					if (ic_estatus=='4'||ic_estatus=='5'||ic_estatus=='6') {
										valorN=registro.get('FN_MONTO')*registro.get('FN_AFORO')/100;
										//alert(valorN);
										registro.set('FN_MONTO_DSCTO',valorN);	
						}
					if (registro.get('CS_DSCTO_ESPECIAL')	==	'V'||registro.get('CS_DSCTO_ESPECIAL')	==	'I') {
						registro.set('FN_MONTO_DSCTO',registro.get('FN_MONTO'));
						dblPorcentaje=100;
						nomIF=value;
					}
					if (registro.get('CS_DSCTO_ESPECIAL')	==	'A'){
						nomIF=value;
					}
				
					if (registro.get('IC_CAMBIO_ESTATUS')	==	29) {
						registro.set('FN_MONTO_DSCTO',registro.get('FN_MONTO'));
						dblPorcentaje=registro.get('FN_AFORO');
						nomIF=value;
					}
					if(dblPorcentaje!=''){
						registro.set('FN_AFORO',dblPorcentaje);
					}
					registro.set('NOMBREIF',nomIF);
					icCambioEstatus=store.getAt(i).get('IC_CAMBIO_ESTATUS');
					firmaManco=store.getAt(i).get('PUB_FIRMA_MANC');
					claveMoneda=store.getAt(i).get('IC_MONEDA')
				if( icCambioEstatus != 8 ||   icCambioEstatus != 28  ||  (icCambioEstatus != 37 &&  icCambioEstatus != 38 &&  icCambioEstatus != 39 &&  icCambioEstatus != 40 && firmaManco=='N' ) )  { /* estatus "Modificacion de Montos y Fechas" */
					
					if(claveMoneda==1){//Moneda Nacionals
						numRegistrosMN++;
						montoTotalMN+=parseFloat(store.getAt(i).get('FN_MONTO'));
						montoTotalDsctoMN+=parseFloat(store.getAt(i).get('FN_MONTO_DSCTO'));
					}else {
						numRegistrosDL++;
						montoTotalDL+=parseFloat(store.getAt(i).get('FN_MONTO'));
						montoTotalDsctoDL+=parseFloat(store.getAt(i).get('FN_MONTO_DSCTO'));
					}
				}else if( icCambioEstatus != 8 || icCambioEstatus != 28   && !firmaManco.equals("N") )  {
						if(claveMoneda==1){//Moneda Nacionals
						numRegistrosMN++;
						montoTotalMN+=parseFloat(store.getAt(i).get('FN_MONTO'));
						montoTotalDsctoMN+=parseFloat(store.getAt(i).get('FN_MONTO_DSCTO'));
					}else {
						numRegistrosDL++;
						montoTotalDL+=parseFloat(store.getAt(i).get('FN_MONTO'));
						montoTotalDsctoDL=parseFloat(store.getAt(i).get('FN_MONTO_DSCTO'));
					}
						
						
				}
				
			}
				totalesData.removeAll();
				var regi = Ext.data.Record.create(['CD_NOMBRE', 'FN_MONTO','FN_MONTO_DSCTO','CAMBIOESTATUSNAFINDE']);
			   totalesData.add(	new regi({MONEDA:'',FN_MONTO: '',FN_MONTO_DSCTO: '',CAMBIOESTATUSNAFINDE: ''}) );
				totalesData.add(	new regi({MONEDA:'',FN_MONTO: '',FN_MONTO_DSCTO: '',CAMBIOESTATUSNAFINDE: ''}) );
				var reg = totalesData.getAt(0);
				reg.set('CD_NOMBRE','MONEDA NACIONAL');
				reg.set('FN_MONTO',montoTotalMN);
				reg.set('FN_MONTO_DSCTO',montoTotalDsctoMN);
				reg.set('CAMBIOESTATUSNAFINDE',numRegistrosMN);
				reg.commit();
				reg = totalesData.getAt(1);
				reg.set('CD_NOMBRE','DOLAR AMERICANO');
				reg.set('FN_MONTO',montoTotalDL);
				reg.set('FN_MONTO_DSCTO',montoTotalDsctoDL);
				reg.set('CAMBIOESTATUSNAFINDE',numRegistrosDL);
				reg.commit();
				gridTotales.show();
			/*Ext.Ajax.request({
					url: '13consulta3Ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Totales'})
					, callback: procesaTotales
				});*/
				el.unmask();
			} else {	
				//botones
				botonPDF.disable();
				botonCSV.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			
			
			var botonE=Ext.getCmp('btnGenerarArchivo');
			botonE.setIconClass('icoXls');
			botonE.enable();
			botonE=Ext.getCmp('btnGenerarPDF');
			botonE.setIconClass('icoPdf');
			botonE.enable();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
function procesaFirmaMancomunada(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
				firmaMancomunada=Ext.util.JSON.decode(response.responseText).firmaMancomunada;
		}else
		{
				firmaMancomunada = '';
		}
	}

function procesaTotales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridTotales.setVisible(true);
			
			totalesData.loadData(Ext.util.JSON.decode(response.responseText).registros);
		} else {
		gridTotales.setVisible(false);
			NE.util.mostrarConnError(response,opts);
		}
	}
var descargaDocumento = function(grid, rowIndex, colIndex, item, event)	{
	var registro = grid.getStore().getAt(rowIndex);
		var ic_documen = registro.get('IC_DOCUMENTO');
	Ext.Ajax.request({
					url: '13consulta3ExtAutorizacion.jsp',
					params: Ext.apply({
						icDocumento: ic_documen})
					, callback: procesarArchivoSuccess
				});
}
//----------------Stores-----------------------------

var totalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'CD_NOMBRE'},
			{name: 'FN_MONTO'},
			{name: 'FN_MONTO_DSCTO'},
			{name: 'CAMBIOESTATUSNAFINDE'}
			
		],
		autoLoad: false,	listeners: {exception: NE.util.mostrarDataProxyError}
	});
	
 var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta3Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
   var catalogoFactoraje = new Ext.data.JsonStore
  ({
	   id: 'catalogoFactoraje',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta3Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoFactoraje'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
 var catalogoBanco = new Ext.data.JsonStore
  ({
	   id: 'catalogoBanco',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta3Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoBanco'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 load:{ fn:function (){
			Ext.getCmp('banco').setValue('1');
		 }}, 
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
 var catalogoProveedores = new Ext.data.JsonStore
  ({
	   id: 'catalogoProveedores',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta3Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoProveedores'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

	var catalogoMoneda = new Ext.data.JsonStore({
	   id:     'catalogoMoneda',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '13consulta3Ext.data.jsp',
		baseParams: {
		 informacion: 'catalogoMoneda'
		},
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatus = new Ext.data.JsonStore({
		id:     'catalogoEstatus',
		root:   'registros',
		fields: ['clave', 'descripcion', 'loadMsg'],
		url:    '13consulta3Ext.data.jsp',
		baseParams: {
		 informacion: 'catalogoEstatus'
		},
		autoLoad: false,
		listeners: {
			exception:  NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '13consulta3Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
			fields: [
					{name: 'NOMBREEPO'},
					{name: 'NOMBREPYME'},
					{name: 'IG_NUMERO_DOCTO'},
					{name: 'DF_FECHA_DOCTO'},
					{name: 'DF_FECHA_VENC'},
					{name: 'DC_FECHA_CAMBIO'},
					{name: 'MONEDA'},
					{name: 'IC_MONEDA'},
					{name: 'CD_NOMBRE'},
					{name: 'FN_MONTO'},
					{name: 'FN_AFORO'},
					{name: 'FN_MONTO_DSCTO'},
					{name: 'CAMBIOESTATUS'},
					{name: 'CT_CAMBIO_MOTIVO'},
					{name: 'IC_CAMBIO_ESTATUS'},
					{name: 'FN_MONTO_ANTERIOR'},
					{name: 'NOMBREMANDANTE'},
					{name: 'FN_MONTO_NUEVO'},
					{name: 'RECIBIR_BENEF'},
					{name: 'CS_DSCTO_ESPECIAL'},
					{name: 'NOMBREFACTORAJE'},
					{name: 'NOMBREIF'},
					{name: 'CG_NOMBRE_USUARIO'},
					{name: 'IC_DOCUMENTO'},
					{name: 'BENEFICIARIO'},
					{name: 'FN_PORC_BENEFICIARIO'},
					{name: 'RECIBIR_BENEF'},
					{name: 'FECHAVENCANT'},
					{name: 'PUB_FIRMA_MANC'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
//------------------Componentes--------------------

var elementosForma = 
				[
				{
				xtype: 'combo',
				editable:false,
				forceSelection: true,
				fieldLabel: 'Banco de Fondeo',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				name:'Hbanco',
				store: catalogoBanco,
				id: 'banco',
				mode: 'local',
				hiddenName: 'Hbanco',
				hidden: true,
				emptyText: 'Seleccionar',
				listeners: {
							select: {
								fn: function(combo) {
									var banco = combo.getValue();
									var iEpo = Ext.getCmp('icEpo');
									iEpo.setValue('');
									iEpo.store.removeAll();
									catalogoEpo.load({
										params: {
												Hbanco: combo.getValue()
											}
									});
									
								}
							}
					}
				},{
				xtype: 'combo',
				fieldLabel: 'EPO',
				emptyText: 'Seleccionar una EPO',
				displayField: 'descripcion',
				//allowBlank: false,
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpo,
				tpl: NE.util.templateMensajeCargaCombo,
				name:'HicEpo',
				id: 'icEpo',
				mode: 'local',
				hiddenName: 'HicEpo',
				forceSelection: true,
				listeners: {
							select: {
								fn: function(combo) {
								
									catalogoProveedores.load({
										params: {
												HicEpo: combo.getValue()
											}
									});
									catalogoFactoraje.load({
										params: {
												HicEpo: combo.getValue()
												
											}
									
									});
									catalogoEstatus.load({
										params: {
												HicEpo: combo.getValue()
												
											}
										});
									Ext.Ajax.request({
										url: '13consulta3Ext.data.jsp',
										params: Ext.apply({
											informacion: 'firmaMancomunada',
											HicEpo: combo.getValue()})
										, callback: procesaFirmaMancomunada
									});
								}
								
							}
					}
			},{
				xtype: 'combo',
				//editable:false,
				fieldLabel: 'Proveedor',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				forceSelection: true,
				name:'Hproveedor',
				store: catalogoProveedores,
				id: 'proveedor',
				mode: 'local',
				hiddenName: 'Hproveedor',
				hidden: false,
				emptyText: 'Seleccionar'
				},{
						xtype: 'textfield',
						name: 'documento',
						id: 'documento',
						allowBlank: true,
						maxLength: 16,
						width: 110,
						msgTarget: 'side',
						fieldLabel: 'N�mero de Documento'
					},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Vencimiento',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaVenc1',
									id: 'fechaVenc1',
									allowBlank: true,
									startDay: 0,
									width: 100,
									minValue: '01/01/1901',
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaVenc2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 20
								},
								{
									xtype: 'datefield',
									name: 'fechaVenc2',
									id: 'fechaVenc2',
									allowBlank: true,
									startDay: 1,
									width: 100,
									minValue: '01/01/1901',
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'fechaVenc1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
									xtype: 'combo',
									fieldLabel: 'Moneda',
									displayField: 'descripcion',
									valueField: 'clave',
									triggerAction: 'all',
									typeAhead: true,
									//allowBlank: false,
									minChars: 1,
									name:'cbMoneda',
									id: 'cbMoneda',
									mode: 'local',
									forceSelection : true,
									hiddenName: 'HcbMoneda',
									hidden: false,
									emptyText: 'Seleccionar Moneda',
									store: catalogoMoneda,
									tpl: NE.util.templateMensajeCargaCombo
								},{
							xtype: 'compositefield',
							fieldLabel: 'Monto',
							msgTarget: 'side',
							combineErrors: false,
							items: [
								{
									xtype: 'numberfield',
									name: 'monto1',
									id: 'monto1',
									allowBlank: true,
									maxLength: 9,
									width: 110,
									msgTarget: 'side',
									vtype: 'rangoValor',
									campoFinValor: 'monto2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'a',
									width: 15
								},
								{
									xtype: 'numberfield',
									name: 'monto2',
									id: 'monto2',
									allowBlank: true,
									maxLength: 9,
									width: 110,
									msgTarget: 'side',
									vtype: 'rangoValor',
									campoInicioValor: 'monto1',
									margins: '0 20 0 0'	  //necesario para mostrar el icono de error
								}
								
							]
						},{
							xtype: 'combo',
							name: 'Hestatus',
							id:	'estatus',
							fieldLabel: 'Tipo de Cambio de Estatus',
							emptyText: 'Seleccionar',
							mode: 'local',
							width: 200,
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'Hestatus',
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoEstatus,
							tpl : NE.util.templateMensajeCargaCombo
						},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Emisi�n',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaEm1',
									id: 'fechaEm1',
									allowBlank: true,
									startDay: 0,
									width: 100,
									minValue: '01/01/1901',
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaEm2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaEm2',
									id: 'fechaEm2',
									allowBlank: true,
									startDay: 1,
									minValue: '01/01/1901',
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'fechaEm1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
							xtype: 'compositefield',
							fieldLabel: 'Fecha de Cambio de Estatus',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'datefield',
									name: 'fechaIF1',
									id: 'fechaIF1',
									allowBlank: true,
									startDay: 0,
									minValue: '01/01/1901',
									width: 100,
									msgTarget: 'side',
									vtype: 'rangofecha', 
									campoFinFecha: 'fechaIF2',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								},
								{
									xtype: 'displayfield',
									value: 'al',
									width: 24
								},
								{
									xtype: 'datefield',
									name: 'fechaIF2',
									id: 'fechaIF2',
									allowBlank: true,
									startDay: 1,
									width: 100,
									minValue: '01/01/1901',
									msgTarget: 'side',
									vtype: 'rangofecha',
									campoInicioFecha: 'fechaIF1',
									margins: '0 20 0 0'  //necesario para mostrar el icono de error
								}
							]
						},{
							xtype: 'combo',
							name: 'Hfactoraje',
							id:	'factoraje',
							fieldLabel: 'Tipo de Factoraje',
							emptyText: 'Seleccionar',
							mode: 'local',
							displayField: 'descripcion',
							valueField: 'clave',
							hiddenName : 'Hfactoraje',
							forceSelection : true,
							triggerAction : 'all',
							typeAhead: true,
							minChars : 1,
							store: catalogoFactoraje,
							tpl : NE.util.templateMensajeCargaCombo
						}
				];
				
	var gridTotales =  new Ext.grid.GridPanel({
		xtype:'grid',	store:totalesData,	id:'gridTotales',		width:940,	height:110,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'Moneda',	dataIndex: 'CD_NOMBRE',	width: 200,	align: 'right',menuDisabled:true
			},{
				header: 'Total Monto',	dataIndex: 'FN_MONTO',	width: 200,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Total Monto Descuento',dataIndex: 'FN_MONTO_DSCTO',	width:200,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
			header: 'Numero de Documentos',dataIndex: 'CAMBIOESTATUSNAFINDE',	width:200,	align: 'right',menuDisabled:true
			}
		]
	});
	
var grid = new Ext.grid.GridPanel({
		store: consultaData,
		viewConfig:{markDirty:false},
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'EPO',	dataIndex: 'NOMBREEPO',
				sortable: true, width: 200, resizable: true, hidden: false,align: 'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: 'Proveedor', tooltip: 'Proveedor',	dataIndex: 'NOMBREPYME',
				sortable: true, width: 150, resizable: true, hidden: false,align: 'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header : 'Nombre IF', tooltip: 'Nombre IF',	dataIndex : 'NOMBREIF',
				sortable : true, width : 100, align: 'center',  hidden: false,align: 'left',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
				
				
			},{
				header : 'Num. Documento', tooltip: 'Num. Documento',	dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, width : 100, align: 'center', hidden: false
			},{
				header : 'Fecha Emisi�n', tooltip: 'Fecha Emisi�n',	dataIndex : 'DF_FECHA_DOCTO',
				sortable : true, width : 150, hidden: false,align: 'center'
			},{
				header : 'Fecha Vencimiento', tooltip: 'Fecha Vencimiento',	dataIndex : 'DF_FECHA_VENC',
				sortable : true, width : 80, hidden: false, hideable: false,align: 'center'
			},{
				header : 'Fecha Cambio de Estatus', tooltip: 'Fecha Cambio de Estatus',	dataIndex : 'DC_FECHA_CAMBIO',
				sortable : true, width : 80, hidden: false, hideable: false,align: 'center'
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'CD_NOMBRE',
				sortable : true, width : 100, hidden: false,hideable: false, align: 'center'
			},{
				header: 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	dataIndex: 'NOMBREFACTORAJE',
				sortable: true,	width: 100,	resizable: true, hidden: false, hideable: false,align: 'center'
			},{
				header: 'Monto', tooltip: 'Monto',	dataIndex: 'FN_MONTO',
				sortable: true,	width: 100,	resizable: true, hidden: false, hideable: false,align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									if (registro.get('IC_CAMBIO_ESTATUS')	==	28) {
									
										return Ext.util.Format.number(registro.get('FN_MONTO_NUEVO'), '$0,0.00');
										
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},{
				header : 'Porcentaje de Descuento', tooltip: 'Porcentaje de Descuento',	dataIndex : 'FN_AFORO',
				width : 100, sortable : true, hidden: false,hideable: false,align: 'center',renderer: Ext.util.Format.numberRenderer('0.00%')
			},{
				header : 'Recurso en Garant�a', tooltip: 'Recurso en Garant�a',	dataIndex : 'CG_CAMPO1',
				width : 100, sortable : true, resizable: true, hidden: false,hideable: false,align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
								estatus= registro.get('IC_CAMBIO_ESTATUS');
								monto=registro.get('FN_MONTO');
								if(estatus==28){
									monto=registro.get('FN_MONTO_NUEVO');
								}
								//if(estatus!=8||estatus!=28||(estatus!=37&&estatus!=38&&estatus!=39&&estatus!=40&&firmaMancomunada=='N')){
									return Ext.util.Format.number(monto-registro.get('FN_MONTO_DSCTO'), '$0,0.00');
									
								}
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'FN_MONTO_DSCTO',
				width : 100, sortable : true, resizable: true, hidden: false,hideable: false,align: 'center',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									if (registro.get('IC_CAMBIO_ESTATUS')	==	28) {
										
										return Ext.util.Format.number(registro.get('FN_MONTO_NUEVO'), '$0,0.00');
										
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},{
				header: 'Tipo Cambio de Estatus', tooltip: 'Tipo Cambio de Estatus',	dataIndex: 'CAMBIOESTATUS',
				width: 100, sortable: true, resizable: true, hidden: false,hideable: false,align: 'center',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									metaData.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
									if (registro.get('IC_CAMBIO_ESTATUS')	==	28) {
										
										return value +' '+registro.get('DF_FECHA_VENC');
										
									}
									return value;
								}
			},{
				header: 'Causa', tooltip: 'Causa',	dataIndex: 'CT_CAMBIO_MOTIVO',
				sortable: true,	width: 150,	align: 'left', resizable: true, hidden: false,hideable: false,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: 'Usuario', tooltip: 'Usuario',	dataIndex: 'CG_NOMBRE_USUARIO',
				sortable: true,	width: 150,	align: 'left', resizable: true, hidden: false,hideable: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				xtype: 'actioncolumn',
				header: 'Documento Autorizaci�n', tooltip: 'Documento Autorizaci�n',	dataIndex: 'IC_DOCUMENTO',
				sortable: true, width: 100, resizable: true, hideable: false, align: 'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('IC_CAMBIO_ESTATUS')	==	31) {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else{return '';}
							
						}
						,handler:	descargaDocumento
					}
				]
			},{
			
				header: 'Monto Anterior', tooltip: 'Monto Anterior',	dataIndex: 'FN_MONTO_ANTERIOR',
				sortable: true, width: 100, resizable: true, hideable: false, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{//28
				header : 'Fecha de Vencimiento Anterior', tooltip: 'Fecha de Vencimiento Anterior',	dataIndex : 'FECHAVENCANT',
				sortable : true, width : 100, hidden: false,hideable: false,align: 'center',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									return value;
								}
			},{
				header: 'Beneficiario', tooltip: 'Beneficiario',	dataIndex: 'BENEFICIARIO',
				sortable: true, width: 100, resizable: true, hidden: false,hideable: false,align: 'left',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header : '% Beneficiario', tooltip: '% Beneficiario',	dataIndex : 'FN_PORC_BENEFICIARIO',
				sortable : true, width : 100, align: 'center', resizable: true,  hidden: false,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									if (value	!=	-1) {
										
										return Ext.util.Format.number(value, '00.00%');
										
									}
									return '';
								}
			},{
				header: 'Importe a Recibir Beneficiario',
				tooltip: 'Importe a Recibir Beneficiario',
				width: 130,
				dataIndex: 'RECIBIR_BENEF',
				align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									if (value	!=	-1) {
										
										return Ext.util.Format.number(value, '$0,0.00');
										
									}
									return '';
								}
			},{
				header : 'Mandante', tooltip: 'Mandante',	dataIndex : 'NOMBREMANDANTE',
				sortable : true, width : 100, align: 'center', hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: 'No hay registros.',
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					tooltip: 'Imprime todos los registros en formato CSV.',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta3Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'}),
							callback: procesarArchivoSuccess
						});
					}
				},
				'-',
				{
					xtype:   'button',
					text:    'Generar Todo',
					tooltip: 'Imprime todos los registros en formato PDF.',
					iconCls: 'icoPdf',
					id:      'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta3Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'ArchivoPDF',
									start: 0,
									limit: 0
								}),
							callback: procesarArchivoSuccess
						});
					}
				}
/*
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '13consulta3Ext.data.jsp',
							params: {
									informacion: 'ArchivoPDF',
									start: cmpBarraPaginacion.cursor,
									limit: cmpBarraPaginacion.pageSize
								},
							callback: procesarArchivoSuccess
						});
					}
				}
*/
			]
		}
	});
//-----------------Principal----------------------

var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 150,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						
						if(!fp.getForm().isValid()){
							return;
						}
						if(verificaFechas('fechaVenc1','fechaVenc2')&&verificaFechas('monto1','monto2')&&verificaFechas('fechaEm1','fechaEm2')
								&&verificaFechas('fechaIF1','fechaIF2')){
						//gridTotales.hide();
						if(!comparaFechas('fechaVenc1','fechaEm1')||!comparaFechas('fechaVenc2','fechaEm2')){
							Ext.Msg.alert("Mensaje de p�gina web.",	"La fecha de vencimiento debe ser mayor a la del documento");
							return;
						}
						if(!comparaFechas('fechaIF1','fechaEm1')||!comparaFechas('fechaIF2','fechaEm2')){
							Ext.Msg.alert("Mensaje de p�gina web.",	"La fecha de cambio de estatus debe ser mayor a la del documento");
							return;
						}
						grid.show();
						gridTotales.hide();
						consultaData.load({ params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15})
								});
						}
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});


var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',

	  width: 940,
	  items:
	   [
	  fp,NE.util.getEspaciador(30),grid,NE.util.getEspaciador(30),gridTotales
	  ]
  });
	catalogoBanco.load();
	catalogoEpo.load();
	catalogoFactoraje.load();
	catalogoMoneda.load();
	catalogoEstatus.load();
});