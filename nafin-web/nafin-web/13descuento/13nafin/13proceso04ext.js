var showPanelLayoutNotificacion;

Ext.onReady(function() {
	
	//--------------------------------- OVERRIDES ------------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});
	
	//-------------------------------- VALIDACIONES -----------------------------------
 
	//----------------------------------- HANDLERS ------------------------------------
 
	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaArchivo(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaArchivo(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceValidacion();
         
         // Ocultar mascara del panel de resultados de la validacion
         var element = Ext.getCmp("panelResultadosValidacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var cargaArchivo = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"					){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13proceso04ext.data.jsp',
				params: 	{
					informacion:		'CargaArchivo.inicializacion'
				},
				callback: 				procesaCargaArchivo
			});
			
		} else if(			estado == "MOSTRAR_AVISO"					){

			if(!Ext.isEmpty(respuesta.aviso)){
				
				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);
				
				var panelAvisos	= Ext.getCmp('panelAvisos');
				panelAvisos.show();
				
			}
 
		} else if(			estado == "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES"	){
 
			// Mostrar panel de carga de archivo
			var panelFormaCargaArchivo = Ext.getCmp("panelFormaCargaArchivo");
			panelFormaCargaArchivo.getForm().clearInvalid();
			panelFormaCargaArchivo.show();
		
			// Definir el texto del layout de carga
			if(!Ext.isEmpty(respuesta.reinstalacionesLayout)){
				Ext.getCmp("labelLayoutCarga").setText(respuesta.reinstalacionesLayout,false);
			}
 
		} else if(			estado == "SUBIR_ARCHIVO"								){
			
			Ext.getCmp("panelFormaCargaArchivo").getForm().submit({
				clientValidation: 	true,
				url: 						'13proceso04ext.data.jsp?informacion=CargaArchivo.subirArchivo',
				waitMsg:   				'Subiendo archivo...',
				waitTitle: 				'Por favor espere',
				success: function(form, action) {
					 procesaCargaArchivo(null,  true,  action.response );
				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaCargaArchivo(null,  false, action.response );
				}
			});
			
		} else if(			estado == "REALIZAR_VALIDACION"         			){
 
			showWindowAvanceValidacion();
			
			Ext.Ajax.request({
				url: 		'13proceso04ext.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.realizarValidacionEInsercion',
					fileName:									respuesta.fileName
				},
				callback: 										procesaCargaArchivo
			});
		
		} else if(  estado == "ESPERAR_DECISION_VALIDACION_INSERCION" ){
 			
			var totalRegistros 				= respuesta.totalRegistros;   
			var numeroRegistrosSinErrores	= respuesta.numeroRegistrosSinErrores;
			var numeroRegistrosConErrores	= respuesta.numeroRegistrosConErrores;
			var cargaExistosa 				= respuesta.cargaExistosa;

			// SETUP PARAMETROS VALIDACION

			// Recordar el total de registros
			Ext.getCmp("panelResultadosValidacion.totalRegistros").setValue(totalRegistros);
 
         // Inicializar panel de resultados
			Ext.getCmp("numeroRegistrosSinErrores").setValue(numeroRegistrosSinErrores);
			Ext.getCmp("numeroRegistrosConErrores").setValue(numeroRegistrosConErrores);
 
			// Determinar si se mostrar� el bot�n de continuar carga
			var botonAceptarCarga       = Ext.getCmp('botonAceptarCarga');
			// Habilitar boton aceptar carga
			botonAceptarCarga.enable();
 
			// MOSTRAR COMPONENTES
			
			// Resetear campo de carga de archivo
			Ext.getCmp("archivo").reset();
			
			// Ocultar ventana de avance validacion
			hideWindowAvanceValidacion();
			
			 // Remover contenido anterior de los grids
         var registrosSinErroresData 		= Ext.StoreMgr.key('registrosSinErroresDataStore');
         var registrosConErroresData 		= Ext.StoreMgr.key('registrosConErroresDataStore');
         
         registrosSinErroresData.removeAll();
         registrosConErroresData.removeAll();
 
         // Mostrar espaciador del Panel de resultados
			Ext.getCmp("espaciadorPanelResultadosValidacion").show();
			// Mostrar Panel de resultados
         Ext.getCmp('panelResultadosValidacion').show();

         // Mostrar Panel de Resultados de la validacion
         var tabPanelResultadosValidacion = Ext.getCmp('tabPanelResultadosValidacion');
         var panelFormaCargaArchivo			= Ext.getCmp('panelFormaCargaArchivo');
         var botonesResultadosValidacion	= Ext.getCmp('botonesResultadosValidacion');
         var labelOperacionExitosa			= Ext.getCmp('labelOperacionExitosa');
         
         if( cargaExistosa ){
         	
         	panelFormaCargaArchivo.hide();
         	tabPanelResultadosValidacion.unhideTabStripItem(0);
         	tabPanelResultadosValidacion.setActiveTab(0);
				registrosSinErroresData.loadData(respuesta.registrosSinErroresDataArray);
				Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
	 
				tabPanelResultadosValidacion.hideTabStripItem(1);
				botonesResultadosValidacion.show();
				
				labelOperacionExitosa.show();
				
			} else {
				
				panelFormaCargaArchivo.show();
				tabPanelResultadosValidacion.unhideTabStripItem(1);
				tabPanelResultadosValidacion.setActiveTab(1);
				registrosConErroresData.loadData(respuesta.registrosConErroresDataArray);
				Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
				
				tabPanelResultadosValidacion.hideTabStripItem(0);
				botonesResultadosValidacion.hide();
				
         }
 
      } else if(  estado == "ESPERAR_DECISION"  			){
      	
      	// Ocultar mascara del panel de preacuse
			var element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
      	return;
 
		} else if(	estado == "FIN"								){
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "13proceso04ext.jsp";
			forma.target	= "_self";
			forma.submit();
 
		}
		
		return;
		
	}
 
	//---------------------------- 3. PANEL LAYOUT DE CARGA ------------------------------
	
	var hidePanelLayoutNotificacion = function(){
		Ext.getCmp('panelLayoutNotificacion').hide();
		Ext.getCmp('espaciadorPanelLayoutNotificacion').hide();
	}
	
	showPanelLayoutNotificacion = function(){
		Ext.getCmp('panelLayoutNotificacion').show();
		Ext.getCmp('espaciadorPanelLayoutNotificacion').show();
	}
	                             
	var elementosLayoutNotificacion = [
		{
			xtype: 	'label',
			id:		'labelLayoutCarga',
			name:		'labelLayoutCarga',
			html: 	"" ,
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginBottom: 	'10px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		}
	];
	
	var panelLayoutNotificacion = {
		xtype:			'panel',
		id: 				'panelLayoutNotificacion',
		hidden:			true,
		width: 			600,
		title: 			'Descripci�n del Layout de Carga',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosLayoutNotificacion,
		tools: [
			{
				id:			'close',
				handler: function(event, toolEl, panel){
					hidePanelLayoutNotificacion();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      )
	}
	
	var espaciadorPanelLayoutNotificacion = {
		xtype: 	'box',
		id:		'espaciadorPanelLayoutNotificacion',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//------------------------- 5. PANEL RESULTADOS VALIDACION ---------------------------
 
	var procesarConsultaRegistrosSinErrores 	= function(store, registros, opts){
		
		var gridRegistrosSinErrores 						= Ext.getCmp('gridRegistrosSinErrores');
		
		if (registros != null) {
			
			var el 							= gridRegistrosSinErrores.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaRegistrosConErrores = function(store, registros, opts){
		
		var gridRegistrosConErrores						= Ext.getCmp('gridRegistrosConErrores');
		
		if (registros != null) {
			
			var el 							= gridRegistrosConErrores.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
 
	var registrosSinErroresData = new Ext.data.ArrayStore({
			
		storeId: 'registrosSinErroresDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' 								},
			{ name: 'FOLIO', 				mapping: 1 													},
			{ name: 'NUMERO_PRESTAMO', mapping: 2 													},
			{ name: 'FECHA_OPERACION', mapping: 3, type: 'date', dateFormat: 'd/m/Y'	}
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosSinErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosSinErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var registrosConErroresData = new Ext.data.ArrayStore({
			
		storeId: 'registrosConErroresDataStore',
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' },
			{ name: 'DESCRIPCION', 		mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosConErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosConErrores(null, null, null);						
				}
			}
		}
		
	});
 
	var elementosResultadosValidacion = [
		{
			xtype: 	'label',
			id:	 	'labelOperacionExitosa',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:2px;',
			html:  	"La operaci�n se complet� con �xito.",
			hidden:	true
		},
		{ 
			xtype: 			'tabpanel',
			id:				'tabPanelResultadosValidacion',
			activeTab:		0,
			plain:			true,
			height: 			400,
			//bodyStyle:		'padding: 10px;',
			items: [
				{
					xtype:		'container',
					title: 		'Resumen', //'Registros sin Errores',
					layout:		'vbox',
               layoutConfig: {
						align:   'stretch',
						pack:    'start'
					},
					items: [
						{
							store: 		registrosSinErroresData,
							xtype: 		'grid',
							id:			'gridRegistrosSinErrores',
							stripeRows: true,
							loadMask: 	true,	
							frame: 		false,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'Folio',
									tooltip: 	'Folio',
									dataIndex: 	'FOLIO',
									sortable: 	true,
									resizable: 	true,
									width: 		264,
									hidden: 		false
								},
								{
									header: 		'N�mero de Pr�stamo',
									tooltip: 	'N�mero de Pr�stamo',
									dataIndex: 	'NUMERO_PRESTAMO',
									sortable: 	true,
									resizable: 	true,
									width: 		150,
									hidden: 		false
								},
								{
									header: 		'Fecha de Operaci�n',
									tooltip: 	'Fecha de Operaci�n',
									dataIndex: 	'FECHA_OPERACION',
									sortable: 	true,
									resizable: 	true,
									width: 		150,
									hidden: 		false,
									renderer: 	Ext.util.Format.dateRenderer('d/m/Y')
								}
							]					
						},
						{
							xtype: 		'container',
							layout:		'form',
							labelWidth: 200,
							style:		'padding:8px;',
							height:		35,
							//width:		400,
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Total de Folios Recibidos',
									name: 			'numeroRegistrosSinErrores',
									id: 				'numeroRegistrosSinErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		6, // Maximo 6 	
									msgTarget: 		'side',
									anchor:			'-220',
									style: {
										textAlign: 'right'
									}
								}								
							]
						}
					]
				},
				{
					xtype:		'container',
					title: 		'Errores Generados', //'Registros con Errores',
					layout:		'vbox',
               layoutConfig: {
						align:   'stretch',
						pack:    'start'
					},
					items: [
						{
							store: 		registrosConErroresData,
							xtype: 		'grid',
							id:			'gridRegistrosConErrores',
							stripeRows: true,
							loadMask: 	true,	
							frame: 		false,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'Descripci�n',
									tooltip: 	'Descripci�n',
									dataIndex: 	'DESCRIPCION',
									sortable: 	true,
									resizable: 	true,
									width: 		1150,
									hidden: 		false
								}
							]
						},
						{
							xtype: 		'container',
							layout:		'form',
							labelWidth: 200,
							style:		"padding:8px;",
							height:		70,
							//width:		400,
							hidden:		true,
							items: [
								// TEXTFIELD Total de Registros
								{ 
									xtype: 			'textfield',
									fieldLabel:		'Total de Registros',
									name: 			'numeroRegistrosConErrores',
									id: 				'numeroRegistrosConErrores',
									readOnly:		true,
									hidden: 			false,
									maxLength: 		6,	// Maximo 6
									msgTarget: 		'side',
									anchor:			'-220',
									style: {
									  textAlign: 'right'
								   }
								}
							]
						}
					]
				}
			]
		},
		{
			xtype: 			'container',
			id:				'botonesResultadosValidacion',
			anchor: 			'100%',
			style: 			'margin: 0px;padding-top:15px;',
			renderHidden: 	false,
			layout: {
				type: 		'hbox',
				pack: 		'center'
			},
			items: [
				{
					xtype: 		'button',
					minWidth: 	128,				// Nota: Se usa minWidth debido a un raro bug en IE7 que ocasiona no se actualicen las posiciones de los botones
					text: 		'Aceptar',
					iconCls: 	'icoAceptar',
					id: 			'botonAceptarCarga',
					margins: 	' 3',
					handler:    function(boton, evento) {
						cargaArchivo("FIN",null);
					}
				}
			] 
		},
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.totalRegistros',
        id: 	'panelResultadosValidacion.totalRegistros'
      }
	];
	
	var panelResultadosValidacion = {
		title:			'Resultados', //'Resultado de la Validaci�n',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosValidacion',
		width: 			600, //949,
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 10px',
		items: 			elementosResultadosValidacion,
		layout:			'anchor'
	};

	var espaciadorPanelResultadosValidacion = {
		xtype: 	'box',
		id:		'espaciadorPanelResultadosValidacion',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//---------------------------- PANEL AVANCE VALIDACION ------------------------------
	
	var elementosAvanceValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Revisando solicitudes...'
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceValidacion',
			name:	 'barrarAvanceValidacion',
			cls:	 'center-align',
			value: 0.00,
			text:  '0.00%'
		}
	];
	
	var panelAvanceValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceValidacion',
		frame: 			true,
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		items: 			elementosAvanceValidacion
	});
	
	var showWindowAvanceValidacion = function(){
		
		var barrarAvanceValidacion		= Ext.getCmp("barrarAvanceValidacion");
 
		barrarAvanceValidacion.updateProgress(0.00,"0.00 %",true);
		barrarAvanceValidacion.wait({
            interval:	200,
            increment:	15
      });
 
		var ventana = Ext.getCmp('windowAvanceValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				width:			300,
				resizable:		false,
				id: 				'windowAvanceValidacion',
				modal:			true,
				closable: 		false,
				items: [
					Ext.getCmp("panelAvanceValidacion")
				]
			}).show();
			
		}
		
	}	

	var hideWindowAvanceValidacion = function(){
		
		var ventanaAvanceValidacion = Ext.getCmp('windowAvanceValidacion');
		
		if(ventanaAvanceValidacion && ventanaAvanceValidacion.isVisible() ){
			var barrarAvanceValidacion	 = Ext.getCmp("barrarAvanceValidacion");
			barrarAvanceValidacion.reset(); 
			
			
			if (ventanaAvanceValidacion) {
				ventanaAvanceValidacion.hide();
			}
      }
      
	}
 
	//-------------------------- 1. FORMA DE CARGA DE ARCHIVO ---------------------------
 
	var elementosFormaCargaArchivo = [
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivo',
		  name: 			'archivo',
		  emptyText: 	'Seleccione...',
		  fieldLabel: 	"Ruta del Archivo de Origen", 
		  buttonText: 	null,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'-20'
		  //vtype: 		'archivotxt'
		},
		{
			xtype: 		'panel',
			anchor: 		'100%',
			style: {
				marginTop: 		'10px'
			},
			layout: {
				type: 'hbox',
				pack: 'end',
				align: 'middle'
			},
			items: [
				{
					xtype: 			'button',
					text: 			'Continuar',
					iconCls: 		'icoContinuar',
					id: 				'botonContinuarCargaArchivo',				
					handler:    function(boton, evento) {
						
						// Revisar si la forma es invalida
						var forma 		= Ext.getCmp("panelFormaCargaArchivo").getForm();
						if(!forma.isValid()){
							return;
						}
	 
						// Validar que se haya especificado un archivo
						var archivo 	= Ext.getCmp("archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}
						
						// Revisar el tipo de extension del archivo
						var myRegexTXT = /^.+\.([tT][xX][tT])$/;
						
						var nombreArchivo 	= archivo.getValue();
						if( !myRegexTXT.test(nombreArchivo) ) {
							archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n txt.");
							return;
						}
						
						// Cargar archivo
						cargaArchivo("SUBIR_ARCHIVO",null);

					}
				}
			]
		}
	];
	
	var panelFormaCargaArchivo = new Ext.form.FormPanel({
		id: 				'panelFormaCargaArchivo',
		width: 			600,
		title: 			'Carga de Archivo',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		fileUpload: 	true,
		hidden:			true,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	190,
		defaultType: 	'textfield',
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'archivo\'">',
           		 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
						  		'<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showPanelLayoutNotificacion();"></a>&nbsp;',
						  		'{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
						  '</div>',
					 '</div>',
			  '</tpl>',
			  '<tpl if="id!=\'archivo\'">',
					 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
					 '</div>',
			  '</tpl>'
		   )
		},
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementosFormaCargaArchivo,
		monitorValid: 	false
	});
 
	//------------------------------------ 1. PANEL AVISOS ---------------------------------------
	
	var elementosPanelAvisos = [
		{
			xtype: 	'label',
			id:	 	'labelAviso',
			cls:		'x-form-item',
			style: 	'font-weight:normal;text-align:center;margin:15px;color:black;',
			html:  	''
		}
	];
	
	var panelAvisos = {
		xtype:			'panel',
		id: 				'panelAvisos',
		hidden:			true,
		width: 			600,
		title: 			'Avisos',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosPanelAvisos
	}
	
	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			panelAvisos,
			panelFormaCargaArchivo,
			espaciadorPanelLayoutNotificacion,
			panelLayoutNotificacion,
			espaciadorPanelResultadosValidacion,
			panelResultadosValidacion
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	cargaArchivo("INICIALIZACION",null);
 
});;