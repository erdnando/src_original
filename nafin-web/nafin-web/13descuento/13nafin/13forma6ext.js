Ext.onReady(function(){

//--------------------------------HANDLERS-----------------------------------
	//FUNCION PARA OCULTAR LOS CAMPOS DE FECHAS Y RESETEARLAS
	var accionLimpiar = function(){
		Ext.getCmp('compEstados').hide();
		Ext.getCmp('compVencimientos').hide();
		Ext.getCmp('compOperados').hide();
		Ext.getCmp('gridPrincipal').hide();
		resetearFechas();
	}
	//FUNCION PARA RESETEAR TODAS LAS FECHAS
	var resetearFechas = function(){
		Ext.getCmp('fecha_ejec_de').reset();
		Ext.getCmp('fecha_ejec_a').reset();
		Ext.getCmp('fecha_fin_mes_de').reset();
		Ext.getCmp('fecha_fin_mes_a').reset();
		Ext.getCmp('fecha_prob_de').reset();
		Ext.getCmp('fecha_prob_a').reset();
		Ext.getCmp('fecha_oper_de').reset();
		Ext.getCmp('fecha_oper_a').reset();
		if(gridPrincipal.isVisible()){
			gridPrincipal.hide();
		}
	}
	
	var cambioRadio = function( radio, newValue, oldValue, eOpts ){
		accionLimpiar();
	}
	
	//FUNCION PARA MOSTRAR U OCULTAR ATRIBUTOS DEL GRID Y LAS FECHAS
	var seleccionCombo = function(combo){
		var valueProceso = combo.getValue();
		var compoEstados = Ext.getCmp('compEstados');
		var compoVencimientos = Ext.getCmp('compVencimientos');
		var compoOperados = Ext.getCmp('compOperados');	
		if(valueProceso == 'E'){
			var cm = gridPrincipal.getColumnModel();
			gridPrincipal.setWidth(685);
			cm.setHidden(cm.findColumnIndex('ID_PROCEDIMIENTO'), true);
			cm.setHidden(cm.findColumnIndex('FECHA_FIN_MES'), false);
			cm.setHidden(cm.findColumnIndex('FECHA_PROB_PAGO'), true);
			cm.setHidden(cm.findColumnIndex('FECHA_OPERACION'), true);
			cm.setHidden(cm.getIndexById('actionCReprocesar'), true);
			compoEstados.show();
			compoVencimientos.hide();
			compoOperados.hide();
			resetearFechas();
		}else if(valueProceso == 'V'){
			gridPrincipal.setWidth(800);
			var cm = gridPrincipal.getColumnModel();
			cm.setHidden(cm.findColumnIndex('ID_PROCEDIMIENTO'), false);
			cm.setHidden(cm.findColumnIndex('FECHA_FIN_MES'), true);
			cm.setHidden(cm.findColumnIndex('FECHA_PROB_PAGO'), false);
			cm.setHidden(cm.findColumnIndex('FECHA_OPERACION'), true);
			cm.setHidden(cm.getIndexById('actionCReprocesar'), false);
			compoEstados.hide();
			compoVencimientos.show();
			compoOperados.hide();
			resetearFechas();
		}else if(valueProceso == 'O'){
			gridPrincipal.setWidth(760);
			var cm = gridPrincipal.getColumnModel();
			cm.setHidden(cm.findColumnIndex('ID_PROCEDIMIENTO'), true);
			cm.setHidden(cm.findColumnIndex('FECHA_FIN_MES'), true);
			cm.setHidden(cm.findColumnIndex('FECHA_PROB_PAGO'), true);
			cm.setHidden(cm.findColumnIndex('FECHA_OPERACION'), false);
			cm.setHidden(cm.getIndexById('actionCReprocesar'), false);
			compoEstados.hide();
			compoVencimientos.hide();
			compoOperados.show();
			resetearFechas();
		}
	}
	//FUNCION PARA LAS VALIDACIONES Y CONSULTA 
	function realizarConsulta(){
		var bandera    = 0;
		var banderaAux = 0;
		var fechaDe    = '';
		var fechaA     = '';
		var tipoProceso         = Ext.getCmp('cmbProceso').getValue();
		var radioIF             = Ext.getCmp('radioIF').getValue().getRawValue();
		var fechaEjecDe         = Ext.getCmp('fecha_ejec_de');
		var fechaEjecA          = Ext.getCmp('fecha_ejec_a');
		var fechaEstadosDe      = Ext.getCmp('fecha_fin_mes_de');
		var fechaEstadosA       = Ext.getCmp('fecha_fin_mes_a');
		var fechaVencimientosDe = Ext.getCmp('fecha_prob_de');
		var fechaVencimientosA  = Ext.getCmp('fecha_prob_a');
		var fechaOperadosDe     = Ext.getCmp('fecha_oper_de');
		var fechaOperadosA      = Ext.getCmp('fecha_oper_a');

		if(!Ext.getCmp('forma').getForm().isValid()){
			return;
		}
		if(fechaEjecDe.getValue() == '' && fechaEjecA.getValue() != ''){
			fechaEjecDe.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}
		if(fechaEjecDe.getValue() != '' && fechaEjecA.getValue() == ''){
			fechaEjecA.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			return;
		}
		if(fechaEjecDe.getValue() != '' && fechaEjecA.getValue() != ''){
			banderaAux = 1;
		}

		if(tipoProceso == 'E'){

			if(fechaEstadosDe.getValue() == '' && fechaEstadosA.getValue() != ''){
				fechaEstadosDe.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				return;
			}
			if(fechaEstadosDe.getValue() != '' && fechaEstadosA.getValue() == ''){
				fechaEstadosA.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				return;
			}
			if((fechaEjecDe.getValue() != '' && fechaEjecA.getValue() != '') && (fechaEstadosDe.getValue() != '' && fechaEstadosA.getValue() != '' ) ){
				Ext.MessageBox.alert('Mensaje','Debe seleccionar solo un rango de fechas ya sea Fecha de Ejecuci�n o Fecha Fin de Mes');
				return;
			}
			bandera = 1;
			fechaDe = fechaEstadosDe.getRawValue();
			fechaA = fechaEstadosA.getRawValue();

		} else if(tipoProceso == 'V'){

			if(fechaVencimientosDe.getValue() == '' && fechaVencimientosA.getValue() != ''){
				fechaVencimientosDe.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				return;
			}
			if(fechaVencimientosDe.getValue() != '' && fechaVencimientosA.getValue() == ''){
				fechaVencimientosA.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				return;
			}
			if((fechaEjecDe.getValue() != '' && fechaEjecA.getValue() != '') && (fechaVencimientosDe.getValue() != '' && fechaVencimientosA.getValue() != '' ) ){
				Ext.MessageBox.alert('Mensaje','Debe seleccionar solo un rango de fechas ya sea Fecha de Ejecuci�n o Fecha Probable de Pago');
				return;
			}
			bandera = 1;
			fechaDe = fechaVencimientosDe.getRawValue();
			fechaA = fechaVencimientosA.getRawValue();

		} else if(tipoProceso == 'O'){

			if(fechaOperadosDe.getValue() == '' && fechaOperadosA.getValue() != ''){
				fechaOperadosDe.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				return;
			}
			if(fechaOperadosDe.getValue() != '' && fechaOperadosA.getValue() == ''){
				fechaOperadosA.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
				return;
			}
			if((fechaEjecDe.getValue() != '' && fechaEjecA.getValue() != '') && (fechaOperadosDe.getValue() != '' && fechaOperadosA.getValue() != '' ) ){
				Ext.MessageBox.alert('Mensaje','Debe seleccionar solo un rango de fechas ya sea Fecha de Ejecuci�n o Fecha Probable de Pago');
				return;
			}
			bandera = 1;
			fechaDe = fechaOperadosDe.getRawValue();
			fechaA = fechaOperadosA.getRawValue();

		}

		if(bandera == 1 || banderaAux == 1){
			pnl.el.mask('Procesando...', 'x-mask-loading');
			consultaData.load({
				params: {
					informacion: 'consultaProcesos',
					fecha_ejec_de: fechaEjecDe.getRawValue(),
					fecha_ejec_a: fechaEjecA.getRawValue(),
					fecha_proc_de: fechaDe,
					fecha_proc_a: fechaA,
					tipo_proceso: tipoProceso,
					tipo_if: radioIF
				}
			});
		}
	}

	//FUNCION QUE VALIDA QUE LA CONSULTA SE REALICE EXITOSAMENTE
	var procesarConsultaProcesosData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('gridPrincipal');
		var btnCancelar = Ext.getCmp('btnCancelar');
		grid.el.unmask();
		pnl.el.unmask();
		if (arrRegistros != null) {	
			if(!gridPrincipal.isVisible()){
				gridPrincipal.show();
			}
			if(store.getTotalCount() <= 0) {
				btnCancelar.disable();
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			} else {							
				grid.el.unmask();
				btnCancelar.enable();
			}
		}
	}
	//FUNCION PARA CANCELAR PROCESOS
	var cancelarProceso = function(sel){
		gridPrincipal.el.mask('Procesando...','x-mask-loading');
		var btnCancelar = Ext.getCmp('btnCancelar');
		var tipoProceso = Ext.getCmp('cmbProceso').getValue();
		var selections = sel.getSelections();
		var chk_cancel_proceso = new Array();
		if(selections.length <= 0){
			Ext.MessageBox.alert("Alerta","Debe seleccionar al menos una opci�n para Cancelar un Proceso");
			gridPrincipal.el.unmask();
			btnCancelar.enable();
		}else{	
			for(i=0 ;i < selections.length; i++){			
				chk_cancel_proceso[i] = selections[i].json.ID_PROCEDIMIENTO;
			}
			Ext.Ajax.request({
				url: '13forma6ext.data.jsp',
				params: {
					informacion: 'cancelarProcesos',
					ids_procesos: chk_cancel_proceso,
					tipo_proceso: tipoProceso
				},
				callback: procesarCancelacionProcesos
			});
		}
	}
	//FUNCION QUE VALIDA QUE LA CANCELACION SE REALICE EXITOSAMENTE
	var procesarCancelacionProcesos = function(opts, success, response){		
		if (success == true) {
			Ext.getCmp('btnCancelar').enable();
			Ext.MessageBox.alert("Alerta","Los registros se eliminaron satisfactoriamente");
			realizarConsulta();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	//FUNCION QUE PERMITE REPROCESAR LOS PROCESOS
	var accionReprocesar = function(grid, rowIndex, colIndex, item, event){
		var tipoProceso = Ext.getCmp('cmbProceso').getValue();
		var registro = gridPrincipal.getStore().getAt(rowIndex);
		var icProd = registro.get('ID_PROCEDIMIENTO');
		var chk_reprocesar = new Array();
		chk_reprocesar[0] = icProd;
		Ext.Msg.show({
			title: "Confirmar",
			msg: "IMPORTANTE:\n Se reprocesar�n todos los\n registros que " +
			"correspondan\n a la misma Fecha de Ejecuci�n. �Desea continuar?",
			buttons: Ext.Msg.OKCANCEL,
			fn: function peticionAjax(btn){
				if (btn == 'ok') {
					gridPrincipal.el.mask('Procesando...','x-mask-loading');
					Ext.Ajax.request({
						url: '13forma6ext.data.jsp',
						params: {
							informacion: 'reprocesarProcesos',
							ids_procesos: chk_reprocesar,
							tipo_proceso: tipoProceso
						},
						callback: procesarAccionReprocesar
					});
				}
			},
			closable:false,
			icon: Ext.MessageBox.QUESTION
		});
	}
	//FUNCION QUE VALIDA QUE EL REPROCESO SE REALICE EXITOSAMENTE
	var procesarAccionReprocesar = function(opts, success, response){
		var resp = 	Ext.util.JSON.decode(response.responseText);
		if (success == true) {
			Ext.getCmp('btnCancelar').enable();
			Ext.Msg.alert("Mensaje de p�gina web.",	resp.textoMensaje);
			realizarConsulta();
			gridPrincipal.el.unmask();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var mostrarVentanaDetalles = function(grid, rowIndex, colIndex, item, event){
		var tipoProceso = Ext.getCmp('cmbProceso').getValue();
		var nombreProceso = Ext.getCmp('cmbProceso').getRawValue();
		var radioIF = Ext.getCmp('radioIF').getValue().getRawValue();
		
		var registro = gridPrincipal.getStore().getAt(rowIndex);
		var idProceso = registro.get('ID_PROCEDIMIENTO');
		var fecha = registro.get('FECHA_OPERACION');
		var grid1 = Ext.getCmp('gridDetalles');
		grid1.setTitle(nombreProceso + ' correspondientes al ' + fecha);
		//gridDetalles.el.mask('Leyendo...','x-mask-loading');
		consultaDataDetalle.load({
			params: {
				informacion: 'consultaDetalleProcesos',
				tipo_proceso: tipoProceso,
				tipo_if: radioIF,
				id_proceso: idProceso,
				fecha_detalle: fecha
			}
		});
		consultaTotalesData.load({
			params: {
				informacion: 'ConsultarTotales',
				tipo_proceso: tipoProceso,
				tipo_if: radioIF,
				id_proceso: idProceso,
				fecha_detalle: fecha
			}
		});
		Ext.getCmp('winDetalles').show();
	}
	
	var procesarConsultaDataDetalle = function(store, arrRegistros, opts) 	{
		var grid = Ext.getCmp('gridDetalles');
		var tipoProceso = Ext.getCmp('cmbProceso').getValue();
		var radioIF = Ext.getCmp('radioIF').getValue().getRawValue();
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		var btnRegresar = Ext.getCmp('btnBajarArchivo');
		grid.show();
		grid.el.mask('Leyendo...','x-mask-loading');
		gridDetalles.el.mask('Leyendo...','x-mask-loading');
		if (arrRegistros != null) {	
			if(!grid.isVisible()){
				grid.show();
			}
			if(store.getTotalCount() <= 0) {
				btnBajarArchivo.disable();
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			} else {							
				grid.el.unmask();
				btnBajarArchivo.enable();
			}
		}
	}//procesarConsultaTotalesData
	
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var grid = Ext.getCmp('gridTotales');
		var record = consultaTotalesData.getAt(0);
		grid.show();
		grid.el.mask('Leyendo...','x-mask-loading');
		if (arrRegistros != null) {	
			if(!grid.isVisible()){
				grid.show();
			}
			if(store.getTotalCount() <= 0) {
				grid.el.mask('No se encontr� ning�n registro', 'x-mask');
			} else {
			if(record.get('TOTALES_MN') != "0" || record.get('TOTALES_DLS') != "0"){
					grid.el.unmask();
				}else{
					grid.el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	}
	
	var descargaArchivoMN = function(grid, rowIndex, colIndex, item, event){
		var tipoProceso = Ext.getCmp('cmbProceso').getValue();
	//	var grid = Ext.getCmp('gridDetalles');	
	//	grid.el.mask('Procesando consulta', 'x-mask');
		var registro = gridDetalles.getStore().getAt(rowIndex);
		var numIf = registro.get('IC_IF');
		var icMoneda = '1';
		var fechaDCorte = registro.get('FECHA_CORTE');		
	//	alert('numIf ' + numIf + '\nicMoneda ' +icMoneda + '\nfechaDCorte ' + fechaDCorte + '\ntipoProceso '+tipoProceso);
		Ext.Ajax.request({
			url: '13forma6ext.data.jsp',
			params:{							
				informacion: 'descargarArchivo',
				ic_if: numIf,
				moneda: icMoneda,
				FechaCorte: fechaDCorte,
				tipo_proceso: tipoProceso
			},	
			callback: procesarDescargaArchivo
		});
	}
	
	var descargaArchivoDLS = function(grid, rowIndex, colIndex, item, event){
	//	var grid = Ext.getCmp('gridDetalles');	
	//	grid.el.mask('Procesando consulta', 'x-mask');
		var tipoProceso = Ext.getCmp('cmbProceso').getValue();		
		var registro = gridDetalles.getStore().getAt(rowIndex);
		var numIf = registro.get('IC_IF');
		var icMoneda = '54';
		var fechaDCorte = registro.get('FECHA_CORTE');	
	//	alert('numIf ' + numIf + '\nicMoneda ' +icMoneda + '\nfechaDCorte ' + fechaDCorte + '\ntipoProceso '+tipoProceso);
		Ext.Ajax.request({
			url: '13forma6ext.data.jsp',
			params:{							
				informacion: 'descargarArchivo',
				ic_if: numIf,
				moneda: icMoneda,
				FechaCorte: fechaDCorte,
				tipo_proceso: tipoProceso
			},	
			callback: procesarDescargaArchivo
		});
	}
	
	function procesarDescargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var boton = Ext.getCmp('btnBajarArchivo');
			boton.setIconClass('icoXls');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot + '/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}	
	}

//---------------------------------STORES------------------------------------
	
	//STORE PARA LLENAR EL COMBO DE TIPO DE PROCESO
	var storeComboProceso = new Ext.data.SimpleStore({
		fields: ['tipoProceso', 'nombreProceso'],
		data:[
			['E','Estados de cuenta'],
			['V','Vencimientos'],
			['O','Operados']
		]
	});
	
	//STORE PARA LLENAR EL GRID DE DETALLE
	var consultaDataDetalle = new Ext.data.JsonStore({
		root: 'registros',
		url: '13forma6ext.data.jsp',
		baseParams: {
			informacion: 'consultaProcesosDetalle'
		},
		fields: [
			{name: 'IC_FINANCIERA'},
			{name: 'IC_IF'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'TOTAL_MN'},
			{name: 'TOTAL_DLS'},
			{name: 'FECHA_CORTE'},
			{name: 'IC_MONEDA'},
			{name: 'ARCHIVO_ZIP'}		
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataDetalle,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaDataDetalle(null, null, null);
				}
			}
		}
	});
	
	//STORE PARA LLENAR EL GRID DE PROCESOS (gridPrincipal)
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '13forma6ext.data.jsp',
		baseParams: {
			informacion: 'consultaProcesos'
		},
		fields: [
			{name: 'ID_PROCEDIMIENTO'},
			{name: 'FECHA_EJECUCION'},
			{name: 'HORA_EJECUCION'},
			{name: 'FECHA_FIN_MES'},
			{name: 'FECHA_PROB_PAGO'},
			{name: 'FECHA_OPERACION'},
			{name: 'NUMREG_MN'},
			{name: 'NUMREG_DLS'},
			{name: 'NUM_INTERM'},
			{name: 'CG_ARCHIVO'}					
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaProcesosData,
			exception: {
				fn: function(proxy, type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response,args);
					procesarConsultaProcesosData(null, null, null);
				}
			}
		}
	});
	
	var consultaTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13forma6ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [	
		   {	name: 'TOTALES_MN' },
			{	name: 'TOTALES_DLS' }
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);					
				}
			}
		}		
	});
	
	

//------------------------------COMPONENTES----------------------------------
	
	//ELEMENTOS QUE SE PRESENTAN EN EL FORM
	var elementosForma = [
		{
			xtype: 'radiogroup',
			columns: 4,
			id: 'radioIF',
			vertical: false,
			items: [
				{ boxLabel: 'IF', name: 'rb', inputValue: 'B', checked: true },
				{ boxLabel: 'IFNB', name: 'rb', inputValue: 'NB' }
			],
			listeners:{
				change: cambioRadio
			} 
		},
		{
			xtype: 'datefield',
			name: 'fecha_actual',
			id: 'fecha_actual',
			allowBlank: false,
			hidden: true,
			editable: false,
			fieldLabel: 'Fecha de operaci�n',
			startDay: 0,
			anchor: '40%',
			msgTardet: 'side',
			minValue: '01/01/1901',
			value: new Date(),
			margins: '0 20 0 0'
		},
		{
			xtype: 'combo',
			name: 'ic_proceso',
			id: 'cmbProceso',
			allowBlank: false,
			fieldLabel : 'Tipo de Proceso',
			mode: 'local',
			displayField : 'nombreProceso',
			valueField: 'tipoProceso',
			editable: false,
			emptyText: 'Seleccionar Proceso',
			forceSeleccion : false,
			triggerAction: 'all',
			anchor: '95%',
			typeAhead: true,
			minValue: '01/01/1901',
			minChars : 1,
			store: storeComboProceso,
			listeners:{
				select: seleccionCombo
			}		
		},
		{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			anchor: '95%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Fecha de Ejecuci�n',
					name: '_fecha_ejec_de',
					id: 'fecha_ejec_de',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_ejec_a',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'datefield',
					name: '_fecha_ejec_a',
					id: 'fecha_ejec_a',
					allowBlank: true,
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_ejec_de',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'
				}
			]
		},
		//COMPOSITE PARA ESTADOS DE CUENTA		
		{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'compEstados',
			hidden: true,
			msgTarget: 'side',
			anchor: '100%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Fecha Fin de Mes',
					name: '_fecha_fin_mes_de',
					id: 'fecha_fin_mes_de',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_fin_mes_a',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'datefield',
					name: '_fecha_fin_mes_a',
					id: 'fecha_fin_mes_a',
					allowBlank: true,
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_fin_mes_de',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'
				}
			]
		},
		//COMPOSITE PARA VENCIMIENTOS		
		{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'compVencimientos',
			hidden: true,
			msgTarget: 'side',
			anchor: '100%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Fecha Probable de Pago',
					name: '_fecha_prob_de',
					id: 'fecha_prob_de',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_prob_a',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'datefield',
					name: '_fecha_prob_a',
					id: 'fecha_prob_a',
					allowBlank: true,
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_prob_de',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'
				}
			]
		},
		//COMPOSITE PARA OPERADOS	
		{
			xtype: 'compositefield',
			combineErrors: false,
			id: 'compOperados',
			hidden: true,
			msgTarget: 'side',
			anchor: '100%',
			items:[			
				{
					xtype: 'datefield',
					fieldLabel: 'Fecha de Operaci�n',
					name: '_fecha_oper_de',
					id: 'fecha_oper_de',
					allowBlank: true,
					vtype: 'rangofecha', 
					campoFinFecha: 'fecha_oper_a',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 15
				},
				{
					xtype: 'datefield',
					name: '_fecha_oper_a',
					id: 'fecha_oper_a',
					allowBlank: true,
					vtype: 'rangofecha',
					campoInicioFecha: 'fecha_oper_de',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					minValue: '01/01/1901',
					margins: '0 20 0 0'
				}
			]
		}
	];
	
	//FORM PANEL PARA AGREGAR LOS ELEMENTOS ANTERIORES
	var fp = new Ext.form.FormPanel({
		width:         440,
		labelWidth:    150,
		id:            'forma',
		style:         'margin:0 auto;',
		bodyStyle:     'padding: 6px',
		title:         'Criterios de B�squeda',
		collapsible:   true,
		titleCollapse: false,
		frame:         true,
		defaults:{
			msgTarget:  'side',
			anchor:     '-20'
		},
		items:         elementosForma,
		buttons: [{
			text:       'Consultar',
			id:         'btnConsultar',
			iconCls:    'icoBuscar',
			formBind:   true,
			handler:    function(boton, evento){
				gridPrincipal.hide();
				realizarConsulta();
			}
		},{
			text:       'Limpiar',
			id:         'btnLimpiar',
			iconCls:    'icoLimpiar',
			handler:    function() {
				Ext.getCmp('cmbProceso').reset();
				accionLimpiar();
			}
		}]
	});

	//SELECIONMODEL PARA AGREGAR LA COLUMNA CON CHECKBOX EN gridPrincipal
	var sm = new Ext.grid.CheckboxSelectionModel({
		header : '',
		checkOnly:true,
		hideable:true,
		singleSelect:false,
		id:'chkProc',
		align: 'center',
		width:30	  			
	});
	//GRID EN EL QUE SE CARGAN LOS PROCESOS ENCONTRADOS EN LA BUSQUEDA
	var gridPrincipal = new Ext.grid.GridPanel({
		id: 'gridPrincipal',
		store: consultaData,
		height: 350,
		hidden: true,
		sm: sm,
	//	rendered: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto',
		bodyStyle: 'padding: 6px',
		//title: 'Movimiento para el d�a',
		columns:[
			{
				header: 'Id de<br>Procedimiento',
				tooltip: 'Id de Procedimiento',
		//		width: 180,
				dataIndex: 'ID_PROCEDIMIENTO',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Fecha de<br>Ejecuci�n',
				tooltip: 'Fecha de Ejecuci�n',
		//		width: 180,
				dataIndex: 'FECHA_EJECUCION',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Hora de<br>Ejecuci�n',
				tooltip: 'Hora de Ejecuci�n',
		//		width: 180,
				dataIndex: 'HORA_EJECUCION',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Fecha Fin<br>de Mes',
				tooltip: 'Fecha Fin de Mes',
		//		width: 180,
				dataIndex: 'FECHA_FIN_MES',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Fecha Probable<br>de Pago',
				tooltip: 'Fecha Probable de Pago',
		//		width: 180,
				dataIndex: 'FECHA_PROB_PAGO',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Fecha de<br>Operaci�n',
				tooltip: 'Fecha de Operaci�n',
		//		width: 180,
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'N�m. de Registros<br>Moneda Nacional',
				tooltip: 'N�m. de Registros Moneda Nacional',
		//		width: 180,
				dataIndex: 'NUMREG_MN',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'N�m. de Registros<br>D�lares',
				tooltip: 'N�m. de Registros D�lares',
		//		width: 180,
				dataIndex: 'NUMREG_DLS',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'N�m. de<br>Intermediarios',
				tooltip: 'N�m. de Intermediarios',
		//		width: 180,
				dataIndex: 'NUM_INTERM',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalle',
				tooltip: 'Detalle',
				width: 50,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Detalle';
							return 'iconoLupa';										
						},
						handler: mostrarVentanaDetalles
					}
				]				
			},
			sm,
			{
				xtype: 'actioncolumn',
				header: 'Reprocesar',
				tooltip: 'Reprocesar',
				width: 80,
				id: 'actionCReprocesar',
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							var fechaActual = Ext.getCmp("fecha_actual").getRawValue();
							if(registro.data['FECHA_EJECUCION'] == fechaActual && registro.data['CG_ARCHIVO'] == ''){
								this.items[0].tooltip = 'Reprocesar';
								return 'icoRegresar';										
							}
						},
						handler: accionReprocesar
					}
				]				
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Cancelar Proceso',
					id: 'btnCancelar',
					iconCls: 'cancelar',
					hidden: false,
					handler: function(boton, evento){
						boton.disable();
						cancelarProceso(sm);
					}
				}
			]
		}
	});
	
	//GRID EN EL QUE SE CARGAN LOS DETALLES DE PROCESOS
	var gridDetalles = new Ext.grid.GridPanel({
		id: 'gridDetalles',
		store: consultaDataDetalle,
		height: 350,
		hidden: false,
		bodyStyle: 'padding: 6px',
		align: 'center',
		style: 'margin:0 auto',
		title: 'Dinamico',
		loadMask: true,
		columns:[
			{
				header: 'N�m. IF<br>SIRAC',
				tooltip: 'N�m. IF SIRAC',
		//		width: 180,
				dataIndex: 'IC_FINANCIERA',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'N�m. IF<br>N@E',
				tooltip: 'N�m. IF N@E',
		//		width: 180,
				dataIndex: 'IC_IF',
				sortable: true,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Nombre del Intermediario',
				tooltip: 'Nombre del Intermediario',
				width: 250,
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				resizable: true,
				align: 'left'
			},
			{
				xtype: 'actioncolumn',
				header: 'N�mero de<br>Registros MN',
				tooltip: 'N�mero de Registros MN',
			//	width: 50,
				dataIndex: 'TOTAL_MN',
				align: 'center',
				renderer:  function (value,metaData,registro,rowIndex,colIndex,store){
					return value + ' ';
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Detalle MN';
							return 'iconoLupa';										
						},
						handler: descargaArchivoMN
					}
				]				
			},
			{
				xtype: 'actioncolumn',
				header: 'N�mero de<br>Registros DLS',
				tooltip: 'N�mero de Registros DLS',
			//	width: 50,
				dataIndex: 'TOTAL_DLS',
				align: 'center',
				renderer:  function (value,metaData,registro,rowIndex,colIndex,store){
					return value + ' ';
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver Detalle DLS';
							return 'iconoLupa';										
						},
						handler: descargaArchivoDLS
					}
				]				
			},
			{
				header: 'Generaci�n de<br>Archivo ZIP',
				tooltip: 'Generaci�n de Archivo ZIP',
		//		width: 180,
				dataIndex: 'ARCHIVO_ZIP',
				sortable: true,
				resizable: true,
				align: 'center'
			}
		],
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					iconCls: 'icoXls',
					hidden: false,
					handler: function(boton, evento){
					//	boton.disable();
						var tipoProceso = Ext.getCmp('cmbProceso').getValue();
						var nombreProceso = Ext.getCmp('cmbProceso').getRawValue();
						var radioIF = Ext.getCmp('radioIF').getValue().getRawValue();
						boton.setIconClass('loading-indicator');
						var registro = gridPrincipal.getStore().getAt(0);
						var idProceso = registro.get('ID_PROCEDIMIENTO');
						var fecha = registro.get('FECHA_OPERACION');
						Ext.Ajax.request({
							url: '13forma6ext.data.jsp',
							params:{							
								informacion: 'ArchivoCSV',
								tipo_proceso: tipoProceso,
								tipo_if: radioIF,
								id_proceso: idProceso,
								fecha_detalle: fecha
							},	
							callback: procesarDescargaArchivo
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Regresar',
					id: 'btnRegresar',
					iconCls: 'cancelar',
					hidden: false,
					handler: function(boton, evento){
						Ext.getCmp('winDetalles').hide();
					}
				}
			]
		}
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '',	
		align: 'left',
		hidden: false,
		columns: [	
			{
				header: 'Total de Registros MN',
				tooltip: 'Total de Registros MN',
				dataIndex: 'TOTALES_MN',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},		
			{
				header: 'Total de Registros DLS',
				tooltip: 'Total de Registros DLS',
				dataIndex: 'TOTALES_DLS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			}
		],	
		stripeRows: true,
		loadMask: true,
		height: 80,
		frame: true	
	});
	
	var ventanaDetalles = new Ext.Window({
		id: 'winDetalles',
		width: 770,
		height: 470,
		modal: true,
		closeAction: 'hide',
		title: '.:: N@fin Electr�nico :: Pagos de IFNB ::.',
		items:[
			gridDetalles,
			gridTotales
		]
	});

//-------------------------COMPONENTE PRINCIPAL------------------------------
	
	//PANEL PRINCIPAL
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		items: [
			fp,
			NE.util.getEspaciador(10),
			gridPrincipal
		]
	});
});