Ext.onReady(function() {  
	var ic_pyme = 0 ;
/*--------------------------------- HANDLERS -------------------------------*/

	/****** Handler�s Grid�s *******/
	
	function verificaPyme() {
		ic_pyme = '0';
		Ext.getCmp("txt_nombre").reset();
		var NafElect = (Ext.getCmp("txt_nafelec")).getValue();
		var epo = (Ext.getCmp("id_cmb_epo").getValue());
		
		Ext.getCmp('suceptibleFloating').enable();
		Ext.getCmp('aplicaFloating2').setValue(false);
		Ext.getCmp('aplicaFloating1').setValue(false);
				
		if(NafElect != '' && ic_pyme == '0') {
			Ext.Ajax.request({
					url: '13consBiTasaPredeNegoExt.data.jsp',
					params: Ext.apply ({
						informacion: 'NombreProveedor',
						noNafin : NafElect
					}),
					callback: procesarSuccessFailureNombreProveedor
			});
		}
		else if(NafElect != '' && ic_pyme !='0' && epo != '')
		{
			Ext.Ajax.request({
					url: '13consBiTasaPredeNegoExt.data.jsp',
					params: Ext.apply ({
						informacion: 'VerificaNafElectronico',
						noEpo: epo,
						noNafin : NafElect
					}),
					callback: procesarSuccessFailureVerificaNafElectronico
			});
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		pnl.el.unmask();
		var fp = Ext.getCmp('forma');
		
		if (arrRegistros != null) 
		{
			if (!grid.isVisible()) {
				grid.show();
			}
			
			var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivo");
			var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivo');
			var el = grid.getGridEl();
			btnBajarArchivo.hide();
			
			if (store.getTotalCount() > 0 ) {	// si la consulta tiene un store
				btnGenerarArchivo.enable();	// habilito los botones
				el.unmask();						//quito la mascara
			}
			if (arrRegistros == '')  {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				btnGenerarArchivo.disable();
			}
			else if (arrRegistros != '') {
				btnGenerarArchivo.enable();
			}
		}	
	}
	

  	 var procesarSuccessFailureGenerarCSV = function(opts, success, response) 
    {
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		btnBajarArchivo.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
 
 
		/**** End Handler�s Grid�s ****/
		
	var procesarCatalogoEpo = function(store, arrRegistros, opts) {
		if (!grid.isVisible()) 	
			grid.hide();
	}
	
	var procesarCatalogoMoneda = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 0) { 
			Ext.getCmp('id_cmb_moneda').setValue('1');
			if (!grid.isVisible()) 	
				grid.hide();
		}
	}
	
	var procesarSuccessFailureNombreProveedor =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var reg = Ext.util.JSON.decode(response.responseText).registros[0];
			if ( reg != null) {
				var txt_nombre = reg.txt_nombre ;
				Ext.getCmp('txt_nombre').setValue(txt_nombre);
				ic_pyme = reg.ic_pyme;				
			} 
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	} 
	var procesarSuccessFailureVerificaNafElectronico =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var reg = Ext.util.JSON.decode(response.responseText).registros[0];
			if (reg != null) {
				var txt_nombre = reg.txt_nombre; //cambiar aki!!!
				Ext.getCmp('txt_nombre').setValue(txt_nombre);
				ic_pyme = reg.ic_pyme; //cambiar aki!!!
			} 
			else {
				Ext.Msg.alert('Mensaje de P�gina Web','El nafin electronico no corresponde a una\nPyME afiliada a la EPO o no existe.');
				ic_pyme = '0';
				Ext.getCmp('txt_nombre').setValue('');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	} 
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success)
		{
			if (jsonData.msgRegistros!='')
			{
				Ext.Msg.alert('Buscar',jsonData.msgRegistros);
				Ext.getCmp('fBusqAvanzada').getForm().reset();
				return;
			}
			else
			{
				//si trajo datos entonces que muestre el combo
				Ext.getCmp('id_nombreAvanz').show();
				Ext.getCmp('btnAceptar').show();
				Ext.getCmp('btnCancelar').show();
			}

			Ext.getCmp('id_nombreAvanz').focus();
		}
	}
//-----------------------------------------------------------------------------//

/*--------------------------------- STORE'S -------------------------------*/
	var catalogoTasa = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['P','PREFERENCIAL'],
			['N','NEGOCIADA']
		 ]
	}) ;
	
	var catalogoEpo = new Ext.data.JsonStore({
	   id				: 'catEpo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13consBiTasaPredeNegoExt.data.jsp',
		baseParams	: { informacion: 'CatalogoEPO'},
		totalProperty : 'total',
		autoLoad		: true,
		listeners	:
		{
			load:procesarCatalogoEpo,
			exception: NE.util.mostrarDataProxyError 
		}
	});		
	
	var catalogoIF = new Ext.data.JsonStore({
	   id				: 'catIf',
		root 			: 'registros',
		fields 		: ['lsClave', 'lsNombre', 'loadMsg'],
		url 			: '13consBiTasaPredeNegoExt.data.jsp',
		baseParams	: { informacion: 'CatalogoIF'},
		autoLoad		: false,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError
		}
	});	
	
	var catalogoMoneda = new Ext.data.JsonStore({
	   id				: 'catMoneda',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13consBiTasaPredeNegoExt.data.jsp',
		baseParams	: { informacion: 'CatalogoMoneda'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoMoneda,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var catalogoEstatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['A','Alta'],
			['E','Eliminar']
		 ]
	}) ;
	
	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['IC_NAFIN_ELECTRONICO','IC_PYME', 'DESPLIEGA','CG_RAZON_SOCIAL', 'loadMsg' ],
		url : '13consBiTasaPredeNegoExt.data.jsp',
		baseParams: { informacion: 'CatalogoBuscaAvanzada'	},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//-----------------------------------------------------------------------------//	
	
	
		


	
	/****** Store�s Grid�s *******/	
	var consultaData = new Ext.data.JsonStore
	({
		root:'registros',	
		url:'13consBiTasaPredeNegoExt.data.jsp',	
		baseParams:{informacion:'ConsultaGrid'},	
		
		totalProperty:'total',	
		messageProperty:'msg',	
		autoLoad:false,
		fields: [{name: 'NOMBREEPO'},
					{name: 'NOMBREPROVEEDOR' },	
					{name: 'MONEDA' },	
					{name: 'TIPOTASA'  },		
					{name: 'ESTATUS' },
					{name: 'PUNTOS'},
					{name: 'VALOR'},
					{name: 'FECHAREGISTRO'},
					{name: 'USUARIO'}],
	
		listeners: 
		{
			load: procesarConsultaData,
			exception: NE.util.mostrarDataProxyError
		}
		
	});

	/****** End Store�s Grid�s *******/
	
	
	/*********** Grid�s *************/

	var grid = new Ext.grid.GridPanel
	({
		title:'',
		store: consultaData,
		margins: '20 0 0 0',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		style: ' margin:0 auto;',
		stripeRows: true,
		loadMask:true,	
		height:450,
		width:900,
		frame:true, 
		hidden:true, 
		header:true,
		columns: [
			{
				header:'EPO',
				tooltip:'EPO',			
				dataIndex:'NOMBREEPO',	
				sortable:true,	
				resizable:true,	
				width:200,
				/*???*/renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},
			{
				header:'Proveedor',
				tooltip: 'Proveedor',
				dataIndex: 'NOMBREPROVEEDOR',	
				sortable:true,
				resizable:true,	
				width:150, 
				align:'left'
				
			},
			{
				header:'Moneda', 
				tooltip: 'Moneda',	
				dataIndex:'MONEDA',	
				sortable:true, 
				width:120, 	
				align:'center'
				
			},
			{
				header:'Tipo Tasa',
				tooltip:'Tipo Tasa',
				dataIndex: 'TIPOTASA',		
				sortable:true, 
				width:120, 
				align:'center'
				
			},
			{
				header:'Estatus',
				tooltip:'Estatus',	
				dataIndex:'ESTATUS',	
				sortable:true,	
				width:120,	
				align:'center'
				
			},
			{
				header:'Puntos', 		
				tooltip:'Puntos',			
				dataIndex:'PUNTOS',		
				sortable:true,	
				width:60,	
				align:'center'
				
			},
			{
				header:'Valor',
				tooltip:'Valor',	
				dataIndex: 'VALOR',		
				sortable:true,	
				width:60,	
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')	
			},
			{
				header:'Fecha Registro', 		
				tooltip:'Fecha Registro',			
				dataIndex:'FECHAREGISTRO',		
				sortable:true,	
				width:120,	
				align:'center'
				
			},
			{
				header:'Usuario',
				tooltip:'Usuario',	
				dataIndex: 'USUARIO',		
				sortable:true,	
				width:120,	
				align:'center'
				
			}
		],
		
		bbar: {
			xtype: 'paging',
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consultaData,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No se encontro ning�n registro",
					
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) 
					{
						boton.disable();
						boton.setIconClass('loading-indicator');
						totalProperty:'total',
						
						Ext.Ajax.request
						({
							url: '13consBiTasaPredeNegoExt.data.jsp',	
							params: Ext.apply (fp.getForm().getValues(),{								
								informacion: 'GenerarCSV',
								ic_pyme: ic_pyme 
								})
								,callback: procesarSuccessFailureGenerarCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				}
				
			]
		}
	});

		/****  End Grid�s ****/
	
	
   var elementosForma = [
	{
		xtype: 'panel',
		labelWidth: 126,
		layout: 'form',
		items: [ 
			{
					xtype				: 'combo',
					id					: 'id_cmb_tasa',
					name				: 'cmb_tasa',
					hiddenName 		: 'cmb_tasa',
					fieldLabel		: 'Tipo de Tasa',
					width				: 150,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccionar',
					store				: catalogoTasa,
					listeners: {
						select: function(combo) {
							grid.hide();
							Ext.getCmp("suceptibleFloating").hide();
							Ext.getCmp('aplicaFloating2').setValue(false);
							Ext.getCmp('aplicaFloating1').setValue(false);
							if(combo.value==='P'){
								Ext.getCmp("suceptibleFloating").show();								
							}
					
						}
					}
			},
			{
					xtype				: 'combo',
					id					: 'id_cmb_epo',
					name				: 'cmb_epo',
					hiddenName 		: 'cmb_epo',
					fieldLabel		: 'EPO Relacionada',
					width				: 450,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccionar',
					store				: catalogoEpo,
					listeners: {
						select: function(combo) {
							grid.hide();
							Ext.getCmp('id_nombre_if').reset();
							catalogoIF.load({
								params : Ext.apply({	ic_epo: combo.getValue(), moneda: (Ext.getCmp('id_cmb_moneda')).getValue() })
							});
						}
					}
			},
			{
					xtype: 'combo',
					id: 'id_nombre_if',
					name: 'nombre_if',
					hiddenName: 'nombre_if',
					fieldLabel: 'Nombre del IF',
					width:280,
					emptyText: 'Seleccionar ',  
					forceSelection: true,
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					mode: 'local',
					displayField: 'lsNombre',
					valueField: 'lsClave',
					store: catalogoIF
			},
			{
						xtype: 'combo',
						id: 'id_cmb_moneda',
						name: 'cmb_moneda',
						hiddenName: 'cmb_moneda',
						fieldLabel: 'Moneda',
						width:150,
						emptyText: 'Seleccionar ',  
						forceSelection: true,
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						store: catalogoMoneda
			},
			{	
				xtype: 'panel',
				layout: 'hbox',
				anchor:'100%',
				items: [
				{
					xtype:'panel', 
					layout:'form', 
					msgTarget:'side',
					bodyStyle: 'padding: 2px',
					items:[
					{
							xtype        	: 'textfield',
							id					: 'txt_nafelec',
							name    			: 'txt_nafelec',
							fieldLabel     : 'Proveedor',
							allowBlank		: true,
							width				: 90,
							maxLength 		: 12,
							msgTarget		: 'side',
							margins			: '0 20 0 0',  //necesario para mostrar el icono de error
							listeners: 
							{
									blur: {
										fn: function() {
											verificaPyme();											
										}
									}
							}
					}]
				},
				{
					xtype:'panel',
					//layout: 'form',
					msgTarget:'side',
					bodyStyle: 'padding: 2px',
					items: 
					[
						{
							xtype: 'textfield',
							id: 'txt_nombre',
							name: 'txt_nombre',
							fieldLabel:'',
							disabled :true,		
							allosBlank: true,
							width: 350,
							msgTarget:'side',
							margins: ' 0 20 0 0'
						}
					]
				}]
			},
			{
					xtype: 'panel',
					msgTarget:'side',
					bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:131px;',
					items:
					[
						{
							xtype: 'button',
							text: 'B�squeda Avanzada...',
							id: 'btnBusqAv',
							handler: function(boton, evento) 
							{
							
								var ventana = Ext.getCmp('winBusqAvan');
								if (ventana) {
									ventana.show();
								} 
								else 
								{
									new Ext.Window({
											title: 			'B�squeda Avanzada',
											layout: 			'fit',
											width: 			400,
											height: 			400,
											minWidth: 		400,
											minHeight: 		400,
											modal: 			true,
											buttonAlign: 	'center',
											id: 				'winBusqAvan',
											closeAction: 	'hide',
											items: 	fpBusqAvanzada
										}).show();
								}
							}
						}
					]
			},
			{
						xtype: 'combo',
						id: 'id_cmb_estatus',
						name: 'cmb_estatus',
						hiddenName: 'cmb_estatus',
						fieldLabel: 'Estatus',
						width:120,
						emptyText: 'Seleccionar ',  
						forceSelection: true,
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						store: catalogoEstatus
			},
			{
					xtype: 'compositefield',
					fieldLabel: 'Fecha de Registro',
					combineErrors: false,
					msgTarget: 'side',
					items: [
						{
							xtype: 'datefield',
							name: 'txtFchReg1',
							id: 'txtFchReg1',
							allowBlank: true,
							startDay: 0,
							width: 130,
							msgTarget: 'side',
							vtype: 'rangofecha', 
							campoFinFecha: 'txtFchReg2',
							margins: '0 20 0 0'  //necesario para mostrar el icono de error
						},
						{
							xtype: 'displayfield',
							value: 'a',
							width: 24
						},
						{
								xtype: 'datefield',
								name: 'txtFchReg2',
								id: 'txtFchReg2',
								
								allowBlank: true,
								startDay: 1,
								width: 130,
								msgTarget: 'side',
								vtype: 'rangofecha', 
								campoInicioFecha: 'txtFchReg1',
								margins: '0 20 0 0'  //necesario para mostrar el icono de error
						}
					]
			},
			{  
			xtype:  'radiogroup',   
			fieldLabel: "Aplica Floating",    
			name: 'suceptibleFloating',   
			id: 'suceptibleFloating',  			
			align: 'center',
			hidden: true,
			width: 100,							
				items: [						
					{ 
						boxLabel:    "Si",             
						name:        'aplicaFloating', 
						id: 'aplicaFloating1',
						inputValue:  "S" ,
						value: 'S',
						checked: false,
						width: 10
					}, 				
					{           
						boxLabel: "No",             
						name: 'aplicaFloating', 
						id: 'aplicaFloating2',
						inputValue:  "N", 
						value: 'N',							
						width: 10						
					} 				
				]
			}
	  ]
						
	}];
						
	 
	 
/*******************Componentes*******************************/
// -------------------------- Pop up -------------------------- //
var elementsFormBusq = [
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: 
			{ 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 'displayfield',
			value: '  '		
		},
		
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		
		
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 8px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			
			  buttons: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	85,
						handler: function(boton, evento) 
						{
							//aqui los escondo
							Ext.getCmp('btnAceptar').hide();
							
							catalogoNombreData.load({	
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{	})																	
								});
						}
					},	
					//boton de cancelar
					{
						xtype: 'button',
						text: 'Cancelar',
						iconCls: 'icoLimpiar',
						handler: function()
						{					
							var ventana = Ext.getCmp('winBusqAvan');
							fpBusqAvanzada.getForm().reset();
							ventana.hide();
						}				
					}
			  ]
		},
		
		{
			xtype: 'displayfield',
			value: '  '		
		},
		{
			xtype: 'displayfield',
			value: '  '		
		},
		
		{		
				xtype: 'combo',
				name: 'nombreAvanz',
				id: 'id_nombreAvanz',
				hiddenName: 'nombreAvanz',
				fieldLabel: 'Nombre',
				hidden: false,
				forceSelection: true,
				triggerAction: 'all',
				typeAhead: true,
				emptyText : 'Seleccione proveedor',
				minChars: 1,
				mode: 'local',
				displayField: 'DESPLIEGA',
				valueField: 'IC_PYME',
				store: catalogoNombreData
				
		},	
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',			 
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			
			  buttons: [
					{
						xtype: 'button',
						text: 'Aceptar',
						id: 'btnAceptar',
						hidden: true,
						width: 	85,
						handler: function(boton, evento) {
							//pregunto si viene informacion
							var combo  = Ext.getCmp('id_nombreAvanz');
							var indice = catalogoNombreData.findExact('IC_PYME',combo.getValue());
							
							//trae un registro del store
							var record = catalogoNombreData.getAt(indice);
							var numero = Ext.getCmp('txt_nafelec');
							var nombre = Ext.getCmp('txt_nombre');
							numero.setValue(record.data['IC_NAFIN_ELECTRONICO']);
							nombre.setValue(record.data['CG_RAZON_SOCIAL']);
							//ic_pyme = record.data['IC_NAFIN_ELECTRONICO'];//*
							verificaPyme();
							var ventana = Ext.getCmp('winBusqAvan');
							fpBusqAvanzada.getForm().reset();
							ventana.hide();						
							
						}
					},
					{
						xtype: 'button',
						text: 'Cancelar',
						id: 'btnCancelar',
						hidden: false,
						handler: function()
						{					
							var ventana = Ext.getCmp('winBusqAvan');
							fpBusqAvanzada.getForm().reset();
							ventana.hide();
						}				
					}
			  ]
		}
	];
	
	
		var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true
	});

// -------------------------- FIN Pop up -------------------------- //	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		frame:true,
		style: ' margin:0 auto;',
		title: '',
		collapsible: true,		///////////
		titleCollapse: true,		//////////
		bodyStyle: 'padding: 6px',
		labelWidth: 120,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items:	elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					
					var fechaOper1 = Ext.getCmp('txtFchReg1');
					var fechaOper2 = Ext.getCmp('txtFchReg2');
	
					if (!Ext.isEmpty(fechaOper1.getValue()) || !Ext.isEmpty(fechaOper2.getValue()) ) {
						if(Ext.isEmpty(fechaOper1.getValue()))	{
							fechaOper1.markInvalid('Debe capturar ambas fechas de registro o dejarlas en blanco');
							fechaOper1.focus();
							return;
						}else if (Ext.isEmpty(fechaOper2.getValue())){
							fechaOper2.markInvalid('Debe capturar ambas fechas de registro o dejarlas en blanco');
							fechaOper2.focus();
							return;
						}
					}
					pnl.el.mask('Enviando...', 'x-mask-loading');
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{	
						operacion: 'Generar',
						start : 0,
						limit: 15,
						ic_pyme : ic_pyme
						})
					});						
					
				} //fin handler
			}, //fin boton Consultar
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consBiTasaPredeNegoExt.jsp';
				}
			}
		]							
	}); 
	
	
	 var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fp, 
			NE.util.getEspaciador(20),
			grid  			
		]
	});
	
	
	
	catalogoEpo.load();
	catalogoMoneda.load();
	 
	

});//Fin de funcion principal
