Ext.onReady(function(){
	Ext.QuickTips.init();
	var flag = null;
	function procesaValoresIniciales(opts, success, response) {
		fp.el.mask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
				if(jsonValoresIniciales != null){
					if(jsonValoresIniciales.strTipoUsuario=='NAFIN'){					
						Ext.getCmp('fpBotones').show();
					}
				
					if(jsonValoresIniciales.mensaje!=""){
						Ext.Msg.alert('Mensaje informativo',jsonValoresIniciales.mensaje,function(btn){
						});
					}else{
						fp.el.unmask();
					}
				}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
	
	
//----------------------------------HANDLERS------------------------------------

	function mostrarArchivoPlano(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var generarArchivoPlano = function (cargaSiaff, flag){
		Ext.Ajax.request({
			url: '13consulta19ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ArchivoPlano',
						cargaSiaff: cargaSiaff,
						flag: flag
			}),
			callback: mostrarArchivoPlano
		});	
	}
	
	var generarArchivoPlanoRegistrosCargados = function (grid,rowIndex,colIndex,item){
		var registro = grid.getStore().getAt(rowIndex);
		var cargaSiaff = registro.get('ICSIAFF');
		flag = 'T';
		generarArchivoPlano(cargaSiaff,flag);
	}
	var generarArchivoPlanoRegistrosEncontrados = function (grid,rowIndex,colIndex,item){
		var registro = grid.getStore().getAt(rowIndex);
		var cargaSiaff = registro.get('ICSIAFF');
		flag = 'S';
		generarArchivoPlano(cargaSiaff,flag);
	}
	var generarArchivoPlanoRegistrosNoEncontrados = function (grid,rowIndex,colIndex,item){
		var registro = grid.getStore().getAt(rowIndex);
		var cargaSiaff = registro.get('ICSIAFF');
		flag = 'N';
		generarArchivoPlano(cargaSiaff,flag);
	}
	
	var generarArchivoPlanoRegistrosConError = function (grid,rowIndex,colIndex,item){
		var registro = grid.getStore().getAt(rowIndex);
		var cargaSiaff = registro.get('ICSIAFF');
		flag = 'E';
		generarArchivoPlano(cargaSiaff,flag);
	}
	
	function transmiteEliminarCarga(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);		
			consultaData.load({
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'Consulta'				
				})
			});
			
		} else {
				NE.util.mostrarConnError(response,opts);							
		}
	}
	
	var procesarEliminarCarga = function (grid,rowIndex,colIndex,item){
		var registro = grid.getStore().getAt(rowIndex);
		var cargaSiaff = registro.get('ICSIAFF');		
		
		Ext.Msg.show({
			title:	"Confirmaci�n",
			msg:		'�Esta seguro de eliminar esta carga?',
			buttons:	Ext.Msg.OKCANCEL,
			fn: function resultMsj(btn){
				if(btn =='ok'){	
					Ext.Ajax.request({
						url: '13consulta19ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'EliminarCarga',
							cargaSiaff:cargaSiaff	
						}),
						callback: transmiteEliminarCarga
					});
				}
			}
		});
	}
	

	var procesarConsultaData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(!grid.isVisible()){
				grid.show();
			}
			var jsonData = store.reader.jsonData;	
			var cm = grid.getColumnModel();
			if(jsonData.strTipoUsuario=='NAFIN') {
				grid.getColumnModel().setHidden(cm.findColumnIndex('REGISTRO_ERRORES'), false);
				grid.getColumnModel().setHidden(cm.findColumnIndex('ARCHIVO'), false);
			}
			
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	} 
//-----------------------------------STORES-------------------------------------
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '13consulta19ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'TIPO_PROCESO'},
			{name: 'FECHA_CARGA'},
			{name: 'NOMBRE_ARCHIVO'},
			{name: 'NOMBRE_USUARIO'},
			{name: 'REGISTROS_CARGADOS', type: 'int'},
			{name: 'REGISTROS_ENCONTRADOS', type: 'int'},
			{name: 'REGISTROS_NO_ENCONTRADOS', type: 'int'},
			{name: 'REGISTRO_ERRORES', type: 'int'},
			{name: 'ARCHIVO'}	,
			{name: 'TIPO_CARGA'},
			{name: 'ICSIAFF'}				
			
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
						fn: function(proxy,type,action,optionsRequest,response,args){
								NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
								procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
						}			
			}
		}
	});
//---------------------------------COMPONENTES----------------------------------
	var grid = new Ext.grid.GridPanel({
		id: 'grid',
		store: consultaData,
		style: 'margin:0 auto;',
		hidden: true,
		frame: true,
		columns: [
						{
							header: 'Tipo de Proceso',
							tooltip: 'Tipo de Proceso',
							sortable: true,
							dataIndex: 'TIPO_PROCESO',
							align: 'center',
							width: 150
						},
						{
							header: 'Fecha y Hora de Carga',
							tooltip: 'Fecha y Hora de Carga',
							sortable: true,
							dataIndex: 'FECHA_CARGA',
							align: 'center',
							width: 150
						},
						{
							header: 'Nombre del Archivo SIAFF',
							tooltip: 'Nombre del Archivo SIAFF',
							sortable: true,
							dataIndex: 'NOMBRE_ARCHIVO',
							align: 'left',
							width: 150
						},
						{
							header: 'Usuario Responsable <br> de Carga',
							tooltip: 'Usuario Responsable <br> de Carga',
							sortable: true,
							dataIndex: 'NOMBRE_USUARIO',
							align: 'left',
							width: 250
						},						
						{
							xtype: 'actioncolumn',
							header: 'Registros Cargados',
							tooltip: 'Registros Cargados',
							sortable: true,
							dataIndex: 'REGISTROS_CARGADOS',
							align: 'center',
							width: 150,
							renderer: function(value,metaData,registro,rowIndex,colIndex,store){
								return value;
							},
							items: [
								{
									getClass: function(valor,metadata,registro,rowIndex,colIndex,store){
										if(registro.get('REGISTROS_CARGADOS')!=0){										
											this.items[0].tooltip = 'Ver';
											return 'iconoLupa';
										}
									},
									handler: generarArchivoPlanoRegistrosCargados
										}	
							]
						},					
						{
							xtype: 'actioncolumn',
							header: 'Registros Encontrados <br> en Cadenas',
							tooltip: 'Registros Encontrados en Cadenas',
							sortable: true,
							dataIndex: 'REGISTROS_ENCONTRADOS',
							align: 'center',
							width: 150,
							renderer: function(value,metaData,registro,rowIndex,colIndex,store){
								if(registro.get('REGISTROS_ENCONTRADOS')!=0){	
									return value;
								}else {
									return '0';
								}
							},							
							items: [
								{
									getClass: function(value,metadata,registro,rowIndex,colIndex,store){										
										if(registro.get('REGISTROS_ENCONTRADOS')!=0){											
											this.items[0].tooltip = 'Ver';
											return 'iconoLupa';
										}
									},
									handler: generarArchivoPlanoRegistrosEncontrados
								}										
							]					
						},
						{
							xtype: 'actioncolumn',
							header: 'Registros No Encontrados <br> en Cadenas',
							tooltip: 'Registros No Encontrados en Cadenas',
							sortable: true,
							dataIndex: 'REGISTROS_NO_ENCONTRADOS',
							align: 'center',
							width: 150,
							renderer: function(value,metaData,registro,rowIndex,colIndex,store){
								if(registro.get('REGISTROS_NO_ENCONTRADOS')!=0){	
									return value;
								}else {
									return '0';
								}
							},
							items: [
								{
									getClass: function(value,metadata,registro,rowIndex,colIndex,store){
									if(registro.get('REGISTROS_NO_ENCONTRADOS')!=0){
											this.items[0].tooltip = 'Ver';
											return 'iconoLupa';
										}
									},
									handler: generarArchivoPlanoRegistrosNoEncontrados
								}										
							]
						},
						{
							xtype: 'actioncolumn',
							header: 'Registros con Error',
							tooltip: 'Registros con Error',
							sortable: true,
							dataIndex: 'REGISTRO_ERRORES',
							hidden: true,
							align: 'center',
							width: 150,
							renderer: function(value,metaData,registro,rowIndex,colIndex,store){
								if(registro.get('REGISTRO_ERRORES')==0){								
									return  'NA';
								}else {
									return value;
								}
							},
							items: [
								{
									getClass: function(value,metadata,registro,rowIndex,colIndex,store){
									if(registro.get('REGISTRO_ERRORES')!=0){
											this.items[0].tooltip = 'Ver';
											return 'iconoLupa';
										}
									},
									handler: generarArchivoPlanoRegistrosConError
								}										
							]
						},
						{
							xtype: 'actioncolumn',
							header: 'Archivo',
							tooltip: 'Archivo',
							sortable: true,
							dataIndex: 'ARCHIVO',
							align: 'center',
							hidden: true,
							width: 150,
							renderer: function(value,metaData,registro,rowIndex,colIndex,store){
								if(registro.get('TIPO_CARGA')!='A'){								
									return value;
								}
							},
							items: [
								{
									getClass: function(value,metadata,registro,rowIndex,colIndex,store){
									if(registro.get('TIPO_CARGA')=='A'){
											this.items[0].tooltip = 'Eliminar Carga';
											return 'borrar';
										}
									}
									,handler: procesarEliminarCarga
								}										
							]
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,
		title: ''
	});
	
	var elementosForma = [
									{
										xtype: 'compositefield',
										fieldLabel: 'Fecha de carga',
										combineErrors: false,
										msgTarget: 'side',
										anchor:'90%',
										items: [
													{
														xtype: 'datefield',
														name: 'df_fecha_carga_de',
														id: 'dc_fecha_cargaMin',
														allowBlank: true,
														startDay: 0,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoFinFecho: 'dc_fecha_cargaMax',
														margins: '0 20 0 0' //necesario para mostrar el icono de error
													},
													{
														xtype: 'displayfield',
														value: 'al',
														width: 24
													},
													{
														xtype: 'datefield',
														name: 'df_fecha_carga_a',
														id: 'dc_fecha_cargaMax',
														allowBlank: true,
														startDay: 1,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoInicioFecha: 'dc_fecha_cargaMin',
														margins: '0 20 0 0' //necesario para mostrar el icono de error
													}
											]
									}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 450,
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
						{
							text: 'Consultar',
							id: 'btnConsultar',
							iconCls: 'icoBuscar',
							formBind: true,
							handler: function (boton,evento){
								var dc_fecha_cargaMin = Ext.getCmp('dc_fecha_cargaMin');
								var dc_fecha_cargaMax = Ext.getCmp('dc_fecha_cargaMax');
								
								if(Ext.isEmpty(dc_fecha_cargaMin.getValue())||Ext.isEmpty(dc_fecha_cargaMax.getValue())){
									if(Ext.isEmpty(dc_fecha_cargaMin.getValue())){
										dc_fecha_cargaMin.markInvalid('El campo es obligatorio');
										dc_fecha_cargaMin.focus();
										fp.el.unmask();
										return;
									}
									if(Ext.isEmpty(dc_fecha_cargaMax.getValue())){
										dc_fecha_cargaMax.markInvalid('El campo es obligatorio');
										dc_fecha_cargaMax.focus();
										fp.el.unmask();
										return;
									}
								}
																
								var dc_fecha_cargaMin_ = Ext.util.Format.date(dc_fecha_cargaMin.getValue(),'d/m/Y');
								var dc_fecha_cargaMax_ = Ext.util.Format.date(dc_fecha_cargaMax.getValue(),'d/m/Y');
								
								if(!Ext.isEmpty(dc_fecha_cargaMin.getValue())){
									if(!isdate(dc_fecha_cargaMin_)) { 
										dc_fecha_cargaMin.markInvalid('La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa');
										dc_fecha_cargaMin.focus();
										return;
									}
								}
								if( !Ext.isEmpty(dc_fecha_cargaMax.getValue())){
									if(!isdate(dc_fecha_cargaMax_)) { 
										dc_fecha_cargaMax.markInvalid('La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa');
										dc_fecha_cargaMax.focus();
										return;
									}
								}
								
								fp.el.mask('Enviando...','x-msk-loading');							
								consultaData.load({
									params: Ext.apply(fp.getForm().getValues(),{
									})
								});
							}
						},
						{
							text: 'Limpiar',
							iconCls: 'icoLimpiar',
							handler: function(){
								var grid = Ext.getCmp('grid');
								grid.hide();
								fp.getForm().reset();
							}
						}
		]
	});	
	
	
	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',
		hidden: true,
      frame:     	false,
      border:    	false,
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: '<h1><b>Consulta al Reporte SIAFF</h1></b>',			
				id: 'btnCons1',					
				handler: function() {
					window.location = '13consulta19ext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Captura del Reporte SIAFF',			
				id: 'btnCap',					
				handler: function() {
					window.location = '13forma19ext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Reportes para TOICs',			
				id: 'btnCons2',					
				handler: function() {
					window.location = '13consulta19aext.jsp';
				}
			}
		]
	};
//---------------------------------PRINCIPAL------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
					NE.util.getEspaciador(20),
					fpBotones,
					NE.util.getEspaciador(20),					
					fp,
					NE.util.getEspaciador(20),
					grid
		]
	});
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '13consulta19ext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
});