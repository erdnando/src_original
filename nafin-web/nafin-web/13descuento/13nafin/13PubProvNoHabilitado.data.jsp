<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		com.netro.afiliacion.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoEPONafin,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";

if (informacion.equals("catalogoBancoFondeo")){

	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("comcat_banco_fondeo");
	cat.setCampoClave("ic_banco_fondeo");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setOrden("2");

	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CatalogoEpo")){

	String ic_banco_fondeo	=	(request.getParameter("ic_banco_fondeo")==null)?"":request.getParameter("ic_banco_fondeo");

	CatalogoEPONafin cat = new CatalogoEPONafin();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setProducto("1");
	cat.setModalidad("1");
	if(!"".equals(ic_banco_fondeo)){
		cat.setBancoFondeo(ic_banco_fondeo);
	}
	cat.setOrden("2");

	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CatalogoBuscaAvanzada")){

	String num_pyme	= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
	String rfc_pyme	= (request.getParameter("rfc_pyme")==null)?"":request.getParameter("rfc_pyme");
	String nombre_pyme= (request.getParameter("nombre_pyme")==null)?"":request.getParameter("nombre_pyme");
	String ic_epo		= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String ic_pyme		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);

	Registros registros = afiliacion.getProveedoresDist(ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme);

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(registros.getString("IC_PYME"));
		elementoCatalogo.setDescripcion(registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("NOMBRE_PROV"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("obtenNombrePyme")){

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	JSONObject jsonObj			= new JSONObject();
	String numeroDeProveedor	= (request.getParameter("numeroDeProveedor")!=null)?request.getParameter("numeroDeProveedor"):"";
	String ic_banco_fondeo		= (request.getParameter("ic_banco_fondeo")==null)?"":request.getParameter("ic_banco_fondeo");
	String ic_epo					= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	ic_banco_fondeo				= (ic_banco_fondeo.equals(""))?"0":ic_banco_fondeo;
	Registros registros			= new Registros();

	String ic_pyme					= "";
	String txtCadenasPymes		= "";

	if(ic_epo.equals("")){

		registros					= BeanParamDscto.getParametrosPymeNafin(numeroDeProveedor);

		if(registros != null && registros.next()){
			ic_pyme					= (registros.getString("PYME")	== null)?"":registros.getString("PYME");
			txtCadenasPymes		= (registros.getString("NOMBRE")	== null)?"":registros.getString("NOMBRE");
		}

	}else{

		registros					= BeanParamDscto.getParametrosPymeNafin(numeroDeProveedor,	ic_banco_fondeo,	ic_epo);

		if(registros != null && registros.next()){
			ic_pyme					= (registros.getString("PYME")	== null)?"":registros.getString("PYME");
			txtCadenasPymes		= (registros.getString("NOMBRE")	== null)?"":registros.getString("NOMBRE");
		}else{
			jsonObj.put("muestraMensaje", new Boolean(true));
			jsonObj.put("textoMensaje", 	"El nafin electrónico no corresponde a una<br>PyME afiliada a la EPO o no existe.");
		}
	}

	jsonObj.put("ic_pyme",					ic_pyme									);
	jsonObj.put("txt_nafelec",				numeroDeProveedor						);
	jsonObj.put("txtCadenasPymes",		txtCadenasPymes						);
	jsonObj.put("estadoSiguiente",		"RESPUESTA_OBTENER_NOMBRE_PYME"	);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Consultar") || informacion.equals("ResumenTotales") || informacion.equals("ArchivoXPDF") || informacion.equals("ArchivoCSV")){

	int start = 0;
	int limit = 0;

	String operacion               = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	String ic_banco_fondeo         = (request.getParameter("ic_banco_fondeo")==null)?"0":request.getParameter("ic_banco_fondeo");
	String ic_epo                  = (request.getParameter("_ic_epo") == null) ? "0" :request.getParameter("_ic_epo");
	String ic_pyme                 = (request.getParameter("hid_ic_pyme") == null) ? "0": request.getParameter("hid_ic_pyme");
	String df_fecha_publicacion_de = (request.getParameter("df_fecha_publicacion_de") == null) ? "": request.getParameter("df_fecha_publicacion_de");
	String df_fecha_publicacion_a  = (request.getParameter("df_fecha_publicacion_a") == null) ? "" : request.getParameter("df_fecha_publicacion_a");
	ic_banco_fondeo                = (ic_banco_fondeo.equals(""))?"0":ic_banco_fondeo;

	DesPublicProvedorNoHabilitado paginador = new DesPublicProvedorNoHabilitado();
	paginador.setIc_banco_fondeo(ic_banco_fondeo);
	paginador.setIc_epo(	ic_epo.equals("")?"0":ic_epo	);
	paginador.setIc_pyme(	ic_pyme.equals("")?"0":ic_pyme);
	paginador.setDf_fecha_publicacion_de(df_fecha_publicacion_de);
	paginador.setDf_fecha_publicacion_a(df_fecha_publicacion_a);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if(informacion.equals("Consultar")){

		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));

		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}

			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

	} else if (informacion.equals("ResumenTotales")) {		
		//Debe ser llamado despues de Consulta
		queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion			

	} else if (informacion.equals("ArchivoXPDF")) {
		JSONObject jsonObj = new JSONObject();
		try {
			//start = Integer.parseInt(request.getParameter("start"));
			//limit = Integer.parseInt(request.getParameter("limit"));
			//String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	

			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}

	} else if (informacion.equals("ArchivoPDF")) {

		try {

			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}

	} else if (informacion.equals("ArchivoCSV")) {

		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
}

%>
<%=infoRegresar%>