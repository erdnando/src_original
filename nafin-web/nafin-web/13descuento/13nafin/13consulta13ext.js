Ext.onReady(function() {
//---------------------------------- HANDLERS ----------------------------------
  var procesarConsultaParametrosPorEpoData = function(store, arrRegistros, opts) {
    var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
    var forma = Ext.getCmp('forma').getForm();
    var claveEpo = forma.getValues().claveEpo;
    //Se remueven los componentes anteriores... ya no pueden ser accesados
    contenedorPrincipalCmp.remove('gridConsultaParametrosPorEpo');
    //Se Agregan los nuevos componentes
    contenedorPrincipalCmp.add(gridConsultaParametrosPorEpo);
    contenedorPrincipalCmp.doLayout();
    if (!Ext.isEmpty(Ext.getCmp('botonGenerarPDF'))) {
      Ext.getCmp('botonGenerarPDF').enable();
    }
    if (!Ext.isEmpty(Ext.getCmp('botonBajarPDF'))) {
      Ext.getCmp('botonBajarPDF').hide();
    }
    if (!Ext.isEmpty(Ext.getCmp('botonGenerarArchivo'))) {
      Ext.getCmp('botonGenerarArchivo').enable();
    }
    if (!Ext.isEmpty(Ext.getCmp('botonBajarArchivo'))) {
      Ext.getCmp('botonBajarArchivo').hide();
    }
    Ext.getCmp('gridConsultaParametrosPorEpo').focus();
  }
  
  var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
    var botonGenerarArchivo = Ext.getCmp('botonGenerarArchivo');
    botonGenerarArchivo.setIconClass('');
    //Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
    if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
      var botonBajarArchivo = Ext.getCmp('botonBajarArchivo');
      botonBajarArchivo.show();
      botonBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
      botonBajarArchivo.setHandler( function(boton, evento) {
        var forma = Ext.getDom('formAux');
        forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
        forma.submit();
      });
    } else {
      botonGenerarArchivo.enable();
      NE.util.mostrarConnError(response, opts);
    }
  }
  
  var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
    var botonGenerarPDF = Ext.getCmp('botonGenerarPDF');
    botonGenerarPDF.setIconClass('');
    //Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
    if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
      var botonBajarPDF = Ext.getCmp('botonBajarPDF');
      botonBajarPDF.show();
      botonBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
      botonBajarPDF.setHandler( function(boton, evento) {
        var forma = Ext.getDom('formAux');
        forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
        forma.submit();
      });
    } else {
      botonGenerarPDF.enable();
      NE.util.mostrarConnError(response, opts);
    }
  }
//---------------------------------- STORES ------------------------------------
	var catalogoEpoData = new Ext.data.JsonStore({
		id: 'catalogoEpoDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		autoDestroy : true,
		url : '13consulta13ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
  
	var consultaParametrosPorEpoData = new Ext.data.JsonStore({
    id: 'consultaParametrosPorEpoDataStore',
		root : 'registros',
		fields : ["ORDEN", "DESCRIPCION", "VALOR"],
		autoDestroy : false,
		url : '13consulta13ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaParametrosPorEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
      load: procesarConsultaParametrosPorEpoData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
//----------------------------------------------------------------------------
	var elementosForma = [{
			xtype: 'combo',
			name: 'claveEpo',
			mode: 'local',
			displayField: 'descripcion',
			emptyText: 'Seleccione EPO...',
			valueField: 'clave',
			hiddenName : 'claveEpo',
			fieldLabel: 'Nombre de la EPO',
			disabled: false,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEpoData,
      listeners: {
        select: {
          fn: function (combo) {
            consultaParametrosPorEpoData.load({
              params: {
                claveEpo: combo.getValue()
              }
            });
          }
        }
      }
		}];

	var formPanel = {
		xtype: 'form',
		id: 'forma',
		fileUpload: true,
		width: 600,
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true
	};
  
	var gridConsultaParametrosPorEpo = {
		xtype: 'grid',
		id: 'gridConsultaParametrosPorEpo',
		columns: [
			{
				header: ' ',
				dataIndex: 'ORDEN',
				sortable: false,
				resizable: false,
				hidden: false,
        align: 'center',
				width: 100
			},
			{
				header: ' ',
				dataIndex: 'DESCRIPCION',
				sortable: false,
				hideable: false,
				resizable: false,
				align: 'left',
        width: 350,
        renderer: function (valor, columna, registro) {
          columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
          return valor;
        }
			},
			{
				header: ' ',
				dataIndex: 'VALOR',
				sortable: false,
				hideable: false,
				resizable: false,
				align: 'center',
        width: 150,
        renderer: function (valor, columna, registro) {
          columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(valor) + '"';
          if (valor.length > 10) {
            valor = 'Ver';
          }
          return valor;
        }
			}
		],
		store: consultaParametrosPorEpoData,
		margins: '10 0 0 0',
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		autoHeight: true,
		width: 600,
		title: '<center>Parámetros por EPO</center>',
		frame: true,
		bbar: {
      id: 'barraBotones',
      items: [
        {
          xtype: 'button',
          text: 'Generar Archivo',
          id: 'botonGenerarArchivo',
          handler: function (boton, evento) {
            var forma = Ext.getCmp('forma').getForm();
            boton.disable();
            boton.setIconClass('loading-indicator');
            Ext.Ajax.request({
              url: '13consulta13exta.jsp',
              params: {
                claveEpo: forma.getValues().claveEpo
              },
              callback: procesarSuccessFailureGenerarArchivo
            });
          }
        },
        {
          xtype: 'button',
          text: 'Bajar Archivo',
          id: 'botonBajarArchivo',
          hidden: true
        },
        {
          xtype: 'button',
          text: 'Generar PDF',
          id: 'botonGenerarPDF',
          handler: function(boton, evento) {
            var forma = Ext.getCmp('forma').getForm();
            boton.disable();
            boton.setIconClass('loading-indicator');
            Ext.Ajax.request({
              url: '13consulta13extpdf.jsp',
              params: {
                claveEpo: forma.getValues().claveEpo
              },
              callback: procesarSuccessFailureGenerarPDF
            });
          }
        },
        {
          xtype: 'button',
          text: 'Bajar PDF',
          id: 'botonBajarPDF',
          hidden: true
        }
      ]
    }
	};
//-------------------------------- PRINCIPAL -----------------------------------
//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		layout: 'vbox',
		width: 950,
		height: 3000,
		layoutConfig: {
			align:'center'
		},
		items: [
			formPanel
		]
	});

//------------------------------------------------------------------------------

});