<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%
System.out.println("13consulta18exta  (E)");
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_banco_fondeo = request.getParameter("ic_banco_fondeo") == null?"1":(String)request.getParameter("ic_banco_fondeo");
String ic_epo = request.getParameter("ic_epo")== null?"0":(String)request.getParameter("ic_epo");
if(ic_epo.equals("")){ ic_epo = "0"; }
System.out.println("ic_banco_fondeo   "+ic_banco_fondeo);
System.out.println("ic_epo   "+ic_epo);
System.out.println("informacion   "+informacion);

AccesoDB con = new AccesoDB();
JSONObject jsonObj = new JSONObject();
CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo= new StringBuffer(); 
String nombreArchivo="";

%>

<%
try {
	con.conexionDB();
	
	StringBuffer  strQuery 		= new StringBuffer(); 
	strQuery.append( " SELECT cat.cg_razon_social nombre, rel.ig_dias_minimo as dmin, rel.ig_dias_maximo as dmax, rel.fn_aforo as fecha_aforo, rel.fn_aforo_dl as fecha_aforo_dol,"    );
	strQuery.append( " rel.cs_factoraje_vencido as fact_venc, rel.cs_factoraje_distribuido as fact_distr"    );
	strQuery.append( " FROM comrel_producto_epo rel,"    );
	strQuery.append( "	comcat_epo cat"  );
	strQuery.append( " WHERE rel.ic_epo=cat.ic_epo" );
	strQuery.append( " AND rel.ic_producto_nafin = ?" );
  strQuery.append( "  AND cat.ic_banco_fondeo = ?"   );
	if(!ic_epo.equals("0")){
		strQuery.append("    AND rel.ic_epo = ?" );
	}
	strQuery.append(" ORDER BY cat.cg_razon_social ");   
	PreparedStatement ps = con.queryPrecompilado(strQuery.toString());
  ps.setInt(1, 1);
  ps.setInt(2, Integer.parseInt(ic_banco_fondeo));
	if(!ic_epo.equals("0")){
		ps.setString(3, ic_epo);
	}
	ResultSet rs = ps.executeQuery();			
	System.out.println(":::13consulta18apop ::"+ strQuery.toString());
	
	contenidoArchivo.append("Epo, Días Mín. para Descuento, Días Máx. para Descuento, Porcentaje de Descuento M.N, Porcentaje de Descuento Dol., Opera Factoraje Vencido, Opera Factoraje Distribuido "); 
	contenidoArchivo.append("\n");
	
	while (rs.next()) {
		String nombre = (rs.getString("nombre")==null)?"":rs.getString("nombre").trim();
		String dmin = (rs.getString("dmin")==null)?"":rs.getString("dmin").trim();
		String dmax  = (rs.getString("dmax")==null)?"":rs.getString("dmax").trim();
		String fecha_aforo  = (rs.getString("fecha_aforo")==null)?"":rs.getString("fecha_aforo").trim();
		String fecha_aforo_dol = (rs.getString("fecha_aforo_dol")==null)?"":rs.getString("fecha_aforo_dol").trim();
		String fact_venc = (rs.getString("fact_venc")==null)?"":rs.getString("fact_venc").trim();
		String fact_distr = (rs.getString("fact_distr")==null)?"":rs.getString("fact_distr").trim();
		
		contenidoArchivo.append(nombre.replace(',',' ')+", ");
		contenidoArchivo.append(dmin.replace(',',' ')+", ");
		contenidoArchivo.append(dmax.replace(',',' ')+", ");
		contenidoArchivo.append(fecha_aforo.replace(',',' ')+"%, ");
		contenidoArchivo.append(fecha_aforo_dol.replace(',',' ')+"%, ");
		contenidoArchivo.append(fact_venc.replace(',',' ')+", ");
		contenidoArchivo.append(fact_distr.replace(',',' ')+"\n");
	
	}
	rs.close();
	ps.close();
	con.cierraStatement();
	
	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo CSV ");
	} else {
		nombreArchivo = archivo.nombre;		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
		
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	if (con.hayConexionAbierta() == true) {
		con.cierraConexionDB();
	}
}
%>
	<%=jsonObj%>	

<%System.out.println("jsonObj  "+jsonObj); %>

<%System.out.println("13consulta18exta  (S)"); %>
