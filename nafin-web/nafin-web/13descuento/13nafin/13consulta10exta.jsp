<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>
<%System.out.println("13consulta10a.jsp (E)");%>

<%
String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
String df_operacion_inicio = (request.getParameter("df_operacion_inicio")==null)?"":request.getParameter("df_operacion_inicio");
String df_operacion_fin=(request.getParameter("df_operacion_fin")==null)?"":request.getParameter("df_operacion_fin");
String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")==null)?"":request.getParameter("ic_banco_fondeo");  //FODEA 007 - 2009
if(ic_banco_fondeo.equals("")){ic_banco_fondeo = iTipoPerfil.equals("8")?"2":"1";} //FODEA 007 - 2009
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

ResultSet rs = null;
CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer(1024);
String nombreArchivo=null;
int registros=0;
JSONObject jsonObj = new JSONObject();
String 	NOM_EPO = "", NOM_PYME = "",NOM_IF = "", 	NUM_DOCTO = "",	FECHA_EMISION ="",
		FECHA_VENC = "", 	MONEDA = "", 	TIPODESC ="", 	MONTO_DOCTO = "",	PORC_DESCTO = "",
		MONTO_DESCONTAR = "", TIPO_DSCTOAUTO = "", DIAS_MINIMOS = "", LIMITE_IFEPO = "",
		CAUSA_RECHAZO = "", FECHA_OPERACION = ""; 
				
CQueryHelper	queryHelper = null;
AccesoDB 		con			= new AccesoDB();
/*System.out.println("df_operacion_inicio---->"+df_operacion_inicio);
System.out.println("df_operacion_fin---->"+df_operacion_fin);
System.out.println("operacion---->"+operacion);
System.out.println("ic_banco_fondeo---->"+ic_banco_fondeo);
System.out.println("informacion---->"+informacion);	
*/
try{
	con.conexionDB();
	queryHelper = new CQueryHelper(new  ConsRechDescNafinDE());
	//Cabecera del archivo
	contenidoArchivo.append("Nombre de EPO,PYME,IF,Num. Documento,Fecha Emisión,Fecha Vencimiento,Moneda,Tipo de Descuento,Monto,% Descuento,Monto a Descontar,Tipo de Dscto. Automático ,Días Mínimos Cadena,Límite IF-EPO,Causa de Rechazo,Fecha y Hora Descto.\n");
	//Generacion del archivo 
	rs = queryHelper.getCreateFile(request,con);
	while (rs.next()) {
		NOM_EPO = (rs.getString(1)==null)?"":rs.getString(1).replace(',',' ');
		NOM_PYME = (rs.getString(2) == null)?"":rs.getString(2).replace(',',' ');
		NOM_IF = (rs.getString(3) == null)?"":rs.getString(3).replace(',',' ');
		NUM_DOCTO = (rs.getString(4)==null)?"":rs.getString(4).replace(',',' ');
		FECHA_EMISION = (rs.getString(5)==null)?"":rs.getString(5);
		FECHA_VENC = (rs.getString(6)==null)?"":rs.getString(6);
		MONEDA = (rs.getString(7)==null)?"":rs.getString(7);
		TIPODESC = (rs.getString(8)==null)?"":rs.getString(8);
		MONTO_DOCTO = (rs.getString(9)==null)?"":rs.getString(9);
		PORC_DESCTO = (rs.getString(10)==null)?"":rs.getString(10);
		MONTO_DESCONTAR = (rs.getString(11)==null)?"":rs.getString(11);
		TIPO_DSCTOAUTO = (rs.getString(12)==null)?"":rs.getString(12);
		DIAS_MINIMOS = (rs.getString(13)==null)?"":rs.getString(13);
		LIMITE_IFEPO = (rs.getString(14)==null)?"":rs.getString(14);
		CAUSA_RECHAZO = (rs.getString(15)==null)?"":rs.getString(15).replace(',',' ');
		FECHA_OPERACION = (rs.getString(16)==null)?"":rs.getString(16);

		//Contenido del archivo
		contenidoArchivo.append(
				NOM_EPO + "," +
				NOM_PYME + "," +
				NOM_IF + "," +
				NUM_DOCTO + "," +
				FECHA_EMISION + "," +
				FECHA_VENC + "," +
				MONEDA + "," +
				TIPODESC + "," +
				"$"+Comunes.formatoDecimal(MONTO_DOCTO,2,SIN_COMAS) + "," +
				Comunes.formatoDecimal(PORC_DESCTO,2,SIN_COMAS)+"% ," +
				"$"+Comunes.formatoDecimal(MONTO_DESCONTAR,2,SIN_COMAS) + "," +
				TIPO_DSCTOAUTO + "," +
				DIAS_MINIMOS  + "," +
				( LIMITE_IFEPO!=null && !LIMITE_IFEPO.equals("")?"$"+Comunes.formatoDecimal(LIMITE_IFEPO,2,SIN_COMAS):"" ) + "," +
				CAUSA_RECHAZO + "," +
				FECHA_OPERACION + "\n");
		registros++;
	} //fin del while
	rs.close();
		
	if(registros!=0){
		if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo CSV ");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
} finally {
	if (con.hayConexionAbierta()) {
		con.cierraConexionDB();
	}
}
%>
<%=jsonObj%>
<%System.out.println("13consulta10a.jsp (S)");%>
