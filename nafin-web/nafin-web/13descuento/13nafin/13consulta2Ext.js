Ext.onReady(function(){

var camposAdicionales=0;
var sesPerfil;
var DocumentoEnviar;

	var VisorAcusesDeCarga = function(rec){
		var ic_acuse = rec.get('ACUSE_DOCTO');
		if(ic_acuse != ''){
			var win = new NE.AcusesDeCarga.VisorAcusesDeCarga(ic_acuse);
		}
	}

function verificaFechas(fec,fec2){
		
		var fecha1= Ext.getCmp(fec);
		var fecha2=Ext.getCmp(fec2);
		if(fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
				fecha1.markInvalid('Ambos Valores son necesarios');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('Ambos Valores son necesarios');
				fecha2.focus();
				return false;
			}
		}
	return true;
}	
var accionConsulta = function(estadoSiguiente, respuesta){
if(  estadoSiguiente == "OBTENER_NOMBRE_PYME" ){
		
			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '13consulta2Ext.data.jsp',	
				params: {
							numeroDeProveedor:	respuesta.noNafinElec,
							ic_banco_fondeo:		respuesta.ic_banco_fondeo,
							ic_epo:					respuesta.ic_epo,
							informacion: 			"obtenNombrePyme" 
				},
				callback: procesaConsulta
			});

		}else if(  estadoSiguiente == "RESPUESTA_OBTENER_NOMBRE_PYME"){
		
			Ext.getCmp('hid_ic_pyme').setValue(respuesta.ic_pyme);
			Ext.getCmp('_txt_nombre').setValue(respuesta.txtCadenasPymes);
			fp.el.unmask();
			if (respuesta.muestraMensaje != undefined && respuesta.muestraMensaje){
				Ext.Msg.alert("Mensaje de p�gina web.",	respuesta.textoMensaje);
				Ext.getCmp('_txt_nafelec').setValue('');
			}else{

			}

		}
}
var procesaConsulta = function(opts, success, response) {
		//Ext.getCmp('contenedorPrincipal').ocultaMascaras();

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);

				// Procesar respuesta
				if(!Ext.isEmpty(resp.msg)){
					
				} else {
					accionConsulta(resp.estadoSiguiente,resp);
				}
		}else{
				// Mostrar mensaje de error
				NE.util.mostrarConnError(response,opts);
		}
	}
	function columnas(){
		var ic_estatus_docto=Ext.getCmp('estatus').getValue();
		var cm = grid.getColumnModel();
		var numCol = cm.getColumnCount();
		for (i=0;i<numCol;i++){
				cm.setHidden(i, true);
			}
		if (ic_estatus_docto=="" || ic_estatus_docto==("1") || ic_estatus_docto==("2") || ic_estatus_docto==("5")
			|| ic_estatus_docto==("6") || ic_estatus_docto==("7") || ic_estatus_docto==("9")
			|| ic_estatus_docto==("10") || ic_estatus_docto==("21") || ic_estatus_docto==("23")
			|| ic_estatus_docto==("28") || ic_estatus_docto==("29")|| ic_estatus_docto==("30")|| ic_estatus_docto==("31") || ic_estatus_docto==("33"))//FODEA 005 - 2009 ACF
			
		{
			cm.setHidden(cm.findColumnIndex('NOMBREEPO'), false);
			cm.setHidden(cm.findColumnIndex('NOMBREPYME'), false);
			cm.setHidden(cm.findColumnIndex('NOMBREIF'), false);
			cm.setHidden(cm.findColumnIndex('IG_NUMERO_DOCTO'), false);
			cm.setHidden(cm.findColumnIndex('DF_FECHA_DOCTO'), false);
			cm.setHidden(cm.findColumnIndex('DF_FECHA_VENC'), false);
			cm.setHidden(cm.findColumnIndex('MONEDA'), false);
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
			cm.setHidden(cm.findColumnIndex('FN_MONTO'), false);
			cm.setHidden(cm.findColumnIndex('FN_PORC_ANTICIPO'), false);
			cm.setHidden(cm.findColumnIndex('DBL_RECURSO'), false);
			cm.setHidden(cm.findColumnIndex('FN_MONTO_DSCTO'), false);
			cm.setHidden(cm.findColumnIndex('CD_DESCRIPCION'), false);//ESTATUS
			cm.setHidden(cm.findColumnIndex('MOD_MONTOS_COL'), false);
			cm.setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEF'), false);
			cm.setHidden(cm.findColumnIndex('DF_PROGRAMACION'), false);
			cm.setHidden(cm.findColumnIndex('DF_ALTA'), false);
			cm.setHidden(cm.findColumnIndex('DUPLICADO'), false);
			cm.setHidden(cm.findColumnIndex('NUMEROSIAFF'), false);
			cm.setHidden(cm.findColumnIndex('DF_ENTREGA'), false);
			cm.setHidden(cm.findColumnIndex('CG_TIPO_COMPRA'), false);
			cm.setHidden(cm.findColumnIndex('CG_CLAVE_PRESUPUESTARIA'), false);
			cm.setHidden(cm.findColumnIndex('CG_PERIODO'), false);
			if(catalogoFactoraje.reader.jsonData.mandante==true)
			cm.setHidden(cm.findColumnIndex('NOMBREMANDANTE'), false);//MANDATE	*/
			cm.setHidden(cm.findColumnIndex('FIDEICOMISO'), false);
			cm.setHidden(cm.findColumnIndex('ACUSE_DOCTO'), false);
			cm.setHidden(cm.findColumnIndex('VALIDACION_JUR'), false);
		}else{
			cm.setHidden(cm.findColumnIndex('NOMBREEPO'), false);	
			cm.setHidden(cm.findColumnIndex('NOMBREPYME'), false);
			cm.setHidden(cm.findColumnIndex('NOMBREIF'), false);
			cm.setHidden(cm.findColumnIndex('IG_NUMERO_DOCTO'), false);
			cm.setHidden(cm.findColumnIndex('DF_FECHA_DOCTO'), false);
			cm.setHidden(cm.findColumnIndex('DF_FECHA_VENC'), false);
			cm.setHidden(cm.findColumnIndex('MONEDA'), false);
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			//cm.setHidden(cm.findColumnIndex('FN_MONTO'), false);
			cm.setHidden(cm.findColumnIndex('FN_PORC_ANTICIPO'), false);
			cm.setHidden(cm.findColumnIndex('DBL_RECURSO'), false);
			cm.setHidden(cm.findColumnIndex('FN_MONTO_DSCTO'), false);
			cm.setHidden(cm.findColumnIndex('FN_MONTO_DOC'), false);
			cm.setHidden(cm.findColumnIndex('IG_PLAZO'), false);
			cm.setHidden(cm.findColumnIndex('IN_IMPORTE_DEPOSITAR_PYME'), false);
			
			cm.setHidden(cm.findColumnIndex('IN_IMPORTE_INTERES'), false);	
			cm.setHidden(cm.findColumnIndex('IN_IMPORTE_RECIBIR'), false);	
			cm.setHidden(cm.findColumnIndex('IN_TASA_ACEPTADA'), false);	
			cm.setHidden(cm.findColumnIndex('CC_ACUSE'), false);
			cm.setHidden(cm.findColumnIndex('CD_DESCRIPCION'), false);//ESTATUS
			
			cm.setHidden(cm.findColumnIndex('MOD_MONTOS'), false);
			if (ic_estatus_docto=="4" || ic_estatus_docto==("11") || ic_estatus_docto==("12"))
				cm.setHidden(cm.findColumnIndex('DF_FECHA_SOLICITUD'), false);
			
			cm.setHidden(cm.findColumnIndex('DETALLE'), false);
			cm.setHidden(cm.findColumnIndex('NETO_REC_PYME'), false);
			cm.setHidden(cm.findColumnIndex('PORCDOCTOAPLICADO'), false);
			cm.setHidden(cm.findColumnIndex('FN_MONTO_PAGO'), false);			     
			
			cm.setHidden(cm.findColumnIndex('BENEFICIARIO'), false);	
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEF'), false);	
			cm.setHidden(cm.findColumnIndex('DF_PROGRAMACION'), false);
			cm.setHidden(cm.findColumnIndex('DF_ALTA'), false);
			cm.setHidden(cm.findColumnIndex('NUMEROSIAFF'), false);
			cm.setHidden(cm.findColumnIndex('DF_ENTREGA'), false);
			cm.setHidden(cm.findColumnIndex('CG_TIPO_COMPRA'), false);
			cm.setHidden(cm.findColumnIndex('CG_CLAVE_PRESUPUESTARIA'), false);
			cm.setHidden(cm.findColumnIndex('CG_PERIODO'), false);
			if(catalogoFactoraje.reader.jsonData.mandante==true)
			cm.setHidden(cm.findColumnIndex('NOMBREMANDANTE'), false);//MANDATE	*/
			cm.setHidden(cm.findColumnIndex('DETALLES'),false);
			cm.setHidden(cm.findColumnIndex('FIDEICOMISO'), false);
			cm.setHidden(cm.findColumnIndex('VALIDACION_JUR'), false);
		
		}
		
		cm.setHidden(cm.findColumnIndex('COPADE'), false); //QC-2018
		cm.setHidden(cm.findColumnIndex('CONTRATO'), false); //QC-2018
			
	}


var muestraDetalleAplicacion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_docto = registro.get('IC_DOCUMENTO');
		DocumentoEnviar=ic_docto;
		var ventana = Ext.getCmp('winAplica');
		if (ventana){
			ventana.hide();
			
		}
		gridAplicaData.loadData('');
		Ext.getCmp('disDocto').setValue('');
			Ext.getCmp('disMontoAplica').setValue('');
		
		Ext.Ajax.request({url: '13consulta2Ext.data.jsp',params: Ext.apply({informacion: "obtenAplica",ic_documento: ic_docto}),callback: procesarMuestraPanelAplica});
	}
	
		function procesarMuestraPanelAplica(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var dataAplica = Ext.util.JSON.decode(response.responseText);
			if (dataAplica.datosAplica != undefined)	{
				gridAplicaData.loadData(dataAplica.datosAplica);
				var disDocto	= Ext.getCmp('disDocto');
				var disMontoAplica	= Ext.getCmp('disMontoAplica');
				
				Ext.each(dataAplica.datosAplica, function(item, index, arrItems){
					disDocto.setValue(item.IG_NUMERO_DOCTO);
					disMontoAplica.setValue(Ext.util.Format.number(item.IN_IMPORTE_RECIBIR, '$0,0.00'));
					if (index > 0){return false;}
				});
				var storeGrid = gridAplicaData.data;
				var sumImporteAplica=0;
				var sumSaldoCre=0;
				var sumImporteDepo=0;
				var sumImporteAplicaDL=0;
				var sumSaldoCreDL=0;
				var sumImporteDepoDL=0;
				if(storeGrid.length > 0) {
					var orden = 1;
					storeGrid.each(function(registro,items){
						registro.set('ORDEN_APLICA',orden);
						if (registro.get('IC_MONEDA') == "1"){
							sumImporteAplica += registro.get('IMPORTE_APLICADO_CREDITO');
							sumSaldoCre += registro.get('SALDO_CREDITO');
						}
						if (registro.get('IC_MONEDA') == "54"){
							sumImporteAplicaDL += registro.get('IMPORTE_APLICADO_CREDITO');
							sumSaldoCreDL += registro.get('SALDO_CREDITO');								
						}
						orden++;
					});
					sumImporteDepo = disMontoAplica - sumImporteAplica;
					sumImporteDepoDL = disMontoAplica - sumImporteAplicaDL;
					if (sumImporteDepo <= 0 ){sumImporteDepo = 0;}
					if (sumImporteDepoDL <= 0 ){sumImporteDepoDL = 0;}

					var reg = gridTotalAplicaData.getAt(0);//Primer registro de conteo
					reg.set('TOTAL_IMPORTE_APLICADO',sumImporteAplica);
					reg.set('TOTAL_SALDO_CREDITO',sumSaldoCre);
					reg.set('TOTAL_IMPORTE_RECIBIR',sumImporteDepo);
					
					var regDL = gridTotalAplicaData.getAt(1);//Segundo registro de conteo
					regDL.set('TOTAL_IMPORTE_APLICADO',sumImporteAplicaDL);
					regDL.set('TOTAL_SALDO_CREDITO',sumSaldoCreDL);
					regDL.set('TOTAL_IMPORTE_RECIBIR',sumImporteDepoDL);
				}
			}
			var ventana = Ext.getCmp('winAplica');
			
			if (ventana) {
				ventana.show();
			}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				width: 803,
				height: 480,
				id: 'winAplica',
				closeAction: 'hide',
				items: [fpAplica,NE.util.getEspaciador(5),gridAplica,NE.util.getEspaciador(3),gridTotalAplica],
				title: 'Detalle de los cr�ditos pagados con el documento',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',	{xtype: 'button',	text: 'Generar PDF',		id: 'btnPdfAplica'}
												,{xtype: 'button',	text: 'Bajar PDF',		id: 'btnBajarPdfAplica',hidden: true}
										,'-',	{xtype: 'button',	text: 'Generar Archivo',id: 'btnCsvAplica'}
												,{xtype: 'button',	text: 'Bajar Archivo',	id: 'btnBajarCsvAplica',hidden: true}	]
				}
				}).show();
			}
			var storeGrid = gridAplicaData.data;
			var btnPdfAplica = Ext.getCmp('btnPdfAplica');
			var btnBajarPdfAplica = Ext.getCmp('btnBajarPdfAplica');
			var btnCsvAplica = Ext.getCmp('btnCsvAplica');
			var btnBajarCsvAplica = Ext.getCmp('btnBajarCsvAplica');
			gridTotalAplica = Ext.getCmp('gridTotalAplica');
			if(storeGrid.length < 1) {
				gridAplica.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				gridTotalAplica.hide();
				btnPdfAplica.disable();
				btnCsvAplica.disable();
			}else	{
				gridAplica.getGridEl().unmask();
				btnPdfAplica.enable();
				btnCsvAplica.enable();
				btnBajarPdfAplica.hide();
				btnBajarCsvAplica.hide();
				if (!gridTotalAplica.isVisible()){
					gridTotalAplica.show();
				}
			}
			var btnGenerar = Ext.getCmp('btnPdfAplica');
			btnGenerar.setHandler(
				function(boton, evento) {
				
					Ext.Ajax.request({
						url: '13consulta2Ext_apl_pdf.jsp',
						params: Ext.apply({ic_documento:DocumentoEnviar}),
						callback: procesarArchivoSuccess
					});
				}
			);
		
			var btnGenerarCSV = Ext.getCmp('btnCsvAplica');
			btnGenerarCSV.setHandler(
			function(boton, evento) {
				
				Ext.Ajax.request({
						url: '13consulta2Ext_apl_csv.jsp',
						params: Ext.apply({ic_documento: DocumentoEnviar}),
						callback: procesarArchivoSuccess
					});
				}
			);

		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarConDetalleData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
		var gridDetalle = Ext.getCmp('gridDetalle');
	
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) {
				gridDetalle.show();
			}					
			
			var jsonData = store.reader.jsonData;	
	
			var btnImpDetallePDF = Ext.getCmp('btnImpDetallePDF');
			var btnBajarImpDetallePDF = Ext.getCmp('btnBajarImpDetallePDF');
			var btnArchiDetalleCsv = Ext.getCmp('btnArchiDetalleCsv');
			var btnBajarArchiDetalleCsv = Ext.getCmp('btnBajarArchiDetalleCsv');
			var gridTotalesDetalle = Ext.getCmp('gridTotalesDetalle');
			btnBajarImpDetallePDF.hide();
			btnBajarArchiDetalleCsv.hide();
					
			gridDetalle.setTitle('<div style="float:left">N�mero de documento: '+jsonData.claveDocumento+'</div><div style="float:right"> Monto aplicado '+jsonData.monto+'</div>');						
						
			if(store.getTotalCount() > 0) {	
			
				consTotalDetalleData.load({
					params:	{
						clavePyme:	jsonData.clavePyme,
						claveDocumento:jsonData.claveDocumento
					}
				});	
				
				gridTotalesDetalle.show();
			
				btnImpDetallePDF.enable();
				btnArchiDetalleCsv.enable();
				gridDetalle.el.unmask();							
			} else {	
				btnImpDetallePDF.disable();
				btnArchiDetalleCsv.disable();
				gridTotalesDetalle.hide();
				gridDetalle.el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var verDetalleAplicacion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		
		var clavePyme = registro.get('IC_PYME');
		var claveDocumento = registro.get('IC_DOCUMENTO');
		
			consultaDetalleData.load({
				params:	{
					clavePyme:	clavePyme,
					claveDocumento:claveDocumento
				}
			});	
					
		var ventana = Ext.getCmp('verDetalle');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 830,
				modal: true,
				height: 'auto',			
				id: 'verDetalle',
				closeAction: 'hide',
				autoDestroy:true,
				closable:false,
				items: [	
				gridDetalle,
				gridTotalesDetalle
				],
				title: 'Detalle de los cr�ditos pagados con el documento ',
				bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{
							xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrarDe', 
							handler: function(){
								Ext.getCmp('gridDetalle').hide();
								Ext.getCmp('verDetalle').destroy();
							} 
						},
						'-',
						{
							xtype: 'button',
							text: 'Generar Archivo',
							tooltip:	'Generar Archivo',
							id: 'btnArchiDetalleCsv',	
							handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								var barraPaginacion = Ext.getCmp("barraPaginacion");
								Ext.Ajax.request({
									url: '13consulta2Ext.data.jsp',
									params:	{
										informacion:'ImpCsvDetalle',
										clavePyme:	clavePyme,
										claveDocumento:claveDocumento
									}					
									,callback: procesarSuccessFailureImpDetalleCsv
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip:	'Bajar Archivo',
							id: 'btnBajarArchiDetalleCsv',
							hidden: true
						},
						{
							xtype: 'button',
							text: 'Imprimir',
							tooltip:	'Imprimir',
							id: 'btnImpDetallePDF',	
							handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								var barraPaginacion = Ext.getCmp("barraPaginacion");
								Ext.Ajax.request({
									url: '13consulta2Ext.data.jsp',
									params:	{
										informacion:'ImpPDFDetalle',										
										clavePyme:	clavePyme,
										claveDocumento:claveDocumento
									}					
									,callback: procesarSuccessFailureImpDetallePDF
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip:	'Bajar Archivo',
							id: 'btnBajarImpDetallePDF',
							hidden: true
						}
					]
				}
			}).show();
		}	
	}	
	
	
	function procesarMuestraPanelAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var dataAcuse = Ext.util.JSON.decode(response.responseText);
			if (dataAcuse.datosAcuse2 != undefined)	{
				Ext.each(dataAcuse.datosAcuse2, function(item, index, arrItems){
					var disFechaCarga		= Ext.getCmp('disFechaCarga');
					var disHoraCarga	= Ext.getCmp('disHoraCarga');
					var disUsuario	= Ext.getCmp('disUsuario');
					disFechaCarga.setValue(item.FECHA_CARGA);
					disHoraCarga.setValue(item.HORA_CARGA);
					disUsuario.setValue(item.IC_USUARIO);
				});
			}

			if (dataAcuse.datosAcuse != undefined){
				gridAcuseData.loadData(dataAcuse.datosAcuse);
				if (!gridAcuse.isVisible()) {
					gridAcuse.show();
				}
			}
			if (dataAcuse.datosAcuseTotal != undefined){
				gridTotalAcuseData.loadData(dataAcuse.datosAcuseTotal);
			}
			var ventana = Ext.getCmp('winAcuse');
			if (ventana) {
				ventana.show();
			}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				width: 800,
				style: 'margin:0 auto;',
				height: 540,
				id: 'winAcuse',
				closeAction: 'hide',
				items: [gridAcuse,NE.util.getEspaciador(10),gridTotalAcuse,NE.util.getEspaciador(10),fpAcuse],
				title: 'Acuse',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',	text: 'Generar PDF',		id: 'btnPdfAcuse'}
											,{xtype: 'button',	text: 'Bajar PDF',		id: 'btnBajarPdfAcuse',hidden: true}
										,'-',{xtype: 'button',	text: 'Generar Archivo',id: 'btnCsvAcuse'}
											,{xtype: 'button',	text: 'Bajar Archivo',	id: 'btnBajarCsvAcuse',hidden: true}]
				}
				}).show();
			}
			var storeGrid = gridAcuseData.data;
			var btnPdfAcuse = Ext.getCmp('btnPdfAcuse');
			var btnBajarPdfAcuse = Ext.getCmp('btnBajarPdfAcuse');
			var btnCsvAcuse = Ext.getCmp('btnCsvAcuse');
			var btnBajarCsvAcuse = Ext.getCmp('btnBajarCsvAcuse');
			if(storeGrid.length < 1) {
				gridAcuse.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				btnPdfAcuse.disable();
				btnCsvAcuse.disable();
				gridTotalAcuseData.loadData('');
				gridTotalAcuse.getGridEl().mask('');
			} else	{
				btnPdfAcuse.enable();
				btnCsvAcuse.enable();
				btnBajarPdfAcuse.hide();
				btnBajarCsvAcuse.hide();
			}
			var btnGenerar = Ext.getCmp('btnPdfAcuse');
			btnGenerar.setHandler(
				function(boton, evento) {
					boton.disable();
						boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta02ext_acu_pdf.jsp',
						params: Ext.apply({ccAcuse: cc_acuse}),
						callback: procesarArchivoSuccess
					});
				}
			);

			var btnGenerarCSV = Ext.getCmp('btnCsvAcuse');
			btnGenerarCSV.setHandler(
			function(boton, evento) {
				
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
						url: '13consulta02ext_acu_csv.jsp',
						params: Ext.apply({ccAcuse: cc_acuse}),
						callback: procesarArchivoSuccess
					});
				}
			);

		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var muestraPanelAcuse = function(grid, rowIndex, colIndex, item, event) {
	var registro = grid.getStore().getAt(rowIndex);
	cc_acuse = registro.get('CC_ACUSE');
	var ventana = Ext.getCmp('winAcuse');
	if (ventana){
		ventana.hide();
	}
	gridTotalAcuseData.loadData('');
	gridAcuseData.loadData('');
	var disAcuse	= Ext.getCmp('disAcuse');
	var disFechaCarga		= Ext.getCmp('disFechaCarga');
	var disHoraCarga	= Ext.getCmp('disHoraCarga');
	var disUsuario	= Ext.getCmp('disUsuario');
	disAcuse.setValue(cc_acuse);
	disFechaCarga.setValue('');
	disHoraCarga.setValue('');
	disUsuario.setValue('');
	
	Ext.Ajax.request({url: '13consulta2Ext.data.jsp',params: Ext.apply({informacion: "obtenAcuse",ccAcuse: cc_acuse}),callback: procesarMuestraPanelAcuse});
		
	}
	
	var fpAplica=new Ext.FormPanel({
		height:34,
		width: 790,
		bodyStyle: 'padding: 2px',
		border: false,
		labelWidth: 150,
		items: elementosAplica,
		hidden: false
	});

	function procesarMuestraPanelModifica(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var dataModifica = Ext.util.JSON.decode(response.responseText);
			
			if (dataModifica.datosModifica != undefined)	{
				gridModificaData.loadData(dataModifica.datosModifica);
			}

			var ventana = Ext.getCmp('winModifica');
			if (ventana) {
				Ext.getCmp('btnPdfModifica').enable();
				Ext.getCmp('btnBajarPdfModifica').hide();
				ventana.show();
				var storeGrid = gridModificaData.data;
				if(storeGrid.length < 1) {
					gridModifica.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					//Ext.getCmp('btnPdfModifica').disable();
				}
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var muestraPanelModifica = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var ic_documen = registro.get('IC_DOCUMENTO');
		var cg_pyme_epo_int = registro.get('CG_PYME_EPO_INTERNO');
		var nombreProv = registro.get('NOMBREPYME');
		var nombre_if = registro.get('NOMBREIF');
		var icDocto = registro.get('IG_NUMERO_DOCTO');
		var fecha_emis =	registro.get("DF_FECHA_DOCTO");
		var fecha_venc = registro.get("DF_FECHA_VENC");
		var nombreMoneda = registro.get('MONEDA');
		var fnMonto = registro.get('FN_MONTO');
		var estatusDocto = registro.get('IC_ESTATUS_DOCTO');
		//
		gridModificaData.loadData('');
		//
		Ext.getCmp('disProv').setValue(cg_pyme_epo_int + "   " +nombreProv);
		Ext.getCmp('disIfM').setValue(nombre_if);
		Ext.getCmp('disDoctoM').setValue(icDocto);
		Ext.getCmp('disFecEmiM').setValue(fecha_emis);
		Ext.getCmp('disFecVencM').setValue(fecha_venc);
		Ext.getCmp('disMonedaM').setValue(nombreMoneda);
		Ext.getCmp('disMontoM').setValue(Ext.util.Format.number(fnMonto,'$ 0,0.00'));
		Ext.getCmp('disEstatusM').setValue(estatusDocto);
		var ventana = Ext.getCmp('winModifica');
		if (ventana) {
			ventana.hide();
		}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				width: 803,
				//height: 480,
				autoHeight:true,
				id: 'winModifica',
				closeAction: 'hide',
				items: [fpModifica,NE.util.getEspaciador(10),gridModifica],
				title: 'Modificaci�n montos y/o Fechas',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',text: 'Generar PDF',	id: 'btnPdfModifica'},{xtype: 'button',text: 'Bajar PDF',id: 'btnBajarPdfModifica',hidden: true}
									,'-',{xtype: 'button',	text: 'Cerrar',id: 'btnCloseMod'}]
				}
			});
		}
		
		Ext.getCmp('btnPdfModifica').setHandler(
			function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '13consulta01ext_mod_pdf.jsp',
					params: Ext.apply({ic_docto: ic_documen, cg_pyme_epo_interno:cg_pyme_epo_int,	nombreProv:nombreProv, ic_if: nombre_if,ig_numero_docto:icDocto,fch_emision:fecha_emis,fch_venc:fecha_venc,moneda:nombreMoneda,
											monto:fnMonto,estatus:estatusDocto}),
					callback: procesarArchivoSuccess
				});
			}
		);

		Ext.getCmp('btnCloseMod').setHandler(
			function(boton, evento) {
				Ext.getCmp('winModifica').hide();
			}
		);

		Ext.Ajax.request({url: '13consulta2Ext.data.jsp',params: Ext.apply(
		{informacion: "modificaMontos",ic_docto: ic_documen	}),callback: procesarMuestraPanelModifica});

	}

function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('btnGenerarArchivo');
			boton.setIconClass('icoXls');
			boton.enable();
			
			var botonE=Ext.getCmp('btnPdfModifica');
			if(botonE!=undefined){
				botonE.setIconClass('');
				botonE.enable();
			}
			var botonE=Ext.getCmp('btnPdfAcuse');
			if(botonE!=undefined){
				botonE.setIconClass('');
				botonE.enable();
			}
			
			var botonE=Ext.getCmp('btnCsvAcuse');
			if(botonE!=undefined){
				botonE.setIconClass('');
				botonE.enable();
			}
			
			 
			var botonPdf=Ext.getCmp('btnGenerarPDF');
			botonPdf.setIconClass('icoPdf');
			botonPdf.enable();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
function procesaTotales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			gridTotales.setVisible(true);
			totalesData.loadData(Ext.util.JSON.decode(response.responseText).registros);
		} else {
		gridTotales.setVisible(false);
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarCamposAdicionales = function(opts, success, response) 
 {
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
			var jsonData=Ext.util.JSON.decode(response.responseText);
			var cm = grid.getColumnModel();
			var hayCamposAdicionales=Ext.util.JSON.decode(response.responseText).TOTALES;
			if(hayCamposAdicionales=='0'){
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='1'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.CAMPO1);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='2'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.CAMPO1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.CAMPO2);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='3'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.CAMPO1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.CAMPO2);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.CAMPO3);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='4'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.CAMPO1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.CAMPO2);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.CAMPO3);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.CAMPO4);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
			}
			if(hayCamposAdicionales=='5'){
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.CAMPO1);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.CAMPO2);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.CAMPO3);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.CAMPO4);	
				grid.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.CAMPO5);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
				grid.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
			}
	 } else {
		 
		 NE.util.mostrarConnError(response,opts);
   }
  }

	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		var botonPDF=Ext.getCmp('btnGenerarPDF');
		
		var botonCSV=Ext.getCmp('btnGenerarArchivo');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				
				grid.show();
			}
			columnas();
			var miEpo = Ext.getCmp('icEpo').getValue();
			 if(miEpo!=''){
				Ext.Ajax.request({
									url: '13consulta2Ext.data.jsp',
									params: {
											informacion: 'camposAdicionales',
											HicEpo: miEpo
										},
									callback: procesarCamposAdicionales
								});
			 
			 }
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			
			
			var el = grid.getGridEl();						
			if(store.getTotalCount() > 0) {
					Ext.Ajax.request({
						url: '13consulta2Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Totales'})
						, callback: procesaTotales
					});
					
				var jsonData = store.reader.jsonData;	
				var  hayCamposAdicionales = jsonData.hayCamposAdicionales;	
				var  clasificacionEpo = jsonData.clasificacionEpo;	
				el.unmask();
				botonPDF.enable();
				botonCSV.enable();
				
			} else {
			
				botonPDF.disable();
				botonCSV.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
//--------------------Stores--------------------------

			var gridTotalAplicaData = new Ext.data.JsonStore({
		fields:	[{name: 'TOTAL'},{name: 'TOTAL_IMPORTE_APLICADO',type: 'float'},{name: 'TOTAL_SALDO_CREDITO',type: 'float'},{name: 'TOTAL_IMPORTE_DEPOSITAR',type: 'float'}],
		data:		[{'TOTAL':'MONEDA NACIONAL','TOTAL_IMPORTE_APLICADO':0,'TOTAL_SALDO_CREDITO':0,'TOTAL_IMPORTE_DEPOSITAR':0},
						{'TOTAL':'DOLAR AMERICANOS','TOTAL_IMPORTE_APLICADO':0,'TOTAL_SALDO_CREDITO':0,'TOTAL_IMPORTE_DEPOSITAR':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}		
	});
	
	
		var gridAplicaData = new Ext.data.JsonStore({
		fields:	[{name: 'IC_MONEDA'},{name: 'NOMBRE_IF'},{name: 'IC_NOMBRE'},{name: 'DF_FECHA_SELECCION',type: 'date', dateFormat: 'd/m/Y'},{name: 'DF_OPERACION',type: 'date', dateFormat: 'd/m/Y'},{name: 'IC_MENSUALIDAD'},{name: 'NUMERO_CREDITO'},{name: 'CG_TIPO_CONTRATO'},{name: 'IG_NUMERO_PRESTAMO',type: 'float'},{name: 'IMPORTE_APLICADO_CREDITO',	type: 'float'},{name: 'SALDO_CREDITO',	type: 'float'},{name: 'ORDEN_APLICA',type: 'float'}],
		data:		[{'IC_MONEDA':'','NOMBRE_IF':'','IC_NOMBRE':'','DF_FECHA_SELECCION':'','DF_OPERACION':'','IC_MENSUALIDAD':'','NUMERO_CREDITO':'','CG_TIPO_CONTRATO':'','IG_NUMERO_PRESTAMO':0,'IMPORTE_APLICADO_CREDITO':0,'SALDO_CREDITO':0,'ORDEN_APLICA':0}],
		totalProperty: 'total',
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	
 	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta2Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
 var catalogoIF = new Ext.data.JsonStore
  ({
	   id: 'catologoIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'CatologoIF'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });

 var catalogoBanco = new Ext.data.JsonStore
  ({
	   id: 'catalogoBanco',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoBanco'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
 var catalogoEpo = new Ext.data.JsonStore
  ({
	   id: 'catalogoEpo',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEpo'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
   var catalogoFactoraje = new Ext.data.JsonStore
  ({
	   id: 'catalogoFactoraje',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoFactoraje'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
var consTotalDetalleData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta2Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotalDetalle'
		},
		fields: [
			{name: 'MONEDA'},	
			{name: 'IMPORTE_PYME'},	
			{name: 'IMPORTE_APLICADO'},	
			{name: 'SALDO_CREDITO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {							
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.								
				}
			}
		}
	});
 var catalogoMoneda = new Ext.data.JsonStore
  ({
	   id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoMoneda'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
 var catalogoEstatus = new Ext.data.JsonStore
  ({
	   id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta2Ext.data.jsp',
		baseParams: 
		{
		 informacion: 'catalogoEstatus'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });
  
var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta2Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
			fields: [
					{name: 'NOMBREEPO'},//
					{name: 'NOMBREPYME'},//
					{name: 'IG_NUMERO_DOCTO'},//
					{name: 'DF_FECHA_DOCTO'},//
					{name: 'DF_FECHA_VENC'},//
					{name: 'FSOLICITUDINI'},
					{name: 'MONEDA'},//
					{name: 'FN_MONTO'},//
					{name: 'FN_PORC_ANTICIPO'},//
					{name: 'FN_MONTO_DSCTO'},//
					{name: 'CD_DESCRIPCION'},//
					{name: 'IC_MONEDA'},//
					{name: 'IC_ESTATUS_DOCTO'},//
					{name: 'CAMPO1'},
					{name: 'CAMPO2'},
					{name: 'CAMPO3'},
					{name: 'CAMPO4'},
					{name: 'CAMPO5'},
					{name: 'IC_DOCUMENTO'},//
					{name: 'CS_CAMBIO_IMPORTE'},//
					{name: 'NOMBREIF'},//
					{name: 'BENEFICIARIO'},//
					{name: 'FN_PORC_BENEFICIARIO'},//
					{name: 'RECIBIR_BENEF'},//
					{name: 'DF_ALTA'},//
					{name: 'DUPLICADO'},//
					{name: 'DF_ENTREGA'},//
					{name: 'CC_ACUSE'},//
					{name: 'ICEPO'},//
					{name: 'CG_TIPO_COMPRA'},//
					{name: 'CG_CLAVE_PRESUPUESTARIA'},//
					{name: 'CG_PERIODO'},//
					{name: 'TIPO_FACTORAJE'},//
					{name: 'NOMBREMANDANTE'},//
					{name: 'DF_FECHA_SOLICITUD'},
					{name: 'NETO_REC_PYME'},
					{name: 'PORCDOCTOAPLICADO'},
					{name: 'FN_MONTO_PAGO'},
					{name: 'FN_REMANENTE'},
					{name: 'IN_IMPORTE_DEPOSITAR_PYME'},
					{name: 'IG_PLAZO'},
					{name: 'NUMEROSIAFF'},
					{name: 'FN_MONTO_DOC'},
					{name: 'DBL_RECURSO'},
					{name: 'IN_TASA_ACEPTADA'},
					{name: 'IN_IMPORTE_RECIBIR'},
					{name: 'IN_IMPORTE_INTERES'},
					{name: 'IG_TIPO_PISO'},
					{name: 'FIDEICOMISO'},
					{name: 'ACUSE_DOCTO'},
					{name: 'MUESTRA_VISOR'},
					{name: 'VALIDACION_JUR'},
					{name: 'CONTRATO'},
					{name: 'COPADE'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);						
					}
				}
			}
		});	
		
	var totalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'MONEDA'},
			{name: 'FN_MONTO'},
			{name: 'FN_MONTO_DSCTO'},
			{name: 'TIPODOCTO'},
			{name: 'CONSDOCTOSNAFINDE'},
			{name: 'CD_NOMBRE'}
			
		],
		autoLoad: false,	listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var gridModificaData = new Ext.data.JsonStore({
		fields:	[{name:'FECHA_CAMBIO',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'FN_MONTO_NUEVO',type: 'float'},
					{name: 'FN_MONTO_ANTERIOR',	type: 'float'},
					{name: 'FEC_VENC_NUEVA',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'FEC_VENC_ANTERIOR',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'CT_CAMBIO_MOTIVO'},
					{name: 'CC_ACUSE'}],
		data:		[{'CC_ACUSE':'','CT_CAMBIO_MOTIVO':'','FEC_VENC_ANTERIOR':'','FEC_VENC_NUEVA':'','FECHA_CAMBIO':'','FN_MONTO_ANTERIOR':0,'FN_MONTO_NUEVO':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	
var consultaDetalleData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta2Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarDetalle'
		},
		fields: [
			{name: 'PRODUCTO'},	
			{name: 'NOMBRE_IF'},
			{name: 'ORDER_APLICACION'},	
			{name: 'FECHA_APLICACION'},	
			{name: 'FECHA_OPERACION'},	
			{name: 'NUMERO_MENSUALIDAD'},	
			{name: 'NUMERO_PEDIDO'},	
			{name: 'TIPO_CONTRATO'},	
			{name: 'NUMERO_PRESTAMO'},	
			{name: 'IMPORTE_APLICADO'},	
			{name: 'SALDO_CREDITO'}					
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConDetalleData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConDetalleData(null, null, null);						
				}
			}
		}
	});	
			
		var gridAplica = new Ext.grid.GridPanel({
		store: gridAplicaData,
		columns: [
			{header: 'Producto',tooltip: 'Producto',	dataIndex: 'IC_NOMBRE',	width: 150,	align: 'left'},
			{header: 'IF',tooltip: 'IF',	dataIndex: 'NOMBRE_IF',	width: 150,	align: 'left'},
			{header: 'Orden de Aplicaci�n',tooltip: 'Orden de Aplicaci�n',	dataIndex: 'ORDEN_APLICA',	width: 100,	align: 'center'},
			{header: 'Fecha de Aplicaci�n',tooltip: 'Fecha de Aplicaci�n',	dataIndex: 'DF_FECHA_SELECCION',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Fecha de Operaci�n Cr�dito',tooltip: 'Fecha de Operaci�n Cr�dito',	dataIndex: 'DF_OPERACION',sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'N�mero de Mensualidad',tooltip: 'N�mero de Mensualidad',	dataIndex: 'IC_MENSUALIDAD',	width: 110,	align: 'center'},
			{header: 'N�mero de Pedido / Disposici�n',tooltip: 'N�mero de Pedido/Disposici�n',	dataIndex: 'NUMERO_CREDITO',	width: 110,	align: 'center'},
			{header: 'Tipo de Contrato / Operaci�n',tooltip: 'Tipo de Contrato / Operaci�n',	dataIndex: 'CG_TIPO_CONTRATO',	width: 110,	align: 'center'},
			{header: 'N�mero de Pr�stamo NAFIN',tooltip: 'N�mero de Pr�stamo NAFIN',dataIndex: 'IG_NUMERO_PRESTAMO',	width: 110,	align: 'center'},
			{header: 'Importe Aplicado al Cr�dito',tooltip: 'Importe Aplicado al Cr�dito',	dataIndex: 'IMPORTE_APLICADO_CREDITO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Saldo del Cr�dito',tooltip: 'Saldo del Cr�dito',dataIndex: 'SALDO_CREDITO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 280,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});
	
	var gridDetalle = {
		xtype: 'editorgrid',
		store: consultaDetalleData,
		id: 'gridDetalle',		
		hidden: true,
		title: 'Detalle de los cr�ditos pagados con el documento ',
		columns: [		
			{
				header: 'Producto', 
				tooltip: 'Producto',
				dataIndex: 'PRODUCTO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'IF', 
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Orden de Aplicaci�n', 
				tooltip: 'Orden de Aplicaci�n',
				dataIndex: 'ORDER_APLICACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Fecha de Aplicaci�n', 
				tooltip: 'Fecha de Aplicaci�n',
				dataIndex: 'FECHA_APLICACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Fecha de operaci�n cr�dito', 
				tooltip: 'Fecha de operaci�n cr�dito',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'N�mero de mensualidad', 
				tooltip: 'N�mero de mensualidad',
				dataIndex: 'NUMERO_MENSUALIDAD',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'N�mero de Pedido / Disposici�n', 
				tooltip: 'N�mero de Pedido / Disposici�n',
				dataIndex: 'NUMERO_PEDIDO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Tipo de Contrato', 
				tooltip: 'Tipo de Contrato',
				dataIndex: 'TIPO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'N�mero de Pr�stamo NAFIN', 
				tooltip: 'N�mero de Pr�stamo NAFIN',
				dataIndex: 'NUMERO_PRESTAMO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Importe Aplicado a Cr�dito', 
				tooltip: 'Importe Aplicado a Cr�dito',
				dataIndex: 'IMPORTE_APLICADO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Saldo Cr�dito', 
				tooltip: 'Saldo Cr�dito',
				dataIndex: 'SALDO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 150,
		width: 800
	}
	
var gridTotalesDetalle = {
		xtype: 'editorgrid',
		store: consTotalDetalleData,
		id: 'gridTotalesDetalle',		
		hidden: true,		
		columns: [		
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Importe a depositar de la PYME', 
				tooltip: 'Importe a depositar de la PYME',
				dataIndex: 'IMPORTE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Importe Aplicado a Cr�dito', 
				tooltip: 'Total Importe Aplicado a Cr�dito',
				dataIndex: 'IMPORTE_APLICADO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Saldo del Cr�dito', 
				tooltip: 'Total Saldo del Cr�dito',
				dataIndex: 'SALDO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		height: 120,
		width: 800
	}
//--------------------Formas------------------------
	var elementosModifica = [
		{
			xtype: 'container',
			layout:'column',
				items:[{
					xtype: 'panel',id:	'izqModifica',
					width:	200,
					//layout: 'form',
					border:false,
					//bodyStyle: 'padding: 2px',
					//items: [{html: ' '}]
					items: [{xtype: 'displayfield', value:''}]
				},{
					xtype: 'panel',	id:	'centroModifica',
					//title:	'Cifras de Control',
					width:	400,
					layout: 'form',
					bodyStyle: 'padding: 2px',
					border: false,
					items: [
						{
							xtype: 'displayfield',	id: 'disProv',		fieldLabel: 'Proveedor',				value: ''
						},{
							xtype: 'displayfield',	id: 'disIfM',		fieldLabel: 'Intermediario Financiero',value: ''
						},{
							xtype: 'displayfield',	id: 'disDoctoM',	fieldLabel: 'N�mero de Documento',	value: ''
						},{
							xtype: 'displayfield',	id: 'disFecEmiM',	fieldLabel: 'Fecha de Emisi�n',		value: ''
						},{
							xtype: 'displayfield',	id: 'disFecVencM',fieldLabel: 'Fecha de Vencimiento',	value: ''
						},{
							xtype: 'displayfield',	id: 'disMonedaM',	fieldLabel: 'Moneda',					value: ''
						},{
							xtype: 'displayfield',	id: 'disMontoM',	fieldLabel: 'Monto',						value: ''
						},{
							xtype: 'displayfield',	id: 'disEstatusM',fieldLabel: 'Estatus',					value: ''
						}
					]
			},{
					xtype: 'panel',id:	'derModifica',
					width:	180,
					//layout: 'form',
					border:false,
					items: []
			}]
		}
	];

	var elementosForma = [{
		layout:           'column',
		id:               'panelleft',
		items:[{
			//columnWidth:   .5,
			width:         '100%',
			layout:        'form',
			items: [{
				xtype:         'combo',
				id:            'banco',
				name:          'Hbanco',
				hiddenName:    'Hbanco',
				fieldLabel:    'Banco de Fondeo',
				emptyText:     'Seleccionar...',
				displayField:  'descripcion',
				valueField:    'clave',
				triggerAction: 'all',
				mode:          'local',
				anchor:        '5%',
				editable:      false,
				typeAhead:     true,
				hidden:        true,
				store:         catalogoBanco,
				listeners: {
					select: {
						fn: function(combo) {
							var banco = combo.getValue();
							var iEpo = Ext.getCmp('icEpo');
							iEpo.setValue('');
							iEpo.store.removeAll();
								catalogoEpo.load({
								params: {
									Hbanco: combo.getValue()
								}
							});
						}
					}
				}
			},{
				xtype:          'combo',
				id:             'icEpo',
				name:           'HicEpo',
				hiddenName:     'HicEpo',
				fieldLabel:     'Nombre de la EPO',
				emptyText:      'Seleccionar una EPO...',
				displayField:   'descripcion',
				valueField:     'clave',
				triggerAction:  'all',
				mode:           'local',
				anchor:         '95%',
				typeAhead:      true,
				forceSelection: true,
				//hidden:         false,
				store:          catalogoEpo,
				tpl:            NE.util.templateMensajeCargaCombo,
				listeners:{
					blur:{
						fn:function(combo){
							if(combo.getValue()!=''){
								Ext.getCmp('_txt_nafelec').setDisabled(false);
							} else{
								Ext.getCmp('_txt_nafelec').setDisabled(true);
							}
						}	
					},
					select: {
						fn: function(combo) {
							catalogoIF.load({
								params: {
									HicEpo: combo.getValue()
								}
							});
							catalogoFactoraje.load({
								params: {
									HicEpo: combo.getValue()
								}
							});
						}
					}
				}
			}]
		}]
	},{
		layout:                    'column',
		items:[{
			columnWidth:            .5,
			id:                     'panelIzq',
			width:                  '50%',
			layout:                 'form',
			items: [{
				xtype:               'hidden',
				id:                  'hid_nombre',
				value:               ''
			},{
				xtype:               'hidden',
				id:                  'hid_ic_pyme',
				value:               ''
			},{
				xtype:               'textfield',
				fieldLabel:          'N@E',
				id:                  '_txt_nafelec',
				name:                'txt_nafelec',
				msgTarget:           'side',
				anchor:              '90%',
				allowBlank:          true,
				disabled:            true,
				hidden:              false,
				maskRe:              /[0-9]/,
				maxLength:           25,
				listeners: {
					'change': function(field){
						if(!Ext.isEmpty(field.getValue())){
							var respuesta = new Object();
							respuesta["noNafinElec"] = field.getValue();
								accionConsulta("OBTENER_NOMBRE_PYME",respuesta);
						}
					}
				}
			},{
				xtype:               'textfield',
				name:                'txt_nombre',
				fieldLabel:          'Nombre de proveedor',
				id:                  '_txt_nombre',
				anchor:              '90%',
				disabledClass:       "x2", //Para cambiar el estilo de la clase deshabilitada
				disabled:            true,
				allowBlank:          true,
				maxLength:           100
			},{
				xtype:               'compositefield',
				combineErrors:       false,
				msgTarget:           'side',
				items: [{
					xtype:            'displayfield',
					id:               'disEspacio',
					text:             ''
				},{
					xtype:            'button',
					id:               'btnBuscaA',
					iconCls:          'icoBuscar',
					text:             'B�squeda Avanzada',
//-----------------------------------------------------------
					handler: function(boton, evento){
						if(Ext.getCmp('icEpo').getValue()==''){
							Ext.MessageBox.alert('Mensaje...','Debe de Seleccionar una EPO.');
							return;
						}
						var winVen = Ext.getCmp('winBuscaA');
						if (winVen){
							Ext.getCmp('fpWinBusca').getForm().reset();
							Ext.getCmp('fpWinBuscaB').getForm().reset();
							Ext.getCmp('cmb_num_ne').setValue();
							Ext.getCmp('cmb_num_ne').store.removeAll();
							Ext.getCmp('cmb_num_ne').reset();
							winVen.show();
						} else{
							var winBuscaA = new Ext.Window ({
								id:              'winBuscaA',
								height:          320,
								x:               300,
								y:               100,
								width:           550,
								heigth:          100,
								modal:           true,
								closeAction:     'hide',
								title:           'B�squeda Avanzada',
								items:[{
									xtype:        'form',
									id:           'fpWinBusca',
									frame:        true,
									border:       false,
									style:        'margin: 0 auto',
									bodyStyle:    'padding:10px',
									defaults: {
										msgTarget: 'side',
										anchor:    '-20'
									},
									labelWidth:   130,
									items:[{
										xtype:     'displayfield',
										frame:     true,
										border:    false,
										value:     'Utilice el * para b�squeda gen�rica'
									},{
										xtype:     'displayfield',
										frame:     true,
										border:    false,
										value:     ''
									},{
										xtype:     'textfield',
										name:      'nombre_pyme',
										id:        'txtNombre',
										fieldLabel:'Nombre',
										maxLength: 100
									},{
										xtype:     'textfield',
										name:      'rfc_pyme',
										id:        'txtRfc',
										fieldLabel:'RFC',
										maxLength: 20
									},{
										xtype:     'textfield',
										name:      'num_pyme',
										id:        'txtNe',
										fieldLabel:'N�mero Proveedor',
										maxLength: 25
									}],
									buttonAlign:  'center',
									buttons:[{
										text:      'Buscar',
										iconCls:   'icoBuscar',
										handler:   function(boton){
											catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues(),{HicEpo:Ext.getCmp('icEpo').getValue()}) });
										}
									},{
										text:      'Cancelar',
										iconCls:   'icoRechazar',
										handler:   function(){
											Ext.getCmp('winBuscaA').hide();
										}
									}]
								},{
									xtype:        'form',
									frame:        true,
									id:           'fpWinBuscaB',
									style:        'margin: 0 auto',
									bodyStyle:    'padding:10px',
									monitorValid: true,
									defaults: {
										msgTarget: 'side',
										anchor:    '-20'
									},
									labelWidth:   80,
									items:[{
										xtype:     'combo',
										id:        'cmb_num_ne',
										name:      'ic_pyme',
										hiddenName:'ic_pyme',
										fieldLabel:'Nombre',
										emptyText: 'Seleccione Proveedor. . .',
										displayField:'descripcion',
										valueField:'clave',
										triggerAction:'all',
										forceSelection:true,
										allowBlank:false,
										typeAhead: true,
										mode:      'local',
										minChars:  1,
										store:     catalogoNombreProvData,
										tpl:       NE.util.templateMensajeCargaCombo
									}],
									buttonAlign:  'center',
									buttons:[{
										text:      'Aceptar',
										iconCls:   'aceptar',
										formBind:  true,
										handler:   function(){
											if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
												var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
												var cveP = disp.substr(0,disp.indexOf(" "));
												var desc = disp.slice(disp.indexOf(" ")+1);
												Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('cmb_num_ne').getValue())
												Ext.getCmp('_txt_nafelec').setValue(cveP);
												Ext.getCmp('_txt_nombre').setValue(desc);
												Ext.getCmp('winBuscaA').hide();
											}
										}
									},{
										text:      'Cancelar',
										iconCls:   'icoRechazar',
										handler:   function() {
											Ext.getCmp('winBuscaA').hide();
										}
									}]
								}]
							})
							.show();
						}
					}
//-----------------------------------------------------------
				}]
			},{
				xtype:               'combo',
				anchor:              '90%',
				emptyText:           'Seleccionar un IF...',
				fieldLabel:          'Nombre del IF',
				displayField:        'descripcion',
				valueField:          'clave',
				triggerAction:       'all',
				typeAhead:           true,
				minChars:            1,
				store:               catalogoIF,
				tpl:                 NE.util.templateMensajeCargaCombo,
				name:                'Hif',
				id:                  'if',
				mode:                'local',
				hiddenName:          'Hif',
				forceSelection:      true
			},{
				xtype:               'textfield',
				name:                'documento',
				id:                  'documento',
				allowBlank:          true,
				maxLength:           16,
				anchor:              '90%',
				msgTarget:           'side',
				fieldLabel:          'N�mero de Documento'
			},{
				xtype:               'compositefield',
				fieldLabel:          'Fecha de Emisi�n',
				combineErrors:       false,
				msgTarget:           'side',
				items: [{
					xtype:            'datefield',
					name:             'fechaEm1',
					id:               'fechaEm1',
					allowBlank:       true,
					startDay:         0,
					minValue:         '01/01/1901',
					width:            115,
					msgTarget:        'side',
					vtype:            'rangofecha', 
					campoFinFecha:    'fechaEm2'
				},{
					xtype:            'displayfield',
					value:            '&nbsp;&nbsp; al',
					width:            35
				},{
					xtype:            'datefield',
					name:             'fechaEm2',
					id:               'fechaEm2',
					minValue:         '01/01/1901',
					allowBlank:       true,
					startDay:         1,
					width:            115,
					msgTarget:        'side',
					vtype:            'rangofecha',
					campoInicioFecha: 'fechaEm1'
				}]
			},{
				xtype:               'combo',
				name:                'Hfactoraje',
				id:                  'factoraje',
				fieldLabel:          'Tipo de factoraje',
				emptyText:           'Seleccionar...',
				mode:                'local',
				anchor:              '90%',
				displayField:        'descripcion',
				valueField:          'clave',
				hiddenName:          'Hfactoraje',
				forceSelection:      true,
				triggerAction:       'all',
				typeAhead:           true,
				minChars:            1,
				store:               catalogoFactoraje,
				tpl:                 NE.util.templateMensajeCargaCombo
			},{
				xtype:               'radiogroup',
				fieldLabel:          '<br/>Informaci�n procesada en',
				id:                  'radioGp',
				columns:             1,
				hidden:              false,
				items:[{
					boxLabel:         'Operaci�n Telefonica',
					id:               'telefonica',
					name:             'stat',
					inputValue:       '8',
					checked:          false,
					listeners:{
						check: function(radio){
							if (radio.checked){
								Ext.getCmp('ninguna').show();
								catalogoEstatus.load({
									params:{stat:'8'}
								});
							}
						}
					}
				},{
					boxLabel:         'Cobranza Manual',
					id:               'cobranza',
					name:             'stat',
					inputValue:       '9',
					checked:          false,
					listeners: {
						check: function(radio){
							if (radio.checked){
								Ext.getCmp('ninguna').show();
								catalogoEstatus.load({
									params:{stat:'9'}
								});
							}
						}
					}
				},{
					boxLabel:         'Descuento Autom�tico..',
					id:               'descuento',
					name:             'stat',
					inputValue:       '5',
					checked:          false,
					listeners: {
						check: function(radio){
							if (radio.checked){
								Ext.getCmp('ninguna').show();
								catalogoEstatus.load({
									params:{stat:'5'}
								});
							}
						}
					}
				},{
					boxLabel:         'Operaci�n Fideicomiso',
					id:               'fide',
					name:             'stat',
					hidden:           false,
					inputValue:       'S',
					checked:          false,
					listeners: {
						check: function(radio){
							if (radio.checked){
								Ext.getCmp('ninguna').show();
								catalogoEstatus.load({
									params:{stat:'S'}
								});
							}
						}
					}
				},{
					boxLabel:         'Ninguna de las anteriores',
					id:               'ninguna',
					name:             'stat',
					hidden:           false,
					inputValue:       '0',
					checked:          false,
					listeners: {
						check: function(radio){
							if (radio.checked){
								radio.hide();
								catalogoEstatus.load({
									params:{stat:'0'}
								});
							}
						}
					}
				}]
			}]
		},{
			columnWidth:            .5,
			width:                  '50%',
			id:                     'panelDer',
			layout:                 'form',
			items: [{
				xtype:               'compositefield',
				fieldLabel:          'Fecha de vencimiento',
				msgTarget:           'side',
				combineErrors:       false,
				items: [{
					xtype:            'datefield',
					name:             'fechaVenc1',
					id:               'fechaVenc1',
					allowBlank:       true,
					startDay:         0,
					minValue:         '01/01/1901',
					width:            115,
					msgTarget:        'side',
					vtype:            'rangofecha', 
					campoFinFecha:    'fechaVenc2'
				},{
					xtype:            'displayfield',
					value:            '&nbsp;&nbsp; al',
					width:            35
				},{
					xtype:            'datefield',
					name:             'fechaVenc2',
					id:               'fechaVenc2',
					minValue:         '01/01/1901',
					allowBlank:       true,
					startDay:         1,
					width:            115,
					msgTarget:        'side',
					vtype:            'rangofecha',
					campoInicioFecha: 'fechaVenc1'
				}]
			},{
				xtype:               'combo',
				fieldLabel:          'Moneda',
				displayField:        'descripcion',
				valueField:          'clave',
				triggerAction:       'all',
				typeAhead:           true,
				minChars:            1,
				name:                'cbMoneda',
				id:                  'cbMoneda',
				mode:                'local',
				forceSelection:      true,
				hiddenName:          'HcbMoneda',
				hidden:              false,
				emptyText:           'Seleccionar Moneda...',
				store:               catalogoMoneda,
				tpl:                 NE.util.templateMensajeCargaCombo,
				anchor:              '90%'
			},{
				xtype:               'compositefield',
				fieldLabel:          'Monto',
				msgTarget:           'side',
				combineErrors:       false,
				items: [{
					xtype:            'numberfield',
					name:             'monto1',
					id:               'monto1',
					allowBlank:       true,
					maxLength:        9,
					width:            115,
					msgTarget:        'side',
					vtype:            'rangoValor',
					campoFinValor:    'monto2'
				},{
					xtype:            'displayfield',
					value:            '&nbsp;&nbsp; a',
					width:            35
				},{
					xtype:            'numberfield',
					name:             'monto2',
					id:               'monto2',
					allowBlank:       true,
					maxLength:        9,
					width:            115,
					msgTarget:        'side',
					vtype:            'rangoValor',
					campoInicioValor: 'monto1'
				}]
			},{
				xtype:               'combo',
				name:                'Hestatus',
				id:                  'estatus',
				fieldLabel:          'Estatus',
				emptyText:           'Seleccionar Estatus...',
				mode:                'local',
				anchor:              '90%',
				displayField:        'descripcion',
				valueField:          'clave',
				hiddenName:          'Hestatus',
				forceSelection:      true,
				triggerAction:       'all',
				typeAhead:           true,
				minChars:            1,
				store:               catalogoEstatus,
				tpl:                 NE.util.templateMensajeCargaCombo
			},{
				xtype:               'compositefield',
				fieldLabel:          'Fecha Aut. IF',
				combineErrors:       false,
				msgTarget:           'side',
				items: [{
					xtype:            'datefield',
					name:             'fechaIF1',
					id:               'fechaIF1',
					allowBlank:       true,
					startDay:         0,
					minValue:         '01/01/1901',
					width:            115,
					msgTarget:        'side',
					vtype:            'rangofecha', 
					campoFinFecha:    'fechaIF2'
				},{
					xtype:            'displayfield',
					value:            '&nbsp;&nbsp; al',
					width:            35
				},{
					xtype:            'datefield',
					name:             'fechaIF2',
					id:               'fechaIF2',
					allowBlank:       true,
					startDay:         1,
					minValue:         '01/01/1901',
					width:            115,
					msgTarget:        'side',
					vtype:            'rangofecha',
					campoInicioFecha: 'fechaIF1'
				}]
			},{
				xtype:               'compositefield',
				fieldLabel:          'Tasa',
				msgTarget:           'side',
				combineErrors:       false, 
				items: [{
					xtype:            'numberfield',
					name:             'tasa1',
					id:               'tasa1',
					allowBlank:       true,
					maxLength:        9,
					width:            115,
					msgTarget:        'side',
					vtype:            'rangoValor',
					campoFinValor:    'tasa2'
				},{
					xtype:            'displayfield',
					value:            '&nbsp;&nbsp; a',
					width:            35
				},{
					xtype:            'numberfield',
					name:             'tasa2',
					id:               'tasa2',
					allowBlank:       true,
					maxLength:        9,
					width:            115,
					msgTarget:        'side',
					vtype:            'rangoValor',
					campoInicioValor: 'tasa1'
				}]
			},{
				xtype:               'textfield',
				name:                'digito',
				id:                  'digito',
				allowBlank:          true,
				anchor:              '90%',
				fieldLabel:          'D�gito Identificador',
				maxLength:           15,
				minLength:           15,
				msgTarget:           'side'
			},{
				xtype:               'combo',
				name:                'duplicado',
				id:                  'duplicado',
				fieldLabel:          'Notificado como duplicado',
				emptyText:           'Seleccione...',
				anchor:              '90%',
				msgTarget:           'side',
				mode:                'local',
				triggerAction:       'all',
				forceSelection:      true,
				typeAhead:           true,
				allowBlank:          true,
				store:               [['S','Si'], ['N','No']]
			},			
			{
			    xtype: 'combo',
			    id	: 'validaDoctoEPO',
			    name: 'validaDoctoEPO',
			    hiddenName	:'validaDoctoEPO', 
			    fieldLabel: 'Validaci�n de Documento EPO',
			    forceSelection:      true,
			    typeAhead:           true,
			    allowBlank:          true,
			    triggerAction: 'all',
			    mode: 'local',
			    anchor:   '90%',
			    valueField	: 'clave',
			    displayField: 'descripcion',
			    store:               [['S','Si'], ['N','No']]										
			},
			{
				xtype:               'textfield',
				name:                'copade',
				id:                  'copade',
				allowBlank:          true,
				anchor:              '90%',
				fieldLabel:          'COPADE',
				maxLength:           10,				
				msgTarget:           'side'
			},
			{
				xtype:               'textfield',
				name:                'contrato',
				id:                  'contrato',
				allowBlank:          true,
				anchor:              '90%',
				fieldLabel:          'Contrato',
				maxLength:           10,			
				msgTarget:           'side'
			}
			
			]
		}]
	},{
		xtype:                     'compositefield',
		msgTarget:                 'side',
		hidden:                    true,
		combineErrors:             false,
		items: [{
			xtype:                  'numberfield',
			name:                   'oIF',
			id:                     'oIF',
			allowBlank:             true,
			maxLength:              1,
			maxValue:               4,
			minValue:               1,
			width:                  40,
			msgTarget:              'side'
		},{
			xtype:                  'displayfield',
			value:                  'I.F.',
			width:                  50
		},{
			xtype:                  'numberfield',
			name:                   'oPYME',
			id:                     'oPYME',
			allowBlank:             true,
			maxValue:               4,
			minValue:               1,
			maxLength:              1,
			width:                  40,
			msgTarget:              'side'
		},{
			xtype:                  'displayfield',
			value:                  'PYME',
			width:                  15
		}]
	},{
		xtype:                     'compositefield',
		msgTarget:                 'side',
		hidden:                    true,
		combineErrors:             false,
		items: [{
			xtype:                  'numberfield',
			name:                   'oEpo',
			id:                     'oEpo',
			allowBlank:             true,
			maxLength:              1,
			maxValue:               4,
			minValue:               1,
			width:                  40,
			msgTarget:              'side'
		},{
			xtype:                  'displayfield',
			value:                  'EPO',
			width:                  50
		},{
			xtype:                  'numberfield',
			name:                   'oFecha',
			id:                     'oFecha',
			allowBlank:             true,
			maxValue:               4,
			minValue:               1,
			maxLength:              1,
			width:                  40,
			msgTarget:              'side'
		},{
			xtype:                  'displayfield',
			value:                  'Fecha de Vencimiento',
			width:                  15
		}]
	}	
	
	];


var gridModifica = new Ext.grid.GridPanel({
		id: 'gridModifica',
		store: gridModificaData,
		columns: [
			{header: 'Fecha Movimiento',tooltip: 'Fecha Movimiento',	dataIndex: 'FECHA_CAMBIO',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Monto',tooltip: 'Monto',	dataIndex: 'FN_MONTO_NUEVO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Anterior',tooltip: 'Monto Anterior',	dataIndex: 'FN_MONTO_ANTERIOR',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Fecha de Vencimiento',tooltip: 'Fecha de Vencimiento',	dataIndex: 'FEC_VENC_NUEVA',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Fecha de Vencimiento Anterior',tooltip: 'Fecha de Vencimiento Anterior',	dataIndex: 'FEC_VENC_ANTERIOR',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Causa',tooltip: 'Causa',	dataIndex: 'CT_CAMBIO_MOTIVO',	width: 200,	align: 'left'},
			{header: 'Acuse',tooltip: 'Acuse',	dataIndex: 'CC_ACUSE',	width: 150,	align: 'left'}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 200,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	dataIndex: 'NOMBREEPO',
				sortable: true, width: 180, resizable: true, hidden: false,align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header: 'PYME', tooltip: 'PYME',	dataIndex: 'NOMBREPYME',
				sortable: true, width: 180, resizable: true, hidden: false,align: 'center',align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header : 'IF', tooltip: 'IF',	dataIndex : 'NOMBREIF',
				sortable : true, width : 180, align: 'left',  hidden: false,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header : 'Fideicomiso', tooltip: 'Fideicomiso',	dataIndex : 'FIDEICOMISO',
				sortable : true, width : 180, align: 'left',  hidden: false,
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			
			},{
				header : 'Num. Documento', tooltip: 'Num. Documento',	dataIndex : 'IG_NUMERO_DOCTO',
				sortable : true, width : 110, align: 'center', hidden: false
			},{
				xtype:	'actioncolumn',
				header : 'N�mero de Acuse', tooltip: 'N�mero de Acuse',	dataIndex : 'CC_ACUSE',
				width:	150,	align: 'center', hidden: false, hideable: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return (record.get('CC_ACUSE'));
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CC_ACUSE') != '')	{
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else	{
								return value;
							}
						},
						handler:	muestraPanelAcuse
					}
				]
			},{
				xtype: 'actioncolumn',
				header: 'No. Acuse carga', 
				tooltip: 'No. Acuse carga', 
				dataIndex: 'ACUSE_DOCTO',
				width: 150,
				align: 'center', 
				hidden: false, 
				hideable: false,
				renderer: function(value, metadata, record, rowindex, colindex, store){
					return (record.get('ACUSE_DOCTO'));
				},
				items: [{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store){
						if(registro.get('ACUSE_DOCTO') != '' && registro.get('MUESTRA_VISOR') == 'S'){
							this.items[0].tooltip = 'Ver n�mero del acuse de publicaci�n ';
							return 'iconoLupa';
						} else{
							return value;
						}
					},
					handler: function(grid, rowIndex, colIndex){
						var rec = consultaData.getAt(rowIndex);
						VisorAcusesDeCarga(rec);
					}
				}]
			},{
				header : 'Fecha Emisi�n', tooltip: 'Fecha Emisi�n',	dataIndex : 'DF_FECHA_DOCTO',
				sortable : true, width : 80, hidden: false,align: 'center'
			},{
				header : 'Fecha Vencimiento', tooltip: 'Fecha Vencimiento',	dataIndex : 'DF_FECHA_VENC',
				sortable : true, width : 80, hidden: false, hideable: false,align: 'center'
			},{
				header : 'Plazo', tooltip: 'Plazo',	dataIndex : 'IG_PLAZO',
				sortable : true, width : 100, align: 'center', hidden: false, hideable: false
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'MONEDA',
				sortable : true, width : 180, hidden: false,hideable: false, align: 'center'
			},{
				header: 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	dataIndex: 'TIPO_FACTORAJE',
				sortable: true,	width: 180,	resizable: true, hidden: false, hideable: false,align: 'center'
			},{
				header: 'Monto', tooltip: 'Monto',	dataIndex: 'FN_MONTO',
				sortable: true,	width: 100,	resizable: true, hidden: false, hideable: false,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Monto Documento', tooltip: 'Monto Documento',	dataIndex: 'FN_MONTO_DOC',
				sortable: true,	width: 100,	resizable: true, hidden: false, hideable: false,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Porcentaje de Descuento', tooltip: 'Porcentaje de Descuento',	dataIndex : 'FN_PORC_ANTICIPO',
				width : 100, sortable : true, hidden: true,hideable: false,align: 'center',renderer: Ext.util.Format.numberRenderer('0.00%')
			},{
				header : 'Recurso en Garant�a', tooltip: 'Recurso en Garant�a',	dataIndex : 'DBL_RECURSO',
				width : 100, sortable : true, resizable: true, hidden: false,hideable: false,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'FN_MONTO_DSCTO',
				width : 100, sortable : true, resizable: true, hidden: false,hideable: false,align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									
										return Ext.util.Format.number(value, '$0,0.00');
									
								}
			},{
				header : 'Intereses del documento', tooltip: 'Intereses del documento',	dataIndex : 'IN_IMPORTE_INTERES',
				width : 100, sortable : true, resizable: true, hidden: false,hideable: false,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Monto a Recibir', tooltip: 'Monto a Recibir',	dataIndex : 'IN_IMPORTE_RECIBIR',
				width : 100, sortable : true, resizable: true, hidden: false,hideable: false,align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									
										return Ext.util.Format.number(value, '$0,0.00');
									
								}
			},{
				header : 'Tasa', tooltip: 'Tasa',	dataIndex : 'IN_TASA_ACEPTADA',
				width : 100, sortable : true, resizable: true, hidden: false,hideable: false,align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									
										return Ext.util.Format.number(value, '0.0000%');
									
								}
			},{
				header: 'Estatus', tooltip: 'Estatus',	dataIndex: 'CD_DESCRIPCION',
				sortable: true,	width: 150,	align: 'center', resizable: true, hidden: false,hideable: false
			},{
				xtype:	'actioncolumn',
				header : 'Modificaci�n Montos y/o Fechas', tooltip: 'Modificaci�n Montos y/o Fechas',	dataIndex : 'MOD_MONTOS_COL',
				width:	130,	align: 'center', resizable: true, hidden: false,hideable: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('CS_CAMBIO_IMPORTE')	==	'S') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else{return '';}
							
						}
						,handler:	muestraPanelModifica
					}
				]
			},{
				header: 'Importe aplicado a cr�dito', tooltip: 'Importe aplicado a cr�dito',	dataIndex: 'FN_MONTO_PAGO',
				sortable: true,	width: 150,	align: 'right', resizable: true, hidden: false,hideable: false,renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header: 'Porcentaje del documento aplicado', tooltip: 'Porcentaje del documento aplicado',	dataIndex: 'PORCDOCTOAPLICADO',
				sortable: true, width: 100, resizable: true, hideable: false, align: 'center',renderer: Ext.util.Format.numberRenderer('0.00%')
			},{
				header: 'Importe a depositar PyME', tooltip: 'Importe a depositar PyME',	dataIndex: 'IN_IMPORTE_DEPOSITAR_PYME',
				sortable: true,	width: 120,	resizable: true, hideable : false, align: 'right' ,hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if(value=='0'||value==''){
										return '';
									}
										return Ext.util.Format.number(registro.get('IN_IMPORTE_DEPOSITAR_PYME'), '$0,0.00');
									
								}
			},{
				xtype:	'actioncolumn',
				header : 'Detalle de Aplicaci�n', tooltip: 'Detalle de Aplicaci�n',
				dataIndex : 'DETALLE',
				width:	110,	align: 'center',  resizable: true, hidden: true,hideable: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							
						},
						handler:	muestraDetalleAplicacion
					}
				]
			////////		aqui van las columnas dinamicas	//////////////
			},{
				header : 'Fecha Aut. IF', tooltip: 'Fecha Aut. IF',	dataIndex : 'DF_FECHA_SOLICITUD',
				sortable : true, width : 100, align: 'center', hidden: false
			},{
				header : 'Modificaci�n Montos', tooltip: 'Modificaci�n Montos',	dataIndex : 'MOD_MONTOS',
				sortable : true, width : 100, align: 'center', hidden: false
			},{
				header : 'Neto a Recibir PYME', tooltip: 'Neto a Recibir PYME',	dataIndex : 'NETO_REC_PYME',
				sortable : true, width : 100, align: 'right', hidden: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if(value=='0')
									return '';
										return Ext.util.Format.number(value, '$0,0.00');
									
								}
			},{
				header : 'Doctos Aplicados a Nota de Cr�dito', tooltip: 'Doctos Aplicados a Nota de Cr�dito',	dataIndex : 'DETALLES',
				sortable : true, width : 100, hidden: false, hideable: false,align: 'right', hidden: false
			},{
				header: 'Beneficiario', tooltip: 'Beneficiario',	dataIndex: 'BENEFICIARIO',
				sortable: true, width: 250, resizable: true, hidden: false,hideable: false,align: 'center'
			},{
				header : '% Beneficiario', tooltip: '% Beneficiario',	dataIndex : 'FN_PORC_BENEFICIARIO',
				sortable : true, width : 100, align: 'center', resizable: true,  hidden: false,hideable: false,renderer: Ext.util.Format.numberRenderer('0.00%')
			},{
				header: 'Importe a Recibir Beneficiario',
				tooltip: 'Importe a Recibir Beneficiario',
				width: 130,
				dataIndex: 'RECIBIR_BENEF',
				align: 'center',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if(value=='0')
									return '';
										return Ext.util.Format.number(value, '$0,0.00');
									
								}
			},{
				header : 'Fecha para Efectuar Descuento', tooltip: 'Fecha para Efectuar Descuento',	dataIndex : 'DF_PROGRAMACION',
				sortable : true, width : 100, align: 'center',hidden: false
			},{
				header : 'Fecha Alta Doc.', tooltip: 'Fecha Alta Doc.',	dataIndex : 'DF_ALTA',
				sortable : true, width : 100, align: 'center', hidden: false
			},{
				header : 'Notificado como Duplicado', tooltip: 'Notificado como Duplicado',	dataIndex : 'DUPLICADO',
				sortable : true, width : 155, align: 'center', hidden: false
			},{
				header : 'Digito Identificador', tooltip: 'Digito Identificador',	dataIndex : 'NUMEROSIAFF',
				sortable : true, width : 100, align: 'center', hidden: false
			},{
				header : 'Fecha de Recepci�n de Bienes y Servicios', tooltip: 'Fecha de Recepci�n de Bienes y Servicios',	dataIndex : 'DF_ENTREGA',
				sortable : true, width : 100, align: 'center', hidden: false
			},{
				header : 'Tipo de Compra (procedimiento)', tooltip: 'Tipo de Compra (procedimiento)',	dataIndex : 'CG_TIPO_COMPRA',
				sortable : true, width : 100, hidden: false, hideable: false,align: 'center'
			},{
				header: 'Clasificador por Objeto del Gasto', tooltip: 'Clasificador por Objeto del Gasto',	dataIndex: 'CG_CLAVE_PRESUPUESTARIA',
				sortable: true, width: 150, resizable: true, hidden: false,hideable: false,align: 'center'
			},{
				header : 'Plazo M�ximo', tooltip: 'Plazo M�ximo',	dataIndex : 'CG_PERIODO',
				sortable : true, width : 100, align: 'center', resizable: true, hidden: false,hideable: false
			},{
				header: 'Campo 1', tooltip: 'Campo 1',	dataIndex: 'CAMPO1',sortable: true,resizable: true	,hidden: true,
				width: 130,align: 'left'			
			},
			{
				header: 'Campo 2', tooltip: 'Campo 2',dataIndex: 'CAMPO2',hidden: true,
				sortable: true,resizable: true,width: 130,align: 'left'			
			},
			{
				header: 'Campo 3',tooltip: 'Campo 3',dataIndex: 'CAMPO3',hidden: true,
				sortable: true,resizable: true,width: 130,align: 'left'			
			},
			{
				header: 'Campo 4',tooltip: 'Campo 4',dataIndex: 'CAMPO4',hidden: true,
				sortable: true,resizable: true,width: 130,align: 'left'			
			},
			{
				header: 'Campo 5',tooltip: 'Campo 5',dataIndex: 'CAMPO5',hidden: true,
				sortable: true,resizable: true,width: 130,align: 'left'			
			},{
				header : 'Fecha Registro Operaci�n', tooltip: 'Fecha Registro Operaci�n',	dataIndex : 'FECHA_REGISTRO_OPERACION',
				sortable : true, width : 100, align: 'center', hidden: false
			},{
				header : 'Mandante', tooltip: 'Mandante',	dataIndex : 'NOMBREMANDANTE',
				sortable : true, width : 100, align: 'left', hidden: false
			},
			{
				header : 'Validaci�n de <br>Documentos EPO', tooltip: 'Validaci�n de Documentos EPO',	dataIndex : 'VALIDACION_JUR',
				sortable : true, width : 110, align: 'center', hidden: false
			},
			{
				header : 'Contrato', tooltip: 'Contrato',	dataIndex : 'CONTRATO',
				sortable : true, width : 100, align: 'center', hidden: false
			},
			{
				header : 'COPADE', tooltip: 'COPADE',	dataIndex : 'COPADE',
				sortable : true, width : 100, align: 'center'	, hidden: false					
			}	
		    ],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: 'No hay registros.',
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					tooltip: 'Imprime todos los registros en formato CSV.',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({							
							url: '13consulta2Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ArchivoCSV'
							}),
							callback: procesarArchivoSuccess
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Todo',
					id: 'btnGenerarPDF',
					tooltip: 'Imprime todos los registros en formato PDF.',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta2Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'GenerarPDF',
									start: 0,
									limit: 0,
									mandante: catalogoFactoraje.reader.jsonData.mandante
								}),
							callback: procesarArchivoSuccess
						});
					}
				}
/*
	Fodea 011-2015
	La opci�n de generar el PDF por paginaci�n queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el m�todo por si se requiere nuevamente 
	Fecha: 26/06/2015
	BY: Agust�n Bautista Ruiz
*/
/*
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '13consulta2Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'GenerarPDF',
									start: cmpBarraPaginacion.cursor,
									limit: cmpBarraPaginacion.pageSize,
									mandante: catalogoFactoraje.reader.jsonData.mandante
								}),
							callback: procesarArchivoSuccess
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
*/
			]
		}
	});
	
	var gridTotales =  new Ext.grid.GridPanel({
		xtype:'grid',	store:totalesData,	id:'gridTotales',		width:940,	height:150,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'TOTAL',	dataIndex: 'TIPODOCTO',	align: 'left',	width: 250, menuDisabled:true,renderer:
				function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('TIPODOCTO') =='NC'){
						value= 'NOTAS DE CREDITO';
					}else if(registro.get('TIPODOCTO') =='DN'){
						value = 'DOCUMENTOS NEGOCIABLES';
					}else{
						value='DOCUMENTOS';
						}
					return value;
				}
			},{
				header: 'Moneda',	dataIndex: 'CD_NOMBRE',	width: 150,	align: 'right',menuDisabled:true
			},{
				header: 'Total Monto',	dataIndex: 'FN_MONTO',	width: 150,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Valido',dataIndex: 'FN_MONTO_DSCTO',	width:250,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'N�mero de Documentos',dataIndex: 'CONSDOCTOSNAFINDE',	width:250,	align: 'center',menuDisabled:true
			}
		]
	});

	var gridAcuseData = new Ext.data.JsonStore({
		fields:	[{name: 'NOMBRE_IF'},{name: 'NOMBRE_EPO'},{name: 'IG_NUMERO_DOCTO'},{name: 'CD_NOMBRE'},{name: 'FN_MONTO',type: 'float'},{name: 'FN_PORC_ANTICIPO',type: 'float'},{name: 'FN_MONTO_DSCTO',type: 'float'},{name: 'IN_IMPORTE_INTERES',	type: 'float'},{name: 'IN_IMPORTE_RECIBIR',	type: 'float'}],
		data:		[{'NOMBRE_IF':'','NOMBRE_EPO':'','IG_NUMERO_DOCTO':'','CD_NOMBRE':'','FN_MONTO':0,'FN_PORC_ANTICIPO':0,'FN_MONTO_DSCTO':0,'IN_IMPORTE_INTERES':0,'IN_IMPORTE_RECIBIR':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridTotalAcuseData = new Ext.data.JsonStore({
		fields:	[{name: 'CD_NOMBRE'},{name: 'NOMBRE_EPO'},{name: 'TOTAL_REGISTROS',type: 'float'},{name: 'TOTAL_MONTO',type: 'float'},{name: 'TOTAL_MONTO_DSCTO',type: 'float'},{name: 'TOTAL_INTERES',type: 'float'},{name: 'TOTAL_RECIBIR',type: 'float'}],
		data:		[{'CD_NOMBRE':'','NOMBRE_EPO':'','TOTAL_REGISTROS':0,'TOTAL_MONTO':0,'TOTAL_MONTO_DSCTO':0,'TOTAL_INTERES':0,'TOTAL_RECIBIR':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
		var gridAcuse = new Ext.grid.GridPanel({
		store: gridAcuseData,
		columns: [
			{header: 'IF Seleccionado',tooltip: 'IF Seleccionado',	dataIndex: 'NOMBRE_IF',	width: 150,	align: 'left'},
			{header: 'EPO',tooltip: 'EPO',	dataIndex: 'NOMBRE_EPO',	width: 150,	align: 'left'},
			{header: 'N�mero de Documento',tooltip: 'N�mero de Documento',	dataIndex: 'IG_NUMERO_DOCTO',	width: 110,	align: 'left'},
			{header: 'Moneda',tooltip: 'Moneda',	dataIndex: 'CD_NOMBRE',	width: 130,	align: 'left'},
			{header: 'Monto Documento',tooltip: 'Monto Documento',	dataIndex: 'FN_MONTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Porcentaje de Descuento',tooltip: 'Porcentaje de Descuento',	dataIndex: 'FN_PORC_ANTICIPO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('0%')},
			{header: 'Monto a Descontar',tooltip: 'Monto a Descontar',	dataIndex: 'FN_MONTO_DSCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Intereses',tooltip: 'Monto Intereses',	dataIndex: 'IN_IMPORTE_INTERES',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto a Recibir',tooltip: 'Monto a Recibir',	dataIndex: 'IN_IMPORTE_RECIBIR',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 175,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

	var gridTotalAcuse = {
		xtype: 'grid',
		store: gridTotalAcuseData,
		id: 'gridTotalAcuse',
		hidden:	false,
		columns: [
			{header: 'TOTALES',dataIndex: 'CD_NOMBRE',align: 'left',	width: 125},
			{header: 'E P O  - DATOS', tooltip:'E P O  - DATOS', dataIndex: 'NOMBRE_EPO',align: 'left',	width: 150},
			{header: 'Total Monto Documento', tooltip:'Total Monto Documento',dataIndex: 'TOTAL_MONTO',width: 130,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Monto Descuento',tooltip:'Total Monto Descuento',dataIndex: 'TOTAL_MONTO_DSCTO',width: 130,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Monto de Inter�s',tooltip:'Total Monto de Inter�s',dataIndex: 'TOTAL_INTERES',width: 125,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Importe a Recibir',tooltip:'Total Importe a Recibir',dataIndex: 'TOTAL_RECIBIR',width: 125,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		width: 790,
		height: 78,
		//title: 'Totales',
		frame: false
	};
	var gridMontos =  new Ext.grid.GridPanel({
		xtype:'grid',	store:totalesData,	id:'gridMontos',		width:940,	height:150,	title:'Totales', hidden:true,	frame: false,
		columns: [
			{
				header: 'TOTAL',	dataIndex: 'DC_FECHA_CAMBIO',	align: 'left',	width: 250, menuDisabled:true
			},{
				header: 'Moneda',	dataIndex: 'FN_MONTO_NUEVO',	width: 150,	align: 'right',menuDisabled:true
			},{
				header: 'Total Monto',	dataIndex: 'FN_MONTO_ANTERIOR',	width: 150,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
				header: 'Monto Valido',dataIndex: 'CT_CAMBIO_MOTIVO',	width:250,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'),menuDisabled:true
			},{
			header: 'N�mero de Documentos',dataIndex: 'CC_ACUSE',	width:250,	align: 'right',menuDisabled:true
			},{
				header: 'Moneda',	dataIndex: 'DF_FECHA_VENC_ANTERIOR',	width: 150,	align: 'right',menuDisabled:true
			}
		]
	});
	

	
		var gridTotalAplica = {
		xtype: 'grid',
		store: gridTotalAplicaData,
		id: 'gridTotalAplica',
		hidden:	false,
		columns: [
			{header: 'Totales',tooltip:'Totales',dataIndex: 'TOTAL',align: 'left',	width: 200},
			{header: 'Total Importe Aplicado a Cr�dito',tooltip:'Total Importe Aplicado a Cr�dito',dataIndex: 'TOTAL_IMPORTE_APLICADO',width: 190,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Saldo del Cr�dito',tooltip:'Total Saldo del Cr�dito',dataIndex: 'TOTAL_SALDO_CREDITO',width: 190,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Importe a Depositar PYME',tooltip:'Total Importe a Depositar PYME',dataIndex: 'TOTAL_IMPORTE_DEPOSITAR',width: 190,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		width: 790,
		height: 100,
		//title: 'Totales',
		frame: false
	};
	
//--------

	var fpModifica=new Ext.FormPanel({
		autoHeight:true,
		//height:212,
		width: 790,
		border: true,
		labelWidth: 150,
		items: elementosModifica,
		hidden: false
	});
	var elementosAcuse = [
		{
			xtype: 'container',
			layout:'column',
				items:[{
					xtype:'label',
					//frame: false,
					width:	227//para que se muestre centrado
				},{
					xtype: 'panel',
					id:	'centro',
					title:	'Cifras de Control',
					width:	350,
					layout: 'form',
					bodyStyle: 'padding: 1px',
					items: [
						{
							xtype: 'displayfield',	id: 'disAcuse',	fieldLabel: 'No. Acuse',	value: ''
						},{
							xtype: 'displayfield',	id: 'disFechaCarga',	fieldLabel: 'Fecha de Carga',	value: ''
						},{
							xtype: 'displayfield',	id: 'disHoraCarga',		fieldLabel: 'Hora de Carga',	value: ''
						},{
							xtype: 'displayfield',	id: 'disUsuario',	fieldLabel: 'Usuario de Captura',	value: ''
						},{
							xtype: 'panel',	id: 'centro2', border: false,bodyborder: false,	bodyStyle: 'padding: 2px', items: [{html:'Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad cediendo los derechos de cr�dito de los DOCUMENTOS que constan en el mismo al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales'}]
						}
					]
			}]
		}
	];	
	
	var elementosAplica = [
		{
			xtype: 'container',
			layout:'column',
			border: true,
				items:[{
					xtype: 'panel',id:	'izqAplica',
					columnWidth:.5,
					layout: 'form',
					bodyStyle: 'padding: 2px',
					items: [{xtype: 'displayfield',	id: 'disDocto',	fieldLabel: 'N�mero de documento',	anchor:'95%',	value: ''}]
				},{
					xtype: 'panel',id:	'derAplica',
					columnWidth:.5,
					layout: 'form',
					bodyStyle: 'padding: 2px',
					items: [{xtype: 'displayfield',	id: 'disMontoAplica',	fieldLabel: 'Monto Aplicado',	anchor:'95%',	value: ''}]
			}]
		}
	];	
	var fpAplica=new Ext.FormPanel({
		height:34,
		width: 790,
		bodyStyle: 'padding: 2px',
		border: false,
		labelWidth: 150,
		items: elementosAplica,
		hidden: false
	});
	var fpAcuse=new Ext.FormPanel({
		height:500,
		width: 790,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 1px',
		border: false,
		labelWidth: 130,
		items: elementosAcuse,
		hidden: false
	});


var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 410,
		width: 940,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 8px',
		labelWidth: 130,
		//defaultType: 'textfield',
		frame: true,
		
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side'/*,
			anchor: '-20'*/
		},
		items: elementosForma,
		buttons:
		[
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
					{	
						if(!fp.getForm().isValid())
						{
							return;
						}

						if(verificaFechas('fechaEm1','fechaEm2')&&verificaFechas('monto1','monto2')&&verificaFechas('fechaVenc1','fechaVenc2')
								&&verificaFechas('fechaIF1','fechaIF2')&&verificaFechas('tasa1','tasa2')){
						gridTotales.hide();
						grid.show();
						consultaData.load({ params: Ext.apply(fp.getForm().getValues(),{
								operacion: 'Generar', //Generar datos para la consulta
								start: 0,
								limit: 15})
								});
						}
						/*//Mi acci�n
						grid.show();
						consulta.load({ params: Ext.apply(fp.getForm().getValues(),{txt_ne: Ext.getCmp('txt_ne').getValue()})
						});				
*/
					}
			 },{
				text:'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
						 location.reload();
				}
			 }
			 
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 940,
		items: [
			fp,
			NE.util.getEspaciador(30),
			grid,
			NE.util.getEspaciador(30),
			gridTotales
		]
	});

	Ext.getCmp('ninguna').hide();
	catalogoEpo.load();
	catalogoFactoraje.load();
	catalogoMoneda.load();
	catalogoEstatus.load();

});