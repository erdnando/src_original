Ext.onReady(function(){


	var procesarSuccessFailureArchivo =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var resp = 	Ext.util.JSON.decode(response.responseText);	
			
			Ext.getCmp('fpResultado').show();	
			Ext.getCmp('respuesta').setText(resp.respuesta,	false);				
			Ext.getCmp('respuesta').show();
			Ext.getCmp('pagos_con_registros1').hide();
			Ext.getCmp('pagos_sin_registros1').hide();
			Ext.getCmp('pagos_duplicados1').hide();
			Ext.getCmp('error1').hide();
			Ext.getCmp('total_registros').hide();
			Ext.getCmp('btncancelar').hide();
			Ext.getCmp('btnprocesar').hide();				
			Ext.getCmp("archivo").setValue('');
			Ext.Msg.alert('Mensaje',resp.mensaje,function(btn){
			
			});
			
		} else {		
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	

	var procesarArchivo = function() {
		
		var numeroProceso= Ext.getCmp('numeroProceso').getValue();	
		var nombreArchivo = Ext.getCmp('nombreArchivo').getValue();	
	
		Ext.Ajax.request({
			url: '13forma19.data.jsp',
			params:{														
				informacion: 'ProcesarCargaArchivo',
				numeroProceso:numeroProceso,
				nombreArchivo:nombreArchivo				
			},
			callback: procesarSuccessFailureArchivo
		});			
	}
	

	var validarSuccessFailureArchivo =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var resp = 	Ext.util.JSON.decode(response.responseText);	
			
			Ext.getCmp('fpResultado').show();	
						
			if(resp.respuesta=='S'){
				Ext.getCmp("pagos_con_registros1").setValue(resp.con_registro);
				Ext.getCmp('pagos_sin_registros1').setValue(resp.sin_registro);
				Ext.getCmp('pagos_duplicados1').setValue(resp.duplicados);			
				Ext.getCmp('error1').setValue(resp.errores);			
				Ext.getCmp('total_registros').setText(' Total de Registros:  '+resp.total_registros,	false);
				Ext.getCmp('respuesta').hide();
				Ext.getCmp('btncancelar').show();
				Ext.getCmp('btnprocesar').hide();
				
				Ext.getCmp('numeroProceso').setValue(resp.numeroProceso);	
				Ext.getCmp('nombreArchivo').setValue(resp.nombreArchivo);					
				
				if(resp.errores!='') {
					Ext.getCmp('pagos_con_registros1').hide();
					Ext.getCmp('pagos_sin_registros1').hide();
					Ext.getCmp('pagos_duplicados1').hide();
					Ext.getCmp('error1').show();
				}else if(resp.errores=='') {
					Ext.getCmp('pagos_con_registros1').show();
					Ext.getCmp('pagos_sin_registros1').show();
					Ext.getCmp('pagos_duplicados1').show();
					Ext.getCmp('error1').hide();					
					Ext.getCmp('total_registros').show();
				}
			}else  {
				Ext.getCmp('pagos_con_registros1').hide();
				Ext.getCmp('pagos_sin_registros1').hide();
				Ext.getCmp('pagos_duplicados1').hide();
				Ext.getCmp('error1').hide();
				Ext.getCmp('total_registros').hide();
				Ext.getCmp('btncancelar').show();
				Ext.getCmp('respuesta').setText(resp.respuesta,	false);
				Ext.getCmp('respuesta').show();						
			}			
			if(resp.boton_procesar=='S') {
				Ext.getCmp('btnprocesar').show();
				Ext.getCmp('btncancelar').show();
			}
			
			
		} else {
		
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var validarArchivo = function() {
	
		var archivo = Ext.getCmp("archivo");
		if (Ext.isEmpty(archivo.getValue()) ) {
			archivo.markInvalid('Seleccione el Archivo de Carga');	
			return;
		}
		
		var fpCarga = Ext.getCmp("fpCarga");
		fpCarga.el.mask('Procesando Carga de informaci�n..', 'x-mask-loading');	
		fpCarga.getForm().submit({
			url: '13forma19file.jsp',									
			 waitMsg: 'Enviando datos...',
			 waitTitle :'Por favor, espere',			
			success: function(form, action) {								
				var resp = action.result;
				var mArchivo =resp.archivo;
				var error_tam = resp.error_tam;																		
				if(mArchivo!='') {
					Ext.Ajax.request({
						url: '13forma19.data.jsp',
						params:{														
							informacion: 'ValidaCargaArchivo',
							archivo:mArchivo							
						},
						callback: validarSuccessFailureArchivo
					});			
				}	else{
					Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB.");
					return;
				}
			}
			,failure: NE.util.mostrarSubmitError
		});
		fpCarga.el.unmask();
	
	}
	
	var fpResultado = new Ext.form.FormPanel({
		id:			'fpResultado',
		width:		700,
		style:		'margin:0 auto;',
		hidden:		true,
		frame:		true,
		renderHidden:true,
		bodyStyle:	'padding: 6px',
		labelWidth:	180,
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [			
			{
				xtype:'textarea',	
				fieldLabel: 'Pagos con Registro en Cadenas',
				name:'pagos_con_registros',	
				id:'pagos_con_registros1',	
				readOnly:true,	
				anchor:'100%',	
				width:300,	
				height:100,	
				value:''		
			},		
			{	
				xtype:'textarea',	
				fieldLabel: 'Pagos sin Registro en Cadenas',
				name:'pagos_sin_registros',	
				id:'pagos_sin_registros1',	
				readOnly:true,	
				anchor:'100%',	
				width:300,	
				height:100,
				value:''		
			},	
			{	
				xtype:'textarea',
				fieldLabel: 'Pagos Duplicados en Cadenas',
				name:'pagos_duplicados',	
				id:'pagos_duplicados1',	
				readOnly:true,	
				anchor:'100%',	
				width:300,	
				height:100,
				value:''		
			},	
			{	
				xtype:'textarea',	
				fieldLabel: 'ERRORES EN EL ARCHIVO',
				name:'error',	
				id:'error1',	
				readOnly:true,	
				anchor:'100%',	
				width:300,	
				height:100,
				value:''		
			},
			{
				xtype: 	'label',
				id:	 	'total_registros',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				hidden: true
			},
			{
				xtype: 	'label',
				id:	 	'respuesta',
				cls:		'x-form-item',
				style: 	'font-weight:bold;text-align:left;margin:12px;',
				hidden: true
			},
			{ 	xtype: 'textfield', hidden: true, id: 'numeroProceso', 	value: '' },
			{ 	xtype: 'textfield', hidden: true, id: 'nombreArchivo', 	value: '' }			
		],
		buttons: [
			{
				text: 'Cancelar',
				id: 'btncancelar',
				iconCls: 'borrar',
				formBind: true,
				hidden: true,
				handler: function() {
					window.location = '13forma19ext.jsp';
				}
			},
			{
				text: 'Procesar',
				id: 'btnprocesar',
				iconCls: 'icoAceptar',
				formBind: true,
				hidden: true,
				handler: procesarArchivo				
			}
		]
	});
	

	
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMCAMPO'},
			{name: 'NOMBRE_CAMPO'},
			{name: 'TIPODATO'},
			{name: 'LONGITUD'},
			{name: 'OBLIGATORIO'},
			{name: 'DESCRIPCION'},
			{name: 'EJEMPLO'}
	  ]
	});
	var infoLayout = [
		['1', 'Ramo Creador de la CLC', 'Num�rico', '2', 'S','Clave del Ramo que emite la CLC','10 = Secretar�a de Econom�a' ],
		['2', 'Clave de la Unidad' ,'Alfanum�rico', '3', 'S', 'Clave de la Unidad creadora de la CLC', '310 = Direcci�n de Recursos Financieros' ],
		['3', 'Folio CLC','Num�rico','8','S','N�mero asignado por el SIAFF a la CLC','2502' ],
		['4', 'Folio Dependencia ','Alfanum�rico','15','S', 'N�mero de Documento asignado el Ramo a la CLC','CLC-2081-1 '],
		['5','Fecha de Captura', 'Fecha','10','S','Fecha de Captura de la CLC', '05/06/2009' ],
		['6','D�gito Identificador',' Alfanum�rico' ,'40', 'S', 'D�GITO IDENTIFICADOR POR DOCUMENTO, asignado por Cadenas Productivas NAFIN al momento de cargar un documento','024500020000000' ],
		['7', 'RFC Beneficiario ','Alfanum�rico','13', 'S', 'RFC del Beneficiario sin guiones.','BIN931011519' ],
		['8', 'Nombre Beneficiario',' Alfanum�rico' ,'120','S','Nombre del Beneficiario', 'BANCO INTERACCIONES, S.A.' ],
		['9','Clave de la Divisa','Alfanum�rico','3','S',' Clave de la Divisa de la CLC ','MXN = Peso Mexicano' ],
		['10', 'Ogto','Num�rico','4', 'S','Clasificador del Objeto de Gasto','3702'],
		['11','Fecha de Pago','Fecha','10','S','Fecha de Vencimiento del Pago','08/06/2009'],
		['12','Estatus','Alfanum�rico','15','S','Estatus de la CLC','Pagada'],
		['13','Importe','Num�rico','19,2', 'S','Importe del Pago','7521547.85']
	];
	
	storeLayoutData.loadData(infoLayout);

	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'N�mero <br>de Campo',
				dataIndex : 'NUMCAMPO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Nombre<br> de Campo',				
				dataIndex : 'NOMBRE_CAMPO',
				width : 150,
				align: 'left',
				sortable : true
			},
			{
				header : 'Tipo de Dato',
				dataIndex : 'TIPODATO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Longitud',
				dataIndex : 'LONGITUD',
				width : 80,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Obligatorio',
				dataIndex : 'OBLIGATORIO',
				width : 80,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Descripci�n',
				dataIndex : 'DESCRIPCION',
				width : 280,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Ejemplo',
				dataIndex : 'EJEMPLO',
				width : 240,
				sortable : true,
				align: 'left'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 830,
		height: 350,
		frame: true
	});
	
	var elementosFormaCarga = [					
		{
			xtype:	'panel',
			layout:	'column',
			width: 700,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[				
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){						
						if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}							
					}
				},				
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 140,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									emptyText: 'Nombre del Archivo',
									fieldLabel: 'Nombre del Archivo',
									name: 'archivo1',   
									buttonText: ' ',
									width: 300,	  
									buttonCfg: {
										iconCls: 'upload-icon'
									},
									anchor: '100%',									
									regex: /^.*\.(txt|TXT)$/,
									regexText:'Solo se admiten archivos TXT'
								}						
							]
						}
					]
				}
			]
		}
	];

	var fpCarga = new Ext.form.FormPanel({
	  id: 'fpCarga',
		width: 600,
		title: 'Ruta del archivo de Origen',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 140,
		defaultType: 'textfield',		
		items: elementosFormaCarga,			
		monitorValid: true,
		buttons: [
			{
				text: 'Continuar',
				id: 'continuar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:validarArchivo
			}			
		]
	});

	var fpBotones = {
		xtype:     	'panel',
		id: 'fpBotones',	
      frame:     	false,
      border:    	false,		
      bodyStyle: 	'background:transparent;',
      layout:    	'hbox',
      style: 		'margin: 0 auto',
      width:			650,
      height: 		75,
      layoutConfig: {
			align: 	'middle',
         pack: 		'center'
      },			  
		items: [	
			{
				xtype: 'button',
				text: 'Consulta al Reporte SIAFF',			
				id: 'btnCons1',					
				handler: function() {
					window.location = '13consulta19ext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<h1><b>Captura del Reporte SIAFF</b></h1>',			
				id: 'btnCap',					
				handler: function() {
					window.location = '13forma19ext.jsp';
				}
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: 'Reportes para TOICs',			
				id: 'btnCons2',					
				handler: function() {
					window.location = '13consulta19aext.jsp';
				}
			}
		]
	};
	
//--------------------------------PRINCIPAL-------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),
			fpCarga,
			NE.util.getEspaciador(20),
			gridLayout,
			fpResultado,
			NE.util.getEspaciador(20)
		]		
	});


});