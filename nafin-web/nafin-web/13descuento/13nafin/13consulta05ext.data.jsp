 <%@ page contentType="application/json;charset=UTF-8"
	import=" 
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		java.text.SimpleDateFormat,
		com.netro.descuento.*, 
		
		com.netro.model.catalogos.*,
		net.sf.json.JSONArray, net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion     =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	String infoRegresar  = "";
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	
				
	if (informacion.equals("CatalogoMoneda")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setOrden("DESCRIPCION");	//Solo acepta CLAVE ó DESCRIPCION (predeterminado)  (Tambien se puede usar 1 para clave y 2 para la descripcion)
		List elementos = cat.getListaElementos();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsObjArray.add(JSONObject.fromObject(ec));
		}
		infoRegresar = "{\"success\":true, \"total\":\""+jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		
	}
	
	else if (informacion.equals("CatalogoTasa")) {
		String moneda  = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		CatalogoTasa cat = new CatalogoTasa();
		cat.setCampoClave("ic_tasa");
		cat.setCampoDescripcion("cd_nombre"); 
		cat.setMoneda(moneda);
		List elementos = cat.getListaElementos();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsObjArray.add(JSONObject.fromObject(ec));
		}
		infoRegresar = "{\"success\":true, \"total\":\""+jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		
	}
	
	else if(informacion.equals("ConsultaGrid") || informacion.equals("GeneraPDF") ||informacion.equals("GeneraCSV")  )
	{
		try 
		{
			String ic_moneda = (request.getParameter("cmb_moneda")==null)?"":request.getParameter("cmb_moneda");
			String ic_tasa   = (request.getParameter("cmb_tasa")==null)?"":request.getParameter("cmb_tasa");
			
			ConsolidadoTasas paginador = new ConsolidadoTasas();
			paginador.setMoneda(ic_moneda);
			paginador.setTasa(ic_tasa);
			paginador.setConsulta(true);
				
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
			if (informacion.equals("ConsultaGrid")) {
				Registros reg = queryHelper.doSearch();
				
				String tasa;
				Hashtable fechaTasa = new Hashtable();
				Hashtable nombreTasa = new Hashtable();
				Hashtable valorTasa = new Hashtable();
				
				String claveTasa;
				String claveEpo;
				String nombreEpo;
				String claveIf;
				String nombreIf;
				String relacionMatematica;
				String claveEpoAnterior="";
				String puntos;
				double tasaBase=0.00000;;
				if (reg.getNumeroRegistros() > 0) {
					while(reg.next())
					{
						tasa = reg.getString("IC_TASA");
						fechaTasa.put(tasa,reg.getString("DC_FECHA"));
						nombreTasa.put(tasa,reg.getString("CD_NOMBRE"));
						valorTasa.put(tasa,reg.getString("FN_VALOR"));
					}
					
					paginador.setConsulta(false);
					reg = queryHelper.doSearch();
					while(reg.next()) {
						claveTasa 				= (reg.getString("IC_TASA") == null) ? "" 	: reg.getString("IC_TASA");
						claveEpo 				= (reg.getString("IC_EPO") == null) ? "" 		: reg.getString("IC_EPO");
						nombreEpo 				= (reg.getString("NOMBREEPO") == null) ? "" 	: reg.getString("NOMBREEPO");
						claveIf 					= (reg.getString("IC_IF") == null) ? "" 		: reg.getString("IC_IF");
						nombreIf 				= (reg.getString("NOMBREIF") == null) ? "" 	: reg.getString("NOMBREIF");
						relacionMatematica 	= (reg.getString("CG_REL_MAT") == null) ? "" : reg.getString("CG_REL_MAT");
						puntos					= (reg.getString("FN_PUNTOS") == null) ? "0" : reg.getString("FN_PUNTOS");
		
						if (relacionMatematica.equals("+"))
							tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())+Double.parseDouble(puntos);
						else if (relacionMatematica.equals("-"))
							tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())-Double.parseDouble(puntos);
						else if (relacionMatematica.equals("*"))
							tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())*Double.parseDouble(puntos);
						else if (relacionMatematica.equals("/"))
							tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())/Double.parseDouble(puntos);
		
						if (!claveEpo.equals(claveEpoAnterior)) {	
							claveEpoAnterior	= claveEpo;
							
							reg.setObject("NOMBREEPO",nombreEpo);									
						} 
						reg.setObject("NOMBREIF",nombreIf);	
						reg.setObject("IC_TASA",nombreTasa.get(claveTasa)+" "+relacionMatematica+" "+puntos);	
						reg.setObject("CG_REL_MAT",fechaTasa.get(claveTasa));	
						reg.setObject("FN_PUNTOS",String.valueOf(tasaBase));	
					
					}//fin del while
					infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
				}
				else
					infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
				
			}
			
			else if (informacion.equals("GeneraPDF")) {
				paginador.setConsulta(true);
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");			
				paginador.setConsulta(false);
				nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
			else if (informacion.equals("GeneraCSV")) {
				paginador.setConsulta(true);
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	
				paginador.setConsulta(false);
				nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");					
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
			
		} catch(Exception e) 
			{
				throw new AppException("Error en la recuperacion de datos!", e);
			}
		
   }

//System.out.println("infoRegresar============"+infoRegresar);  
%>
<%=infoRegresar%> 