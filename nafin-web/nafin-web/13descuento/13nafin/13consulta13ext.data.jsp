<%@ page contentType="application/json;charset=UTF-8"
import="netropology.utilerias.*,
        com.netro.model.catalogos.*,
        com.netro.descuento.*,
        java.util.*,
        net.sf.json.*"
errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf"%>
<%@ include file="/13descuento/13secsession_extjs.jspf"%>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

String infoRegresar = "";
if (informacion.equals("CatalogoEpo")) {
	CatalogoSimple cat = new CatalogoSimple();
  cat.setTabla("comcat_epo");
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
}

if (informacion.equals("ConsultaParametrosPorEpo")) {
  ParametrosDescuento parametrosDescuentoBean = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
  String claveEpo = request.getParameter("claveEpo")==null?"":request.getParameter("claveEpo");
  infoRegresar = parametrosDescuentoBean.obtenerJSONparametrosPorEpo(claveEpo, "1");
}
%>
<%=infoRegresar%>