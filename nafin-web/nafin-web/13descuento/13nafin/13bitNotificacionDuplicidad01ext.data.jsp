<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	org.apache.commons.logging.Log,
	netropology.utilerias.*,
	com.netro.threads.*,
	com.netro.model.catalogos.*,
	com.netro.afiliacion.*,
	com.netro.descuento.*,
	com.netro.descuento.BitacoraNotificacionDupl,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%!
private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = request.getParameter("informacion")  == null ? "" : (String)request.getParameter("informacion");
String operacion     = request.getParameter("operacion")    == null ? "" : (String)request.getParameter("operacion");
//Form principal
String epo           = request.getParameter("epo")          == null ? "" : (String)request.getParameter("epo");
String ne_pyme       = request.getParameter("ne_pyme")      == null ? "" : (String)request.getParameter("ne_pyme");
String nombre_pyme   = request.getParameter("nombre_pyme")  == null ? "" : (String)request.getParameter("nombre_pyme");
String ic_pyme       = request.getParameter("ic_pyme")      == null ? "" : (String)request.getParameter("ic_pyme");
String num_docto     = request.getParameter("num_docto")    == null ? "" : (String)request.getParameter("num_docto");
String fecha_inicio  = request.getParameter("fecha_inicio") == null ? "" : (String)request.getParameter("fecha_inicio");
String fecha_final   = request.getParameter("fecha_final")  == null ? "" : (String)request.getParameter("fecha_final");
//Busqueda Avanzada
String nombre        = request.getParameter("nombre")       == null ? "" : (String)request.getParameter("nombre");
String rfc           = request.getParameter("rfc")          == null ? "" : (String)request.getParameter("rfc");
String num_pyme      = request.getParameter("num_pyme")     == null ? "" : (String)request.getParameter("num_pyme");
String cmb_pyme      = request.getParameter("cmb_pyme")     == null ? "" : (String)request.getParameter("cmb_pyme");
//Variables globales
String mensaje       = "";
String consulta      = "";
String nombreArchivo = "";
String infoRegresar  = "";
JSONObject resultado = new JSONObject();
HashMap mapa         = new HashMap();
boolean success      = true;
//Paginación
int start = 0;
int limit = 0;

log.debug("***** informacion: " + informacion + " *****");

if(informacion.equals("Catalogo_Epo")){

	JSONArray jsonArr = new JSONArray();
	try{
		CatalogoEPO catalogo = new CatalogoEPO();
		catalogo.setCampoClave("IC_EPO");
		catalogo.setCampoDescripcion("CG_RAZON_SOCIAL");
		List elementos = catalogo.getListaElementosModalidad();
		Iterator it = elementos.iterator();
		while(it.hasNext()){
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo){
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
	} catch(Exception e){
		success = false;
		log.warn("Error en " + informacion + ". " + e);
	}
	resultado.put("success",   new Boolean(success));
	resultado.put("total",     ""+jsonArr.size());
	resultado.put("registros", jsonArr.toString());
	infoRegresar = resultado.toString();

} else if(informacion.equals("Catalogo_Pyme")){

	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
	Registros registros = afiliacion.getProveedores("", epo, num_pyme, rfc, nombre, ic_pyme, "", "");

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();

	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(registros.getString("IC_PYME"));
		elementoCatalogo.setDescripcion(registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		coleccionElementos.add(elementoCatalogo);
	}	

	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}

	infoRegresar = jsonObj.toString();

} else if(informacion.equals("Obtiene_Nombre_Pyme")){

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	Registros registros = new Registros();
	registros = BeanParamDscto.getParametrosPymeNafinSeleccionIF(ne_pyme);
	if(registros != null && registros.next()){
			ic_pyme     = (registros.getString("PYME")   == null)?"":registros.getString("PYME");
			nombre_pyme = (registros.getString("NOMBRE") == null)?"":registros.getString("NOMBRE");
	}else{
		success = false;
		mensaje = "El nafin electrónico no corresponde a una PyME o no existe.";
	}

	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje             );
	resultado.put("ic_pyme", ic_pyme             );
	resultado.put("nombre",  nombre_pyme         );
	infoRegresar = resultado.toString();

} else if(informacion.equals("Consultar_Datos") || informacion.equals("Imprimir_PDF") || informacion.equals("Imprimir_CSV")){

	/***** Le doy formato a las fechas [dd/mm/aaaa] *****/
	if(!fecha_inicio.equals("")){
		fecha_inicio = fecha_inicio.substring(8,10) + "/" + fecha_inicio.substring(5,7) + "/" + fecha_inicio.substring(0,4);
	}
	if(!fecha_final.equals("")){
		fecha_final = fecha_final.substring(8,10) + "/" + fecha_final.substring(5,7) + "/" + fecha_final.substring(0,4);
	}

	BitacoraNotificacionDupl paginador = new BitacoraNotificacionDupl();

	paginador.setIcEpo(epo);
	paginador.setIcPyme(ic_pyme);
	paginador.setNumDocto(num_docto);
	paginador.setFechaInicio(fecha_inicio);
	paginador.setFechaFinal(fecha_final);
	//CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador); // Se cambia esta línea por la siguiente para utilizar hilos
	CQueryHelperThreadRegExtJS queryHelper = new CQueryHelperThreadRegExtJS(paginador);

	if (informacion.equals("Consultar_Datos")){
		try{
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e){
			start = 0;
			limit = 15;
			throw new AppException("Error en los parametros start y limit recibidos", e);
		}
		try{
			if (operacion.equals("generar")){ //Nueva consulta
			log.debug("...");
				queryHelper.executePKQuery(request);
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e){
			throw new AppException(" Error en la paginacion. ", e);
		}
		resultado = JSONObject.fromObject(consulta);
	} else if(informacion.equals("Imprimir_PDF")){

		try{
			String estatusArchivo = request.getParameter("estatusArchivo");
			String porcentaje = "";

			// Genera el PDF utilizando paginación.
			//nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF"); 

			/*
			// Este bloque de código genera el pdf con todos los registros implementando la interfaz IQueryGeneratorRegExtJS
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			*/

			if(estatusArchivo.equals("INICIO")){
				session.removeAttribute("13bitNotificacionDuplicidad01extThreadCreateFiles");
				ThreadCreateFiles threadCreateFiles = queryHelper.getThreadCreateCustomFile(request, strDirectorioTemp, "PDF");
				session.setAttribute("13bitNotificacionDuplicidad01extThreadCreateFiles", threadCreateFiles);
				estatusArchivo = "PROCESANDO";
			} else if(estatusArchivo.equals("PROCESANDO")){
				ThreadCreateFiles threadCreateFiles = (ThreadCreateFiles)session.getAttribute("13bitNotificacionDuplicidad01extThreadCreateFiles");
				System.out.println("queryHelper.getPercent() = "+ threadCreateFiles.getPercent());
				porcentaje = String.valueOf(threadCreateFiles.getPercent()) + " %";
				if(!threadCreateFiles.isRunning() && !threadCreateFiles.hasError()){
					nombreArchivo = threadCreateFiles.getNombreArchivo();
					estatusArchivo = "FINAL";
				}else if(threadCreateFiles.hasError()){
					estatusArchivo = "ERROR";
				}
			}

			if(porcentaje.equals("") || porcentaje.equals("0") || porcentaje.equals("0 %")){
				porcentaje = "Procesando...";
			}
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			resultado.put("estatusArchivo", estatusArchivo);
			resultado.put("porcentaje", porcentaje);

		} catch(Throwable e){
			resultado.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF. ", e);
		}

	} else if(informacion.equals("Imprimir_CSV")){
		try{
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			resultado.put("success", new Boolean(true));
			resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			resultado.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo CSV. ", e);
		}
	}

	infoRegresar = resultado.toString();
}

%>
<%=infoRegresar%>