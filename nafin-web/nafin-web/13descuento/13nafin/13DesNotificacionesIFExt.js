Ext.onReady(function(){


//---------------------- OVERRIDES ------------------------------
	/*
		Nota: Se corrige un bug que ocasionaba que el icono de error se mostrara dentro del combo box.
		Version original:	http://docs.sencha.com/ext-js/3-4/source/Element.style.html#Ext-Element-method-getWidth
	*/
	Ext.override(Ext.Element, {
		getWidth : function(contentWidth){
			var me = this,
			dom    = me.dom,
			hidden = Ext.isIE && me.isStyle('display', 'none'),
			//w = MATH.max(dom.offsetWidth, hidden ? 0 : dom.clientWidth) || 0;
			w      = Math.max( dom.offsetWidth || me.getComputedWidth(), hidden ? 0 : dom.clientWidth) || 0;
			w      = !contentWidth ? w : w - me.getBorderWidth("lr") - me.getPadding("lr");
			return w < 0 ? 0 : w;
		}
	});

///////////////////////////////////////////////////////////////////////////////
//- - - - - - - - - - - - HANDLER�S- - - - - - - - - - - - - - - - - - - - - -
function procesarArchivoSuccess(opts, success, response) {		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			if(infoR.correos=="OK")
			{
			archivo = archivo.replace('/nafin','');
			
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			}
			else
			{
				Ext.MessageBox.alert("Mensaje",
							'No existen archivos cargados.');
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
function procesarEliminarDesSuccess(opts, success, response) {		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			
			if(infoR.elimino!="")
			{
			Ext.MessageBox.alert("Mensaje",
							'Los destinatarios Fueron Eliminados.');
				
			}
			else
			{
				Ext.MessageBox.alert("Mensaje",
							'Existio un error al realizar el proceso');
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function RealizarCarga()
	{
		var forma = Ext.getCmp("forma");
		forma.getForm().submit({
			url: '13DesNotificacionesIFExt_carga.jsp',
			method: 'POST',
			params:{
						intermediario:Ext.getCmp('ifID').getValue(),
						notificacion:Ext.getCmp('radioTipoRec').getValue()
																		
					},
			success:function(form, o) {
				var resu=Ext.util.JSON.decode(o.response.responseText);
				if(resu.success==true)
				{													
					//alert(resu.erroresm);
					if(resu.erroresm!="")
					{
					var resultadoErr=Ext.getCmp('conErr');
					var resultadoErrPanel=Ext.getCmp('panelReportCarga');
					var rl=replaceAll(resu.erroresm,'--','\n');

					resultadoErr.setValue(rl);
					resultadoErrPanel.show();
					fp.hide();
					}else
					{
						addRow("Archivo cargado",resu.archivoCar);
						addRow("Total de registros cargados",resu.totalC);
						addRow("Fecha de carga",resu.fechaC);
						addRow("Hora de carga",resu.horaC);
						addRow("Usuario",resu.usuario);
						paneOK.show();
						fp.hide();
					}
				}											
			 },
			 failure:function(form, o) {
				NE.util.mostrarConnError(o.response);
			 }
			}
		);
		//fin codigo submit
	}

function addRow(Nombre,Valor){
	var newRow=Ext.getCmp('idInforme');
	newRow.add({html: '<div align="center">'+Nombre+'</div>'},{html: '<div align="center">'+Valor+'</div>'});
	newRow.doLayout();
}
/////////////////////////////////////////////////////////////////////////////////77

//- - - - - - -  - - - - - STORE�S - - - - - - - - - - - - - - - - - - - - - - 
var catalogoIF= new Ext.data.JsonStore
  ({
	   id: 'catIF',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13DesNotificacionesIFExt.data.jsp',
		baseParams: 
		{
		 informacion: 'CatalogoIF'
		},
		autoLoad: false,
		listeners:
		{
		 exception: NE.util.mostrarDataProxyError,
		 beforeload: NE.util.initMensajeCargaCombo
		}
  });


//////////////////////////////////////////////////////////////////////////////
///---------------------------COMPONENTES VISUALES----------------------------
//Panel de informacion correcta
var elementosCoins = [
		{
			xtype: 'panel',id:'idInforme',layout:'table',width:500,border:true,layoutConfig:{ columns: 2 },
			defaults: {frame:false, border:true,width:250, height: 35,bodyStyle:'padding:8px'}
		}		
	];
var paneOK= new Ext.Panel({
			id:'panelReportCargaOK',
			hidden:true,
			width:500,
			title:'<center>Resultados del proceso de carga</center>',
			layoutConfig: {			 
				 pack:'center'				 
			  },
			  style: ' margin:0 auto;',
			items:[elementosCoins],
			buttons: [
			{
				text:"Regresar",
				id: 'btnRegok',
				formBind: true,
				handler: function(boton, evento) {
					window.location = '13DesNotificacionesIFExt.jsp'
				} //fin handler			
			}]
		 });

//Elementos de reporte de carga
	var elementosReporteCarga=[
		
		{	
				xtype: 'textarea',
				name: 'conErr',
				readOnly:true,
				id: 'conErr',
				width: 450,
				height: 300,				
				anchor:'100%',
				value:''
		}
	];
//fin de elementos de reporte de carga

var paneRes= new Ext.Panel({
			id:'panelReportCarga',
			hidden:true,
			width:450,
			title:'<center>Errores durante la carga del archivo</center>',
			layoutConfig: {			 
				 pack:'center'				 
			  },
			  style: ' margin:0 auto;',
			items:[elementosReporteCarga],
			buttons: [
			{
				text:"Regresar",
				id: 'btnReg',
				formBind: true,
				handler: function(boton, evento) {
					window.location = '13DesNotificacionesIFExt.jsp'
				} //fin handler			
			}]
		 });


var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_if',
			hiddenName : 'ic_if',
			id: 'ifID',
			fieldLabel: 'Nombre del IF',
			emptyText:'Seleccione...',
			mode: 'local',
			triggerAction: 'all',
			valueField: 'clave',
			store: catalogoIF,
			allowBlank: false,
			forceSelection: true,
			displayField: 'descripcion',
			width: 200
		},
		{
            xtype: 'radiogroup',
				id:'radioTipoRec',
            fieldLabel: '',
				allowBlank: false,
				items: [
                {boxLabel: 'Notificar Operaciones del d�a', name: 'notificacion',inputValue: 'OD'},
                {boxLabel: 'Notificar Documentos de Cambio de Estatus ', name: 'notificacion',inputValue: 'CE' }
            ]
        },
		  {
			xtype: 'compositefield',
			fieldLabel: '* Consultar Ultimo<br> Archivo Cargado',
			id:'id3',
			items: [				
				{
					xtype: 'button',
					id: 'id_descargar',
					text:'Descargar',
					handler: function(boton, evento) {
					if(Ext.getCmp('ifID').getValue()=='')
					{
						
							Ext.MessageBox.alert("Mensaje",
							'Es necesario seleccionar un IF');
						return;
					}
					var noti=Ext.getCmp('radioTipoRec').getValue();
					if(Ext.getCmp('radioTipoRec').getValue()==null)
					{
						
							Ext.MessageBox.alert("Mensaje",
							'Es necesario seleccionar un Tipo de Notificaci�n');
						return;
					}
					
					Ext.Ajax.request({
						url: '13DesNotificacionesIFExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'DescargaArchivo'
						})
						,callback: procesarArchivoSuccess
						});
					}
				
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 20
				},
				{
					xtype: 'button',
					id: 'id_elimina_des',
					text:'Eliminar Destinatarios',
					handler: function(boton, evento) {
					if(Ext.getCmp('ifID').getValue()=='')
					{
						
							Ext.MessageBox.alert("Mensaje",
							'Es necesario seleccionar un IF');
						return;
					}
					var noti=Ext.getCmp('radioTipoRec').getValue();
					if(Ext.getCmp('radioTipoRec').getValue()==null)
					{
						
							Ext.MessageBox.alert("Mensaje",
							'Es necesario seleccionar un Tipo de Notificaci�n');
						return;
					}
					Ext.Ajax.request({
						url: '13DesNotificacionesIFExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Eliminar'
						})
						,callback: procesarEliminarDesSuccess
						});
					}
				}
			]
		}
		////
		,
		  {
			xtype: 'compositefield',
			fieldLabel: 'Ruta del archivo',
			id:'id77',
			items: [				
				{
				xtype: 'button',	columnWidth: .05,	autoWidth: true,
				autoHeight: true,	iconCls: 'icoAyuda',fieldLabel: 'Ruta del Archivo',
				handler: function() {
				Ext.MessageBox.alert("Mensaje",
							'Se deber� cargar un archivo de texto (.txt) con las direcciones de correo electr�nico de los destinatarios, los correo electr�nicos deber�n ser separados por pipe "|". <br>'+
							'Ejemplo: usuario@nafin.gob.mx|usuario2@nafin.gob.mx|usuario3@nafin.gob.mx ');
					}
				},
				{
					xtype: 'displayfield',
					value: '',
					width: 1
				},
				{
					xtype: 'fileuploadfield',
					id: 'archivo',
					width: 400,	  
					fieldLabel: 'Ruta del Archivo',
					name: 'archivoCesion',
					buttonText: 'Examinar...',
					anchor: '90%',
					allowBlank: false
				}
			]
		}
		
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			680,
		style:			'margin:0 auto;',
		collapsible:	true,
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		120,
		title:'Destinatarios Notificaciones IF',
		fileUpload: true,
		defaults:		{	msgTarget:	'side',	anchor:	'-20'	},
		items:			elementosForma,
		monitorValid:	true,
		buttons: [
			{
				text:"Continuar",
				id: 'btnContAdjArch',
				formBind: true,
				handler: function(boton, evento) {
					var cargaArchivo = Ext.getCmp('archivo');
						if (!cargaArchivo.isValid()){
							cargaArchivo.focus();
							return;
						}
					var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
					if (/^.*\.(txt)$/.test(ifile)){
							RealizarCarga();
					}
					else{
						Ext.MessageBox.alert('Mensaje','El archivo origen debe tener formato .txt');
					}
					//
				} //fin handler			
			}
			
		]
	});

//----------------Principal------------------------------
var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 930,
	  height: 'auto',
	  items: 
	    [  
			fp ,
			NE.util.getEspaciador(10)	,
			paneRes,
			paneOK
		 ]
  });
		
	catalogoIF.load();
		
});