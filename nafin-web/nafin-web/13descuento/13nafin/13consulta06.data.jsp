<%@ page contentType="application/json;charset=UTF-8"
	import=" 
		java.util.*,
		java.math.*,		
		java.util.Vector,
		netropology.utilerias.*,
		com.netro.descuento.*, 
		com.netro.exception.*,
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoSimple"
	errorPage="/00utils/error_extjs.jsp"%>		
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%
System.out.println("13consulta06.data.jsp (E)"); 
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String operacion = request.getParameter("operacion") == null?"":(String)request.getParameter("operacion");
String icEpo = (request.getParameter("no_epo")==null)?"":request.getParameter("no_epo");
String icIF = (request.getParameter("no_if")==null)?"":request.getParameter("no_if");
String ic_banco_fondeo = request.getParameter("ic_banco_fondeo") == null?"":(String)request.getParameter("ic_banco_fondeo");
String fechaAnterior = (request.getParameter("fechaAnterior")==null)?"":request.getParameter("fechaAnterior");
if("ADMIN BANCOMEXT".equals(strPerfil)) { ic_banco_fondeo = "2"; }
String infoRegresar = "";

AutorizacionTasas tasa = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

Vector vIfs = new Vector();
HashMap columnas = new HashMap();
JSONArray registros = new JSONArray();
List lic_ifs = new ArrayList();
String todoIF = "";
int numRegistrosIf = 0;
	if(!icEpo.equals("")){
		vIfs  = tasa.getIFAsociados(icEpo);
		numRegistrosIf = vIfs.size();
		for(int n=0; n<vIfs.size(); n++) { 
			Vector vRegistro = (Vector)vIfs.get(n);	
				todoIF += vRegistro.get(0).toString()+",";		
		}
	}
	if(!todoIF.equals("")){
		int tamanio2 =	todoIF.length();
			String valor =  todoIF.substring(tamanio2-1, tamanio2);
			if(valor.equals(",")){
				todoIF =todoIF.substring(0,tamanio2-1);
			}
		}
	if(icIF.equals("")){
		icIF = todoIF;
	}
		

	if (informacion.equals("CatalogoBanco")) {
		infoRegresar = "";
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("IC_BANCO_FONDEO");
		catalogo.setCampoDescripcion("CD_DESCRIPCION");
		catalogo.setTabla("COMCAT_BANCO_FONDEO");
		catalogo.setOrden("IC_BANCO_FONDEO");
		infoRegresar = catalogo.getJSONElementos();
		
	}else if (informacion.equals("CatalogoEPO") && !ic_banco_fondeo.equals("")) {
		infoRegresar = "";
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		cat.setClaveBancoFondeo(ic_banco_fondeo);
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();	
		
	}
	else if (informacion.equals("CatalogoEPO") ) {
		infoRegresar = "";
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social"); 
		
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();	
		
	}
	else if (informacion.equals("CatalogoIF") ) {
	if(vIfs.size()>0){
//		int numRegistrosIf = vIfs.size();
		for(int n=0; n<vIfs.size(); n++) { 
			Vector vRegistro = (Vector)vIfs.get(n);		
			columnas = new HashMap();		
			columnas.put("clave",vRegistro.get(0));
			columnas.put("descripcion",vRegistro.get(1));
			registros.add(columnas);
		}	
	}
	
	infoRegresar = "({\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"})";
			
	
	}else if (informacion.equals("Consulta") && operacion.equals("GenerarC") ) {
	
		JSONObject 	resultado	= new JSONObject();
		String consulta = "";
		if(numRegistrosIf > 0){
			consulta = tasa.getTasaAutIFEXT(icEpo, icIF, "1,54");
		}
		else { //numRegistrosIf == 0
			consulta = "{\"success\": true, \"total\": \"" + 0 + "\", \"registros\": " + "{}"+"}";
		}
		resultado = JSONObject.fromObject(consulta);
				
		// obtiene  la parametrizacion de la EPo
		int valorfven =tasa.operaFactorajeVencido(icEpo);
		int count = 0;
		if(valorfven>0) {
			count = valorfven;
			if(count>0) {
			resultado.put("VALOR","S");
			}else{
			resultado.put("VALOR","N");
			}
		}else{
			resultado.put("VALOR","N");
		}
		
		String nombreEPo =tasa.nombreEPO(icEpo);
		resultado.put("NOMBREEPO",nombreEPo);
		
		System.out.println("nombreEPo  "+nombreEPo);
		
		infoRegresar = resultado.toString();
	
	}else if (informacion.equals("Historico") && !icEpo.equals("") ) {
		// Historico de Tasas
		infoRegresar =  tasa.consultaHistTasaEXT(icEpo, fechaAnterior);
		
	}else if (informacion.equals("VerTasas") ) {
		
	infoRegresar = tasa.consultaTasaAutoIfEXT(icEpo,fechaAnterior,icIF,"EN");
	
	}

%>

<%=infoRegresar%>

<%System.out.println("13consulta06.data.jsp (S)");%>
