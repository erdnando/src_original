Ext.onReady(function(){
	var noEpo;	
/*--------------------------------- Handler's -------------------------------*/

	var procesarSuccessFailureModificaEPO = function(opts, success, response){
		var cmpForma = Ext.getCmp('forma');
		cmpForma.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.Msg.alert('Mensaje...', 'Se guardo con �xito la parametrizaci�n de esta EPO');
		}
		else{
			Ext.Msg.alert('Error...', 'Ocurrio un error durante la modificaci�n  de la parametrizaci�n de la EPO');
		}
	}

	var procesarSuccessFailureConsultaParametrosEPO = function(opts, success, response){

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var fp = Ext.getCmp('forma');
			fp.el.unmask();

			var reg = Ext.util.JSON.decode(response.responseText).registros[0];
			if (reg != null){

				if(reg.OPERA_NOTAS_CRED == 'S'){	Ext.getCmp('id_sOperaNotasCredito').setValue('S');	} 
				else if (reg.OPERA_NOTAS_CRED == 'N'){	Ext.getCmp('id_sOperaNotasCredito').setValue('N'); }

				if(reg.CS_APLICAR_NOTAS_CRED == 'S'){	Ext.getCmp('id_sAplicarNotasCredito').setValue('S');	} 
				else if (reg.CS_APLICAR_NOTAS_CRED == 'N'){	Ext.getCmp('id_sAplicarNotasCredito').setValue('N'); }

				if(reg.MONTO_MINIMO != 'N' && reg.MONTO_MINIMO != ''){	Ext.getCmp('id_fn_monto_minimo').setValue(reg.MONTO_MINIMO); }
				else if(reg.MONTO_MINIMO == '') {	Ext.getCmp('id_fn_monto_minimo').setValue(reg.MONTO_MINIMO); 	}

				if(reg.DSCTO_AUTO_OBLIGATORIO == 'S'){	Ext.getCmp('id_sDsctoObligatorio').setValue('N');	}   //Obsoleto
				else if (reg.DSCTO_AUTO_OBLIGATORIO == 'N'){	Ext.getCmp('id_sDsctoObligatorio').setValue('N'); }

				if(reg.DESCTO_DINAM == 'S'){	Ext.getCmp('id_sDsctoDinam').setValue('N');	} //Obsoleto
				else if (reg.DESCTO_DINAM == 'N'){	Ext.getCmp('id_sDsctoDinam').setValue('N'); }

				if(reg.LIMITE_PYME == 'S'){	Ext.getCmp('id_sLimitePyme').setValue('S');	} 
				else if (reg.LIMITE_PYME == 'N'){	Ext.getCmp('id_sLimitePyme').setValue('N'); }

				if(reg.PUB_FIRMA_MANC == 'S'){	Ext.getCmp('id_sFimaManc').setValue('S');	} 
				else if (reg.PUB_FIRMA_MANC == 'N'){	Ext.getCmp('id_sFimaManc').setValue('N'); }

				if(reg.PUB_HASH == 'S'){	Ext.getCmp('id_sCriterioHash').setValue('S');	} 
				else if (reg.PUB_HASH == 'N'){	Ext.getCmp('id_sCriterioHash').setValue('N'); }

				if(reg.FECHA_VENC_PYME == 'S'){	Ext.getCmp('id_sFechaVencPyme').setValue('S');	} 
				else if (reg.FECHA_VENC_PYME == 'N'){	Ext.getCmp('id_sFechaVencPyme').setValue('N'); }

				if(reg.PLAZO_PAGO_FVP != 'N' && reg.PLAZO_PAGO_FVP != '') {	Ext.getCmp('id_diasMaximosAd').setValue(reg.PLAZO_PAGO_FVP); }
				else if( reg.PLAZO_PAGO_FVP == '') {	Ext.getCmp('id_diasMaximosAd').setValue(reg.PLAZO_PAGO_FVP); }

				if(reg.PLAZO_MAX1_FVP != 'N' && reg.PLAZO_MAX1_FVP != '') {	Ext.getCmp('id_PlazoMaxFechasVePyDc').setValue(reg.PLAZO_MAX1_FVP); }
				else if( reg.PLAZO_MAX1_FVP == '') {	Ext.getCmp('id_PlazoMaxFechasVePyDc').setValue(reg.PLAZO_MAX1_FVP); }

				if(reg.PLAZO_MAX2_FVP != 'N' && reg.PLAZO_MAX2_FVP != '') {	Ext.getCmp('id_PlazoMaxFechasVenPyDm').setValue(reg.PLAZO_MAX2_FVP); }
				else if( reg.PLAZO_MAX2_FVP == '') {	Ext.getCmp('id_PlazoMaxFechasVenPyDm').setValue(reg.PLAZO_MAX2_FVP); }

				if(reg.PUB_DOCTO_VENC == 'S'){	Ext.getCmp('id_sPubDoctoVenc').setValue('S');	} 
				else if (reg.PUB_DOCTO_VENC == 'N'){	Ext.getCmp('id_sPubDoctoVenc').setValue('N'); }

				if(reg.PREAFILIACION == 'S'){	Ext.getCmp('id_sOperPreafilia').setValue('S');	} 
				else if (reg.PREAFILIACION == 'N'){	Ext.getCmp('id_sOperPreafilia').setValue('N'); }

				if(reg.DESC_AUTO_PYME == 'S'){	Ext.getCmp('id_sPermitirDescuentoAutomatico').setValue('S');	} 
				else if (reg.DESC_AUTO_PYME == 'N'){	Ext.getCmp('id_sPermitirDescuentoAutomatico').setValue('N'); }

				if(reg.PUBLICACION_EPO_PEF == 'S'){	Ext.getCmp('id_sPublicacionEPOPEF').setValue('S');	} 
				else if (reg.PUBLICACION_EPO_PEF == 'N'){	Ext.getCmp('id_sPublicacionEPOPEF').setValue('N'); }

				if(reg.PUB_EPO_PEF_USA_SIAFF == 'S'){	Ext.getCmp('id_sPubEPOPEFSIAFF').setValue('S');	} 
				else if (reg.PUB_EPO_PEF_USA_SIAFF == 'N'){	Ext.getCmp('id_sPubEPOPEFSIAFF').setValue('N'); }

				if(reg.PUB_EPO_PEF_FECHA_RECEPCION == 'S'){	Ext.getCmp('id_sPubEPOPEFFlag').setValue('S');	} 
				else if (reg.PUB_EPO_PEF_FECHA_RECEPCION == 'N'){	Ext.getCmp('id_sPubEPOPEFFlag').setValue('N'); }

				if(reg.PUB_EPO_PEF_FECHA_ENTREGA == 'S'){	Ext.getCmp('id_sPubEPOPEFDocto1').setValue('S');	} 
				else if (reg.PUB_EPO_PEF_FECHA_ENTREGA == 'N'){	Ext.getCmp('id_sPubEPOPEFDocto1').setValue('N'); }

				if(reg.PUB_EPO_PEF_TIPO_COMPRA == 'S'){	Ext.getCmp('id_sPubEPOPEFDocto2').setValue('S');	} 
				else if (reg.PUB_EPO_PEF_TIPO_COMPRA == 'N'){	Ext.getCmp('id_sPubEPOPEFDocto2').setValue('N'); }

				if(reg.PUB_EPO_PEF_CLAVE_PRESUPUESTAL == 'S'){	Ext.getCmp('id_sPubEPOPEFDocto3').setValue('S');	} 
				else if (reg.PUB_EPO_PEF_CLAVE_PRESUPUESTAL == 'N'){	Ext.getCmp('id_sPubEPOPEFDocto3').setValue('N'); }

				if(reg.PUB_EPO_PEF_PERIODO == 'S'){	Ext.getCmp('id_sPubEPOPEFDocto4').setValue('S');	} 
				else if (reg.PUB_EPO_PEF_PERIODO == 'N'){	Ext.getCmp('id_sPubEPOPEFDocto4').setValue('N'); }

				if(reg.PUB_EPO_OPERA_MANDATO == 'S'){	Ext.getCmp('id_sOperaFactorajeConMandato').setValue('S');	} 
				else if (reg.PUB_EPO_OPERA_MANDATO == 'N'){	Ext.getCmp('id_sOperaFactorajeConMandato').setValue('N'); }

				if(reg.PUB_EPO_VENC_INFONAVIT == 'S'){	Ext.getCmp('id_sOperaFactorajeVencimiento').setValue('S');	} 
				else if (reg.PUB_EPO_VENC_INFONAVIT == 'N'){	Ext.getCmp('id_sOperaFactorajeVencimiento').setValue('N'); }

				if(reg.OPER_SOLIC_CONS_CDER == 'S'){	Ext.getCmp('id_sOperaSolicitudCesionDerechos').setValue('S');	} 
				else if (reg.OPER_SOLIC_CONS_CDER == 'N'){	Ext.getCmp('id_sOperaSolicitudCesionDerechos').setValue('N'); }

				if(reg.PUB_EPO_FACT_24HRS == 'S'){	Ext.getCmp('id_sOperaFactoraje24hrs').setValue('S');	} 
				else if (reg.PUB_EPO_FACT_24HRS == 'N'){	Ext.getCmp('id_sOperaFactoraje24hrs').setValue('N'); }

				if(reg.cs_factoraje_vencido == 'S'){	Ext.getCmp('id_sOperaFactorajeVencido').setValue('S');	} 
				else if (reg.cs_factoraje_vencido == 'N'){	Ext.getCmp('id_sOperaFactorajeVencido').setValue('N'); }

				if(reg.PUB_EPO_DESCAUTO_FACTVENCIDO == 'S'){	Ext.getCmp('id_sDesAutoFactVenci').setValue('S');	} 
				else if (reg.PUB_EPO_DESCAUTO_FACTVENCIDO == 'N'){	Ext.getCmp('id_sDesAutoFactVenci').setValue('N'); }

				if(reg.DESC_AUT_ULTIMO_DIA == 'S'){	Ext.getCmp('id_sOperaDsctoAutUltimoDia').setValue('S');	} 
				else if (reg.DESC_AUT_ULTIMO_DIA == 'N'){	Ext.getCmp('id_sOperaDsctoAutUltimoDia').setValue('N'); }

				if(reg.PUB_EPO_FACTORAJE_DISTRIBUIDO == 'S'){	Ext.getCmp('id_sFactorajeDist').setValue('S');	} 
				else if (reg.PUB_EPO_FACTORAJE_DISTRIBUIDO == 'N'){	Ext.getCmp('id_sFactorajeDist').setValue('N'); }

				if(reg.AUTORIZA_CTAS_BANC_ENTIDADES == 'S'){	Ext.getCmp('id_sAutorizacionCuentasBanc').setValue('S');	} 
				else if (reg.AUTORIZA_CTAS_BANC_ENTIDADES == 'N'){	Ext.getCmp('id_sAutorizacionCuentasBanc').setValue('N'); }

				if(reg.OPER_TASAS_ESPECIALES == 'S'){	Ext.getCmp('id_sOperaTasasEspeciales').setValue('S');	} 
				else if (reg.OPER_TASAS_ESPECIALES == 'N'){	Ext.getCmp('id_sOperaTasasEspeciales').setValue('N'); }

				if(reg.TIPO_TASA == 'P'){	Ext.getCmp('id_sTipoTasa').setValue('P');	} 
				else if (reg.TIPO_TASA == 'A'){	Ext.getCmp('id_sTipoTasa').setValue('A'); }
				else if (reg.TIPO_TASA == 'NG'){	Ext.getCmp('id_sTipoTasa').setValue('NG'); }

				if(reg.HORA_ENV_OPE_IF != 'N' && reg.HORA_ENV_OPE_IF != '') { Ext.getCmp('id_sHoraEnvOpIf').setValue(reg.HORA_ENV_OPE_IF); }
				else { Ext.getCmp('id_sHoraEnvOpIf').setValue(''); }
				if(reg.CS_SHOW_FCHA_AUT_IF_INFO_DCTOS == 'S'){	Ext.getCmp('id_sMostrarFechaAutorizacionIFInfoDoctos').setValue('S');	} 
				else if (reg.CS_SHOW_FCHA_AUT_IF_INFO_DCTOS == 'N'){	Ext.getCmp('id_sMostrarFechaAutorizacionIFInfoDoctos').setValue('N'); }

				if(reg.FACTORAJE_IF == 'S'){	Ext.getCmp('id_sOperaFactorajeIF').setValue('S');	} 
				else if (reg.FACTORAJE_IF == 'N'){	Ext.getCmp('id_sOperaFactorajeIF').setValue('N'); }

				if(reg.CS_CONVENIO_UNICO == ''){	
					Ext.getCmp('cUnicoIF').setValue('');	
				} 	else if (reg.CS_CONVENIO_UNICO != ''){	
					Ext.getCmp('cUnicoIF').setValue(reg.CS_CONVENIO_UNICO); 
				}	
				if(reg.CS_OPERA_FIDEICOMISO == 'S'){	Ext.getCmp('id_FideDePro').setValue('S');	} 
				else if (reg.CS_OPERA_FIDEICOMISO == 'N'){	Ext.getCmp('id_FideDePro').setValue('N'); }

				if(reg.TASA_OPERACION != 'N'){	Ext.getCmp('id_tasaOperacion').setValue(reg.TASA_OPERACION);	} 
				else if (reg.TASA_OPERACION == 'N'){	Ext.getCmp('id_tasaOperacion').setValue(''); }

				Ext.getCmp('id_valida_duplic').setValue(reg.VALIDA_DUPLIC); //FODEA 38-2014
				Ext.getCmp('id_email_notificacion').setValue(reg.EMAIL_DUPLIC); //FODEA 38-2014

			}
		}
	}
	

/*------------------------------- End Handler's -----------------------------*/
/*****************************************************************************/
 
  
 
	
/*---------------------------------- Store's --------------------------------*/

	var catalogoEpo = new Ext.data.JsonStore({
	   id				: 'catEpo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13forma09Ext.data.jsp',
		baseParams	: { informacion: 'CatalogoEPO'},
		totalProperty : 'total',
		autoLoad		: true,
		listeners	:
		{ exception: NE.util.mostrarDataProxyError }
	});		
	
	

/*--------------------------------- End Store's -----------------------------*/
/*****************************************************************************/
/* ----------------------------------- Grid's ------------------------------ */
	
	
	
/*-------------------------------- Componentes ------------------------------*/
	function mostrarAyuda(parametro){
		var titulo = '';
		var descripcion =[];
		
		if(parametro==1) {
			titulo = " 1. Opera Notas de Cr�dito ";			
		}else if(parametro==2) {
			titulo = " 2.  Monto m�nimo resultante para la aplicaci�n de Notas de Cr�dito  ";			
		} else if(parametro==3) {
			titulo = "3.  Descuento Autom�tico obligatorio ";			
		}else if(parametro==4) {
			titulo = "4.  Descuento Autom�tico Din�mico ";			
		} else if(parametro==5) {
			titulo = "5. Opera L�mite de L�nea por PyME";			
		}else	if(parametro==6) {
			titulo = "6. Publicaci�n con Firma Mancomunada";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Permite publicar documentos con estatus Pre Negociable o Pre No Negociable.'  +
						'<BR><B>( FODA 044- Hash publicaci�n EPO 190606  -2006) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		}if(parametro==7) {
			titulo = "7. Publicaci�n con Criterio HASH (S�lo para Firma M.)";
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Permite publicar documentos con estatus Pre Negociable o Pre No Negociable,  Al realizar la carga de los documentos, el sistema de manera interna deber� generar la Clave HASH; del contenido del archivo que se cargar� mediante la utilizaci�n del algoritmo MD5, esta CLAVE HASH la EPO podr� visualizar en la pantalla Avisos de Notificaci�n.'  +
						'<BR><B>( FODA 044- Hash publicaci�n EPO 190606 -2006) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		} if(parametro==8) {
			titulo = "8. Operaci�n con Fecha Vencimiento Proveedor ";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Cuando la EPO publique documentos (Carga Individual o Masiva de Documentos Negociables) se solicitar� la Fecha de Vencimiento Proveedor, esta fecha debe ser menor a la Fecha de Vencimiento del documento.'  +
						'<BR>El proveedor al operar en la cadena parametrizada, deber� de consultar y operar con la fecha de vencimiento proveedor. '+
						'<BR>�  Las operaciones seleccionadas se les aplicar� una comisi�n por parte del Intermediario Financiero con el que realice la transacci�n '+
						'<BR>En la autorizaci�n del IF le deber� aparecer la fecha de vencimiento del documento y no con la que opero la PYME. (Fecha Vencimiento Proveedor) '+
						'<BR><B>( FODA 017 - Fecha Vencimiento Proveedor -2006) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		}else	if(parametro==9) {
			titulo = "9. Permitir publicaci�n de documentos vencidos";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Permite publicar los documentos que se encuentran en estatus Vencidos sin operar'  +
						'<BR>� Si la fecha de vencimiento capturada es menor  al par�metro �D�as Min. Para Descto� pero igual o mayor al d�a actual el documento se puede guardar con el estatus Vencido sin Operar. '+
						'<BR>� Si la fecha de vencimiento es mayor al par�metro �D�as Min. Para Descto� el documento  se puede guardar con el estatus Negociable '+
						'<BR><B>FODA 075 PEF Publicaci�n de todos los documentos -2007) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		}else if(parametro==10) {
			titulo = "10. Opera Pre-Afiliaci�n a Descuento PYMES";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>SI  : Indica que la Epo si podr� operar Pre-Afiliaci�n La PYME pueda solicitar su Afiliaci�n con alg�n IF habilitado en la Cadena Productiva'  +
						'<BR>El IF, consultara las solicitudes realizadas por la PyME y podr� asignar un estatus a la solicitud. '+						
						'<BR> �	Las PyMEs que el intermediario autorice quedaran afiliadas autom�ticamente. '+
						'<BR> �	Las PyMEs podr�n ejercer el descuento electr�nico con este intermediario. '+
						'<BR><B>FODEA 011 PreafiliacionaDesctoPEF -2007) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		} else if(parametro==11) {
			titulo = "11. Permitir Descuento Autom�tico PYMES";			
		} 	if(parametro==12) {
			titulo = "12. Publicaci�n EPO PEF (Digito Identificador)";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Generar un n�mero �nico por cada documento llamado "Digito Identificar" que permita a las EPOS del PEF el registro en el sistema SIAFF de la Tesofe de SHCP para registrar sus CLC (Cuentas por Liquidar Certificadas).'  +
						'<BR>Estructura Digito Identificador: '+						
						'<BR>� N�mero de EPO (IC_EPO) =  4 caracteres '+
						'<BR>� N�mero de EPO (IC_EPO) =  4 caracteres  '+
						'<BR>� Consecutivo de Publicaci�n (IC_DOCUMENTO):11 caracteres '+
						'<BR><B>(FODEA 045 NUMERO SIAFF-2008) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		}else if(parametro==13) {
			titulo = "13. Publicaci�n EPO PEF";
		}else if(parametro==14) {
			titulo = "14. Opera Factoraje con Mandato";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Mecanismo para otorgar financiamiento a empresas que no cuentan con una Cadena Productiva.'  +
						'<BR>Mediante las Cadenas Productivas se desarrollaran los procesos para que el Intermediario Financiero en su car�cter de Mandatario (EPO) de los Desarrolladores del sector de la construcci�n pueda publicar, recibir y aceptar notificaciones de Nafin. '+
						'<BR>La EPO podr� publicar documentos negociables con Tipo de Factoraje: �Mandato�, asignar� un Mandante y el Intermediario Financiero que operar� la cuenta por pagar y se har� el respectivo cargo de inter�s.   '+
						'<BR><B>FODEA 023 DESC ELECT- Factoraje con Mandato -2009) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		} else	if(parametro==15) {
			titulo = "15. Opera Factoraje Vencimiento Infonavit";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Mecanismo con la misma funcionalidad de Factoraje Distribuido (Beneficiario), adicionando que en la publicaci�n se asigne el Intermediario Financiero con el que se debe realizar el factoraje, sin el cobro de intereses (Tasa 0%).'  +
						'<BR>En la publicaci�n la EPO deber� indicar los siguientes datos: '+
						'<BR>a)Tipo de Factoraje : Vencimientos INFONAVIT '+
						'<BR>b) Nombre del IF '+
						'<BR>c) Nombre del Beneficiario '+
						'<BR>d) Porcentaje Beneficiario '+
						'<BR>e) Monto Beneficiario '+
						'<BR><B>FODEA 042 DESC ELECT- Factoraje al Vencimiento Infonavit -2009) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		} else	if(parametro==16) {
			titulo = "16. Opera Solicitud de Consentimiento de Cesi�n de Derechos";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Operaci�n de Ceder los Derechos de un contrato con el Gobierno Federal al IF, el cual ser� cedido por la PyME para conseguir financiamiento.'  +
						'<BR>� La PyME realiza una solicitud de Consentimiento de Cesi�n de Derechos de Cobro para conseguir financiamiento. '+
						'<BR>� La EPO acepta la solicitud de consentimiento de Cesi�n de Derechos y se vuelve un �COTRATO DE CESION�. '+
						'<BR>� La PyME autoriza el Contrato de Cesi�n. '+
						'<BR>� La IF autoriza el Contrato de Cesi�n autorizado por la PYME. '+
						'<BR>	 o	Dos TESTIGOS atestiguan la autorizaci�n del Contrato. '+
						'<BR>� La EPO acepta la notificaci�n del contrato de Cesi�n y notifica a la Ventanilla correspondiente.'+
						'<BR>� La VENTANILLA  realiza el re direccionamiento de pago. '+
						'<BR><B>(FODEA 037 Cesi�n de Derechos -2009) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		} else	if(parametro==17) {
			titulo = "17. Opera Factoraje 24 hrs";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Permite el descuento de sus documentos negociables a las PYMES de Cadenas Productivas fuera de horario de servicio..'  +
						'<BR>� La Pyme podr� operar fuera de los horarios de servicio siempre y cuando se hayan capturado las tasas del siguiente d�a h�bil.'+
						'<BR>�	El documento quedar� con estatus �Programado Pyme� y quedar� �programado para el siguiente d�a h�bil '+
						'<BR>Las operaciones que se realicen despu�s del horario de servicio: '+
						'<BR>� Se les aplicar� la tasa del siguiente d�a (h�bil).'+
						'<BR>�	Al plazo le ser� restado un d�a (h�bil) '+
						'<BR><B>( FODEA 016-DESC ELEC-Factoraje 24hxEPO-IF -2010)'+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		} else  if(parametro==18) {
			titulo = "18. Factoraje Vencido";			
		}else 	if(parametro==19) {
			titulo = "19. Opera Descuento Autom�tico � ultimo d�a antes del vencimiento";		
		}else 	if(parametro==20) {
			titulo = "20. Factoraje Distribuido";			
		}else 		if(parametro==21) {
			titulo = "21. Opera Tasa Especiales";
			descripcion = ['<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Permitir la Parametrizaci�n por EPO el uso de las Tasas Preferenciales y Negociadas a los Intermediarios Financieros para las PYMES seleccionadas..'  +
						'<BR><B>(FODEA 036 - DESC ELEC Tasas Preferenciales Nafin -2010) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		}else if(parametro==22) {
			titulo = "22. Hora para envi� de operaci�n a IFS";			
		} 	if(parametro==23) {
			titulo = "23. Mostrar Fecha de Autorizaci�n en Info. De Documentos ";
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Permite indicar si la EPO puede visualizar la Fecha de Autorizaci�n del IF en la pantalla de  Info. De Documentos.'  +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		}else 	if(parametro==24) {
			titulo = "24. Factoraje IF";
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Operaci�n de factoraje normal en donde la EPO pueda se�alar el Intermediario Financiero con el cual podr� la PYME descontar en DESCUENTO AUTOMATICO sus cuentas por pagar.'  +
						'<BR>�La EPO publicar� sus documentos especificando nafin electr�nico del Intermediario Financiero con el que se descontar�n los documentos. '+
						'<BR><B>(FODEA 026 DESC ELECT Factoraje con IF -2012) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		} 	if(parametro==25) {
			titulo = "25. Convenio �nico IF";
		} if(parametro==26) {
			titulo = "26. Opera Fideicomiso para Desarrollo de Proveedores";
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Permite operar el factoraje tradicional considerando como Intermediario a un Fideicomiso para cada EPO.'  +
						'<BR>Internamente los documentos se asignar�n a un Intermediario Financiero que aplicar� el re-descuento de los mismos con una tasa menor a la aplicada por el Fideicomiso.  '+
						'<BR><B> (FODEA 017 - DSCTO ELECT- Fideicomiso para el Desarrollo de Proveedores -2013) '+
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		} if(parametro==27) {
			titulo = "27. Validar duplicidad de documentos por monto";
			descripcion =[ '<table width="500" cellpadding="3" cellspacing="1" border="0">' +
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">' +
						'<BR>Permite validar por EPO la posible duplicidad que pudiera ' +
						'presentarse entre los documentos publicados a trav�s de NAFINET, Web Services y/o ' +
						'Enlaces y los registros existentes en el sistema.'  +
						'<BR>� Al tener activo este par�metro se enviar� notificaci�n de la publicaci�n ' +
						'de documentos posiblemente duplicados (estatus �Pendiente Duplicado�) ' + 
						'v�a correo electr�nico, a la lista de correos capturados en el par�metro 27.1.' +
						'</td></tr>'+
						'<tr><td class="formas" style="text-align: justify;"  colspan="3">&nbsp;' +
						'</td></tr></table>'];
		}
				
		 new Ext.Window({								
				id:'ventanaAyuda',
				modal: true,
				width: 520,			
				autoHeight: true,
				resizable: false,
				constrain: true,
				closable:false,	
				autoScroll:true,
				closeAction: 'hide',
				html: descripcion.join(''),				
				bbar: {
						xtype: 'toolbar',	buttonAlign:'center',	
						buttons: ['->',
							{	xtype: 'button',	text: 'Salir',	iconCls: 'icoLimpiar', 	id: 'btnCerraP', handler: function(){Ext.getCmp('ventanaAyuda').destroy();} },
							'-'							
						]
					}
			}).show().setTitle(titulo);
	//	}
	}
	/*-------------------------- Popup Dispersion --------------------------*/

	var elementos = [
	{
		xtype: 'panel',
		labelWidth: 126,
		layout: 'form',
		bodyStyle		: 'padding: 12px',
		items: [ 
			
			{
				xtype				: 'combo',
				id					: 'id_cmb_epo',
				name				: 'cmb_epo',
				hiddenName 		: 'cmb_epo',
				fieldLabel		: 'Nombre de la EPO',
				width				: 300,
				forceSelection	: true,
				triggerAction	: 'all',
				mode				: 'local',
				valueField		: 'clave',
				displayField	: 'descripcion',
				emptyText		: 'Seleccione EPO',
				store				: catalogoEpo,
				listeners: {
					select: function(combo, record, index) {
						noEpo = record.json.clave;
						
						fp.el.mask('Enviando...', 'x-mask-loading');			
						Ext.Ajax.request({
								url: '13forma09Ext.data.jsp',
								params: Ext.apply({
									informacion: 'ConsultaParametrosEPO',
									sNoEPO : noEpo
									}),				
								callback: procesarSuccessFailureConsultaParametrosEPO
						});
					}
				}
			}
			//,{	xtype: 'displayfield',	width: 170	}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{	width:600,frame:true,	colspan:'4',	border:true,	html:'<div align="center"><b>Parametros por EPO</b	><br>&nbsp;</div>'	}
			]
		},
		{
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				{		xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('1');  		} 	},				
				//1
				{	width: 40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 1 <br>&nbsp;</div>'	},				
				{	width:350,	frame:false,	border:false, 	html:'<div class="formas" align="left"><br> Opera Notas de Cr�dito<br>&nbsp;</div>'	},
				{  width:100,	xtype	:'radiogroup',	msgTarget:'side',	id:'id_sOperaNotasCredito',	name:'sOperaNotasCredito',	cls:'x-check-group-alt',	columns:[50, 50],
					items			: 
						[{	boxLabel	: 'SI',	name : 'sOperaNotasCredito', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperaNotasCredito', inputValue: 'N' }],
					listeners	: {
						change:{
							fn:function(){
								var valor =	(Ext.getCmp('id_sOperaNotasCredito')).getValue();
								if(valor.getGroupValue() == 'S'){
									Ext.getCmp('num_1.1').setVisible(true);Ext.getCmp('str_1.1').setVisible(true);
									Ext.getCmp('id_sAplicarNotasCredito').setVisible(true);Ext.getCmp('panel_1_1').setVisible(true);
								} 
								else {
									Ext.getCmp('num_1.1').setVisible(false);Ext.getCmp('str_1.1').setVisible(false);
									Ext.getCmp('id_sAplicarNotasCredito').setVisible(false);Ext.getCmp('panel_1_1').setVisible(false);
								}
							}
						}
					}
				}
			]
		},
		{			
			id: 'panel_1_1',	hidden: true,	xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
			//1.1
				{	id:'num_1.1',						hidden: true,	width: 40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 1.1 <br>&nbsp;</div>'	},				
				{	id:'str_1.1',						hidden: true,  width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Aplicar una Nota de Cr�dito a varios Documentos<br>&nbsp;</div>'	},
				{ 	id:'id_sAplicarNotasCredito',	hidden: true,  width:100, 	xtype:'radiogroup',	msgTarget:'side',	name:'sAplicarNotasCredito',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sAplicarNotasCredito', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sAplicarNotasCredito', inputValue: 'N' }]
				}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
			//2
				{		xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('2');  		} 	},					
				{	width: 40,	frame:false,	border:false,	html:'<div class="formas" align="center"> <br> 2 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Monto m�nimo resultante para la aplicaci�n de Notas de Cr�dito<br>&nbsp;</div>'	},
				{	width: 100,	xtype:'textfield',	msgTarget:'side',	id:'id_fn_monto_minimo',	name:'fn_monto_minimo',	maxLength	: 6	}
			]
		},		
		{			
			xtype:'panel', layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
					//3
				{		xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('3');  		} 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 3 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false, 	html:'<div class="formas" align="left"><br> Descuento Autom�tico Obligatorio (Obsoleto) <br>&nbsp;</div>'	},				
				{	width:100, 	xtype:'radiogroup',	msgTarget:'side',	id:'id_sDsctoObligatorio',	name:'sDsctoObligatorio', 	disabled: true,	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sDsctoObligatorio', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sDsctoObligatorio', inputValue: 'N' }]
				}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//4
				{		xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('4');  		} 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 4 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Descuento Autom�tico Din�mico (Obsoleto)<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sDsctoDinam', disabled: true,  name:'sDsctoDinam',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sDsctoDinam', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sDsctoDinam', inputValue: 'N' }]
				} 	
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				//5
				{		xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('5');  		} 	},
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 5 <br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Opera L�mite de L�nea por PyME<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sLimitePyme',	name:'sLimitePyme',	cls:'x-check-group-alt',	columns:[50, 50],
					items			: 
						[{	boxLabel	: 'SI',	name : 'sLimitePyme', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sLimitePyme', inputValue: 'N' }]
				}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				//6
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('6');  		} 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 6 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Publicaci�n con Firma Mancomunada<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sFimaManc',	name:'sFimaManc',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sFimaManc', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sFimaManc', inputValue: 'N' }]
				}
			]
		},
		
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				//7
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('7');  		} 	},
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 7 <br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Publicaci�n con Criterio HASH (S�lo para Firma M.)<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sCriterioHash',	name:'sCriterioHash',	cls:'x-check-group-alt',	columns:[50, 50],
					items	: 
						[{	boxLabel	: 'SI',	name : 'sCriterioHash', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sCriterioHash', inputValue: 'N' }]
				}	
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				//8
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('8');  		} 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 8 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Operaci�n con Fecha Vencimiento Proveedor<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sFechaVencPyme',	name:'sFechaVencPyme',	cls:'x-check-group-alt',	columns:[50, 50],
					items			: 
						[{	boxLabel	: 'SI',	name : 'sFechaVencPyme', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sFechaVencPyme', inputValue: 'N' }]
				}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//8.1
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 8.1 <br><br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Plazo para determinar d�as m�ximos adicionales a la fecha de vencimiento del proveedor *<br>&nbsp;</div>'	},
				{	width:60,	xtype:'textfield',	msgTarget:'side',	id:'id_diasMaximosAd',	name:'diasMaximosAd',	maxLength: 2	}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				
				//8.2
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"> <br><br> 8.2 <br><br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Plazo m�ximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento, cuyo plazo de pago al proveedor a partir de la publicaci�n sea menor al par�metro<br>&nbsp;</div>'	},
				{	width:60,	xtype:'textfield',	msgTarget:'side',	id:'id_PlazoMaxFechasVePyDc',	name:'PlazoMaxFechasVePyDc',	maxLength: 2	}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [ 
				//8.3
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"> <br><br> 8.3<br><br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Plazo m�ximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento, cuyo plazo de pago al proveedor a partir de la publicaci�n sea mayor al par�metro<br>&nbsp;</div>'	},
				{	width:60,	xtype:'textfield',	msgTarget:'side',	id:'id_PlazoMaxFechasVenPyDm',	name:'PlazoMaxFechasVenPyDm',	maxLength: 2	}	
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//9
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('9');  		} 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 9 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Permitir publicaci�n de documentos vencidos<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sPubDoctoVenc',	name:'sPubDoctoVenc',	cls:'x-check-group-alt',	columns:[50, 50],
					items	: 
						[{	boxLabel	: 'SI',	name : 'sPubDoctoVenc', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sPubDoctoVenc', inputValue: 'N' }]
				}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//10
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('10'); 	} 	},
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 10 <br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Opera Pre-Afiliaci�n a Descuento PYMES<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sOperPreafilia',	name:'sOperPreafilia',	cls:'x-check-group-alt',columns:[50, 50],
					items			: 
						[{	boxLabel	: 'SI',	name : 'sOperPreafilia', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperPreafilia', inputValue: 'N' }]
				}
			]
		},
		
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//11
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('11');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 11 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Permitir Descuento Autom�tico a PYMES<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sPermitirDescuentoAutomatico',	name:'sPermitirDescuentoAutomatico',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sPermitirDescuentoAutomatico', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sPermitirDescuentoAutomatico', inputValue: 'N' }]
				}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//12
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('12');  } 	},
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 12 <br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Publicaci�n EPO PEF (Digito Identificador)<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sPublicacionEPOPEF',	name:'sPublicacionEPOPEF',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sPublicacionEPOPEF', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sPublicacionEPOPEF', inputValue: 'N' }],
					listeners	: {
						change:{
							fn:function(){
								var valor =	(Ext.getCmp('id_sPublicacionEPOPEF')).getValue();
								if(valor.getGroupValue() == 'S'){
									Ext.getCmp('num_12.1').setVisible(true);					Ext.getCmp('str_12.1').setVisible(true);
									Ext.getCmp('id_sPubEPOPEFSIAFF').setVisible(true);		Ext.getCmp('panel_12_1').setVisible(true);
								} 
								else {
									Ext.getCmp('num_12.1').setVisible(false);					Ext.getCmp('str_12.1').setVisible(false);
									Ext.getCmp('id_sPubEPOPEFSIAFF').setVisible(false);	Ext.getCmp('panel_12_1').setVisible(false);
								}
							}
						}
					}
				}
			]
		},
		{
			id: 'panel_12_1',	hidden: true,	xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
					//12.1
				{	id:'num_12.1',	hidden:true,	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 12.1 <br>&nbsp;</div>'	},
				{	id:'str_12.1',	hidden:true,	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Utiliza SIAFF (Digito Identificador)<br>&nbsp;</div>'	},
				{	hidden:true,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sPubEPOPEFSIAFF',	name:'sPubEPOPEFSIAFF',	width:100,	cls:'x-check-group-alt',	columns:[50, 50],
					items			: 
						[{	boxLabel	: 'SI',	name : 'sPubEPOPEFSIAFF', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sPubEPOPEFSIAFF', inputValue: 'N' }]
				}
			]
		},

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//13
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('13');  } 	},
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 13 <br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Publicaci�n EPO PEF<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sPubEPOPEFFlag',	name:'sPubEPOPEFFlag',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sPubEPOPEFFlag', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sPubEPOPEFFlag', inputValue: 'N' }],
					listeners	: {
						change:{
							fn:function(){
								var valor =	(Ext.getCmp('id_sPubEPOPEFFlag')).getValue();
								if(valor.getGroupValue() == 'S'){
									Ext.getCmp('num_13_').setVisible(true);	Ext.getCmp('str_13_').setVisible(true);  Ext.getCmp('cmp_13_').setVisible(true);	Ext.getCmp('panel_13_').setVisible(true);
									Ext.getCmp('num_13.1').setVisible(true);	Ext.getCmp('str_13.1').setVisible(true); Ext.getCmp('id_sPubEPOPEFDocto1').setVisible(true);	
									Ext.getCmp('num_13.2').setVisible(true);	Ext.getCmp('str_13.2').setVisible(true); Ext.getCmp('id_sPubEPOPEFDocto2').setVisible(true);	
									Ext.getCmp('num_13.3').setVisible(true);	Ext.getCmp('str_13.3').setVisible(true); Ext.getCmp('id_sPubEPOPEFDocto3').setVisible(true);	
									Ext.getCmp('num_13.4').setVisible(true);	Ext.getCmp('str_13.4').setVisible(true); Ext.getCmp('id_sPubEPOPEFDocto4').setVisible(true);	
									
								} 
								else {
									Ext.getCmp('num_13_').setVisible(false);	Ext.getCmp('str_13_').setVisible(false);	Ext.getCmp('cmp_13_').setVisible(false);					Ext.getCmp('panel_13_').setVisible(false);
									Ext.getCmp('num_13.1').setVisible(false);	Ext.getCmp('str_13.1').setVisible(false);	Ext.getCmp('id_sPubEPOPEFDocto1').setVisible(false);	
									Ext.getCmp('num_13.2').setVisible(false);	Ext.getCmp('str_13.2').setVisible(false);	Ext.getCmp('id_sPubEPOPEFDocto2').setVisible(false);	
									Ext.getCmp('num_13.3').setVisible(false);	Ext.getCmp('str_13.3').setVisible(false);	Ext.getCmp('id_sPubEPOPEFDocto3').setVisible(false);	
									Ext.getCmp('num_13.4').setVisible(false);	Ext.getCmp('str_13.4').setVisible(false);	Ext.getCmp('id_sPubEPOPEFDocto4').setVisible(false);	
									
								}
							}
						}
					}	
				}				
			]
		},
	
		{			
			id: 'panel_13_',	hidden: true,	xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				{	id:'num_13_',	hidden : true,	width:52,	frame:true,		border:true,	html:'<div class="formas" align="left">&nbsp;  </div>'},
				{	id:'str_13_',	hidden : true,	width:350,	frame:true,		border:true,	html:'<div class="formas" align="left"> CAMPO</div>'	},
				{	id:'cmp_13_',	hidden : true,	width:110,	frame:true,		border:true,	html:'<div class="formas" align="left"> OBLIGATORIO </div>'	},
					//13.1
				{	id:'num_13.1',	hidden : true,	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 13.1 <br>&nbsp;</div>'	},
				{	id:'str_13.1',	hidden : true,	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Fecha de Recepci�n de Bienes y Servicios <br>&nbsp;</div>'	},
				{	hidden:true,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sPubEPOPEFDocto1',	name:'sPubEPOPEFDocto1',	width:110,	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sPubEPOPEFDocto1', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sPubEPOPEFDocto1', inputValue: 'N' }]
				},
				{	id:'num_13.2',	hidden : true,	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 13.2 <br>&nbsp;</div>'	},
				{	id:'str_13.2',	hidden : true,	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tipo de Compra (procedimiento) <br>&nbsp;</div>'	},
				{	hidden:true,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sPubEPOPEFDocto2',	name:'sPubEPOPEFDocto2',	width:110,	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sPubEPOPEFDocto2', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sPubEPOPEFDocto2', inputValue: 'N' }]
				},
				//13.3
				{	id:'num_13.3',	hidden : true,	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 13.3 <br>&nbsp;</div>'	},
				{	id:'str_13.3',	hidden : true,	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Clasificador por Objeto del Gasto <br>&nbsp;</div>'	},
				{	hidden:true,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sPubEPOPEFDocto3',	name:'sPubEPOPEFDocto3',	width:110,	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sPubEPOPEFDocto3', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sPubEPOPEFDocto3', inputValue: 'N' }]
				},	
				//13.4
				{	id:'num_13.4',	hidden : true,	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 13.4 <br>&nbsp;</div>'	},
				{	id:'str_13.4',	hidden : true,	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Plazo M�ximo <br>&nbsp;</div>'	},
				{	hidden:true,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sPubEPOPEFDocto4',	name:'sPubEPOPEFDocto4',	width:110,	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sPubEPOPEFDocto4', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sPubEPOPEFDocto4', inputValue: 'N' }]
				}
			]
		},		
		
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//14
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('14');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 14 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Opera Factoraje con Mandato<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sOperaFactorajeConMandato',	name:'sOperaFactorajeConMandato',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sOperaFactorajeConMandato', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperaFactorajeConMandato', inputValue: 'N' }]
				}
			]
		},
		
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
							
				//15
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('15');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 15 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Opera Factoraje Vencimiento Infonavit<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sOperaFactorajeVencimiento',	name:'sOperaFactorajeVencimiento',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sOperaFactorajeVencimiento', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperaFactorajeVencimiento', inputValue: 'N' }]
				}
			]
		},
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
					//16
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('16');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 16 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Opera Solicitud de Consentimiento de Cesi�n de Derechos<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sOperaSolicitudCesionDerechos',	name:'sOperaSolicitudCesionDerechos',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sOperaSolicitudCesionDerechos', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperaSolicitudCesionDerechos', inputValue: 'N' }]
				}
			]
		},

		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
					//17
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('17');  } 	},
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 17 <br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Opera Factoraje 24hrs<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sOperaFactoraje24hrs',	name:'sOperaFactoraje24hrs',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sOperaFactoraje24hrs', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperaFactoraje24hrs', inputValue: 'N' }]
				}				
			]
		},

		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				
				//18
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('18');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 18 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Factoraje al Vencimiento<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sOperaFactorajeVencido',	name:'sOperaFactorajeVencido',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sOperaFactorajeVencido', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperaFactorajeVencido', inputValue: 'N' }],
					listeners	: {
						change:{
							fn:function(){
								var valor =	(Ext.getCmp('id_sOperaFactorajeVencido')).getValue();
								if(valor.getGroupValue() == 'S'){
									Ext.getCmp('num_18_').setVisible(true);	Ext.getCmp('str_18_').setVisible(true); 	Ext.getCmp('cmp_18_').setVisible(true);					Ext.getCmp('panel_18_').setVisible(true);
									Ext.getCmp('num_18.1').setVisible(true);	Ext.getCmp('str_18.1').setVisible(true); 	Ext.getCmp('id_sDesAutoFactVenci').setVisible(true);	
								} 
								else {
									Ext.getCmp('num_18_').setVisible(false);	Ext.getCmp('str_18_').setVisible(false);	Ext.getCmp('cmp_18_').setVisible(false);					Ext.getCmp('panel_18_').setVisible(false);
									Ext.getCmp('num_18.1').setVisible(false);	Ext.getCmp('str_18.1').setVisible(false);	Ext.getCmp('id_sDesAutoFactVenci').setVisible(false);	
								}
							}
						}
					}		
				}		
			]
		},

		{			
			id: 'panel_18_',	hidden: true,	xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
					
				{	id:'num_18_',	hidden : true,	width:51,	frame:true,		border:false,	html:'<div class="formas" align="left">&nbsp;  </div>'},
				{	id:'str_18_',	hidden : true,	width:350,	frame:true,		border:false,	html:'<div class="formas" align="left"> CAMPO</div>'	},
				{	id:'cmp_18_',	hidden : true,	width:110,	frame:true,		border:false,	html:'<div class="formas" align="left"> OBLIGATORIO </div>'	},		
				//18.1
				{	id:'num_18.1',	hidden : true,	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 18.1 <br>&nbsp;</div>'	},
				{	id:'str_18.1',	hidden : true,	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Opera Descuendo Autom�tico - Factoraje al Vencimiento <br>&nbsp;</div>'	},
				{	hidden:true,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sDesAutoFactVenci',	name:'sDesAutoFactVenci',	width:110,	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sDesAutoFactVenci', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sDesAutoFactVenci', inputValue: 'N' }]
				}
			]
		},

		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//19
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('19');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 19 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Opera Descuento Autom�tico - �ltimo d�a antes del vencimiento<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sOperaDsctoAutUltimoDia',	name:'sOperaDsctoAutUltimoDia',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sOperaDsctoAutUltimoDia', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperaDsctoAutUltimoDia', inputValue: 'N' }]
				}
			]
		},

		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				
				//20
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('20');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 20 <br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Factoraje Distribuido<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sFactorajeDist',	name:'sFactorajeDist',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sFactorajeDist', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sFactorajeDist', inputValue: 'N' }],
					listeners	: {
						change:{
							fn:function(){
								var valor =	(Ext.getCmp('id_sFactorajeDist')).getValue();
								if(valor.getGroupValue() == 'S'){
									Ext.getCmp('num_20_').setVisible(true);	Ext.getCmp('str_20_').setVisible(true); 	Ext.getCmp('cmp_20_').setVisible(true);							Ext.getCmp('panel_20_').setVisible(true);
									Ext.getCmp('num_20.1').setVisible(true);	Ext.getCmp('str_20.1').setVisible(true); 	Ext.getCmp('id_sAutorizacionCuentasBanc').setVisible(true);	
								} 
								else {
									Ext.getCmp('num_20_').setVisible(false);	Ext.getCmp('str_20_').setVisible(false);	Ext.getCmp('cmp_20_').setVisible(false);							Ext.getCmp('panel_20_').setVisible(false);
									Ext.getCmp('num_20.1').setVisible(false);	Ext.getCmp('str_20.1').setVisible(false);	Ext.getCmp('id_sAutorizacionCuentasBanc').setVisible(false);
								}
							}
						}
					}		
				}
			]
		},

		
		{			
			id: 'panel_20_',	hidden: true,	xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				
				{	id:'num_20_',	hidden : true,	width:52,	frame:true,		border:false,	html:'<div class="formas" align="left">&nbsp;  </div>'},
				{	id:'str_20_',	hidden : true,	width:350,	frame:true,		border:false,	html:'<div class="formas" align="left"> CAMPO</div>'	},
				{	id:'cmp_20_',	hidden : true,	width:110,	frame:true,		border:false,	html:'<div class="formas" align="left"> OBLIGATORIO </div>'	},
				{	id:'num_20.1',	hidden : true,	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 20.1 <br>&nbsp;</div>'	},
				{	id:'str_20.1',	hidden : true,	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Autorizaci�n cuentas bancarias de entidad <br>&nbsp;</div>'	},
				{	hidden:true,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sAutorizacionCuentasBanc',	name:'sAutorizacionCuentasBanc',	width:110,	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sAutorizacionCuentasBanc', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sAutorizacionCuentasBanc', inputValue: 'N' }]
				}	
			]
		},

		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//21
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('21');  } 	},
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 21 <br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br>Opera Tasas Especiales<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sOperaTasasEspeciales',	name:'sOperaTasasEspeciales',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sOperaTasasEspeciales', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperaTasasEspeciales', inputValue: 'N' }]
				}
			]
		},
		
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//21.1
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 21.1  <br><br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Tipo Tasa  <br><br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	style: {
            width: '80%',
            marginLeft: '10px'
        },	align:'left',	id:'id_sTipoTasa',	name:'sTipoTasa',	cls:'x-check-group-alt',	columns:[90, 83,63],
					items: 
						[{	boxLabel	: ' Preferencial',	name : 'sTipoTasa', inputValue: 'P'	},{	boxLabel	: 'Negociada',	name : 'sTipoTasa', inputValue: 'NG'	}, {	boxLabel	: 'Ambas',	name : 'sTipoTasa', inputValue: 'A' }]
				}				
			]
		},
		
		{			
			xtype:'panel',	layout:'table',	width:610,	layoutConfig:{ columns: 6 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//22		
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('22');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 22 <br>&nbsp;</div>'	},
				{	width:260,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Hora para env�o de operaci�n a IF� s<br>&nbsp;</div>'	},
				{	width:100,	xtype:'textfield',	msgTarget:'side',	id:'id_sHoraEnvOpIf',	name:'sHoraEnvOpIf',	
					maxLength	: 5,	minLength	: 5,			regex			:/^(0[1-9]|1\d|2[0-3]):[0-5][0-9]$/,
					regexText	:'Por favor escriba correctamente la Hora en formato 24 hrs, hora dos puntos y minutos en el formato HH:MM donde: HH:representa las horas en un rango desde 00 hasta 24,  '+
									 'MM:representa los minutos en un rango desde 00 hasta 59 '
				}
				//,{	xtype: 'displayfield', width:40	}
			//	,{	xtype: 'displayfield', width:350	}
				,{	width:100,	frame:false,	border:false,	html:'<div class="formas" align="center"> * Formato 24 hrs</div>'	}
				
			]
		},
		
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//23
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('23');  } 	},
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 23 <br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br>Mostrar Fecha de Autorizaci�n IF en Info. de Documentos<br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sMostrarFechaAutorizacionIFInfoDoctos',	name:'sMostrarFechaAutorizacionIFInfoDoctos',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sMostrarFechaAutorizacionIFInfoDoctos', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sMostrarFechaAutorizacionIFInfoDoctos', inputValue: 'N' }]
				}
			]
		},
		
		{
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//24
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('24');  } 	},
				{	width:40,frame:false,	border:false,	html:'<div class="formas" align="center"><br> 24 <br>&nbsp;</div>'	},
				{	width:350,frame:false,	border:false,	html:'<div class="formas" align="left"><br> Factoraje IF <br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	msgTarget:'side',	id:'id_sOperaFactorajeIF',	name:'sOperaFactorajeIF',	cls:'x-check-group-alt',	columns:[50, 50],
					items: 
						[{	boxLabel	: 'SI',	name : 'sOperaFactorajeIF', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'sOperaFactorajeIF', inputValue: 'N' }]
				}
				
			]//items
		},
		//Fodea 010-2013				
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//25
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('25');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 25  <br><br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Convenio �nico IF <br><br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	style: {
            width: '80%',
            marginLeft: '10px'
        },	align:'left',	id:'cUnicoIF',	name:'cUnicoIF',	cls:'x-check-group-alt',	columns:[83, 88,78],
					items: 
						[{	boxLabel	: ' Afiliados',	name : 'cUnicoIF', inputValue: 'AF'	},{	boxLabel	: 'Por Afiliar',	name : 'cUnicoIF', inputValue: 'PA'	}, {	boxLabel	: 'Ambas',	name : 'cUnicoIF', inputValue: 'A' }]
				}				
			]
		},
		//Fodea 017-2013
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//26
				{	xtype: 'button',		id: 'btnAyuda',	iconCls: 'icoAyuda', 	handler: function(){			mostrarAyuda('26');  } 	},
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 26  <br><br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br> Opera Fideicomiso de Desarrollo de Proveedores  <br><br>&nbsp;</div>'	},
				{	width:100,	xtype:'radiogroup',	style: {
            width: '80%',
            marginLeft: '10px'
        },	align:'left',	id:'id_FideDePro',	name:'FideDePro',	cls:'x-check-group-alt',	columns:[50, 50],
				items: 
					[{	boxLabel	: 'SI',	name : 'FideDePro', inputValue: 'S'	}, {	boxLabel	: 'NO',	name : 'FideDePro', inputValue: 'N' }],
					listeners	: {
						change:{
							fn:function(){
								var valor =	(Ext.getCmp('id_FideDePro')).getValue();
								if(valor.getGroupValue() == 'S'){
									(Ext.getCmp('id_sLimitePyme')).setValue('N');  						 (Ext.getCmp('id_sLimitePyme')).disable();	//5
									(Ext.getCmp('id_sPubDoctoVenc')).setValue('N'); 					 (Ext.getCmp('id_sPubDoctoVenc')).disable();	//9
									(Ext.getCmp('id_sOperPreafilia')).setValue('N'); 					 (Ext.getCmp('id_sOperPreafilia')).disable();	//10
									(Ext.getCmp('id_sPermitirDescuentoAutomatico')).setValue('N');  (Ext.getCmp('id_sPermitirDescuentoAutomatico')).disable();	//11
									(Ext.getCmp('id_sPublicacionEPOPEF')).setValue('N'); 				 (Ext.getCmp('id_sPublicacionEPOPEF')).disable();	//12
									(Ext.getCmp('id_sPubEPOPEFFlag')).setValue('N'); 					 (Ext.getCmp('id_sPubEPOPEFFlag')).disable();	//13
									(Ext.getCmp('id_sOperaFactorajeConMandato')).setValue('N');     (Ext.getCmp('id_sOperaFactorajeConMandato')).disable();	//14
									(Ext.getCmp('id_sOperaFactorajeVencimiento')).setValue('N'); 	 (Ext.getCmp('id_sOperaFactorajeVencimiento')).disable();	//15									
									(Ext.getCmp('id_sOperaSolicitudCesionDerechos')).setValue('N'); (Ext.getCmp('id_sOperaSolicitudCesionDerechos')).disable();	//16
									(Ext.getCmp('id_sOperaFactorajeVencido')).setValue('N');        (Ext.getCmp('id_sOperaFactorajeVencido')).disable();	//18
									(Ext.getCmp('id_sFactorajeDist')).setValue('N');					 (Ext.getCmp('id_sFactorajeDist')).disable();	//20																										
									
								} 		else {									
									(Ext.getCmp('id_sLimitePyme')).enable();	//5
									(Ext.getCmp('id_sPubDoctoVenc')).enable();	//9
									(Ext.getCmp('id_sOperPreafilia')).enable();	//10
									(Ext.getCmp('id_sPermitirDescuentoAutomatico')).enable();	//11
									(Ext.getCmp('id_sPublicacionEPOPEF')).enable();	//12
									(Ext.getCmp('id_sPubEPOPEFFlag')).enable();	//13
									(Ext.getCmp('id_sOperaFactorajeConMandato')).enable();	//14
									(Ext.getCmp('id_sOperaFactorajeVencimiento')).enable();	//15									
									(Ext.getCmp('id_sOperaSolicitudCesionDerechos')).enable();	//16
									(Ext.getCmp('id_sOperaFactorajeVencido')).enable();	//18
									(Ext.getCmp('id_sFactorajeDist')).enable();	//20										
								}
							}
						}
					}
				}				
			]
		},	
		{			
			xtype:'panel',	layout:'table',	width:600,	layoutConfig:{ columns: 3 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [
				//26.1
				{	width:40,	frame:false,	border:false,	html:'<div class="formas" align="center"><br> 26.1  <br><br>&nbsp;</div>'	},
				{	width:350,	frame:false,	border:false,	html:'<div class="formas" align="left"><br>Tasa Contraprestaci�n (pago a NAFIN)  <br><br>&nbsp;</div>'	},				
				{	width: 80,	
					xtype:'bigdecimal',		
					blankText:'El tama�o m�ximo del campos es de 3 enteros 5 decimales',				
					maxValue: '999.99999', 
					allowDecimals: true,
					allowNegative: false,
					//allowBlank: 	false,
					hidden: 			false,				
					msgTarget: 		'side',
					anchor:			'-20',						
					format:			'000.00000',
					id:'id_tasaOperacion',	
					name:'tasaOperacion'	
				}
		  ]
		},
		{
			xtype:        'panel',
			layout:       'form',
			bodyStyle: 	  'padding: 2px;',
			items: [ 
								
								{ frame:false,	border:false,	html:'<div class="formas" align="center"><br> *S�lo aplica si el par�metro 8 esta activado. Si se deja vac�o no aplica <br>&nbsp;</div>'	}
			]
		},
		{
			xtype:'panel', layout:'table', width:600, layoutConfig:{ columns: 4 },
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [{ //27
				xtype:     'button',
				id:        'btnAyuda',
				iconCls:   'icoAyuda',
				handler: function(){
					mostrarAyuda('27');
				}
			},{
				width:     40,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="center"><br> 27 <br><br>&nbsp;</div>'
			},{
				width:     350,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"><br> Validar duplicidad de documentos por monto <br><br>&nbsp;</div>'
			},{
				width:     100,
				xtype:     'radiogroup',
				style: {
					width:      '80%',
					marginLeft: '10px'
				},
				align:     'left',
				id:        'id_valida_duplic',
				name:      'valida_duplic',
				cls:       'x-check-group-alt',
				columns:   [50, 50],
				items:[
					{boxLabel: 'SI', name: 'valida_duplic', inputValue: 'S'},
					{boxLabel: 'NO', name: 'valida_duplic', inputValue: 'N'}
				],
				listeners: {
					change: {
						fn:function(){
							var valor = (Ext.getCmp('id_valida_duplic')).getValue();
							if(valor != null){
								if(valor.getGroupValue() == 'S'){
									Ext.getCmp('panel_email_notificacion').show();
								} else{
									Ext.getCmp('panel_email_notificacion').hide();
									Ext.getCmp('id_email_notificacion').setValue('');
								}
							}
						}
					}
				}
			}]
		},
		{
			xtype:'panel', id: 'panel_email_notificacion', hidden: true, layout:'table', width:600, layoutConfig: {columns:3},
			defaults: {align:'center', bodyStyle:'padding:2px,'},
			items: [{ //27.1
				width:     40,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="center"><br> 27.1 <br><br>&nbsp;</div>'},
			{
				width:     350,
				frame:     false,
				border:    false,
				html:      '<div class="formas" align="left"><br> Correo(s) a notificar: <br><br>&nbsp;</div>'
			},{
				width:     185,
				xtype:     'textarea',
				msgTarget: 'side',
				anchor:    '-20',
				id:        'id_email_notificacion',
				name:      'email_notificacion',
				maxLength: 300
			}]
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id           : 'forma',
		frame        : false,
		width        : 600,
		autoHeight   : true,
		//align      : 'center',
		layout       : 'form',
		style        : 'margin:0 auto;',
		defaults     : {xtype: 'textfield', msgTarget: 'side'},
		items        : elementos,
		monitorValid : true,
		buttons: [
			{	
				text     : 'Grabar',
				id       : 'btnAceptar',
				width    : 90,
				formBind : true,
				handler  : function(boton, evento)
				{
					/********************** Validaciones ********************/
					var epo              = Ext.getCmp('id_cmb_epo');                  //(0)
					var notasCred        = Ext.getCmp('id_sOperaNotasCredito');       //(1)
					var aplicarNot       = Ext.getCmp('id_sAplicarNotasCredito');     //(1.1)
					var montoMin         = Ext.getCmp('id_fn_monto_minimo');          //(2)
					var dsctoOblig       = Ext.getCmp('id_sDsctoObligatorio');        //(3)
					var fchVenc          = Ext.getCmp('id_sFechaVencPyme');           //(8)
					var fch81            = Ext.getCmp('id_diasMaximosAd');            //(8.1)
					var fch82            = Ext.getCmp('id_PlazoMaxFechasVePyDc');     //(8.2)
					var fch83            = Ext.getCmp('id_PlazoMaxFechasVenPyDm');    //(8.3)
					var factDistr        = Ext.getCmp('id_sFactorajeDist');           //(20)
					var autorizac        = Ext.getCmp('id_sAutorizacionCuentasBanc'); //(20.1)
					var autorizac        = Ext.getCmp('id_sAutorizacionCuentasBanc'); //(20.1)
					var id_FideDePro     = Ext.getCmp('id_FideDePro');                //(26)
					var id_tasaOperacion = Ext.getCmp('id_tasaOperacion');            //(26.1)	

					//CAMPOS OBLIGATORIOS
					if(Ext.isEmpty(epo.getValue())){ //0
						Ext.MessageBox.alert('Mensaje de p�gina web','Debe de seleccionar una EPO' );
						epo.focus();
						return;
					}

					if(Ext.isEmpty(notasCred.getValue())){ //1
						Ext.MessageBox.alert('Mensaje de p�gina web','Debe de seleccionar SI o NO para poder parametrizar la Operaci�n con Notas de Cr�dito.' );
						notasCred.focus();
						return;
					}
	
					if( !Ext.isEmpty(notasCred.getValue()) &&  (notasCred.getValue()).getGroupValue() == 'S' ) { //2
	
						if (Ext.isEmpty(montoMin.getValue())) {
							Ext.MessageBox.alert('Mensaje de p�gina web','Debe indicar el Monto m�nimo resultante para la aplicaci�n de notas de cr�dito' );
							montoMin.focus();
							return;
						} else {
							if (isNaN(montoMin.getValue())) {
								Ext.MessageBox.alert('Mensaje de p�gina web','No es un numero valido para el monto minimo ' );
								montoMin.focus();
								return;
							}
							else if( montoMin.getValue() <= 0 ) {
								Ext.MessageBox.alert('Mensaje de p�gina web','El monto minimo debe ser mayor de cero' );
								montoMin.focus();
								return;
							}
						}
	
					}

					if(Ext.isEmpty(dsctoOblig.getValue())){ //3
						Ext.MessageBox.alert('Mensaje de p�gina web','Debe de seleccionar SI o NO para poder parametrizar el Descuento Automatico Obligatorio.' );
						dsctoOblig.focus();
						return;
					}

					if(Ext.isEmpty(fchVenc.getValue())){ //8
						Ext.MessageBox.alert('Mensaje de p�gina web','Debe de seleccionar SI o NO en Operaci�n con Fecha Vencimiento Proveedor para poder parametrizar la Operaci�n con Notas de Cr�dito.' );
						fchVenc.focus();
						return;
					}
					//////////////////////////////////////////////////
					
					if(fch81.getValue() !=''){
						if (isNaN(fch81.getValue())) {
							Ext.MessageBox.alert('Mensaje de p�gina web','No es un n�mero valido para determinar d�as m�ximos adicionales ' );
							fch81.focus();
							return;
						}
						if( fch81.getValue() <= 0 ) {
							Ext.MessageBox.alert('Mensaje de p�gina web','El monto minimo debe ser mayor de cero.' );
							fch81.focus();
							return;
						}
					}
					
					if(fch82.getValue() !=''){
						if (isNaN(fch82.getValue())) {
							Ext.MessageBox.alert('Mensaje de p�gina web','No es un numero valido para el Plazo m�ximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento, cuyo plazo de pago al proveedor a partir de la publicaci�n sea menor al par�metro 8.1 ' );
							fch82.focus();
							return;
						}
						if( fch82.getValue() <= 0 ) {
							Ext.MessageBox.alert('Mensaje de p�gina web','El monto minimo debe ser mayor de cero.' );
							fch82.focus();
							return;
						}
					}
					
					if(fch83.getValue() !=''){
						if (isNaN(fch83.getValue())) {
							Ext.MessageBox.alert('Mensaje de p�gina web','No es un n�mero valido para determinar d�as m�ximos adicionales ' );
							fch81.focus();
							return;
						}
						if( fch83.getValue() <= 0 ) {
							Ext.MessageBox.alert('Mensaje de p�gina web','El monto minimo debe ser mayor de cero	1' );
							fch83.focus();
							return;
						}
					}
					 
					if((id_FideDePro.getValue()).getGroupValue()=='S' &&  id_tasaOperacion.getValue() =='') {
						id_tasaOperacion.markInvalid('Es un campo obligatorio' );
						id_tasaOperacion.focus();
						return;
					}
					
					//SI NO APLICA CAMPO PRINCIPAL TAMPOCO APLICA CAMPO SECUNDARIO
					if(!Ext.isEmpty(notasCred.getValue()) && (notasCred.getValue()).getGroupValue() =='N' ){ //1 y 1.1
						Ext.getCmp('id_sAplicarNotasCredito').setValue('N');
					}
					if(!Ext.isEmpty(factDistr.getValue()) &&(factDistr.getValue()).getGroupValue() =='N' ){ //20 y 20.1
						Ext.getCmp('id_sAutorizacionCuentasBanc').setValue('N');
					}

					var valida_duplic = (Ext.getCmp('id_valida_duplic')).getValue();    //27
					var email_notific = Ext.getCmp('id_email_notificacion').getValue(); //27.1
					if(valida_duplic != null){
						/*if(valida_duplic.getGroupValue() == 'S' && email_notific == ''){ //27.1
							Ext.MessageBox.alert('Mensaje de p�gina web', 'El campo Correo(s) a notificar es obligatorio.');
							return;
						}*/
						if(valida_duplic.getGroupValue() == 'S' && email_notific.length > 300){ //27.1
							Ext.MessageBox.alert('Mensaje de p�gina web', 'El tama�o m�ximo para el campo Correo(s) a notificar es de 300.');
							return;
						}
					}
					Ext.Ajax.request({
						url: '13forma09Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ModificaParametrosEPO',
							sNoEPO: noEpo
						}),
						callback: procesarSuccessFailureModificaEPO
					});
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id      : 'contenedorPrincipal',
		applyTo : 'areaContenido',
		width   : 890,
		height  : 'auto',
		style   : 'margin:0 auto;',
		items   : [ 
			NE.util.getEspaciador(20),
			fp
			//ventanaAyuda
		]
	});

	catalogoEpo.load();
});