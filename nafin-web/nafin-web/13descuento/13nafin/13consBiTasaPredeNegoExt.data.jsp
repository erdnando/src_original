 <%@ page contentType="application/json;charset=UTF-8"    
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.pdf.*,
		com.netro.descuento.*, 
		com.netro.model.catalogos.*,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray, net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion	  =	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion       =	(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
String infoRegresar  = "";
int start=0; int limit=0;
JSONObject jsonObj = new JSONObject();	

JSONArray jsObjArray = new JSONArray();
// ------------------------------------- Catalogos ------------------------------------- //
 if (informacion.equals("CatalogoEPO") ) {
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("cg_razon_social");
		
		List elementos = cat.getListaElementos(); 
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsObjArray.add(JSONObject.fromObject(ec));
			}
		}
		infoRegresar= "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
 }
 
 else if(informacion.equals("CatalogoIF")){
	try {		
		String sNoEPO = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String sMoneda= (request.getParameter("moneda")==null)?"":request.getParameter("moneda");
		
		Vector lovDatos = null;
		Vector lovRegistro = null;
		String lsClave = "";
		String lsNombre = "";
		HashMap resultado = null;
		List listIfs = new ArrayList();
		AutorizacionTasas BeanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
		
			if(!sNoEPO.equals("") && !sMoneda.equals("")) 
				lovDatos = BeanTasas.getIntermediario(sMoneda,sNoEPO);
			
			for (int i= 0; i < lovDatos.size(); i++) {
				resultado = new HashMap();
				lovRegistro = (Vector) lovDatos.elementAt(i);
				lsClave  = lovRegistro.elementAt(0).toString();
				lsNombre = lovRegistro.elementAt(1).toString();
				resultado.put("lsClave",lsClave);
				resultado.put("lsNombre",lsNombre);
				listIfs.add(resultado);
			} 
			jsObjArray = JSONArray.fromObject(listIfs);
			infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
			
		} 	catch(Exception e) { 
					throw new AppException("Error en la recuperación de datos", e);
			}
	}
	
	else if (informacion.equals("CatalogoMoneda")) {
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_moneda");
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("ic_moneda");
		cat.setValoresCondicionIn("1,54",Integer.class);
		infoRegresar = cat.getJSONElementos();		
	}
// ------------------------------------- END Catalogos ------------------------------------- //	
	
	else if (informacion.equals("VerificaNafElectronico")) {
		String sNoEPO  = (request.getParameter("noEpo")==null)?"":request.getParameter("noEpo");
		String sNoNafin= (request.getParameter("noNafin")==null)?"":request.getParameter("noNafin");
		String ic_pyme = "", nombrePyme = "";
		try {
		
			if(sNoEPO != null &&  !sNoEPO.equals("")){
				if(!Comunes.esNumeroEnteroPositivo(sNoEPO))	{
					throw new AppException("fondoJuniorValidacionSelPyme :: ERROR ::.. El numero de cliente no es un número valido");//error 
				}
			}
			
			HashMap resultado = null;
			List listPymes = new ArrayList();
			AutorizacionTasas BeanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
			listPymes = BeanTasas.verificaNafinElectronico(sNoNafin,sNoEPO);
	
			if(!listPymes.isEmpty()){
				jsObjArray = JSONArray.fromObject(listPymes);
				infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		  }
		} catch(Exception e) {
			throw new AppException("AutorizacionTasasBean_VerificaNafElectronico :: ERROR ::..", e);
		  }
	}
	 
	else if (informacion.equals("NombreProveedor")) {
		String sNoNafin= (request.getParameter("noNafin")==null)?"":request.getParameter("noNafin");
		try {
		
			if(sNoNafin != null &&  !sNoNafin.equals("")){
				if(!Comunes.esNumeroEnteroPositivo(sNoNafin))	{
					throw new AppException(" ERROR ::.. El numero de cliente no es un número valido");//error 
				}
			}
			
			HashMap resultado = null;
			List listPymes = new ArrayList();
			AutorizacionTasas BeanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
			listPymes = BeanTasas.busquedaProveedor(sNoNafin);
	
			if(!listPymes.isEmpty()){
				jsObjArray = JSONArray.fromObject(listPymes);
				infoRegresar = "{\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"}";
		  }
		} catch(Exception e) {
			throw new AppException("AutorizacionTasasBean_busquedaProveedor :: ERROR ::..", e);
		  }
	}
	
	else if (informacion.equals("CatalogoBuscaAvanzada"))
	{
		try {
			String val_nombre =  (request.getParameter("nombrePyme")==null)?"":request.getParameter("nombrePyme");
			String val_rfc 	=  (request.getParameter("rfcPyme")==null)?"":request.getParameter("rfcPyme");
	
			if(!val_nombre.equals("") && (val_nombre.indexOf("*") == -1)){val_nombre = "*" + val_nombre + "*";}
			if(!val_rfc.equals("") && (val_nombre.indexOf("*") == -1)){val_rfc = "*" + val_rfc + "*";}
			
			//Instanciación para uso del EJB
			AutorizacionTasas BeanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
			List informacion_proveedores = BeanTasas.busquedaAvanzadaProveedor(val_nombre, val_rfc);
			//List lstInfoProv = new ArrayList();
			if(!informacion_proveedores.isEmpty())
			{
			  if(informacion_proveedores.size() < 500 && informacion_proveedores.size() > 0)
			  {
				 //AQUI BARRIDO DE LISTA PARA FORMAR OTRA LISTA DE HASHMAPS
				
				 jsObjArray = JSONArray.fromObject(informacion_proveedores);
				 jsonObj.put("registros",jsObjArray.toString());
				 jsonObj.put("msgRegistros", "");
				 infoRegresar = jsonObj.toString();
				 
			  }
			  else
			  {
				 jsonObj.put("msgRegistros", "Demasiado registros encontrados, por favor sea mas específico en la búsqueda");
				 jsonObj.put("registros","");
			  }
			}
			else
			{
				jsonObj.put("msgRegistros", "No se encontró información");
				jsonObj.put("registros","");
			}
			
			jsonObj.put("success",new Boolean(true));
			infoRegresar = jsonObj.toString();
		} catch(Exception e) {
			throw new AppException("AutorizacionTasasBean_busquedaProveedor :: ERROR ::..", e);
		  }
	}
	
	else if(informacion.equals("ConsultaGrid") || informacion.equals("GenerarCSV")  )
	{
		String ic_tasa		 = (request.getParameter("cmb_tasa")==null)?"":request.getParameter("cmb_tasa");
		String ic_epo  	 = (request.getParameter("cmb_epo")==null)?"":request.getParameter("cmb_epo");
		String ic_if		 = (request.getParameter("nombre_if")==null)?"":request.getParameter("nombre_if");
		String moneda		 = (request.getParameter("cmb_moneda")==null)?"":request.getParameter("cmb_moneda");
		String iProveedor  = (request.getParameter("ic_pyme")==null)?"0":request.getParameter("ic_pyme");
		String estatus		 = (request.getParameter("cmb_estatus")==null)?"":request.getParameter("cmb_estatus");
		String fchRegistro1= (request.getParameter("txtFchReg1")==null)?"":request.getParameter("txtFchReg1");
		String fchRegistro2= (request.getParameter("txtFchReg2")==null)?"":request.getParameter("txtFchReg2");
		String aplicaFloating = (request.getParameter("aplicaFloating") != null) ? request.getParameter("aplicaFloating") : "";			
		
		ConsBitaTasaPrefeNegoNafinExt paginador = new ConsBitaTasaPrefeNegoNafinExt();
		paginador.setTipoTasa(ic_tasa);
		paginador.setEpo(ic_epo);
		paginador.setClaveIF(ic_if);
		paginador.setMoneda(moneda);
		paginador.setProveedor(iProveedor);
		paginador.setEstatus(estatus);
		paginador.setFecha_registro_de(fchRegistro1);
		paginador.setFecha_registro_a(fchRegistro2);
		paginador.setAplicaFloating(aplicaFloating);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
		try
		{
			 start = Integer.parseInt(request.getParameter("start"));
			 limit = Integer.parseInt(request.getParameter("limit"));	
			
		} catch(Exception e)
		  { System.out.println("Error en parametros de paginación"); }
		  
		try 
		{
			if(informacion.equals("ConsultaGrid"))
			{
				if(operacion.equals("Generar")) 
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				
				String  consultar = queryHelper.getJSONPageResultSet(request,start,limit);	
				jsonObj = JSONObject.fromObject(consultar);
				infoRegresar=jsonObj.toString();				
			}
			
			else if (informacion.equals("GenerarCSV"))
			{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
		} catch(Exception e) 
			{
				throw new AppException("Error en la recuperacion de datos!", e);
			}
		
}

System.out.println("infoRegresar============"+infoRegresar); 
%>
<%=infoRegresar%>