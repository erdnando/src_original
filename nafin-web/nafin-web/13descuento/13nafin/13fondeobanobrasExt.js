Ext.onReady(function() {


	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarConsultaTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}								
			if(store.getTotalCount() > 0) {	
				
				el.unmask();					
			} else {							
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	//****************Consulta de Totales **********************************
	var consultaTotalesData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13fondeobanobras.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [	
		   {	name: 'TOTAL_DOCTOS' },
			{	name: 'TOTAL_MONTOS'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTotalesData(null, null, null);					
				}
			}
		}		
	});
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		store: consultaTotalesData,
		id: 'gridTotales',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: '',	
		align: 'left',
		hidden: true,
		columns: [	
			{
				header: 'Total de Documentos',
				tooltip: 'Total de Documentos',
				dataIndex: 'TOTAL_DOCTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},		
			{
				header: 'Total Monto de Fondeo ',
				tooltip: 'Total Monto de Fondeo ',
				dataIndex: 'TOTAL_MONTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},				
			{
				header: '',
				tooltip: '',				
				sortable: true,
				width: 580,			
				resizable: true,				
				align: 'left'	
			}	
		],	
		stripeRows: true,
		loadMask: true,
		height: 100,
		width: 900,		
		frame: true	
	});
	
	 //***************Consulta General***************************+
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnArchivoPDF').enable();
				Ext.getCmp('btnArchivoCSV').enable();
				
				consultaTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotales'
					})
				});

				el.unmask();					
			} else {	
				Ext.getCmp('btnArchivoPDF').disable();
				Ext.getCmp('btnArchivoCSV').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13fondeobanobras.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'FECHA_OPERACION'},
			{	name: 'FECHA_VENCIMIENTO'},
			{	name: 'PLAZO'},
			{	name: 'MONEDA'},
			{	name: 'TOTAL_DOCUMENTOS'},
			{	name: 'MONTO_FONDEO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Fecha Operaci�n',
				tooltip: 'Fecha Operaci�n',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Total de Documentos',
				tooltip: 'Total de Documentos',
				dataIndex: 'TOTAL_DOCUMENTOS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'				
			},			
			{
				header: 'Monto de Fondeo',
				tooltip: 'Monto de Fondeo',
				dataIndex: 'MONTO_FONDEO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}		
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
					{
					xtype: 'button',
					text: 'Generar Archivo',					
					tooltip:	'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnArchivoCSV',
					handler: function(boton, evento) {												
							Ext.Ajax.request({
							url: '13fondeobanobras.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoCSV'
							}),							
							callback: procesarDescargaArchivos
						});						
					}
				},
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnArchivoPDF',
					handler: function(boton, evento) {
						var barraPaginacionA = Ext.getCmp("barraPaginacion");						
							Ext.Ajax.request({
							url: '13fondeobanobras.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{							
								informacion: 'ArchivoPDF'
							}),
							callback: procesarDescargaArchivos
						});						
					}
				}
			]
		}
	});	
	
	//********************CRITERIOS DE BUSQUEDA**************************
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13fondeobanobras.data.jsp',
			baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {			
			exception: NE.util.mostrarDataProxyError
		}
	});
	
	var elementosForma =[		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_vencimiento_inicio',
					id: 'df_vencimiento_inicio',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_vencimiento_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_vencimiento_fin',
					id: 'df_vencimiento_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_vencimiento_inicio',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_operacion_inicio',
					id: 'df_operacion_inicio',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_operacion_fin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_operacion_fin',
					id: 'df_operacion_fin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_operacion_inicio',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store : catalogoMoneda,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		title: 'Operaciones Etiquetadas como OBRA- PUBLICA de EPOS PEF ',
		frame: true,
		//collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {					
				
				
					var df_vencimiento_inicio = Ext.getCmp('df_vencimiento_inicio');
					var df_vencimiento_fin = Ext.getCmp('df_vencimiento_fin');
					var df_operacion_inicio = Ext.getCmp('df_operacion_inicio');
					var df_operacion_fin = Ext.getCmp('df_operacion_fin');
					
					if(Ext.isEmpty(df_vencimiento_inicio.getValue()) &&  Ext.isEmpty(df_vencimiento_fin.getValue())
						&& Ext.isEmpty(df_operacion_inicio.getValue()) && Ext.isEmpty(df_operacion_fin.getValue())){						
						Ext.MessageBox.alert('Mensaje','Seleccione un rango de fechas');
						return;						
					}					
					
					if(!Ext.isEmpty(df_vencimiento_inicio.getValue()) || !Ext.isEmpty(df_vencimiento_fin.getValue())){
						if(Ext.isEmpty(df_vencimiento_inicio.getValue())){
							df_vencimiento_inicio.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_vencimiento_inicio.focus();
							return;
						}
						if(Ext.isEmpty(df_vencimiento_fin.getValue())){
							df_vencimiento_fin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_vencimiento_fin.focus();
							return;
						}
					}	
										
					
					if(!Ext.isEmpty(df_operacion_inicio.getValue()) || !Ext.isEmpty(df_operacion_fin.getValue())){
						if(Ext.isEmpty(df_operacion_inicio.getValue())){
							df_operacion_inicio.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_operacion_inicio.focus();
							return;
						}
						if(Ext.isEmpty(df_operacion_fin.getValue())){
							df_operacion_fin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_operacion_fin.focus();
							return;
						}
					}
			
					var df_vencimiento_inicio_ = Ext.util.Format.date(df_vencimiento_inicio.getValue(),'d/m/Y');
					var df_vencimiento_fin_ = Ext.util.Format.date(df_vencimiento_fin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(df_vencimiento_inicio.getValue())){
						if(!isdate(df_vencimiento_inicio_)) { 
							df_vencimiento_inicio.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_vencimiento_inicio.focus();
							return;
						}
					}
					if( !Ext.isEmpty(df_vencimiento_fin.getValue())){
						if(!isdate(df_vencimiento_fin_)) { 
							df_vencimiento_fin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_vencimiento_fin.focus();
							return;
						}
					}
									
					var df_operacion_inicio_ = Ext.util.Format.date(df_operacion_inicio.getValue(),'d/m/Y');
					var df_operacion_fin_ = Ext.util.Format.date(df_operacion_fin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(df_operacion_inicio.getValue())){
						if(!isdate(df_operacion_inicio_)) { 
							df_operacion_inicio.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_operacion_inicio.focus();
							return;
						}
					}
					
					if( !Ext.isEmpty(df_operacion_fin.getValue())){
						if(!isdate(df_operacion_fin_)) { 
							df_operacion_fin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_operacion_fin.focus();
							return;
						}
					}			
										
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							informacion: 'Consultar'
						})
					});
				
				}							
					
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13fondeobanobrasExt.jsp';					
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [					
			NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20)			
		]
	});
	
	catalogoMoneda.load();
});	
