/*Consolidado de Tasas */

Ext.onReady(function() {
	var ic_pyme = 0 ;
/*--------------------------------- HANDLERS -------------------------------*/

	/****** Handler�s Grid�s *******/
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		pnl.el.unmask();
		var fp = Ext.getCmp('forma');
		
		if (arrRegistros != null) 
		{
			if (!grid.isVisible()) {
				grid.show();
			}
			
			var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivo");
			var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivo');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');		
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var el = grid.getGridEl();
			btnBajarArchivo.hide();
			btnBajarPDF.hide();
			btnGenerarArchivo.enable();	// habilito los botones
			btnImprimirPDF.enable();	
				
			if (store.getTotalCount() > 0 ) {	// si la consulta tiene un store
				el.unmask();						//quito la mascara
			}
			if (arrRegistros == '')  {
				el.mask('No se encontr� ning�n registro', 'x-mask');
				btnGenerarArchivo.disable();
				btnImprimirPDF.disable();
			}
			else if (arrRegistros != '') {
				btnGenerarArchivo.enable();
				btnImprimirPDF.enable();
			}
		}	
	}
	
	 var procesarSuccessFailureGenerarPDF = function(opts, success, response) 
    {
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnBajarPDF.setIconClass('icoPdf');
		btnImprimirPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
 
  	var procesarSuccessFailureGenerarCSV = function(opts, success, response) 
    {
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		btnBajarArchivo.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
 
 
		/**** End Handler�s Grid�s ****/
		
	var procesarCatalogoTasa = function(store, arrRegistros, opts) {
		if (!grid.isVisible()) 	
			grid.hide();
		if(store.getTotalCount() > 0) { 
			Ext.getCmp("id_cmb_tasa").setValue((catalogoTasa.getRange()[0].data).clave);

		}
	}
	
	var procesarCatalogoMoneda = function(store, arrRegistros, opts) {
		if (!grid.isVisible()) 	
			grid.hide();
	}
	
	
/*--------------------------------- STORE'S -------------------------------*/
	var catalogoTasa = new Ext.data.JsonStore({
	   id				: 'catTasa',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13consulta05ext.data.jsp',
		baseParams	: { informacion: 'CatalogoTasa'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoTasa,
			exception: NE.util.mostrarDataProxyError
		}
	});
	var catalogoMoneda = new Ext.data.JsonStore({
	   id				: 'catMoneda',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13consulta05ext.data.jsp',
		baseParams	: { informacion: 'CatalogoMoneda'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoMoneda,
			exception: NE.util.mostrarDataProxyError
		}
	});
	
//-----------------------------------------------------------------------------//	
	
	/****** Store�s Grid�s *******/	
	
	var consultaData   = new Ext.data.GroupingStore({		
		root:'registros',	
		url:'13consulta05ext.data.jsp',	
		baseParams:{informacion:'ConsultaGrid'},	
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',		
				fields: [	
					{name:'NOMBREEPO'},
					{name: 'NOMBREIF' },	
					{name: 'CG_REL_MAT' },	
					{name: 'FN_PUNTOS'  },		
					{name: 'IC_TASA' }
				]
		}),
		autoLoad: false,
		groupField: 'NOMBREEPO',
		sortInfo:{ field: 'NOMBREEPO', direction: "DESC"},
		listeners: 
		{
			load: procesarConsultaData,
			exception: NE.util.mostrarDataProxyError
		}
		
	});

	/****** End Store�s Grid�s *******/
	
	
	/*********** Grid�s *************/

	
	var grid = new Ext.grid.EditorGridPanel({
		title:'',
		id: 'grid',
		store: consultaData,
		stripeRows: true,
		loadMask:false,
		hidden: true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		style: ' margin:0 auto;',
		height:400,
		width:650,			
		frame:true, 
		header:true,						
		columns: [
			{
				header:		'EPO',
				tooltip:		'',			
				dataIndex:	'NOMBREEPO',	
				hidden: true,
				sortable:true,	
				resizable:true,	
				width:200,
				align:'center'	
			},
			{
				header:		'Intermediario Financiero',
				tooltip:		'Intermediario Financiero',
				dataIndex:	'NOMBREIF',	
				sortable:true,	
				resizable:true,	
				width:250,
				align:'left'
			},
			{
				header:		'Nombre de la Tasa',
				tooltip: 	'Nombre de la Tasa',
				dataIndex:	'IC_TASA',	
				sortable:true,
				resizable:true,	
				width:150, 
				align:'right'				
			},
			{
				header:		'Fecha', 
				tooltip: 	'Fecha',	
				dataIndex: 	'CG_REL_MAT',	
				sortable:true, 
				width:90, 	
				align:'right'
			},
			{
				header:		'Valor de la Tasa',
				tooltip:		'Valor de la Tasa',
				dataIndex:	'FN_PUNTOS',		
				sortable:true, 
				width:120, 
				align:'right',
				renderer: Ext.util.Format.numberRenderer('%0.00000')
			}			
		],	
		view: new Ext.grid.GroupingView({
            forceFit:false,
            groupTextTpl: '{text}'					 
        }),
		frame: true,		
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) 
					{
						boton.disable();
						boton.setIconClass('loading-indicator');
						totalProperty:'total',
						Ext.Ajax.request	({
							url: '13consulta05ext.data.jsp',	
							params: Ext.apply (fp.getForm().getValues(),{informacion: 'GeneraCSV'}),
							callback: procesarSuccessFailureGenerarCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo ',
					id: 'btnBajarArchivo',
					width: 10,
					hidden: true
				},
				'-',
				{
				
					xtype: 'button',
					text: 'Imprimir PDF',	
					id: 'btnImprimirPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						totalProperty:'total',
						Ext.Ajax.request({
							url: '13consulta05ext.data.jsp', 
							params: Ext.apply (fp.getForm().getValues(),{informacion: 'GeneraPDF'}),								
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF ',
					id: 'btnBajarPDF',
					width: 5, 
					hidden: true
				}				
			]
		}
	});

		/****  End Grid�s ****/
	
	var elementosForma = [
	{
		xtype: 'panel',
		columnWidth:.10, 
		labelWidth:90,
		labelAlign:'right',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		layout: 'form',
		items: [
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
						xtype				: 'combo',
						id					: 'id_cmb_moneda',
						name				: 'cmb_moneda',
						hiddenName 		: 'cmb_moneda',
						fieldLabel		: 'Moneda',
						width				: 150,
						forceSelection	: true,
						triggerAction	: 'all',
						mode				: 'local',
						allowBlank		: false,
						valueField		: 'clave',
						displayField	: 'descripcion',
						emptyText		: 'Seleccione Moneda',
						store				: catalogoMoneda,
						listeners: {
							select: function(combo,record,index) {
								Ext.getCmp('id_cmb_tasa').reset();
								catalogoTasa.load({ params : Ext.apply({ 	ic_moneda: record.json.clave }) });
								grid.hide();
							}
						}
				},
			   {
					xtype: 'displayfield',
					value: 'Tasa:',
					width: 50
				},
				{
						xtype				: 'combo',
						id					: 'id_cmb_tasa',
						name				: 'cmb_tasa',
						hiddenName 		: 'cmb_tasa',
						fieldLabel		: '',
						width				: 250,
						forceSelection	: true,
						triggerAction	: 'all',
						mode				: 'local',
						valueField		: 'clave',
						displayField	: 'descripcion',
						emptyText		: 'Seleccionar',
						store				: catalogoTasa,
						listeners: {
							select: function(combo) {
								grid.hide();
							}
						}
				}
			]
		}]
	}];
			
/*******************Componentes*******************************/	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		frame:true,
		style: ' margin:0 auto;',
		title: 'Consolidado de Tasas',
		collapsible: true,		///////////
		titleCollapse: true,		//////////
		bodyStyle: 'padding: 6px',
		labelWidth: 90,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items:	elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: false,
				handler: function(boton, evento) {
					grid.hide();					
					pnl.el.mask('Enviando...', 'x-mask-loading');
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues())
					});						
					
				} //fin handler
			} //fin boton Consultar
		]							
	}); 
	
	
	 var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fp, 
			NE.util.getEspaciador(20),
			grid  			
		]
	});
	
	
	catalogoMoneda.load();
	
});//Fin de funcion principal
