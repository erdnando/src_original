<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

String claveDocumento = (request.getParameter("ic_documento") == null) ? "" : request.getParameter("ic_documento");
JSONObject jsonObj = new JSONObject();

try {
	if (!claveDocumento.equals(""))	{
		ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
		//	Variables de uso local
		int numRegistros = 0;
		int ordenAplicacion = 0;
				//	De despliegue
		String nombreEpo = "";
		String nombreIF = "";
		String numeroDocto = "";
		String fechaEmision = "";
		String fechaVencimiento = "";
		double dblImporteRecibir = 0;
		String producto = null;
		String fechaSeleccion = null;
		String fechaOperacion = null;
		String numMensualidad = null;
		String numCredito = null;
		String tipoContrato = null;
		String numPrestamo = null;
		double dblImporteAplicadoCredito = 0;
		double dblSaldoCredito = 0;
		String nombreMoneda = "";
		double dblMontoPago = 0;
		double dblInteresPago = 0;
		String monedaClave = "";
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		
		Vector vDetalleDocumentoAplicado = BeanSeleccionDocumento.ovgetDetalleDocumentoAplicado("", claveDocumento);

		ordenAplicacion = 0;	
		numRegistros = 0;

		for (int i = 0; i < vDetalleDocumentoAplicado.size(); i++) {
			numRegistros++;
			ordenAplicacion++;
			Vector vDatosDetalleDocumentoAplicado = (Vector) vDetalleDocumentoAplicado.get(i);

			numeroDocto = vDatosDetalleDocumentoAplicado.get(0).toString();
			dblImporteRecibir = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(1).toString());
			producto = vDatosDetalleDocumentoAplicado.get(2).toString();
			nombreIF = vDatosDetalleDocumentoAplicado.get(3).toString();
			fechaSeleccion = vDatosDetalleDocumentoAplicado.get(4).toString();
			fechaOperacion = vDatosDetalleDocumentoAplicado.get(5).toString();
			numMensualidad = vDatosDetalleDocumentoAplicado.get(6).toString();
			numCredito = vDatosDetalleDocumentoAplicado.get(7).toString();
			tipoContrato = vDatosDetalleDocumentoAplicado.get(8).toString();
			numPrestamo = vDatosDetalleDocumentoAplicado.get(9).toString();
			dblImporteAplicadoCredito = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(10).toString());
			dblSaldoCredito = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(11).toString());
			monedaClave = vDatosDetalleDocumentoAplicado.get(12).toString();
			if(numRegistros == 1) {
				contenidoArchivo.append("Número de documento,Monto aplicado\n");
				contenidoArchivo.append(numeroDocto + "," + dblImporteRecibir + "\n\n\n");
		
				contenidoArchivo.append("Producto,IF,Orden de aplicación,Fecha de aplicación," +
						"Fecha de operacion crédito,Número de mensualidad," +
						"Número de pedido/disposición,Tipo de contrato/operación,Número de prestamo NAFIN," +
						"Importe aplicado a crédito,Saldo del crédito\n");
			}
						contenidoArchivo.append(producto + "," + 
								nombreIF.replace(',',' ') + "," + 
								ordenAplicacion + "," + 
								fechaSeleccion + ","+
								fechaOperacion + "," + 
								numMensualidad + "," + 
								numCredito + "," + 
								tipoContrato + "," + 
								numPrestamo + "," + 
								Comunes.formatoDecimal(dblImporteAplicadoCredito,2,false) + "," + 
								Comunes.formatoDecimal(dblSaldoCredito,2,false) + "," + 
								"\n");
		}

		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else {
			nombreArchivo = archivo.nombre;
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {}
%>
<%=jsonObj%>