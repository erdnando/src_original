<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
        javax.naming.*,
        com.netro.descuento.*,
        com.netro.exception.*,
        netropology.utilerias.*,
        net.sf.json.*"
errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/13descuento/13secsession_extjs.jspf"%>

<%
JSONObject jsonObject = new JSONObject();

try {
  ParametrosDescuento parametrosDescuentoBean = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

  String claveEpo = (request.getParameter("claveEpo")==null)?"":request.getParameter("claveEpo");
  
  String sNombreEPO = "";
  String sParamOperNotasCred = "";
  String sAplicarNotasCredito = "";
  String sParamDsctoObligatorio = "";
  String sParam_fn_monto_minimo = "";
  String sParam_dscto_dinam = "";
  String sParam_limite_pyme = "";
  String sParam_firma_manc = "";
  String sParam_criterio_hash = "";
  String sParam_fecha_venc_pyme = "";
  String sParam_pub_docto_venc = "";
  String sdiasMaximosAd = "";
  String sPlazoMaxFechasVePyDc = "";
  String sPlazoMaxFechasVenPyDm = "";
  String sParamPreafiliacion = "";
  String sPermitirDescAutomatico = "";
  String sPubEPOPEF = "";
	String sPubEPOPEFSIAFFval = "";
	String sPubEPOPEFRecepcion = "";
	String sPubEPOPEFEntrega = "";
	String sPubEPOPEFTipoCompra = "";
	String sPubEPOPEFPresupuestal = "";
	String sPubEPOPEFPeriodo = "";
	String sOperaFactConMandato = "";
	String sOperaFactVencimiento = "";
	String sOperaCesionDerechos = "";
	String sOperaFacVencido = "";
	String sOperaDesAutoFacVencido = "";
	String sOperposicionje24hrs = "";
	String sOperaDsctoAutUltimoDia = "";
	String sOperaFactorajeDist = "";
	String sAutorizacionCuentasBanc	= "";
  String sParamOperaTasasEspeciales	= "";
  String sParamTipoTasa = "";
  String sHoraEnvOpIf = "";
  String sMostrarFechaAutorizacionIFInfoDoctos = "";
  String sOperaFactorajeIF = ""; 
  StringBuffer contenidoArchivo = new StringBuffer();

  HashMap informacionEpo = parametrosDescuentoBean.obtenerInformacionEpo(claveEpo);
  Hashtable alParamEPO = parametrosDescuentoBean.getParametrosEPO(claveEpo, 1);

  contenidoArchivo.append("Nombre de la EPO:," + (informacionEpo.get("nombreEpo")==null?"":informacionEpo.get("nombreEpo").toString().replaceAll(",", "")) + "\r\n\r\n");
  contenidoArchivo.append("Parámetros por EPO\r\n");

  if (alParamEPO != null) {
    sParamOperNotasCred 						= alParamEPO.get("OPERA_NOTAS_CRED")==null?"":(String)alParamEPO.get("OPERA_NOTAS_CRED");
    sAplicarNotasCredito 						= alParamEPO.get("CS_APLICAR_NOTAS_CRED")==null?"":(String)alParamEPO.get("CS_APLICAR_NOTAS_CRED");
	 sParam_fn_monto_minimo 					= alParamEPO.get("MONTO_MINIMO")==null?"":(String)alParamEPO.get("MONTO_MINIMO");
	 sParamDsctoObligatorio 					= alParamEPO.get("DSCTO_AUTO_OBLIGATORIO")==null?"":(String)alParamEPO.get("DSCTO_AUTO_OBLIGATORIO");
    sParam_dscto_dinam 							= alParamEPO.get("DESCTO_DINAM")==null?"":(String)alParamEPO.get("DESCTO_DINAM");
    sParam_limite_pyme 							= alParamEPO.get("LIMITE_PYME")==null?"":(String)alParamEPO.get("LIMITE_PYME");
    sParam_firma_manc 							= alParamEPO.get("PUB_FIRMA_MANC")==null?"":(String)alParamEPO.get("PUB_FIRMA_MANC");
    sParam_criterio_hash 						= alParamEPO.get("PUB_HASH")==null?"":(String)alParamEPO.get("PUB_HASH");
    sParam_fecha_venc_pyme 					= alParamEPO.get("FECHA_VENC_PYME")==null?"":(String)alParamEPO.get("FECHA_VENC_PYME");
    sParam_pub_docto_venc						= alParamEPO.get("PUB_DOCTO_VENC")==null?"":(String)alParamEPO.get("PUB_DOCTO_VENC");
    sdiasMaximosAd 								= alParamEPO.get("PLAZO_PAGO_FVP")==null?"":(String)alParamEPO.get("PLAZO_PAGO_FVP");
    sPlazoMaxFechasVePyDc 						= alParamEPO.get("PLAZO_MAX1_FVP")==null?"":(String)alParamEPO.get("PLAZO_MAX1_FVP");
    sPlazoMaxFechasVenPyDm 					= alParamEPO.get("PLAZO_MAX2_FVP")==null?"":(String)alParamEPO.get("PLAZO_MAX2_FVP");
    sParamPreafiliacion 						= alParamEPO.get("PREAFILIACION")==null?"":(String)alParamEPO.get("PREAFILIACION");
    sPermitirDescAutomatico					= alParamEPO.get("DESC_AUTO_PYME")==null?"":(String)alParamEPO.get("DESC_AUTO_PYME");
    sPubEPOPEF 									= alParamEPO.get("PUBLICACION_EPO_PEF")==null?"":(String)alParamEPO.get("PUBLICACION_EPO_PEF");
    sPubEPOPEFSIAFFval 							= (alParamEPO.get("PUB_EPO_PEF_USA_SIAFF")==null||alParamEPO.get("PUB_EPO_PEF_USA_SIAFF").equals(""))?"N":(String)alParamEPO.get("PUB_EPO_PEF_USA_SIAFF");
    sPubEPOPEFRecepcion 						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION");		// 13. PUB_EPO_PEF_FECHA_RECEPCION 
    sPubEPOPEFEntrega 							= alParamEPO.get("PUB_EPO_PEF_FECHA_ENTREGA")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_FECHA_ENTREGA");				// 13.1 PUB_EPO_PEF_FECHA_ENTREGA
    sPubEPOPEFTipoCompra 						= alParamEPO.get("PUB_EPO_PEF_TIPO_COMPRA")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_TIPO_COMPRA");					// 13.2 PUB_EPO_PEF_TIPO_COMPRA
    sPubEPOPEFPresupuestal 					= alParamEPO.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL");// 13.3 PUB_EPO_PEF_CLAVE_PRESUPUESTAL
    sPubEPOPEFPeriodo 							= alParamEPO.get("PUB_EPO_PEF_PERIODO")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_PERIODO");								// 13.4 PUB_EPO_PEF_PERIODO
    sOperaFactConMandato 						= alParamEPO.get("PUB_EPO_OPERA_MANDATO")==null||alParamEPO.get("PUB_EPO_OPERA_MANDATO").equals("")?"N":(String)alParamEPO.get("PUB_EPO_OPERA_MANDATO");	// 14. PUB_EPO_OPERA_MANDATO
    sOperaFactVencimiento 						= alParamEPO.get("PUB_EPO_VENC_INFONAVIT")==null||alParamEPO.get("PUB_EPO_VENC_INFONAVIT").equals("")?"N":(String)alParamEPO.get("PUB_EPO_VENC_INFONAVIT");	// 15. PUB_EPO_VENC_INFONAVIT  -FODEA 042 - Agosto/2009
    sOperaCesionDerechos 						= alParamEPO.get("OPER_SOLIC_CONS_CDER")==null||alParamEPO.get("OPER_SOLIC_CONS_CDER").equals("")?"N":(String)alParamEPO.get("OPER_SOLIC_CONS_CDER");	 //Fodea 018-2010
    sOperaFacVencido 							= alParamEPO.get("cs_factoraje_vencido")==null||alParamEPO.get("cs_factoraje_vencido").equals("")?"N":(String)alParamEPO.get("cs_factoraje_vencido");	 //Fodea 018-2010
    sOperaDesAutoFacVencido					= alParamEPO.get("PUB_EPO_DESCAUTO_FACTVENCIDO")==null||alParamEPO.get("PUB_EPO_DESCAUTO_FACTVENCIDO").equals("")?"N":(String)alParamEPO.get("PUB_EPO_DESCAUTO_FACTVENCIDO");	 //Fodea 018-2010
    sOperposicionje24hrs 						= alParamEPO.get("PUB_EPO_FACT_24HRS")==null||alParamEPO.get("PUB_EPO_FACT_24HRS").equals("")?"N":(String)alParamEPO.get("PUB_EPO_FACT_24HRS");	 //Fodea 018-2010
    sOperaDsctoAutUltimoDia					= alParamEPO.get("DESC_AUT_ULTIMO_DIA")==null||alParamEPO.get("DESC_AUT_ULTIMO_DIA").equals("")?"N":(String)alParamEPO.get("DESC_AUT_ULTIMO_DIA");	 //Fodea 018-2010
    sOperaFactorajeDist 						= alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO")==null||alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").equals("")?"N":(String)alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO");	 //FODEA  032-2010 FVR
    sAutorizacionCuentasBanc 					= alParamEPO.get("AUTORIZA_CTAS_BANC_ENTIDADES")==null||alParamEPO.get("AUTORIZA_CTAS_BANC_ENTIDADES").equals("")?"N":(String)alParamEPO.get("AUTORIZA_CTAS_BANC_ENTIDADES");	 //FODEA  032-2010 FVR
    sParamOperaTasasEspeciales				= alParamEPO.get("OPER_TASAS_ESPECIALES") == null || alParamEPO.get("OPER_TASAS_ESPECIALES").equals("") ?"N":(String)alParamEPO.get("OPER_TASAS_ESPECIALES");	 //FODEA  036-2010
    sParamTipoTasa 								= alParamEPO.get("TIPO_TASA") == null || alParamEPO.get("TIPO_TASA").equals("") ?"N":(String)alParamEPO.get("TIPO_TASA");	 //FODEA  036-2010
	 sHoraEnvOpIf									= (alParamEPO.get("HORA_ENV_OPE_IF")==null || alParamEPO.get("HORA_ENV_OPE_IF").equals("N"))?"":(String)alParamEPO.get("HORA_ENV_OPE_IF");
	 sMostrarFechaAutorizacionIFInfoDoctos	= alParamEPO.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS")==null||alParamEPO.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS").equals("")?"N":(String)alParamEPO.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS");	 
	 sOperaFactorajeIF							= alParamEPO.get("FACTORAJE_IF")==null||alParamEPO.get("FACTORAJE_IF").equals("")?"N":(String)alParamEPO.get("FACTORAJE_IF");	 //FODEA026  2012
	}

	contenidoArchivo.append("1,Opera Notas de Crédito,"+(sParamOperNotasCred.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	if (sParamOperNotasCred.equalsIgnoreCase("S")) {
    contenidoArchivo.append("1.1,Aplicar una Nota de Crédito a varios Documentos,"+(sAplicarNotasCredito.equalsIgnoreCase("S")?"SI":(sAplicarNotasCredito.equalsIgnoreCase("N")?"NO":""))+"\r\n"); 
   } 
	
	contenidoArchivo.append("2,Monto mínimo resultante para la aplicación de Notas de Crédito"+ ","+ sParam_fn_monto_minimo +"\r\n");
	contenidoArchivo.append("3,Descuento Automático Obligatorio,"+(sParamDsctoObligatorio.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	contenidoArchivo.append("4,Descuento Automatico Dinamico,"+(sParam_dscto_dinam.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	contenidoArchivo.append("5,Opera Límite de Linea por PyME,"+(sParam_limite_pyme.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	contenidoArchivo.append("6,Publicación con Firma Mancomunada,"+(sParam_firma_manc.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	contenidoArchivo.append("7,Publicación con Criterio Hash,"+(sParam_criterio_hash.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	contenidoArchivo.append("8,Operación con Fecha Vencimiento Proveedor,"+(sParam_fecha_venc_pyme.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	contenidoArchivo.append("8.1,Plazo para determinar días máximos adicionales a la fecha de vencimiento del proveedor"+ ","+ sdiasMaximosAd +"\r\n");
	contenidoArchivo.append("8.2,Plazo máximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento; cuyo plazo de pago al proveedor a partir de la publicación sea menor al parámetro 8.1"+ ","+ sPlazoMaxFechasVePyDc +"\r\n");
	contenidoArchivo.append("8.3,Plazo máximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento; cuyo plazo de pago al proveedor a partir de la publicación sea mayor  al parámetro 8.1"+ ","+ sPlazoMaxFechasVenPyDm +"\r\n");
	contenidoArchivo.append("9,Publicacion Documentos Vencidos,"+(sParam_pub_docto_venc.equalsIgnoreCase("S")?"SI":(sParam_pub_docto_venc.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	contenidoArchivo.append("10,Opera Pre-Afiliación a Descuento PYMES,"+(sParamPreafiliacion.equalsIgnoreCase("S")?"SI":"NO")+"\r\n");
	contenidoArchivo.append("11,Permitir Descuento Automático a PYMES,"+(sPermitirDescAutomatico.equalsIgnoreCase("S")?"SI":(sPermitirDescAutomatico.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	contenidoArchivo.append("12,Publicación EPO PEF (Clave SIAFF),"+(sPubEPOPEF.equalsIgnoreCase("S")?"SI":(sPubEPOPEF.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	if (sPubEPOPEF.equalsIgnoreCase("S")) {
    contenidoArchivo.append("12.1, Utiliza SIAFF,"+(sPubEPOPEFSIAFFval.equalsIgnoreCase("S")?"SI":(sPubEPOPEFSIAFFval.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	}
	contenidoArchivo.append("13,Publicación EPO PEF,"+(sPubEPOPEFRecepcion.equalsIgnoreCase("S")?"SI":(sPubEPOPEFRecepcion.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	if (sPubEPOPEFRecepcion.equalsIgnoreCase("S")) {
    contenidoArchivo.append("13.1,Fecha de Recepción de Bienes y Servicios,"+(sPubEPOPEFEntrega.equalsIgnoreCase("S")?"SI":(sPubEPOPEFEntrega.equalsIgnoreCase("N")?"NO":""))+"\r\n");
    contenidoArchivo.append("13.2,Tipo de Compra (procedimiento),"+(sPubEPOPEFTipoCompra.equalsIgnoreCase("S")?"SI":(sPubEPOPEFTipoCompra.equalsIgnoreCase("N")?"NO":""))+"\r\n");
    contenidoArchivo.append("13.3,Clasificador por Objeto del Gasto,"+(sPubEPOPEFPresupuestal.equalsIgnoreCase("S")?"SI":(sPubEPOPEFPresupuestal.equalsIgnoreCase("N")?"NO":""))+"\r\n");
    contenidoArchivo.append("13.4,Plazo Máximo,"+(sPubEPOPEFPeriodo.equalsIgnoreCase("S")?"SI":(sPubEPOPEFPeriodo.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	} 
	contenidoArchivo.append("14,Opera Factoraje con Mandato,"+(sOperaFactConMandato.equalsIgnoreCase("S")?"SI":(sOperaFactConMandato.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	contenidoArchivo.append("15,Opera Factoraje Vencimiento Infonavit,"+(sOperaFactVencimiento.equalsIgnoreCase("S")?"SI":(sOperaFactVencimiento.equalsIgnoreCase("N")?"NO":""))+"\r\n");
   contenidoArchivo.append("16,Opera Solicitud de Consentimiento de Cesión de Derechos,"+(sOperaCesionDerechos.equalsIgnoreCase("S")?"SI":(sOperaCesionDerechos.equalsIgnoreCase("N")?"NO":""))+"\r\n");
   contenidoArchivo.append("17,Opera Factoraje 24hrs,"+(sOperposicionje24hrs.equalsIgnoreCase("S")?"SI":(sOperposicionje24hrs.equalsIgnoreCase("N")?"NO":""))+"\r\n");
   contenidoArchivo.append("18,Opera Factoraje Vencido,"+(sOperaFacVencido.equalsIgnoreCase("S")?"SI":(sOperaFacVencido.equalsIgnoreCase("N")?"NO":""))+"\r\n");
   if (sOperaFacVencido.equalsIgnoreCase("S")) {
    contenidoArchivo.append("18.1,Opera Descuento Automático – Factoraje Vencido,"+(sOperaDesAutoFacVencido.equalsIgnoreCase("S")?"SI":(sOperaDesAutoFacVencido.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	}
	contenidoArchivo.append("19,Opera Descuento Automático – Último día antes del vencimiento,"+(sOperaDsctoAutUltimoDia.equalsIgnoreCase("S")?"SI":(sOperaDsctoAutUltimoDia.equalsIgnoreCase("N")?"NO":""))+"\r\n");
   contenidoArchivo.append("20,Factoraje Distribuido,"+(sOperaFactorajeDist.equalsIgnoreCase("S")?"SI":(sOperaFactorajeDist.equalsIgnoreCase("N")?"NO":""))+"\r\n");
   contenidoArchivo.append("20.1,Autorización cuentas bancarias de entidades,"+(sAutorizacionCuentasBanc.equalsIgnoreCase("S")?"SI":(sAutorizacionCuentasBanc.equalsIgnoreCase("N")?"NO":""))+"\r\n");
   contenidoArchivo.append("21,Opera Tasas Especiales,"+(sParamOperaTasasEspeciales.equalsIgnoreCase("S")?"SI":(sParamOperaTasasEspeciales.equalsIgnoreCase("N")?"NO":""))+"\r\n");
   contenidoArchivo.append("21.1,Tipo Tasa,"+(sParamTipoTasa.equalsIgnoreCase("P")?"SI":(sParamOperaTasasEspeciales.equalsIgnoreCase("N")?"NO":"NO")) +",Preferencial\r\n");
   contenidoArchivo.append(",,"+(sParamTipoTasa.equalsIgnoreCase("NG")?"SI":(sParamOperaTasasEspeciales.equalsIgnoreCase("N")?"NO":"NO")) +",Negociada\r\n");
   contenidoArchivo.append(",,"+(sParamTipoTasa.equalsIgnoreCase("A")?"SI":(sParamOperaTasasEspeciales.equalsIgnoreCase("N")?"NO":"NO")) +",Ambas\r\n");
	
	contenidoArchivo.append("22,Hora para envío de operación a IF’ s"+ ","+ sHoraEnvOpIf +"\r\n");	  
	contenidoArchivo.append("23,Mostrar Fecha de Autorización IF en Info. de Documentos,"+(sMostrarFechaAutorizacionIFInfoDoctos.equalsIgnoreCase("S")?"SI":(sMostrarFechaAutorizacionIFInfoDoctos.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	contenidoArchivo.append("24,Factoraje IF ,"+(sOperaFactorajeIF.equalsIgnoreCase("S")?"SI":(sOperaFactorajeIF.equalsIgnoreCase("N")?"NO":""))+"\r\n");
	String nombreArchivo = "";
	CreaArchivo archivo = new CreaArchivo();
  
 	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObject.put("success", new Boolean(false));
		jsonObject.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		jsonObject.put("success", new Boolean(true));
		jsonObject.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
	}
} catch(Exception e) {
  e.getMessage();
}
%>
<%=jsonObject%>