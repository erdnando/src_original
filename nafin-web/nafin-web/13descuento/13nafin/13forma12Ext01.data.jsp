<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, 
    com.netro.model.catalogos.*,
    com.netro.descuento.ParametrosIf,
    netropology.utilerias.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject "
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
	String informacion 	= (request.getParameter("informacion") != null) ? request.getParameter("informacion") :"";
	String infoRegresar = "";

if(informacion.equals("Consultar")){
 
  ParametrosIf param = new ParametrosIf();
  JSONObject jsonObj  = new JSONObject();
  Registros   layout = param.layoutParamGen();
//--
  List principal = new ArrayList();
  HashMap mio = new HashMap();
  String CS_LAYOUT_COMPLETO_VENC="";
  String CS_LAYOUT_COMPLETO_OPER="";
 
  while (layout.next() ){
    try {
       CS_LAYOUT_COMPLETO_VENC = layout.getString("CS_LAYOUT_COMPLETO_VENC");
       CS_LAYOUT_COMPLETO_OPER = layout.getString("CS_LAYOUT_COMPLETO_OPER");
    } catch (Exception e){
     System.err.println("error: "+ e);
    }
  }
  mio = new HashMap();
  mio.put("DESCRIPCION","\"Desplegar archivo de vencimiento con todos los campos de Sirac\"");
  mio.put("CS_LAYOUT_COMPLETO_VENC","\""+CS_LAYOUT_COMPLETO_VENC+"\"");
  principal.add(mio);
  
  mio = new HashMap();
  mio.put("DESCRIPCION","\"Desplegar archivo de operados con todos los campos de Sirac\"");
  mio.put("CS_LAYOUT_COMPLETO_OPER","\""+CS_LAYOUT_COMPLETO_OPER+"\"");
  principal.add(mio);
 
  
  String consulta = 	"{\"success\": true, \"total\": \""+ 2 +"\", \"registros\": " + principal+"}";

 jsonObj = JSONObject.fromObject(consulta); 
 infoRegresar = jsonObj.toString();
  
//----
//  jsonObj.put("registros", layout.getJSONData() );
//  infoRegresar = jsonObj.toString(); System.err.println("infoRegresar: "+infoRegresar);
  
} else if(informacion.equals("grabar")) {

  String   lCompletoVenc	 = (request.getParameter("lCompletoVenc") 	!= null) ? request.getParameter("lCompletoVenc"):"";
  String   lCompletoOper	 = (request.getParameter("lCompletoOper") 	!= null) ? request.getParameter("lCompletoOper"):"";
   
  JSONObject jsonObj  = new JSONObject();
  ParametrosIf param = new ParametrosIf();
  
  param.guardarParametrosGralLayout(lCompletoVenc,lCompletoOper);

  jsonObj.put("success", new Boolean (true));
	infoRegresar = jsonObj.toString(); 
}


%>
<%=infoRegresar%>
