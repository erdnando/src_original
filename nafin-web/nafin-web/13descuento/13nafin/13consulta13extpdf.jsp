<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
        javax.naming.*,
        com.netro.descuento.*,
        java.io.File,
        com.netro.pdf.ComunesPDF,
        netropology.utilerias.*,
        net.sf.json.*"
errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/13descuento/13secsession_extjs.jspf"%>

<%
JSONObject jsonObject = new JSONObject();
CreaArchivo archivo = new CreaArchivo();
File objetoArchivo = null;
String nombreArchivo = "";

try {
  ParametrosDescuento parametrosDescuentoBean = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

  String claveEpo = (request.getParameter("claveEpo")==null)?"":request.getParameter("claveEpo");
  
  String sNombreEPO = "";
  String sParamOperNotasCred = "";
  String sAplicarNotasCredito = "";
  String sParam_fn_monto_minimo = "";
  String sParamDsctoObligatorio = "";
  String sParam_dscto_dinam = "";
  String sParam_limite_pyme = "";
  String sParam_firma_manc = "";
  String sParam_criterio_hash = "";
  String sParam_fecha_venc_pyme = "";
  String sParam_pub_docto_venc = "";
  String sdiasMaximosAd = "";
  String sPlazoMaxFechasVePyDc = "";
  String sPlazoMaxFechasVenPyDm = "";
  String sParamPreafiliacion = "";
  String sPermitirDescAutomatico = "";
  String sPubEPOPEF = "";
	String sPubEPOPEFSIAFFval = "";
	String sPubEPOPEFRecepcion = "";
	String sPubEPOPEFEntrega = "";
	String sPubEPOPEFTipoCompra = "";
	String sPubEPOPEFPresupuestal = "";
	String sPubEPOPEFPeriodo = "";
	String sOperaFactConMandato = "";
	String sOperaFactVencimiento = "";
	String sOperaCesionDerechos = "";
	String sOperaFacVencido = "";
	String sOperaDesAutoFacVencido = "";
	String sOperposicionje24hrs = "";
	String sOperaDsctoAutUltimoDia = "";
	String sOperaFactorajeDist = "";
	String sAutorizacionCuentasBanc	= "";
  String sParamOperaTasasEspeciales	= "";
  String sParamTipoTasa = "";
  String sHoraEnvOpIf = "";
  String sMostrarFechaAutorizacionIFInfoDoctos = "";
  String sOperaFactorajeIF = "";
  StringBuffer contenidoArchivo = new StringBuffer();

  HashMap informacionEpo = parametrosDescuentoBean.obtenerInformacionEpo(claveEpo);
  Hashtable alParamEPO = parametrosDescuentoBean.getParametrosEPO(claveEpo, 1);

  contenidoArchivo.append("Nombre de la EPO:," + (informacionEpo.get("nombreEpo")==null?"":informacionEpo.get("nombreEpo")) + "\r\n\r\n");
  contenidoArchivo.append("Parametros por EPO\r\n");

  if (alParamEPO != null) {
    sParamOperNotasCred 						= alParamEPO.get("OPERA_NOTAS_CRED")==null?"":(String)alParamEPO.get("OPERA_NOTAS_CRED");
	 sAplicarNotasCredito 						= alParamEPO.get("CS_APLICAR_NOTAS_CRED")==null?"":(String)alParamEPO.get("CS_APLICAR_NOTAS_CRED");
    sParam_fn_monto_minimo 					= alParamEPO.get("MONTO_MINIMO")==null?"":(String)alParamEPO.get("MONTO_MINIMO");
    sParamDsctoObligatorio 					= alParamEPO.get("DSCTO_AUTO_OBLIGATORIO")==null?"":(String)alParamEPO.get("DSCTO_AUTO_OBLIGATORIO");
    sParam_dscto_dinam 							= alParamEPO.get("DESCTO_DINAM")==null?"":(String)alParamEPO.get("DESCTO_DINAM");
    sParam_limite_pyme 							= alParamEPO.get("LIMITE_PYME")==null?"":(String)alParamEPO.get("LIMITE_PYME");
    sParam_firma_manc 							= alParamEPO.get("PUB_FIRMA_MANC")==null?"":(String)alParamEPO.get("PUB_FIRMA_MANC");
    sParam_criterio_hash 						= alParamEPO.get("PUB_HASH")==null?"":(String)alParamEPO.get("PUB_HASH");
    sParam_fecha_venc_pyme 					= alParamEPO.get("FECHA_VENC_PYME")==null?"":(String)alParamEPO.get("FECHA_VENC_PYME");
    sParam_pub_docto_venc 						= alParamEPO.get("PUB_DOCTO_VENC")==null?"":(String)alParamEPO.get("PUB_DOCTO_VENC");
    sdiasMaximosAd 								= alParamEPO.get("PLAZO_PAGO_FVP")==null?"":(String)alParamEPO.get("PLAZO_PAGO_FVP");
    sPlazoMaxFechasVePyDc 						= alParamEPO.get("PLAZO_MAX1_FVP")==null?"":(String)alParamEPO.get("PLAZO_MAX1_FVP");
    sPlazoMaxFechasVenPyDm 					= alParamEPO.get("PLAZO_MAX2_FVP")==null?"":(String)alParamEPO.get("PLAZO_MAX2_FVP");
    sParamPreafiliacion 						= alParamEPO.get("PREAFILIACION")==null?"":(String)alParamEPO.get("PREAFILIACION");
    sPermitirDescAutomatico 					= alParamEPO.get("DESC_AUTO_PYME")==null?"":(String)alParamEPO.get("DESC_AUTO_PYME");
    sPubEPOPEF 									= alParamEPO.get("PUBLICACION_EPO_PEF")==null?"":(String)alParamEPO.get("PUBLICACION_EPO_PEF");
    sPubEPOPEFSIAFFval 							= (alParamEPO.get("PUB_EPO_PEF_USA_SIAFF")==null||alParamEPO.get("PUB_EPO_PEF_USA_SIAFF").equals(""))?"N":(String)alParamEPO.get("PUB_EPO_PEF_USA_SIAFF");
    sPubEPOPEFRecepcion 						= alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_FECHA_RECEPCION");		// 13. PUB_EPO_PEF_FECHA_RECEPCION 
    sPubEPOPEFEntrega 							= alParamEPO.get("PUB_EPO_PEF_FECHA_ENTREGA")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_FECHA_ENTREGA");				// 13.1 PUB_EPO_PEF_FECHA_ENTREGA
    sPubEPOPEFTipoCompra 						= alParamEPO.get("PUB_EPO_PEF_TIPO_COMPRA")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_TIPO_COMPRA");					// 13.2 PUB_EPO_PEF_TIPO_COMPRA
    sPubEPOPEFPresupuestal 					= alParamEPO.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_CLAVE_PRESUPUESTAL");// 13.3 PUB_EPO_PEF_CLAVE_PRESUPUESTAL
    sPubEPOPEFPeriodo							= alParamEPO.get("PUB_EPO_PEF_PERIODO")==null?"":(String)alParamEPO.get("PUB_EPO_PEF_PERIODO");								// 13.4 PUB_EPO_PEF_PERIODO
    sOperaFactConMandato 						= alParamEPO.get("PUB_EPO_OPERA_MANDATO")==null||alParamEPO.get("PUB_EPO_OPERA_MANDATO").equals("")?"N":(String)alParamEPO.get("PUB_EPO_OPERA_MANDATO");	// 14. PUB_EPO_OPERA_MANDATO
    sOperaFactVencimiento 						= alParamEPO.get("PUB_EPO_VENC_INFONAVIT")==null||alParamEPO.get("PUB_EPO_VENC_INFONAVIT").equals("")?"N":(String)alParamEPO.get("PUB_EPO_VENC_INFONAVIT");	// 15. PUB_EPO_VENC_INFONAVIT  -FODEA 042 - Agosto/2009
    sOperaCesionDerechos 						= alParamEPO.get("OPER_SOLIC_CONS_CDER")==null||alParamEPO.get("OPER_SOLIC_CONS_CDER").equals("")?"N":(String)alParamEPO.get("OPER_SOLIC_CONS_CDER");	 //Fodea 018-2010
    sOperaFacVencido 							= alParamEPO.get("cs_factoraje_vencido")==null||alParamEPO.get("cs_factoraje_vencido").equals("")?"N":(String)alParamEPO.get("cs_factoraje_vencido");	 //Fodea 018-2010
    sOperaDesAutoFacVencido 					= alParamEPO.get("PUB_EPO_DESCAUTO_FACTVENCIDO")==null||alParamEPO.get("PUB_EPO_DESCAUTO_FACTVENCIDO").equals("")?"N":(String)alParamEPO.get("PUB_EPO_DESCAUTO_FACTVENCIDO");	 //Fodea 018-2010
    sOperposicionje24hrs 						= alParamEPO.get("PUB_EPO_FACT_24HRS")==null||alParamEPO.get("PUB_EPO_FACT_24HRS").equals("")?"N":(String)alParamEPO.get("PUB_EPO_FACT_24HRS");	 //Fodea 018-2010
    sOperaDsctoAutUltimoDia 					= alParamEPO.get("DESC_AUT_ULTIMO_DIA")==null||alParamEPO.get("DESC_AUT_ULTIMO_DIA").equals("")?"N":(String)alParamEPO.get("DESC_AUT_ULTIMO_DIA");	 //Fodea 018-2010
    sOperaFactorajeDist 						= alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO")==null||alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").equals("")?"N":(String)alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO");	 //FODEA  032-2010 FVR
    sAutorizacionCuentasBanc 					= alParamEPO.get("AUTORIZA_CTAS_BANC_ENTIDADES")==null||alParamEPO.get("AUTORIZA_CTAS_BANC_ENTIDADES").equals("")?"N":(String)alParamEPO.get("AUTORIZA_CTAS_BANC_ENTIDADES");	 //FODEA  032-2010 FVR
    sParamOperaTasasEspeciales 				= alParamEPO.get("OPER_TASAS_ESPECIALES") == null || alParamEPO.get("OPER_TASAS_ESPECIALES").equals("") ?"N":(String)alParamEPO.get("OPER_TASAS_ESPECIALES");	 //FODEA  036-2010
    sParamTipoTasa 								= alParamEPO.get("TIPO_TASA") == null || alParamEPO.get("TIPO_TASA").equals("") ?"N":(String)alParamEPO.get("TIPO_TASA");	 //FODEA  036-2010
	 sHoraEnvOpIf									= (alParamEPO.get("HORA_ENV_OPE_IF")==null || alParamEPO.get("HORA_ENV_OPE_IF").equals("N"))?"":(String)alParamEPO.get("HORA_ENV_OPE_IF");
	 sMostrarFechaAutorizacionIFInfoDoctos	= alParamEPO.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS")==null||alParamEPO.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS").equals("")?"N":(String)alParamEPO.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS");	 
	 sOperaFactorajeIF							= alParamEPO.get("FACTORAJE_IF")==null||alParamEPO.get("FACTORAJE_IF").equals("")?"N":(String)alParamEPO.get("FACTORAJE_IF");	 //FODEA026  2012
	}

  nombreArchivo = archivo.nombreArchivo() + ".pdf";
  
  ComunesPDF pdfDoc = new ComunesPDF(1, strDirectorioTemp + nombreArchivo, "", false, true, true);
  
  String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
  String noCliente = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
  String nombre = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
  String nombreUsr = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
  String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
  
  pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
  
  pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
  pdfDoc.addText("Nombre de la EPO: " + (informacionEpo.get("nombreEpo")==null?"":informacionEpo.get("nombreEpo")), "formas", ComunesPDF.CENTER);
  pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

  float anchoCelda2[] = {10f, 60f, 30f};
  pdfDoc.setTable(3, 80, anchoCelda2);

  pdfDoc.setCell("Parámetros por EPO", "celda02", ComunesPDF.CENTER, 3);

  pdfDoc.setCell("1", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Notas de Crédito", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParamOperNotasCred.equalsIgnoreCase("S")?"SI":"NO"), "formas", ComunesPDF.CENTER);
  
  if (sParamOperNotasCred.equalsIgnoreCase("S")) {
    pdfDoc.setCell("1.1", "formas", ComunesPDF.CENTER);
    pdfDoc.setCell("Aplicar una Nota de Crédito a varios Documentos", "formas", ComunesPDF.LEFT);
    pdfDoc.setCell((sAplicarNotasCredito.equalsIgnoreCase("S")?"SI":"NO"), "formas", ComunesPDF.CENTER);
  } 

  pdfDoc.setCell("2", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Monto mínimo resultante para la aplicación de Notas de Crédito", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell(sParam_fn_monto_minimo, "formas", ComunesPDF.CENTER);
  
  pdfDoc.setCell("3", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Descuento Obligatorio", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParamDsctoObligatorio.equalsIgnoreCase("S")?"SI":"NO"), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("4", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Descuento Automático Dinámico", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParam_dscto_dinam.equalsIgnoreCase("S")?"SI":"NO"), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("5", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Límite de Linea por PyME", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParam_limite_pyme.equalsIgnoreCase("S")?"SI":"NO"), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("6", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Publicación con Firma Mancomunada", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParam_firma_manc.equalsIgnoreCase("S")?"SI":"NO"), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("7", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Publicación con Criterio Hash", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParam_criterio_hash.equalsIgnoreCase("S")?"SI":"NO"), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("8", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Operación con Fecha Vencimiento Proveedor", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParam_fecha_venc_pyme.equalsIgnoreCase("S")?"SI":"NO"), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("8.1", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Plazo para determinar días máximos adicionales a la fecha de vencimiento del proveedor", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell(sdiasMaximosAd, "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("8.2", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Plazo máximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento, cuyo plazo de pago al proveedor a partir de la publicación sea menor al parámetro 8.1", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell(sPlazoMaxFechasVePyDc, "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("8.3", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Plazo máximo entre la fecha de Vencimiento de Proveedor y Fecha de Vencimiento del Documento, cuyo plazo de pago al proveedor a partir de la publicación sea mayor  al parámetro 8.1", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell(sPlazoMaxFechasVenPyDm, "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("9", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Publicación Documentos Vencidos", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParam_pub_docto_venc.equalsIgnoreCase("S")?"SI":(sParam_pub_docto_venc.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("10", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Pre-Afiliación a Descuento PYMES", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParamPreafiliacion.equalsIgnoreCase("S")?"SI":"NO"), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("11", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Permitir Descuento Automático a PYMES", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sPermitirDescAutomatico.equalsIgnoreCase("S")?"SI":(sPermitirDescAutomatico.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("12", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Publicación EPO PEF (Clave SIAFF)", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sPubEPOPEF.equalsIgnoreCase("S")?"SI":(sPubEPOPEF.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  if (sPubEPOPEF.equalsIgnoreCase("S")) {
    pdfDoc.setCell("12.1", "formas", ComunesPDF.CENTER);
    pdfDoc.setCell("Utiliza SIAFF", "formas", ComunesPDF.LEFT);
    pdfDoc.setCell((sPubEPOPEFSIAFFval.equalsIgnoreCase("S")?"SI":(sPubEPOPEFSIAFFval.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);
  }

  pdfDoc.setCell("13", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Publicación EPO PEF", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sPubEPOPEFRecepcion.equalsIgnoreCase("S")?"SI":(sPubEPOPEFRecepcion.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  if (sPubEPOPEFRecepcion.equalsIgnoreCase("S")) {
    pdfDoc.setCell("13.1", "formas", ComunesPDF.CENTER);
    pdfDoc.setCell("Fecha de Recepción de Bienes y Servicios", "formas", ComunesPDF.LEFT);
    pdfDoc.setCell((sPubEPOPEFEntrega.equalsIgnoreCase("S")?"SI":(sPubEPOPEFEntrega.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);
    
    pdfDoc.setCell("13.2", "formas", ComunesPDF.CENTER);
    pdfDoc.setCell("Tipo de Compra (procedimiento)", "formas", ComunesPDF.LEFT);
    pdfDoc.setCell((sPubEPOPEFTipoCompra.equalsIgnoreCase("S")?"SI":(sPubEPOPEFTipoCompra.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);
    
    pdfDoc.setCell("13.3", "formas", ComunesPDF.CENTER);
    pdfDoc.setCell("Clasificador por Objeto del Gasto", "formas", ComunesPDF.LEFT);
    pdfDoc.setCell((sPubEPOPEFPresupuestal.equalsIgnoreCase("S")?"SI":(sPubEPOPEFPresupuestal.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);
    
    pdfDoc.setCell("13.4", "formas", ComunesPDF.CENTER);
    pdfDoc.setCell("Plazo Máximo", "formas", ComunesPDF.LEFT);
    pdfDoc.setCell((sPubEPOPEFPeriodo.equalsIgnoreCase("S")?"SI":(sPubEPOPEFPeriodo.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);
  } 

  pdfDoc.setCell("14", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Factoraje con Mandato", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sOperaFactConMandato.equalsIgnoreCase("S")?"SI":(sOperaFactConMandato.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("15", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Factoraje Vencimiento Infonavit", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sOperaFactVencimiento.equalsIgnoreCase("S")?"SI":(sOperaFactVencimiento.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("16", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Solicitud de Consentimiento de Cesión de Derechos", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sOperaCesionDerechos.equalsIgnoreCase("S")?"SI":(sOperaCesionDerechos.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("17", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Factoraje 24hrs", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sOperposicionje24hrs.equalsIgnoreCase("S")?"SI":(sOperposicionje24hrs.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("18", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Factoraje Vencido", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sOperaFacVencido.equalsIgnoreCase("S")?"SI":(sOperaFacVencido.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);
  
  if (sOperaFacVencido.equalsIgnoreCase("S")) {
    pdfDoc.setCell("18.1", "formas", ComunesPDF.CENTER);
    pdfDoc.setCell("Opera Descuento Automático – Factoraje Vencido", "formas", ComunesPDF.LEFT);
    pdfDoc.setCell((sOperaDesAutoFacVencido.equalsIgnoreCase("S")?"SI":(sOperaDesAutoFacVencido.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);
  }
    
  pdfDoc.setCell("19", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Descuento Automático – Último día antes del vencimiento", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sOperaDsctoAutUltimoDia.equalsIgnoreCase("S")?"SI":(sOperaDsctoAutUltimoDia.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("20", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Factoraje Distribuido", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sOperaFactorajeDist.equalsIgnoreCase("S")?"SI":(sOperaFactorajeDist.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("20.1", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Autorización cuentas bancarias de entidades", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sAutorizacionCuentasBanc.equalsIgnoreCase("S")?"SI":(sAutorizacionCuentasBanc.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("21", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Opera Tasas Especiales", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sParamOperaTasasEspeciales.equalsIgnoreCase("S")?"SI":(sParamOperaTasasEspeciales.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  StringBuffer strBuffTipoTasa = new StringBuffer();
  strBuffTipoTasa.append((sParamTipoTasa.equalsIgnoreCase("P")?"SI":"NO") + " - Preferencial\n");
  strBuffTipoTasa.append((sParamTipoTasa.equalsIgnoreCase("NG")?"SI":"NO") + " - Negociada\n");
  strBuffTipoTasa.append((sParamTipoTasa.equalsIgnoreCase("A")?"SI":"NO") + " - Ambas\n");

  pdfDoc.setCell("21.1", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Tipo Tasa", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell(strBuffTipoTasa.toString(), "formas", ComunesPDF.CENTER);

  pdfDoc.setCell("22", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Hora para envío de operación a IF’ s", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell(sHoraEnvOpIf, "formas", ComunesPDF.CENTER);
  
  pdfDoc.setCell("23", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Mostrar Fecha de Autorización IF en Info. de Documentos", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sMostrarFechaAutorizacionIFInfoDoctos.equalsIgnoreCase("S")?"SI":(sMostrarFechaAutorizacionIFInfoDoctos.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);
  
  pdfDoc.setCell("24", "formas", ComunesPDF.CENTER);
  pdfDoc.setCell("Factoraje IF", "formas", ComunesPDF.LEFT);
  pdfDoc.setCell((sOperaFactorajeIF.equalsIgnoreCase("S")?"SI":(sOperaFactorajeIF.equalsIgnoreCase("N")?"NO":"")), "formas", ComunesPDF.CENTER);

  pdfDoc.addTable();
  pdfDoc.endDocument();
  
  objetoArchivo = new File(strDirectorioTemp + nombreArchivo);
  
 	if (!objetoArchivo.exists()) {
		jsonObject.put("success", new Boolean(false));
		jsonObject.put("msg", "Error al generar el archivo");
	} else {
		jsonObject.put("success", new Boolean(true));
		jsonObject.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
	}
} catch(Exception e) {
  e.getMessage();
}
%>
<%=jsonObject%>