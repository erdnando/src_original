<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%
System.out.println("13consulta18exti  (E)");
 
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String ic_banco_fondeo = request.getParameter("ic_banco_fondeo") == null?"":(String)request.getParameter("ic_banco_fondeo");
String ic_epo = request.getParameter("ic_epo")== null?"0":(String)request.getParameter("ic_epo");
if(ic_epo.equals("")){ ic_epo = "0"; }
System.out.println("ic_banco_fondeo   "+ic_banco_fondeo);
System.out.println("ic_epo   "+ic_epo);
System.out.println("informacion   "+informacion);
JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
AccesoDB con = new AccesoDB();

try {
	con.conexionDB();
	
	ComunesPDF pdfDoc = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);	
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
	
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
  (String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
	
 	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" OTROS PARAMETROS POR EPO","formas",ComunesPDF.CENTER);	
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
	pdfDoc.setTable(7,100);
	pdfDoc.setCell("Epo","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Días Mín. para Descuento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Días Máx. para Descuento","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Porcentaje de Descuento M.N","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Porcentaje de Descuento Dol.,","celda01",ComunesPDF.CENTER);		
	pdfDoc.setCell("Opera Factoraje Vencido","celda01",ComunesPDF.CENTER);	
	pdfDoc.setCell("Opera Factoraje Distribuido","celda01",ComunesPDF.CENTER);

	StringBuffer  strQuery 		= new StringBuffer(); 	
	strQuery.append( " SELECT cat.cg_razon_social nombre, rel.ig_dias_minimo as dmin, rel.ig_dias_maximo as dmax, rel.fn_aforo as fecha_aforo, rel.fn_aforo_dl as fecha_aforo_dol,"    );
	strQuery.append( " rel.cs_factoraje_vencido as fact_venc, rel.cs_factoraje_distribuido as fact_distr"    );
	strQuery.append( " FROM comrel_producto_epo rel,"    );
	strQuery.append( "	comcat_epo cat"  );
	strQuery.append( " WHERE rel.ic_epo=cat.ic_epo" );
	strQuery.append( " AND rel.ic_producto_nafin = ?" );
  strQuery.append( "  AND cat.ic_banco_fondeo = ?"  );
	if(!ic_epo.equals("0")){
		strQuery.append("    AND rel.ic_epo = ?" );
	}
	strQuery.append(" ORDER BY cat.cg_razon_social ");   
	 
	PreparedStatement ps = con.queryPrecompilado(strQuery.toString());
  ps.setInt(1, 1);
  ps.setInt(2, Integer.parseInt(ic_banco_fondeo));
	if(!ic_epo.equals("0")){
		ps.setString(3, ic_epo);
	}
	ResultSet rs = ps.executeQuery();	
	
	
	System.out.println(":::13consulta18ipop::"+ strQuery.toString());	
	
	while (rs.next()) {
	
		String nombre = (rs.getString("nombre")==null)?"":rs.getString("nombre").trim();
		String dmin = (rs.getString("dmin")==null)?"":rs.getString("dmin").trim();
		String dmax  = (rs.getString("dmax")==null)?"":rs.getString("dmax").trim();
		String fecha_aforo  = (rs.getString("fecha_aforo")==null)?"":rs.getString("fecha_aforo").trim();
		String fecha_aforo_dol = (rs.getString("fecha_aforo_dol")==null)?"":rs.getString("fecha_aforo_dol").trim();
		String fact_venc = (rs.getString("fact_venc")==null)?"":rs.getString("fact_venc").trim();
		String fact_distr = (rs.getString("fact_distr")==null)?"":rs.getString("fact_distr").trim();
		
		pdfDoc.setCell(nombre,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(dmin,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(dmax,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fecha_aforo+"%","formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fecha_aforo_dol+"%","formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fact_venc,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(fact_distr,"formas",ComunesPDF.CENTER);
				
	}	
	rs.close();
	ps.close();
	pdfDoc.addTable();
	pdfDoc.endDocument();
	con.cierraStatement();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch(Exception e) {
	jsonObj.put("success", new Boolean(false));
	jsonObj.put("msg", "Error al generar el archivo PDF");
}
%>
<%=jsonObj%>


<%System.out.println("13consulta18exti  (S)");%>