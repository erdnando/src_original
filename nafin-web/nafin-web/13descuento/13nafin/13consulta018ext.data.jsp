 <%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.pdf.*,
		com.netro.descuento.*, 
		com.netro.model.catalogos.CatalogoEPONafin,
		com.netro.distribuidores.*,
		net.sf.json.JSONArray, net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion				=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_banco_fondeo = (request.getParameter("cmb_Banco")==null)?"":request.getParameter("cmb_Banco");
String ic_epo = (request.getParameter("cmb_Epo")==null)?"":request.getParameter("cmb_Epo");
String infoRegresar  = "";


if (informacion.equals("CatalogoEPO") ) {

		CatalogoEPONafin cat = new CatalogoEPONafin();
		cat.setCampoClave ("e.ic_epo");
		cat.setCampoDescripcion("e.cg_razon_social"); 
		cat.setProducto("1");
		cat.setBancoFondeo(ic_banco_fondeo);
		List elementos = cat.getListaElementos(); 
		
		//necesitamos una cadena JSON por lo que convertimos el List
		//ArrayList alListadeEPO = null;   
		JSONArray jsonArr = new JSONArray();
		HashMap hm = new HashMap();
		hm.put("descripcion","Todas");
		hm.put("clave","0");		
		jsonArr.add(hm);
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
				
		infoRegresar= "{\"success\": true, \"total\": \"" + jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
}
else if(informacion.equals("ConsultaGrid")) {
 	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	infoRegresar=BeanParamDscto.getParamPorEPOEXTJS(ic_epo, ic_banco_fondeo);
}

else if (informacion.equals("GeneraPDF"))
{	
	AccesoDB con = new AccesoDB();
	CreaArchivo archivo = new CreaArchivo();	
	JSONObject jsonObj = new JSONObject();
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	
	try
	{
		con.conexionDB();
		
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		ic_epo = (request.getParameter("listaEPO")==null)?"":request.getParameter("listaEPO");
		
		
		///////////////////////////////////////////////////////////////////////////
		/*Todos los pdf's llevan esto*/
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String) application.getAttribute("strDirectorioPublicacion"));	
	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		///////////////////////////////////////////////////////////////////////////
		
		
		ArrayList alListadeEPO = BeanParamDscto.getParamPorEPO(ic_epo, ic_banco_fondeo);
		
		HashMap hmParametros = new HashMap();
		pdfDoc.setTable(7, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
		pdfDoc.setCell("EPO","celda01",ComunesPDF.LEFT);
		pdfDoc.setCell("Días Mín. para Desc","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Días Máx. para Desc","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Porcentaje de Desc M.X.","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Porcentaje de Desc Dol","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Opera Fact. Vencido","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Opera Fact. Dist","celda01",ComunesPDF.CENTER);
		
		for (int i=0; i< alListadeEPO.size(); i++)
		{ 
			hmParametros = (HashMap)alListadeEPO.get(i);	
			
			pdfDoc.setCell(hmParametros.get("nombre")==null?"":(String)hmParametros.get("nombre"),"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(hmParametros.get("dmin")==null?"":(String)hmParametros.get("dmin"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(hmParametros.get("dmax")==null?"":(String)hmParametros.get("dmax"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(hmParametros.get("fecha_aforo")==null?"":(String)hmParametros.get("fecha_aforo"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(hmParametros.get("fecha_aforo_dol")==null?"":(String)hmParametros.get("fecha_aforo_dol"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(hmParametros.get("fact_venc")==null?"":(String)hmParametros.get("fact_venc"),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(hmParametros.get("fact_distr")==null?"":(String)hmParametros.get("fact_distr"),"formas",ComunesPDF.CENTER);
			
		}
		
		pdfDoc.addTable();
		pdfDoc.endDocument();
			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString(); 	
	
	} catch(Exception e)
	{
		throw new AppException("Error al generar el archivo", e);
	} finally 
		{ 
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
}
else if (informacion.equals("GeneraCSV"))
{
	
	AccesoDB con = new AccesoDB();
	CreaArchivo archivo = new CreaArchivo();	
	JSONObject jsonObj = new JSONObject();
	String nombreArchivo = "";
	
	try
	{
		con.conexionDB();
		
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		ic_epo = (request.getParameter("listaEPO")==null)?"":request.getParameter("listaEPO");
		
		/*Todos los pdf's llevan esto*/
		
		///////////////////////////////////////////////////////////////////////////
		
		
		ArrayList alListadeEPO = BeanParamDscto.getParamPorEPO(ic_epo, ic_banco_fondeo);
		StringBuffer contenidoArchivo = new StringBuffer();
		HashMap hmParametros = new HashMap();
		
		contenidoArchivo.append("EPO,");
		contenidoArchivo.append("Días Mín. para Desc,");
		contenidoArchivo.append("Días Máx. para Desc,");
		contenidoArchivo.append("Porcentaje de Desc MX,");
		contenidoArchivo.append("Porcentaje de Desc Dol,");
		contenidoArchivo.append("Opera Fact. Vencido,");
		contenidoArchivo.append("Opera Fact. Dist,");
		contenidoArchivo.append("\n");
		for (int i=0; i< alListadeEPO.size(); i++)
		{ 
			hmParametros = (HashMap)alListadeEPO.get(i);	
			
			contenidoArchivo.append(hmParametros.get("nombre")==null?",":(String)hmParametros.get("nombre")+ ",");
			contenidoArchivo.append(hmParametros.get("dmin")==null?",":(String)hmParametros.get("dmin")+ ",");
			contenidoArchivo.append(hmParametros.get("dmax")==null?",":(String)hmParametros.get("dmax")+ ",");
			contenidoArchivo.append(hmParametros.get("fecha_aforo")==null?",":(String)hmParametros.get("fecha_aforo")+ ",");
			contenidoArchivo.append(hmParametros.get("fecha_aforo_dol")==null?",":(String)hmParametros.get("fecha_aforo_dol")+ ",");
			contenidoArchivo.append(hmParametros.get("fact_venc")==null?",":(String)hmParametros.get("fact_venc")+ ",");
			contenidoArchivo.append(hmParametros.get("fact_distr")==null?",":(String)hmParametros.get("fact_distr")+ ",");
			contenidoArchivo.append("\n");
		}
		
		if (archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
			nombreArchivo=archivo.nombre;
											
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString(); 	
	
	} catch(Exception e)
	{
		throw new AppException("Error al generar el archivo", e);
	} finally 
		{ 
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
}

%>
<%=infoRegresar%>