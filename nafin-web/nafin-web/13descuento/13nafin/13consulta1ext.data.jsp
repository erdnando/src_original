<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	com.netro.model.catalogos.*,
	java.sql.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	netropology.utilerias.*,
	javax.naming.*,
	java.text.*,
	java.math.*,
	com.netro.threads.*,
	com.netro.exception.*,
	com.netro.pdf.*,
	com.netro.mandatodoc.*,
	com.netro.descuento.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";

String cc_acuse = (request.getParameter("cc_acuse") != null) ? request.getParameter("cc_acuse") : "";
String ic_epo = (request.getParameter("ic_epo") != null) ? request.getParameter("ic_epo") : "";
String fn_monto_dsctoMin = (request.getParameter("fn_montoMin2") != null) ? request.getParameter("fn_montoMin2") : "";
String fn_monto_dsctoMax = (request.getParameter("fn_montoMax2") != null) ? request.getParameter("fn_montoMax2") : "";
String ic_if = (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") : "";
String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") != null) ? request.getParameter("df_fecha_solicitudMin") : "";
String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") != null) ? request.getParameter("df_fecha_solicitudMax") : "";
String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
String ic_estado = (request.getParameter("ic_estado") != null) ? request.getParameter("ic_estado") : "";
String ic_folioMin = (request.getParameter("ic_folioMin") != null) ? request.getParameter("ic_folioMin") : "";
String ic_estatus_solic = (request.getParameter("ic_estatus_solic") != null) ? request.getParameter("ic_estatus_solic") : "";
String ic_moneda = (request.getParameter("ic_moneda") != null) ? request.getParameter("ic_moneda") : "";
String ic_tasa = (request.getParameter("ic_tasa") != null) ? request.getParameter("ic_tasa") : "";
String tipo_busqueda = (request.getParameter("ic_busqueda") != null) ? request.getParameter("ic_busqueda") : "";
String ordenaIf = (request.getParameter("ordenaIF") != null) ? request.getParameter("ordenaIF") : "";
String ordenaPyme = (request.getParameter("ordenaPyme") != null) ? request.getParameter("ordenaPyme") : "";
String ordenaEpo = (request.getParameter("ordenaEPO") != null) ? request.getParameter("ordenaEPO") : "";
String ordenaFecha = (request.getParameter("ordenaFecha") != null) ? request.getParameter("ordenaFecha") : "";
String ordenaEstatus = (request.getParameter("ordenaEstatus") != null) ? request.getParameter("ordenaEstatus") : "";
String ordenaFolio = (request.getParameter("ordenaFolio") != null) ? request.getParameter("ordenaFolio") : "";
//String esTotal2 = (request.getParameter("esTotal2") != null) ? request.getParameter("esTotal2") : "";
String aplicaFloating = (request.getParameter("aplicaFloating") != null) ? request.getParameter("aplicaFloating") : "";


int  start= 0, limit =0;
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();	
String infoRegresar="", consulta="", 	esBANCOMEXT  = "N";
	if("ADMIN BANCOMEXT".equals(strPerfil)) {	
		esBANCOMEXT 		= "S";
	}
if (informacion.equals("valoresIniciales")  ) {	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("esBANCOMEXT", esBANCOMEXT);	
	infoRegresar = jsonObj.toString();		
	
		
}else if (informacion.equals("catalogoEPO")){
	List resultCatEpo = new ArrayList();
	CatalogoEPO catalogo = new CatalogoEPO();	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");		
	resultCatEpo	= catalogo.getListaElementosInfoDoctosNafin();
		
	if(resultCatEpo.size() >0 ){	
		for (int i= 0; i < resultCatEpo.size(); i++) {			
			String registro =  resultCatEpo.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
	consulta =  "{\"success\": true, \"total\": \"" + resultCatEpo.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	
		
		
}else if(informacion.equals("catalogoIF")){

	List resultCatIF = new ArrayList();
	CatalogoIF catalogo = new CatalogoIF();	
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");		
	catalogo.setClaveEpo(ic_epo);
	
	if(!"".equals(ic_epo)){
		Hashtable alParametros = new Hashtable();
		String bOperaFideicomiso = "";
		
		ParametrosDescuento parametrosDescuento = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

		alParametros = parametrosDescuento.getParametrosEPO(ic_epo,1);
		//parametrosDescuento.actualizaPymesConExpedientesCargadosEnEfile(qd.getQueryStringWithParameters());	
		if (alParametros!=null) {
			bOperaFideicomiso = alParametros.get("CS_OPERA_FIDEICOMISO").toString();		
		}	
		System.out.println("$#%$#%  Valor de boperaFid es "+bOperaFideicomiso);
		if(bOperaFideicomiso.equals("S")){
			catalogo.setFideicomiso("N");
		}	
	}
	
	resultCatIF	= catalogo.getListaElementosIF();
			
	if(resultCatIF.size() >0 ){	
		for (int i= 0; i < resultCatIF.size(); i++) {			
			String registro =  resultCatIF.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
	consulta =  "{\"success\": true, \"total\": \"" + resultCatIF.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	
			
}else if (informacion.equals("catalogoPYME")){
				
	List resultCatPYME = new ArrayList();
	CatalogoPYME catalogo = new CatalogoPYME();	
	catalogo.setCampoClave("ic_pyme");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setConPymeEpoInterno(true);	
			
	catalogo.setClavesEpo(ic_epo);
	resultCatPYME	= catalogo.getListaElementos();
			
	if(resultCatPYME.size() >0 ){	
		for (int i= 0; i < resultCatPYME.size(); i++) {			
			String registro =  resultCatPYME.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
		
	consulta =  "{\"success\": true, \"total\": \"" + resultCatPYME.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	
				
				 
}else if (informacion.equals("catalogoEstado")){
	
	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_estado");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_estado");		
	catalogo.setOrden("cd_nombre");
	infoRegresar = catalogo.getJSONElementos();
			
}else if (informacion.equals("catalogoEstatus")){

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_estatus_solic");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_estatus_solic");		
	catalogo.setValoresCondicionNotIn("7,8,9", Integer.class);  
	infoRegresar = catalogo.getJSONElementos();
		
}else if (informacion.equals("catalogoMoneda")){

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("cd_nombre");
	catalogo.setValoresCondicionIn("1,54", Integer.class);  
	infoRegresar = catalogo.getJSONElementos();
		
}else if (informacion.equals("Consultar") || informacion.equals("ConsultarTotales")  ||  informacion.equals("ConsultarTotalesC") 
		|| informacion.equals("GenerarArchivoCSV")  || informacion.equals("GenerarArchivoPDF") || informacion.equals("ConsultarT2")) {
	
	String campos[]={"nombreEpo","nombreIf","df_fecha_solicitud","nombrePyme","cd_descripcion","ic_folio"};
	String orden[]={ordenaEpo,ordenaIf,ordenaFecha,ordenaPyme,ordenaEstatus,ordenaFolio};
	String criterioOrdenamiento=Comunes.ordenarCampos(orden,campos);
	
	ConsSolicNafinDE paginador = new ConsSolicNafinDE();
	paginador.setIc_if(ic_if);
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_folio(ic_folioMin);
	paginador.setDf_fecha_solicitudMin(df_fecha_solicitudMin);
	paginador.setDf_fecha_solicitudMax(df_fecha_solicitudMax);
	paginador.setIc_estatus_solic(ic_estatus_solic);
	paginador.setIc_moneda(ic_moneda);
	paginador.setFn_montoMin(fn_monto_dsctoMin);
	paginador.setFn_montoMax(fn_monto_dsctoMax);
	paginador.setTipoTasa(ic_tasa);
	paginador.setTipoBusqueda(tipo_busqueda);
	paginador.setAcuse(cc_acuse);
	paginador.setEstado(ic_estado);
	paginador.setOrdenadoBy(criterioOrdenamiento);
	paginador.setEsBANCOMEXT(esBANCOMEXT);
	paginador.setAplicaFloating(aplicaFloating);
	
	//paginador.setTotal2(esTotal2);

	//CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	CQueryHelperThreadRegExtJS queryHelper = new CQueryHelperThreadRegExtJS(paginador);

	if (informacion.equals("Consultar") ){
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));

		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) { //Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit); 

		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("esBANCOMEXT", esBANCOMEXT);
		infoRegresar = jsonObj.toString();

	} else if (informacion.equals("ConsultarTotales")) { //Datos para el Resumen de Totales
		//Debe ser llamado despues de Consulta
		queryHelper = new CQueryHelperThreadRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
		infoRegresar  = queryHelper.getJSONResultCount(request); //los saca de sesion	

	} else if (informacion.equals("ConsultarT2")) { //Datos para el Resumen de Totales
		//Debe ser llamado despues de Consulta
		//queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
		//infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion	
				Registros reg = paginador.getTotales2();
				consulta = "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
				jsonObj.put("success", new Boolean(true));
				jsonObj = JSONObject.fromObject(consulta);
				infoRegresar = jsonObj.toString();
	} else if (informacion.equals("GenerarArchivoCSV")){
		
		try {
		
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);	
			infoRegresar = jsonObj.toString();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	
	} else if (informacion.equals("GenerarArchivoPDF")){
		try {
			String estatusArchivo = request.getParameter("estatusArchivo");
			String porcentaje = "";
			String nombreArchivo = "";

			if(estatusArchivo.equals("INICIO")){
				session.removeAttribute("13consulta1ThreadCreateFiles");
				ThreadCreateFiles threadCreateFiles = queryHelper.getThreadCreateCustomFile(request, strDirectorioTemp, "PDF");
				session.setAttribute("13consulta1ThreadCreateFiles", threadCreateFiles);
				estatusArchivo = "PROCESANDO";
			} else if(estatusArchivo.equals("PROCESANDO")){
				ThreadCreateFiles threadCreateFiles = (ThreadCreateFiles)session.getAttribute("13consulta1ThreadCreateFiles");
				System.out.println("queryHelper.getPercent() = "+ threadCreateFiles.getPercent());
				porcentaje = String.valueOf(threadCreateFiles.getPercent()) + " %";
				if(!threadCreateFiles.isRunning() && !threadCreateFiles.hasError()){
					nombreArchivo = threadCreateFiles.getNombreArchivo();
					estatusArchivo = "FINAL";
				}else if(threadCreateFiles.hasError()){
					estatusArchivo = "ERROR";
				}
			}

			if(porcentaje.equals("") || porcentaje.equals("0") || porcentaje.equals("0 %"))
				porcentaje = "Procesando...";
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			jsonObj.put("estatusArchivo", estatusArchivo);
			jsonObj.put("porcentaje", porcentaje);

		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();
	}
/*
	Fodea 011-2015
	La opción de generar el PDF por paginación queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el método por si se requiere nuevamente 
	Fecha: 24/06/2015
	BY: Agustín Bautista Ruiz
*/
/*
else if (informacion.equals("GenerarArchivoPDF")){
	
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
			
		try {
		
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit, strDirectorioTemp, "PDF");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			infoRegresar = jsonObj.toString();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	
	}
*/
}
%>


<%=infoRegresar%>