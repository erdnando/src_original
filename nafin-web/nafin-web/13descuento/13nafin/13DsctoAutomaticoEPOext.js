Ext.onReady(function() {

var indiceFila = '';
var ic_proceso = 0;
var ic_epo		= 0;
/*----------------------------------------- HANDLER'S -----------------------------------------*/
	
	var procSuccessFailureProceso = function (opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var info = Ext.util.JSON.decode(response.responseText);					
			
						
			if(info.numTotales==info.numRegistro){
				Ext.MessageBox.alert('Mensaje','Se ejecuto el proceso.' );	
				pnl.el.unmask();
				consultaData.reload();						
			}else {
				ejecutarProceso(info.numRegistro);
			}
		}	
		else {			
			 NE.util.mostrarConnError(response,opts);
		}
	}
	

	var ejecutarProceso = function(numReg ) {
	
		var  grid = Ext.getCmp('grid');
		var store = grid.getStore();		
		var numTotal = store.getTotalCount();		
		
		store.each(function(record) {	
			
			var  numRegistro = store.indexOf(record);			
			var dt= Date();
			var fecha = Ext.util.Format.date(dt,'d/m/Y/h:i:s');
			var regis = numReg-1;			
			if(numRegistro==regis)  {
				record.data['CG_ESTATUS']='Ejecuci�n Completa';
				record.commit();
			}
			if(numRegistro==numReg)  {			
				record.data['CG_ESTATUS']='En Ejecuci�n';
				record.data['DF_FECHA_HORA_INI']=fecha;
				record.data['DF_FECHA_HORA_FIN']='';
				record.commit();
				
				var epo = record.get('IC_EPO');
				var icProceso = record.data['IC_PROCESO'];
								
				Ext.Ajax.request	({
					url: '13DsctoAutomaticoEPOext.data.jsp',	
					params: Ext.apply (fp.getForm().getValues(),{					 
						informacion: 'CorreProceso',
						ic_epo: epo,
						ic_proceso: icProceso,
						numRegistro:numRegistro,
						numTotal:numTotal
					}),
					callback: procSuccessFailureProceso
				});	
			}
			
		});		
	}
	
	var ejecutarProcesoTodo = function() {
		pnl.el.mask('Ejecutando Proceso ...', 'x-mask-loading');
		ejecutarProceso(0);
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 
	{
		pnl.el.unmask();
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
		var el = grid.getGridEl();
			
		if (arrRegistros != null )	{
			if (!grid.isVisible()) 	{ 
				grid.show();
			}
			
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();	
				Ext.getCmp('btnEjProceso').enable();
				if (arrRegistros == '') {
					Ext.getCmp('btnEjProceso').disable();
					grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}	
	}
	
	var detalle_doctos = function(grid, rowIndex, colIndex, item, event) 
	{		
		var registro 	= grid.getStore().getAt(rowIndex);
		ic_proceso  = registro.get('IC_PROCESO');
		ic_epo		= registro.get('IC_EPO'); 
		var ventana = 	Ext.getCmp('winDetalle');
		Ext.getCmp('icProceso_id').setValue(registro.get('IC_PROCESO'));
		if (ventana) {		
			ventana.show();			
		}
		else {
			new Ext.Window({
						width: 890,
						height: 300,
						id: 'winDetalle',
						closeAction:'hide',
						plain: true,
						modal: true,
						items: [gridDetalle]
						//title: nombreEPO
						}).show();			
		}
		
		consultaDetalle.load({
						params: Ext.apply(fp.getForm().getValues(),{	
							operacion: 'Generar',
							ic_proceso: Ext.getCmp('icProceso_id').getValue(),
							ic_epo: ic_epo,
							start: 0,
							limit: 15
						})
		});
	}
	
  var procesarSuccessFailureProceso = function (opts, success, response) {
	  var record = grid.getStore().getAt(indiceFila);  // Se Obtiene objeto Record del store
	  indiceFila = '';
	  record.set('CS_PROC_EJEC','');
	  
	  if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
	  
			Ext.MessageBox.alert('Mensaje de p�gina web','Se ejecuto el proceso.' );
			consultaData.reload();
		}
		else {
			 //btnGenerarArchivo.enable();
			 NE.util.mostrarConnError(response,opts);
		}
	}
	
	function leeRespuesta(){
		window.location = '13DsctoAutomaticoEPOext.jsp';
	}
	
	
/*----------------------------------- END  HANDLER'S ------------------------------------*/	
/* _____________________________________________________________________________________ */

/*-------------------------------------- STORE'S ----------------------------------------*/
	
	var catalogoDescuento = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['N','Normal'],
			['V','Vencido'],
			['I','Infonavit'],
			['VE','Factoraje Vencido EPO'],
			//['M','Mandato'],
			['II','Instrucci�n Irrevocable'],
			['D','Distribuido 1er piso '],
			['D2','Distribuido 2do piso '],
			['A','Factoraje IF'],
			['N19','Autom�tico EPO']
		 ]
	}) ;
	
	var catalogoModalidad = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['P','Primer d�a desde su publicaci�n'],
			['U','�ltimo d�a desde su Vencimiento considerando d�as m�nimos']
		 ]
	}) ;
	
	//-----------------------------------------------------------------------------//	
	/****** Store�s Grid�s *******/	
	var consultaData = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13DsctoAutomaticoEPOext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'IC_PROCESO'},
				{name: 'IC_EPO'},
				{name: 'CG_RAZON_SOCIAL'},
				{name: 'NUMDOCTOSMN'},
				{name: 'NUMDOCTOSDL'},
				{name: 'DF_FECHA_HORA_INI'},					
				{name: 'DF_FECHA_HORA_FIN'},
				{name: 'CG_ESTATUS'},
				{name: 'CS_PROC_EJEC'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData,
				exception: NE.util.mostrarDataProxyError
			}
	});
	
	
	/****** End Store�s Grid�s *******/
	
	
/*------------------------------- END STORE'S -----------------------------*/	


	/*********** Grid�s *************/

	var grid = new Ext.grid.EditorGridPanel
	({
		title:'',
		store: consultaData,
		margins: '20 0 0 0',
		clicksToEdit: 1,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		}, 
		columns: [
			{
				header:'Nombre de la EPO',
				tooltip:'Nombre de la EPO',			
				dataIndex:'CG_RAZON_SOCIAL',
				sortable:true,	
				resizable:true,	
				width:250,
				align : 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},
			{
				header:'Doctos Moneda Nacional',
				tooltip:'Doctos Moneda Nacional',
				dataIndex:'NUMDOCTOSMN',
				sortable:true,
				resizable:true,	
				width:90, 
				align:'center'
				
			},
			{
				header:'Doctos en D�lares',
				tooltip:'Doctos en D�lares',
				dataIndex:'NUMDOCTOSDL',
				sortable:true, 
				width:90, 	
				align:'center'
			},
			{
				header:'Proceso',
				tooltip:'Proceso',
				dataIndex:'IC_PROCESO',
				width:70, 
				align:'center',
				renderer:function(value) {	
					return '<img src="/nafin/00utils/gif/file_add.png" alt="Ejecutar Proceso" style="border-style:none" />';	
				}
			},
			{
				header:'Inicio �ltima Ejecuci�n',
				tooltip:'Inicio �ltima Ejecuci�n',	
				dataIndex:'DF_FECHA_HORA_INI',
				sortable:true,	
				width:120,	
				align:'center'//renderer:Ext.util.Format.dateRenderer('d/m/Y')
				
			},
			{
				header:'Fin �ltima Ejecuci�n', 		
				tooltip:'Fin �ltima Ejecuci�n',			
				dataIndex:'DF_FECHA_HORA_FIN',
				sortable:true,	
				width:120,	
				align:'center'				
			},
			{
				header:'Resultado',
				tooltip:'Resultado',
				dataIndex:'CG_ESTATUS',		
				sortable:true,	
				width:100,	
				align:'center'
			},
			{
				xtype:	'actioncolumn',
				header : 'Detalle', 
				tooltip: 'Detalle',
				dataIndex : 'DETALLE',
				width:	70,	
				align: 'center',
				
				items: [
					{
						getClass: function(value, metadata, record, rowIndex, colIndex, store) {
							//return 'iconoLupa';
							if (record.get('CG_ESTATUS') == ''){
								return 'N/A';
							}else{
								return 'iconoLupa';
							}

						},
						handler:	detalle_doctos
					}
				]
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,	
		//autoHeight:true,
		height: 400,
		width:900,
		frame:true, 
		hidden:true, 
		id: 'grid',
		listeners: {
			cellclick :function(grid, rowIndex, colIndex, evento){
					var record = grid.getStore().getAt(rowIndex);  // Se Obtiene objeto Record del store
					var fieldName = grid.getColumnModel().getDataIndex(colIndex); // Se obtine nombre del Campo (Fields del Store)
					var ejecucion = record.get('CS_PROC_EJEC');
					
					if(fieldName == 'IC_PROCESO' && ejecucion !='S' ) {
						
						indiceFila = rowIndex;
						record.set('CS_PROC_EJEC','S');
						
						var dt= Date();
						var fecha = Ext.util.Format.date(dt,'d/m/Y/h:i:s');
						record.set('CG_ESTATUS','En Ejecuci�n');
						record.set('DF_FECHA_HORA_INI',fecha);
						record.set('DF_FECHA_HORA_FIN','');
						
						var epo = record.get('IC_EPO');
						var ic_proceso = record.get('IC_PROCESO');
						Ext.Ajax.request
						({
							url: '13DsctoAutomaticoEPOext.data.jsp',	
							params: Ext.apply (fp.getForm().getValues(),{					 
								informacion: 'CorreProceso',
								ic_epo: epo,
								ic_proceso: ic_proceso
							}),
							callback: procesarSuccessFailureProceso
						});						
					}
			}
		},
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Ejecutar Proceso TODO',
					id: 'btnEjProceso',
					iconCls: 'icoAceptar',
					handler: ejecutarProcesoTodo					
				}
			]
		}
	});

	/****  End Grid�s ****/
	
	
	/* -----------------  Pop Up -----------------  */

	 /* 1.- Handler */
	 
	var procesarConsultaDetalle = function(store, arrRegistros, opts) {
	//var fp = Ext.getCmp('forma');
	//	fp.el.unmask();		
			
		var gridDetalle = Ext.getCmp('gridDetalle');
		if (arrRegistros != null ) {			
				
			if (!gridDetalle.isVisible()) {
				gridDetalle.show();
			}						
		}
		
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		var el = gridDetalle.getGridEl();
		btnGenerarArchivo.enable();
		if (arrRegistros == '') {
			el.mask('No se encontr� ning�n registro', 'x-mask');
			btnGenerarArchivo.disable();
		}
		else if (arrRegistros != ''){
			el.unmask();
			btnGenerarArchivo.enable();
		}
		
	}			
	
	var procesarSuccessFailureGenerarCSV = function (opts, success, response) 
  {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		}
		else 
		{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
  }
	 /* 2.- Store 	 */
	 var consultaDetalle = new Ext.data.JsonStore
	({
			root:'registros',
			url:'13DsctoAutomaticoEPOext.data.jsp',
			baseParams:{informacion:'ConsultaDetalle'},
			totalProperty: 'total',
			fields: [
				{name: 'NOMBRE_EPO'},
				{name: 'NAFIN_ELECTRO'},
				{name: 'NOMBRE_PYME'},
				{name: 'RFCPYME'},
				{name: 'NUMDOCTOSMN'},
				{name: 'NUMDOCTOSDL'},
				{name: 'MONTOMN'},
				{name: 'MONTODL'}
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				beforeLoad: {fn: function(store, options){
					Ext.apply(options.params,{
						ic_proceso: Ext.getCmp('icProceso_id').getValue()
					});
				}},
				load: procesarConsultaDetalle,
				exception: NE.util.mostrarDataProxyError
			}
	});

	 /* 3.- Grid	 */
	
	var gridDetalle = new Ext.grid.GridPanel
	({
		title:'',
		store: consultaDetalle,
		id: 'gridDetalle',
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask:true,	
		//autoHeight:true,
		width:875,
		height: 265,
		frame:true, 
		hidden:true, 
		header:true,
		columns: [
			{
				header:'EPO',
				tooltip:'EPO',			
				dataIndex:'NOMBRE_EPO',
				sortable:true,	
				resizable:true,	
				width:250,
				align : 'center',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},
			{
				header:'N@E PyME',
				tooltip: 'N@E PyME',
				dataIndex:'NAFIN_ELECTRO',
				sortable:true,
				resizable:true,	
				width:100, 
				align:'center'
				
			},
			{
				header:'PyME',
				tooltip: 'PyME',
				dataIndex:'NOMBRE_PYME',
				sortable:true, 
				width:100, 	
				align:'center'
			},
			{
				header:'RFC',
				tooltip:'RFC',
				dataIndex:'RFCPYME',
				sortable:true, 
				width:100, 
				align:'center'				
			},
			{
				header:'Documentos Moneda Nacional',
				tooltip:'Documentos Moneda Nacional',
				dataIndex:'NUMDOCTOSMN',
				sortable:true,	
				width:100,	
				align:'center'//renderer:Ext.util.Format.dateRenderer('d/m/Y')
				
			},
			{
				header:'Documentos en Dol�res',
				tooltip:'Documentos en Dol�res',
				dataIndex:'NUMDOCTOSDL',
				sortable:true,	
				width:100,	
				align:'center'				
			},
			{
				header:'Monto Total Moneada Nacional',
				tooltip:'Monto Total Moneada Nacional',
				dataIndex:'MONTOMN',		
				sortable:true,	
				width:100,	
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0.00')
			},
			{
				header:'Monto Total Dol�res',
				tooltip:'Monto Total Dol�res',
				dataIndex:'MONTODL',
				sortable:true,	
				width:100,	
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0.00')
			}
		],
		
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaDetalle,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',
				{
					xtype: 'button',
					text: 'Descargar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) 
					{
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						
						Ext.Ajax.request
						({
							url: '13DsctoAutomaticoEPOext.data.jsp',	
							params: Ext.apply (fp.getForm().getValues(),{					 
								informacion: 'GeneraCSV',
								ic_proceso : ic_proceso,
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarCSV
						});
					}
				},
				
				'-'
			]
		}
	});
	
	/* --------------  END Pop Up --------------  */
	
/*******************Componentes*******************************/
	
	var elementosForma = [{
		xtype:             'panel',
		labelWidth:        110,
		layout:            'form',
		items: [{
			xtype:          'combo',
			id:             'id_descuento',
			name:           'descuento',
			hiddenName:     'descuento',
			fieldLabel:     'Tipo de Descuento',
			width:          300,
			forceSelection: true,
			triggerAction:  'all',
			mode:           'local',
			allowBlank:     false,
			valueField:     'clave',
			displayField:   'descripcion',
			store:          catalogoDescuento,
			margins:        '0 20 0 0',
			msgTarget:      'side',
			listeners: {
				select: function(combo) {
					Ext.getCmp('id_modalidad').reset();
					var tipo_descuento = Ext.getCmp('id_descuento').getValue();
					if (tipo_descuento === 'N' || tipo_descuento === 'V' || tipo_descuento === 'D2')
						Ext.getCmp('id_modalidad').setVisible(true);
					else
						Ext.getCmp('id_modalidad').hide(true);
				}
			}
		},{
			xtype:          'combo',
			id:             'id_modalidad',
			name:           'modalidad',
			hiddenName:     'modalidad',
			fieldLabel:     'Modalidad',
			width:          300,
			forceSelection: true,
			triggerAction:  'all',
			mode:           'local',
			margins:        '0 20 0 0',
			msgTarget:      'side',
			hidden:         true,
			allowBlank:     true,
			valueField:     'clave',
			displayField:   'descripcion',
			store:          catalogoModalidad
		},{
			xtype:          'textfield',
			id:             'icProceso_id',
			name:           'icProceso',
			hidden:         true
		}]
	}];




	var fp = new Ext.form.FormPanel
	({
		id: 'forma',
		width: 500,
		style: ' margin:0 auto;',
		title: 'Descuento Autom�tico',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 110,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					Ext.getCmp('icProceso_id').setValue('');
					var tipo_descuento = Ext.getCmp('id_descuento').getValue();
					var modal			 = Ext.getCmp('id_modalidad');
					if (tipo_descuento === 'N' || tipo_descuento === 'V' || tipo_descuento === 'D2') {
						if(Ext.isEmpty(modal.getValue())){
							modal.markInvalid('El campo Modalidad es requerido para el Tipo de Descuento seleccionado');
							modal.focus();
							return;
						}
					}
					pnl.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues())
					});	
					
				} //fin handler
			}, //fin boton Consultar
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					leeRespuesta();
				}
			}
		]
	});
	var pnl = new Ext.Container
	({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [			
			fp,	
			NE.util.getEspaciador(20),				
			grid
		]
	});
	
	
});