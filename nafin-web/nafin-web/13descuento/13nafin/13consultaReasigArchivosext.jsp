<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,	
	java.sql.*,	
   java.text.*, 
   javax.naming.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject,
	com.netro.pdf.*, 	
	netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />

<%
String epo = (request.getParameter("epo") == null)?"":request.getParameter("epo");
String cg_no_electronico_prov = (request.getParameter("cg_no_electronico_prov") == null)?"":request.getParameter("cg_no_electronico_prov");
String cg_no_provedor = (request.getParameter("cg_no_provedor") == null)?"":request.getParameter("cg_no_provedor");
String cg_rfc = (request.getParameter("cg_rfc") == null)?"":request.getParameter("cg_rfc");
String cg_no_docto = (request.getParameter("cg_no_docto") == null)?"":request.getParameter("cg_no_docto");
String cg_usuario_reasigno = (request.getParameter("cg_usuario_reasigno") == null)?"":request.getParameter("cg_usuario_reasigno");
String df_reasignacion_i = (request.getParameter("df_reasignacion_i") == null)?"":request.getParameter("df_reasignacion_i");
String df_reasignacion_f = (request.getParameter("df_reasignacion_f") == null)?"":request.getParameter("df_reasignacion_f");
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String hidnombreArchivo = (request.getParameter("hidnombreArchivo")!=null)?request.getParameter("hidnombreArchivo"):"";
String infoRegresar ="", nombreArchivo="";

if(informacion.equals("GeneraArchivo"))  {

AccesoDB con = new AccesoDB();
ResultSet rs = null;
IMantenimiento IMantenimientos = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);

try	{

//>>CREA REPORTE PDF
	CreaArchivo archivo = new CreaArchivo();
	nombreArchivo = archivo.nombreArchivo()+".pdf";
//<<CREA REPORTE PDF	
Calendar fecha = Calendar.getInstance();
List vd = null;
List vDoctosCargados = new ArrayList();

	try {
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String nombreMesIngles[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		int mes = fecha.get(Calendar.MONTH);
		
	    pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	                                 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			    					 (String)session.getAttribute("sesExterno"),
	                                 (String) session.getAttribute("strNombre"),
                                     (String) session.getAttribute("strNombreUsuario"),
						    		 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
									
		
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
	
		float widths[] = {3,5,7};
		
		int iColSpan =19;
		int numCols = 12;

		float widthsDocs[] = new float[numCols];
		
		pdfDoc.setTable(19,100);

		pdfDoc.setCell("Documentos Reasignados","formasmenB",ComunesPDF.CENTER,iColSpan);
		pdfDoc.setCell("Fecha Reasignación","formasmenB",ComunesPDF.CENTER,1,2);		
		pdfDoc.setCell("PYME ORIGEN","formasmenB",ComunesPDF.CENTER,5);	
		pdfDoc.setCell("Nombre EPO","formasmenB",ComunesPDF.CENTER,1,2);	
		pdfDoc.setCell("PYME DESTINO","formasmenB",ComunesPDF.CENTER,5);	
		pdfDoc.setCell("No. Documento","formasmenB",ComunesPDF.CENTER,1,2);	
		pdfDoc.setCell("Fecha Documento","formasmenB",ComunesPDF.CENTER,1,2);							
		pdfDoc.setCell("Fecha Vencimiento","formasmenB",ComunesPDF.CENTER,1,2);	
		pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER,1,2);	
		pdfDoc.setCell("Tipo Factoraje","formasmenB",ComunesPDF.CENTER,1,2);	
		pdfDoc.setCell("Monto Documento","formasmenB",ComunesPDF.CENTER,1,2);									
		pdfDoc.setCell("Causas de Rechazo","formasmenB",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Nombre","formasmenB",ComunesPDF.CENTER);	
		pdfDoc.setCell("RFC","formasmenB",ComunesPDF.CENTER);	
		pdfDoc.setCell("Dirección","formasmenB",ComunesPDF.CENTER);	
		pdfDoc.setCell("Colonia","formasmenB",ComunesPDF.CENTER);	
		pdfDoc.setCell("Teléfono","formasmenB",ComunesPDF.CENTER);	
		pdfDoc.setCell("Nombre","formasmenB",ComunesPDF.CENTER);	
		pdfDoc.setCell("RFC","formasmenB",ComunesPDF.CENTER);	
		pdfDoc.setCell("Dirección","formasmenB",ComunesPDF.CENTER);	
		pdfDoc.setCell("Colonia","formasmenB",ComunesPDF.CENTER);	
		pdfDoc.setCell("Teléfono","formasmenB",ComunesPDF.CENTER);	
		
		vDoctosCargados = IMantenimientos.mostrarDocumentosReasignados(epo, cg_no_electronico_prov, cg_no_provedor, cg_rfc, cg_no_docto, cg_usuario_reasigno, df_reasignacion_i,df_reasignacion_f,strDirectorioTemp);
		for(int i=0;i<vDoctosCargados.size();i++){
			vd = (List)vDoctosCargados.get(i);
			long nunreg = vDoctosCargados.size ();
			if(nunreg > 0){
				pdfDoc.setCell(vd.get(0).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(1).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(2).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(3).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(4).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(5).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(6).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(7).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(8).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(9).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(10).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell(vd.get(11).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell((vd.get(12)==null)?"":vd.get(12).toString(),"formasmen",ComunesPDF.CENTER);
				
				pdfDoc.setCell((vd.get(13)==null)?"":vd.get(13).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell((vd.get(14)==null)?"":vd.get(14).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell((vd.get(15)==null)?"":vd.get(15).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell((vd.get(16)==null)?"":vd.get(16).toString(),"formasmen",ComunesPDF.CENTER);
				pdfDoc.setCell((vd.get(17)==null)?"":vd.get(17).toString(),"formasmen",ComunesPDF.CENTER);				
				pdfDoc.setCell((vd.get(23)==null)?"":vd.get(23).toString(),"formasmen",ComunesPDF.CENTER);
				 
			}else{
				pdfDoc.setCell("No hay registros que mostrar","formasmen",ComunesPDF.CENTER,25);
			}
		}
		pdfDoc.addTable();
		
		pdfDoc.endDocument();
		
	} catch (Exception e) {
		e.printStackTrace();
	}


	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar = jsonObj.toString();
		
} catch(Exception e) { 
	e.printStackTrace();
	out.println("Error: " + e.getMessage()); 
}
finally{
	if (con.hayConexionAbierta()){
		con.cierraConexionDB();
	}
}

}else if(informacion.equals("documentoAutorizacion"))  {

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+hidnombreArchivo);	
	infoRegresar = jsonObj.toString();

}

%>

<%=infoRegresar%>


