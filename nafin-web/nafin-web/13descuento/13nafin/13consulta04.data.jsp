<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.model.catalogos.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%
String informacion           = (request.getParameter("informacion")           !=null)?request.getParameter("informacion"):"";
String ic_banco_fondeo       = "";//(request.getParameter("ic_banco_fondeo")!=null)?request.getParameter("ic_banco_fondeo"):"1";
String ic_epo                = (request.getParameter("ic_epo")                !=null)?request.getParameter("ic_epo"):"";
String ic_pyme               = (request.getParameter("ic_pyme")               !=null)?request.getParameter("ic_pyme"):"";
String ic_if                 = (request.getParameter("ic_if")                 !=null)?request.getParameter("ic_if"):"";
String ic_folio              = (request.getParameter("ic_folio")              !=null)?request.getParameter("ic_folio"):"";
String ig_plazoMin           = (request.getParameter("ig_plazoMin")           !=null)?request.getParameter("ig_plazoMin"):"";
String ig_plazoMax           = (request.getParameter("ig_plazoMax")           !=null)?request.getParameter("ig_plazoMax"):"";
String ic_moneda             = (request.getParameter("ic_moneda")             !=null)?request.getParameter("ic_moneda"):"";
String ic_cambio_estatus     = (request.getParameter("ic_cambio_estatus")     !=null)?request.getParameter("ic_cambio_estatus"):"";
String fn_montoMin           = (request.getParameter("fn_montoMin")           !=null)?request.getParameter("fn_montoMin"):"";
String fn_montoMax           = (request.getParameter("fn_montoMax")           !=null)?request.getParameter("fn_montoMax"):"";
String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") !=null)?request.getParameter("df_fecha_solicitudMin"):"";
String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") !=null)?request.getParameter("df_fecha_solicitudMax"):"";
String dc_fecha_cambioMin    = (request.getParameter("dc_fecha_cambioMin")    !=null)?request.getParameter("dc_fecha_cambioMin"):"";
String dc_fecha_cambioMax    = (request.getParameter("dc_fecha_cambioMax")    !=null)?request.getParameter("dc_fecha_cambioMax"):"";
String  operacion            = (request.getParameter("operacion")             !=null)?request.getParameter("operacion"):"";
int  start= 0, limit =0;

String infoRegresar = "", consulta ="";
JSONObject jsonObj = new JSONObject();

if(informacion.equals("CatalogoBanco")) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_banco_fondeo");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_banco_fondeo");	
	if("ADMIN BANCOMEXT".equals(strPerfil)) {
		catalogo.setValoresCondicionIn("2", Integer.class);
	}else {
		catalogo.setValoresCondicionIn("1,2", Integer.class);
	}
	infoRegresar = catalogo.getJSONElementos();

}else  if( informacion.equals("catalogoEPO") ){

	List catEPO = getCatalogoEP0(ic_banco_fondeo);
	jsonObj.put("registros", catEPO);
	jsonObj.put("success",  new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if( informacion.equals("catalogoPYME") && !ic_epo.equals("") ) {

	CatalogoPYME catalogo = new CatalogoPYME();
	catalogo.setCampoClave("ic_pyme");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setNoepo(ic_epo);
	catalogo.setOrden("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();

}else  if( informacion.equals("catalogoIF") ) {

	CatalogoIF catalogo = new CatalogoIF();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setOrden("cg_razon_social");
	catalogo.setG_fondeo(ic_banco_fondeo);	
	infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("catalogoMoneda")) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("CD_NOMBRE");
	catalogo.setTabla("comcat_moneda");	
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("catalogoEstatus")) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_cambio_estatus");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_cambio_estatus");	
	catalogo.setValoresCondicionIn("7", Integer.class);
	infoRegresar = catalogo.getJSONElementos();

}else if(informacion.equals("Consultar") || informacion.equals("ConsultarTotales") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")) {

	ConsCStSolicNafinDE paginador = new ConsCStSolicNafinDE();
	paginador.setIc_banco_fondeo(ic_banco_fondeo);
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_if(ic_if);
	paginador.setIc_folio(ic_folio);
	paginador.setIg_plazoMin(ig_plazoMin);
	paginador.setIg_plazoMax(ig_plazoMax);
	paginador.setIc_moneda(ic_moneda);
	paginador.setIc_cambio_estatus(ic_cambio_estatus);
	paginador.setFn_montoMin(fn_montoMin);
	paginador.setFn_montoMax(fn_montoMax);
	paginador.setDf_fecha_solicitudMin(df_fecha_solicitudMin);
	paginador.setDf_fecha_solicitudMax(df_fecha_solicitudMax);
	paginador.setDc_fecha_cambioMin(dc_fecha_cambioMin);
	paginador.setDc_fecha_cambioMax(dc_fecha_cambioMax);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

	if(informacion.equals("Consultar") ){
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
					
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

		jsonObj = JSONObject.fromObject(consulta);

	} else if(informacion.equals("ConsultarTotales")) {

		consulta = queryHelper.getJSONResultCount(request);
		jsonObj = JSONObject.fromObject(consulta);

	} else if(informacion.equals("ArchivoPDF") ){
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		
	} else if(informacion.equals("ArchivoCSV")){
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
/*
	Fodea 011-2015
	La opción de generar el PDF por paginación queda obsoleta, ya que ahora se genera el archivo con todos los registros del grid. 
	Pero se deja el método por si se requiere nuevamente 
	Fecha: 29/06/2015
	BY: Agustín Bautista Ruiz
*/
/*
else if(informacion.equals("ArchivoPDF") ){
	
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		
	}
*/

	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>


<%!

public List  getCatalogoEP0( String ic_banco_fondeo ){
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;	
	HashMap datos =  new HashMap();
	List registros  = new ArrayList();
	try{
		con.conexionDB();
		
		String SQL =" select distinct e.ic_epo, e.cg_razon_social "   +
					" from comhis_cambio_estatus cce,"   +
					"      com_documento d,"   +
					"   comcat_epo e, comrel_producto_epo pe "   +
					" where cce.ic_documento=d.ic_documento"   +
					" and   d.ic_epo=e.ic_epo"   +
					" and d.ic_epo = pe.ic_epo " +
					" and pe.ic_producto_nafin = 1 " +				
					" and exists (select 1"   +
					"             from com_solicitud p"   +
					" 			where d.ic_documento=p.ic_documento)"   +
					" and   e.cs_habilitado='S'"   +
					" and cce.ic_cambio_estatus = 7"   +
					(ic_banco_fondeo.equals("")?"":" and e.ic_banco_fondeo=" + ic_banco_fondeo + " " ) +
					" order by 2"  ;

		rs = con.queryDB(SQL);			
		while(rs.next()) {			
			datos = new HashMap();			
			datos.put("clave", rs.getString(1));
			datos.put("descripcion", rs.getString(2));
			registros.add(datos);
		}
		rs.close();
		con.cierraStatement();
	
	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	} 
	return registros;
}
%>