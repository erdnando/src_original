Ext.onReady(function() {

	var limite = 'N';
/*----------------------------------------- Hanlers -----------------------------------------*/
	var procesarCatalogoBanco = function(store, arrRegistros, opts) {
		if(store.getTotalCount() > 1) {
			Ext.getCmp('id_bco_fondeo').setValue('1');
			catalogoIF.load({params: {fondeo: '1' }});
			catalogoEpo.load({params: {fondeo: '1' }});	
		}		
		else if(store.getTotalCount() > 0) { //iTipoPerfil == "8"
			Ext.getCmp('id_bco_fondeo').setValue('2');	
			catalogoIF.load({params: {fondeo: '2' }});
			catalogoEpo.load({params: {fondeo: '2' }});	
		}
	}
	
	var procesarCatalogoEpo = function(store, arrRegistros, opts) {
		if (!gridS.isVisible()) 	
			gridS.hide();
		if (!gridC.isVisible()) 	
			gridC.hide();
	}
	
	 var procesarCatalogoProveedor = function(store, arrRegistros, opts) 
	 {
		Ext.getCmp('id_nombre_proveedor').setVisible(true);
		if (gridS.isVisible()) 	gridS.hide();
		if (gridC.isVisible()) 	gridC.hide();
	}
	
	var procesarSuccessFailureVerificaLimites =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var res = Ext.util.JSON.decode(response.responseText).limitesEPO
			if ( res == 'S') {
				limite = 'S';
				catalogoProveedor.load({
					params : Ext.apply({
						ic_epo : (Ext.getCmp('id_cmb_epo')).getValue(),
						ic_if : (Ext.getCmp('id_nombre_if')).getValue()
					}),
					callback: procesarCatalogoProveedor
				});
			} 
			else {
				limite = 'N';
				Ext.getCmp('id_nombre_proveedor').hide();
			}
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 
	{
		pnl.el.unmask();
		var fp = Ext.getCmp('forma');
		
		if (arrRegistros != null )	{
			if (!gridS.isVisible()) 	{ 
				gridS.show();
				gridC.hide();
			}
			
			/*declaro botones y una variable para manipular el grid*/
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoS');
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDFS'); 
			var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivoS');
			var btnBajarPDF 		 = Ext.getCmp('btnBajarPDFS');
			var el = gridS.getGridEl();
			btnGenerarArchivo.enable();
			btnImprimirPDF.enable();
			btnBajarArchivo.hide();
			btnBajarPDF.hide();
			
			
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();			
				if (arrRegistros == '') {
					gridS.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnGenerarArchivo.disable();
					btnImprimirPDF.disable();
				}
				
				else if (arrRegistros != '')	{
					gridS.getGridEl().unmask();
					btnGenerarArchivo.enable();
					btnImprimirPDF.enable();
				}
			}
		}	
	}
	
	var procesarConsultaDataLimites = function(store, arrRegistros, opts) 
	{
		pnl.el.unmask();
		var fp = Ext.getCmp('forma');
		
		if (arrRegistros != null )	{
			if (!gridC.isVisible()) 	{ 
				gridC.show();
				gridS.hide();
			}
			
			/*declaro botones y una variable para manipular el grid*/
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoC');
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDFC'); 
			var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivoC');
			var btnBajarPDF 		 = Ext.getCmp('btnBajarPDFC');
			var el = gridC.getGridEl();
			btnGenerarArchivo.enable();
			btnImprimirPDF.enable();
			btnBajarArchivo.hide();
			btnBajarPDF.hide();
			
			
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();	
				if (arrRegistros == '') {
					gridC.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnGenerarArchivo.disable();
					btnImprimirPDF.disable();
				}
				
				else if (arrRegistros != '')	{
					//gridC.getGridEl().unmask();
					
					btnGenerarArchivo.enable();
					btnImprimirPDF.enable();
				}
			}
		}	
	}
	
	
	function leeRespuesta(){
		window.location = '13consulta07ext.jsp';
	}
	
	
	var procesarSuccessFailureGenerarCSVS = function(opts, success, response) 
	{
		 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoS');
		 var btnBajarArchivo = Ext.getCmp('btnBajarArchivoS');
		 btnGenerarArchivo.setIconClass('');
		 btnBajarArchivo.setIconClass('icoXls');
		 
		 
		 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		 {
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler(function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		 } else {
			 btnGenerarArchivo.enable();
			 NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarCSVC = function(opts, success, response) 
	{
		 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoC');
		 var btnBajarArchivo = Ext.getCmp('btnBajarArchivoC');
		 btnGenerarArchivo.setIconClass('');
		 btnBajarArchivo.setIconClass('icoXls');
		 
		 
		 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		 {
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler(function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		 } else {
			 btnGenerarArchivo.enable();
			 NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarSuccessFailureGenerarPDFS = function(opts, success, response) 
   {
		var btnBajarPDF = Ext.getCmp('btnBajarPDFS');
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDFS');
		btnBajarPDF.setIconClass('icoPdf');
		btnImprimirPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPDFC = function(opts, success, response) 
   {
		var btnBajarPDF = Ext.getCmp('btnBajarPDFC');
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDFC');
		btnBajarPDF.setIconClass('icoPdf');
		btnImprimirPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	
/*--------------------------------- STORE'S -------------------------------*/
//-----------------------------------------------------------------------------//

	var catalogoBanco = new Ext.data.JsonStore({
	   id				: 'catBanco',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13consulta07ext.data.jsp',
		baseParams	: { informacion: 'CatalogoBanco'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoBanco,
			exception: NE.util.mostrarDataProxyError
		}
	});	
	
	var catalogoIF = new Ext.data.JsonStore({
	   id				: 'catIf',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13consulta07ext.data.jsp',
		baseParams	: { informacion: 'CatalogoIF'},
		autoLoad		: false,
		listeners	:
		{
			exception: NE.util.mostrarDataProxyError
		}
	});	
	
	var catalogoEpo = new Ext.data.JsonStore({
	   id				: 'catEpo',
		root 			: 'registros',
		fields 		: ['clave', 'descripcion', 'loadMsg'],
		url 			: '13consulta07ext.data.jsp',
		baseParams	: { informacion: 'CatalogoEPO'},
		totalProperty : 'total',
		autoLoad		: true,
		listeners	:
		{
			load:procesarCatalogoEpo,
			exception: NE.util.mostrarDataProxyError 
		}
	});		
	
	var catalogoProveedor = new Ext.data.JsonStore({
	   id				: 'catProveedor',
		root 			: 'registros',
		fields 		: ['rs_pyme', 'rs_nombre_pyme', 'loadMsg'],
		url 			: '13consulta07ext.data.jsp',
		baseParams	: { informacion: 'CatalogoProveedor'},
		autoLoad		: false,
		listeners	:
		{
			load:procesarCatalogoProveedor,
			exception: NE.util.mostrarDataProxyError
		}
	});		
//-----------------------------------------------------------------------------//	
	/****** Store�s Grid�s *******/	
	var consultaData = new Ext.data.JsonStore
	({		
			root:'registros',	
			url:'13consulta07ext.data.jsp',	
			baseParams:{informacion:'ConsultaGrid'},	
			totalProperty: 'total',		
			fields: [	
				{name: 'NOMBRE_EPO'},
				{name: 'IF_NOMBRE'},
				{name: 'LIMITE'},
				{name: 'PORCENTAJE'},
				{name: 'MONTO'},					
				{name: 'ESTATUS'},
				{name: 'DF_VENC_LINEA'},					
				{name: 'AVISO_VENC_LINEA '},
				{name: 'CAMBIO_ADMON'},
				{name: 'AVISO_CAMBIO_ADMON'},
				{name: 'MONTO_COMPROMETIDO'},
				{name: ''},//RESTA
				{name: 'CSFECHALIMITE'},
				{name: 'LIMITE_DL'},
				{name: 'PORCENTAJE_DL'},
				{name: 'MONTO_DL'},	
				{name: 'MONTO_COMPROMETIDO_DL'},
				{name: 'DISPONIBLE_DES'},
				{name: 'DISPONIBLE_DES_DL'},
				{name: 'FECHA_BLOQ_DESB'},
				{name: 'USUARIO_BLOQ_DESB'},
				{name: 'DEPENDENCIA_BLOQ_DESB'}	
			],
			messageProperty: 'msg',
			autoLoad: false,
			listeners: 
			{
				load: procesarConsultaData,
				exception: NE.util.mostrarDataProxyError
			}
	});
	
	var consultaDataLimites = new Ext.data.JsonStore
	({
			root : 'registros',
			url : '13consulta07ext.data.jsp',
			baseParams: { informacion: 'ConsultaGridLimites'	},
			hidden: true,
			fields: [	
				{name: 'EPO'},
				{name: 'IF'},
				{name: 'PYME'},
				{name: 'LIMITE'},
				{name: 'PORCENTAJE'},
				{name: 'UTILIZADO'},
				{name: 'ESTATUS'},
				{name: 'MONTO_COMPROMETIDO'},
				{name: ''},//RESTA
				{name: 'CSFECHALIMITE'}						
			],	
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,		
			listeners: 
			{
				load: procesarConsultaDataLimites,
				exception: NE.util.mostrarDataProxyError
			}		
	});
	/****** End Store�s Grid�s *******/
	
	
	/*------------------------------------------ Grids ------------------------------------------*/

	var grupoSinLimites = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 12, align: 'center'},
					{header: 'Vencimiento L�nea de Cr�dito', colspan: 2, align: 'center'},
					{header: 'Cambio de Administraci�n', colspan: 3, align: 'center'},
					{header: 'Cambio de Estatus', colspan: 4, align: 'center'}					
				]
			]
	});	
	
	var gridS = new Ext.grid.GridPanel
	({
		title:'',
		id: 'gridS',
		store: consultaData,
		style: ' margin:0 auto;',
		plugins: grupoSinLimites,
		stripeRows: true,
		loadMask:false,
		hidden: true,
		height:420,
		width:900,	
		frame:true, 
		header:true,
		columns: [
			{										
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 250,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex : 'IF_NOMBRE',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},		
			{
										
				header : '<center> Monto L�mite <br>Moneda Nacional',
				tooltip: 'Monto L�mite Moneda Nacional',
				dataIndex : 'LIMITE',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
										
				header : '<center> Porcentaje de Utilizaci�n <br>Moneda Nacional',
				tooltip: 'Porcentaje de Utilizaci�n Moneda Nacional',
				dataIndex : 'PORCENTAJE',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000%')
			},
			{
										
				header : '<center> Monto Disponible <br>Moneda Nacional',
				tooltip: 'Monto Disponible Moneda Nacional',
				dataIndex : 'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			
			{
										
				header : '<center> Monto L�mite <br>D�lar Americano',
				tooltip: 'Monto L�mite D�lar Americano',
				dataIndex : 'LIMITE_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
										
				header : '<center> Porcentaje de Utilizaci�n <br>D�lar Americano',
				tooltip: 'Porcentaje de Utilizaci�n D�lar Americano',
				dataIndex : 'PORCENTAJE_DL',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000%')
			},
			{
										
				header : '<center> Monto Disponible <br>D�lar Americano',
				tooltip: 'Monto Disponible D�lar Americano',
				dataIndex : 'MONTO_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{										
				header : '<center> Monto Comprometido <br>Moneda Nacional',
				tooltip: 'Monto Comprometido <br>Moneda Nacional',
				dataIndex : 'MONTO_COMPROMETIDO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
				{										
				header : '<center> Monto Comprometido <br>D�lar Americano',
				tooltip: 'Monto Comprometido  <br>D�lar Americano',
				dataIndex : 'MONTO_COMPROMETIDO_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},			
			{										
				header : '<center>Monto Disponible despu�s de <br> Comprometido Moneda Nacional',
				tooltip: '<center>Monto Disponible despu�s de <br> Comprometido Moneda Nacional',
				dataIndex : '',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				dataIndex: 'DISPONIBLE_DES',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{										
				header : '<center>Monto Disponible despu�s de <br> Comprometido D�lar Americano',
				tooltip: '<center>Monto Disponible despu�s de <br> Comprometido D�lar Americano',
				dataIndex : '',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				dataIndex: 'DISPONIBLE_DES_DL',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			//Vencimiento L�nea de Cr�dito
			{										
				header : 'Fecha',
				tooltip: 'Fecha',
				dataIndex : 'DF_VENC_LINEA',
				sortable: true,
				width: 80,
				resizable: true,				
				align: 'center'	
			},
			
			{										
				header : 'Aviso D�as Previos',
				tooltip: 'Aviso D�as Previos',
				dataIndex : 'AVISO_VENC_LINEA',
				sortable: true,
				width: 60,
				resizable: true,				
				align: 'center'	
			},
			//Cambio de Administraci�n
			{										
				header : 'Fecha',
				tooltip: 'Fecha',
				dataIndex : 'CAMBIO_ADMON',
				sortable: true,
				width: 80,
				resizable: true,				
				align: 'center'	
			},
			{										
				header : 'Aviso D�as Previos',
				tooltip: 'Aviso D�as Previos',
				dataIndex : 'AVISO_CAMBIO_ADMON',
				sortable: true,
				width: 60,
				resizable: true,				
				align: 'center',
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {	
					if(registro.get('AVISO_CAMBIO_ADMON') == 0)
						return value = '';	
					
					else 
						return value ;				
				}
			},			
			{
										
				header : 'Validar Fechas L�mite',
				tooltip: 'Validar Fechas L�mite',
				dataIndex : 'CSFECHALIMITE',
				sortable: true,
				width: 50,
				resizable: true,				
				align: 'center'	
			},
			//Cambio de Estatus			
			{
										
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de  <br> Bloqueo/Desbloqueo',
				tooltip: 'Fecha de Bloqueo/Desbloqueo',
				dataIndex: 'FECHA_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: '<center> Usuario <br> de Bloqueo/Desbloqueo ',
				tooltip: 'Usuario de Bloqueo/Desbloqueo ',
				dataIndex: 'USUARIO_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},			
			{
				header: '<center> Dependencia que <br> Bloqueo/Desbloqueo',
				tooltip: 'Dependencia que Bloqueo/Desbloqueo ',
				dataIndex: 'DEPENDENCIA_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			}			
		],
		bbar: {
					xtype: 'paging',
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacionS',
					displayInfo: true,
					store: consultaData,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: 'No se encontro ning�n registro',
					
					items: ['->','-',
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivoS',
									tooltip: 'Imprime todos los registros en formato CSV.',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										totalProperty:'total',
										Ext.Ajax.request({
											url: '13consulta07ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'GenerarCSV',
												limite:limite, limites:'N'}),
											callback: procesarSuccessFailureGenerarCSVS
										});
									}
								},{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivoS',
									hidden: true
								},'-',{
									xtype: 'button',
									text:  'Generar Todo',
									tooltip: 'Imprime todos los registros en formato PDF.',
									id: 'btnImprimirPDFS',
									handler: function(boton, evento)
									{
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacionS");
										Ext.Ajax.request({
											url: '13consulta07ext.data.jsp',
											params: Ext.apply (fp.getForm().getValues(),{
												informacion: 'GenerarPDF',
												limite:limite,
												limites:'N',
												start: cmpBarraPaginacion.cursor,
												limit: cmpBarraPaginacion.pageSize
												}),
												
											callback: procesarSuccessFailureGenerarPDFS
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDFS',
									hidden: true
								},
								'-'
					]
				}	
	});
	

	var gridC = new Ext.grid.GridPanel({
		id: 'gridC',				
		store: consultaDataLimites,	
		style: 'margin:0 auto;',
		title:'',
		stripeRows: true,
		loadMask:false,
		height:420,
		width:900,	
		frame: true,
		hidden: true,
		header:true,
		columns: [
			{
										
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'EPO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex : 'IF',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'Nombre de la PyME',
				tooltip: 'Nombre de la PyME',
				dataIndex : 'PYME',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'left'
			},
			{
										
				header : 'Monto L�mite',
				tooltip: 'Monto L�mite',
				dataIndex : 'LIMITE',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{
										
				header : 'Porcentaje de Utilizaci�n',
				tooltip: 'Porcentaje de Utilizaci�n',
				dataIndex : 'PORCENTAJE',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000%')
			},
			{
										
				header : 'Monto Disponible',
				tooltip: 'Monto Disponible',
				dataIndex : 'UTILIZADO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},					
			{
										
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center'	
			},
			{
										
				header : 'Monto Comprometido ',
				tooltip: 'Monto Comprometido ',
				dataIndex : 'MONTO_COMPROMETIDO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{										
				header : 'Monto Disponible despu�s de Comprometido',
				tooltip: 'Monto Disponible despu�s de Comprometido',
				dataIndex : '',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {	
					if(registro.get('UTILIZADO') > 0)
					{
						var disp_desp_comp = registro.get('UTILIZADO') - registro.get('MONTO_COMPROMETIDO');
						return "$"+Ext.util.Format.number(disp_desp_comp,'0,000.00');
					}
					else 
						return value = '';				
				}
			},			
			{
										
				header : 'Validar Fechas L�mite',
				tooltip: 'Validar Fechas L�mite',
				dataIndex : 'CSFECHALIMITE',
				sortable: true,
				width: 50,
				resizable: true,				
				align: 'center'	
			}
		],
		bbar: {
					xtype: 'paging',
					pageSize: 15,
					buttonAlign: 'left',
					id: 'barraPaginacionC',
					displayInfo: true,
					store: consultaDataLimites,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No se encontro ning�n registro",
					
					items: ['->','-',
								{
									xtype: 'button',
									text: 'Generar Archivo',
									id: 'btnGenerarArchivoC',
									handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										totalProperty:'total',
										Ext.Ajax.request({
											url: '13consulta07ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
												informacion: 'GenerarCSV',
												limite:limite, limites:'S'}),
											callback: procesarSuccessFailureGenerarCSVC
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivoC',
									hidden: true
								},
								'-',
								{
				
									xtype: 'button',
									text: 'Imprimir PDF',	
									id: 'btnImprimirPDFC',
									handler: function(boton, evento) 
									{
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacionC");

										Ext.Ajax.request({
											url: '13consulta07ext.data.jsp',
											params: Ext.apply (fp.getForm().getValues(),{
												informacion: 'GenerarPDF',
												limite:limite,
												limites:'S',
												start: cmpBarraPaginacion.cursor,
												limit: cmpBarraPaginacion.pageSize
												}),
												
											callback: procesarSuccessFailureGenerarPDFC
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar PDF',
									id: 'btnBajarPDFC',
									hidden: true
								},
								'-'
					]
				}
	});
	
	
/*******************Componentes*******************************/
	
	var elementosForma = 
	[{
		xtype: 'panel',
		labelWidth: 120,
		layout: 'form',
		items: [ 
				
				{//FODEA 007 - 2009
					xtype				: 'combo',
					id					: 'id_bco_fondeo',
					name				: 'bco_fondeo',
					hiddenName 		: 'bco_fondeo',
					fieldLabel		: 'Banco de Fondeo',
					width				: 110,
					forceSelection	: true,
					hidden:true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					store				: catalogoBanco,
					listeners: {
						select: function(combo) {
							Ext.getCmp('id_nombre_if').reset();
							Ext.getCmp('id_cmb_epo').reset();
							Ext.getCmp('id_nombre_proveedor').reset();
							catalogoIF.load({params: {fondeo: combo.getValue() }});							
							catalogoEpo.load({params: {fondeo: combo.getValue() }});
						}
					}
				},
				{
						xtype: 'combo',
						id: 'id_nombre_if',
						name: 'nombre_if',
						hiddenName: 'nombre_if',
						fieldLabel: 'Nombre del IF',
						width:530,
						emptyText: 'Seleccionar ',  
						forceSelection: true,
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						store: catalogoIF,
						listeners: {
							select: function(combo) {
								Ext.getCmp('id_nombre_proveedor').reset();
								Ext.getCmp('id_cmb_epo').reset();

								catalogoEpo.load({
									params : Ext.apply({	ic_if: combo.getValue(), fondeo: (Ext.getCmp('id_bco_fondeo')).getValue() })
								});
							}
					}
				},
				{
					xtype				: 'combo',
					id					: 'id_cmb_epo',
					name				: 'cmb_epo',
					hiddenName 		: 'cmb_epo',
					fieldLabel		: 'Nombre de la EPO',
					width				: 530,
					forceSelection	: true,
					triggerAction	: 'all',
					mode				: 'local',
					valueField		: 'clave',
					displayField	: 'descripcion',
					emptyText		: 'Seleccione EPO',
					store				: catalogoEpo,
					listeners: {
						select: function(combo, record, index) {
							gridC.hide();
							gridS.hide();		
					
							noEpo = record.json.clave;
							Ext.Ajax.request({
								url: '13consulta07ext.data.jsp',
								params: Ext.apply ({
									informacion: 'VerificaLimites',
									noEpo: noEpo
								}),
								callback: procesarSuccessFailureVerificaLimites
							});
						}
					}
				},
				{
						xtype: 'combo',
						id: 'id_nombre_proveedor',
						name: 'nombre_proveedor',
						hiddenName: 'nombre_proveedor',
						fieldLabel: 'Nombre del Proveedor',
						width:200,
						emptyText: 'Seleccione PYME',  
						hidden: true,
						forceSelection: true,
						triggerAction: 'all',
						typeAhead: true,
						minChars: 1,
						mode: 'local',
						valueField:'rs_pyme', 
						displayField: 'rs_nombre_pyme', 
						store: catalogoProveedor
				}
			]
	}];
	

	var fp = new Ext.form.FormPanel
	({
		id: 'forma',
		width: 700,
		style: ' margin:0 auto;',
		title:	'',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 140,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					gridC.hide();
					gridS.hide();		
					pnl.el.mask('Enviando...', 'x-mask-loading');
					
					var ic_pyme_cmb = (Ext.getCmp('id_nombre_proveedor')).getValue();
					var ic_epo = (Ext.getCmp('id_cmb_epo')).getValue();
					
					if(ic_pyme_cmb=='' && limite != 'S')
					{
						consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{	
						operacion: 'Generar',
						start : 0,
						limit: 15,
						limite : limite})
						});					
					}					
					else if(ic_epo != '' && limite == 'S')
					{
						consultaDataLimites.load({
						params: Ext.apply(fp.getForm().getValues(),{	
						operacion: 'Generar',
						start : 0,
						limit: 15,
						limite : limite})
						});						
					}
					else {
						consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{	
						operacion: 'Generar',
						start : 0,
						limit: 15,
						limite : limite})
						});			
					}
					
				} //fin handler
			}, //fin boton Consultar
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					leeRespuesta();
				}
			}
		]
	});
	
	var pnl = new Ext.Container
	({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			
			fp,	NE.util.getEspaciador(20),
			gridS,	
			gridC,	
			NE.util.getEspaciador(10)	
		]
	});
	
	catalogoBanco.load();
	
});