Ext.onReady(function() {
/*--------------------------------- HANDLERS -------------------------------*/

	/****** Handler�s Grid�s *******/
	
	//var consul
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
	//var consultaData = function(store, arrRegistros, opts) {
		pnl.el.unmask();
		var fp = Ext.getCmp('forma');
		
		/*si el arrRegistros es distinto de nulo*/
		if (arrRegistros != null) 
		{
			/*si el grid no es visible, lo muestro*/
			if (!grid.isVisible()) 
				grid.show();
			
			/*declaro botones y una variable para manipular el grid*/
			var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivo");
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDF'); 
			var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivo');
			var btnBajarPDF 		 = Ext.getCmp('btnBajarPDF');
			var el = grid.getGridEl();
			btnBajarArchivo.hide();
			btnBajarPDF.hide();
						
			/* 
				Si el registro viene vacio manda un mensaje y desactiva los botones 
				para generar archivo*/
			if (store.getTotalCount() >= 0 ) 	{	
				el.unmask();		
				
				if (arrRegistros == '') {
					grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnGenerarArchivo.disable();
					btnImprimirPDF.disable();
				}
				
				else if (arrRegistros != '')	{
					btnGenerarArchivo.enable();
					btnImprimirPDF.enable();
				}
			}
		}	
	}
	

  var procesarSuccessFailureGenerarPDF = function(opts, success, response) 
  {
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnBajarPDF.setIconClass('icoPdf');
		btnImprimirPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	 var procesarSuccessFailureGenerarCSV = function(opts, success, response) 
    {
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnBajarArchivo.setIconClass('icoXls');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{

			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
 
 
		/**** End Handler�s Grid�s ****/
		


/*--------------------------------- STOTORES -------------------------------*/
	//var tipoBanco = new Ext.data.ArrayStore({
	var catalogoBanco = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','NAFIN'],
			['2','BANCOMEXT']
		 ]
	}) ;
		
	var catalogoEpoData = new Ext.data.JsonStore
	({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta018ext.data.jsp',
		baseParams: {	informacion: 'CatalogoEPO'	}, 
		totalProperty : 'total',
		autoLoad: true,
		listeners: 
		{ 	
			
			exception: NE.util.mostrarDataProxyError,	
			beforeload: NE.util.initMensajeCargaCombo	
		}
	});


	
	/****** Store�s Grid�s *******/	
	var consultaData = new Ext.data.JsonStore
	({
		root:'registros',	
		url:'13consulta018ext.data.jsp',	
		baseParams:{informacion:'ConsultaGrid'},	
		
		totalProperty:'total',	
		messageProperty:'msg',	
		autoLoad:false,
		fields: [{name:'nombre'},	//type: 'date', dateFormat: 'd/m/Y'},
					{name: 'dmin' },	//type: 'float'},
					{name: 'dmax' },	//type: 'float'},
					{name: 'fecha_aforo'  },		//type: 'date', dateFormat: 'd/m/Y'},
					{name: 'fecha_aforo_dol' },	//type: 'date', dateFormat: 'd/m/Y'},
					{name: 'fact_venc'},
					{name: 'fact_distr'}],
	
		listeners: 
		{
			//load: procesarConsultaData,
			load: procesarConsultaData,
			exception: NE.util.mostrarDataProxyError
		}
		
	});

	/****** End Store�s Grid�s *******/
	
	
	/*********** Grid�s *************/

	var grid = new Ext.grid.GridPanel
	({
		title:'',
		store: consultaData,
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask:false,//true,	
		autoHeight:true,
		width:866,
		frame:true, 
		hidden:true, 
		header:true,
		columns: [
			{
				header:'EPO',
				tooltip:'EPO',			
				dataIndex:/*'NOMBRE_EPO',*/'nombre',	
				sortable:true,	
				resizable:true,	
				width:250,
				align : 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},
			{
				header:'D�as M�n. para Desc',
				tooltip: 'D�as M�nimos para Descuento',
				dataIndex:/*'DIAS_MIN',*/ 'dmin',	
				sortable:true,
				resizable:true,	
				width:100, 
				align:'center'
				
			},
			{
				header:'D�as M�x. para Desc', 
				tooltip: 'D�as M�ximos para Descuento',	
				dataIndex:/*'DIAS_MAX',*/ 'dmax',	
				sortable:true, 
				width:100, 	
				align:'center'
			},
			{
				header:'Porcentaje de Desc M.X.',
				tooltip:'Porcentaje de Descuento M.N.',
				dataIndex:/*'DESC_MN',*/ 'fecha_aforo',		
				sortable:true, 
				width:100, 
				align:'center'				
			},
			{
				header:'Porcentaje de Desc Dol',
				tooltip:'Porcentaje de Descuento Dol�res',	
				dataIndex:/*'DESC_DOL',*/ 'fecha_aforo_dol',	
				sortable:true,	
				width:100,	
				align:'center'//renderer:Ext.util.Format.dateRenderer('d/m/Y')
				
			},
			{
				header:'Opera Fact. Vencido', 		
				tooltip:'Opera Factoraje Vencido',			
				dataIndex:/*'FAC_VEN',*/ 'fact_venc',		
				sortable:true,	
				width:100,	
				align:'center'				
			},
			{
				header:'Opera Fact. Dist',
				tooltip:'Opera Factoraje Distribuido',	
				dataIndex:/*'FAC_DIS',*/ 'fact_distr',		
				sortable:true,	
				width:100,	
				align:'center' //

			}
		],
		
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) 
					{
						boton.disable();
						boton.setIconClass('loading-indicator');
						var bco_fondeo = Ext.getCmp('id_cmbBanco');
						var epo = Ext.getCmp('id_listaEPO');
						Ext.Ajax.request
						({
							url: '13consulta018ext.data.jsp',	
							//params: Ext.apply(fp.getForm().getValues(),{
							params: Ext.apply (fp.getForm().getValues(),{					 
								
								informacion: 'GeneraCSV'
								
							}),
							callback: procesarSuccessFailureGenerarCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
							{
					xtype: 'button',
					text: 'Imprimir PDF',
					id: 'btnImprimirPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						totalProperty:'total',
						Ext.Ajax.request({
							url: '13consulta018ext.data.jsp', 
							//params: fp.getForm().getValues(),{
							params: Ext.apply (fp.getForm().getValues(),{
								informacion: 'GeneraPDF'
								
								}),
								
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
				
			]
		}
	});

		/****  End Grid�s ****/
	
	
   var elementosForma = [
	{
		xtype: 'panel',
		labelWidth: 126,
		layout: 'form',
		items: [ 
			{
				xtype: 'combo',
				name: 'cmb_Banco',
				id: 'id_cmbBanco',
				hiddenName: 'cmb_Banco',
				fieldLabel: 'Banco de Fondeo',
				emptyText: 'Seleccionar Banco',  
				forceSelection: true,
				triggerAction: 'all',
				typeAhead: true, 
				hidden:true,
				minChars: 1,
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				value:1,
				store: catalogoBanco,
			
				listeners:{
					select:
					{
						fn: function(combo)
						{
							grid.hide();
							Ext.getCmp('id_listaEPO').setValue("");
							Ext.getCmp('id_listaEPO').store.removeAll();
							Ext.getCmp('id_listaEPO').store.reload({params: {cmb_Banco: combo.getValue()  }	});
						}
					}
				}
				
						
	   }, {
				xtype: 'combo',
				id: 'id_listaEPO',
				name: 'listaEPO',
				hiddenName: 'listaEPO',
				fieldLabel: 'Nombre de la EPO',
				width: 350,
				emptyText: 'Seleccione EPO...',
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				forceSelection: true,
				triggerAction: 'all',
				typeAhead: true,
				minChars: 1,
				store: catalogoEpoData,
				
				listeners:
				{					
					select:
					{
						fn: function(combo)						{
							grid.hide();
							var ic_bco_fondeo = Ext.getCmp('id_cmbBanco');
							consultaData.load({params: {cmb_Banco: ic_bco_fondeo.getValue(), cmb_Epo: combo.getValue()  }	});
						}
					}
				}
			
				
		}
	  ]
						
	}];
						
	 
	 
/*******************Componentes*******************************/

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 550,
		frame:true,
		style: ' margin:0 auto;',
		title: '<div><center>Otros Par�metros por EPO</div>',
		collapsible: true,		///////////
		titleCollapse: true,		//////////
		bodyStyle: 'padding: 6px',
		labelWidth: 120,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items:	elementosForma,
		monitorValid: true						
	}); 
	
	
	 var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fp, 
			NE.util.getEspaciador(20),
			grid  			
		]
	});
	
	
	catalogoEpoData.load({params: {cmb_Banco: '1' }});
	
	 
	

});//Fin de funcion principal
