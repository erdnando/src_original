<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.text.*,
	java.util.Map,
	java.util.Set,
	java.io.*,
	javax.naming.*,
	com.netro.model.catalogos.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	org.apache.commons.logging.Log,
	com.netro.descuento.*,
	com.netro.afiliacion.*,
	com.netro.exception.*,
	com.netro.model.catalogos.CatalogoSimple,
	com.netro.model.catalogos.CatalogoEPONafin,
	netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>	
<%!
		private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
	String indiceEst= (request.getParameter("indiceEst") != null) ? request.getParameter("indiceEst") : "";
	SimpleDateFormat formatoHora2 = new SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a");
	String intermediario = (String)(request.getParameter("intermediario") == null?"":request.getParameter("intermediario"));
	String icSolicitud = (String)(request.getParameter("icSolicitud") == null?"":request.getParameter("icSolicitud"));
	String infoRegresar ="", consulta="";
	JSONObject jsonObj = new JSONObject();
	AutorizacionSolicitud autorizacionSolicitud = ServiceLocator.getInstance().lookup("AutorizacionSolicitudEJB", AutorizacionSolicitud.class);
	JSONArray registros= new JSONArray();
	if(informacion.equals("catIntermediarioFinanciero")){
		List catalogo = new ArrayList();
		catalogo = autorizacionSolicitud.getIFDescuento();
		for(int i=0;i<catalogo.size();i++){
			registros.add(catalogo.get(i));	
		}
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar = jsonObj.toString(); 	
	}else if(informacion.equals("Consultar")){
		List comboIf = new ArrayList();
		List tablasEmergentes =autorizacionSolicitud.consultarTablasEmergentesExt(intermediario);
		for(int i=0;i<tablasEmergentes.size();i++){
			registros.add(tablasEmergentes.get(i));	
		}
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("accion", "C");
		infoRegresar = jsonObj.toString(); 		
	}else if(informacion.equals("Depurar")){   
		System.out.println("DescargaTablasEmergentesAction::depurarTablas(S)");
		List comboIf = new ArrayList();								
		HashMap tablasEmergentes = new HashMap();
		String solicitudes = "";
		try{
			autorizacionSolicitud.eliminarTablasEmergentes(icSolicitud, intermediario);
			jsonObj.put("success", new Boolean(true));
		} catch(Throwable e){
			throw new   AppException("Error al consultar los datos", e);
		 }
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("Descargar")){
		CreaArchivo archivo = new CreaArchivo();
		File file = null;
		String contentType = "text/html;charset=ISO-8859-1";
		try{
			file = autorizacionSolicitud.getArchivoTablasEmergentes(icSolicitud, intermediario, strDirectorioTemp);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+file.getName());			
      } catch(Throwable e){
			throw new   AppException("Error al descargar archivo ", e);
		 }
		infoRegresar = jsonObj.toString();
   }
%>
<%=infoRegresar%>

