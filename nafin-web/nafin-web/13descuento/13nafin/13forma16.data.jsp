<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		com.netro.seguridadbean.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		javax.naming.Context,
		org.apache.commons.logging.Log,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String infoRegresar ="";

log.debug("informacion = <"+informacion+">");

if (informacion.equals("Aceptar") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	ParametrosDescuento paramDsctoEJB = null;
	try {

		paramDsctoEJB = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	}catch(Exception e){
		success		= false;
		log.error("AceptaCapura.Aceptar(Exception): Obtener instancia del EJB de ParametrosDescuento");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ParametrosDescuento.");

	}

	String cg_login = (request.getParameter("cg_login") == null)?"":request.getParameter("cg_login");
	String idUser="",	afiliacion="",	ic_usuario="",	cs_estatus_mail="",
				claveAfiliado="",	email="",	estadoSiguiente="", msg="",
				ic_nafin_electronico="",	cg_razon_social="",	cg_rfc="";

	UtilUsr 	usuario		= new UtilUsr(); 
	Usuario 	usuarioAux	= new Usuario(); 
				usuarioAux	= usuario.getUsuario(cg_login);
            
   log.debug("usuarioAux <" + usuarioAux + ">");
            
   if(usuarioAux!=null){
      afiliacion		= usuarioAux.getTipoAfiliado();
      idUser			= usuarioAux.getLogin();
		claveAfiliado	= usuarioAux.getClaveAfiliado();
		email				= usuarioAux.getEmail();
		try{
			
			Registros reg = paramDsctoEJB.getUsuarioCesion(idUser);
			
         while (reg.next()) {
            ic_usuario		=	(reg.getString(1))==null?"":reg.getString(1);
            cs_estatus_mail=	(reg.getString(2))==null?"":reg.getString(2);
         }

      }catch(Exception e){
         log.error("Aqui hay error!");
			success = false;
         e.printStackTrace();
         new RuntimeException(e);
      }
   }

	if(!cg_login.equals(idUser)) {
		msg = "El usuario no existe, favor de verificar";

	}	else if(!"P".equals(afiliacion)) {
		msg = "El usuario no pertenece a una PYME, favor de verificar";

	}	else if(!cg_login.equals(ic_usuario)){
		msg = "El usuario no tiene clave de confirmación de cesión de derechos, favor de verificar";

	}else {

	    com.netro.seguridadbean.Seguridad beanSegFacultad = null;

		try {
		
			beanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
		
		}catch(Exception e){
			success		= false;
			log.error("Aceptar(Exception): Obtener instancia del EJB de Seguridad");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
		
		}

		List datosPyme = beanSegFacultad.getDatosPyME(claveAfiliado);
		if(datosPyme.size()>0) {
			ic_nafin_electronico	=datosPyme.get(0).toString();
			cg_razon_social		=datosPyme.get(1).toString();
			cg_rfc					=datosPyme.get(2).toString();
			//in_numero_sirac  	 =datosPyme.get(3).toString();
		}

		estadoSiguiente = "RESPUESTA_ACEPTAR";
		jsonObj.put("idUser",					idUser					);
		jsonObj.put("ic_nafin_electronico",	ic_nafin_electronico	);
		jsonObj.put("cg_razon_social",		cg_razon_social		);
		jsonObj.put("cg_rfc",					cg_rfc					);
		jsonObj.put("email",						email						);
		jsonObj.put("cs_estatus_mail",		cs_estatus_mail		);

	}

	jsonObj.put("msg",					msg							);
	jsonObj.put("estadoSiguiente",	estadoSiguiente			);
	jsonObj.put("success",				new Boolean(success)		);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Save") ) {

	JSONObject jsonObj		= new JSONObject();
	boolean success			= true;

	com.netro.seguridadbean.Seguridad beanSegFacultad = null;
	try {

		beanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);

	}catch(Exception e){
		success		= false;
		log.error("Aceptar(Exception): Obtener instancia del EJB de Seguridad");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Seguridad.");
	
	}

	String cg_login = (request.getParameter("cg_login") == null)?"":request.getParameter("cg_login");

	beanSegFacultad.eliminaCesion(cg_login);

	jsonObj.put("estadoSiguiente",	"RESPUESTA_DESBLOQUEAR"		);
	jsonObj.put("success",				new Boolean(success)			);
	infoRegresar = jsonObj.toString();

}

log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>