<% String version = (String)session.getAttribute("version"); %>
<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>


<html>
      <head>
        <title>Nafinet</title>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

        <%@ include file="/extjs.jspf" %>
		  <%@ include file="/01principal/menu.jspf"%>
		  
        <script language="JavaScript" src="/nafin/00utils/valida.js"></script> 
        <script type="text/javascript" src="13consulta17ext.js"></script>
		  <script type="text/javascript" src="/nafin/13descuento/AcusesDeCarga.js"></script>
        <style type="text/css">
	       .x-selectable, .x-selectable * 
		    {
		     -moz-user-select: text!important;
		     -khtml-user-select: text!important;
	       }
        </style>
	  
      </head>
		
	<form id='formParametros' name="formParametros">
			<input type="hidden" id="strPerfil" name=" " value="<%=strPerfil%>"/>	
			
			<input type="hidden" id="strTipoUsuario" name=" " value="<%=strTipoUsuario%>"/>	
	</form>
<%
	if(strTipoUsuario.equals("EPO")) {//if(strPerfil.equals("ADMIN EPO")) {
%>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<%}%>
		<div id="_menuApp"></div>
		
	<div id="Contcentral">
	<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
   <%}%>
		<div id="areaContenido"><div style="height:230px"></div></div>
		</div>
		
	<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/pie.jspf"%>
   <%}%>
	
	<form id='formAux' name="formAux" target='_new'></form>
<%
	}
	else if(strTipoUsuario.equals("NAFIN")) {//else if(strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT")) {
%>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	
	<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<%}%>
		<div id="_menuApp"></div>
		
	<div id="Contcentral">
	<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
   <%}%>
		<div id="areaContenido"><div style="height:230px"></div></div>
		</div>
		
	<%if(version!=null){%>
                <%@ include file="/01principal/01nafin/pie.jspf"%>
   <%}%>

		<form id='formAux' name="formAux" target='_new'></form>
		
<%}%>

</body>
</html>
