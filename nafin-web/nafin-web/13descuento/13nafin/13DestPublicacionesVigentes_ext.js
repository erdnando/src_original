Ext.onReady(function() {


	var processSuccessFailureArchivo = function(opts, success, response) {
		var btnGenArchivo = Ext.getCmp('btnGenArchivo');
		btnGenArchivo.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			btnGenArchivo.setIconClass('icoTxt');
			btnGenArchivo.setText('Abrir Archivo');
			
			var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};

			btnGenArchivo.urlArchivo =  NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			//btnGenArchivo.urlArchivo =  archivo;
			btnGenArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			
		
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						btnGenArchivo.setIconClass('icoTxt');
						btnGenArchivo.setText('Descargar');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				btnGenArchivo.setIconClass('icoTxt');
				btnGenArchivo.setText('Descargar');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
	};

//------------------------------------------------------------------------------
	var storeAcuseData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });


//------------------------------------------------------------------------------
	var elementosFormCarga = [
		{
			xtype: 'panel',
			name: 'pnlMsgValid',
			id: 'pnlMsgValid1',
			width: 605,
			style: 'margin:0 auto;',
			frame: false,
			hidden: false,
			layout: 'hbox',
			items:[
				{
					xtype: 'displayfield',
					value: '* Consultar �ltimo archivo cargado'
				},
				{
					xtype: 	'button',
					text: 	'Descargar',
					id: 		'btnGenArchivo',
					iconCls: 'icoGenerarDocumento',
					urlArchivo: '',
					handler: function(boton){
						if(Ext.isEmpty(boton.urlArchivo)){
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							Ext.Ajax.request({
								url: '13DestPublicacionesVigentes_ext.data.jsp',
								params:{
									informacion: 'descargaUltimoArchivo'
								},
								callback: processSuccessFailureArchivo
							});
						}else{

							var fp = Ext.getCmp('formaCarga');
							fp.getForm().getEl().dom.action = boton.urlArchivo;
							fp.getForm().getEl().dom.submit();
						
						}
					}
				}
			]
		
		},
		NE.util.getEspaciador(20),
		{
			xtype:	'panel',
			layout:	'table',
			width: 600,
			anchor: '100%',
			id:'cargaArchivo1',
			layoutConfig: {
				columns: 3
			},
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					//columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){
						pnlLayout.show();
					}
				},{
					xtype: 'panel',
					id:		'pnlArchivo',
					//columnWidth: .95,
					//anchor: '100%',
					//frame: true,
					layout: 'form',
					fileUpload: true,
					labelWidth: 140,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side'
						//anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							//id: 'cargaArchivo1',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									width: 320,
									emptyText: 'Ruta del Archivo',
									fieldLabel: 'Ruta del Archivo Origen',
									name: 'archivoCesion',
									allowBlank: false,
									buttonCfg: {
									  iconCls: 'upload-icon'
									},
									buttonText: null,
									regex: /^.*\.(txt|TXT)$/,
									regexText:'Solo se admiten archivos TXT'
								},
								{
									xtype: 'hidden',
									id:	'hidExtension',
									name:	'hidExtension',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidNumTotal',
									name:	'hidNumTotal',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidTotalMonto',
									name:	'hidTotalMonto',
									value:	''
								}
								
							]
						}
							
					]
				},
				{
					xtype: 	'button',
					text: 	'Continuar',
					id: 		'btnContinuar',
					iconCls: 'icoContinuar',
					style: { 
						  marginBottom:  '10px' 
					},
					handler: function(){
						var cargaArchivo = Ext.getCmp('archivo');
						if (!cargaArchivo.isValid()){
							cargaArchivo.focus();
							return;
						}
						var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
						var extArchivo = Ext.getCmp('hidExtension');
						var numTotal = Ext.getCmp('hidNumTotal');
						var totalMonto = Ext.getCmp('hidTotalMonto');
						var objMessage = Ext.getCmp('pnlMsgValid1');
						
						
						//objMessage.hide();
						
						if (/^.*\.(txt)$/.test(ifile)){
							extArchivo.setValue('txt');
						}
						
						fpCarga.getForm().submit({
							url: '13DestPublicVigentes_ext_file.jsp',
							waitMsg: 'Cargando Archivo...',
							success: function(form, action) {
								pnlLayout.hide();
								var resp = action.result;
								var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
								var objMsg = Ext.getCmp('pnlMsgValid1');

								if(resp.valida=='ok'){
									//objMsg.show();
									//objMsg.body.update(resp.msgError);
									Ext.getCmp('formaCarga').hide();
									var acuseInfo = [
										['Archivo cargado:',resp.nombreArch],
										['Total de registros cargados:', resp.registros],
										['Fecha de carga:', resp.fechaHoy],
										['Hora de carga:', resp.horaActual],
										['Usuario:', resp.strLogin+' '+resp.usuario]
									];
									
									storeAcuseData.loadData(acuseInfo);
									
									var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
									if (!gridAcuse.isVisible()) {
										contenedorPrincipalCmp.add(gridAcuse);
										contenedorPrincipalCmp.doLayout();
									}
									
								}else if(resp.valida=='er'){
									//objMsg.show();
									//objMsg.body.update('El archivo TXT no puede contener mas de 30000 registros');
									Ext.getCmp('formaCarga').hide();
									var txtAreaError = Ext.getCmp('dataError1');
									txtAreaError.setValue(resp.errores+'\n'+resp.linea);
									if (!pnlErrores.isVisible()) {
										contenedorPrincipalCmp.add(pnlErrores);
										contenedorPrincipalCmp.doLayout();
									}
								}else{
									//ObjGral.numProc = resp.numProceso;
								}

							},
							failure: NE.util.mostrarSubmitError
						})	
					}
				}
			]
		}
		//barraProgreso
	];

//------------------------------------------------------------------------------
	var fpCarga = new Ext.form.FormPanel({
		id: 'formaCarga',
		width: 605,
		title: 'Destinatarios de Publicaciones Vigentes',
		frame: true,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px; text-align:left;',
		items: elementosFormCarga,		
		monitorValid: true
	});
	
	var gridAcuse = new Ext.grid.GridPanel({
		id: 'gridAcu',
		store: storeAcuseData,
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		hideHeaders : true,
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 190,
				align: 'right',
				sortable : false
			},
			{
				header : 'Informacion',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'informacion',
				width : 190,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 400,
		height: 170,
		title: ' ',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'center',
			items: [
				'-',
				{
					text: 'Regresar',
					//iconCls:'vacio',
					handler: function(){
						window.location.href='13DestPublicacionesVigentes_ext.jsp';
					}
				},
				'-'
			]
		}
	});
	
	var pnlErrores = new Ext.Panel({
		id: 'pnlErrores1',
		width: 512,
		title: '<p align="center">Errores durante la carga del archivo</p>',
		frame: true,
		style: 'margin:0 auto;',
		//bodyStyle: 'padding: 6px; text-align:left;',
		items: [
			{
				xtype: 'textarea',
				id: 'dataError1',
				width: 500,
				style: 'margin:0 auto;',
				readOnly: true,
				height: 200
			}
		],
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'center',
			items: [
				'-',
				{
					text: 'Regresar',
					//iconCls:'cancelar',
					handler: function(){
						window.location.href='13DestPublicacionesVigentes_ext.jsp';
					}
				},
				'-'
			]
		}
	});
	
	
	var pnlLayout = new Ext.Panel({
		id: 'pnlLayout',
		width: 605,
		title: '',
		frame: true,
		hidden: true,
		style: 'margin:0 auto;',
		//bodyStyle: 'padding: 6px; text-align:left;',
		items: [
			{
				xtype: 'displayfield',
				id: 'dataLayout1',
				width: 600,
				style: 'margin:0 auto;',
				height: 65,
				value: '<p align="center">Se deber� cargar un archivo de texto (.txt) con las direcciones de correo electr�nico de los destinatarios,<br>'+
						 'los correo electr�nicos deber�n ser separados por pipe "|".<br> '+
						 'Ejemplo: usuario@nafin.gob.mx|usuario2@nafin.gob.mx|usuario3@nafin.gob.mx</p>'
			}
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	});

//------------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fpCarga,
			NE.util.getEspaciador(20),
			pnlLayout
		]
	});

});