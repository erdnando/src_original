Ext.onReady(function() {
	
	//----------------------------- NUEVOS COMPONENTES DE EXTJS --------------------------
	
		// Custom Action Column Component
		Ext.grid.CustomizedActionColumn = Ext.extend(Ext.grid.ActionColumn, {
			constructor: function(cfg) {
				  var me = this,
						items = cfg.items || (me.items = [me]),
						l = items.length,
						i,
						item;
		
				  Ext.grid.CustomizedActionColumn.superclass.constructor.call(me, cfg);
		
				  // Renderer closure iterates through items creating an <img> element for each and tagging with an identifying 
				  // class name x-action-col-{n}
				  me.renderer = function(v, meta) {
						// Allow a configured renderer to create initial value (And set the other values in the "metadata" argument!)
						v = Ext.isFunction(cfg.renderer) ? cfg.renderer.apply(this, arguments)||'' : '';
		
						meta.css += ' x-action-col-cell';
						for (i = 0; i < l; i++) {
							 item = items[i];
							 if( Ext.isEmpty(item) ) continue;
							 
							 // Check for icon position
							 var iconPosition = !Ext.isEmpty(item.iconPosition) && "right" == item.iconPosition?"right":"left";
							
							 // Render icon to left side
							 if( "right" == iconPosition ){
								 
								 v += Ext.isEmpty(item.text)?'':'<a class="x-action-col-icon x-action-col-' + String(i) + '" style="color:#0000FF;text-decoration:underline;" >'+item.text+'</a>&nbsp;';
								 v += '<img alt="' + ( item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
										'" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
										' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
										((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />'; 
								 
							 // Render icon to right side
							 } else if (  "left" == iconPosition  ){
								 
								 v += '<img alt="' + ( item.altText || me.altText) + '" src="' + (item.icon || Ext.BLANK_IMAGE_URL) +
										'" class="x-action-col-icon x-action-col-' + String(i) + ' ' + (item.iconCls || '') +
										' ' + (Ext.isFunction(item.getClass) ? item.getClass.apply(item.scope||this.scope||this, arguments) : '') + '"' +
										((item.tooltip) ? ' ext:qtip="' + item.tooltip + '"' : '') + ' align="middle" />'; 
								 v += Ext.isEmpty(item.text)?'':'&nbsp;<a class="x-action-col-icon x-action-col-' + String(i) + '" style="color:#0000FF;text-decoration:underline;" >'+item.text+'</a>';
								 //v += Ext.isEmpty(item.text)?"":"&nbsp;"+item.text;
							 }
							 
						}
						
						return v;
				  };
			 }
		});
		Ext.apply(Ext.grid.Column.types, { customizedactioncolumn: Ext.grid.CustomizedActionColumn }  );
	
	//------------------------------- VALIDACIONES ---------------------------------

	//--------------------------------- HANDLERS -----------------------------------
	
	//---------------------------------- STORES ------------------------------------
	
	//----------------------------- MAQUINA DE ESTADO ------------------------------
	
	var procesaParametrizacionTasas = function(opts, success, response) {
		
		// Ocultar mascaras segun se requiera
      Ext.getCmp('contenedorPrincipal').ocultaMascaras();
        	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						capturaTasas(resp.estadoSiguiente,resp);
					}
				);
			} else {
				capturaTasas(resp.estadoSiguiente,resp);
			}
			
		}else{
 
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
 
		}
		
	}
	
	var capturaTasas = function(estadoSiguiente, respuesta){
		
		if(         estadoSiguiente == "INICIALIZACION" 										){
			
			// Cargar COMBO de EPOs
			var catalogoEPOData        = Ext.StoreMgr.key('catalogoEPODataStore');
			catalogoEPOData.load();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	{
					informacion:		'CapturaTasas.inicializacion'
				},
				callback: 				procesaParametrizacionTasas
			});
		
		} else if(  estadoSiguiente == "CONSULTA_TASA_BASE_POR_EPO"                 ){
			
			// Agregar mascara de b�squeda
			var panelFormaTasasBasePorEPO = Ext.getCmp("panelFormaTasasBasePorEPO");
			panelFormaTasasBasePorEPO.getEl().mask('Buscando...','x-mask-loading');

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CapturaTasas.consultaTasaBasePorEPO'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "CARGAR_CATALOGO_IFS_ASOCIADOS"						){
			
			// Volver a agregar mascara de b�squeda
			var panelFormaTasasBasePorEPO = Ext.getCmp("panelFormaTasasBasePorEPO");
			panelFormaTasasBasePorEPO.getEl().mask('Buscando...','x-mask-loading');
			
			// Habilitar bot�n Agregar Relacion IF
			Ext.getCmp("botonAgregarRelacionIF").enable();
			// Habilitar bot�n Agregar Nueva Tasa Base
			Ext.getCmp("botonAgregarNuevaTasaBase").enable();
			// Habilitar Combo Filtro IF
			Ext.getCmp("labelIFTasasPorEpo").enable();
			Ext.getCmp("comboIFTasasPorEpo").enable();
			
			// Remover contenido del Grid de resultados
			Ext.StoreMgr.key("catalogoIFDataStore").loadData(respuesta.catalogoIfsAsociados);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	{
					informacion:		'CapturaTasas.cargarCatalogoIfsAsociados'
				},
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "AGREGAR_NUEVA_TASA_BASE"                      ){
 
			var botonAgregarNuevaTasaBase = Ext.getCmp("botonAgregarNuevaTasaBase");
			var gridDetalleTasasPorEpo    = Ext.getCmp("gridDetalleTasasPorEpo");
			
			// Remover contenido del Grid de resultados
			var detalleTasasPorEpoData = Ext.StoreMgr.key("detalleTasasPorEpoDataStore");
			detalleTasasPorEpoData.removeAll();
			if( !Ext.isEmpty(detalleTasasPorEpoData.lastOptions) ){
				detalleTasasPorEpoData.lastOptions.params = { informacion : "ConsultaDetalleTasasPorEpoData" };
			}
			// Remover contenido del Filtro IFs
			Ext.StoreMgr.key("catalogoIFDataStore").removeAll();
			Ext.getCmp('comboIFTasasPorEpo').setValue("TODOS");
			// Deshabilitar bot�n Agregar Relacion IF
			Ext.getCmp("botonAgregarRelacionIF").disable();
			// Deshabilitar Combo Filtro IF
			Ext.getCmp("labelIFTasasPorEpo").disable();
			Ext.getCmp("comboIFTasasPorEpo").disable();
			// Habilitar    bot�n Agregar Nueva Tasa Base
			botonAgregarNuevaTasaBase.enable();
			// Mostrar grid de resultados
			gridDetalleTasasPorEpo.show();
			// Agregar mensaje de mascara al grid de resultados
			gridDetalleTasasPorEpo.getGridEl().mask("No se encontr&oacute; ning&uacute;n registro, es necesario agregar una nueva tasa base.", 'x-mask');
			// Agregar animacion de resaltado en amarillo a agregar nueva tasa de b�squeda.
			botonAgregarNuevaTasaBase.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
 		
		} else if(  estadoSiguiente == "CONSULTA_DETALLE_TASAS_POR_EPO_DATA"          ){
			
			// Volver a agregar mascara de b�squeda
			var panelFormaTasasBasePorEPO = Ext.getCmp("panelFormaTasasBasePorEPO");
			panelFormaTasasBasePorEPO.getEl().mask('Buscando...','x-mask-loading');
			
			// Cargando...
			var gridDetalleTasasPorEpo = Ext.getCmp("gridDetalleTasasPorEpo");
			// Si hay m�scara, suprimir la m�scara...
			var element = gridDetalleTasasPorEpo.getGridEl();
			if( element.isMasked()){
				element.unmask();
			}

			// Resetear combo IF, en caso se realice una nueva busqueda.
			if( Ext.isEmpty(respuesta.claveIF) ){
				Ext.getCmp('comboIFTasasPorEpo').setValue("TODOS");
			}
			
			// Extraer la lista de todos los ids de los IFS
			var ifAsociados 		= new Array();
			var catalogoIFData 	= Ext.StoreMgr.key("catalogoIFDataStore");
			catalogoIFData.each( function(record){
				if( "TODOS" != record.json['clave'] ){
					ifAsociados.push(record.json);
				}
			}); 
			
			// Si no se especifico el registro correspondiente al IF seleccionado
			// agregar Array vac�o.
			if( Ext.isEmpty(respuesta.claveIF)){
				respuesta.claveIF = "TODOS";
			}
			if( Ext.isEmpty(respuesta.ifSeleccionado) ){
				var ifSeleccionado	   = new Array();
				catalogoIFData.each( function(record){
					if( respuesta.claveIF === record.data['clave'] ){
						ifSeleccionado.push(record.json);
						return false;
					}
				}); 
				respuesta.ifSeleccionado = ifSeleccionado;
			}
 
			// Consultar TASAS
			var detalleTasasPorEpoData = Ext.StoreMgr.key("detalleTasasPorEpoDataStore");
			detalleTasasPorEpoData.load({
				params:  { 
					ifAsociados: 	 Ext.encode(ifAsociados),
					ifSeleccionado: Ext.encode(respuesta.ifSeleccionado),
					claveIF:		 	 respuesta.claveIF,
					claveEPO:	 	 Ext.getCmp("comboEPO").getValue()
				}
			});
			
		} else if(  estadoSiguiente == "MODIFICAR_TASA_BASE"                 ){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CapturaTasas.modificarTasaBase'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "MOSTRAR_WINDOW_MODIFICAR_TASA_BASE"           ){
			
			showWindowModificarTasaBase(respuesta);
			
		} else if(  estadoSiguiente == "GUARDAR_MODIFICACION_TASA_BASE" 					){
			
			// Agregar m�scara
			Ext.getCmp('formaModificarTasaBase').getEl().mask('Guardando...','x-mask-loading');
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CapturaTasas.guardarModificacionTasaBase'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "OCULTAR_WINDOW_MODIFICAR_TASA_BASE"				){
			
			hideWindowModificarTasaBase();
			if(respuesta.ACTUALIZAR_DETALLE_TASA_POR_EPO){
				Ext.StoreMgr.key("detalleTasasPorEpoDataStore").reload();
			}
 
		} else if(  estadoSiguiente == "ESPERAR_DECISION"                 				){
 
		} else if(  estadoSiguiente == "CARGAR_GRID_DETALLE_IF_RELACIONAR"				){ // ESTADO GRID_DETALLE_IF_RELACIONAR
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CapturaTasas.cargarGridDetalleIFRelacionar'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "SIN_DETALLE_IF_RELACIONAR"							){ // ESTADO GRID_DETALLE_IF_RELACIONAR
						
			var botonAutorizarTasas 		= Ext.getCmp("DetalleIFRelacionar.botonAutorizarTasas");
			var botonCancelar 				= Ext.getCmp("DetalleIFRelacionar.botonCancelar");
			
			var gridDetalleIFRelacionar	= Ext.getCmp("gridDetalleIFRelacionar");
			
			// Remover contenido del Grid de resultados
			Ext.StoreMgr.key("detalleIFRelacionarDataStore").removeAll();
			// Remover contenido del Filtro IFs
			Ext.StoreMgr.key("catalogoIFxRelacionarDataStore").removeAll();
			Ext.getCmp('DetalleIFRelacionar.comboIF').clearValue();
			
			// Deshabilitar bot�n Autorizar Tasas
			botonAutorizarTasas.disable();
			// Habilitar    bot�n Cancelar Tasas
			botonCancelar.enable();
			// Deshabilitar Combo Filtro IF
			Ext.getCmp('DetalleIFRelacionar.labelIF').disable();
			Ext.getCmp('DetalleIFRelacionar.comboIF').disable();
			// Quitar valores de los campos de Puntos L�mite
			Ext.getCmp('DetalleIFRelacionar.csPuntosLimite').setValue(''); 
			Ext.getCmp('DetalleIFRelacionar.fnPuntosLimite').setValue(''); 
			Ext.getCmp('DetalleIFRelacionar.claveEPO').setValue(''); 		
			// Deshabilitar controles de configuracion de propiedades de grupo
			gridDetalleIFRelacionar.deshabilitaPanelConfigurarGrupo();
 
			// Mostrar grid de resultados
			Ext.getCmp("panelFormaTasasBasePorEPO").hide();
			Ext.getCmp("gridDetalleTasasPorEpo").hide();
			gridDetalleIFRelacionar.show();
			// Agregar mensaje de mascara al grid de resultados
			gridDetalleIFRelacionar.getGridEl().mask("No se encontr&oacute; ning&uacute;n registro.", 'x-mask');
			
		} else if(  estadoSiguiente == "CARGAR_CATALOGO_IFS_RELACIONADOS"					){ // ESTADO GRID_DETALLE_IF_RELACIONAR
						
			var gridDetalleIFRelacionar 	= Ext.getCmp("gridDetalleIFRelacionar");
			var botonAutorizarTasas 		= Ext.getCmp("DetalleIFRelacionar.botonAutorizarTasas");
			var botonCancelar 				= Ext.getCmp("DetalleIFRelacionar.botonCancelar");
			
			// Habilitar bot�n Autorizar Tasas
			botonAutorizarTasas.enable();
			// Habilitar bot�n Cancelar Tasas
			botonCancelar.enable();
			// Habilitar Combo Filtro IF
			Ext.getCmp('DetalleIFRelacionar.labelIF').enable();
			var comboIF = Ext.getCmp('DetalleIFRelacionar.comboIF');
			comboIF.enable();
			comboIF.setValue("");
			// Inicializar configuracion y valor de los puntos limite
			Ext.getCmp('DetalleIFRelacionar.csPuntosLimite').setValue(''); 
			Ext.getCmp('DetalleIFRelacionar.fnPuntosLimite').setValue(''); 
			Ext.getCmp('DetalleIFRelacionar.claveEPO').setValue(''); 		
			
			// Habilitar controles de configuracion de propiedades de grupo
			gridDetalleIFRelacionar.inicializarPanelConfigurarGrupo();
			
			// Quitar contenido que pudo haber quedado en el grid
			gridDetalleIFRelacionar.getStore().removeAll();
			
			// Remover contenido del Grid de resultados
			Ext.StoreMgr.key("catalogoIFxRelacionarDataStore").loadData(respuesta.catalogoIfsRelacionados);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	{
					informacion:		'CapturaTasas.cargarCatalogoIfsRelacionados'
				},
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "MOSTRAR_GRID_DETALLE_IF_RELACIONAR"				){ // ESTADO GRID_DETALLE_IF_RELACIONAR
 
			var gridDetalleIFRelacionar = Ext.getCmp("gridDetalleIFRelacionar");
			
			Ext.getCmp("panelFormaTasasBasePorEPO").hide();
			Ext.getCmp("gridDetalleTasasPorEpo").hide();

			gridDetalleIFRelacionar.show();
			gridDetalleIFRelacionar.getGridEl().mask("Por favor, seleccione un Intermediario Financiero (IF).");
			Ext.getCmp("DetalleIFRelacionar.labelIF").el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			
		} else if(  estadoSiguiente == "OCULTAR_GRID_DETALLE_IF_RELACIONAR"				){ // ESTADO GRID_DETALLE_IF_RELACIONAR
 
			Ext.getCmp("panelFormaTasasBasePorEPO").show();
			Ext.getCmp("gridDetalleTasasPorEpo").show();
			Ext.getCmp("gridDetalleIFRelacionar").hide();
		
			if(respuesta.ACTUALIZAR_DETALLE_TASA_POR_EPO){
				Ext.StoreMgr.key("detalleTasasPorEpoDataStore").reload();
			}
			
		} else if(  estadoSiguiente == "CONSULTAR_TASA_BASE_IF_RELACIONAR"				){ // ESTADO GRID_DETALLE_IF_RELACIONAR
			
			// Aplicar m�scara
			var gridDetalleIFRelacionar = Ext.getCmp("gridDetalleIFRelacionar");
			var element 					 = gridDetalleIFRelacionar.getGridEl();
			if( element.isMasked() ){	element.unmask(); }
			gridDetalleIFRelacionar.getGridEl().mask("Cargando...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CapturaTasas.consultarTasaBaseIfRelacionar'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "MOSTRAR_CONSULTA_TASA_BASE_IF_RELACIONAR"		){ // ESTADO GRID_DETALLE_IF_RELACIONAR
			
			var gridDetalleIFRelacionar	= Ext.getCmp("gridDetalleIFRelacionar");
			var botonAutorizarTasas 		= Ext.getCmp("DetalleIFRelacionar.botonAutorizarTasas");
			var botonCancelar 				= Ext.getCmp("DetalleIFRelacionar.botonCancelar");
			var csPuntosLimite				= Ext.getCmp('DetalleIFRelacionar.csPuntosLimite');
			var fnPuntosLimite				= Ext.getCmp('DetalleIFRelacionar.fnPuntosLimite');
			var claveEPO						= Ext.getCmp('DetalleIFRelacionar.claveEPO');
 
			if( respuesta.mostrarGridMsg ){
				
				// Deshabilitar Bot�n "Autorizar Tasas"
				botonAutorizarTasas.disable();
				// Habilitar    Bot�n "Cancelar"
				botonCancelar.enable();
				// Establecer configuracion y valor de los puntos limite
				csPuntosLimite.setValue(''); 
				fnPuntosLimite.setValue(''); 
				claveEPO.setValue('');		  
				// Deshabilitar panel de configuracion de grupo.
				gridDetalleIFRelacionar.deshabilitaPanelConfigurarGrupo();
				// Quitar contenido de alguna consulta previa
				gridDetalleIFRelacionar.getStore().removeAll();
				
				// Mostrar mensaje
				var element 				 = gridDetalleIFRelacionar.getGridEl();
				if( element.isMasked() ){ element.unmask(); }
				gridDetalleIFRelacionar.getGridEl().mask(respuesta.gridMsg,"x-mask");
				
			} else if( respuesta.cargarDetalle ){
				
				var comboGrupo = Ext.getCmp('DetalleIFRelacionar.comboGrupo');
				
				// Habilitar Bot�n "Autorizar Tasas"
				botonAutorizarTasas.enable();
				// Habilitar Bot�n "Cancelar"
				botonCancelar.enable();
				// Establecer configuracion y valor de los puntos limite
				csPuntosLimite.setValue(respuesta.csPuntosLimite);
				fnPuntosLimite.setValue(respuesta.fnPuntosLimite);
				claveEPO.setValue(respuesta.claveEPO);
				// Habilitar panel de configuracion de grupo.
				gridDetalleIFRelacionar.inicializarPanelConfigurarGrupo();	
				// Cargar detalle de la consulta
				gridDetalleIFRelacionar.getStore().loadData( respuesta.detalleIFRelacionarData );
				comboGrupo.getStore().loadData(respuesta.comboGrupoData);

			}
			
		} else if(  estadoSiguiente == "GUARDAR_TASA_BASE_IF_RELACIONAR"					){ // ESTADO GRID_DETALLE_IF_RELACIONAR
			
			// Aplicar m�scara
			var gridDetalleIFRelacionar = Ext.getCmp("gridDetalleIFRelacionar");
			var element 					 = gridDetalleIFRelacionar.getEl();
			if( element.isMasked() ){	element.unmask(); }
			gridDetalleIFRelacionar.getGridEl().mask("Autorizando Tasas...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CapturaTasas.guardarTasaBaseIfRelacionar'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
 
		} else if(  estadoSiguiente == "CARGAR_GRID_DETALLE_NUEVA_TASA_BASE"				){ // ESTADO GRID_DETALLE_NUEVA_TASA_BASE
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CapturaTasas.cargarGridDetalleNuevaTasaBase'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
						
		} else if(  estadoSiguiente == "CARGAR_CATALOGO_IFS_NUEVA_TASA_BASE"					){ // ESTADO GRID_DETALLE_NUEVA_TASA_BASE
						
			var gridDetalleNuevaTasaBase 	= Ext.getCmp("gridDetalleNuevaTasaBase");
			var botonAutorizarTasas 		= Ext.getCmp("DetalleNuevaTasaBase.botonAutorizarTasas");
			var botonCancelar 				= Ext.getCmp("DetalleNuevaTasaBase.botonCancelar");
			
			// Habilitar bot�n Autorizar Tasas
			botonAutorizarTasas.enable();
			// Habilitar bot�n Cancelar Tasas
			botonCancelar.enable();
			// Habilitar Combo Filtro IF
			Ext.getCmp('DetalleNuevaTasaBase.labelIF').enable();
			var comboIF = Ext.getCmp('DetalleNuevaTasaBase.comboIF');
			comboIF.enable();
			comboIF.setValue("TODOS");
			// Inicializar configuracion y valor de los puntos limite
			Ext.getCmp('DetalleNuevaTasaBase.csPuntosLimite').setValue(''); 
			Ext.getCmp('DetalleNuevaTasaBase.fnPuntosLimite').setValue(''); 
			Ext.getCmp('DetalleNuevaTasaBase.claveEPO').setValue(''); 		
			Ext.getCmp('DetalleNuevaTasaBase.numRegistrosTipificados').setValue(''); 		
 
			// Habilitar controles de configuracion de propiedades de grupo
			gridDetalleNuevaTasaBase.inicializarPanelConfigurarGrupo();
			
			// Quitar contenido que pudo haber quedado en el grid
			gridDetalleNuevaTasaBase.getStore().removeAll();
			
			// Remover contenido del Grid de resultados
			Ext.StoreMgr.key("catalogoIFNuevaTasaBaseDataStore").loadData(respuesta.catalogoIfsAsociados);
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	{
					informacion:		'CapturaTasas.cargarCatalogoIfsNuevaTasaBase'
				},
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "MOSTRAR_GRID_DETALLE_NUEVA_TASA_BASE"				){ // ESTADO GRID_DETALLE_NUEVA_TASA_BASE
 
			var gridDetalleNuevaTasaBase = Ext.getCmp("gridDetalleNuevaTasaBase");
			
			Ext.getCmp("panelFormaTasasBasePorEPO").hide();
			Ext.getCmp("gridDetalleTasasPorEpo").hide();

			gridDetalleNuevaTasaBase.show();
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	{
					informacion:		'CapturaTasas.mostrarGridDetalleNuevaTasaBase'
				},
				callback: 				procesaParametrizacionTasas
			});
			
			/*
			gridDetalleNuevaTasaBase.getGridEl().mask("Por favor, seleccione un Intermediario Financiero (IF).");
			Ext.getCmp("DetalleNuevaTasaBase.labelIF").el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			*/
			
		} else if(  estadoSiguiente == "OCULTAR_GRID_DETALLE_NUEVA_TASA_BASE"				){ // ESTADO GRID_DETALLE_NUEVA_TASA_BASE
 
			Ext.getCmp("panelFormaTasasBasePorEPO").show();
			Ext.getCmp("gridDetalleTasasPorEpo").show();
			Ext.getCmp("gridDetalleNuevaTasaBase").hide();

			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:	'CapturaTasas.ocultarGridDetalleNuevaTasaBase'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "CONSULTAR_TASA_BASE_NUEVA"								){ // ESTADO GRID_DETALLE_NUEVA_TASA_BASE
 
			if( respuesta.consultarTodos ){
				
				var comboIF = Ext.getCmp('DetalleNuevaTasaBase.comboIF');
				comboIF.setValue("TODOS");
				respuesta['claveIF']  = comboIF.getValue();
				respuesta['claveEPO'] = Ext.getCmp("comboEPO").getValue(); 
				// Obtener clave y descripcion del IF seleccionado
				var ifSeleccionados	 = new Array();
				if( respuesta['claveIF'] === "TODOS" ){
					comboIF.getStore().each( function(record){
						if( "TODOS" !== record.json['clave']  ){
							ifSeleccionados.push(record.json);
						}
					}); 
				} else {
					comboIF.getStore().each( function(record){
						if( respuesta['claveIF'] === record.data['clave']  ){
							ifSeleccionados.push(record.json);
							return false;
						}
					});
				}
				respuesta['ifSeleccionados'] = Ext.encode(ifSeleccionados);
				
			}
			
			// Aplicar m�scara
			var gridDetalleNuevaTasaBase = Ext.getCmp("gridDetalleNuevaTasaBase");
			var element 					 = gridDetalleNuevaTasaBase.getGridEl();
			if( element.isMasked() ){	element.unmask(); }
			gridDetalleNuevaTasaBase.getGridEl().mask("Cargando...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:	'CapturaTasas.consultarTasaBaseNueva'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "MOSTRAR_CONSULTA_TASA_BASE_NUEVA"						){ // ESTADO GRID_DETALLE_NUEVA_TASA_BASE
			
			var gridDetalleNuevaTasaBase	= Ext.getCmp("gridDetalleNuevaTasaBase");
			var botonAutorizarTasas 		= Ext.getCmp("DetalleNuevaTasaBase.botonAutorizarTasas");
			var botonCancelar 				= Ext.getCmp("DetalleNuevaTasaBase.botonCancelar");
			var csPuntosLimite				= Ext.getCmp('DetalleNuevaTasaBase.csPuntosLimite');
			var fnPuntosLimite				= Ext.getCmp('DetalleNuevaTasaBase.fnPuntosLimite');
			var claveEPO						= Ext.getCmp('DetalleNuevaTasaBase.claveEPO');
			var numRegistrosTipificados	= Ext.getCmp('DetalleNuevaTasaBase.numRegistrosTipificados');
				
			if( respuesta.mostrarGridMsg ){
				
				// Deshabilitar Bot�n "Autorizar Tasas"
				botonAutorizarTasas.disable();
				// Habilitar    Bot�n "Cancelar"
				botonCancelar.enable();
				// Establecer configuracion y valor de los puntos limite
				csPuntosLimite.setValue(''); 				
				fnPuntosLimite.setValue(''); 				
				claveEPO.setValue('');		  				
				numRegistrosTipificados.setValue('');  
				// Deshabilitar panel de configuracion de grupo.
				gridDetalleNuevaTasaBase.deshabilitaPanelConfigurarGrupo();
				// Quitar contenido de alguna consulta previa
				gridDetalleNuevaTasaBase.getStore().removeAll();
				
				// Mostrar mensaje
				var element 				 = gridDetalleNuevaTasaBase.getGridEl();
				if( element.isMasked() ){ element.unmask(); }
				gridDetalleNuevaTasaBase.getGridEl().mask(respuesta.gridMsg,"x-mask");
				
			} else if( respuesta.cargarDetalle ){
				
				var comboGrupo = Ext.getCmp('DetalleNuevaTasaBase.comboGrupo');
				
				// Habilitar Bot�n "Autorizar Tasas"
				botonAutorizarTasas.enable();
				// Habilitar Bot�n "Cancelar"
				botonCancelar.enable();
				// Establecer configuracion y valor de los puntos limite
				csPuntosLimite.setValue(respuesta.csPuntosLimite);
				fnPuntosLimite.setValue(respuesta.fnPuntosLimite);
				claveEPO.setValue(respuesta.claveEPO);
				numRegistrosTipificados.setValue(respuesta.numRegistrosTipificados); 
				// Habilitar panel de configuracion de grupo.
				gridDetalleNuevaTasaBase.inicializarPanelConfigurarGrupo();	
				// Cargar detalle de la consulta
				gridDetalleNuevaTasaBase.getStore().loadData( respuesta.detalleNuevaTasaBaseData );
				comboGrupo.getStore().loadData(respuesta.comboGrupoData);

			}
			
		} else if(  estadoSiguiente == "GUARDAR_NUEVAS_TASAS_BASE"								){ // ESTADO GRID_DETALLE_NUEVA_TASA_BASE
			
			// Aplicar m�scara
			var gridDetalleNuevaTasaBase = Ext.getCmp("gridDetalleNuevaTasaBase");
			var element 					 = gridDetalleNuevaTasaBase.getEl();
			if( element.isMasked() ){	element.unmask(); }
			gridDetalleNuevaTasaBase.getGridEl().mask("Autorizando Tasas...","x-mask-loading");
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma02ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CapturaTasas.guardarNuevasTasasBase'
					},
					respuesta
				),
				callback: 				procesaParametrizacionTasas
			});
			
		} else if(  estadoSiguiente == "FIN"                 									){
			
			var destino = !Ext.isEmpty(respuesta.destino)?respuesta.destino:"13forma02ext.data.jsp";
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= destino;
			forma.target	= "_self";
			forma.submit();

		}
		
	}
 
	//----------------------- GRIDPANEL AGREGAR NUEVA TASA BASE --------------------------
	
		var calcularTasa02			= function(tasa,relacionMatematica,puntos){
 
		var valorTasa			 	= parseFloat( tasa   );
		var puntosAdicionales  	= parseFloat( puntos );
		
		var tasaAAplicar;
		switch (relacionMatematica) {
			case "+":
				tasaAAplicar 		= valorTasa + puntosAdicionales;
			break;
			case "-":
				tasaAAplicar 		= valorTasa - puntosAdicionales;
			break;
			case "/":
				if(puntosAdicionales!=0)
					tasaAAplicar 	= valorTasa / puntosAdicionales;
			break;
			case "*":
				tasaAAplicar 		= valorTasa * puntosAdicionales;
			break;
		}
		tasaAAplicar = roundOff(tasaAAplicar,5);
		return tasaAAplicar;
		
	}
	
	var calcularTasasPorGrupo02 = function(parametros){
 
		// Revisar que se hayan enviado todos los par�metros
		if(Ext.isEmpty(parametros)){
			return;
		} else if(Ext.isEmpty(parametros.claveGrupo)){
			return;
		} else if(Ext.isEmpty(parametros.relacionMatematica)){
			return;
		} else if(Ext.isEmpty(parametros.puntosAdicionales)){
			return;
		}
 
		// Asignar Puntos Adicionales y Tasa a Aplicar a cada uno de los registros del grupo.
		var detalleNuevaTasaBaseData = Ext.StoreMgr.key("detalleNuevaTasaBaseDataStore");
		detalleNuevaTasaBaseData.each(
			function(record){
 
				if( record.get('GROUP_ID') === parametros.claveGrupo ){
					var tasaAAplicar = calcularTasa02(
						record.get('VALOR_TASA'),
						parametros.relacionMatematica,
						parametros.puntosAdicionales
					);
					record.set('PUNTOS_ADICIONALES',	 parametros.puntosAdicionales	 );
					record.set('RELACION_MATEMATICA', parametros.relacionMatematica );
					record.set('TASA_A_APLICAR',		 tasaAAplicar						 );
				}
				
			}
		);
			
	}
 								
	var valorTasaRenderer02 = function( value, metadata, record, rowIndex, colIndex, store ){
		
		return Ext.isEmpty(value)?'':Ext.util.Format.number(value,'0.00000') + "%";
		
	}
	
	var puntosAdicionalesRenderer02 = function( value, metadata, record, rowIndex, colIndex, store ){
 
		metadata.attr  = 'style="border: 1px solid gray;"';
		return Ext.util.Format.number(value,'0.00000');

	}
	
	var tasaAAplicarRenderer02 = function( value, metadata, record, rowIndex, colIndex, store ){
 
		// metadata.attr  = 'style="border: 1px solid gray;"';
		return Ext.isEmpty(value)?'':Ext.util.Format.number(value,'0.00000') + "%";
		
	}
	
	/*var fechaDetalleRenderer     = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;" ';
		return Ext.util.Format.date(value, 'd/m/Y H:i:s');
		
	}*/

	var catalogoEPOMoneda02Data = new Ext.data.JsonStore({
		id: 			'catalogoEPOMoneda02DataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13forma02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPOMoneda02'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
					
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("DetalleNuevaTasaBase.comboGrupo").setValue(defaultValue);
						Ext.getCmp("DetalleNuevaTasaBase.comboGrupo").originalValue = defaultValue;
					}
					//Ext.getCmp("DetalleNuevaTasaBase.comboGrupo").setReadOnly(true);
								
				} 
									
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoRelacionMatematica02Data = new Ext.data.ArrayStore({
		id: 		'catalogoRelacionMatematica02DataStore',
		fields:	[ 'clave', 'descripcion', 'loadMsg' ],
		data:  	[
			[ '+', '+' ],
			[ '-', '-' ],
			[ '*', '*' ],
			[ '/', '/' ]
		],
		autoDestroy: 	true
	});
		
	var catalogoIFNuevaTasaBaseData = new Ext.data.JsonStore({
		id: 			'catalogoIFNuevaTasaBaseDataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13forma02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIFNuevaTasaBase'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
					
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("DetalleNuevaTasaBase.comboIF").setValue(defaultValue);
						Ext.getCmp("DetalleNuevaTasaBase.comboIF").originalValue = defaultValue;
					}
					//Ext.getCmp("DetalleNuevaTasaBase.comboIF").setReadOnly(true);
								
				} 
									
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
 	
	// Definir handler para procesar las consultas
	var procesarDetalleNuevaTasaBaseData = function(store, registros, opts){
				
		var panelFormaTasasBasePorEPO = Ext.getCmp('panelFormaTasasBasePorEPO');
		panelFormaTasasBasePorEPO.el.unmask();
		
		var gridDetalleNuevaTasaBase = Ext.getCmp('gridDetalleNuevaTasaBase');
		
		if (registros != null) {
			
			/*
			if (!gridDetalleNuevaTasaBase.isVisible()) {
				gridDetalleNuevaTasaBase.show();
			}
			*/
			
			var el 					 = gridDetalleNuevaTasaBase.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
 
		}
					
	};

	// Crear JsonStore que se encargar� de cargar el detalle
	var detalleNuevaTasaBaseData = new Ext.data.GroupingStore({
		id:					'detalleNuevaTasaBaseDataStore',
		name:					'detalleNuevaTasaBaseDataStore',
		root:					'registros',
		url: 					'13forma02ext.data.jsp',
		autoDestroy:		true,
		baseParams: {
			informacion: 	'ConsultaDetalleNuevaTasaBaseData'
		},			
		reader: new Ext.data.JsonReader({
			root: 			'registros',	
			totalProperty: 'total',
			fields: [
				{ name:"GROUP_ID",							type:"int" 	  }, // IC_IF + ":" + IC_MONEDA
				{ name:"INTERMEDIARIO_FINANCIERO",		type:"string" },
				{ name:"MONEDA",								type:"string" },
				{ name:"TIPO_TASA",							type:"string" },
				{ name:"PLAZO",								type:"string" },
				{ name:"VALOR_TASA",							type:"string" },
				{ name:"RELACION_MATEMATICA",				type:"string" },
				{ name:"PUNTOS_ADICIONALES",				type:"string" },
				{ name:"TASA_A_APLICAR",					type:"string" },
				{ name:"IC_MONEDA",							type:"string" },
				{ name:"IC_IF",								type:"string" },
				{ name:"IC_EPO",								type:"string" },
				{ name:"CLAVE_TASA",							type:"string" },
				{ name:"CLAVE_PLAZO",						type:"string" }
				/*
				{ name:"FECHA_TASA_BASE",					type:"string" },
				{ name:"VOBO_EPO",							type:"string" },
				{ name:"VOBO_IF",								type:"string" },
				{ name:"VOBO_NAFIN",							type:"string" },
				{ name:"RANGO_PLAZO",						type:"string" }
				*/				
			]
		}),
		groupField: 			'GROUP_ID', 
		sortInfo: 				{ field: 'INTERMEDIARIO_FINANCIERO', direction: "ASC" },
		autoLoad: 				false,
		totalProperty: 		'total',
		messageProperty: 		'msg',
		pruneModifiedRecords: true, // Para que no se conserven los registros modificados en consulta anterior.
		listeners: { 
			beforeload: {
				fn: function( store, opts ) {
					var grid = Ext.getCmp('gridDetalleNuevaTasaBase');
					if (grid) {
						grid.show();
					} 			
				}
			},
			load: 	procesarDetalleNuevaTasaBaseData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarDetalleNuevaTasaBaseData(null, null, null);						
				}
			},
			update: function( detalleNuevaTasaBaseDataStore, record, operation ){
				if( operation === Ext.data.Record.EDIT ){
					var changes 				= record.getChanges();
					if( !Ext.isEmpty(changes['PUNTOS_ADICIONALES']) ){
						var tasaAAplicar 		= calcularTasa02( record.get('VALOR_TASA'), record.get('RELACION_MATEMATICA'), changes['PUNTOS_ADICIONALES'] );
						record.set( 'TASA_A_APLICAR', tasaAAplicar );
					}
				}
			}
		}
	});
    
	var gridDetalleNuevaTasaBase = {
		frame:			true,
		store: 			detalleNuevaTasaBaseData,
		title:			'Agregar Nueva Tasa Base',
		xtype: 			'editorgrid',
		id:				'gridDetalleNuevaTasaBase',
		hidden:			true,
		stripeRows: 	true,
		loadMask: 		true,
		width: 			650, 
		height:			480,
		region: 			'center',
		style: 			'margin: 0 auto',
		autoExpandColumn: 'TIPO_TASA',
		view:		 		new Ext.grid.GroupingView(
			{
				forceFit:				false, 
				groupTextTpl: 			'{group}', 
				hideGroupedColumn: 	true,
				enableGroupingMenu:	false
			}),
		columns: [
			{
				header: 		'GROUP ID',
				tooltip: 	'GROUP ID',
				dataIndex: 	'GROUP_ID', // IC_IF + ":" + IC_MONEDA 
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				//width: 		315,
				hidden: 		true,
				hideable:	false,
				groupRenderer: function(value,unused,record,rowIndex,colIndex,dsStore){
					return  record.json['INTERMEDIARIO_FINANCIERO'] + "&nbsp;(&nbsp;<span style='font-weight:bold;color:black;'>" + record.json['MONEDA'] +"</span>&nbsp;)";
				}
			},
			{
				header: 		'Intermediario Financiero',
				tooltip: 	'Intermediario Financiero',
				dataIndex: 	'INTERMEDIARIO_FINANCIERO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		315,
				hidden: 		true,
				hideable:	false
			},
			{
				header: 		'Moneda',
				tooltip: 	'Moneda',
				dataIndex: 	'MONEDA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		50,
				hidden: 		true,
				hideable:	false
			},
			{
				id:			'TIPO_TASA',
				header: 		'Tipo Tasa<br>&nbsp;',
				tooltip: 	'Tipo Tasa',
				dataIndex: 	'TIPO_TASA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Plazo<br>&nbsp;',
				tooltip: 	'Plazo',
				dataIndex: 	'PLAZO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Valor<br>&nbsp;',
				tooltip: 	'Valor',
				dataIndex: 	'VALOR_TASA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer: 	valorTasaRenderer02
			},
			{
				header: 		'Relaci�n<br>Matem�tica',
				tooltip: 	'Relaci�n Matem�tica',
				dataIndex: 	'RELACION_MATEMATICA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Puntos<br>Adicionales',
				tooltip: 	'Puntos Adicionales',
				dataIndex: 	'PUNTOS_ADICIONALES',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	puntosAdicionalesRenderer02,
				editor: new Ext.form.NumberField({
            	// allowBlank: false,
            	// allowNegative: false,
            	style: (
            		Ext.isIE7?'top: 0px; padding-bottom: 2px;':
            		( Ext.isIE8?'top: 0px; padding-bottom: 2px;':
            		'top: 1px; padding-bottom: 2px;')
            	),
            	format: '0.00000',
            	decimalPrecision: 5,
            	minValue: -99999.99999, 
            	maxValue: 99999.99999
            })
			},
			{
				header: 		'Tasa a<br>Aplicar',
				tooltip: 	'Tasa a Aplicar',
				dataIndex: 	'TASA_A_APLICAR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer: 	tasaAAplicarRenderer02
			}
		],
		stateful:	false,
		tbar: {
			xtype: 'container',
			items: [
			{
				xtype: 'toolbar',
				items: [
					// BOTON: AUTORIZAR TASAS
					{
						xtype:	'button',
						id:		'DetalleNuevaTasaBase.botonAutorizarTasas',
						name:		'botonAutorizarTasas',
						text:  	'Autorizar Tasas',
						iconCls:	'icoAutorizar',
						handler: function(button,event){
 
							var detalleNuevaTasaBaseData 	= Ext.StoreMgr.key("detalleNuevaTasaBaseDataStore");
							
							// 1. Validar que se hayan capturado todos los puntos de las Tasas.
							var faltanPuntos			 		= false;
							detalleNuevaTasaBaseData.each(
								function(record){
									if( Ext.isEmpty(record.get('PUNTOS_ADICIONALES')) ){
										faltanPuntos = true;
										return false;
									}
								}
							);
							if(faltanPuntos){
								Ext.Msg.alert("Aviso","Debe capturar todos los puntos de las tasas");
								return;
							}
							
							// 2. Validar que cumplan con los puntos l�mite.
							// Nota: En vez de validar solo las cabceras de grupo de los puntos adicionales, se valida
							// cada uno de los campos de forma individual
							var csPuntosLimite 		= Ext.getCmp("DetalleNuevaTasaBase.csPuntosLimite").getValue();
							var excedePuntosLimite 	= false;
							if (csPuntosLimite === "S") {                            
								var puntosAdicionalesLimite     = Ext.getCmp('DetalleNuevaTasaBase.fnPuntosLimite').getValue();	
								puntosAdicionalesLimite         = parseFloat(puntosAdicionalesLimite);
                                //puntosAdicionalesLimite       = roundOff(puntosAdicionalesLimite, 5);                                
                                //alert("puntosAdicionalesLimite:"+(puntosAdicionalesLimite+1.1));
                                
								detalleNuevaTasaBaseData.each(
									function(record) {                                    
                                        var puntosAdicionalesLimiteAux = record.get('PUNTOS_ADICIONALES');
                                        puntosAdicionalesLimiteAux = parseFloat(puntosAdicionalesLimiteAux);
                                        //alert("puntosAdicionalesLimite:"+puntosAdicionalesLimite);
                                        //alert("puntosAdicionalesLimiteAux:"+puntosAdicionalesLimiteAux);
										if ( puntosAdicionalesLimite < puntosAdicionalesLimiteAux ) {
                                            excedePuntosLimite = true;
											return false;
										}
									}
								);
								
							}
							if(excedePuntosLimite){
								Ext.Msg.alert("Aviso","No se puede capturar puntos adicionales mayor a los puntos adicionales l�mite.");
								return;
							}
		
							// 3. Solicitar confirmacion final
							Ext.MessageBox.confirm(
								'Confirm', 
								'Estas tasas ser�n aplicadas al operar los documentos \n�Est� seguro de enviar los valores que ha capturado?',
								function(boton) {
														
									if (boton == 'yes') {
										var respuesta = new Object();
										// Agregar detalle de todos los registros con sus tasas
										var detalleNuevaTasaBase = new Array();
										detalleNuevaTasaBaseData.each(
											function(record){
												detalleNuevaTasaBase.push(record.data);
											}
										);
										respuesta['detalleNuevaTasaBaseData'] = Ext.encode(detalleNuevaTasaBase);
										respuesta['claveIF'] 					  = Ext.getCmp('DetalleNuevaTasaBase.comboIF').getValue();
										respuesta['claveEPO'] 					  = Ext.getCmp('DetalleNuevaTasaBase.claveEPO').getValue();
										respuesta['numRegistrosTipificados']  = Ext.getCmp('DetalleNuevaTasaBase.numRegistrosTipificados').getValue();
										
										
										capturaTasas("GUARDAR_NUEVAS_TASAS_BASE",respuesta);
									} else if (boton == 'no') {
										return;
									}
														
								}
							);
								
						}
					},
					// BOTON:  CANCELAR
					{
						xtype:	'button',
						id:		'DetalleNuevaTasaBase.botonCancelar',
						name:		'botonCancelar',
						text:		'Cancelar',
						iconCls:	'cancelar',
						handler:	function(button,event){
							var detalleNuevaTasaBaseData = Ext.StoreMgr.key('detalleNuevaTasaBaseDataStore');
							if( detalleNuevaTasaBaseData.getModifiedRecords().length > 0 ){
								Ext.MessageBox.confirm(
									'Confirm', 
									'�Est� seguro que desea cancelar la operaci�n?',
									function(boton) {
														
										if (boton == 'yes') {
											var respuesta = new Object();
											capturaTasas("OCULTAR_GRID_DETALLE_NUEVA_TASA_BASE",respuesta);
										} else if (boton == 'no') {
											return;
										}
														
									}
								);
							} else {
								var respuesta = new Object();
								capturaTasas("OCULTAR_GRID_DETALLE_NUEVA_TASA_BASE",respuesta);
							}
						}
					},			
					"->",
					"-",
					// COMBO IF
					{
						xtype: 'label',
						id:	 'DetalleNuevaTasaBase.labelIF',
						html:  'IF:&nbsp;',
						style: 'padding-right:5pt;padding-left:5pt;',
						width: 30
					},
					{
						xtype: 				'combo',
						width:				300,
						id: 					'DetalleNuevaTasaBase.comboIF',
						name: 				'comboIF',
						fieldLabel: 		'IF',
						allowBlank: 		false, 
						mode: 				'local', 
						displayField: 		'descripcion',
						valueField: 		'clave',
						hiddenName: 		'claveIF',
						emptyText: 			'Seleccione un IF...', 
						forceSelection: 	true,
						triggerAction: 	'all',
						typeAhead: 			true,
						minChars: 			1,
						store: 				catalogoIFNuevaTasaBaseData,
						tpl: 					NE.util.templateMensajeCargaCombo,
						listeners:	{
							collapse: function( comboIF ){

								var respuesta 			 = new Object();
								respuesta['claveIF']  = comboIF.getValue();
								respuesta['claveEPO'] = Ext.getCmp("comboEPO").getValue(); 
								// Obtener clave y descripcion del IF seleccionado
								var ifSeleccionados	 = new Array();
								if( respuesta['claveIF'] === "TODOS" ){
									comboIF.getStore().each( function(record){
										if( "TODOS" !== record.json['clave']  ){
											ifSeleccionados.push(record.json);
										}
									}); 
								} else {
									comboIF.getStore().each( function(record){
										if( respuesta['claveIF'] === record.data['clave']  ){
											ifSeleccionados.push(record.json);
											return false;
										}
									});
								}
								respuesta['ifSeleccionados'] = Ext.encode(ifSeleccionados);
								capturaTasas("CONSULTAR_TASA_BASE_NUEVA",respuesta);
							
							}
						}
						//, anchor:				'-20'
					},
					{
						xtype: 		'hidden',
						id:	 		'DetalleNuevaTasaBase.csPuntosLimite',
						name:  		'csPuntosLimite'
					},
					{
						xtype: 		'hidden',
						id:	 		'DetalleNuevaTasaBase.fnPuntosLimite',
						name:  		'fnPuntosLimite'
					},
					{
						xtype: 		'hidden',
						id:	 		'DetalleNuevaTasaBase.claveEPO',
						name:  		'claveEPO'
					},
					{
						xtype:		'hidden',
						id:			'DetalleNuevaTasaBase.numRegistrosTipificados',
						name:			'numRegistrosTipificados'
					}
				]
			},
			{
				xtype: 'toolbar',
				style:	'padding: 5px;',
				items:[
					{
						xtype: 'label',
						id:    'DetalleNuevaTasaBase.labelConfigurar',
						html:  'Configurar Propiedades del Grupo',
						style: 'font-weight:bold;padding-bottom:10pt;text-align:center'
					}
				]
			},
			{
				xtype: 'toolbar',
				style:	'padding: 5px;',
				items:[
							{
								xtype: 'label',
								id:    'DetalleNuevaTasaBase.labelGrupo',
								html:  'Grupo:&nbsp;',
								style: 'padding-right:5pt;padding-left:5pt;'
							},
							{
								xtype: 				'combo',
								width:				200,
								id: 					'DetalleNuevaTasaBase.comboGrupo',
								fieldLabel: 		'Grupo',
								allowBlank: 		true,
								mode: 				'local', 
								displayField: 		'descripcion',
								valueField: 		'clave',
								hiddenName: 		'claveGrupo',
								emptyText:			'Seleccione...',
								forceSelection: 	true,
								triggerAction: 	'all',
								typeAhead: 			true,
								minChars: 			1,
								store: 				catalogoEPOMoneda02Data,
								//tpl: 					NE.util.templateMensajeCargaCombo
								tpl:					'<tpl for=".">' +
														'<tpl if="!Ext.isEmpty(loadMsg)">'+
														'<div class="loading-indicator">{loadMsg}</div>'+
														'</tpl>'+
														'<tpl if="Ext.isEmpty(loadMsg)">'+
														'<div class="x-combo-list-item">' +
														'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
														'</div></tpl></tpl>',
								listeners: {
									collapse: function( comboGrupo, newValue, oldValue ){
										Ext.getCmp('DetalleNuevaTasaBase.comboRelacionMatematica').reset();
										Ext.getCmp('DetalleNuevaTasaBase.puntosAdicionales').reset();
									}
								}
							},
							{
								xtype: 'label',
								id: 	 'DetalleNuevaTasaBase.labelRelacionMatematica',
								html:  'Relaci�n Matem�tica:&nbsp;',
								style: 'padding-right:5pt;padding-left:5pt;'
							},
							{
								xtype: 				'combo',
								width:				50,
								id: 					'DetalleNuevaTasaBase.comboRelacionMatematica',
								value:				'+',
								fieldLabel: 		'Relaci�n Matem�tica',
								allowBlank: 		false,
								mode: 				'local', 
								displayField: 		'descripcion',
								valueField: 		'clave',
								hiddenName: 		'relacionMatematica',
								emptyText:			'Seleccione...',
								forceSelection: 	true,
								triggerAction: 	'all',
								typeAhead: 			true,
								minChars: 			1,
								store: 				catalogoRelacionMatematica02Data,
								tpl: 					NE.util.templateMensajeCargaCombo,
								listeners: {
									change: function( comboRelacionMatematica, newValue, oldValue ){
										// Nota: No se hace nada por el momento.
									}
								}
							},
							{
								xtype: 'label',
								id: 	 'DetalleNuevaTasaBase.labelPuntosAdicionales',
								html:  'Puntos Adicionales:&nbsp;',
								style: 'padding-right:5pt;padding-left:5pt;'
							},
							{
								xtype: 		'numberfield',
								width:		90,
								id:			'DetalleNuevaTasaBase.puntosAdicionales',
								value:		'',
								name:			'puntosAdicionales',
								fieldLabel: 'Puntos Adicionales',
								decimalPrecision: 5,
								maxValue: 	999.99999,  
								minValue:	0.00000,  // Debido a que el signo se especifica en el Combo: Relaci�n Matem�tica.
								listeners: {
									blur: function( puntosAdicionales ){
										var parametros 						= new Object();
										parametros['claveGrupo'] 			= Ext.getCmp('DetalleNuevaTasaBase.comboGrupo').getValue();
										parametros['relacionMatematica'] = Ext.getCmp('DetalleNuevaTasaBase.comboRelacionMatematica').getValue();
										parametros['puntosAdicionales']  = puntosAdicionales.isValid()?puntosAdicionales.getValue():'';
										calcularTasasPorGrupo02(parametros);
									}
								}
							}
						]
				}
		]		
		},
		inicializarPanelConfigurarGrupo: function(){
			Ext.getCmp('DetalleNuevaTasaBase.labelConfigurar').enable();
			Ext.getCmp('DetalleNuevaTasaBase.labelGrupo').enable();
			var comboGrupo = Ext.getCmp('DetalleNuevaTasaBase.comboGrupo');
			comboGrupo.enable();
			comboGrupo.clearValue();
			comboGrupo.getStore().removeAll();
			Ext.getCmp('DetalleNuevaTasaBase.labelRelacionMatematica').enable();
			var comboRelacionMatematica = Ext.getCmp('DetalleNuevaTasaBase.comboRelacionMatematica');
			comboRelacionMatematica.enable();
			comboRelacionMatematica.setValue("+");
			Ext.getCmp('DetalleNuevaTasaBase.labelPuntosAdicionales').enable();
			var puntosAdicionales = Ext.getCmp('DetalleNuevaTasaBase.puntosAdicionales');
			puntosAdicionales.enable();
			puntosAdicionales.setValue("");			
		},
		deshabilitaPanelConfigurarGrupo: function(){
			Ext.getCmp('DetalleNuevaTasaBase.labelConfigurar').disable();
			Ext.getCmp('DetalleNuevaTasaBase.labelGrupo').disable();
			var comboGrupo = Ext.getCmp('DetalleNuevaTasaBase.comboGrupo');
			comboGrupo.disable();
			comboGrupo.clearValue();
			comboGrupo.getStore().removeAll();
			Ext.getCmp('DetalleNuevaTasaBase.labelRelacionMatematica').disable();
			var comboRelacionMatematica = Ext.getCmp('DetalleNuevaTasaBase.comboRelacionMatematica');
			comboRelacionMatematica.disable();
			comboRelacionMatematica.setValue("+");
			Ext.getCmp('DetalleNuevaTasaBase.labelPuntosAdicionales').disable();
			var puntosAdicionales = Ext.getCmp('DetalleNuevaTasaBase.puntosAdicionales');
			puntosAdicionales.disable();
			puntosAdicionales.setValue("");		
		}
	};
	
	//----------------------- GRIDPANEL AGREGAR NUEVA RELACION IF --------------------------
	
	var calcularTasa01			= function(tasa,relacionMatematica,puntos){
 
		var valorTasa			 	= parseFloat( tasa   );
		var puntosAdicionales  	= parseFloat( puntos );
		
		var tasaAAplicar;
		switch (relacionMatematica) {
			case "+":
				tasaAAplicar 		= valorTasa + puntosAdicionales;
			break;
			case "-":
				tasaAAplicar 		= valorTasa - puntosAdicionales;
			break;
			case "/":
				if(puntosAdicionales!=0)
					tasaAAplicar 	= valorTasa / puntosAdicionales;
			break;
			case "*":
				tasaAAplicar 		= valorTasa * puntosAdicionales;
			break;
		}
		tasaAAplicar = roundOff(tasaAAplicar,5);
		return tasaAAplicar;
		
	}
	
	var calcularTasasPorGrupo01 = function(parametros){
 
		// Revisar que se hayan enviado todos los par�metros
		if(Ext.isEmpty(parametros)){
			return;
		} else if(Ext.isEmpty(parametros.claveGrupo)){
			return;
		} else if(Ext.isEmpty(parametros.relacionMatematica)){
			return;
		} else if(Ext.isEmpty(parametros.puntosAdicionales)){
			return;
		}
 
		// Asignar Puntos Adicionales y Tasa a Aplicar a cada uno de los registros del grupo.
		var detalleIFRelacionarData = Ext.StoreMgr.key("detalleIFRelacionarDataStore");
		detalleIFRelacionarData.each(
			function(record){
 
				if( record.get('GROUP_ID') === parametros.claveGrupo ){
					var tasaAAplicar = calcularTasa01(
						record.get('VALOR_TASA'),
						parametros.relacionMatematica,
						parametros.puntosAdicionales
					);
					record.set('PUNTOS_ADICIONALES',	 parametros.puntosAdicionales	 );
					record.set('RELACION_MATEMATICA', parametros.relacionMatematica );
					record.set('TASA_A_APLICAR',		 tasaAAplicar						 );
				}
				
			}
		);
			
	}
 								
	var valorTasaRenderer01 = function( value, metadata, record, rowIndex, colIndex, store ){
		
		return Ext.isEmpty(value)?'':Ext.util.Format.number(value,'0.00000') + "%";
		
	}
	
	var puntosAdicionalesRenderer01 = function( value, metadata, record, rowIndex, colIndex, store ){
 
		metadata.attr  = 'style="border: 1px solid gray;"';
		return Ext.util.Format.number(value,'0.00000');

	}
	
	var tasaAAplicarRenderer01 = function( value, metadata, record, rowIndex, colIndex, store ){
 
		// metadata.attr  = 'style="border: 1px solid gray;"';
		return Ext.isEmpty(value)?'':Ext.util.Format.number(value,'0.00000') + "%";
		
	}
	
	/*var fechaDetalleRenderer     = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;" ';
		return Ext.util.Format.date(value, 'd/m/Y H:i:s');
		
	}*/

	var catalogoEPOMoneda01Data = new Ext.data.JsonStore({
		id: 			'catalogoEPOMoneda01DataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13forma02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPOMoneda01'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
					
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("DetalleIFRelacionar.comboGrupo").setValue(defaultValue);
						Ext.getCmp("DetalleIFRelacionar.comboGrupo").originalValue = defaultValue;
					}
					//Ext.getCmp("DetalleIFRelacionar.comboGrupo").setReadOnly(true);
								
				} 
									
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoRelacionMatematica01Data = new Ext.data.ArrayStore({
		id: 		'catalogoRelacionMatematica01DataStore',
		fields:	[ 'clave', 'descripcion', 'loadMsg' ],
		data:  	[
			[ '+', '+' ],
			[ '-', '-' ],
			[ '*', '*' ],
			[ '/', '/' ]
		],
		autoDestroy: 	true
	});
		
	var catalogoIFxRelacionarData = new Ext.data.JsonStore({
		id: 			'catalogoIFxRelacionarDataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13forma02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIFxRelacionar'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
					
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("DetalleIFRelacionar.comboIF").setValue(defaultValue);
						Ext.getCmp("DetalleIFRelacionar.comboIF").originalValue = defaultValue;
					}
					//Ext.getCmp("DetalleIFRelacionar.comboIF").setReadOnly(true);
								
				} 
									
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
 
	// Definir handler para procesar las consultas
	var procesarDetalleIFRelacionarData = function(store, registros, opts){
				
		var panelFormaTasasBasePorEPO = Ext.getCmp('panelFormaTasasBasePorEPO');
		panelFormaTasasBasePorEPO.el.unmask();
		
		var gridDetalleIFRelacionar = Ext.getCmp('gridDetalleIFRelacionar');
		
		if (registros != null) {
			
			/*
			if (!gridDetalleIFRelacionar.isVisible()) {
				gridDetalleIFRelacionar.show();
			}
			*/
			
			var el 					 = gridDetalleIFRelacionar.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
					
	};

	// Crear JsonStore que se encargar� de cargar el detalle
	var detalleIFRelacionarData = new Ext.data.GroupingStore({
		id:					'detalleIFRelacionarDataStore',
		name:					'detalleIFRelacionarDataStore',
		root:					'registros',
		url: 					'13forma02ext.data.jsp',
		autoDestroy:		true,
		baseParams: {
			informacion: 	'ConsultaDetalleIFRelacionarData'
		},			
		reader: new Ext.data.JsonReader({
			root: 			'registros',	
			totalProperty: 'total',
			fields: [
				{ name:"GROUP_ID",							type:"int" 	  }, // IC_IF + ":" + IC_MONEDA
				{ name:"INTERMEDIARIO_FINANCIERO",		type:"string" },
				{ name:"MONEDA",								type:"string" },
				{ name:"TIPO_TASA",							type:"string" },
				{ name:"PLAZO",								type:"string" },
				{ name:"VALOR_TASA",							type:"string" },
				{ name:"RELACION_MATEMATICA",				type:"string" },
				{ name:"PUNTOS_ADICIONALES",				type:"string" },
				{ name:"TASA_A_APLICAR",					type:"string" },
				{ name:"IC_MONEDA",							type:"string" },
				{ name:"IC_IF",								type:"string" },
				{ name:"IC_EPO",								type:"string" },
				{ name:"CLAVE_TASA",							type:"string" }
				/*
				{ name:"FECHA_TASA_BASE",					type:"string" },
				{ name:"VOBO_EPO",							type:"string" },
				{ name:"VOBO_IF",								type:"string" },
				{ name:"VOBO_NAFIN",							type:"string" },
				{ name:"RANGO_PLAZO",						type:"string" }
				*/				
			]
		}),
		groupField: 			'GROUP_ID', 
		sortInfo: 				{ field: 'INTERMEDIARIO_FINANCIERO', direction: "ASC" },
		autoLoad: 				false,
		totalProperty: 		'total',
		messageProperty: 		'msg',
		pruneModifiedRecords: true, // Para que no se conserven los registros modificados en consulta anterior.
		listeners: { 
			beforeload: {
				fn: function( store, opts ) {
					var grid = Ext.getCmp('gridDetalleIFRelacionar');
					if (grid) {
						grid.show();
					} 			
				}
			},
			load: 	procesarDetalleIFRelacionarData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarDetalleIFRelacionarData(null, null, null);						
				}
			},
			update: function( detalleIFRelacionarDataStore, record, operation ){
				if( operation === Ext.data.Record.EDIT ){
					var changes 				= record.getChanges();
					if( !Ext.isEmpty(changes['PUNTOS_ADICIONALES']) ){
						var tasaAAplicar 		= calcularTasa01( record.get('VALOR_TASA'), record.get('RELACION_MATEMATICA'), changes['PUNTOS_ADICIONALES'] );
						record.set( 'TASA_A_APLICAR', tasaAAplicar );
					}
				}
			}
		}
	});
    
	var gridDetalleIFRelacionar = {
		frame:			true,
		store: 			detalleIFRelacionarData,
		title:			'Agregar Relaci�n IF',
		xtype: 			'editorgrid',
		id:				'gridDetalleIFRelacionar',
		hidden:			true,
		stripeRows: 	true,
		loadMask: 		true,
		width: 			650, 
		height:			480,
		region: 			'center',
		style: 			'margin: 0 auto',
		autoExpandColumn: 'TIPO_TASA',
		view:		 		new Ext.grid.GroupingView(
			{
				forceFit:				false, 
				groupTextTpl: 			'{group}', 
				hideGroupedColumn: 	true,
				enableGroupingMenu:	false
			}),
		columns: [
			{
				header: 		'GROUP ID',
				tooltip: 	'GROUP ID',
				dataIndex: 	'GROUP_ID', // IC_IF + ":" + IC_MONEDA 
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				//width: 		315,
				hidden: 		true,
				hideable:	false,
				groupRenderer: function(value,unused,record,rowIndex,colIndex,dsStore){
					return  record.json['INTERMEDIARIO_FINANCIERO'] + "&nbsp;(&nbsp;<span style='font-weight:bold;color:black;'>" + record.json['MONEDA'] +"</span>&nbsp;)";
				}
			},
			{
				header: 		'Intermediario Financiero',
				tooltip: 	'Intermediario Financiero',
				dataIndex: 	'INTERMEDIARIO_FINANCIERO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		315,
				hidden: 		true,
				hideable:	false
			},
			{
				header: 		'Moneda',
				tooltip: 	'Moneda',
				dataIndex: 	'MONEDA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		50,
				hidden: 		true,
				hideable:	false
			},
			{
				id:			'TIPO_TASA',
				header: 		'Tipo Tasa<br>&nbsp;',
				tooltip: 	'Tipo Tasa',
				dataIndex: 	'TIPO_TASA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Plazo<br>&nbsp;',
				tooltip: 	'Plazo',
				dataIndex: 	'PLAZO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Valor<br>&nbsp;',
				tooltip: 	'Valor',
				dataIndex: 	'VALOR_TASA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer: 	valorTasaRenderer01
			},
			{
				header: 		'Relaci�n<br>Matem�tica',
				tooltip: 	'Relaci�n Matem�tica',
				dataIndex: 	'RELACION_MATEMATICA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Puntos<br>Adicionales',
				tooltip: 	'Puntos Adicionales',
				dataIndex: 	'PUNTOS_ADICIONALES',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	puntosAdicionalesRenderer01,
				editor: new Ext.grid.GridEditor(
					new Ext.form.NumberField({
						// allowBlank: false,
						// allowNegative: false,
						style: (
								Ext.isIE7?'top: 0px; padding-bottom: 2px;':
								( Ext.isIE8?'top: 0px; padding-bottom: 2px;':
									'top: 1px; padding-bottom: 2px;')
						),
						format: 				'0.00000',
						decimalPrecision: 5,
						maxValue: 			999.99999,
            		minValue: 			0.00000  // Debido a que el signo se especifica en el Combo: Relaci�n Matem�tica.
            	}),
					{
						revertInvalid: 	false // Evita que se resetee el valor capturado cuando este es invalido y se guarde sin cambios.
					}
				)
			},
			{
				header: 		'Tasa a<br>Aplicar',
				tooltip: 	'Tasa a Aplicar',
				dataIndex: 	'TASA_A_APLICAR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer: 	tasaAAplicarRenderer01
			}
		],
		stateful:	false,
		tbar: {
			xtype: 'container',
			items: [
			{
				xtype: 'toolbar',
				items: [
					// BOTON: AUTORIZAR TASAS
					{
						xtype:	'button',
						id:		'DetalleIFRelacionar.botonAutorizarTasas',
						name:		'botonAutorizarTasas',
						text:  	'Autorizar Tasas',
						iconCls:	'icoAutorizar',
						handler: function(button,event){
							
							// 0. Si el editor de puntos adicionales se encuentra en estado inv�lido, cancelar la operaci�n.
							var columnModel					= Ext.getCmp("gridDetalleIFRelacionar").getColumnModel();
							var puntosAdicionalesIndex 	= columnModel.findColumnIndex('PUNTOS_ADICIONALES');
							var puntosAdicionalesEditor	= columnModel.getCellEditor(puntosAdicionalesIndex,0); // Es el mismo editor para la toda la columna
							if( puntosAdicionalesEditor.isVisible() && !puntosAdicionalesEditor.field.isValid() ){
								return;
							}
							
							var detalleIFRelacionarData 	= Ext.StoreMgr.key("detalleIFRelacionarDataStore");
							// 1. Validar que se hayan capturado todos los puntos de las Tasas.
							var faltanPuntos			 		= false;
							var faltanTasas					= false;
							detalleIFRelacionarData.each(
								function(record){
									if( Ext.isEmpty(record.get('PUNTOS_ADICIONALES')) ){
										faltanPuntos 	= true;
										return false;
									} else if( Ext.isEmpty(record.get('TASA_A_APLICAR')) ){
										faltanTasas 	= true;
										return false;
									}
								}
							);
							if( faltanPuntos || faltanTasas ){
								Ext.Msg.alert("Aviso","Debe capturar todos los puntos de las tasas");
								return;
							}
							
							// 2. Validar que cumplan con los puntos l�mite.
							// Nota: En vez de validar solo las cabceras de grupo de los puntos adicionales, se valida
							// cada uno de los campos de forma individual
							var csPuntosLimite 		= Ext.getCmp("DetalleIFRelacionar.csPuntosLimite").getValue();
							var excedePuntosLimite 	= false;
							if (csPuntosLimite === "S") {
								
								var puntosAdicionalesLimite 	= Ext.getCmp('DetalleIFRelacionar.fnPuntosLimite').getValue();	
								puntosAdicionalesLimite			= parseFloat(puntosAdicionalesLimite);
																
								detalleIFRelacionarData.each(
									function(record){
 
										if ( roundOff(puntosAdicionalesLimite, 5) < roundOff(record.get('PUNTOS_ADICIONALES'), 5) ){
											excedePuntosLimite = true;
											return false;
										}
									}
								);
								
							}
							if(excedePuntosLimite){
								Ext.Msg.alert("Aviso","No se puede capturar puntos adicionales mayor a los puntos adicionales l�mite.");
								return;
							}
		
							// 3. Solicitar confirmacion final
							Ext.MessageBox.confirm(
								'Confirm', 
								'Estas tasas ser�n aplicadas al operar los documentos \n�Est� seguro de enviar los valores que ha capturado?',
								function(boton) {
														
									if (boton == 'yes') {
										var respuesta = new Object();
										// Agregar detalle de todos los registros con sus tasas
										var detalleIFRelacionar = new Array();
										detalleIFRelacionarData.each(
											function(record){
												detalleIFRelacionar.push(record.data);
											}
										);
										respuesta['detalleIFRelacionarData'] = Ext.encode(detalleIFRelacionar);
										respuesta['claveIF'] 					 = Ext.getCmp('DetalleIFRelacionar.comboIF').getValue();
										respuesta['claveEPO'] 					 = Ext.getCmp('DetalleIFRelacionar.claveEPO').getValue();
										
										
										capturaTasas("GUARDAR_TASA_BASE_IF_RELACIONAR",respuesta);
									} else if (boton == 'no') {
										return;
									}
														
								}
							);
								
						}
					},
					// BOTON:  CANCELAR
					{
						xtype:	'button',
						id:		'DetalleIFRelacionar.botonCancelar',
						name:		'botonCancelar',
						text:		'Cancelar',
						iconCls:	'cancelar',
						handler:	function(button,event){
							var detalleIFRelacionarData = Ext.StoreMgr.key('detalleIFRelacionarDataStore');
							if( detalleIFRelacionarData.getModifiedRecords().length > 0 ){
								Ext.MessageBox.confirm(
									'Confirm', 
									'�Est� seguro que desea cancelar la operaci�n?',
									function(boton) {
														
										if (boton == 'yes') {
											var respuesta = new Object();
											capturaTasas("OCULTAR_GRID_DETALLE_IF_RELACIONAR",respuesta);
										} else if (boton == 'no') {
											return;
										}
														
									}
								);
							} else {
								var respuesta = new Object();
								capturaTasas("OCULTAR_GRID_DETALLE_IF_RELACIONAR",respuesta);
							}
						}
					},			
					"->",
					"-",
					// COMBO IF
					{
						xtype: 'label',
						id:	 'DetalleIFRelacionar.labelIF',
						html:  'IF:&nbsp;',
						style: 'padding-right:5pt;padding-left:5pt;',
						width: 30
					},
					{
						xtype: 				'combo',
						width:				300,
						id: 					'DetalleIFRelacionar.comboIF',
						name: 				'comboIF',
						fieldLabel: 		'IF',
						allowBlank: 		true,
						mode: 				'local', 
						displayField: 		'descripcion',
						valueField: 		'clave',
						hiddenName: 		'claveIF',
						emptyText: 			'Seleccione un IF...', 
						forceSelection: 	true,
						triggerAction: 	'all',
						typeAhead: 			true,
						minChars: 			1,
						store: 				catalogoIFxRelacionarData,
						tpl: 					NE.util.templateMensajeCargaCombo,
						listeners:	{
							collapse: function( comboIF ){
								var respuesta 			 = new Object();
								respuesta['claveIF']  = comboIF.getValue();
								respuesta['claveEPO'] = Ext.getCmp("comboEPO").getValue(); 
								// Obtener clave y descripcion del IF seleccionado
								var ifSeleccionado	 = new Object();
								comboIF.getStore().each( function(record){
									if( respuesta['claveIF'] === record.data['clave'] ){
										Ext.apply(ifSeleccionado,record.json);
										return false;
									}
								}); 
								respuesta['ifSeleccionado'] = Ext.encode(ifSeleccionado);
								capturaTasas("CONSULTAR_TASA_BASE_IF_RELACIONAR",respuesta);
							}
						}
						//, anchor:				'-20'
					},
					{
						xtype: 		'hidden',
						id:	 		'DetalleIFRelacionar.csPuntosLimite',
						name:  		'csPuntosLimite'
					},
					{
						xtype: 		'hidden',
						id:	 		'DetalleIFRelacionar.fnPuntosLimite',
						name:  		'fnPuntosLimite'
					},
					{
						xtype: 		'hidden',
						id:	 		'DetalleIFRelacionar.claveEPO',
						name:  		'claveEPO'
					}					
				]
			},
			{
				xtype: 'toolbar',
				style:	'padding: 5px;',
				items:[
					{
						xtype: 'label',
						id:    'DetalleIFRelacionar.labelConfigurar',
						html:  'Configurar Propiedades del Grupo',
						style: 'font-weight:bold;padding-bottom:10pt;text-align:center'
					}
				]
			},
			{
				xtype: 'toolbar',
				style:	'padding: 5px;',
				items:[
							{
								xtype: 'label',
								id:    'DetalleIFRelacionar.labelGrupo',
								html:  'Grupo:&nbsp;',
								style: 'padding-right:5pt;padding-left:5pt;'
							},
							{
								xtype: 				'combo',
								width:				200,
								id: 					'DetalleIFRelacionar.comboGrupo',
								fieldLabel: 		'Grupo',
								allowBlank: 		true,
								mode: 				'local', 
								displayField: 		'descripcion',
								valueField: 		'clave',
								hiddenName: 		'claveGrupo',
								emptyText:			'Seleccione...',
								forceSelection: 	true,
								triggerAction: 	'all',
								typeAhead: 			true,
								minChars: 			1,
								store: 				catalogoEPOMoneda01Data,
								//tpl: 					NE.util.templateMensajeCargaCombo
								tpl:					'<tpl for=".">' +
														'<tpl if="!Ext.isEmpty(loadMsg)">'+
														'<div class="loading-indicator">{loadMsg}</div>'+
														'</tpl>'+
														'<tpl if="Ext.isEmpty(loadMsg)">'+
														'<div class="x-combo-list-item">' +
														'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
														'</div></tpl></tpl>',
								listeners: {
									collapse: function( comboGrupo, newValue, oldValue ){
										Ext.getCmp('DetalleIFRelacionar.comboRelacionMatematica').reset();
										Ext.getCmp('DetalleIFRelacionar.puntosAdicionales').reset();
									}
								}
							},
							{
								xtype: 'label',
								id: 	 'DetalleIFRelacionar.labelRelacionMatematica',
								html:  'Relaci�n Matem�tica:&nbsp;',
								style: 'padding-right:5pt;padding-left:5pt;'
							},
							{
								xtype: 				'combo',
								width:				50,
								id: 					'DetalleIFRelacionar.comboRelacionMatematica',
								value:				'+',
								fieldLabel: 		'Relaci�n Matem�tica',
								allowBlank: 		false,
								mode: 				'local', 
								displayField: 		'descripcion',
								valueField: 		'clave',
								hiddenName: 		'relacionMatematica',
								emptyText:			'Seleccione...',
								forceSelection: 	true,
								triggerAction: 	'all',
								typeAhead: 			true,
								minChars: 			1,
								store: 				catalogoRelacionMatematica01Data,
								tpl: 					NE.util.templateMensajeCargaCombo,
								listeners: {
									change: function( comboRelacionMatematica, newValue, oldValue ){
										// Nota: No se hace nada por el momento.
									}
								}
							},
							{
								xtype: 'label',
								id: 	 'DetalleIFRelacionar.labelPuntosAdicionales',
								html:  'Puntos Adicionales:&nbsp;',
								style: 'padding-right:5pt;padding-left:5pt;'
							},
							{
								xtype: 		'numberfield',
								width:		90,
								id:			'DetalleIFRelacionar.puntosAdicionales',
								value:		'',
								name:			'puntosAdicionales',
								fieldLabel: 'Puntos Adicionales',
								decimalPrecision: 5,
								maxValue: 	999.99999,  
								minValue:	0.00000,  // Debido a que el signo se especifica en el Combo: Relaci�n Matem�tica.
								listeners: {
									blur: function( puntosAdicionales ){
										var parametros 						= new Object();
										parametros['claveGrupo'] 			= Ext.getCmp('DetalleIFRelacionar.comboGrupo').getValue();
										parametros['relacionMatematica'] = Ext.getCmp('DetalleIFRelacionar.comboRelacionMatematica').getValue();
										parametros['puntosAdicionales']  = puntosAdicionales.isValid()?puntosAdicionales.getValue():'';
										calcularTasasPorGrupo01(parametros);
									}
								}
							}
						]
				}
		]		
		},
		inicializarPanelConfigurarGrupo: function(){
			Ext.getCmp('DetalleIFRelacionar.labelConfigurar').enable();
			Ext.getCmp('DetalleIFRelacionar.labelGrupo').enable();
			var comboGrupo = Ext.getCmp('DetalleIFRelacionar.comboGrupo');
			comboGrupo.enable();
			comboGrupo.clearValue();
			comboGrupo.getStore().removeAll();
			Ext.getCmp('DetalleIFRelacionar.labelRelacionMatematica').enable();
			var comboRelacionMatematica = Ext.getCmp('DetalleIFRelacionar.comboRelacionMatematica');
			comboRelacionMatematica.enable();
			comboRelacionMatematica.setValue("+");
			Ext.getCmp('DetalleIFRelacionar.labelPuntosAdicionales').enable();
			var puntosAdicionales = Ext.getCmp('DetalleIFRelacionar.puntosAdicionales');
			puntosAdicionales.enable();
			puntosAdicionales.setValue("");			
		},
		deshabilitaPanelConfigurarGrupo: function(){
			Ext.getCmp('DetalleIFRelacionar.labelConfigurar').disable();
			Ext.getCmp('DetalleIFRelacionar.labelGrupo').disable();
			var comboGrupo = Ext.getCmp('DetalleIFRelacionar.comboGrupo');
			comboGrupo.disable();
			comboGrupo.clearValue();
			comboGrupo.getStore().removeAll();
			Ext.getCmp('DetalleIFRelacionar.labelRelacionMatematica').disable();
			var comboRelacionMatematica = Ext.getCmp('DetalleIFRelacionar.comboRelacionMatematica');
			comboRelacionMatematica.disable();
			comboRelacionMatematica.setValue("+");
			Ext.getCmp('DetalleIFRelacionar.labelPuntosAdicionales').disable();
			var puntosAdicionales = Ext.getCmp('DetalleIFRelacionar.puntosAdicionales');
			puntosAdicionales.disable();
			puntosAdicionales.setValue("");		
		}
	};
	
	//------------------------ WINDOW MODIFICAR TASA BASE  --------------------------
	
	var calcularTasaAAplicar = function(){
					
		var puntosAdicionales 			= Ext.getCmp("ModificarTasaBase.puntosAdicionales");
		var tasaAAplicar					= Ext.getCmp("ModificarTasaBase.tasaAAplicar");
		var comboRelacionMatematica 	= Ext.getCmp("ModificarTasaBase.comboRelacionMatematica");
					
		var relMat 							= comboRelacionMatematica.getValue();
		var puntos 							= puntosAdicionales.getValue();
		var tasa								= Ext.getCmp("ModificarTasaBase.valor").getValue();
		
		var operacion;
		if( puntosAdicionales.isValid() ) {
						
			switch (relMat) {
				case "+":
					operacion 		= parseFloat(tasa)+parseFloat(puntos);
				break;
				case "-":
					operacion 		= parseFloat(tasa)-parseFloat(puntos);
				break;
				case "/":
					if(puntos!=0)
						operacion 	= parseFloat(tasa)/parseFloat(puntos);
				break;
				case "*":
					operacion 		= parseFloat(tasa)*parseFloat(puntos);
				break;
			}
			tasaAAplicar.setValue(roundOff(operacion,5));				
		} 
		
	}
	
	function validaPlazoEspecifico(){
		
		var plazoEspecificoField		= Ext.getCmp('ModificarTasaBase.plazoEspecifico');
		var plazoEspecifico 				= plazoEspecificoField.getValue();
		var plazoDiasInicio				= Ext.getCmp('ModificarTasaBase.plazoDiasInicio').getValue();
		var plazoDiasFin					= Ext.getCmp('ModificarTasaBase.plazoDiasFin').getValue();
		
		// Se permite borrar el plazo espec�fico
		if( Ext.isEmpty(plazoEspecifico) ){
			return true;
		}
		
		// Validar que el plazo espec�fico sea un n�mero valido
		if( !plazoEspecifico.toString().match("^[0-9]+$") ){
			plazoEspecificoField.markInvalid("El n�mero capturado no es v�lido.");
			return false;
		}

		if( Ext.isEmpty(plazoDiasInicio) ){
			plazoEspecificoField.markInvalid("Error al validar plazo espec�fico, el d�a inicial del rango no existe, favor de reportarlo al administrador del sistema.");
			return false;
		} else if( !plazoDiasInicio.toString().match("^[0-9]+$")  ){
			plazoEspecificoField.markInvalid("Error al validar plazo espec�fico, el d�a inicial del rango no corresponde a un n�mero v�lido, favor de reportarlo al administrador del sistema.");
			return false;
		}
			
		if( Ext.isEmpty(plazoDiasFin) ){
			plazoEspecificoField.markInvalid("Error al validar plazo espec�fico, el d�a final del rango no existe, favor de reportarlo al administrador del sistema.");
			return false;
		} else if( !plazoDiasFin.toString().match("^[0-9]+$")  ){
			plazoEspecificoField.markInvalid("Error al validar plazo espec�fico, el d�a final del rango no corresponde a un n�mero v�lido, favor de reportarlo al administrador del sistema.");
			return false;
		}
			
		plazoEspecifico 	= parseInt(plazoEspecifico,10); 
		plazoDiasInicio	= parseInt(plazoDiasInicio,10); 
		plazoDiasFin		= parseInt(plazoDiasFin,10); 
			
		if( 			plazoEspecifico < plazoDiasInicio 	){
			plazoEspecificoField.markInvalid("El valor capturado no debe ser menor al rango del plazo.");
			return false;
		} else if( 	plazoEspecifico > plazoDiasFin 		){
			plazoEspecificoField.markInvalid("El valor capturado sobrepasa el l&iacute;mite del plazo.");
			return false;
		}

		return true;
		
	}
	
	function validaLimitePuntosAdicionales() {
		
		var puntosAdicionalesField		= Ext.getCmp('ModificarTasaBase.puntosAdicionales');
		
		var puntosAdicionalesLimite 	= new Number( Ext.getCmp('ModificarTasaBase.fnPuntosLimite').getValue() );
		var puntosAdicionalesPrev 		= new Number( Ext.getCmp('ModificarTasaBase.puntosAdicionalesPrev').getValue() );
		var puntosAdicionales 			= parseFloat(puntosAdicionalesField.getValue());
		
		//if (roundOff(puntosAdicionalesPrev, 5) != roundOff(puntosAdicionales, 5)) {
        if (puntosAdicionalesPrev != puntosAdicionales) {
			//if (roundOff(puntosAdicionalesLimite, 5) < roundOff(puntosAdicionales, 5)) {
            if (puntosAdicionalesLimite < puntosAdicionales) {
				puntosAdicionalesField.markInvalid("No se puede capturar puntos adicionales mayor a los puntos adicionales l�mite.");
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
		
	}
	
	var modificarTasaBase = function(){
		
		// Validar Forma
		if( !Ext.getCmp('formaModificarTasaBase').getForm().isValid() ){
			return;
		}
		
		var hayErrorValidacionEspecifica = false;
		
		// Validar Plazo Espec�fico en caso de que el registro modificado corresponda al rango m�ximo.
		var esRangoMaximo = Ext.getCmp('ModificarTasaBase.esRangoMaximo').getValue() === 'true'?true:false;
		if( esRangoMaximo ){
			if (!validaPlazoEspecifico()) {
				hayErrorValidacionEspecifica = true;
			}
		}
		
		// Validar Puntos L�mite
		var csPuntosLimite = Ext.getCmp('ModificarTasaBase.csPuntosLimite').getValue();
		if (csPuntosLimite === "S") {
			if (!validaLimitePuntosAdicionales()) {
				hayErrorValidacionEspecifica = true;
			}
		}
		
		if( hayErrorValidacionEspecifica ){
			return;
		}
		
		// Solicitar confirmacion por parte del usuario.
		var autorizaCheckbox = Ext.getCmp('ModificarTasaBase.autorizaCheckbox').getValue(); 
		Ext.MessageBox.confirm(
			'Confirm', 
			'�Est� seguro de modificar el registro?',
			function(boton) {
								
				if (boton == 'yes') {
 
					if( autorizaCheckbox ) {
						Ext.MessageBox.confirm(
							'Confirm', 
							'�Est� seguro que desea autorizar?',
							function(boton) {
												
								if (boton == 'yes') {
									Ext.getCmp('ModificarTasaBase.autoriza').setValue("S");
									var respuesta = Ext.getCmp('formaModificarTasaBase').getForm().getValues();
									if( Ext.getCmp('ModificarTasaBase.esRangoMaximo').getValue() !== 'true' ){
										respuesta['plazoEspecifico'] = null;
									}
									capturaTasas("GUARDAR_MODIFICACION_TASA_BASE",respuesta);
								} else if (boton == 'no') {
									return;
								}
												
							}
						);
					} else {
						Ext.MessageBox.confirm(
							'Confirm', 
							'�Est� seguro que desea cambiar la autorizaci�n a N?',
							function(boton) {
												
								if (boton == 'yes') {			
									Ext.getCmp('ModificarTasaBase.autoriza').setValue("N");
									var respuesta = Ext.getCmp('formaModificarTasaBase').getForm().getValues();
									if( Ext.getCmp('ModificarTasaBase.esRangoMaximo').getValue() !== 'true' ){
										respuesta['plazoEspecifico'] = null;
									}
									capturaTasas("GUARDAR_MODIFICACION_TASA_BASE",respuesta);
								} else if (boton == 'no') {
									return;
								}
												
							}
						);
					}		
					
				} else if (boton == 'no') {
					return;
				}
								
			}
		);
 
	}
	
	var cancelaModificarTasaBase = function(){
		
		Ext.MessageBox.confirm(
			'Confirm', 
			'�Est� seguro que desea cancelar la operaci�n?',
			function(boton) {
												
				if (boton == 'yes') {		
					var respuesta = new Object();
					capturaTasas("OCULTAR_WINDOW_MODIFICAR_TASA_BASE",respuesta);
				} else if (boton == 'no') {
					return;
				}
												
			}
		);
		
	}
	
	var catalogoRelacionMatematicaData = new Ext.data.ArrayStore({
		id: 		'catalogoRelacionMatematicaDataStore',
		fields:	[ 'clave', 'descripcion', 'loadMsg' ],
		data:  	[
			[ '+', '+' ],
			[ '-', '-' ],
			[ '*', '*' ],
			[ '/', '/' ]
		],
		autoDestroy: 	true
	});
	
	var elementosFormaModificarTasaBase = [
		{
			xtype: 			'label',	
			id:				'ModificarTasaBase.nombreIF',
			cls: 				'x-form-item',
			//boxMaxHeight: 	44,
			//height: 		 	44,
			//boxMinHeight: 	44,
			style:			'font-weight:bold;padding-bottom:10pt;text-align:center',
			html: 			''
		},
		{
			xtype: 		'displayfield',
			id:			'ModificarTasaBase.moneda',
			fieldLabel: 'Moneda',
			name:    	'moneda',
			value: 		'',
			readOnly:	true,
			anchor:		'-20'
		},
		{
			xtype: 		'displayfield',
			id:			'ModificarTasaBase.tipoTasa',
			fieldLabel: 'Tipo Tasa',
			name:    	'tipoTasa',
			value: 		'',
			anchor:		'-20'
		},
		{
			xtype: 		'displayfield',
			id:			'ModificarTasaBase.plazo',
			fieldLabel: 'Plazo',
			name:    	'plazo',
			value: 		'',
			anchor:		'-20'
		},
		{
			xtype: 		'textfield',
			id:			'ModificarTasaBase.plazoEspecifico',
			fieldLabel: 'Plazo Espec&iacute;fico',
			name:    	'plazoEspecifico',
			value: 		'',
			anchor:		'-20',
			maxLength: 	5,
			msgTarget:  'side',
			maskRe: 		new RegExp(/\d/)
		},
		{
			xtype: 		'displayfield',
			id:			'ModificarTasaBase.valor',
			fieldLabel: 'Valor',
			name:    	'valor',
			value: 		'',
			anchor:		'-20'
		},
		{
			xtype: 				'combo',
			id:					'ModificarTasaBase.comboRelacionMatematica',
			width:				75,
			name: 				'comboRelacionMatematica',
			fieldLabel: 		'Relaci�n Matem�tica',
			allowBlank: 		true,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'relacionMatematica',
			emptyText: 			'Seleccionar...', 
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoRelacionMatematicaData,
			tpl: 					NE.util.templateMensajeCargaCombo, 
			anchor:				'-224',
			listeners:			{
				collapse: calcularTasaAAplicar
			}
		},
		{
			xtype: 				'numberfield',
			id:					'ModificarTasaBase.puntosAdicionales',
			fieldLabel:			'Puntos Adicionales',
			name:    			'puntosAdicionales',
			decimalPrecision: 5, 
			readOnly:			false,
			anchor:				'-20',
			allowBlank: 		false,
			blankText:			'Debe de introducir un valor Num�rico Entero o Decimal',
			msgTarget:        'side',
			maxValue: 			999.99999,  
			minValue:			0.00000, // Debido a que el signo se especifica en el Combo: Relaci�n Matem�tica.
			listeners:			{
				change: calcularTasaAAplicar
			}
		},
		{
			xtype: 		'textfield',
			id:			'ModificarTasaBase.tasaAAplicar',
			fieldLabel: 'Tasa a Aplicar',
			name:    	'tasaAAplicar',
			decimalPrecision: 5, 
			value: 		'',
			style:		'background: gainsboro;',
			readOnly:	true,
			anchor:		'-20'
		},
		{
			xtype: 		'checkbox',
			id:			'ModificarTasaBase.autorizaCheckbox',
			fieldLabel: 'Autoriza',
			name:    	'autorizaCheckbox',
			anchor:		'-20'
		},
		{
			xtype: 		'hidden',
			id:	 		'ModificarTasaBase.csPuntosLimite',
			name:  		'csPuntosLimite'
		},
		{
			xtype: 		'hidden',
			id:	 		'ModificarTasaBase.fnPuntosLimite',
			name:  		'fnPuntosLimite'
		},
		{
			xtype: 		'hidden',
			id:	 		'ModificarTasaBase.puntosAdicionalesPrev',
			name:  		'puntosAdicionalesPrev'
		},
		{
			xtype: 		'hidden',
			id:			'ModificarTasaBase.autoriza',
			name:  		'autoriza'
		},
		{
			xtype: 		'hidden',
			id:	 		'ModificarTasaBase.icIf',
			name:  		'icIf'
		},
		{
			xtype: 		'hidden',
			id:			'ModificarTasaBase.icEpo',
			name:  		'icEpo'
		},
		{
			xtype: 		'hidden',
			id:			'ModificarTasaBase.fechaTasaBase',
			name:  		'fechaTasaBase'
		},
		{
			xtype: 		'hidden',
			id:			'ModificarTasaBase.icPlazo',
			name:  		'icPlazo'
		},
		{
			xtype: 		'hidden',
			id:			'ModificarTasaBase.plazoDiasInicio',
			name:  		'plazoDiasInicio'
		},
		{
			xtype: 		'hidden',
			id:			'ModificarTasaBase.plazoDiasFin',
			name:  		'plazoDiasFin'
		},
		{
			xtype: 		'hidden',
			id:			'ModificarTasaBase.esRangoMaximo',
			name:  		'esRangoMaximo'
		}
	];
	
	var formaModificarTasaBase = new Ext.form.FormPanel({
		id: 				'formaModificarTasaBase',
   	baseCls:			'x-plain',
   	labelWidth:		125,
   	items: 			elementosFormaModificarTasaBase,
   	//width:			400,
		//style: 		'margin: 0 auto;',
		//bodyStyle:	'padding: 10px',
		//bodyCfg:     { tag:'center' },
		//plain:       true,
		//frame:       true,		
		//border:      false,
		buttonAlign:	'center',
		buttons: [
			// BOTON ACEPTAR
			{
				xtype: 	'button',
				text:  	 'Guardar',    //'Aceptar',
				iconCls:  'icoGuardar', //'icoAceptar',
				// width:		100,
				handler: modificarTasaBase
			},
			// BOTON CANCELAR
			{
				xtype: 	'button',
				text:  	'Cancelar',
				iconCls: 'cancelar',
				//width:	100,
				handler: cancelaModificarTasaBase
			}
		]
   });
	
	var showWindowModificarTasaBase = function(respuesta){
 
		// 2. Establecer las propiedades de los parametros
		Ext.getCmp('ModificarTasaBase.nombreIF').setText(						respuesta['INTERMEDIARIO_FINANCIERO']							);
		Ext.getCmp('ModificarTasaBase.moneda').setValue(						respuesta['NOMBRE_MONEDA']											);
		Ext.getCmp('ModificarTasaBase.tipoTasa').setValue(						respuesta['TASA_BASE']												);
		Ext.getCmp('ModificarTasaBase.plazo').setValue(							respuesta['RANGO_PLAZO']											);
		//Ext.getCmp('ModificarTasaBase.plazoEspecifico').setValue(			respuesta['PLAZO_ESPECIFICO']										);
		Ext.getCmp('ModificarTasaBase.valor').setValue(							respuesta['VALOR_TASA']												);
		Ext.getCmp('ModificarTasaBase.comboRelacionMatematica').setValue( respuesta['RELACION_MATEMATICA']									);
		Ext.getCmp('ModificarTasaBase.puntosAdicionales').setValue(			respuesta['PUNTOS_ADICIONALES']									);
		Ext.getCmp('ModificarTasaBase.tasaAAplicar').setValue(				respuesta['TASA_A_APLICAR']										);
		Ext.getCmp('ModificarTasaBase.autorizaCheckbox').setValue(			respuesta['AUTORIZA']												);
		Ext.getCmp('ModificarTasaBase.csPuntosLimite').setValue(				respuesta['CS_PUNTOS_LIMITE']										);
		Ext.getCmp('ModificarTasaBase.fnPuntosLimite').setValue(				respuesta['FN_PUNTOS_LIMITE']										);
		Ext.getCmp('ModificarTasaBase.puntosAdicionalesPrev').setValue(	respuesta['PUNTOS_ADICIONALES']									);
		Ext.getCmp('ModificarTasaBase.autoriza').setValue(						respuesta['AUTORIZA']?"S":"N"										);
		Ext.getCmp('ModificarTasaBase.icIf').setValue(							respuesta['IC_IF']													);
		Ext.getCmp('ModificarTasaBase.icEpo').setValue(							respuesta['IC_EPO']													);
		Ext.getCmp('ModificarTasaBase.fechaTasaBase').setValue(				respuesta['FECHA_TASA_BASE']										);
		Ext.getCmp('ModificarTasaBase.icPlazo').setValue(						respuesta['IC_PLAZO']												);
		Ext.getCmp('ModificarTasaBase.plazoDiasInicio').setValue(			respuesta['PLAZO_DIAS_INICIO']									);
		Ext.getCmp('ModificarTasaBase.plazoDiasFin').setValue(				respuesta['PLAZO_DIAS_FIN']										);
		Ext.getCmp('ModificarTasaBase.esRangoMaximo').setValue(				respuesta['ES_RANGO_MAXIMO']										);

		// Configurar plazo espec�fico
		
		var plazoEspecifico = Ext.getCmp('ModificarTasaBase.plazoEspecifico');
		var esRangoMaximo	  = respuesta['ES_RANGO_MAXIMO'] === 'true'?true:false;
		if( esRangoMaximo ){
			var plazoEspecificoValue = Ext.isEmpty(respuesta['PLAZO_ESPECIFICO'])?'':respuesta['PLAZO_ESPECIFICO'];
			plazoEspecifico.setValue( plazoEspecificoValue );
			plazoEspecifico.setReadOnly(false);
		} else {
			plazoEspecifico.setValue( 'N/A' );
			plazoEspecifico.setReadOnly(true);
		}
		
		// 3. Mostrar ventana
		var windowModificarTasaBase = Ext.getCmp('windowModificarTasaBase');
		if (windowModificarTasaBase) {
			windowModificarTasaBase.show();
		} else {
			new Ext.Window({
				id: 			'windowModificarTasaBase',
				title:		'Modificar Tipo Tasa',
				width:		450,
				height:		400,
				resizable:	false,
				plain:		true,
				modal:		true,
				closable: 	false,
				bodyStyle:	'padding:10px;',
				layout: 		'fit',
				items: 		formaModificarTasaBase,
				listeners: {
					show: function(window){
						var esRangoMaximo	  = Ext.getCmp('ModificarTasaBase.esRangoMaximo').getValue() === 'true'?true:false;
						if( esRangoMaximo ){
							plazoEspecifico.getEl().setStyle( 'background', 'white' );
						} else {
							plazoEspecifico.getEl().setStyle( 'background', 'gainsboro' );
						}
					}
				}
			}).show();
		}
		
	}	

	var hideWindowModificarTasaBase = function(){
		
		var windowModificarTasaBase = Ext.getCmp('windowModificarTasaBase');
		
		if(windowModificarTasaBase && windowModificarTasaBase.isVisible() ){	
			windowModificarTasaBase.hide();
		}
      
	}
	
	//------------------------ GRID DETALLE TASAS POR EPO --------------------------
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 			'catalogoIFDataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13forma02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
					
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboIF").setValue(defaultValue);
						Ext.getCmp("comboIF").originalValue = defaultValue;
					}
					//Ext.getCmp("comboIF").setReadOnly(true);
								
				} 
									
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
 
	var plazoEspecificoRenderer = function( value, metadata, record, rowIndex, colIndex, store){
		if( record.get("ES_RANGO_MAXIMO") ){
			return Ext.isEmpty(value)?'':String(value);
		} else {
			return 'N/A';
		}
	}
	
	var tasaAAplicarRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;" ';
		return value + "%";
		
	}
	
	/*var fechaDetalleRenderer     = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word;" ';
		return Ext.util.Format.date(value, 'd/m/Y H:i:s');
		
	}*/
	
	// Definir handler para procesar las consultas
	var procesarDetalleTasasPorEpoData = function(store, registros, opts){
				
		var panelFormaTasasBasePorEPO = Ext.getCmp('panelFormaTasasBasePorEPO');
		panelFormaTasasBasePorEPO.el.unmask();
		
		var gridDetalleTasasPorEpo = Ext.getCmp('gridDetalleTasasPorEpo');
		
		if (registros != null) {

			var el 					 = gridDetalleTasasPorEpo.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
 
		}
					
	};
 
	// Crear JsonStore que se encargar� de cargar el detalle
	var detalleTasasPorEpoData = new Ext.data.GroupingStore({
		id:					'detalleTasasPorEpoDataStore',
		name:					'detalleTasasPorEpoDataStore',
		root:					'registros',
		url: 					'13forma02ext.data.jsp',
		autoDestroy:		true,
		sortInfo: 			{ field: 'INTERMEDIARIO_FINANCIERO', direction: "DESC" },
		baseParams: {
			informacion: 	'ConsultaDetalleTasasPorEpoData'
		},			
		reader: new Ext.data.JsonReader({
			root: 			'registros',	
			totalProperty: 'total',
			fields: [
				{ name:"GROUP_ID",							type:"int" 	  }, // IC_IF + ":" + IC_MONEDA
				{ name:"IC_IF",								type:"int"	  },
				{ name:"IC_MONEDA",							type:"int"	  },
				{ name:"INTERMEDIARIO_FINANCIERO",		type:"string" },
				{ name:"MONEDA",								type:"string" },
				{ name:"TASA_BASE",							type:"string" },
				{ name:"PLAZO",								type:"string" },
				{ name:"PLAZO_ESPECIFICO",					type:"int",	  		convert: function(value, record){ return (Ext.isEmpty(value)?null:parseInt(value, 10)); } },
				{ name:"VALOR_TASA",							type:"string" },
				{ name:"RELACION_MATEMATICA",				type:"string" },
				{ name:"PUNTOS_ADICIONALES",				type:"string" },
				{ name:"TASA_A_APLICAR",					type:"string" },
				{ name:"IC_PLAZO",							type:"string" },
				{ name:"PLAZO_DIAS_INICIO",				type:"int",   		convert: function(value, record){ return (Ext.isEmpty(value)?null:parseInt(value, 10)); } },
				{ name:"PLAZO_DIAS_FIN",					type:"int",	  		convert: function(value, record){ return (Ext.isEmpty(value)?null:parseInt(value, 10)); } },
				{ name:"ES_RANGO_MAXIMO",					type:"boolean" }
			]
		}),
		groupField: 		'GROUP_ID', 
		autoLoad: 			false,
		totalProperty: 	'total',
		messageProperty: 	'msg',
		listeners: { 
			beforeload: {
				fn: function( store, opts ) {
					var grid = Ext.getCmp('gridDetalleTasasPorEpo');
					if (grid) {
						grid.show();
					} 			
				}
			},
			load: 	procesarDetalleTasasPorEpoData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//Llama procesar consulta, para que desbloquee los componentes.
					procesarDetalleTasasPorEpoData(null, null, null);						
				}
			}
		}
	});
 
	var gridDetalleTasasPorEpo = {
		frame:			true,
		store: 			detalleTasasPorEpoData,
		xtype: 			'grid',
		id:				'gridDetalleTasasPorEpo',
		hidden: 			true,
		stripeRows: 	true,
		loadMask: 		true,
		width: 			725, 
		height:			480,
		region: 			'center',
		style: 			'margin: 0 auto',
		autoExpandColumn: 'TASA_BASE',
		view:		 		new Ext.grid.GroupingView(
			{
				forceFit:				false, 
				groupTextTpl: 			'{group}', 
				hideGroupedColumn: 	true,
				enableGroupingMenu:	false
			}),
		columns: [
			{
				header: 		'CLAVE IF MONEDA',
				tooltip: 	'CLAVE IF MONEDA',
				dataIndex: 	'GROUP_ID', // IC_IF + ":" + IC_MONEDA 
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				// width: 		315,
				hidden: 		true,
				hideable:	false,
				groupRenderer: function(value,unused,record,rowIndex,colIndex,dsStore){
					return  record.json['INTERMEDIARIO_FINANCIERO'] + "&nbsp;(&nbsp;<span style='font-weight:bold;color:black;'>" + record.json['MONEDA'] +"</span>&nbsp;)";
				}
			},
			{
				header: 		'Intermediario Financiero',
				tooltip: 	'Intermediario Financiero',
				dataIndex: 	'INTERMEDIARIO_FINANCIERO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		315,
				hidden: 		true,
				hideable:	false
			},
			{
				header: 		'Moneda',
				tooltip: 	'Moneda',
				dataIndex: 	'MONEDA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		50,
				hidden: 		true,
				hideable:	false
			},
			{
				id:			'TASA_BASE',
				header: 		'Tasa Base<br>&nbsp;',
				tooltip: 	'Tasa Base',
				dataIndex: 	'TASA_BASE',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Plazo<br>&nbsp;',
				tooltip: 	'Plazo',
				dataIndex: 	'PLAZO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Plazo<br>Espec&iacute;fico',
				tooltip: 	'Plazo Espec&iacute;fico',
				dataIndex: 	'PLAZO_ESPECIFICO',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer:	plazoEspecificoRenderer
			},
			{
				header: 		'Valor<br>&nbsp;',
				tooltip: 	'Valor',
				dataIndex: 	'VALOR_TASA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Relaci�n<br>Matem�tica',
				tooltip: 	'Relaci�n Matem�tica',
				dataIndex: 	'RELACION_MATEMATICA',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Puntos<br>Adicionales',
				tooltip: 	'Puntos Adicionales',
				dataIndex: 	'PUNTOS_ADICIONALES',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false
			},
			{
				header: 		'Tasa a<br>Aplicar',
				tooltip: 	'Tasa a Aplicar',
				dataIndex: 	'TASA_A_APLICAR',
				align:		'center',
				sortable: 	true,
				resizable: 	true,
				width: 		75,
				hidden: 		false,
				renderer: 	tasaAAplicarRenderer
			},
			{
				xtype: 		'customizedactioncolumn',
				header: 		'Modificar<br>&nbsp;',
				tooltip: 	'Modificar',
				align:		'center',
				width: 		75,
				hidden: 		false,
				items: [
					// MODIFICAR
					{
							
						getClass: function(value, metadata, record) { 
								
							this.items[0].tooltip 		= 'Modificar';
							this.items[0].text	 		= 'Modificar';
							this.items[0].iconPosition = 'left';
							return 'icoConfigurar';
									
						},
						handler: function(grid, rowIndex, colIndex) {
									  
							var record 		= Ext.StoreMgr.key('detalleTasasPorEpoDataStore').getAt(rowIndex);
							var respuesta 	= new Object();
							respuesta['TASA_BASE'] 						= record.json['TASA_BASE']; 						// 'nombre_tbase'
							respuesta['INTERMEDIARIO_FINANCIERO']	= record.json['INTERMEDIARIO_FINANCIERO']; 	// 'nombre_if'
							respuesta['FECHA_TASA_BASE']				= record.json['FECHA_TASA_BASE']; 				// 'fecha_tbase'
							respuesta['IC_IF']							= record.json['IC_IF']; 							// 'n_if'
							respuesta['IC_EPO']							= record.json['IC_EPO']; 							// 'n_epo'
							respuesta['VOBO_EPO']						= record.json['VOBO_EPO']; 						// 'voboEpo'
							respuesta['VOBO_IF']							= record.json['VOBO_IF']; 							// 'voboIf'
							respuesta['VOBO_NAFIN']						= record.json['VOBO_NAFIN']; 						// 'voboNafin'
							respuesta['IC_MONEDA']						= record.json['IC_MONEDA']; 						// 'icmoneda'
							respuesta['RANGO_PLAZO']					= record.json['RANGO_PLAZO']; 					// 'rplazo'
							respuesta['PLAZO_ESPECIFICO']				= record.json['PLAZO_ESPECIFICO']; 				// 'plazo_especifico'
							respuesta['VALOR_TASA']						= record.json['VALOR_TASA']; 						// 'vtasa'
							respuesta['RELACION_MATEMATICA']			= record.json['RELACION_MATEMATICA']; 			// 'rmtasa'
							respuesta['PUNTOS_ADICIONALES']			= record.json['PUNTOS_ADICIONALES']; 			// 'ptasa'
							respuesta['IC_PLAZO']						= record.json['IC_PLAZO']; 						// 'ic_plazo'
							respuesta['PLAZO_DIAS_INICIO']			= record.json['PLAZO_DIAS_INICIO'];				// 'plazo_dias_inicio'
							respuesta['PLAZO_DIAS_FIN']				= record.json['PLAZO_DIAS_FIN']; 				// 'plazo_dias_fin'
							respuesta['ES_RANGO_MAXIMO']				= record.json['ES_RANGO_MAXIMO'];				// 'es_rango_maximo'
							capturaTasas("MODIFICAR_TASA_BASE",respuesta); 
 
						}
								 	
					}
				]
			}
		],
		stateful:	false,
		tbar:			[
			// BOTON: AGREGAR RELACION IF
			{
				xtype:	'button',
				id:		'botonAgregarRelacionIF',
				text:  	'Agregar Relaci�n IF',
				iconCls:	'icoAgregarForma',
				handler: function(button,event){
					var respuesta = new Object();
					respuesta['claveEPO'] = Ext.getCmp("comboEPO").getValue();
					capturaTasas("CARGAR_GRID_DETALLE_IF_RELACIONAR",respuesta);
				}
			},
			// BOTON:  AGREGAR NUEVA TASA BASE
			{
				xtype:	'button',
				id:		'botonAgregarNuevaTasaBase',
				text:		'Agregar Nueva Tasa Base',
				iconCls:	'icoAgregarForma',
				handler:	function(button,event){
					var respuesta = new Object();
					respuesta['claveEPO'] = Ext.getCmp("comboEPO").getValue();
					capturaTasas("CARGAR_GRID_DETALLE_NUEVA_TASA_BASE",respuesta);
				}
			},			
			"->",
			"-",
			// COMBO IF
			{
				xtype: 'label',
				id: 	 'labelIFTasasPorEpo',
				html:  'IF:&nbsp;',
				style: 'padding-right:5pt;padding-left:5pt;',
				width: 30
			},
			{
				xtype: 				'combo',
				width:				300,
				id: 					'comboIFTasasPorEpo',
				//id: 					'comboIF',
				fieldLabel: 		'IF',
				allowBlank: 		true,
				mode: 				'local', 
				displayField: 		'descripcion',
				valueField: 		'clave',
				hiddenName: 		'claveIF',
				emptyText:			'Seleccione...',
				//emptyText: 			'TODOS',
				//emptyClass:			'',
				forceSelection: 	true,
				triggerAction: 	'all',
				typeAhead: 			true,
				minChars: 			1,
				store: 				catalogoIFData,
				//tpl: 					NE.util.templateMensajeCargaCombo
				tpl:					'<tpl for=".">' +
										'<tpl if="!Ext.isEmpty(loadMsg)">'+
										'<div class="loading-indicator">{loadMsg}</div>'+
										'</tpl>'+
										'<tpl if="Ext.isEmpty(loadMsg)">'+
										'<div class="x-combo-list-item">' +
										'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
										'</div></tpl></tpl>',
				//, anchor:				'-20',
				listeners: {
					collapse: function( comboIFTasasPorEpo ){
						
						var catalogoIFData 	= comboIFTasasPorEpo.getStore();
						
						var respuesta 			= new Object();
						// Obtener clave del IF seleccionado
						respuesta['claveIF'] = comboIFTasasPorEpo.getValue();
						// Obtener clave y descripcion del IF seleccionado
						var ifSeleccionado	= new Array();
						catalogoIFData.each( function(record){
							if( respuesta['claveIF'] === record.data['clave'] ){
								ifSeleccionado.push(record.json);
								return false;
							}
						}); 
						respuesta['ifSeleccionado'] = ifSeleccionado;
						// Realizar consulta
						capturaTasas("CONSULTA_DETALLE_TASAS_POR_EPO_DATA",respuesta);
						
					}
				}
			}
			
		]
	};
				
	//------------------------ FORMA PARAMETRIZACION EPOS --------------------------
	
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 			'catalogoEPODataStore',
		root: 		'registros',
		fields: 		[ 'clave', 'descripcion', 'loadMsg' ],
		url: 			'13forma02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty: 'total',
		autoLoad: 		false,
		autoDestroy: 	true,
		listeners: {
			load: function(store,records,options){
					
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
									
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("comboEPO").setValue(defaultValue);
						Ext.getCmp("comboEPO").originalValue = defaultValue;
					}
					//Ext.getCmp("comboEPO").setReadOnly(true);
								
				} 
									
			},
			exception: 	NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
 
	var elementosPanelFormaTasasBasePorEPO = [
		// COMBO EPO
		{
			xtype: 				'combo',
			name: 				'epo',
			id: 					'comboEPO',
			fieldLabel: 		'EPO',
			allowBlank: 		false,
			blankText:			'Para realizar esta operaci�n, es necesario seleccionar una EPO.',
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'claveEPO',
			emptyText: 			'Seleccionar...', 
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoEPOData,
			//tpl: 					NE.util.templateMensajeCargaCombo,
			tpl:					'<tpl for=".">' +
									'<tpl if="!Ext.isEmpty(loadMsg)">'+
									'<div class="loading-indicator">{loadMsg}</div>'+
									'</tpl>'+
									'<tpl if="Ext.isEmpty(loadMsg)">'+
									'<div class="x-combo-list-item">' +
									'<div style="white-space: normal; word-wrap:break-word; ">{descripcion}</div>' +
									'</div></tpl></tpl>',
			anchor:				'-20'
		}
	];                                                  
	
	var panelFormaTasasBasePorEPO = {
		xtype:			'form',
		id: 				'panelFormaTasasBasePorEPO',
		hidden:			false,
		width: 			725,
		title: 			'Consultar Tasas Base por EPO',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	35,
		labelAlign: 	'right',
		defaultType: 	'textfield',
		items: 			elementosPanelFormaTasasBasePorEPO,
		monitorValid: 	false,
		buttons: [
			// BOTON BUSCAR
			{
				id:				'botonConsultar',
				text: 			'Consultar',
				hidden: 			false,
				iconCls: 		'icoBuscar',
				handler: 		function() {
					
					// Si la forma no es invalida, suspender operaci�n
					var panelFormaTasasBasePorEPO = Ext.getCmp("panelFormaTasasBasePorEPO");
					// Si hay error en la forma, suspender la consulta
					if( !panelFormaTasasBasePorEPO.getForm().isValid() ){
						return;
					}
					// Consulta Tasa Base por EPO
					var respuesta 						= panelFormaTasasBasePorEPO.getForm().getValues();
					capturaTasas("CONSULTA_TASA_BASE_POR_EPO", respuesta);
				}
			},
			// BOTON LIMPIAR
			{
				text: 		'Limpiar',
				hidden: 		false,
				iconCls: 	'icoLimpiar',
				handler: 	function() {
					Ext.getCmp("panelFormaTasasBasePorEPO").getForm().reset();
					Ext.getCmp("gridDetalleTasasPorEpo").hide();
				}
			}
		]
	};	
 
	//------------------------ PANEL MODO PANTALLA --------------------------
	
	var panelModoPantalla = {
           xtype:     	'panel',
           frame:     	false,
           border:    	false,
           bodyStyle: 	'background:transparent;',
           layout:    	'hbox',
           style: 		'margin: 0 auto',
           width:			650,
           height: 		75,
           layoutConfig: {
              align: 	'middle',
              pack: 		'center'
           },
           items: [
				  {	
					  xtype: 	'button',
					  text: 	   'Parametrizaci�n',
					  width: 	126,
					  id: 		'botonPantallaParametrizacion',
					  handler: function(){
					  	  var respuesta = new Object();
					  	  respuesta['destino']   			= "13ParametrizacionPuntosLimite01ext.jsp";
					  	  capturaTasas("FIN",respuesta); 
					  }
				  },
				  {
					  xtype: 	'box',
					  width: 	8
				  },
				  {
					  xtype: 	'button',
					  text: 	   'Captura',
					  width: 	126,
					  pressed: 	true,
					  id: 		'botonPantallaCaptura'
				  }
           ]
        };
        
	//-------------------------------- PRINCIPAL -----------------------------------
   
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		disabled: false,
		items: 	[
			panelModoPantalla,
			panelFormaTasasBasePorEPO,
			NE.util.getEspaciador(10),
			gridDetalleTasasPorEpo,
			gridDetalleIFRelacionar,
			gridDetalleNuevaTasaBase
		],
		ocultaMascaras: function(){
			
			// Suprimir mascara del Panel Forma Tasas Base Por EPO
			var element = Ext.getCmp("panelFormaTasasBasePorEPO").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Suprimir mascara de la Forma Modificar Tasa Base
			element     = Ext.getCmp("formaModificarTasaBase").getEl();
			if( element && element.isMasked()){
				element.unmask();
			}	
			
			element     = Ext.getCmp("gridDetalleIFRelacionar").getGridEl();
			if( element && element.isMasked()){
				element.unmask();
			}	
			
			element     = Ext.getCmp("gridDetalleIFRelacionar").getEl();
			if( element && element.isMasked()){
				element.unmask();
			}	
 
			// Derefenrencia ultimo elemento...
			element = null;
 
		}
	});
 			
	//------------------------ ACCIONES DE INICIALIZACION --------------------------
	
	//Peticion para realizar la inicializacion de la pantalla.
	var respuesta 						= new Object();
	capturaTasas("INICIALIZACION", respuesta);
	
	// SECCION DE CODIGO VOLATIL
	/*
	var dynamicData = {
		registros: [
			{ 
				GROUP_ID: 1, 
				IC_IF: 100,
				IC_MONEDA: 1,
				INTERMEDIARIO_FINANCIERO: 'NACIONAL FINANCIERA 1ER PISO',
				MONEDA: 'MONEDA NACIONAL',
				TIPO_TASA: 'TIIE',
				PLAZO: '1-60',
				VALOR: 2.000000,
				RELACION_MATEMATICA: '*',
				PUNTOS_ADICIONALES: '1',
				TASA_A_APLICAR: 2.00000
			},
			{ 
				GROUP_ID: 1, 
				IC_IF: 100,
				IC_MONEDA: 1,
				INTERMEDIARIO_FINANCIERO: 'NACIONAL FINANCIERA 1ER PISO',
				MONEDA: 'MONEDA NACIONAL',
				TIPO_TASA: 'TIIE 91 DIAS',
				PLAZO: '61-120',
				VALOR: 2.000000,
				RELACION_MATEMATICA: '*',
				PUNTOS_ADICIONALES: '2',
				TASA_A_APLICAR: 4.00000
			},
			{ 
				GROUP_ID: 2, 
				IC_IF: 100,
				IC_MONEDA: 54,
				INTERMEDIARIO_FINANCIERO: 'NACIONAL FINANCIERA 1ER PISO',
				MONEDA: 'DOLAR AMERICANO',
				TIPO_TASA: 'LIBOR 3 MESES (DIARIA)',
				PLAZO: '121-180',
				VALOR: 2.000000,
				RELACION_MATEMATICA: '+',
				PUNTOS_ADICIONALES: '4',
				TASA_A_APLICAR: 6.00000
			},
			{ 
				GROUP_ID: 2, 
				IC_IF: 100,
				IC_MONEDA: 54,
				INTERMEDIARIO_FINANCIERO: 'NACIONAL FINANCIERA 1ER PISO',
				MONEDA: 'DOLAR AMERICANO',
				TIPO_TASA: 'LIBOR 1 MES (DIARIA)',
				PLAZO: '1-120',
				VALOR: 2.000000,
				RELACION_MATEMATICA: '+',
				PUNTOS_ADICIONALES: '3',
				TASA_A_APLICAR: 5.00000
			},
			{ 
				GROUP_ID: 3, 
				IC_IF: 101,
				IC_MONEDA: 1,
				INTERMEDIARIO_FINANCIERO: 'NACIONAL FINANCIERA 1ER PISO',
				MONEDA: 'MONEDA NACIONAL',
				TIPO_TASA: 'TIIE',
				PLAZO: '1-60',
				VALOR: 2.000000,
				RELACION_MATEMATICA: '*',
				PUNTOS_ADICIONALES: '1',
				TASA_A_APLICAR: 2.00000
			},
			{ 
				GROUP_ID: 3, 
				IC_IF: 101,
				IC_MONEDA: 1,
				INTERMEDIARIO_FINANCIERO: 'NACIONAL FINANCIERA 1ER PISO',
				MONEDA: 'MONEDA NACIONAL',
				TIPO_TASA: 'TIIE 91 DIAS',
				PLAZO: '61-120',
				VALOR: 2.000000,
				RELACION_MATEMATICA: '*',
				PUNTOS_ADICIONALES: '2',
				TASA_A_APLICAR: 4.00000
			}
		],
		total: 4
	};		
	
	Ext.StoreMgr.key('detalleIFRelacionarDataStore').loadData(dynamicData);
	*/
	
});