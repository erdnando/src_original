<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		javax.naming.Context,
		java.io.BufferedReader,
		java.io.InputStreamReader,
		java.io.OutputStreamWriter,
		java.io.FileOutputStream,
		java.io.BufferedWriter,
		java.io.FileInputStream,
		com.netro.exception.*, 
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.seguridadbean.*,
		com.netro.garantias.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.cadenas.*,
		com.netro.dispersion.*,
		com.netro.afiliacion.*,
		com.netro.descuento.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CargaArchivo.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
 
	if(!hayAviso){
		
		String reinstalacionesLayout = 
			"<table width=\"100%\" >"  +
			"<tr>"  +
			"	<td class=\"titulos\" align=\"center\">"  +
			"		<span class=\"titulos\">"  +
			"			Estructura de Archivo<br>Carga masiva"  +
			"		</span>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td colspan=\"2\" align=\"center\" >"  +
			"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" >"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" colspan=\"10\"  style=\"height:30px;\" >"  +
			"					Layout de Solicitudes SIRAC - NAFINET<br>"  +
			"					Archivo de extensi&oacute;n TXT; los campos deben de estar separados por pipes (\"|\")"  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"celda01\" align=\"center\" width=\"25px\"  style=\"height:30px;\" >No.</td>"  +
			"				<td class=\"celda01\" align=\"center\">Nombre del Campo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Tipo</td>"  +
			"				<td class=\"celda01\" align=\"center\">Longitud<br>M&aacute;xima</td>"  +
			"				<td class=\"celda01\" align=\"center\">Obligatorio</td>"  +
			"				<td class=\"celda01\" align=\"center\">Observaciones</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >1</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Folio "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">CHAR</td>"  +
			"				<td class=\"formas\" align=\"center\">15</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					El Folio de la Solicitud. "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >2</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					N&uacute;mero de Pr&eacute;stamo "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">NUMBER</td>"  +
			"				<td class=\"formas\" align=\"center\">8</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					&nbsp; "  +
			"				</td>"  +
			"			</tr>"  +
			"			<tr>"  +
			"				<td class=\"formas\" align=\"center\" style=\"height:30px;\" >3</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Fecha de Operaci&oacute;n "  +
			"				</td>"  +
			"				<td class=\"formas\" align=\"center\">DATE</td>"  +
			"				<td class=\"formas\" align=\"center\">8</td>"  +
			"				<td class=\"formas\" align=\"center\">S&iacute;</td>"  +
			"				<td class=\"formas\" align=\"center\">"  +
			"					Fecha con formato: DD/MM/AAAA. "  +
			"				</td>"  +
			"			</tr>"  +
			"		</table>"  +
			"	</td>"  +
			"</tr>"  +
			"<tr>"  +
			"	<td>&nbsp;</td>"  +
			"</tr>"  +
			"</table>";	
		resultado.put("reinstalacionesLayout", 	reinstalacionesLayout			);
		
	}
	
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();
 
} else if (        	informacion.equals("CargaArchivo.subirArchivo") 				)	{	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
 
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 2097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 2 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	
	// Guardar Archivo en Disco
	int numFiles = 0;
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," ")	 );
		resultado.put("fileName", 	myFile.getFileName().replaceAll("\\s+"," ")			);

	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
 
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 		"REALIZAR_VALIDACION" 	);
	// Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacionEInsercion") 				)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;

	String 		fileName 	= (request.getParameter("fileName") == null)?"":request.getParameter("fileName");
	String 		rutaArchivo = strDirectorioTemp + iNoUsuario + "." + fileName	;
	
	// Obtener instancia del EJB de ProcesoSNSolicitud
	ProcesoSNSolicitud beanProcesos = null;
	try {
				
		beanProcesos = ServiceLocator.getInstance().lookup("ProcesoSNSolicitudEJB", ProcesoSNSolicitud.class);
			
	} catch(Exception e) {
	 
		log.error("CargaArchivo.realizarValidacionEInsercion(Exception): Obtener instancia del EJB de ProcesoSNSolicitud");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de ProcesoSNSolicitud.");
		
	}
		
	// Preprocesar Archivo
	int 				ctaLineas		= 0;
	java.io.File 	lfArchivo		= new java.io.File(rutaArchivo);
	BufferedReader br					= new BufferedReader(new InputStreamReader(new FileInputStream(lfArchivo)));
	String 			lsLinea 			= "";
	StringBuffer 	lsbDocumentos 	= new StringBuffer();
	while( (lsLinea = br.readLine()) != null ) {
		lsLinea = lsLinea.replace('\'',' ');
		lsbDocumentos.append(lsLinea.trim()+"\n");
		ctaLineas++;
	}//while linea
	
	// Realizar validacion/inserción de solicitudes
	Hashtable	lhResultados 	= beanProcesos.ohprocesarSolicitudes(lsbDocumentos.toString());
	boolean 		lbOk 				= new Boolean(lhResultados.get("lbOk").toString()).booleanValue();

	// Leer Resultado de la Validacion
	String 		totalRegistros	= String.valueOf(ctaLineas);
	
	//  Leer Resultado de la Validacion ( Registros sin Errores )
	JSONArray		registrosSinErroresDataArray	= new JSONArray();
	String 			registrosSinErrores 				= "0";
	String 			numeroRegistrosSinErrores		= "0";

	if( lbOk ){
		
		int 	 ctaRegistros 		= 0;
		
		// Folio
		Vector lvFolio 			= (Vector) lhResultados.get("lvFolio");
		// Número de Prestamo
		Vector lvNumeroPrestamo = (Vector) lhResultados.get("lvNumeroPrestamo");
		// Fecha de Operación
		Vector lvFecha 			= (Vector) lhResultados.get("lvFecha");
		
		for (int i=0; i<lvFolio.size(); i++) {
 
			JSONArray registro = new JSONArray();
			
			// Agregar número de línea
			registro.add(String.valueOf(++ctaRegistros)		);
			
			// Folio
			registro.add(lvFolio.get(i).toString()				);
			// Numero de Prestamo
			registro.add(lvNumeroPrestamo.get(i).toString()	);
			// Fecha de Operación
			registro.add(lvFecha.get(i).toString()				);
			
			registrosSinErroresDataArray.add(registro);
			
		}
		
		// Total de Solicitudes exitosas recibidas:
		registrosSinErrores 			= String.valueOf(lvFolio.size());
		numeroRegistrosSinErrores	= Comunes.formatoDecimal(lvFolio.size(),0);
		
	}
	
	//  Leer Resultado de la Validacion ( Registros con Errores )
	JSONArray		registrosConErroresDataArray 	= new JSONArray();
	String 			registrosConErrores 				= "0";
	String 			numeroRegistrosConErrores		= "0";
 
	if( !lbOk ){
 
		StringBuffer	buffer			= new StringBuffer();
		int				cuentaMensajes	= 0;
		char 				lastChar			= 0;
		char 				previousChar	= 0;
			
		registrosConErrores				= lhResultados.get("lsbError").toString();
		
		for(int i=0;i<registrosConErrores.length();i++){
				
			lastChar = registrosConErrores.charAt(i);
			if( 		  previousChar == '\n' && lastChar == '\n' ){
					
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(++cuentaMensajes));
				registro.add(buffer.toString());
				registrosConErroresDataArray.add(registro);
					
				buffer.setLength(0);
					
			} else if( previousChar != '\n' && lastChar == '\n' ){
				// No hacer nada
			} else if( previousChar == '\n' && lastChar != '\n' ){
					
				buffer.append(previousChar);
				buffer.append(lastChar);
					
			} else { // || previousChar != '\n' && lastChar != '\n' 
					
				buffer.append(lastChar);
					
			}
			previousChar = lastChar;
				
		}
		if(buffer.length() > 0){
				
			if( previousChar == '\n' ){
				buffer.append(previousChar);
			}
				
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(++cuentaMensajes));
			registro.add(buffer.toString());
			registrosConErroresDataArray.add(registro);
				
			buffer.setLength(0);
				
		}
			
		// Total de Solicitudes exitosas recibidas:
		registrosConErrores 			= "0";
		numeroRegistrosConErrores	= "0";
 
	}
 
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	JSONArray erroresVsCifrasControlDataArray 			= new JSONArray();
	// Determinar si se mostrará el boton continuar carga	
	Boolean 	 cargaExistosa 									= lbOk?new Boolean(true):new Boolean(false);
 
	// Enviar resultado general de la validación/inserción
	resultado.put("totalRegistros", 							totalRegistros							);
	
	resultado.put("registrosSinErroresDataArray",		registrosSinErroresDataArray		);
	resultado.put("numeroRegistrosSinErrores",			numeroRegistrosSinErrores			);
	
	resultado.put("registrosConErroresDataArray",		registrosConErroresDataArray		);
	resultado.put("numeroRegistrosConErrores",			numeroRegistrosConErrores			);
	
	resultado.put("erroresVsCifrasControlDataArray",	erroresVsCifrasControlDataArray	);
	resultado.put("cargaExistosa",							cargaExistosa							);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 						"ESPERAR_DECISION_VALIDACION_INSERCION" );
	// Enviar resultado de la operacion
	resultado.put("success", 									new Boolean(success)					);
 
	infoRegresar = resultado.toString();
 
} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	response.sendRedirect("13proceso04ext.jsp"); 
 
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 
log.debug("infoRegresar = <" + infoRegresar + ">"); 

%>
<%=infoRegresar%>