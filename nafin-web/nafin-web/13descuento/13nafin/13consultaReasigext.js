Ext.onReady(function() {

//----------------------DESCARGA ARCHIVO DE CONSULTA ----------------------------
	function procesarArchivoCons(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------DESCARGA Documento Autorizacion ----------------------------

	function procesarDocumentoAuto(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	var descargaDocumentoAuto = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var hidnombreArchivo = registro.get('NOMBRE_ARCHIVO');
		
			Ext.Ajax.request({
			url: '13consultaReasigArchivosext.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
			informacion: 'documentoAutorizacion',
			hidnombreArchivo:hidnombreArchivo
			}),
			callback: procesarDocumentoAuto
		});		
	}
	
//---------------------------CONSULTA ----------------------------

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
						
			if(store.getTotalCount() > 0) {			
				el.unmask();	
				Ext.getCmp('btnImprimir').enable();
			} else {		
				Ext.getCmp('btnImprimir').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13consultaReasig.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{	name: 'FECHA_REASIGNACION'},
			{	name: 'NOMBRE_PYME'},
			{	name: 'RFC_PYME'},
			{	name: 'DIRECCION_PYME'},
			{	name: 'COLONIA_PYME'},
			{	name: 'TELEFONO_PYME'},
			{	name: 'NOMBRE_EPO'},
			{	name: 'NOMBRE_PYME_DES'},
			{	name: 'RFC_PYME_DES'},
			{	name: 'DIRECCION_PYME_DES'},
			{	name: 'COLONIA_PYME_DES'},
			{	name: 'TELEFONO_PYME_DES'},
			{	name: 'NO_DOCUMENTO'},
			{	name: 'FECHA_DOCUMENTO'},
			{	name: 'FECHA_VENCIMIENTO'},
			{	name: 'MONEDA'},
			{	name: 'TIPO_FACTORAJE'},
			{	name: 'MONTO_DOCUMENTO'},
			{	name: 'CAUSA_RECHAZO'},
			{	name: 'NOMBRE_ARCHIVO'}
			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: ' ', colspan: 1, align: 'center'},
				{header: 'PYME ORIGEN', colspan: 5, align: 'center'},
				{header: ' ', colspan: 1, align: 'center'},
				{header: 'PYME DESTINO', colspan: 5, align: 'center'},
				{header: ' ', colspan: 8, align: 'center'}
			]
		]
	});	
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Documentos Reasignados',
		clicksToEdit: 1,
		plugins: grupos,
		hidden: true,		
		columns: [	
			{
				header: 'Fecha Reasignaci�n',
				tooltip: 'Fecha Reasignaci�n',
				dataIndex: 'FECHA_REASIGNACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Nombre',
				tooltip: ' Nombre',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'RFC',
				tooltip: ' RFC',
				dataIndex: 'RFC_PYME',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Direcci�n',
				tooltip: 'Direcci�n',
				dataIndex: 'DIRECCION_PYME',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: ' Colonia',
				tooltip: 'Colonia',
				dataIndex: 'COLONIA_PYME',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: ' Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO_PYME',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: ' Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Nombre',
				tooltip: ' Nombre',
				dataIndex: 'NOMBRE_PYME_DES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'RFC',
				tooltip: ' RFC',
				dataIndex: 'RFC_PYME_DES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Direcci�n',
				tooltip: 'Direcci�n',
				dataIndex: 'DIRECCION_PYME_DES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Colonia',
				tooltip: 'Colonia',
				dataIndex: 'COLONIA_PYME_DES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Tel�fono',
				tooltip: 'Tel�fono',
				dataIndex: 'TELEFONO_PYME_DES',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'No. Documento',
				tooltip: 'No. Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Documento',
				tooltip: 'Fecha Documento',
				dataIndex: 'FECHA_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Causas de Rechazo',
				tooltip: 'Causas de Rechazo',
				dataIndex: 'CAUSA_RECHAZO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				xtype: 'actioncolumn',
				header: 'Documento Autorizaci�n',
				tooltip: 'Documento Autorizaci�n',
				dataIndex: 'NOMBRE_ARCHIVO',
				 width: 150,
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('NOMBRE_ARCHIVO') =='N' ){
						return 'No Existen Documentos';
					}
				},
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('NOMBRE_ARCHIVO') !='N' ){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';										
							}
						}
						,handler: descargaDocumentoAuto
					}
				]				
			}			
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir',
					iconCls: 'icoPdf',
					id: 'btnImprimir',
					handler: function(boton, evento) {
					
						Ext.Ajax.request({
							url: '13consultaReasigArchivosext.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'GeneraArchivo'							
							}),
							callback: procesarArchivoCons
						});
						
					}
				}				
			]
		}
	});
		
		
	//-----------------------CRITERIOS DE BUSQUEDA 	------------------------
		
	function procesaValoresIniciales(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
	
			Ext.getCmp("df_reasignacion_i").setValue(jsonValoresIniciales.fecha_Actual);
			Ext.getCmp("df_reasignacion_f").setValue(jsonValoresIniciales.fecha_Actual);
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consultaReasig.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var  elementosForma  = [	
		{
			xtype: 'combo',
			fieldLabel: 'EPO',
			name: 'epo',
			id: 'epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'epo',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			store: catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo
		},	
		{
			xtype: 'numberfield',
			fieldLabel: 'No. Electr�nico Proveedor',		 
			name: 'cg_no_electronico_prov',
			id: 'cg_no_electronico_prov1',
			allowBlank: true,			
			maxLength: 10,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'N�mero Proveedor',		
			name: 'cg_no_provedor',
			id: 'cg_no_provedor1',
			allowBlank: true,
			maxLength: 25,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'RFC',		
			name: 'cg_rfc',
			id: 'cg_rfc1',
			allowBlank: true,
			maxLength: 20,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'N�mero Documento',		
			name: 'cg_no_docto',
			id: 'cg_no_docto1',
			allowBlank: true,
			maxLength: 15,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},
		{
			xtype: 'textfield',
			fieldLabel: 'Usuario que reasigno',		
			name: 'cg_usuario_reasigno',
			id: 'cg_usuario_reasigno1',
			allowBlank: true,
			maxLength: 15,
			width: 100,
			msgTarget: 'side',
			margins: '0 20 0 0'
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Carga del Archivo',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_reasignacion_i',
					id: 'df_reasignacion_i',
					allowBlank: true,				
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					minValue: '01/01/1901',
					campoFinFecha: 'df_reasignacion_f',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_reasignacion_f',
					id: 'df_reasignacion_f',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					minValue: '01/01/1901',
					campoInicioFecha: 'df_reasignacion_i',
					margins: '0 20 0 0' 
				}
			]
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de Busqueda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {			
				
				
					var df_reasignacion_i = Ext.getCmp('df_reasignacion_i');
					var df_reasignacion_f = Ext.getCmp('df_reasignacion_f');
					
					if(Ext.isEmpty(df_reasignacion_i.getValue()) || Ext.isEmpty(df_reasignacion_f.getValue())){
						if(Ext.isEmpty(df_reasignacion_i.getValue())){
							df_reasignacion_i.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_reasignacion_i.focus();
							return;
						}
						if(Ext.isEmpty(df_reasignacion_f.getValue())){
							df_reasignacion_f.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_reasignacion_f.focus();
							return;
						}
					}
					
					var df_reasignacion_i_ = Ext.util.Format.date(df_reasignacion_i.getValue(),'d/m/Y');
					var df_reasignacion_f_ = Ext.util.Format.date(df_reasignacion_f.getValue(),'d/m/Y');
					
					
					if(!Ext.isEmpty(df_reasignacion_i.getValue())   ||   !Ext.isEmpty(df_reasignacion_f.getValue())     ){
						if(!isdate(df_reasignacion_i_)) { 
							df_reasignacion_i.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_reasignacion_i.focus();
							return;
						}else  if(!isdate(df_reasignacion_f_)) { 
							df_reasignacion_f.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_reasignacion_f.focus();
							return;
						}
						
						if(datecomp(df_reasignacion_i_,df_reasignacion_f_)==1) {
							df_reasignacion_i.markInvalid("La fecha de este campo debe de ser anterior a."+df_reasignacion_f_);
							df_reasignacion_i.focus();
							return;
						}
						
						
					}
					

					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'						
						})
					});										
				}
			},
			{
				text: 'Limpiar',
				id: 'limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consultaReasigext.jsp';					
				}
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),			
			fp,			
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});


	catalogoEPO.load();
	
	Ext.Ajax.request({
		url: '13consultaReasig.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});
	
});	