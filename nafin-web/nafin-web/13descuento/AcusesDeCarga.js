Ext.namespace('NE.AcusesDeCarga');

	var vent;

	var objGral={
		acuse: null
	}

//--------------------------------- HANDLERS -------------------------------
	NE.AcusesDeCarga.VisorAcusesDeCarga = function (acuse){

		objGral.acuse = acuse;

		if(!vent){
			vent = new Ext.Window({
				width:       300,
				id:          'ventanaDescargaArchivos',
				closeAction: 'hide',
				layout:      'fit',
				plain:       true,
				modal:       true,
				resizable:   false,
				closable:    true,
				autoDestroy: false,
				items: [
					NE.AcusesDeCarga.gridConsulta
				]
			})
		}
		vent.show().setTitle('Acuses de carga');

		consultaData.load({
			params:{
				informacion: 'Consultar',
				acuse:       acuse
			}
		});

	}


	Ext.reg('VisorAcusesDeCarga', NE.AcusesDeCarga.VisorAcusesDeCarga);


	//Realizo la consulta para obtener el archivo seleccionado en el grid
	function getArchivo(rec){

		var gridConsulta = Ext.getCmp('NE.AcusesDeCarga.gridConsulta');
		var el = gridConsulta.getGridEl();
		el.mask('Procesando...', 'x-mask-loading');

		Ext.Ajax.request({
			url: NE.appWebContextRoot+'/13descuento/AcusesDeCarga.data.jsp',
			params: {
				informacion:  'Descarga_Archivo',
				acuse:        rec.get('IC_ACUSE'),
				tipo_archivo: rec.get('TIPO_ARCHIVO')
			},
			callback: procesarDescargaArchivos
		});

	}


	//Descarga el archivo seleccionado en el grid
	function procesarDescargaArchivos(opts, success, response){

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){

			var infoR    = Ext.util.JSON.decode(response.responseText);
			var archivo  = infoR.urlArchivo;
			archivo      = archivo.replace('/nafin','');
			var params   = {nombreArchivo: archivo};
			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			forma.method = 'post';
			forma.target = '_self'; 
			forma.submit();

		} else{
			if(Ext.util.JSON.decode(response.responseText).mensaje == ''){
				NE.util.mostrarConnError(response,opts);
			} else{
				Ext.Msg.alert('Mensaje', Ext.util.JSON.decode(response.responseText).mensaje);
			}
			
		}

		var gridConsulta = Ext.getCmp('NE.AcusesDeCarga.gridConsulta');
		var el = gridConsulta.getGridEl();
		el.unmask();

	}

	//Muestra el grid despues de realizar la consulta
	var procesarConsultaData = function(store,arrRegistros,opts){
		if (arrRegistros != null){
			var jsonData = store.reader.jsonData;
			var gridConsulta = Ext.getCmp('NE.AcusesDeCarga.gridConsulta');
			var el = gridConsulta.getGridEl();

			gridConsulta.show();
			gridConsulta.setTitle('No. Acuse: ' + objGral.acuse);

			if(store.getTotalCount() > 0){
				var jsonData = store.reader.jsonData;
				var cm = gridConsulta.getColumnModel();
				el.unmask();
			} else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	//Esta funci�n es para que pueda mostrar el valor del registro junto con el �cono
	function renderDato(value, metaData, record, rowIndex, colIndex, store){
		return value + '&nbsp;';
	}


//-------------------------------- STORES -----------------------------------
	var consultaData = new Ext.data.JsonStore({ 
		root: 'registros',
		url:  NE.appWebContextRoot+'/13descuento/AcusesDeCarga.data.jsp',
		baseParams:{
			informacion:  'Consultar'
		},
		fields: [
			{name: 'IC_ACUSE'      },
			{name: 'NOMBRE_ARCHIVO'},
			{name: 'TIPO_ARCHIVO'  }
		],
		totalProperty:   'total',
		messageProperty: 'msg',
		autoLoad:        false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args){
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
	});


//------------------------------ COMPONENTES----------------------------------
	NE.AcusesDeCarga.gridConsulta = {
		width:        285,
		height:       120,
		store:        consultaData,
		xtype:        'editorgrid',
		id:           'NE.AcusesDeCarga.gridConsulta',
		title:        '&nbsp;',
		align:        'center',
		style:        'margin:0 auto;',
		frame:        false,
		loadMask:     true,
		hidden:       false,
		hideHeaders:  true,
		columns: [{
			width:     280,
			xtype:     'actioncolumn',
			dataIndex: 'NOMBRE_ARCHIVO',
			align:     'left',
			sortable:  false,
			resizable: false,
			renderer:  renderDato,
			items: [{
				getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
					if(registro.get('TIPO_ARCHIVO') == 'PDF' ){
						this.items[0].tooltip = 'Descargar archivo PDF';
						return 'icoPdf';		
					} else if(registro.get('TIPO_ARCHIVO') == 'CSV' ){
						this.items[0].tooltip = 'Descargar archivo CSV';
						return 'icoXls';		
					} else if(registro.get('TIPO_ARCHIVO') == 'XML' ){
						this.items[0].tooltip = 'Descargar Archivo XML';
						return 'icoWhiteCode';		
					}
				},
				handler: function(gridConsulta, rowIndex, colIndex){
					var rec = consultaData.getAt(rowIndex);
					getArchivo(rec);
				}
			}]
		}]
	}