Ext.onReady(function() {
	var cveEpoOriginal= '';
	var comboEpoVacio= false;
	var cveIFtmp;
	
	var strUsuario = Ext.getDom('hidStrUsuario').value;
	var hidCboEpo = Ext.getDom('hidCboEpo').value;
	var cvePyme = Ext.getDom('cvePyme').value;
	var strNePymeAsigna = Ext.getDom('strNePymeAsigna').value;
	var strNombrePymeAsigna = Ext.getDom('strNombrePymeAsigna').value;
	var hidCboMoneda = Ext.getDom('hidCboMoneda').value;
	var hidTxtFechaVencDe = Ext.getDom('hidTxtFechaVencDe').value;
	var hidTxtFechaVenca = Ext.getDom('hidTxtFechaVenca').value;
	var strNombrePymeAsigna = Ext.getDom('strNombrePymeAsigna').value;
	var strUsr = Ext.getDom('hidStrUsr').value;
//	var envia = Ext.getDom('envia').value;
	
	
	var Inicializacion = {
		catalogoMoneda : false,
		catalogoEpo: false
	};
	
	var objCalc={
		totalDescuento:0,
		totalMonto:0,
		totalDescuento:0,
		totalIntereses:0,
		totalRecibir:0,
		totaldocs:0,
		tipoCambio: null
	}
	
	var objGral={
		inicial:'S',
		strTipoUsuario:null,
		//pymeBloq:null,
		//tipo_servicio:null,
		iNoEPO:null,
		//urlCapturaDatos:null,
		//fecDiaSigHabil:null,
		//msgDiaSigHabil:null,
		//msgOperFact24hrs:null,
		//msgFecVencPyme:null,
		//msgCboIf:null,
		valorTC: null,
		//paramEpoPef: null,
		//numCamposDinamicos: null,
		//mapNombres:null,
		//operaNotasDeCredito: null,
		//aplicarNotasDeCreditoAVariosDoctos: null,
		//icDoctosNotas: null,
		//icNotasAp: null,
		//bancoDeFondeo: null,
		//limites: null,
		sTipoLimite: null,
		monto: null,
		descuento: null,
		interes: null,
		recibir: null,
		valEPO: null,
		valIF: null,
		//sFecVencLineaCred: 'N',
		msgError: null,
		consultaIni:'S'
	}

//--------------------------------------------------------------------------------
var fnTransmitirDerechoCallback = function(vpkcs7, vtextoFirmar, vokResp, btn){
	if (Ext.isEmpty(vpkcs7)) {
		btn.enable();
		return;	//Error en la firma. Termina...
	}else{
		realizaConfirmacionNAFIN(vokResp, vpkcs7, vtextoFirmar);
	}
}

var fnTransmitirDerecho = function(btn, certificado){
		if((strUsuario=='PYME' || strUsuario=='NAFIN') && certificado!=true){
			var win = new NE.cesion.WinCesionDerechos({
				getResultValid:resulValidCesion,
				cvePerfProt: 'PPYMAUTOR',
				cveFacultad: '13PYME13AUTORIZA',
				strUsuario: strUsr,
				tipoUsuario: strUsuario
			}).show();
			win.el.dom.scrollIntoView();
			
		}else{
			btn.disable();
			var infoTotal = '';
			storeTotalData.each(function(record) {
				infoTotal=infoTotal+
							record.data['EPO']+'|'+
							record.data['TOTALMONTO']+'|'+
							record.data['TOTALDESCTO']+'|'+
							record.data['TOTALINTERES']+'|'+
							record.data['TOTALIMPORT']+'|\n';
			});
		
			var textoFirmar = "E P O-DATOS|Total Monto Documento|Total Monto Descuento|Total Monto de Inter�s|Total Importe a Recibir\n"+ infoTotal +
			"Al solicitar el factoraje electr�nico o descuento electr�nico del documento que selecciono e identifico, transmito los derechos"+ 
			" que sobre el mismo ejerzo, de acuerdo con lo establecido por los art�culos 427 de la Ley General de T�tulos y Operaciones de Cr�dito,"+ 
			" 32 c del C�digo Fiscal de la Federaci�n y 2038 del C�digo Civil Federal, haci�ndome sabedor de su contenido y alcance, condicionado a"+
			" que se efect�e el descuento electr�nico o el factoraje electr�nico";
			
			NE.util.obtenerPKCS7(fnTransmitirDerechoCallback, textoFirmar, 'S', btn);
			/*var pkcs7 = NE.util.firmar(textoFirmar);
			if (Ext.isEmpty(pkcs7)) {
				btn.enable();
				return;	//Error en la firma. Termina...
			}else{
				realizaConfirmacionNAFIN('S', pkcs7,textoFirmar);
			}*/
		
		}
		
	}

var procesarSuccessValoresIni = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);
			objGral.inicial = 'N'
         objGral.icgrupo = resp.icgrupo;
			objGral.strTipoUsuario = resp.strTipoUsuario;
			objGral.iNoEPO = resp.iNoEPO;
			
			if( !resp.bloqueoPymeEpo ){
				fp.show();
			
				if(resp.msgError==''){
					var valorInicialCboEpo = Ext.getDom("hidCboEpo").value;
					var cboEpo = Ext.getCmp('cboEpo1');
	
					if(cboEpo.getValue()=='' && valorInicialCboEpo == ''){
						if (storeCatEpoData.findExact("clave", objGral.iNoEPO) != -1 ) {
							cboEpo.setValue(objGral.iNoEPO);
						}
						Inicializacion.catalogoEpo = true;
					} else if (valorInicialCboEpo != ''){
						if (storeCatEpoData.findExact("clave", valorInicialCboEpo) != -1 ) {
							cboEpo.setValue(valorInicialCboEpo);
						}
						Inicializacion.catalogoEpo = true;
						if(NE.util.allTrue(Inicializacion)) {
							//Si cuando llega aqui ya estan inicializados los valores iniciales necesarios,
							//realiza la consulta. Esta secci�n solo aplica cuando la consulta recibe parametros iniciales
							consultar();
						}
					}

					
					if(resp.msgError!=''){
						var objMsg = Ext.getCmp('mensajes1');
						objMsg.body.update(resp.msgError);
						objMsg.show();
						fp.hide();
					}else{
						fp.show();
						Ext.getCmp('cfFechaVenc').doLayout();
					}
					
				}else{
					var objMsg = Ext.getCmp('mensajes1');
					
					if(resp.msgError!=''){
						objMsg.body.update(resp.msgError);
					}else{
						objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
								'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
								'Cd. De M�xico 50-89-61-07. <br>'+
								'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
					}
										
					objMsg.show();
					fp.hide();
				}
			}else{
				fp.hide();
				var objMsg = Ext.getCmp('mensajes1');
				objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
							'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
							'Cd. De M�xico 50-89-61-07. <br>'+
							'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
				objMsg.show();
				
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var consultar = function(boton, evento) {
		
		var cboEpo = Ext.getCmp('cboEpo1');
		var txtFechaVencDe = Ext.getCmp('txtFechaVencDe');
		var txtFechaVenca = Ext.getCmp('txtFechaVenca');
	
		var record; 
		var valEPO; 
		
		if(txtFechaVencDe.getValue()!='' || txtFechaVenca.getValue()!=''){
			if(txtFechaVencDe.getValue()==''){
				txtFechaVencDe.markInvalid('Ingrese fecha inicial');
				return;
			}else if (txtFechaVenca.getValue()==''){
				txtFechaVenca.markInvalid('Ingrese fecha final');
				return;
			}
		}
		
		pnl.el.mask('Consultando...', 'x-mask-loading');
		if(comboEpoVacio){
			valEPO = cveEpoOriginal+'|'+'SI ALGO MAS';
		}else{
			record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
			valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
		}
		
		objGral.valEPO = valEPO;
		
		Ext.Ajax.request({
			url: '13forma10ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'GeneraInfoCons',
				cboEpo: valEPO,
				cvePyme: Ext.getDom("cvePyme").value,
				cveEpoOriginal: cveEpoOriginal
			}),
			callback: procesarSuccessValoresGrales
		});  
	}
	
	var procesarSuccessValoresGrales = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			
			if(!resp.horarioValido){
				var panelTotales = Ext.getCmp('panelTotales1');
				if(objMsg)
					objMsg.hide();
				if(panelTotales)
					panelTotales.hide();
				
				var objMsg = Ext.getCmp('mensajes1');
				objMsg.body.update("Servicio No Disponible por el momento. Intente mas tarde");
				objMsg.show();
				fp.hide();
				pnl.el.unmask();
			}else{
				objGral.strTipoUsuario = resp.strTipoUsuario;
				objGral.msgError = resp.msgError;
				
				objGral.valorTC = resp.valorTC;
				objGral.sTipoLimite = resp.sTipoLimite;
				objGral.noTasasAceptada = resp.noTasasAceptada;
				
				//se asignan datos al objeto creado para el calculo de montos
				objCalc.totalMonto = 0;
				objCalc.totalDescuento = 0;
				objCalc.totalIntereses = 0;
				objCalc.totalRecibir = 0;
				objCalc.totaldocs = 0;
				objCalc.tipoCambio = parseFloat(objGral.valorTC,10);
				objGral.consultaIni = 'N';
				
				//var cboIf = Ext.getCmp('cboIf1');
				
				storeCatIfData.loadData(resp)
				storeDoctosData.loadData(resp);
			}
			

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		pnl.el.unmask();
		
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridColumnMod = grid.getColumnModel();
		var strTipoUsuario = objGral.strTipoUsuario;
		
		
		panelTotales.getForm().reset();
		
			grid.show();
			panelTotales.show();
			if (arrRegistros != null) {
				if (!grid.isVisible()) {
					contenedorPrincipalCmp.add(grid);
					contenedorPrincipalCmp.doLayout();
				}else{
					//selectDefaultDoctos(grid);
				}
				
				var el = grid.getGridEl();
				if(store.getTotalCount() > 0) {
					Ext.getCmp('btnTerminar').enable();
					
					if(objGral.msgError!=''){
							Ext.getCmp('btnTerminar').disable();
							var objMsg = Ext.getCmp('mensajes1');
							objMsg.body.update(objGral.msgError);
							objMsg.show();
							
							Ext.MessageBox.alert('Mensaje',objGral.msgError);
					}
					
					el.unmask();
				}else {
					Ext.getCmp('btnTerminar').disable();
					if(objGral.msgError!=''){
						Ext.getCmp('btnTerminar').disable();
						//Ext.MessageBox.alert('Mensaje',objGral.msgError);
						el.mask(objGral.msgError, 'x-mask');
					}else{
						if(objGral.noTasasAceptada){
							el.mask('No se ha encontrado una tasa certificada por EPO-IF-NAFIN"', 'x-mask');
						}else{
							el.mask('No se encontr� ning�n registro', 'x-mask');
						}
						
					}
				//}
			}
		}else{
			grid.hide();
			panelTotales.hide();
			Ext.MessageBox.alert('Aviso','Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.\n	Por favor comun�quese al Centro de Atenci�n a Clientes al tel�fono 50-89-61-07 � \n del interior de la Republica  al 01-800-NAFINSA (01800 623-4672).')
		}		
	}
	
	
	var recalcular = function(selModel,record, rowIndex, isSelect){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		//var cboIf = Ext.getCmp('cboIf1');
		var cboMoneda = Ext.getCmp('cboMoneda1');
		
		var objeto		= record.data['SELECHID'];//f.seleccionados;
		var notascred	= record.data['SELECNOTAS'];//f.notascred.value;
		var notnotas = record.data['SELECBOOL'];//f.aplicado;
		var cveIF = record.data['CLAVEIF'];//f.aplicado;
		
			
			if (!panelTotales.isVisible()) {
				var recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());
				panelTotales.setTitle('TOTALES - '+recordMoneda.get(cboMoneda.displayField));
				contenedorPrincipalCmp.add(panelTotales);
				contenedorPrincipalCmp.doLayout();
			}

			var totalDescto = 0;
				
			var valorDocto = 0;
			var valorLimiteUtilizado = 0;
			var nuevoValorUtilizado = 0;
			var valorLimiteMaximo = 0;
			var validarlDisponible = "N";
			var valorDisponible = 0;
			var nuevoValorDisponible = 0;
			
			var indexIf = storeCatIfData.findExact('lsIf', cveIF);
			var recordIF = storeCatIfData.getAt(indexIf);
			
			if (isSelect){ //true --> Suma
				var totalDescto = 0;
				
				if (recordIF ) {
					valorDocto = roundOff(parseFloat(record.data['IMPORTREC']) * parseFloat(objCalc.tipoCambio,10) ,2);
					valorLimiteUtilizado = roundOff(parseFloat(recordIF.data['lsLimiteUt']),2); 
					nuevoValorUtilizado = roundOff(parseFloat(valorLimiteUtilizado) + parseFloat(valorDocto),2);
					valorLimiteMaximo = roundOff(parseFloat(recordIF.data['lsLimiteMax']),2);
					validarlDisponible = recordIF.data['lsValidaDisp'];
					//f.hidTipoLimite[indice].value = tipoLimite[z];
					//objeto.value += "|"+tipoLimite[z];;
					record.data['SELECHID']=record.data['SELECHID']+'|'+recordIF.data['rs_tipo_lim'];
					if (parseFloat(nuevoValorUtilizado) > parseFloat(valorLimiteMaximo)) {
						alert('error');
						selModel.deselectRow(rowIndex);
						return false;
						//return 'e';
					} else if (validarlDisponible == 'S') {
						valorDisponible = roundOff(parseFloat(recordIF.data['lsDispLineaFondeo']),2);
						nuevoValorDisponible = roundOff(parseFloat(valorDisponible) - parseFloat(valorDocto),2);
						if (nuevoValorDisponible < 0){
							alert('Error');
							selModel.deselectRow(rowIndex);
							return false;
							//return 'e';
						} else {
							recordIF.data['lsDispLineaFondeo'] = nuevoValorDisponible;
						}
					}
					recordIF.data['lsLimiteUt'] = nuevoValorUtilizado;
				}
				
					
				objCalc.totalMonto 		= roundOff(parseFloat(objCalc.totalMonto) + parseFloat(record.data['MONTO']),2);
				objCalc.totalDescuento 	= roundOff(parseFloat(objCalc.totalDescuento) + parseFloat(record.data['MONTODESC']),2);
				objCalc.totalIntereses 	= roundOff(parseFloat(objCalc.totalIntereses) + parseFloat(record.data['IMPORTINT']),2);
				objCalc.totalRecibir 	= roundOff(parseFloat(objCalc.totalRecibir) + parseFloat(record.data['IMPORTREC']),2);
				objCalc.totaldocs 		= parseInt(objCalc.totaldocs) + 1;
				
			}else{
				
				if (recordIF ) {
					valorDocto = roundOff(parseFloat(record.data['IMPORTREC']) * parseFloat(objCalc.tipoCambio,10) ,2);
					valorLimiteUtilizado = roundOff(parseFloat(recordIF.data['lsLimiteUt']),2); 
					nuevoValorUtilizado = roundOff(parseFloat(valorLimiteUtilizado) + parseFloat(valorDocto),2);
					valorLimiteMaximo = roundOff(parseFloat(recordIF.data['lsLimiteMax']),2);
					validarlDisponible = recordIF.data['lsValidaDisp'];
				
					valorDisponible = roundOff(parseFloat(recordIF.data['lsDispLineaFondeo']),2);
					nuevoValorDisponible = roundOff(parseFloat(valorDisponible) - parseFloat(valorDocto),2);
					
					nuevoValorUtilizado = roundOff(parseFloat(valorLimiteUtilizado) + parseFloat(valorDocto),2);
					recordIF.data['lsLimiteUt'] = nuevoValorUtilizado;
					
					objCalc.totalMonto 		= roundOff(parseFloat(objCalc.totalMonto) - parseFloat(record.data['MONTO']),2);
					objCalc.totalDescuento 	= roundOff(parseFloat(objCalc.totalDescuento) - parseFloat(record.data['MONTODESC']),2);
					objCalc.totalIntereses 	= roundOff(parseFloat(objCalc.totalIntereses) - parseFloat(record.data['IMPORTINT']),2);
					objCalc.totalRecibir 	= roundOff(parseFloat(objCalc.totalRecibir) - parseFloat(record.data['IMPORTREC']),2);
					objCalc.totaldocs = parseInt(objCalc.totaldocs) - 1;
				}
			}
	
			Ext.getCmp('totalDoctosDesp1').setValue(objCalc.totaldocs);
			Ext.getCmp('totalMontoDesp1').setValue('$'+formatoFlotante(objCalc.totalMonto,"aplicar"));
			Ext.getCmp('totalMontoDescDesp1').setValue('$'+formatoFlotante(objCalc.totalDescuento,"aplicar"));
			Ext.getCmp('totalImpIntDesp1').setValue('$'+formatoFlotante(objCalc.totalIntereses,"aplicar"));
			Ext.getCmp('totalImpRecibDesp1').setValue('$'+formatoFlotante(objCalc.totalRecibir,"aplicar"));

		return true;
		
	}
	
	var terminar = function() {
		//var cboIf = Ext.getCmp('cboIf1');
		var cboEpo = Ext.getCmp('cboEpo1');
		
		if (cboEpo.getValue() == ''){
			Ext.MessageBox.alert('Aviso','Debes seleccionar una Cadena');
			return;
		}else{
			if (objCalc.totalMonto == 0) {
				Ext.MessageBox.alert('Aviso','No ha seleccionado documentos');
			} else {
				if (true){ // Paso las validaciones de los limites exitosamente
					//CODIGO PARA PREACUSE
					generaPreAcuse();
					
				}else{ // Sobrepaso alguno de los limites en cuestion
					var mensaje 	=	"Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.\n\n"+
							"Por favor comuniquese al Centro de Atencion a Clientes al telefono 50-89-61-07 o\n"+
							"del interior al 01-800-NAFINSA (01-800-6234672).";
					Ext.MessageBox.alert('Aviso',mensaje);
					return;
				}
			}	
		}
	}
	
	
	//FUNCION: genera la pantalla de Preacuse manipulando los objetos involucrados en el pantalla
	// y generando el grid de preacuse para verificar que los documentos son los seleccionados
	var generaPreAcuse = function() {
		var store = grid.getStore();
		var columnModelGrid = grid.getColumnModel();
		var noRegistros = false;
		var numRegistro = -1;
		var registrosPreAcu = [];

		//se recorre grid principal para detectar los documentos seleccionados
		store.each(function(record) {
			numRegistro = store.indexOf(record);
			if(record.data['SELECCION']=='S'){
				registrosPreAcu.push(record);
				noRegistros = true;
			}
		});
		
		//si existen doctos seleccionados, entonces se muestra el grid de preacuse
		if(noRegistros){
			var gridColumnMod = gridPreAcu.getColumnModel();
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var strTipoUsuario = objGral.strTipoUsuario;
			grid.stopEditing();
			
			var ftotales = Ext.getCmp('panelTotales1');
			var objTotales = ftotales.getForm().getValues();
			var vcboEpo = Ext.getCmp('cboEpo1');
			var vrecord = vcboEpo.findRecord(vcboEpo.valueField, vcboEpo.getValue());
			
			var dataTotales = [
					[vrecord.get(vcboEpo.displayField), objTotales.totalMontoDesp, objTotales.totalMontoDescDesp, objTotales.totalImpIntDesp, objTotales.totalImpRecibDesp ]
				];
			
			
			//se le carga informacion al grid de preacuse y al grid de totales
			storePreAcuData.add(registrosPreAcu);
			storeTotalData.loadData(dataTotales);
			
			//se valida si el grid de preacuse ya es visible
			if (!gridPreAcu.isVisible()) {
				contenedorPrincipalCmp.findById('forma').hide();
				//contenedorPrincipalCmp.findById('intermediariosS1').hide();
				contenedorPrincipalCmp.findById('gridDoctos').hide();
				contenedorPrincipalCmp.findById('panelTotales1').hide();
				
				if(contenedorPrincipalCmp.findById('gridDoctosPreAcu')){
					contenedorPrincipalCmp.findById('gridDoctosPreAcu').show();
					contenedorPrincipalCmp.findById('gridTotales1').show();
					contenedorPrincipalCmp.findById('pnmsgLegal1').show();
					
				}else{
					contenedorPrincipalCmp.insert(5,gridPreAcu);
					contenedorPrincipalCmp.insert(6,gridTotales);
					contenedorPrincipalCmp.add(panelMsgLegal);
				
				}
				
				
				contenedorPrincipalCmp.doLayout();
			}			
			
		}
		
	}//fin function generaPreAcuse
	
	
	//FUNCION:retorna de la pabtalla de Pre Acuse a la parte de seleccion de documentos 
	//para poder verificar la seleccion que se realizo
	var regresaAseleccion = function(){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		contenedorPrincipalCmp.findById('forma').show();
		contenedorPrincipalCmp.findById('gridDoctos').show();
		contenedorPrincipalCmp.findById('panelTotales1').show();
		
		contenedorPrincipalCmp.findById('gridDoctosPreAcu').hide();
		contenedorPrincipalCmp.findById('gridTotales1').hide();
		contenedorPrincipalCmp.findById('gridDoctosPreAcu').getStore().removeAll(true);
		contenedorPrincipalCmp.findById('gridTotales1').getStore().removeAll(true);
		contenedorPrincipalCmp.findById('pnmsgLegal1').hide();
		
		contenedorPrincipalCmp.doLayout();
	}
	
	var realizaConfirmacionNAFIN = function(okResp, pkcs7, textoFirmar){
		var registrosEnviar = [];
		if(okResp=='S'){
			
			Ext.Ajax.request({
				url: '13forma10ext.data.jsp',
				params: {
					informacion: 'validaHorario',
					cboIf: objGral.valIF,
					cboEpo: objGral.valEPO
				},
				callback: function(opts, success, response) {
					if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
						var resp = 	Ext.util.JSON.decode(response.responseText);
						
						if(resp.msgError!=''){
							Ext.MessageBox.alert('Mensaje',resp.msgError);
							Ext.getCmp('btnTransmitir').enable();
						}else{
							
							storePreAcuData.each(function(record){
								registrosEnviar.push(record.data);
							});
							
							
							Ext.Ajax.request({
								url: '13forma10ext.data.jsp',
								params: {
									informacion: 'confirmaClaveCesion',
									registros: Ext.encode(registrosEnviar),
									totalDescuento: objCalc.totalDescuento,
									totalMonto: objCalc.totalMonto,
									totalDescuento: objCalc.totalDescuento,
									totalIntereses: objCalc.totalIntereses,
									totalRecibir: objCalc.totalRecibir,
									sTipoLimite: objGral.sTipoLimite,
									svalorTC: objGral.valorTC,
									cboIf: objGral.valIF,
									cboEpo: objGral.valEPO,
									cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
									valCve:okResp,
									correo:'',
									email: '',
									Pkcs7: pkcs7,
									TextoFirmado: textoFirmar,
									strUsr:strUsr
								},
								callback: procesarSuccessConfirmaCesion
							});  
						}
					} else {
						NE.util.mostrarConnError(response,opts);
					}
				}
			});
		}
	}
	
	var resulValidCesion= function(okResp, siCorreo, email, errorMessage, vTipoUsuario){
		var registrosEnviar = [];
		if(okResp=='S'){
			if(vTipoUsuario!='NAFIN'){
				Ext.Ajax.request({
					url: '13forma10ext.data.jsp',
					params: {
						informacion: 'validaHorario',
						cboIf: objGral.valIF,
						cboEpo: objGral.valEPO
					},
					callback: function(opts, success, response) {
						if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
							var resp = 	Ext.util.JSON.decode(response.responseText);
							
							if(resp.msgError!=''){
								Ext.MessageBox.alert('Mensaje',resp.msgError);
							{
									storePreAcuData.each(function(record){
										registrosEnviar.push(record.data);
									});
									
									
									Ext.Ajax.request({
										url: '13forma10ext.data.jsp',
										params: {
											informacion: 'confirmaClaveCesion',
											registros: Ext.encode(registrosEnviar),
											totalDescuento: objCalc.totalDescuento,
											totalMonto: objCalc.totalMonto,
											totalDescuento: objCalc.totalDescuento,
											totalIntereses: objCalc.totalIntereses,
											totalRecibir: objCalc.totalRecibir,
											sTipoLimite: objGral.sTipoLimite,
											svalorTC: objGral.valorTC,
											cboIf: objGral.valIF,
											cboEpo: objGral.valEPO,
											cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
											valCve:okResp,
											correo:siCorreo,
											email: email
										},
										callback: procesarSuccessConfirmaCesion
									});
								}
							}
						} else {
							NE.util.mostrarConnError(response,opts);
						}
					}
				});
			}else{
				fnTransmitirDerecho(Ext.getCmp('btnTransmitir'), true);
			}
		}if(okResp=='R'){
			Ext.Msg.alert('Aviso', 'La clave fue enviada por correo electr�nico', function(btn){
			var cboEpo = Ext.getCmp('cboEpo1');								
			var record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
			var valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
			window.location.href='13forma10ext.jsp?strUsr='+strUsr;
			});
		}
	}

	var procesarSuccessConfirmaCesion = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			
			
			if(resp.msgError=='' && resp.esCveCorrecta){
				
				var acuseCifras = [
					['N�mero de Acuse', resp.objCifras.numAcuse],
					['Fecha de Carga', resp.objCifras.fecCarga],
					['Hora de Carga', resp.objCifras.horaCarga],
					['Usuario de Captura', resp.objCifras.captUser]
				];
				
				storeCifrasData.loadData(acuseCifras);
				
				//se muestran componentes para el acuse de la seleccion de doctos				
				
				if(strUsuario=='NAFIN'){
					panelMsgAcuse.html='<p align="center"><b>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br>Recibo: '+resp._acuse+'</b></p>';
					contenedorPrincipalCmp.insert(1,panelMsgAcuse);
				}
				contenedorPrincipalCmp.findById('gridDoctosPreAcu').setTitle('Acuse - Selecci�n Documentos');
				contenedorPrincipalCmp.remove(panelMsgLegal);
				
				contenedorPrincipalCmp.insert(2,gridCifrasCtrl);
				contenedorPrincipalCmp.insert(3, panelMsgCifrado);
				contenedorPrincipalCmp.insert(4, NE.util.getEspaciador(10));
				contenedorPrincipalCmp.doLayout();
				//se muestran botones para salir o generar acuse en pdf
				Ext.getCmp('btnRevisar').hide();
				Ext.getCmp('btnAbrirPDF').show();
				Ext.getCmp('btnTransmitir').hide()
				Ext.getCmp('btnSalir').show();
				Ext.getCmp('btnCancelar').hide();
				
				Ext.getCmp('btnAbrirPDF').setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = resp.urlArchivo;
					forma.submit();
				});

			}else if(resp.msgError!='' && !resp.esCveCorrecta){
				Ext.MessageBox.alert('Aviso',resp.msgError);
			}else if(resp.msgError!=''){
			
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//------------------------------------------------------------------------------
	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'catalogoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma10ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, option){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						if(objGral.inicial=='S'){
							iniRequest(store);
						}
					}else if(store.getTotalCount()<1 ){
						cveEpoOriginal = Ext.getDom("hidCboEpo").value;
						//validaContratoEpoPyme(cveEpoOriginal);
					}
			}
		}
	});
	
	var storeCatMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma10ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store, records, oprion){
				if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
					var valorInicialCboMoneda = Ext.getDom("hidCboMoneda").value;
					var cboMoneda = Ext.getCmp('cboMoneda1');
					if(cboMoneda.getValue()=='' && valorInicialCboMoneda == ''){
						cboMoneda.setValue(records[0].data['clave']);
						Inicializacion.catalogoMoneda = true;
					} else if (valorInicialCboMoneda != '	') {
						cboMoneda.setValue(valorInicialCboMoneda);
						Inicializacion.catalogoMoneda = true;
						if(NE.util.allTrue(Inicializacion)) {
							//Si cuando llega aqui ya estan inicializados los valores iniciales necesarios,
							//realiza la consulta. Esta secci�n solo aplica cuando la consulta recibe parametros iniciales
							consultar();
							//alert('carga completa');
						}
					}
				}
			}
		}
	});
	
	var storeCatIfData = new Ext.data.JsonStore({
		root : 'registrosIF',
		fields : ['lsIf', 'lsNombreCuenta', 'lsTipoPiso','lsLimiteUt', 'lsLimiteMax', 'lsValidaDisp', 'lsDispLineaFondeo', 'rs_tipo_lim', 'lsMontoComprometido', 'loadMsg'],
		totalProperty : 'totalCount',
		autoLoad: false,
		listeners: {
			//load: procesarConsulta,
			exception: NE.util.mostrarDataProxyError
			//beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		fields: [
			{name: 'NUMDOCTO'},
			{name: 'FECEMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECVENC',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORCDESC', type: 'float'},
			{name: 'MONTODESC', type: 'float'},
			{name: 'SELECCION'},  //--   aqui va el check de seleccion
			{name: 'SELECBOOL'},
			{name: 'SELECHID'},
			{name: 'IMPORTINT', type: 'float'},
			{name: 'IMPORTREC', type: 'float'},
			{name: 'NOMBREIF'},
			{name: 'BANCDEPOSITO'},
			{name: 'SUCURSAL'},
			{name: 'CTADEPOSITO'},
			{name: 'NETORECIBPYME', type: 'float'},
			{name: 'BENEFICIARIO'},
			{name: 'PORCBENEFIC'},
			{name: 'IMPORTEBENEFIC'},
			{name: 'CLAVEIF'},
			{name: 'LIMITEACTIVO'},
			{name: 'ENTIDAD_GOBIERNO'},
			{name: 'BLO_PYME_EPO_IF'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});
	
	var storePreAcuData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMDOCTO'},
			{name: 'FECEMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECVENC',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORCDESC', type: 'float'},
			{name: 'MONTODESC', type: 'float'},
			{name: 'SELECCION'},  //--   aqui va el check de seleccion
			{name: 'SELECBOOL'},
			{name: 'SELECHID'},
			{name: 'IMPORTINT', type: 'float'},
			{name: 'IMPORTREC', type: 'float'},
			{name: 'NOMBREIF'},
			{name: 'BANCDEPOSITO'},
			{name: 'SUCURSAL'},
			{name: 'CTADEPOSITO'},
			{name: 'NETORECIBPYME', type: 'float'},
			{name: 'BENEFICIARIO'},
			{name: 'PORCBENEFIC'},
			{name: 'IMPORTEBENEFIC'},
			{name: 'CLAVEIF'}
		]
	});
	
	var storeTotalData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'EPO'},
			  {name: 'TOTALMONTO', type: 'float'},
			  {name: 'TOTALDESCTO', type: 'float'},
			  {name: 'TOTALINTERES', type: 'float'},
			  {name: 'TOTALIMPORT', type: 'float'}
		  ]
	 });
	 
	 var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });

//------------------------------------------------------------------------------	
	var elementosForma = [
		{
			xtype: 'displayfield',
			fieldLabel: 'No. Nafin El�ctronico Pyme',
			value: strUsuario=='NAFIN'?(Ext.getDom("strNePymeAsigna").value+' '+Ext.getDom("strNombrePymeAsigna").value):'',
			hidden: strUsuario=='NAFIN'?false:true
		},
		{
			xtype: 'combo',
			name: 'cboEpo',
			id: 'cboEpo1',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEpo',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEpoData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'cboMoneda',
			id: 'cboMoneda1',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMoneda',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			id: 'cfFechaVenc',
			fieldLabel: 'Fecha Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txtFechaVencDe',
					id: 'txtFechaVencDe',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'txtFechaVenca',
					margins: '0 20 0 0',
					value: Ext.getDom('hidTxtFechaVencDe').value
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtFechaVenca',
					id: 'txtFechaVenca',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFechaVencDe',
					margins: '0 20 0 0',
					value: Ext.getDom('hidTxtFechaVenca').value
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		}
	] 
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		//hidden:true,
		style: 'margin:0 auto;',
		title: 'Selecci�n Documentos',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				id:'btnConsultar',
				formBind: true,
				handler: consultar
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href="13forma10ext.jsp?strUsr="+strUsr;
				}
				
			}
		]
	});	
	
	
	var selectModel = new Ext.grid.CheckboxSelectionModel({
        checkOnly: true,
		  renderer: function(v, p, record){
				if (record.data['SELECBOOL']){
					return '<div class="x-grid3-row-checker">&#160;</div>';
				}else{
					return '<div>&#160;</div>';
				}
			},
		  listeners: {
            rowselect: function(selectModel, rowIndex, record) {
					 if(record.data['SELECCION']!='S')
						if(recalcular(selectModel, record, rowIndex, true))
							record.data['SELECCION']='S';
            },
				rowdeselect: function(selectModel, rowIndex, record) {
					//if(record.data['SELECBOOL'] && record.data['SELECCION']=='S'){
					//selectModel.selectRow(rowIndex,true);
					//}else{
					 if(record.data['SELECCION']!='N')
						if(recalcular(selectModel, record, rowIndex, false))
							record.data['SELECCION']='N';
						//}
					
            },
				beforerowselect: function( selectModel, rowIndex, keepExisting, record ){
					var mensaje = '';
					if(record.data['SELECBOOL']){
						if(record.data['LIMITEACTIVO']=='N'){
							mensaje 	=	"Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.\n\n"+
								"Por favor comuniquese al Centro de Atencion a Clientes al telefono 50-89-61-07 o\n"+
								"del interior al 01-800-NAFINSA (01-800-6234672).";							
						}else  if(record.data['ENTIDAD_GOBIERNO']=='S'){
							mensaje  = 'No se puede operar el documento por que el Proveedor es Entidad de Gobierno';						
						}
						
					}else{
						mensaje  = 'No es posible seleccionar el documento';
					}
					if(record.data['BLO_PYME_EPO_IF']=='S')	{
						mensaje ="Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado- IF. Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes Cd. De M�xico 50-89-61-07. Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)";
					}
					
					if(mensaje!=''){
						Ext.MessageBox.alert('Aviso',mensaje);
						return false;
					}
				}
        }
    });
	
	var grid = new Ext.grid.EditorGridPanel({
	id: 'gridDoctos',
	store: storeDoctosData,
	margins: '20 0 0 0',
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	sm: selectModel,
	columns: [
		selectModel,
		{//1
			header: 'N�mero Documento',
			tooltip: 'N�mero Documento',
			dataIndex: 'NUMDOCTO',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false
		},
		{//2
			header: 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex: 'FECEMISION',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//3
			header: 'Fecha de Vencimiento',
			tooltip: 'Fecha de Vencimiento',
			dataIndex: 'FECVENC',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//4
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO',
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//6
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			sortable : true
		},
		{//7
			header : '<center>Monto</center>',
			tooltip: 'Monto',
			dataIndex : 'MONTO',
			width : 150,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//8
			header : 'Porcentaje de Descuento',
			tooltip: 'Porcentaje de Descuento',
			dataIndex : 'PORCDESC',
			sortable : true,
			width : 100,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		},
		{//9
			header : '<center>Monto a Descontar</center>',
			tooltip: 'Monto a Descontar',
			dataIndex : 'MONTODESC',
			sortable : true,
			width : 100,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : '<center>Importe de Intereses</center>',
			tooltip: 'Importe de Intereses',
			dataIndex : 'IMPORTINT',
			width : 150,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//12
			header : '<center>Importe a Recibir</center>',
			tooltip: 'Importe a Recibir',
			dataIndex : 'IMPORTREC',
			width : 150,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//18
			header : 'Nombre del IF',
			tooltip: 'Nombre del IF',
			dataIndex : 'NOMBREIF',
			width : 150,
			sortable : true,
			align: 'left'
		},
		{
			header : 'Banco de Dep�sito',
			tooltip: 'Banco de Deposito',
			dataIndex : 'BANCDEPOSITO',
			width : 150,
			sortable : true,
			align: 'left'
		},
		{
			header : 'Sucursal',
			tooltip: 'Sucursal',
			dataIndex : 'SUCURSAL',
			width : 150,
			sortable : true,
			align: 'center'
		},
		{
			header : 'Cuenta de Dep�sito',
			tooltip: 'Cuenta de Dep�sito',
			dataIndex : 'CTADEPOSITO',
			width : 150,
			sortable : true,
			align: 'center'
		},
		{
			header : '<center>Neto a Recibir Pyme</center>',
			tooltip: 'Neto a Recibir Pyme',
			dataIndex : 'NETORECIBPYME',
			width : 150,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//19
			header : 'Beneficiario',
			tooltip: 'Beneficiario',
			dataIndex : 'BENEFICIARIO',
			sortable : true,
			width : 100,
			align: 'left'
		},
		{//20
			header : '% Beneficiario',
			tooltip: '% Beneficiario',
			dataIndex : 'PORCBENEFIC',
			sortable : true,
			width : 80,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		},
		{//21
			header : '<center>Importe a Recibir Beneficiario</center>',
			tooltip: 'Importe a Recibir Beneficiario',
			dataIndex : 'IMPORTEBENEFIC',
			width : 80,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 900,
	style: 'margin:0 auto;',
	title: '',
	frame: true,
	/*listeners: {
		viewReady: selectDefaultDoctos
	},*/
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Terminar',
			id: 'btnTerminar',
			handler: terminar
			}
		]
	}
	});
	
	var panelTotales = new Ext.form.FormPanel({    
		name: 'panelTotales',
		id: 'panelTotales1',
		title:'-',
		width: 900,
		height: 90,
		style: 'margin:0 auto;',
		frame: true,
		layout:'absolute',
		defaultType: 'textfield',
      labelWidth: 0,  
		items:[
			{   
			  xtype:'label',   
			  y:5,    
			  x:142,   
			  text:'Total Documentos',   
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:262,   
			  text:'Monto',   
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:382,   
			  text:'Monto a Descontar',   
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:502,   
			  text:'Importe de Inter�s',   
			  style:'color:#777777'  
			}, 
			{   
			  xtype:'label',   
			  y:5,    
			  x:622,   
			  text:'Importe a Recibir',   
			  style:'color:#777777'  
			}, 
			{   
			  name: 'totalDoctosDesp', // campo ID   
			  id:'totalDoctosDesp1',
			  x:142,   
			  y:25,   
			  style:'padding:4px 3px;',   
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			{   
			  name: 'totalMontoDesp', // campo ID   
			  id:'totalMontoDesp1',
			  x:262,   
			  y:25,   
			  style:'padding:4px 3px;',   
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {   
			  name: 'totalMontoDescDesp', // campo ID   
			  id:'totalMontoDescDesp1',
			  x:382,   
			  y:25,   
			  style:'padding:4px 3px;',   
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {   
			  name: 'totalImpIntDesp', // campo ID   
			  id:'totalImpIntDesp1',
			  x:502,   
			  y:25,   
			  style:'padding:4px 3px;',   
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {   
			  name: 'totalImpRecibDesp', // campo ID   
			  id:'totalImpRecibDesp1',
			  x:622,   
			  y:25,   
			  style:'padding:4px 3px;',      
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 }
			 
		]
	});
	
	var gridPreAcu = new Ext.grid.GridPanel({
	id: 'gridDoctosPreAcu',
	store: storePreAcuData,
	margins: '20 0 0 0',
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [	

		{//18
			header : 'IF Seleccionado',
			tooltip: 'IF Seleccionado',
			dataIndex : 'NOMBREIF',
			width : 150,
			sortable : true,
			align: 'left'
		},
		{
			header : 'Banco de Dep�sito',
			tooltip: 'Banco de Deposito',
			dataIndex : 'BANCDEPOSITO',
			width : 150,
			sortable : true,
			align: 'left'
		},
		{
			header : 'Sucursal',
			tooltip: 'Sucursal',
			dataIndex : 'SUCURSAL',
			width : 150,
			sortable : true,
			align: 'center'
		},
		{
			header : 'Cuenta de Dep�sito',
			tooltip: 'Cuenta de Dep�sito',
			dataIndex : 'CTADEPOSITO',
			width : 150,
			sortable : true,
			align: 'center'
		},
		{//2
			header: 'EPO',
			tooltip: 'EPO',
			dataIndex: 'EPOSELECT',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left',
			renderer:  function (causa, columna, registro){
						var cboEPO = Ext.getCmp('cboEpo1');
						var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
						causa = recordEPO.get(cboEPO.displayField);
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
			
		},
		{//3 -- se queda pendiente para la parte NAFIN
			header: 'PYME',
			tooltip: 'PYME',
			dataIndex: 'PYMESELECT',
			sortable: true,
			width: 150,
			resizable: true,
			//hidden: strUsuario=='NAFIN'?false:true, 
			hidden: true, 
			renderer:  function (causa, columna, registro){
						if(strUsuario=='NAFIN'){
							causa = Ext.getDom("strNombrePymeAsigna").value;
						}
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
		},
		{//4
			header: 'N�mero de Documento',
			tooltip: 'N�mero de Documento',
			dataIndex: 'NUMDOCTO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'center'
		},
		{//5
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			sortable : true
		},
		{//6
			header : '<center>Monto Documento</center>',
			tooltip: 'Monto',
			dataIndex : 'MONTO',
			width : 150,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//7
			header : 'Porcentaje de Descuento',
			tooltip: 'Porcentaje de Descuento',
			dataIndex : 'PORCDESC',
			sortable : true,
			width : 100,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		},
		{//8
			header : '<center>Monto a Descontar</center>',
			tooltip: 'Monto a Descontar',
			dataIndex : 'MONTODESC',
			sortable : true,
			width : 100,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//9
			header : '<center>Monto Intereses</center>',
			tooltip: 'Monto Intereses',
			dataIndex : 'IMPORTINT',
			width : 150,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//10
			header : '<center>Monto a Recibir</center>',
			tooltip: 'Monto a Recibir',
			dataIndex : 'IMPORTREC',
			width : 150,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : '<center>Neto a Recibir Pyme</center>',
			tooltip: 'Neto a Recibir Pyme',
			dataIndex : 'NETORECIBPYME',
			sortable : true,
			//hidden: true,
			width : 100,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//12
			header : 'Beneficiario',
			tooltip: 'Beneficiario',
			dataIndex : 'BENEFICIARIO',
			//hidden: true,
			sortable : true,
			width : 100,
			align: 'left'
		},
		{//13
			header : '% Beneficiario',
			tooltip: '% Beneficiario',
			dataIndex : 'PORCBENEFIC',
			//hidden: true,
			sortable : true,
			width : 100,
			align: 'center',
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		},
		{//14
			header : '<center>Importe a Recibir Beneficiario</center>',
			tooltip: 'Importe a Recibir Beneficiario',
			dataIndex : 'IMPORTEBENEFIC',
			//hidden: true,
			sortable : true,
			width : 100,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	style: 'margin:0 auto;',
	width: 900,
	title: 'PRE ACUSE - SELECCI�N DE DOCUMENTOS',
	frame: true,
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Revisar',
			id: 'btnRevisar',
			handler: regresaAseleccion
			},
			{
			text: 'Generar PDF',
			id: 'btnGenerarPDF',
			hidden: true
			//handler: 
			},
			{
			text: 'Abrir PDF',
			id: 'btnAbrirPDF',
			hidden: true
			//handler: 
			},
			'-',
			{
			text: 'Transmitir Derecho',
			id: 'btnTransmitir',
			handler: fnTransmitirDerecho
			},
			{
			text: 'Salir',
			id: 'btnSalir',
			hidden: true,
			handler: function(){
					window.location.href='13forma10ext.jsp?strUsr='+strUsr;
				}
			},
			'-',
			{
			text: 'Cancelar',
			id: 'btnCancelar',
			handler: function(){
					Ext.Msg.confirm('Confirmaci�n', '�Est� usted seguro de cancelar la operaci�n?', function(btn){
						if(btn=='yes'){
							window.location.href='13forma10ext.jsp?strUsr='+strUsr;
						}
					});
				}
			}
			
		]
	}
	});
	
	var gridTotales = new Ext.grid.GridPanel({
	id: 'gridTotales1',
	store: storeTotalData,
	margins: '20 0 0 0',
	columns: [
		{
			header : 'EPO',
			dataIndex : 'EPO',
			width : 200,
			sortable : true,
			renderer:  function (causa, columna, registro){
						var cboEPO = Ext.getCmp('cboEpo1');
						var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
						causa = recordEPO.get(cboEPO.displayField);
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
		},
		{
			header : 'PYME',
			dataIndex : 'PYME',
			width : 200,
			sortable : true,
			//hidden: strUsuario=='NAFIN'?false:true, 
			hidden: true, 
			renderer:  function (causa, columna, registro){
					if(strUsuario=='NAFIN'){
						causa = Ext.getDom("strNombrePymeAsigna").value;
					}
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
			}
		},
		{
			header : '<center>Total Monto Documento</center>',
			tooltip: 'Total Monto Documento',
			dataIndex : 'TOTALMONTO',
			width : 169,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : '<center>Total Monto Descuento</center>',
			tooltip: 'Total Monto Descuento',
			dataIndex : 'TOTALDESCTO',
			width : 166,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : '<center>Total Monto Inter�s</center>',
			tooltip: 'Total Monto Inter�s',
			dataIndex : 'TOTALINTERES',
			width : 166,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : '<center>Total Importe Recibir</center>',
			tooltip: 'Total Importe Recibir',
			dataIndex : 'TOTALIMPORT',
			width : 166,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 100,
	width: 900,
	style: 'margin:0 auto;',
	title: ' ',
	frame: true
	});
	
	var gridCifrasCtrl = new Ext.grid.GridPanel({
	id: 'gridCifrasCtrl1',
	store: storeCifrasData,
	margins: '20 0 0 0',
	hideHeaders : true,
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 150,
			sortable : true
		},
		{
			header : 'Informacion',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'informacion',
			width : 230,
			sortable : true,
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 400,
	style: 'margin:0 auto;',
	autoHeight : true,
	title: 'Cifras de Control',
	frame: true
	});
	
	
	
	var panelMsgLegal = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'pnmsgLegal',
		id: 'pnmsgLegal1',
		width: 900,
		style: 'margin:0 auto;',
		frame: true,
		html:'Al solicitar el factoraje electr&oacute;nico o descuento electr&oacute;nico del documento que selecciono e identifico, '+
				'transmito los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art&iacute;culos 427 de la Ley General de T&iacute;tulos y Operaciones de Cr&eacute;dito, '+
				'32 c del C&oacute;digo Fiscal de la Federaci&oacute;n y 2038 del C&oacute;digo Civil Federal, haci&eacute;ndome sabedor de su contenido y alcance, condicionado a que se efect&uacute;e '+
				'el descuento electr&oacute;nico o el factoraje electr&oacute;nico.<br/>'+
                'Asimismo, en este acto manifiesto bajo protesta de decir verdad, que s&iacute; he emitido o emitir&eacute; a la Empresa de Primer Orden el CFDI ' +
                'por la operaci�n comercial que le dio origen a esta transacci&oacute;n, seg&uacuten sea el caso y conforme establezcan las disposiciones fiscales vigentes'
	});
	
	var panelMsgCifrado = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'panelMsgCifrado',
		id: 'panelMsgCifrado1',
		width: 900,
		style: 'margin:0 auto;',
		frame: true,
		html:'Al solicitar el factoraje electr&oacute;nico o descuento electr&oacute;nico del documento que selecciono e identifico, '+
				'transmito los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art&iacute;culos 427 de la Ley General de T&iacute;tulos y Operaciones de Cr&eacute;dito, '+
				'32 c del C&oacute;digo Fiscal de la Federaci&oacute;n y 2038 del C&oacute;digo Civil Federal, haci&eacute;ndome sabedor de su contenido y alcance, condicionado a que se efect&uacute;e '+
				'el descuento electr&oacute;nico o el factoraje electr&oacute;nico.<br/>'+
                'Asimismo, en este acto manifiesto bajo protesta de decir verdad, que s&iacute; he emitido o emitir&eacute; a la Empresa de Primer Orden el CFDI ' +
                'por la operaci�n comercial que le dio origen a esta transacci&oacute;n, seg&uacuten sea el caso y conforme establezcan las disposiciones fiscales vigentes'
	});
	
	var panelMsgAcuse = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'pnmsgAcuse',
		id: 'pnmsgAcuse1',
		width: 400,
		style: 'margin:0 auto;',
		frame: true
	});
		
//-----------------------------------------------------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			{//Panel para mostrar avisos
				xtype: 'panel',
				name: 'mensajes',
				id: 'mensajes1',
				width: 600,
				frame: true,
				style: 'margin:0 auto;',
				hidden: true
			},
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(10)
			
		]
	});
	
	fp.hide();
	storeCatEpoData.load();
	storeCatMonedaData.load();
	
	var iniRequest = function(storeEpo){
		var valorEpo = '';
		var cboEpo = Ext.getCmp('cboEpo1');

		if(strUsuario=='NAFIN'){
			var record = '';
			if(Ext.getDom("hidCboEpo").value!='' || comboEpoVacio ){
				cveEpoOriginal = Ext.getDom("hidCboEpo").value;								
			
				if(comboEpoVacio){
					valorEpo = cveEpoOriginal+'|'+'IF';
				}else{
					if(storeEpo.findExact("clave", Ext.getDom("hidCboEpo").value) != -1 ){
						cboEpo.setValue(Ext.getDom("hidCboEpo").value);
						record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());		
						valorEpo = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
					}else{
						record = storeEpo.getAt(0);
						cboEpo.setValue(record.data['clave']);
						valorEpo = cboEpo.getValue()+'|'+record.data['descripcion'];
					}
				}
			}else{
				record = storeEpo.getAt(0);
				cboEpo.setValue(record.data['clave']);
				valorEpo = cboEpo.getValue()+'|'+record.data['descripcion'];
			}
		}
		
		Ext.Ajax.request({
			url: '13forma10ext.data.jsp',
			params: {
				informacion: 'valoresIniciales',
				cboEpo: valorEpo,
				cvePyme: Ext.getDom("cvePyme").value,
				cveEpoOriginal: cveEpoOriginal
				},
			callback: procesarSuccessValoresIni
		});
	}
	

});