<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	netropology.utilerias.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% 

/*** OBJETOS ***/
CQueryHelperRegExtJS queryHelper;
JSONObject jsonObj;
/*** FIN DE OBJETOS ***/

String correoRepresentanteLegal 	= request.getParameter("correoRepresentanteLegal")	==null?"":(String)request.getParameter("correoRepresentanteLegal");
String anteriorParametrizacion	= request.getParameter("anteriorParametrizacion")	==null?"":(String)request.getParameter("anteriorParametrizacion");
String informacion					= request.getParameter("informacion")					==null?"": request.getParameter("informacion");
String infoRegresar = "";
String claveBeneficiario = iNoCliente;

if(informacion.equals("Inicializar")){			
    //Se crea la instancia del EJB para su utilizaci�n.
   ParametrosDescuento parametrosDescuentoBean = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		
	correoRepresentanteLegal = parametrosDescuentoBean.obtenerCorreoRepresentanteLegalBenef(claveBeneficiario);
	anteriorParametrizacion = parametrosDescuentoBean.obtenerEstatusAvisoPublicacionBenef(claveBeneficiario);
	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", anteriorParametrizacion);
	jsonObj.put("correoRepresentanteLegal", correoRepresentanteLegal);
	infoRegresar=jsonObj.toString();
}

if(informacion.equals("Guardar")){
	String strLoginUsuario 	= (String)request.getSession().getAttribute("Clave_usuario");
	String csActivaServicio = request.getParameter("csActivaServicio")==null ? "" : request.getParameter("csActivaServicio");
	
	 //Se crea la instancia del EJB para su utilización.
	ParametrosDescuento parametrosDescuentoBean = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
			
	parametrosDescuentoBean.guardarEstatusAvisoPublicacionBenef(claveBeneficiario, strLoginUsuario, csActivaServicio);
	correoRepresentanteLegal = parametrosDescuentoBean.obtenerCorreoRepresentanteLegalBenef(claveBeneficiario);
	anteriorParametrizacion = parametrosDescuentoBean.obtenerEstatusAvisoPublicacionBenef(claveBeneficiario);
}

if(informacion.equals("Consulta")){			
   //Se crea la instancia del EJB para su utilización.
   ParametrosDescuento parametrosDescuentoBean = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);;
			
	HashMap bitacoraAvisoPubBenef = parametrosDescuentoBean.obtenerBitacoraAvisoPublicacionBenef(claveBeneficiario);
	int numeroRegistros = Integer.parseInt(bitacoraAvisoPubBenef.get("numeroRegistros").toString());
	HashMap datos;
	List reg=new ArrayList();
		
	for (int i = 0; i < numeroRegistros; i++) {
		HashMap registroBit = (HashMap)bitacoraAvisoPubBenef.get("registroBit" + i);
		datos = new HashMap();
		datos.put("ACCION", registroBit.get("accion"));
		datos.put("USUARIO", registroBit.get("usuario"));
		datos.put("FECHA", registroBit.get("fecha_activacion"));
		reg.add(datos);
	}
	
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar=jsonObj.toString();
}
%>

<%=infoRegresar%>
