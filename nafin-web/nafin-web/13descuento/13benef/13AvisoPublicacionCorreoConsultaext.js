Ext.onReady(function() {

	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '13AvisoPublicacionCorreoext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'ACCION'},
			{name: 'USUARIO'},
			{name: 'FECHA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			//load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: false,
		deferRowRender: false,
		clicksToEdit:1,
		view:new Ext.grid.GridView({forceFit:true}),
		columns: [{
			header: 'Acci�n', tooltip: 'Acci�n', dataIndex: 'ACCION',
			sortable: true, width: 150, resizable: true, align: 'center',menuDisabled:true
		},{
			header: 'Usuario', tooltip: 'Usuario',	dataIndex: 'USUARIO', 
			sortable: true,	align: 'center',width: 150, resizable: true, align: 'center',menuDisabled:true
		},{
			header: 'Fecha', tooltip: 'Fecha', dataIndex: 'FECHA',
			sortable: true,	align: 'center', width: 150, resizable: true, align: 'center',menuDisabled:true
		}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 533,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: false
	};
	
	var fp = {
		xtype: 'form',
		id: 'forma',
		title: 'Consulta',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 530,
		titleCollapse: false,
		bodyStyle: 'padding: 10px 6px 6px 6px;',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[grid]
	};//FIN DE LA FORMA
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [fp]
	}).show();

});