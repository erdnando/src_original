Ext.onReady(function() {

	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta1.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},
		hidden: true,
		fields: [	
			{name: 'MONEDA'},
			{name: 'NUM_DOCTOS'},
			{name: 'TOTAL_MONTO'},
			{name: 'TOTAL_MONTO_IMPORTE'}								
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'',
		hidden: true,
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'N�mero de Documentos',
				tooltip: 'N�mero de Documentos',
				dataIndex : 'NUM_DOCTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'Total Monto Descuento',
				tooltip: 'Total Monto Descuento',
				dataIndex : 'TOTAL_MONTO',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Total Importe Recibir Beneficiario',
				tooltip: 'Total Importe Recibir Beneficiario',
				dataIndex : 'TOTAL_MONTO_IMPORTE',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : '',
				tooltip: '',				
				sortable: true,
				width: 240,
				resizable: true,				
				align: 'right'				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 100,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	

	var procesarGenerarArchivoPDF =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarPDF');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarPDF');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarArchivoCSV =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnGenerarCSV');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('btnBajarCSV');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}						
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;	
			
			  Ext.getCmp('btnBajarPDF').hide();
			  Ext.getCmp('btnBajarCSV').hide();
			  
			if(store.getTotalCount() > 0) {	
				
				consultaDataTotales.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotales'	
					})
				});
				Ext.getCmp('gridTotales').show();				
				
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnGenerarCSV').enable();
				el.unmask();					
			} else {
				Ext.getCmp('gridTotales').hide();	
				Ext.getCmp('btnGenerarCSV').disable();
				Ext.getCmp('btnGenerarPDF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13consulta1.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'NOMBRE_EPO'},
			{name: 'FECHA_NOTIFICACION'},
			{name: 'ACUSE_NOTIFICACION'},
			{name: 'NOMBRE_PYME'},
			{name: 'NUM_DOCUMENTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'MONEDA'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'BENEFICIARIO'},
			{name: 'PORCE_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR'}			
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});


	var gridConsulta = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: 'Avisos de Notificaci�n',
		columns: [	
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Fecha de Notificaci�n',
				tooltip: 'Fecha de Notificaci�n',
				dataIndex: 'FECHA_NOTIFICACION',
				sortable: true,
				width: 130,				
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Acuse de Notificaci�n',
				tooltip: 'Acuse de Notificaci�n',
				dataIndex: 'ACUSE_NOTIFICACION',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Nombre del Proveedor',
				tooltip: 'Nombre del Proveedor',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex: 'NUM_DOCUMENTO',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Monto Descuento',
				tooltip: 'Monto Descuento',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex: 'PORCE_BENEFICIARIO',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe Recibir Beneficiario',
				tooltip: 'Importe Recibir Beneficiario',
				dataIndex: 'IMPORTE_RECIBIR',
				sortable: true,
				width: 130,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {	
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',	
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var barraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '13consulta1.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoPDF',
								start: barraPaginacion.cursor,
								limit: barraPaginacion.pageSize							
							})
							,callback: procesarGenerarArchivoPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},	
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.disable();
						Ext.Ajax.request({
							url: '13consulta1.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV'												
							})
							,callback: procesarGenerarArchivoCSV
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				}					
			]				
		}		
	});
	
//***********************FORMA ***********************
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta1.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	
	var  elementosForma  = [	
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO					
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de notificaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_notMin',
					id: 'df_fecha_notMin',
					allowBlank: true,
					startDay: 0,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_notMax',
					margins: '0 20 0 0' ,
					formBind: true},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_notMax',
					id: 'df_fecha_notMax',
					allowBlank: true,
					startDay: 1,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_fecha_notMin',
					margins: '0 20 0 0',
					formBind: true
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Acuse de notificaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'textfield',
					fieldLabel: 'Acuse de notificaci�n',		
					name: 'cc_acuse',
					id: 'cc_acuse1',
					allowBlank: true,
					maxLength: 15,
					width: 90,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		}
	];
	
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Avisos de Notificaciones',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 160,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,	  		
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
					
					var df_fecha_notMin =  Ext.getCmp("df_fecha_notMin");
					var df_fecha_notMax =  Ext.getCmp("df_fecha_notMax");
					if ( ( !Ext.isEmpty(df_fecha_notMin.getValue()) &&  Ext.isEmpty(df_fecha_notMax.getValue()) )  
							|| ( Ext.isEmpty(df_fecha_notMin.getValue()) &&  !Ext.isEmpty(df_fecha_notMax.getValue()) ) ) {	
						df_fecha_notMin.markInvalid('Debe seleccionar un rango de fechas');	
						df_fecha_notMax.markInvalid('Debe seleccionar un rango de fechas');	
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
										
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15
						})
					});				
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta1ext.jsp';								
				}				
			}
		]
	});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,	
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});
	
	catalogoEPO.load();
	
});