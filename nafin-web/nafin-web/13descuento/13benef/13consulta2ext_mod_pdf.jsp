<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();

String ic_docto = request.getParameter("ic_docto").trim();
String cg_pyme_epo_int = request.getParameter("cg_pyme_epo_interno")==null?"":request.getParameter("cg_pyme_epo_interno");
String nombreProv = request.getParameter("nombreProv")==null?"":request.getParameter("nombreProv");
String ic_if = iNoCliente;
String ig_numero_docto = request.getParameter("ig_numero_docto").trim();
String fch_emision = request.getParameter("fch_emision").trim();
String fch_venc = request.getParameter("fch_venc").trim();
String moneda = request.getParameter("moneda").trim();
String monto = request.getParameter("monto").trim();
String estatus = request.getParameter("estatus").trim();

try {
	if (!ic_docto.equals("") && ic_docto != null)	{
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		pdfDoc.addText("Proveedor:					"+cg_pyme_epo_int + "   " + nombreProv,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Número de Documento:	"+ig_numero_docto,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Fecha de Emisión:		"+fch_emision,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Fecha de Vencimiento:	"+fch_venc,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Moneda:						"+moneda,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Monto:						"+Comunes.formatoMN(monto),"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Estatus:					"+estatus,"formas",ComunesPDF.LEFT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		pdfDoc.setTable(7, 100);
		pdfDoc.setCell("Fecha Movimiento", "celda01", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto", "celda01", ComunesPDF.CENTER);
		pdfDoc.setCell("Monto Anterior", "celda01", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Vencimiento", "celda01", ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Vencimiento Anterior", "celda01", ComunesPDF.CENTER);
		pdfDoc.setCell("Causa", "celda01", ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Acuse", "celda01", ComunesPDF.CENTER);
		
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		
		Registros reg = new Registros();
		reg = BeanParamDscto.getModificacionMontos("", ic_docto);
		
		String fn_monto_nuevo = "";
		String fn_monto_anterior = "";
		String df_fecha_venc= "";
		int count = 0;
		while(reg.next())	{
		   fn_monto_nuevo = reg.getString(2)==null?"":reg.getString(2);
         fn_monto_anterior = reg.getString(3)==null?"":reg.getString(3);
			df_fecha_venc=reg.getString(6)==null?"":reg.getString(6);
			if(!"".equals(df_fecha_venc) || !"".equals(fn_monto_nuevo) ) {
				pdfDoc.setCell(reg.getString(1), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell((fn_monto_nuevo.equals("")?"":Comunes.formatoMN(fn_monto_nuevo)), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell((fn_monto_anterior.equals("")?"":Comunes.formatoMN(fn_monto_anterior)), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formas", ComunesPDF.CENTER);
				pdfDoc.setCell((reg.getString(7)==null?"":reg.getString(7)), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(reg.getString(4), "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(((reg.getString(5)==null)?"":reg.getString(5)), "formas", ComunesPDF.CENTER);
			  count++;
			}
		}
		if (count == 0)	{
			pdfDoc.setCell("No se encontraron registros", "formas", ComunesPDF.CENTER,7);
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) { 
	throw new AppException("Error al generar el archivo", e);
} finally {}
%>
<%=jsonObj%>