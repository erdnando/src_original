<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
String ic_documento = (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto");
try {
	if(!ic_documento.equals("") ){
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
		ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		Registros regCamposAdicionales = null;
		Registros regCamposDetalle = null;
		Registros regDatos = null;
		Registros regDatosDetalle = null;
		
		int totalcd=0;
		totalcd = BeanSeleccionDocumento.getTotalCamposAdicionales(iNoCliente);
		if(totalcd >=1) {
			regCamposAdicionales = new Registros();
			regCamposAdicionales = BeanParamDscto.getCamposAdicionales(iNoCliente);
			regCamposDetalle = new Registros();
			regCamposDetalle = BeanParamDscto.getCamposAdicionalesDetalle(ic_epo);
		}
		regDatos = new Registros();
		regDatos = BeanParamDscto.getDatosDocumentos("",ic_documento);
		regDatosDetalle = new Registros();
		regDatosDetalle = BeanParamDscto.getDatosDocumentosDetalle(ic_documento);
      
      
      System.out.println("::::::::::::: iNoCliente ::::::::::: + " + iNoCliente);
      System.out.println("::::::::::::: ic_documento ::::::::::: + " + ic_documento);
      
		String nombre ="",	rfc ="",	calle ="",	colonia ="",	cp ="",	estado ="",	delomun ="",	numdocto ="",	fchdocto ="",	fchvenc ="";
		String monto ="",	moneda ="",	campo1 ="",	campo2 ="",	campo3 ="",	campo4 ="",	campo5 ="";

		if(regDatos.next()){
			nombre = (regDatos.getString(1)==null)?"&nbsp;":regDatos.getString(1);
			rfc = (regDatos.getString(2)==null)?"&nbsp;":regDatos.getString(2);
			calle = (regDatos.getString(3)==null)?"&nbsp;":regDatos.getString(3);
			colonia = (regDatos.getString(4)==null)?"&nbsp;":regDatos.getString(4);
			cp = (regDatos.getString(5)==null)?"&nbsp;":regDatos.getString(5);
			estado = (regDatos.getString(6)==null)?"&nbsp;":regDatos.getString(6);
			delomun = (regDatos.getString(7)==null)?"&nbsp;":regDatos.getString(7);
			numdocto = (regDatos.getString(8)==null)?"&nbsp;":regDatos.getString(8);
			fchdocto = (regDatos.getString(9)==null)?"&nbsp;":regDatos.getString(9);
			fchvenc = (regDatos.getString(10)==null)?"&nbsp;":regDatos.getString(10);
			monto = (regDatos.getString(11)==null)?"&nbsp;":regDatos.getString(11);
			moneda = (regDatos.getString(12)==null)?"&nbsp;":regDatos.getString(12);
			campo1 = (regDatos.getString(13)==null)?"&nbsp;":regDatos.getString(13);
			campo2 = (regDatos.getString(14)==null)?"&nbsp;":regDatos.getString(14);
			campo3 = (regDatos.getString(15)==null)?"&nbsp;":regDatos.getString(15);
			campo4 = (regDatos.getString(16)==null)?"&nbsp;":regDatos.getString(16);
			campo5 = (regDatos.getString(17)==null)?"&nbsp;":regDatos.getString(17);
		}

		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText("Nombre:						"+nombre, "formasrep", ComunesPDF.LEFT);
		pdfDoc.addText("RFC:							"+rfc,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Calle:						"+calle,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Colonia:					"+colonia,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("CP.							"+cp,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Estado:						"+estado,"formas",ComunesPDF.LEFT);
		pdfDoc.addText("Del. O Munn.				"+delomun,"formas",ComunesPDF.LEFT);
		String ic_no_campo = "";
		String nombre_campo = "";
		while (regCamposAdicionales.next()){
         System.out.println("::::::::::: (1) regCamposAdicionales ::::::::::: " + regCamposAdicionales.getString("IC_NO_CAMPO"));
			ic_no_campo		=	regCamposAdicionales.getString("IC_NO_CAMPO");
			nombre_campo	=	regCamposAdicionales.getString("NOMBRE_CAMPO"); 
			if (ic_no_campo.equals("1")){
				pdfDoc.addText(nombre_campo+":	"+campo1,"formas",ComunesPDF.LEFT);
			}
			if (ic_no_campo.equals("2")){
				pdfDoc.addText(nombre_campo+":	"+campo2,"formas",ComunesPDF.LEFT);
			}
			if (ic_no_campo.equals("3")){
				pdfDoc.addText(nombre_campo+":	"+campo3,"formas",ComunesPDF.LEFT);
			}
			if (ic_no_campo.equals("4")){
				pdfDoc.addText(nombre_campo+":	"+campo4,"formas",ComunesPDF.LEFT);
			}
			if (ic_no_campo.equals("5")){
				pdfDoc.addText(nombre_campo+":	"+campo5,"formas",ComunesPDF.LEFT);
			}
		}
		pdfDoc.addText (" ","formas",ComunesPDF.CENTER);
		pdfDoc.setTable(2, 30);
		pdfDoc.setCell ("Datos del Documento"               ,"formasmenB"  ,ComunesPDF.CENTER,2);
		pdfDoc.setCell ("No. Documento:"                    ,"formas"      ,ComunesPDF.LEFT);
		pdfDoc.setCell (numdocto                            ,"formas"      ,ComunesPDF.RIGHT);
		pdfDoc.setCell ("Fecha de Emisión:"                 ,"formas"      ,ComunesPDF.LEFT);
		pdfDoc.setCell (fchdocto                            ,"formas"      ,ComunesPDF.RIGHT);
		pdfDoc.setCell ("Fecha de Vencimiento:"             ,"formas"      ,ComunesPDF.LEFT);
		pdfDoc.setCell (fchvenc                             ,"formas"      ,ComunesPDF.RIGHT);
		pdfDoc.setCell ("Importe:"                          ,"formas"      ,ComunesPDF.LEFT);
		pdfDoc.setCell ("$"+Comunes.formatoDecimal(monto,2) ,"formas"      ,ComunesPDF.RIGHT);
		pdfDoc.setCell ("Moneda:"                           ,"formas"      ,ComunesPDF.LEFT);
		pdfDoc.setCell (moneda                              ,"formas"      ,ComunesPDF.RIGHT);
		pdfDoc.addTable(  );
      
      System.out.println("......:::::: Cierra Tabla y crea una nueva :::::..... (" + regCamposDetalle.getNumeroRegistros() + ")");
      
		pdfDoc.addText ("","formas",ComunesPDF.CENTER);
		if (regCamposDetalle != null){
			List lEncabezados = new ArrayList();
			int nocampos=0;
         pdfDoc.setTable(regCamposDetalle.getNumeroRegistros(), 100);
         regCamposDetalle.rewind();
			while(regCamposDetalle.next()){
            System.out.println("::::::::::: (2) regCamposDetalle ::::::::::: " + regCamposDetalle.getString("CG_NOMBRE_CAMPO"));
				//lEncabezados.add(regCamposDetalle.getString("CG_NOMBRE_CAMPO"));
            pdfDoc.setCell (regCamposDetalle.getString("CG_NOMBRE_CAMPO"), "celda01", ComunesPDF.CENTER);
            nocampos++;
            
			}
         /*
         pdfDoc.addText ("","formas",ComunesPDF.CENTER);
         pdfDoc.setTable(nocampos, 100);
			pdfDoc.setTable(lEncabezados, "formasmenB", 100);
         */
			String icDocumento="", icDoctoDetalle  ="", cgCampo1="", cgCampo2="", cgCampo3="", cgCampo4="";
			String cgCampo5   ="", cgCampo6        ="", cgCampo7="", cgCampo8="", cgCampo9="", cgCampo10="", igNumeroDocto="";
			regDatosDetalle.rewind();
			while (regDatosDetalle.next()){
            System.out.println("::::::::::: (3) regDatosDetalle ::::::::::: Campo 1 (" + regDatosDetalle.getString(3)+")");
				icDocumento    = (regDatosDetalle.getString(1) == null) ? "" : regDatosDetalle.getString(1);
				icDoctoDetalle = (regDatosDetalle.getString(2) == null) ? "" : regDatosDetalle.getString(2);
				cgCampo1       = (regDatosDetalle.getString(3) == null) ? "" : regDatosDetalle.getString(3);
				cgCampo2       = (regDatosDetalle.getString(4) == null) ? "" : regDatosDetalle.getString(4);
				cgCampo3       = (regDatosDetalle.getString(5) == null) ? "" : regDatosDetalle.getString(5);
				cgCampo4       = (regDatosDetalle.getString(6) == null) ? "" : regDatosDetalle.getString(6);
				cgCampo5       = (regDatosDetalle.getString(7) == null) ? "" : regDatosDetalle.getString(7);
				cgCampo6       = (regDatosDetalle.getString(8) == null) ? "" : regDatosDetalle.getString(8);
				cgCampo7       = (regDatosDetalle.getString(9) == null) ? "" : regDatosDetalle.getString(9);
				cgCampo8       = (regDatosDetalle.getString(10) == null) ? "" : regDatosDetalle.getString(10);
				cgCampo9       = (regDatosDetalle.getString(11) == null) ? "" : regDatosDetalle.getString(11);
				cgCampo10      = (regDatosDetalle.getString(12) == null) ? "" : regDatosDetalle.getString(12);
				igNumeroDocto  = (regDatosDetalle.getString(13) == null) ? "" : regDatosDetalle.getString(13);
				
				if (nocampos>=1) {pdfDoc.setCell(cgCampo1, "formasmen", ComunesPDF.CENTER);}
				if (nocampos>=2) {pdfDoc.setCell(cgCampo2, "formasmen", ComunesPDF.CENTER);}
				if (nocampos>=3) {pdfDoc.setCell(cgCampo3, "formasmen", ComunesPDF.CENTER);}
				if (nocampos>=4) {pdfDoc.setCell(cgCampo4, "formasmen", ComunesPDF.CENTER);}
				if (nocampos>=5) {pdfDoc.setCell(cgCampo5, "formasmen", ComunesPDF.CENTER);}
				if (nocampos>=6) {pdfDoc.setCell(cgCampo6, "formasmen", ComunesPDF.CENTER);}
				if (nocampos>=7) {pdfDoc.setCell(cgCampo7, "formasmen", ComunesPDF.CENTER);}
				if (nocampos>=8) {pdfDoc.setCell(cgCampo8, "formasmen", ComunesPDF.CENTER);}
				if (nocampos>=9) {pdfDoc.setCell(cgCampo9, "formasmen", ComunesPDF.CENTER);}
				if (nocampos>=10){pdfDoc.setCell(cgCampo10,"formasmen", ComunesPDF.CENTER);}
			}
			pdfDoc.addTable();
		}
		pdfDoc.endDocument();
			
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	System.err.println("Error al generar el archivo " + e);
} finally {
}
%>
<%=jsonObj%>