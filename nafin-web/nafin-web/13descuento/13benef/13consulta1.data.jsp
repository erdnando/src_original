<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8" 
	import="
	java.util.*,
	java.text.SimpleDateFormat,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	com.netro.descuento.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String df_fecha_notMin = (request.getParameter("df_fecha_notMin")!=null)?request.getParameter("df_fecha_notMin"):"";
String df_fecha_notMax = (request.getParameter("df_fecha_notMax")!=null)?request.getParameter("df_fecha_notMax"):"";
String cc_acuse = (request.getParameter("cc_acuse")!=null)?request.getParameter("cc_acuse"):"";
String fechaActual = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());

String ic_if =iNoCliente,  infoRegresar ="", consulta ="";
int start = 0, limit = 0;
			
JSONObject 	jsonObj	= new JSONObject();
HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();

if (informacion.equals("catalogoEPO")  ) {
		
	List resultCatEpo = new ArrayList();
	CatalogoEPO catalogo = new CatalogoEPO();	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setClaveIf(ic_if);	
	resultCatEpo	= catalogo.getListaElementosInfoDoctos();
	
	if(resultCatEpo.size() >0 ){	
		for (int i= 0; i < resultCatEpo.size(); i++) {			
			String registro =  resultCatEpo.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + resultCatEpo.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	


}else  if (informacion.equals("Consultar")  ||  informacion.equals("ConsultarTotales")
	||  informacion.equals("ArchivoCSV")  ||  informacion.equals("ArchivoPDF")  ) {
	
	AvisNotiBenefDE paginador = new AvisNotiBenefDE();
	
	paginador.setClaveEpo(ic_epo);
	paginador.setClaveIF( ic_if);
	paginador.setDf_fecha_notMin(df_fecha_notMin);
	paginador.setDf_fecha_notMax(df_fecha_notMax);
	paginador.setCc_acuse(cc_acuse);
	paginador.setFechaActual(fechaActual);
		
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);	
	
	if (informacion.equals("Consultar")  ||  informacion.equals("ArchivoPDF") ) {
			
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));		
	
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
	}
	
	if (informacion.equals("Consultar")  ) {
		try {
			if (operacion.equals("Generar")) {
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta	
			}		
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);	
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("strPerfil", strPerfil);
		infoRegresar =jsonObj.toString();
		
	}else if (informacion.equals("ArchivoCSV")) {
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}else if (informacion.equals("ArchivoPDF")) {
	
		try {
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}	else if (informacion.equals("ConsultarTotales")) {		//Datos para el Resumen de Totales
		//Debe ser llamado despues de Consulta
		queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion	
	
	}
}

%>

<%=infoRegresar%>