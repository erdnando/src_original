<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8" 
	import="
	java.util.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% 
response.addHeader("Pragma", "No-cache");
response.addHeader("Cache-Control", "no-cache");

/*** OBJETOS ***/
CQueryHelperRegExtJS queryHelper;
JSONObject jsonObj;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String informacion			= (request.getParameter("informacion")==null)?"":request.getParameter("informacion");
String operacion 				= (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
String ic_epo 					= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo").trim();
String ic_docto				= (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto").trim();
String ic_pyme	 				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme").trim();
String ic_estatus_docto 	= (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto").trim();
String ic_moneda 				= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
String no_docto 				= (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
String df_fecha_docto_de	= (request.getParameter("df_fecha_docto_de")==null)?"":request.getParameter("df_fecha_docto_de");
String df_fecha_docto_a 	= (request.getParameter("df_fecha_docto_a")==null)?"":request.getParameter("df_fecha_docto_a");
String fn_monto_de 			= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
String fn_monto_a 			= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
String df_fecha_venc_de 	= (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
String df_fecha_venc_a 		= (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
String resultado 				= null;
String ic_if					= iNoCliente;
int start 						= 0;
int limit						= 0;
AccesoDB con 					= null;
PreparedStatement ps 		= null;
ResultSet rs 					= null;

//ic_docto = "89313";

/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/

/*** INICIO CATALOGO NOMBRE EPO ***/
if(informacion.equals("catalogoNombreEPO")){
	List resultCatEpo = new ArrayList(); 
	CatalogoEPO catalogo = new CatalogoEPO();
	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social"); 
	catalogo.setClaveIf(ic_if);
	resultCatEpo = catalogo.getListaElementosInfoDoctos();
	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", resultCatEpo);
	resultado=jsonObj.toString();		
} /*** FIN CATALOGO NOMBRE EPO ***/

/*** INICIO CATALOGO ESTATUS ***/
else if(informacion.equals("catalogoEstatus")){
	CatalogoSimple catalogo = new CatalogoSimple();
	List listSimple = new ArrayList();
	List catSimple = new ArrayList();
	
	catalogo.setTabla("comcat_estatus_docto");
	catalogo.setCampoClave("ic_estatus_docto");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setOrden("ic_estatus_docto");
	
	listSimple = catalogo.getListaElementos();
	
	for(int x=0; x<listSimple.size(); x++){
		if((x+1)==2 || (x+1)==9 || (x+1)==10){
			catSimple.add(listSimple.get(x));
		}
	}
	
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", catSimple);
	resultado=jsonObj.toString();
} /*** FIN CATALOGO ESTATUS ***/

/*** INICIO CATALOGO MONEDA ***/
else if(informacion.equals("catalogoMoneda")){
	CatalogoMoneda catalogo = new CatalogoMoneda();
	
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setOrden("1");
	
	resultado = catalogo.getJSONElementos();
} /*** FIN CATALOGO MONEDA ***/

/*** INICIO CONSULTA ***/
else if(informacion.equals("Consulta")) { 
	try {
		start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
		limit = Integer.parseInt((request.getParameter("limit")==null)?"15":request.getParameter("limit"));				
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	ConsDoctosBenefDE paginador = new ConsDoctosBenefDE();
	paginador.setNo_docto(no_docto);
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_estatus_docto(ic_estatus_docto);
	paginador.setDf_fecha_docto_de(df_fecha_docto_de);
	paginador.setDf_fecha_docto_a(df_fecha_docto_a);     
	paginador.setDf_fecha_venc_de(df_fecha_venc_de);
	paginador.setDf_fecha_venc_a(df_fecha_venc_a);
	paginador.setFn_monto_de(fn_monto_de);
	paginador.setFn_monto_a(fn_monto_a);
	paginador.setIc_beneficiario(ic_if);
	paginador.setIc_moneda(ic_moneda);
	CQueryHelper queryHelper2 = new CQueryHelper(paginador);
	CQueryHelperRegExtJS queryHelperRegExtJS	= new CQueryHelperRegExtJS( paginador ); 
	
	if((informacion.equals("Consulta") && !operacion.equals("ResumenTotales"))){ 
		try {
			if(operacion.equals("Generar")){ //Nueva consulta
				queryHelperRegExtJS.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			String cadena = queryHelperRegExtJS.getJSONPageResultSet(request,start,limit);			
			resultado	= cadena;
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}
   if(operacion.equals("XLS")){
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelperRegExtJS.getCreateCustomFile(request, strDirectorioTemp,operacion);
		
		System.out.println("\n\n nombreArchivo :::" + nombreArchivo);
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	}else if(operacion.equals("PDF")){
		jsonObj = new JSONObject();
		String nombreArchivo = queryHelperRegExtJS.getCreatePageCustomFile(request, start,limit, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	}else if(operacion.equals("ResumenTotales")){
      paginador = new ConsDoctosBenefDE();
      paginador.setNo_docto(no_docto);
      paginador.setIc_epo(ic_epo);
      paginador.setIc_pyme(ic_pyme);
      paginador.setIc_estatus_docto(ic_estatus_docto);
      paginador.setDf_fecha_docto_de(df_fecha_docto_de);
      paginador.setDf_fecha_docto_a(df_fecha_docto_a);     
      paginador.setDf_fecha_venc_de(df_fecha_venc_de);
      paginador.setDf_fecha_venc_a(df_fecha_venc_a);
      paginador.setFn_monto_de(fn_monto_de);
      paginador.setFn_monto_a(fn_monto_a);
      paginador.setIc_beneficiario(ic_if);
      paginador.setIc_moneda(ic_moneda);
      queryHelper2 = new CQueryHelper(paginador);
      
      Registros vecTotales = queryHelperRegExtJS.getResultCount(  request  );
		HashMap datos;
		List reg=new ArrayList();
      String title = "";
      String label = "";
				
		if(vecTotales.getNumeroRegistros()>0){
         int i = 0, c=0;
         List vecColumnas = null;
         for(i=0; i<vecTotales.getNumeroRegistros(); i++) {
            vecColumnas = vecTotales.getData((i));
            String totalRegistros = vecColumnas.get(0).toString();
            String totalMonto = vecColumnas.get(1).toString();
            String totalMontoDescuento = vecColumnas.get(2).toString();
            String totalMontoNegociable = vecColumnas.get(3).toString();
            String nombreMonedaTotales = vecColumnas.get(4).toString();
                      
            datos=new HashMap();
            datos.put("TITULOFILA","MONTO TOTAL DOCUMENTOS " + nombreMonedaTotales);
            datos.put("TOTALMONTODESCUENTO",totalMonto);
            reg.add(datos);  
            
            datos=new HashMap();
            datos.put("TITULOFILA","TOTAL DOCUMENTOS " + nombreMonedaTotales);
            datos.put("TOTALMONTODESCUENTO",totalRegistros);
            reg.add(datos);     
            
            datos=new HashMap();
            datos.put("TITULOFILA","TOTAL " + nombreMonedaTotales + " SALDO NEGOCIABLE");
            datos.put("TOTALMONTODESCUENTO",totalMontoNegociable);
            reg.add(datos);					
			}
		}
      System.err.println("ResumenTotales");
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		resultado=jsonObj.toString();
	}	
} /*** FIN DE CONSULTA ***/

/*** INICIO DETALLE DATOS DOCTO ***/
else if(informacion.equals("Detalle")) {
	JSONArray reg = new JSONArray();
	String qrySentencia;
	
	try{
		con = new AccesoDB();
		con.conexionDB();
		CQueryHelper queryHelper2 = new CQueryHelper(new ConsDoctosBenefDE());
		
		int total_cd = 0;
		qrySentencia = "select count(*) from comrel_visor " +
				"where ic_epo="+ic_epo+
			 " and ic_producto_nafin=1"+
				" and ic_no_campo between 1 and 5";

		rs = con.queryDB(qrySentencia);
		while(rs.next())
			total_cd = rs.getInt(1);
		con.cierraStatement();

		int[] ic_no_campo = new int[total_cd];
		int[] ig_longitud = new int[total_cd];
		String[] nombre_campo = new String[total_cd];
		String[] cg_tipo_dato = new String[total_cd];
		String campos_dinamicos = "";
		String noms_cad="";
		String cd17="", cd18="", cd19="", cd20="", cd21="";
	
		if(total_cd > 0) {
			int cd = 0;
		  qrySentencia = "SELECT ic_no_campo, ic_epo, INITCAP(cg_nombre_campo), cg_tipo_dato, ig_longitud " +
					"FROM comrel_visor WHERE ic_epo="+ic_epo+
				" AND ic_producto_nafin=1"+
					" AND ic_no_campo BETWEEN 1 AND 5 ORDER BY ic_no_campo";
		  // out.println(query+"<br><hr>");
			rs = con.queryDB(qrySentencia);
         
			while (rs.next()) {
				ic_no_campo[cd] = rs.getInt(1);// out.println("ic_no_campo["+cd+"]: "+ic_no_campo[cd]+"<br>");
				nombre_campo[cd] = rs.getString(3);// out.println("nombre_campo["+cd+"]: "+nombre_campo[cd]+"<br>");
				cg_tipo_dato[cd] = rs.getString(4); //out.println("tipo_dato["+cd+"]: "+tipo_dato[cd]+"<br>");
				ig_longitud[cd] = rs.getInt(5); //out.println("ig_longitud["+cd+"]: "+ig_longitud[cd]+"<br>");
	
				if(ic_no_campo[cd] == 1) { campos_dinamicos+=", D.CG_CAMPO1"; noms_cad+=", "+nombre_campo[cd]+" "; cd17="Si";}
				if(ic_no_campo[cd] == 2) { campos_dinamicos+=", D.CG_CAMPO2"; noms_cad+=", "+nombre_campo[cd]+" "; cd18="Si";}
				if(ic_no_campo[cd] == 3) { campos_dinamicos+=", D.CG_CAMPO3"; noms_cad+=", "+nombre_campo[cd]+" "; cd19="Si";}
				if(ic_no_campo[cd] == 4) { campos_dinamicos+=", D.CG_CAMPO4"; noms_cad+=", "+nombre_campo[cd]+" "; cd20="Si";}
				if(ic_no_campo[cd] == 5) { campos_dinamicos+=", D.CG_CAMPO5"; noms_cad+=", "+nombre_campo[cd]+" "; cd21="Si";}
				//out.println("campos_dinamicos: "+campos_dinamicos+"<br><hr>");
				cd++;
			} // while
			con.cierraStatement();
		} // if
		if(!cd17.equals("Si")) { noms_cad+=",Campo Adicional 1"; }
		if(!cd18.equals("Si")) { noms_cad+=",Campo Adicional 2"; }
		if(!cd19.equals("Si")) { noms_cad+=",Campo Adicional 3"; }
		if(!cd20.equals("Si")) { noms_cad+=",Campo Adicional 4"; }
		if(!cd21.equals("Si")) { noms_cad+=",Campo Adicional 5"; }
		
		queryHelper2.cleanSession(request);

			qrySentencia =
				" SELECT p.cg_razon_social " +
				" 	, p.cg_rfc " +
				" 	, do.cg_calle " +
				" 	, do.cg_colonia " +
				" 	, do.cn_cp " +
				" 	, e.cd_nombre " +
				" 	, do.cg_municipio " +
				" 	, d.ig_numero_docto " +
				" 	, to_char(d.df_fecha_docto,'dd/mm/yyyy') " +
				" 	, to_char(d.df_fecha_venc,'dd/mm/yyyy') " +
				" 	, d.fn_monto " +
				" 	, m.cd_nombre " +
				" 	, d.cg_campo1 " +
				" 	, d.cg_campo2 " +
				" 	, d.cg_campo3 " +
				" 	, d.cg_campo4 " +
				" 	, d.cg_campo5 " +
				" FROM com_documento d " +
				" 	, comcat_moneda m " +
				"  , comcat_pyme p  " +
				"  , com_domicilio do " +
				"  , comcat_estado e " +
				" WHERE d.ic_documento = " + ic_docto  + 
				" 	AND d.ic_moneda = m.ic_moneda " +
				"  AND d.ic_pyme = p.ic_pyme " +
				"  AND p.ic_pyme = do.ic_pyme " +
				"  AND do.cs_fiscal = 'S' " +
				"  AND do.ic_estado = e.ic_estado ";
				
			ps = con.queryPrecompilado(qrySentencia);			
			rs = ps.executeQuery();  
			
			HashMap datos;
			
			String[] arr = new String[14];
			
			arr[9]  = "No. Documento:";
			arr[10] = "Fecha de Emisi�n:";
			arr[11] = "Fecha de Vencimiento:";
			arr[12] = "Importe:";
			arr[13] = "Moneda:";
			int x2 = 0;
			
			if(rs.next()){
				for(int x=1; x<=12; x++){
					if(x<8 || x>=13){
						datos = new HashMap();
						datos.put("DETALLES",  rs.getString(x));
						
						if(x2>4){
							reg.add(datos);
						}
						String det_all = "";
						for(int k=0; k < ic_no_campo.length && x==7; k++) { // Mostrar los Nombres de los campos Adicionales.
                     String detalles = new String();
							if(k==0){ 
								if(cd17.equals("Si") )
									detalles = (rs.getString(13)==null?"&nbsp;":rs.getString(13));
							}
							if(k==1){
								if(cd18.equals("Si"))
									detalles = (rs.getString(14)==null?"&nbsp;":rs.getString(14));
							}
							if(k==2){
								if(cd19.equals("Si"))
									detalles = (rs.getString(15)==null?"&nbsp;":rs.getString(15));
							}
							if(k==3){
								if(cd20.equals("Si"))
									detalles = (rs.getString(16)==null?"&nbsp;":rs.getString(16));
							}
							if(k==4){
								if(cd21.equals("Si"))
									detalles = (rs.getString(17)==null?"&nbsp;":rs.getString(17));
							}
                     
                     det_all += "<div style='border-bottom:dashed 1px #c4c4c4;float:left'><b style='width:100px;float:left'>" + nombre_campo[k] + "</b> <b style='float:left;margin-left:30px'>" + detalles + "</b></div><br /><br />";
                     
                     datos = new HashMap();
							datos.put("DETALLES", "<div style='border-bottom:dashed 1px #c4c4c4;float:left'><b style='width:100px;float:left'>" + nombre_campo[k] + "</b> <b style='float:left;margin-left:30px'>" + detalles + "</b></div>");
							reg.add(datos);
						} // END FOR
						if(x2<=4){
							//datos = new HashMap();
							datos.put("TITLEDATOSDOCTO",  arr[x+8]);
							datos.put("DETALLESDATOSDOCTO",  rs.getString(x+7));
							reg.add(datos);
							x2++;
						}
					}
				}
			}
			datos = new HashMap();
			rs.close();
			ps.close();

	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}	
	
	resultado = "{\"success\": true, \"total\": \"" + reg.size() + "\", \"registros\": " + reg.toString()+"}";;
} /*** FIN DETALLE DATOS DOCTO ***/

/*** INICIO DETALLE CONSULTA ***/
else if(informacion.equals("DetalleConsulta")) {
	JSONArray reg = new JSONArray();
	try{
		con = new AccesoDB();
		con.conexionDB();
		//ic_epo=iNoCliente;
		CQueryHelper queryHelper2 = new CQueryHelper(new ConsDoctosBenefDE());
		queryHelper2.cleanSession(request);

			String qrySentencia = "select INITCAP(cg_nombre_campo) from comrel_visor_detalle " +
            "where ic_epo="+ic_epo+
				" and ic_producto_nafin=1"+
            " and ic_no_campo between 1 and 10 order by ic_no_campo";
			
			
			System.out.println("\n\n\n.:::: qrySentencia ::::." + qrySentencia);
				
			ps = con.queryPrecompilado(qrySentencia);			
			rs = ps.executeQuery();  
			
			HashMap datos;
			
			int cont = 1;
			int cont2 = 0;
			String[] arr = new String [11];
			String arrN = "";
			
			arr[0]="";
			arr[1]="";
			arr[2]="";
			arr[3]="";
			arr[4]="";
			arr[5]="";
			arr[6]="";
			arr[7]="";
			arr[8]="";
			arr[9]="";
			arr[10]="";
			
			while(rs.next()){
				String titulo 	= (rs.getString(1)==null) ? "" : rs.getString(1);
				
				switch(cont){
					case 1: arr[1] = titulo; break;
					case 2: arr[2] = titulo; break;
					case 3: arr[3] = titulo; break;
					case 4: arr[4] = titulo; break;
					case 5: arr[5] = titulo; break;
					case 6: arr[6] = titulo; break;
					case 7: arr[7] = titulo; break;
					case 8: arr[8] = titulo; break;
					case 9: arr[9] = titulo; break;
					case 10: arr[10] = titulo; break;
				}
				cont++;
			}
			
			qrySentencia = "select DD.ic_documento, DD.ic_docto_detalle, DD.cg_campo1, " +
				"DD.cg_campo2, DD.cg_campo3, DD.cg_campo4, DD.cg_campo5, " +
            "DD.cg_campo6, DD.cg_campo7, DD.cg_campo8, DD.cg_campo9, " +
            "DD.cg_campo10, D.ig_numero_docto " +
            "FROM com_documento_detalle DD, com_documento D " +
            "WHERE DD.ic_documento = "+ic_docto+
            " and D.ic_documento = "+ic_docto+
            " and DD.ic_documento = D.ic_documento " +
            "ORDER BY DD.ic_docto_detalle";
				
			System.out.println("\n\n\n.:::: qrySentencia ::::." + qrySentencia);
			
			ps = con.queryPrecompilado(qrySentencia);			
			rs = ps.executeQuery();  
			
			while(rs.next()){
				datos = new HashMap();
				datos.put("DATOS",Integer.toString(cont));
				datos.put("DATO1",arr[1]);
				datos.put("DATO2",arr[2]);
				datos.put("DATO3",arr[3]);
				datos.put("DATO4",arr[4]);
				datos.put("DATO5",arr[5]);
				datos.put("DATO6",arr[6]);
				datos.put("DATO7",arr[7]);
				datos.put("DATO8",arr[8]);
				datos.put("DATO9",arr[9]);
				datos.put("DATO10",arr[10]);
				
				datos.put("CAMPO1",rs.getString(3));
				datos.put("CAMPO2",rs.getString(4));
				datos.put("CAMPO3",rs.getString(5));
				datos.put("CAMPO4",rs.getString(6));
				datos.put("CAMPO5",rs.getString(7));
				datos.put("CAMPO6",rs.getString(8));
				datos.put("CAMPO7",rs.getString(9));
				datos.put("CAMPO8",rs.getString(10));
				datos.put("CAMPO9",rs.getString(11));
				datos.put("CAMPO10",rs.getString(12));
				reg.add(datos);
			}
			rs.close();
			ps.close();
		
		/*	
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		resultado=jsonObj.toString();
		*/
		jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			resultado=jsonObj.toString();
		
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}		
} /*** FIN DETALLE CONSULTA ***/

/*** INICIO MODIFICA ***/
else if(informacion.equals("Modifica")) {
	JSONArray reg = new JSONArray();
	String qrySentencia;
	
	try{
		con = new AccesoDB();
		con.conexionDB();
		qrySentencia = " SELECT to_char(C.dc_fecha_cambio, 'DD/MM/YYYY'), "
					+ " fn_monto_nuevo, fn_monto_anterior, ct_cambio_motivo, "
					+ " C.cc_acuse, TO_CHAR(C.df_fecha_venc_nueva,'dd/mm/yyyy'), "
					+ " TO_CHAR(C.df_fecha_venc_anterior,'dd/mm/yyyy') "
					+ " FROM com_documento D, comhis_cambio_estatus C "
					+ " WHERE D.ic_documento=" + ic_docto
					+ " and D.ic_documento=C.ic_documento and C.ic_cambio_estatus in (8,28) "
					+ " order by C.dc_fecha_cambio ";
					
			ps = con.queryPrecompilado(qrySentencia);			
			rs = ps.executeQuery();  
			
			HashMap datos;
			
			while(rs.next()){
				datos = new HashMap();
				datos.put("F_MOV",		rs.getString(1));
				datos.put("MONTO",		rs.getString(2));
				datos.put("MONTO_ANT",	rs.getString(3));
				datos.put("CAUSA",		rs.getString(4));
				datos.put("NUM_ACUSE",	rs.getString(5));
				datos.put("F_VENC",		rs.getString(6));
				datos.put("F_VENC_ANT",	rs.getString(7));
				reg.add(datos);
			}
			
			rs.close();
			ps.close();

	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}	
	
	resultado = "{\"success\": true, \"total\": \"" + reg.size() + "\", \"registros\": " + reg.toString()+"}";;
} /*** FIN MODIFICA ***/


%>
<%= resultado%>
