Ext.onReady(function() {

var correoRepresentanteLegal = ' No se carg� correctamente ';

   Ext.getBody().mask('Inicializando Pantalla ...');
   var obtieneParametroInicial = {
      procesarObtieneParametroInicial : function(  response, opts ) { 
         
         Ext.getCmp('contenedorPrincipal').show();
         Ext.getBody().unmask();
         
         if (Ext.util.JSON.decode(response.responseText).success == true) {
            var fmpForma = Ext.getCmp('forma');
            var csActivaServicio = Ext.getCmp('servicio');
            correoRepresentanteLegal = Ext.util.JSON.decode(response.responseText).correoRepresentanteLegal;
            
            Ext.getCmp('correoRepresentanteLegal').setText('<span style="font-weight:bold;text-decoration: underline">'+correoRepresentanteLegal+'</span> la informaci�n de las publicaciones que las<br />', false);
            
            if(   Ext.util.JSON.decode(response.responseText).registros=='S'  ){
               csActivaServicio.items.items[0].setValue(true);
            }else if(   Ext.util.JSON.decode(response.responseText).registros=='N'  ){
               csActivaServicio.items.items[1].setValue(true);
            }
         }
      },
      
      obtieneParametroFnAjax : function(){
         Ext.Ajax.request({
            url: '13AvisoPublicacionCorreoext.data.jsp',
            params: {
               informacion:'Inicializar'
            },
            failure: function(response, opts) {         
               Ext.Msg.show({
                  title    :  '('+response.status+') ' + response.statusText,
                  msg      :  "No se logr� cargar la pantalla correctamente...",
                  icon     :  Ext.Msg.ERROR,
                  buttons  :  Ext.Msg.OK
               }); 
            },
            success: this.procesarObtieneParametroInicial
         });
      }
   }
   
   obtieneParametroInicial.obtieneParametroFnAjax();
   
	var fp = {
		xtype: 'form',
		id: 'forma',
		title: 'Aviso de publicaci�n por Correo',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 600,
		titleCollapse: false,
		bodyStyle: 'text-align:center;font-size:13px;padding: 10px 6px 6px 6px; color: #2E6398;',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[{
         xtype: 'label', html: '<b>Estimado USUARIO de CADENAS PRODUCTIVAS</b><br /><br />'
      },{
			xtype:'label', html: 'Recibe de manera gratuita en tu cuenta de correo<br />'
		},{
			xtype:'label', html: 'electr�nico que tienes con nosotros <br />'
		},{
			xtype:'label', id: 'correoRepresentanteLegal',  html: '<span style="font-weight:bold;text-decoration: underline">'+correoRepresentanteLegal+'</span> la informaci�n de las publicaciones que las<br />'
		},{
			xtype:'label', html: 'publicaciones que las EPOs den de alta.<br />'
		},{
			xtype: 'radiogroup', fieldLabel:'', id:'servicio', style:'margin: 50px 0px 0px -30px',
			defaults: {xtype: "radio",name: "csActivaServicio"},
			items:[{
				boxLabel: 'Servicio Activo',
				inputValue: 'S'
			},{
				boxLabel: 'Servicio Inactivo',
				inputValue: 'N'
			}]
		},{
			buttons:[{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: '',
				handler: function(){
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					Ext.Ajax.request({
						url: '13AvisoPublicacionCorreoext.data.jsp',
						params: Ext.apply(paramSubmit,{
							informacion: 'Guardar'	
						}),
						callback: function(){
							window.location = '13AvisoPublicacionCorreoext.jsp'
						}
					});
				}
			}]
		}]
	};//FIN DE LA FORMA
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
      hidden   : true,
      style: 'font-size:15px',
		width: 940,
		height: 'auto',
		items: [fp]
	});
   
});