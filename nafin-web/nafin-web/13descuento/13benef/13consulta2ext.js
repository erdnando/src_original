Ext.onReady(function() {

var ic_if =  Ext.getDom('ic_if').value;
var ic_epo = '';
var decide = 0;

	var busqAvanzadaSelect=0;
	Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);
	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
	var procesarConsultaDataDetalles = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridDatosDoctoConsulta.isVisible()) {
				gridDatosDoctoConsulta.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var cm = gridDatosDoctoConsulta.getColumnModel();
			
				if(store.getAt(0).get('DATO1')!=""){	
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(1,store.getAt(0).get('DATO1'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(1, false);
				}
				if(store.getAt(0).get('DATO2')!=""){					
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(2,store.getAt(0).get('DATO2'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);
				}
				if(store.getAt(0).get('DATO3')!=""){					
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(3,store.getAt(0).get('DATO3'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);
				}
				if(store.getAt(0).get('DATO4')!=""){					
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(4,store.getAt(0).get('DATO4'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);
				}
				if(store.getAt(0).get('DATO5')!=""){					
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(5,store.getAt(0).get('DATO5'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}
				if(store.getAt(0).get('DATO6')!=""){					
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(6,store.getAt(0).get('DATO6'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO6'), false);
				}
				if(store.getAt(0).get('DATO7')!=""){					
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(7,store.getAt(0).get('DATO7'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO7'), false);
				}
				if(store.getAt(0).get('DATO8')!=""){					
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(8,store.getAt(0).get('DATO8'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO8'), false);
				}
				if(store.getAt(0).get('DATO9')!=""){					
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(9,store.getAt(0).get('DATO9'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO9'), false);
				}
				if(store.getAt(0).get('DATO10')!=""){					
					gridDatosDoctoConsulta.getColumnModel().setColumnHeader(10,store.getAt(0).get('DATO10'));
					gridDatosDoctoConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO10'), false);
				}
				
			
			var btnBajarPDFDetalles = Ext.getCmp('btnBajarPDFDetalles');
			var btnGenerarPDFDetalles = Ext.getCmp('btnGenerarPDFDetalles');
			
			var el = gridDatosDoctoConsulta.getGridEl();						
			if(store.getTotalCount() > 0) {			
				btnGenerarPDFDetalles.enable();			
				btnBajarPDFDetalles.hide();	
				el.unmask();
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	
	
	var procesarConsultaDataModifica = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridModifica.isVisible()) {
				gridModifica.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var cm = gridModifica.getColumnModel();
			
			var btnBajarPDFDetalles = Ext.getCmp('btnBajarPDFModifica');
			var btnGenerarPDFDetalles = Ext.getCmp('btnGenerarPDFModifica');
			
			var el = gridModifica.getGridEl();						
			if(store.getTotalCount() > 0) {			
				btnGenerarPDFDetalles.enable();			
				btnBajarPDFDetalles.hide();	
				el.unmask();
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
   var procesarConsultaDataTotales = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('gridTotales');
      
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			
			var el = grid.getGridEl();
				
			if(store.getTotalCount() > 0) {
				gridTotales.show();
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
   
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		var nompyme = Ext.getCmp('nompyme');
		
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var gridTotales = Ext.getCmp('gridTotales')
	
			var el = grid.getGridEl();
				
			if(store.getTotalCount() > 0) {
            var cmpForma = Ext.getCmp('forma');
            paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
            resumenTotalesData.load({
               params: Ext.apply(paramSubmit,{
                  informacion:'Consulta',
                  operacion: 'ResumenTotales'
               })
            });
				gridTotales.show();
				
				//Ext.getCmp('gridDatosDoctoConsulta').setHeader(store.getAt(0)).get('TITULODOCTO');
				
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnBajarPDF').hide();
				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
		}
	}
	var bajarContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();			
		} else {
			alert("error");
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesarMuestraPanelModifica(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var dataModifica = Ext.util.JSON.decode(response.responseText);
			
			if (dataModifica.datosModifica != undefined)	{
				gridModificaData.loadData(dataModifica.datosModifica);
			}

			var ventana = Ext.getCmp('winModifica');
			if (ventana) {
				Ext.getCmp('btnPdfModifica').enable();
				Ext.getCmp('btnBajarPdfModifica').hide();
				ventana.show();
				var storeGrid = gridModificaData.data;
				if(storeGrid.length < 1) {
					gridModifica.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('btnPdfModifica').disable();
				}
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Procesar Generar archivo Detalles
	var procesarSuccessFailureGenerarPDFDetalles =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFDetalles');
		btnGenerarPDF.setIconClass('icoPdf');

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFDetalles');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPDFModifica =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFModifica');
		btnGenerarPDF.setIconClass('icoPdf');

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFModifica');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
	
	var procesarConsultaDataDetalleTop = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		var jsonData = store.reader.jsonData;	
		var cm = gridDatosDoctoConsulta.getColumnModel();
		var nompyme = Ext.getCmp('LblNomPyme');
		
		nompyme.setValue(store.getAt(0).get('DETALLES'));
	}
	
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta2ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBREEPO'},
			{name: 'NOMBREPYME'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'DF_FECHA_DOCTO'},
			{name: 'DF_FECHA_VENC'},
			{name: 'CD_NOMBRE'},
			{name: 'TIPOFACTORAJE'},
			{name: 'FN_MONTO'},
			{name: 'FN_PORC_ANTICIPO'},
			{name: 'FN_MONTO_DSCTO'},
			{name: 'CD_DESCRIPCION'},
			{name: 'CT_REFERENCIA'},
			{name: 'BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'FN_IMPORTE_RECIBIR_BENEF'},
			{name: 'CS_DETALLE'},
			{name: 'CS_CAMBIO_IMPORTE'},
			{name: 'IC_EPO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var resumenTotalesData = new Ext.data.JsonStore({		
		root : 'registros',
		url : '13consulta2ext.data.jsp',
		baseParams: {
			informacion: 'Consulta',
			operacion: 'ResumenTotales'
		},
		fields: [
			{name: 'TOTALMONTO',  type: 'float',mapping: 'TOTALMONTO'},
			{name: 'TOTALMONTODESCUENTO',  type: 'float', mapping: 'TOTALMONTODESCUENTO'},
			{name: 'TOTALMONTONEGOCIABLE',type: 'float',  mapping: 'TOTALMONTONEGOCIABLE'}			,
			{name: 'TITULOFILA',type: 'string',  mapping: 'TITULOFILA'}			
		],
		totalProperty : 'total', 
		listeners: {
         load: procesarConsultaDataTotales,
			exception: NE.util.mostrarDataProxyError
		}		
	});
	var catalogoNombreEPO = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '13consulta2ext.data.jsp',
		listeners: {		
			//load: procesarCatalogoNombreEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoNombreEPO'			
		},
		totalProperty : 'total',
		autoLoad: true
	});
	var catalogoEstatus = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '13consulta2ext.data.jsp',
		listeners: {		
			//load: procesarCatalogoNombreEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoEstatus'			
		},
		totalProperty : 'total',
		autoLoad: true
	});
	var catalogoMoneda = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '13consulta2ext.data.jsp',
		listeners: {		
			//load: procesarCatalogoNombreEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoMoneda'			
		},
		totalProperty : 'total',
		autoLoad: true
	});
	
	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion','ic_pyme','loadMsg'],
		url : '13consulta2ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	
var storeDetalle = new Ext.data.JsonStore({
   root : 'registros',
		url : '13consulta2ext.data.jsp',
		baseParams: {
			informacion: 'Detalle'
		},
		fields: [
			{name: 'DETALLES',  type: 'string',mapping: 'DETALLES'},	
			{name: 'TITLEDATOSDOCTO',  type: 'string',mapping: 'TITLEDATOSDOCTO'},
			{name: 'DETALLESDATOSDOCTO',  type: 'string',mapping: 'DETALLESDATOSDOCTO'}	
		],
		totalProperty : 'total'
});

	var storeDetalleConsulta = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta2ext.data.jsp',
		baseParams: {
			informacion: 'DetalleConsulta'
		},
		fields: [
			{name: 'DATO1'},
			{name: 'DATO2'},
			{name: 'DATO3'},
			{name: 'DATO4'},
			{name: 'DATO5'},
			{name: 'DATO6'},
			{name: 'DATO7'},
			{name: 'DATO8'},
			{name: 'DATO9'},
			{name: 'DATO10'},
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'CAMPO5'},
			{name: 'CAMPO6'},
			{name: 'CAMPO7'},
			{name: 'CAMPO8'},
			{name: 'CAMPO9'},
			{name: 'CAMPO10'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataDetalles,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

var gridModificaData = new Ext.data.JsonStore({		
		root : 'registros',
		url : '13consulta2ext.data.jsp',
		baseParams: {
			informacion: 'Modifica'
		},
		fields:	[{name:'F_MOV',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'MONTO',type: 'float'},
					{name: 'MONTO_ANT',	type: 'float'},
					{name: 'F_VENC',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'F_VENC_ANT',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'CAUSA'},
					{name: 'NUM_ACUSE'}],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataModifica,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
/******************************************************************************/
var procesarSuccessFailureEditables =  function(opts, success, response) {
	//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		var jsonObj = Ext.util.JSON.decode(response.responseText)
							
		grid.hide();	
		var formaDatos = Ext.getCmp('formaDatos');		
		
		if(!formaDatos.isVisible()) 	{
			formaDatos.show();
		}			
		publicacion  = jsonObj.publicacion;	
		camposEditables  = jsonObj.camposEditables;	
		consultaDataEditables.fields =  jsonObj.columnasStore;
		consultaDataEditables.data =  jsonObj.columnasRecords;
		gridDatos.columns = jsonObj.columnasGrid;	
		noCampos =  jsonObj.noCampos;	
		campoFechas = jsonObj.campoFechas;							 
			
		formaDatos.add(gridDatos);
		formaDatos.el.unmask();
		formaDatos.doLayout();	
			
		var noEditables  = jsonObj.noEditables;	
		if(noEditables>0){
			Ext.getCmp('btnGuardar').hide();
		}
			
		//esto es para formar el grid de los totales
		Ext.Ajax.request({
			url: '13consulta2ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'DATOSTOTALES',
				publicacion: publicacion
			}),
			callback: procesarSuccessFailureTotales
		});	
	} else {		
		NE.util.mostrarConnError(response,opts);
	}		
}
/*******************************************************************************/

//para procesar los datos editables 
	var VerdatosT = function(grid, rowIndex, colIndex, item, event) {	
		var registro = grid.getStore().getAt(rowIndex);
		var publicacion = registro.get('IC_EPO');
		Ext.Ajax.request({
			url: '13consulta2ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'DetalleConsulta',
				ic_epo: publicacion
			}),
			callback: procesarSuccessFailureEditables
		});	
	}


var gridDatosDocto = new Ext.list.ListView({
   store: storeDetalle,
	hideHeaders:true,
   emptyText: '',
   reserveScrollOffset: true,
   columns: [{
		header: '',
      dataIndex: 'TITLEDATOSDOCTO'
    },{
      header: '',
      dataIndex: 'DETALLESDATOSDOCTO'
    }]
});

	var gridModifica = new Ext.grid.GridPanel({
		id: 'gridModifica',
		store: gridModificaData,
		columns: [
			{header: 'Fecha Movimiento',tooltip: 'Fecha Movimiento',	dataIndex: 'F_MOV',menuDisabled:true,	sortable : false, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Monto',tooltip: 'Monto',	dataIndex: 'MONTO',menuDisabled:true,	width: 100,sortable : false,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Anterior',tooltip: 'Monto Anterior',	dataIndex: 'MONTO_ANT',menuDisabled:true,sortable : false,	width: 100,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Fecha de Vencimiento',tooltip: 'Fecha de Vencimiento',	dataIndex: 'F_VENC',menuDisabled:true,	sortable : false, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Fecha de Vencimiento Anterior',tooltip: 'Fecha de Vencimiento Anterior',	dataIndex: 'F_VENC_ANT',menuDisabled:true,	sortable : false, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Causa',tooltip: 'Causa',	dataIndex: 'CAUSA',menuDisabled:true,sortable : false,	width: 100,	align: 'left'},
			{header: 'N�mero de Acuse',tooltip: 'Acuse',	dataIndex: 'NUM_ACUSE',menuDisabled:true,sortable : false,	width: 150,	align: 'center'}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 200,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});
	
var gridDatosDoctoConsulta = new Ext.grid.GridPanel({ 
   store: storeDetalleConsulta,
	id: 'gridDatosDoctoConsulta',
	margins: '20 0 0 0',
	hidden: true,
	title: 'Consulta Notificaciones',
	autoScroll: true, height: 150,
	stripeRows: true,
	loadMask: true,
	deferRowRender: false,
	clicksToEdit:1,
	//view:new Ext.grid.GridView({forceFit:true}),
   columns: [{
		header: 'Datos', dataIndex: 'DATOS',
		menuDisabled:true,
		hidden: true
	},{
		header: 'DATO1', dataIndex: 'CAMPO1',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	},{
		header: 'DATO2', dataIndex: 'CAMPO2',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	},{
		header: 'DATO3', dataIndex: 'CAMPO3',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	},{
		header: 'DATO4', dataIndex: 'CAMPO4',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	},{
		header: 'DATO5', dataIndex: 'CAMPO5',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	},{
		header: 'DATO6', dataIndex: 'CAMPO6',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	},{
		header: 'DATO7', dataIndex: 'CAMPO7',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	},{
		header: 'DATO8', dataIndex: 'CAMPO8',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	},{
		header: 'DATO9', dataIndex: 'CAMPO9',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	},{
		header: 'DATO10', dataIndex: 'CAMPO10',
		menuDisabled:true,
		sortable: false,
		width: 80, resizable: true,
		align: 'left',
		hidden: true
	}]
});

var gridTotales  = new Ext.grid.GridPanel({ 
		id: 'gridTotales',
		xtype:'grid',
		title: 'Totales',
		hidden: true,
      stripeRows: true,
      loadMask: true,
      deferRowRender: false,
		hideHeaders:true,
		collapsible:true,
      columnLines: true,
		align: 'center',
		store: resumenTotalesData,
			columns: [	
				{
					dataIndex: 'TITULOFILA',
					align: 'right',
					width: 350
				},
				{
					dataIndex: 'TOTALMONTODESCUENTO',
					width: 140,
					align: 'right',
					renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
						var str = Ext.util.Format.usMoney(record.get('TOTALMONTODESCUENTO'));
						if(rowIdx==0) decide=1;
						
						if(decide==rowIdx){
							str = record.get('TOTALMONTODESCUENTO');
							decide += 3;
						}
						
						return str;
					}
				}
			],
			height: 155,		
			width: 495,
			style: 'margin:0 auto',
         viewConfig: {
            getRowClass: function(record, index) {
               var c = record.get('change');
               if (c < 0) {
                  return 'price-fall';
               } else if (c > 0) {
                  return 'price-rise';
               }
            }
         }
		});		

var listView = new Ext.list.ListView({
   store: storeDetalle,
	hideHeaders:true,
   reserveScrollOffset: true,
   columns: [{
		dataIndex: 'DETALLES'
   }]
});

var elementosModifica = [
		{
			xtype: 'container',
			layout:'column',
				items:[{
					xtype: 'panel',id:	'izqModifica',
					width:	200,
					border:false,
					items: [{xtype: 'displayfield', value:''}]
				},{
					xtype: 'panel',	id:	'centroModifica',
					width:	400,
					layout: 'form',
					bodyStyle: 'padding: 2px',
					border: false,
					items: [
						{
							xtype: 'displayfield',	id: 'disProv',		fieldLabel: 'Proveedor',				value: ''
						},{
							xtype: 'displayfield',	id: 'disNDoc',		fieldLabel: 'Numero de Documento',value: ''
						},{
							xtype: 'displayfield',	id: 'disFEmi',	fieldLabel: 'Fecha de Emision',	value: ''
						},{
							xtype: 'displayfield',	id: 'disFVen',	fieldLabel: 'Fecha de Vencimiento',		value: ''
						},{
							xtype: 'displayfield',	id: 'disMone',fieldLabel: 'Moneda',	value: ''
						},{
							xtype: 'displayfield',	id: 'disMont',	fieldLabel: 'Monto',					value: ''
						},{
							xtype: 'displayfield',	id: 'disEsta',	fieldLabel: 'Estatus',						value: ''
						}
					]
			},{
					xtype: 'panel',id:	'derModifica',
					width:	180,
					border:false,
					items: []
			}]
		}
	];	
	
	
	var fpModifica=new Ext.FormPanel({
		autoHeight:true,
		width: 790,
		border: true,
		labelWidth: 150,
		items: elementosModifica,
		hidden: false
	});
	
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		clicksToEdit:1,
		columns: [{
				header: 'Nombre de la EPO', tooltip: 'Nombre de la EPO',
				menuDisabled:true,
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 200, resizable: true,
				align: 'left'
			},{
				header: 'Nombre del Proveedor', tooltip: 'Nombre del Proveedor',
				menuDisabled:true,
				dataIndex: 'NOMBREPYME',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'left'
			},{
				header: 'N�mero Documento', tooltip: 'N�mero de Documento',
				menuDisabled:true,
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'left'
			},{
				header: 'Fecha Emisi�n', tooltip: 'Fecha de Emisi�n',
				menuDisabled:true,
				dataIndex: 'DF_FECHA_DOCTO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center'
			},{
				header: 'Fecha Vencimiento', tooltip: 'Fecha de Vencimiento',
				menuDisabled:true,
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center'
			},{
				header: 'Moneda', tooltip: 'Moneda',
				menuDisabled:true,
				dataIndex: 'CD_NOMBRE',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'left'
			},{
				header: 'Tipo Factoraje', tooltip: 'Tipo Factoraje',
				menuDisabled:true,
				dataIndex: 'TIPOFACTORAJE',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'left'
			},{
				header: 'Monto', tooltip: 'Monto',
				menuDisabled:true,
				dataIndex: 'FN_MONTO',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'right',
				renderer : 'usMoney'
			},{
				header: 'Porcentaje Descuento', tooltip: 'Porcentaje de Descuento',
				menuDisabled:true,
				dataIndex: 'FN_PORC_ANTICIPO',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'center',
				renderer: function(value){
					return value + ' %';
				}
			},{
				header: 'Monto a Descontar', tooltip: 'Monto a Descontar',
				menuDisabled:true,
				dataIndex: 'FN_MONTO_DSCTO',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'right',
				renderer : 'usMoney'
			},{
				header: 'Estatus', tooltip: 'Estatus',
				menuDisabled:true,
				dataIndex: 'CD_DESCRIPCION',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'center'
			},{
				header: 'Referencia', tooltip: 'Referencia',
				menuDisabled:true,
				dataIndex: 'CT_REFERENCIA',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'left'
			},{
				header: 'Beneficiario', tooltip: 'Beneficiario',
				menuDisabled:true,
				dataIndex: 'BENEFICIARIO',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'left'
			},{
				header: '% Beneficiario', tooltip: '% Beneficiario',
				menuDisabled:true,
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center'
			},{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario',
				menuDisabled:true,
				dataIndex: 'FN_IMPORTE_RECIBIR_BENEF',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'right',
				renderer : 'usMoney'
			},{
				xtype: 'actioncolumn',
				menuDisabled:true,
				header: 'Detalles', tooltip: 'Detalles',
				dataIndex: 'CS_DETALLE',
				width: 60, resizable: true,
				align: 'center', 
				renderer: function(value){ return value == 'S' ? 'Ver ' : '';  },
				items: [
					{
						getClass: function(value) { return value=='Ver ' ? 'icoBuscar' : ''; },
						tooltip: 'Ver Detalle',
						handler: function(grid, rowIdx, colIds){
												
							var reg = grid.getStore().getAt(rowIdx);
							var fp = Ext.getCmp('forma');
							ic_documento = reg.get('IC_DOCUMENTO');
                     ic_epo   =  reg.get('IC_EPO');
						
							paramSubmit = (fp)?fp.getForm().getValues():{};
							storeDetalle.load({
								params:Ext.apply(paramSubmit,{
									ic_docto : reg.get('IC_DOCUMENTO'),	
									ic_epo : ic_epo
								})
							})
						
							paramSubmit = (fp)?fp.getForm().getValues():{};
							storeDetalleConsulta.load({
								params:Ext.apply(paramSubmit,{
									ic_docto : reg.get('IC_DOCUMENTO'),	
									ic_epo : ic_epo		
								})
							})
						
							winDetalle.show(); 					
						}				
					}
				]
			},{
				xtype: 'actioncolumn',
				menuDisabled:true,
				header: 'Modificaci�n de Montos y/o Fechas', tooltip: 'Modificaci�n de Montos y/o Fechas',
				dataIndex: 'CS_CAMBIO_IMPORTE',
				width: 100, resizable: true,
				align: 'left', 
				renderer: function(value){ return value=='S' ? 'Ver ' : ''; },
				items: [
					{ 
						getClass: function(value) { return value=='Ver ' ? 'icoBuscar' : ''; },
						tooltip: 'Descragar Clausulado',
						handler: function(grid, rowIdx, colIds){
							var reg = grid.getStore().getAt(rowIdx);
							var fp = Ext.getCmp('forma');
							
							ic_documento = reg.get('IC_DOCUMENTO');
							desc_pyme = reg.get('NOMBREPYME');
							ig_numero_docto = reg.get('IG_NUMERO_DOCTO');
							fch_emision = reg.get('DF_FECHA_DOCTO');
							fch_venc =  reg.get('DF_FECHA_VENC');
							moneda = reg.get('CD_NOMBRE');
							monto = reg.get('FN_MONTO');
							estatus = reg.get('CD_DESCRIPCION');
							
							Ext.getCmp('disProv').setValue(desc_pyme);
							Ext.getCmp('disNDoc').setValue(ig_numero_docto);
							Ext.getCmp('disFEmi').setValue(fch_emision);
							Ext.getCmp('disFVen').setValue(fch_venc);
							Ext.getCmp('disMone').setValue(moneda);
							Ext.getCmp('disMont').setValue(Ext.util.Format.number(monto,'$ 0,0.00'));
							Ext.getCmp('disEsta').setValue(estatus);
						
							paramSubmit = (fp)?fp.getForm().getValues():{};
							gridModificaData.load({
								params:Ext.apply(paramSubmit,{
									ic_docto : reg.get('IC_DOCUMENTO'),	
									ic_epo : reg.get('IC_EPO')	
								})
							})
							winFpModifica.show(); 					
						}	
				}
			]
			}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 910,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
		xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta2ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'XLS',
								informacion: 'Consulta'
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta2ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'PDF',
								informacion: 'Consulta'
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		},
		title:''
	};
	
	
		
	var panDetalle = new Ext.Panel({
      width: 687,
      height: 400,
      layout: 'border',
      items: [
			{
         region: 'south',     // position for region
         xtype: 'panel',
         height: 100,
         split: true,         // enable resizing
         margins: '0 5 5 5',
			items:[gridDatosDoctoConsulta]
        },{
         // xtype: 'panel' implied by default
         title: '',
         region:'west',
         xtype: 'panel',
         margins: '5 0 0 5',
         width: 400,
         collapsible: false,   // make collapsible
         id: 'west-region-container',
			items: [listView]
        },{
         title: 'Datos del Documento',
         region: 'center',     // center region is required, no width/height specified
         xtype: 'panel',
			width:220,
         layout: 'fit',
         margins: '5 10 0 5',
			items: [gridDatosDocto]
        }
		],bbar: [
				'->',	
				'-',{
			xtype:'button',
			text: 'Cerrar',
			scope: this,
         handler: function(){
				Ext.getCmp('winDetalle').hide();
			}
      },{
			xtype:'button',
			text: 'Imprimir',
			id: 'btnGenerarPDFDetalles',
			iconCls: 'icoPdf',
         handler: function(boton, evento){
				boton.disable();
				boton.setIconClass('loading-indicator');
				var cmpForma = Ext.getCmp('forma');
				paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
				Ext.Ajax.request({
					url: '13consulta2ext_det_pdf.jsp',
					params: Ext.apply(paramSubmit,{
						ic_docto : ic_documento,
                  ic_epo   :  ic_epo
					}),
					callback: procesarSuccessFailureGenerarPDFDetalles
				});
			}
      },
		{
			xtype: 'button',
			text: 'Bajar PDF',
			id: 'btnBajarPDFDetalles',
			hidden: true
		}]
	});
	
	var winDetalle = new Ext.Window({  
		id:'winDetalle',
		title: 'Detalle del Documento', 
		closable:false,
		modal: true,
		resizable: false,
		constrain:true,
		width: 700,
		height: 500,  
		autoHeight:true,
		minimizable: false,  
		maximizable: false,  
		items:[panDetalle]
	});
	
	var winFpModifica = new Ext.Window({  
		id:'winFpModifica',
		closable:false,
		modal: true,
		resizable: false,
		constrain:true,
		width: 760,
		height: 500,  
		autoHeight:true,
		minimizable: false,  
		maximizable: false,  
		items:[fpModifica,NE.util.getEspaciador(10),gridModifica],
		bbar: {
		xtype: 'toolbar',
			buttons: [
				'->','-',
				{
					xtype: 'button',	
					text: 'Cerrar',
					scope: this,
					handler: function(){
						Ext.getCmp('winFpModifica').hide();
					}
				},{
					xtype:'button',
					text: 'Imprimir',
					id: 'btnGenerarPDFModifica',
					iconCls: 'icoPdf',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta2ext_mod_pdf.jsp',
							params: Ext.apply(paramSubmit,{
								ic_docto : ic_documento,
								nombreProv : desc_pyme,
								ig_numero_docto : ig_numero_docto,
								fch_emision : fch_emision,
								fch_venc : fch_venc,
								moneda : moneda,
								monto : monto,
								estatus : estatus
								
							}),
							callback: procesarSuccessFailureGenerarPDFModifica
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFModifica',
					hidden: true
				}
			]
		}
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'noIc_epo',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO',
			emptyText: 'Seleccione EPO',
			store: catalogoNombreEPO,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			width:300
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Emisi�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_docto_de',
					id: 'df_fecha_docto_de',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_docto_a',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					formBind: true
					},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_docto_a',
					id: 'df_fecha_docto_a',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_fecha_docto_de',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_de',
					id: 'df_fecha_venc_de',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_venc_a',
					margins: '0 20 0 0' , //necesario para mostrar el icono de error
					formBind: true
					},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_a',
					id: 'df_fecha_venc_a',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_fecha_venc_de',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: 'Monto',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'textfield',
					name: 'fn_monto_de',
					id: 'fn_monto_de',
					margins: '0 20 0 0',
               width: 100
					},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'textfield',
					name: 'fn_monto_a',
					id: 'fn_monto_a',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
               width: 100
				}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: 'Estatus',
			combineErrors: false,
			msgTarget: 'side',
			width: 300,
			items: [{
				xtype: 'combo',
				name: 'ic_estatus_docto',
				id: 'id_ic_estatus_docto',
				hiddenName : 'ic_estatus_docto',
				emptyText: 'Seleccione Estatus',
				store: catalogoEstatus,
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				width:130
			}]
	},{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 300,
			items: [{
				xtype: 'combo',
				name: 'ic_moneda',
				id: 'id_ic_moneda',
				hiddenName : 'ic_moneda',
				emptyText: 'Seleccione Moneda',
				store: catalogoMoneda,
				mode: 'local',
				displayField: 'descripcion',
				valueField: 'clave',
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				width:130
			}]
	},{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Documento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [{
				xtype: 'textfield',
				name: 'no_docto',
				id: 'no_docto',
				hiddenName : 'no_docto',
				width: 100
			}]
	}];
	//Forma para hacer la busqueda filtrada
   
	var fp = {
		xtype: 'form',
		id: 'forma',
		title: 'Informaci�n de Documentos',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 600,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosForma],
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
					var cmpForma = Ext.getCmp('forma');
					var fp = Ext.getCmp('forma');
               gridTotales.hide();
               
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					consultaDataGrid.load({
						params: Ext.apply(paramSubmit,{
						operacion: 'Generar',
						start:0,
						limit:15							
					})
				});
				
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta2ext.jsp';
				}
			}
		]
	};//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10),
			gridTotales
		]
	});
});
