Ext.onReady(function() {

var vusuario = Ext.getDom('strUsr').value;
var vnombreusr = Ext.getDom('strNombreUsr').value;
var vcorreousr = Ext.getDom('strCorreoUsr').value;

//-------------------
var procesarConsultaData = function(store, arrRegistros, opts) {
	//var fp = Ext.getCmp('forma');
	//fp.el.unmask();
	
	var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
	var panelContenido = Ext.getCmp('panelContenido1');
	if (arrRegistros != null) {
		if (!gridResDoctos.isVisible()) {
			panelContenido.add(gridResDoctos);
			panelContenido.doLayout();
		}
		
		var el = gridResDoctos.getGridEl();
		if(store.getTotalCount() > 0) {
			el.unmask();
		}else {
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
		
	}
}


//-------------------

var storeDoctosData = new Ext.data.JsonStore({
	root : 'registros',
	url : NE.appWebContextRoot+'/13descuento/13forma5_ext.data.jsp',
	baseParams: {
		informacion: 'resumenDoctos'
	},
	fields: [
		{name: 'NOMBREEPO'},
		{name: 'DESCESPECIAL'},
		{name: 'NUMDOCTOMN'},
		{name: 'MONTOMN'},
		{name: 'LINKMN'},
		{name: 'NUMDOCTODA'},
		{name: 'MONTODA'},
		{name: 'LINKDL'},
		{name: 'TIPOFACTORAJE'},
		{name: 'CVEEPO'}
	],
	totalProperty : 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
			fn: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				//LLama procesar consulta, para que desbloquee los componentes.
				procesarConsultaData(null, null, null);
			}
		}
	}
	
});


var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: 'Resumen', colspan: 2, align: 'center'},
				{header: 'Moneda Nacional', colspan: 3, align: 'center'},
				{header: 'D�lares', colspan: 3, align: 'center'}
			]
		]
	});
	

var gridResDoctos = new Ext.grid.GridPanel({
	id: 'gridResDoctos1',
	store: storeDoctosData,
	margins: '20 0 0 0',
	columns: [
		{
			header : 'Cadena',
			dataIndex : 'NOMBREEPO',
			width : 200,
			sortable : true
		},
		{
			header : 'Tipo de Factoraje',
			dataIndex : 'TIPOFACTORAJE',
			width : 100,
			sortable : true
		},
		{
			header : 'No. Documentos',
			dataIndex : 'NUMDOCTOMN',
			width : 100,
			align: 'center',
			sortable : true
			
		},
		{
			header : 'Monto',
			dataIndex : 'MONTOMN',
			width : 120,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Descontar',
			id:'linkDesc',
			dataIndex : 'LINKMN',
			width : 110,
			sortable : true,
			renderer:  function (valor, columna, registro){
				if(valor!='')
					return String.format('<a href="{0}" ><font color="blue">Descontar</font></a>', valor+'&strUsr='+vusuario+'&strNombreUsr='+vnombreusr+'&strCorreoUsr='+vcorreousr);
				else
					return '';
			}
		},
		{
			header : 'No. Documentos',
			dataIndex : 'NUMDOCTODA',
			width : 100,
			align: 'center',
			sortable : true
		},
		{
			header : 'Monto',
			dataIndex : 'MONTODA',
			width : 120,
			sortable : true,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Descontar',
			dataIndex : 'LINKDL',
			id:'linkDescB',
			width : 110,
			sortable : true,
			renderer:  function (valor, columna, registro){
				if(valor!='')
					return String.format('<a href="{0}" ><font color="blue">Descontar</font></a>', valor);
				else
					return  '';
			}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 870,
	height: 200,
	title: '',
	plugins: grupos,
	frame: true,
	listeners:{
		 viewready: function(g){
			  //g.getView().getRow(1).style.color="#f30";
		 }
	}
	});

//---



var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			{
				xtype: 'panel',
				name:'panelContenido',
				id:'panelContenido1',
				frame:true,
				width: 880,
				style: 'margin: 0 auto;',
				items: [
						{ 
							xtype:   'label',  
							html:		'<p align="center">No. Nafin Electr�nico Pyme: '+Ext.getDom("strNePymeAsigna").value+' '+Ext.getDom("strNombrePymeAsigna").value+'</p>', 
							cls:		'x-form-item', 
							style: { 
								width:   		'100%', 
								textAlign: 		'left'
							} 
						},
						gridResDoctos
					]
			},
			
			NE.util.getEspaciador(20)
		]
	});	
	
	var pyme = Ext.get('ic_pyme').getValue();

	storeDoctosData.load({
		params:{
			ic_pyme: pyme
		}
	});

});


