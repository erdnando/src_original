Ext.onReady(function() {


//********************************9 - Programado Pyme **********************************************************

	var procesarGenerarCSVProgramado =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarCSVProgramado');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarCSVProgramado');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDFProgramado =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFProgramado');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFProgramado');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarProgramadoData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();	
	
		if (arrRegistros != null) {
			if (!gridProgramado.isVisible()) {
				gridProgramado.show();
			}							
			var jsonData = store.reader.jsonData;		
			var cm = gridProgramado.getColumnModel();
						
			var el = gridProgramado.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				consuTotalesData.load({  params:{  estatus: 9  }  });
											
				Ext.getCmp('btnGenerarPDFProgramado').enable();	
				Ext.getCmp('btnGenerarCSVProgramado').enable();
				Ext.getCmp('gridTotales').show();
				
				el.unmask();							
			} else {							
				Ext.getCmp('btnGenerarPDFProgramado').disable();	
				Ext.getCmp('btnGenerarCSVProgramado').disable();
				Ext.getCmp('gridTotales').hide();						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaProgramadoData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_PYME'},
			{name: 'NUM_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC'},
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_DOCTO'},
			{name: 'POR_DESC'},
			{name: 'MONTO_DESC'},
			{name: 'INTERMEDIARIO'},
			{name: 'TASA'},
			{name: 'PLAZO_DIAS'},
			{name: 'MONTO_INT'},
			{name: 'IMP_RECIBIR'},
			{name: 'NUM_ACUSE'},
			{name: 'REFERENCIA'},
			{name: 'NETO_RECIBIR'},
			{name: 'BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'MANDATARIO'},
			{name: 'FECHA_REGISTRO'},
			{name: 'REFERENCIA_TASA'},	
			{name: 'REFERENCIA_PYME'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarProgramadoData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarProgramadoData(null, null, null);					
				}
			}
		}		
	});
	

	var gridProgramado = new Ext.grid.GridPanel({
		store: consultaProgramadoData,
		id: 'gridProgramado',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus: Programado PYME</div><div style="float:right"></div></div>',					
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Nombre PYME',
				tooltip: 'Nombre PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},			
			{
				header: 'N�mero Documento',
				tooltip: 'N�mero Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de  Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},			
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex: 'INTERMEDIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'				
			},			
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000')
			},
			{
				header: 'Plazo D�as',
				tooltip: 'Plazo D�as',
				dataIndex: 'PLAZO_DIAS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto Interes.',
				tooltip: 'Monto Interes.',
				dataIndex: 'MONTO_INT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�mero Acuse',
				tooltip: 'N�mero Acuse',
				dataIndex: 'NUM_ACUSE',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'			
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},		
			{
				header: 'Neto a Recibir',
				tooltip: 'Neto a Recibir',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}, 			
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'left'				
			},			
			{
				header: 'Porcentaje del Beneficiario',
				tooltip: 'Porcentaje del Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Mandatario',
				tooltip: 'Mandatario',
				dataIndex: 'MANDATARIO',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Fecha del Registro de la operaci�n',
				tooltip: 'Fecha del Registro de la operaci�n',
				dataIndex: 'FECHA_REGISTRO',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Referencia Tasa',
				tooltip: 'Referencia Tasa',
				dataIndex: 'REFERENCIA_TASA',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia PYME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,				
				resizable: true,
				align: 'left'				
			}		
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVProgramado',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV'
							}),
							callback: procesarGenerarCSVProgramado
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVProgramado',
					hidden: true
				}, 
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFProgramado',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDFProgramado
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFProgramado',
					hidden: true
				}								
			]
		}
	});
		


//********************************6 - Operada Pagada **********************************************************

	var procesarGenerarCSVPagada =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarCSVPagada');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarCSVPagada');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDFPagada =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFPagada');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFPagada');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarPagadaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();	
	
		if (arrRegistros != null) {
			if (!gridPagada.isVisible()) {
				gridPagada.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = gridPagada.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				gridPagada.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S'){
				gridPagada.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridPagada.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridPagada.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				gridPagada.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				gridPagada.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridPagada.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				gridPagada.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}	
			
			var el = gridPagada.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				consuTotalesData.load({  params:{  estatus: 6 }  });
											
				Ext.getCmp('btnGenerarPDFPagada').enable();	
				Ext.getCmp('btnGenerarCSVPagada').enable();
				Ext.getCmp('gridTotales').show();
				
				el.unmask();							
			} else {							
				Ext.getCmp('btnGenerarPDFPagada').disable();	
				Ext.getCmp('btnGenerarCSVPagada').disable();
				Ext.getCmp('gridTotales').hide();						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaPagadaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
				{name: 'NOMBRE_EPO'}	,
				{name: 'NOMBRE_PYME'},
				{name: 'NUM_SOLICITUD'},
				{name: 'NUM_DOCTO'},
				{name: 'MONEDA'},	
				{name: 'TIPO_FACTORAJE'},
				{name: 'MONTO_DOCTO'},
				{name: 'TASA'},
				{name: 'POR_DESC'},
				{name: 'REC_GARANTIA'},
				{name: 'MONTO_DESC'},
				{name: 'FECHA_VENC'},				
				{name: 'ORIGEN'},			
				{name: 'BENEFICIARIO'},
				{name: 'BANCO_BENEFICIARIO'},
				{name: 'SUCURSAL_BENEFICIARIO'},
				{name: 'CUENTA_BENEFICIARIO'},
				{name: 'PORC_BENEFICIARIO'},
				{name: 'IMP_RECIBIR'},
				{name: 'NETO_RECIBIR'},			
				{name: 'REFERENCIA'},
				{name: 'REFERENCIA_PYME'} //FODEA 17
			],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarPagadaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarPagadaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridPagada = new Ext.grid.GridPanel({
		store: consultaPagadaData,
		id: 'gridPagada',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus: Operada Pagada </div><div style="float:right"></div></div>',					
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Nombre Cliente/Cedente',
				tooltip: 'Nombre Cliente',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},		
			{
				header: 'N�mero Solicitud',
				tooltip: 'N�mero Solicitud',
				dataIndex: 'NUM_SOLICITUD',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},		
			{
				header: 'N�mero Documento',
				tooltip: 'N�mero Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto ',
				tooltip: 'Monto ',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000')
			},			
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Pago',
				tooltip: 'Fecha de  Pago',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Origen',
				tooltip: 'Origen',
				dataIndex: 'ORIGEN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'			
			},				
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Referencia Tasa',
				tooltip: 'Referencia Tasa',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},			
			{
				header: 'Referencia',
				tooltip: 'Referencia PYME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVPagada',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV'
							}),
							callback: procesarGenerarCSVPagada
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVPagada',
					hidden: true
				}, 
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFPagada',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDFPagada
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFPagada',
					hidden: true
				}								
			]
		}
	});
		

//********************************11 - Operado con Fondeo Propio **********************************************************

	var procesarGenerarCSVFondeo =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarCSVFondeo');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarCSVFondeo');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDFFondeo =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFFondeo');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFFondeo');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarFondeoData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();	
	
		if (arrRegistros != null) {
			if (!gridFondeo.isVisible()) {
				gridFondeo.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = gridFondeo.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				gridFondeo.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S' ){
				gridFondeo.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridFondeo.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridFondeo.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				gridFondeo.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				gridFondeo.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridFondeo.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				gridFondeo.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}	
			
			var el = gridFondeo.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				consuTotalesData.load({  params:{  estatus: 11 }  });
											
				Ext.getCmp('btnGenerarPDFFondeo').enable();	
				Ext.getCmp('btnGenerarCSVFondeo').enable();
				Ext.getCmp('gridTotales').show();
				
				el.unmask();							
			} else {							
				Ext.getCmp('btnGenerarPDFFondeo').disable();	
				Ext.getCmp('btnGenerarCSVFondeo').disable();
				Ext.getCmp('gridTotales').hide();						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consultaFondeoData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'NOMBRE_EPO'}	,
			{name: 'NOMBRE_PYME'},				
			{name: 'NUM_DOCTO'},
			{name: 'FECHA_EMISION'},				
			{name: 'FECHA_VENC'},
			{name: 'NUM_SOLICITUD'},		
			{name: 'MONEDA'},				
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_DOCTO'},
			{name: 'POR_DESC'},
			{name: 'REC_GARANTIA'},
			{name: 'MONTO_DESC'},
			{name: 'TASA'},
			{name: 'PLAZO'},
			{name: 'MONTO_INT'},
			{name: 'MONTO_OPER'},
			{name: 'ORIGEN'},
			{name: 'NUM_ACUSE_IF'},
			{name: 'BENEFICIARIO'},
			{name: 'BANCO_BENEFICIARIO'},
			{name: 'SUCURSAL_BENEFICIARIO'},
			{name: 'CUENTA_BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMP_RECIBIR'},
			{name: 'NETO_RECIBIR'},			
			{name: 'REFERENCIA'},
			{name: 'REFERENCIA_PYME'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarFondeoData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarFondeoData(null, null, null);					
				}
			}
		}		
	});
	

	var gridFondeo = new Ext.grid.GridPanel({
		store: consultaFondeoData,
		id: 'gridFondeo',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus: Operado con Fondeo Propio</div><div style="float:right"></div></div>',					
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Nombre PYME',
				tooltip: 'Nombre PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},			
			{
				header: 'N�mero Documento',
				tooltip: 'N�mero Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Emisi�n',
				tooltip: 'Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'N�mero Solicitud',
				tooltip: 'N�mero Solicitud',
				dataIndex: 'NUM_SOLICITUD',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000')
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto Int.',
				tooltip: 'Monto Int.',
				dataIndex: 'MONTO_INT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Operar.',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPER',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Origen',
				tooltip: 'Origen',
				dataIndex: 'ORIGEN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'			
			},
			{
				header: 'N�mero Acuse IF',
				tooltip: 'N�mero Acuse IF',
				dataIndex: 'NUM_ACUSE_IF',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'			
			},
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Referencia Tasa',
				tooltip: 'Referencia Tasa',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},			
			{
				header: 'Referencia',
				tooltip: 'Referencia PYME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVFondeo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV'
							}),
							callback: procesarGenerarCSVFondeo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVFondeo',
					hidden: true
				}, 
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFFondeo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDFFondeo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFFondeo',
					hidden: true
				}								
			]
		}		
	});
		


//******************************** 5 -Operada **********************************************************

	var procesarGenerarCSVOperada =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarCSVOperada');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarCSVOperada');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDFOperada =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFOperada');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFOperada');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarOperadaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();	
	
		if (arrRegistros != null) {
			if (!gridOperada.isVisible()) {
				gridOperada.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = gridOperada.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				gridOperada.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S' ){
				gridOperada.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridOperada.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridOperada.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				gridOperada.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				gridOperada.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridOperada.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				gridOperada.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}	
			
			var el = gridOperada.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				consuTotalesData.load({  params:{  estatus: 5 }  });
											
				Ext.getCmp('btnGenerarPDFOperada').enable();	
				Ext.getCmp('btnGenerarCSVOperada').enable();
				Ext.getCmp('gridTotales').show();
				
				el.unmask();							
			} else {							
				Ext.getCmp('btnGenerarPDFOperada').disable();	
				Ext.getCmp('btnGenerarCSVOperada').disable();
				Ext.getCmp('gridTotales').hide();						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaOperadaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'NOMBRE_EPO'}	,
			{name: 'NOMBRE_PYME'},				
			{name: 'NUM_DOCTO'},
			{name: 'FECHA_EMISION'},				
			{name: 'FECHA_VENC'},
			{name: 'NUM_SOLICITUD'},		
			{name: 'MONEDA'},				
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_DOCTO'},
			{name: 'POR_DESC'},
			{name: 'REC_GARANTIA'},
			{name: 'MONTO_DESC'},
			{name: 'TASA'},
			{name: 'PLAZO'},			
			{name: 'MONTO_OPER'},
			{name: 'ORIGEN'},			
			{name: 'BENEFICIARIO'},
			{name: 'BANCO_BENEFICIARIO'},
			{name: 'SUCURSAL_BENEFICIARIO'},
			{name: 'CUENTA_BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMP_RECIBIR'},
			{name: 'NETO_RECIBIR'},			
			{name: 'REFERENCIA'},
			{name: 'REFERENCIA_PYME'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarOperadaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarOperadaData(null, null, null);					
				}
			}
		}		
	});	
	
	var gridOperada = new Ext.grid.GridPanel({
		store: consultaOperadaData,
		id: 'gridOperada',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus: Operada</div><div style="float:right"></div></div>',					
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Nombre Cliente/Cedente',
				tooltip: 'Nombre Cliente',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'N�mero Solicitud',
				tooltip: 'N�mero Solicitud',
				dataIndex: 'NUM_SOLICITUD',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},		
			{
				header: 'N�mero Documento',
				tooltip: 'N�mero Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},				
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000')
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Monto a Operar.',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPER',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Origen',
				tooltip: 'Origen',
				dataIndex: 'ORIGEN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'			
			},			
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Referencia Tasa',
				tooltip: 'Referencia Tasa',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},			
			{
				header: 'Referencia',
				tooltip: 'Referencia PYME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
			bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVOperada',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV'
							}),
							callback: procesarGenerarCSVOperada
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVOperada',
					hidden: true
				}, 
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFOperada',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDFOperada
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFOperada',
					hidden: true
				}								
			]
		}
	});
	
	
//********************************En Proceso **********************************************************
	
	var procesarGenerarPDFProceso=  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFProceso');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFProceso');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarProcesoData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();	
	
		if (arrRegistros != null) {
			if (!gridProceso.isVisible()) {
				gridProceso.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = gridProceso.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				gridProceso.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S' ){
				gridProceso.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridProceso.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridProceso.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				gridProceso.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				gridProceso.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridProceso.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				gridProceso.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}	
			
			var el = gridProceso.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				consuTotalesData.load({  params:{  estatus: 4 }  });
											
				Ext.getCmp('btnGenerarPDFProceso').enable();					
				Ext.getCmp('gridTotales').show();
				
				el.unmask();							
			} else {							
				Ext.getCmp('btnGenerarPDFProceso').disable();					
				Ext.getCmp('gridTotales').hide();						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaProcesoData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
				{name: 'NOMBRE_EPO'}	,
				{name: 'NOMBRE_PYME'},				
				{name: 'NUM_DOCTO'},
				{name: 'FECHA_EMISION'},				
				{name: 'FECHA_VENC'},
				{name: 'NUM_SOLICITUD'},		
				{name: 'MONEDA'},				
				{name: 'TIPO_FACTORAJE'},
				{name: 'MONTO_DOCTO'},
				{name: 'POR_DESC'},
				{name: 'REC_GARANTIA'},
				{name: 'MONTO_DESC'},
				{name: 'TASA'},
				{name: 'PLAZO'},
				{name: 'MONTO_INT'},
				{name: 'MONTO_OPER'},
				{name: 'ORIGEN'},				
				{name: 'BENEFICIARIO'},
				{name: 'BANCO_BENEFICIARIO'},
				{name: 'SUCURSAL_BENEFICIARIO'},
				{name: 'CUENTA_BENEFICIARIO'},
				{name: 'PORC_BENEFICIARIO'},
				{name: 'IMP_RECIBIR'},
				{name: 'NETO_RECIBIR'},
				{name: 'REFERENCIA_PYME'}
			],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarProcesoData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarProcesoData(null, null, null);					
				}
			}
		}		
	});
	


	var gridProceso = new Ext.grid.GridPanel({
		store: consultaProcesoData,
		id: 'gridProceso',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus: En Proceso</div><div style="float:right"></div></div>',					
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Nombre Cliente/Cedente',
				tooltip: 'Nombre Cliente',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},		
			{
				header: 'N�mero Solicitud',
				tooltip: 'N�mero Solicitud',
				dataIndex: 'NUM_SOLICITUD',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},					
			{
				header: 'N�mero Documento',
				tooltip: 'N�mero Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Emisi�n',
				tooltip: 'Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
				
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000')
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto Int.',
				tooltip: 'Monto Int.',
				dataIndex: 'MONTO_INT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Operar.',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPER',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Origen',
				tooltip: 'Origen',
				dataIndex: 'ORIGEN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'			
			},			
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia PyME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				hidden:false,
				resizable: true,
				align: 'left'
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',					
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFProceso',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDFProceso
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFProceso',
					hidden: true
				}								
			]
		}
	});
		

//********************************Seleccionada IF **********************************************************

	var procesarGenerarCSVSelecIF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarCSVSelecIF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarCSVSelecIF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDFSelecIF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFSelecIF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFSelecIF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSelecIFData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();	
	
		if (arrRegistros != null) {
			if (!gridSelecIF.isVisible()) {
				gridSelecIF.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = gridSelecIF.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				gridSelecIF.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S' ){
				gridSelecIF.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridSelecIF.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridSelecIF.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				gridSelecIF.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				gridSelecIF.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridSelecIF.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				gridSelecIF.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}	
			
			var el = gridSelecIF.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				consuTotalesData.load({  params:{  estatus: 3 }  });
											
				Ext.getCmp('btnGenerarPDFSelecIF').enable();	
				Ext.getCmp('btnGenerarCSVSelecIF').enable();
				Ext.getCmp('gridTotales').show();
				
				el.unmask();							
			} else {							
				Ext.getCmp('btnGenerarPDFSelecIF').disable();	
				Ext.getCmp('btnGenerarCSVSelecIF').disable();
				Ext.getCmp('gridTotales').hide();						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consultaSelecIFData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
				{name: 'NOMBRE_EPO'}	,
				{name: 'NOMBRE_PYME'},				
				{name: 'NUM_DOCTO'},
				{name: 'FECHA_EMISION'},				
				{name: 'FECHA_VENC'},
				{name: 'NUM_SOLICITUD'},		
				{name: 'MONEDA'},				
				{name: 'TIPO_FACTORAJE'},
				{name: 'MONTO_DOCTO'},
				{name: 'POR_DESC'},
				{name: 'REC_GARANTIA'},
				{name: 'MONTO_DESC'},
				{name: 'TASA'},
				{name: 'PLAZO'},
				{name: 'MONTO_INT'},
				{name: 'MONTO_OPER'},
				{name: 'ORIGEN'},
				{name: 'NUM_ACUSE_IF'},
				{name: 'BENEFICIARIO'},
				{name: 'BANCO_BENEFICIARIO'},
				{name: 'SUCURSAL_BENEFICIARIO'},
				{name: 'CUENTA_BENEFICIARIO'},
				{name: 'PORC_BENEFICIARIO'},
				{name: 'IMP_RECIBIR'},
				{name: 'NETO_RECIBIR'},			
				{name: 'REFERENCIA'},
				{name: 'REFERENCIA_PYME'}
			],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarSelecIFData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarSelecIFData(null, null, null);					
				}
			}
		}		
	});
	

	var gridSelecIF = new Ext.grid.GridPanel({
		store: consultaSelecIFData,
		id: 'gridSelecIF',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus: Seleccionada IF</div><div style="float:right"></div></div>',					
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Nombre PYME',
				tooltip: 'Nombre PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},			
			{
				header: 'N�mero Documento',
				tooltip: 'N�mero Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Emisi�n',
				tooltip: 'Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'N�mero Solicitud',
				tooltip: 'N�mero Solicitud',
				dataIndex: 'NUM_SOLICITUD',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000')
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto Int.',
				tooltip: 'Monto Int.',
				dataIndex: 'MONTO_INT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Operar.',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPER',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Origen',
				tooltip: 'Origen',
				dataIndex: 'ORIGEN',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'			
			},
			{
				header: 'N�mero Acuse IF',
				tooltip: 'N�mero Acuse IF',
				dataIndex: 'NUM_ACUSE_IF',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'			
			},
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Referencia Tasa',
				tooltip: 'Referencia Tasa',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},			
			{
				header: 'Referencia',
				tooltip: 'Referencia PyME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVSelecIF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV'
							}),
							callback: procesarGenerarCSVSelecIF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVSelecIF',
					hidden: true
				}, 
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFSelecIF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDFSelecIF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFSelecIF',
					hidden: true
				}								
			]
		}
		});

//********************************En Proceso de Autorizaci�n IF **********************************************************

	var procesarGenerarCSVAutoIF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarCSVAutoIF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarCSVAutoIF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDFAutoIF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFAutoIF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFAutoIF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarAutoIFData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();	
	
		if (arrRegistros != null) {
			if (!gridAutoIF.isVisible()) {
				gridAutoIF.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = gridAutoIF.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				gridAutoIF.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S' ){
				gridAutoIF.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridAutoIF.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridAutoIF.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				gridAutoIF.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				gridAutoIF.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridAutoIF.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				gridAutoIF.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}			
			
			var el = gridAutoIF.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				consuTotalesData.load({  params:{  estatus: 2 }  });
											
				Ext.getCmp('btnGenerarPDFAutoIF').enable();	
				Ext.getCmp('btnGenerarCSVAutoIF').enable();
				Ext.getCmp('gridTotales').show();
				
				el.unmask();							
			} else {							
				Ext.getCmp('btnGenerarPDFAutoIF').disable();	
				Ext.getCmp('btnGenerarCSVAutoIF').disable();
				Ext.getCmp('gridTotales').hide();						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consultaAutoIFData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
				{name: 'NOMBRE_EPO'}	,		
				{name: 'NUM_DOCTO'},
				{name: 'FECHA_DOCTO'},
				{name: 'FECHA_VENC'},
				{name: 'NOMBRE_CLIENTE'},
				{name: 'TASA'},
				{name: 'PLAZO_DIAS'},
				{name: 'MONEDA'},				
				{name: 'TIPO_FACTORAJE'},
				{name: 'MONTO_DOCTO'},
				{name: 'POR_DESC'},
				{name: 'REC_GARANTIA'},
				{name: 'MONTO_DESC'},
				{name: 'MONTO_INT'},
				{name: 'MONTO_OPER'},
				{name: 'BENEFICIARIO'},
				{name: 'BANCO_BENEFICIARIO'},
				{name: 'SUCURSAL_BENEFICIARIO'},
				{name: 'CUENTA_BENEFICIARIO'},
				{name: 'PORC_BENEFICIARIO'},
				{name: 'IMP_RECIBIR'},
				{name: 'NETO_RECIBIR'},				
				{name: 'REFERENCIA'},
				{name: 'REFERENCIA_PYME'}
			],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarAutoIFData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarAutoIFData(null, null, null);					
				}
			}
		}		
	});
	

	var gridAutoIF = new Ext.grid.GridPanel({
		store: consultaAutoIFData,
		id: 'gridAutoIF',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus: En Proceso de Autorizaci�n IF</div><div style="float:right"></div></div>',	
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'N�mero Documento',
				tooltip: 'N�mero Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Documento',
				tooltip: 'Fecha Documento',
				dataIndex: 'FECHA_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Nombre Cliente/Cedente',
				tooltip: 'Nombre Cliente',
				dataIndex: 'NOMBRE_CLIENTE',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000')
			},
			{
				header: 'Plazo (d�as)',
				tooltip: 'Plazo (d�as)',
				dataIndex: 'PLAZO_DIAS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Int.',
				tooltip: 'Monto Int.',
				dataIndex: 'MONTO_INT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Operar.',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPER',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Referencia Tasa',
				tooltip: 'Referencia Tasa',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},			
			{
				header: 'Referencia',
				tooltip: 'Referencia PyME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVAutoIF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV'
							}),
							callback: procesarGenerarCSVAutoIF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVAutoIF',
					hidden: true
				}, 
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFAutoIF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDFAutoIF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFAutoIF',
					hidden: true
				}								
			]
		}	
	});
		

//********************************Seleccionada PYME **********************************************************
		
//---------------------------descargaArchivo------------------------------------
function descargaArchivo(opts, success, response) {	
if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnExp').setIconClass('');		
			var btnBajarPDF = Ext.getCmp('btnBajarCSVSelecPYMEExp');
			var archivo = infoR.urlArchivo; 
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//---------------------------fin descargaArchivo--------------------------------
	var procesarGenerarCSVSelecPYME =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarCSVSelecPYME');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarCSVSelecPYME');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarGenerarPDFSelecPYME =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFSelecPYME');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFSelecPYME');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var procesarSelecPYMEData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
	
		if (arrRegistros != null) {
			if (!gridSelecPYME.isVisible()) {
				gridSelecPYME.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = gridSelecPYME.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				gridSelecPYME.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S' ){
				gridSelecPYME.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridSelecPYME.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridSelecPYME.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				gridSelecPYME.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				gridSelecPYME.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridSelecPYME.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				gridSelecPYME.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}	
			
			var el = gridSelecPYME.getGridEl();				
			if(store.getTotalCount() > 0) {	
			
				consuTotalesData.load({  params:{  estatus: 1 }  });	
				
				Ext.getCmp('btnGenerarPDFSelecPYME').enable();	
				Ext.getCmp('btnGenerarCSVSelecPYME').enable();	
				Ext.getCmp('btnExp').enable();	
				Ext.getCmp('gridTotales').show();	
				el.unmask();							
			} else {		
				Ext.getCmp('btnGenerarPDFSelecPYME').disable();	
				Ext.getCmp('btnGenerarCSVSelecPYME').disable();
				Ext.getCmp('btnExp').disable();
				
				Ext.getCmp('gridTotales').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaSelecPYMEData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte01.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
				{name: 'NOMBRE_EPO'}	,		
				{name: 'IC_EPO'},		
				{name: 'NUM_DOCTO'},
				{name: 'FECHA_DOCTO'},
				{name: 'FECHA_VENC'},
				{name: 'NOMBRE_CLIENTE'},
				{name: 'TASA'},
				{name: 'PLAZO_DIAS'},
				{name: 'MONEDA'},				
				{name: 'TIPO_FACTORAJE'},
				{name: 'MONTO_DOCTO'},
				{name: 'POR_DESC'},
				{name: 'REC_GARANTIA'},
				{name: 'MONTO_DESC'},
				{name: 'MONTO_INT'},
				{name: 'MONTO_OPER'},
				{name: 'BENEFICIARIO'},
				{name: 'BANCO_BENEFICIARIO'},
				{name: 'SUCURSAL_BENEFICIARIO'},
				{name: 'CUENTA_BENEFICIARIO'},
				{name: 'PORC_BENEFICIARIO'},
				{name: 'IMP_RECIBIR'},
				{name: 'NETO_RECIBIR'},
				{name: 'FECHA_REGISTRO'},
				{name: 'REFERENCIA'},
				{name: 'REFERENCIA_PYME'}
			],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarSelecPYMEData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarSelecPYMEData(null, null, null);					
				}
			}
		}		
	});
	

	var gridSelecPYME = new Ext.grid.GridPanel({
		store: consultaSelecPYMEData,
		id: 'gridSelecPYME',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus: Seleccionada PYME</div><div style="float:right"></div></div>',			
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'N�mero Documento',
				tooltip: 'N�mero Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Documento',
				tooltip: 'Fecha Documento',
				dataIndex: 'FECHA_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Nombre Cliente/Cedente',
				tooltip: 'Nombre Cliente',
				dataIndex: 'NOMBRE_CLIENTE',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000')
			},
			{
				header: 'Plazo (d�as)',
				tooltip: 'Plazo (d�as)',
				dataIndex: 'PLAZO_DIAS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Int.',
				tooltip: 'Monto Int.',
				dataIndex: 'MONTO_INT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Operar.',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPER',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Fecha Registro Operaci�n',
				tooltip: 'Fecha Registro Operaci�n',
				dataIndex: 'FECHA_REGISTRO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Referencia Tasa',
				tooltip: 'Referencia Tasa',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia PyME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype 	: 'button',
					id    	: 'btnExp',
					text  	: 'Exp. Interfase Variable.',					
					tooltip	: 'Exp. Interfase Variable.',
					handler	: function(boton, evento) {
								boton.setDisabled(true);
								boton.setIconClass('loading-indicator');
								var reg = consultaSelecPYMEData.getAt(0);
								var idEpo = reg.get('IC_EPO');
								Ext.Ajax.request({
									url: '13reporte01.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
									informacion: 'Consultar',
									operacion:'ArchivoCSVExp',
									epo:idEpo
								}),
								success : function(response) {
								 boton.setDisabled(false);
								},
								callback: descargaArchivo
								});
							}
					},	{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVSelecPYMEExp',
					hidden: true
				},	{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarCSVSelecPYME',	
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoCSV'
							}),
							success : function(response) {
								 boton.setDisabled(false);
							},
							callback: procesarGenerarCSVSelecPYME
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar CSV',
					id: 'btnBajarCSVSelecPYME',
					hidden: true
				}, 
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDFSelecPYME',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDFSelecPYME
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFSelecPYME',
					hidden: true
				}								
			]
		}		
	});

//************************** PARA LOS TOTALES DE  Seleccionada PYME  , En Proceso de Autorizaci�n IF ********
	
	
	var procesarTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}						
			//edito el titulo de la columna
			var cm = gridTotales.getColumnModel();			
			var jsonData = store.reader.jsonData;		
		
			var estatus1 = Ext.getCmp('estatus1').getValue();			
			if(estatus1 ==5 ||  estatus1  == 6  ){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_INT'), true);							
			}else  {
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_INT'), false);
			}
			if(estatus1  == 6  ||   estatus1  == 9 ){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_OPER'), true);	
			}else {
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_OPER'), false);	
			}
			if(estatus1  == 9) {
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_IMPORTE_RECIBIR'), false);			
			}else  {
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_IMPORTE_RECIBIR'), true);	
			}
		
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var consuTotalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte01.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'		
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'NUM_DOCTOS', type: 'float', mapping: 'NUM_DOCTOS'},			
			{name: 'TOTAL_MONTO_DOCTO', type: 'float', mapping: 'TOTAL_MONTO_DOCTO'},	
			{name: 'TOTAL_MONTO_DESC', type: 'float', mapping: 'TOTAL_MONTO_DESC'},	
			{name: 'TOTAL_MONTO_INT', type: 'float', mapping: 'TOTAL_MONTO_INT'},
			{name: 'TOTAL_MONTO_OPER', type: 'float', mapping: 'TOTAL_MONTO_OPER'},	
			{name: 'TOTAL_IMPORTE_RECIBIR', type: 'float', mapping: 'TOTAL_IMPORTE_RECIBIR'}	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}		
	});
						
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consuTotalesData,	
		style: 'margin:0 auto;',
		title:'',
		hidden: true,		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'			
			},						
			{
				header: 'Num. Documentos',
				dataIndex: 'NUM_DOCTOS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Documento',
				dataIndex: 'TOTAL_MONTO_DOCTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto a Descontar',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto Int',
				dataIndex: 'TOTAL_MONTO_INT',
				width: 150,
				hidden: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto a Operar',
				dataIndex: 'TOTAL_MONTO_OPER',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Importe a Recibir',
				dataIndex: 'TOTAL_IMPORTE_RECIBIR',
				width: 150,
				hidden: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		height: 100,
		width: 900,
		title: '',
		frame: false
	});
	
	//********************************FORMA **********************************************************
	function procesaValoresIniciales(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if(jsonValoresIniciales != null){	
				var strNombreUsuario = Ext.getCmp('strNombreUsuario');
				var fechaHora = Ext.getCmp('fechaHora');
				strNombreUsuario.getEl().update(jsonValoresIniciales.strNombreUsuario);
				fechaHora.getEl().update(jsonValoresIniciales.fechaHora);
	
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}
	
	//Panel de valores iniciales
	var dataStatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','Seleccionada PYME'],
			['2','En Proceso de Autorizaci�n IF'],
			['3','Seleccionada IF'],
			['4','En Proceso'],
			['5','Operada'],
			['11','Operado con Fondeo Propio'],
			['6','Operada Pagada'],
			['9','Programado Pyme']		
		 ]
	});

	var elementosForma = [
		{
			xtype: 'label',
			id: 'strNombreUsuario',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Usuario',
			text: '--'
		},
		{
			xtype: 'label',
			id: 'fechaHora',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Fecha',
			text: '--'
		},
		{
			xtype: 'combo',
			name: 'estatus',
			id: 'estatus1',
			fieldLabel: 'Estatus',
			emptyText: 'Seleccione...',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'estatus',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : dataStatus,
			listeners: {
				select: {
					fn: function(combo){
												
						Ext.getCmp('btnBajarCSVSelecPYMEExp').hide();	//fodea 41
						Ext.getCmp('btnGenerarPDFSelecPYME').enable();	
						Ext.getCmp('btnGenerarCSVSelecPYME').enable();
						Ext.getCmp('btnGenerarPDFAutoIF').enable();	
						Ext.getCmp('btnGenerarCSVAutoIF').enable();
						Ext.getCmp('btnGenerarPDFSelecIF').enable();	
						Ext.getCmp('btnGenerarCSVSelecIF').enable();
						Ext.getCmp('btnGenerarPDFProceso').enable();							
						Ext.getCmp('btnGenerarCSVOperada').enable();
						Ext.getCmp('btnGenerarPDFOperada').enable();	
						Ext.getCmp('btnGenerarCSVFondeo').enable();
						Ext.getCmp('btnGenerarPDFFondeo').enable();	
						Ext.getCmp('btnGenerarCSVPagada').enable();
						Ext.getCmp('btnGenerarPDFPagada').enable();							
						Ext.getCmp('btnGenerarCSVProgramado').enable();
						Ext.getCmp('btnGenerarPDFProgramado').enable();							
						
			
						Ext.getCmp('btnBajarPDFSelecPYME').hide();	
						Ext.getCmp('btnBajarCSVSelecPYME').hide();
						Ext.getCmp('btnBajarPDFAutoIF').hide();
						Ext.getCmp('btnBajarCSVAutoIF').hide();
						Ext.getCmp('btnBajarPDFSelecIF').hide();
						Ext.getCmp('btnBajarCSVSelecIF').hide();
						Ext.getCmp('btnBajarPDFProceso').hide();						
						Ext.getCmp('btnBajarPDFOperada').hide();
						Ext.getCmp('btnBajarCSVOperada').hide();	
						Ext.getCmp('btnBajarPDFFondeo').hide();
						Ext.getCmp('btnBajarCSVFondeo').hide();						
						Ext.getCmp('btnBajarPDFPagada').hide();
						Ext.getCmp('btnBajarCSVPagada').hide();						
						Ext.getCmp('btnBajarPDFProgramado').hide();
						Ext.getCmp('btnBajarCSVProgramado').hide();
											
						
						Ext.getCmp('gridTotales').hide();
						Ext.getCmp('gridSelecPYME').hide();
						Ext.getCmp('gridAutoIF').hide();
						Ext.getCmp('gridSelecIF').hide();	
						Ext.getCmp('gridProceso').hide();
						Ext.getCmp('gridOperada').hide();
						Ext.getCmp('gridFondeo').hide();
						Ext.getCmp('gridPagada').hide();
						Ext.getCmp('gridProgramado').hide();						
						
		
						if(combo.getValue() ==1) {				
							consultaSelecPYMEData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}
						if(combo.getValue() ==2) {					
							consultaAutoIFData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}
						
						if(combo.getValue() ==3) {					
							consultaSelecIFData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}	
						
						if(combo.getValue() ==4) {					
							consultaProcesoData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}	
						
						if(combo.getValue() ==5) {					
							consultaOperadaData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}	
						
						if(combo.getValue() ==11) {					
							consultaFondeoData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}	
						
						if(combo.getValue() ==6) {					
							consultaPagadaData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}
						
						if(combo.getValue() ==9) {					
							consultaProgramadoData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});
						}				
						
					}
				}
			}
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: 'margin:0 auto;',
		frame: true,
		border: false,
		title: '<div><center>Doctos / Solic. por Estatus</div>',
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		hidden: false,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true
	});

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),			
			gridSelecPYME, 
			gridAutoIF, 
			gridSelecIF,
			gridProceso,
			gridOperada, 
			gridFondeo,
			gridPagada, 
			gridProgramado, 
			gridTotales, 
			NE.util.getEspaciador(20)
		]
	});


	var valIniciales = function(){
		Ext.Ajax.request({
			url: '13reporte01.data.jsp',
			params: {
						informacion: "valoresIniciales"
			},
			callback: procesaValoresIniciales
		});
	}
	
	valIniciales();
	
	
});