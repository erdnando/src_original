<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.model.catalogos.*,
	com.netro.descuento.*, 	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String estatus = (request.getParameter("estatus") != null) ? request.getParameter("estatus") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String  ic_if =iNoCliente,  infoRegresar ="", consulta="", 
bTipoFactoraje ="N", bOperaFactorajeDist  ="N", bOperaFactVencimientoInfonavit ="N",  sOperaFactConMandato ="N", bOperaFactAutomatico = "N";
SimpleDateFormat formatoHora2 = new SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a");

ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);


Hashtable  alParamIF =BeanParamDscto.parametrizacionIF(ic_if);
if (alParamIF!=null) {
	bTipoFactoraje	                  =	alParamIF.get("bTipoFactoraje").toString();
	bOperaFactorajeDist	            =	alParamIF.get("bOperaFactorajeDist").toString();
	bOperaFactVencimientoInfonavit	=	alParamIF.get("PUB_EPO_VENC_INFONAVIT").toString();
   bOperaFactAutomatico             =  alParamIF.get("cs_factoraje_automatico").toString();
	sOperaFactConMandato	            =	alParamIF.get("PUB_EPO_OPERA_MANDATO").toString(); 
}	
JSONObject jsonObj = new JSONObject();

if (informacion.equals("valoresIniciales")  ) {  

	jsonObj.put("success",new Boolean(true));
	jsonObj.put("strNombreUsuario", strNombreUsuario);
	jsonObj.put("fechaHora",formatoHora2.format(new java.util.Date()));
	infoRegresar = jsonObj.toString();
 
} else  if (informacion.equals("Consultar")  ||  informacion.equals("ResumenTotales")  ) {

	ConsDoctosSolicXEstatusIF paginador = new ConsDoctosSolicXEstatusIF();
	String epo = (request.getParameter("epo") != null) ? request.getParameter("epo") : "";
	String fecha =formatoHora2.format(new java.util.Date());
	String [] newFecha = fecha.split(";");
	paginador.setEstatus(estatus);
	paginador.setClaveIF(ic_if);
	paginador.setBTipoFactoraje(bTipoFactoraje);
	paginador.setBOperaFactorajeDist(bOperaFactorajeDist);
	paginador.setBOperaFactVencimientoInfonavit(bOperaFactVencimientoInfonavit);
   paginador.setBOperaFactAutomatico(bOperaFactAutomatico);
	paginador.setSOperaFactConMandato(sOperaFactConMandato);
	paginador.setIdEpo(epo);
	paginador.setFecha(newFecha[0]);
	if (operacion.equals("ArchivoCSVExp")) {
		paginador.setGeneraExp("Exp");
	}
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if (operacion.equals("Generar") ){
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}	
	} else if (operacion.equals("ArchivoPDF")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}				
	} else if (operacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}		
	} else if (operacion.equals("ArchivoCSVExp")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}		
	}
	
	
	if ( informacion.equals("ResumenTotales")  ) {
	
		Registros reg	=	queryHelper.doSearch();		
		double nacional = 0, nacional_desc = 0, nacional_int = 0, nacional_descIF = 0, 
		dolares = 0, dolares_desc = 0,  dolares_int = 0,  dolares_descIF = 0, dolares_descIMP  =0,
		nacional_descIMP  =0;
		
		int  doc_nacional =0,doc_dolares =0;
		HashMap registrosTot = new HashMap();	
		JSONArray registrosTotales = new JSONArray();
		
		while (reg.next()) {
			
			String ic_moneda = (reg.getString("IC_MONEDA") == null) ? "" : reg.getString("IC_MONEDA");
			String monto_docto = (reg.getString("MONTO_DOCTO") == null) ? "0" : reg.getString("MONTO_DOCTO");
			String monto_desc = (reg.getString("MONTO_DESC") == null) ? "0" : reg.getString("MONTO_DESC");
			String monto_int = (reg.getString("MONTO_INT") == null) ? "0" : reg.getString("MONTO_INT");
			String monto_oper = (reg.getString("MONTO_OPER") == null) ? "0" : reg.getString("MONTO_OPER");
			String imp_recibir = (reg.getString("IMP_RECIBIR") == null) ? "0" : reg.getString("IMP_RECIBIR");	
		
			if (ic_moneda.equals("1")) {	
				nacional+=Double.parseDouble((String)monto_docto);
				nacional_desc+=Double.parseDouble((String)monto_desc);  
				nacional_int+=Double.parseDouble((String)monto_int);    
				nacional_descIF+=Double.parseDouble((String)monto_oper);  
				nacional_descIMP+=Double.parseDouble((String)imp_recibir);  
				doc_nacional++;
			}
			if (ic_moneda.equals("54")) {	
				dolares+=Double.parseDouble((String)monto_docto);
				dolares_desc+=Double.parseDouble((String)monto_desc);  
				dolares_int+=Double.parseDouble((String)monto_int);    
				dolares_descIF+=Double.parseDouble((String)monto_oper); 
				dolares_descIMP+=Double.parseDouble((String)imp_recibir); 
				doc_dolares++;
			}	
		}
		
		for(int t =0; t<2; t++) {	
			registrosTot = new HashMap();		
			if(t==0){ 				
				registrosTot.put("MONEDA", "Total MN");
				registrosTot.put("NUM_DOCTOS", Double.toString(doc_nacional));		
				registrosTot.put("TOTAL_MONTO_DOCTO",Double.toString (nacional) );					
				registrosTot.put("TOTAL_MONTO_DESC", Double.toString (nacional_desc) );
				registrosTot.put("TOTAL_MONTO_INT", Double.toString (nacional_int) );
				registrosTot.put("TOTAL_MONTO_OPER", Double.toString (nacional_descIF) );
				registrosTot.put("TOTAL_IMPORTE_RECIBIR", Double.toString (nacional_descIMP) );
			}
			if(t==1){ 			
				registrosTot.put("MONEDA", "Total USD");
				registrosTot.put("NUM_DOCTOS", Double.toString(doc_dolares));		
				registrosTot.put("TOTAL_MONTO_DOCTO", Double.toString (dolares));					
				registrosTot.put("TOTAL_MONTO_DESC", Double.toString (dolares_desc) );
				registrosTot.put("TOTAL_MONTO_INT", Double.toString (dolares_int) );
				registrosTot.put("TOTAL_MONTO_OPER", Double.toString (dolares_descIF) );
				registrosTot.put("TOTAL_IMPORTE_RECIBIR", Double.toString (dolares_descIMP) );
			}
			registrosTotales.add(registrosTot);		
		}
			infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";					
	}
	
	if (informacion.equals("Consultar")){
		jsonObj.put("bTipoFactoraje",bTipoFactoraje);
		jsonObj.put("bOperaFactorajeDist",bOperaFactorajeDist);
		jsonObj.put("bOperaFactVencimientoInfonavit",bOperaFactVencimientoInfonavit);
		jsonObj.put("bOperaFactAutomatico",bOperaFactAutomatico);
		jsonObj.put("sOperaFactConMandato",sOperaFactConMandato);
		jsonObj.put("estatus",estatus);		
		infoRegresar	=	jsonObj.toString();	
	}			
}
	
%>
<%=infoRegresar%>
