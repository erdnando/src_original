	Ext.onReady(function() {
	
   // Obj parametros globales
  var parametroGlobal = {
      parametro   :  '',
      ic_if       : '',
      sirac       : '',
      financiera  : '',
      strPerfil: Ext.getDom('strPerfil').value
  }
    
  parametroGlobal.ic_if = Ext.getDom('ic_if').value;
  
  
  function validaPerfil(perfil){
    if(perfil =='LINEA CREDITO' || perfil =='IF LI' || perfil =='IF 4CP' || perfil =='IF 5CP' ||
       perfil =='IF 4MIC' || perfil =='IF 5MIC' ){
      return 'LINEA CREDITO';
    }else{
      return 'DESCUENTO';
    }
  }
  
  //------------------- OBTIENE PARAMETRO INICIAL ------------------------------
   var obtieneParametroInicial = {   
      procesarObtieneParametro :  function(response, opts) { 
         if (Ext.util.JSON.decode(response.responseText).success == true) {
            parametroGlobal.parametro = Ext.util.JSON.decode(response.responseText).parametro;
            parametroGlobal.financiera = Ext.util.JSON.decode(response.responseText).IC_FINANCIERA;
            parametroGlobal.sirac = Ext.util.JSON.decode(response.responseText).NUMERO_SIRAC;
            
            if(validaPerfil(parametroGlobal.strPerfil)!='LINEA CREDITO'){
              if(parametroGlobal.sirac=='' || parametroGlobal.financiera==''){
                Ext.getCmp("tipoLinea").hide();
                if(parametroGlobal.financiera==''){
                  Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
                  Ext.getCmp("cliente").setValue(parametroGlobal.sirac);
                  parametroGlobal.ic_if =12
                }
              }else{
                Ext.getCmp("tipoLinea").show();
              }
            }else{
              Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
              Ext.getCmp("cliente").setValue(parametroGlobal.sirac);
              parametroGlobal.ic_if =12
            }
         }
         
         catalogoFechaCorte.load({
          params:{
			tipoLinea: Ext.getCmp("tipoLinea").getValue().value,
            ic_if: parametroGlobal.ic_if,
			cliente: Ext.getCmp("cliente").getValue()
          }
         });
      },      
      obtieneParametroFnAjax : function(){
         Ext.Ajax.request({
            url: '13consulta8ext.data.jsp',
            params: {
               informacion:'ObtieneParametro'
            },
            failure: function(response, opts) {         
               Ext.Msg.show({
                  title    :  '('+response.status+') ' + response.statusText,
                  msg      :  "No se logr� cargar la pantalla correctamente...",
                  icon     :  Ext.Msg.ERROR,
                  buttons  :  Ext.Msg.OK
               }); 
            },
            success: this.procesarObtieneParametro
         });
      }
   }
   //------------------ FIN OBTIENE PARAMETRO INICIAL --------------------------

	// PROCESAR ZIP
		var procesarGenerarZip =  function(opts, success, response) {
		var btnArchivoPDF = Ext.getCmp('btnGenerarZip');
		btnArchivoPDF.setIconClass('icoZip');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarZip');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
            try{
              //var forma = Ext.getDom('formAux');
              //forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
              //forma.submit();
               
              var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;				
              archivo = archivo.replace('/nafin','');
              var params = {nombreArchivo: archivo};				
              var forma = Ext.getCmp('forma');            
              forma.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
              forma.getForm().getEl().dom.submit();
            }catch(e){
               alert(e);
            }
			});
		} else {
			btnArchivoPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// Procesar Imprimir PDF
	var procesarSuccessFailureImprimirPDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureNotaC =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirNotaC');
		btnImprimirPDF.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaDataTotales = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}
			var el = gridTotales.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
            gridTotales.hide();
			}
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo     = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo   = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF         = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF       = Ext.getCmp('btnGenerarPDF');
			var btnImprimirPDF      = Ext.getCmp('btnImprimirPDF');
			var btnImprimirPDF      = Ext.getCmp('btnImprimirNotaC');
			var gridTotales         = Ext.getCmp('gridTotales');
      var el                  = grid.getGridEl();
			var jsonData            = store.reader.jsonData;
         
         try{         
            if(parametroGlobal.parametro=='S'){
              grid.getColumnModel().setHidden(3, false);
            }else{
              grid.getColumnModel().setHidden(3, true);
            }
         
            if(store.getTotalCount() > 0) {               
               var cmpForma = Ext.getCmp('forma');
               var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");  // Id barra paginacion
               paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
               consultaDataGridTotales.load({
                  params: Ext.apply(paramSubmit,{
                     ic_if: parametroGlobal.ic_if,
                     informacion :  'Consultar',
                     operacion   :  'Totales',
                     parametro   :  parametroGlobal.parametro,
                     start       :  opts.params.start,
                     limit       :  opts.params.limit
                  })
               });
               Ext.getCmp('btnGenerarArchivo').enable();
               Ext.getCmp('btnBajarArchivo').hide();
               Ext.getCmp('btnImprimirPDF').enable();
               el.unmask();
            } else {
               btnGenerarArchivo.disable();
               gridTotales.hide();
               el.mask('No se encontr� ning�n registro', 'x-mask');
            }
         }catch(e){
            Ext.Msg.show({
               title    :     'Error',
               msg      :     e.description,
               icon     :     Ext.Msg.ERROR,
               buttons  :     Ext.Msg.OK
            }); 
         }
		}
	}
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
		}
	}
	var bajarContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			alert("error");
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoTxt');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				//var forma = Ext.getDom('formAux');
				//forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				//forma.submit();
        
        var archivo =  Ext.util.JSON.decode(response.responseText).urlArchivo;				
        archivo = archivo.replace('/nafin','');
        var params = {nombreArchivo: archivo};				
        var forma = Ext.getCmp('forma');            
        forma.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
        forma.getForm().getEl().dom.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec?fico en la b?squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci?n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE?S-*-*-*-*-*-*-INIT
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta8ext.data.jsp',
		baseParams: {
			informacion: 'Consultar',
         parametro   :  parametroGlobal.parametro,
			operacion:     'Generar',
         start       : 0,
         limit       : 15
		},
		fields: [
			{name: 'CLIENTE'},
			{name: 'FECHA_OPERACION'},
			{name: 'NOMBRE_CLIENTE'},
			{name: 'PRESTAMO'},
			//{name: 'IG_TASAREFERENCIAL'},
      {name: 'TASA'},
			{name: 'SPREAD'},
			{name: 'MARGEN'},
      {name: 'CD_NOMBRE'},
			{name: 'MONTO_OPERADO'},
			{name: 'MONTO_PRIMER_CUOTA'},
			{name: 'NETO_OTORGADO'},
			//{name: 'FG_CAPITALVENCIDO'},
			//{name: 'FG_INTERESVENCIDO'},
			//{name: 'FG_INTERESMORAT'},
			//{name: 'FG_ADEUDOTOTAL'},
       //{name: 'TO_CHAR(DF_FECHAOPERACION,DD/MM/YYYY)'},
       {name: 'NUMERO_ELECTRONICO'},
       {name: 'FECHA_PRIMER_PAGO'}
       //{name: 'IG_TASA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
         beforeload: {
            fn: function(store, options){
               var moneda 	   =     Ext.getCmp('id_moneda').getValue();
               var cliente    =     Ext.getCmp('cliente').getValue();
               var prestamo   =     Ext.getCmp('prestamo').getValue();
               var fechaCorte =     Ext.getCmp('id_FechaCorte').getValue();
               
               Ext.apply(options.params, {
                  moneda: moneda,
                  cliente:cliente,
                  prestamo:prestamo,
                  FechaCorte:fechaCorte,
				  ic_if: parametroGlobal.ic_if
				  
               })
            }
         },
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var consultaDataGridTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta8ext.data.jsp',
		baseParams: {
			informacion : 'Consultar',
			operacion   : 'Totales',
         parametro   :  parametroGlobal.parametro,
         start       :  0,
         limit       :  15
		},
		fields: [
			{name: 'MONEDA'},
			{name: 'N_REGISTROS'},
			{name: 'MONTO_OPERADO'},
			{name: 'SALDO_INSOLUTO'},
			{name: 'CAPITAL_VIG'},
			{name: 'CAPITAL_VENCIDO'},
			{name: 'INTERES_VENCIDO'},
			{name: 'INTERES_MORATORIO'},
			{name: 'ADEUDO_TOTAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataTotales,
         beforeload: {
            fn: function(store, options){
               var moneda 	   =     Ext.getCmp('id_moneda').getValue();
               var cliente    =     Ext.getCmp('cliente').getValue();
               var prestamo   =     Ext.getCmp('prestamo').getValue();
               var fechaCorte =     Ext.getCmp('id_FechaCorte').getValue();
               
               Ext.apply(options.params, {
                  moneda: moneda,
                  cliente:cliente,
                  prestamo:prestamo,
                  FechaCorte:fechaCorte
               })
            }
         },
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var catalogoMoneda = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '13consulta8ext.data.jsp',
		listeners: {
			//load: procesarCatalogoEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: true
	});
	var catalogoFechaCorte = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '13consulta8ext.data.jsp',
		listeners: {
			//load: procesarCatalogoEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'CatalogoFechaCorte'
		},
		totalProperty : 'total',
		autoLoad: false
	});
	
/*******************************************************************************
*											GRID'S													 *
*******************************************************************************/
	var gridTotales = new Ext.grid.GridPanel({
		store: consultaDataGridTotales,
		xtype:'grid',
		id: 'gridTotales',
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 180,
		width: 692,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		hidden: true,
		title:'Totales',
		columns: [{
			header: 'Total Parcial', tooltip: 'Total Parcial',
			dataIndex: 'MONEDA',
			sortable: true,
			width: 165, resizable: true,
			align: 'right'
		},{
			header: 'N�mero de Registros', tooltip: 'N�mero de Registros',
			dataIndex: 'N_REGISTROS',
			sortable: true,align:'right',
			width: 150, resizable: true, renderer: function(o){return Ext.util.Format.number(o,'0,0')}
		},{
			header: 'Monto Operado', tooltip: 'Monto Operado',
			dataIndex: 'MONTO_OPERADO',
			sortable: true,align:'right',
			width: 130, resizable: true,renderer:'usMoney'
		},{
			header: 'Saldo Insoluto', tooltip: 'Saldo Insoluto',
			dataIndex: 'SALDO_INSOLUTO',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Capital Vigente', tooltip: 'Capital Vigente',
			dataIndex: 'CAPITAL_VIG',
			sortable: true,
			width: 130, resizable: true,
			align: 'right', renderer: 'usMoney'
		}]
	});
	var grid = new Ext.grid.GridPanel({	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
      columnLines :true,
		columns: [{
				header: 'Cliente', tooltip: 'Cliente',
				dataIndex: 'CLIENTE',
				sortable: true,
				width: 65, resizable: true,
				align: 'left'
			},{
				header: 'Nombre del Cliente', tooltip: 'Nombre del Cliente',
				dataIndex: 'NOMBRE_CLIENTE',
				sortable: true,	align: 'center',
				width: 170, resizable: true,
				align: 'left'
			},{
				header: 'Pr�stamo', tooltip: 'Pr�stamo',
				dataIndex: 'PRESTAMO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'left'
			},{
				header: 'No. Elec.', tooltip: 'No. Elec.',
				dataIndex: 'NUMERO_ELECTRONICO',
				sortable: true,	align: 'center',
				width: 100, resizable: true,hidden:true,
				align: 'center'
			},{
				header: 'Fecha Operaci�n', tooltip: 'Fecha Operaci�n',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center'
			},{
				header: 'Fecha 1er. pago', tooltip: 'Fecha 1er. pago',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center'
			},{
				header: 'Tasa', tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,	align: 'center',
				width: 50, resizable: true,
				align: 'center'
			},{
				header: 'Moneda', tooltip: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				sortable: true,	align: 'center',
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Spread', tooltip: 'Spread',
				dataIndex: 'SPREAD',
				sortable: true,	align: 'center',
				width: 60, resizable: true,
				align: 'center'
			},{
				header: 'Margen', tooltip: 'Margen',
				dataIndex: 'MARGEN',
				sortable: true,	align: 'center',
				width: 60, resizable: true,
				align: 'center'
			},{
				header: 'Monto Operado', tooltip: 'Monto Operado',
				dataIndex: 'MONTO_OPERADO',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
            renderer: function(val){ return Ext.util.Format.number(val, '$ 0,0.00'); }
			},{
				header: 'Monto 1er. cuota', tooltip: 'Monto 1er. cuota',
				dataIndex: 'MONTO_PRIMER_CUOTA',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
            renderer: function(val){ return Ext.util.Format.number(val, '$ 0,0.00'); }
			},{
				header: 'Neto Otorgado', tooltip: 'Neto Otorgado',
				dataIndex: 'NETO_OTORGADO',   
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
            renderer: function(val){ return Ext.util.Format.number(val, '$ 0,0.00'); }
			}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 926,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
      xtype: 'paging',
			id: 'barraPaginacion',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoTxt',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta8ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'GeneraTXT',
								informacion: 'Consultar',
                        ic_if: parametroGlobal.ic_if,
                        parametro   : parametroGlobal.parametro
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				{
					xtype: 'button',
					text: 'Imprimir',
					id: 'btnImprimirPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta8ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion   :'PDF',
								informacion : 'Consultar',
                        ic_if: parametroGlobal.ic_if,
                        parametro   : parametroGlobal.parametro,
                        start: cmpBarraPaginacion.cursor,
                        limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureImprimirPDF
						});
					}
				}
			]
		}
	});
/*******************************************************************************
*											FORMAS													 *
*******************************************************************************/
	var elementosForma = [
    {
      xtype: 'radiogroup',
      id:'tipoLinea',
      name: 'tipoLinea',
      fieldLabel: 'Tipo de Linea',
      //hidden: true,
      items: [
         {boxLabel: 'LINEA CREDITO', name: 'tipoLinea', id:'tipoLineaC', inputValue: 'C', value:'C' },
         {boxLabel: 'LINEA DESCUENTO', name: 'tipoLinea', id:'tipoLineaD', inputValue: 'D', checked:true, value:'D' }
      ],
      listeners:{
        change: function(rgroup, radio){
          if(radio.value=='C'){
            Ext.getCmp('cliente').hide();
            Ext.getCmp('cliente').setValue(parametroGlobal.sirac);
            parametroGlobal.ic_if =12
            //Ext.getCmp('cliente').allowBlank = true;

          }else{
            Ext.getCmp('cliente').show();
            Ext.getCmp('cliente').setValue("");
            parametroGlobal.ic_if = Ext.getDom('ic_if').value;
            //Ext.getCmp('cliente').allowBlank = true;
          }
		  
		  Ext.getCmp('id_FechaCorte').setValue('');
		  catalogoFechaCorte.load({
          params:{
			tipoLinea: radio.value,
            ic_if: parametroGlobal.ic_if,
			cliente: Ext.getCmp("cliente").getValue()
          }
         });
        }
      }
    },
		{
			xtype: 'combo',
			name: 'moneda',
			id: 'id_moneda',
			hiddenName : 'moneda',
			fieldLabel: 'Moneda',
			emptyText: '- Seleccione -',
			store: catalogoMoneda,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			anchor: '80%'
		},{
			xtype:'numberfield',
			fieldLabel:'Cliente',
			id:'cliente',
			name:'cliente',
			anchor:'55%'
		},{
			xtype: 'numberfield',
			fieldLabel:'Pr�stamo',
			id:'prestamo',
			name:'prestamo',
			anchor:'55%'
		},{
			xtype: 'combo',
			name: 'FechaCorte',
			id: 'id_FechaCorte',
			hiddenName : 'FechaCorte',
			fieldLabel: 'Fecha de corte',
			emptyText: '- Seleccione -',
			store: catalogoFechaCorte,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			anchor: '80%'
		}
	];
	//Forma para hacer la busqueda filtrada
	var fp = {
		xtype: 'form',
		id: 'forma',
		style: 'text-align:left;margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 400,
		labelWidth:100,
		titleCollapse: false,
		title: 'Operados',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosForma],
		buttons: [
		{
			text: 'Consultar',
			id: 'btnConsultar',
			iconCls: 'icoBuscar',
			handler: function(boton, evento) {
				var cmpBarraPaginacion  = Ext.getCmp("barraPaginacion");
				var cmpForma            = Ext.getCmp('forma');
				paramSubmit             = (cmpForma)?cmpForma.getForm().getValues():{};
				
				if(validaPerfil(parametroGlobal.strPerfil)=='LINEA CREDITO'){
					var fechaCorte=Ext.getCmp('id_FechaCorte');
					if(fechaCorte.getValue()==''){
					Ext.Msg.show({
						title:'Mensaje',
						msg:'Debe proporcionar la fecha de corte',
						icon: Ext.Msg.ERROR,
						buttons:Ext.Msg.OK
					});
					return false;
					}
				}
				
				grid.show();
        
				consultaDataGrid.load({
					params: Ext.apply(paramSubmit,{
						ic_if: parametroGlobal.ic_if,
						informacion:   'Consultar',
						operacion:     'Generar',
                  parametro:     parametroGlobal.parametro,
                  start: 0,
                  limit: 15
					})
				});
			} //fin handler			
		},			
		{
			text: 'Limpiar',
			iconCls: 'icoLimpiar',
			handler: function() {
				window.location = '13consulta8ext.jsp';
			}
		},{
			text: 'Bajar Archivo ZIP',
			id :'btnGenerarZip',
			iconCls:'icoZip',
			handler: function(boton, evento) {
				var moneda 	   =     Ext.getCmp('id_moneda');
				var cliente    =     Ext.getCmp('cliente');
				var prestamo   =     Ext.getCmp('prestamo');
				var fechaCorte =     Ext.getCmp('id_FechaCorte');
				
				if(moneda.getValue()!='' || (cliente.getValue()!='' && Ext.getCmp("tipoLinea").getValue().value!='C') || prestamo.getValue()!=''){
					moneda.reset();
					if(Ext.getCmp("tipoLinea").getValue().value!='C'){
						cliente.setValue('');
					}
					prestamo.setValue('');
					Ext.Msg.show({
						title:'Mensaje',
						msg: 'Solo es necesario el criterio de Fecha de Corte para esta consulta',
						icon: Ext.Msg.ERROR,
						buttons: Ext.Msg.OK
					});
					return false;
				}
				if(fechaCorte.getValue()==''){
					Ext.Msg.show({
						title:'Mensaje',
						msg:'Debe proporcionar la fecha de corte',
						icon: Ext.Msg.ERROR,
						buttons:Ext.Msg.OK
					});
					return false;
				}
				boton.disable();
				boton.setIconClass('loading-indicator');
        
        var cmpForma            = Ext.getCmp('forma');
				paramSubmit             = (cmpForma)?cmpForma.getForm().getValues():{};
        
				Ext.Ajax.request({
					url: '13consulta8ext.data.jsp',
					params: Ext.apply(paramSubmit,{
						informacion:'DescargaZip',
						//FechaCorte: Ext.getCmp('id_FechaCorte').getValue(),
            parametro:     parametroGlobal.parametro
					}),
					callback: procesarGenerarZip
				});
			} //fin handler
		},{
			text:'Bajar ZIP',
			id:'btnBajarZip',
			iconCls:'icoZip',
			hidden:true
		}]
	};//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10),
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});
  	//parametroGlobal
   
   obtieneParametroInicial.obtieneParametroFnAjax();
   Ext.getCmp("tipoLinea").hide();
});
