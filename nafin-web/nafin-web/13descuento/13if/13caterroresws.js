 var doEliminar;
 var doModificar;
 
 // Variables a usar cuando se trate de modificaciones
 var doModifyRegister = false;
 var claveAnterior	 = '';

 // CONSTRUIR FORMA, CONFIGURAR MODIFICAR, TRAER DATOS.
Ext.onReady(function(){
  Ext.QuickTips.init();

  var nombreIFTablaCatalogo = Ext.getDom('strNombre').value;
  var claveIFTablaCatalogo = Ext.getDom('iNoCliente').value;

  // 1. VALIDACIONES DE LOS CAMPOS DE ENTRADA
  // 1.1 Validar solo caracteres alfanumericos
  var myValidFn = function(v) {
     var myRegex = /^[a-zA-Z0-9_��]+$/;
     return myRegex.test(v);
  }
  Ext.apply(Ext.form.VTypes, {
       alfanumerico : myValidFn,
       alfanumericoText : 'S�lo se permiten caracteres Alfanum�ricos.'
  });
  // 1.2 Validar solo caracteres alfanumericos y espacios
  myValidFn = function(v){
     var myRegex 			= /^[a-zA-Z0-9_�� ]+$/;
     if(!myRegex.test(v)){
       this.alfanumericoConEspaciosText = 'S�lo se permiten caracteres Alfanum�ricos y Espacios.';
       return false;
     }
     var myRegexVacio 	= /^[ ]+$/;
     if( myRegexVacio.test(v) ){
       this.alfanumericoConEspaciosText = 'La descripci�n no puede llevar s�lo espacios en blanco.';
       return false;
     }
     return true; 
  }
  Ext.apply(Ext.form.VTypes, {
       alfanumericoConEspacios : myValidFn
  });	
  
  // 2. SE CREAN OBJETOS PARA LOS CRITERIOS DE BUSQUEDA
  // 2.1 Input clave
  var txt_clave =  new Ext.form.TextField({
    fieldLabel: 'Clave',
    name: 'clave',
    id: 'clave',
    vtype:'alfanumerico',
    //allowBlank: false,
    msgTarget: 'side',// Mostrar signo de admiracion con alerta de error
    labelStyle:'font-family: Arial, Helvetica, sans-serif; font-size: 11px;',
    autoCreate: {tag: 'input', type: 'text', size: '12', autocomplete: 'off', maxlength: '8' }
  });
  // 2.2 Input Descripcion
  var txt_descripcion = new Ext.form.TextField({
    fieldLabel: 'Descripci�n',
    name: 'descripcion',
    id: 'descripcion',	
    vtype: 'alfanumericoConEspacios',
    //allowBlank: false,
    msgTarget: 'side',// Mostrar signo de admiracion con alerta de error
    labelStyle:'font-family: Arial, Helvetica, sans-serif; font-size: 11px;',
    autoCreate: {tag: 'input', type: 'text', size: '70', autocomplete: 'off', maxlength: '255' }
  });

  // 7.  se define un renderer
  function renderAction(value, p, record){
    if(value == 'Modificar'){
      return String.format('<a href="javascript:doModificar(\'{1}\',\'{2}\');" style="color: #0000FF;text-decoration:none;">{0}</a>',value,record.data.clave,record.data.descripcion);
    }else{
      return String.format('<a href="javascript:doEliminar(\'{1}\');" style="color: #FF0000;text-decoration:none;">{0}</a>',value,record.data.clave);
    }	
  }
   
  var colorTextBlue = function(textoCelda) {
    return '<span style="color: #0000FF;">' + textoCelda + '</span>';
  }
  
  // 3. SE CREA EL MODELO DE COLUMNA A UTILIZAR EN EL GRIDPANEL			
  var modeloDeLaColumna = new Ext.grid.ColumnModel([
      {id:'clave',       header: 'Clave',        width: 100, sortable: true,  dataIndex: 'clave'      ,                          align:'left'  },
      {id:'descripcion', header: 'Descripci�n',              sortable: true,  dataIndex: 'descripcion',                          align:'left'  },
      {id:'modificar',   header: '',             width: 60,  sortable: false, dataIndex: 'modificar', renderer: renderAction,    align:'center', hideable: false},
      {id:'eliminar',    header: '',             width: 60,  sortable: false, dataIndex: 'eliminar',  renderer: renderAction,    align:'center', hideable: false}
  ]);
  
  // 4. SE MAPEAN LOS CAMPOS DEVUELTOS EN LA CONSULTA AL SERVIDOR, CON LOS DEFINIDOS EN NUESTRO MODELO DE COLUMNA
  /*
  var recordFields = [ 
    { name : 'clave',         mapping : 'columnaClave'        },
    { name : 'descripcion',   mapping : 'columnaDescripcion'  },
    { name : 'modificar',      mapping : 'columnaModificar'     }
  ];*/
  
  var recordFields = [ 
    { name : 'clave',         mapping : 'clave'        },
    { name : 'descripcion',   mapping : 'descripcion'  },
    { name : 'modificar',     mapping : 'modificar'    },
    { name : 'eliminar',      mapping : 'eliminar'     }
  ];

  // 8. CONFIGURAR LAS FUNCIONES QUE MANEJARAN LA RESPUESTA A LA CONSULTA 
  var manejadorFallas = function(f,a){
                            if (a.failureType === Ext.form.Action.CONNECT_FAILURE){
                    // Mostrar mensaje de Error
                    Ext.getCmp('textoAviso').setText('Error: '+a.response.status+' '+a.response.statusText );
                    Ext.get('textoAviso').setStyle('color','red');
                    // Actualizar mensaje de error mostrado ( Se realiza varias veces debido a un raro bug )
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                            }
                            if (a.failureType === Ext.form.Action.SERVER_INVALID){
                     var result = Ext.util.JSON.decode(a.response.responseText);
                     // MOSTRAR MENSAJE DE ADVERTENCIA 
                     Ext.getCmp('textoAviso').setText('Error: '+ ( result != undefined && result.errors != undefined && result.errors.reason != undefined?result.errors.reason:""));
                    Ext.get('textoAviso').setStyle('color','red');
                    // Actualizar mensaje de error mostrado ( Se realiza varias veces debido a un raro bug )
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                    Ext.getCmp('contenedorTextoAviso').doLayout();
                    // MOSTRAR POPUP CON MENSAJE SI SE ENVIA ALGUNO
                    if(result != undefined && result.popup != undefined){
                      // Determinar el tipo de mensaje a mostrar
                      var iconType = Ext.Msg.INFO;
                      var title	 = "Info";
                      if(result.popup.type != undefined ){
                        if( result.popup.type == 'ERROR'){
                          iconType = Ext.Msg.ERROR; //Error icon
                          title	 = "Error";
                        }else if( result.popup.type == 'INFO'){
                          iconType = Ext.Msg.INFO; //Information icon 
                          title	 = "Info";
                        }else if( result.popup.type == 'WARNING'){
                          iconType = Ext.Msg.WARNING; //Warning icon  
                          title	 = "Advertencia";
                        }else if( result.popup.type == 'QUESTION'){
                          iconType = Ext.Msg.QUESTION; //Question icon 
                          title	 = "Pregunta";
                        }
                      }
                      //Determinar el mensaje a mostrar
                      var msgPopup = 'undefined';
                      if(result.popup.message != undefined){
                        msgPopup = result.popup.message;
                      }
                      Ext.Msg.show({
                          title:    title,
                          msg:      msgPopup,
                          buttons:  Ext.Msg.OK,
                          icon:     iconType
                      });
                      
                    }
                    
                    
                            }
                        }
       
  var manejadorConsultaExitosa = function(f,a){
                  // Mostrar mensaje de operacion exitosa
                  var result = Ext.util.JSON.decode(a.response.responseText);
                  Ext.getCmp('textoAviso').setText(result.successmsg != undefined?result.successmsg:"");
                  Ext.get('textoAviso').setStyle('color','green');
                  // Actualizar mensaje de error mostrado ( Se realiza varias veces debido a un raro bug )
                  Ext.getCmp('contenedorTextoAviso').doLayout();
                  Ext.getCmp('contenedorTextoAviso').doLayout();
                  Ext.getCmp('contenedorTextoAviso').doLayout();
                  Ext.getCmp('contenedorTextoAviso').doLayout();
                  Ext.getCmp('contenedorTextoAviso').doLayout();
                  // Para que la barra de paginacion pueda enviar parametros extra, se utiliza setBaseParam
                  Ext.StoreMgr.get('ourRemoteStore').setBaseParam('comboIF',		nombreIFTablaCatalogo);
                  Ext.StoreMgr.get('ourRemoteStore').setBaseParam('claveIF',		claveIFTablaCatalogo);
                  Ext.StoreMgr.get('ourRemoteStore').setBaseParam('clave',      	result.clave );      
                  Ext.StoreMgr.get('ourRemoteStore').setBaseParam('descripcion',	result.descripcion);
                  
                  // Permitir volver a modificar un registro cuyo intento previo arrojo mensaje de error
                  if(result.doModifyRegisterAgain != undefined && result.doModifyRegisterAgain == true){
                    doModifyRegister = true;
                    claveAnterior    = result.claveAnterior != undefined?result.claveAnterior:'';
                  }
                    
                  // Actualizar store
                  Ext.StoreMgr.get('ourRemoteStore').loadData(result);
                  
                  // Borrar los campos de la forma si la accion realizada fue guardar o modificar
                  if(result.doSaveAction != undefined && result.doSaveAction == true){
                    gridForm.getForm().reset();
                  }
                  
                  // MOSTRAR POPUP CON MENSAJE SI SE ENVIA ALGUNO
                  if(result != undefined && result.popup != undefined){
                      // Determinar el tipo de mensaje a mostrar
                      var iconType = Ext.Msg.INFO;
                      var title	 = "Info";
                      if(result.popup.type != undefined ){
                        if( result.popup.type == 'ERROR'){
                          iconType = Ext.Msg.ERROR; //Error icon
                          title	 = "Error";
                        }else if( result.popup.type == 'INFO'){
                          iconType = Ext.Msg.INFO; //Information icon 
                          title	 = "Info";
                        }else if( result.popup.type == 'WARNING'){
                          iconType = Ext.Msg.WARNING; //Warning icon  
                          title	 = "Advertencia";
                        }else if( result.popup.type == 'QUESTION'){
                          iconType = Ext.Msg.QUESTION; //Question icon 
                          title	 = "Pregunta";
                        }
                      }
                      //Determinar el mensaje a mostrar
                      var msgPopup = 'undefined';
                      if(result.popup.message != undefined){
                        msgPopup = result.popup.message;
                      }
                      Ext.Msg.show({
                          title:    title,
                          msg:      msgPopup,
                          buttons:  Ext.Msg.OK,
                          icon:     iconType
                      });
                      
                    }
                    
                    
                  
                      }

  // 5. SE CREA EL DATA STORE (JSON STORE) EL CUAL SE ENCARGA DE GESTIONAR LA CONSULTA DE DATOS, Y ALIMENTAR
  // LOS DATOS RECIBIDOS A LOS WIDGETS DE CONSUMO DE DATOS COMO EL GRIDPANEL :)
  var remoteJsonStore = new Ext.data.JsonStore({ 
    fields:         recordFields,
    //url:            '/nafin/13descuento/13if/13CatErroresWSQuery.do',
    url:            '/nafin/13descuento/13if/13caterroresws.data.jsp',
    totalProperty:  'totalCount',
    root:           'registros', // Nombre del Objeto que contendra el resultado de la consulta
    id:             'ourRemoteStore',
    autoLoad:        false,
    remoteSort:      true,
    baseParams: 	 {informacion:'ConsCatalogoWS', start: 0, limit: 25}
  });

  // 6. SE DEFINE EL GRIDPANEL QUE MOSTRARA LOS REGISTROS CONSULTADOS 
  var grid = new Ext.grid.GridPanel({
       id: 'tablaCatalogoErrores',
      store: remoteJsonStore,
      cm: modeloDeLaColumna,
      height: 252,
      autoHeight: true,
      border: true,
      loadMask:true,
      columnLines: true,
      hideHeaders: false,
      disableSelection: true, // Para eliminar la seleccion de la celda
      trackMouseOver:false,   // Para elminar la seleccion de la columna cuando el mouse se encuentra sobre ella
      title:'Cat�logo de Errores WebService - ' + nombreIFTablaCatalogo,
      //style:'text-align:center;',
      autoExpandColumn: 'descripcion',
       bbar: {
                xtype: 'paging',
                pageSize: 25,
                buttonAlign: 'left',
                id: 'barraPaginacion',
          displayInfo: true,
          store: remoteJsonStore,
          displayMsg: '{0} - {1} de {2}',
          emptyMsg: "No hay registros."
            }
   });

  // DEFINIMOS EL MODELO DE SELECCION DE LOS REGISTROS DEL GRIDPANEL
  var selModel = new Ext.grid.CellSelectionModel({ 
        singleSelect: true
  })
  
   // 9. SE DEFINE EL FORM PANEL
   var gridForm = new Ext.FormPanel({
      //url:'35IfAfilIni.do',
      id:           'catalogo',        
      labelAlign:   'top',
      plain:        true,		  
      width:        705,
      border:       false,
      monitorValid: true,
      frame:        false,
      labelAlign:  'right',	
      selModel: 	 selModel,
      failure: manejadorFallas,
      success: manejadorConsultaExitosa,
      style:			'margin: 0 auto',
      bodyStyle: Ext.isIE ? 'padding:20px 5px 15px; text-align: left;' : 'padding:10px 15px; text-align: -moz-left;',
      items: [
        {
            xtype: 'container',
            layout: 'hbox',
            height: 75,
            id: 'contenedorTipoCarga',
            layoutConfig: {
                align: 'middle',
          pack: 'center'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Individual',
                    width: 100,
            pressed: 'true',
                    id: 'botonCargaIndividual'
                },
          {
            xtype: 'label',
            text: 'x',
            style: 'color:white;',
            id: 'espacioBoton'
          },
          {
                    xtype: 'button',
                    text: 'Masiva',
                    width: 100,
                    id: 'botonCargaMasiva',
            handler: function(){
              gridForm.getForm().getEl().dom.action = '/nafin/13descuento/13if/13caterroreswsifm.jsp';
              gridForm.getForm().getEl().dom.submit();
            }
                }
            ]
      },
        {
         xtype: 'container',
         layout: 'hbox',
         id: 'contenedorTextoAviso',
         layoutConfig: {
            pack: 'center',
            align: 'middle',
            padding: 20
         },
         items: [
            {
              xtype: 'label',
              text: '',
              style: 'text-align:center; color:red; font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;',
              id: 'textoAviso'
            }
         ]
      },
      {
        xtype: 'fieldset',
        width: 500,
        labelWidth: 60,
        defaultType: 'textfield',				
        layout:'form',
        border: false,
        items: [txt_clave,txt_descripcion]
      },
      {
        xtype: 'fieldset',
        defaults: {width: 100, border:false},  				
        border: false,								
        buttons: [{
            text: 'Guardar',						
            handler: doGuardar
        },{
            text: 'Consultar',						
            handler: doConsultar
        },{
            text: 'Limpiar',						
            handler: function(){
                    gridForm.getForm().reset();
                  }
                  
          }
        ]
      },			
      grid]
      //renderTo: Ext.get("areaContenido")
  });
  
  // 7. REALIZAR CONSULTA INICIAL
  gridForm.getForm().submit(
       {
         //url: '/nafin/13descuento/13if/13CatErroresWSQuery.do',
         url: '/nafin/13descuento/13if/13caterroresws.data.jsp',
         params: {
            informacion:'ConsCatalogoWS',
            start : 0,
            limit : 25
         },
         clientValidation: false,
         failure: manejadorFallas,
         success: manejadorConsultaExitosa
       }
  );
       
  function doNotification(msg){
    Ext.getCmp('textoAviso').setText(msg != undefined?msg:"");
    Ext.get('textoAviso').setStyle('color','green');
    // Actualizar mensaje de error mostrado ( Se realiza varias veces debido a un raro bug )
    Ext.getCmp('contenedorTextoAviso').doLayout();
    Ext.getCmp('contenedorTextoAviso').doLayout();
    Ext.getCmp('contenedorTextoAviso').doLayout();
    Ext.getCmp('contenedorTextoAviso').doLayout();
    Ext.getCmp('contenedorTextoAviso').doLayout();
  }
  
   function doConsultar(){
     
     // Deshabilitar validacion de campo vacio
     txt_clave.allowBlank = true;
     // Deshabilitar validacion de campo vacio
     txt_descripcion.allowBlank = true;
     // Resetear claveAnterior y campo de modificacion de registro
     doModifyRegister = false;
     claveAnterior    = '';
     
      // Realizar consulta
      gridForm.getForm().submit(
       {
         //url: '/nafin/13descuento/13if/13CatErroresWSQuery.do',
         url: '/nafin/13descuento/13if/13caterroresws.data.jsp',
         params: {
            informacion:'ConsCatalogoWS',
            start : 0,
            limit : 25
         },
         clientValidation: true,
         failure: manejadorFallas,
         success: manejadorConsultaExitosa 
       }
     );
    }
    
    function doGuardar(){  
     // Habilitar la validacion de campo vacio para el campo clave
     txt_clave.allowBlank = false;
     var validacion1 = txt_clave.validateValue(txt_clave.getValue());
     // Habilitar la validacion de campo vacio para el campo descripcion
     txt_descripcion.allowBlank = false;
     var validacion2 = txt_descripcion.validateValue(txt_descripcion.getValue());
     // Si alguno de los campos viene vacio detener la consulta
     if(validacion1 == false || validacion2 == false){
       // Deshabilitar validacion de campo vacio
       txt_clave.allowBlank = true;
       // Deshabilitar validacion de campo vacio
       txt_descripcion.allowBlank = true;
       return; 
     }
     // REalizar consulta
     gridForm.getForm().submit(
       {
         //url: '/nafin/13descuento/13if/13CatErroresWSSave.do',
         url: '/nafin/13descuento/13if/13caterroresws.data.jsp',
         params: {
            informacion: 'CatWsSave',
            start : 0,
            limit : 25,
            doModifyRegister: doModifyRegister,
            claveAnterior:	claveAnterior
         },
         clientValidation: true,
         failure: manejadorFallas,
         success: manejadorConsultaExitosa
       }
     );
     // Deshabilitar validacion de campo vacio
     txt_clave.allowBlank = true;
     // Deshabilitar validacion de campo vacio
     txt_descripcion.allowBlank = true;
     // Resetear claveAnterior y campo de modificacion de registro
     doModifyRegister = false;
     claveAnterior    = '';
    }

     function doModificaRegistro(clave,descripcion){
      txt_clave.setValue(clave); 
      txt_descripcion.setValue(descripcion);
      doNotification('De click en el bot�n "Guardar" cuando haya finalizado sus modificaciones.');
      doModifyRegister = true;
      claveAnterior = txt_clave.getValue();
    }
    
    function doEliminaRegistro(clave){
      Ext.Msg.show({
          title:    'Pregunta',
          msg:      '�Est� seguro de querer eliminar el Registro?',
          buttons:  Ext.Msg.YESNO,
          icon:     Ext.Msg.QUESTION,
          fn:       function(btn, text){
                   //Ext.Msg.alert('Status', 'btn = ' + clave);
                   if (btn == 'yes'){
                    // Realizar Solicitud para Eliminar el Registro
                  gridForm.getForm().submit(
                   {
                     //url: '/nafin/13descuento/13if/13CatErroresWSDelete.do',
                     url: '/nafin/13descuento/13if/13caterroresws.data.jsp',
                     params: {
                        informacion: 'CatWsDelete',
                        start : 0,
                        limit : 25,
                        clave: clave
                     },
                     clientValidation: false,
                     failure: manejadorFallas,
                     success: manejadorConsultaExitosa 
                   }
                 );
                  }
            }
      });
      
    }
    
    doEliminar  = doEliminaRegistro;
    doModificar = doModificaRegistro;
    
    
    var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			NE.util.getEspaciador(20),
      gridForm
		]
	});
    
});