<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	java.text.*,
	netropology.utilerias.*,
	com.netro.afiliacion.*,
	com.netro.parametrosgrales.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.pdf.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% 

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String informacion	= (request.getParameter("informacion") == null) ? "" : request.getParameter("informacion");
String moneda 			= (request.getParameter("moneda") 		== null) ? "" : request.getParameter("moneda");
String cliente 		= (request.getParameter("cliente") 		== null) ? "" : request.getParameter("cliente");
String prestamo 		= (request.getParameter("prestamo") 	== null) ? "" : request.getParameter("prestamo");
String FechaCorte 	= (request.getParameter("FechaCorte") 	== null) ? "" : request.getParameter("FechaCorte");
String operacion_2 		= (request.getParameter("operacion_2") 	== null) ? "" : request.getParameter("operacion_2");
String operacion 		= (request.getParameter("operacion") 	== null) ? "" : request.getParameter("operacion");
String tipoLinea  	= (request.getParameter("tipoLinea") 	== null) ? "" : request.getParameter("tipoLinea");
String cveCliente		= "C".equals(tipoLinea)?"12":iNoCliente;
//iNoCliente 		= (request.getParameter("ic_if") 	== null) ? iNoCliente : request.getParameter("ic_if");
String infoRegresar	= null;
String cs_tipo="";
String guia="";
ResultSet rs_fc;
CQueryHelper	queryHelper = null;
int offset = 0;
ResultSet rs;
String  bgColor = "#FFFFFF"; // default
int TOTAL_CAMPOS = 16;
int nRow = 0, numMon = 0, numDL = 0;
double totMontoOperMon    = 0.0, totMontoOperDL    = 0.0;
double totSaldoInsMon     = 0.0, totSaldoInsDL     = 0.0;
double totCapitalVigMon   = 0.0, totCapitalVigDL   = 0.0;
double totCapitalVencMon  = 0.0, totCapitalVencDL  = 0.0;
double totInteresVencMon  = 0.0, totInteresVencDL  = 0.0;
double totInteresMoratMon = 0.0, totInteresMoratDL = 0.0;
double totAdeudoTotMon    = 0.0, totAdeudoTotDL    = 0.0;	

PagosIFNB  beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
//iNoCliente 		= (request.getParameter("ic_if") 	== null) ? iNoCliente : request.getParameter("ic_if");

if(informacion.equals("Consultar")){
	int start = 0, limit = 10;
	try {
		start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
		limit = Integer.parseInt((request.getParameter("limit")==null)?"0":request.getParameter("limit"));				
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	EstCuentaIfDE paginador = new EstCuentaIfDE();
	paginador.setMoneda     (  moneda      );
	paginador.setCliente    (  cliente     );
	paginador.setPrestamo   (  prestamo    );
	paginador.setFechaCorte (  FechaCorte  );
	paginador.setIc_if      (  cveCliente  );
  paginador.setTipoLinea  (  tipoLinea  );
  //paginador.setIc_if      (  (request.getParameter("ic_if") 	== null) ? iNoCliente : request.getParameter("ic_if")  );
  
	CQueryHelperRegExtJS queryHelperRegExtJS	= new CQueryHelperRegExtJS(   paginador   ); 
	
	if((informacion.equals("Consultar") && operacion.equals("Generar")) || (informacion.equals("Consultar") && operacion.equals(""))){ 
		try {
			if(operacion.equals("Generar")){ //Nueva consulta
				queryHelperRegExtJS.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			String cadena = queryHelperRegExtJS.getJSONPageResultSet(request,start,limit);
			
			if(queryHelperRegExtJS.getIdsSize()>0) {	
				////se guardara en bitacora				
        if(!"LINEA CREDITO".equals(strPerfil)){
          if(!"C".equals(tipoLinea)){
				beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,iNoCliente,"","E");
          }else{
            beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,iNoCliente,"","E", "", "C");
          }
        }else{
          beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,"","","E", iNoCliente, "C");
        }
			}
			
			infoRegresar	= cadena;
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if(operacion.equals("PDF")){
		try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				//String nombreArchivo = queryHelperRegExtJS.getCreatePageCustomFile(request, start, limit,strDirectorioTemp, "PDF");
            
            String linea = "";
            OutputStreamWriter writer = null;
            BufferedWriter buffer = null;
            String nombreArchivo = "";
            StringBuffer contenidoArchivo = new StringBuffer();
            CreaArchivo archivo 	= new CreaArchivo();
              
             nRow = 0; 
            numMon = 0;
            numDL = 0;
            totMontoOperMon    = 0.0;
            totMontoOperDL    = 0.0;
            totSaldoInsMon     = 0.0;
            totSaldoInsDL     = 0.0;
            totCapitalVigMon   = 0.0;
            totCapitalVigDL   = 0.0;
            totCapitalVencMon  = 0.0;
            totCapitalVencDL  = 0.0;
            totInteresVencMon  = 0.0;
            totInteresVencDL  = 0.0;
            totInteresMoratMon = 0.0;
            totInteresMoratDL = 0.0;
            totAdeudoTotMon    = 0.0;
            totAdeudoTotDL    = 0.0;	
            int mx = 0;	int dl = 0;
            
            try {
               session = request.getSession();
               nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
               ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo);
                  
               String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
               String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
               String diaActual    = fechaActual.substring(0,2);
               String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
               String anioActual   = fechaActual.substring(6,10);
               String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                     
               pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
               session.getAttribute("iNoNafinElectronico").toString(),
               (String)session.getAttribute("sesExterno"),
               (String) session.getAttribute("strNombre"),
               (String) session.getAttribute("strNombreUsuario"),
               (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
                     
               pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
               pdfDoc.setTable(14, 100);
               pdfDoc.setCell("Cliente", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Nombre del Cliente", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Fecha Operaci�n", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Pr�stamo", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Tasa ref.", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Spread", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Margen", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Monto Operado", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Saldo insoluto", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Capital vigente", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Capital vencido", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Inter�s vencido", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Inter�s moratorio", "formasmenB", ComunesPDF.CENTER);
               pdfDoc.setCell("Adeudo Total", "formasmenB", ComunesPDF.CENTER);
                       
               Registros regTotales= queryHelperRegExtJS.getPageResultSet(request, start, limit);                       
                           
               while (regTotales.next()) {
                  String ig_cliente 			= regTotales.getString("ig_cliente")==null?"":regTotales.getString("ig_cliente");
                  String fecha_oper 			= regTotales.getString(2)==null?"":regTotales.getString(2);
                  String cg_nombrecliente 	= regTotales.getString("cg_nombrecliente")==null?"":regTotales.getString("cg_nombrecliente");
                  String ig_prestamo 			= regTotales.getString("ig_prestamo")==null?"":regTotales.getString("ig_prestamo");
                  String ig_tasareferencial 	= regTotales.getString("ig_tasareferencial")==null?"":regTotales.getString("ig_tasareferencial");
                  String fg_spread 				= regTotales.getString("fg_spread")==null?"":regTotales.getString("fg_spread");
                  String fg_margen				= regTotales.getString("fg_margen")==null?"":regTotales.getString("fg_margen");
                  String fg_montooperado 		= regTotales.getString("fg_montooperado")==null?"":regTotales.getString("fg_montooperado");
                  String fg_saldoinsoluto 	= regTotales.getString("fg_saldoinsoluto")==null?"":regTotales.getString("fg_saldoinsoluto");
                  String fg_capitalvigente 	= regTotales.getString("fg_capitalvigente")==null?"":regTotales.getString("fg_capitalvigente");
                  String fg_capitalvencido 	= regTotales.getString("fg_capitalvencido")==null?"":regTotales.getString("fg_capitalvencido");
                  String fg_interesvencido	= regTotales.getString("fg_interesvencido")==null?"":regTotales.getString("fg_interesvencido");
                  String fg_interesmorat 		= regTotales.getString("fg_interesmorat")==null?"":regTotales.getString("fg_interesmorat");
                  String fg_adeudototal 		= regTotales.getString("fg_adeudototal")==null?"":regTotales.getString("fg_adeudototal");
                  
                  Vector vecDatos = new Vector();
                  for(int i=1; i<=16; i++) {
                     vecDatos.add(regTotales.getString(i));   
                  }
                 System.err.println("regTotales.getString(14) " + regTotales.getString(14));
                  if(regTotales.getString(14).equals("54")){
                     numDL++;
                     totMontoOperDL    += Double.valueOf(regTotales.getString(8)).doubleValue();
                     totSaldoInsDL     += Double.valueOf(regTotales.getString(9)).doubleValue();
                     totCapitalVigDL   += Double.valueOf(regTotales.getString(10)).doubleValue();
                     totCapitalVencDL  += Double.valueOf(regTotales.getString(12)).doubleValue();
                     totInteresVencDL  += Double.valueOf(regTotales.getString(13)).doubleValue();
                     totInteresMoratDL += Double.valueOf(regTotales.getString(14)).doubleValue();
                     totAdeudoTotDL    += Double.valueOf(regTotales.getString(15)).doubleValue();
                  } else {
                     numMon++;
                     totMontoOperMon    += Double.valueOf(regTotales.getString(8)).doubleValue();
                     totSaldoInsMon     += Double.valueOf(regTotales.getString(9)).doubleValue();
                     totCapitalVigMon   += Double.valueOf(regTotales.getString(10)).doubleValue();
                     totCapitalVencMon  += Double.valueOf(regTotales.getString(11)).doubleValue();
                     totInteresVencMon  += Double.valueOf(regTotales.getString(12)).doubleValue();
                     totInteresMoratMon += Double.valueOf(regTotales.getString(13)).doubleValue();
                     totAdeudoTotMon    += Double.valueOf(regTotales.getString(14)).doubleValue();
                  }
               
                  pdfDoc.setCell(vecDatos.get(0).toString().trim(), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell(vecDatos.get(2).toString().trim(), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell(vecDatos.get(1).toString().trim(), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell(vecDatos.get(3).toString().trim(), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell(vecDatos.get(4).toString().trim(), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell(Comunes.formatoDecimal(vecDatos.get(5).toString().trim(), 5, false), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell(Comunes.formatoDecimal(vecDatos.get(6).toString().trim(), 2, false), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(7).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(8).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(9).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(10).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(11).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(12).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
                  pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(13).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
                  nRow++;
               }
               
               if(nRow > 0) { 
                  if(numDL > 0){
                     pdfDoc.setCell("TOTAL PARCIAL D�LARES", "formasmenB", ComunesPDF.RIGHT, 6);
                     pdfDoc.setCell(" ", "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totMontoOperDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totCapitalVigDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totCapitalVencDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresVencDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresMoratDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAdeudoTotDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
                  }           
                  if(numMon>0){
                     pdfDoc.setCell("TOTAL PARCIAL MONEDA NACIONAL", "formasmenB", ComunesPDF.RIGHT, 6);
                     pdfDoc.setCell(" ", "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totMontoOperMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totCapitalVigMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totCapitalVencMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresVencMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresMoratMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAdeudoTotMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
                  }
               }
               
               Registros vecTotales = queryHelperRegExtJS.getResultCount(request);
               if(vecTotales.getNumeroRegistros()>0){
                  int j = 0;
                  List vecColumnas = null;
                  for(j=0;j<vecTotales.getNumeroRegistros();j++){
                     vecColumnas = vecTotales.getData(j);
                     pdfDoc.setCell("TOTAL " + vecColumnas.get(8).toString(), "formasmenB", ComunesPDF.RIGHT, 6);
                     pdfDoc.setCell("N�mero de registros: " + vecColumnas.get(0).toString(), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(1).toString()),2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(2).toString()),2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(3).toString()),2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(4).toString()),2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(5).toString()),2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(6).toString()),2), "formasmen", ComunesPDF.CENTER);
                     pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(7).toString()),2), "formasmen", ComunesPDF.CENTER);
                  }
               }
               
               pdfDoc.addTable();
               pdfDoc.endDocument();
            } catch(Throwable e) {
               throw new AppException("Error al generar el archivo", e);
            } finally {
               try {
                  //rs.close();
               } catch(Exception e) {}
            }
            
            
            
            
            
            
            
            
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF", e);
			};
	}
	if(operacion.equals("Totales")){
		//Registros registros = queryHelperRegExtJS.doSearch();
      try {
         start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
         limit = Integer.parseInt((request.getParameter("limit")==null)?"0":request.getParameter("limit"));				
      } catch(Exception e) {
         throw new AppException("Error en los parametros recibidos", e);
      }
      
      System.out.println("Start : " + start + " / Limit : " + limit);
      System.out.println("Start : " + start + " / Limit : " + limit);
      
      Registros regTotales= queryHelperRegExtJS.getPageResultSet(request, start, limit);
		List reg=new ArrayList();
		int mx = 0;
		int dl = 0;
				
		while(regTotales.next()){
			Vector vecDatos = new Vector();
			//for(int i=1; i<=TOTAL_CAMPOS; i++) {
            //vecDatos.add(regTotales.getString(i)); break;
            vecDatos.add(regTotales.getString(1));
            vecDatos.add(regTotales.getString(2));
            vecDatos.add(regTotales.getString(3));
            vecDatos.add(regTotales.getString(4));
            vecDatos.add(regTotales.getString(5));
            vecDatos.add(regTotales.getString(6));
            vecDatos.add(regTotales.getString(7));
            vecDatos.add(regTotales.getString(8));
            vecDatos.add(regTotales.getString(9));
            vecDatos.add(regTotales.getString(10));
            vecDatos.add(regTotales.getString(11));
            vecDatos.add(regTotales.getString(12));
            vecDatos.add(regTotales.getString(13));
            vecDatos.add(regTotales.getString(14));
            vecDatos.add(regTotales.getString(15));
            vecDatos.add(regTotales.getString(16));
            System.err.println("FechaCorte " + FechaCorte);
            System.err.println("vecDatos.add(regTotales.getString(16)) " + vecDatos.add(regTotales.getString(16)));
			//}
		  System.err.println("regTotales.getString(15) " + regTotales.getString(15));
			if(regTotales.getString(15).equals("54")){
				numDL++; 
				dl=1;
				totMontoOperDL    += Double.valueOf(regTotales.getString(8)).doubleValue();
				totSaldoInsDL     += Double.valueOf(regTotales.getString(9)).doubleValue();
				totCapitalVigDL   += Double.valueOf(regTotales.getString(10)).doubleValue();
				totCapitalVencDL  += Double.valueOf(regTotales.getString(11)).doubleValue();
				totInteresVencDL  += Double.valueOf(regTotales.getString(12)).doubleValue();
				totInteresMoratDL += Double.valueOf(regTotales.getString(13)).doubleValue();
				totAdeudoTotDL    += Double.valueOf(regTotales.getString(14)).doubleValue();
			} else {		
				numMon++; 
				mx=1;
				totMontoOperMon    += Double.valueOf(regTotales.getString(8)).doubleValue();
				totSaldoInsMon     += Double.valueOf(regTotales.getString(9)).doubleValue();
				totCapitalVigMon   += Double.valueOf(regTotales.getString(10)).doubleValue();
				totCapitalVencMon  += Double.valueOf(regTotales.getString(11)).doubleValue();
				totInteresVencMon  += Double.valueOf(regTotales.getString(12)).doubleValue();
				totInteresMoratMon += Double.valueOf(regTotales.getString(13)).doubleValue();
				totAdeudoTotMon    += Double.valueOf(regTotales.getString(14)).doubleValue();
			}
         System.err.println("//------------------------------------------------------------------");
         System.err.println("regTotales.getString(8)" + regTotales.getString(8));
         System.err.println("regTotales.getString(9)" + regTotales.getString(9));
         System.err.println("regTotales.getString(10)" + regTotales.getString(10));
         System.err.println("regTotales.getString(11)" + regTotales.getString(11));
         System.err.println("regTotales.getString(12)" + regTotales.getString(12));
         System.err.println("regTotales.getString(13)" + regTotales.getString(13));
         System.err.println("regTotales.getString(14)" + regTotales.getString(14));
         System.err.println("//------------------------------------------------------------------");
		}
			
      HashMap datos;	
      if(mx>0){
         datos = new HashMap();
         datos.put("MONEDA","Total Parcial Moneda Nacional");
         datos.put("N_REGISTROS","");
         datos.put("MONTO_OPERADO","" + totMontoOperMon); 
         datos.put("SALDO_INSOLUTO","" + totSaldoInsMon);
         datos.put("CAPITAL_VIG","" + totCapitalVigMon);
         datos.put("CAPITAL_VENCIDO","" + totCapitalVencMon);
         datos.put("INTERES_VENCIDO","" + totInteresVencMon);
         datos.put("INTERES_MORATORIO","" + totInteresMoratMon);
         datos.put("ADEUDO_TOTAL","" + totAdeudoTotMon);
         reg.add(datos);
      }		
      if(dl>0){
         datos = new HashMap();
         datos.put("MONEDA",           "Total Parcial Moneda D�lares");
         datos.put("N_REGISTROS",      "");
         datos.put("MONTO_OPERADO",    "" + totMontoOperDL);
         datos.put("SALDO_INSOLUTO",   "" + totSaldoInsDL);
         datos.put("CAPITAL_VIG",      "" + totCapitalVigDL);
         datos.put("CAPITAL_VENCIDO",  "" + totCapitalVencDL);
         datos.put("INTERES_VENCIDO",  "" + totInteresVencDL);
         datos.put("INTERES_MORATORIO","" + totInteresMoratDL);
         datos.put("ADEUDO_TOTAL",     "" + totAdeudoTotDL);
         reg.add(datos);
      }
			
		Registros vecTotales = queryHelperRegExtJS.getResultCount(request);
		if(vecTotales!=null && vecTotales.getNumeroRegistros()>0){
			int j = 0;
			List vecColumnas = null;
			for(j=0;j<vecTotales.getNumeroRegistros();j++){
				vecColumnas = vecTotales.getData(j);
				
				datos = new HashMap();
				datos.put("MONEDA","Total " + vecColumnas.get(8).toString());
				datos.put("N_REGISTROS",vecColumnas.get(0).toString());
				datos.put("MONTO_OPERADO","" + vecColumnas.get(1).toString().toString());
				datos.put("SALDO_INSOLUTO","" + vecColumnas.get(2).toString().toString());
				datos.put("CAPITAL_VIG","" + vecColumnas.get(3).toString().toString());
				datos.put("CAPITAL_VENCIDO","" + vecColumnas.get(4).toString().toString());
				datos.put("INTERES_VENCIDO","" + vecColumnas.get(5).toString().toString());
				datos.put("INTERES_MORATORIO","" + vecColumnas.get(6).toString().toString());
				datos.put("ADEUDO_TOTAL","" + vecColumnas.get(7).toString().toString());
				reg.add(datos);
			}
		}
			
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar=jsonObj.toString();
	}else if(operacion.equals("GeneraTXT")){
		AccesoDB con = new AccesoDB();
		CreaArchivo archivo              = new CreaArchivo();
		StringBuffer contenidoArchivo    = new StringBuffer("");
		String nombreArchivo             = null;
		
		try {
			con.conexionDB();
			CQueryHelper queryHelperJS = new CQueryHelper(new EstCuentaIfDE());			
			contenidoArchivo.append("FechaFinMes         ;Moneda              ;Descripcion         ;Intermediario       ;Descripcion         ;CodSucursal         ;NomSucursal         ;TipIntermediario    ;Descripcion         ;DescModPago         ;ProdBanco           ;Descripcion         ;SubApl              ;Descripcion         ;Cliente             ;NombreCliente       ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Disposicion         ;FrecInteres         ;FrecCapital         ;FechOperacion       ;FechVencimiento     ;TasaReferencial     ;DescripcionTasa     ;RelMat1             ;Spread              ;Margen              ;TasaMoratoria       ;Sancion             ;1aCuotaVen          ;DiaVencimiento      ;DiaProvision        ;MontoOperado        ;SaldoInsoluto       ;CapitalVigente      ;CapitalVencido      ;InteresProvi        ;IntCobAnt           ;InteresGravProv     ;IVAProv             ;InteresVencido      ;InteresVencidoGravado;IVAVencido          ;InteresMorat        ;InteresMoratGravado ;IVAsobreMoratorios  ;SobreTasaMor        ;SobreTasaMorGravado ;OtrosAdeudos        ;ComisionGtia        ;Comisiones          ;SobTasaGtia%        ;FinAdicOtorg        ;FinanAdicRecup      ;TotalFinan          ;AdeudoTotal         ;CapitalRecup        ;InteresRecup        ;InteresRecupGravado ;MoraRecup           ;MoraRecupGravado    ;IVA Recup           ;SubsidioAplicado    ;SaldoNafin          ;SaldoBursatil       ;ValorTasa           ;TasaTotal           ;EdoCartera          ;\n");
			rs = queryHelperJS.getCreateFile(request,con);
		
			GenerarContenidoArchivos conten = new GenerarContenidoArchivos();
			String contArchivo = conten.llenarContenido(rs, "S", 3,"","");
			
			if(!archivo.make(contArchivo, strDirectorioTemp, ".txt"))
				System.err.print("<--!Error al generar el archivo-->");
			else
				nombreArchivo = archivo.nombre;
					               
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Exception e) {
			 System.err.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta() == true)
			con.cierraConexionDB();
		}
	}
}

else if(informacion.equals("DescargaZip")){
   
  String nombreZip = "archedocuenta_"+FechaCorte.replace('/','_')+"_"+cveCliente+"_S.zip";
  //if("C".equals(tipoLinea)){
    EstCuentaIfDE paginador = new EstCuentaIfDE();
    paginador.setMoneda     (  moneda      );
    paginador.setCliente    (  cliente     );
    paginador.setPrestamo   (  prestamo    );
    paginador.setFechaCorte (  FechaCorte  );
    paginador.setTipoLinea  (  tipoLinea  );
    paginador.setIc_if      (  cveCliente );
    //paginador.setIc_if      (  ("C".equals(tipoLinea))?"12":iNoCliente );
    //paginador.setIc_if      (  (request.getParameter("ic_if") 	== null) ? iNoCliente : request.getParameter("ic_if")  );
    
    CQueryHelperRegExtJS queryHelperRegExtJS	= new CQueryHelperRegExtJS(   paginador   ); 
    String nombreArchivo = queryHelperRegExtJS.getCreateCustomFile(request, strDirectorioTemp, "ZIP");
    
    GenerarArchivoZip zip = new GenerarArchivoZip();
    //String directory = strDirectorioPublicacion + "16archivos/descuento/procesos/edocuenta/";
    zip.generandoZip(strDirectorioTemp,strDirectorioTemp,nombreArchivo.replaceAll(".txt",""));
    //beanPagos.guardarArchivosZip(nombreZip, strDirectorioTemp);
  /*  
  }else{
	
   String path =  beanPagos.consultaArchivosZip(nombreZip, strDirectorioTemp);
	System.out.println(" path ********* >>>  "+path);
	if(path.equals("")){
		System.out.println(" ENTRO path ");
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String fechaCorte = FechaCorte.replace('/','_');
		String nombreArchivo = "archedocuenta_"+FechaCorte.replace('/','_')+"_"+cveCliente+"_S.txt";
		nombreArchivo = "archedocuenta_"+fechaCorte+"_"+cveCliente+"_S.txt";
		writer = new OutputStreamWriter(new FileOutputStream(strDirectorioTemp + nombreArchivo, false),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
		GenerarArchivoZip zip = new GenerarArchivoZip();
		zip.generandoZip(strDirectorioTemp,strDirectorioTemp,nombreArchivo.replaceAll(".txt",""));
	}
	
  }*/
	//String pathFileZip = "/nafin/16archivos/descuento/procesos/edocuenta/archedocuenta_"+FechaCorte.replace('/','_') + "_" + iNoCliente + "_S.zip";
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreZip);
	infoRegresar = jsonObj.toString();
}
 
else if(informacion.equals("CatalogoMoneda")){
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setClave("ic_moneda");
	cat.setDescripcion("cd_nombre");
	cat.setValoresCondicionIn("1,54", Integer.class);
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();
}

else if(informacion.equals("CatalogoFechaCorte")){
	AccesoDB con = new AccesoDB();
	try{
		con.conexionDB();
		System.out.println("cveCliente ==== "+cveCliente);
		cs_tipo=beanPagos.getTipoIF(cveCliente);
		
		if("B".equals(cs_tipo)){
			guia="GuiaPagosdeCarteraIFB.htm";
		}else{
			guia="GuiaPagosdeCarteraIFNB.htm";
		}	
		
		queryHelper = new CQueryHelper(new EstCuentaIfDE());
		queryHelper.cleanSession(request);
		
		String qryString= "";
		
		if(!"C".equals(tipoLinea)){
			qryString= " Select distinct TO_CHAR (df_fechafinmes,'dd/mm/yyyy'),df_fechafinmes "+
			" From com_estado_cuenta"+
				" where ic_if ="+cveCliente+
				" order by df_fechafinmes";
		}else{
			qryString= " Select distinct TO_CHAR (df_fechafinmes,'dd/mm/yyyy'),df_fechafinmes "+
				" From com_estado_cuenta"+
				" where ic_if = 12" +
				" and ig_cliente = "+cliente+
			" order by df_fechafinmes";
		}
			
		rs = con.queryDB(qryString);
		List reg = new ArrayList();
		HashMap datos;
		while (rs.next()) {
			datos = new HashMap();
			datos.put("clave",rs.getString(1));
			datos.put("descripcion",rs.getString(1));
			reg.add(datos);
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
		rs.close();
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
}else if(informacion.equals("ObtenerSiracFinanciera")){
  
  JSONObject jsonObj = new JSONObject();
  jsonObj.put("success", new Boolean(true));
  
  if(!"LINEA CREDITO".equals(strPerfil)){
    HashMap hmDataIf = beanPagos.getSiracFinaciera(cveCliente);
    
	String numSirac = (hmDataIf.get("NUMERO_SIRAC")==null || ((String)hmDataIf.get("NUMERO_SIRAC")).equals(""))?"0":(String)hmDataIf.get("NUMERO_SIRAC");
    jsonObj.put("IC_FINANCIERA", hmDataIf.get("IC_FINANCIERA"));
    jsonObj.put("NUMERO_SIRAC", numSirac);
  }else{
    String numSirac = beanPagos.getSiracLineaCredito(cveCliente);
    jsonObj.put("IC_FINANCIERA","");
    jsonObj.put("NUMERO_SIRAC", ("".equals(numSirac)?"0":numSirac));
  }
  infoRegresar = jsonObj.toString();
}

%>
<%= infoRegresar%>
