<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*,
		com.netro.descuento.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion")!= null)?request.getParameter("informacion"):"";
	String infoRegresar = "";
	 if(informacion.equals("Consulta")){
		PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
		//strTipoBanco;
		
		String sTipoBanco = (session.getAttribute("strTipoBanco")==null)?"":session.getAttribute("strTipoBanco").toString().trim();
		Registros reg= bean.getArchivoPagos(iNoCliente);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("sTipoBanco",sTipoBanco);
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("registros", reg.getJSONData());
		infoRegresar=jsonObj.toString();
	 }else  if(informacion.equals("ConsultaPagos")){
		PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
		String hidBitacora = (request.getParameter("hidBitacora")!= null)?request.getParameter("hidBitacora"):"";
		String sFechaVencIni = (request.getParameter("sFechaVencIni")!= null)?request.getParameter("sFechaVencIni"):"";
		String sFechaVencFin = (request.getParameter("sFechaVencFin")!= null)?request.getParameter("sFechaVencFin"):"";
		String sFechaProbIni = (request.getParameter("sFechaProbIni")!= null)?request.getParameter("sFechaProbIni"):"";
		String sFechaProbFin = (request.getParameter("sFechaProbFin")!= null)?request.getParameter("sFechaProbFin"):"";
		String icMoneda = (request.getParameter("icMoneda")!= null)?request.getParameter("icMoneda"):"";

		Integer totalReg=new Integer(0);
		Double totalValor=new Double(0.0d);
		
		bean.setBitacoraPagos(iNoCliente, hidBitacora, sFechaVencIni, sFechaVencFin, sFechaProbIni);

		String sTipoBanco = (session.getAttribute("strTipoBanco")==null)?"":session.getAttribute("strTipoBanco").toString().trim();


		List listaLista=bean.getPagosFecha(icMoneda,iNoCliente,sFechaVencIni,sFechaVencFin,sFechaProbIni,sFechaProbFin,sTipoBanco,totalReg,totalValor);
		List listaReg=(List)listaLista.remove(0);
		List reg=(List)listaLista.remove(0);
		int i=1;
		JSONObject jsonObj 	      = new JSONObject();
		while(!listaReg.isEmpty()){
			jsonObj.put("registrosDetalles"+i, listaReg.remove(0));
			jsonObj.put("registrosEncabezados"+i, listaReg.remove(0));
			i++;
		}
		
		jsonObj.put("registros2", JSONArray.fromObject(reg));
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("totales", i+"");
		infoRegresar=jsonObj.toString();
	 }
%>
<%=infoRegresar%>