Ext.onReady(function() {

//_--------------------------------- HANDLERS -------------------------------

	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
					
					var fp = Ext.getCmp('forma');
					fp.el.unmask();			
					
					if(jsonData.moneda_mn =='S' &&   jsonData.moneda_dl =='S')  {						
						Ext.getCmp("in_limite_epom_1").show();
						Ext.getCmp("in_limite_epo_dl_1").show();
					
					}else  if(jsonData.moneda_mn =='S' &&   jsonData.moneda_dl =='N')  {
						Ext.getCmp("in_limite_epom_1").show();
						Ext.getCmp("in_limite_epo_dl_1").hide();
					
					}else  if(jsonData.moneda_mn =='N' &&   jsonData.moneda_dl =='S')  {					
						Ext.getCmp("in_limite_epom_1").hide();
						Ext.getCmp("in_limite_epo_dl_1").show();
					}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	//GENERAR ARCHIVO DE GRID DE DETALLE DE DOCUMENTOS
	var procesarSuccessFailureGenerarArchivoVer =  function(opts, success, response) {
		var btnGenerarArchivoVer = Ext.getCmp('btnGenerarArchivoVer');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivoVer = Ext.getCmp('btnBajarArchivoVer');
			btnBajarArchivoVer.show();
			btnBajarArchivoVer.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivoVer.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivoVer.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	//GENERAR ARCHIVO DE GRID EN PDF DE DETALLE DE DOCUMENTOS
	var procesarSuccessFailureGenerarArchivoPDFVer =  function(opts, success, response) {
		var btnImprimirPDFVer = Ext.getCmp('btnImprimirPDFVer');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFVer = Ext.getCmp('btnBajarPDFVer');
			btnBajarPDFVer.show();
			btnBajarPDFVer.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFVer.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnImprimirPDFVer.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}



	var procesarVerDetalle = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_epo = registro.get('CVEEPO');
		consultaDataVER.load({	params: {	ic_epo: ic_epo }	});
		new Ext.Window({
				layout: 'fit',
				width: 800,
				height: 305,
				id: 'winVerDetalle',
				closeAction: 'close',
				items: [
					gridVerDetalle
				],
				title: 'Detalle Doctos Programados'
				}).show();
	}

//-------------------------------- STORES -----------------------------------

//consulta para el grid de detalles
	var consultaDataVER = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'VerDetalle',
			operacion: 'Generar',
			start: 0,
			limit: 15
		},
		fields: [
			{name:'IC_EPO'},
			{name:'NOMBREEPO'},
			{name:'NOMBREPYME'},
			{name:'NUMERODOCTO'},
			{name:'FECHADOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name:'FECHAVENC', type: 'date', dateFormat: 'd/m/Y'},
			{name:'NOMBREMONEDA'},
			{name:'NOMBRE_TIPO_FACTORAJE'},
			{name:'MONTODOCTO', type: 'float'},
			{name:'PORCANTICIPO'},
			{name:'MONTODSCTO', type: 'float'},
			{name:'NOMBREIF'},
			{name:'PLAZO'},
			{name:'IMPORTEINTERES', type: 'float'},
			{name:'IMPORTERECIBIR', type: 'float'},
			{name:'ACUSE'},
			{name:'REFERENCIA'},
			{name:'IMPORTENETO', type: 'float'},
			{name:'BENEFICIARIO'},
			{name:'PORCBENEFICIARIO'},
			{name:'NOMBRE_MANDANTE'},
			{name: 'DF_PROGRAMACION', type: 'date', dateFormat: 'd/m/Y'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});


	//CREA EL GRID PARA LA OPCION DE VER
	var gridVerDetalle = {
		xtype: 'grid',
		store: consultaDataVER,
		id: 'gridVerDetalle',
		margins: '20 0 0 0',
		columns: [
		{
				header: 'No EPO',
				tooltip: 'No EPO',
				dataIndex: 'IC_EPO',
				sortable: true,
				width: 10,
				resizable: true,
				hidden: true
			},
			{
				header: 'Nombre Epo',
				tooltip: 'Nombre Epo',
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false
			},
			{
				header: 'Nombre Pyme',
				tooltip: 'Nombre Pyme',
				dataIndex: 'NOMBREPYME',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right'
			},
			{
				header: 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex: 'NUMERODOCTO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			},
			{
				header: 'Fecha de Emision',
				tooltip: 'Fecha de Emision',
				dataIndex: 'FECHADOCTO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Emision',
				dataIndex: 'FECHAVENC',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{	header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'NOMBREMONEDA',
				sortable: true,
				hideable: false,
				align: 'center',
				width: 130
			},
			{
				header: 'Tipo de Factoraje',
				tooltip: 'Tipo de Factoraje',
				dataIndex: 'NOMBRE_TIPO_FACTORAJE',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			},
			{
				header: 'Monto de Documento',
				tooltip: 'Monto de Documento',
				dataIndex: 'MONTODOCTO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORCANTICIPO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTODSCTO',
				sortable : true,
				width : 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex : 'NOMBREIF',
				sortable : true,
				width : 130
			},

			{	header: 'Tasa Plazo Dias',
				tooltip: 'Tasa Plazo Dias',
				dataIndex: 'PLAZO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			},
			{	header: 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex: 'IMPORTEINTERES',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{	header: 'Importe a Recibir',
				tooltip: 'Importe a Recibir',
				dataIndex: 'IMPORTERECIBIR',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	,
			{	header: 'N�mero de Acuse',
				tooltip: 'N�mero de Acuse',
				dataIndex: 'ACUSE',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			}	,
			{	header: 'Referencia',
				tooltip: 'Referencia',
				dataIndex: 'REFERENCIA',
				sortable: true,
				hideable: false,
				width: 130
			},
			{	header: 'Neto a Recibir',
				tooltip: 'Neto a Recibir',
				dataIndex: 'IMPORTENETO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	,
			{	header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				hideable: false,
				width: 130
			},
			{	header: 'Porcentaje del Beneficiario',
				tooltip: 'Porcentaje del Beneficiario',
				dataIndex: 'PORCBENEFICIARIO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			}	,
			{	header: 'Mandatario',
				tooltip: 'Mandatario',
				dataIndex: 'NOMBRE_MANDANTE',
				sortable: true,
				hideable: false,
				width: 130
			},
			{	header: 'Fecha del Registro de la operaci�n',
				tooltip: 'Fecha del Registro de la operaci�n',
				dataIndex: 'DF_PROGRAMACION',
				sortable: true,
				hideable: false,
				width: 130,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 885,
		title: '',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacionVer',
			displayInfo: true,
			store: consultaDataVER,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivoVer',
					handler: function(boton, evento) {
						boton.disable();
						Ext.Ajax.request({
							url: '13consulta04ext.data.jsp',
							params: {
								informacion: 'ArchivoDetalleCSV',
								tipo: 'CSV',
								ic_epo: consultaDataVER.getAt(0).get('IC_EPO')
							},
							callback: procesarSuccessFailureGenerarArchivoVer
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivoVer',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnImprimirPDFVer',
					handler: function(boton, evento) {
						boton.disable();
						Ext.Ajax.request({
							url: '13consulta04ext.data.jsp',
							params: {
								informacion: 'ArchivoDetallePDF',
								tipo: 'PDF',
								ic_epo: consultaDataVER.getAt(0).get('IC_EPO')
							},
							callback: procesarSuccessFailureGenerarArchivoPDFVer
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDFVer',
					hidden: true
				},
				'-'
			]
		}
	};





//_--------------------------------- PARA EL PANEL  GENERAL -------------------------------

//_--------------------------------- HANDLERS -------------------------------

//GRID GENERAL
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		
		fp.el.unmask();
		if (arrRegistros != null) {

			if (!grid.isVisible()) {
				grid.show();
			}

			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}
				if(!btnBajarPDF.isVisible()) {
					btnImprimirPDF.enable();
				} else {
					btnImprimirPDF.disable();
				}

				el.unmask();
			} else {
				btnGenerarArchivo.disable();
				btnImprimirPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}



	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarArchivoPDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}


//GENERAR ARCHIVO DE GRID GENERAL
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//-------------------------------- STORES -----------------------------------
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});



	//CONSULTA PARA LOS DEL GRID GENERAL
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
		  {name:'CVEEPO'},
			{name: 'NOMBRE_EPO'},
			{name: 'LIMITE'},
			{name: 'PORCENTAJE'},
			{name: 'DISPONIBLE', type: 'float'},
			{name: 'ESTATUS'},
			{name: 'MONTO_COMPROMETIDO', type: 'float'},
			{name: 'MONTODISPONIBLE', type: 'float'},
			{name: 'FECHALINEA', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHACAMBIO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CSFECHALIMITE'},
			{name: 'LIMITE_DL'},
			{name: 'PORCENTAJE_DL'},
			{name: 'DISPONIBLE_DL', type: 'float'},
			{name: 'MONTODISPONIBLE_DL', type: 'float'},
			{name: 'MONTO_COMPROMETIDO_DL', type: 'float'},
			{name: 'DESCRIPCION_ESTATUS'},			
			{name: 'FECHA_BLOQ_DESB'},
			{name: 'USUARIO_BLOQ_DESB'},
			{name: 'DEPENDENCIA_BLOQ_DESB'}				
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}

	});

//-------------------------------------------------------------------

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Seleccione...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						
						Ext.Ajax.request({
							url: '13consulta04ext.data.jsp',
							params: {
								informacion: "valoresIniciales",
								ic_epo:combo.getValue()
							},
							callback: procesaValoresIniciales
						});
					}
				}
			}
		},		
		{
			xtype: 'numberfield',
			fieldLabel: 'Monto L�mite Moneda Nacional Hasta ',
			name: 'in_limite_epom',
			id: 'in_limite_epom_1',
			allowBlank: true,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 20,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'Monto L�mite D�lar  Hasta ',
			name: 'in_limite_epo_dl',
			id: 'in_limite_epo_dl_1',
			allowBlank: true,
			maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 20,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		}
		];

	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: ' ', colspan: 16, align: 'center'},
				{header: '<B>Cambio de Estatus</B>', colspan: 4, align: 'center'}					
			]
		]
	});
	
	// create the Grid GENERAL
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		margins: '20 0 0 0',
		plugins: grupos,
		columns: [
		{
				header: 'cveEpo',
				tooltip: 'cveEpo',
				dataIndex: 'CVEEPO',
				sortable: true,
				resizable: true,
				hidden: true
			},
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header: '<center>Monto L�mite <br> Moneda Nacional',
				tooltip: 'Monto L�mite Moneda Nacional',
				dataIndex: 'LIMITE',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: '<center> Porcentaje de utilizaci�n <br> Moneda Nacional',
				tooltip: 'Porcentaje de utilizaci�n  Moneda Nacional',
				dataIndex: 'PORCENTAJE',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: '<center>  Monto Disponible <br> Moneda Nacional',
				tooltip: 'Monto Disponible  Moneda Nacional',
				dataIndex: 'DISPONIBLE',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: '<center>Monto L�mite <br> D�lar Americano',
				tooltip: 'Monto L�mite D�lar Americano',
				dataIndex: 'LIMITE_DL',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: '<center> Porcentaje de utilizaci�n  <br> D�lar Americano',
				tooltip: 'Porcentaje de utilizaci�n  D�lar Americano',
				dataIndex: 'PORCENTAJE_DL',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: '<center>  Monto Disponible  <br> D�lar Americano',
				tooltip: 'Monto Disponible D�lar Americano',
				dataIndex: 'DISPONIBLE_DL',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Susceptible a Operarse <br> en Factoraje 24 Horas <br> Moneda Nacional',
				tooltip: 'Monto Susceptible a Operarse en Factoraje 24 Horas  Moneda Nacional',
				dataIndex: 'MONTO_COMPROMETIDO',
				sortable: true,
				hideable: false,
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Susceptible a Operarse <br> en Factoraje 24 Horas <br> D�lar Americano',
				tooltip: 'Monto Susceptible a Operarse en Factoraje 24 Horas D�lar Americano',
				dataIndex: 'MONTO_COMPROMETIDO_DL',
				sortable: true,
				hideable: false,
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Disponible despu�s de <br>Factoraje 24 Horas Moneda Nacional',
				tooltip: 'Monto Disponible despu�s de Factoraje 24 Horas Moneda Nacional',
				dataIndex: 'MONTODISPONIBLE',
				sortable: true,
				hideable: false,
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Disponible despu�s de  <br> Factoraje 24 Horas  D�lar Americano',
				tooltip: 'Monto Disponible despu�s de Factoraje 24 Horas D�lar Americano',
				dataIndex: 'MONTODISPONIBLE_DL',
				sortable: true,
				hideable: false,
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalle Doctos Programados',
				tooltip: 'Detalle Doctos Programados',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('MONTO_COMPROMETIDO') > 0) {
								this.items[0].tooltip = 'Ver detalle';
								return 'iconoLupa';
							}
						},
						handler: procesarVerDetalle
					}
				]
/*				renderer: 	function (valor, metadata, registro) {
					var valor= registro.get('MONTO_COMPROMETIDO');
					var ic_epo= registro.get('CVEEPO');
					if(valor>0) {
						return String.format('<a href="javascript:verDetalle(\'{0}\');" style="color: #FF0000;text-decoration:none;"><img src="" ></a>',ic_epo);
					}
				}*/
			},
			{
				header : 'Fecha Vencimiento L�nea de Cr�dito',
				tooltip: 'Fecha Vencimiento L�nea de Cr�dito',
				dataIndex : 'FECHALINEA',
				sortable : true,
				width : 130,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Cambio de Administraci�n',
				tooltip: 'Fecha de Cambio de Administraci�n',
				dataIndex : 'FECHACAMBIO',
				sortable : true,
				width : 130,
				align: 'center',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{	header: 'Validar Fechas L�mite',
				tooltip: 'Validar Fechas L�mite',
				dataIndex: 'CSFECHALIMITE',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'DESCRIPCION_ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},					 
			{
				header: 'Fecha de  <br> Bloqueo/Desbloqueo',
				tooltip: 'Fecha de Bloqueo/Desbloqueo',
				dataIndex: 'FECHA_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: '<center> Usuario <br> de Bloqueo/Desbloqueo ',
				tooltip: 'Usuario de Bloqueo/Desbloqueo ',
				dataIndex: 'USUARIO_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},			
			{
				header: '<center> Dependencia que <br> Bloqueo/Desbloqueo',
				tooltip: 'Dependencia que Bloqueo/Desbloqueo ',
				dataIndex: 'DEPENDENCIA_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			}	
		],
		
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 885,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						Ext.Ajax.request({
							url: '13consulta04ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoCSV',
								tipo: 'CSV'
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnImprimirPDF',
					handler: function(boton, evento) {
						boton.disable();
						Ext.Ajax.request({
							url: '13consulta04ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'ArchivoPDF',
								tipo: 'PDF'
							}),
							callback: procesarSuccessFailureGenerarArchivoPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-'
			]
		}

	});

//-------------------------------- PRINCIPAL -----------------------------------

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		style: 'margin:0 auto;',
		width: 600,
		title: 'Limites Por EPO - Criterios de B�squeda.',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar', //Generar datos para la consulta
							start: 0,
							limit: 15
						})
					});
					Ext.getCmp('btnGenerarArchivo').disable();
					Ext.getCmp('btnBajarArchivo').hide();
					Ext.getCmp('btnImprimirPDF').disable();
					Ext.getCmp('btnBajarPDF').hide();

				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta04ext.jsp';
				}
			}

		]

	});

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
//		layout: 'vbox',
		width: 890,
//		height: 3000,
//		layoutConfig: {
//			align:'center'
//		},
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid
		]
	});

//-------------------------------- ----------------- -----------------------------------

	catalogoEPOData.load();
	
	//Peticion para obtener valores iniciales y la parametrizaci�n
 fp.el.mask('Cargando...', 'x-mask-loading');			
	Ext.Ajax.request({
		url: '13consulta04ext.data.jsp',
		params: {
			informacion: "valoresIniciales"			
		},
		callback: procesaValoresIniciales
	});
	
});