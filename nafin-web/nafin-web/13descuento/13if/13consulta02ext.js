Ext.onReady(function() {

//_--------------------------------- HANDLERS -------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				//var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				//contenedorPrincipalCmp.add(grid);
				//contenedorPrincipalCmp.doLayout();
				grid.show();
			}
			//var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			//var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');

			var btnTotales = Ext.getCmp('btnTotales');

			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnGenerarArchivo.enable();
				/*btnBajarPDF.hide();
				if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}*/
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
/*
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}
//-------------------------------- STORES -----------------------------------
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPymeData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCambioEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMBREEPO'},
			{name: 'NOMBREPYME'},
			{name: 'CD_NOMBRE'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'DF_FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IN_TASA_ACEPTADA'},
			{name: 'PLAZO'},
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO'},
			{name: 'FN_MONTO_DSCTO', type: 'float'},
			{name: 'IN_IMPORTE_RECIBIR'},
			{name: 'REFERENCIA_PYME'},
			{name: 'CAMBIOESTATUS'},
			{name: 'DC_FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CT_CAMBIO_MOTIVO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});


	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'col0'},
			{name: 'TOTAL_MONTO_DOCUMENTOS', type: 'float', mapping: 'col1'},
			{name: 'TOTAL_MONTO_DESCUENTO', type: 'float', mapping: 'col2'},
			{name: 'TOTAL_IMPORTE_RECIBIR', type: 'float', mapping: 'col3'},
			//{name: 'CLAVE_MONEDA', type: 'float', mapping: 'col4'}
			{name: 'MONEDA', mapping: 'col5' }
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
		
	});


//-------------------------------------------------------------------

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'cmbEpo',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Seleccione...',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPOData,
			//tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var pymeComboCmp = Ext.getCmp('cmbPyme');
						pymeComboCmp.setValue('');
						pymeComboCmp.setDisabled(false);
						pymeComboCmp.store.load({
								params: {
									claveEpo: combo.getValue()
								}
						});
					}
				}
			},
			tpl: '<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}</div>'+
				'</tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				'</div></tpl></tpl>'
		},
		{
			xtype: 'combo',
			name: 'ic_pyme',
			id: 'cmbPyme',
			mode: 'local',
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Proveedor',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoPymeData,
			tpl: '<tpl for=".">' +
				'<tpl if="!Ext.isEmpty(loadMsg)">'+
				'<div class="loading-indicator">{loadMsg}</div>'+
				'</tpl>'+
				'<tpl if="Ext.isEmpty(loadMsg)">'+
				'<div class="x-combo-list-item">' +
				'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
				'</div></tpl></tpl>'
		},
		{
			xtype: 'combo',
			name: 'ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccione...',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_moneda',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto a operar',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'numberfield',
					name: 'fn_montoMin',
					allowBlank: true,
					maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 190,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 30
				},
				{
					xtype: 'numberfield',
					name: 'fn_montoMax',
					allowBlank: true,
					maxLength: 12,
					width: 190,
					msgTarget: 'side',
					margins: '0 20 0 0'	  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Cambio de Estatus del',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'dc_fecha_cambioMin',
					id: 'dc_fecha_cambioMin',
					allowBlank: true,
					startDay: 0,
					width: 190,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'dc_fecha_cambioMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 30
				},
				{
					xtype: 'datefield',
					name: 'dc_fecha_cambioMax',
					id: 'dc_fecha_cambioMax',
					allowBlank: true,
					startDay: 1,
					width: 190,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'dc_fecha_cambioMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'combo',
			name: 'ic_cambio_estatus',
			fieldLabel: 'Estatus',
			emptyText: 'Seleccione...',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_cambio_estatus',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEstatusData,
			tpl : NE.util.templateMensajeCargaCombo
		}
	];


	var gridTotales = {
		xtype: 'grid',
		store: resumenTotalesData,
		id: 'gridTotales',
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
			{
				header: '&nbsp;',
				dataIndex: 'MONEDA',
				align: 'left',
				width: 250
			},
			{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},
			{
				header: 'Monto Documentos',
				dataIndex: 'TOTAL_MONTO_DOCUMENTOS',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Descuento',
				dataIndex: 'TOTAL_MONTO_DESCUENTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Recibir',
				dataIndex: 'TOTAL_IMPORTE_RECIBIR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		width: 850,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};
	
    // create the Grid
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		bodyStyle: 'padding: 6px',
		style: 'margin:0 auto;',
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header: 'Proveedor/Cedente',
				tooltip: 'Proveedor',
				dataIndex: 'NOMBREPYME',
				sortable: true,
				hideable: false,
				width: 250,
				align: 'left'
			},
			{
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false,
				width : 150//,
				//css: 'color: #CC00FF;'
			},
			{
				header : 'Num. Documento',
				tooltip: 'Numero de Documento',
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 150,
				sortable : true
			},
			{
				header : 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex : 'DF_FECHA_DOCTO',
				sortable : true,
				width : 100,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC',
				sortable : true,
				width : 100,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Tasa',
				tooltip: 'Tasa',
				dataIndex : 'IN_TASA_ACEPTADA',
				sortable : true,
				width : 50,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header : 'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable : true,
				width : 50,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},
			{
				header : 'Monto de Documento',
				tooltip: 'Monto del Documento',
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 100,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'FN_PORC_ANTICIPO',
				sortable : true,
				width : 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header : 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex : '',
				sortable : true,
				width : 150,
				align: 'right',
				renderer: function (valor, metadata, registro) {
					return Ext.util.Format.number(registro.get('FN_MONTO') - registro.get('FN_MONTO_DSCTO'), '$0,0.00');
				}
			},
			{
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'FN_MONTO_DSCTO',
				sortable : true,
				width : 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true,
				width : 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Cambio de Estatus',
				tooltip: 'Cambio de Estatus',
				dataIndex : 'CAMBIOESTATUS',
				sortable : true,
				width : 200
			},
			{
				header : 'Fecha del Cambio de Estatus',
				tooltip: 'Fecha del Cambio de Estatus',
				dataIndex : 'DC_FECHA_CAMBIO',
				sortable : true,
				width : 150,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Causa',
				tooltip: 'Causa',
				dataIndex : 'CT_CAMBIO_MOTIVO',
				sortable : true,
				width : 200,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
				}
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				hideable: false,
				width: 210,
				align: 'left'
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		//maxHeight: 400,
		width: 850,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						resumenTotalesData.load();
						var totalesCmp = Ext.getCmp('gridTotales');
						if (!totalesCmp.isVisible()) {
							totalesCmp.show();
							totalesCmp.el.dom.scrollIntoView();
						}
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta02exta.jsp',
							params: fp.getForm().getValues(),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},/*
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},*/
				'-',
				{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						//var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '13consulta02ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Imprime_PDF'/*,
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize*/
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},/*
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},*/
				'-'
			]
		}
	});

/*	var pnlTotales = {
		xtype: 'panel',
		layout: 'fit',
		id: 'pnlTotales',
		title: 'Totales',
		hidden: true,
		items: [
			gridTotales
		],
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		]
	}
*/

//-------------------------------- PRINCIPAL -----------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Cambio de Estatus - Criterios de B�squeda.',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyles: 'padding: 6px',
		style: 'margin:0 auto',	//Para centrar el objeto
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					
					var fechaCambioMin = Ext.getCmp("dc_fecha_cambioMin");
					var fechaCambioMax = Ext.getCmp("dc_fecha_cambioMax");
					if (!Ext.isEmpty(fechaCambioMin.getValue()) && Ext.isEmpty(fechaCambioMax.getValue()) ) {
						fechaCambioMax.markInvalid('Por favor, especifique la fecha de cambio final');
						return;
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');
					var totalesCmp = Ext.getCmp('gridTotales');
					if (totalesCmp.isVisible()) {
						totalesCmp.hide();
					}
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar', //Generar datos para la consulta
							start: 0,
							limit: 15
						})
					});
					Ext.getCmp('btnGenerarArchivo').disable();
					Ext.getCmp('btnTotales').disable();
					//Ext.getCmp('btnBajarArchivo').hide();
					//Ext.getCmp('btnBajarPDF').hide();
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					fp.getForm().reset();
					grid.hide();
				}
				
			}
		]
	});

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid,
			gridTotales
		]
	});

	
//-------------------------------- ----------------- -----------------------------------
	
	catalogoEPOData.load();
	catalogoMonedaData.load();
	catalogoEstatusData.load();
});