<%@ page contentType="application/json;charset=UTF-8" import="
com.netro.anticipos.*,netropology.utilerias.*,
java.util.*,
org.apache.commons.logging.Log,
com.netro.exception.*,
com.netro.model.catalogos.*,
netropology.utilerias.usuarios.*,   
net.sf.json.JSONObject,
com.netro.descuento.ConsBloqueoProveedor,
org.apache.commons.beanutils.BeanUtils,
net.sf.json.JSONArray,
com.netro.afiliacion.*"
errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion      = (request.getParameter("informacion")       == null)?"": request.getParameter("informacion");
String nafinElectronico = (request.getParameter("nafin_electronico") == null)?"": request.getParameter("nafin_electronico");
String rfc              = (request.getParameter("rfc")               == null)?"": request.getParameter("rfc");
String icEpo            = (request.getParameter("ic_epo")            == null)?"": request.getParameter("ic_epo");
String icIf             = (request.getParameter("ic_if")             == null)?"": request.getParameter("ic_if");
String icPyme           = (request.getParameter("ic_pyme")           == null)?"": request.getParameter("ic_pyme");
String datosGrid        = (request.getParameter("datos_grid")        == null)?"": request.getParameter("datos_grid");
String consulta         = "";
String mensaje          = "";
String infoRegresar     = "";
String nombreArchivo    = "";
JSONObject resultado    = new JSONObject();
boolean success         = true;

log.info("informacion: <<<<<"      + informacion      + ">>>>>");
/*
log.info("icIf: <<<<<"             + icIf             + ">>>>>");
log.info("icEpo: <<<<<"            + icEpo            + ">>>>>");
log.info("icPyme: <<<<<"           + icPyme           + ">>>>>");
log.info("nafinElectronico: <<<<<" + nafinElectronico + ">>>>>");
*/
if(informacion.equals("CATALOGO_EPO")){

	CatalogoIFEpoProveedores cat = new CatalogoIFEpoProveedores();
	cat.setCampoClave("IC_EPO");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setUsuario(iNoCliente);
	infoRegresar = cat.getJSONElementos();

} else if(informacion.equals("CONSULTA_DATA")){

	ConsBloqueoProveedor paginador = new ConsBloqueoProveedor();
	paginador.setIcEpo(icEpo);
	paginador.setNafinElectronico(nafinElectronico);
	paginador.setRfc(rfc);
	paginador.setIcIf(iNoCliente);
	paginador.setIcPyme(icPyme);
	paginador.setInformacion(informacion);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros listaRegistros = new Registros();

	try{
		listaRegistros = queryHelper.doSearch();
		consulta = listaRegistros.getJSONData();
		resultado.put("total", "" + listaRegistros.getNumeroRegistros());
		resultado.put("registros", consulta);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error en la paginacion", e);
	}
	
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("CONSULTA_HISTORICO")){

	ConsBloqueoProveedor paginador = new ConsBloqueoProveedor();
	paginador.setIcEpo(icEpo);
	paginador.setNafinElectronico(nafinElectronico);
	paginador.setRfc(rfc);
	paginador.setIcIf(icIf);
	paginador.setIcPyme(icPyme);
	paginador.setNafinElectronico(nafinElectronico);
	paginador.setInformacion(informacion);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	Registros listaRegistros = new Registros();

	try{
		listaRegistros = queryHelper.doSearch();
		consulta = listaRegistros.getJSONData();
		resultado.put("total", "" + listaRegistros.getNumeroRegistros());
		resultado.put("registros", consulta);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error en la paginacion", e);
	}
	
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("DESCARGA_PDF_CONSULTA")){

	ConsBloqueoProveedor paginador = new ConsBloqueoProveedor();
	paginador.setIcEpo(icEpo);
	paginador.setNafinElectronico(nafinElectronico);
	paginador.setRfc(rfc);
	paginador.setIcIf(iNoCliente);
	paginador.setIcPyme(icPyme);
	paginador.setInformacion("CONSULTA_DATA");
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try{
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error al generar el archivo PDF", e);
	}

	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("DESCARGA_PDF_HISTORICO")){

	ConsBloqueoProveedor paginador = new ConsBloqueoProveedor();
	paginador.setIcEpo(icEpo);
	paginador.setNafinElectronico(nafinElectronico);
	paginador.setRfc(rfc);
	paginador.setIcIf(icIf);
	paginador.setIcPyme(icPyme);
	paginador.setNafinElectronico(nafinElectronico);
	paginador.setInformacion("CONSULTA_HISTORICO");
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try{
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Exception e) {
		success = false;
		throw new AppException("Error al generar el archivo PDF", e);
	}
	
	resultado.put("success", new Boolean(success));
	infoRegresar = resultado.toString();

} else if(informacion.equals("CONFIRMA_BLOQUEO")){

	JSONArray arrayGrid = JSONArray.fromObject(datosGrid);
	HashMap   mapaGrid  = new HashMap();
	List      listaGrid = new ArrayList();

	try{

	for(int i=0; i<arrayGrid.size(); i++){

		JSONObject auxiliar = arrayGrid.getJSONObject(i);
		mapaGrid   = new HashMap();

		if((""+ auxiliar.getString("BLOQUEO_ACT")).equals("true")){ // Solo paso los registros que fueron seleccionados

			System.out.println("IC_IF: "       + auxiliar.getString("IC_IF")      );
			System.out.println("IC_EPO: "      + auxiliar.getString("IC_EPO")     );
			System.out.println("IC_PYME: "     + auxiliar.getString("IC_PYME")    );
			System.out.println("BLOQUEO_ANT: " + auxiliar.getString("BLOQUEO_ANT"));

			System.out.println();
			mapaGrid.put("IC_IF",         auxiliar.getString("IC_IF")        );
			mapaGrid.put("IC_EPO",        auxiliar.getString("IC_EPO")       );
			mapaGrid.put("IC_PYME",       auxiliar.getString("IC_PYME")      );
			mapaGrid.put("NE",            auxiliar.getString("NE")           );
			mapaGrid.put("BLOQUEO_ANT",   auxiliar.getString("BLOQUEO_ANT")  );
			mapaGrid.put("CAUSA_BLOQUEO", auxiliar.getString("CAUSA_BLOQUEO"));
			if(("" + auxiliar.getString("BLOQUEO_ANT")).equals("S")){
				mapaGrid.put("BLOQUEO_ACT",   "N"                             );
			} else{
				mapaGrid.put("BLOQUEO_ACT",   "S"                             );
			}
			listaGrid.add(mapaGrid);
		}

	}

		if(!listaGrid.isEmpty()){
			ConsBloqueoProveedor paginador = new ConsBloqueoProveedor();	
			paginador.setListaGrid(listaGrid);
			paginador.setUsuario(iNoUsuario);
			paginador.setNafinElectronico("" + iNoNafinElectronico);
			mensaje = paginador.cambiaEstatusBloqueo();
		} else{
			mensaje = "No se actualizó ningún registro";
		}

	} catch(Throwable e) {
		success = false;
		//mensaje = "Error al actualizar los registros" + e;
		throw new AppException(e);
	}

	resultado.put("success", new Boolean(success));
	resultado.put("mensaje", mensaje);
	infoRegresar = resultado.toString();

}

%>

<%=infoRegresar%>
