	Ext.onReady(function() {
  var ic_if = Ext.getDom('ic_if').value;
  var objGral = {
    ic_if:ic_if,
    strPerfil: Ext.getDom('strPerfil').value
  };
  
	var parametroGlobal = {
      parametro      :  "",
      paramNumDocto  :  "",
      selecBF        :  "N",
      moneda         :  "",
      cliente        :  "",
      prestamo       :  "",
      FechaCorte     :  "",
      sirac          :  "",
      financiera     :  ""
   };
   


function validaPerfil(perfil){
    if(perfil =='LINEA CREDITO' || perfil =='IF LI' || perfil =='IF 4CP' || perfil =='IF 5CP' ||
       perfil =='IF 4MIC' || perfil =='IF 5MIC' ){
      return 'LINEA CREDITO';
    }else{
      return 'DESCUENTO';
    }
  }

function ocultaMuestraColumnas(accion){
  var cmGConsulta = Ext.getCmp('grid').getColumnModel();
  cmGConsulta.setHidden(cmGConsulta.findColumnIndex('FG_TOTDESCFOP'), accion);
  cmGConsulta.setHidden(cmGConsulta.findColumnIndex('FG_TOTDESCFINAPE'), accion);

  var cmGConsultaTotal = Ext.getCmp('gridTotales').getColumnModel();
  cmGConsultaTotal.setHidden(cmGConsultaTotal.findColumnIndex('TOTA_4'), accion);
  cmGConsultaTotal.setHidden(cmGConsultaTotal.findColumnIndex('TOTA_5'), accion);

}
  
	/****************************************************************************
	*										  PROCESAR	 											 *			
	****************************************************************************/
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoTxt');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');         
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
            var archivo =  Ext.util.JSON.decode(response.responseText).urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma = Ext.getCmp('forma');            
            forma.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
            forma.getForm().getEl().dom.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarArchivoB =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoB');
		btnGenerarArchivo.setIconClass('icoTxt');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivoB');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
            var archivo =  Ext.util.JSON.decode(response.responseText).urlArchivo;				
            archivo = archivo.replace('/nafin','');
            var params = {nombreArchivo: archivo};				
            var forma = Ext.getCmp('forma');            
            forma.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
            forma.getForm().getEl().dom.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	// Procesar Imprimir PDF
	var procesarSuccessFailureImprimirPDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaDataTotales = function(store, arrRegistros, opts) {
		var gridTotales = Ext.getCmp('gridTotales');
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnImprimirPDF = Ext.getCmp('btnImprimirNotaC');	
			var el = gridTotales.getGridEl();
			
         try{
            gridTotales.getColumnModel().setHidden(0, false);
            gridTotales.getColumnModel().setHidden(1, false);
            gridTotales.getColumnModel().setHidden(2, false);
            gridTotales.getColumnModel().setHidden(3, false);
            gridTotales.getColumnModel().setHidden(4, false);
            gridTotales.getColumnModel().setHidden(5, false);
            gridTotales.getColumnModel().setHidden(6, false);
            gridTotales.getColumnModel().setHidden(7, false);
            gridTotales.getColumnModel().setHidden(8, false);
            
            if(parametroGlobal.parametro=='S')
               gridTotales.getColumnModel().setHidden(9, false);
            else
               gridTotales.getColumnModel().setHidden(9, true);
               
            gridTotales.getColumnModel().setHidden(10, false);
            
            if(parametroGlobal.paramNumDocto=='S')
               gridTotales.getColumnModel().setHidden(11, false);
            else
               gridTotales.getColumnModel().setHidden(11, true);
               
            
            /*if(Ext.getCmp('tipoLinea').getValue().value=='C'){
                ocultaMuestraColumnas(true);
              }else{
                ocultaMuestraColumnas(false);
              }*/
              ocultaMuestraColumnas(true);
         
            if(store.getTotalCount() > 0) {
               el.unmask();
            } else {
               el.mask('No se encontr� ning�n registro', 'x-mask');
            }
         
         }catch(e){
            Ext.Msg.show({
               title :  'Error',
               msg   :  e.description,
               buttons: Ext.Msg.OK,
               icon  :  Ext.Msg.ERROR
            });
         }
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnImprimirPDF = Ext.getCmp('btnImprimirNotaC');
			var gridTotales = Ext.getCmp('gridTotales');
	
			var el = grid.getGridEl();
			
         try{
            if(parametroGlobal.parametro=='N'){
                  grid.getColumnModel().setHidden(0, false);
                  grid.getColumnModel().setHidden(2, false);
                  grid.getColumnModel().setHidden(3, true);
                  grid.getColumnModel().setHidden(4, false);
                  grid.getColumnModel().setHidden(5, true);
                  grid.getColumnModel().setHidden(6, false);
                  grid.getColumnModel().setHidden(7, false);
                  grid.getColumnModel().setHidden(8, false);
                  grid.getColumnModel().setHidden(9, false);
                  grid.getColumnModel().setHidden(10, false);
                  grid.getColumnModel().setHidden(11, false);
                  grid.getColumnModel().setHidden(12, false);
                  grid.getColumnModel().setHidden(13, false);
                  grid.getColumnModel().setHidden(14, true);
                  grid.getColumnModel().setHidden(15, false);
                  if(parametroGlobal.paramNumDocto=='S')
                     grid.getColumnModel().setHidden(17, false);
                  else
                     grid.getColumnModel().setHidden(17, true);
               }else{
                  grid.getColumnModel().setHidden(0, false);
                  grid.getColumnModel().setHidden(2, false);
                  grid.getColumnModel().setHidden(3, false);
                  grid.getColumnModel().setHidden(4, false);
                  grid.getColumnModel().setHidden(5, false);
                  grid.getColumnModel().setHidden(6, false);
                  grid.getColumnModel().setHidden(7, false);
                  grid.getColumnModel().setHidden(8, false);
                  grid.getColumnModel().setHidden(9, false);
                  grid.getColumnModel().setHidden(10, false);
                  grid.getColumnModel().setHidden(11, false);
                  grid.getColumnModel().setHidden(12, false);
                  grid.getColumnModel().setHidden(13, false);
                  grid.getColumnModel().setHidden(14, false);
                  grid.getColumnModel().setHidden(15, false);
                  grid.getColumnModel().setHidden(15, false);
                  if(parametroGlobal.paramNumDocto=='S')
                     grid.getColumnModel().setHidden(17, false);
                  else
                     grid.getColumnModel().setHidden(17, true);
               }
               
              /*if(Ext.getCmp('tipoLinea').getValue().value=='C'){
                ocultaMuestraColumnas(true);
              }else{
                ocultaMuestraColumnas(false);
              }*/
              ocultaMuestraColumnas(true);
            
            if(store.getTotalCount() > 0) {
               gridTotales.show();	
               
               consultaDataGridTotales.load({                  
                     params: Ext.apply(paramSubmit,{
                        ic_ifs: objGral.ic_if,
                        informacion:   'Consultar',
                        operacion:     'Totales',
                        start       :  opts.params.start,
                        limit       :  opts.params.limit
                     })
               });
               
               
               Ext.getCmp('btnGenerarArchivo').enable();
               Ext.getCmp('btnBajarArchivo').hide();
               Ext.getCmp('btnImprimirPDF').enable();
               el.unmask();
            } else {
               btnGenerarArchivo.disable();
					Ext.getCmp('btnImprimirPDF').disable();/////
					gridTotales.hide();///
               el.mask('No se encontr� ning�n registro', 'x-mask');
            }
         }catch(e){
            Ext.Msg.show({
               title :  'Error',
               msg   :  e.description,
               buttons: Ext.Msg.OK,
               icon  :  Ext.Msg.ERROR
            });
         }
		}
	}
	
	var procesarConsultaDataBancomext = function(store, arrRegistros, opts) {
		//var gridBancomext = Ext.getCmp('gridBancomext');
		if (arrRegistros != null) {
			if (!gridBancomext.isVisible()) {
				gridBancomext.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivoB');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivoB');
			var btnBajarPDF = Ext.getCmp('btnBajarPDFB');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDFB'); //btnImprimirPDFB
			var btnImprimirPDF = Ext.getCmp('btnImprimirNotaCB');
			var gridTotalesBancomex = Ext.getCmp('gridTotalesBancomex');
			var el = gridBancomext.getGridEl();
			
			//parametroGlobal
			//var jsonData = store.reader.jsonData;
			try{
            
            if(parametroGlobal.parametro=='N'){
               gridBancomext.getColumnModel().setHidden(0, false);
               gridBancomext.getColumnModel().setHidden(1, false);
               gridBancomext.getColumnModel().setHidden(2, false);
               gridBancomext.getColumnModel().setHidden(3, false);
               gridBancomext.getColumnModel().setHidden(4, false);
               gridBancomext.getColumnModel().setHidden(5, false);
               gridBancomext.getColumnModel().setHidden(6, false);
               gridBancomext.getColumnModel().setHidden(7, false);
               gridBancomext.getColumnModel().setHidden(8, true);
               gridBancomext.getColumnModel().setHidden(9, true);
               gridBancomext.getColumnModel().setHidden(10, true);
               gridBancomext.getColumnModel().setHidden(11, true);
               gridBancomext.getColumnModel().setHidden(12, true);
               gridBancomext.getColumnModel().setHidden(13, true);
               gridBancomext.getColumnModel().setHidden(14, true);
               gridBancomext.getColumnModel().setHidden(15, true);
            }else{
               gridBancomext.getColumnModel().setHidden(0, false);
               gridBancomext.getColumnModel().setHidden(1, false);
               gridBancomext.getColumnModel().setHidden(2, false);
               gridBancomext.getColumnModel().setHidden(3, false);
               gridBancomext.getColumnModel().setHidden(4, false);
               gridBancomext.getColumnModel().setHidden(5, false);
               gridBancomext.getColumnModel().setHidden(6, false);
               gridBancomext.getColumnModel().setHidden(7, false);
               gridBancomext.getColumnModel().setHidden(8, false);
               gridBancomext.getColumnModel().setHidden(9, false);
               gridBancomext.getColumnModel().setHidden(10, false);
               gridBancomext.getColumnModel().setHidden(11, false);
               gridBancomext.getColumnModel().setHidden(12, false);
               gridBancomext.getColumnModel().setHidden(13, false);
               gridBancomext.getColumnModel().setHidden(14, false);
               gridBancomext.getColumnModel().setHidden(15, false);
            }
            
            if(store.getTotalCount() > 0) {
               gridTotalesBancomex.show();
               
               consultaDataGridTotalesBancomex.load({
                  params: Ext.apply(paramSubmit,{
                        ic_ifs: objGral.ic_if,
                        informacion: 'Consultar',
                        operacion:'Totales',
                        start       :  opts.params.start,
                        limit       :  opts.params.limit
                     })
               });
               
               Ext.getCmp('btnGenerarArchivoB').enable();
               Ext.getCmp('btnBajarArchivoB').hide();
               Ext.getCmp('btnImprimirPDFB').enable();
               el.unmask();
            } else {
               btnGenerarArchivo.disable();
					Ext.getCmp('btnImprimirPDFB').disable();
					gridTotalesBancomex.hide();
               el.mask('No se encontr� ning�n registro', 'x-mask');
            }
         
         }catch(e){
            Ext.Msg.show({
               title :  'Error',
               msg   :  e.description,
               buttons: Ext.Msg.OK,
               icon  :  Ext.Msg.ERROR
            });
         }
		}
	}
	var procesarGenerarZip =  function(opts, success, response) {
		var btnArchivoPDF = Ext.getCmp('btnGenerarZip');
		btnArchivoPDF.setIconClass('icoZip');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarZip');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				//var forma = Ext.getDom('formAux');
				//forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				//forma.submit();
        var archivo =  Ext.util.JSON.decode(response.responseText).urlArchivo;				
        archivo = archivo.replace('/nafin','');
        var params = {nombreArchivo: archivo};				
        var forma = Ext.getCmp('forma');            
        forma.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
        forma.getForm().getEl().dom.submit();
        
			});
		} else {
			btnArchivoPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarObtieneParametro =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
      var resp =  Ext.util.JSON.decode(response.responseText);
			parametroGlobal.parametro     = Ext.util.JSON.decode(response.responseText).parametro;
			parametroGlobal.paramNumDocto = Ext.util.JSON.decode(response.responseText).paramNumDocto;
      parametroGlobal.financiera = resp.IC_FINANCIERA;
	  parametroGlobal.sirac = resp.NUMERO_SIRAC;
      
      if(validaPerfil(objGral.strPerfil)!='LINEA CREDITO'){
        if(resp.NUMERO_SIRAC=='' || resp.IC_FINANCIERA==''){
          Ext.getCmp("tipoLinea").hide();
          if(resp.IC_FINANCIERA==''){
            Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
            Ext.getCmp("cliente").setValue(resp.NUMERO_SIRAC);
            objGral.ic_if = 12;
          }
        }else{
          Ext.getCmp("tipoLinea").show();
        }
      }else{
        Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
        Ext.getCmp("cliente").setValue(parametroGlobal.sirac);
        objGral.ic_if = 12;
      }
      
      muestraForma();
      
		} 
	}
  
  var muestraForma = function(){
  
    var FechaCorte = Ext.getCmp('id_FechaCorte');
    var moneda		= Ext.getCmp('id_moneda');
    var prestamo	= Ext.getCmp('prestamo');
    var btnGenerarZip= Ext.getCmp('btnGenerarZip');
    var btnBajarZip= Ext.getCmp('btnBajarZip');
    var cliente 	= Ext.getCmp('cliente');
    var btnLimpiar	= Ext.getCmp('btnLimpiar');
    var btnConsultar= Ext.getCmp('btnConsultar');
    var btnConsultarB= Ext.getCmp('btnConsultarB');
    var cmpForma = Ext.getCmp('forma');
    var grid = Ext.getCmp('grid');
    var gridTotales = Ext.getCmp('gridTotales');
    var gridTotalesBancomex = Ext.getCmp('gridTotalesBancomex');
    var gridBancomext = Ext.getCmp('gridBancomext');
    
    FechaCorte.show();
    moneda.show();
    cliente.show();
    prestamo.show();
    btnGenerarZip.show();
    btnBajarZip.hide();
    btnLimpiar.show();
    btnConsultar.show();
    btnConsultarB.hide();	
    grid.hide();	
    gridTotales.hide();
    gridTotalesBancomex.hide();
    gridBancomext.hide();
    
    paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
    catalogoFechaCorte.load({
      params: Ext.apply(paramSubmit,{
        ic_ifs: objGral.ic_if,
        informacion: 'CatFechaCorte',
        selecBF: parametroGlobal.selecBF 
      })
    });		

  }
	/****************************************************************************
	*									CATALOGOS COMBOS											 *			
	****************************************************************************/
	var catalogoBancoFondeo = new Ext.data.SimpleStore({
		fields:	['clave','descripcion'],
		data:		[['N','NAFIN'],['B','BANCOMEXT']]
	});
	
	var catalogoMoneda = new Ext.data.JsonStore({
		xtype:	'jsonstore',
		root:		'registros',
		fields:	['clave','descripcion'],
		url:		'13consulta7ext.data.jsp',
		listeners:	{
			//load:	true,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams:	{
			informacion: 'CatMoneda'
		},
		totalProperty:	'total',
		autoLoad:	true
	});
	
	var catalogoFechaCorte = new Ext.data.JsonStore({
		xtype:	'jsonstore',
		root:		'registros',
		fields:	['clave','descripcion'],
		url:		'13consulta7ext.data.jsp',
		listeners:	{
			//load:	true,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams:	{
			informacion: 'CatFechaCorte'
		},
		totalProperty:	'total',
		autoLoad:	false
	});
	/****************************************************************************
	*									JSON STORE's OF GRID's									 *			
	****************************************************************************/	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta7ext.data.jsp',
		baseParams: {
			informacion: 'Consultar',
         operacion:  'Generar',
         selecBF:    'N',
         start :  0,
         limit :  15
		},
		fields: [
			{name: 'IG_CLIENTE'},
         {name: 'FG_SDOINSNVO'},
			{name: 'CG_NOMBRECLIENTE'},
			{name: 'TO_CHAR(COM_FECHAPROBABLEPAGO,DD/MM/YYYY)'},
			{name: 'IG_PRESTAMO'},
			{name: 'IG_NUMELECTRONICO'},
			{name: 'CG_ESQUEMATASAS'},
			{name: 'FG_MARGEN'},
			{name: 'FG_SDOINSOLUTO'},
			{name: 'FG_AMORTIZACION'},
			{name: 'FG_INTERES'},
			{name: 'FG_TOTDESCFOP'},
			{name: 'FG_TOTDESCFINAPE'},
			{name: 'FG_TOTALVENCIMIENTO'},
			{name: 'FG_TOTALEXIGIBLE'},
			{name: 'FG_ADEUDOTOTAL'},
			{name: 'FG_SDOINSNVO'},
			{name: 'NUMERO_DOCUMENTO'},
      {name: 'CD_NOMBRE'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
         beforeLoad:	{
            fn: function(store, options){               
               Ext.apply(options.params, {
                  selecBF:	   parametroGlobal.selecBF,
                  moneda:     parametroGlobal.moneda,
                  cliente:    parametroGlobal.cliente,
                  prestamo:   parametroGlobal.prestamo,
                  FechaCorte: parametroGlobal.FechaCorte
               });
            }		
         },
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var consultaDataGridBancomext = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta7ext.data.jsp',
		baseParams: {
			informacion: 'Consultar',
         operacion:  'Generar',
         selecBF:    'B'
		},
		fields: [
			{name: 'COM_FECHAPROBABLEPAGO'},
			{name: 'IC_MONEDA'},
			{name: 'CD_NOMBRE'},
			{name: 'IC_IF'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'IG_PRESTAMO'},
			{name: 'IG_CAPITAL_INSOLUTO'},
			{name: 'DF_APERTURA'},
			{name: 'IG_PLAZO'},
			{name: 'FG_TASA_BASE'},
			{name: 'FG_SOBRE_TASA'},
			{name: 'FG_TASA_TOTAL'},
			{name: 'FG_INTERES_CALC'},
			{name: 'FG_TOTAL'},
			{name: 'CG_ESTATUS_DOC'},
			{name: 'CG_NUM_FACTURA'},
			{name: 'CG_BENEFICIARIO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataBancomext,
         beforeLoad:	{
            fn: function(store, options){               
               Ext.apply(options.params, {
                  selecBF:	   parametroGlobal.selecBF,
                  moneda:     parametroGlobal.moneda,
                  cliente:    parametroGlobal.cliente,
                  prestamo:   parametroGlobal.prestamo,
                  FechaCorte: parametroGlobal.FechaCorte
               });
            }		
         },
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var consultaDataGridTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta7ext.data.jsp',
		baseParams: {
			informacion: 'Consulta',
			operacion: 'Totales'
		},
		fields: [
			{name: 'MONEDA'},
			{name: 'N_REGISTROS'},
			{name: 'TOTA_1'},
			{name: 'TOTA_2'},
			{name: 'TOTA_3'},
			{name: 'TOTA_4'},
			{name: 'TOTA_5'},
			{name: 'TOTA_6'},
			{name: 'TOTA_7'},
			{name: 'TOTA_8'},
			{name: 'TOTA_9'},
			{name: 'TOTA_10'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataTotales,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var consultaDataGridTotalesBancomex = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta7ext.data.jsp',
		baseParams: {
			informacion: 'Consulta',
			operacion: 'Totales'
		},
		fields: [
			{name: 'MONEDA'},
			{name: 'N_REGISTROS'},
			{name: 'TOTA_1'},
			{name: 'TOTA_2'},
			{name: 'TOTA_3'},
			{name: 'TOTA_4'},
			{name: 'TOTA_5'},
			{name: 'TOTA_6'},
			{name: 'TOTA_7'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			//load: procesarConsultaDataTotalesBancomext,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	/****************************************************************************
	*									     GRID'S													 *			
	****************************************************************************/
	var gridTotales = {
		store: consultaDataGridTotales,
		xtype:'grid',
		id: 'gridTotales',
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 180,
		width: 925,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		hidden: true,
		title:'Totales',
		columns: [{
			header: '',
			dataIndex: 'MONEDA',
			sortable: true,
			width: 165, resizable: true,
			align: 'right'
		},{
			header: 'No. de Registros',
			dataIndex: 'N_REGISTROS',
			sortable: true,align:'center',
			width: 130, resizable: true
		},{
			header: 'Saldo Insoluto',
			dataIndex: 'TOTA_1',
			sortable: true, 
			align: 'right',
			width: 130, resizable: true,renderer:'usMoney'
		},{
			header: 'Amortizaci�n',
			dataIndex: 'TOTA_2',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Intereses',
			dataIndex: 'TOTA_3',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Total desc. FOPYME',
			dataIndex: 'TOTA_4',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Total desc. FINAPE',
			dataIndex: 'TOTA_5',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Total vencimiento',
			dataIndex: 'TOTA_6',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Total exigible',
			dataIndex: 'TOTA_7',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Adeudo Total',
			dataIndex: 'TOTA_8',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Saldo insoluto nuevo',
			dataIndex: 'TOTA_9',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Num. Documento',
			dataIndex: 'TOTA_10',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		}]
	};

	var gridTotalesBancomex = new Ext.grid.GridPanel({ 
		store: consultaDataGridTotalesBancomex,
		id: 'gridTotalesBancomex',
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 180,
		width: 925,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		hidden: true,
		title:'Totales',
		columns: [{
			header: '', tooltip: 'Total Parcial',
			dataIndex: 'MONEDA',
			sortable: true,
			width: 165, resizable: true,
			align: 'right'
		},{
			header: 'Saldo Insoluto', tooltip: 'Saldo Insoluto',
			dataIndex: 'N_REGISTROS',
			sortable: true,
			width: 130, resizable: true,
			align: 'right'
		},{
			header: 'Capital Insoluto',
			dataIndex: 'TOTA_1',
			sortable: true,
			align: 'right',
			width: 130, resizable: true,renderer:'usMoney'
		},{
			header: 'Tasa Base', 
			dataIndex: 'TOTA_2',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Sobre Tasa',
			dataIndex: 'TOTA_3',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Tasa Total',
			dataIndex: 'TOTA_4',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Inter�s Calculado',
			dataIndex: 'TOTA_5',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Total',
			dataIndex: 'TOTA_6',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		}]
	});
   
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [{
				header: 'Cliente', tooltip: 'Cliente',
				dataIndex: 'IG_CLIENTE',
				sortable: true,
				width: 65, resizable: true,
				align: 'left'
			},{
				header: 'Nombre del Cliente', tooltip: 'Nombre del Cliente',
				dataIndex: 'CG_NOMBRECLIENTE',
				sortable: true,	align: 'center',
				width: 170, resizable: true,
				align: 'left'
			},{
				header: 'Pr�ximo Pago', tooltip: 'Pr�ximo Pago',
				dataIndex: 'TO_CHAR(COM_FECHAPROBABLEPAGO,DD/MM/YYYY)',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center'
			},{
				header: 'Pr�stamo', tooltip: 'Pr�stamo',
				dataIndex: 'IG_PRESTAMO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'left'
			},{
				header: 'No. Elec.', tooltip: 'No. Elec.',
				dataIndex: 'IG_NUMELECTRONICO',
				sortable: true,	align: 'center',
				width: 60, resizable: true,
				align: 'center'
			},{
				header: 'Esquema de tasas', tooltip: 'Esquema de tasas',
				dataIndex: 'CG_ESQUEMATASAS',
				sortable: true,	align: 'center',
				width: 120, resizable: true,
				align: 'center'
      },{
				header: '<center>Moneda</center>', tooltip: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				sortable: true,	align: 'center',
				width: 170, resizable: true,
				align: 'left'
			},{
				header: 'Margen', tooltip: 'Margen',
				dataIndex: 'FG_MARGEN',
				sortable: true,	align: 'center',
				width: 60, resizable: true,
				align: 'center'
			},{
				header: 'Saldo insoluto', tooltip: 'Saldo insoluto',
				dataIndex: 'FG_SDOINSOLUTO',
				sortable: true,	align: 'center',
				width: 100, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Amortizaci�n', tooltip: 'Amortizaci�n',
				dataIndex: 'FG_AMORTIZACION',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
				renderer : 'usMoney'
			},{
				header: 'Intereses', tooltip: 'Intereses',
				dataIndex: 'FG_INTERES',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Total desc. FOPYME', tooltip: 'Total desc. FOPYME',
				dataIndex: 'FG_TOTDESCFOP',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Total desc. FINAPE', tooltip: 'Total desc. FINAPE',
				dataIndex: 'FG_TOTDESCFINAPE',
				sortable: true,	align: 'center',
				width: 120, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Total vencimiento', tooltip: 'Total vencimiento',
				dataIndex: 'FG_TOTALVENCIMIENTO',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Total exigible', tooltip: 'Total exigible',
				dataIndex: 'FG_TOTALEXIGIBLE',
				sortable: true,	align: 'center',
				width: 105, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Adeudo Total', tooltip: 'Adeudo Total',
				dataIndex: 'FG_ADEUDOTOTAL',
				sortable: true,	align: 'center',
				width: 105, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Saldo insoluto nuevo', tooltip: 'Saldo insoluto nuevo',
				dataIndex: 'FG_SDOINSNVO',
				sortable: true,	align: 'center',
				width: 120, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Num. Documento', tooltip: 'Num. Documento',
				dataIndex: 'NUMERO_DOCUMENTO',
				sortable: true,	align: 'center',
				width: 105, resizable: true,
				align: 'right'
			}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 926,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
         xtype: 'paging',
			id: 'barraPaginacion',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype    : 'button',
					text     : 'Generar Archivo',
					iconCls  : 'icoTxt',
					id:      'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta7ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'TXT',
								informacion: 'Consultar',
                        selecBF:	   parametroGlobal.selecBF,
                        moneda:     parametroGlobal.moneda,
                        cliente:    parametroGlobal.cliente,
                        prestamo:   parametroGlobal.prestamo,
                        FechaCorte: parametroGlobal.FechaCorte,
                        ic_ifs: objGral.ic_if,
                        ic_if :  Ext.getDom('ic_if').value=="12"?"12":objGral.ic_if
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				{
					xtype: 'button',
					text: 'Imprimir',
					id: 'btnImprimirPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						
						Ext.Ajax.request({
							url: '13consulta7ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								ic_ifs: objGral.ic_if,
								parametro: parametroGlobal.parametro,
								paramNumDocto: parametroGlobal.paramNumDocto,
								operacion:'PDF',
								informacion: 'Consultar',
								start: cmpBarraPaginacion.cursor,//0,
								limit: cmpBarraPaginacion.pageSize//15
							}),
							callback: procesarSuccessFailureImprimirPDF
						});
					}
				}
			]
		}
	};
	
	// GRID 2 
	var gridBancomext = new Ext.grid.GridPanel({ 
		store: consultaDataGridBancomext,
		id: 'gridBancomext',
		hidden: true,
		columns: [{
				header: 'Cliente',
				dataIndex: 'IC_IF',
				sortable: true,hidden: true,hidden: true,
				width: 65, resizable: true,
				align: 'left'
			},{
				header: 'Raz�n Social',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Beneficiario',
				dataIndex: 'CG_BENEFICIARIO',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Fecha de Apertura',
				dataIndex: 'DF_APERTURA',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Fecha de Vencimiento',
				dataIndex: 'COM_FECHAPROBABLEPAGO',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Numero Factura',
				dataIndex: 'CG_NUM_FACTURA',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Pr�stamo',
				dataIndex: 'IG_PRESTAMO',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Estatus',
				dataIndex: 'CG_ESTATUS_DOC',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Plazo',
				dataIndex: 'IG_PLAZO',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Capital Insoluto',
				dataIndex: 'IG_CAPITAL_INSOLUTO',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'right',
            renderer :  Ext.util.Format.usMoney
			},{
				header: 'Tasa Base',
				dataIndex: 'FG_TASA_BASE',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Sobre Tasa',
				dataIndex: 'FG_SOBRE_TASA',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Tasa Total',
				dataIndex: 'FG_TASA_TOTAL',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'left'
			},{
				header: 'Interes Calculado',
				dataIndex: 'FG_INTERES_CALC',
				sortable: true,hidden: true,
				width: 120, resizable: true,
				align: 'right',
            renderer : Ext.util.Format.usMoney
			},{
				header: 'Total',
				dataIndex: 'FG_TOTAL',
				sortable: true,hidden: true,
				width: 110, resizable: true,
				align: 'right',
            renderer : Ext.util.Format.usMoney
			}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 926,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
		xtype: 'paging',
			id: 'barraPaginacion2',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDataGridBancomext,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoTxt',
					id: 'btnGenerarArchivoB',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta7ext.data.jsp',
							params: Ext.apply(paramSubmit,{
                ic_ifs: objGral.ic_if,
								operacion:'TXT',
								informacion: 'Consultar'
							}),
							callback: procesarSuccessFailureGenerarArchivoB
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivoB',
					hidden: true
				},
				{
					xtype: 'button',
					text: 'Imprimir',
					id: 'btnImprimirPDFB',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta7ext.data.jsp',
							params: Ext.apply(paramSubmit,{
                ic_ifs: objGral.ic_if,
								operacion:'PDF',
								informacion: 'Consultar',
								start: 0,
								limit: 15
							}),
							callback: procesarSuccessFailureImprimirPDF
						});
					}
				}
			]
		}
	});
	//FIN GRID 2
	/****************************************************************************
	*									ELEMENTOS FORMA											 *			
	****************************************************************************/	
	var elementosForma = [
  
     {
      xtype: 'radiogroup',
      id:'tipoLinea',
      name: 'tipoLinea',
      fieldLabel: 'Tipo de Linea',
      //hidden: true,
      items: [
         {boxLabel: 'LINEA CREDITO', name: 'tipoLinea', id:'tipoLineaC', inputValue: 'C', value:'C' },
         {boxLabel: 'LINEA DESCUENTO', name: 'tipoLinea', id:'tipoLineaD', inputValue: 'D', checked:true, value:'D' }
      ],
      listeners:{
        change: function(rgroup, radio){
          if(radio.value=='C'){
            Ext.getCmp('cliente').hide();
            Ext.getCmp("cliente").setValue(parametroGlobal.sirac);
            objGral.ic_if = 12;
          }else{
            Ext.getCmp('cliente').show();
            Ext.getCmp('cliente').setValue("");
            objGral.ic_if = Ext.getDom('ic_if').value;
          }
		  Ext.getCmp('btnGenerarZip').enable();
		  Ext.getCmp('btnBajarZip').hide();
		  Ext.getCmp('id_FechaCorte').setValue('');
		  catalogoFechaCorte.load({
              params: Ext.apply(paramSubmit,{
                tipoLinea: radio.value,
				cliente: Ext.getCmp("cliente").getValue(),
                informacion: 'CatFechaCorte',
                selecBF: parametroGlobal.selecBF 
              })
            });
        }
      }
    },
    /*
    {
		xtype:			'combo',
		id:				'id_selecBF',
		name:				'selecBF',
		hiddenName:		'selecBF',
		fieldLabel:		'Banco Fondeo',
		emptyText:		'- Seleccione -',
		store:			catalogoBancoFondeo,
		mode:				'local',
		displayField:	'descripcion',
		valueField:		'clave',
		triggerAction:	'all',
		typeAhead:		true,
		minchars:		1,
		anchor:			'80%',
		forceSelection : true,editable:false,
    hidden:true,
		listeners:		{
			select: function(combo, record, index){
				var FechaCorte = Ext.getCmp('id_FechaCorte');
				var moneda		= Ext.getCmp('id_moneda');
				var prestamo	= Ext.getCmp('prestamo');
				var btnGenerarZip= Ext.getCmp('btnGenerarZip');
				var btnBajarZip= Ext.getCmp('btnBajarZip');
				var cliente 	= Ext.getCmp('cliente');
				var btnLimpiar	= Ext.getCmp('btnLimpiar');
				var btnConsultar= Ext.getCmp('btnConsultar');
				var btnConsultarB= Ext.getCmp('btnConsultarB');
				var cmpForma = Ext.getCmp('forma');
				var grid = Ext.getCmp('grid');
				var gridTotales = Ext.getCmp('gridTotales');
				var gridTotalesBancomex = Ext.getCmp('gridTotalesBancomex');
				var gridBancomext = Ext.getCmp('gridBancomext');
				
				if(combo.getValue()=='B'){
					FechaCorte.show();
					moneda.show();
					cliente.hide();
					prestamo.show();
					btnGenerarZip.show();
					btnBajarZip.hide();
					btnLimpiar.show();
					btnConsultar.hide();
					btnConsultarB.show();
					grid.hide();
					gridTotales.hide();
					gridBancomext.hide();
               gridTotalesBancomex.hide();
					
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					catalogoFechaCorte.load({
						params: Ext.apply(paramSubmit,{
							informacion: 'CatFechaCorte',
							selecBF: combo.getValue()
						})
					});
				}else if(combo.getValue()=='N'){
					FechaCorte.show();
					moneda.show();
					cliente.show();
					prestamo.show();
					btnGenerarZip.show();
					btnBajarZip.hide();
					btnLimpiar.show();
					btnConsultar.show();
					btnConsultarB.hide();	
					grid.hide();	
					gridTotales.hide();
               gridTotalesBancomex.hide();
					gridBancomext.hide();
					
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					catalogoFechaCorte.load({
						params: Ext.apply(paramSubmit,{
							informacion: 'CatFechaCorte',
							selecBF: combo.getValue()
						})
					});		
				}
				
			},
         beforeselect   :  function() {
            Ext.getCmp('forma').getForm().reset();
            if(parametroGlobal.sirac=='' ||  parametroGlobal.financiera==''){
              Ext.getCmp("tipoLinea").hide();
              if(parametroGlobal.financiera==''){
                Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
                Ext.getCmp("cliente").setValue(parametroGlobal.sirac);
                //objGral.ic_if = 12;
              }
            }
         }
		}
	}*/
  {
		xtype:			'combo',
		hidden:			true,
		id:				'id_moneda',
		name:				'moneda',
		hiddenName:		'moneda',
		fieldLabel:		'Moneda',
		emptyText:		'- Seleccione -',
		store:			catalogoMoneda,
		mode:				'local',
		displayField:	'descripcion',
		valueField:		'clave',
		triggerAction:	'all',
		typeAhead:		true,
		minchars:		1,
		anchor:			'80%',
		forceSelection : true,editable:false
	},{
		xtype:			'numberfield',
		hidden:			true,
		id:				'cliente',
		name:				'cliente',
		hiddenName:		'cliente',
		fieldLabel:		'Cliente',
		enforceMaxLength:true,
		maxLength: 		12,
		maxLengthText:	'S�lo acepta m�ximo hasta 12 car�cteres',
		anchor:			'80%'
	},{
		xtype:			'numberfield',
		hidden:			true,
		id:				'prestamo',
		name:				'prestamo',
		hiddenName:		'prestamo',
		fieldLabel:		'Pr�stamo',
		enforceMaxLength:true,
		maxLength: 		12,
		maxLengthText:	'S�lo acepta m�ximo hasta 12 car�cteres',
		anchor:			'80%'
	},{
		xtype:			'combo',
		id:				'id_FechaCorte',
		name:				'FechaCorte',
		hiddenName:		'FechaCorte',
		hidden:			true,
		fieldLabel:		'Fecha de Corte',
		emptyText:		'- Seleccione -',
		store:			catalogoFechaCorte,
		mode:				'local',
		displayField:	'descripcion',
		valueField:		'clave',
		triggerAction:	'all',
		typeAhead:		true,
		minchars:		1,
		anchor:			'80%',
		forceSelection : true,editable:false,
		listeners:{
			select: function(combo, record, index){
				var btnGenerarZip= Ext.getCmp('btnGenerarZip');
				var btnBajarZip= Ext.getCmp('btnBajarZip');
				btnGenerarZip.enable();
				btnBajarZip.hide();
			}
		}
	}];
	
	var fp = {
		xtype:			'form',
		id:				'forma',
		style: 			'text-align:left;margin:0 auto;',
		collapsible:	true,
		frame:			true,
		width:			400,
		labelWidth:		100,
		titleCollapse:	false,
		title:			'Vencimientos',
		bodyStyle:		'padding: 6px',
		defaults:		{
			msgTarget:	'side',
			anchor:		'-20'
		},
		items:			[elementosForma],
		buttons:[{
			text:		'Bajar Archivo ZIP',
			id:		'btnGenerarZip',
			iconCls:	'icoZip',
			hidden:	true,
			handler: function(boton, evento){
				var FechaCorte = Ext.getCmp('id_FechaCorte');
				var moneda		= Ext.getCmp('id_moneda');
				var prestamo	= Ext.getCmp('prestamo');
				
				if(moneda.getValue()!='' || prestamo.getValue()!=''){
					Ext.Msg.show({
						title:	'Alerta',
						msg:		'Solo es necesario el criterio de Fecha de Corte para esta consulta',
						buttons:	Ext.Msg.OK,
						icon:		Ext.Msg.ERROR
					});
					moneda.reset();
					prestamo.setValue('');
					return false
				}else if(FechaCorte.getValue()==''){
					Ext.Msg.show({
						title:	'Alerta',
						msg:		'Debe proporcionar la fecha de corte',
						buttons:	Ext.Msg.OK,
						icon:		Ext.Msg.ERROR
					});
					return false
				}
				boton.disable();
				boton.setIconClass('loading-indicator');
        var cmpForma = Ext.getCmp('forma');
				var paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
				Ext.Ajax.request({
					url: '13consulta7ext.data.jsp',
					params: Ext.apply(paramSubmit,{
						informacion:'DescargaZIP'
					}),
					callback: procesarGenerarZip
				});
			} // fin handler
		},{
			text:		'Bajar ZIP',
			id:		'btnBajarZip',
			iconCls:	'icoZip',
			hidden:	true
		},{
			text:		'Consultar',
			id: 		'btnConsultar',
			hidden:	true,
			iconCls: 'icoBuscar',
			handler: function(boton, evento) {
				var cmpForma            = Ext.getCmp('forma');
        var cmpBarraPaginacion  = Ext.getCmp('barraPaginacion');
        
        parametroGlobal.FechaCorte = Ext.getCmp('id_FechaCorte').getValue();
        //parametroGlobal.selecBF    = Ext.getCmp('id_selecBF').getValue();
        parametroGlobal.moneda     = Ext.getCmp('id_moneda').getValue();
        parametroGlobal.prestamo   = Ext.getCmp('prestamo').getValue();
        parametroGlobal.cliente    = Ext.getCmp('cliente').getValue();
        
		if(validaPerfil(objGral.strPerfil)=='LINEA CREDITO'){
			var fechaCorte=Ext.getCmp('id_FechaCorte');
			if(fechaCorte.getValue()==''){
				Ext.Msg.show({
					title:'Mensaje',
					msg:'Debe proporcionar la fecha de corte',
					icon: Ext.Msg.ERROR,
					buttons:Ext.Msg.OK
				});
				return false;
			}
		}
            
				paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
				consultaDataGrid.load({
					params: Ext.apply(paramSubmit,{
            ic_ifs: objGral.ic_if,
						informacion :  'Consultar',
						operacion   :  'Generar',
                  start       :  0,
                  limit       :  15
					})
				})
			} // fin handler
		},{
			text:		'Consultar',
			id: 		'btnConsultarB',
			hidden:	true,
			iconCls: 'icoBuscar',
			handler: function(boton, evento) {
				var cmpForma = Ext.getCmp('forma');
            
            parametroGlobal.FechaCorte = Ext.getCmp('id_FechaCorte').getValue();
            //parametroGlobal.selecBF    = Ext.getCmp('id_selecBF').getValue();
            parametroGlobal.moneda     = Ext.getCmp('id_moneda').getValue();
            parametroGlobal.prestamo   = Ext.getCmp('prestamo').getValue();
            parametroGlobal.cliente    = Ext.getCmp('cliente').getValue();
            
				paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
				consultaDataGridBancomext.load({
					params: Ext.apply(paramSubmit,{
            ic_ifs: objGral.ic_if,
						informacion: 'Consultar',
						operacion:'Generar',
                  start       :  0,
                  limit       :  15
					})
				});
			} // fin handler
		},{
			text: 'Limpiar',
			id:	'btnLimpiar',
			iconCls: 'icoLimpiar',
			hidden:	true,
			handler: function() {
				window.location = '13consulta7ext.jsp';
			}
		}]
	};

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,gridBancomext,
			NE.util.getEspaciador(10),
			gridTotales,
         gridTotalesBancomex,
			NE.util.getEspaciador(10)
		]
	});
	
	//parametroGlobal
	Ext.Ajax.request({
		url: '13consulta7ext.data.jsp',
		params: {
			informacion:'ObtieneParametro'
		},
		callback: procesarObtieneParametro
	});
	
	Ext.getCmp("tipoLinea").hide();
  
});
