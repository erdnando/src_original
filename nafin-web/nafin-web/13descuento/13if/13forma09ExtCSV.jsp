<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.descuento.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String sFechaVencIni  = (request.getParameter("sFechaVencIni")==null)?"":request.getParameter("sFechaVencIni");
String sFechaVencFin  = (request.getParameter("sFechaVencFin")==null)?"":request.getParameter("sFechaVencFin");
String sFechaProbIni  = (request.getParameter("sFechaProbIni")==null)?"":request.getParameter("sFechaProbIni");
String sFechaProbFin  = (request.getParameter("sFechaProbFin")==null)?"":request.getParameter("sFechaProbFin");
String sTipoBanco = (session.getAttribute("strTipoBanco")==null)?"":session.getAttribute("strTipoBanco").toString().trim();
String icMoneda = (request.getParameter("icMoneda")==null)?"":request.getParameter("icMoneda");
int registros=0;
AccesoDB con = new AccesoDB();
CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = "";
StringBuffer contenidoArchivo = new StringBuffer();
try{
	con.conexionDB();


		String sCondicion="", sOrden = "", sFechaDel="", sFechaAl="", sNombre1="";
		if(!sFechaVencIni.equals("") && !sFechaVencFin.equals("")){//BUSQUEDA POR FECHA DE VENCIMIENTO
			sCondicion = " AND V.df_periodofin >= to_date(?,'dd/mm/yyyy') "   +
								" AND V.df_periodofin < to_date(?,'dd/mm/yyyy')+1 ";
			sOrden=" V.df_periodofin ";
			sFechaDel=sFechaVencIni;
			sFechaAl=sFechaVencFin;
			sNombre1="V";
		} else { //BUSQUEDA POR FECHA PROBABLE DE PAGO
			sCondicion = " AND V.com_fechaprobablepago >= to_date(?,'dd/mm/yyyy') "   +
								" AND V.com_fechaprobablepago < to_date(?,'dd/mm/yyyy')+1 ";
			sOrden=" V.com_fechaprobablepago ";
			sFechaDel=sFechaProbIni;
			sFechaAl=sFechaProbFin;
			sNombre1="P";
		}
		String query = " SELECT "	+
			    	           "			V.ic_if CLAVE_IF"   +
							   "			,V.ic_moneda CLAVE_MONEDA"	+
		      				   "			,M.cd_nombre MONEDA"	+
							   "			,TO_CHAR(V.df_periodofin,'DD/MM/YYYY') FECHA_VENCIMIENTO"   +
							   "			,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY') FECHA_PROB_PAGO"   +
			  				   "			,SUM(V.fg_totalexigible) IMPORTE_DEPOSITO"	+
			                   "			,F.ic_estado CLAVE_ESTADO"	+
			                   "			,E.cd_nombre NOMBRE_ESTADO"   +
							   "			,V.df_periodofin"   +
							   "			,V.com_fechaprobablepago"   +
							   "	FROM com_vencimiento V"   +
							   "			,comcat_moneda M"   +
						 	   "			,comcat_if I"	+
		                	   "			,comcat_financiera F"	+
			                   "			,comcat_estado E"	+
							   "  WHERE V.ic_moneda = M.ic_moneda"	+
			  				   "      AND V.ic_if = I.ic_if"		+
		               	       "      AND I.ic_financiera = F.ic_financiera"	+
			                   "      AND F.ic_estado = E.ic_estado"	+
									 "     AND V.ic_moneda = ?"   +
							   "      AND V.ic_if = ?"   +
							   sCondicion+
							   " GROUP BY V.ic_if"   +
							   "			,V.ic_moneda"	+
							   "			,M.cd_nombre"   +
							   "			,TO_CHAR(V.df_periodofin,'DD/MM/YYYY')"   +
							   "			,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY')"	+
							   "			,F.ic_estado"	+
							   "			,E.cd_nombre"	+
							   "			,V.df_periodofin"   +
							   "			,V.com_fechaprobablepago"   +
							   " ORDER BY "	+
							   sOrden;
		//out.println("query:"+query+"<br>");
		PreparedStatement ps = con.queryPrecompilado(query);
		ps.setString(1,icMoneda);
		ps.setString(2, iNoCliente);
		ps.setString(3, sFechaDel);
		ps.setString(4, sFechaAl);
		ResultSet rs = ps.executeQuery();
		String sClaveIF = "", sClaveMoneda = "", sMoneda = "", sFechaVencimiento = "", sFechaProbPago = "", sImporteDeposito = "", sClaveEstado = "", sEstado = "", sRefInter = "", sClaveRefInter = "";
		String sQueryInterno = "";
		double dImporteDepositoTotal=0;
		while (rs.next()) {
			sClaveIF = (rs.getString("CLAVE_IF") == null) ? "" : rs.getString("CLAVE_IF").trim();
        	sClaveMoneda = (rs.getString("CLAVE_MONEDA") == null) ? "" : rs.getString("CLAVE_MONEDA").trim();
        	sMoneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA").trim();
        	sFechaVencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO").trim();
        	sFechaProbPago = (rs.getString("FECHA_PROB_PAGO") == null) ? "" : rs.getString("FECHA_PROB_PAGO").trim();
			sImporteDeposito = (rs.getString("IMPORTE_DEPOSITO") == null) ? "" : rs.getString("IMPORTE_DEPOSITO").trim();
        	sClaveEstado = (rs.getString("CLAVE_ESTADO") == null) ? "" : rs.getString("CLAVE_ESTADO").trim();
        	sEstado = (rs.getString("NOMBRE_ESTADO") == null) ? "" : rs.getString("NOMBRE_ESTADO").trim();
			
			/*archivo .csv */
			if(sTipoBanco.equals("NB"))
				contenidoArchivo.append("E,"+sClaveIF+","+sClaveEstado+","+sClaveMoneda+",,'"+sFechaVencimiento+",'"+sFechaProbPago+",'"+sFechaProbPago+","+sImporteDeposito+"\n");
			else if(sTipoBanco.equals("B"))
				contenidoArchivo.append("E,"+sClaveIF+","+sClaveEstado+","+sClaveMoneda+",,'"+sFechaProbPago+",'"+sFechaProbPago+","+sImporteDeposito+"\n");
			
			//SUBQUERY
			if(sTipoBanco.equals("NB")) {	// Si es IF no Bancario.
				sQueryInterno=" SELECT  "	+
					                   "			V.ig_subaplicaCion SUBAPLICACION"   +
							   		   "			,V.ig_prestamo PRESTAMO"   +
									   "			,V.ig_cliente CLAVE_SIRAC"   +
									   "			,V.fg_amortizacion CAPITAL"   +
							           "			,V.fg_interes INTERESES"   +
							           "			,V.fg_subsidio SUBSIDIO"   +
							           "			,V.fg_comision COMISION"   +
							           "			,V.fg_totalexigible IMPORTE_PAGO"   +
							           "	FROM COM_VENCIMIENTO V"   +
							           "  WHERE V.ic_if = ?"   +
							           "      AND V.ic_moneda = ?"   +
							           "      AND V.df_periodofin >= to_date(?,'dd/mm/yyyy')"   +
							           "      AND V.com_fechaprobablepago < to_date(?,'dd/mm/yyyy')+1"  ;
			} else if(sTipoBanco.equals("B")) {			// IF Bancario.
				sQueryInterno= " SELECT  "	+
										"			V.ig_sucursal SUCURSAL, "	+
										"			V.ig_subaplicaCion SUBAPLICACION, "	+
										"			V.ig_prestamo PRESTAMO, "	+
										"			V.cg_nombrecliente ACREDITADO, "	+
										"			TO_CHAR(V.df_periodoinic,'DD/MM/YYYY') FECHA_OPERACION, "	+
										"			TO_CHAR(V.df_periodofin,'DD/MM/YYYY') FECHA_VENCIMIENTO, "	+
										"			TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY') FECHA_PAGO, "	+
										"			V.ig_tasabase TASA, "	+
										"			V.fg_amortizacion CAPITAL, "	+
										"			V.ig_dias DIAS, "	+
										"			V.fg_interes INTERESES, "	+
										"			V.fg_comision COMISION, "	+
										"			V.fg_totalexigible IMPORTE_PAGO "	+
										"   FROM COM_VENCIMIENTO V"   +
										" WHERE V.ic_if = ?"   +
										"     AND V.ic_moneda = ?"   +
										"     AND V.df_periodofin >= to_date(?,'dd/mm/yyyy')"   +
										"     AND V.com_fechaprobablepago < to_date(?,'dd/mm/yyyy')+1"  ;
			}
			//out.println("sQueryInterno:"+sQueryInterno+"<br>");
			PreparedStatement ps2 = con.queryPrecompilado(sQueryInterno);
			ps2.setString(1, sClaveIF);
			ps2.setString(2, sClaveMoneda);
			ps2.setString(3, sFechaVencimiento);
			ps2.setString(4, sFechaProbPago);
			ResultSet rs2 = ps2.executeQuery();
			int iSubRegistros=0;
			String sSubaplicacion="", sPrestamo="", sClaveSirac="", sCapital="", sInteres="", sSubsidio="", sComision="", sImportePago="";
			String sSucursal="", sAcreditado="",  sFechaOper="", sFechaVenc="", sFechaPago="", sTasa="", sDias="";
			while (rs2.next()) {
				sSubaplicacion = (rs2.getString("SUBAPLICACION") == null) ? "" : rs2.getString("SUBAPLICACION").trim();
				sPrestamo = (rs2.getString("PRESTAMO") == null) ? "" : rs2.getString("PRESTAMO").trim();
				sCapital = (rs2.getString("CAPITAL") == null) ? "" : rs2.getString("CAPITAL").trim();
				sInteres = (rs2.getString("INTERESES") == null) ? "" : rs2.getString("INTERESES").trim();
				sComision = (rs2.getString("COMISION") == null) ? "" : rs2.getString("COMISION").trim();
				sImportePago = (rs2.getString("IMPORTE_PAGO") == null) ? "" : rs2.getString("IMPORTE_PAGO").trim();
				if(sTipoBanco.equals("NB")) {
	        		sClaveSirac = (rs2.getString("CLAVE_SIRAC") == null) ? "" : rs2.getString("CLAVE_SIRAC").trim();
	        		sSubsidio = (rs2.getString("SUBSIDIO") == null) ? "" : rs2.getString("SUBSIDIO").trim();
				} else if(sTipoBanco.equals("B")) {
					sSucursal = (rs2.getString("SUCURSAL") == null) ? "" : rs2.getString("SUCURSAL").trim();
					sAcreditado = (rs2.getString("ACREDITADO") == null) ? "" : rs2.getString("ACREDITADO").trim();
					sFechaOper = (rs2.getString("FECHA_OPERACION") == null) ? "" : rs2.getString("FECHA_OPERACION").trim();
					sFechaVenc = (rs2.getString("FECHA_VENCIMIENTO") == null) ? "" : rs2.getString("FECHA_VENCIMIENTO").trim();
					sFechaPago = (rs2.getString("FECHA_PAGO") == null) ? "" : rs2.getString("FECHA_PAGO").trim();
					sTasa = (rs2.getString("TASA") == null) ? "" : rs2.getString("TASA").trim();
					sDias = (rs2.getString("DIAS") == null) ? "" : rs2.getString("DIAS").trim();
				}
				/*archivo .csv */
				if(sTipoBanco.equals("NB"))
					contenidoArchivo.append("D,"+sSubaplicacion+","+sPrestamo+","+sClaveSirac+","+sCapital+","+sInteres+",0,"+sSubsidio+","+sComision+",0,"+sImportePago+",VE,A\r\n");
				else if(sTipoBanco.equals("B"))
					contenidoArchivo.append("D,"+sSucursal+","+sSubaplicacion+","+sPrestamo+","+sAcreditado.replace(',',' ')+","+sFechaOper+","+sFechaVenc+","+sFechaPago+",0,"+sTasa+","+sCapital+","+sDias+","+sInteres+","+sComision+",0,"+sImportePago+"\r\n");
				
				iSubRegistros++;
			}//fin while 2
			rs2.close();
			if(ps2!=null) ps2.close();
			//FIN SUBQUERY
			dImporteDepositoTotal	+= Double.parseDouble(sImporteDeposito);
			registros++;
		}//while

    String sNombre2 = Comunes.rellenaCeros(String.valueOf(iNoCliente),5);
	DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT,new Locale("ES","MX"));
  	SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMyy");  
	
  	String sNombre3 = sdf1.format(df.parse(sFechaDel));
  	java.util.Date date2 = new java.util.Date();
  	SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMyyyy");
  	String sNombre4 = sdf2.format(date2);
	String sLike=sNombre1+sNombre2+sNombre3+sNombre4;
	String sQueryArchivo = "SELECT COUNT(1) CONSECUTIVO FROM com_encabezado_pago "+
										" WHERE ic_if = ? AND cg_nombre_archivo LIKE '%"+sLike+"%' ";
	//out.println("sQueryArchivo:"+sQueryArchivo+"<br>");
	ps = con.queryPrecompilado(sQueryArchivo);
	ps.setString(1, iNoCliente);
	rs = ps.executeQuery();
	String sConsecutivo="";
	while (rs.next()) {
		sConsecutivo  = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
	}//fin while 3
	rs.close();
	if(ps!=null) ps.close();
	
	String sNombre5= (Integer.parseInt(sConsecutivo)==0)?"-1":"-"+String.valueOf(Integer.parseInt(sConsecutivo)+1);
	String sNombreTemporal="";
	sNombreTemporal = sNombre1+sNombre2+sNombre3+sNombre4+sNombre5;
	JSONObject jsonObj 	      = new JSONObject();
	jsonObj.put("success", new Boolean(false));
	if(registros >= 1){
		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, sNombreTemporal,".csv"))//.txt o .csv
			out.print("<--!Error al generar el archivo-->");
		else {
			nombreArchivo = archivo.nombre;		
			//strDirecVirtualTemp+nombreArchivo
			
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo );
			jsonObj.put("success", new Boolean(true));
			
		} // else interno.
	} 
	
	infoRegresar=jsonObj.toString();

	// sin registros	%>
<%
	 // FIN ES CONSULTA
} catch(Exception e) { 
	out.println(e);
} finally { 
	if(con.hayConexionAbierta()) con.cierraConexionDB();
}
%>
<%=infoRegresar%>

