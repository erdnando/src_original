<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.util.Map.Entry,
		netropology.utilerias.*,
		com.netro.exception.*,
		javax.naming.*,				
		com.netro.descuento.*, 		
		com.netro.model.catalogos.CatalogoSimple,		
		net.sf.json.JSONArray,
		net.sf.json.JSONObject"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
	String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String operacion     =(request.getParameter("operacion")      != null) ?   request.getParameter("operacion")   :"";
	String infoRegresar  = "", consulta ="";
	
	String sFechaVencIni  = (request.getParameter("txtFchOper1")==null)?"":request.getParameter("txtFchOper1");
	String sFechaVencFin  = (request.getParameter("txtFchOper2")==null)?"":request.getParameter("txtFchOper2");
	String sFechaProbIni  = (request.getParameter("txtFchPro1")==null)?"":request.getParameter("txtFchPro1");
	String sFechaProbFin  = (request.getParameter("txtFchPro2")==null)?"":request.getParameter("txtFchPro2");
	String sFechaPagoIni  = (request.getParameter("txtFchPago1")==null)?"":request.getParameter("txtFchPago1");
	String sFechaPagoFin  = (request.getParameter("txtFchPago2")==null)?"":request.getParameter("txtFchPago2");
	String sTipoBanco		 = (session.getAttribute("strTipoBanco")==null)?"":session.getAttribute("strTipoBanco").toString().trim();
	
	int start=0; int limit=0;
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	
	
	if(informacion.equals("ConsultaGrid") || informacion.equals("ConsultarTotales") || informacion.equals("GenerarPDF") || informacion.equals("GenerarCSV")  )
	{
		ConsPagosEnviadosIF paginador = new ConsPagosEnviadosIF();
		paginador.setClaveIF(iNoCliente);
		paginador.setsTipoBanco(sTipoBanco);
		paginador.setsFechaVencIni(sFechaVencIni);
		paginador.setsFechaVencFin(sFechaVencFin);
		paginador.setsFechaProIni(sFechaProbIni);
		paginador.setsFechaProFin(sFechaProbFin);
		paginador.setsFechaPagoIni(sFechaPagoIni);
		paginador.setsFechaPagoFin(sFechaPagoFin);

		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		try
		{
			 start = Integer.parseInt(request.getParameter("start"));
			 limit = Integer.parseInt(request.getParameter("limit"));	
			
		} catch(Exception e)
		  { System.out.println("Error en parametros"); }
		  
		try 
		{
			if(informacion.equals("ConsultaGrid"))
			{
				if(operacion.equals("Generar"))
				{	//Nueva consulta, es decir la primera vez
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				//String  consultar = queryHelper.getJSONPageResultSet(request,start,limit);	
				//jsonObj = JSONObject.fromObject(consultar);
				//infoRegresar=jsonObj.toString();
				String temp ="";
				Registros reg = queryHelper.getPageResultSet(request,start,limit);	
				ArrayList lista_encabezados = new ArrayList();
				
			
				while(reg.next())
				{
					int band = 0;
					String ic_encabezado =  (reg.getString("CVE_ENCABEZADO_PAGO") == null ? "" : reg.getString("CVE_ENCABEZADO_PAGO"));
					
					for(int i=0; i<lista_encabezados.size(); i++)
					{
						temp = lista_encabezados.get(i).toString();
						if( temp.equals(ic_encabezado))
							band = 1 ;
					}
					if(band == 0)	//si no lo encontro repetido, lo agrega a la lista de encabezados
					{
						lista_encabezados.add(ic_encabezado);
					}
					else if (band == 1) //si esta repetido borra sus campos
					{
						reg.setObject("CVE_IF","");	
						reg.setObject("CVE_SUCURSAL","");	
						reg.setObject("CVE_MONEDA","");	
						reg.setObject("NOMBRE_BANCO","");			
						reg.setObject("FECHA_PERIODO_FIN","");	
						reg.setObject("FECHA_PROB_PAGO","");	
						reg.setObject("FECHA_DEPOSITO","");
						reg.setObject("IMPORTE_DEPOSITO","");
						reg.setObject("REFERENCIA_INTER","");			
						reg.setObject("REFERENCIA_BANCO","");	
					}
			
				}
				
				String consultar = 	"{\"success\": true, \"total\": \""+ queryHelper.getIdsSize() +"\", \"registros\": " + reg.getJSONData()+"}";
				jsonObj = JSONObject.fromObject(consultar);
				infoRegresar=jsonObj.toString();				
			}
			
			else  if (informacion.equals("ConsultarTotales"))
			{
			
				infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion
			}
			
			else if (informacion.equals("GenerarPDF"))
			{
				String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
			
			else if (informacion.equals("GenerarCSV"))
			{
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");			
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();	
			}
		} catch(Exception e) 
			{
				throw new AppException("Error en la recuperacion de datos!", e);
			}
		
		}
		
	
%>
<%=infoRegresar%>
