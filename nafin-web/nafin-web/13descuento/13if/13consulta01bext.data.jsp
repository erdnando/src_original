<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	com.netro.mandatodoc.*,	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String clavePyme = (request.getParameter("clavePyme") != null) ? request.getParameter("clavePyme") : "";
String claveDocumento = (request.getParameter("claveDocumento") != null) ? request.getParameter("claveDocumento") : "";
String infoRegresar ="", consulta ="", numeroDocto = "", producto ="",  nombreIF ="", fechaSeleccion = "", fechaOperacion ="", 	numMensualidad = "", numCredito = "", 
		 tipoContrato = "", numPrestamo = "",  	monedaClave ="", nombreArchivo = "";		 
int ordenAplicacion =0, numRegistrosMN  =0, numRegistrosDL=0, numRegistros  =0;
double dblTotalSaldoCreditoPesos = 0, dblTotalSaldoCreditoDolares = 0,  dblTotalAplicadoCreditoPesos = 0,  dblTotalAplicadoCreditoDolares = 0, 
       dblImporteRecibir = 0, dblSaldoCredito=0, dblImporteAplicadoCredito  =0, dblImporteDepositarPymePesos=0, dblImporteDepositarPymeDolares =0;

ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	
StringBuffer contenidoArchivo = new StringBuffer();			
CreaArchivo creaArchivo = new CreaArchivo();
ComunesPDF pdfDoc = new ComunesPDF();
HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

if (informacion.equals("ConsultarDetalle")  ||  informacion.equals("ConsultarTotalDetalle")  
	|| informacion.equals("ImpCsvDetalle") || informacion.equals("ImpPDFDetalle")   )   {

	if( informacion.equals("ImpPDFDetalle") )   {
	
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	
		pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
		pdfDoc.addText("Detalle de los créditos pagados con el documento  ","font4",ComunesPDF.LEFT);
		pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);	
	}


	try {
		Vector vDetalleDocumentoAplicado = BeanSeleccionDocumento.ovgetDetalleDocumentoAplicado(clavePyme, claveDocumento);

		for (int i = 0; i < vDetalleDocumentoAplicado.size(); i++) {
			numRegistros++;
			ordenAplicacion++;
			Vector vDatosDetalleDocumentoAplicado = (Vector) vDetalleDocumentoAplicado.get(i);

			numeroDocto = vDatosDetalleDocumentoAplicado.get(0).toString();
			dblImporteRecibir = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(1).toString());
			producto = vDatosDetalleDocumentoAplicado.get(2).toString();
			nombreIF = vDatosDetalleDocumentoAplicado.get(3).toString();
			fechaSeleccion = vDatosDetalleDocumentoAplicado.get(4).toString();
			fechaOperacion = vDatosDetalleDocumentoAplicado.get(5).toString();
			numMensualidad = vDatosDetalleDocumentoAplicado.get(6).toString();
			numCredito = vDatosDetalleDocumentoAplicado.get(7).toString();
			tipoContrato = vDatosDetalleDocumentoAplicado.get(8).toString();
			numPrestamo = vDatosDetalleDocumentoAplicado.get(9).toString();
			dblImporteAplicadoCredito = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(10).toString());
			dblSaldoCredito = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(11).toString());
			monedaClave = vDatosDetalleDocumentoAplicado.get(12).toString();
			
			if (informacion.equals("ConsultarDetalle"))  {
				datos = new HashMap();
				datos.put("PRODUCTO", producto);
				datos.put("NOMBRE_IF", nombreIF);
				datos.put("ORDER_APLICACION", Double.toString (ordenAplicacion));
				datos.put("FECHA_APLICACION", fechaSeleccion);
				datos.put("FECHA_OPERACION", fechaOperacion);
				datos.put("NUMERO_MENSUALIDAD", numMensualidad);
				datos.put("NUMERO_PEDIDO", numCredito);
				datos.put("TIPO_CONTRATO", tipoContrato);			
				datos.put("NUMERO_PRESTAMO", numPrestamo);
				datos.put("IMPORTE_APLICADO", Double.toString (dblImporteAplicadoCredito));
				datos.put("SALDO_CREDITO", Double.toString (dblSaldoCredito));
				registros.add(datos);	
			}
			
			if( informacion.equals("ImpPDFDetalle") )  {
			
				if(numRegistros == 1) { //Establece encabezados
					pdfDoc.setTable(11,100);				
					pdfDoc.setCell("Número de documento","forma",ComunesPDF.CENTER, 3);
					pdfDoc.setCell(numeroDocto,"forma",ComunesPDF.CENTER, 3);
					pdfDoc.setCell("Número de documento","forma",ComunesPDF.CENTER, 3);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblImporteRecibir,2),"formas",ComunesPDF.RIGHT,2);	
					
					pdfDoc.setCell("Producto","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Orden de aplicación","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de aplicación","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de operacion crédito","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Número de mensualidad","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Número de pedido/disposición","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo de contrato","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Número de prestamo NAFIN","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe aplicado a crédito","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Saldo del crédito","celda01",ComunesPDF.CENTER);				
				}
				
				pdfDoc.setCell(producto,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(nombreIF,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(Double.toString (ordenAplicacion),"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(fechaSeleccion,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(fechaOperacion,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(numMensualidad,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(numCredito,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(tipoContrato,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(numPrestamo,"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(Comunes.formatoDecimal(dblImporteAplicadoCredito,2),"forma",ComunesPDF.CENTER);	
				pdfDoc.setCell(Comunes.formatoDecimal(dblSaldoCredito,2),"forma",ComunesPDF.CENTER);	
								
			}
			
			if( informacion.equals("ImpCsvDetalle") )  {
				if(numRegistros == 1) { //Establece encabezados
					contenidoArchivo.append("Número de documento,Monto aplicado\n");
					contenidoArchivo.append(numeroDocto + "," + dblImporteRecibir + "\n\n\n");
					contenidoArchivo.append("Producto,IF,Orden de aplicación,Fecha de aplicación," +
													"Fecha de operacion crédito,Número de mensualidad," +
													"Número de pedido/disposición,Tipo de contrato,Número de prestamo NAFIN," +
													"Importe aplicado a crédito,Saldo del crédito\n");						
				}
			
			contenidoArchivo.append(producto + "," + 
								nombreIF.replace(',',' ') + "," + 
								ordenAplicacion + "," + 
								fechaSeleccion + ","+
								fechaOperacion + "," + 
								numMensualidad + "," + 
								numCredito + "," + 
								tipoContrato + "," + 
								numPrestamo + "," + 
								Comunes.formatoDecimal(dblImporteAplicadoCredito,2,false) + "," + 
								Comunes.formatoDecimal(dblSaldoCredito,2,false) + "," + 
								"\n");				
			}		
			
			if ("1".equals(monedaClave)){	//moneda nacional
				numRegistrosMN++;			
				dblTotalAplicadoCreditoPesos += dblImporteAplicadoCredito;
				dblTotalSaldoCreditoPesos += dblSaldoCredito;
				
			} else if("54".equals(monedaClave) ) {	//dolar
				numRegistrosDL++;
				dblTotalAplicadoCreditoDolares += dblImporteAplicadoCredito;
				dblTotalSaldoCreditoDolares += dblSaldoCredito;
			}	
			
		}//FOR 
		
		dblImporteDepositarPymePesos = dblImporteRecibir - dblTotalAplicadoCreditoPesos;
		dblImporteDepositarPymeDolares = dblImporteRecibir - dblTotalAplicadoCreditoDolares;
		if (dblImporteDepositarPymePesos <= 0) {
			dblImporteDepositarPymePesos = 0;
		}
			
		if (informacion.equals("ConsultarDetalle"))  {
			consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("clavePyme", clavePyme);	
			jsonObj.put("claveDocumento", claveDocumento);	
			jsonObj.put("monto", "$"+Comunes.formatoDecimal(dblImporteRecibir, 2));
			
		}
			
		if( informacion.equals("ImpCsvDetalle") )  {
			if (!creaArchivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
				out.print("<--!Error al generar el archivo-->");
			else{
				nombreArchivo = creaArchivo.nombre;
			}
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		
		if( informacion.equals("ImpPDFDetalle") )  {			
			
			if(numRegistrosMN>0){
				pdfDoc.setCell("Total Moneda Nacional","celda01",ComunesPDF.LEFT,3);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalAplicadoCreditoPesos, 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalSaldoCreditoPesos, 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("Importe a depositar de la PYME","celda01",ComunesPDF.LEFT,3);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblImporteDepositarPymePesos, 2),"formas",ComunesPDF.LEFT,8);	
			}
			if(numRegistrosDL>0){
				pdfDoc.setCell("Total Dólares","celda01",ComunesPDF.LEFT,3);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalAplicadoCreditoDolares, 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalSaldoCreditoDolares, 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("Importe a depositar de la PYME","celda01",ComunesPDF.LEFT,3);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( dblImporteDepositarPymeDolares, 2),"formas",ComunesPDF.LEFT,8);
			}
			pdfDoc.addTable();					
			pdfDoc.endDocument();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		
		if( informacion.equals("ConsultarTotalDetalle") )  {
		
			for(int i=0; i<2; i++){
			
				datos = new HashMap();
				if(i==0 && numRegistrosMN>0  ){					
					datos.put("MONEDA", "Total Moneda Nacional");
					datos.put("IMPORTE_PYME", Double.toString (dblTotalAplicadoCreditoPesos)  );
					datos.put("IMPORTE_APLICADO", Double.toString (dblTotalSaldoCreditoPesos));
					datos.put("SALDO_CREDITO", Double.toString (dblImporteDepositarPymePesos));						
				}			
				if(i==1&& numRegistrosDL>0  ){				
					datos.put("MONEDA", "Total Dólares");
					datos.put("IMPORTE_PYME", Double.toString (dblTotalAplicadoCreditoDolares)  );
					datos.put("IMPORTE_APLICADO", Double.toString (dblTotalSaldoCreditoDolares));
					datos.put("SALDO_CREDITO", Double.toString ( dblImporteDepositarPymeDolares));						
				}
		
			registros.add(datos);
			}		
			consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
		}
		
		
		infoRegresar=jsonObj.toString();	
	} catch (NafinException ne) {
		ne.printStackTrace();
	}
	
}

%>
<%=infoRegresar%>

