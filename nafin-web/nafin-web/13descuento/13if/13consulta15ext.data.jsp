<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String infoRegresar = "";
	//ConsCalculoRebateIF paginador = new ConsCalculoRebateIF();
	//paginador.setClaveEpo();
	if(informacion.equals("CatalogoEPOPuntosRebate")){
		 CatalogoEPOPuntosRebate catalogo = new CatalogoEPOPuntosRebate();
		 catalogo.setCampoClave("ic_epo");
		 catalogo.setCampoDescripcion("cg_razon_social");
		 catalogo.setClaveIf(iNoCliente);
		 infoRegresar = catalogo.getJSONElementos();
	}
	else if(informacion.equals("CatalogoMoneda")){
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("1,54", Integer.class);
		infoRegresar = catalogo.getJSONElementos();
	}else if(informacion.equals("Consulta")||informacion.equals("ArchivoPaginaPDF")||informacion.equals("ArchivoPDF")||informacion.equals("ArchivoCSV")){
		int start			= 0;
		int limit			= 0;
		String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
		String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
		String df_fecha_operacion_de = (request.getParameter("df_fecha_operacion_de"))!=null?request.getParameter("df_fecha_operacion_de"):"";
		String df_fecha_operacion_a = (request.getParameter("df_fecha_operacion_a"))!=null?request.getParameter("df_fecha_operacion_a"):"";
		String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de"))!=null?request.getParameter("df_fecha_venc_de"):"";
		String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a"))!=null?request.getParameter("df_fecha_venc_a"):"";
	
		ConsCalculoRebateIF paginador = new ConsCalculoRebateIF();
		paginador.setClaveEpo(ic_epo);
		paginador.setClaveIf(iNoCliente);
		paginador.setMoneda(ic_moneda);
		paginador.setFechaOperacionMin(df_fecha_operacion_de);
		paginador.setFechaOperacionMax(df_fecha_operacion_a);
		paginador.setFechaVencMin(df_fecha_venc_de);
		paginador.setFechaVencMax(df_fecha_venc_a);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if(informacion.equals("Consulta")){
			String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			}catch(Exception e){
				throw new AppException("Error en los parámetros obtenidos.",e);
			}
			try{
				if(operacion.equals("Generar")){
					queryHelper.executePKQuery(request);
				}
				infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
			}catch(Exception e){
				throw new AppException("Error en la paginación",e);
			}
		}
		else if(informacion.equals("ArchivoPaginaPDF")){
			try{
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start, limit,strDirectorioTemp, "PDF");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
		else if(informacion.equals("ArchivoPDF")){
			JSONObject jsonObj = new JSONObject();
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			}catch(Throwable e){
				jsonObj.put("success",new Boolean(false));
				throw new AppException("Error al generar el archivo PDF");
			}
			infoRegresar = jsonObj.toString();
		}else if(informacion.equals("ArchivoCSV")){
			JSONObject jsonObj = new JSONObject();
			try{
			String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"CSV");
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			}catch(Throwable e){
				jsonObj.put("success",new Boolean(false));
				throw new AppException("Error al generar el archivo PDF");
			}
			infoRegresar = jsonObj.toString();
		}
	}else if(informacion.equals("ResumenTotales")){
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesión
		infoRegresar = queryHelper.getJSONResultCount(request);
	}
%>
<%=infoRegresar%>
