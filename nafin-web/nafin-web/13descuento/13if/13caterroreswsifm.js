// Muestra la Tabla del Layout
		

		function doEnviar(){
			var f = document.forma;
			
			var Archivo = f.txtarchivo.value;
			var ext 		= "txt";
			var tipo 	= 6;
			if(f.txtarchivo.value==""){
				alert("Favor de capturar una ruta valida");
				return;
			}
			if(!formatoValido(Archivo, tipo)) {
				alert('El formato del archivo de origen no es el correcto. Debe tener extensi�n '+ext);
				f.txtarchivo.focus();
				f.txtarchivo.select();
				return;
			}
			
			f.submit();
		}
    
// CONSTRUIR FORMA PARA CONSERVAR EL ESTILO DE LOS BOTONES

Ext.onReady(function(){
	var lstNumerosLineaOk;
  var icProceso;
  var nombreIF
  Ext.QuickTips.init();
  
  var fnBtnContinuar = function(btn){
    Ext.Msg.confirm('Confirmaci�n', 'Al confirmar la carga se sustituira la informaci�n previamente cargada. �Desea continuar?', function(btn){
      if(btn=='yes'){
        Ext.Ajax.request({
          url: '13caterroreswsifm.data.jsp',
          params: {
            informacion: 'RealizarCargaMasiva',
            lstNumerosLineaOk: lstNumerosLineaOk,
            icProceso: icProceso
          },
          callback: procesarSuccessSave
        });
        
      }else{
        Ext.getCmp('btnTransmitir').enable();
      }
    });
  }
  
  var procesarSuccessSave =  function(opts, success, response) {

		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
      var resp = 	Ext.util.JSON.decode(response.responseText);
      var registrosCargados = [];
      
      fpCarga.hide();
      gridLayout.hide();
      panelResultValid.hide();
      gridResumen.show();
      gridRegCargados.show();
      
      storeRegCorrectos.each(function(record){
        registrosCargados.push(record);
      })
      
      storeRegCargados.add(registrosCargados);
      
      var data = [
                ["No. Total de Registros Cargados", Number(Ext.getCmp('totalRC').getValue())+Number(Ext.getCmp('totalRE').getValue())],
                ["No. Total de Registros Cargados Correctamente", Ext.getCmp('totalRC').getValue()],
                ["No. Total de Registros con Error", Ext.getCmp('totalRE').getValue()],
                ["Usuario", resp.strNombreUsuario]
                ];
      
      storeResumen.loadData(data);
      
      //alert(resp.insercionExitosa);
      
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
  
  var procesarSuccessDescarga = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			//if(resp.existenInconsistencias){
				var archivo = resp.urlArchivo;				
        archivo = archivo.replace('/nafin','');
        var params = {nombreArchivo: archivo};				
        gridForm.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
        gridForm.getForm().getEl().dom.submit();
      //}else{
				//Ext.Msg.alert('Aviso','No existen inconsistencias');
			//}
			

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
  //---------------------------------------------------------------------------
      
  var elementosFormCarga = [
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){
						var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');

						if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}

					}
				},{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo1',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									width: 290,
									emptyText: 'Ruta del Archivo',
									fieldLabel: 'Ruta del Archivo',
									name: 'archivoCesion',
									buttonText: '',
									buttonCfg: {
									  iconCls: 'upload-icon'
									},
									//anchor: '95%',
									regex: /^.*\.(txt|TXT)$/,
									regexText:'Solo se admiten archivos TXT'
								},
								{
									xtype: 'hidden',
									id:	'hidExtension',
									name:	'hidExtension',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidNumTotal',
									name:	'hidNumTotal',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidTotalMonto',
									name:	'hidTotalMonto',
									value:	''
								},
								{
									xtype: 	'button',
									text: 	'Continuar',
									id: 		'btnContinuar',
									iconCls: 'icoContinuar',
									style: {
										  marginBottom:  '10px'
									},
									handler: function(){
										var cargaArchivo = Ext.getCmp('archivo');
										if (!cargaArchivo.isValid()){
											cargaArchivo.focus();
											return;
										}
										var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
										var extArchivo = Ext.getCmp('hidExtension');
										var numTotal = Ext.getCmp('hidNumTotal');
										var totalMonto = Ext.getCmp('hidTotalMonto');
										//var objMessage = Ext.getCmp('pnlMsgValid1');


										//objMessage.hide();
										//numTotal.setValue(Ext.getCmp('numtotalvenc1').getValue());
										//totalMonto.setValue(Ext.getCmp('montototalvenc1').getValue());

										if (/^.*\.(txt)$/.test(ifile)){
											extArchivo.setValue('txt');
										}

										fpCarga.getForm().submit({
											url: '13caterroreswsifm_file.data.jsp',
											waitMsg: 'Enviando datos...',
											success: function(form, action) {
												var resp = action.result;
												var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
												//var objMsg = Ext.getCmp('pnlMsgValid1');

												if(!resp.flag){
													//SE MOSTRARAN LEYENDAS DE VALIDACIONES
													//alert('error x');
												}else{
													//alert(resp.icProceso);
                          lstNumerosLineaOk = resp.lstNumerosLineaOk;
                          icProceso = resp.icProceso;
                          nombreIF = resp.nombreIF;
                          storeRegCorrectos.loadData(resp);
                          storeRegErroneos.loadData(resp);
                          Ext.getCmp('totalRC').setValue(resp.getNumRegOk);
                          Ext.getCmp('totalRE').setValue(resp.getNumRegErr);
                          panelResultValid.show();
						  if(Number(resp.getNumRegOk)<=0){
							  Ext.getCmp('btnContinuarCarga').disable();
						  }else{
							 Ext.getCmp('btnContinuarCarga').enable();
						  }
												}

											},
											failure: NE.util.mostrarSubmitError
										})
									}
								}
							]
						}

					]
				},
				{
					xtype: 'panel',
					name: 'pnlMsgValid',
					id: 'pnlMsgValid1',
					width: 500,
					style: 'margin:0 auto;',
					frame: true,
					hidden: true

				}
			]
		}
		//barraProgreso
	];
  
  //----------------------------------------------------------------------------
      
      var fpCarga = new Ext.form.FormPanel({
        id: 'formaCarga',
        width: 555,
        title: 'Carga Masiva',
        frame: true,
        fileUpload: true,
        style: 'margin:0 auto;',
        bodyStyle: 'padding: 6px; text-align:left;',
        defaultType: 'textfield',
        defaults: {
          msgTarget: 'side',
          anchor: '-20'
        },
        items: elementosFormCarga,
        monitorValid: true
      });
 
			 // SE DEFINE EL FORM PANEL DE LOS BOTONES
			 var gridForm = new Ext.FormPanel({
				  id:           'catalogo',        
				  labelAlign:   'top',
				  plain:        true,		  
				  width:        705,
				  border:       false,
				  monitorValid: true,
				  frame:        false,
				  labelAlign:  'right',	
				  style: 'margin: 0 auto',
				  bodyStyle: Ext.isIE ? 'padding:20px 5px 15px; text-align: left;' : 'padding:10px 15px; text-align: -moz-left;',
				  items: [
				  	{
                xtype: 'container',
                layout: 'hbox',
                height: 75,
                id: 'contenedorTipoCarga',
                layoutConfig: {
                    align: 'middle',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'Individual',
                        width: 100,
                        id: 'botonCargaIndividual',
                        handler: function(){
                          gridForm.getForm().getEl().dom.action = '/nafin/13descuento/13if/13caterroresws.jsp';
                          gridForm.getForm().getEl().dom.submit();
                        }
                    },
                  {
                    xtype: 'label',
                    text: 'x',
                    style: 'color:white;',
                    id: 'espacioBoton'
                  },
                  {
                        xtype: 'button',
                        text: 'Masiva',
                        width: 100,
                        id: 'botonCargaMasiva',
                        pressed: 'true'
                    }
                ]
					}
				]		
					//renderTo: Ext.get("formaCatalogo")
			});
    
    var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
    style: 'margin: 0 auto',
		store:  new Ext.data.ArrayStore({
        fields: [
          {name: 'NUMCAMPO'},
          {name: 'DESC'},
          {name: 'TIPO'},
          {name: 'LONGITUD'},
          {name: 'OBSERV'}
        ],
        data: [
                ['1','Clave','Alfanum�rico','8','Clave del Error'],
                ['2','Descripci�n','Alfanum�rico','255','Mensaje del Error']
              ]
     }),
		margins: '20 0 0 0',
		columns: [
			{ header : 'No.', dataIndex : 'NUMCAMPO', width : 50, sortable : true},
			{ header : 'Campo', dataIndex : 'DESC', width : 120, sortable : true},
      { header : 'Tipo Dato', dataIndex : 'TIPO', width : 120, sortable : true},
      { header : 'Long. Max', dataIndex : 'LONGITUD', width : 100, sortable : true},
      { header : 'Observaciones', dataIndex : 'OBSERV', width : 140, sortable : true}
		],
		stripeRows: true,
		columnLines : true,
    hidden:true,
		loadMask: true,
		width: 555,
		height: 130,
		frame: true,
    bbar:{
      xtype:'panel',
      html:'Los registros se transmitir�n en un archivo de texto plano con extensi�n .txt, los campos estar�n separados por el caracter "|" (pipe) y en el orden que se describen (No se permiten caracteres especiales). '
    }
	});
  
  var storeRegCorrectos = new Ext.data.JsonStore({
	  root: 'registrosValidos',
    fields: [
		  {name: 'BANCO'},
      {name: 'LINEA'},
		  {name: 'CLAVE'},
      {name: 'DESCRIPCION'}
	  ]
  });
 
  var storeRegErroneos = new Ext.data.JsonStore({
    root: 'registrosErroneos',
	  fields: [
		  {name: 'LINEA'},
		  {name: 'CAMPO'},
      {name: 'OBSERVACION'}
	  ]
  });
  
  var storeRegCargados = new Ext.data.ArrayStore({
    //root: 'registrosErroneos',
	  fields: [
		  {name: 'BANCO'},
      {name: 'LINEA'},
		  {name: 'CLAVE'},
      {name: 'DESCRIPCION'}
	  ]
  });
  
  var storeResumen = new Ext.data.ArrayStore({
    //root: 'registrosErroneos',
	  fields: [
		  {name: 'ETIQUETA'},
		  {name: 'TOTAL'}
	  ]
  });
  var gridRegCorrectos = new Ext.grid.GridPanel({
		id: 'gridRegCorrectos1',
		store: storeRegCorrectos,
		margins: '0 0 0 0', hideHeaders : false, stripeRows: true,
    columnLines : true,	loadMask: true,	width: 400, frame: true, height:200,
    style: 'margin:0 auto;',	title: 'Registro sin Errores',
		columns: [
			{header : 'L�nea', dataIndex : 'LINEA', width : 50, sortable : true },
      {header : 'Clave', dataIndex : 'CLAVE', width : 100, sortable : true },
      {header : 'Descripci�n', dataIndex : 'DESCRIPCION', width : 230, sortable : true, renderer:  function (causa, columna, registro){
        columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
        return causa; }
      }
		],
    bbar:{
      xtype:'panel', layout: 'form',
      items:[
        {xtype:'textfield', id:'totalRC', fieldLabel:'Total', value:'0', anchor: "50%"}
      ]
    }
	});
  
  var gridRegEroneos = new Ext.grid.GridPanel({
		id: 'gridRegEroneos1',
		store: storeRegErroneos,
		margins: '0 0 0 0', hideHeaders : false, stripeRows: true,
    columnLines : true,	loadMask: true,	width: 400, frame: true, height:200,
    style: 'margin:0 auto;',	title: 'Registro con Errores',
		columns: [
			{header : 'L�nea', dataIndex : 'LINEA', width : 50, sortable : true },
      {header : 'Campo', dataIndex : 'CAMPO', width : 100, sortable : true },
      {header : 'Observaci�n', dataIndex : 'OBSERVACION', width : 230, sortable : true, renderer:  function (causa, columna, registro){
        columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
        return causa; }
      }
		],
     bbar:{
      xtype:'panel', layout: 'form',
      items:[
        {xtype:'textfield', id:'totalRE', fieldLabel:'Total', value:'0', anchor: "50%"}
      ]
    }
	});
  
  
   var gridResumen = new Ext.grid.GridPanel({
		id: 'gridResumen1',
		store: storeResumen,
		margins: '0 0 0 0', hideHeaders : false, stripeRows: true,
    columnLines : true,	loadMask: true,	width: 400, frame: true, height:130,
    style: 'margin:0 auto;',	title: '', hidden:false,
		columns: [
			{header : ' ', dataIndex : 'ETIQUETA', width : 280, sortable : true },
      {header : 'Total', dataIndex : 'TOTAL', width : 100, sortable : true }
		]
    /*bbar:{
     
    }*/
	});
  
  var gridRegCargados = new Ext.grid.GridPanel({
		id: 'gridRegCargados1',
		store: storeRegCargados,
		margins: '0 0 0 0', hideHeaders : false, stripeRows: true,
    columnLines : true,	loadMask: true,	width: 600, frame: true, height:200,
    style: 'margin:0 auto;',	title: '', hidden:false,
		columns: [
			{header : 'Intermediario Financiero', dataIndex : 'BANCO', width : 250, sortable : true, renderer:  function (causa, columna, registro){
        registro.data['BANCO']=nombreIF;
        return nombreIF; } },
      {header : 'Clave', dataIndex : 'CLAVE', width : 100, sortable : true },
      {header : 'Descripci�n', dataIndex : 'DESCRIPCION', width : 230, sortable : true, renderer:  function (causa, columna, registro){
        columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
        return causa; }
      }
		],
    bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Desargar Archivo',
			id: 'btnDescargar',
			handler: function(){
          var registrosResumen=[];
          var registrosCargados=[];
          storeResumen.each(function(record){
            registrosResumen.push(record.data);
          });
          
          //storeRegCargados.commitChanges();
          storeRegCargados.each(function(record){
            registrosCargados.push(record.data);
          });
          
          Ext.Ajax.request({
            url: '13caterroreswsifm.data.jsp',
            params: {
              informacion: 'DescargaArchivo',
              registrosResumen: Ext.encode(registrosResumen),
              registrosCargados: Ext.encode(registrosCargados)
            },
            callback: procesarSuccessDescarga
          });
        }
			},
      '-',
      {
			text: 'Salir',
			id: 'btnSalir',
			handler: function(btn){
        gridForm.getForm().getEl().dom.method='GET';
        gridForm.getForm().getEl().dom.action = '/nafin/13descuento/13if/13caterroresws.jsp';
        gridForm.getForm().getEl().dom.submit();
			}
      }
		]
	}
	});
  
  var panelResultValid = new Ext.Panel({
    frame: false,
    layout: 'hbox',
    border: 0,
    width: 810,
    style: 'margin:0 auto;',
    //hidden: true,
    items:[
      gridRegCorrectos,
      gridRegEroneos
    ],
    buttons:[
      {id:'btnRegresar', name:'btnRegresar', text:'Regresar', handler: function(){ 
					window.location.href='13caterroreswsifm.jsp';
                    //gridForm.getForm().getEl().dom.submit();
			}},
      {id:'btnContinuarCarga', name:'btnContinuar', text:'Continuar Carga', handler:fnBtnContinuar}
    ]
  
  });

    var pnl = new Ext.Container({
      id: 'contenedorPrincipal',
      applyTo: 'areaContenido',
      width: 890,
      height: 'auto',
      items: [
        NE.util.getEspaciador(20),
        gridForm,
        fpCarga,
        gridLayout,
        NE.util.getEspaciador(10),
        panelResultValid,
        gridResumen,
        NE.util.getEspaciador(20),
        gridRegCargados
        
      ]
    });
		
    panelResultValid.hide();
    gridResumen.hide();
    gridRegCargados.hide();
        
			 			  

});
  
		