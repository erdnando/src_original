<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.math.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.dispersion.*, 
		com.netro.descuento.*,
    java.sql.*,
		javax.naming.*,
		net.sf.json.*,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>

<%!
  
  private boolean esClaveDeErrorRepetida(String claveIF,String claveError,String claveErrorExclusion)
		throws AppException{
		
		AccesoDB 				con  				= new AccesoDB();
		StringBuffer 			qrySentencia 	= new StringBuffer();
		ResultSet 				rs 				= null;
		PreparedStatement 	ps		 			= null;
		boolean					claveRepetida  = false;
		 
		try{
			
			con.conexionDB();
			qrySentencia.append(
				"SELECT                        								"  +
				"	DECODE(COUNT(1),0,'false','true') AS HAY_REGISTROS	"  +
				"FROM                                                 "  +
				"	COM_ERROR_IF                                    "  +
				"WHERE                                                "  +
				"  IC_IF           = ?                                "  +
				"	AND CC_ERROR_IF = ?                                "  +
				((claveErrorExclusion != null && !claveErrorExclusion.equals(""))?" AND CC_ERROR_IF NOT IN ( ? ) ":"")
			);
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,    Integer.parseInt(claveIF));
			ps.setString(2, claveError);
			if(claveErrorExclusion != null && !claveErrorExclusion.equals("")) ps.setString(3, claveErrorExclusion);
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				claveRepetida = "true".equals(rs.getString("HAY_REGISTROS"))?true:false;	
			}
			
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Ocurrio un Error al revisar si la Clave del Error esta repetida");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		return claveRepetida;
	}
	
	private boolean esDescripcionDeErrorRepetida(String claveIF,String descripcionError,String claveErrorExclusion)
		throws AppException{
		
		AccesoDB 				con  						= new AccesoDB();
		StringBuffer 			qrySentencia 			= new StringBuffer();
		ResultSet 				rs 						= null;
		PreparedStatement 	ps		 					= null;
		boolean					descripcionRepetida  = false;
		 
		try{
			
			con.conexionDB();
			qrySentencia.append(
				"SELECT                        								"  +
				"	DECODE(COUNT(1),0,'false','true') AS HAY_REGISTROS	"  +
				"FROM                                                 "  +
				"	COM_ERROR_IF                                    "  +
				"WHERE                                                "  +
				"  IC_IF              = ?                             "  +
				"	AND CG_DESCRIPCION = ?                             "  +
				((claveErrorExclusion != null && !claveErrorExclusion.equals(""))?" AND CC_ERROR_IF NOT IN ( ? ) ":"")
			);
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,    Integer.parseInt(claveIF));
			ps.setString(2, descripcionError);
			if(claveErrorExclusion != null && !claveErrorExclusion.equals("")) ps.setString(3, claveErrorExclusion);
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				descripcionRepetida = "true".equals(rs.getString("HAY_REGISTROS"))?true:false;	
			}
			
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Ocurrio un Error al revisar si la Descripcion del Error esta repetida");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		return descripcionRepetida;
	}
	
	private boolean borraRegistro(String claveIF,String claveError){
		
		AccesoDB				con 		= null;
		PreparedStatement ps 		= null;
		StringBuffer		strSQL 	= new StringBuffer();
		boolean				exito		= true;
		
		if(claveIF    == null 	|| claveIF.trim().equals("")   ){ return false;}
		if(claveError == null 	|| claveError.trim().equals("")){ return false;}
		
		try {
 
			con 		= new AccesoDB();
			con.conexionDB();
			strSQL.append(" delete from com_error_if  where ic_if = ? AND cc_error_if = ? ");

			ps =  con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, 		Integer.parseInt(claveIF));
			ps.setString(2, 	claveError);
			ps.executeUpdate();
 
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
		} finally {

		   if(ps != null) { try { ps.close();}catch(Exception e){} }	
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}

		return exito;
	}
	
	private boolean insertaRegistro(String claveIF,String claveError,String descripcionError){
		
		AccesoDB				con 		= null;
		PreparedStatement ps 		= null;
		StringBuffer		strSQL 	= new StringBuffer();
		boolean				exito		= true;
		
		if(claveIF    			== null 	|| claveIF.trim().equals("")   			){ return false;}
		if(claveError 			== null 	|| claveError.trim().equals("")			){ return false;}
		if(descripcionError 	== null 	|| descripcionError.trim().equals("")	){ return false;}
		
		try {
 
			con 		= new AccesoDB();
			con.conexionDB();
			strSQL.append(" insert into com_error_if(ic_if,cc_error_if,cg_descripcion) values(?,?,?) ");

			ps =  con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, 		Integer.parseInt(claveIF));
			ps.setString(2, 	claveError);
			ps.setString(3, 	descripcionError);
			ps.executeUpdate();
 
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
		} finally {

		   if(ps != null) { try { ps.close();}catch(Exception e){} }	
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}

		return exito;
	}
	
	private boolean borraRegistro(String claveIF,String claveError,AccesoDB con){
		
		//AccesoDB				con 		= null;
		PreparedStatement ps 		= null;
		StringBuffer		strSQL 	= new StringBuffer();
		boolean				exito		= true;
		
		if(claveIF    == null 	|| claveIF.trim().equals("")   ){ return false;}
		if(claveError == null 	|| claveError.trim().equals("")){ return false;}
		
		try {
 
			//con 		= new AccesoDB();
			//con.conexionDB();
			strSQL.append(" delete from com_error_if  where ic_if = ? AND cc_error_if = ? ");

			ps =  con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, 		Integer.parseInt(claveIF));
			ps.setString(2, 	claveError);
			ps.executeUpdate();
 
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
		} finally {
		   if(ps != null) { try { ps.close();}catch(Exception e){} }	
		}

		return exito;
	}
	
	private boolean insertaRegistro(String claveIF,String claveError,String descripcionError,AccesoDB con){
		
		//AccesoDB				con 		= null;
		PreparedStatement ps 		= null;
		StringBuffer		strSQL 	= new StringBuffer();
		boolean				exito		= true;
		
		if(claveIF    			== null 	|| claveIF.trim().equals("")   			){ return false;}
		if(claveError 			== null 	|| claveError.trim().equals("")			){ return false;}
		if(descripcionError 	== null 	|| descripcionError.trim().equals("")	){ return false;}
		
		try {
 
			//con 		= new AccesoDB();
			//con.conexionDB();
			strSQL.append(" insert into com_error_if(ic_if,cc_error_if,cg_descripcion) values(?,?,?) ");

			ps =  con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, 		Integer.parseInt(claveIF));
			ps.setString(2, 	claveError);
			ps.setString(3, 	descripcionError);
			ps.executeUpdate();
 
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
		} finally {

		   if(ps != null) { try { ps.close();}catch(Exception e){} }	

		}

		return exito;
	}
%>

<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String msgError = "";


String  tipoUsuario   = (String) request.getSession().getAttribute("strTipoUsuario");
String  claveIF = "0";

System.out.println(" ---> tipoUsuario   = <"+tipoUsuario+">");
System.out.println(" ---> numeroCliente = <"+((String) request.getSession().getAttribute("iNoCliente"))+">");

if("IF".equals(tipoUsuario)){
  claveIF = (String) request.getSession().getAttribute("iNoCliente");
}else if("NAFIN".equals(tipoUsuario)){
  claveIF = (String) request.getParameter("claveIF");
}

claveIF = claveIF==null?"0":claveIF;

	if (informacion.equals("DatosIniciales")) {
		JSONObject jsonObj = new JSONObject();
		SimpleDateFormat 	sdf 			= new SimpleDateFormat("dd/MM/yyyy");
		String 				sFechaHoy 	= sdf.format(new java.util.Date());
		
		CargaDocumento cargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB",CargaDocumento.class);
		
		boolean tieneParamEpoPef = cargaDocumentos.tieneParamEpoPef(iNoCliente);
		
		jsonObj.put("tieneParamEpoPef", new Boolean(tieneParamEpoPef));
		jsonObj.put("operaFVPyme", operaFVPyme);
		jsonObj.put("strNombreUsuario",strLogin + " " + strNombreUsuario);
		jsonObj.put("success", new Boolean(true));
		
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("ConsCatalogoWS")) {
		 AccesoDB 				con  				= new AccesoDB();
		 StringBuffer 			qrySentencia 	= new StringBuffer();
		 ResultSet 				rs 				= null;
		 PreparedStatement 	ps		 			= null;
		 int 						ctaRegistros 	= 0;
	    JSONArray 				jsonArray 		= null; 
		 
    try{
      con.conexionDB();
      
      // 1. Imprimir todos los parametros de consulta
      //doPrintRequestParameters(request);
      // 2. Leer clave y nombre de los IFs
      String claveError		 		= request.getParameter("clave");
      String descripcionError		= request.getParameter("descripcion");
      
      String nombreIF 		 = request.getParameter("comboIF"); 
      //String claveIF  		 = claveIF;
      String lineaInicio	 = request.getParameter("start");
      String numeroLineas   = request.getParameter("limit");
      String dir				 = request.getParameter("dir");
      String sort				 = request.getParameter("sort");
      
      nombreIF  = nombreIF     	== null?""     :nombreIF;
      int start = lineaInicio  	== null?0      :Integer.parseInt(lineaInicio);
      int limit = numeroLineas 	== null?0      :Integer.parseInt(numeroLineas);
      dir		 = dir 				== null?"ASC"  :dir;
      sort		 = sort 				== null?"clave":sort;
          
      String sortString = ("clave".equals(sort)?"CC_ERROR_IF":("descripcion".equals(sort)?"CG_DESCRIPCION":"CC_ERROR_IF"))+" "+("ASC".equals(dir)?"ASC":("DESC".equals(dir)?"DESC":"ASC"));
      
      boolean esNafin = "NAFIN".equals( (String) request.getSession().getAttribute("strTipoUsuario") )?true:false;
      
      qrySentencia.append(
        " SELECT                          "  +
        "	COUNT(1) AS NUM_REGISTROS  	 "  +
        "FROM                             "  +
        "	COM_ERROR_IF                "  +
        "WHERE                            "  +
        "  IC_IF = ?                      "  + 
        (claveError       != null && !claveError.trim().equals("")?" AND CC_ERROR_IF    = ? ":"") +
        (descripcionError != null && !descripcionError.trim().equals("")?" AND CG_DESCRIPCION = ? ":"")
      );
      
      ps = con.queryPrecompilado(qrySentencia.toString());
      ps.setInt(1, Integer.parseInt(claveIF) );
      int cta = 2;
      if(claveError       != null && !claveError.trim().equals("")) ps.setString(cta++,claveError);
      if(descripcionError != null && !descripcionError.trim().equals("")) ps.setString(cta++,descripcionError);		
      rs = ps.executeQuery();
      
      int totalCount = 0;
      if(rs != null && rs.next()){
        totalCount = rs.getInt("NUM_REGISTROS");
      }
      rs.close();
      ps.close();
      
      qrySentencia = new StringBuffer();
      qrySentencia.append(
        " SELECT                          "  +
        "	CC_ERROR_IF    AS CLAVE,       "  +
        "	CG_DESCRIPCION AS DESCRIPCION  "  +
        "FROM                             "  +
        "	COM_ERROR_IF                "  +
        "WHERE                            "  +
        "  IC_IF = ?                      "  +
        (claveError       != null && !claveError.trim().equals("")?" AND CC_ERROR_IF    = ? ":"")+
        (descripcionError != null && !descripcionError.trim().equals("")?" AND CG_DESCRIPCION = ? ":"")+
        "ORDER BY " + sortString + "      "
      );
      
      ps = con.queryPrecompilado(qrySentencia.toString());
      ps.setInt(1, Integer.parseInt(claveIF) );
      cta = 2;
      if(claveError       != null && !claveError.trim().equals("")) ps.setString(cta++,claveError);
      if(descripcionError != null && !descripcionError.trim().equals("")) ps.setString(cta++,descripcionError);
      rs = ps.executeQuery();
        
      jsonArray = new JSONArray();
      while(rs != null && rs.next()){
          
        if(ctaRegistros >= start && ctaRegistros <(start+limit)){
          
          String clave  = rs.getString("CLAVE");
          String descripcion = rs.getString("DESCRIPCION");
            
          clave  = clave  == null?"0"            :clave;
          descripcion = descripcion == null?"NO DISPONIBLE":descripcion;
            
          JSONObject json = new JSONObject();
          json.put("clave",        clave);
          json.put("descripcion",  descripcion);
          json.put("modificar",    "Modificar");
          json.put("eliminar",     "Eliminar");
            
          jsonArray.add(json);
          
          if(ctaRegistros == (start+limit)) break;
          
        }	
        
        ctaRegistros++;
          
      }
   
      response.setContentType("application/json; charset=iso-8859-1");
      if(totalCount == 0){
        infoRegresar = "{success:true,claveIF:"+claveIF+",nombreIF:\""+nombreIF+"\",totalCount:"+totalCount+
                        (esNafin?",successmsg:\"No hay claves de rechazo registradas.\"":",popup:{type:'WARNING',message:'No hay claves de rechazo registradas. Favor de Verificar.'}")+
                        ",\"registros\":"+jsonArray.toString()+",clave:\""+claveError+"\",descripcion:\""+descripcionError+"\"}";
      }else{
        infoRegresar = "{success:true,claveIF:"+claveIF+",nombreIF:\""+nombreIF+"\",totalCount:"+totalCount+",\"registros\":"+jsonArray.toString()+",clave:\""+claveError+"\",descripcion:\""+descripcionError+"\"}";	
      }
      
      } catch(Exception e) {
        e.printStackTrace();
      } finally {
        if(rs != null) { try { rs.close();}catch(Exception e){} }
        if(ps != null) { try { ps.close();}catch(Exception e){} }
        if(con.hayConexionAbierta()) {
          con.cierraConexionDB();
        }
    }

 
		
		//infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registrosDoctos\": " + jsObjArray.toString() + " }";


}else if(informacion.equals("CatWsSave")) {

   AccesoDB 				con 				= new AccesoDB();
	 StringBuffer 			qrySentencia 	= new StringBuffer();
	 ResultSet 				rs 				= null;
	 PreparedStatement 	ps		 			= null;
	 int 						ctaRegistros 	= 0;
	 JSONArray 				jsonArray 		= null;
	 
	 boolean hayClaveRepetida			= false;
	 boolean hayDescripcionRepetida	= false;
	 boolean hayExitoOperacion			= false;
		 
    try {
      
		 con.conexionDB(); 
 
		 // 2. LEER PARAMETROS DE LA CONSULTA
		String 	nombreIF 			= request.getParameter("comboIF"); 
		//111111111111111111111111111111111111String 	claveIF  			= getClaveIF(request);
		
		boolean 	doModifyRegister 	= ("true".equals((String) request.getParameter("doModifyRegister")))?true:false;
		boolean  doSaverRegister	= doModifyRegister?false:true;
		
		String   claveAnterior 		= request.getParameter("claveAnterior");
		
		String 	claveError		 	= request.getParameter("clave");
		String 	descripcionError	= request.getParameter("descripcion");
		
		// 3. FORMATEAR PARAMETROS LEIDOS
		nombreIF 		= nombreIF == null?"":nombreIF;
		claveAnterior  = (claveAnterior == null || claveAnterior.trim().equals(""))?"":claveAnterior;
		 
		// 4. Si es una modificacion
		if(doSaverRegister){
			// 4.1 Verificar si la clave ya fue dada de alta
			hayClaveRepetida 				= esClaveDeErrorRepetida(claveIF,claveError,null);
			// 4.2 Verificar si la descripcion del error ya fue dada de alta
			if(!hayClaveRepetida){	
				hayDescripcionRepetida 	= esDescripcionDeErrorRepetida(claveIF,descripcionError,null);
			}
			// 4.3 Insertar nuevo registro
			if( !hayClaveRepetida && !hayDescripcionRepetida){
				hayExitoOperacion = insertaRegistro(claveIF,claveError,descripcionError);
			}
		// 5. SI ES GUARDAR REGISTRO
		}else if(doModifyRegister){
			// 4.1 Verificar si la clave nueva ya fue dada de alta
			hayClaveRepetida 			= esClaveDeErrorRepetida(claveIF,claveError,claveAnterior);
			// 4.2 Verificar si la descripcion nueva ya fue dada de alta
			if(!hayClaveRepetida){
				hayDescripcionRepetida 	= esDescripcionDeErrorRepetida(claveIF,descripcionError,claveAnterior);
			}
			// 4.3 Borrar registro actual
			if(!hayClaveRepetida && !hayDescripcionRepetida){
				hayExitoOperacion = borraRegistro(claveIF,claveAnterior,con);
			}
			// 4.4 Insertar registro nuevo
			if(!hayClaveRepetida && !hayDescripcionRepetida && hayExitoOperacion){
				hayExitoOperacion = insertaRegistro(claveIF,claveError,descripcionError,con);
			}
		}

		// 5. Realizar consulta para que se actualice el gridpanel 
		claveError 			= "";
		descripcionError 	= "";
		
		String lineaInicio	 = request.getParameter("start");
		String numeroLineas   = request.getParameter("limit");
		String dir				 = request.getParameter("dir");
		String sort				 = request.getParameter("sort");
		
		nombreIF  = nombreIF     	== null?""     :nombreIF;
		int start = lineaInicio  	== null?0      :Integer.parseInt(lineaInicio);
		int limit = numeroLineas 	== null?0      :Integer.parseInt(numeroLineas);
		dir		 = dir 				== null?"ASC"  :dir;
		sort		 = sort 				== null?"clave":sort;
				
		String sortString = ("clave".equals(sort)?"CC_ERROR_IF":("descripcion".equals(sort)?"CG_DESCRIPCION":"CC_ERROR_IF"))+" "+("ASC".equals(dir)?"ASC":("DESC".equals(dir)?"DESC":"ASC"));
		
		boolean esNafin = "NAFIN".equals( (String) request.getSession().getAttribute("strTipoUsuario") )?true:false;
		
		qrySentencia.append(
			" SELECT                          "  +
			"	COUNT(1) AS NUM_REGISTROS  	 "  +
			"FROM                             "  +
			"	COM_ERROR_IF                "  +
			"WHERE                            "  +
			"  IC_IF = ?                      "  + 
			(claveError       != null && !claveError.trim().equals("")?" AND CC_ERROR_IF    = ? ":"") +
			(descripcionError != null && !descripcionError.trim().equals("")?" AND CG_DESCRIPCION = ? ":"")
		);
		
		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, Integer.parseInt(claveIF) );
		int cta = 2;
		if(claveError       != null && !claveError.trim().equals("")) ps.setString(cta++,claveError);
		if(descripcionError != null && !descripcionError.trim().equals("")) ps.setString(cta++,descripcionError);		
		rs = ps.executeQuery();
		
		int totalCount = 0;
		if(rs != null && rs.next()){
			totalCount = rs.getInt("NUM_REGISTROS");
		}
		rs.close();
		ps.close();
		
		qrySentencia = new StringBuffer();
		qrySentencia.append(
			" SELECT                          "  +
			"	CC_ERROR_IF    AS CLAVE,       "  +
			"	CG_DESCRIPCION AS DESCRIPCION  "  +
			"FROM                             "  +
			"	COM_ERROR_IF                "  +
			"WHERE                            "  +
			"  IC_IF = ?                      "  +
			(claveError       != null && !claveError.trim().equals("")?" AND CC_ERROR_IF    = ? ":"")+
			(descripcionError != null && !descripcionError.trim().equals("")?" AND CG_DESCRIPCION = ? ":"")+
			"ORDER BY " + sortString + "      "
		);
		
		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, Integer.parseInt(claveIF) );
		cta = 2;
		if(claveError       != null && !claveError.trim().equals("")) ps.setString(cta++,claveError);
		if(descripcionError != null && !descripcionError.trim().equals("")) ps.setString(cta++,descripcionError);
		rs = ps.executeQuery();
			
		jsonArray = new JSONArray();
		while(rs != null && rs.next()){
				
			if(ctaRegistros >= start && ctaRegistros <(start+limit)){
				
				String clave  = rs.getString("CLAVE");
				String descripcion = rs.getString("DESCRIPCION");
					
				clave  = clave  == null?"0"            :clave;
				descripcion = descripcion == null?"NO DISPONIBLE":descripcion;
					
				JSONObject json = new JSONObject();
				json.put("clave",        clave);
				json.put("descripcion",  descripcion);
				json.put("modificar",    "Modificar");
				json.put("eliminar",     "Eliminar");
					
				jsonArray.add(json);
				
				if(ctaRegistros == (start+limit)) break;
				
			}	
			
			ctaRegistros++;
				
		}
 
		boolean 	hayError 		= false;
		String 	mensajeError 	= "";
		if(hayClaveRepetida){
			mensajeError 	= "La clave de error a dar de alta ya se encuentra registrada.";
			hayError			= true;
		}else if(hayDescripcionRepetida){
			mensajeError 	= "La descripcion del error a dar de alta ya se encuentra registrada.";
			hayError			= true;
		}else if(!hayExitoOperacion){
			mensajeError 	= "Ocurrio un error al "+(doModifyRegister?"modificar":"guardar")+" el registro.";
			hayError			= true;
		}
		
		response.setContentType("application/json; charset=iso-8859-1");
		if(totalCount == 0){
			infoRegresar = "{success:true,claveIF:"+claveIF+",nombreIF:\""+nombreIF+"\",totalCount:"+
                      totalCount+(esNafin?",successmsg:\"No hay claves de rechazo registradas.\"":",popup:{type:'WARNING',message:'"+(hayError?mensajeError:"No hay claves de rechazo registradas. Favor de Verificar.")+"'}")+
                      ",\"registros\":"+jsonArray.toString()+",clave:\""+claveError+"\",descripcion:\""+descripcionError+"\""+(doModifyRegister && hayError?",doModifyRegisterAgain:true,claveAnterior:'"+claveAnterior+"'":"")+
                      ",doSaveAction:true}";
		}else{
			infoRegresar = "{success:true,claveIF:"+claveIF+",nombreIF:\""+nombreIF+"\",totalCount:"+totalCount+",\"registros\":"+jsonArray.toString()+",clave:\""+
                    claveError+"\",descripcion:\""+descripcionError+"\""+(hayError?",popup:{type:'WARNING',message:'"+
                    mensajeError+"'}":",popup:{type:'INFO',message:'La descripcion y/o clave del error se "+(doModifyRegister?"modifico":"guardo")+" de forma exitosa'}")+
                    (doModifyRegister && hayError?",doModifyRegisterAgain:true,claveAnterior:'"+claveAnterior+"'":"")+",doSaveAction:true}";	
		}
		 
    	} catch(Exception e) {
        e.printStackTrace();
    	} finally {
        if(rs != null) { try { rs.close();}catch(Exception e){} }
        if(ps != null) { try { ps.close();}catch(Exception e){} }
        if (con.hayConexionAbierta()) {
          con.terminaTransaccion(hayExitoOperacion);
          con.cierraConexionDB();
        }
        
    	}


}else if(informacion.equals("CatWsDelete")) {

 AccesoDB 	con  				= new AccesoDB();
	 boolean		bOk				= true;
	 StringBuffer 			qrySentencia 	= new StringBuffer();
	 ResultSet 				rs 				= null;
	 PreparedStatement 	ps		 			= null;
	 int 						ctaRegistros 	= 0;
	 JSONArray 				jsonArray 		= null;
    
    try {
		// 1. Leer clave y nombre de los IFs
		String nombreIF 	= request.getParameter("comboIF");
		String claveError = request.getParameter("clave");
		//String claveIF  	= getClaveIF(request);
		
		nombreIF = nombreIF == null?"":nombreIF;
 
		bOk = borraRegistro(claveIF,claveError);
		
		// 2. Leer clave y nombre de los IFs
		con.conexionDB();
		String descripcionError		= request.getParameter("descripcion");
		claveError 			= "";
		descripcionError	= "";
		
		String lineaInicio	 = request.getParameter("start");
		String numeroLineas   = request.getParameter("limit");
		String dir				 = request.getParameter("dir");
		String sort				 = request.getParameter("sort");
		
		nombreIF  = nombreIF     	== null?""     :nombreIF;
		int start = lineaInicio  	== null?0      :Integer.parseInt(lineaInicio);
		int limit = numeroLineas 	== null?0      :Integer.parseInt(numeroLineas);
		dir		 = dir 				== null?"ASC"  :dir;
		sort		 = sort 				== null?"clave":sort;
				
		String sortString = ("clave".equals(sort)?"CC_ERROR_IF":("descripcion".equals(sort)?"CG_DESCRIPCION":"CC_ERROR_IF"))+" "+("ASC".equals(dir)?"ASC":("DESC".equals(dir)?"DESC":"ASC"));
		
		boolean esNafin = "NAFIN".equals( (String) request.getSession().getAttribute("strTipoUsuario") )?true:false;
		
		qrySentencia = new StringBuffer();
		qrySentencia.append(
			" SELECT                          "  +
			"	COUNT(1) AS NUM_REGISTROS  	 "  +
			"FROM                             "  +
			"	COM_ERROR_IF                "  +
			"WHERE                            "  +
			"  IC_IF = ?                      "  + 
			(claveError       != null && !claveError.trim().equals("")?" AND CC_ERROR_IF    = ? ":"") +
			(descripcionError != null && !descripcionError.trim().equals("")?" AND CG_DESCRIPCION = ? ":"")
		);
		
		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, Integer.parseInt(claveIF) );
		int cta = 2;
		if(claveError       != null && !claveError.trim().equals("")) ps.setString(cta++,claveError);
		if(descripcionError != null && !descripcionError.trim().equals("")) ps.setString(cta++,descripcionError);		
		rs = ps.executeQuery();
		
		int totalCount = 0;
		if(rs != null && rs.next()){
			totalCount = rs.getInt("NUM_REGISTROS");
		}
		rs.close();
		ps.close();
		
		qrySentencia = new StringBuffer();
		qrySentencia.append(
			" SELECT                          "  +
			"	CC_ERROR_IF    AS CLAVE,       "  +
			"	CG_DESCRIPCION AS DESCRIPCION  "  +
			"FROM                             "  +
			"	COM_ERROR_IF                "  +
			"WHERE                            "  +
			"  IC_IF = ?                      "  +
			(claveError       != null && !claveError.trim().equals("")?" AND CC_ERROR_IF    = ? ":"")+
			(descripcionError != null && !descripcionError.trim().equals("")?" AND CG_DESCRIPCION = ? ":"")+
			"ORDER BY " + sortString + "      "
		);
		
		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, Integer.parseInt(claveIF) );
		cta = 2;
		if(claveError       != null && !claveError.trim().equals("")) ps.setString(cta++,claveError);
		if(descripcionError != null && !descripcionError.trim().equals("")) ps.setString(cta++,descripcionError);
		rs = ps.executeQuery();
			
		jsonArray = new JSONArray();
		while(rs != null && rs.next()){
				
			if(ctaRegistros >= start && ctaRegistros <(start+limit)){
				
				String clave  = rs.getString("CLAVE");
				String descripcion = rs.getString("DESCRIPCION");
					
				clave  = clave  == null?"0"            :clave;
				descripcion = descripcion == null?"NO DISPONIBLE":descripcion;
					
				JSONObject json = new JSONObject();
				json.put("clave",        clave);
				json.put("descripcion",  descripcion);
				json.put("modificar",    "Modificar");
				json.put("eliminar",     "Eliminar");
					
				jsonArray.add(json);
				
				if(ctaRegistros == (start+limit)) break;
				
			}	
			
			ctaRegistros++;
				
		}
 
		response.setContentType("application/json; charset=iso-8859-1");
		if(totalCount == 0){
			infoRegresar = "{success:true,claveIF:"+claveIF+",nombreIF:\""+nombreIF+"\",totalCount:"+
                      totalCount+(esNafin?",successmsg:\"No hay claves de rechazo registradas.\"":",popup:{type:'WARNING',message:'El registro "+(!bOk?"NO":"")+" fue Eliminado.'}")+
                      ",\"registros\":"+jsonArray.toString()+",clave:\""+claveError+"\",descripcion:\""+descripcionError+"\"}";
		}else{
			infoRegresar = "{success:true,claveIF:"+claveIF+",nombreIF:\""+nombreIF+"\",totalCount:"+totalCount+",\"registros\":"+jsonArray.toString()+
                    ",popup:{type:'WARNING',message:'El registro "+(!bOk?"NO":"")+" fue Eliminado.'},clave:\""+claveError+"\",descripcion:\""+descripcionError+"\"}";	
		}
		
    } catch(Exception e) {
		
      e.printStackTrace();
    } finally {
    	if(rs != null) { try { rs.close();}catch(Exception e){} }
      if(ps != null) { try { ps.close();}catch(Exception e){} }
      if(con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      
    }

}else if(informacion.equals("ConsCatalogoWS")) {



}

	

%>


<%=infoRegresar%>