<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.math.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.dispersion.*, 
		com.netro.descuento.*,
    java.sql.*,
		javax.naming.*,
		net.sf.json.*,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>

<%!
  
  private boolean esClaveDeErrorRepetida(String claveIF,String claveError,String claveErrorExclusion)
		throws AppException{
		
		AccesoDB 				con  				= new AccesoDB();
		StringBuffer 			qrySentencia 	= new StringBuffer();
		ResultSet 				rs 				= null;
		PreparedStatement 	ps		 			= null;
		boolean					claveRepetida  = false;
		 
		try{
			
			con.conexionDB();
			qrySentencia.append(
				"SELECT                        								"  +
				"	DECODE(COUNT(1),0,'false','true') AS HAY_REGISTROS	"  +
				"FROM                                                 "  +
				"	COM_ERROR_IF                                    "  +
				"WHERE                                                "  +
				"  IC_IF           = ?                                "  +
				"	AND CC_ERROR_IF = ?                                "  +
				((claveErrorExclusion != null && !claveErrorExclusion.equals(""))?" AND CC_ERROR_IF NOT IN ( ? ) ":"")
			);
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,    Integer.parseInt(claveIF));
			ps.setString(2, claveError);
			if(claveErrorExclusion != null && !claveErrorExclusion.equals("")) ps.setString(3, claveErrorExclusion);
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				claveRepetida = "true".equals(rs.getString("HAY_REGISTROS"))?true:false;	
			}
			
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Ocurrio un Error al revisar si la Clave del Error esta repetida");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		return claveRepetida;
	}
	
	private boolean esDescripcionDeErrorRepetida(String claveIF,String descripcionError,String claveErrorExclusion)
		throws AppException{
		
		AccesoDB 				con  						= new AccesoDB();
		StringBuffer 			qrySentencia 			= new StringBuffer();
		ResultSet 				rs 						= null;
		PreparedStatement 	ps		 					= null;
		boolean					descripcionRepetida  = false;
		 
		try{
			
			con.conexionDB();
			qrySentencia.append(
				"SELECT                        								"  +
				"	DECODE(COUNT(1),0,'false','true') AS HAY_REGISTROS	"  +
				"FROM                                                 "  +
				"	COM_ERROR_IF                                    "  +
				"WHERE                                                "  +
				"  IC_IF              = ?                             "  +
				"	AND CG_DESCRIPCION = ?                             "  +
				((claveErrorExclusion != null && !claveErrorExclusion.equals(""))?" AND CC_ERROR_IF NOT IN ( ? ) ":"")
			);
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,    Integer.parseInt(claveIF));
			ps.setString(2, descripcionError);
			if(claveErrorExclusion != null && !claveErrorExclusion.equals("")) ps.setString(3, claveErrorExclusion);
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				descripcionRepetida = "true".equals(rs.getString("HAY_REGISTROS"))?true:false;	
			}
			
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Ocurrio un Error al revisar si la Descripcion del Error esta repetida");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		return descripcionRepetida;
	}
	
	private boolean borraRegistro(String claveIF,String claveError){
		
		AccesoDB				con 		= null;
		PreparedStatement ps 		= null;
		StringBuffer		strSQL 	= new StringBuffer();
		boolean				exito		= true;
		
		if(claveIF    == null 	|| claveIF.trim().equals("")   ){ return false;}
		if(claveError == null 	|| claveError.trim().equals("")){ return false;}
		
		try {
 
			con 		= new AccesoDB();
			con.conexionDB();
			strSQL.append(" delete from com_error_if  where ic_if = ? AND cc_error_if = ? ");

			ps =  con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, 		Integer.parseInt(claveIF));
			ps.setString(2, 	claveError);
			ps.executeUpdate();
 
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
		} finally {

		   if(ps != null) { try { ps.close();}catch(Exception e){} }	
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}

		return exito;
	}
	
	private boolean insertaRegistro(String claveIF,String claveError,String descripcionError){
		
		AccesoDB				con 		= null;
		PreparedStatement ps 		= null;
		StringBuffer		strSQL 	= new StringBuffer();
		boolean				exito		= true;
		
		if(claveIF    			== null 	|| claveIF.trim().equals("")   			){ return false;}
		if(claveError 			== null 	|| claveError.trim().equals("")			){ return false;}
		if(descripcionError 	== null 	|| descripcionError.trim().equals("")	){ return false;}
		
		try {
 
			con 		= new AccesoDB();
			con.conexionDB();
			strSQL.append(" insert into com_error_if(ic_if,cc_error_if,cg_descripcion) values(?,?,?) ");

			ps =  con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, 		Integer.parseInt(claveIF));
			ps.setString(2, 	claveError);
			ps.setString(3, 	descripcionError);
			ps.executeUpdate();
 
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
		} finally {

		   if(ps != null) { try { ps.close();}catch(Exception e){} }	
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}

		return exito;
	}
	
	private boolean borraRegistro(String claveIF,String claveError,AccesoDB con){
		
		//AccesoDB				con 		= null;
		PreparedStatement ps 		= null;
		StringBuffer		strSQL 	= new StringBuffer();
		boolean				exito		= true;
		
		if(claveIF    == null 	|| claveIF.trim().equals("")   ){ return false;}
		if(claveError == null 	|| claveError.trim().equals("")){ return false;}
		
		try {
 
			//con 		= new AccesoDB();
			//con.conexionDB();
			strSQL.append(" delete from com_error_if  where ic_if = ? AND cc_error_if = ? ");

			ps =  con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, 		Integer.parseInt(claveIF));
			ps.setString(2, 	claveError);
			ps.executeUpdate();
 
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
		} finally {
		   if(ps != null) { try { ps.close();}catch(Exception e){} }	
		}

		return exito;
	}
	
	private boolean insertaRegistro(String claveIF,String claveError,String descripcionError,AccesoDB con){
		
		//AccesoDB				con 		= null;
		PreparedStatement ps 		= null;
		StringBuffer		strSQL 	= new StringBuffer();
		boolean				exito		= true;
		
		if(claveIF    			== null 	|| claveIF.trim().equals("")   			){ return false;}
		if(claveError 			== null 	|| claveError.trim().equals("")			){ return false;}
		if(descripcionError 	== null 	|| descripcionError.trim().equals("")	){ return false;}
		
		try {
 
			//con 		= new AccesoDB();
			//con.conexionDB();
			strSQL.append(" insert into com_error_if(ic_if,cc_error_if,cg_descripcion) values(?,?,?) ");

			ps =  con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, 		Integer.parseInt(claveIF));
			ps.setString(2, 	claveError);
			ps.setString(3, 	descripcionError);
			ps.executeUpdate();
 
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
		} finally {

		   if(ps != null) { try { ps.close();}catch(Exception e){} }	

		}

		return exito;
	}
%>

<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String msgError = "";


String  tipoUsuario   = (String) request.getSession().getAttribute("strTipoUsuario");
String  claveIF = "0";

System.out.println(" ---> tipoUsuario   = <"+tipoUsuario+">");
System.out.println(" ---> numeroCliente = <"+((String) request.getSession().getAttribute("iNoCliente"))+">");

if("IF".equals(tipoUsuario)){
  claveIF = (String) request.getSession().getAttribute("iNoCliente");
}else if("NAFIN".equals(tipoUsuario)){
  claveIF = (String) request.getParameter("claveIF");
}

claveIF = claveIF==null?"0":claveIF;

if (informacion.equals("RealizarCargaMasiva")) {
  JSONObject jsonObj = new JSONObject();
  String icProceso = (request.getParameter("icProceso")!=null)?request.getParameter("icProceso"):"";
  String lstNumerosLineaOk = (request.getParameter("lstNumerosLineaOk")!=null)?request.getParameter("lstNumerosLineaOk"):"";
  
  boolean insercionExitosa = false;
  if(session.getAttribute("13CATERRORESWSIFMB_INSERTED") != null){
     System.out.println("Los registros ya se insertaron por lo que no volveran a ser insertados");
     insercionExitosa = true;
  }else{
     CatalogoErroresWSIF catalogo = new CatalogoErroresWSIF();
     insercionExitosa 		= catalogo.insertarRegistros(claveIF,lstNumerosLineaOk,icProceso);
     if(insercionExitosa) 	session.setAttribute("13CATERRORESWSIFMB_INSERTED","true");
     if(!insercionExitosa){ 	
       //totalRegistrosCargadosCorrectamente 	= "0";
       //totalRegistrosCargados 					= totalRegistrosConError;
     } 
  }
  
  jsonObj.put("insercionExitosa", new Boolean(insercionExitosa));
  jsonObj.put("strNombreUsuario",strNombreUsuario);
  jsonObj.put("success", new Boolean(true));
  
  infoRegresar = jsonObj.toString();

}else if (informacion.equals("DescargaArchivo")) {
  
  String registrosResumen = (request.getParameter("registrosResumen") == null)?"":request.getParameter("registrosResumen");
  String registrosCargados = (request.getParameter("registrosCargados") == null)?"":request.getParameter("registrosCargados");
  
  
  CreaArchivo 	archivo 				= new CreaArchivo();
  String 			nombreArchivo 		= "";
  StringBuffer 	contenidoArchivo 	= new StringBuffer();
  
  List arrRegistros = JSONArray.fromObject(registrosResumen);
  Iterator itReg = arrRegistros.iterator();
  
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
    contenidoArchivo.append(registro.getString("ETIQUETA")+","+registro.getString("TOTAL")+"\r\n");
	}
  
  /*contenidoArchivo.append("No. Total de Registros Cargados,"+1+"\r\n");
  contenidoArchivo.append("No. Total de Registros Cargados Correctamente,"+1+"\r\n");
  contenidoArchivo.append("No. Total de Registros con Error,"+1+"\r\n");
  contenidoArchivo.append("Usuario,\""+strNombreUsuario+"\"\r\n");*/
  
  contenidoArchivo.append(",,\r\n");
  
  // AGREGAR ENCABEZADO
  contenidoArchivo.append("Intermediario Financiero,Clave,Descripción\n");
  // AGREGAR CONTENIDO
  
  String nombreIF = "";
  String clave = "";
  String descripcion = "";
  
  arrRegistros = JSONArray.fromObject(registrosCargados);
  itReg = arrRegistros.iterator();
  
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();
     contenidoArchivo.append(
      "\""+registro.getString("BANCO") +"\",\""+registro.getString("CLAVE") +"\",\""+registro.getString("DESCRIPCION") + "\"\r\n"
    );
	}
  
  if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
			out.print("<--!Error al generar el archivo-->");
		else
			nombreArchivo = archivo.nombre;
  
  JSONObject jsonObj = new JSONObject();
  jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
  infoRegresar = jsonObj.toString();
}
%>


<%=infoRegresar%>