<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	java.text.*,
	com.netro.afiliacion.*,
	com.netro.parametrosgrales.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
        org.apache.commons.logging.Log,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%!private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% 
/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String informacion		= (request.getParameter("informacion") == null) ? "" : request.getParameter("informacion");
String operacion			= (request.getParameter("operacion") == null) ? "" : request.getParameter("operacion");
String ic_if				= (request.getParameter("ic_if") == null) ? iNoCliente : request.getParameter("ic_if");
log.trace("request.getParameter:"+ic_if);
String moneda				= (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");
String cliente				= (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
String prestamo			= (request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
String FechaInicio		= (request.getParameter("FechaInicio") == null) ? "" : request.getParameter("FechaInicio");
String FechaFin			= (request.getParameter("FechaFin") == null) ? "" : request.getParameter("FechaFin");
String FechaCorte			= (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");
String tipoLinea			= (request.getParameter("tipoLinea") == null) ? "" : request.getParameter("tipoLinea");
String selecBF				= (request.getParameter("selecBF") == null) ? "N" : request.getParameter("selecBF");

String infoRegresar = null;
int TOTAL_CAMPOS = 18;
int nRow = 0, numMon = 0, numDL = 0;
double totSaldoInsMon = 0.0;
double totAmortizMon = 0.0;
double totInteresMon = 0.0;
double totDescFopMon = 0.0;
double totDescFinapeMon	= 0.0;
double totVentoMon = 0.0;
double totAdeudoTotMon = 0.0;
double totSaldoInsNvoMon = 0.0;
double totExigibleMon = 0.0;
double totSaldoInsDL = 0.0;
double totAmortizDL = 0.0;
double totInteresDL = 0.0;
double totDescFopDL = 0.0;
double totDescFinapeDL	= 0.0;
double totVentoDL = 0.0;
double totAdeudoTotDL = 0.0;
double totSaldoInsNvoDL = 0.0;
double totExigibleDL = 0.0;

PagosIFNB  beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
iNoCliente 		= (request.getParameter("ic_ifs") 	== null || "".equals(request.getParameter("ic_ifs"))) ? iNoCliente : request.getParameter("ic_ifs");
log.trace("iNoCliente  ========= " + iNoCliente);
if(informacion.equals("CatMoneda")){
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setClave("ic_moneda");
	cat.setDescripcion("cd_nombre");
	cat.setValoresCondicionIn("1,54", Integer.class);
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();
}

else if(informacion.equals("CatFechaCorte")){
	AccesoDB con = new AccesoDB();
	String strQuery = "";
	try{
		con.conexionDB();
		if(selecBF.equals("B")){
			strQuery = 
				" Select distinct TO_CHAR (com_fechaprobablepago,'dd/mm/yyyy'),com_fechaprobablepago "+
				" From bmx_vencimiento"+
				" where ic_if ="+iNoCliente+
				" order by com_fechaprobablepago";
		}else if(selecBF.equals("N")){
			
			if(!"C".equals(tipoLinea)){
			strQuery = 
				" Select distinct TO_CHAR (com_fechaprobablepago,'dd/mm/yyyy'),com_fechaprobablepago "+
 				" From com_vencimiento"+
 				" where ic_if ="+iNoCliente+
				" order by com_fechaprobablepago";
			}else{
				strQuery = 
					" Select distinct TO_CHAR (com_fechaprobablepago,'dd/mm/yyyy'),com_fechaprobablepago "+
					" From com_vencimiento"+
					" where ic_if = 12 " +
					" and ig_cliente = "+cliente+
					" order by com_fechaprobablepago";
			}
		}
		ResultSet rs = con.queryDB(strQuery);
		
		List reg = new ArrayList();
		HashMap datos;
		
		if(rs.getRow()>0){ reg = new ArrayList(); }
		while(rs.next()) {
			datos = new HashMap();
			datos.put("clave",rs.getString(1));
			datos.put("descripcion",rs.getString(1));
			reg.add(datos);
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
		rs.close();
	}catch(SQLException e){
		String message = e.getMessage();
		String sqlState = e.getSQLState();
		int errorCode = e.getErrorCode();
		log.error("-Message:" + message + "\n\t-SQL State: " + sqlState + "\n\t\t-Error Code: " + errorCode);
		e.printStackTrace();
	}finally{
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
}

else if(informacion.equals("DescargaZIP")){	
	
  String nombreZip = "archvenc_"+FechaCorte.replace('/','_')+"_"+iNoCliente+"_S.zip";
  //if("C".equals(tipoLinea)){
    VencIfDE paginador = new VencIfDE();			
    paginador.setMoneda(moneda);
    paginador.setCliente(cliente);
    paginador.setPrestamo(prestamo);
    paginador.setFechaCorte(FechaCorte);
    paginador.setIc_if(iNoCliente);
    paginador.setTipoLinea(tipoLinea);
    
    
    CQueryHelperRegExtJS  queryHelper = new CQueryHelperRegExtJS( paginador);
    String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "ZIP");
    GenerarArchivoZip zip = new GenerarArchivoZip();
    
    //String directory = strDirectorioPublicacion + "16archivos/descuento/procesos/vencimiento/";
    //zip.generandoZip(strDirectorioTemp,directory,nombreArchivo.replaceAll(".txt",""));
    zip.generandoZip(strDirectorioTemp,strDirectorioTemp,nombreArchivo.replaceAll(".txt",""));
    //beanPagos.guardarArchivosZip(nombreZip, strDirectorioTemp);
  /*  
  }else{
    beanPagos.consultaArchivosZip(nombreZip, strDirectorioTemp);
  }*/
  
  //String pathFileZip = "/nafin/16archivos/descuento/procesos/vencimiento/archvenc_"+FechaCorte.replaceAll("/","_") + "_"+iNoCliente+"_S.zip";
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	//jsonObj.put("urlArchivo", pathFileZip);
  jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreZip);
	infoRegresar = jsonObj.toString();
  
  
  
  
}

else if(informacion.equals("Consultar")){
	int start = 0, limit = 15;
	try {
		start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
		limit = Integer.parseInt((request.getParameter("limit")==null)?"0":request.getParameter("limit"));				
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
		log.debug("selecBF  " + selecBF);
      
      
	if(selecBF.equals("N")){
   
      String qryStr = "SELECT   count(1) FROM com_vencimiento v, comcat_moneda m WHERE v.ic_moneda = m.ic_moneda AND v.ic_if = " + iNoCliente;
      AccesoDB con = new AccesoDB();
      int nRegistrosQry = 0;
      try{
         ResultSet rs;
         con.conexionDB();
            rs = con.queryDB(qryStr);         
            List reg = new ArrayList();
            HashMap datos;
            if (rs!=null && rs.next()) {
               nRegistrosQry = Integer.parseInt(rs.getString(1));
            }        
         }catch(Exception e) { 
            e.printStackTrace();
            throw new RuntimeException(e);
      }finally {	
         if(con.hayConexionAbierta()){ 
            con.cierraConexionDB();	
         }
      }
   
		VencIfDE paginador = new VencIfDE();			
		paginador.setMoneda(moneda);
		paginador.setCliente(cliente);
		paginador.setPrestamo(prestamo);
		paginador.setFechaCorte(FechaCorte);
                log.trace("paginador-iNoCliente:"+iNoCliente);
		paginador.setIc_if(iNoCliente);
			
		CQueryHelperRegExtJS queryHelperRegExtJS	= new CQueryHelperRegExtJS(paginador); 
      if(nRegistrosQry>1000){
         queryHelperRegExtJS.setMaximoNumeroRegistros(nRegistrosQry);
      }
		
		if((informacion.equals("Consultar") && operacion.equals("Generar")) || (informacion.equals("Consultar") && operacion.equals(""))){ 
			try {
				if(operacion.equals("Generar")){ //Nueva consulta
					queryHelperRegExtJS.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				String cadena = queryHelperRegExtJS.getJSONPageResultSet(request,start,limit);	
				
				if(queryHelperRegExtJS.getIdsSize()>0) {					//se guardara en bitacora
					//beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,iNoCliente,"","V");
					if(!"LINEA CREDITO".equals(strPerfil)){
						if(!"C".equals(tipoLinea)){
					beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,iNoCliente,"","V");			
						}else{
							beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,ic_if,"","V", "", "C");
						}
					}else{
							beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,"","","V", ic_if, "C");
					}
				}
				infoRegresar	= cadena;
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		}
		
		String parametro = "";
		String paramNumDocto = "";//FODEA 015 - 2009 ACF
		String Query="";
		String Query2="";
		String Query3="";
		ResultSet rs_fc;
		ResultSet rs_par;
		ResultSet rs_par2;
		con = new AccesoDB();
		
		try{
			con.conexionDB();
               Query2= 
					" Select cs_layout_completo_venc "+
					" From com_param_x_if"+
					" Where ic_if ="+ic_if;
				  rs_par = con.queryDB(Query2);
				  if (rs_par.next()) {
					 parametro=rs_par.getString(1);
                log.debug("rs_par.getString(1) " + rs_par.getString(1));
				  } else 
				  {
					Query3= 
					" Select cs_layout_completo_venc "+
					" From com_param_gral"+
					" Where ic_param_gral = 1 ";
					rs_par2 = con.queryDB(Query3);
					if(rs_par2.next()) { 
                  parametro=rs_par2.getString(1);
                  log.debug("(2) rs_par2.getString(1) " + rs_par2.getString(1));
					} else {
                  parametro="S";
					}
				  }
				//FODEA 015 - 2009 ACF (I)
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			strSQL.append("SELECT cs_layout_doctos_venc FROM com_param_x_if WHERE ic_if = ?");
			varBind.add(new Long(ic_if));
			PreparedStatement pst = con.queryPrecompilado(strSQL.toString(), varBind);
			ResultSet rst = pst.executeQuery();
			if(rst.next()){
            log.debug("rst.getString(1) " + rst.getString(1));
			  paramNumDocto = rst.getString(1)==null?"":rst.getString(1);
			}else{
			  paramNumDocto = "N";
			}
			rst.close();
			pst.close();
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}finally{
			if(con.hayConexionAbierta()){ 
				con.cierraConexionDB();	
			}
		}
		if(operacion.equals("PDF")){
			paginador.setVecTotales(queryHelperRegExtJS.getResultCount(request));
			paginador.setParamNumDocto(paramNumDocto);
			paginador.setParametro(parametro);
			String nombreArchivo = queryHelperRegExtJS.getCreatePageCustomFile(request, start, limit,strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}else if(operacion.equals("TXT")){
			con = new AccesoDB();
			CreaArchivo archivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer("");
			String nombreArchivo          = null;
         String contArchivo            = null;
			
			try {
				con.conexionDB();
				CQueryHelper queryHelper = new CQueryHelper(new VencIfDE());
				if(parametro.equals("S")) {
				  contenidoArchivo.append("FechaProbablePago   ;ModalidadPago       ;CodMoneda           ;DescMoneda          ;Intermediario       ;DescIntermediario   ;BaseOperacion       ;DescBaseOper        ;Cliente             ;Nombre              ;Sucursal            ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Bursatilizado       ;Disposicion         ;SubAplicacion       ;CuotasEmit          ;CuotasPorVenc       ;TotalCuotas         ;AnioModalidad       ;TipoEstrato         ;FechaOpera          ;Sancionado          ;FrecuenciaCapital   ;FrecuenciaInteres   ;TasaBase            ;EsquemaTasas        ;RelMat_1            ;Spread_1            ;RelMat_2            ;Spread_2            ;RelMat_3            ;Spread_3            ;RelMat_4            ;Margen              ;TipoGarantia        ;PorcDescFop         ;TotDescFop          ;PorcDescFinape      ;TotDescFinape       ;FCrecimiento        ;PeriodoInic         ;PeriodoFin          ;Dias                ;Comision            ;PorcGarantia        ;PorcComision        ;SdoInsoluto         ;Amortizacion        ;Interes             ;IntCobAnt           ;TotalVencimiento    ;PagoTrad            ;Subsidio            ;TotalExigible       ;CapitalVencido      ;InteresVencido      ;TotalCarVen         ;AdeudoTotal         ;FinAdicOtorgado     ;FinAdicRecup        ;SdoInsNvo           ;SdoInsBase\n");
				 } else {
				  contenidoArchivo.append("Cliente             ;Nombre              ;Prestamo            ;EsquemaTasas        ;Margen              ;TotDescFop          ;TotDescFinape       ;SdoInsoluto         ;Amortizacion        ;Interes             ;TotalVencimiento    ;TotalExigible       ;SdoInsNvo\n");
				}
				
				ResultSet rs = queryHelper.getCreateFile(request,con);
			
				GenerarContenidoArchivos conten = new GenerarContenidoArchivos();
                                log.trace("parametro:"+parametro);
				contArchivo = conten.llenarContenido(rs, parametro, 1,"","");//FODEA 015 - 2009 ACF
						
				if(!archivo.make(contArchivo, strDirectorioTemp, ".txt"))
					log.error("<--!Error al generar el archivo-->");
				else
					nombreArchivo = archivo.nombre;
			} catch(Exception e) {
				 out.println("Error: " + e);
			} finally {
				if (con.hayConexionAbierta() == true)
				con.cierraConexionDB();
			}	
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}else	if(operacion.equals("Totales")){
         log.trace("...........:::::::::: Totalesnext()::::::......");
         log.trace("...........:::::::::: start / limit :::::::::...... (" + start + " / " + limit + ")");
			//Registros registros = queryHelperRegExtJS.doSearch();
         Registros regTotales= queryHelperRegExtJS.getPageResultSet(request, start, limit);
         log.trace("....::::::::: regTotales ::::::::..... (" + regTotales.getNumeroRegistros() + ")");
         //Registros regTotales= queryHelperRegExtJS.getPageResultSet(request, start, limit);
			List reg=new ArrayList();
			int mx = 0;
			int dl = 0;
			int cont = 0;	
			int contador = 0;
			
			while(regTotales.next()){
            log.trace("While regTotales :::::::::::::::::::::::");
				//Vector vecDatos = new Vector();
				//for(int i=1; i<=TOTAL_CAMPOS; i++) {
				//	vecDatos.add(registros.getString(i));
				//}
				//FODEA 015 - 2009 ACF
				//String numeroDocumento = regTotales.getString("numerodocumento")==null?"":regTotales.getString("numerodocumento");
				
				//if(start<=cont && (start+15)>cont){	
					contador++;
               log.trace("......:::::::::: regTotales.getString(3) " + regTotales.getString(3));
					if(regTotales.getString(16).equals("54")){
						numDL++; 
						dl=1;
						totSaldoInsDL		+= Double.valueOf(regTotales.getString(8)).doubleValue();
						totAmortizDL		+= Double.valueOf(regTotales.getString(9)).doubleValue();
						totInteresDL		+= Double.valueOf(regTotales.getString(10)).doubleValue();
						totDescFopDL		+= Double.valueOf(regTotales.getString(11)).doubleValue();
						totDescFinapeDL	+= Double.valueOf(regTotales.getString(12)).doubleValue();
						totVentoDL			+= Double.valueOf(regTotales.getString(13)).doubleValue();
						totAdeudoTotDL		+= Double.valueOf(regTotales.getString(14)).doubleValue();
						totSaldoInsNvoDL	+= Double.valueOf(regTotales.getString(15)).doubleValue();
						totExigibleDL		+= Double.valueOf(regTotales.getString("fg_totalexigible")).doubleValue();
					} else {		
						numMon++; 
						mx=1;
						totSaldoInsMon		+= Double.valueOf(regTotales.getString(8)).doubleValue();
						totAmortizMon		+= Double.valueOf(regTotales.getString(9)).doubleValue();
						totInteresMon		+= Double.valueOf(regTotales.getString(10)).doubleValue();
						totDescFopMon		+= Double.valueOf(regTotales.getString(11)).doubleValue();
						totDescFinapeMon	+= Double.valueOf(regTotales.getString(12)).doubleValue();
						totVentoMon			+= Double.valueOf(regTotales.getString(13)).doubleValue();
						totAdeudoTotMon	+= Double.valueOf(regTotales.getString(14)).doubleValue();
						totSaldoInsNvoMon	+= Double.valueOf(regTotales.getString(15)).doubleValue();
						totExigibleMon		+= Double.valueOf(regTotales.getString("fg_totalexigible")).doubleValue();
					}
				//}
				cont++;
			}
			
         log.debug("....:::: END WHILE :::::.... ");
         
				HashMap datos;
				if(mx>0){
					datos = new HashMap();
					datos.put("MONEDA","Total Parcial Moneda Nacional");
					datos.put("N_REGISTROS","");
					datos.put("TOTA_1","" 	+ totSaldoInsMon);
					datos.put("TOTA_2","" 	+ totAmortizMon);
					datos.put("TOTA_3","" 	+ totInteresMon);
					datos.put("TOTA_4","" 	+ totDescFopMon);
					datos.put("TOTA_5",""   + totDescFinapeMon);
					datos.put("TOTA_6","" 	+ totVentoMon);
					datos.put("TOTA_7",""   + totExigibleMon);
					datos.put("TOTA_8",""   + totAdeudoTotMon);
					datos.put("TOTA_9",""   + totSaldoInsNvoMon);
					datos.put("TOTA_10","");
					reg.add(datos);
				}			
				if(dl>0){				
					datos = new HashMap();
					datos.put("MONEDA","Total Parcial Moneda D�lares");
					datos.put("N_REGISTROS","");
					datos.put("TOTA_1","" 	+ totSaldoInsDL);
					datos.put("TOTA_2","" 	+ totAmortizDL);
					datos.put("TOTA_3","" 	+ totInteresDL);
					datos.put("TOTA_4",""   + totDescFopDL);
					datos.put("TOTA_5","" 	+ totDescFinapeDL);
					datos.put("TOTA_6",""   + totVentoDL);
					datos.put("TOTA_7",""   + totExigibleDL);
					datos.put("TOTA_8",""   + totAdeudoTotDL);
					datos.put("TOTA_9",""   + totSaldoInsNvoDL);
					datos.put("TOTA_10","");
					reg.add(datos);
				}
				
			Registros vecTotales = queryHelperRegExtJS.getResultCount(request);
         log.debug("....:::::: vecTotales :::::..... " + vecTotales.getNumeroRegistros());
			if(vecTotales.getNumeroRegistros()>0){
				int j = 0;
				List vecColumnas = null;
				for(j=0;j<vecTotales.getNumeroRegistros();j++){
					vecColumnas = vecTotales.getData(j);
					
					datos = new HashMap();
					datos.put("MONEDA","Total " + vecColumnas.get(9).toString());
					datos.put("N_REGISTROS",vecColumnas.get(0).toString());
					datos.put("TOTA_1","" + vecColumnas.get(1).toString().toString());
					datos.put("TOTA_2","" + vecColumnas.get(2).toString().toString());
					datos.put("TOTA_3","" + vecColumnas.get(3).toString().toString());
					datos.put("TOTA_4","" + vecColumnas.get(4).toString().toString());
					datos.put("TOTA_5","" + vecColumnas.get(5).toString().toString());
					datos.put("TOTA_6","" + vecColumnas.get(6).toString().toString());
					datos.put("TOTA_7","" + vecColumnas.get(10).toString().toString());
					datos.put("TOTA_8","" + vecColumnas.get(7).toString().toString());
					datos.put("TOTA_9","" + vecColumnas.get(8).toString().toString());
					datos.put("TOTA_10","");
					reg.add(datos);
				}
			}
				
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			infoRegresar=jsonObj.toString();
		}
	}
	/***************************************************************************
	***************************************************************************/
	/*else{ 
		nRow = 0; numMon = 0; numDL = 0;
		double totCapInsMon = 0.0;
		double totTasaBaseMon = 0.0;
		double totSobreTasaMon = 0.0;
		double totTasaTotalMon = 0.0;
		double totIntCalcMon	= 0.0;
		double totTotalMon = 0.0;
		double totCapInsDL = 0.0;
		double totTasaBaseDL = 0.0;
		double totSobreTasaDL = 0.0;
		double totTasaTotalDL = 0.0;
		double totIntCalcDL	= 0.0;
		double totTotalDL = 0.0;      
   
      String qryStr = "SELECT   COUNT(1) FROM bmx_vencimiento v, comcat_moneda m WHERE v.ic_moneda = m.ic_moneda_bmx AND v.ic_if = 11";
      AccesoDB con = new AccesoDB();
      int nRegistrosQry = 0;
      try{
         ResultSet rs;
         con.conexionDB();
            rs = con.queryDB(qryStr);         
            List reg = new ArrayList();
            HashMap datos;
            if (rs!=null && rs.next()) {
               nRegistrosQry = Integer.parseInt(rs.getString(1));
            }        
         }catch(Exception e) { 
            e.printStackTrace();
            throw new RuntimeException(e);
      }finally {	
         if(con.hayConexionAbierta()){ 
            con.cierraConexionDB();	
         }
      }
   
		
		VencIfBancomext paginador = new VencIfBancomext();			
		paginador.setMoneda(moneda);
		paginador.setCliente(cliente);
		paginador.setPrestamo(prestamo);
		paginador.setFechaCorte(FechaCorte);
		paginador.setIc_if(iNoCliente);
			
		CQueryHelperRegExtJS queryHelperRegExtJS	= new CQueryHelperRegExtJS(paginador); 
      if(nRegistrosQry>1000){
         queryHelperRegExtJS.setMaximoNumeroRegistros(nRegistrosQry);
      }
      
		
		if((informacion.equals("Consultar") && operacion.equals("Generar")) || (informacion.equals("Consultar") && operacion.equals(""))){  //Nueva consulta
			try {
				if(operacion.equals("Generar")){ //Nueva consulta
					queryHelperRegExtJS.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				String cadena = queryHelperRegExtJS.getJSONPageResultSet(request,start,limit);			
				infoRegresar	= cadena;
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		}
		
      
      
		con = new AccesoDB();
      String parametro = "";
		String paramNumDocto = "";//FODEA 015 - 2009 ACF
		String Query="";
		String Query2="";
		String Query3="";
		ResultSet rs_fc;
		ResultSet rs_par;
		ResultSet rs_par2;
      
      if(!operacion.equals("Generar") || !operacion.equals("")){
		
		try{
			con.conexionDB();
			Query2= 
					" Select cs_layout_completo_venc "+
					" From com_param_x_if"+
					" Where ic_if ="+iNoCliente;
				  rs_par = con.queryDB(Query2);
				  if (rs_par.next()) {
					 parametro=rs_par.getString(1);
				  } else 
				  {
					Query3= 
					" Select cs_layout_completo_venc "+
					" From com_param_gral"+
					" Where ic_param_gral = 1 ";
					rs_par2 = con.queryDB(Query3);
					if(rs_par2.next()) { 
					parametro=rs_par2.getString(1);
					} else {
					 parametro="S";
					}
				  }
				//FODEA 015 - 2009 ACF (I)
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			strSQL.append("SELECT cs_layout_doctos_venc FROM com_param_x_if WHERE ic_if = ?");
			varBind.add(new Long(iNoCliente));
			PreparedStatement pst = con.queryPrecompilado(strSQL.toString(), varBind);
			ResultSet rst = pst.executeQuery();
			if(rst.next()){
			  paramNumDocto = rst.getString(1)==null?"":rst.getString(1);
			}else{
			  paramNumDocto = "N";
			}
			rst.close();
			pst.close();
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}finally{
			if(con.hayConexionAbierta()){ 
				con.cierraConexionDB();	
			}
		}
   }if(operacion.equals("PDF")){
			paginador.setVecTotales(queryHelperRegExtJS.getResultCount(request));
			paginador.setParamNumDocto(paramNumDocto);
			paginador.setParametro(parametro);
			String nombreArchivo = queryHelperRegExtJS.getCreatePageCustomFile(request, start, limit,strDirectorioTemp, "PDF");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}else if(operacion.equals("TXT")){
			con = new AccesoDB();
			CreaArchivo archivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer("");
			String nombreArchivo = null;
			
			try {
				con.conexionDB();
				CQueryHelper queryHelper = new CQueryHelper(new VencIfDE());
				if(parametro.equals("S")) {
				  contenidoArchivo.append("FechaProbablePago   ;ModalidadPago       ;CodMoneda           ;DescMoneda          ;Intermediario       ;DescIntermediario   ;BaseOperacion       ;DescBaseOper        ;Cliente             ;Nombre              ;Sucursal            ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Bursatilizado       ;Disposicion         ;SubAplicacion       ;CuotasEmit          ;CuotasPorVenc       ;TotalCuotas         ;AnioModalidad       ;TipoEstrato         ;FechaOpera          ;Sancionado          ;FrecuenciaCapital   ;FrecuenciaInteres   ;TasaBase            ;EsquemaTasas        ;RelMat_1            ;Spread_1            ;RelMat_2            ;Spread_2            ;RelMat_3            ;Spread_3            ;RelMat_4            ;Margen              ;TipoGarantia        ;PorcDescFop         ;TotDescFop          ;PorcDescFinape      ;TotDescFinape       ;FCrecimiento        ;PeriodoInic         ;PeriodoFin          ;Dias                ;Comision            ;PorcGarantia        ;PorcComision        ;SdoInsoluto         ;Amortizacion        ;Interes             ;IntCobAnt           ;TotalVencimiento    ;PagoTrad            ;Subsidio            ;TotalExigible       ;CapitalVencido      ;InteresVencido      ;TotalCarVen         ;AdeudoTotal         ;FinAdicOtorgado     ;FinAdicRecup        ;SdoInsNvo           ;SdoInsBase\n");
				 } else {
				  contenidoArchivo.append("Cliente             ;Nombre              ;Prestamo            ;EsquemaTasas        ;Margen              ;TotDescFop          ;TotDescFinape       ;SdoInsoluto         ;Amortizacion        ;Interes             ;TotalVencimiento    ;TotalExigible       ;SdoInsNvo\n");
				}
				
				ResultSet rs = queryHelper.getCreateFile(request,con);
			
				GenerarContenidoArchivos conten = new GenerarContenidoArchivos();
				String contArchivo = conten.llenarContenido(rs, parametro, 1,paramNumDocto,"");//FODEA 015 - 2009 ACF
						
				if(!archivo.make(contArchivo, strDirectorioTemp, ".txt"))
					out.print("<--!Error al generar el archivo-->");
				else
					nombreArchivo = archivo.nombre;
			} catch(Exception e) {
				 out.println("Error: " + e);
			} finally {
				if (con.hayConexionAbierta() == true)
				con.cierraConexionDB();
			}	
         System.out.println("strDirecVirtualTemp+nombreArchivo " + strDirecVirtualTemp+nombreArchivo);
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		}else	if(operacion.equals("Totales")){
         try {
            start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
            limit = Integer.parseInt((request.getParameter("limit")==null)?"0":request.getParameter("limit"));				
         } catch(Exception e) {
            throw new AppException("Error en los parametros recibidos", e);
         }
         
         System.out.println("...........:::::::::: Totales B :::::::::......");
         System.out.println("...........:::::::::: start / limit :::::::::...... (" + start + " / " + limit + ")");
         
			Registros registros = queryHelperRegExtJS.doSearch();
         Registros regTotales= queryHelperRegExtJS.getPageResultSet(request, start, limit);
			List reg=new ArrayList();
			int mx = 0;
			int dl = 0;
			int cont = 0;	
			int contador = 0;
			
         System.err.println("registros.getNumeroRegistros() + " + registros.getNumeroRegistros());
         
			while(regTotales.next()){
				Vector vecDatos = new Vector();
				//for(int i=1; i<=TOTAL_CAMPOS; i++) {
					//vecDatos.add(registros.getString(i));
				//}
				//FODEA 015 - 2009 ACF
				//String numeroDocumento = registros.getString("numerodocumento")==null?"":registros.getString("numerodocumento");
            
            
				
            System.err.println("regTotales.getString(2) " + regTotales.getString(2));
				if(regTotales.getString(2).equals("2")){
					numDL++;
					totCapInsDL		+= Double.valueOf(regTotales.getString(7)).doubleValue();
					totTasaBaseDL	+= Double.valueOf(regTotales.getString(10)).doubleValue();
					totSobreTasaDL	+= Double.valueOf(regTotales.getString(11)).doubleValue();
					totTasaTotalDL	+= Double.valueOf(regTotales.getString(12)).doubleValue();
					totIntCalcDL	+= Double.valueOf(regTotales.getString(13)).doubleValue();
					totTotalDL		+= Double.valueOf(regTotales.getString(14)).doubleValue();
				} else {
					numMon++;
					totCapInsMon	+= Double.valueOf(regTotales.getString(7)).doubleValue();
					totTasaBaseMon	+= Double.valueOf(regTotales.getString(10)).doubleValue();
					totSobreTasaMon	+= Double.valueOf(regTotales.getString(11)).doubleValue();
					totTasaTotalMon	+= Double.valueOf(regTotales.getString(12)).doubleValue();
					totIntCalcMon	+= Double.valueOf(regTotales.getString(13)).doubleValue();
					totTotalMon		+= Double.valueOf(regTotales.getString(14)).doubleValue();
				}
			}
				HashMap datos;		            
				if(mx>0){
					datos = new HashMap();
					datos.put("MONEDA","Total Parcial Moneda Nacional");
					datos.put("N_REGISTROS","");
					datos.put("SALDOINS","" 	+ totSaldoInsMon);
					datos.put("TOTAMORTIZ","" 	+ totAmortizMon);
					datos.put("TOTINTERES","" 	+ totInteresMon);
					datos.put("TOTDESFINAPE",""+ totDescFinapeMon);
					datos.put("TOTVENTO","" 	+ totVentoMon);
					datos.put("TOTADEUDOTOT",""+ totAdeudoTotMon);
					datos.put("TOTSALDOINSNVO",""+ totSaldoInsNvoMon);
					datos.put("TOTALEXIGIBLE","" + totExigibleMon);
					reg.add(datos);
				}
				if(dl>0){				
					datos = new HashMap();
					datos.put("MONEDA","Total Parcial Moneda D�lares");
					datos.put("N_REGISTROS","");
					datos.put("SALDOINS","" 	+ totSaldoInsDL);
					datos.put("TOTAMORTIZ","" 	+ totAmortizDL);
					datos.put("TOTINTERES","" 	+ totInteresDL);
					datos.put("TOTDESFINAPE",""+ totDescFinapeDL);
					datos.put("TOTVENTO","" 	+ totVentoDL);
					datos.put("TOTADEUDOTOT",""+ totAdeudoTotDL);
					datos.put("TOTSALDOINSNVO","" + totSaldoInsNvoDL);
					datos.put("TOTALEXIGIBLE","" + totExigibleDL);
					reg.add(datos);
				}
				
         System.err.println("@@@@@@@@@@@ OBTIENE TOTALES @@@@@@@@@@@");
			Registros vecTotales = queryHelperRegExtJS.getResultCount(request);
         System.err.println("vecTotales.getNumeroRegistros() " + vecTotales.getNumeroRegistros());
			if(vecTotales.getNumeroRegistros()>0){
				int j = 0;
				List vecColumnas = null;
				for(j=0;j<vecTotales.getNumeroRegistros();j++){
					vecColumnas = vecTotales.getData(j);
					
					datos = new HashMap();
					datos.put("MONEDA","Total " + vecColumnas.get(7).toString());
					datos.put("N_REGISTROS",vecColumnas.get(0).toString());
					datos.put("TOTA_1","" + vecColumnas.get(1).toString().toString());
					datos.put("TOTA_2","" + vecColumnas.get(2).toString().toString());
					datos.put("TOTA_3","" + vecColumnas.get(3).toString().toString());
					datos.put("TOTA_4","" + vecColumnas.get(4).toString().toString());
					datos.put("TOTA_5","" + vecColumnas.get(5).toString().toString());
					datos.put("TOTA_6","" + vecColumnas.get(6).toString().toString());
					reg.add(datos);
				}
			}
				
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			infoRegresar=jsonObj.toString();
		}
	}*/
}


else if(informacion.equals("ObtieneParametro")){
	AccesoDB con = new AccesoDB();
	String parametro = "";
	String paramNumDocto = "";//FODEA 015 - 2009 ACF
	String Query="";
	String Query2="";
	String Query3="";
	ResultSet rs_fc;
	ResultSet rs_par;
	ResultSet rs_par2;
	try{
			con.conexionDB();
			Query2= 
				" Select cs_layout_completo_venc "+
 				" From com_param_x_if"+
 				" Where ic_if ="+iNoCliente;
                                log.trace("Query2:"+Query2);
			  rs_par = con.queryDB(Query2);
			  if (rs_par.next()) {
			    parametro=rs_par.getString(1);
			  } else 
			  {
			   Query3= 
				" Select cs_layout_completo_venc "+
 				" From com_param_gral"+
 				" Where ic_param_gral = 1 ";
			   rs_par2 = con.queryDB(Query3);
			   if(rs_par2.next()) { 
				parametro=rs_par2.getString(1);
			   } else {
			    parametro="S";
			   }
			  }
           	//FODEA 015 - 2009 ACF (I)
            StringBuffer strSQL = new StringBuffer();
            List varBind = new ArrayList();
            strSQL.append("SELECT cs_layout_doctos_venc FROM com_param_x_if WHERE ic_if = ?");
            varBind.add(new Long(iNoCliente));
            PreparedStatement pst = con.queryPrecompilado(strSQL.toString(), varBind);
            ResultSet rst = pst.executeQuery();
            if(rst.next()){
              paramNumDocto = rst.getString(1)==null?"":rst.getString(1);
            }else{
              paramNumDocto = "N";
            }
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}finally{
			if(con.hayConexionAbierta()){ 
				con.cierraConexionDB();	
			}
		}
	JSONObject jsonObj = new JSONObject();
  if(!"LINEA CREDITO".equals(strPerfil)){
    HashMap hmDataIf = beanPagos.getSiracFinaciera(iNoCliente);
	
	String numSirac = (hmDataIf.get("NUMERO_SIRAC")==null || ((String)hmDataIf.get("NUMERO_SIRAC")).equals(""))?"0":(String)hmDataIf.get("NUMERO_SIRAC");
    jsonObj.put("IC_FINANCIERA", hmDataIf.get("IC_FINANCIERA"));
    jsonObj.put("NUMERO_SIRAC", numSirac);
  }else{
    String numSirac = beanPagos.getSiracLineaCredito(iNoCliente);
    jsonObj.put("IC_FINANCIERA","");
    jsonObj.put("NUMERO_SIRAC", ("".equals(numSirac)?"0":numSirac));
  }
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("parametro", parametro);
   jsonObj.put("paramNumDocto", paramNumDocto);
	infoRegresar=jsonObj.toString();
}
%>
<%= infoRegresar%>
