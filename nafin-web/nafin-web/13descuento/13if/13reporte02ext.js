Ext.onReady(function() {

//********************** 32  Programado Pyme a Seleccionado Pyme ******************+++++++++

	var procesarGenerarPDF32 =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF32');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF32');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesar32Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		if (arrRegistros != null) {
			if (!grid32.isVisible()) {
				grid32.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = grid32.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				grid32.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S' ){
				grid32.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				grid32.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				grid07.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				grid32.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				grid32.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				grid32.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				grid32.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}	
			
			var el = grid32.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				Ext.getCmp('btnGenerarPDF32').enable();				
				consuTotalesData.load({  params:{  ic_cambio_estatus: 32 }  });
				el.unmask();							
			} else {		
				Ext.getCmp('btnGenerarPDF32').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consulta32Data = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'NOMBRE_PYME'}	,
			{name: 'NOMBRE_EPO'}	,		
			{name: 'NUM_DOCTO'},				
			{name: 'MONEDA'},				
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_DOCTO'},
			{name: 'POR_DESC'},
			{name: 'REC_GARANTIA'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'TASA'},
			{name: 'MONTO_INT'},			
			{name: 'BENEFICIARIO'},
			{name: 'BANCO_BENEFICIARIO'},
			{name: 'SUCURSAL_BENEFICIARIO'},
			{name: 'CUENTA_BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMP_RECIBIR'},
			{name: 'NETO_RECIBIR'},
			{name: 'REFERENCIA_PYME'} //FODEA 17
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesar32Data,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesar32Data(null, null, null);					
				}
			}
		}		
	});
	
	var grid32= new Ext.grid.GridPanel({
		store: consulta32Data,
		id: 'grid32',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus:Operada - Operada Pendiente de Pago </div><div style="float:right"></div></div>',			
		columns: [	
			{
				header: 'Nombre PYME/Cedente',
				tooltip: 'Nombre PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},				
			{
				header: 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')
			},
			{
				header: 'Monto Int',
				tooltip: 'Monto Int',
				dataIndex: 'MONTO_INT',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia PyME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}		
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF32',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDF32
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF32',
					hidden: true
				}								
			]
		}
	});
	
	

//********************** 26  Pignorado a Negociable ******************+++++++++

	var procesarGenerarPDF26 =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF26');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF26');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesar26Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
	
		if (arrRegistros != null) {
			if (!grid26.isVisible()) {
				grid26.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = grid26.getColumnModel();
		
		
			var el = grid26.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				Ext.getCmp('btnGenerarPDF26').enable();				
				consuTotalesData.load({  params:{  ic_cambio_estatus: 26  }  });
				el.unmask();							
			} else {		
				Ext.getCmp('btnGenerarPDF26').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consulta26Data = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'NOMBRE_EPO'}	,		
			{name: 'NUM_DOCTO'},			
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'MONEDA'},					
			{name: 'MONTO_DOCTO'},
			{name: 'POR_DESC'},		
			{name: 'MONTO_DESCONTAR'},
			{name: 'REFERENCIA_PYME'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesar26Data,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesar26Data(null, null, null);					
				}
			}
		}		
	});
	
	var grid26 = new Ext.grid.GridPanel({
		store: consulta26Data,
		id: 'grid26',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus:Pignorado a Negociable </div><div style="float:right"></div></div>',			
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},			
			{
				header: 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},			
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Referencia',
				tooltip: 'Referencia PyME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,				
				resizable: true,
				align: 'left'
			}			
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF26',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDF26
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF26',
					hidden: true
				}								
			]
		}
	});
	
	

//********************** 7  Operada - Operada Pendiente de Pago ******************+++++++++


	var procesarGenerarPDF07 =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF07');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF07');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesar07Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
	
		if (arrRegistros != null) {
			if (!grid07.isVisible()) {
				grid07.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = grid07.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				grid07.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S' ){
				grid07.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				grid07.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				grid07.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				grid07.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				grid07.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				grid07.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				grid07.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}	
			
			var el = grid07.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				Ext.getCmp('btnGenerarPDF07').enable();				
				consuTotalesData.load({  params:{  ic_cambio_estatus: 7  }  });
				el.unmask();							
			} else {		
				Ext.getCmp('btnGenerarPDF07').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consulta07Data = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'NOMBRE_PYME'}	,
			{name: 'NOMBRE_EPO'}	,		
			{name: 'NUM_DOCTO'},			
			{name: 'NUM_SOLICITUD'},
			{name: 'MONEDA'},				
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_DOCTO'},
			{name: 'POR_DESC'},
			{name: 'REC_GARANTIA'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'TASA'},
			{name: 'MONTO_INT'},
			{name: 'MONTO_OPERAR'},
			{name: 'BENEFICIARIO'},
			{name: 'BANCO_BENEFICIARIO'},
			{name: 'SUCURSAL_BENEFICIARIO'},
			{name: 'CUENTA_BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMP_RECIBIR'},
			{name: 'NETO_RECIBIR'},
			{name: 'REFERENCIA_PYME'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesar07Data,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesar07Data(null, null, null);					
				}
			}
		}		
	});
	
	var grid07 = new Ext.grid.GridPanel({
		store: consulta07Data,
		id: 'grid07',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus:Operada - Operada Pendiente de Pago </div><div style="float:right"></div></div>',			
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Nombre PYME/Cedente',
				tooltip: 'Nombre PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'N�mero Solicitud',
				tooltip: 'N�mero Solicitud',
				dataIndex: 'NUM_SOLICITUD',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')
			},
			{
				header: 'Monto Int',
				tooltip: 'Monto Int',
				dataIndex: 'MONTO_INT',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPERAR',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia PyME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}			
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF07',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDF07
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF07',
					hidden: true
				}								
			]
		}
	});
	
//******************   2  Seleccionada PYME a Negociable ****************

	var procesarGenerarPDF02 =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF02');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF02');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesar02Data = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
	
		if (arrRegistros != null) {
			if (!grid02.isVisible()) {
				grid02.show();
			}				
			
			var jsonData = store.reader.jsonData;		
			var cm = grid02.getColumnModel();
			
			if(jsonData.bTipoFactoraje=='S') { 
				grid02.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);	
			}
			if (jsonData.bOperaFactorajeDist =='S' || jsonData.bOperaFactVencimientoInfonavit =='S' ){
				grid02.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				grid02.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				grid02.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				grid02.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				grid02.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				grid02.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR'), false);
				grid02.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);				
			}	
			
			var el = grid02.getGridEl();				
			if(store.getTotalCount() > 0) {	
				
				Ext.getCmp('btnGenerarPDF02').enable();				
				consuTotalesData.load({  params:{  ic_cambio_estatus: 2  }  });
				el.unmask();							
			} else {		
				Ext.getCmp('btnGenerarPDF02').disable();	
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var consulta02Data = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [			
			{name: 'NOMBRE_PYME'}	,
			{name: 'NOMBRE_EPO'}	,		
			{name: 'NUM_DOCTO'},
			{name: 'NUM_ACUSE_IF'},
			{name: 'MONEDA'},				
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_DOCTO'},
			{name: 'POR_DESC'},
			{name: 'REC_GARANTIA'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'TASA'},
			{name: 'MONTO_INT'},
			{name: 'MONTO_OPERAR'},
			{name: 'BENEFICIARIO'},
			{name: 'BANCO_BENEFICIARIO'},
			{name: 'SUCURSAL_BENEFICIARIO'},
			{name: 'CUENTA_BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMP_RECIBIR'},
			{name: 'NETO_RECIBIR'},
			{name: 'REFERENCIA_PYME'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesar02Data,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesar02Data(null, null, null);					
				}
			}
		}		
	});
	
	var grid02 = new Ext.grid.GridPanel({
		store: consulta02Data,
		id: 'grid02',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		title: '<div><div style="float:left">Estatus:Seleccionada PYME a Negociable </div><div style="float:right"></div></div>',			
		columns: [	
		{
				header: 'Nombre PYME/Cedente',
				tooltip: 'Nombre PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'N�mero Acuse IF',
				tooltip: 'N�mero Acuse IF',
				dataIndex: 'NUM_ACUSE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center'	
			},
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCTO',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESC',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex: 'REC_GARANTIA',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tasa',
				tooltip: 'Tasa',
				dataIndex: 'TASA',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0,0.00')
			},
			{
				header: 'Monto Int',
				tooltip: 'Monto Int',
				dataIndex: 'MONTO_INT',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex: 'MONTO_OPERAR',
				sortable: true,
				width: 130,				
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex: 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Sucursal Beneficiario',
				tooltip: 'Sucursal Beneficiario',
				dataIndex: 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: 'Cuenta Beneficiario',
				tooltip: 'Cuenta Beneficiario',
				dataIndex: 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'left'				
			},
			{
				header: '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario.',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMP_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Neto a Recibir PyME.',
				tooltip: 'Neto a Recibir PyME',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia PyME',
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}	
		],					 
     	stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,		
		frame: true,
		bbar: {
			items:[
				'->','-',	
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF02',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion: 'Consultar',
								operacion:'ArchivoPDF'
							}),
							callback: procesarGenerarPDF02
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF02',
					hidden: true
				}								
			]
		}
	});


	var procesarTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridTotales = Ext.getCmp('gridTotales');	
		
		
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}						
			//edito el titulo de la columna
			var cm = gridTotales.getColumnModel();			
			var jsonData = store.reader.jsonData;		
			var el = gridTotales.getGridEl();
		
			var ic_cambio_estatus = Ext.getCmp('ic_cambio_estatus1').getValue();			
			if(ic_cambio_estatus ==26  ){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_INT'), true);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_OPER'), true);
			}else if(ic_cambio_estatus ==32){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_INT'), false);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_OPER'), true);
			}else  {
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_INT'), false);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_OPER'), false);
			}
							
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consuTotalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte02.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'		
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'NUM_DOCTOS', type: 'float', mapping: 'NUM_DOCTOS'},			
			{name: 'TOTAL_MONTO_DOCTO', type: 'float', mapping: 'TOTAL_MONTO_DOCTO'},	
			{name: 'TOTAL_MONTO_DESC', type: 'float', mapping: 'TOTAL_MONTO_DESC'},	
			{name: 'TOTAL_MONTO_INT', type: 'float', mapping: 'TOTAL_MONTO_INT'},
			{name: 'TOTAL_MONTO_OPER', type: 'float', mapping: 'TOTAL_MONTO_OPER'}
			
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}		
	});
						
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consuTotalesData,	
		style: 'margin:0 auto;',
		title:'',
		hidden: true,		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'			
			},						
			{
				header: 'Num. Documentos',
				dataIndex: 'NUM_DOCTOS',
				width: 150,
				align: 'center'				
			},			
			{
				header: 'Total Monto Documento',
				dataIndex: 'TOTAL_MONTO_DOCTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto a Descontar',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto Int',
				dataIndex: 'TOTAL_MONTO_INT',
				width: 150,	
				hidden: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto a Operar',
				dataIndex: 'TOTAL_MONTO_OPER',
				width: 150,
				hidden: true,	
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		height: 100,
		width: 900,
		title: '',
		frame: false
	});
	
	
//******************Forma ****************

	function procesaValoresIniciales(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if(jsonValoresIniciales != null){	
				var strNombreUsuario = Ext.getCmp('strNombreUsuario');
				var fechaHora = Ext.getCmp('fechaHora');
				strNombreUsuario.getEl().update(jsonValoresIniciales.strNombreUsuario);
				fechaHora.getEl().update(jsonValoresIniciales.fechaHora);
	
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}

	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13reporte02.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {				
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_cambio_estatus',
			id: 'ic_cambio_estatus1',
			fieldLabel: 'Estatus',
			emptyText: 'Seleccione...',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_cambio_estatus',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEstatus,
			listeners: {
				select: {
					fn: function(combo){
										
						Ext.getCmp('btnGenerarPDF02').enable();			
						Ext.getCmp('btnBajarPDF02').hide();
						Ext.getCmp('btnGenerarPDF07').enable();			
						Ext.getCmp('btnBajarPDF07').hide();
						Ext.getCmp('btnGenerarPDF26').enable();			
						Ext.getCmp('btnBajarPDF26').hide();						
						Ext.getCmp('btnGenerarPDF32').enable();			
						Ext.getCmp('btnBajarPDF32').hide();
						
						Ext.getCmp('gridTotales').hide();
						Ext.getCmp('grid02').hide();
						Ext.getCmp('grid07').hide();
						Ext.getCmp('grid26').hide();
						Ext.getCmp('grid32').hide();
												
						
						if(combo.getValue() ==2) {		
							consulta02Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});					
						}
						
						if(combo.getValue() ==7) {		
							consulta07Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});					
						}
						if(combo.getValue() ==26) {		
							consulta26Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});					
						}
						
						if(combo.getValue() ==32) {		
							consulta32Data.load({
								params: Ext.apply(fp.getForm().getValues(),{
									operacion: 'Generar'							
								})
							});					
						}
						
						
					}
				}
			}
		},
		{
			xtype: 'label',
			id: 'strNombreUsuario',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Usuario',
			text: '--'
		},
		{
			xtype: 'label',
			id: 'fechaHora',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Fecha',
			text: '--'
		}	
	];
	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: 'margin:0 auto;',
		frame: true,
		border: false,
		title: '<div><center>Cambios de Estatus</div>',
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		hidden: false,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true
	});
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		style: 'margin:0 auto;',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid02,
			grid07,
			grid26,
			grid32, 
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});
	
	
	var valIniciales = function(){
		Ext.Ajax.request({
			url: '13reporte02.data.jsp',
			params: {
						informacion: "valoresIniciales"
			},
			callback: procesaValoresIniciales
		});
	}
	
	valIniciales();
	
	catalogoEstatus.load();
	
});