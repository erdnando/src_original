<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.text.*,
	java.sql.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>  
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_epo = (request.getParameter("ic_epo") != null) ? request.getParameter("ic_epo") : "";
String num_electronico = (request.getParameter("num_electronico") != null) ? request.getParameter("num_electronico") : "";
String ic_estatus_solic = (request.getParameter("ic_estatus_solic") != null) ? request.getParameter("ic_estatus_solic") : "";
String ic_moneda = (request.getParameter("ic_moneda") != null) ? request.getParameter("ic_moneda") : "";
String ic_folio = (request.getParameter("ic_folio") != null) ? request.getParameter("ic_folio") : "";
String tipoTasa = (request.getParameter("tipoTasa") != null) ? request.getParameter("tipoTasa") : "";
String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") != null) ? request.getParameter("df_fecha_solicitudMin") : "";
String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") != null) ? request.getParameter("df_fecha_solicitudMax") : "";
String df_fecha_vencMin = (request.getParameter("df_fecha_vencMin") != null) ? request.getParameter("df_fecha_vencMin") : "";
String df_fecha_vencMax = (request.getParameter("df_fecha_vencMax") != null) ? request.getParameter("df_fecha_vencMax") : ""; 
String tipoFactoraje = (request.getParameter("tipoFactoraje") != null) ? request.getParameter("tipoFactoraje") : "";
String ordenaEpo = (request.getParameter("ordenaEpo") != null) ? request.getParameter("ordenaEpo") : "";
String ordenaPyme = (request.getParameter("ordenaPyme") != null) ? request.getParameter("ordenaPyme") : "";
String ordenaFecha = (request.getParameter("ordenaFecha") != null) ? request.getParameter("ordenaFecha") : "";
String ordenaMonto = (request.getParameter("ordenaMonto") != null) ? request.getParameter("ordenaMonto") : "";
String ordenaEstatus = (request.getParameter("ordenaEstatus") != null) ? request.getParameter("ordenaEstatus") : "";
String ordenaFolio = (request.getParameter("ordenaFolio") != null) ? request.getParameter("ordenaFolio") : "";
String fn_montoMin = (request.getParameter("fn_montoMin") != null) ? request.getParameter("fn_montoMin") : "";
String fn_montoMax = (request.getParameter("fn_montoMax") != null) ? request.getParameter("fn_montoMax") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String tipoArchivo = (request.getParameter("tipoArchivo") != null) ? request.getParameter("tipoArchivo") : "";
String extensionArchivo = (request.getParameter("extensionArchivo") != null) ? request.getParameter("extensionArchivo") : "";
String clavePyme = (request.getParameter("clavePyme") != null) ? request.getParameter("clavePyme") : "";
String claveDocumento = (request.getParameter("claveDocumento") != null) ? request.getParameter("claveDocumento") : "";
String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String  ic_if =iNoCliente,  infoRegresar ="",ic_pyme ="", txtNombre ="", consulta  ="",bOperaFactorajeDist2 = "N",
bOperaFactorajeVencINFO2 ="N",  bTipoFactoraje2 ="N", bOperaPubHash ="N", bOperaNC="N";
String aplicaFloating = (request.getParameter("aplicaFloating") != null) ? request.getParameter("aplicaFloating") : "";
if (!"P".equals(tipoTasa)  ||  "A".equals(aplicaFloating)  ) {
	aplicaFloating = "";
}
List  tipoFactCombo =new ArrayList();
int numRegistros =0,  start= 0, limit =0;
boolean bOperaFactorajeVencido = false, bOperaFactorajeVencidoGral = false, bOperaFactorajeDist = false,  bOperaFactorajeDistGral = false, 
bOperaFactorajeMandatoGral = false, bOperaFactorajeAutomaticoGral=false,  bOperaFactConMandato = false, bTipoFactoraje = false,  bOperaFactorajeVencINFO = false, bOperaFactorajeAutomatico=false,
bOperaFactorajeVencINFOGral = false, bOperaFactorajeAutINFOGral=false;
JSONObject 	jsonObj	= new JSONObject();
HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();


ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

String sOperaFactConMandato = new String("");
Hashtable alParamEPO = new Hashtable();

if(!ic_epo.equals("")) {
	alParamEPO = BeanParamDscto.getParametrosEPO(ic_epo, 1);
	if (alParamEPO!=null) {
		bOperaFactConMandato   		= ("S".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?true:false;
		bOperaFactorajeVencido 		= ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?true:false;
		bOperaFactorajeAutomatico	= ("S".equals(alParamEPO.get("FACTORAJE_IF").toString()))?true:false;
		bOperaFactorajeDist   		= ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?true:false;
		bOperaFactorajeVencINFO		= ("S".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?true:false;	
	}		
}

	sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";	
	Hashtable alParamIF = new Hashtable();
	alParamIF = BeanParamDscto.parametrizacion( ic_if, ic_epo );
	
	bOperaFactorajeAutomaticoGral 	= ("S".equals(alParamIF.get("cs_factoraje_automatico").toString()))?true:false;
	bOperaFactorajeVencidoGral 	= ("S".equals(alParamIF.get("cs_factoraje_vencido").toString()))?true:false;
	bOperaFactorajeDistGral   	= ("S".equals(alParamIF.get("cs_factoraje_distribuido").toString()))?true:false;
	bOperaFactorajeMandatoGral = ("S".equals(alParamIF.get("PUB_EPO_OPERA_MANDATO").toString()))?true:false;
	bOperaFactorajeVencINFOGral = ("S".equals(alParamIF.get("PUB_EPO_VENC_INFONAVIT").toString()))?true:false;
	bOperaPubHash = alParamIF.get("CS_PUB_hash").toString();
	bOperaNC = alParamIF.get("bOperaNC").toString();

if(bOperaFactorajeAutomaticoGral || bOperaFactorajeVencidoGral || bOperaFactorajeDistGral || bOperaFactorajeMandatoGral || bOperaFactorajeVencINFOGral){
	bOperaFactorajeVencido = false;
   bOperaFactorajeAutomatico = false;
	if("S".equals(sOperaFactConMandato)){
		bOperaFactorajeVencido = true;
      bOperaFactorajeAutomatico=true;
	}
	
	if(ic_epo.equals("")) {   
		if(bOperaFactorajeAutomaticoGral) { bOperaFactorajeAutomatico = true; }
		if(bOperaFactorajeVencidoGral) { bOperaFactorajeVencido = true; }
		if(bOperaFactorajeDistGral) { bOperaFactorajeDist = true; }
		if(bOperaFactorajeMandatoGral) { bOperaFactConMandato = true;  }
		if(bOperaFactorajeVencINFOGral) {	bOperaFactorajeVencINFO = true; }
	}

	bTipoFactoraje = (bOperaFactorajeAutomatico || bOperaFactorajeVencido || bOperaFactorajeDist || bOperaFactConMandato || bOperaFactorajeVencINFO)?true:false;
	tipoFactCombo.add("N"); // el tipo de Factoraje normal aparecera por default
	if(bOperaFactorajeDist){ tipoFactCombo.add("D"); }
	if(bOperaFactConMandato){ tipoFactCombo.add("M"); }
	if(bOperaFactorajeVencido){ tipoFactCombo.add("V"); }
	if(bOperaFactorajeVencINFO){ tipoFactCombo.add("I"); }
   if(bOperaFactorajeAutomatico){ tipoFactCombo.add("A");}
}
if(bTipoFactoraje) { bTipoFactoraje2= "S"; }
if(bOperaFactorajeDist) { bOperaFactorajeDist2= "S"; }
if(bOperaFactorajeVencINFO) { bOperaFactorajeVencINFO2= "S"; }

if(ic_epo.equals("")) {  
	bOperaNC =	parambOperaNC(ic_if);
}

if (informacion.equals("valoresIniciales")  ) {

	String  limitarRangoFechas =	getLimitarRangoFechas();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("fechaHoy", fechaHoy);
	jsonObj.put("limitarRangoFechas", limitarRangoFechas);
	jsonObj.put("bOperaPubHash", bOperaPubHash);	
	infoRegresar = jsonObj.toString();	
	
}  else  if (informacion.equals("catalogoEPO")  ) {

	List resultCatEpo = new ArrayList();
	CatalogoEPO catalogo = new CatalogoEPO();	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");	
	catalogo.setClaveIf(ic_if);
	resultCatEpo	= catalogo.getListaElementosNotificaciones();
	
	if(resultCatEpo.size() >0 ){	
		for (int i= 0; i < resultCatEpo.size(); i++) {			
			String registro =  resultCatEpo.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + resultCatEpo.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	

	
} else if (informacion.equals("busquedaAvanzada") ) {

	String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
	String noPyme = request.getParameter("noPyme")==null?"":request.getParameter("noPyme");
		
	CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion(" crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");	
	if(!rfcPyme.equals(""))
	cat.setRfc_pyme(rfcPyme);
	if(!noPyme.equals(""))
	cat.setIc_pyme(noPyme);	
	if(!nombrePyme.equals(""))
	cat.setNombre_pyme(nombrePyme);
	if(!ic_epo.equals(""))
	cat.setIc_epo(ic_epo);
	cat.setPantalla("SoliAfi");		
	cat.setOrden("p.cg_razon_social");		
	infoRegresar = cat.getJSONElementos();
	
}  else if (informacion.equals("pymeNombre")  ) {
	
	List datosPymes =  BeanParamDscto.datosPymes(num_electronico, ic_epo ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	jsonObj.put("ic_pyme", ic_pyme);
	infoRegresar = jsonObj.toString();		

}else if (informacion.equals("catalogoEstatus")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_estatus_solic");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_estatus_solic");		
	catalogo.setOrden("cd_descripcion");
	catalogo.setValoresCondicionNotIn("7,8,9", Integer.class);  
	infoRegresar = catalogo.getJSONElementos();	
	
	
}else if (informacion.equals("catalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("cd_nombre");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
	
}else if (informacion.equals("catalogoTipoFactoraje") ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("cc_tipo_factoraje");
	catalogo.setCampoDescripcion("cg_nombre");
	catalogo.setTabla("comcat_tipo_factoraje");		
	catalogo.setOrden("cc_tipo_factoraje");		
	catalogo.setValoresCondicionIn(tipoFactCombo);	
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("Consultar") 
|| informacion.equals("ConsultarTotales")  ||  informacion.equals("ConsultarTotalesC") 
|| informacion.equals("ArchivoCSV_TXT")  || informacion.equals("ArchivoPDFTodo") || informacion.equals("ArchivoPDFxPagina")  ) {

	String campos[]={"EPO_RELACIONADA","NOMBRE_PYME","fechavencdoc","MONTO_DOCTO","ESTATUS","FOLIO_SOLIC"};
	String orden[]={ordenaEpo,ordenaPyme,ordenaFecha,ordenaMonto,ordenaEstatus,ordenaFolio};
	String criterioOrdenamiento=Comunes.ordenarCampos(orden,campos);
	
	ConsSolicIfDE paginador = new ConsSolicIfDE();
	if(!num_electronico.equals(""))  {
		List datosPymes =  BeanParamDscto.datosPymes(num_electronico, ic_epo ); //para obtener 
		if(datosPymes.size()>0){
			ic_pyme= (String)datosPymes.get(0);
			txtNombre= (String)datosPymes.get(1);
		}
	}
	
	paginador.setIc_if(ic_if);	
	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_folio(ic_folio);	
	paginador.setDf_fecha_solicitudMin(df_fecha_solicitudMin);
	paginador.setDf_fecha_solicitudMax(df_fecha_solicitudMax);
	paginador.setDf_fecha_vencMin(df_fecha_vencMin);
	paginador.setDf_fecha_vencMax(df_fecha_vencMax);
	paginador.setIc_estatus_solic(ic_estatus_solic);
	paginador.setIc_moneda(ic_moneda);
	paginador.setIc_estatus_solic(ic_estatus_solic); 
	paginador.setFn_montoMin(fn_montoMin);
	paginador.setFn_montoMax(fn_montoMax);
	paginador.setTipoFactoraje(tipoFactoraje);
	paginador.setTipoTasa(tipoTasa);  
	// para la generación de los archivos  
	paginador.setTipoArchivo(tipoArchivo);  	
	paginador.setBTipoFactoraje(bTipoFactoraje2); 
	paginador.setBOperaFactorajeDist(bOperaFactorajeDist2); 
	paginador.setBOperaFactorajeVencINFO(bOperaFactorajeVencINFO2);    
	paginador.setBOperaPubHash(bOperaPubHash);  	
	paginador.setOrdenadoBy(criterioOrdenamiento); 		
	paginador.setAplicaFloating(aplicaFloating);
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if (informacion.equals("Consultar")  ) {	
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
				
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);										
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
		
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("bTipoFactoraje",bTipoFactoraje2);		
		jsonObj.put("bOperaFactorajeDist",bOperaFactorajeDist2);
		jsonObj.put("bOperaFactorajeVencINFO",bOperaFactorajeVencINFO2);	
		jsonObj.put("bOperaNC",bOperaNC);
		jsonObj.put("bOperaPubHash", bOperaPubHash);	
		infoRegresar = jsonObj.toString();
		
	}else if (informacion.equals("ConsultarTotales")) {		//Datos para el Resumen de Totales
		//Debe ser llamado despues de Consulta
		queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion	
	
	}else if (informacion.equals("ConsultarTotalesC")) {		//Datos para el Resumen de Totales
		//Debe ser llamado despues de Consulta
		queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion	
	
	}else if (informacion.equals("ArchivoCSV_TXT")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, extensionArchivo);
			String nombreArchivoMD5 = nombreArchivo.substring(0, nombreArchivo.lastIndexOf(".")) + "_MD5.txt";
			String strDirBase = strDirecVirtualTemp.replaceAll("^\\/nafin\\/","/");  //Se elimina /nafin para enviar la ruta al Servlet de descarga
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirBase+nombreArchivo);
			jsonObj.put("urlArchivoMD5", strDirBase+nombreArchivoMD5);
			infoRegresar = jsonObj.toString();
					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}				
	}else if (informacion.equals("ArchivoPDFxPagina")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
			String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");			
				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF por Pagina", e);
		}
	} else if (informacion.equals("ArchivoPDFTodo")) {
		try {
			//start = 0;
			//limit = 15;
			//String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(true));
			throw new AppException("Error al generar el archivo PDF por Pagina", e);
		}
		infoRegresar = jsonObj.toString();
	}
}
%>
<%=infoRegresar%>

<%!



/**
 * Determina si es necesario limitar el rango de fechas que se especifique como
 * criterios de búsqueda, con el fin de evitar consultas demasiado pesadas en
 * horario de alta demanda.
 * @return Si la hora actual es menor a las 15 h, regresa true
 * 		false de lo contrario.
 */
private String getLimitarRangoFechas() {
	SimpleDateFormat sdf = new SimpleDateFormat("HH");	//HH es formato 24 horas.
	String hora = sdf.format(new java.util.Date());
	int iHora = Integer.parseInt(hora);
	if (iHora < 15) {
		return "S";
	} else {
		return "N";
	}
}

%>
<%!

	public String parambOperaNC(String  ic_if ){
		
		String 	qrySentencia 	= null;
		PreparedStatement 	ps	= null;
		ResultSet	rs	= null;
		AccesoDB con =new AccesoDB();
		String  bOperaNC="N";

		try{
			con.conexionDB();

			 qrySentencia= " SELECT count(*) " +
							  " FROM comcat_epo e, comrel_if_epo ie, comrel_producto_epo cpe, comcat_if i " +
							  " WHERE e.ic_epo = ie.ic_epo " +
							  " AND ie.ic_epo = cpe.ic_epo " +
							  "	AND i.ic_if = ie.ic_if " +
							"	AND i.ic_if = ?  " +
							"	AND UPPER (ie.cs_vobo_nafin) = 'S' " +
							"	AND e.cs_habilitado = 'S' " +
							"	AND cpe.ic_producto_nafin = 1  " +
							"	AND  cpe.cs_opera_notas_cred=  'S' ";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_if);
				
				rs = ps.executeQuery();
				if (rs.next()) {  
					if(rs.getInt(1)>=0) {
					bOperaNC = "S";
					}
				}
				rs.close();
				ps.close();
			
		} catch(Exception e) {

			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
			return bOperaNC;
		}
%>
