Ext.onReady(function() {
	
	
	var sTipoBanco =  Ext.getDom('sTipoBanco').value;	
/*----------------------------------------- Hanlers -----------------------------------------*/
		
	var procesarConsultaDataNB = function(store, arrRegistros, opts) 
	{
		var fp = Ext.getCmp('forma');
		pnl.el.unmask();	
		/*si el arrRegistros es distinto de nulo*/
		if (arrRegistros != null )// && arrRegistros!= '') 
		{
			var el = gridNB.getGridEl();
			
			/*si el grid no es visible, lo muestro*/
			if (!gridNB.isVisible()) 
			{
				gridNB.show();
			}
			
			/*declaro botones y una variable para manipular el grid*/
			var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivoNB");
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDFNB'); 
			var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivoNB');
			var btnBajarPDF 		 = Ext.getCmp('btnBajarPDFNB');
			
			btnBajarArchivo.hide();
			btnBajarPDF.hide();
			
			if (store.getTotalCount() > 0 )
			{	
				el.unmask();
				consultaDataTotales.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultarTotales'})  });
				
				gridTotales.show();
			}
			/*Si el registro viene vacio desactiva los botones para generar archivo*/
			
			if (arrRegistros == '')  
			{
				gridNB.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				btnGenerarArchivo.disable();
				btnImprimirPDF.disable();
			}
			
			/* Sino activa los botones para generar archivo */
			//else if (arrRegistros != null)
			else if (arrRegistros != '')
			{
				btnGenerarArchivo.enable();
				btnImprimirPDF.enable();
			}
		}	
	}
	
	var procesarConsultaDataB = function(store, arrRegistros, opts) 
	{
		var fp = Ext.getCmp('forma');
		pnl.el.unmask();	
		/*si el arrRegistros es distinto de nulo*/
		if (arrRegistros != null )// && arrRegistros!= '') 
		{
			var el = gridB.getGridEl();
			
			/*si el grid no es visible, lo muestro*/
			if (!gridB.isVisible()) 
			{
				gridB.show();
			}
			
			/*declaro botones y una variable para manipular el grid*/
			var btnGenerarArchivo = Ext.getCmp("btnGenerarArchivoB");
			var btnImprimirPDF 	 = Ext.getCmp('btnImprimirPDFB'); 
			var btnBajarArchivo 	 = Ext.getCmp('btnBajarArchivoB');
			var btnBajarPDF 		 = Ext.getCmp('btnBajarPDFB');
			
			btnBajarArchivo.hide();
			btnBajarPDF.hide();
			
			if (store.getTotalCount() > 0 )
			{	
				el.unmask();
				consultaDataTotales.load({ params: Ext.apply(fp.getForm().getValues(), {informacion: 'ConsultarTotales'})  });
				
				gridTotales.show();
			}
			/*Si el registro viene vacio desactiva los botones para generar archivo*/
			
			if (arrRegistros == '')  
			{
				gridB.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				btnGenerarArchivo.disable();
				btnImprimirPDF.disable();
			}
			
			/* Sino activa los botones para generar archivo */
			//else if (arrRegistros != null)
			else if (arrRegistros != '')
			{
				btnGenerarArchivo.enable();
				btnImprimirPDF.enable();
			}
		}	
	}
	function leeRespuesta(){
		window.location = '13forma012ext.jsp';
	}
	
	var procesarSuccessFailureGenerarPDFNB = function(opts, success, response) 
   {
		var btnBajarPDF = Ext.getCmp('btnBajarPDFNB');
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDFNB');
		btnBajarPDF.setIconClass('icoPdf');
		btnImprimirPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPDFB = function(opts, success, response) 
   {
		var btnBajarPDF = Ext.getCmp('btnBajarPDFB');
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDFB');
		btnBajarPDF.setIconClass('icoPdf');
		btnImprimirPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarCSVNB = function(opts, success, response) 
   {
		var btnBajarArchivoNB = Ext.getCmp('btnBajarArchivoNB');
		var btnGenerarArchivoNB = Ext.getCmp('btnGenerarArchivoNB');
		btnBajarArchivoNB.setIconClass('icoXls');
		btnGenerarArchivoNB.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{

			btnBajarArchivoNB.show();
			btnBajarArchivoNB.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivoNB.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnGenerarArchivoNB.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarCSVB = function(opts, success, response) 
   {
		var btnBajarArchivoB = Ext.getCmp('btnBajarArchivoB');
		var btnGenerarArchivoB = Ext.getCmp('btnGenerarArchivoB');
		btnBajarArchivoB.setIconClass('icoXls');
		btnGenerarArchivoB.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{

			btnBajarArchivoB.show();
			btnBajarArchivoB.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivoB.setHandler( function(boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} 
		else 
		{
			btnGenerarArchivoB.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
/*/----------------------------------------- Store's -----------------------------------------*/
	/****** Store�s Grid�s *******/	
	var consultaDataNB   = new Ext.data.GroupingStore({		
		root:'registros',	
		url:'13forma012ext.data.jsp',	
		baseParams:{informacion:'ConsultaGrid'},	
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',		
				fields: [	
					{name: 'CVE_ENCABEZADO_PAGO'},
					{name: 'CVE_IF'},
					{name: 'CVE_SUCURSAL'},
					{name: 'CVE_MONEDA'},
					{name: 'NOMBRE_BANCO'},
					{name: 'FECHA_PERIODO_FIN'},
					{name: 'FECHA_PROB_PAGO'},
					{name: 'FECHA_DEPOSITO'},
					{name: 'IMPORTE_DEPOSITO'},
					{name: 'REFERENCIA_BANCO'},
					{name: 'REFERENCIA_INTER'},
					
					{name: 'SUBAPLICACION'},
					{name: 'PRESTAMO'},
					{name: 'CLIENTE_SIRAC'},
					{name: 'TOTAL_AMORTIZACION'},
					{name: 'INTERES'},
					{name: 'INTERES_MORA'},
					{name: 'SUBSIDIO'},
					{name: 'COMISION'},
					{name: 'IVA'},
					{name: 'TOTAL_EXIGIBLE'},
					{name: 'CONCEPTO_PAGO'},
					{name: 'ORIGEN_PAGO' }
					
					]
				}),
		autoLoad: false,
		groupField: 'CVE_ENCABEZADO_PAGO',
		sortInfo:{ field: 'CVE_ENCABEZADO_PAGO', direction: "ASC"},
		listeners: 
		{
			load: procesarConsultaDataNB,
			exception: NE.util.mostrarDataProxyError
		}
		
	});
	
		var consultaDataB   = new Ext.data.GroupingStore({		
		root:'registros',	
		url:'13forma012ext.data.jsp',	
		baseParams:{informacion:'ConsultaGrid'},	
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',		
				fields: [	
					{name: 'CVE_ENCABEZADO_PAGO'},
					{name: 'CVE_IF'},
					{name: 'CVE_SUCURSAL'},
					{name: 'CVE_MONEDA'},
					{name: 'NOMBRE_BANCO'},
					{name: 'FECHA_PROB_PAGO'},
					{name: 'FECHA_DEPOSITO'},
					{name: 'IMPORTE_DEPOSITO'},
					{name: 'REFERENCIA_BANCO'},
					{name: 'REFERENCIA_INTER'},
					
					{name: 'SUCURSAL'},
					{name: 'SUBAPLICACION'},
					{name: 'PRESTAMO'},
					{name: 'ACREDITADO'},
					{name: 'FECHA_OPERACION'},
					{name: 'FECHA_VENCIMIENTO'},
					{name: 'FECHA_PAGO'},
					{name: 'SALDO_INSOLUTO'},
					{name: 'TASA'},
					{name: 'TOTAL_AMORTIZACION'},
					{name: 'DIAS'},
					{name: 'INTERES'},
					{name: 'COMISION'},
					{name: 'IVA'},
					{name: 'TOTAL_EXIGIBLE'}
					
					]
				}),
		autoLoad: false,
		groupField: 'CVE_ENCABEZADO_PAGO',
		sortInfo:{ field: 'CVE_ENCABEZADO_PAGO', direction: "ASC"},
		listeners: 
		{
			load: procesarConsultaDataB,
			exception: NE.util.mostrarDataProxyError
		}
		
	});
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma012ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},
		hidden: true,
		fields: [	
			{name: 'TOTAL_PAGOS'},			
			{name: 'TOTAL_IMPORTE'}
							
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	

	/****** End Store�s Grid�s *******/

/*------------------------------------------ Grids ------------------------------------------*/

	var gruposAcuseNB = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Encabezado', colspan: 11, align: 'center'},
					{header: 'Detalle', colspan: 12, align: 'center'}					
				]
			]
	});	

	var gruposAcuseB = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: 'Encabezado', colspan: 10, align: 'center'},
					{header: 'Detalle', colspan: 15, align: 'center'}					
				]
			]
	});	

	

		var gridNB = new Ext.grid.EditorGridPanel({
		title:'',
		id: 'gridNB',
		store: consultaDataNB,
		plugins: gruposAcuseNB,
		//margins: '20 0 0 0',
		stripeRows: true,
		loadMask:false,
		hidden: true,
		height:300,
		width:600,			///////�!
		frame:true, 
		header:true,
		
					
		columns: 
		[
			
			{
				header:		'Encabezado',
				tooltip:		'Encabezado',			
				dataIndex:	'CVE_ENCABEZADO_PAGO',	
				hidden: true,
				sortable:true,	
				resizable:true,	
				width:50
				
			},
		
			{
				header:		'Clave del I.F',
				tooltip:		'Clave del I.F',			
				dataIndex:	'CVE_IF',	
				sortable:true,	
				resizable:true,	
				width:50
				
			},
			{
				header:		'Clave direcci�n estatal',
				tooltip: 	'Clave direcci�n estatal',
				dataIndex:	'CVE_SUCURSAL',	
				sortable:true,
				resizable:true,	
				width:50, 
				align:"center"
				
			},
			{
				header:		'Clave moneda', 
				tooltip: 	'Clave moneda',	
				dataIndex: 	'CVE_MONEDA',	
				sortable:true, 
				width:50, 	
				align:"center"
				
			},
			{
				header:		'Banco de servicio',
				tooltip:		'Banco de servicio',
				dataIndex:	'NOMBRE_BANCO',		
				sortable:true, 
				width:120, 
				align:"center"
				
			},
			{
				header:		'Fecha de vencimiento',
				tooltip:		'Fecha de vencimiento',	
				dataIndex:  'FECHA_PERIODO_FIN',	
				sortable:true,	
				width:100,	
				align:"center"
				
			},
			{
				header:		'Fecha probable de pago', 		
				tooltip:		'Fecha probable de pago',			
				dataIndex:  'FECHA_PROB_PAGO',		
				sortable:true,	
				width:100,	
				align:"center"
				
			},
			{
				header:		'Fecha de dep�sito',
				tooltip:		'Fecha de dep�sito',	
				dataIndex:	'FECHA_DEPOSITO',		
				sortable:true,	
				width:100,	
				align:'center' //
				
			},
			
			{
				header:		'Importe de dep�sito',
				tooltip: 	'Importe de dep�sito',
				dataIndex:	'IMPORTE_DEPOSITO',	
				sortable:true,
				resizable:true,	
				width:100, 
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
				
			},
			{
				header:		'Referencia Banco', 
				tooltip: 	'Referencia Banco',	
				dataIndex: 	'REFERENCIA_BANCO',	
				sortable:true, 
				width:100, 	
				align:'center'
				
			},
			{
				header:		'Referencia <br>Intermediario',
				tooltip:		'Referencia Intermediario',
				dataIndex:	'REFERENCIA_INTER',		
				sortable:true, 
				width:100, 
				align:'center'
				
			},
			
			//////////////////////DETALLES//////////////////////////
			
			{
				header:		'Subaplicaci�n', 		
				tooltip:		'Subaplicaci�n',			
				dataIndex:  'SUBAPLICACION',		
				sortable:true,	
				width:50,	
				align:'center'
				
			},
			{
				header:		'Pr�stamo',
				tooltip:		'Pr�stamo',	
				dataIndex:	'PRESTAMO',		
				sortable:true,	
				width:50,	
				align:'center' //
				
			},
			
			{	//NB
				header:		'Clave SIRAC Cliente',
				tooltip: 	'Clave SIRAC Cliente',
				dataIndex:	'CLIENTE_SIRAC',	
				sortable:true,
				resizable:true,	
				width:50, 
				align:'center'
				
			},
			
			{
				header:		'Capital', 
				tooltip: 	'Capital',	
				dataIndex: 	'TOTAL_AMORTIZACION',	
				sortable:true, 
				width:70, 	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			
			{	//B
				header:		'Intereses',
				tooltip:		'Intereses',	
				dataIndex:  'INTERES',	
				sortable:true,	
				width:70,	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			{	//NB
				header:		'Moratorios', 		
				tooltip:		'Moratorios',			
				dataIndex:  'INTERES_MORA',		
				sortable:true,	
				width:70,
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			{	//NB
				header:		'Subsidio',
				tooltip:		'Subsidio',	
				dataIndex:	'SUBSIDIO',		
				sortable:true,	
				width:70,	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			{
				header:		'Comisi�n', 
				tooltip: 	'Comisi�n',	
				dataIndex: 	'COMISION',	
				sortable:true, 
				width:70, 	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			{	
				header:		'IVA',
				tooltip:		'IVA',
				dataIndex:	'IVA',		
				sortable:true, 
				width:70, 
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			{	
				header:		'Importe Pago',
				tooltip:		'Importe Pago',	
				dataIndex:  'TOTAL_EXIGIBLE',	
				sortable:true,	
				width:100,	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			{	//NB
				header:		'Concepto Pago', 		
				tooltip:		'Concepto Pago',			
				dataIndex:  'CONCEPTO_PAGO',		
				sortable:true,	
				width:100,	
				align:'center'
				
			},
			{	//NB
				header:		'Origen Pago',
				tooltip:		'Origen Pago',	
				dataIndex:	'ORIGEN_PAGO',		
				sortable:true,	
				width:100,	
				align:'center' //
				
			}
		],
		view: new Ext.grid.GroupingView({
            forceFit:false,
            groupTextTpl: '{text}'					 
        }),
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true,
		
		
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacionNB',
			displayInfo: true,
			store: consultaDataNB,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivoNB',
					handler: function(boton, evento) 
					{
						boton.disable();
						boton.setIconClass('loading-indicator');
						totalProperty:'total',
						Ext.Ajax.request
						({
							url: '13forma012ext.data.jsp',	
							params: Ext.apply (fp.getForm().getValues(),{					 
							informacion: 'GenerarCSV'
								
							}),
							callback: procesarSuccessFailureGenerarCSVNB
						});
					}
				},
			  
				{
					xtype: 'button',
					text: 'Bajar Archivo ',
					id: 'btnBajarArchivoNB',
					width: 10,
					hidden: true
				},
				'-',
				{
				
					xtype: 'button',
					text: 'Imprimir PDF',	
					id: 'btnImprimirPDFNB',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						totalProperty:'total',
						Ext.Ajax.request({
							url: '13forma012ext.data.jsp', 
							//params: fp.getForm().getValues(),{
							params: Ext.apply (fp.getForm().getValues(),{
								informacion: 'GenerarPDF'
								
								}),
								
							callback: procesarSuccessFailureGenerarPDFNB
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF ',
					id: 'btnBajarPDFNB',
					width: 5, 
					hidden: true
				}
				
			]
		}
		
	});
	
	var gridB = new Ext.grid.EditorGridPanel({
		title:'',
		id: 'gridB',
		store: consultaDataB,
		plugins: gruposAcuseB,
		//margins: '20 0 0 0',
		stripeRows: true,
		loadMask:false,
		hidden: true,
		height:300,
		width:600,			///////�!
		frame:true, 
		header:true,
		
					
		columns: 
		[
			
			{
				header:		'Encabezado',
				tooltip:		'Encabezado',			
				dataIndex:	'CVE_ENCABEZADO_PAGO',	
				hidden: true,
				sortable:true,	
				resizable:true,	
				width:50
				
			},
		
			{
				header:		'Clave<br> del I.F',
				tooltip:		'Clave del I.F',			
				dataIndex:	'CVE_IF',	
				sortable:true,	
				resizable:true,	
				width:50
				
			},
			{
				header:		'Clave <br> direcci�n <br>estatal',
				tooltip: 	'Clave direcci�n estatal',
				dataIndex:	'CVE_SUCURSAL',	
				sortable:true,
				resizable:true,	
				width:50, 
				align:"center"
				
			},
			{
				header:		'Clave <br> moneda', 
				tooltip: 	'Clave moneda',	
				dataIndex: 	'CVE_MONEDA',	
				sortable:true, 
				width:50, 	
				align:"center"
				
			},
			{
				header:		'Banco de servicio',
				tooltip:		'Banco de servicio',
				dataIndex:	'NOMBRE_BANCO',		
				sortable:true, 
				width:120, 
				align:"center"
				
			},
			
			{
				header:		'Fecha probable de pago', 		
				tooltip:		'Fecha probable de pago',			
				dataIndex:  'FECHA_PROB_PAGO',		
				sortable:true,	
				width:100,	
				align:"center"
				
			},
			{
				header:		'Fecha de dep�sito',
				tooltip:		'Fecha de dep�sito',	
				dataIndex:	'FECHA_DEPOSITO',		
				sortable:true,	
				width:100,	
				align:'center' //
				
			},
			
			{
				header:		'Importe de dep�sito',
				tooltip: 	'Importe de dep�sito',
				dataIndex:	'IMPORTE_DEPOSITO',	
				sortable:true,
				resizable:true,	
				width:100, 
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
				
			},
			{
				header:		'Referencia Banco', 
				tooltip: 	'Referencia Banco',	
				dataIndex: 	'REFERENCIA_BANCO',	
				sortable:true, 
				width:100, 	
				align:'center'
				
			},
			{
				header:		'Referencia <br> Intermediario',
				tooltip:		'Referencia Intermediario',
				dataIndex:	'REFERENCIA_INTER',		
				sortable:true, 
				width:100, 
				align:'center'
				
			},
			
			//////////////////////DETALLES//////////////////////////
			{
				header:		'Sucursal', 		
				tooltip:		'Sucursal',			
				dataIndex:  'SUCURSAL',		
				sortable:true,	
				width:50,	
				align:'center'
				
			},
			{
				header:		'Subaplicaci�n', 		
				tooltip:		'Subaplicaci�n',			
				dataIndex:  'SUBAPLICACION',		
				sortable:true,	
				width:50,	
				align:'center'
				
			},
			{
				header:		'Pr�stamo',
				tooltip:		'Pr�stamo',	
				dataIndex:	'PRESTAMO',		
				sortable:true,	
				width:50,	
				align:'center' //
				
			}
			,
			
			{	
				header:		'Acreditado',
				tooltip: 	'Acreditado',
				dataIndex:	'ACREDITADO',	
				sortable:true,
				resizable:true,	
				width:50, 
				align:'center'
				
			},
			{
				header:		'Fecha Operaci�n',
				tooltip:		'Fecha Operaci�n',	
				dataIndex:	'FECHA_OPERACION',		
				sortable:true,	
				width:100,	
				align:'center' //
				
			},
			
			{
				header:		'Fecha Vencimiento',
				tooltip:		'Fecha Vencimiento',	
				dataIndex:	'FECHA_VENCIMIENTO',		
				sortable:true,	
				width:100,	
				align:'center' //
				
			},
			{
				header:		'Fecha Pago',
				tooltip:		'Fecha Pago',	
				dataIndex:	'FECHA_PAGO',		
				sortable:true,	
				width:100,	
				align:'center' //
				
			},
			
			{
				header:		'Saldo Insoluto', 
				tooltip: 	'Saldo Insoluto',	
				dataIndex: 	'SALDO_INSOLUTO',	
				sortable:true, 
				width:70, 	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			{	
				header:		'Tasa',
				tooltip:		'Tasa',	
				dataIndex:	'TASA',		
				sortable:true,	
				width:70,	
				align:'right'
				
			},
			{
				header:		'Capital', 
				tooltip: 	'Capital',	
				dataIndex: 	'TOTAL_AMORTIZACION',	
				sortable:true, 
				width:70, 	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			
			{	
				header:		'D�as',
				tooltip:		'D�as',	
				dataIndex:	'DIAS',		
				sortable:true,	
				width:70,	
				
				align:'right'
			},	
			{	//B
				header:		'Inter�s',
				tooltip:		'Inter�s',	
				dataIndex:  'INTERES',	
				sortable:true,	
				width:70,	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			
			{
				header:		'Comisi�n', 
				tooltip: 	'Comisi�n',	
				dataIndex: 	'COMISION',	
				sortable:true, 
				width:70, 	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			{	
				header:		'IVA',
				tooltip:		'IVA',
				dataIndex:	'IVA',		
				sortable:true, 
				width:70, 
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			},
			{	
				header:		'Importe Pago',
				tooltip:		'Importe Pago',	
				dataIndex:  'TOTAL_EXIGIBLE',	
				sortable:true,	
				width:100,	
				renderer: Ext.util.Format.numberRenderer('$0,0.00'),
				align:'right'
				
			}
		],
		view: new Ext.grid.GroupingView({
            forceFit:false,
            groupTextTpl: '{text}'					 
        }),
		  stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true,
		
		
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacionB',
			displayInfo: true,
			store: consultaDataB,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivoB',
					handler: function(boton, evento) 
					{
						boton.disable();
						boton.setIconClass('loading-indicator');
						totalProperty:'total',
						Ext.Ajax.request
						({
							url: '13forma012ext.data.jsp',	
							params: Ext.apply (fp.getForm().getValues(),{					 
							informacion: 'GenerarCSV'
								
							}),
							callback: procesarSuccessFailureGenerarCSVB
						});
					}
				},
			  
				{
					xtype: 'button',
					text: 'Bajar Archivo ',
					id: 'btnBajarArchivoB',
					width: 10,
					hidden: true
				},
				'-',
				{
				
					xtype: 'button',
					text: 'Imprimir PDF',	
					id: 'btnImprimirPDFB',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						totalProperty:'total',
						Ext.Ajax.request({
							url: '13forma012ext.data.jsp', 
							//params: fp.getForm().getValues(),{
							params: Ext.apply (fp.getForm().getValues(),{
								informacion: 'GenerarPDF'
								
								}),
								
							callback: procesarSuccessFailureGenerarPDFB
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Bajar PDF ',
					id: 'btnBajarPDFB',
					width: 5, 
					hidden: true
				}
				
			]
		}
		
	});

var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		//style: 'margin:0 auto;',
		title:'',
		stripeRows: true,
		height: 60,
		width:  940,//600,
		frame: false,
		hidden: true,
		columns: [
			{							
				header : 'Total Pagos',
				tooltip: 'Total Pagos',
				dataIndex : 'TOTAL_PAGOS',
				sortable: true,
				width: 80,
				resizable: true,				
				align: 'right'	
			},		
			{							
				header : 'Total Importe de Dep�sito',
				tooltip: 'Total Importe de Dep�sito',
				dataIndex : 'TOTAL_IMPORTE',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
			
		]
	});
	

		/****  End Grid�s ****/
/*----------------------------------------Componentes----------------------------------------*/
	var elementosForma = [
	{
		xtype: 'panel',
		columnWidth:.10, 
		labelWidth:146,
		labelAlign:'right',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		layout: 'form',
		items: [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de operaci�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
			{
				xtype: 'datefield',
				name: 'txtFchOper1',
				id: 'txtFchOper1',
				allowBlank: true,
				startDay: 0,
				width: 130,
				msgTarget: 'side',
				vtype: 'rangofecha', 
				campoFinFecha: 'txtFchOper2',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},
			{
				xtype: 'displayfield',
				value: 'a',
				width: 24
			},
			{
					xtype: 'datefield',
					name: 'txtFchOper2',
					id: 'txtFchOper2',
					allowBlank: true,
					startDay: 1,
					width: 130,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFchOper1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
			}
			]
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Probable de Pago',
			combineErrors: false,
			msgTarget: 'side',
			items: [
			{
				xtype: 'datefield',
				name: 'txtFchPro1',
				id: 'txtFchPro1',
				allowBlank: true,
				startDay: 0,
				width: 130,
				msgTarget: 'side',
				vtype: 'rangofecha', 
				campoFinFecha: 'txtFchPro2',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},
			{
				xtype: 'displayfield',
				value: 'a',
				width: 24
			},
			{
					xtype: 'datefield',
					name: 'txtFchPro2',
					id: 'txtFchPro2',
					allowBlank: true,
					startDay: 1,
					width: 130,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFchPro1',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
			}
			]
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Pago',
			combineErrors: false,
			msgTarget: 'side',
			items: [
			{
				xtype: 'datefield',
				name: 'txtFchPago1',
				id: 'txtFchPago1',
				allowBlank: true,
				startDay: 0,
				width: 130,
				msgTarget: 'side',
				vtype: 'rangofecha', 
				campoFinFecha: 'txtFchPago2',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},
			{
				xtype: 'displayfield',
				value: 'a',
				width: 24
			},
			{
					xtype: 'datefield',
					name: 'txtFchPago2',
					id: 'txtFchPago2',
					allowBlank: true,
					startDay: 1,
					width: 130,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'txtFchPago1',
					margins: '0 20 0 0'  
			}]
			
		}]
	}];
				
		
	var fp = new Ext.form.FormPanel
	({
		id: 'forma',
		width: 540,
		style: ' margin:0 auto;',
		title:	'<div align="center">Criterios de B�squeda</div>',
		frame: true,
		collapsible: true,
		titleCollapse: true,
		bodyStyle: 'padding: 6px',
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		items: elementosForma, 
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				
								
				handler: function(boton, evento) {
					gridNB.hide();
					gridB.hide();
					var totalesCmp = Ext.getCmp('gridTotales');
					if (totalesCmp.isVisible()) {
						totalesCmp.hide();
					}
					
					var fechaOper1 = Ext.getCmp('txtFchOper1');
					var fechaOper2 = Ext.getCmp('txtFchOper2');
					var fechaPro1  = Ext.getCmp('txtFchPro1');
					var fechaPro2  = Ext.getCmp('txtFchPro2');
					var fechaPago1 = Ext.getCmp('txtFchPago1');
					var fechaPago2 = Ext.getCmp('txtFchPago2');
	
					if (!Ext.isEmpty(fechaOper1.getValue()) || !Ext.isEmpty(fechaOper2.getValue()) ) {
						if(Ext.isEmpty(fechaOper1.getValue()))	{
							fechaOper1.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaOper1.focus();
							return;
						}else if (Ext.isEmpty(fechaOper2.getValue())){
							fechaOper2.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaOper2.focus();
							return;
						}
					}
					
					if (!Ext.isEmpty(fechaPro1.getValue()) || !Ext.isEmpty(fechaPro2.getValue()) ) {
						if(Ext.isEmpty(fechaPro1.getValue()))	{
							fechaPro1.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaPro1.focus();
							return;
						}else if (Ext.isEmpty(fechaPro2.getValue())){
							fechaPro2.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaPro2.focus();
							return;
						}
					}
					
					if (!Ext.isEmpty(fechaPago1.getValue()) || !Ext.isEmpty(fechaPago2.getValue()) ) {
						if(Ext.isEmpty(fechaPago1.getValue()))	{
							fechaPago1.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaPago1.focus();
							return;
						}else if (Ext.isEmpty(fechaPago2.getValue())){
							fechaPago2.markInvalid('Debe capturar ambas fechas de solicitud o dejarlas en blanco');
							fechaPago2.focus();
							return;
						}
					}
					
					
					if ( fechaOper1.getValue()!= "" && fechaOper2.getValue()!="" && (fechaPro1.getValue()!="" || fechaPro2.getValue()!="" || fechaPago1.getValue()!="" || fechaPago2.getValue()!="") )
					{
						Ext.MessageBox.alert('Mensaje de p�gina web','Los criterios de busqueda son excluyentes. S�lo se puede realizar por Fecha de Vencimiento, Fecha Probable de Pago o Fecha de Pago' );
						return;
					}
					else if ( fechaPro1.getValue()!= "" && fechaPro2.getValue()!="" && (fechaOper1.getValue()!="" || fechaOper2.getValue()!="" || fechaPago1.getValue()!="" || fechaPago2.getValue()!="") )
					{
						Ext.MessageBox.alert('Mensaje de p�gina web','Los criterios de busqueda son excluyentes. S�lo se puede realizar por Fecha de Vencimiento, Fecha Probable de Pago o Fecha de Pago');
						return;
					}
					else if ( fechaPago1.getValue()!= "" && fechaPago2.getValue()!="" && (fechaPro1.getValue()!="" || fechaPro2.getValue()!="" || fechaOper1.getValue()!="" || fechaOper2.getValue()!="") )
					{
						Ext.MessageBox.alert('Mensaje de p�gina web','Los criterios de busqueda son excluyentes. S�lo se puede realizar por Fecha de Vencimiento, Fecha Probable de Pago o Fecha de Pago');
						return;
					}
					
					pnl.el.mask('Enviando...', 'x-mask-loading');
					
					if(sTipoBanco=='B') 
					{
						consultaDataB.load({
						params: Ext.apply(fp.getForm().getValues(),{	
						operacion: 'Generar',
						start : 0,
						limit: 15})
						});					
					}
					else  if(sTipoBanco=='NB') 
					{
						consultaDataNB.load({
						params: Ext.apply(fp.getForm().getValues(),{	
						operacion: 'Generar',
						start : 0,
						limit: 15})
						});						
					}
					
				} //fin handler
				
			}, //fin boton Consultar
			
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					leeRespuesta();
				}
			}
		]
	});
	
	var pnl = new Ext.Container
	({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(10),
			gridNB,	
			gridB,	
			gridTotales,	
			NE.util.getEspaciador(10)	
		]
	});
	
	
});