<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.descuento.*,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoPymeBusqAvazada,
		com.netro.model.catalogos.CatalogoIFoferta,
		javax.naming.*,
		net.sf.json.*,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String cveIf = (request.getParameter("cboIf")!=null)?request.getParameter("cboIf"):"";
String msgError = "";
try{

	AutorizacionTasas EJBautorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	String sesCveIf = "";
	
	if(strTipoUsuario.equals("IF")){
		sesCveIf = iNoCliente;
	}else{
		sesCveIf = cveIf;
	}
	
	System.out.println("sesCveIf==="+sesCveIf);
	
	if (informacion.equals("valoresIniciales")) {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("tipoUsuario",strTipoUsuario);
		jsonObj.put("success",new Boolean(true));
		infoRegresar = jsonObj.toString();
	
	}else if (informacion.equals("catalogoIf")) {
		CatalogoIFoferta cat = new CatalogoIFoferta();
		cat.setCampoClave("ic_if");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	
	}else	if (informacion.equals("catalogoEpo") && !"".equals(sesCveIf)) {
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setClaveIf(sesCveIf);
		cat.setHabilitado("S");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	
	}else if (informacion.equals("catalogoMoneda")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();
	
	}else if(informacion.equals("busquedaAvanzada")){
	
		String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
		String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		String numElecPyme = (request.getParameter("numElecPyme")!=null)?request.getParameter("numElecPyme"):"";
		String csNoParam = (request.getParameter("csNoParam")!=null)?request.getParameter("csNoParam"):"";
		
		
		/*
		CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
		cat.setCampoClave("crn.ic_nafin_electronico");
		cat.setCampoDescripcion("RPAD (crn.ic_nafin_electronico, 10, ' ') || p.cg_razon_social");
		if(!nombrePyme.equals(""))
			cat.setNombre_pyme(nombrePyme);
		if(!rfcPyme.equals(""))
			cat.setRfc_pyme(rfcPyme);
		if(!icEpo.equals(""))
			cat.setIc_epo(icEpo);
		//cat.setOrden("p.cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	*/
	
	JSONArray registros = new JSONArray();
	HashMap info = new HashMap();
	
			
		
	List lstPymesOferTasa = EJBautorizacionTasas.getPymesOferTasasCons(sesCveIf, cboEpo, cboMoneda, numElecPyme, "N",nombrePyme, rfcPyme);
		
	for (int i = 0; i <lstPymesOferTasa.size(); i++) {		
		HashMap datos 	= (HashMap) lstPymesOferTasa.get(i);	
		String 	clave = (String)   datos.get("NAFINE");  
		String 	descripcion = (String)   datos.get("RAZONSOCIAL");  
		
		info = new HashMap();
		info.put("clave", clave);
		info.put("descripcion", descripcion);	
		registros.add(info);
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";


	}else if(informacion.equals("consultaOfertaTasas")){
	
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		String numElecPyme = (request.getParameter("numElecPyme")!=null)?request.getParameter("numElecPyme"):"";
		JSONArray jsObjArray = new JSONArray();
		
		List lstTasasOfer = EJBautorizacionTasas.consTasasOfertaIf(sesCveIf, cboEpo, cboMoneda, numElecPyme);
		jsObjArray = JSONArray.fromObject(lstTasasOfer);
		
		infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";		
	
	}else if(informacion.equals("consultaPymesParam")){
		
		
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		String numElecPyme = (request.getParameter("numElecPyme")!=null)?request.getParameter("numElecPyme"):"";
		String csNoParam = (request.getParameter("csNoParam")!=null)?request.getParameter("csNoParam"):"";
		
		JSONArray jsObjArray = new JSONArray();
		List lstPymesOferTasa = EJBautorizacionTasas.getPymesOferTasasCons(sesCveIf, cboEpo, cboMoneda, numElecPyme, csNoParam,"","");
		
		jsObjArray = JSONArray.fromObject(lstPymesOferTasa);
		infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";		
		
		
	}else if(informacion.equals("generarArchivo")){
	
		JSONObject jsonObj = new JSONObject();
		String  tipoArchivo = (request.getParameter("tipoArchivo") == null)?"":request.getParameter("tipoArchivo");
		String nombreArchivo = "";
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		String numElecPyme = (request.getParameter("numElecPyme")!=null)?request.getParameter("numElecPyme"):"";
		String csNoParam = (request.getParameter("csNoParam")!=null)?request.getParameter("csNoParam"):"";
		String  recordEpo = (request.getParameter("recordEpo") == null)?"":request.getParameter("recordEpo");
		String  recordMoneda = (request.getParameter("recordMoneda") == null)?"":request.getParameter("recordMoneda");
	
		
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
			
		//consulta
		List lstTasasOfer = EJBautorizacionTasas.consTasasOfertaIf(sesCveIf, cboEpo, cboMoneda, numElecPyme);
		
		contenidoArchivo.append(" EPO ,"+recordEpo+"\n");			
		contenidoArchivo.append(" Moneda , "+recordMoneda+"\n");
		
		
		if(lstTasasOfer.size()>0) {		
			contenidoArchivo.append("\n Monto Publicado Desde , Monto Publicado Hasta, Puntos Preferenciales', Estatus \n") ;
		}
			
		for (int i = 0; i <lstTasasOfer.size(); i++) {		
			HashMap datos 	= (HashMap) lstTasasOfer.get(i);				
			String 	 montodesde = (String)   datos.get("MONTODESDE");  
			String 	 montohasta = (String)   datos.get("MONTOHASTA"); 
			String 	 estatus = (String)   datos.get("ESTATUS");    
			String 	 puntos = (String)   datos.get("PUNTOSPREF");    
			
			contenidoArchivo.append("\"$"+Comunes.formatoDecimal(montodesde, 2, true)+"\",");
			contenidoArchivo.append("\"$"+Comunes.formatoDecimal(montohasta, 2, true)+"\",");
			contenidoArchivo.append("\""+Comunes.formatoDecimal(puntos, 2, true)+"%"+"\",");	
			contenidoArchivo.append(estatus+",");
			contenidoArchivo.append("\n");			
		}
				
		//pymes parametrizadas
		List lstPymesOferTasa = EJBautorizacionTasas.getPymesOferTasasCons(sesCveIf, cboEpo, cboMoneda, numElecPyme, "S","","");
		if(lstPymesOferTasa.size()>0){
		 contenidoArchivo.append(" "+"\n");	
			contenidoArchivo.append("\n Parametrización de PyME(s) \n");	
			contenidoArchivo.append("RFC, Nombre de la PyME \n");
		}

		for (int i = 0; i <lstPymesOferTasa.size(); i++) {		
			HashMap datos 	= (HashMap) lstPymesOferTasa.get(i);	
			String 	 rfc = (String)   datos.get("RFCPYME");  
			String 	 nombre = (String)   datos.get("RAZONSOCIAL");  
			
			contenidoArchivo.append(rfc+",");
			contenidoArchivo.append(nombre.replaceAll(",", "")+",");
			contenidoArchivo.append("\n");	
		}
			




		
			if(archivo.make(contenidoArchivo.toString(), strDirectorioTemp, "."+tipoArchivo))
				nombreArchivo = archivo.nombre;
		
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		infoRegresar = jsonObj.toString();
		
	}
}catch(Throwable t){
	t.printStackTrace();
}finally{
	
}
%>


<%=infoRegresar%>