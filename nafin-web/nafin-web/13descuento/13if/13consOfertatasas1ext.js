	Ext.onReady(function() {
	
	var busquedaAvanza = ['Utilice el *para b�squeda gen�rica'];
	var recordEpo;
	var recordMoneda;	
	var  comboEpol;
	var  cboMoneda;
	
	var Inicializacion = {
		catalogoIf : false,
		catalogoEpo: false,
		tipoUsuario: null
	};
	
	var procesarSuccessValoresIni = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			Inicializacion.tipoUsuario = resp.tipoUsuario;
			
			if(Inicializacion.tipoUsuario == 'IF'){
				storeCatEpoData.load();
				
			}else if(Inicializacion.tipoUsuario == 'NAFIN'){
				Ext.getCmp('cboIf1').show();
				storeCatIfData.load();
			}
			


		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarArchivo=  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	//STORES------------------------------------------------------------------------
	var storeBusqAvanzada = new Ext.data.JsonStore({
		id: 'storeBusqAvanzada',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '13consOfertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var storeCatIfData = new Ext.data.JsonStore({
		id: 'catalogoIfStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consOfertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIf'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'catalogoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consOfertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeCatMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consOfertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var procesarConsultaTasas = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var el = gridOfertaTasas.getGridEl();
			
		if (arrRegistros != null) {
			 comboEpo = Ext.getCmp('cboEpo1');
			recordEpo = comboEpo.findRecord(comboEpo.valueField, comboEpo.getValue());
			 cboMoneda = Ext.getCmp('cboMoneda1');
			 recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());			
			
			gridOfertaTasas.setTitle(recordEpo.get(comboEpo.displayField)+' - '+recordMoneda.get(cboMoneda.displayField));
			if (!gridOfertaTasas.isVisible()) {
				gridOfertaTasas.show();				
			}
			
			if(store.getTotalCount() > 0) {	
				fpGenerar.show();	
				el.unmask();			
			
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');		
			}
		}
	}
	
	var procesarConsultaPymes = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var el = gridPymes.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridPymes.isVisible()) {
				gridPymes.show();
			}
			if(store.getTotalCount() > 0) {
				fpGenerar.show();	
				el.unmask();				
			}else{
				el.mask('No hay Pymes en la relacion IF - EPO','x-mask')
			}
		}
	}
	
	var storeTasasData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consOfertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'consultaOfertaTasas'
		},
		fields: [
			{name:'CVETASA'},
			{name:'CVEIF'},
			{name:'CVEEPO'},
			{name:'TIPOTASA'},
			{name:'MONTODESDE', type: 'float'},
			{name:'MONTOHASTA', type: 'float'},
			{name:'PUNTOSPREF', type: 'float'},
			{name:'ESTATUS'},
			{name:'CVEMONEDA'},
			{name:'ACUSE'},
			{name:'SELECCION'}			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaTasas,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConsultaTasas(null, null, null);						
				}
			}
		}
	});
	
	
	var gridOfertaTasas = new Ext.grid.EditorGridPanel({
		id: 'gridOfertaTasas1',
		store: storeTasasData,
		margins: '20 0 0 0',
		clicksToEdit: 1,
		title:' ',	
		hidden: true,
		columns: [		
			{
				header : 'Monto Pub. Desde',
				tooltip: 'Monto Publicado Desde',
				dataIndex : 'MONTODESDE',
				sortable : true,
				width : 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto Pub. Hasta',
				tooltip: 'Monto Publicado Hasta',
				dataIndex : 'MONTOHASTA',
				sortable : true,
				width : 150,
				align: 'right',		
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Puntos Preferenciales',
				tooltip: 'Puntos Preferenciales',
				dataIndex : 'PUNTOSPREF',
				width : 150,
				sortable : true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('%0,0.0000')
			},
			{
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				width : 130,
				sortable : true,
				align: 'center'			
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		autoHeight: true,
		width: 600,
		bodyStyle:'text-align:left',
		frame: true
	});
	
		
	var storePymeData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consOfertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'consultaPymesParam'
		},
		fields: [
			{name:'CVEPYME'},
			{name:'RFCPYME'},
			{name:'CTABANC'},
			{name:'RAZONSOCIAL'},
			{name:'PARAMTASAS'},
			{name:'SELECCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			load: procesarConsultaPymes,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaPymes(null, null, null);
				}
			}
		}
	});
	
	var gridPymes = new Ext.grid.EditorGridPanel({
		id: 'gridPymes1',
		store: storePymeData,
		margins: '20 0 0 0',
		clicksToEdit: 1,
		hidden: true,
		title:'Parametrizaci�n de PyME(s)',
		columns: [
			{
				header : 'RFC',
				tooltip: 'RFC',
				dataIndex : 'RFCPYME',
				sortable : true,
				width : 150,
				align: 'left'
			},
			{
				header : 'Nombre de la PyME',
				tooltip: 'Nombre de la PyME',
				dataIndex : 'RAZONSOCIAL',
				sortable : true,
				width : 415,
				align: 'left'
			}		
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,		
		autoHeight: true,
		width: 600,
		bodyStyle:'text-align:left',
		frame: true	
	});

	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  
		},				
		{
			xtype: 'panel',
			items: [
					{
						xtype: 'button',
						width: 80,
						height:10,
						text: 'Buscar',
						iconCls: 'iconoLupa',
						id: 'btnBuscar',
						anchor: '',
						style: 'float:right',
						handler: function(boton, evento) {
							var nombre= Ext.getCmp("nombrePyme1");
							var rfc= Ext.getCmp("rfcPyme1");	
							var cboEpo = Ext.getCmp("cboEpo1");
							var cboMoneda = Ext.getCmp("cboMoneda1");

							if (Ext.isEmpty(nombre.getValue())  &&  Ext.isEmpty(rfc.getValue()) ) {
									nombre.markInvalid('Es necesario que ingrese datos en al menos un campo');						
									return;
							}	
							var cbpyme = Ext.getCmp('cbpyme1');
							cbpyme.setValue('');
							cbpyme.setDisabled(false);				
							storeBusqAvanzada.load({
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
								cboEpo: cboEpo.getValue(),
								cboMoneda:cboMoneda.getValue()							
								})	
								});	
						}
					}
			]
		},				
		{
			xtype: 'combo',
			name: 'cbpyme',
			id: 'cbpyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzada
		}
	];
	
	//panel para mostrar  el texto 
	var fpB = {
		xtype: 'panel',
		id: 'forma2',
		width: 600,			
		frame: true,		
		titleCollapse: false,
		bodyStyle: 'padding: 6px',			
		defaultType: 'textfield',
		align: 'center',
		html: busquedaAvanza.join('')
	};
			
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 'fBusqAvanzada',
		width: 400,
		title: 'Busqueda Avanzada',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
				fpB, NE.util.getEspaciador(20), elementsFormBusq
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoBuscar',
				formBind: true,
				disabled: true,
				align: 'center',
				handler: function(boton, evento) {	
					var cbpyme= Ext.getCmp("cbpyme1");
					var numElecPyme = Ext.getCmp('numElecPyme1');
					
					if (Ext.isEmpty(cbpyme.getValue())) {
						cbpyme.markInvalid('Seleccione la Pyme');
						return;
					}
						
					var record = cbpyme.findRecord(cbpyme.valueField, cbpyme.getValue());
					record =  record ? record.get(cbpyme.displayField) : cbpyme.valueNotFoundText;					
					numElecPyme.setValue(cbpyme.getValue());
						
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();					
				}				
			}
		]
	});
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'cboIf',
			id: 'cboIf1',
			fieldLabel: 'IF',
			mode: 'local',
			hidden: false,
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboIf',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatIfData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				beforeselect: function( combo, record, index ){
					Ext.getCmp('cboEpo1').setValue('');
				},
				change: function( combo, newValue, oldValue ){
					if(newValue!=oldValue){
						if(newValue!=''){
							
							storeCatEpoData.load(
								{
									params: Ext.apply(fp.getForm().getValues())
								}
							);
						}
					}
				
				}
			}
		},
		{
			xtype: 'combo',
			name: 'cboEpo',
			id: 'cboEpo1',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEpo',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEpoData,
			tpl : NE.util.templateMensajeCargaCombo		
		},
		{
			xtype: 'combo',
			name: 'cboMoneda',
			id: 'cboMoneda1',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMoneda',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMonedaData,
			tpl : NE.util.templateMensajeCargaCombo		
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�m. Electr�nico PyME',			
			msgTarget: 'side',
			id: 'compositefield1',
			combineErrors: false,		
			items: [
				{
					xtype: 'numberfield',
					name: 'numElecPyme',
					id: 'numElecPyme1',
					allowBlank: true,
					hidden: false,
					maxLength: 12,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
				xtype: 'button',
				hidden: false,
				text: 'Busqueda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
				
				var cboEpo = Ext.getCmp("cboEpo1");
				if (Ext.isEmpty(cboEpo.getValue()) ) {
					cboEpo.markInvalid('Debe Seleccionar una Epo');					
					return;
				}
				
				var cboMoneda = Ext.getCmp("cboMoneda1");
				if (Ext.isEmpty(cboMoneda.getValue())  ) {
					cboMoneda.markInvalid('Debe Seleccionar  el tipo de Moneda');					
					return;
				}
				
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								layout: 'fit',
								width: 400,
								height: 300,
								id: 'winBusqAvan',
								closeAction: 'hide',
								items: [
									fpBusqAvanzada
								]
							}).show();
					}
				}
			}					
			]
		}			
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Oferta de Tasas por Montos',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) 			{	
				
				var cboEpo = Ext.getCmp("cboEpo1");
				if (Ext.isEmpty(cboEpo.getValue()) ) {
					cboEpo.markInvalid('Debe Seleccionar una Epo');					
					return;
				}
				
				var cboMoneda = Ext.getCmp("cboMoneda1");
				if (Ext.isEmpty(cboMoneda.getValue())  ) {
					cboMoneda.markInvalid('Debe Seleccionar  el tipo de Moneda');					
					return;
				}
				fp.el.mask('Consultando...', 'x-mask');
				storeTasasData.load({
					params: Ext.apply(fp.getForm().getValues())
				});
													
				storePymeData.load({									
					params: Ext.apply(fp.getForm().getValues(),{
					csNoParam: 'S'						
					})					
				});
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnGenerarArchivo').enable();			
				
				}	
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consOfertatasas1ext.jsp';
				}
			}
		],		
		monitorValid: true
	});
	
	
	
	var fpGenerar = new Ext.Container({
		layout: 'table',
		id: 'formaI',
		hidden: true,
		layoutConfig: {
			columns: 3
		},
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
		{
			xtype: 'button',
			text: 'Generar Archivo',
			id: 'btnGenerarArchivo',			
			handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
				url: '13consOfertatasas1ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					tipoArchivo: 'csv',
					informacion: 'generarArchivo',
					recordEpo: recordEpo.get(comboEpo.displayField),
					recordMoneda:recordMoneda.get(cboMoneda.displayField)	
					}),
					callback: procesarSuccessFailureGenerarArchivo
				});
			}
		},
		{
			xtype: 'button',
			text: 'Bajar Archivo',
			id: 'btnBajarArchivo',
			hidden: true
		}	
	]
	});
	
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridOfertaTasas,
			NE.util.getEspaciador(20),
			gridPymes,
			NE.util.getEspaciador(20),
			fpGenerar		
		]
	});
	
	
	Ext.Ajax.request({
		url: '13consOfertatasas1ext.data.jsp',
		params: {
			informacion: 'valoresIniciales'
			},
		callback: procesarSuccessValoresIni
	});

	
	
	storeCatMonedaData.load();

});