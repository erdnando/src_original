<%@ page
	contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.sql.*, 
    java.math.*, 
    java.text.*,	
    netropology.utilerias.*, 
    com.netro.exception.*,	
    javax.naming.*,	
    com.netro.descuento.*, 
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
	JSONObject archivoCSV = new JSONObject();
	JSONObject archivoPDF = new JSONObject();
	JSONObject registroAcuse = new JSONObject();
	JSONArray arrAcuse = new JSONArray();
	boolean flag = true;
	boolean flagExt = true;
	boolean flagReg = true;
	ParametrosRequest req = null;
	String rutaArchivo = "";
	String PATH_FILE	=	strDirectorioTemp;
	String itemArchivo = "";
	String extension = "";
	String folio = "";
	String fechaHora = "";
	String numProceso = "";
	String nombreIF = "";
	String claveIF = "";
	String msgError = "";
	int icProceso = 0;
	int tamanio = 0;
	
  JSONArray regJsonArray = new JSONArray();
  JSONArray regValidosjsonArray = new JSONArray();
  StringBuffer 	listaNumerosLinea	= new StringBuffer();
	
  ResultadosValidacionCatalogo 	resultados = null;
  
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));

    extension = (req.getParameter("hidExtension") == null)? "" : req.getParameter("hidExtension");
    claveIF = "IF".equals(strTipoUsuario)?iNoCliente:((req.getParameter("claveIF")  == null)?"0":req.getParameter("claveIF"));
    nombreIF = "IF".equals(strTipoUsuario)?strNombre :((req.getParameter("nombreIF") == null)?"NO DISPONIBLE":req.getParameter("nombreIF"));
		
		//try{
			int tamaPer = 0;
			if (extension.equals("txt")){
				upload.setSizeMax(200 * 1024);	//700 Kb
				tamaPer = (200 * 1024);
			}

			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			InputStream archivoZip = fItem.getInputStream();
			tamanio			= (int)fItem.getSize();

			rutaArchivo = PATH_FILE+itemArchivo;
			fItem.write(new File(rutaArchivo));
			System.out.println("tamanio=="+tamanio);
			System.out.println("tamaPer=="+tamaPer);
			System.out.println("itemArchivo=="+itemArchivo);
			
			if (tamanio > tamaPer){
				flag = false;
				msgError = "El tamaño del archivo TXT es mayor al permitido";
			}
			//if (flag){

				fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				//try{
					
          // Cargar Archivo en Directorio Temporal
          //String rutaArch									= null;
          //myUpload.save(strDirectorioTemp);
          //com.jspsmart.upload.File myFile 		= myUpload.getFiles().getFile(0);
          //ruta2											= strDirectorioTemp+myFile.getFileName();
          
          CatalogoErroresWSIF validador = new CatalogoErroresWSIF();
          System.out.println("rutaArchivo === "+rutaArchivo);
          resultados = validador.procesarCargaMasiva(strDirectorioTemp, rutaArchivo, claveIF);
          // Obtener Registros Validos
          ArrayList lista = (ArrayList) resultados.getListaDeRegistrosExitosos(); 	
          icProceso = resultados.getIcProceso(); 											
          ArrayList registrosValidos = validador.getRegistrosValidos(lista,icProceso);
          
          // Construir lista con los Numeros de Linea de los Registros Validos
          
          for(int i=0;i<lista.size();i++){
            if(i>0) listaNumerosLinea.append(",");
              listaNumerosLinea.append((String) lista.get(i));
          }
          
          List registros = resultados.getRegistros();
          List regErroneos = new ArrayList();
          for(int i=0;i<registros.size();i++){
            HashMap hm = new HashMap();
            ValidacionCatalogo v = (ValidacionCatalogo) registros.get(i);
            if(v.hayError()){
              
              if(v.hayErrorDeClave()){
                hm.put("LINEA", String.valueOf(v.getNumeroLinea()));
                hm.put("CAMPO", "Clave");
                hm.put("OBSERVACION", v.getErroresClave());
                regErroneos.add(hm);
              }
              if(v.hayErrorDeDescripcion()){
                hm.put("LINEA", String.valueOf(v.getNumeroLinea()));
                hm.put("CAMPO", "Clave");
                hm.put("OBSERVACION", v.getErroresDescripcion());
                regErroneos.add(hm);
              }
              if(v.hayErrorGenerico()){
                hm.put("LINEA", String.valueOf(v.getNumeroLinea()));
                hm.put("CAMPO", " ");
                hm.put("OBSERVACION", v.getErrorGenerico());
                regErroneos.add(hm);
              }
              
            }
            
          }
                          
                          
          regJsonArray = JSONArray.fromObject(regErroneos);
          regValidosjsonArray = JSONArray.fromObject(registrosValidos);	
          
          session.removeAttribute("13CATERRORESWSIFMB_INSERTED");
					

		/*		}catch (Exception e){
						throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			
			}
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}*/
	}
	
%>	
{
	"success": true,
	"flag":	<%=(flag)?"true":"false"%>,
	"lstNumerosLineaOk":	'<%=listaNumerosLinea.toString()%>',
	"claveIF":	'<%=claveIF%>',
	"icProceso":	'<%=icProceso%>',
	"registrosValidos":	<%=regValidosjsonArray%>,
  "registrosErroneos":	<%=regJsonArray%>,
	"nombreIF":	'<%=nombreIF%>',
  "getNumRegOk":	'<%=resultados.getNumRegOk()%>',
  "getNumRegErr":	'<%=resultados.getNumRegErr()%>'
	
}