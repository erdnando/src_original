Ext.onReady(function() {

//--------------------Handlers-----------------------
var strTipoBanco;

var fechaProb;
var fechaVenc;
var consultasEncabezados=new Array();
var gridsEncabezados=new Array();
var consultasDetalles=new Array();
var gridDetalles=new Array();
var icMoneda;

var creaComponentes=function(numeroCmp,respuesta){
	
	consultasEncabezados=new Array(numeroCmp);
	gridsEncabezados=new Array(numeroCmp);
	consultasDetalles=new Array(numeroCmp);
	gridDetalles=new Array(numeroCmp);
	var cad;
	for(var i=1;i<numeroCmp;i++){
		
		consultasEncabezados[i-1] = new Ext.data.JsonStore({
						fields: [
								{name: 'CLAVE_IF'},
								{name: 'CLAVE_MONEDA'},
								{name: 'MONEDA'},
								{name: 'FECHA_VENCIMIENTO'},
								{name: 'FECHA_PROB_PAGO'},
								{name: 'IMPORTE_DEPOSITO'},
								{name: 'CLAVE_ESTADO'},
								{name: 'NOMBRE_ESTADO'}
						],
						//data: [{'IC_ENCABEZADO_PAGO_TMP':'','IC_MONEDA':'','IG_SUCURSAL':'','NOMBREMONEDA':'','DF_PERIODO_FIN':'',
							//		'DF_PROBABLE_PAGO':'','DF_DEPOSITO':'','IG_IMPORTE_DEPOSITO':'','NOMBREFINANCIERA':''}],
						//autoLoad: true,
						listeners: {exception: NE.util.mostrarDataProxyError}
					});
					
					
					if(strTipoBanco=='NB'){
						gridsEncabezados[i-1] = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,	monitorResize: true,	height: 90,	width: 790,frame: false,style: 'margin: 0 auto',
							title: 'Encabezado',
							//id: 'encabezado'+i,
							store: consultasEncabezados[i-1],
							columns: [
								{header: 'Clave del I.F.',tooltip: 'Clave del I.F.',	dataIndex: 'CLAVE_IF',	width: 100,	align: 'center'},
								{header: 'Clave direcci&oacute;n estatal IF',tooltip: 'Clave direcci&oacute;n estatal IF',	dataIndex: 'CLAVE_ESTADO',	width: 100,	align: 'center'},
								{header: 'Clave moneda',tooltip: 'Clave moneda',	dataIndex: 'CLAVE_MONEDA',	width: 100,	align: 'center'},
								{header: 'Banco de servicio',tooltip: 'Banco de servicio',	dataIndex: 'BANCO',	width: 100,	align: 'center'},
								{header: 'Fecha de vencimiento',tooltip: 'Fecha de vencimiento',	dataIndex: 'FECHA_VENCIMIENTO',	width: 100,	align: 'center' },
								{header: 'Fecha probable de pago',tooltip: 'Fecha probable de pago',	dataIndex: 'FECHA_PROB_PAGO',	width: 100,	align: 'center' },
								{header: 'Fecha de dep�sito',tooltip: 'Fecha de dep�sito',	dataIndex: 'FECHA_PROB_PAGO',	width: 100,	align: 'center' },
								{header: 'Importe de dep�sito',tooltip: 'Importe de dep�sito',	dataIndex: 'IMPORTE_DEPOSITO',	width: 100,	align: 'center',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
								{header: 'Referencia Banco',tooltip: 'Referencia Banco',	dataIndex: 'REF_BANCO',	width: 100,	align: 'center' },
								{header: 'Referencia Intermediario',tooltip: 'Referencia Intermediario',	dataIndex: 'REF_INTERMEDIARIO',	width: 100,	align: 'center' }
	
							]
						});
						
						}
						else{
							gridsEncabezados[i-1] = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,	monitorResize: true,	height: 90,	width: 790,frame: false,style: 'margin: 0 auto',
							title: 'Encabezado',
							//id: 'encabezado'+i,
							store: consultasEncabezados[i-1],
							columns: [
								{header: 'Clave del I.F.',tooltip: 'Clave del I.F.',	dataIndex: 'CLAVE_IF',	width: 100,	align: 'center'},
								{header: 'Clave direcci&oacute;n estatal IF',tooltip: 'Clave direcci&oacute;n estatal IF',	dataIndex: 'CLAVE_ESTADO',	width: 100,	align: 'center'},
								{header: 'Clave moneda',tooltip: 'Clave moneda',	dataIndex: 'CLAVE_MONEDA',	width: 100,	align: 'center'},
								{header: 'Banco de servicio',tooltip: 'Banco de servicio',	dataIndex: 'BANCO',	width: 100,	align: 'center'},
								{header: 'Fecha probable de pago',tooltip: 'Fecha probable de pago',	dataIndex: 'FECHA_PROB_PAGO',	width: 100,	align: 'center' },
								{header: 'Fecha de dep�sito',tooltip: 'Fecha de dep�sito',	dataIndex: 'FECHA_PROB_PAGO',	width: 100,	align: 'center' },
								{header: 'Importe de dep�sito',tooltip: 'Importe de dep�sito',	dataIndex: 'IMPORTE_DEPOSITO',	width: 100,	align: 'center',renderer: Ext.util.Format.numberRenderer('$0,0.00') },
								{header: 'Referencia Banco',tooltip: 'Referencia Banco',	dataIndex: 'REF_BANCO',	width: 100,	align: 'center' },
								{header: 'Referencia Intermediario',tooltip: 'Referencia Intermediario',	dataIndex: 'REF_INTERMEDIARIO',	width: 100,	align: 'center' }

								]
						});
						
						}
						cad='registrosEncabezados'+i;
						consultasEncabezados[i-1].loadData(respuesta[cad]);
						
						
						
						
		consultasDetalles[i-1] = new Ext.data.JsonStore({
						fields: [
							{name: 'SUCURSAL'},
							{name: 'SUBAPLICACION'},
							{name: 'PRESTAMO'},
							{name: 'ACREDITADO'},
							{name: 'FECHA_OPERACION'},
							{name: 'FECHA_VENCIMIENTO'},
							{name: 'FECHA_PAGO'},
							{name: 'SALDO'},
							{name: 'TASA'},
							{name: 'CAPITAL'},
							{name: 'DIAS'},
							{name: 'INTERESES'},
							{name: 'COMISION'},
							{name: 'IVA'},
							{name: 'IMPORTE_PAGO'},
							{name: 'CLAVE_SIRAC'},
							{name: 'MORATORIOS'},
							{name: 'SUBSIDIO'},
							{name: 'CONCEPTO_PAGO'},
							{name: 'ORIGEN_PAGO'}
							
						],
						//data: [{'IC_ENCABEZADO_PAGO_TMP':'','IC_MONEDA':'','IG_SUCURSAL':'','NOMBREMONEDA':'','DF_PERIODO_FIN':'',
						//			'DF_PROBABLE_PAGO':'','DF_DEPOSITO':'','IG_IMPORTE_DEPOSITO':'','NOMBREFINANCIERA':''}],
						//autoLoad: true,
						listeners: {exception: NE.util.mostrarDataProxyError}
					});
					
					if(strTipoBanco=='NB'){
					
						gridDetalles[i-1] = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,	monitorResize: true,	height: 150,	width: 790,frame: false,style: 'margin: 0 auto',
							title: 'Detalles',
							//id: 'detalles'+i,
							store: consultasDetalles[i-1],
							columns: [
								{header: 'Subaplicaci&oacute;n',tooltip: 'Subaplicaci&oacute;n',	dataIndex: 'SUBAPLICACION',	width: 100,	align: 'center'},
								{header: 'Pr&eacute;stamo',tooltip: 'Pr&eacute;stamo',	dataIndex: 'PRESTAMO',	width: 100,	align: 'center'},
								{header: 'Clave SIRAC',tooltip: 'Clave SIRAC',	dataIndex: 'CLAVE_SIRAC',	width: 100,	align: 'center'},
								{header: 'Capital',tooltip: 'Capital',	dataIndex: 'CAPITAL',	width: 100,	align: 'center'},
								{header: 'Inter&eacute;s',tooltip: 'Inter&eacute;s',	dataIndex: 'INTERESES',	width: 100,	align: 'center' },
								{header: 'Moratorios',tooltip: 'Moratorios',	dataIndex: 'MORATORIOS',	width: 100,	align: 'center' },
								{header: 'Subsidio',tooltip: 'Subsidio',	dataIndex: 'SUBSIDIO',	width: 100,	align: 'center' },
								{header: 'Comisi&oacute;n',tooltip: 'Comisi&oacute;n',	dataIndex: 'COMISION',	width: 100,	align: 'center' },
								{header: 'IVA',tooltip: 'IVA',	dataIndex: 'IVA',	width: 100,	align: 'center' },
								{header: 'Importe Pago',tooltip: 'Importe Pago',	dataIndex: 'IMPORTE_PAGO',	width: 100,	align: 'center' },
								{header: 'Concepto pago',tooltip: 'Concepto pago',	dataIndex: 'CONCEPTO_PAGO',	width: 100,	align: 'center' },
								{header: 'Origen pago',tooltip: 'Origen pago',	dataIndex: 'ORIGEN_PAGO',	width: 100,	align: 'center' }
							]
						});
						}else {
							gridDetalles[i-1] = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,	monitorResize: true,	height: 150,	width: 790,frame: false,style: 'margin: 0 auto',
							title: 'Detalles',
							//id: 'detalles'+i,
							store: consultasDetalles[i-1],
							columns: [
								{header: 'Sucursal',tooltip: 'Sucursal',	dataIndex: 'SUCURSAL',	width: 100,	align: 'center'},
								{header: 'Subaplicaci&oacute;n',tooltip: 'Subaplicaci&oacute;n',	dataIndex: 'SUBAPLICACION',	width: 100,	align: 'center'},
								{header: 'Pr&eacute;stamo',tooltip: 'Pr&eacute;stamo',	dataIndex: 'PRESTAMO',	width: 100,	align: 'center'},
								{header: 'Acreditado',tooltip: 'Acreditado',	dataIndex: 'ACREDITADO',	width: 100,	align: 'center',renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
								}},
								{header: 'Fecha de Operaci&oacute;n',tooltip: 'Fecha de Operaci&oacute;n',	dataIndex: 'FECHA_OPERACION',	width: 100,	align: 'center'},
								{header: 'Fecha de Vencimiento',tooltip: 'Fecha de Vencimiento',	dataIndex: 'FECHA_VENCIMIENTO',	width: 100,	align: 'center'},
								{header: 'Fecha de Pago',tooltip: 'Fecha de Pago',	dataIndex: 'FECHA_PAGO',	width: 100,	align: 'center'},
								{header: 'Saldo Insoluto',tooltip: 'Saldo Insoluto',	dataIndex: 'SALDO',	width: 100,	align: 'center'},
								{header: 'Tasa',tooltip: 'Tasa',	dataIndex: 'TASA',	width: 100,	align: 'center'},
								{header: 'Capital',tooltip: 'Capital',	dataIndex: 'CAPITAL',	width: 100,	align: 'center'},
								{header: 'D&iacute;as',tooltip: 'D&iacute;as',	dataIndex: 'DIAS',	width: 100,	align: 'center'},
								{header: 'Inter&eacute;s',tooltip: 'Inter&eacute;s',	dataIndex: 'INTERESES',	width: 100,	align: 'center'},
								{header: 'Comisi&oacute;n',tooltip: 'Comisi&oacute;n',	dataIndex: 'COMISION',	width: 100,	align: 'center' },
								{header: 'IVA',tooltip: 'IVA',	dataIndex: 'IVA',	width: 100,	align: 'center' },
								{header: 'Importe Pago',tooltip: 'Importe Pago',	dataIndex: 'IMPORTE_PAGO',	width: 100,	align: 'center' }
								
							]
						});
						
						}
						cad='registrosDetalles'+i;
						consultasDetalles[i-1].loadData(respuesta[cad]);
						

						
						fp.add(NE.util.getEspaciador(15));
						fp.add(gridsEncabezados[i-1]);
						fp.add(NE.util.getEspaciador(5));
						
						fp.add(gridDetalles[i-1]);
						
						//pnl.insert(i+1,gridreg+i);
						
		
		
	}
	//fp.add(tb);
	//fp.add(gridTotales);
	fp.doLayout();
}





var procesarConsultaSuccess = function(opts, success, response){
	
	if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
		fp.setVisible(true);
		fp.el.unmask();
		grid.el.unmask();
		if(Ext.util.JSON.decode(response.responseText).totales>1){
		dataTotal.loadData(Ext.util.JSON.decode(response.responseText).registros2);
		creaComponentes(Ext.util.JSON.decode(response.responseText).totales,Ext.util.JSON.decode(response.responseText));
		Ext.getCmp('generarArchivo').enable();
		Ext.getCmp('bajarArchivo').hide();
		tb.enable();
		tb.show();
		}
		else{
			tb.disable();
			fp.el.mask('No se encontr� ning�n registro', 'x-mask');
		}
		
	}else{
		fp.el.mask('No se encontr� ning�n registro', 'x-mask');
	}
}

function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
var procesarConsultaFechaPago = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
		fp.removeAll();
		icMoneda=registro.get('CLAVE_MONEDA');
		var hidBitacora=registro.get('CLAVE_MONEDA')+'|'+registro.get('FECHA_VENCIMIENTO')+'|'+registro.get('IMPORTE_DEPOSITO');
		grid.el.mask('Cargando...','x-mask');
		tb.setVisible(false);
		fechaVenc='';
		fechaProb=registro.get('FECHA_PROB_PAGO');
					Ext.Ajax.request({
							url: '13forma09Ext.data.jsp',
							params: Ext.apply({informacion:'ConsultaPagos',hidBitacora:hidBitacora,sFechaProbIni: registro.get('FECHA_PROB_PAGO'),sFechaProbFin: registro.get('FECHA_PROB_PAGO'),icMoneda:registro.get('CLAVE_MONEDA') }),
							callback: procesarConsultaSuccess
						});
	}

var procesarConsultaFechaVenc = function(grid, rowIndex, colIndex, item, event) {  
		var registro = consulta.getAt(rowIndex);
		fp.removeAll();
		icMoneda=registro.get('CLAVE_MONEDA');
		var hidBitacora=registro.get('CLAVE_MONEDA')+'|'+registro.get('FECHA_VENCIMIENTO')+'|'+registro.get('IMPORTE_DEPOSITO');
		tb.setVisible(false);
		grid.el.mask('Cargando...','x-mask');
		fechaProb='';
		fechaVenc=registro.get('FECHA_VENCIMIENTO');
					Ext.Ajax.request({
							url: '13forma09Ext.data.jsp',
							params: Ext.apply({informacion:'ConsultaPagos',hidBitacora:hidBitacora,sFechaVencIni: registro.get('FECHA_VENCIMIENTO'),sFechaVencFin: registro.get('FECHA_VENCIMIENTO'),icMoneda:registro.get('CLAVE_MONEDA') }),
							callback: procesarConsultaSuccess
						});
	}
var procesarConsultaData = function(store,arrRegistros,opts){

			if(arrRegistros!=null){
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
			 strTipoBanco=store.reader.jsonData.sTipoBanco;
			 if(strTipoBanco=='NB'){
			 }else if(strTipoBanco=='B'){
			 }
				el.unmask();
			}else{
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
		
var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('generarArchivo');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajar = Ext.getCmp('bajarArchivo');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//--------------------Stores------------------------

	var dataTotal= new Ext.data.JsonStore({
				autoLoad: false,
        fields: [
            'PRESTAMOS',
				'TOTAL_IMPORTE'
        ],
		  listeners: {exception: NE.util.mostrarDataProxyError}
				
        //data: [[totalDocumentos,totalMontoDocumentos,store.reader.jsonData.GRANTOTALMONTOCOMISION]]
				
    });

var consultaFechas = new Ext.data.JsonStore({
	
	fields: [
				{name: 'CLAVE_IF'},
				{name: 'CLAVE_MONEDA'},
				{name: 'MONEDA'},
				{name: 'FECHA_VENCIMIENTO'},
				{name: 'FECHA_PROB_PAGO'},
				{name: 'IMPORTE_DEPOSITO'},
				{name: 'CLAVE_ESTADO'},
				{name: 'IMPORTE_DEPOSITO'},
				{name: 'NOMBRE_ESTADO'},
				{name: 'SUBAPLICACION'},
				{name: 'PRESTAMO'},
				{name: 'CLAVE_SIRAC'},
				{name: 'CAPITAL'},
				{name: 'INTERESES'},
				{name: 'MORATORIOS'},
				{name: 'SUBSIDIO'},
				{name: 'COMISION'},
				{name: 'IVA'},
				{name: 'IMPORTE_PAGO'},
				{name: 'CONCEPTO_PAGO'},
				{name: 'ORIGEN_PAGO'},
				{name: 'SUCURSAL'},
				{name: 'ACREDITADO'},
				{name: 'FECHA_OPERACION'},
				{name: 'FECHA_PAGO'},
				{name: 'SALDO'},
				{name: 'TASA'},
				{name: 'DIAS'},
				{name: 'INTERESES'},
				{name: 'IMPORTE_PAGO'}
				
	],
	
	listeners: {exception: NE.util.mostrarDataProxyError}
});


var consulta = new Ext.data.JsonStore({
	root: 'registros',
	url: '13forma09Ext.data.jsp',
	baseParams: {
		informacion: 'Consulta'
	},
	fields: [
				{name: 'MONEDA'},
				{name: 'FECHA_VENCIMIENTO'},
				{name: 'FECHA_PROB_PAGO'},
				{name: 'IMPORTE_DEPOSITO'},
				{name: 'CLAVE_MONEDA'}
				
	],
	totalProperty: 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar  consulta, para que desbloquee los componentes.
					}
		}
	}
});

var grid = new Ext.grid.GridPanel({
				id: 'grid',
				hidden: false,
				header:true,
				store: consulta,
				style: 'margin:0 auto;',
				columns:[
				//CAMPOS DEL GRID
						{
						
						header:'Moneda',
						tooltip: 'Moneda',
						sortable: true,
						dataIndex: 'MONEDA',
						width: 140,
						align: 'center'
						},
						{
						xtype: 'actioncolumn',
						header: 'Fecha Vencimiento',
						tooltip: 'Fecha Vencimiento',
						sortable: true,
						dataIndex: 'FECHA_VENCIMIENTO',
						width: 140,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								return value;
							},
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
								 }
								 ,handler: procesarConsultaFechaVenc
					      }]
						},
						{
						xtype: 'actioncolumn',
						header: 'Fecha Probable de Pago',
						tooltip: 'Fecha Probable de Pago',
						dataIndex: 'FECHA_PROB_PAGO',
						sortable: true,
						width: 140,
						align: 'center',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
								return value;
							},
						items: [{
						       getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
								 }
								 ,handler: procesarConsultaFechaPago
					      }]
						},
						{
						header: 'Total a Pagar',
						tooltip: 'Total a Pagar',
						sortable: true,
						width: 110,
						dataIndex: 'IMPORTE_DEPOSITO',
						align: 'right',
						renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
						}
						
						
				
				],
				stripeRows: true,
				loadMask: true,
				height: 250,
				width: 550,
				style: 'margin:0 auto;',
				frame: true
				
		});

var gridTotales = new Ext.grid.EditorGridPanel({
		store: dataTotal,
		id: 'gridTotales',
		hidden:	true,
		style: 'margin: 0 auto;',
		columns: [
			{
				header: 'Total Prestamos',
				dataIndex: 'PRESTAMOS',
				align: 'left',	width: 350
			},{
				header: 'Total Importe de Depositos',
				dataIndex: 'TOTAL_IMPORTE',
				width: 350,	align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		width: 750,
		height: 90,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	});
var tb = new Ext.Toolbar({
    renderTo: pnl,
    width: 750,
	 style: 'margin: 0 auto;',
	hidden: true,
    frame:true,
	 items: [
        '->',{
            xtype: 'button', // default for Toolbars, same as 'tbbutton'
            text: 'Totales',
				handler: function(boton, evento){
										if(!gridTotales.isVisible()){
											gridTotales.show();
										}else{
											gridTotales.hide();
										}
									}
        },{
				hidden:false,
            xtype: 'button', // default for Toolbars, same as 'tbbutton'
            text: 'Generar Archivo',
				id: 'generarArchivo',
				handler: function(boton, evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '13forma09ExtCSV.jsp',
											params: Ext.apply({sFechaVencFin:fechaVenc,sFechaVencIni: fechaVenc,sFechaProbIni: fechaProb,sFechaProbFin:fechaProb,icMoneda:icMoneda }),
											callback: procesarSuccessFailureGenerarArchivo
										});
									}
        },
		  
		  {
				hidden:true,
            xtype: 'button', // default for Toolbars, same as 'tbbutton'
            text: 'Bajar Archivo',
				id:'bajarArchivo'
        }
    ]
});

var fp = new Ext.form.FormPanel
  ({
		hidden: true,
		height: 'auto',
		width: 750,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		//bodyStyle: 'padding: 10px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: []
	});

	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[grid,NE.util.getEspaciador(30),fp,tb,gridTotales]
	});
	consulta.load();

});