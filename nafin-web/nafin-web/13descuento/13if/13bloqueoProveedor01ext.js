Ext.onReady(function(){

//------------------------------ Variables globales -------------------
	var banderaCC    = ''; // Posibles valores: CAPTURA - CONSULTA
	var grid_ic_if   = '';
	var grid_ic_epo  = '';
	var grid_ic_pyme = '';
	var grid_ic_ne   = '';

//------------------------------ Handlers -----------------------------

	// Si el registro ya est� bloqueado, se muestra en color rojo
	function renderColorBloqueo(value, metaData, record, rowIndex, colIndex, store){
		if (record.data.BLOQUEO_ANT == 'S')
			return '<font color="red">'+value+'</font>';
		else
			return value;
	}

	function renderColorBloqueoBis(value, metaData, record, rowIndex, colIndex, store){
		var valor = record.data.CAUSA_BLOQUEO;
		if (record.data.BLOQUEO_ANT == 'S')
			return '<font color="red">'+valor+'</font>';
		else
			return valor;
	}

	// Este es para el grid de consulta, muestra si ya est� bloqueado o no
	function rendererBloqueoHistorico(value, metaData, record, rowIndex, colIndex, store){
		if (record.data.BLOQUEO_ANT == 'S')
			return '<font color="red">Bloqueado</font>';
		else
			return 'Desbloqueado';
	}

	// Procesa las condiciones iniciales de la pantalla
	function valoresIniciales(){
		// Por default la pantalla se encuentra en captura
		banderaCC = 'CAPTURA';
		// Realizo la consulta del cat�logo EPO
		catalogoEPO.load();
	}

	// Realiza la consulta para generar el grid de Consulta/Captura
	function buscar(){

		var forma  = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var cmpNe  = forma.query('#nafin_electronico_id')[0].getValue(); // El campo tipo numerico es nulo cuando viene vac�o.
		var cmpRfc = forma.query('#rfc_id')[0].getValue();

		if(banderaCC == 'CAPTURA'){
			if((cmpNe == '' || cmpNe == null) && (cmpRfc == '' || cmpRfc == null)){
				Ext.Msg.alert('Mensaje', '<TABLE width=240 cellspacing="0" cellpadding="0"><TR><TD width=240 align=center> Capture el N@E Pyme o el RFC para realizar la consulta </TD></TR></TABLE>');
				return;
			}
		}

		main.el.mask('Procesando...', 'x-mask-loading');
		var grid = Ext.ComponentQuery.query('#gridCaptura')[0];
		grid.hide();
		grid = Ext.ComponentQuery.query('#gridHistorico')[0];
		grid.hide();
		consultaData.load({
			params: Ext.apply({
				nafin_electronico: forma.query('#nafin_electronico_id')[0].getValue(),
				rfc:               forma.query('#rfc_id')[0].getValue(),
				ic_epo:            forma.query('#epo_id')[0].getValue()
			})
		});

	}

	// Indica que la pantalla se encuentra en modo de Captura
	function verPantallaCaptura(){

		banderaCC = 'CAPTURA';

		var grid = Ext.ComponentQuery.query('#gridCaptura')[0];
		grid.hide();
		grid = Ext.ComponentQuery.query('#gridHistorico')[0];
		grid.hide();
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		forma.getForm().reset();
		forma.setTitle('Captura');
		forma = Ext.ComponentQuery.query('#menuToolbar')[0];
		boton = forma.query('#btnCaptura')[0];
		boton.setText('<b>Captura</b>');
		boton = forma.query('#btnConsulta')[0];
		boton.setText('Consulta');

		grid_ic_if   = '';
		grid_ic_epo  = '';
		grid_ic_pyme = '';
		grid_ic_ne   = '';

	}

	// Indica que la pantalla se encuentra en modo de Consulta
	function verPantallaConsulta(){

		banderaCC = 'CONSULTA';

		var grid = Ext.ComponentQuery.query('#gridCaptura')[0];
		grid.hide();
		grid = Ext.ComponentQuery.query('#gridHistorico')[0];
		grid.hide();
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		forma.getForm().reset();
		forma.setTitle('Consulta');
		forma = Ext.ComponentQuery.query('#menuToolbar')[0];
		boton = forma.query('#btnCaptura')[0];
		boton.setText('Captura');
		boton = forma.query('#btnConsulta')[0];
		boton.setText('<b>Consulta</b>');

		grid_ic_if   = '';
		grid_ic_epo  = '';
		grid_ic_pyme = '';
		grid_ic_ne   = '';

	}

	// Bloquea los registros seleccionados en el grid de captura
	function confirmaBloqueo(){

		var gridCaptura = Ext.ComponentQuery.query('#gridCaptura')[0];
		var records     = consultaData.getRange();
		var edit        = gridCaptura.editingPlugin;
		var banderaOk   = true;
		var contador    = 0;

		// Primero valido los campos obligatorios del grid
		for (var i = 0; i < records.length; i++){
			if(records[i].data.BLOQUEO_ACT == true){
				contador++;
			}
			if(records[i].data.BLOQUEO_ACT == true && records[i].data.CAUSA_BLOQUEO == ''){
				banderaOk = false;
				Ext.MessageBox.alert('Mensaje','<TABLE width=240 cellspacing="0" cellpadding="0"><TR><TD width=240 align=center> Es necesario indicar las causas de los registros seleccionados </TD></TR></TABLE>',
					function(){
						edit.startEditByPosition({row: i, column: 10 });
						return;
					}
				);
				break;
			}
		}

		if(contador == 0){
			// No se seleccionaron registros
			Ext.MessageBox.alert('Mensaje','<TABLE width=240 cellspacing="0" cellpadding="0"><TR><TD width=240 align=center> Seleccione al menos un registro </TD></TR></TABLE>');
			return
		}

		if(banderaOk == true){

			Ext.MessageBox.confirm('Mensaje','<TABLE width=240 cellspacing="0" cellpadding="0"><TR><TD width=240 align=center> �Est� seguro de querer enviar su informaci�n? </TD></TR></TABLE>', function(resp){
				if(resp =='yes'){
					// Si todo es correcto, env�o los datos
					var jsonDataEncode = '';
					var datar = new Array();
					for (var i = 0; i < records.length; i++) {
						datar.push(records[i].data);
					}
					jsonDataEncode = Ext.encode(datar);
					main.el.mask('Procesando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '13bloqueoProveedor01ext.data.jsp',
						params: Ext.apply({
							informacion: 'CONFIRMA_BLOQUEO',
							datos_grid:  jsonDataEncode
						}),
						callback: procesaConfirmaBloqueo
					});
				}
			}, Ext.MessageBox.QUESTION);

		} else{
			return;
		}

	}

	// Muestra elgrid de historial de bloqueos/desbloqueos
	var verHistorial = function(grid, rowIndex, colIndex){
		var rec = consultaData.getAt(rowIndex);
		grid_ic_if = rec.get('IC_IF');
		grid_ic_epo = rec.get('IC_EPO');
		grid_ic_pyme = rec.get('IC_PYME');
		grid_ic_ne   = rec.get('NE');
		main.el.mask('Procesando...', 'x-mask-loading');
		consultaHistorico.load({
			params: Ext.apply({
				ic_if:             grid_ic_if,
				ic_epo:            grid_ic_epo,
				ic_pyme:           grid_ic_pyme,
				nafin_electronico: grid_ic_ne
			})
		});
	}

	// Cierra el grid Captura/Consulta
	function cerrarGrid(){
		if(banderaCC = 'CAPTURA'){
			verPantallaCaptura();
		} else if(banderaCC = 'CONSULTA'){
			verPantallaConsulta();
		}
	}

	// Cierra el grid Hist�rico
	function cerrarGridHistorico(){
		var grid = Ext.ComponentQuery.query('#gridHistorico')[0];
		grid.hide();
		grid = Ext.ComponentQuery.query('#gridCaptura')[0];
		grid.show();
	}

	// Manda a descargar el pdf del grid consulta
	function descargaPdfConsulta(){
		var forma = Ext.ComponentQuery.query('#formaPrincipal')[0];
		var grid  = Ext.ComponentQuery.query('#gridCaptura')[0];
		var boton = grid.query('#btnGenerarPdf')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '13bloqueoProveedor01ext.data.jsp',
			params: Ext.apply({
				informacion:       'DESCARGA_PDF_CONSULTA',
				nafin_electronico: forma.query('#nafin_electronico_id')[0].getValue(),
				rfc:               forma.query('#rfc_id')[0].getValue(),
				ic_epo:            forma.query('#epo_id')[0].getValue(),
				_botonId:          boton.getId()
			}),
			callback: procesaGeneraArchivo
		});
	}

	// Manda a descargar el pdf del grid HIST�RICO
	function descargaPdfHistorico(){
		var grid  = Ext.ComponentQuery.query('#gridHistorico')[0];
		var boton = grid.query('#btnImprimirPdf')[0];
		boton.disable();
		boton.setIconCls('x-mask-msg-text');
		Ext.Ajax.request({
			url: '13bloqueoProveedor01ext.data.jsp',
			params: Ext.apply({
				informacion:       'DESCARGA_PDF_HISTORICO',
				ic_if:             grid_ic_if,
				ic_epo:            grid_ic_epo,
				ic_pyme:           grid_ic_pyme,
				nafin_electronico: grid_ic_ne,
				_botonId:          boton.getId()
			}),
			callback: procesaGeneraArchivo
		});
	}

//------------------------------ Callback -----------------------------

	// Proceso posterior a la consulta del grid
	var procesarConsultaData = function(store, arrRegistros, success, opts){
		var grid = Ext.ComponentQuery.query('#gridCaptura')[0];
		if (arrRegistros != null){
			grid.show();
			if(store.getTotalCount() > 0){

				if(banderaCC == 'CAPTURA'){

					grid.setTitle('Captura');
					grid.query('#btnGenerarPdf')[0].hide();
					grid.query('#btnConfirmar')[0].show();
					grid.query('#btnConfirmar')[0].enable();
					grid.query('#btnCancelarGrid')[0].show();
					grid.query('#btnCancelarGrid')[0].enable();
					grid.down("gridcolumn[dataIndex=BLOQUEO_HISTORICO]").hide();
					grid.down("gridcolumn[dataIndex=CAUSA_BLOQUEO_LEC]").hide();
					grid.down("gridcolumn[dataIndex=USUARIO]").hide();
					grid.down("gridcolumn[dataIndex=BLOQUEO_ACT]").show();
					grid.down("gridcolumn[dataIndex=CAUSA_BLOQUEO]").show();
					

				} else if(banderaCC == 'CONSULTA'){

					grid.setTitle('Consulta');
					grid.query('#btnGenerarPdf')[0].show();
					grid.query('#btnGenerarPdf')[0].enable();
					grid.query('#btnConfirmar')[0].hide();
					grid.query('#btnCancelarGrid')[0].hide();
					grid.down("gridcolumn[dataIndex=BLOQUEO_HISTORICO]").show();
					grid.down("gridcolumn[dataIndex=CAUSA_BLOQUEO_LEC]").show();
					grid.down("gridcolumn[dataIndex=USUARIO]").show();
					grid.down("gridcolumn[dataIndex=BLOQUEO_ACT]").hide();
					grid.down("gridcolumn[dataIndex=CAUSA_BLOQUEO]").hide();

				}

			} else{

				if(banderaCC == 'CAPTURA'){

					grid.setTitle('Captura');
					grid.query('#btnGenerarPdf')[0].hide();
					grid.query('#btnConfirmar')[0].show();
					grid.query('#btnConfirmar')[0].disable();
					grid.query('#btnCancelarGrid')[0].show();
					grid.query('#btnCancelarGrid')[0].enable();

				} else if(banderaCC == 'CONSULTA'){

					grid.setTitle('Consulta');
					grid.query('#btnGenerarPdf')[0].show();
					grid.query('#btnGenerarPdf')[0].disable();
					grid.query('#btnConfirmar')[0].hide();
					grid.query('#btnCancelarGrid')[0].hide();

				}

				grid.getView().getEl().mask('<TABLE width=240 cellspacing="0" cellpadding="0"><TR><TD width=240 align=center>No se encontraron registros </TD></TR></TABLE>');

			}
		}
		main.el.unmask();
	}

	// Proceso posterior a la consulta del grid Hist�rico 
	var procesarConsultaHistorico = function(store, arrRegistros, success, opts){
		var grid = Ext.ComponentQuery.query('#gridCaptura')[0];
		grid.hide();
		grid = Ext.ComponentQuery.query('#gridHistorico')[0];
		if (arrRegistros != null){
			grid.show();
			if(store.getTotalCount() > 0){
				grid.query('#btnImprimirPdf')[0].enable();
			} else{
				grid.query('#btnImprimirPdf')[0].disable();
				grid.getView().getEl().mask('<TABLE width=240 cellspacing="0" cellpadding="0"><TR><TD width=240 align=center> No se encontraron registros </TD></TR></TABLE>');
			}
		}
		main.el.unmask();
	}

	// Se procesa la respuesta del proceso de bloqueo, el mensaje indica si fue exitoso o no
	var procesaConfirmaBloqueo = function(options, success, response){
		main.el.unmask();
		if (success == true && Ext.JSON.decode(response.responseText).success == true){
			var jsonData = Ext.JSON.decode(response.responseText);
			Ext.MessageBox.alert('Mensaje', '<TABLE width=240 cellspacing="0" cellpadding="0"><TR><TD width=240 align=center>' + jsonData.mensaje + '</TD></TR></TABLE>');
			var forma  = Ext.ComponentQuery.query('#formaPrincipal')[0];
			main.el.mask('Procesando...', 'x-mask-loading');
			consultaData.load({
				params: Ext.apply({
					nafin_electronico: forma.query('#nafin_electronico_id')[0].getValue(),
					rfc:               forma.query('#rfc_id')[0].getValue(),
					ic_epo:            forma.query('#epo_id')[0].getValue()
				})
			});
		} else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	// Handler que procesa respuesta creacion archivo
	function procesaGeneraArchivo(opts, success, response){

		// PARSEAR RESPUESTA DEL SERVIDOR
		var resp = null;
		try {
			resp = Ext.JSON.decode(response.responseText);
		} catch(error) {
			NE.util.mostrarErrorResponse4(
				{ status: -1 }, // Para que no asuma que es un est�tus devuelto por el servidor
				error.name + " - " + error.message
			);
			return;
		}

		// REALIZAR LA DESCARGA AUTOM�TICA DEL ARCHIVO
		if ( success == true && resp.success == true ) {

			var forma    = Ext.getDom('formAux');
			forma.action = NE.appWebContextRoot+'/DescargaArchivo';
			forma.method = 'post';
			forma.target = '_self';

			// Suprimir el Context root del url del archivo
			var urlArchivo = resp.urlArchivo;
			nombreArchivo  = urlArchivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');

			// Insertar Nombre Archivo
			var inputNombreArchivo = Ext.DomHelper.insertFirst(
				forma,
				{
					tag:   'input',
					type:  'hidden',
					id:    'nombreArchivo',
					name:  'nombreArchivo',
					value: nombreArchivo
				},
				true
			);
			// Solicitar Archivo Al Servidor
			forma.submit();
			// Remover nodo agregado
			inputNombreArchivo.remove();

		} else {
			NE.util.mostrarErrorResponse4(response); // Se muestra si hay una excepci�n
		}

		var boton = Ext.getCmp(opts.params._botonId);
		boton.setIconCls('icoPdf');
		boton.enable();

	}

//------------------------------ Stores ------------------------------
	// Se crea el MODEL para el grid 'Captura'
	Ext.define('ListaCaptura',{
		extend: 'Ext.data.Model',
		fields: [
			{name: 'IC_IF'            },
			{name: 'IC_PYME'          },
			{name: 'IC_EPO'           },
			{name: 'BLOQUEO_ANT'      },
			{name: 'BLOQUEO_ACT', type: 'bool'},
			{name: 'EPO'              },
			{name: 'PYME'             },
			{name: 'NE'               },
			{name: 'RFC'              },
			{name: 'EMAIL'            },
			{name: 'FECHA_MOD'        },
			{name: 'CAUSA_BLOQUEO'    },
			{name: 'CAUSA_BLOQUEO_LEC'},
			{name: 'BLOQUEO_HISTORICO'},
			{name: 'USUARIO'          }
		]
	});

	// Se crea el MODEL para el cat�logo 'EPO'
	Ext.define('ModelCatologoEPO',{
		extend: 'Ext.data.Model',
			fields: [
				{ name: 'clave',       type: 'string'},
				{ name: 'descripcion', type: 'string'}
			]
		}
	);

	// Se crea el STORE del grid 'Captura/Consulta'
	var consultaData = Ext.create('Ext.data.Store',{
		storeId:            'consultaData',
		model:              'ListaCaptura',
		proxy:              {
			type:            'ajax',
			url:             '13bloqueoProveedor01ext.data.jsp',
			reader:          {
				type:         'json',
				root:         'registros'
			},
			extraParams:     {
				informacion:  'CONSULTA_DATA'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners:       {
				exception:    NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultaData
		}
	});

	// Se crea el STORE del grid 'Hist�rico'
	var consultaHistorico = Ext.create('Ext.data.Store',{
		storeId:            'consultaHistorico',
		model:              'ListaCaptura',
		proxy:              {
			type:            'ajax',
			url:             '13bloqueoProveedor01ext.data.jsp',
			reader:          {
				type:         'json',
				root:         'registros'
			},
			extraParams:     {
				informacion:  'CONSULTA_HISTORICO'
			},
			totalProperty:   'total',
			messageProperty: 'msg',
			autoLoad:        false,
			listeners:       {
				exception:    NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false,
		listeners: {
			load: procesarConsultaHistorico
		}
	});

	// Se crea el STORE para el cat�logo 'EPO'
	var catalogoEPO = Ext.create('Ext.data.Store',{
		model: 'ModelCatologoEPO',
		proxy: {
			type: 'ajax',
			url: '13bloqueoProveedor01ext.data.jsp',
			reader: {
				type: 'json',
				root: 'registros'
			},
			extraParams: {
				informacion: 'CATALOGO_EPO'
			},
			autoLoad: false,
			listeners: {
				exception: NE.util.mostrarProxyAjaxError
			}
		},
		autoLoad: false
	});

//------------------------------ Componentes --------------------------

	// Se crea el grid de captura/Consulta
	var gridCaptura = Ext.create('Ext.grid.Panel',{
		store:            Ext.data.StoreManager.lookup('consultaData'),
		plugins:          [Ext.create('Ext.grid.plugin.CellEditing',{clicksToEdit: 1})],
		height:           350,
		width:            900,
		xtype:            'cell-editing',
		itemId:           'gridCaptura',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'Captura',
		border:           true,
		stripeRows:       true,
		hidden:           true,
		frame:            false,
		enableColumnMove: false,
		enableColumnHide: false,
		columns: [{
			width:         200,
			dataIndex:     'EPO',
			header:        'EPO',
			align:         'left',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         200,
			dataIndex:     'PYME',
			header:        'Pyme',
			align:         'left',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         160,
			dataIndex:     'NE',
			header:        'N@E',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         120,
			dataIndex:     'RFC',
			header:        'RFC',
			align:         'left',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         200,
			dataIndex:     'EMAIL',
			header:        'Email',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         150,
			xtype:         'actioncolumn',
			dataIndex:     'BLOQUEO_HISTORICO',
			header:        'Bloqueo Hist�rico',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      rendererBloqueoHistorico,
			items: [{
				iconCls:    'icoAceptar',
				handler:    verHistorial
			}]
		},{
			width:         90,
			dataIndex:     'BLOQUEO_ACT',
			header:        'Seleccionar',
			xtype:         'checkcolumn',
			sortable:      false,
			resizable:     false,
			stopSelection: false
		},{
			width:         160,
			dataIndex:     'FECHA_MOD',
			header:        'Fecha de Modificaci�n',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         120,
			dataIndex:     'USUARIO',
			header:        'Usuario',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         160,
			dataIndex:     'CAUSA_BLOQUEO_LEC',
			header:        'Causa del Bloqueo',
			align:         'left',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueoBis
		},{
			width:         200,
			dataIndex:     'CAUSA_BLOQUEO',
			header:        'Causa del Bloqueo',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo,
			editor: {
			xtype:         'textarea',
				maxLength:  120,
				height:     50
			}
		}],
		bbar: ['->','-',
			{xtype:'button', itemId:'btnGenerarPdf',   iconCls:'icoPdf',      text:'Generar Archivo', handler:descargaPdfConsulta },'-',
			{xtype:'button', itemId:'btnConfirmar',    iconCls:'icoAceptar',  text:'Confirmar',       handler:confirmaBloqueo     },'-',
			{xtype:'button', itemId:'btnCancelarGrid', iconCls:'icoCancelar', text:'Cancelar',        handler:cerrarGrid          }
		]
	});


	// Se crea el grid Hist�rico
	var gridHistorico = Ext.create('Ext.grid.Panel',{
		store:            Ext.data.StoreManager.lookup('consultaHistorico'),
		height:           350,
		width:            900,
		itemId:           'gridHistorico',
		style:            'margin:0 auto;',
		bodyStyle:        'padding: 6px',
		title:            'Hist�rico bloqueo y desbloqueo',
		border:           true,
		stripeRows:       true,
		hidden:           true,
		frame:            false,
		columns: [{
			width:         200,
			dataIndex:     'EPO',
			header:        'EPO',
			align:         'left',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         200,
			dataIndex:     'PYME',
			header:        'Pyme',
			align:         'left',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         160,
			dataIndex:     'NE',
			header:        'N@E',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         120,
			dataIndex:     'RFC',
			header:        'RFC',
			align:         'left',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         200,
			dataIndex:     'EMAIL',
			header:        'Email',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         150,
			dataIndex:     'BLOQUEO_HISTORICO',
			header:        'Bloqueo Hist�rico',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      rendererBloqueoHistorico
		},{
			width:         160,
			dataIndex:     'FECHA_MOD',
			header:        'Fecha y Hora Bloqueo',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         120,
			dataIndex:     'USUARIO',
			header:        'Usuario',
			align:         'center',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		},{
			width:         160,
			dataIndex:     'CAUSA_BLOQUEO',
			header:        'Causa del Bloqueo',
			align:         'left',
			sortable:      true,
			resizable:     true,
			renderer:      renderColorBloqueo
		}],
		bbar: ['->','-',
			{xtype:'button', itemId:'btnImprimirPdf', iconCls:'icoPdf',      text:'Imprimir', handler:descargaPdfHistorico},'-',
			{xtype:'button', itemId:'btnCerrarGrid',  iconCls:'icoCancelar', text:'Cerrar',   handler:cerrarGridHistorico }
		]
	});

	// Estos botones permiten cambiar entre la vista de captura o consulta
	var menuToolbar = Ext.create('Ext.toolbar.Toolbar', {
		width:       600,
		itemId:      'menuToolbar',
		bodyPadding: '12 6 12 6',
		style:       'margin: 0px auto 0px auto;',
		frame:       true,
		border:      true,
		items: [{
			width:    295,
			itemId:   'btnCaptura',
			text:     '<b>Captura</b>',
			handler:  verPantallaCaptura
		},'-',{
			width:    295,
			itemId:   'btnConsulta',
			text:     'Consulta',
			handler:  verPantallaConsulta
		}]
	});

	// Se crea el form principal
	var formaPrincipal = Ext.create( 'Ext.form.Panel',{
		width:             600,
		itemId:            'formaPrincipal',
		title:             'Captura',
		bodyPadding:       '12 6 12 6',
		style:             'margin: 0px auto 0px auto;',
		frame:             true,
		border:            true,
		fieldDefaults:{
			msgTarget:      'side',
			labelWidth:     80
		},
		items:[{
			anchor:         '95%',
			xtype:          'numberfield',
			itemId:         'nafin_electronico_id',
			name:           'nafin_electronico',
			hiddenName:     'nafin_electronico',
			fieldLabel:     'N@E PYME',
			allowDecimals:  true,
			maxLength:      38,
			minValue:       0
		},{
			anchor:         '95%',
			xtype:          'textfield',
			itemId:         'rfc_id',
			name:           'rfc',
			hiddenName:     'rfc',
			fieldLabel:     'RFC'
		},{
			anchor:         '95%',
			xtype:          'combobox',
			itemId:         'epo_id',
			name:           'epo',
			hiddenName:     'epo',
			fieldLabel:     'EPO',
			emptyText:      'Seleccione ...',
			displayField:   'descripcion',
			valueField:     'clave',
			queryMode:      'local',
			triggerAction:  'all',
			listClass:      'x-combo-list-small',
			typeAhead:      true,
			selectOnTab:    true,
			lazyRender:     true,
			forceSelection: true,
			store:          catalogoEPO
		}],
		buttons: [{
			text:           'Buscar',
			itemId:         'btnBuscar',
			iconCls:        'icoBuscar',
			handler:        buscar
		},{
			text:           'Cancelar',
			itemId:         'btnCancelar',
			iconCls:        'icoCancelar',
			handler:        verPantallaCaptura
		}]
	});

	// Contenedor principal
	var main = Ext.create('Ext.container.Container',{
		width:      949,
		minHeight:  650,
		autoHeight: true,
		id:         'contenedorPrincipal',
		renderTo:   'areaContenido',
		style:      'margin:0 auto;',
		items: [
			NE.util.getEspaciador(10),
			menuToolbar,
			formaPrincipal,
			NE.util.getEspaciador(20),
			gridCaptura,
			gridHistorico
		]
	});

	valoresIniciales();

});