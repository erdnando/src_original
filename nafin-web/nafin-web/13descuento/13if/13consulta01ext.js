Ext.onReady(function() {
	var numero;

	var VisorAcusesDeCarga = function(rec){
		var ic_acuse = rec.get('CC_ACUSE');
		if(ic_acuse != ''){
			var win = new NE.AcusesDeCarga.VisorAcusesDeCarga(ic_acuse);
		}
	}

	var procesarConDetalleData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
		var gridDetalle = Ext.getCmp('gridDetalle');
	
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) {
				gridDetalle.show();
			}					
	
			var jsonData = store.reader.jsonData;	
	
			var btnImpDetallePDF = Ext.getCmp('btnImpDetallePDF');
			var btnBajarImpDetallePDF = Ext.getCmp('btnBajarImpDetallePDF');
			var btnArchiDetalleCsv = Ext.getCmp('btnArchiDetalleCsv');
			var btnBajarArchiDetalleCsv = Ext.getCmp('btnBajarArchiDetalleCsv');
			var gridTotalesDetalle = Ext.getCmp('gridTotalesDetalle');
			btnBajarImpDetallePDF.hide();
			btnBajarArchiDetalleCsv.hide();
					
			gridDetalle.setTitle('<div style="float:left">N�mero de documento: '+jsonData.claveDocumento+'</div><div style="float:right"> Monto aplicado '+jsonData.monto+'</div>');						
						
			if(store.getTotalCount() > 0) {	
			
				consTotalDetalleData.load({
					params:	{
						clavePyme:	jsonData.clavePyme,
						claveDocumento:jsonData.claveDocumento
					}
				});	
				
				gridTotalesDetalle.show();
			
				btnImpDetallePDF.enable();
				btnArchiDetalleCsv.enable();
				el.unmask();							
			} else {	
				btnImpDetallePDF.disable();
				btnArchiDetalleCsv.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var procesarSuccessFailureImpDetallePDF =  function(opts, success, response) {
		var btnImpDetallePDF = Ext.getCmp('btnImpDetallePDF');
		btnImpDetallePDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarImpDetallePDF = Ext.getCmp('btnBajarImpDetallePDF');
			btnBajarImpDetallePDF.show();
			btnBajarImpDetallePDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarImpDetallePDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					var infoR = Ext.util.JSON.decode(response.responseText);
					var archivo = infoR.urlArchivo;				
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};				
					fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					fp.getForm().getEl().dom.submit();
					
				}
			);
		} else {
			btnImpDetallePDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var procesarSuccessFailureImpDetalleCsv =  function(opts, success, response) {
		var btnArchiDetalleCsv = Ext.getCmp('btnArchiDetalleCsv');
		btnArchiDetalleCsv.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchiDetalleCsv = Ext.getCmp('btnBajarArchiDetalleCsv');
			btnBajarArchiDetalleCsv.show();
			btnBajarArchiDetalleCsv.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchiDetalleCsv.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					var infoR = Ext.util.JSON.decode(response.responseText);
					var archivo = infoR.urlArchivo;				
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};				
					fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					fp.getForm().getEl().dom.submit();
				}
			);
		} else {
			btnArchiDetalleCsv.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var verDetalleAplicacion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		
		var clavePyme = registro.get('IC_PYME');
		var claveDocumento = registro.get('IC_DOCUMENTO');
		
			consultaDetalleData.load({
				params:	{
					clavePyme:	clavePyme,
					claveDocumento:claveDocumento
				}
			});	
					
		var ventana = Ext.getCmp('verDetalle');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 830,
				height: 'auto',			
				id: 'verDetalle',
				closeAction: 'hide',
				autoDestroy:true,
				closable:false,
				items: [	
				gridDetalle,
				gridTotalesDetalle
				],
				title: 'Detalle de los cr�ditos pagados con el documento ',
				bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{
							xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrarDe', 
							handler: function(){
								Ext.getCmp('gridDetalle').hide();
								Ext.getCmp('verDetalle').destroy();
							} 
						},
						'-',
						{
							xtype: 'button',
							text: 'Generar Archivo',
							tooltip:	'Generar Archivo',
							id: 'btnArchiDetalleCsv',	
							handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								var barraPaginacion = Ext.getCmp("barraPaginacion");
								Ext.Ajax.request({
									url: '13consulta01bext.data.jsp',
									params:	{
										informacion:'ImpCsvDetalle',
										clavePyme:	clavePyme,
										claveDocumento:claveDocumento
									}					
									,callback: procesarSuccessFailureImpDetalleCsv
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip:	'Bajar Archivo',
							id: 'btnBajarArchiDetalleCsv',
							hidden: true
						},
						{
							xtype: 'button',
							text: 'Imprimir',
							tooltip:	'Imprimir',
							id: 'btnImpDetallePDF',	
							handler: function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
								var barraPaginacion = Ext.getCmp("barraPaginacion");
								Ext.Ajax.request({
									url: '13consulta01bext.data.jsp',
									params:	{
										informacion:'ImpPDFDetalle',										
										clavePyme:	clavePyme,
										claveDocumento:claveDocumento
									}					
									,callback: procesarSuccessFailureImpDetallePDF
								});
							}
						},
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip:	'Bajar Archivo',
							id: 'btnBajarImpDetallePDF',
							hidden: true
						}
					]
				}
			}).show();
		}	
	}	
	
	var consTotalDetalleData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta01bext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotalDetalle'
		},
		fields: [
			{name: 'MONEDA'},	
			{name: 'IMPORTE_PYME'},	
			{name: 'IMPORTE_APLICADO'},	
			{name: 'SALDO_CREDITO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {							
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.								
				}
			}
		}
	});

	var gridTotalesDetalle = {
		xtype: 'editorgrid',
		store: consTotalDetalleData,
		id: 'gridTotalesDetalle',		
		hidden: true,		
		columns: [		
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'Importe a depositar de la PYME', 
				tooltip: 'Importe a depositar de la PYME',
				dataIndex: 'IMPORTE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Importe Aplicado a Cr�dito', 
				tooltip: 'Total Importe Aplicado a Cr�dito',
				dataIndex: 'IMPORTE_APLICADO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Saldo del Cr�dito', 
				tooltip: 'Total Saldo del Cr�dito',
				dataIndex: 'SALDO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}			
		],
		height: 120,
		width: 800
	}
	
	
	var consultaDetalleData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta01bext.data.jsp',
		baseParams: {
			informacion: 'ConsultarDetalle'
		},
		fields: [
			{name: 'PRODUCTO'},	
			{name: 'NOMBRE_IF'},
			{name: 'ORDER_APLICACION'},	
			{name: 'FECHA_APLICACION'},	
			{name: 'FECHA_OPERACION'},	
			{name: 'NUMERO_MENSUALIDAD'},	
			{name: 'NUMERO_PEDIDO'},	
			{name: 'TIPO_CONTRATO'},	
			{name: 'NUMERO_PRESTAMO'},	
			{name: 'IMPORTE_APLICADO'},	
			{name: 'SALDO_CREDITO'}					
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConDetalleData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarConDetalleData(null, null, null);						
				}
			}
		}
	});	
			
	var gridDetalle = {
		xtype: 'editorgrid',
		store: consultaDetalleData,
		id: 'gridDetalle',		
		hidden: true,
		title: 'Detalle de los cr�ditos pagados con el documento ',
		columns: [		
			{
				header: 'Producto', 
				tooltip: 'Producto',
				dataIndex: 'PRODUCTO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'IF', 
				tooltip: 'IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Orden de Aplicaci�n', 
				tooltip: 'Orden de Aplicaci�n',
				dataIndex: 'ORDER_APLICACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Fecha de Aplicaci�n', 
				tooltip: 'Fecha de Aplicaci�n',
				dataIndex: 'FECHA_APLICACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Fecha de operaci�n cr�dito', 
				tooltip: 'Fecha de operaci�n cr�dito',
				dataIndex: 'FECHA_OPERACION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'N�mero de mensualidad', 
				tooltip: 'N�mero de mensualidad',
				dataIndex: 'NUMERO_MENSUALIDAD',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'N�mero de Pedido / Disposici�n', 
				tooltip: 'N�mero de Pedido / Disposici�n',
				dataIndex: 'NUMERO_PEDIDO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Tipo de Contrato', 
				tooltip: 'Tipo de Contrato',
				dataIndex: 'TIPO_CONTRATO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'N�mero de Pr�stamo NAFIN', 
				tooltip: 'N�mero de Pr�stamo NAFIN',
				dataIndex: 'NUMERO_PRESTAMO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Importe Aplicado a Cr�dito', 
				tooltip: 'Importe Aplicado a Cr�dito',
				dataIndex: 'IMPORTE_APLICADO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Saldo Cr�dito', 
				tooltip: 'Saldo Cr�dito',
				dataIndex: 'SALDO_CREDITO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 150,
		width: 800
	}
		

	function procesarSuccessFailureImprimir(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.getCmp('btnImprimirInterfase').setIconClass('');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesarSuccessFailureFija(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnGenerarFija = Ext.getCmp('btnGenerarFija');
			btnGenerarFija.setIconClass('');		
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
		
	var procesarSuccessFailureInterVariableNota =  function(opts, success, response) {
		var btnInterVariableNota = Ext.getCmp('btnInterVariableNota');
		btnInterVariableNota.setIconClass('');
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			var archivoMD5 = infoR.urlArchivoMD5;

			var btnBajarInterVariableNota = Ext.getCmp('btnBajarInterVariableNota');
			btnBajarInterVariableNota.show();
			btnBajarInterVariableNota.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarInterVariableNota.setHandler( 
				function(boton, evento) {
					var params = {nombreArchivo: archivo};
					window.location = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				}
			);
			
			var bOperaPubHash = Ext.getCmp("bOperaPubHash").getValue();
			var btnVariableHashNota = Ext.getCmp('btnVariableHashNota');
			if(bOperaPubHash == 'S') {
				btnVariableHashNota.show();
				btnVariableHashNota.setHandler( 
					function(boton, evento) {
						var params = {nombreArchivo: archivoMD5};
						window.location = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					}
				);
			} else {
				btnVariableHashNota.hide();
			}
		} else {
			btnInterVariableNota.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarInterfazVariableNotas = function() {
	
		var ventana = Ext.getCmp('InterfazVariableNotas');
		if (ventana) {
			ventana.show();
		}else {			
			new Ext.Window({			
				width: 300,				
				height: 150,
				autoScroll:true, 
				resizable: false,
				closeAction: 'hide',
				id: 'InterfazVariableNotas',
				autoDestroy:true,
				closable:false,
				align: 'center',				
				items: [
					NE.util.getEspaciador(20),
					fpInterfazVariableNotas,		
					NE.util.getEspaciador(20)
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	buttons: [
						{
							xtype: 'button',	text: 'Cerrar',id: 'btnCerrarIVN', 
							handler: function(){
								Ext.getCmp('InterfazVariableNotas').hide();																	
							} 
						}
					]
				},
				title: 'Exportar Interfase Variable Notas de Cr�dito'
			}).show();
		}
	}

	var fpInterfazVariableNotas = {
		xtype: 'container',
		id: 'fpInterfazVariableNotas',
		layout: 'table',		
		align: 'center',
		style: 'margin:0 auto;',			
		width:	300,
		heigth:	120,
		items:[	
			{
				xtype: 'displayfield',
				value: '       ',
				width: 30
			},
			{
				xtype: 'button',				
				text: 'Generar Archivo',					
				id: 'btnInterVariableNota',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta01ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion:'ArchivoCSV_TXT',
						tipoArchivo:'IVARIABLED',
						extensionArchivo:'CSV'
					})					
					,callback: procesarSuccessFailureInterVariableNota
					});
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarInterVariableNota',
				hidden: true
			},
			{
				xtype: 'button',				
				text: 'Generar Clave Hash',					
				id: 'btnVariableHashNota',
				hidden: true
			}
		]
	}
	

	var procesarSuccessFailureInterVariable =  function(opts, success, response) {
		var btnInterVariable = Ext.getCmp('btnInterVariable');
		btnInterVariable.setIconClass('');
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			var archivoMD5 = infoR.urlArchivoMD5;
			
			var btnBajarInterVariable = Ext.getCmp('btnBajarInterVariable');
			btnBajarInterVariable.show();
			btnBajarInterVariable.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarInterVariable.setHandler( 
				function(boton, evento) {
					var params = {nombreArchivo: archivo};
					window.location = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				}
			);
			
			var bOperaPubHash = Ext.getCmp("bOperaPubHash").getValue();
			var btnVariableHash = Ext.getCmp('btnVariableHash');
			if(bOperaPubHash == 'S') {
				btnVariableHash.show();
				btnVariableHash.setHandler( 
					function(boton, evento) {
						var params = {nombreArchivo: archivoMD5};
						window.location = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					}
				);
			} else {
				btnVariableHash.hide();
			}
		} else {
			btnInterVariable.enable();
			NE.util.mostrarConnError(response,opts);
		}
	} 
	
	
	var procesarInterfazVariable = function() {
	
		var ventana = Ext.getCmp('InterfazVariable');
		if (ventana) {
			ventana.show();
		}else {			
			new Ext.Window({			
				width: 300,				
				height: 150,
				autoScroll:true, 
				resizable: false,
				closeAction: 'hide',
				id: 'InterfazVariable',
				autoDestroy:true,
				closable:false,
				align: 'center',				
				items: [
					NE.util.getEspaciador(20),
					fpInterfazVariable,		
					NE.util.getEspaciador(20)
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	buttons: [
						{
							xtype: 'button',	text: 'Cerrar',id: 'btnCerrarIV', 
							handler: function(){
								Ext.getCmp('InterfazVariable').hide();	
							} 
						}
					]
				},
				title: 'Exportar Interfase Variable'
			}).show();
		}
	}		
	
	var fpInterfazVariable = {
		xtype: 'container',
		id: 'fpInterfazVariable',
		layout: 'table',		
		align: 'center',
		style: 'margin:0 auto;',			
		width:	300,
		heigth:	100,
		items:[	
			{
				xtype: 'displayfield',
				value: '       ',
				width: 30
			},
			{
				xtype: 'button',				
				text: 'Generar Archivo',					
				id: 'btnInterVariable',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta01ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion:'ArchivoCSV_TXT',
						tipoArchivo:'IVARIABLE',
						extensionArchivo:'CSV'
					})					
					,callback: procesarSuccessFailureInterVariable
					});
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarInterVariable',
				hidden: true
			},
			{
				xtype: 'button',				
				text: 'Generar Clave Hash',					
				id: 'btnVariableHash',
				hidden: true
			}
		]
	}
	

	var procesarSuccessFailureDetalle =  function(opts, success, response) {
		var btnGenerarDet = Ext.getCmp('btnGenerarDet');
		btnGenerarDet.setIconClass('');
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			var archivoMD5 = infoR.urlArchivoMD5;

			var btnBajarDet = Ext.getCmp('btnBajarDet');
			btnBajarDet.show();
			btnBajarDet.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarDet.setHandler( 
				function(boton, evento) {
					var params = {nombreArchivo: archivo};
					window.location = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				}
			);
			
			var bOperaPubHash =   Ext.getCmp("bOperaPubHash").getValue();
			var btnGenerarHash = Ext.getCmp('btnGenerarHash');
			
			if(bOperaPubHash =='S') {
				btnGenerarHash.show();
				btnGenerarHash.setHandler( 
					function(boton, evento) {
						var params = {nombreArchivo: archivoMD5};
						window.location = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					}
				);
			} else {
				btnGenerarHash.hide();
			}
		} else {
			btnGenerarDet.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarGenerarDetalle = function() {
		var ventana = Ext.getCmp('GeneraDetalle');
		if (ventana) {
			ventana.show();
		}else {			
			new Ext.Window({			
				width: 300,				
				height: 150,
				autoScroll:true, 
				resizable: false,
				closeAction: 'hide',
				id: 'GeneraDetalle',
				autoDestroy:true,
				closable:false,
				align: 'center',				
				items: [
					NE.util.getEspaciador(20),
					fpDetalle,		
					NE.util.getEspaciador(20)
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	buttons: [
						{
							xtype: 'button',	text: 'Cerrar',id: 'btnCerrar', 
							handler: function(){
								Ext.getCmp('GeneraDetalle').hide();
							} 
						}
					]
				},
				title: 'Generar Archivo Detalle'
			}).show();
		}
	}
	
	var fpDetalle = {
		xtype: 'container',
		id: 'fpDetalle',
		layout: 'table',		
		align: 'center',
		style: 'margin:0 auto;',			
		width:	300,
		heigth:	100,
		items:[	
			{
				xtype: 'displayfield',
				value: '       ',
				width: 30
			},
			{
				xtype: 'button',				
				text: 'Generar Archivo',					
				id: 'btnGenerarDet',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta01ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
						informacion:'ArchivoCSV_TXT',
						tipoArchivo:'DETALLE',
						extensionArchivo:'CSV'
					})					
					,callback: procesarSuccessFailureDetalle
					});
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarDet',
				hidden: true
			},
			{
				xtype: 'button',				
				text: 'Generar Clave Hash',					
				id: 'btnGenerarHash',
				hidden: true
			}			
		]
	}
/*
	function procesarSuccessFailurePDFxPag(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenerarPDFxPag').setIconClass('');
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	function procesarSuccessFailurePDFTodo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDFTodo').enable();
		Ext.getCmp('btnGenerarPDFTodo').setIconClass('');
	}

	function procesarSuccessFailureCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnGenerarCSV').setIconClass('');
			var archivo = infoR.urlArchivo;				  
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	  
	var fpBotones = new Ext.Container({
		layout: 'table',
		align: 'center',
		style: 'margin:0 auto;',
		id: 'fpBotones',	
		hidden: true,
	   width: '900',
		heigth:'auto',		
		items: [
			{
				xtype: 'button',
				text: 'Generar Archivo',
				width: '30',
				tooltip:	'Generar Archivo',				
				id: 'btnGenerarCSV',	
				handler: function(boton, evento) {		
					
					boton.setIconClass('loading-indicator');	
					Ext.Ajax.request({
						url: '13consulta01ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV_TXT',
							tipoArchivo:'NORMAL',
							extensionArchivo:'CSV'
						})					
						,callback: procesarSuccessFailureCSV
					});
				}
			},
/*
			{
				xtype: 'button',
				text: 'Imp. PDF',				
				tooltip:	'Imp PDF',
				id: 'btnGenerarPDFxPag',	
				handler: function(boton, evento) {		
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13consulta01ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoPDFxPagina',
							tipoArchivo:'NORMAL',
							start: barraPaginacion.cursor,
							limit: barraPaginacion.pageSize	
						})					
						,callback: procesarSuccessFailurePDFxPag
					});
				}
			},
*/
			{
				xtype: 'button',
				text: 'Generar Todo',
				tooltip: 'Imprime los registros en formato PDF.',
				id: 'btnGenerarPDFTodo',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta01ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoPDFTodo',
							tipoArchivo:'NORMAL'	
						})
						,callback: procesarSuccessFailurePDFTodo
					});
				}
			},
			{
				xtype: 'button',
				text: 'Imp.  Detalle',
				tooltip:	'Imp  Detalle',
				id: 'btnGenerarDetalle',	
				handler: procesarGenerarDetalle			
			},
			{
				xtype: 'button',
				text: 'Exp. Inter. Variable',
				tooltip:	'Expo. Inter. Variable',
				id: 'btnInterfazVariable',	
				handler: procesarInterfazVariable		
			},
			{
				xtype: 'button',
				text: 'Exp. Inter. Variable Notas de Cr�dito',
				tooltip:	'Exp. Inter. Variable Notas de Cr�dito',
				id: 'btnInterfazVariableNotas',	
				hidden: true,
				handler: procesarInterfazVariableNotas		
			},
			{
				xtype: 'button',
				text: 'Exp. Inter. Fija',
				tooltip:	'Exp. Inter. Fija',
				id: 'btnGenerarFija',	
				handler: function(boton, evento) {					
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta01ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV_TXT',
							tipoArchivo:'IFIJA',
							extensionArchivo:'TXT'
						})					
						,callback: procesarSuccessFailureFija
					});
				}
			},
			{
				xtype: 'button',
				text: 'Imp. Interfase',
				tooltip:	'Imp. Interfase',
				id: 'btnImprimirInterfase',	
				handler: function(boton, evento) {		
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13consulta01ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoPDFxPagina',
							tipoArchivo:'INTERFASE',
							start: barraPaginacion.cursor,
							limit: barraPaginacion.pageSize	
						})					
						,callback: procesarSuccessFailureImprimir
					});
				}
			}	
		]
	});
			
	var consultaDataTotalesC = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotalesC'
		},
		hidden: true,
		fields: [	
			{name: 'MONEDA'},
			{name: 'TOTAL_SOLIC_X_AUTO'},
			{name: 'TOTAL_SOLIC_OPER'},
			{name: 'TOTAL_SOLIC_RECH'},
			{name: 'MONTO_SOLIC_X_AUTO'},
			{name: 'MONTO_SOLIC_OPER'},
			{name: 'MONTO_SOLIC_RECH'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	
	var gruposTotales = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 1, align: 'center'},
				{	header: 'Por Autorizar', colspan: 2, align: 'center'},	
				{	header: 'Operados ', colspan: 2, align: 'center'},
				{	header: 'Rechazados ', colspan: 2, align: 'center'}
			]
		]
	});
	
	var gridTotalesC = new Ext.grid.GridPanel({
		id: 'gridTotalesC',				
		store: consultaDataTotalesC,	
		style: 'margin:0 auto;',
		title:'',
		plugins: gruposTotales,
		hidden: true,
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'Total de solicitudes ',
				tooltip: 'Total de solicitudes ',
				dataIndex : 'TOTAL_SOLIC_X_AUTO',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Total Montos ',
				tooltip: 'Total Montos ',
				dataIndex : 'MONTO_SOLIC_X_AUTO',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},		
			{							
				header : 'Total de solicitudes ',
				tooltip: 'Total de solicitudes ',
				dataIndex : 'TOTAL_SOLIC_OPER',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'							
			},	
			{							
				header : 'Total Montos ',
				tooltip: 'Total Montos ',
				dataIndex : 'MONTO_SOLIC_OPER',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},		
			{							
				header : 'Total de solicitudes ',
				tooltip: 'Total de solicitudes ',
				dataIndex : 'TOTAL_SOLIC_RECH',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'				
			},
			{							
				header : 'Total Montos ',
				tooltip: 'Total Montos ',
				dataIndex : 'MONTO_SOLIC_RECH',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			}	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 150,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	

	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},
		hidden: true,
		fields: [	
			{name: 'MONEDA'},
			{name: 'NUM_DOCTOS'},
			{name: 'TOTAL_MONTOS_DOCTO'},
			{name: 'TOTAL_MONTOS_DESC'}								
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'',
		hidden: true,
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'N�mero de Documentos',
				tooltip: 'N�mero de Documentos',
				dataIndex : 'NUM_DOCTOS',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'Total Monto Documento',
				tooltip: 'Total Monto Documento',
				dataIndex : 'TOTAL_MONTOS_DOCTO',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
				{							
				header : 'Total Monto a Descontar',
				tooltip: 'Total Monto a Descontar',
				dataIndex : 'TOTAL_MONTOS_DESC',
				sortable: true,
				width: 150,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 100,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}

			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;

			var gridTotales = Ext.getCmp('gridTotales');
			var gridTotalesC = Ext.getCmp('gridTotalesC');
			var fpBotones = Ext.getCmp('fpBotones');
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
			//var btnGenerarPDFxPag = Ext.getCmp('btnGenerarPDFxPag');
			var btnGenerarPDFTodo = Ext.getCmp('btnGenerarPDFTodo');
			var btnInterfazVariable = Ext.getCmp('btnInterfazVariable');
			var btnGenerarFija = Ext.getCmp('btnGenerarFija');
			var btnImprimirInterfase = Ext.getCmp('btnImprimirInterfase');
			var btnInterfazVariableNotas = Ext.getCmp('btnInterfazVariableNotas');
			Ext.getCmp("bOperaPubHash").setValue(jsonData.bOperaPubHash);

			if(jsonData.bTipoFactoraje  =='S') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
			}

			if(jsonData.bOperaFactorajeDist=='S' || jsonData.bOperaFactorajeVencINFO=='S'){	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIA'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIA'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);									
			}
				
			if(store.getTotalCount() > 0) {	
			
				consultaDataTotales.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotales'	
					})
				});		
				
				consultaDataTotalesC.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarTotalesC'	
					})
				});	
				
				if(jsonData.bOperaNC  =='S') {			
					btnInterfazVariableNotas.show();
				}else if(jsonData.bOperaNC  =='N') {	
					btnInterfazVariableNotas.hide();
				}
			
				gridTotales.show();	
				gridTotalesC.show(); 
				fpBotones.show(); 
				btnGenerarCSV.enable();
				//btnGenerarPDFxPag.enable();
				btnGenerarPDFTodo.enable();
				btnInterfazVariable.enable();
				btnGenerarFija.enable();			
				btnImprimirInterfase.enable();				
				el.unmask();							
			} else {	
				fpBotones.hide();					
				gridTotales.hide();	
				gridTotalesC.hide();			
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [	
			{name: 'ORIGEN'},
			{name: 'FOLIO_SOLIC'},
			{name: 'NOMBRE_PYME'},	
			{name: 'EPO_RELACIONADA'},
			{name: 'NUM_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC_SOLIC'},
			{name: 'IC_MONEDA'},		
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_DOCTO'},
			{name: 'POR_DESC'},
			{name: 'RECURSO_GARAN'},
			{name: 'MONTO_DESC'},
			{name: 'INTERES'},
			{name: 'TASA_PYME'},
			{name: 'PLAZO'},
			{name: 'MONTO_OPERAR'},
			{name: 'REFERENCIA_PYME'},	//FODEA 17
			{name: 'FECHA_SOLIC_DESC'},
			{name: 'IC_ESTATUS'},			
			{name: 'ESTATUS'},
			{name: 'NUM_PRESTAMO'},
			{name: 'FECHA_OPERACION'},
			{name: 'TASA_FONDEO'},
			{name: 'BENEFICIARIO'},	
			{name: 'BANCO_BENEFICIARIO'},
			{name: 'SUCURSAL_BENEFICIARIA'},
			{name: 'CUENTA_BENEFICIARIA'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_BENEFICIARIO'},
			{name: 'NETO_RECIBIR'},
			{name: 'REFERENCIA'},
			{name: 'IC_ESTATUS_DOCTO'},	
			{name: 'IC_PYME'}	,
			{name: 'IC_DOCUMENTO'},
			{name: 'CC_ACUSE'},
			{name: 'MUESTRA_VISOR'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Informaci�n de Solicitudes',
		hidden: true,
		clicksToEdit: 1,
		columns: [
			{							
				header : 'Origen',
				tooltip: 'Origen',
				dataIndex : 'ORIGEN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Folio de Solicitud',
				tooltip: 'Folio de Solicitud',
				dataIndex : 'FOLIO_SOLIC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Nombre del Proveedor/Cedente',
				tooltip: 'Nombre del Proveedor',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'EPO Relacionada',
				tooltip: 'EPO Relacionada',
				dataIndex : 'EPO_RELACIONADA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'No. de Documento',
				tooltip: 'No. de Documento',
				dataIndex : 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				width:     180,
				xtype:     'actioncolumn',
				header:    'No. Acuse Autorizaci�n',
				tooltip:   'No. Acuse Autorizaci�n',
				dataIndex: 'CC_ACUSE',
				align:     'center',
				sortable:  true,
				renderer: function(value, metadata, record, rowindex, colindex, store){
					return (record.get('CC_ACUSE'));
				},
				items: [{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store){
						if(registro.get('CC_ACUSE') != '' && registro.get('MUESTRA_VISOR') == 'S'){
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';
						} else{
							return value;
						}
					},
					handler: function(gridConsulta, rowIndex, colIndex){
						var rec = consultaData.getAt(rowIndex);
						VisorAcusesDeCarga(rec);
					}
				}]
			},
			{
				header : 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Fecha de Vencimiento Solicitud',
				tooltip: 'Fecha de Vencimiento Solicitud',
				dataIndex : 'FECHA_VENC_SOLIC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'
			},			
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,			
				align: 'center'
			},
			{							
				header : 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex : 'MONTO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,						
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'POR_DESC',
				sortable: true,
				width: 130,
				resizable: true,						
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{							
				header : 'Recurso en garant�a',  // pendiente porque hay calculosque hacer 
				tooltip: 'Recurso en garant�a',
				dataIndex : 'RECURSO_GARAN',
				sortable: true,
				width: 130,
				resizable: true,						
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,						
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Intereses',
				tooltip: 'Intereses',
				dataIndex : 'INTERES',
				sortable: true,
				width: 130,
				resizable: true,						
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Tasa a PYME',
				tooltip: 'Tasa a PYME',
				dataIndex : 'TASA_PYME',
				sortable: true,
				width: 130,
				resizable: true,						
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{							
				header : 'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,						
				align: 'center'				
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,						
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Fecha de Solicitud del Descuento',
				tooltip: 'Fecha de Solicitud del Descuento',
				dataIndex : 'FECHA_SOLIC_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'N�mero de Pr�stamo',
				tooltip: 'N�mero de Pr�stamo',
				dataIndex : 'NUM_PRESTAMO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('NUM_PRESTAMO') ==0 ){
						return '';
					}else {
						return value;	
					}
				}
			},
			{							
				header : 'Fecha de Operaci�n',
				tooltip: 'Fecha de Operaci�n',
				dataIndex : 'FECHA_OPERACION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tasa de Fondeo',
				tooltip: 'Tasa de Fondeo',
				dataIndex : 'TASA_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('TASA_FONDEO') ==0 ){
						return '';
					}else {
						return value;	
					}
				}
			},
			{
				 xtype: 'actioncolumn',
				header: 'Detalle Aplicaci�n',
				tooltip: 'Detalle Aplicaci�n',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';							
							if(registro.get('IC_ESTATUS_DOCTO') ==16 ){
								return 'iconoLupa';		
							}
						}
						,	handler: verDetalleAplicacion
					}
				]				
			},	
			{							
				header : 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex : 'BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,			
				align: 'left'
			},
			{							
				header : 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex : 'BANCO_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,			
				align: 'left'
			},
			{							
				header : 'Sucursal Beneficiaria',
				tooltip: 'Sucursal Beneficiaria',
				dataIndex : 'SUCURSAL_BENEFICIARIA',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,			
				align: 'left'
			},
			{							
				header : 'Cuenta Beneficiaria',
				tooltip: 'Cuenta Beneficiaria',
				dataIndex : 'CUENTA_BENEFICIARIA',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,			
				align: 'center'
			},
			{							
				header : '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,			
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{							
				header : 'Importe a Recibir Beneficiario',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex : 'IMPORTE_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Neto a Recibir PyME',
				tooltip: 'Neto a Recibir PyME',
				dataIndex : 'NETO_RECIBIR',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{					//Agregado por FODEA 17
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA_PYME',
				sortable: true,
				width: 150,
				resizable: true,						
				align: 'left'
			}			
		],		
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
			
			]
		}
	});
			

	function procesaValoresIniciales(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){			
				Ext.getCmp("df_fecha_solicitudMin").setValue(jsonData.fechaHoy);
				Ext.getCmp("df_fecha_solicitudMax").setValue(jsonData.fechaHoy);	
				Ext.getCmp("limitarRangoFechas").setValue(jsonData.limitarRangoFechas);					
				Ext.getCmp("bOperaPubHash").setValue(jsonData.bOperaPubHash);					
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var successAjaxFn = function(opts, success, response) { 
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	var catalogoTipoTasa = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['N','Negociada'],
			['0','Oferta de Tasas'],
			['P','Preferencial'],
			['B','Tasa Base']			
		 ]
	});	
	
	var catalogoTipoFactoraje = new Ext.data.JsonStore({
		id: 'catalogoTipoFactoraje',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoTipoFactoraje'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'noPyme',
			id: 'noPyme1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);															
							var ic_epo = Ext.getCmp("ic_epo1");
							
							storeBusqAvanzPyme.load({
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
										ic_epo:ic_epo.getValue()						
									})																	
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	
	
	var  elementosForma  = [	
		{ 	xtype: 'displayfield', 	hidden: true, id: 'limitarRangoFechas', 	value: '' },
		{ 	xtype: 'displayfield',  hidden: true, id: 'bOperaPubHash', 	value: '' },
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			listeners: {
				select: {
					fn: function(combo) {				
						var num_electronico1 = Ext.getCmp('num_electronico1');
						num_electronico1.enable();	
						
						var cmbFactoraje = Ext.getCmp('tipoFactoraje1');
						cmbFactoraje.setValue('');
						cmbFactoraje.store.load({
							params: {
								ic_epo:combo.getValue()
							}
						});					
						
					}
				}
			}			
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Proveedor',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'Proveedor',
					disabled:true,
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 50,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){									
						 // Petici�n b�sica  
							Ext.Ajax.request({  
								url: '13consulta01ext.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
								params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue(),
									 ic_epo: Ext.getCmp('ic_epo1').getValue()
								}  
							}); 	
						}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 200,
					disabled:true,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
				xtype: 'button',
				text: 'B�squeda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
				
					var ic_epo = Ext.getCmp("ic_epo1");
					if (Ext.isEmpty(ic_epo.getValue()) ){
						ic_epo.markInvalid('Debe Seleccionar una EPO');
						return;
					}
							
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
					}
				}
			}			
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Estatus',
			combineErrors: true,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			items: [				
				{
					xtype: 'combo',
					fieldLabel: 'Estatus',
					name: 'ic_estatus_solic',
					id: 'ic_estatus_solic1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_estatus_solic',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoEstatus			
				},
				{
					xtype: 'displayfield',
					value: 'Moneda: ',
					width: 80
				},
				{
					xtype: 'combo',
					fieldLabel: 'Moneda',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoMoneda			
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Folio de solicitud',
			combineErrors: true,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 500,
			items: [		
				{
					xtype: 'textfield',
					name: 'ic_folio',
					id: 'ic_folio1',
					allowBlank: true,
					maxLength: 11,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},	
				{
					xtype: 'displayfield',
					value: '  ',
					width: 50				
				},
				{
					xtype: 'displayfield',
					value: ' Tipo Tasa: ',
					width: 85					
				},
				{
					xtype: 'combo',
					fieldLabel: 'Tipo Tasa',
					name: 'tipoTasa',
					id: 'tipoTasa1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'tipoTasa',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoTipoTasa,
					listeners: 	{
						select:function(combo, record, index) {
							
							Ext.getCmp("suceptibleFloating").hide();
							Ext.getCmp('aplicaFloating1').setValue(false);
							Ext.getCmp('aplicaFloating2').setValue(false);	
							Ext.getCmp('aplicaFloating3').setValue(false);
							if(combo.value==='P'){
								Ext.getCmp("suceptibleFloating").show();										
							}
						}
					}
				}		
			]
		},
		
		{
			xtype: 'compositefield',	
			name: 'suceptibleFloating',   
			id: 'suceptibleFloating', 
			combineErrors: true,
			msgTarget: 'side',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width: 700,
			hidden: true,
			items: [				
				{  
					xtype:  'radiogroup', 					 
					value: 'S', 
					align: 'center',
					width: 700,							
					items: [						
						{             
							boxLabel: "Aplica Floating",    
							name: 'aplicaFloating', 
							id: 'aplicaFloating1',
							inputValue:  "S" ,
							value: 'S',
							checked: true							
						}, 
						{           
							boxLabel: "NO Aplica Floating",             
							name: 'aplicaFloating', 
							id: 'aplicaFloating2',
							inputValue:  "N", 
							value: 'N'	
						} ,
						{ 
							boxLabel:    "Ambas",             
							name:        'aplicaFloating', 
							id: 'aplicaFloating3',
							inputValue:  "A" ,
							value: 'A'						
						} 
					]
				}
			]			
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Solicitud de ',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_solicitudMin',
					id: 'df_fecha_solicitudMin',
					allowBlank: true,
					startDay: 0,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_solicitudMax',
					margins: '0 20 0 0' ,
					formBind: true},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_solicitudMax',
					id: 'df_fecha_solicitudMax',
					allowBlank: true,
					startDay: 1,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_fecha_solicitudMin',
					margins: '0 20 0 0',
					formBind: true
				},					
				{
					xtype: 'displayfield',
					value: 'Monto: ',
					width: 50
				},				
				{
					xtype: 'numberfield',
					name: 'fn_montoMin',
					id: 'fn_montoMin',
					allowBlank: true,
					maxLength: 14,
					width: 90,
					msgTarget: 'side',
					vtype:	'rangoValor',
					campoFinValor:	'fn_montoMax',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a:', 
					width: 15
				},
				{
					xtype: 'numberfield',
					name: 'fn_montoMax',
					id: 'fn_montoMax',
					allowBlank: true,
					maxLength: 14,
					width: 90,
					msgTarget: 'side',
					vtype:	'rangoValor',
					campoInicioValor:	'fn_montoMin',
					margins: '0 20 0 0'
				}
			]
		},	
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_vencMin',
					id: 'df_fecha_vencMin',
					allowBlank: true,
					startDay: 0,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_vencMax',
					margins: '0 20 0 0' ,
					formBind: true},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 15
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_vencMax',
					id: 'df_fecha_vencMax',
					allowBlank: true,
					startDay: 1,
					width: 90,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_fecha_vencMin',
					margins: '0 20 0 0',
					formBind: true
				},	
				{
					xtype: 'displayfield',
					value: 'Tipo Factoraje:',
					width: 80
				},
				{
					xtype: 'combo',
					fieldLabel: 'Tipo Factoraje',
					name: 'tipoFactoraje',
					id: 'tipoFactoraje1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'tipoFactoraje',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoTipoFactoraje			
				}	
			]
		},
		{
		  xtype: 'label',
		  id: 'lblCriterios',
		  fieldLabel: 'Criterios ordenamiento',
		  text: ''
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			items: [			
				{
					xtype: 'numberfield',
					name: 'ordenaEpo',
					id: 'ordenaEpo1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'EPO',
					width: 80
				},
				{
					xtype: 'numberfield',
					name: 'ordenaPyme',
					id: 'ordenaPyme1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'PYME',
					width: 80
				},
				{
					xtype: 'numberfield',
					name: 'ordenaFecha',
					id: 'ordenaFecha1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'Fecha de Vencimiento',
					width: 130
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',
			items: [		
				{
					xtype: 'numberfield',
					name: 'ordenaMonto',
					id: 'ordenaMonto1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'Monto',
					width: 80
				},
				{
					xtype: 'numberfield',
					name: 'ordenaEstatus',
					id: 'ordenaEstatus1',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'Estatus',
					width: 80
				},
				{
					xtype: 'numberfield',
					name: 'ordenaFolio',
					id: 'ordenaFolio',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						focus: function(field){ 
						var valor =  field.getValue();
						if(valor==6){
							valor =1; 
						}else	if(valor =='') { 
							valor =1; 
						}	else { valor +=1;	}						
							field.setValue(valor); 
						}					
					}
				},
				{
					xtype: 'displayfield',
					value: 'Folio',
					width: 80
				}		
			]
		}				
	];

	var getDiferenciaEnDias = function(fechaIni, fechaFin) { 
		fechaIni =	Ext.util.Format.date(fechaIni,'d/m/Y');
		fechaFin =	Ext.util.Format.date(fechaFin,'d/m/Y');
		var string1 			= "";
		var temp 				= "";
		var diferenciaEnDias	= "";
		string1 = fechaIni;
		string = "" + string1;
		splitstring = string.split("/");
		var fechaInicial= new Date();
		fechaInicial.setDate(splitstring[0]);
		fechaInicial.setMonth(splitstring[1]-1);
		fechaInicial.setYear(splitstring[2]);
		string1 = fechaFin;
		string = "" + string1;
		splitstring = string.split("/");
		var fechaFinal= new Date();
		fechaFinal.setDate(splitstring[0]);
		fechaFinal.setMonth(splitstring[1]-1);
		fechaFinal.setYear(splitstring[2]);
		//Calcular el numero de milisegundos de un dia
		var milisegundosPorDia=1000*60*60*24;
		//Calculate difference btw the two dates, and convert to days
		diferenciaEnDias = Math.ceil((fechaFinal.getTime()-fechaInicial.getTime())/(milisegundosPorDia));
		return diferenciaEnDias;
	}
	
		
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Informaci�n de Solicitudes',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,	  		
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
		
					var df_fecha_solicitudMin = Ext.getCmp('df_fecha_solicitudMin');
					var df_fecha_solicitudMax = Ext.getCmp('df_fecha_solicitudMax');					
					var limitarRangoFechas = Ext.getCmp('limitarRangoFechas').getValue();
					var df_fecha_vencMin = Ext.getCmp('df_fecha_vencMin');
					var df_fecha_vencMax = Ext.getCmp('df_fecha_vencMax');	
										
					if( Ext.isEmpty(df_fecha_vencMin.getValue()) || Ext.isEmpty(df_fecha_vencMax.getValue())   ) {					
						if ( Ext.isEmpty(df_fecha_solicitudMin.getValue()) || Ext.isEmpty(df_fecha_solicitudMax.getValue())    ) {											
							Ext.MessageBox.alert('Mensaje','Debe de capturar al menos un rango de fechas');
							return;
						}
					}	
					
					if ( ( !Ext.isEmpty(df_fecha_vencMin.getValue())  &&   Ext.isEmpty(df_fecha_vencMax.getValue()) )
						||  ( Ext.isEmpty(df_fecha_vencMin.getValue())  &&   !Ext.isEmpty(df_fecha_vencMax.getValue()) ) ){
						df_fecha_vencMin.markInvalid('Debe capturar ambas Fechas  o dejarlas en blanco');
						df_fecha_vencMax.markInvalid('Debe capturar ambas Fechas o dejarlas en blanco');
						return;
					}
					
					if ( ( !Ext.isEmpty(df_fecha_solicitudMin.getValue())  &&   Ext.isEmpty(df_fecha_solicitudMax.getValue()) )
						||  ( Ext.isEmpty(df_fecha_solicitudMin.getValue())  &&   !Ext.isEmpty(df_fecha_solicitudMax.getValue()) ) ){
						df_fecha_solicitudMin.markInvalid('Debe capturar ambas Fechas  o dejarlas en blanco');
						df_fecha_solicitudMax.markInvalid('Debe capturar ambas Fechas o dejarlas en blanco');
						return;
					}
					
					
					var resultado = datecomp(Ext.util.Format.date(df_fecha_solicitudMin.getValue(),'d/m/Y') , 	Ext.util.Format.date(df_fecha_solicitudMax.getValue(),'d/m/Y')	);
					if (resultado == 1){
						df_fecha_solicitudMax.markInvalid('La fecha de solicitud final debe ser mayor o igual a la inicial');
						return;
					}
					
					var Diferencia = Math.abs(getDiferenciaEnDias(df_fecha_solicitudMax.getValue(),df_fecha_solicitudMin.getValue() ));
					if(limitarRangoFechas =='S') {			
						if ( Diferencia >7 )		{
							df_fecha_solicitudMax.markInvalid('El rango de fechas de solicitud no debe sobrepasar 7 dias');
							return;
						}
					}
									
					var resultado = datecomp(Ext.util.Format.date(df_fecha_vencMin.getValue(),'d/m/Y') , 	Ext.util.Format.date(df_fecha_vencMax.getValue(),'d/m/Y')	);
					if (resultado == 1){
						df_fecha_vencMax.markInvalid('La fecha de vencimiento final debe ser mayor o igual a la inicial');
						return;
					}
					
					Diferencia = Math.abs(getDiferenciaEnDias(df_fecha_vencMax.getValue(),df_fecha_vencMin.getValue() ));
					if(limitarRangoFechas =='S') {			
						if ( Diferencia >7 )		{
							df_fecha_vencMax.markInvalid('El rango de fechas de vencimiento no debe sobrepasar 7 dias');							
							return;
						}
					}
					
					if (!Ext.isEmpty(df_fecha_vencMin.getValue()) &&  !Ext.isEmpty(df_fecha_solicitudMin.getValue()) ) {										
						var resultado = datecomp(Ext.util.Format.date(df_fecha_vencMin.getValue(),'d/m/Y') , 	Ext.util.Format.date(df_fecha_solicitudMin.getValue(),'d/m/Y')	);
						var resultado1 = datecomp(Ext.util.Format.date(df_fecha_vencMax.getValue(),'d/m/Y') , 	Ext.util.Format.date(df_fecha_solicitudMax.getValue(),'d/m/Y')	);
						if (resultado == 2 || resultado1 ==2) 	{							
							Ext.MessageBox.alert('Mensaje','La fecha de vencimiento debe ser mayor o igual a la de la solicitud');
							return;
						}					 
					}
					
					var ventana1 = Ext.getCmp('GeneraDetalle');
					if (ventana1) {	ventana1.destroy();	 	}	
					
					var ventana2 = Ext.getCmp('InterfazVariable');
					if (ventana2) {	ventana2.destroy();	 	}	
					
					var ventana3 = Ext.getCmp('InterfazVariableNotas');
					if (ventana3) {	ventana3.destroy();	 	}	
					
					var ventana4 = Ext.getCmp('verDetalle');
					if (ventana4) {	ventana4.destroy();	 	}	
					
					
					fp.el.mask('Enviando...', 'x-mask-loading')
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15
						})
					});						
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta01ext.jsp';								
				}				
			}
		]
	});
	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,	
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,			
			gridTotales,
			NE.util.getEspaciador(20),
			gridTotalesC,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20)
		]
	});
  

	catalogoEPO.load();
	catalogoEstatus.load();
	catalogoMoneda.load();
	catalogoTipoFactoraje.load();
	
	
	
	//Peticion para obtener valores iniciales y la parametrizaci�n
	fp.el.mask('Inicializando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '13consulta01ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});
	
	
	


});