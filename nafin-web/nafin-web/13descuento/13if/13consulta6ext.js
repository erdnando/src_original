	Ext.onReady(function() {
	
   var objGral = {
      strPerfil: Ext.getDom('strPerfil').value,
      ic_if :  Ext.getDom('ic_if').value,
      sirac : '',
      financiera  : ''
   }   
   
	//
  
  function validaPerfil(perfil){
    if(perfil =='LINEA CREDITO' || perfil =='IF LI' || perfil =='IF 4CP' || perfil =='IF 5CP' ||
       perfil =='IF 4MIC' || perfil =='IF 5MIC' ){
      return 'LINEA CREDITO';
    }else{
      return 'DESCUENTO';
    }
  }
  
  var procesarObtenerSiracFinanciera =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp =  Ext.util.JSON.decode(response.responseText);
      
      objGral.sirac = resp.NUMERO_SIRAC;
      objGral.financiera = resp.IC_FINANCIERA;
      
      if(validaPerfil(objGral.strPerfil)!='LINEA CREDITO'){
        if(resp.NUMERO_SIRAC=='' || resp.IC_FINANCIERA==''){
          Ext.getCmp("tipoLinea").hide();
          if(resp.IC_FINANCIERA==''){
            Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
            Ext.getCmp("cliente").setValue(resp.NUMERO_SIRAC);
            objGral.ic_if = 12;
            
          }
        }else{
          Ext.getCmp("tipoLinea").show();
        }
      }else{
        Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
        Ext.getCmp("cliente").setValue(objGral.sirac);
        objGral.ic_if = 12;
      }
      
      catalogoFechaCorte.load({
        params   :  {
          //ic_if: objGral.ic_if,
          tipoLinea: Ext.getCmp("tipoLinea").getValue().value,
		  cliente: objGral.sirac
        }
      });
		} else {
			
			NE.util.mostrarConnError(response,opts);
		}
	}
  
  // PROCESAR ZIP
	var procesarGenerarZip =  function(opts, success, response) {
		var btnArchivoPDF = Ext.getCmp('btnGenerarZip');
		btnArchivoPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarZip');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				//var forma = Ext.getDom('formAux');
				//forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				//forma.submit();
				
				var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				Ext.getCmp('forma').getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				Ext.getCmp('forma').getForm().getEl().dom.submit();
			});
		} else {
			btnArchivoPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// Procesar Imprimir PDF
	var procesarSuccessFailureImprimirPDF =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
		btnImprimirPDF.setIconClass('icoPdf');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureNotaC =  function(opts, success, response) {
		var btnImprimirPDF = Ext.getCmp('btnImprimirNotaC');
		btnImprimirPDF.setIconClass('icoXls');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			btnImprimirPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaDataTotales = function(store, arrRegistros, opts) {
		var gridTotales = Ext.getCmp('gridTotales');
		if (arrRegistros != null) {
         
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnImprimirPDF = Ext.getCmp('btnImprimirNotaC');	
			var el = gridTotales.getGridEl();
			
			if(store.getTotalCount() > 0) {            
				el.unmask();
			} else {
            Ext.getCmp('gridTotales').hide();
			}
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
      grid.show();
      
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
//				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnImprimirPDF = Ext.getCmp('btnImprimirPDF');
			var btnImprimirPDF = Ext.getCmp('btnImprimirNotaC');
			var gridTotales = Ext.getCmp('gridTotales');
	
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
            //consultaDataGridTotales.removeAll();
            var cmpForma = Ext.getCmp('forma');            
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnImprimirPDF').enable();
				el.unmask();
			} else {
            consultaDataGridTotales.removeAll();
            gridTotales.hide();
				btnGenerarArchivo.disable();
				Ext.getCmp('btnImprimirPDF').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
      
      
      consultaDataGridTotales.load({
         params   :  {
            start:      opts.params.start,
            limit:      opts.params.limit
         }
      });
      
	}
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
			Ext.getCmp('ic_pyme').setValue(jsonObj.ifc_pyme);			
		}
	}
	var bajarContrato =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		} else {
			alert("error");
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoTxt');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				//var forma = Ext.getDom('formAux'); 
				//forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				//forma.submit();
        
        var archivo = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};				
				Ext.getCmp('forma').getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				Ext.getCmp('forma').getForm().getEl().dom.submit();
        
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//Procesar Generar archivo de participantes
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta6ext.data.jsp',
		baseParams: {
			informacion: 'Consultar',
         start: 0,
         limit: 15
		},
		fields: [
			{name: 'IG_CLIENTE'},
			{name: 'TO_CHAR(DF_FECHOPERACION,DD/MM/YYYY)'},
			{name: 'CG_NOMBRECLIENTE'},
			{name: 'IG_PRESTAMO'},
			{name: 'IG_TASAREFERENCIAL'},
			{name: 'FG_SPREAD'},
			{name: 'FG_MARGEN'},
			{name: 'FG_MONTOOPERADO'},
			{name: 'FG_SALDOINSOLUTO'},
			{name: 'FG_CAPITALVIGENTE'},
			{name: 'FG_CAPITALVENCIDO'},
			{name: 'FG_INTERESVENCIDO'},
			{name: 'FG_INTERESMORAT'},
			{name: 'FG_ADEUDOTOTAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var consultaDataGridTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta6ext.data.jsp',
		baseParams: {
			informacion: 'Consultar',
			operacion: 'Totales'
		},
		fields: [
			{name: 'MONEDA'},
			{name: 'N_REGISTROS'},
			{name: 'MONTO_OPERADO'},
			{name: 'SALDO_INSOLUTO'},
			{name: 'CAPITAL_VIG'},
			{name: 'CAPITAL_VENCIDO'},
			{name: 'INTERES_VENCIDO'},
			{name: 'INTERES_MORATORIO'},
			{name: 'ADEUDO_TOTAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
         beforeLoad:	{
				fn: function(store, options){
               var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");  // Id barra paginacion
					Ext.apply(options.params, {
						FechaCorte:	Ext.getCmp('id_FechaCorte').getValue(),
                  moneda:     Ext.getCmp('id_moneda').getValue(),
                  cliente:    Ext.getCmp('cliente').getValue(),
                  prestamo:   Ext.getCmp('prestamo').getValue()
					});
				}		
			},
			load: procesarConsultaDataTotales,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var catalogoMoneda = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '13consulta6ext.data.jsp',
		listeners: {
			//load: procesarCatalogoEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: true
	});
	var catalogoFechaCorte = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '13consulta6ext.data.jsp',
		listeners: {
			//load: procesarCatalogoEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'CatalogoFechaCorte'
		},
		totalProperty : 'total',
		autoLoad: false
	});
	
/*******************************************************************************
*											GRID'S													 *
*******************************************************************************/
	var gridTotales = {
		store: consultaDataGridTotales,
		xtype:'grid',
		id: 'gridTotales',
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 180,
		width: 925,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		hidden: true,
		title:'Totales',
		columns: [{
			header: 'Total Parcial', tooltip: 'Total Parcial',
			dataIndex: 'MONEDA',
			sortable: true,
			width: 165, resizable: true,
			align: 'right'
		},{
			header: 'N�m. Registros', tooltip: '',
			dataIndex: 'N_REGISTROS',
			sortable: true,align:'center',
			width: 130, resizable: true
		},{
			header: 'Monto Operado', tooltip: 'Monto Operado',
			dataIndex: 'MONTO_OPERADO',
			sortable: true, align: 'right',
			width: 130, resizable: true,renderer:'usMoney'
		},{
			header: 'Saldo Insoluto', tooltip: 'Saldo Insoluto',
			dataIndex: 'SALDO_INSOLUTO',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Capital Vigente', tooltip: 'Capital Vigente',
			dataIndex: 'CAPITAL_VIG',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Capital Vencido', tooltip: 'Capital Vencido',
			dataIndex: 'CAPITAL_VENCIDO',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Inter�s Vencido', tooltip: 'Inter�s Vencido',
			dataIndex: 'INTERES_VENCIDO',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Inter�s Moratorio', tooltip: 'Inter�s Moratorio',
			dataIndex: 'INTERES_MORATORIO',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		},{
			header: 'Adeudo Total', tooltip: 'Adeudo Total',
			dataIndex: 'ADEUDO_TOTAL',
			sortable: true,
			width: 100, resizable: true,
			align: 'right', renderer: 'usMoney'
		}]
	};
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [{
				header: 'Cliente', tooltip: 'Cliente',
				dataIndex: 'IG_CLIENTE',
				sortable: true,
				width: 65, resizable: true,
				align: 'left'
			},{
				header: 'Nombre del Cliente', tooltip: 'Nombre del Cliente',
				dataIndex: 'CG_NOMBRECLIENTE',
				sortable: true,	align: 'center',
				width: 150, resizable: true,
				align: 'left'
			},{
				header: 'Fecha Operaci�n', tooltip: 'Fecha Operaci�n',
				dataIndex: 'TO_CHAR(DF_FECHOPERACION,DD/MM/YYYY)',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center'
			},{
				header: 'Pr�stamo', tooltip: 'Pr�stamo',
				dataIndex: 'IG_PRESTAMO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'left'
			},{
				header: 'Tasa ref.', tooltip: 'Tasa ref.',
				dataIndex: 'IG_TASAREFERENCIAL',
				sortable: true,	align: 'center',
				width: 60, resizable: true,
				align: 'center'
			},{
				header: 'Spread', tooltip: 'Spread',
				dataIndex: 'FG_SPREAD',
				sortable: true,	align: 'center',
				width: 60, resizable: true,
				align: 'right'
			},{
				header: 'Margen', tooltip: 'Margen',
				dataIndex: 'FG_MARGEN',
				sortable: true,	align: 'center',
				width: 60, resizable: true,
				align: 'center'
			},{
				header: 'Monto Operado', tooltip: 'Monto Operado',
				dataIndex: 'FG_MONTOOPERADO',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Saldo insoluto', tooltip: 'Saldo insoluto',
				dataIndex: 'FG_SALDOINSOLUTO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Capital vigente', tooltip: 'Capital vigente',
				dataIndex: 'FG_CAPITALVIGENTE',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
				renderer : 'usMoney'
			},{
				header: 'Capital vencido', tooltip: 'Capital vencido',
				dataIndex: 'FG_CAPITALVENCIDO',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Inter�s vencido', tooltip: 'Inter�s vencido',
				dataIndex: 'FG_INTERESVENCIDO',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Inter�s moratorio', tooltip: 'Inter�s moratorio',
				dataIndex: 'FG_INTERESMORAT',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Adeudo Total', tooltip: 'Adeudo Total',
				dataIndex: 'FG_ADEUDOTOTAL',
				sortable: true,	align: 'center',
				width: 105, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			}],
         stripeRows: true,
         loadMask: true,
         deferRowRender: false,
         height: 433,
         width: 926,
         colunmWidth: true,
         frame: true,
         style: 'margin:0 auto;',// para centrar el grid
         collapsible: true,
         bbar: {
            xtype: 'paging',
            id: 'barraPaginacion',
            pageSize: 15,
            buttonAlign: 'left',
            displayInfo: true,
            store: consultaDataGrid,
            displayMsg: '{0} - {1} de {2}',
            items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoTxt',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var cmpForma = Ext.getCmp('forma');
						boton.disable();
						boton.setIconClass('loading-indicator');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};

						Ext.Ajax.request({
							url: '13consulta6ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion :  'GeneraTXT',
								informacion :  'Consultar',
								ic_if       :  objGral.ic_if,
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				{
					xtype: 'button',
					text: 'Imprimir',
					id: 'btnImprimirPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta6ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'PDF',
								informacion: 'Consultar',
                           start       :  cmpBarraPaginacion.cursor,
                           limit       :  cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureImprimirPDF
						});
					}
				}
			]
		}
	};
/*******************************************************************************
*											FORMAS													 *
*******************************************************************************/
	var elementosForma = [
    {
      xtype: 'radiogroup',
      id:'tipoLinea',
      name: 'tipoLinea',
      fieldLabel: 'Tipo de Linea',
      //hidden:true,
      items: [
         {boxLabel: 'LINEA CREDITO', name: 'tipoLinea', id:'tipoLineaC', inputValue: 'C', value:'C' },
         {boxLabel: 'LINEA DESCUENTO', name: 'tipoLinea', id:'tipoLineaD', inputValue: 'D', checked:true, value:'D'}
      ],
      listeners:{
        change: function(rgroup, radio){
          Ext.getCmp('btnGenerarZip').enable();
          Ext.getCmp('btnBajarZip').hide();
          if(radio.value=='C'){
            Ext.getCmp('cliente').hide();
            Ext.getCmp("cliente").setValue(objGral.sirac);
            objGral.ic_if = 12;
            
            //Ext.getCmp('cliente').allowBlank = true;

          }else{
            Ext.getCmp('cliente').show();
            Ext.getCmp('cliente').setValue("");
            objGral.ic_if =  Ext.getDom('ic_if').value;
            //Ext.getCmp('cliente').allowBlank = true;
            
          }
		  
		  Ext.getCmp('id_FechaCorte').setValue('');
		  catalogoFechaCorte.load({
			params   :  {
			  //ic_if: objGral.ic_if,
			  tipoLinea: radio.value,
			  cliente: Ext.getCmp('cliente').getValue()
			}
		  });
        }
      }
    },
		{
			xtype: 'combo',
			name: 'moneda',
			id: 'id_moneda',
			hiddenName : 'moneda',
			fieldLabel: 'Moneda',
			emptyText: '- Seleccione -',
			store: catalogoMoneda,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			anchor: '80%'
		},{
			xtype:'numberfield',
			fieldLabel:'Cliente',
			id:'cliente',
			name:'cliente',
			anchor:'55%'
		},{
			xtype: 'numberfield',
			fieldLabel:'Pr�stamo',
			id:'prestamo',
			name:'prestamo',
			anchor:'55%'
		},{
			xtype: 'combo',
			name: 'FechaCorte',
			id: 'id_FechaCorte',
			hiddenName : 'FechaCorte',
			fieldLabel: 'Fecha de corte',
			emptyText: '- Seleccione -',
			store: catalogoFechaCorte,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			anchor: '80%'
		}
	];
	//Forma para hacer la busqueda filtrada
	var fp = {
		xtype: 'form',
		id: 'forma',
		style: 'text-align:left;margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 500,
		labelWidth:100,
		titleCollapse: false,
		title: 'Estados de Cuenta',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosForma],
		buttons: [
		{
			text: 'Consultar',
			id: 'btnConsultar',
			iconCls: 'icoBuscar',
			handler: function(boton, evento) {
				var moneda 	= Ext.getCmp('id_moneda');
				var cliente =Ext.getCmp('cliente');
				var prestamo=Ext.getCmp('prestamo');
				var fechaCorte=Ext.getCmp('id_FechaCorte');
				
				if(fechaCorte.getValue()==''){
					Ext.Msg.show({
						title:'Mensaje',
						msg:'Debe proporcionar la fecha de corte',
						icon: Ext.Msg.ERROR,
						buttons:Ext.Msg.OK
					});
					return false;
				}	
				var cmpForma = Ext.getCmp('forma');
            var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");  // Id barra paginacion
				paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
				consultaDataGrid.load({
					params: Ext.apply(paramSubmit,{
						informacion :  'Consultar',
						operacion   :  'Generar',
            //ic_if: objGral.ic_if,
            start: cmpBarraPaginacion.cursor,
            limit: cmpBarraPaginacion.pageSize
					})
				});
			} //fin handler			
		},			
		{
			text: 'Limpiar',
			iconCls: 'icoLimpiar',
			handler: function() {
				window.location = '13consulta6ext.jsp';
			}
		},{
			text: 'Bajar Archivo ZIP',
			id :'btnGenerarZip',
			iconCls:'icoZip',
			handler: function(boton, evento) {
				var moneda 	= Ext.getCmp('id_moneda');
				var cliente =Ext.getCmp('cliente');
				var prestamo=Ext.getCmp('prestamo');
				var fechaCorte=Ext.getCmp('id_FechaCorte');
				
				if(moneda.getValue()!='' ||  (cliente.getValue()!='' && Ext.getCmp("tipoLinea").getValue().value!='C') || prestamo.getValue()!=''){
					moneda.reset();
					if( Ext.getCmp("tipoLinea").getValue().value!='C'){
						cliente.setValue('');
					}
					prestamo.setValue('');
					Ext.Msg.show({
						title:'Mensaje',
						msg: 'Solo es necesario el criterio de Fecha de Corte para esta consulta',
						icon: Ext.Msg.ERROR,
						buttons: Ext.Msg.OK
					});
					return false;
				}
				if(fechaCorte.getValue()==''){
					Ext.Msg.show({
						title:'Mensaje',
						msg:'Debe proporcionar la fecha de corte',
						icon: Ext.Msg.ERROR,
						buttons:Ext.Msg.OK
					});
					return false;
				}
				boton.disable();
				boton.setIconClass('loading-indicator');		
        var cmpForma = Ext.getCmp('forma');
        paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
        
				Ext.Ajax.request({
					url: '13consulta6ext.data.jsp',
					params: Ext.apply(paramSubmit,{
						informacion:'DescargaZip'
					})
					,callback: procesarGenerarZip
				});
			} //fin handler
		},{
			text:'Bajar ZIP',
			id:'btnBajarZip',
			iconCls:'icoZip',
			hidden:true
		}
		]
	};//FIN DE LA FORMA

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10),
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});
  
  Ext.Ajax.request({
      url: '13consulta6ext.data.jsp',
      params: {
        informacion:'ObtenerSiracFinanciera'
        //ic_if : objGral.ic_if
      }					
      ,callback: procesarObtenerSiracFinanciera
    });
  
  Ext.getCmp("tipoLinea").hide();
  
});
