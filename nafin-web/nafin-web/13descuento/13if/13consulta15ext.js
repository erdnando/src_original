Ext.onReady(function (){ 
	Ext.QuickTips.init();
/*
	var procesarSuccessFailureGenerarXpaginaPDF = function(opts, success, response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarXpaginaPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarXpaginaPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarTotalPDF = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarTotalPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarTotalPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo = function(opts, success, response){
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarTotalPDF = function(opts,success,response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarTotalPDF').enable();
		Ext.getCmp('btnGenerarTotalPDF').setIconClass('icoPdf');
	}

	var procesarSuccessFailureGenerarArchivo = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}

	var procesarConsultaData = function(store,arrRegistros,opts){
		var fp = Ext.getCmp('forma');
		var btnGenerar = Ext.getCmp('btnGenerar');
		fp.el.mask();
		btnGenerar.enable();
		if(arrRegistros!=null){
			if(!grid.isVisible()){
				grid.show();
			}

			var dc_fecha_opMin = Ext.getCmp('dc_fecha_opMin');
			var dc_fecha_opMax = Ext.getCmp('dc_fecha_opMax');
			
			var fechaOpInicio = new Date(dc_fecha_opMin.getValue()); 
			var dfecOpIni = fechaOpInicio.getDate(); 
			var mfecOpIni = fechaOpInicio.getMonth() + 1; //meses basados en inicial 0
			if(dfecOpIni<10){
				dfecOpIni = "0" + dfecOpIni.toString();
			}
			if(mfecOpIni<10){
				mfecOpIni = "0" + mfecOpIni.toString();
			}
			var afecOpIni = fechaOpInicio.getFullYear(); 
			var fechaOperacionInicio = dfecOpIni + "/" + mfecOpIni + "/" + afecOpIni;
			
			var fechaOpFinal = new Date(dc_fecha_opMax.getValue()); 
			var dfecOpFin = fechaOpFinal.getDate(); 
			var mfecOpFin = fechaOpFinal.getMonth() + 1; //meses basados en inicial 0
			if(dfecOpFin<10){
				dfecOpFin = "0" + dfecOpFin.toString();
			}
			if(mfecOpFin<10){
				mfecOpFin = "0" + mfecOpFin.toString();
			}
			var afecOpFin = fechaOpFinal.getFullYear(); 
			var fechaOperacionFinal = dfecOpFin + "/" + mfecOpFin + "/" + afecOpFin;
			
			
			/*-----*/
			var dc_fecha_vencMin = Ext.getCmp('dc_fecha_vencMin');
			var dc_fecha_vencMax = Ext.getCmp('dc_fecha_vencMax');
			
			var fechaVencInicio = new Date(dc_fecha_vencMin.getValue()); 
			var dfecVencIni = fechaVencInicio.getDate(); 
			var mfecVencIni = fechaVencInicio.getMonth() + 1; //meses basados en inicial 0
			if(dfecVencIni<10){
				dfecVencIni = "0" + dfecVencIni.toString();
			}
			if(mfecVencIni<10){
				mfecVencIni = "0" + mfecVencIni.toString();
			}
			var afecVencIni = fechaVencInicio.getFullYear(); 
			var fechaVencInicio = dfecVencIni + "/" + mfecVencIni + "/" + afecVencIni;
			
			var fechaVencFinal = new Date(dc_fecha_vencMax.getValue()); 
			var dfecVencFin = fechaVencFinal.getDate(); 
			var mfecVencFin = fechaVencFinal.getMonth() + 1; //meses basados en inicial 0
			if(dfecVencFin<10){
				dfecVencFin = "0" + dfecVencFin.toString();
			}
			if(mfecVencFin<10){
				mfecVencFin = "0" + mfecVencFin.toString();
			}
			var afecVencFin = fechaVencFinal.getFullYear(); 
			var fechaVencFinal = dfecVencFin + "/" + mfecVencFin + "/" + afecVencFin;
			
			if(!(dc_fecha_vencMin.getValue()!=null && dc_fecha_vencMin.getValue()!='' && dc_fecha_opMin.getValue()!=null && dc_fecha_opMin.getValue()!='')){
				if(dc_fecha_vencMin.getValue()==null || dc_fecha_vencMin.getValue()==''){
					grid.setTitle('C�lculo del Rebate <br>Reporte de Operaciones del ' + fechaOperacionInicio + ' al ' + fechaOperacionFinal);
				}else
				if(dc_fecha_opMin.getValue()==null || dc_fecha_opMin.getValue()==''){
					grid.setTitle('C�lculo del Rebate <br>Reporte de Vencimientos del ' + fechaVencInicio + ' al ' + fechaVencFinal);
				}
			}
			
			//grid.setTitle('C�lculo del Rebate <br>Reporte del ' + fechaOperacionInicio + ' al ' + fechaOperacionFinal);
			
			//var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			//var btnBajarXpaginaPDF = Ext.getCmp('btnBajarXpaginaPDF');
			//var btnGenerarXpaginaPDF = Ext.getCmp('btnGenerarXpaginaPDF');
			//var btnBajarTotalPDF = Ext.getCmp('btnBajarTotalPDF');
			var btnGenerarTotalPDF = Ext.getCmp('btnGenerarTotalPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				btnTotales.enable();
				//btnGenerarXpaginaPDF.enable();
				btnGenerarTotalPDF.enable();
				//btnBajarXpaginaPDF.hide();
				btnGenerarArchivo.enable();
				//btnBajarTotalPDF.hide();
				//btnBajarArchivo.hide();
				/*if(!btnBajarArchivo.isVisible()){
					btnGenerarArchivo.enable();
				}else{
					btnGenerarArchivo.disable();
				}*/
				fp.el.unmask();
				el.unmask();
			}else{
				btnTotales.disable();
				btnGenerarArchivo.disable();
				//btnGenerarXpaginaPDF.disable();
				btnGenerarTotalPDF.disable();
				//btnBajarXpaginaPDF.hide();
				//btnBajarTotalPDF.hide();
				//btnBajarArchivo.hide();
				fp.el.unmask();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	var resumenTotalesData = new Ext.data.JsonStore({
			root: 'registros',
			url: '13consulta15ext.data.jsp',
			baseParams: {
						informacion: 'ResumenTotales'
			},
			fields: [
						{name: 'NOMBREMONEDA'},
						{name: 'MONTOTOTALINTERESES', type: 'float'},
						{name: 'MONTOTOTALREBATE', type: 'float'}
			],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
						exception: NE.util.mostrarDataProxyError
			}
	});
//-----------------------------------STORES-------------------------------------
	var catalogoMonedaData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '13consulta15ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//-----------------------------------STORES-------------------------------------
	var catalogoEPOData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '13consulta15ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPOPuntosRebate'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '13consulta15ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'NUMERODOCUMENTO'},
					{name: 'NUMEROPROVEEDOR'},
					{name: 'NOMBREPROVEEDOR'},
					{name: 'NOMBREEPO'},
					{name: 'FECHANOTIFICACION'},
					{name: 'FECHAVENCIMIENTO'},
					{name: 'MONTODOCUMENTO'},
					{name: 'MONEDA'},
					{name: 'PLAZOENDIAS'},
					{name: 'TASAOPERADA'},
					{name: 'MONTOINTERESES'},
					{name: 'PUNTOSREBATE'},
					{name: 'MONTOREBATE'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
						fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaData(null,null,null);//Llama procesar consulta, para que desbloquee los componentes.
						}
			}
		}
	});
//-------------------------------COMPONENTES------------------------------------
	var elementosForma = [
									{
										xtype: 'combo',
										name: 'ic_epo',
										id: 'cmbEPO',
										allowBlank: true,
										fieldLabel: 'Nombre de la EPO',
										emptyText: 'Seleccione la EPO',
										mode: 'local',
										displayField: 'descripcion',
										valueField: 'clave',
										hiddenName: 'ic_epo',
										forceSelection: true,
										triggerAction: 'all',
										typeAhead: true,
										minChars: 1,
										store: catalogoEPOData,
										tpl: NE.util.templateMensajeCargaCombo
									},
									{
										xtype: 'combo',
										name: 'ic_moneda',
										fieldLabel: 'Moneda',
										allowBlank: true,
										emptyText: 'Selecione Moneda',
										mode: 'local',
										displayField: 'descripcion',
										valueField: 'clave',
										hiddenName: 'ic_moneda',
										forceSelection: true,
										triggerAction: 'all',
										typeAhead: true,
										minChars: 1,
										store: catalogoMonedaData,
										tpl: NE.util.templateMensajeCargaCombo
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Fecha de operaci�n',
										msgTarget: 'side',
										combineErrors: false,
										items: [
													{
														xtype: 'datefield',
														name: 'df_fecha_operacion_de',
														id: 'dc_fecha_opMin',
														allowBlank: false,
														startDay: 0,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoFinFecha: 'dc_fecha_opMax',
														margins: '0 20 0 0' //Necesario para mostrar el icono de error
													},
													{
														xtype: 'displayfield',
														value: 'al',
														width: 24
													},
													{
														xtype: 'datefield',
														name: 'df_fecha_operacion_a',
														id: 'dc_fecha_opMax',
														allowBlank: false,
														startDay: 1,
														width: 100,
														msgTarget: 'side',
														vtype: 'rangofecha',
														campoInicioFecha: 'dc_fecha_opMin',
														margins: '0 20 0 0'
													}
										]
								},
								{
									xtype: 'compositefield',
									fieldLabel: 'Fecha de Vencimiento',
									msgTarget: 'side',
									combineErrors: false,
									items: [
												{
													xtype: 'datefield',
													name: 'df_fecha_venc_de',
													id: 'dc_fecha_vencMin',
													allowBlank: true,
													startDay: 0,
													width: 100,
													msgTarget: 'side',
													vtype: 'rangofecha',
													campoFinFecha: 'dc_fecha_vencMax',
													margins: '0 20 0 0' //Necesario para mostrar el icono de error
												},
												{
													xtype: 'displayfield',
													value: 'al',
													width: 24
												},
												{
													xtype: 'datefield',
													name: 'df_fecha_venc_a',
													id: 'dc_fecha_vencMax',
													allowBlank: true,
													startDay: 1,
													width: 100,
													msgTarget: 'side',
													vtype: 'rangofecha',
													campoInicioFecha: 'dc_fecha_vencMin',
													margins: '0 20 0 0'
												}
									]
								}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfiel',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
						{
							text: 'Generar',
							id: 'btnGenerar',
							iconCls: 'icoBuscar',
							formBind: true,
							handler: function (boton,evento){
								grid.hide();

								var totales = Ext.getCmp('gridTotales');
								if(totales.isVisible()){
									totales.hide();
								}
								var fechaOperacionMin = Ext.getCmp("dc_fecha_opMin");
								var fechaOperacionMax = Ext.getCmp("dc_fecha_opMax");

								var fechaVencMin = Ext.getCmp("dc_fecha_vencMin");
								var fechaVencMax = Ext.getCmp("dc_fecha_vencMax");
								
								
								if(  ( Ext.isEmpty(fechaOperacionMin.getValue())  ||   Ext.isEmpty(fechaOperacionMax.getValue())   )  
									&& ( Ext.isEmpty(fechaVencMin.getValue())  &&   Ext.isEmpty(fechaVencMax.getValue()) )   ){									
										fechaOperacionMin.markInvalid('Es necesario ingresar una fecha de operaci�n.');
										fechaOperacionMax.markInvalid('Es necesario ingresar una fecha de operaci�n.');									
										return;
								} 
								

								if(!Ext.isEmpty(fechaOperacionMin.getValue())&&!Ext.isEmpty(fechaOperacionMax.getValue())){
									var fechaOpMin = fechaOperacionMin.getValue();
									var fechaOpMax = fechaOperacionMax.getValue();
									var rangoPermitido= fechaOpMax - fechaOpMin;
									var days = Math.round(rangoPermitido/(1000*60*60*24));
									if(days > 30){									
										fechaOperacionMax.markInvalid('Favor de ingresar un rango no mayor a 30 d�as.');
										return;
									}
								}
							
							
								if(   ( Ext.isEmpty(fechaVencMin.getValue())  ||   Ext.isEmpty(fechaVencMax.getValue())  )  
								&& (Ext.isEmpty(fechaOperacionMin.getValue())  ||   Ext.isEmpty(fechaOperacionMax.getValue()) )  ){									
										fechaVencMin.markInvalid('Es necesario ingresar una fecha de vencimiento.');
										fechaVencMax.markInvalid('Es necesario ingresar una fecha de vencimiento.');
										return;
									}
								

								if(!Ext.isEmpty(fechaVencMin.getValue())&&!Ext.isEmpty(fechaVencMax.getValue())){
									var fechaOpMin = fechaVencMin.getValue();
									var fechaOpMax = fechaVencMax.getValue();
									var rangoPermitido= fechaOpMax - fechaOpMin;
									var days = Math.round(rangoPermitido/(1000*60*60*24));
									if(days > 30){
										Ext.Msg.alert('Mensaje informativo','Favor de ingresar un rango no mayor a 30 d�as.');
										fechaVencMax.markInvalid('Favor de ingresar un rango no mayor a 30 d�as.');
										return;
									}
								}
								
								boolValida = true;
								
								if(!Ext.isEmpty(fechaOperacionMin.getValue()) && !Ext.isEmpty(fechaOperacionMax.getValue())){							
						
									var fechaIni_ = Ext.util.Format.date(fechaOperacionMin.getValue(),'d/m/Y');
									var fechaFin_ = Ext.util.Format.date(fechaOperacionMax.getValue(),'d/m/Y');
									
									if(datecomp(fechaIni_,fechaFin_)==1) {
										fechaOperacionMin.markInvalid("La fecha de este campo debe de ser anterior a."+fechaIni_);
										fechaOperacionMin.focus();
										boolValida = false;
										return;
									}
								}
					
							
								if(!Ext.isEmpty(fechaVencMin.getValue()) && !Ext.isEmpty(fechaVencMax.getValue())){							
						
									var fechaIniVen = Ext.util.Format.date(fechaVencMin.getValue(),'d/m/Y');
									var fechaFinVen = Ext.util.Format.date(fechaVencMax.getValue(),'d/m/Y');
									
									if(datecomp(fechaIniVen,fechaFinVen)==1) {
										fechaVencMin.markInvalid("La fecha de este campo debe de ser anterior a."+fechaIniVen);
										fechaVencMin.focus();	
										boolValida = false;
										return;
									}
								}
								if(boolValida)  {
									fp.el.mask('Enviando...','x-msk-loading');
									consultaData.load({
										params: Ext.apply(fp.getForm().getValues(),{
											operacion: 'Generar',
											start: 0,
											limit: 15
										})
									});
								}
									
							}
						},
						{
							text: 'Limpiar',
							iconCls: 'icoLimpiar',
							handler: function(){
								document.location.href = "13consulta15ext.jsp";
							}
						}
		]
	});
	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
						{
							header: 'N�mero de Documento',
							tooltip: 'N�mero de Documento',
							dataIndex: 'NUMERODOCUMENTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 250
						},
						{
							header: 'N�mero de Proveedor',
							tooltip: 'N�mero de Proveedor',
							dataIndex: 'NUMEROPROVEEDOR',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 250
						},
						{
							header: 'Nombre Proveedor',
							tooltip: 'Nombre Proveedor',
							dataIndex: 'NOMBREPROVEEDOR',
							sortable: true,
							resiazable: true,
							align: 'left',
							width: 250
						},
						{
							header: 'EPO',
							tooltip: 'EPO',
							dataIndex: 'NOMBREEPO',
							sortable: true,
							resiazable: true,
							align: 'left',
							width: 250
						},
						{
							header: 'Fecha de Notificaci�n',
							tooltip: 'Fecha de Notificaci�n',
							dataIndex: 'FECHANOTIFICACION',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 100
						},
						{
							header: 'Fecha de Vencimiento',
							tooltip: 'Fecha de Vencimiento',
							dataIndex: 'FECHAVENCIMIENTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 100
						},
						{
							header: 'Monto del Documento',
							tooltip: 'Monto del Documento',
							dataIndex: 'MONTODOCUMENTO',
							sortable: true,
							resiazable: true,
							align: 'right',
							width: 100,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Moneda',
							tooltip: 'Moneda',
							dataIndex: 'MONEDA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 100
						},
						{
							header: 'Plazo en d�as',
							tooltip: 'Plazo en d�as',
							dataIndex: 'PLAZOENDIAS',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 100
						},
						{
							header: 'Tasa Operada',
							tooltip: 'Tasa Operada',
							dataIndex: 'TASAOPERADA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 100,
							renderer: Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto Intereses',
							tooltip: 'Monto Intereses',
							dataIndex: 'MONTOINTERESES',
							sortable: true,
							resiazable: true,
							align: 'right',
							width: 100,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Puntos Rebate',
							tooltip: 'Puntos Rebate',
							dataIndex: 'PUNTOSREBATE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 100,
							renderer: Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto del Rebate',
							tooltip: 'Monto del Rebate',
							dataIndex: 'MONTOREBATE',
							sortable: true,
							resiazable: true,
							align: 'right',
							width: 100,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		style: 'margin:0 auto',
		title: 'Calculo del Rebate',
		frame: true,
		bbar: {
					xtype: 'paging',
					pageSize: 15,
					buttonAling: 'left',
					id: 'barraPaginacion',
					displayInfo: true,
					store: consultaData,
					displayMsg: '{0} - {1} de {2}',
					emptyMsg: "No hay registros.",
					items: [
								'->','-',
								{
									xtype: 'button',
									text: 'Totales',
									id: 'btnTotales',
									hidden: false,
									handler: function (boton,evento) {
												resumenTotalesData.load();
												var gridTotales = Ext.getCmp('gridTotales');
												if(!gridTotales.isVisible()){
													gridTotales.show();
													gridTotales.el.dom.scrollIntoView();
												};
									}
								},
								'-',
								{
									xtype: 'button',
									text: 'Generar Archivo',
									tooltip: 'Imprime los registros en formato CSV.',
									iconCls: 'icoXls',
									id: 'btnGenerarArchivo',
									handler: function(boton,evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
											url: '13consulta15ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'ArchivoCSV'
											}),
											callback: procesarSuccessFailureGenerarArchivo
										});
									}
								},/*
								{
									xtype: 'button',
									text: 'Bajar Archivo',
									id: 'btnBajarArchivo',
									hidden: true
								},
								'-',
								{
									xtype: 'button',
									text: 'Generar x P�gina PDF',
									id: 'btnGenerarXpaginaPDF',
									handler: function(boton,evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
										Ext.Ajax.request({
											url: '13consulta15ext.data.jsp',
											params: Ext.apply(fp.getForm().getValues(),{
													informacion: 'ArchivoPaginaPDF',
													start: cmpBarraPaginacion.cursor,
													limit: cmpBarraPaginacion.pageSize
													
											}),
											callback: procesarSuccessFailureGenerarXpaginaPDF
										});
									}
								},
								{
									xtype: 'button',
									text: 'Bajar x P�gina PDF',
									id: 'btnBajarXpaginaPDF',
									hidden: true
								},*/
								'-',
								{
									xtype: 'button',
									text: 'Generar Todo',
									tooltip: 'Imprime los registros en formato PDF.',
									iconCls: 'icoPdf',
									id: 'btnGenerarTotalPDF',
									handler: function(boton,evento){
										boton.disable();
										boton.setIconClass('loading-indicator');
										Ext.Ajax.request({
												url: '13consulta15ext.data.jsp',
												params: Ext.apply(fp.getForm().getValues(),{
														informacion: 'ArchivoPDF'
												}),
												callback: procesarSuccessFailureGenerarTotalPDF
										});
									}
								}/*,
								{
									xtype: 'button',
									text: 'Bajar Todo PDF',
									id: 'btnBajarTotalPDF',
									hidden: true
								}*/
					]
		}
	});
	var gridTotales = {
		xtype: 'grid',
		store: resumenTotalesData,
		id: 'gridTotales',
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
						{
							header: 'TOTALES',
							dataIndex: 'NOMBREMONEDA',
							align: 'center',
							width: 250
						},
						{
							header: 'Monto Intereses',
							dataIndex: 'MONTOTOTALINTERESES',
							width: 150,
							align: 'right',
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Monto de Rebate',
							dataIndex: 'MONTOTOTALREBATE',
							width: 150,
							align: 'right',
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						}
		],
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
					{
						id: 'close',
						handler: function(evento,toolEl,panel,tc){
									panel.hide();
						}
					}
		],
		frame: false
	};
//----------------------------------PRINCIPAL-----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
					fp,
					NE.util.getEspaciador(20),
					grid,
					gridTotales
		]
	});
	catalogoEPOData.load();
	catalogoMonedaData.load();
});