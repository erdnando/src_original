<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.model.catalogos.*,
	com.netro.descuento.*, 	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") != null) ? request.getParameter("ic_cambio_estatus") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String  ic_if =iNoCliente,  infoRegresar ="", consulta="", 
bTipoFactoraje ="N", bOperaFactorajeDist  ="N", bOperaFactVencimientoInfonavit ="N", bOperaFactAutomatico ="N",  sOperaFactConMandato ="N";
SimpleDateFormat formatoHora2 = new SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a");

ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

Hashtable  alParamIF =BeanParamDscto. parametrizacionIF(ic_if);
if (alParamIF!=null) {
	bTipoFactoraje	=	alParamIF.get("bTipoFactoraje").toString();
	bOperaFactorajeDist	=	alParamIF.get("bOperaFactorajeDist").toString();
	bOperaFactVencimientoInfonavit	=	alParamIF.get("PUB_EPO_VENC_INFONAVIT").toString();
   bOperaFactAutomatico = alParamIF.get("cs_factoraje_automatico").toString();
	sOperaFactConMandato	 =	alParamIF.get("PUB_EPO_OPERA_MANDATO").toString(); 
}	

JSONObject jsonObj = new JSONObject();

if (informacion.equals("valoresIniciales")  ) {

	jsonObj.put("success",new Boolean(true));
	jsonObj.put("strNombreUsuario", strNombreUsuario);
	jsonObj.put("fechaHora",formatoHora2.format(new java.util.Date()));
	infoRegresar = jsonObj.toString();


} else  if (informacion.equals("catalogoEstatus")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_cambio_estatus");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_cambio_estatus");		
	catalogo.setValoresCondicionIn("2,7,32", Integer.class);
	catalogo.setOrden("ic_cambio_estatus");
	infoRegresar = catalogo.getJSONElementos();	
	
} else  if (informacion.equals("Consultar")  ||  informacion.equals("ResumenTotales")  ) {

	ConsCambioEstatusIF paginador = new ConsCambioEstatusIF();
	paginador.setIc_cambio_estatus(ic_cambio_estatus);
	paginador.setClaveIF(ic_if);
	paginador.setBTipoFactoraje(bTipoFactoraje);
	paginador.setBOperaFactorajeDist(bOperaFactorajeDist);
	paginador.setBOperaFactVencimientoInfonavit(bOperaFactVencimientoInfonavit);
   paginador.setBOperaFactAutomatico(bOperaFactAutomatico);
	paginador.setSOperaFactConMandato(sOperaFactConMandato);    
	
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	if (operacion.equals("Generar") ){
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			jsonObj = JSONObject.fromObject(consulta);
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}	
	} else if (operacion.equals("ArchivoPDF")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}				
	} else if (operacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");				
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}		
	}
	
	
	if ( informacion.equals("ResumenTotales")  ) {
	
		Registros reg	=	queryHelper.doSearch();		
		double nacional = 0, nacional_desc = 0, nacional_int = 0, nacional_descIF = 0, 
		dolares = 0, dolares_desc = 0,  dolares_int = 0,  dolares_descIF = 0, dolares_descIMP  =0,
		nacional_descIMP  =0;
		
		int  doc_nacional =0,doc_dolares =0;
		HashMap registrosTot = new HashMap();	
		JSONArray registrosTotales = new JSONArray();
		
		while (reg.next()) {
			
			String ic_moneda = (reg.getString("IC_MONEDA") == null) ? "" : reg.getString("IC_MONEDA");
			String monto_docto = (reg.getString("MONTO_DOCTO") == null) ? "0" : reg.getString("MONTO_DOCTO");
			String monto_desc = (reg.getString("MONTO_DESCONTAR") == null) ? "0" : reg.getString("MONTO_DESCONTAR");
			String monto_int = (reg.getString("MONTO_INT") == null) ? "0" : reg.getString("MONTO_INT");
			String monto_oper = (reg.getString("MONTO_OPERAR") == null) ? "0" : reg.getString("MONTO_OPERAR");				
		
			if (ic_moneda.equals("1")) {	
				nacional+=Double.parseDouble((String)monto_docto);
				nacional_desc+=Double.parseDouble((String)monto_desc);  
				nacional_int+=Double.parseDouble((String)monto_int);    
				nacional_descIF+=Double.parseDouble((String)monto_oper); 
				
				doc_nacional++;
			}
			if (ic_moneda.equals("54")) {	
				dolares+=Double.parseDouble((String)monto_docto);
				dolares_desc+=Double.parseDouble((String)monto_desc);  
				dolares_int+=Double.parseDouble((String)monto_int);    
				dolares_descIF+=Double.parseDouble((String)monto_oper); 				
				doc_dolares++;
			}	
		}
		
		for(int t =0; t<2; t++) {		
			registrosTot = new HashMap();		
			if(t==0){ 				
				registrosTot.put("MONEDA", "Total MN");
				registrosTot.put("NUM_DOCTOS", Double.toString(doc_nacional));		
				registrosTot.put("TOTAL_MONTO_DOCTO",Double.toString (nacional) );					
				registrosTot.put("TOTAL_MONTO_DESC", Double.toString (nacional_desc) );
				registrosTot.put("TOTAL_MONTO_INT", Double.toString (nacional_int) );
				registrosTot.put("TOTAL_MONTO_OPER", Double.toString (nacional_descIF) );
				
			}
			if(t==1){ 			
				registrosTot.put("MONEDA", "Total USD");
				registrosTot.put("NUM_DOCTOS", Double.toString(doc_dolares));		
				registrosTot.put("TOTAL_MONTO_DOCTO", Double.toString (dolares));					
				registrosTot.put("TOTAL_MONTO_DESC", Double.toString (dolares_desc) );
				registrosTot.put("TOTAL_MONTO_INT", Double.toString (dolares_int) );
				registrosTot.put("TOTAL_MONTO_OPER", Double.toString (dolares_descIF) );
				
			}
			registrosTotales.add(registrosTot);		
		}
			infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";					
	}
	
	if (informacion.equals("Consultar")){
		jsonObj.put("bTipoFactoraje",bTipoFactoraje);
		jsonObj.put("bOperaFactorajeDist",bOperaFactorajeDist);
		jsonObj.put("bOperaFactVencimientoInfonavit",bOperaFactVencimientoInfonavit);
      jsonObj.put("bOperaFactAutomatico", bOperaFactAutomatico);
		jsonObj.put("sOperaFactConMandato",sOperaFactConMandato);
		jsonObj.put("ic_cambio_estatus",ic_cambio_estatus);		
		infoRegresar	=	jsonObj.toString();	
	}			
}
	
%>
<%=infoRegresar%>
