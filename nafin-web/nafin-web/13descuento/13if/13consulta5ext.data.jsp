<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	netropology.utilerias.*,
	com.netro.threads.*,
	com.netro.parametrosgrales.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.pdf.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="../13secsession.jspf" %>
<%  

/*** OBJETOS ***/
//CQueryHelperRegExtJS queryHelper;
//CQueryHelperThreadRegExtJS queryHelper;
JSONObject jsonObj;
JSONArray jsonArr;
/*** FIN DE OBJETOS ***/

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String informacion   = (request.getParameter("informacion") == null) ? "" : request.getParameter("informacion");
String envia         = (request.getParameter("envia") == null) ? "" : request.getParameter("envia");
String icEpo         = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
String dfFechaNotMin = (request.getParameter("df_fecha_notMin") == null) ? "" : request.getParameter("df_fecha_notMin");
String dfFechaNotMax = (request.getParameter("df_fecha_notMax") == null) ? "" : request.getParameter("df_fecha_notMax");
String dfFechaVencMin= (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
String dfFechaVencMax= (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
String ccAcuse3      = (request.getParameter("cc_acuse3") == null) ? "" : request.getParameter("cc_acuse3");
String fechaActual   = (request.getParameter("FechaActual") == null) ? "" : request.getParameter("FechaActual");
String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
String tipo = (request.getParameter("tipo") == null) ? "" : request.getParameter("tipo");

String ic_if = iNoCliente;
String resultado			= null;
String epoDefault 		= "";
String qrySentencia		= "";
String condicion 			= "";
int start					= 0;
int limit 					= 0;
AccesoDB con 				= null;
PreparedStatement ps 	= null;
ResultSet rs 				= null;

/*** FIN DE PARAMETROS QUE PROVIENEN DEL JS ***/

boolean bOperaFactorajeVencido = false;
boolean bOperaFactorajeVencidoGral = false;
boolean bOperaFactorajeDist = false;
boolean bOperaFactorajeDistGral = false;
boolean bOperaFactorajeMandatoGral = false;
boolean bOperaFactConMandato = false;
boolean bTipoFactoraje = false;
boolean bOperaFactVencInfonavit = false;//FODEA 042 - 2009 ACF
boolean bOperaFactVencInfonavitGral = false;//FODEA 042 - 2009 ACF
   

if(   informacion.equals("IniciaPrograma") ){
   //_________________________ INICIO DEL PROGRAMA _________________________
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	
	sOperaFactConMandato = new String("");
	Hashtable alParamEPO = new Hashtable();
	//System.out.println("���� icEpo ���� "+icEpo+" ����");
	if(!icEpo.equals("")) {
		alParamEPO = BeanParamDscto.getParametrosEPO(icEpo, 1);
		if (alParamEPO!=null) {
		bOperaFactConMandato   		= ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido 		= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		bOperaFactorajeDist   		= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		bOperaFactVencInfonavit    = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;//FODEA 042 - 2009 ACF
		}
	}
	sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
	//System.out.println("���� sOperaFactConMandato -:consulta:- �� "+ sOperaFactConMandato+" ����");

	StringBuffer tipoFactCombo = new StringBuffer();
	tipoFactCombo.append("'N'");//el tipo de Factoraje normal aparecera por default
	if(bOperaFactorajeDist){ tipoFactCombo.append(",'D'"); }
	if(bOperaFactorajeVencido){ tipoFactCombo.append(",'V'"); }
	if(bOperaFactConMandato){ tipoFactCombo.append(",'M'"); }
	if(bOperaFactVencInfonavit){ tipoFactCombo.append(",'I'"); }//FODEA 042 - 2009 ACF
   
   con   = new AccesoDB();
   try{      
      con.conexionDB();

		String selected = "";

		qrySentencia=
			" SELECT COUNT (1)"   +
			"   FROM comrel_if_epo"   +
			"  WHERE cs_vobo_nafin = 'S'"   +
			"    AND ic_if = ?"   +
			"    AND ic_epo IN (SELECT ic_epo"   +
			"                     FROM comrel_producto_epo"   +
			"                    WHERE ic_producto_nafin = 1"   +
			"                      AND cs_factoraje_vencido = 'S')"  ;

		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,iNoCliente);
		rs = ps.executeQuery();
		if (rs.next()) {
			if(rs.getInt(1)>0) bOperaFactorajeVencidoGral = true;
		}
		rs.close();ps.close();

		qrySentencia=
			" SELECT COUNT (1)"   +
			"   FROM comrel_if_epo"   +
			"  WHERE cs_vobo_nafin = 'S'"   +
			"    AND ic_if = ?"   +
			"    AND ic_epo IN (SELECT ic_epo"   +
			"                     FROM comrel_producto_epo"   +
			"                    WHERE ic_producto_nafin = 1"   +
			"                      AND cs_factoraje_distribuido = 'S')"  ;

		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,iNoCliente);
		rs = ps.executeQuery();
		if (rs.next()) {
			if(rs.getInt(1)>0) bOperaFactorajeDistGral = true;
		}
		rs.close();ps.close();

		qrySentencia=
			" SELECT COUNT (1)"   +
         "   FROM comrel_if_epo"   +
			"  WHERE cs_vobo_nafin = 'S'"   +
			"    AND ic_if = ?"   +
			"    AND ic_epo IN (SELECT ic_epo"   +
			"                     FROM com_parametrizacion_epo"   +
			"                    WHERE CC_PARAMETRO_EPO = 'PUB_EPO_OPERA_MANDATO'"   +
			"                      AND CG_VALOR = 'S')"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,iNoCliente);
			rs = ps.executeQuery();
			bOperaFactorajeMandatoGral = false;
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeMandatoGral = true; }
			rs.close();ps.close();
			//FODEA 042 - 2009 ACF (I)
			StringBuffer strSQL = new StringBuffer();
			strSQL.append(" SELECT COUNT (1)");
			strSQL.append(" FROM comrel_if_epo");
			strSQL.append(" WHERE cs_vobo_nafin = ?");
			strSQL.append(" AND ic_if = ?");
			strSQL.append(" AND ic_epo IN (SELECT ic_epo");
			strSQL.append(" FROM com_parametrizacion_epo");
			strSQL.append(" WHERE CC_PARAMETRO_EPO = ?");
			strSQL.append(" AND CG_VALOR = ?)");

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1, "S");
			ps.setInt(2, Integer.parseInt(iNoCliente));
			ps.setString(3, "PUB_EPO_VENC_INFONAVIT");
			ps.setString(4, "S");

			rs = ps.executeQuery();
			if(rs.next()){if(rs.getInt(1) > 0){bOperaFactVencInfonavitGral = true;}}
			rs.close();

			ps.close();
			//FODEA 042 - 2009 ACF (F)
   } catch(Exception e) {
      e.printStackTrace();
      out.println(e);
   } finally {
      if (con.hayConexionAbierta() == true)
      con.cierraConexionDB();
   }
   
   bOperaFactorajeVencido = false;
   if(icEpo.equals("")) {
      if(bOperaFactorajeVencidoGral)
         bOperaFactorajeVencido = true;
      if(bOperaFactorajeDistGral)
         bOperaFactorajeDist = true;
      if(bOperaFactorajeMandatoGral)
         bOperaFactConMandato = true;
      if(bOperaFactVencInfonavitGral){bOperaFactVencInfonavit = true;}//FODEA 042 - 2009 ACF
   }
   bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDist || bOperaFactConMandato || bOperaFactVencInfonavit)?true:false;//FODEA 042 - 2009 ACF

	ParametrosGrales parametrosbean = ServiceLocator.getInstance().lookup("ParametrosGralesEJB", ParametrosGrales.class);

	HashMap 	permisoConsultaReportesEPO = parametrosbean.getPermisoConsultaReportesEPO();
	boolean	permisoConsulta 				= "true".equals((String)permisoConsultaReportesEPO.get("PERMISO_CONSULTA"))?true:false;
	String   horaReporte						= (String)permisoConsultaReportesEPO.get("HORA_REPORTE");
	boolean 	limitarRangoFechas 			= !permisoConsulta;
	
	
   jsonObj = new JSONObject();
   jsonObj.put("success",                 new Boolean(true));
   jsonObj.put("bTipoFactoraje",          Boolean.toString(bTipoFactoraje));
   jsonObj.put("bOperaFactorajeVencido",  Boolean.toString(bOperaFactorajeVencido));
   jsonObj.put("bOperaFactorajeDist",     Boolean.toString(bOperaFactorajeDist));
	jsonObj.put("limitarRangoFechas",     Boolean.toString(limitarRangoFechas));
   resultado = jsonObj.toString();
}

 
/*** INICIO CATALOGO EPO ***/
else if(informacion.equals("catalogoEPO")){
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.set_claveIf(iNoCliente);
	resultado = cat.getJSONElementos();
} /*** FIN DEL CATALOGO DE BANCO FONDEO***/
 
/*** INICIO CATALOGO TIPO FACTORAJE ***/
if(informacion.equals("catalogoTipoFactoraje")){
	bOperaFactorajeVencido = false;
	bOperaFactorajeDist = false;
	bOperaFactConMandato = false;
	bTipoFactoraje = false;
	bOperaFactVencInfonavit = false;//FODEA 042 - 2009 ACF
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	sOperaFactConMandato = new String("");
	Hashtable alParamEPO = new Hashtable();
	
	if(!icEpo.equals("")) {
		alParamEPO = BeanParamDscto.getParametrosEPO(icEpo, 1);
		if (alParamEPO!=null) {
			bOperaFactConMandato   		= ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			bOperaFactorajeVencido 		= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			bOperaFactorajeDist   		= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
			bOperaFactVencInfonavit 	= ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;//FODEA 042 - 2009 ACF
		}
	}	
   
   sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
   
   StringBuffer tipoFactCombo = new StringBuffer();
	tipoFactCombo.append("'N'");//el tipo de Factoraje normal aparecera por default
	if(bOperaFactorajeDist){ tipoFactCombo.append(",'D'"); }
	if(bOperaFactorajeVencido){ tipoFactCombo.append(",'V'"); }
	if(bOperaFactConMandato){ tipoFactCombo.append(",'M'"); }
	if(bOperaFactVencInfonavit){ tipoFactCombo.append(",'I'"); }//FODEA 042 - 2009 ACF
   
  con   = new AccesoDB();
   try{      
      con.conexionDB();
      
      String selected = "";

		qrySentencia=
			" SELECT COUNT (1)"   +
			"   FROM comrel_if_epo"   +
			"  WHERE cs_vobo_nafin = 'S'"   +
			"    AND ic_if = ?"   +
			"    AND ic_epo IN (SELECT ic_epo"   +
			"                     FROM comrel_producto_epo"   +
			"                    WHERE ic_producto_nafin = 1"   +
			"                      AND cs_factoraje_vencido = 'S')"  ;

		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,iNoCliente);
		rs = ps.executeQuery();
		if (rs.next()) {
			if(rs.getInt(1)>0) bOperaFactorajeVencidoGral = true;
		}
		rs.close();ps.close();

		qrySentencia=
			" SELECT COUNT (1)"   +
			"   FROM comrel_if_epo"   +
			"  WHERE cs_vobo_nafin = 'S'"   +
			"    AND ic_if = ?"   +
			"    AND ic_epo IN (SELECT ic_epo"   +
			"                     FROM comrel_producto_epo"   +
			"                    WHERE ic_producto_nafin = 1"   +
			"                      AND cs_factoraje_distribuido = 'S')"  ;

		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,iNoCliente);
		rs = ps.executeQuery();
		if (rs.next()) {
			if(rs.getInt(1)>0) bOperaFactorajeDistGral = true;
		}
		rs.close();ps.close();

		qrySentencia=
			" SELECT COUNT (1)"   +
         "   FROM comrel_if_epo"   +
			"  WHERE cs_vobo_nafin = 'S'"   +
			"    AND ic_if = ?"   +
			"    AND ic_epo IN (SELECT ic_epo"   +
			"                     FROM com_parametrizacion_epo"   +
			"                    WHERE CC_PARAMETRO_EPO = 'PUB_EPO_OPERA_MANDATO'"   +
			"                      AND CG_VALOR = 'S')"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,iNoCliente);
			rs = ps.executeQuery();
			bOperaFactorajeMandatoGral = false;
			if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeMandatoGral = true; }
			rs.close();ps.close();
			//FODEA 042 - 2009 ACF (I)
			StringBuffer strSQL = new StringBuffer();
			strSQL.append(" SELECT COUNT (1)");
			strSQL.append(" FROM comrel_if_epo");
			strSQL.append(" WHERE cs_vobo_nafin = ?");
			strSQL.append(" AND ic_if = ?");
			strSQL.append(" AND ic_epo IN (SELECT ic_epo");
			strSQL.append(" FROM com_parametrizacion_epo");
			strSQL.append(" WHERE CC_PARAMETRO_EPO = ?");
			strSQL.append(" AND CG_VALOR = ?)");

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1, "S");
			ps.setInt(2, Integer.parseInt(iNoCliente));
			ps.setString(3, "PUB_EPO_VENC_INFONAVIT");
			ps.setString(4, "S");

			rs = ps.executeQuery();
			if(rs.next()){if(rs.getInt(1) > 0){bOperaFactVencInfonavitGral = true;}}

         session = pageContext.getSession();
			sesIdiomaUsuario = (String) session.getAttribute("sesIdiomaUsuario");
			sesIdiomaUsuario = (sesIdiomaUsuario == null)?"":sesIdiomaUsuario;
			String sesTipoUsuario = (String) session.getAttribute("strTipoUsuario");
			String idioma = (sesIdiomaUsuario.equals("EN") && "EPO".equals(sesTipoUsuario))?"_ing":"";
			
			condicion = "  where cc_tipo_factoraje ";
         condicion += " in ("+tipoFactCombo+") ";
         
		/*	if (!tipoFactCombo.equals("")) {
				condicion += (condicion.equals(""))?" where cc_tipo_factoraje ":" and cc_tipo_factoraje ";

				condicion += " in ("+tipoFactCombo+") ";
			}
*/
				condicion += " order by 1 ";
			
			qrySentencia = "Select ";

			qrySentencia += "cc_tipo_factoraje, cg_nombre" + idioma + 
					" from comcat_tipo_factoraje"+condicion;
         
         System.err.println("...:::::: qrySentencia ::::::. " + qrySentencia);
         
			String cadena = "";

			out = pageContext.getOut();

			con.conexionDB();
			rs = con.queryDB(qrySentencia);
         
         List reg = new ArrayList();
         HashMap datos;
			while(rs.next()) {
            datos = new HashMap();
            datos.put("clave",rs.getString(1));
            datos.put("descripcion",rs.getString(2));
            reg.add(datos);
			}
		jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("total", "1");
		jsonObj.put("registros", reg);
		resultado=jsonObj.toString();
	}catch (Exception e) {
      System.err.println("Error: " + e);
   } finally {
      if (con.hayConexionAbierta()) {
         con.cierraConexionDB();
      }
   }
} /*** FIN DEL CATALOGO TIPO FACTORAJE ***/

/*** INICIO CONSULTA ***/
if(informacion.equals("Consulta")) {
	String operacion        = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
	String operacion_2      = (request.getParameter("operacion_2")==null)?"":request.getParameter("operacion_2");
   String bTipoFactoraje_2 = (request.getParameter("bTipoFactoraje")==null)?"":request.getParameter("bTipoFactoraje");
   if(!bTipoFactoraje_2.equals("")){
      tipoFactoraje = bTipoFactoraje_2;
   }
   
   try{
      start = Integer.parseInt(request.getParameter("start"));
      limit = Integer.parseInt(request.getParameter("limit"));
   }catch(Exception e){
      System.err.println(e);
   }
	
	AvisNotiIfDE paginador = new AvisNotiIfDE(	);
	paginador.setINoCliente(	iNoCliente	);
	paginador.setDfFechaNotMin(	dfFechaNotMin	);
	paginador.setDfFechaNotMax(	dfFechaNotMax	);
	paginador.setFechaActual(	fechaActual	);
	paginador.setDfFechaVencMin(	dfFechaVencMin	);
	paginador.setDfFechaVencMax(	dfFechaVencMax	);
	paginador.setIcEpo(	icEpo	);
	paginador.setCcAcuse3(	ccAcuse3	);
	paginador.setTipoFactoraje(	tipoFactoraje	);
	paginador.setTipo(	tipo	);
	
	//CQueryHelperRegExtJS queryHelper	= new CQueryHelperRegExtJS(paginador); 
	CQueryHelperThreadRegExtJS queryHelper = new CQueryHelperThreadRegExtJS(paginador);

	if((informacion.equals("Consulta") && operacion.equals("Generar")) || (informacion.equals("Consulta") && operacion.equals(""))){
		try {
			if(operacion.equals("Generar")){ //Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			String cadena = queryHelper.getJSONPageResultSet(request,start,limit);
			resultado	= cadena;
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if(operacion.equals("XLS") || operacion.equals("XLSNotaC")){
      String nombreArchivo = "";
      
      if(operacion.equals("XLSNotaC")){
         
         ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
         
         // Fodea 002 - 2010
         ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
         // -- VERIFICAR SI LA EPO TIENE PARAMETRIZADO EL USO DE UNA NOTA DE CREDITO EN MULTIPLES DOCUMENTOS --
         // Fodea 002 - 2010
         boolean operaNotasDeCredito                = false;
         boolean aplicarNotasDeCreditoAVariosDoctos = false;
         
         			// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
         String paginaNo= (request.getParameter("paginaNo") == null) ? "1" : request.getParameter("paginaNo"),
		 	nomArchivo     = (request.getParameter("nomArchivo") == null) ? "" : request.getParameter("nomArchivo");
         
         if(!icEpo.equals("")){
            // Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
            operaNotasDeCredito                = BeanParamDscto.operaNotasDeCreditoParaDescuentoElectronico(icEpo);
            // Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
            aplicarNotasDeCreditoAVariosDoctos = BeanParamDscto.hasAplicarNotaDeCreditoAVariosDoctosEnabled(icEpo);
         }
         
         bOperaFactorajeVencido = false;
         bOperaFactorajeVencidoGral = false;
         bOperaFactorajeDist = false;
         bOperaFactorajeDistGral = false;
         bOperaFactorajeMandatoGral = false;
         bOperaFactConMandato = false;
         bTipoFactoraje = false;
         bOperaFactVencInfonavit = false;//FODEA 042 - 2009 ACF
         bOperaFactVencInfonavitGral = false;//FODEA 042 - 2009 ACF
         
         String strGenerarArchivo = (request.getParameter("txtGeneraArchivo") == null) ? "" : request.getParameter("txtGeneraArchivo");
         
         //Variables de uso local
         rs = null;
         int registros = 0, numRegistros = 0, numRegistrosMN = 0, numRegistrosDL = 0;
         
         queryHelper = null;
         con			= new AccesoDB();
        // ResultSet		rsConsulta	= null;
         
         
         try{
            con.conexionDB();
            
            sOperaFactConMandato = new String("");
            Hashtable alParamEPO = new Hashtable(); 
            //System.out.println("���� icEpo ���� "+icEpo+" ����");
            if(!icEpo.equals("")) {
               alParamEPO = BeanParamDscto.getParametrosEPO(icEpo, 1);
               if (alParamEPO!=null) {
               bOperaFactConMandato   	 = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
               bOperaFactorajeVencido 	 = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
               bOperaFactorajeDist  	 = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
               bOperaFactVencInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;//FODEA 042 - 2009 ACF
               }
            }
            sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
            //System.out.println("���� sOperaFactConMandato -:generar archivo:- �� "+ sOperaFactConMandato+" ����");
            
            
            //CQueryHelper queryHelper_2 = new CQueryHelper(new AvisNotiIfDE());
				AvisNotiIfDE pag = new AvisNotiIfDE();
				//CQueryHelperRegExtJS queryHelper_2 = new CQueryHelperRegExtJS( pag );
				CQueryHelperThreadRegExtJS queryHelper_2 = new CQueryHelperThreadRegExtJS(pag);
				
				tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
				
				pag.setDfFechaNotMin(dfFechaNotMin);
				pag.setDfFechaNotMax(dfFechaNotMax);
				pag.setDfFechaVencMin(dfFechaVencMin);
				pag.setDfFechaVencMax(dfFechaVencMax);
				pag.setIcEpo(icEpo);
				pag.setCcAcuse3(ccAcuse3);
				pag.setTipoFactoraje(tipoFactoraje);
				pag.setFechaActual(fechaActual);
				pag.setINoCliente(iNoCliente);
				
            //queryHelper_2.cleanSession(request);
               String selected = "";
            
               qrySentencia= 
                  " SELECT COUNT (1)"   +
                  "   FROM comrel_if_epo"   +
                  "  WHERE cs_vobo_nafin = 'S'"   +
                  "    AND ic_if = ?"   +
                  "    AND ic_epo IN (SELECT ic_epo"   +
                  "                     FROM comrel_producto_epo"   +
                  "                    WHERE ic_producto_nafin = 1"   +
                  "                      AND cs_factoraje_vencido = 'S')"  ;
                  
               ps = con.queryPrecompilado(qrySentencia);
               ps.setString(1,iNoCliente);
               rs = ps.executeQuery();
               if (rs.next()) { 
                  if(rs.getInt(1)>0) bOperaFactorajeVencidoGral = true; 
               }
               ps.close();
               qrySentencia= 
                  " SELECT COUNT (1)"   +
                  "   FROM comrel_if_epo"   +
                  "  WHERE cs_vobo_nafin = 'S'"   +
                  "    AND ic_if = ?"   +
                  "    AND ic_epo IN (SELECT ic_epo"   +
                  "                     FROM comrel_producto_epo"   +
                  "                    WHERE ic_producto_nafin = 1"   +
                  "                      AND cs_factoraje_distribuido = 'S')"  ;
         
               ps = con.queryPrecompilado(qrySentencia);
               ps.setString(1,iNoCliente);
               rs = ps.executeQuery();
               if (rs.next()) { 
                  if(rs.getInt(1)>0) bOperaFactorajeDistGral = true; 
               }
               ps.close();
               
               qrySentencia= 
               " SELECT COUNT (1)"   +
               "   FROM comrel_if_epo"   +
               "  WHERE cs_vobo_nafin = 'S'"   +
               "    AND ic_if = ?"   +
               "    AND ic_epo IN (SELECT ic_epo"   +
               "                     FROM com_parametrizacion_epo"   +
               "                    WHERE CC_PARAMETRO_EPO = 'PUB_EPO_OPERA_MANDATO'"   +
               "                      AND CG_VALOR = 'S')"  ;
         
               ps = con.queryPrecompilado(qrySentencia);
               ps.setString(1,iNoCliente);
               rs = ps.executeQuery();
               bOperaFactorajeMandatoGral = false;
               if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeMandatoGral = true; }
               ps.close();
               //FODEA 042 - 2009 ACF (I)
               StringBuffer strSQL = new StringBuffer();
               strSQL.append(" SELECT COUNT (1)");
               strSQL.append(" FROM comrel_if_epo");
               strSQL.append(" WHERE cs_vobo_nafin = ?");
               strSQL.append(" AND ic_if = ?");
               strSQL.append(" AND ic_epo IN (SELECT ic_epo");
               strSQL.append(" FROM com_parametrizacion_epo");
               strSQL.append(" WHERE CC_PARAMETRO_EPO = ?");
               strSQL.append(" AND CG_VALOR = ?)");
               
               ps = con.queryPrecompilado(strSQL.toString());
               ps.setString(1, "S");
               ps.setInt(2, Integer.parseInt(iNoCliente));
               ps.setString(3, "PUB_EPO_VENC_INFONAVIT");
               ps.setString(4, "S");
               
               rs = ps.executeQuery();			
               if(rs.next()){if(rs.getInt(1) > 0){bOperaFactVencInfonavitGral = true;}}
               rs.close();
               
               ps.close();
               //FODEA 042 - 2009 ACF (F)
               if(bOperaFactorajeVencidoGral || bOperaFactorajeDistGral || bOperaFactorajeMandatoGral || bOperaFactVencInfonavitGral) {//FODEA 042 - 2009 ACF
                  bOperaFactorajeVencido = false;
                  if(icEpo.equals("")) { 
                     if(bOperaFactorajeVencidoGral)
                        bOperaFactorajeVencido = true;
                     if(bOperaFactorajeDistGral)
                        bOperaFactorajeDist = true;
                     if(bOperaFactorajeMandatoGral)
                        bOperaFactConMandato = true;
                     if(bOperaFactVencInfonavitGral){bOperaFactVencInfonavit = true;}//FODEA 042 - 2009 ACF
                     } 
                  }
                  bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDist || bOperaFactConMandato || bOperaFactVencInfonavit)?true:false;//FODEA 042 - 2009 ACF
         
                  //OPT Estos campos agregar al query me los llevaría en el summary
                  // NO SE QUITARON PORQUE AUNQUE LE HACE CASO A PARAMETERS LOS NECESITA PARA LOS TITULOS
         
                  StringBuffer contenidoArchivo = new StringBuffer(); 
                     contenidoArchivo.append("EPO, Fecha de notificación, Acuse de notificación, Nombre del proveedor, Numero de documento, Fecha de emisión, Fecha de vencimiento, Moneda");
                     
                 // if(bTipoFactoraje)
                     contenidoArchivo.append(",Tipo Factoraje");
                     
                     contenidoArchivo.append(
                        ", Monto, Porcentaje Descuento, Recurso Garantia, Monto Descontar");
                  
                     contenidoArchivo.append(",Monto Original,Documento Aplicado, Referencia");
                  
         
         
                  String strCT_REFERENCIA = "";
                  String strCampo = "";
                  //String strCampos = "";
                  String strNombreCampos = "";
                  String strContenidoCampo = "";
                  int    intTotalCampos = 0;
                  Vector vctNumeroCampo = new Vector();
                  Vector vctNombreCampos = new Vector();
                  Vector vctContenidoCampos = new Vector();
                  
                  //OPT No se si se necesite
               session.setAttribute("operacion", "Generar");
            
               SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
         
               envia = "S";
               fechaActual = fecha2.format(new java.util.Date());
               //if (envia != null) {
         
                  int genArch =0, i=0;
                  String epoNombre = "";
                  String fechaNotificacion = "";
                  String acuseNotificacion = "";
                  String pymeNombre = "";
                  String numeroDocto = "";
                  String fechaEmision = "";
                  String fechaVencimiento = "";
                  String monedaNombre = "";
                  String monto = "";
						String dblPorcentaje = "0";
                  double dblRecurso = 0;
						String dblMontoDescuento = "0";
                  String nombreTipoFactoraje="";
                  String tipoFactorajeDesc="";
                  String ic_documento = "";
						String ref = ""; //FODEA 17
               
         
                  //OPT ResultSet rsConsulta = con.queryDB(query);
         
         
                  
                  
                  int numeroTotalDocsMN=0, numeroTotalDocsDols=0;
                  double montoTotalDsctoMN=0, montoTotalDsctoDL=0;
                  double montoTotalMN=0, montoTotalDL=0;
               //out.println(strCampos);
						Registros rsConsulta	=	queryHelper_2.doSearch();
                  while(rsConsulta.next()){
                     vctContenidoCampos.clear();
                     epoNombre = (rsConsulta.getString(1) == null) ? "" : rsConsulta.getString(1).trim();
                     fechaNotificacion = (rsConsulta.getString(3) == null) ? "" : rsConsulta.getString(3).trim();
                     acuseNotificacion = (rsConsulta.getString(4) == null) ? "" : rsConsulta.getString(4).trim();
                     pymeNombre = (rsConsulta.getString(2) == null) ? "" : rsConsulta.getString(2).trim();
                     numeroDocto = (rsConsulta.getString(6) == null) ? "" : rsConsulta.getString(6).trim();
                     fechaEmision = (rsConsulta.getString(7) == null) ? "" : rsConsulta.getString(7).trim();
                     fechaVencimiento = (rsConsulta.getString(8) == null) ? "" : rsConsulta.getString(8).trim();
                     monedaNombre = (rsConsulta.getString(9) == null) ? "" : rsConsulta.getString(9).trim();
                     monto = (rsConsulta.getString(10) == null) ? "0.00" : rsConsulta.getString(10).trim();
							dblPorcentaje = (rsConsulta.getString(11) == null) ? "0.00" : rsConsulta.getString(11).trim();
							dblMontoDescuento = (rsConsulta.getString(12) == null) ? "0.00" : rsConsulta.getString(12).trim();
                     String rs_monto_ant = (rsConsulta.getString("fn_monto_ant") == null) ? "" : rsConsulta.getString("fn_monto_ant").trim();
                     String rs_docto_asoc = (rsConsulta.getString("IC_DOCTO_ASOCIADO") == null) ? "" : rsConsulta.getString("IC_DOCTO_ASOCIADO").trim();
							
							dblRecurso = new Double(monto).doubleValue() - new Double(dblMontoDescuento).doubleValue();
         
                     nombreTipoFactoraje 	= (rsConsulta.getString("CS_DSCTO_ESPECIAL")==null)?"":rsConsulta.getString("CS_DSCTO_ESPECIAL");
                     ic_documento 			= (rsConsulta.getString("IC_DOCUMENTO")==null)?"":rsConsulta.getString("IC_DOCUMENTO");
							ref 			= (rsConsulta.getString("REFERENCIA_PYME")==null)?"":rsConsulta.getString("REFERENCIA_PYME");
                        
                        if(nombreTipoFactoraje.equals("C") && icEpo.equals("")){
                           
                           operaNotasDeCredito                = false;
                           aplicarNotasDeCreditoAVariosDoctos = false;
                           
                           String icDoctoEpo = (rsConsulta.getString("IC_EPO")==null)?"":rsConsulta.getString("IC_EPO");
                           
                           if(!icDoctoEpo.equals("")){
                              // Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
                              operaNotasDeCredito                = BeanParamDscto.operaNotasDeCreditoParaDescuentoElectronico(icDoctoEpo);
                              // Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
                              aplicarNotasDeCreditoAVariosDoctos = BeanParamDscto.hasAplicarNotaDeCreditoAVariosDoctosEnabled(icDoctoEpo);
                           }
                           
                        }
                        
                        if(nombreTipoFactoraje.equals("C") && operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos ){
                           String icNotaCredito = ic_documento; 
                           rs_docto_asoc = "\"" + BeanSeleccionDocumento.getNumerosDeDocumento(icNotaCredito,",") + "\""; 
                        }
                     
                     tipoFactorajeDesc = (rsConsulta.getString("TIPO_FACTORAJE")==null)?"":rsConsulta.getString("TIPO_FACTORAJE");
                     nombreTipoFactoraje = tipoFactorajeDesc;
                     //nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal": (nombreTipoFactoraje.equals("V"))?"Vencido" :  (nombreTipoFactoraje.equals("D"))?"Distribuido" : (nombreTipoFactoraje.equals("C"))?"Nota de Credito" : "";

         
                     epoNombre = epoNombre.replace('*',' ').trim(); pymeNombre = pymeNombre.replace('*',' ').trim();
                        contenidoArchivo.append(
                        "\n" + epoNombre.replace(',', ' ') + "," + fechaNotificacion + "," + acuseNotificacion + ","+ pymeNombre.replace(',', ' ') + "," + numeroDocto + "," + fechaEmision + "," + fechaVencimiento + "," + monedaNombre);
 
                        contenidoArchivo.append(","+nombreTipoFactoraje);
                     contenidoArchivo.append(
                        ","+ Comunes.formatoDecimal(monto,2,false)+ "," + Comunes.formatoDecimal(dblPorcentaje,0,false) + "," + Comunes.formatoDecimal(dblRecurso, 2, false) + "," + Comunes.formatoDecimal(dblMontoDescuento,2,false));
         
                        contenidoArchivo.append(","+rs_monto_ant+","+rs_docto_asoc+","+ref);
                     
         
                     i++;
                     genArch ++;
                     registros ++;
                  } //fin del while
                  con.cierraStatement();
         
               
               if(registros!=0){
                  if (genArch != 0){
                     if (nomArchivo.equals("")){
                        CreaArchivo archivo = new CreaArchivo();
                        if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv"))
                           System.err.println("Error en la generacion del archivo"+strDirectorioTemp);
                        else{
                           nombreArchivo = archivo.nombre;
                           nomArchivo = nombreArchivo;
                        }
                     } else {
                        nombreArchivo = nomArchivo;
                     }
                  }
               }
            }catch(Exception e){
               out.println(e);
               e.printStackTrace();
            } finally {
               if (con.hayConexionAbierta()) con.cierraConexionDB();
            }
      }
      
         
		jsonObj = new JSONObject();
      if(!operacion.equals("XLSNotaC")){
         nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, operacion);
      }
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		resultado = jsonObj.toString();
	}
} /*** FIN DE CONSULTA ***/

/***	RESUMEN TOTALES	***/
else if(informacion.equals("ResumenTotales")){
		//CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesión
		CQueryHelperThreadRegExtJS queryHelper = new CQueryHelperThreadRegExtJS();
		resultado = queryHelper.getJSONResultCount(request);
}

else if(informacion.equals("PDFPrint")){ 
   System.err.println("..::::: + PDFPrint :::::..");
   
      //>>CREA REPORTE PDF
   CreaArchivo archivo = new CreaArchivo();
   String nombreArchivo = archivo.nombreArchivo()+".pdf";
   //<<CREA REPORTE PDF	
   
   CQueryHelper	queryHelper_2 = new CQueryHelper(new AvisNotiIfDE());
   queryHelper_2 = new CQueryHelper(new AvisNotiIfDE());
   //queryHelper.cleanSession(request);
   int 	offset = 0;
 
   // Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
   String paginaNo = (request.getParameter("paginaNo") == null) ? "1" : request.getParameter("paginaNo");
   String nomArchivo = (request.getParameter("nomArchivo") == null) ? "" : request.getParameter("nomArchivo");
   
   bOperaFactorajeVencido = false;
   bOperaFactorajeVencidoGral = false;
   bOperaFactorajeDist = false;
   bOperaFactorajeDistGral = false;
   bOperaFactorajeMandatoGral = false;
   bOperaFactConMandato = false;
   bTipoFactoraje = false;
   bOperaFactVencInfonavit = false;//FODEA 042 - 2009 ACF
   bOperaFactVencInfonavitGral = false;//FODEA 042 - 2009 ACF
   
   con = new AccesoDB();
   boolean validaFact = false;
   try {
      System.err.println("..::::: + PDFPrint TRY {} :::::..");
      //_________________________ INICIO DEL PROGRAMA _________________________
      
      ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
   
      sOperaFactConMandato = new String("");
      Hashtable alParamEPO = new Hashtable(); 
      
      System.err.println("���� icEpo ���� "+icEpo+" ����");
      
      if(!icEpo.equals("")) {
         alParamEPO = BeanParamDscto.getParametrosEPO(icEpo, 1);
         if (alParamEPO!=null) {
         bOperaFactConMandato   		  = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
         bOperaFactorajeVencido 		  = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
         bOperaFactorajeDist   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
         bOperaFactVencInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;//FODEA 042 - 2009 ACF
         }
      }
      sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
      con.conexionDB();
      String selected = "";
      qrySentencia= 
         " SELECT COUNT (1)"   +
         "   FROM comrel_if_epo"   +
         "  WHERE cs_vobo_nafin = 'S'"   +
         "    AND ic_if = ?"   +
         "    AND ic_epo IN (SELECT ic_epo"   +
         "                     FROM comrel_producto_epo"   +
         "                    WHERE ic_producto_nafin = 1"   +
         "                      AND cs_factoraje_vencido = 'S')"  ;
   
      ps = con.queryPrecompilado(qrySentencia);
      ps.setString(1,iNoCliente);
      rs = ps.executeQuery();
      if (rs.next()) { 
         if(rs.getInt(1)>0) bOperaFactorajeVencidoGral = true; 
      }
      System.err.println(" ..:::: qrySentencia :::. " + qrySentencia);
      System.err.println(" ..:::: iNoCliente :::. " + iNoCliente);
      ps.close();
      qrySentencia = 
         " SELECT COUNT (1)"   +
         "   FROM comrel_if_epo"   +
         "  WHERE cs_vobo_nafin = 'S'"   +
         "    AND ic_if = ?"   +
         "    AND ic_epo IN (SELECT ic_epo"   +
         "                     FROM comrel_producto_epo"   +
         "                    WHERE ic_producto_nafin = 1"   +
         "                      AND cs_factoraje_distribuido = 'S')"  ;
   
      ps = con.queryPrecompilado(qrySentencia);
      ps.setString(1,iNoCliente);
      rs = ps.executeQuery();
      if (rs.next()) { 
         if(rs.getInt(1)>0) bOperaFactorajeDistGral = true; 
      }
      System.err.println(" ..:::: qrySentencia :::. " + qrySentencia);
      System.err.println(" ..:::: iNoCliente :::. " + iNoCliente);
      ps.close();
      
      qrySentencia= 
         " SELECT COUNT (1)"   +
         "   FROM comrel_if_epo"   +
         "  WHERE cs_vobo_nafin = 'S'"   +
         "    AND ic_if = ?"   +
         "    AND ic_epo IN (SELECT ic_epo"   +
         "                     FROM com_parametrizacion_epo"   +
         "                    WHERE CC_PARAMETRO_EPO = 'PUB_EPO_OPERA_MANDATO'"   +
         "                      AND CG_VALOR = 'S')"  ;
   
         ps = con.queryPrecompilado(qrySentencia);
         ps.setString(1,iNoCliente);
         rs = ps.executeQuery();
         bOperaFactorajeMandatoGral = false;
         if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeMandatoGral = true; }
      System.err.println(" ..:::: qrySentencia :::. " + qrySentencia);
      System.err.println(" ..:::: iNoCliente :::. " + iNoCliente);
         ps.close();
         //FODEA 042 - 2009 ACF (I)
         StringBuffer strSQL = new StringBuffer();
         strSQL.append(" SELECT COUNT (1)");
         strSQL.append(" FROM comrel_if_epo");
         strSQL.append(" WHERE cs_vobo_nafin = ?");
         strSQL.append(" AND ic_if = ?");
         strSQL.append(" AND ic_epo IN (SELECT ic_epo");
         strSQL.append(" FROM com_parametrizacion_epo");
         strSQL.append(" WHERE CC_PARAMETRO_EPO = ?");
         strSQL.append(" AND CG_VALOR = ?)");
         
         ps = con.queryPrecompilado(strSQL.toString());
         ps.setString(1, "S");
         ps.setInt(2, Integer.parseInt(iNoCliente));
         ps.setString(3, "PUB_EPO_VENC_INFONAVIT");
         ps.setString(4, "S");
         
         System.err.println(" ..:::: strSQL :::. " + strSQL.toString());
         System.err.println(" ..:::: cs_vobo_nafin :::. S");
         System.err.println(" ..:::: iNoCliente :::. " + iNoCliente);
         System.err.println(" ..:::: CC_PARAMETRO_EPO :::. PUB_EPO_VENC_INFONAVIT");
         System.err.println(" ..:::: CG_VALOR :::. S");
         
         rs = ps.executeQuery();			
         if(rs.next()){if(rs.getInt(1) > 0){bOperaFactVencInfonavitGral = true;}}
         rs.close();
         
         ps.close();
         //FODEA 042 - 2009 ACF (F)
      
      ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
      String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
      String fechaAct = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
      String diaActual    = fechaAct.substring(0,2);
      String mesActual    = meses[Integer.parseInt(fechaAct.substring(3,5))-1];
      String anioActual   = fechaAct.substring(6,10);
      String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
   
      pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                                   session.getAttribute("iNoNafinElectronico").toString(),
                            (String)session.getAttribute("sesExterno"),
                                   (String) session.getAttribute("strNombre"),
                                    (String) session.getAttribute("strNombreUsuario"),
                            (String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
      
       pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
      String mensaje = 
            "Las operaciones que aparecen en esta página fueron notificadas a la EMPRESA DE PRIMER ORDEN  de conformidad y para efectos "+
            "de los artículos 45 K de la Ley General de Organizaciones y Actividades Auxiliares del Crédito y 32 C del Código Fiscal o "+
            "del artículo 2038 del Código Civil Federal según corresponda, para los efectos legales conducentes. "+
            "Por otra parte, a partir del 17 de octubre de 2018, el PROVEEDOR ha manifestado bajo protesta de decir verdad, que sí ha emitido o emitirá a la "+
            "EMPRESA DE PRIMER ORDEN el CFDI por la operación comercial que le dio origen a esta transacción, según sea el caso y conforme establezcan las disposiciones fiscales vigentes.";
            
    pdfDoc.addText(mensaje,"formasB",ComunesPDF.CENTER);
      
      
      System.err.println(" ..:::: mensaje :::. " + mensaje);
      
      if(bOperaFactorajeVencidoGral || bOperaFactorajeDistGral || bOperaFactorajeMandatoGral || bOperaFactVencInfonavitGral) {//FODEA 042 - 2009 ACF
         bOperaFactorajeVencido = false;
         if(icEpo.equals("")) { 
            if(bOperaFactorajeVencidoGral)
               bOperaFactorajeVencido = true;
            if(bOperaFactorajeDistGral)
               bOperaFactorajeDist = true;
            if(bOperaFactorajeMandatoGral)
               bOperaFactConMandato = true;
            if(bOperaFactVencInfonavitGral){bOperaFactVencInfonavit = true;}//FODEA 042 - 2009 ACF
         } 
         bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDist || bOperaFactConMandato || bOperaFactVencInfonavit)?true:false;//FODEA 042 - 2009 ACF
      }
      int genArch =0, i=0;
      String epoNombre = "";
      String fechaNotificacion = "";
      String acuseNotificacion = "";
      String pymeNombre = "";
      String numeroDocto = "";
      String fechaEmision = "";
      String fechaVencimiento = "";
      String monedaNombre = "";
      String monto = "";
      double dblPorcentaje = 0;
      double dblRecurso = 0;
      double dblMontoDescuento = 0;
      String nombreTipoFactoraje="";
      String tipoFactorajeDesc="";
		String referencia_pyme = ""; //FODEA 17
      //OPT
      queryHelper_2.executePKQueryIfNecessary(request,con);
      con.cierraStatement();
      
      System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ PUNTO @@@@@@@@@@@@@@@@@@@@@@@@@@@@\n paginaNo " + paginaNo);
      
      try{
         queryHelper_2.readyForPaging(request);
      }catch(Exception e){
         e.printStackTrace();
         throw new RuntimeException(e);
      }
      
      
      if(true){//if (queryHelper_2.readyForPaging(request)){
      
      System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ readyForPaging @@@@@@@@@@@@@@@@@@@@@@@@@@@@ = TRUE");
         int cols=0;         
         ResultSet rsConsulta = null;
         
         try{
            if(!"".equals(paginaNo) && !"T".equals(paginaNo)) {
               int numero=Integer.parseInt(paginaNo);
               numero = (numero-1)*15;
               rsConsulta = queryHelper_2.getPageResultSet(request,con,numero,15);
            } else {
               rsConsulta = queryHelper_2.getCreateFile(request,con);
            }
            cols = 0;
            if(bTipoFactoraje) { cols = 14; } //era 13 Fodea 17
            else {  cols = 13; } //era 12 Fodea 17
         } catch(Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
         }
         
         System.err.println("@@@@@ HASTA AQUI TODO OK @@@@");
        // if(bOperaFactorajeVencido)
        //    cols++;
         pdfDoc.setTable(cols);
         while (rsConsulta.next()) {
            epoNombre = (rsConsulta.getString(1) == null) ? "" : rsConsulta.getString(1).trim();
            fechaNotificacion = (rsConsulta.getString(2) == null) ? "" : rsConsulta.getString(2).trim();
            acuseNotificacion = (rsConsulta.getString(3) == null) ? "" : rsConsulta.getString(3).trim();
            pymeNombre = (rsConsulta.getString(4) == null) ? "" : rsConsulta.getString(4).trim();
            numeroDocto = (rsConsulta.getString(5) == null) ? "" : rsConsulta.getString(5).trim();
            fechaEmision = (rsConsulta.getString(6) == null) ? "" : rsConsulta.getString(6).trim();
            fechaVencimiento = (rsConsulta.getString(7) == null) ? "" : rsConsulta.getString(7).trim();
            monedaNombre = (rsConsulta.getString(8) == null) ? "" : rsConsulta.getString(8).trim();
            monto = (rsConsulta.getString(9) == null) ? "0.00" : rsConsulta.getString(9).trim();
            dblPorcentaje = rsConsulta.getDouble(10);
            dblMontoDescuento = rsConsulta.getDouble(11);
				referencia_pyme = (rsConsulta.getString(15) == null) ? "" : rsConsulta.getString(15).trim();
            dblRecurso = new Double(monto).doubleValue() - dblMontoDescuento;
            if(bTipoFactoraje) { // Para Factoraje Especial
               nombreTipoFactoraje = (rsConsulta.getString("CS_DSCTO_ESPECIAL")==null)?"":rsConsulta.getString("CS_DSCTO_ESPECIAL");
               tipoFactorajeDesc = (rsConsulta.getString("TIPO_FACTORAJE")==null)?"":rsConsulta.getString("TIPO_FACTORAJE");
               nombreTipoFactoraje = tipoFactorajeDesc;
               //nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal": (nombreTipoFactoraje.equals("V"))?"Vencido" :  (nombreTipoFactoraje.equals("D"))?"Distribuido" : "";
            }
            if (i == 0)	{
               pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Fecha de notificación","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Acuse de notificación","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Nombre del proveedor","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Número de documento","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Fecha de emisión","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
               if(bTipoFactoraje) { /* Desplegar si opera algun Factoraje Especial */
                  pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
               }
               pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Recurso en Garantia","celda01",ComunesPDF.CENTER);
               pdfDoc.setCell("Monto Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
               pdfDoc.setHeaders();
            }
            String clase = "formas";
            //if(i%5==4)
              // clase = "celda02";
            //else
              // clase = "formas";
            pdfDoc.setCell(epoNombre,clase,ComunesPDF.CENTER);
            pdfDoc.setCell(fechaNotificacion,clase,ComunesPDF.CENTER);
            pdfDoc.setCell(acuseNotificacion,clase,ComunesPDF.CENTER);
            pdfDoc.setCell(pymeNombre,clase,ComunesPDF.CENTER);
            pdfDoc.setCell(numeroDocto,clase,ComunesPDF.CENTER);
            pdfDoc.setCell(fechaEmision,clase,ComunesPDF.CENTER);
            pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
            pdfDoc.setCell(monedaNombre,clase,ComunesPDF.CENTER);
            if(bTipoFactoraje) { /* Desplegar si opera algun  Factoraje Especial */
            pdfDoc.setCell(nombreTipoFactoraje,clase,ComunesPDF.CENTER);
            }
            pdfDoc.setCell("$ "+Comunes.formatoMoneda2(monto,false),clase,ComunesPDF.CENTER);
            pdfDoc.setCell(Comunes.formatoDecimal(new Double(dblPorcentaje).toString(),0, false)+" %",clase,ComunesPDF.CENTER);
            pdfDoc.setCell("$ "+Comunes.formatoMoneda2(new Double(dblRecurso).toString(), false),clase,ComunesPDF.CENTER);
            pdfDoc.setCell("$ "+Comunes.formatoMoneda2(new Double(dblMontoDescuento).toString(), false),clase,ComunesPDF.CENTER);
				pdfDoc.setCell(referencia_pyme,clase,ComunesPDF.CENTER); //FODEA 17
            i++;
            genArch ++;
         } //fin del while
         con.cierraStatement();
         Vector vecTotales = queryHelper_2.getResultCount(request);
         if(vecTotales.size()>0){
            int ii = 0;
            Vector vecColumnas = null;
            for(ii=0;ii<vecTotales.size();ii++){
               vecColumnas = (Vector)vecTotales.get(ii);
               pdfDoc.setCell("Total "+vecColumnas.get(3).toString(),"formasB",ComunesPDF.RIGHT,cols-6); //era 5
               pdfDoc.setCell(vecColumnas.get(0).toString(),"formas",ComunesPDF.RIGHT);
               pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecColumnas.get(1).toString(),2),"formas",ComunesPDF.RIGHT);
               pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,2);
               pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecColumnas.get(2).toString(),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,1); //Fodea 17
            } // del for de totales
         } // del if de totales
      } //end of if for readyForPaging 
      
      
      pdfDoc.addTable();
      pdfDoc.endDocument();
      validaFact = true;
   } catch(Exception e) {
       out.println(e);
   } finally {
      if (con.hayConexionAbierta() == true)
      con.cierraConexionDB();
   }

   jsonObj = new JSONObject();
   //String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,limit, strDirectorioTemp,operacion);
   jsonObj.put("success", new Boolean(true));
   jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
   resultado = jsonObj.toString();
}
   
   
else if(informacion.equals("PDF")){
      //>>CREA REPORTE PDF
      CreaArchivo archivo = new CreaArchivo();
      String nombreArchivo = archivo.nombreArchivo()+".pdf";
      //<<CREA REPORTE PDF	
      
      CQueryHelper	queryHelper_2 = new CQueryHelper(new AvisNotiIfDE());
      //queryHelper.cleanSession(request);
      int 	offset = 0;
      // Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
      String paginaNo = (request.getParameter("paginaNo") == null) ? "1" : request.getParameter("paginaNo");
      String nomArchivo = (request.getParameter("nomArchivo") == null) ? "" : request.getParameter("nomArchivo");

      bOperaFactorajeVencido = false;
      bOperaFactorajeVencidoGral = false;
      bOperaFactorajeDist = false;
      bOperaFactorajeDistGral = false;
      bOperaFactorajeMandatoGral = false;
      bOperaFactConMandato = false;
      bTipoFactoraje = false;
      bOperaFactVencInfonavit = false;//FODEA 042 - 2009 ACF
      bOperaFactVencInfonavitGral = false;//FODEA 042 - 2009 ACF
      
      con = new AccesoDB();
      boolean validaFact = false;
      SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
      fechaActual = fecha2.format(new java.util.Date());
      System.err.println("fechaActual " + fecha2.format(new java.util.Date()));
      
      try {
         //_________________________ INICIO DEL PROGRAMA _________________________
         
         ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
      
         sOperaFactConMandato = new String("");
         Hashtable alParamEPO = new Hashtable(); 
         //System.out.println("���� icEpo ���� "+icEpo+" ����");
         if(!icEpo.equals("")) {
            alParamEPO = BeanParamDscto.getParametrosEPO(icEpo, 1);
            if (alParamEPO!=null) {
            bOperaFactConMandato   		   = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
            bOperaFactorajeVencido 		   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
            bOperaFactorajeDist           = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
            bOperaFactVencInfonavit       = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;//FODEA 042 - 2009 ACF
            }
         }
         sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
         
         con.conexionDB();
         String selected = "";
         qrySentencia= 
            " SELECT COUNT (1)"   +
            "   FROM comrel_if_epo"   +
            "  WHERE cs_vobo_nafin = 'S'"   +
            "    AND ic_if = ?"   +
            "    AND ic_epo IN (SELECT ic_epo"   +
            "                     FROM comrel_producto_epo"   +
            "                    WHERE ic_producto_nafin = 1"   +
            "                      AND cs_factoraje_vencido = 'S')"  ;
      
         ps = con.queryPrecompilado(qrySentencia);
         ps.setString(1,iNoCliente);
         rs = ps.executeQuery();
         if (rs.next()) { 
            if(rs.getInt(1)>0) bOperaFactorajeVencidoGral = true; 
         }
         ps.close();
         qrySentencia = 
            " SELECT COUNT (1)"   +
            "   FROM comrel_if_epo"   +
            "  WHERE cs_vobo_nafin = 'S'"   +
            "    AND ic_if = ?"   +
            "    AND ic_epo IN (SELECT ic_epo"   +
            "                     FROM comrel_producto_epo"   +
            "                    WHERE ic_producto_nafin = 1"   +
            "                      AND cs_factoraje_distribuido = 'S')"  ;
      
         ps = con.queryPrecompilado(qrySentencia);
         ps.setString(1,iNoCliente);
         rs = ps.executeQuery();
         if (rs.next()) { 
            if(rs.getInt(1)>0) bOperaFactorajeDistGral = true; 
         }
         ps.close();
         
         qrySentencia= 
            " SELECT COUNT (1)"   +
            "   FROM comrel_if_epo"   +
            "  WHERE cs_vobo_nafin = 'S'"   +
            "    AND ic_if = ?"   +
            "    AND ic_epo IN (SELECT ic_epo"   +
            "                     FROM com_parametrizacion_epo"   +
            "                    WHERE CC_PARAMETRO_EPO = 'PUB_EPO_OPERA_MANDATO'"   +
            "                      AND CG_VALOR = 'S')"  ;
      
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1,iNoCliente);
            rs = ps.executeQuery();
            bOperaFactorajeMandatoGral = false;
            if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeMandatoGral = true; }
            ps.close();
            //FODEA 042 - 2009 ACF (I)
            StringBuffer strSQL = new StringBuffer();
            strSQL.append(" SELECT COUNT (1)");
            strSQL.append(" FROM comrel_if_epo");
            strSQL.append(" WHERE cs_vobo_nafin = ?");
            strSQL.append(" AND ic_if = ?");
            strSQL.append(" AND ic_epo IN (SELECT ic_epo");
            strSQL.append(" FROM com_parametrizacion_epo");
            strSQL.append(" WHERE CC_PARAMETRO_EPO = ?");
            strSQL.append(" AND CG_VALOR = ?)");
            
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setString(1, "S");
            ps.setInt(2, Integer.parseInt(iNoCliente));
            ps.setString(3, "PUB_EPO_VENC_INFONAVIT");
            ps.setString(4, "S");
            
            rs = ps.executeQuery();			
            if(rs.next()){if(rs.getInt(1) > 0){bOperaFactVencInfonavitGral = true;}}
            rs.close();
            
            ps.close();
            //FODEA 042 - 2009 ACF (F)
         
         ComunesPDF pdfDoc    = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
         String meses[]       = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
         String fechaAct      = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
         String diaActual     = fechaAct.substring(0,2);
         String mesActual     = meses[Integer.parseInt(fechaAct.substring(3,5))-1];
         String anioActual    = fechaAct.substring(6,10);
         String horaActual    = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
      
         pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                                       session.getAttribute("iNoNafinElectronico").toString(),
                                       (String)session.getAttribute("sesExterno"),
                                       (String) session.getAttribute("strNombre"),
                                       (String) session.getAttribute("strNombreUsuario"),
                                       (String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
         
          pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
         String mensaje = 
               "\nLas operaciones que aparecen en esta página fueron notificadas a la empresa de primer orden de conformidad y para efectos "+
               "de los artículos 45 K de la Ley General de Organizaciones y Actividades Auxiliares del Crédito y 32 C del Código Fiscal o "+
               "del artículo 2038 del Código Civil Federal según corresponda, para los efectos legales conducentes. "+
               "Por otra parte, a partir del 17 de octubre de 2018, el PROVEEDOR ha manifestado bajo protesta de decir verdad, que sí ha emitido o emitirá a la "+
                "EMPRESA DE PRIMER ORDEN el CFDI por la operación comercial que le dio origen a esta transacción, según sea el caso y conforme establezcan las disposiciones fiscales vigentes.";
               
               
         pdfDoc.addText(mensaje,"formasB",ComunesPDF.CENTER);
         
         if(bOperaFactorajeVencidoGral || bOperaFactorajeDistGral || bOperaFactorajeMandatoGral || bOperaFactVencInfonavitGral) {//FODEA 042 - 2009 ACF
            bOperaFactorajeVencido = false;
            if(icEpo.equals("")) { 
               if(bOperaFactorajeVencidoGral)
                  bOperaFactorajeVencido = true;
               if(bOperaFactorajeDistGral)
                  bOperaFactorajeDist = true;
               if(bOperaFactorajeMandatoGral)
                  bOperaFactConMandato = true;
               if(bOperaFactVencInfonavitGral){bOperaFactVencInfonavit = true;}//FODEA 042 - 2009 ACF
            } 
            bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDist || bOperaFactConMandato || bOperaFactVencInfonavit)?true:false;//FODEA 042 - 2009 ACF
         }
         int genArch                =0, i=0;
         String epoNombre           = "";
         String fechaNotificacion   = "";
         String acuseNotificacion   = "";
         String pymeNombre          = "";
         String numeroDocto         = "";
         String fechaEmision        = "";
         String fechaVencimiento    = "";
         String monedaNombre        = "";
         String monto               = "";
         double dblPorcentaje       = 0;
			//String dblPorcentaje="0";
         double dblRecurso          = 0;
         double dblMontoDescuento   = 0;
			//String dblMontoDescuento= "0";
         String nombreTipoFactoraje ="";
         String tipoFactorajeDesc   ="";
			String referencia_pyme=""; //FODEA 17
         //OPT
         queryHelper_2.executePKQueryIfNecessary(request,con);
         con.cierraStatement();
         if (true){
            
            System.err.println("@@@@@ AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII");            
            ResultSet rsConsulta = null;
            //paginaNo = "T"; // String que viene en request con el parametro T (Todos)
            
            if(!"".equals(paginaNo) && !"T".equals(paginaNo)) {  
					int numero=Integer.parseInt(paginaNo);
               numero = (numero-1)*15;
               rsConsulta = queryHelper_2.getPageResultSet(request,con,numero,15);
               rsConsulta = queryHelper_2.getCreateFile(request,con);
            } else {   
               rsConsulta = queryHelper_2.getCreateFile(request,con);
            }
            
            int cols = 0;
            if(bTipoFactoraje) { cols = 14; } //era 13 Fodea 17
            else {  cols = 13; }  //era 12 Fodea 17
            
            System.err.println("cols " + cols);
            
            //if(bOperaFactorajeVencido)
               //cols++;
            pdfDoc.setTable(cols);
            
            System.err.println("@@@@@ AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII");
				
            while (rsConsulta.next()) {
               epoNombre = (rsConsulta.getString(1) == null) ? "" : rsConsulta.getString(1).trim();
               fechaNotificacion = (rsConsulta.getString(3) == null) ? "" : rsConsulta.getString(3).trim();
               acuseNotificacion = (rsConsulta.getString(4) == null) ? "" : rsConsulta.getString(4).trim();
               pymeNombre = (rsConsulta.getString(2) == null) ? "" : rsConsulta.getString(2).trim();
               numeroDocto = (rsConsulta.getString(6) == null) ? "" : rsConsulta.getString(6).trim();
               fechaEmision = (rsConsulta.getString(7) == null) ? "" : rsConsulta.getString(7).trim();
               fechaVencimiento = (rsConsulta.getString(8) == null) ? "" : rsConsulta.getString(8).trim();
               monedaNombre = (rsConsulta.getString(9) == null) ? "" : rsConsulta.getString(9).trim();
               monto = (rsConsulta.getString(10) == null) ? "0.00" : rsConsulta.getString(10).trim();
               dblPorcentaje = rsConsulta.getDouble(11);
               dblMontoDescuento = rsConsulta.getDouble(12);
					referencia_pyme = (rsConsulta.getString("REFERENCIA_PYME") == null) ? "" : rsConsulta.getString("REFERENCIA_PYME").trim(); //Checar el indice correcto
               dblRecurso = new Double(monto).doubleValue() - dblMontoDescuento;
               if(bTipoFactoraje) { // Para Factoraje Especial
                  nombreTipoFactoraje = (rsConsulta.getString("CS_DSCTO_ESPECIAL")==null)?"":rsConsulta.getString("CS_DSCTO_ESPECIAL");
                  tipoFactorajeDesc = (rsConsulta.getString("TIPO_FACTORAJE")==null)?"":rsConsulta.getString("TIPO_FACTORAJE");
                  nombreTipoFactoraje = tipoFactorajeDesc;
                  //nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal": (nombreTipoFactoraje.equals("V"))?"Vencido" :  (nombreTipoFactoraje.equals("D"))?"Distribuido" : "";
               }
               if (i == 0)	{
                  pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Fecha de notificación","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Acuse de notificación","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Nombre del proveedor","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Número de documento","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Fecha de emisión","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
                  if(bTipoFactoraje) { /* Desplegar si opera algun Factoraje Especial */
                     pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
                  }
                  pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Recurso en Garantia","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Monto Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER); //FODEA 17
                  pdfDoc.setHeaders();
               }
               String clase = "formas";
               //if(i%5==4)
                 // clase = "celda02";
                  
               pdfDoc.setCell(epoNombre,clase,ComunesPDF.CENTER);
               pdfDoc.setCell(fechaNotificacion,clase,ComunesPDF.CENTER);
               pdfDoc.setCell(acuseNotificacion,clase,ComunesPDF.CENTER);
               pdfDoc.setCell(pymeNombre,clase,ComunesPDF.CENTER);
               pdfDoc.setCell(numeroDocto,clase,ComunesPDF.CENTER);
               pdfDoc.setCell(fechaEmision,clase,ComunesPDF.CENTER);
               pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
               pdfDoc.setCell(monedaNombre,clase,ComunesPDF.CENTER);
               if(bTipoFactoraje) { /* Desplegar si opera algun  Factoraje Especial */
                  pdfDoc.setCell(nombreTipoFactoraje,clase,ComunesPDF.CENTER);
               }
               pdfDoc.setCell("$ "+Comunes.formatoMoneda2(monto,false),clase,ComunesPDF.CENTER);
               pdfDoc.setCell(Comunes.formatoDecimal(new Double(dblPorcentaje).toString(),0, false)+" %",clase,ComunesPDF.CENTER);
               pdfDoc.setCell("$ "+Comunes.formatoMoneda2(new Double(dblRecurso).toString(), false),clase,ComunesPDF.CENTER);
               pdfDoc.setCell("$ "+Comunes.formatoMoneda2(new Double(dblMontoDescuento).toString(), false),clase,ComunesPDF.CENTER);
					pdfDoc.setCell(referencia_pyme,clase,ComunesPDF.CENTER); //FODEA 17
               i++;
               genArch ++;
            } //fin del while
            con.cierraStatement();
            Vector vecTotales = queryHelper_2.getResultCount(request); 
            if(vecTotales.size()>0){
               int ii = 0;  
               Vector vecColumnas = null;
               for(ii=0;ii<vecTotales.size();ii++){
                  vecColumnas = (Vector)vecTotales.get(ii);
                  pdfDoc.setCell("Total "+vecColumnas.get(3).toString(),"formasB",ComunesPDF.RIGHT,cols-6); //era 5 Fodea 17
                  pdfDoc.setCell(vecColumnas.get(0).toString(),"formas",ComunesPDF.RIGHT);
                  pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecColumnas.get(1).toString(),2),"formas",ComunesPDF.RIGHT);
                  pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,2);
                  pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecColumnas.get(2).toString(),2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,1); //Fodea 17
               } // del for de totales
            } // del if de totales
         } //end of if for readyForPaging 
         pdfDoc.addTable();
         pdfDoc.endDocument();
         validaFact = true;
         
         jsonObj = new JSONObject();
         jsonObj.put("success", new Boolean(true));
         jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
         System.err.println("strDirecVirtualTemp+nombreArchivo " + strDirecVirtualTemp+nombreArchivo);
         resultado = jsonObj.toString();
         
      } catch(Exception e) {
          e.printStackTrace();
          throw new RuntimeException(e);
      } finally {
         if (con.hayConexionAbierta() == true)
         con.cierraConexionDB();
      }
          
      
	} else if(informacion.equals("IMPRIME_PDF")){
/*
 Se genera el PDF con todos los registros de la consulta (sin usar paginación),
 implementa la interfaz IQueryGeneratorRegExtJS para utilizar sus métodos.
 Y se elimina la opción de generar PDF por página.
 El resto de la funcionalidad de la pantalla no cambia.
*/
		String estatusArchivo = request.getParameter("estatusArchivo");
		String porcentaje = "";
		String nombreArchivo = "";
		AvisNotiIfDE paginador = new AvisNotiIfDE();
		paginador.setIcEpo(icEpo);
		paginador.setDfFechaNotMin(dfFechaNotMin);
		paginador.setDfFechaNotMax(dfFechaNotMax);
		paginador.setDfFechaVencMin(dfFechaVencMin);
		paginador.setDfFechaVencMax(dfFechaVencMax);
		paginador .setCcAcuse3(ccAcuse3);
		paginador.setFechaActual(fechaActual);
		paginador.setTipoFactoraje(tipoFactoraje);
		paginador.setTipo(tipo);
		paginador.setINoCliente(iNoCliente);
		//CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		CQueryHelperThreadRegExtJS queryHelper = new CQueryHelperThreadRegExtJS(paginador);
		jsonObj = new JSONObject();

		try {
/*
			nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
*/
			if(estatusArchivo.equals("INICIO")){
				session.removeAttribute("13consulta5ThreadCreateFiles");
				ThreadCreateFiles threadCreateFiles = queryHelper.getThreadCreateCustomFile(request, strDirectorioTemp, "PDF");
				session.setAttribute("13consulta5ThreadCreateFiles", threadCreateFiles);
				estatusArchivo = "PROCESANDO";
			} else if(estatusArchivo.equals("PROCESANDO")){
				ThreadCreateFiles threadCreateFiles = (ThreadCreateFiles)session.getAttribute("13consulta5ThreadCreateFiles");
				System.out.println("queryHelper.getPercent() = "+ threadCreateFiles.getPercent());
				porcentaje = String.valueOf(threadCreateFiles.getPercent()) + " %";
				if(!threadCreateFiles.isRunning() && !threadCreateFiles.hasError()){
					nombreArchivo = threadCreateFiles.getNombreArchivo();
					estatusArchivo = "FINAL";
				}else if(threadCreateFiles.hasError()){
					estatusArchivo = "ERROR";
				}
			}

			if(porcentaje.equals("") || porcentaje.equals("0") || porcentaje.equals("0 %"))
				porcentaje = "Procesando...";
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			jsonObj.put("estatusArchivo", estatusArchivo);
			jsonObj.put("porcentaje", porcentaje);

		} catch(Throwable e) {
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		resultado = jsonObj.toString();

	}


%>
<%= resultado%>
