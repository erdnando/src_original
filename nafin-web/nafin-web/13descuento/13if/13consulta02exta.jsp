<%@ page contentType="application/json;charset=UTF-8"
import="
	java.sql.*,
	java.math.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%!public static final boolean SIN_COMAS = false;%>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
String ses_ic_if = iNoCliente;
String ses_cg_razon_social = strNombre;

//	DECLARACION DE VARIABLES
	//	Parámetros provenienen de la página actual
String envia = (request.getParameter("envia") == null)?"":request.getParameter("envia");
String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null)?"":request.getParameter("dc_fecha_cambioMin");
String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null)?"":request.getParameter("dc_fecha_cambioMax");
String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") == null)?"":request.getParameter("ic_cambio_estatus");

// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
String paginaNo = (request.getParameter("paginaNo") == null) ? "1" : request.getParameter("paginaNo");
String nomArchivo = (request.getParameter("nomArchivo") == null) ? "" : request.getParameter("nomArchivo");
int 	offset = 0;
	//	Variables de uso local
String qrySentencia;
ResultSet rs;

String clave;
String nombre;
String sel;

String condicion ="";

BigDecimal montoTotalMN = new BigDecimal("0.00");
BigDecimal montoTotalDL = new BigDecimal("0.00");
int numRegistros = 0;
int numRegistrosMN = 0;
int numRegistrosDL = 0;

CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo= new StringBuffer(2000); //2000 es la capacidad inicial reservada
String nombreArchivo="";

String colsp="";

		//	De despliegue

String nombreEpo;
String nombrePyme;
int monedaClave;
String monedaNombre;
String numeroDocumento;
String fechaEmision;
String fechaVencimiento;
String tasaAceptada;
String plazo;
String monto;
String referencia; //FODEA 17
double dblMontoDocto = 0;
double dblPorcentaje = 0;
double dblRecurso = 0;
double dblMontoDescuento = 0;
double dblTotalMontoPesos = 0;
double dblTotalMontoDolares = 0;
double dblTotalDescuentoPesos = 0;
double dblTotalDescuentoDolares =0;
String cambioEstatus;
String fechaCambioEstatus;
String cambioMotivo;

AccesoDB con = new AccesoDB();
CQueryHelperExtJS	queryHelper = null;
JSONObject jsonObj = new JSONObject();
try {
	con.conexionDB();
	queryHelper = new CQueryHelperExtJS(new CambioEstatusIfDE());
	
	rs = queryHelper.getCreateFile(request,con);
	while (rs.next()) 
	{
		numRegistros++;
		if(numRegistros==1){	//Muestra el encabezado de resultados{
			contenidoArchivo.append("EPO,Nombre del proveedor,Moneda,Num. Documento,Fecha de emision,Fecha de Vencimiento,Tipo de cambio,Fecha de cambio,Causa,Monto Documento,Porcentaje Descuento,Recurso Garantia,Monto Descontar,Tasa,Plazo,Monto Operar, Referencia\n");
		} //fin del encabezado de resultados
		
		nombreEpo = (rs.getString("NOMBREEPO") == null) ? "" : rs.getString("NOMBREEPO");
		nombrePyme = (rs.getString("NOMBREPYME") == null) ? "" : rs.getString("NOMBREPYME");
		monedaClave = rs.getInt("IC_MONEDA");
		monedaNombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");

		numeroDocumento = (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
		fechaEmision = (rs.getString("DF_FECHA_DOCTO") == null) ? "" : rs.getString("DF_FECHA_DOCTO");
		fechaVencimiento = (rs.getString("DF_FECHA_VENC") == null) ? "" : rs.getString("DF_FECHA_VENC");
		tasaAceptada = (rs.getString("IN_TASA_ACEPTADA") == null) ? "0.00" : rs.getString("IN_TASA_ACEPTADA");
		plazo = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
		dblMontoDocto = rs.getDouble("fn_monto");
		dblPorcentaje = rs.getDouble("fn_porc_anticipo");
		dblMontoDescuento = rs.getDouble("fn_monto_dscto");
		monto = (rs.getString("IN_IMPORTE_RECIBIR") == null) ? "0.00" : rs.getString("IN_IMPORTE_RECIBIR");
		referencia = (rs.getString("REFERENCIA_PYME") == null) ? "" : rs.getString("REFERENCIA_PYME");	//FODEA 17
		cambioEstatus = (rs.getString("CAMBIOESTATUS") == null) ? "" : rs.getString("CAMBIOESTATUS");
		fechaCambioEstatus = (rs.getString("DC_FECHA_CAMBIO") == null) ? "" : rs.getString("DC_FECHA_CAMBIO");
		cambioMotivo = (rs.getString("CT_CAMBIO_MOTIVO") == null) ? "" : rs.getString("CT_CAMBIO_MOTIVO");

		dblRecurso = dblMontoDocto - dblMontoDescuento;

		contenidoArchivo.append(nombreEpo.replace(',',' ')+
			","+nombrePyme.replace(',',' ')+","+monedaNombre+
			","+numeroDocumento+","+fechaEmision+","+fechaVencimiento+","+cambioEstatus+
			","+fechaCambioEstatus+","+cambioMotivo.replace(',',' ')+
			","+Comunes.formatoDecimal(dblMontoDocto,2,SIN_COMAS)+","+Comunes.formatoDecimal(dblPorcentaje,5,SIN_COMAS)+" %,"+dblRecurso+","+Comunes.formatoDecimal(dblMontoDescuento,2,SIN_COMAS)+
			","+tasaAceptada+" %,"+plazo+",$"+Comunes.formatoDecimal(monto,2,SIN_COMAS)+","+referencia+"\n");
	} //fin del while
			con.cierraStatement();

	if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
	
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	if (con.hayConexionAbierta() == true) {
		con.cierraConexionDB();
	}
}
%>
<%=jsonObj%>
