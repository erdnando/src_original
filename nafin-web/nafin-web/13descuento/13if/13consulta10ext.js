Ext.onReady(function (){ 
	//Ext.QuickTips.init();
	
	var procesarSuccessFailureGenerarXpaginaPDF = function(opts, success, response){
		var btnGenerarPDF = Ext.getCmp('btnGenerarXpaginaPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarXpaginaPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPDF = function(opts,success,response){
		var btnGenerarPDF = Ext.getCmp('btnImprimirPDF');
		btnGenerarPDF.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing: 'bounceOut'});
			btnBajarPDF.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response){
		var btnGenerarArchivo = Ext.getCmp('btnGenerarXLS');
		btnGenerarArchivo.setIconClass('');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var btnBajarArchivo = Ext.getCmp('btnBajarXLS');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			btnBajarArchivo.setHandler(function(boton,evento){
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}else{
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//gridTotales
	var procesarConsultaDataTotales = function(store, arrRegistros, opts) {
		var gridTotales = Ext.getCmp('gridTotales');
			
		if (arrRegistros != null) {		
		
			if(store.getTotalCount() > 0) {			
				gridTotales.show();	
			}else {
				gridTotales.hide();	
			}
		}
	}
	
	var procesarConsultaDataGrid = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridGeneral = Ext.getCmp('grid');
		var btnGenerarArchivoXLS 	= Ext.getCmp('btnGenerarXLS');
		var btnBajarArchivoXLS 		= Ext.getCmp('btnBajarXLS');
		var btnGenerarArchivoPDF 	= Ext.getCmp('btnImprimirPDF');
		var btnBajarArchivoPDF 		= Ext.getCmp('btnBajarPDF');
		var el = gridGeneral.getGridEl();		
			
		if (arrRegistros != null) {
			
			if (!gridGeneral.isVisible()) {
				gridGeneral.show();				
				}
			
			if(store.getTotalCount() > 0) {		
			
				resumenTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Totales'						
					})
				});
				
				btnGenerarArchivoXLS.enable();
				btnBajarArchivoXLS.hide();
				btnGenerarArchivoPDF.enable();
				btnBajarArchivoPDF.hide();
				el.unmask();
				
			} else {
				btnGenerarArchivoXLS.disable();
				btnBajarArchivoXLS.hide();
				btnGenerarArchivoPDF.disable();
				btnBajarArchivoPDF.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var resumenTotalesData = new Ext.data.JsonStore({
			root: 'registros',
			url: '13consulta10ext.data.jsp',
			baseParams: {
						informacion: 'Totales'
			},
			fields: [
						{name: 'TOTAL_MONTO_DOC'},
						{name: 'TOTAL_MONTO_COM'},
						{name: 'TOTAL_TXT'}
			],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
				load: procesarConsultaDataTotales,
				exception: NE.util.mostrarDataProxyError
			}
	});
//-----------------------------------STORES-------------------------------------
	var catalogoMonedaData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '13consulta10ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty: 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//-----------------------------------STORES-------------------------------------
	var catalogoEPOData = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '13consulta10ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var consultaDataGrid = new Ext.data.JsonStore({
		root: 'registros',
		url: '13consulta10ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
					{name: 'NOMBREIF'},
					{name: 'IC_NOMBRE'},
					{name: 'NOMBREEPO'},
					{name: 'DF_FECHA_SOLICITUD'},
					{name: 'FG_PORC_COMISION_FONDEO'},
					{name: 'IC_MONEDA'},
					{name: 'CD_NOMBRE'},
					{name: 'FN_MONTO'},
					{name: 'MONTOCOMISION'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataGrid,
			exception: {
						fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							procesarConsultaDataGrid(null,null,null);//Llama procesar consulta, para que desbloquee los componentes.
						}
			}
		}
	});
//-------------------------------COMPONENTES------------------------------------
	var ahora = new Date();
	var mes = ahora.getMonth();
	var unMesAtras = mes+'/'+(ahora.getDate()+1)+'/'+ahora.getFullYear();
	
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'cmbEPO',
			allowBlank: true,
			fieldLabel: 'EPO',
			emptyText: 'Seleccione la EPO',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_epo',
			triggerAction: 'all',
			typeAhead: true,
			forceSelection: true,
			minChars: 1,
			store: catalogoEPOData
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de operaci�n',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_operacion_de',
					id: 'dc_fecha_opMin',
					//allowBlank: false,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecha: 'dc_fecha_opMax',
					margins: '0 20 0 0',
					startDateField: 'startdt',
					value:new Date(unMesAtras),
               formBind: true,
					format: 'd/m/Y'
				},{
					xtype: 'displayfield',
					value: 'a',
					width: 24
				},{
					xtype: 'datefield',
					name: 'df_fecha_operacion_a',
					id: 'dc_fecha_opMax',
					//allowBlank: false,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'dc_fecha_opMin',
               formBind: true,
					margins: '0 20 0 0',
					value:new Date(),
					endDateField: 'enddt'
				}
			]
		},{
			xtype: 'combo',
			name: 'ic_moneda',
			fieldLabel: 'Moneda',
			allowBlank: true,
			emptyText: 'Selecione Moneda',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName: 'ic_moneda',
			forceSelection: true,
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoMonedaData
		}
	];
   
	var fp = {
		xtype: 'form',
		id: 'forma',
		style:   'margin: 0 auto',
		title: '<div style="text-align:center">Comisi�n por Operaciones Fondeo Propio</div>',
		frame:true,
		width: 600,
		titleCollapse: false,
		title: 'Comisi�n por Operaciones Fondeo Propio',
		bodyStyle: 'padding: 6px',
		monitorValid: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
            //formBind: true,
				handler: function(boton, evento) {
               var fp = Ext.getCmp('forma');
               var dc_fecha_opMin = Ext.getCmp('dc_fecha_opMin');
               var dc_fecha_opMax = Ext.getCmp('dc_fecha_opMax');
               
					if((!Ext.isEmpty(dc_fecha_opMin.getValue()) && Ext.isEmpty(dc_fecha_opMax.getValue())) || (Ext.isEmpty(dc_fecha_opMin.getValue()) && !Ext.isEmpty(dc_fecha_opMax.getValue()))){
                  if(!Ext.isEmpty(dc_fecha_opMin.getValue()) && Ext.isEmpty(dc_fecha_opMax.getValue())){
                     dc_fecha_opMax.markInvalid('Debe seleccionar la fecha de operaci�n final');
                  }
                  if(Ext.isEmpty(dc_fecha_opMin.getValue()) && !Ext.isEmpty(dc_fecha_opMax.getValue())){
                     dc_fecha_opMin.markInvalid('Debe seleccionar la fecha de operaci�n inicial');
                  }
						return;
					}
					
				
					if(!Ext.isEmpty(dc_fecha_opMin.getValue()) && !Ext.isEmpty(dc_fecha_opMax.getValue())){							
					
						var fechaIni = Ext.util.Format.date(dc_fecha_opMin.getValue(),'d/m/Y');
						var fechaFin = Ext.util.Format.date(dc_fecha_opMax.getValue(),'d/m/Y');
											
						if(datecomp(fechaIni,fechaFin)==1) {
							dc_fecha_opMin.markInvalid("La fecha de este campo debe de ser anterior a."+fechaIni);
							dc_fecha_opMin.focus();
							return;
						}
					}
					
								
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaDataGrid.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consulta'						
						})
					});		
											
				
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta10ext.jsp';
				}
			}
		]
	};
	var gridGeneral = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		clicksToEdit:1,
		columns: [{
				header: 'Producto', tooltip: 'Producto',
				menuDisabled:true,
				dataIndex: 'IC_NOMBRE',
				sortable: true,
				width: 150, resizable: true,
				align: 'center',
				renderer: function(value){ return '<div style="text-align:left">' + value + "</div>"; }
			},{
				header: 'EPO', tooltip: 'EPO',
				menuDisabled:true,
				dataIndex: 'NOMBREEPO',
				sortable: true,	align: 'center',
				width: 200, resizable: true,
				align: 'center',
				renderer: function(value){ return '<div style="text-align:left">' + value + "</div>"; }
			},{
				header: 'Fecha de Operaci�n', tooltip: 'Fecha de Operaci�n',
				menuDisabled:true,
				dataIndex: 'DF_FECHA_SOLICITUD',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'center'
			},{
				header: '% Comisi�n', tooltip: '% Comisi�n',
				menuDisabled:true,
				dataIndex: 'FG_PORC_COMISION_FONDEO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'right',
				renderer: function(value){ return value + ' %&nbsp;&nbsp;&nbsp;&nbsp;'; }
			},{
				header: 'Moneda', tooltip: 'Moneda',
				menuDisabled:true,
				dataIndex: 'CD_NOMBRE',
				sortable: true,	align: 'center',
				width: 115, resizable: true,
				align: 'center',
				renderer: function(value){ return '<div style="text-align:left">' + value + "</div>"; }
			},{
				header: 'Monto del Documento', tooltip: 'Monto del Documento',
				menuDisabled:true,
				dataIndex: 'FN_MONTO',
				sortable: true,	align: 'center',
				width: 100, resizable: true,
				align: 'right',
				renderer: Ext.util.Format.usMoney
			},{
				header: 'Monto Comisi�n', tooltip: 'Monto Comisi�n',
				menuDisabled:true,
				dataIndex: 'MONTOCOMISION',
				sortable: true,	align: 'center',
				width: 120, resizable: true,
				align: 'right',
				renderer: Ext.util.Format.usMoney
			}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 910,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
		xtype: 'toolbar',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarXLS',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta10ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'XLS',
								informacion: 'Consulta'
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarXLS',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Imprimir',
					id: 'btnImprimirPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta10ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'PDF',
								informacion: 'Consulta'
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		},
		title:''
	};
	var gridTotales = {
		xtype: 'grid',
		store: resumenTotalesData,
		id: 'gridTotales',
		style: 'margin:0 auto;',
		hidden: true,
		enableColumnMove: false,
		columns: [
			{
				header: 'Total Moneda',
				dataIndex: 'TOTAL_TXT',
				align: 'center',
				width: 150,
				renderer: function(val){ return '<div style="text-align:right;font-weight:bold">'+val+'</div>'; }
			},
			{
				header: 'Monto del Documento',
				dataIndex: 'TOTAL_MONTO_DOC',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.usMoney
			},
			{
				header: 'Monto Comisi�n',
				dataIndex: 'TOTAL_MONTO_COM',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.usMoney
			}
		],
		width: 458,
		height: 78,
		frame: false
	};
//----------------------------------PRINCIPAL-----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridGeneral,
			gridTotales,
			NE.util.getEspaciador(30)
		]
	});
	catalogoEPOData.load();
});