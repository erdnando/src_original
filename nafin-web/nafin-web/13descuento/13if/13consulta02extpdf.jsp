<%@ page import=
		"java.util.*, netropology.utilerias.*,com.netro.descuento.*, 
		com.netro.pdf.*, net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

try {
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new com.netro.descuento.CambioEstatusIfDE());

	int start = 0;
	int limit = 0;
	int nRow = 0;
	Registros reg = new Registros();
	
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	reg = queryHelper.getPageResultSet(request,start,limit);

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
	while (reg.next()) {
		if(nRow == 0){
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
		    pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
					
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.setTable(17, 100); //Se agrego 1 por FODEA 17 originalmente eran 16
			pdfDoc.setCell("EPO", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Proveedor", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Numero Documento", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Emisión", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Vencimiento", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Tasa", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Documento", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Porcentaje Descuento", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Recurso en Garantía", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto a Descontar", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto a Operar", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Cambio de Estatus", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha del Cambio de Estatus", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Causa", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Referencia", "formasmenB", ComunesPDF.CENTER); //FODEA 17
		}

		pdfDoc.setCell(reg.getString("nombreEPO"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("nombrePyme"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("cd_nombre"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("ig_numero_docto"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("df_fecha_docto"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("df_fecha_venc"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("in_tasa_aceptada"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("plazo"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("fn_monto"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("fn_porc_anticipo"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell("calcular", "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("fn_monto_dscto"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("in_importe_recibir"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("CambioEstatus"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("dc_fecha_cambio"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("ct_cambio_motivo"), "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(reg.getString("referencia_pyme"), "formasmen", ComunesPDF.CENTER);
		nRow++;
	}

	pdfDoc.addTable();
	pdfDoc.endDocument();

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch(Exception e) {
	throw new AppException("Error al generar el archivo", e);
}
%>
<%=jsonObj%>
