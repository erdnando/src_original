<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,	
		java.sql.*, 
		java.util.Vector,
		org.apache.commons.logging.Log,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoPYME,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String in_limite_epo_dl = (request.getParameter("in_limite_epo_dl")!=null)?request.getParameter("in_limite_epo_dl"):"";
String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	
StringBuffer condicion = new StringBuffer();

String infoRegresar = "";
int start = 0;
int limit = 0;	
JSONObject jsonObj = new JSONObject();

if (informacion.equals("valoresIniciales")) {
	
	LimitesPorEPO beanLimites = ServiceLocator.getInstance().lookup("LimitesPorEPOEJB", LimitesPorEPO.class);
	HashMap monedas = beanLimites.validaMonedas(ic_epo , iNoCliente);	
	String  moneda_mn = monedas.get("MONEDA_MN").toString();	
	String moneda_dl = monedas.get("MONEDA_DL").toString();	 	

	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("moneda_mn", moneda_mn);
	jsonObj.put("moneda_dl", moneda_dl);

	infoRegresar = jsonObj.toString();	
	log.debug(infoRegresar);

}else if (informacion.equals("CatalogoEPO")) {
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveIf(iNoCliente);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
} else if (
		informacion.equals("Consulta") || 
		informacion.equals("ArchivoCSV") ||
		informacion.equals("ArchivoPDF") ) {

	String in_limite_epom = (request.getParameter("in_limite_epom") == null)?"":request.getParameter("in_limite_epom");

	com.netro.descuento.LimitesXEpo paginador = new com.netro.descuento.LimitesXEpo();
	paginador.setClaveEpo(ic_epo);
	paginador.setClaveIf(iNoCliente);
	paginador.setMontoLimite(in_limite_epom);
	paginador.setMontoLimite_dl(in_limite_epo_dl);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	
	//NO USAR queryHelper.setMultiplesPaginadoresXPagina(true);  si no es absolutamente requerido, dado que pudiera afectar rendimiento
	//SOLO colocarlo si hay mas de un paginador en la misma pantalla. 
	queryHelper.setMultiplesPaginadoresXPagina(true);
	

	
	if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);															   		
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	} else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	} else if (informacion.equals("ArchivoPDF")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}
}  else if (
		informacion.equals("VerDetalle") || 
		informacion.equals("ArchivoDetalleCSV") ||
		informacion.equals("ArchivoDetallePDF") ) {
	
	String claveEpo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	
	com.netro.descuento.ConsDoctosProgPyme paginador = new com.netro.descuento.ConsDoctosProgPyme();
	paginador.setIc_if(iNoCliente);
	paginador.setIc_epo(claveEpo);
	paginador.setTipoUsuario(strTipoUsuario);

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	//NO USAR queryHelper.setMultiplesPaginadoresXPagina(true);  si no es requerido, dado que pudiera afectar rendimiento
	//SOLO colocarlo si hay mas de un paginador en la misma pantalla. 
	queryHelper.setMultiplesPaginadoresXPagina(true);
	
	if (informacion.equals("VerDetalle")) {
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);															   		
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

	} else if (informacion.equals("ArchivoDetalleCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	} else if (informacion.equals("ArchivoDetallePDF")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
			jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
	}
}

//Thread.sleep(10*1000);
%>


<%=infoRegresar%>
