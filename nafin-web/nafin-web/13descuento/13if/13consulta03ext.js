Ext.onReady(function() {
	
	

	var verDetalleHistorico = function(grid, rowIndex, colIndex, item, event) {

		var registro = grid.getStore().getAt(rowIndex);	
		var fecha = registro.get('FECHA');	
		var ic_epo = registro.get('IC_EPO');	
		var nombreEPO = registro.get('NOMBRE_EPO');	
			
		consultaDetalleData.load({
				params:{
					informacion: 'ConsultaDetalle',						
					fecha:fecha, 
					ic_epo:ic_epo,
					nombreEPO:nombreEPO					
				}
			});			
					
		var ventana = Ext.getCmp('verDetalle');	
		if (ventana) {	
			ventana.show();	
		} else {	
			new Ext.Window({
				layout: 'fit',
				width: 830,
				height: 'auto',			
				id: 'verDetalle',
				closeAction: 'hide',
				autoDestroy:true,
				closable:false,
				items: [	
				 gridDetalle
				],
				title: 'Detalle Historico Tasas',
				bbar: {
					xtype: 'toolbar',	
					buttons: [
						'-',
						'->',
						{
							xtype: 'button', 	buttonAlign:'right', 	text: 'Cerrar',id: 'btnCerrarDe', 
							handler: function(){
								Ext.getCmp('verDetalle').destroy();
							} 
						}							
					]
				}
			}).show();
		}
	}
	
	
	var procesarDetalleData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var jsonData = store.reader.jsonData;					
		var gridDetalle = Ext.getCmp('gridDetalle');	
	
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) {
				gridDetalle.show();
			}					
					
			//edito el titulo de la columna
			var el = gridDetalle.getGridEl();					
			var jsonData = store.reader.jsonData;				
			
			gridDetalle.setTitle(jsonData.nombreEPO);	
				
			if(store.getTotalCount() > 0) {						
				el.unmask();							
			} else {			
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaDetalleData   = new Ext.data.GroupingStore({	 
		root : 'registros',
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaDetalle'
		},		
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [			
			{name: 'INTERMEDIARIO'},
			{name: 'MONEDA'},
			{name: 'TASA_BASE'},
			{name: 'PLAZO'},
			{name: 'VALOR'},
			{name: 'REL_MAT'},
			{name: 'PUNTOS'},
			{name: 'TASA_APLICAR'}
			]
		}),
		groupField: 'MONEDA',
		sortInfo:{field: 'MONEDA', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDetalleData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDetalleData(null, null, null);
				}
			}
		}		
	});
		
	var gridDetalle = {
		xtype: 'editorgrid',
		store: consultaDetalleData,
		id: 'gridDetalle',		
		hidden: true,
		title: 'Detalle Historico Tasas',
		columns: [		
			{
				header: 'INTERMEDIARIO', 
				tooltip: 'INTERMEDIARIO',
				dataIndex: 'INTERMEDIARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'MONEDA', 
				tooltip: 'MONEDA',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},	
			{
				header: 'Tasa Base', 
				tooltip: 'Tasa Base',
				dataIndex: 'TASA_BASE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Plazo', 
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Valor', 
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Rel. Mat.', 
				tooltip: 'Rel. Mat.',
				dataIndex: 'REL_MAT',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Puntos Adicionales', 
				tooltip: 'Puntos Adicionales',
				dataIndex: 'PUNTOS',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},	
			{
				header: 'Tasa a Aplicar', 
				tooltip: 'Tasa a Aplicar',
				dataIndex: 'TASA_APLICAR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00000%')
			}
		],
		height: 300,
		width: 800,
		view: new Ext.grid.GroupingView({
			forceFit:true,
         groupTextTpl: '{text}'					 
      })
	}
	
	var procesarGeneralData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var jsonData = store.reader.jsonData;					
		var gridConsulta = Ext.getCmp('gridConsulta');	
	
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
				gridGeneral.show();
			}				
					
			//edito el titulo de la columna
			var el = gridGeneral.getGridEl();					
			var jsonData = store.reader.jsonData;				
			
			var gridHistorico = Ext.getCmp('gridHistorico');	
						
			if(store.getTotalCount() > 0) {			
			
				//para el Historico de Tasas
				consultaHistoricoData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ConsultaHistorico',
							lsEposEnc:jsonData.lsEposEnc
						})
					});
					
					gridHistorico.show();
					
				el.unmask();							
			} else {	
					
				gridHistorico.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	//CONSULTA PARA LOS DEL GRID GENERAL
	var consultaGeneralData   = new Ext.data.GroupingStore({	 
		root : 'registros',
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [			
			{name: 'NOMBRE_EPO'},
			{name: 'PROVEEDOR'},
			{name: 'REFERENCIA'},
			{name: 'PLAZO'},
			{name: 'TIPO_TASA'},
			{name: 'VALOR'},
			{name: 'FECHA'},
			{name: 'VALOR_TASA'},
			{name: 'EPO'},
			{name: 'IF'},
			{name: 'NAFIN'}	
			]
		}),
		groupField: 'NOMBRE_EPO',
		sortInfo:{field: 'NOMBRE_EPO', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarGeneralData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarGeneralData(null, null, null);
				}
			}
		}		
	});
	
	
	var procesarHistoricoData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var jsonData = store.reader.jsonData;					
		var gridHistorico = Ext.getCmp('gridHistorico');	
	
		if (arrRegistros != null) {
			if (!gridHistorico.isVisible()) {
				gridHistorico.show();
			}				
					
			//edito el titulo de la columna
			var el = gridHistorico.getGridEl();					
			var jsonData = store.reader.jsonData;				
						
			if(store.getTotalCount() > 0) {					
				el.unmask();							
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
		//CONSULTA PARA LOS DEL GRID GENERAL
	var consultaHistoricoData   = new Ext.data.GroupingStore({	 
		root : 'registros',
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaHistorico'
		},		
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [			
			{name: 'NOMBRE_EPO'},			
			{name: 'FECHA'},
			{name: 'IC_EPO'}
			]
		}),
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarHistoricoData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarHistoricoData(null, null, null);					
				}
			}
		}		
	});
	
	
	var gridHistorico = new Ext.grid.GridPanel({
		store: consultaHistoricoData,
		id: 'gridHistorico',
		margins: '20 0 0 0',
		hidden: true,
		title: 'Hist�rico de Tasas',
		columns: [	
			{
				header: '',
				tooltip: '',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},
			{
				header: '',
				tooltip: '',
				dataIndex: 'FECHA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				 xtype: 'actioncolumn',
				header: 'Detalle Aplicaci�n',
				tooltip: 'Detalle Aplicaci�n',
				width: 130,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Ver';	
							return 'iconoLupa';	
						},												
						 handler: verDetalleHistorico
					}
				]				
			}
		],	
		stripeRows: true,
		style: 'margin:0 auto;',
		loadMask: true,
		height: 400,
		width: 500,		
		frame: true		
	});
	
		//esto esta en duda como manejarlo
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 8, align: 'center'},
					{header: 'Autorizaci�n', colspan: 3, align: 'center'}					
				]
			]
	});
	
	var gridGeneral = new Ext.grid.GridPanel({
		store: consultaGeneralData,
		id: 'gridGeneral',
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hidden: true,
		plugins: grupos,	
		columns: [	
			{
				header: 'NOMBRE EPO',
				tooltip: 'NOMBRE EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				hidden:true,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'PROVEEDOR',
				tooltip: 'PROVEEDOR',
				dataIndex: 'PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia',
				dataIndex: 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: ' Tipo Tasa',
				tooltip: ' Tipo Tasa',
				dataIndex: 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{
				header: ' Valor',
				tooltip: ' Valor',
				dataIndex: 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: ' Fecha',
				tooltip: ' Fecha',
				dataIndex: 'FECHA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: ' Valor tasa',
				tooltip: ' Valor tasa',
				dataIndex: 'VALOR_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: ' EPO',
				tooltip: 'EPO',
				dataIndex: 'EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: ' IF',
				tooltip: 'IF',
				dataIndex: 'IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header: ' NAFIN',
				tooltip: 'NAFIN',
				dataIndex: 'NAFIN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			}			
		],	
		view: new Ext.grid.GroupingView({
			forceFit:true,
         groupTextTpl: '{text}'					 
      }),
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true		
	});
	
	
	
	var catalogoPYME = new Ext.data.JsonStore({
		id: 'catalogoPYME',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'catalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
		
	
	var procesarSuceptibleFloating =  function(opts, success, response) {		
		if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {			
			var resp = 	Ext.util.JSON.decode(response.responseText);				
			
			Ext.getCmp('suceptibleFloating').enable();	
			Ext.getCmp('aplicaFloating2').setValue(false);
			Ext.getCmp('aplicaFloating1').setValue(false);
			if(resp.opeSucepFloating ==='N')  {				
				Ext.getCmp('suceptibleFloating').disable();				
			}
			
		}
	
	};
	
	var procesarCatalogoMonedaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			 var ic_moneda1 = Ext.getCmp('ic_moneda1');
			if(ic_moneda1.getValue()==''){
			ic_moneda1.setValue(records[0].data['clave']);
			}
		}
	}
  
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	
			load: procesarCatalogoMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var catalogoTipoTasa = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
		   ['B','BASE'],	
			['N','NEGOCIADA'],
			['P','PREFERENCIAL'],
			['TODAS','TODAS']	
		 ]
	});
	
		
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	
	var  elementosForma  = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Tasa',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Tipo Tasa',
					name: 'lstTipoTasa',
					id: 'lstTipoTasa1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'lstTipoTasa',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoTipoTasa,
					listeners: 	{
						select:function(combo, record, index) {
							Ext.getCmp("suceptibleFloating").hide();
							Ext.getCmp('aplicaFloating2').setValue(false);
							Ext.getCmp('aplicaFloating1').setValue(false);
							if(combo.value==='P'){
								Ext.getCmp("suceptibleFloating").show();								
							}
						}
					}
				}	
			]
		},		 
		{
			xtype: 'multiselect',
         fieldLabel: 'EPO Relacionada',
         name: 'lstEPO',
			id: 'lstEPO1',
         width: 300,
         height: 200,  
         allowBlank:false,
        store:catalogoEPO,
		  displayField: 'descripcion',
		  valueField: 'clave',		 
        tbar:[{
				text: 'Limpiar',
            handler: function(){
					fp.getForm().findField('lstEPO1').reset();
	         }
         }],
         ddReorder: true,
			 listeners: {
				change: function(combo){
					var cmblstPYME = Ext.getCmp('lstPYME1');
					cmblstPYME.setValue('');
					cmblstPYME.store.load({
						params: {
							lstEPO:combo.getValue()
						}
					});
				}
			 }		
        },
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'combo',
					fieldLabel: 'Moneda',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar ...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoMoneda			
				}
			]
		},
		{
			xtype: 'combo',
			fieldLabel: 'Nombre del Proveedor',
			name: 'lstPYME',
			id: 'lstPYME1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'TODAS ...',			
			valueField: 'clave',
			hiddenName : 'lstPYME',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoPYME,
			listeners: 	{
				select:function(combo, record, index) {
				
					Ext.Ajax.request({
					url : '13consulta03ext.data.jsp',
						params : {
							informacion: 'operaSuceptibleFloating',
							lstPYME:combo.value				
						},
						callback: procesarSuceptibleFloating
					});					
				}
			}
		},		
		{  
			xtype:  'radiogroup',   
			fieldLabel: "Aplica Floating",    
			name: 'suceptibleFloating',   
			id: 'suceptibleFloating',  			 
			align: 'center',
			hidden: true,
			width: 200,							
				items: [						
					{ 
						boxLabel:    "Si",             
						name:        'aplicaFloating', 
						id: 'aplicaFloating1',
						inputValue:  "S" ,
						value: 'S',
						checked: false,
						width: 20
					}, 				
					{           
						boxLabel: "No",             
						name: 'aplicaFloating', 
						id: 'aplicaFloating2',
						inputValue:  "N", 
						value: 'N',							
						width: 20						
					} 				
				]
			}
		];
	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Tasas',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 100,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,	  		
		monitorValid: true,		
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				handler: function(boton, evento) {				
				
				var lstTipoTasa = Ext.getCmp("lstTipoTasa1");				
					if ( Ext.isEmpty(lstTipoTasa.getValue()) ) {
						lstTipoTasa.markInvalid('Por favor, especifique el tipo de Tasa');
						return;
					}
					
					var lstEPO = Ext.getCmp("lstEPO1");				
					if (Ext.isEmpty(lstEPO.getValue()) ) {
						lstEPO.markInvalid('Por favor, especifique la(s) EPO(s) Relacionada(S)');
						return;
					}
					
					var lstPYME = Ext.getCmp("lstPYME1");				
					if ( Ext.isEmpty(lstPYME.getValue()) ) {
						lstPYME.markInvalid('Por favor, especifique el Nombre del Proveedor ');
						return;
					}
					
					var ventana = Ext.getCmp('verDetalle');	
					if (ventana) {	
						ventana.destroy();	
					}
								
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaGeneralData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consulta'						
						})
					});						
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta03ext.jsp';
				}
			}
		]
	});
		
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,	
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridGeneral, 
			NE.util.getEspaciador(20),
			gridHistorico,
			NE.util.getEspaciador(20)
		]
	});
  
  
  catalogoEPO.load();
  catalogoMoneda.load();
  catalogoPYME.load();
  
});