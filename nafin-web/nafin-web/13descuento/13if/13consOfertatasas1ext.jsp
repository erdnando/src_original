<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>

<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>

<%-- El jsp que contiene el dise�o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>

</head>
<%if(esEsquemaExtJS) {%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script type="text/javascript" src="13consOfertatasas1ext.js?<%=session.getId()%>"></script>

<%if(esEsquemaExtJS) {%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<div id="_menuApp" align="center" ></div>
	<div id="Contcentral" align="center">	
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
		<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
   </div>
</div>
<%@ include file="/01principal/01nafin/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
</body>
<%}else  if(!esEsquemaExtJS) {%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id="areaContenido" align="center"></div> <!-- dentro puede ir un style="margin-left: 3px; margin-top: 3px;" -->
<form id='formAux' name="formAux" target='_new'></form>
</body>
<%}%>

</body>
</html>