<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoPYME,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";

String infoRegresar = "";
if (informacion.equals("CatalogoEPO")) {
	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClaveIf(iNoCliente);	//iNoCliente viene de sesion. (ic_if)
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
} else if (informacion.equals("CatalogoPYME")) {
	String claveEpo = (request.getParameter("claveEpo")!=null)?request.getParameter("claveEpo"):"";
	if (!claveEpo.equals("")) {
		CatalogoPYME cat = new CatalogoPYME();
		cat.setCampoClave("ic_pyme");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setClaveEpo(claveEpo);
		cat.setConDocumentosCambioEstatus(true);
		cat.setTipoCambioEstatus("2,23");
		cat.setIfCuentaBancaria(iNoCliente); //ic_if
//		cat.setOrden("cg_razon_social"); //Si existiera ambigüedad se puede usar cat.getPrefijoTablaPrincipal() para usarlo al armar el orden.
		infoRegresar = cat.getJSONElementos();
	}
} else if (informacion.equals("CatalogoMoneda")) {
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
//		cat.setOrden("cg_razon_social"); //Si existiera ambigüedad se puede usar cat.getPrefijoTablaPrincipal() para usarlo al armar el orden.
	infoRegresar = cat.getJSONElementos();
} else if (informacion.equals("CatalogoCambioEstatus")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_cambio_estatus");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_cambio_estatus");
	cat.setValoresCondicionIn("2,23,32,34", String.class);
	cat.setOrden("ic_cambio_estatus");
	infoRegresar = cat.getJSONElementos();
} else if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion
	int start = 0;
	int limit = 0;
	
	
	//if (start == 0) throw new AppException("Exception dummy");
	
	
	
	
	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new com.netro.descuento.CambioEstatusIfDE());
	
	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		
		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		//infoRegresar = "({\"success\": false, \"registros\": []})";
		
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
} else if (informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales
	//Debe ser llamado despues de Consulta
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

} else if(informacion.equals("Imprime_PDF")){

	String ic_epo             = (request.getParameter("ic_epo")             == null)?"":request.getParameter("ic_epo");
	String ic_pyme            = (request.getParameter("ic_pyme")            == null)?"":request.getParameter("ic_pyme");
	String ic_moneda          = (request.getParameter("ic_moneda")          == null)?"":request.getParameter("ic_moneda");
	String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null)?"":request.getParameter("dc_fecha_cambioMin");
	String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null)?"":request.getParameter("dc_fecha_cambioMax");
	String fn_montoMin        = (request.getParameter("fn_montoMin")        == null)?"":request.getParameter("fn_montoMin");
	String fn_montoMax        = (request.getParameter("fn_montoMax")        == null)?"":request.getParameter("fn_montoMax");
	String ic_cambio_estatus  = (request.getParameter("ic_cambio_estatus")  == null)?"":request.getParameter("ic_cambio_estatus");
	String ic_if = (String)request.getSession().getAttribute("iNoCliente");

	JSONObject jsonObj = new JSONObject();
	CambioEstatusIfDE paginador = new CambioEstatusIfDE();
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	paginador.setIc_epo(ic_epo);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_moneda(ic_moneda);
	paginador.setDc_fecha_cambioMin(dc_fecha_cambioMin);
	paginador.setDc_fecha_cambioMax(dc_fecha_cambioMax);
	paginador.setFn_montoMin(fn_montoMin);
	paginador.setFn_montoMax(fn_montoMax);
	paginador.setIc_cambio_estatus(ic_cambio_estatus);
	paginador.setIc_if(ic_if);

	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();

}
%>
<%
//infoRegresar = "({\"success\": false, \"registros\": []})";
//Thread.sleep(10 * 1000);
%>
<%=infoRegresar%>

