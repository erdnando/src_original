Ext.onReady(function() {
	var tasaBorrar =[];
	
	function procesarSuccessFailureEliminar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
						
				Ext.MessageBox.alert('Mensaje','Las Tasas fueron eliminadas con exito');
				var gridConsulta = Ext.getCmp('gridConsulta');
				gridConsulta.hide();
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarCancelar= function() 	{

		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
			tasaBorrar =  [];
			store.each(function(record) {	
			tasaBorrar.push(record.data['TASAS_BORRAR']);		
		});	
		
		Ext.Ajax.request({
			url : '13consulta014ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'EliminarTasas',
				tasaBorrar:tasaBorrar
			}),				
			callback: procesarSuccessFailureEliminar
		});		
		
	}

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();	
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
			gridConsulta.setTitle(jsonData.lsNombreEPO);		
			var btnCancelar = Ext.getCmp('btnCancelar');
			
			var fp = Ext.getCmp('forma');
			
			if(store.getTotalCount() > 0) {	
				btnCancelar.enable();
				el.unmask();				
			} else {						
				btnCancelar.disable();
				el.mask('No se encontraron EPOS relacionadas', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta014ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [	
			{name: 'NOMBRE_PYME'},
			{name: 'REFERENCIA'},	
			{name: 'PLAZO'},
			{name: 'TIPO_TASA'},	
			{name: 'VALOR'},
			{name: 'FECHA'},
			{name: 'VALOR_TASA'},
			{name: 'NOMBRE_EPO'},	
			{name: 'NOMBRE_IF'},
			{name: 'NAFIN'},
			{name: 'TASAS_BORRAR'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
			//esto esta en duda como manejarlo
	var gruposConsulta = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '', colspan: 7, align: 'center'},	
				{	header: 'Autorización ', colspan: 3, align: 'center'}
			]
		]
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Tasas Preferenciales/Negociadas para Mandato',
		hidden: true,
		plugins: gruposConsulta,
		clicksToEdit: 1,
		columns: [			
			{							
				header : 'Mandante',
				tooltip: 'NOMBRE_PYME',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Referencia',
				tooltip: 'REFERENCIA',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo de Tasa',
				tooltip: 'Tipo de Tasa',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Valor',
				tooltip: 'Valor',
				dataIndex : 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'Fecha',
				tooltip: 'Fecha',
				dataIndex : 'FECHA',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{							
				header : 'Valor Tasa',
				tooltip: 'Valor Tasa',
				dataIndex : 'VALOR_TASA',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'IF',
				tooltip: 'IF',
				dataIndex : 'NOMBRE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Nafin',
				tooltip: 'Nafin',
				dataIndex : 'NAFIN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false,		
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Cancelar Relación ',					
					tooltip:	'Cancelar Relación ',
					iconCls: 'icoAceptar',
					id: 'btnCancelar',
					handler: procesarCancelar
				}			
			]
		}
	});
		
		

// ********************** datos para la  Forma 	******************************
		
	var procesarCatalogoMonedaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_moneda1 = Ext.getCmp('ic_moneda1');
			if(ic_moneda1.getValue()==''){
				ic_moneda1.setValue(records[0].data['clave']);
			}
		}
	}		
	
	var procesarCatalogoTipoTasaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var tipo_tasa1 = Ext.getCmp('tipo_tasa1');
			if(tipo_tasa1.getValue()==''){
				tipo_tasa1.setValue(records[0].data['clave']);
			}
		}
	}	
	
	
	var catalogoTipoTasa = new Ext.data.JsonStore({
		id: 'catalogoTipoTasa',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta014ext.data.jsp',
		baseParams: {
			informacion: 'catalogoTipoTasa'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoTipoTasaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
		
	var storeCatalogoMandante = new Ext.data.JsonStore({
		id: 'storeCatalogoMandante',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta014ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMandante'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta014ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta014ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	

	var elementosForma = [	
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Tasa',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'tipo_tasa',
					id: 'tipo_tasa1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccione  Tipo de Tasa...',			
					valueField: 'clave',
					hiddenName : 'tipo_tasa',
					fieldLabel: 'Tipo de Tasa',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoTipoTasa		
				}
			]
		 },	
		 {
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione  EPO...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',
			fieldLabel: 'Epo que opera Factoraje con Mandato',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			listeners: {
				select: {
					fn: function(combo) {
						var cmbMandante = Ext.getCmp('ic_mandante1');
						cmbMandante.setValue('');
						cmbMandante.store.load({
							params: {
								ic_epo:combo.getValue()
							}
						});
					}
				}
			}
		},			
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar Moneda...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',
					fieldLabel: 'Moneda',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoMoneda
				}
			]
		},
		{
			xtype: 'combo',
			name: 'ic_mandante',
			id: 'ic_mandante1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'TODAS',	
			valueField: 'clave',
			hiddenName : 'ic_mandante',
			fieldLabel: 'Mandante',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeCatalogoMandante					
		}		
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: ' Consulta Tasas Preferenciales/Negociadas para Mandato',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200, 
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
			text: 'Buscar',
			id: 'btnBuscar',
			iconCls: 'icoAceptar',
			formBind: true,		
			handler: function(boton, evento) {
				
				var ic_epo = Ext.getCmp("ic_epo1");
				if (Ext.isEmpty(ic_epo.getValue()) ){
					ic_epo.markInvalid('Debe Seleccionar  la EPO.');
					return;
				}			
			
												
				fp.el.mask('Enviando...', 'x-mask-loading');			
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'									
					})
				});					
			}
		}	
		]
	});
	

//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),							
			fp,			
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)
		]
	});
	
	
	catalogoMoneda.load();
	catalogoEPO.load();	
	catalogoTipoTasa.load();
	
	
});