<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
	String df_fecha_operacion_de = (request.getParameter("df_fecha_operacion_de"))!=null?request.getParameter("df_fecha_operacion_de"):"";
	String df_fecha_operacion_a = (request.getParameter("df_fecha_operacion_a"))!=null?request.getParameter("df_fecha_operacion_a"):"";
	String operacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");
	String infoRegresar = "";
	int start			= 0;
	int limit			= 0;
	
	if(informacion.equals("CatalogoEPO")){
		 CatalogoEPO catalogo = new CatalogoEPO();
		 catalogo.setCampoClave("ic_epo");
		 catalogo.setCampoDescripcion("cg_razon_social");
		 catalogo.setClaveIf(iNoCliente);
		 infoRegresar = catalogo.getJSONElementos();
	}else if(informacion.equals("CatalogoMoneda")){
		CatalogoMoneda catalogo = new CatalogoMoneda();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setValoresCondicionIn("0,1,54", Integer.class);
		catalogo.setOrden("2");
		infoRegresar = catalogo.getJSONElementos();
		
		
	}else if(informacion.equals("Consulta")){
		ConsComiOperFondeoPropio paginador = new ConsComiOperFondeoPropio();
		paginador.setSes_ic_if(iNoCliente);
		paginador.setIc_epo(ic_epo);
		paginador.setDf_operacion_inicial(df_fecha_operacion_de);
		paginador.setDf_operacion_final(df_fecha_operacion_a);
		paginador.setIc_moneda(ic_moneda);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		
		if(informacion.equals("Consulta") && !operacion.equals("XLS") && !operacion.equals("PDF")){
			try {
				Registros reg	=	queryHelper.doSearch();
				infoRegresar	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		}else if(operacion.equals("XLS")){
			JSONObject jsonObj = new JSONObject();
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,operacion);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}else if(operacion.equals("PDF")){
			JSONObject jsonObj = new JSONObject();
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,operacion);
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
		}
	} else if (informacion.equals("Totales")) {		//Datos para el Resumen de Totales
			ConsComiOperFondeoPropio paginador = new ConsComiOperFondeoPropio();
			paginador.setSes_ic_if(iNoCliente);
			paginador.setIc_epo(ic_epo);
			paginador.setDf_operacion_inicial(df_fecha_operacion_de);
			paginador.setDf_operacion_final(df_fecha_operacion_a);
			paginador.setIc_moneda(ic_moneda);
			
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
			Registros registros = queryHelper.doSearch();
			HashMap datos;
			List reg=new ArrayList();
			float dblTotalMontoDocumentoMN=0, dblTotalMontoComisionMN=0;
			float dblTotalMontoDocumentoDL=0, dblTotalMontoComisionDL=0;
			
			while(registros.next()){
				
				if(registros.getString("ic_moneda").equals("1")){
					dblTotalMontoDocumentoMN+=Float.valueOf(registros.getString("fn_monto")).floatValue();
					dblTotalMontoComisionMN+=Float.valueOf(registros.getString("MontoComision")).floatValue();
				}else{
					dblTotalMontoDocumentoDL+=Float.valueOf(registros.getString("fn_monto")).floatValue();
					dblTotalMontoComisionDL+=Float.valueOf(registros.getString("MontoComision")).floatValue();;
				}
			}
			
			datos=new HashMap();
			datos.put("TOTAL_MONTO_DOC",String.valueOf(dblTotalMontoDocumentoMN));
			datos.put("TOTAL_MONTO_COM",String.valueOf(dblTotalMontoComisionMN));
			datos.put("TOTAL_TXT","Total Moneda Nacional");
			reg.add(datos);
			
			datos=new HashMap();
			datos.put("TOTAL_MONTO_DOC",String.valueOf(dblTotalMontoDocumentoDL));
			datos.put("TOTAL_MONTO_COM",String.valueOf(dblTotalMontoComisionDL));
			datos.put("TOTAL_TXT","Total Dolares");
			reg.add(datos);
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("registros", reg);
			infoRegresar=jsonObj.toString();
	}
%>
<%=infoRegresar%>
