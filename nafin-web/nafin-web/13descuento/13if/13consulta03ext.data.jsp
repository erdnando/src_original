<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	com.netro.mandatodoc.*,	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String lstTipoTasa = (request.getParameter("lstTipoTasa") != null) ? request.getParameter("lstTipoTasa") : "";
String lstEPO = (request.getParameter("lstEPO") != null) ? request.getParameter("lstEPO") : "";
String ic_moneda = (request.getParameter("ic_moneda") != null) ? request.getParameter("ic_moneda") : "";
String lstPYME = (request.getParameter("lstPYME") != null) ? request.getParameter("lstPYME") : "";
String lsEposEnc = (request.getParameter("lsEposEnc") != null) ? request.getParameter("lsEposEnc") : "";
String  ic_if =iNoCliente,  infoRegresar ="", consulta  ="";
String aplicaFloating = (request.getParameter("aplicaFloating") != null) ? request.getParameter("aplicaFloating") : "";

System.out.println ("aplicaFloating----> "+aplicaFloating);


JSONObject 	jsonObj	= new JSONObject();
HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();

AutorizacionTasas beanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);



 if (informacion.equals("catalogoEPO")  ) {

	CatalogoEPO catalogo = new CatalogoEPO();	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setClaveIf(ic_if);
	catalogo.setAceptacionRelacionIfEpo("S");
	infoRegresar = catalogo.getJSONElementos();	
	
} else  if (informacion.equals("catalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	catalogo.setOrden("ic_moneda");
	infoRegresar = catalogo.getJSONElementos();	

} else  if (informacion.equals("catalogoPYME")  && !lstEPO.equals("") ) {

	CatalogoPYMEDescTasas catalogo = new CatalogoPYMEDescTasas();
	catalogo.setCampoClave("ccb.ic_pyme ");
	catalogo.setCampoDescripcion("cp.cg_razon_social");	
	catalogo.setClaveEPO(lstEPO);	
	catalogo.setClaveIF(ic_if);
	catalogo.setOrden("cp.cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();	
	
} else  if (informacion.equals("Consulta") ) {

	List datosConsulta = new ArrayList();
	
	datosConsulta  = beanTasas.getTasaIFDesc (lstEPO,  ic_if, ic_moneda, lstPYME, lstTipoTasa, aplicaFloating);  

	if(datosConsulta.size()>0) {
		consulta = datosConsulta.get(0).toString();
		lsEposEnc = datosConsulta.get(1).toString();
	}	
	
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("lsEposEnc",lsEposEnc);	
	infoRegresar = jsonObj.toString();

	} else  if (informacion.equals("ConsultaHistorico") ) {

		
	consulta = beanTasas.getTasaIFDescHis ( lsEposEnc , lstEPO);
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	

} else  if (informacion.equals("ConsultaDetalle") ) {	

	String fecha = (request.getParameter("fecha") != null) ? request.getParameter("fecha") : "";
	String ic_epo = (request.getParameter("ic_epo") != null) ? request.getParameter("ic_epo") : "";
	String nombreEPO = (request.getParameter("nombreEPO") != null) ? request.getParameter("nombreEPO") : "";
	int numRegistrosIf =0, i=0;
	Vector vregistros = new Vector();
	String tasa_aplicar  ="", relmat_tasa = "", puntos_tasa = "", valor_tasa = "";
	
	
	Vector TasasAutoIfVec = beanTasas.consultaTasaAutoIf(ic_epo,fecha,iNoCliente);
	numRegistrosIf = TasasAutoIfVec.size();
	if(numRegistrosIf > 0){
		for(i=0;i<numRegistrosIf;i++){
			vregistros.add(TasasAutoIfVec.get(i));			
		}
	}
	
	for(i=0;i<vregistros.size();i++){
		Vector registrosmn = (Vector) vregistros.get(i);
		relmat_tasa = registrosmn.get(9).toString();
		puntos_tasa = registrosmn.get(10).toString();
		valor_tasa = registrosmn.get(12).toString();
		
		tasa_aplicar = "";
		if(relmat_tasa.equals("+")) {
			tasa_aplicar = (new BigDecimal(valor_tasa).add(new BigDecimal(puntos_tasa))).toPlainString();
		}
		if(relmat_tasa.equals("-")) {
			tasa_aplicar = (new BigDecimal(valor_tasa).subtract(new BigDecimal(puntos_tasa))).toPlainString();
		}
		if(relmat_tasa.equals("/")) {
			if (!puntos_tasa.equals("0")) {
				tasa_aplicar = (new BigDecimal(valor_tasa).divide(new BigDecimal(puntos_tasa),5)).toPlainString();
			}
		}
		if(relmat_tasa.equals("*")) {
			tasa_aplicar = (new BigDecimal(valor_tasa).multiply(new BigDecimal(puntos_tasa))).toPlainString();
		}
		
		System.out.println("registrosmn "+registrosmn);
		
		datos = new HashMap();		
		datos.put("INTERMEDIARIO", ((Vector)TasasAutoIfVec.get(0)).get(0).toString() ); 
		datos.put("MONEDA", registrosmn.get(6).toString() ); 
		datos.put("TASA_BASE", registrosmn.get(5).toString() ); 
		datos.put("PLAZO", registrosmn.get(11).toString() ); 
		datos.put("VALOR", registrosmn.get(12).toString() ); 
		datos.put("REL_MAT", registrosmn.get(9).toString() ); 
		datos.put("PUNTOS", registrosmn.get(10).toString() ); 
		datos.put("TASA_APLICAR", tasa_aplicar ); 
		registros.add(datos);		
							
	}
		
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("nombreEPO",nombreEPO);	
	infoRegresar = jsonObj.toString();						

} else  if (informacion.equals("operaSuceptibleFloating") ) {

	String opeSucepFloating  =  beanTasas.suceptibleFloating(lstPYME, "");

	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("opeSucepFloating",opeSucepFloating);	
	infoRegresar = jsonObj.toString();	
		
}


%>
<%=infoRegresar%>


