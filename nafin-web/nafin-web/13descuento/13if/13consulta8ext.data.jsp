<%@ page language="java" %>
<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*,
	netropology.utilerias.*,
	com.netro.afiliacion.*,
	com.netro.parametrosgrales.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"                                                                                                                                                                                                                                                                                                                                                                                                    
%>
<%@ include file="../13secsession.jspf"%>
<% 

/*** PARAMETROS QUE PROVIENEN DEL JS ***/
String informacion	= (request.getParameter("informacion") == null) ? "" : request.getParameter("informacion");
String parametroR    = (request.getParameter("parametro")==null) ? "" : request.getParameter("parametro");
String moneda 			= (request.getParameter("moneda") 		== null) ? "" : request.getParameter("moneda");
String cliente 		= (request.getParameter("cliente") 		== null) ? "" : request.getParameter("cliente");
String prestamo 		= (request.getParameter("prestamo") 	== null) ? "" : request.getParameter("prestamo");
String FechaCorte 	= (request.getParameter("FechaCorte") 	== null) ? "" : request.getParameter("FechaCorte");
String operacion 		= (request.getParameter("operacion") 	== null) ? "" : request.getParameter("operacion");
String tipoLinea 		= (request.getParameter("tipoLinea") 	== null) ? "" : request.getParameter("tipoLinea");
String ic_if 			= iNoCliente;
String infoRegresar	= null;
String cs_tipo="";
String guia="";
ResultSet rs_fc;
int start = 0, limit = 0;
int offset = 0;
ResultSet rs;
int TOTAL_CAMPOS=14;
int nRow = 0, numMon = 0, numDL = 0;
double totMntOperMon        = 0.0,    totMntOperDL       = 0.0;
double totMntPrimCuotMon  	= 0.0,    totMntPrimCuotDL 	 = 0.0;
double totNetoOtorgMon      = 0.0,    totNetoOtorgDL     = 0.0;
int nRegistrosQry = 1000;

PagosIFNB  beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
iNoCliente 		= (request.getParameter("ic_if") 	== null) ? iNoCliente : request.getParameter("ic_if");
if(informacion.equals("Consultar")){	
   CQueryHelperRegExtJS queryHelperRegExtJS;
   OperIfDE paginador;
   
   String qryStr = "select count(1) from COM_OPERADO O, COMCAT_MONEDA M where   O.ic_moneda = M.ic_moneda and O.ic_if = 11";
   
   AccesoDB con = new AccesoDB();
	try{
		con.conexionDB();
         rs = con.queryDB(qryStr);         
         List reg = new ArrayList();
         HashMap datos;
         if (rs!=null && rs.next()) {
            nRegistrosQry = Integer.parseInt(rs.getString(1));
         }        
   	}catch(Exception e) { 
         e.printStackTrace();
         throw new RuntimeException(e);
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
   
   
      paginador               = new OperIfDE();
      paginador.setMoneda     (moneda);
      paginador.setCliente    (cliente);
      paginador.setPrestamo   (prestamo);
      paginador.setFechaCorte (FechaCorte);
      paginador.setIc_if      (iNoCliente);
      queryHelperRegExtJS	   = new CQueryHelperRegExtJS(paginador); 
      if(nRegistrosQry>1000){
         queryHelperRegExtJS.setMaximoNumeroRegistros(nRegistrosQry);
      }
      
	if((informacion.equals("Consultar") && operacion.equals("Generar")) || (informacion.equals("Consultar") && operacion.equals(""))){
      
      try {
         start = Integer.parseInt(request.getParameter("start"));
         limit = Integer.parseInt(request.getParameter("limit"));				
      } catch(Exception e) {
         throw new AppException("Error en los parametros recibidos", e);
      }
   
		try {
			if(operacion.equals("Generar") || operacion.equals("Totales")){ //Nueva consulta
				queryHelperRegExtJS.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			String cadena = queryHelperRegExtJS.getJSONPageResultSet(request,start,limit);	
			
			if(queryHelperRegExtJS.getIdsSize()>0) {			
				//se guardara en bitacora 
				//beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,iNoCliente,"","O");	
				if(!"LINEA CREDITO".equals(strPerfil)){
					if(!"C".equals(tipoLinea)){
				beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,iNoCliente,"","O");			
					}else{
						beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,ic_if,"","O", "", "C");
					}
				}else{
						beanPagos.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,FechaCorte,moneda,cliente,prestamo,"","","O", ic_if, "C");
				}
			}
			
			infoRegresar	= cadena;
			
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}else if(operacion.equals("PDF")){
		try{ 
        paginador.setVecTotales(queryHelperRegExtJS.getResultCount(request));
        paginador.setParametro(parametroR);
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				String nombreArchivo = queryHelperRegExtJS.getCreatePageCustomFile(request, start, limit,strDirectorioTemp, "PDF");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
				infoRegresar = jsonObj.toString();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF", e);
			}
	}else if(operacion.equals("GeneraTXTs")){
		con = new AccesoDB();
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo = null;
       //String parametro = "";
       String paramNumDocto = "";//FODEA 015 - 2009 ACF    
		try {
			con.conexionDB();
			CQueryHelper queryHelperJS = new CQueryHelper(new OperIfDE());		
      if (parametroR.equals("S")) {			
      	contenidoArchivo.append("Status              ;FechaOperacion      ;CodMoneda           ;DescMoneda          ;TipoCambio          ;Intermediario       ;NomIntermediario    ;Calificacion        ;BaseOperacion       ;DescBaseOp          ;Cliente             ;NombreCliente       ;SucCliente          ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Disposicion         ;Estado              ;Municipio           ;SubAplicacion       ;SucTram             ;FechaPriPagCap      ;Tasa                ;RelMat              ;Spread              ;Margen              ;ActEco              ;Anio/Mod            ;Estrato             ;Afianz              ;FrecCap             ;FrecInt             ;NumCuotas           ;TipoAmort           ;ModPago             ;CentroFin           ;SucInter            ;MontoOper           ;MontoPrimCuot       ;MontoUltCuota       ;MontoRecal          ;ComisionPorGar      ;ComNoDisp           ;IntCobAnt           ;TipoGar             ;NetoOtorgado        ;PrimaBruta          \n");
      } else {
        contenidoArchivo.append("FechaOperacion      ;Cliente             ;NombreCliente       ;Prestamo            ;NumElectronico      ;FechaPriPagCap      ;Tasa                ;Spread              ;Margen              ;MontoOper           ;MontoPrimCuot       ;NetoOtorgado          \n");
			}
         rs = queryHelperJS.getCreateFile(request,con);
			GenerarContenidoArchivos conten = new GenerarContenidoArchivos();
			String contArchivo = conten.llenarContenido(rs, "S", 3,"","");
			
			if(!archivo.make(contArchivo, strDirectorioTemp, ".txt"))
				System.out.print("<--!Error al generar el archivo-->");
			else
				nombreArchivo = archivo.nombre;
					System.out.println("\n\n\n" + strDirecVirtualTemp+nombreArchivo + "\n\n\n");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
		} catch(Exception e) {
			 out.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta() == true)
			con.cierraConexionDB();
		}
	}
	if(operacion.equals("Totales")){        
      try {
         start = Integer.parseInt((request.getParameter("start")));
         limit = Integer.parseInt((request.getParameter("limit")));				
      } catch(Exception e) {
         throw new AppException("Error en los parametros recibidos", e);
      }
      
      Registros regTotales= queryHelperRegExtJS.getPageResultSet(request, start, limit);
		List reg=new ArrayList();
		int mx = 0;
		int dl = 0;
      
      System.out.println("Start : " + start + " / Limit : " + limit);
      System.out.println("Start : " + start + " / Limit : " + limit);
      				
		while(regTotales.next()){
			Vector vecDatos = new Vector();
         
      System.err.println("registros.getString(13) " + regTotales.getString(13));
		 // if(cont>=start && cont<(limit + start)){
        if(regTotales.getString("ic_moneda").equals("54")){
          numDL++;
          totMntOperDL      += Double.valueOf(regTotales.getString(10)).doubleValue();
          totMntPrimCuotDL  += Double.valueOf(regTotales.getString(11)).doubleValue();
          totNetoOtorgDL    += Double.valueOf(regTotales.getString(12)).doubleValue();
        }else{
          numMon++;
          totMntOperMon     += Double.valueOf(regTotales.getString(10)).doubleValue();
          totMntPrimCuotMon += Double.valueOf(regTotales.getString(11)).doubleValue();
          totNetoOtorgMon   += Double.valueOf(regTotales.getString(12)).doubleValue();
        }
      //} cont++;
		}
			
         System.err.println("numDL " + numDL);
         System.err.println("numMon " + numMon);
         
			HashMap datos;	
			if(numMon>0){
				datos = new HashMap();
				datos.put("MONEDA",        "Total Parcial Moneda Nacional");
				datos.put("N_REGISTROS",   "");
				datos.put("MONTO_OPERADO", "" + totMntOperMon);
				datos.put("SALDO_INSOLUTO","" + totMntPrimCuotMon);
				datos.put("CAPITAL_VIG",   "" + totNetoOtorgMon);
				reg.add(datos);
			}		
			if(numDL>0){
				datos = new HashMap();
				datos.put("MONEDA",        "Total Parcial Moneda Dolares");
				datos.put("N_REGISTROS",   "");
				datos.put("MONTO_OPERADO", "" + totMntOperDL);
				datos.put("SALDO_INSOLUTO","" + totMntPrimCuotDL);
				datos.put("CAPITAL_VIG",   "" + totNetoOtorgDL);
				reg.add(datos);
			}
			
		Registros vecTotales = queryHelperRegExtJS.getResultCount(request);
		if(vecTotales.getNumeroRegistros()>0){
			int j = 0;
			List vecColumnas = null;
			for(j=0;j<vecTotales.getNumeroRegistros();j++){
				vecColumnas = vecTotales.getData(j);
				
				datos = new HashMap();
				datos.put("MONEDA","Total " + vecColumnas.get(4).toString());
				datos.put("N_REGISTROS", vecColumnas.get(0).toString());
				datos.put("MONTO_OPERADO","" + vecColumnas.get(1).toString());
				datos.put("SALDO_INSOLUTO","" + vecColumnas.get(2).toString());
				datos.put("CAPITAL_VIG","" + vecColumnas.get(3).toString());
				reg.add(datos);
			}
		}
			
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar=jsonObj.toString();
	}else if(operacion.equals("GeneraTXT")){
      String parametro = (request.getParameter("parametro") == null) ? "S" : request.getParameter("parametro");
		con = new AccesoDB();
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		String nombreArchivo = null;
		
		try {
			CQueryHelper queryHelper = new CQueryHelper(new OperIfDE());	
			con.conexionDB();		
			if (parametro.equals("S")) {			
           contenidoArchivo.append("Status              ;FechaOperacion      ;CodMoneda           ;DescMoneda          ;TipoCambio          ;Intermediario       ;NomIntermediario    ;Calificacion        ;BaseOperacion       ;DescBaseOp          ;Cliente             ;NombreCliente       ;SucCliente          ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Disposicion         ;Estado              ;Municipio           ;SubAplicacion       ;SucTram             ;FechaPriPagCap      ;Tasa                ;RelMat              ;Spread              ;Margen              ;ActEco              ;Anio/Mod            ;Estrato             ;Afianz              ;FrecCap             ;FrecInt             ;NumCuotas           ;TipoAmort           ;ModPago             ;CentroFin           ;SucInter            ;MontoOper           ;MontoPrimCuot       ;MontoUltCuota       ;MontoRecal          ;ComisionPorGar      ;ComNoDisp           ;IntCobAnt           ;TipoGar             ;NetoOtorgado        ;PrimaBruta          \n");
          } else {
           contenidoArchivo.append("FechaOperacion      ;Cliente             ;NombreCliente       ;Prestamo            ;NumElectronico      ;FechaPriPagCap      ;Tasa                ;Spread              ;Margen              ;MontoOper           ;MontoPrimCuot       ;NetoOtorgado          \n");
         } 
         rs = queryHelper.getCreateFile(request,con);
		
			GenerarContenidoArchivos conten = new GenerarContenidoArchivos();
			String contArchivo = conten.llenarContenido(rs, "S", 2,"","");
         
         System.err.println(parametro);
         System.err.println(parametro);
         System.err.println(parametro);
         System.err.println(parametro);
         System.err.println(parametro);
         System.err.println(parametro);
         System.err.println(parametro);
         			
			if(!archivo.make(contArchivo, strDirectorioTemp, ".txt"))
				System.out.print("<--!Error al generar el archivo-->");
			else
				nombreArchivo = archivo.nombre;
		} catch(Exception e) {
			 out.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta() == true)
			con.cierraConexionDB();
		}
					
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo",strDirecVirtualTemp + nombreArchivo);
			infoRegresar = jsonObj.toString();
	}
}

else if(informacion.equals("DescargaZip")){
	//String pathFileZip = "/nafin/16archivos/descuento/procesos/edocuenta/archedocuenta_"+FechaCorte.replace('/','_') + "_" + iNoCliente + "_S.zip";		
  String nombreZip = "archoper_"+FechaCorte.replace('/','_') + "_" + iNoCliente + "_S.zip";
	//beanPagos.consultaArchivosZip(nombreArchivo, strDirectorioTemp);
  
 // if("C".equals(tipoLinea)){
    OperIfDE paginador = new OperIfDE();	
    paginador.setIc_if(ic_if);
    paginador.setMoneda(moneda);
    paginador.setCliente(cliente);
    paginador.setPrestamo(prestamo);
    paginador.setFechaCorte(FechaCorte);
    paginador.setTipoLinea(tipoLinea);
    paginador.setParametro("S");
    
    CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
    String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "ZIP");
    GenerarArchivoZip zip = new GenerarArchivoZip();
    //String nombreZip = "archoper_"+FechaCorte+"_"+iNoCliente+"_S.zip";
    
    zip.generandoZip(strDirectorioTemp,strDirectorioTemp,nombreArchivo.replaceAll(".txt",""));
   /* beanPagos.guardarArchivosZip(nombreZip, strDirectorioTemp);
  }else{
    beanPagos.consultaArchivosZip(nombreZip, strDirectorioTemp);
  }*/
  
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	//jsonObj.put("urlArchivo",pathFileZip);
  jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreZip);
	infoRegresar = jsonObj.toString();
}
 
else if(informacion.equals("CatalogoMoneda")){   
   AccesoDB con = new AccesoDB();
	try{
		con.conexionDB();
      String qrySentencia = "select distinct M.ic_moneda, "+
					"M.cd_nombre" +
					" from COMCAT_MONEDA M, COMCAT_TASA T "+
					" where M.ic_moneda = T.ic_moneda "+
					" and T.cs_disponible = 'S' and M.ic_moneda = 1 "+
					" union "+
					" select distinct M.ic_moneda, M.cd_nombre" +
					" from COMCAT_MONEDA M, COMCAT_TASA T, COM_TIPO_CAMBIO TC "+
					" where M.ic_moneda = T.ic_moneda AND T.cs_disponible = 'S' "+
					" and M.ic_moneda = TC.ic_moneda order by 2";
         rs = con.queryDB(qrySentencia);
         
         List reg = new ArrayList();
         HashMap datos;
         while (rs.next()) {
            datos = new HashMap();
            datos.put("clave",rs.getString(1));
            datos.put("descripcion",rs.getString(2));
            reg.add(datos);
         }
         JSONObject jsonObj = new JSONObject();
         jsonObj.put("success", new Boolean(true));
         jsonObj.put("registros", reg);
         infoRegresar = jsonObj.toString();
         rs.close();         
   	}catch(Exception e) {
		System.err.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
}

else if(informacion.equals("CatalogoFechaCorte")){
	AccesoDB con = new AccesoDB();
	try{
		con.conexionDB();
		
		cs_tipo                 =  beanPagos.getTipoIF(iNoCliente);
      
		CQueryHelper queryHelper = new CQueryHelper(new OperIfDE());
		queryHelper.cleanSession(request);
		
		String qryString= "";
		if(!"C".equals(tipoLinea)){
			qryString= " Select distinct TO_CHAR (df_fechaoperacion,'dd/mm/yyyy'),df_fechaoperacion "+
 				" From com_operado"+
 				" where ic_if ="+iNoCliente+
				" order by df_fechaoperacion";
		}else{
			qryString= " Select distinct TO_CHAR (df_fechaoperacion,'dd/mm/yyyy'),df_fechaoperacion "+
					" From com_operado"+
					" where ic_if = 12"+
					" and ig_cliente = "+cliente+
					" order by df_fechaoperacion";
		}
			
		rs = con.queryDB(qryString);
		List reg = new ArrayList();
		HashMap datos;
		while (rs.next()) {
			datos = new HashMap();
			datos.put("clave",rs.getString(1));
			datos.put("descripcion",rs.getString(1));
			reg.add(datos);
		}
      con.cierraStatement();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("registros", reg);
		infoRegresar = jsonObj.toString();
		rs.close();
	}catch(Exception e) {
		System.err.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
}
else if(informacion.equals("ObtieneParametro")){
	AccesoDB con = new AccesoDB();
	String parametro = "";
	String paramNumDocto = "";//FODEA 015 - 2009 ACF
	String Query="";
	String Query2="";
	String Query3="";
	//ResultSet rs_fc;
	ResultSet rs_par;
	ResultSet rs_par1;
	try{
		con.conexionDB();
      String Query1= 
         " Select cs_layout_completo_oper "+
         " From com_param_x_if"+
         " Where ic_if ="+iNoCliente;
      rs_par = con.queryDB(Query1);
      if(rs_par.next()){
         parametro=rs_par.getString(1);
      }else{
         Query2= 
            " Select cs_layout_completo_oper "+
            " From com_param_gral"+
            " Where ic_param_gral = 1 ";
         rs_par1 = con.queryDB(Query2);
         if(rs_par1.next()){ 
            parametro=rs_par1.getString(1);
         }else{
            parametro="S";
         }
      }
	} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
   }finally{
      if(con.hayConexionAbierta()){ 
         con.cierraConexionDB();	
      }
   }

	JSONObject jsonObj = new JSONObject();
  
  if(!"LINEA CREDITO".equals(strPerfil)){
    HashMap hmDataIf = beanPagos.getSiracFinaciera(iNoCliente);
    String numSirac = (hmDataIf.get("NUMERO_SIRAC")==null || ((String)hmDataIf.get("NUMERO_SIRAC")).equals(""))?"0":(String)hmDataIf.get("NUMERO_SIRAC");
    jsonObj.put("IC_FINANCIERA", hmDataIf.get("IC_FINANCIERA"));
    jsonObj.put("NUMERO_SIRAC", numSirac);
  }else{
    String numSirac = beanPagos.getSiracLineaCredito(iNoCliente);
    jsonObj.put("IC_FINANCIERA","");
    jsonObj.put("NUMERO_SIRAC", ("".equals(numSirac)?"0":numSirac));
  }

	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("parametro", parametro);
	infoRegresar=jsonObj.toString();
}

%>
<%= infoRegresar%>