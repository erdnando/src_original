	Ext.onReady(function() {

var ic_if            =  Ext.getDom('ic_if').value;
var globalVariables  = new Object();
var iniciaPrograma   = new Object();

globalVariables.topLabel = '<div class="box" style="text-align: justify;">' +
        'Las operaciones que aparecen en esta p�gina fueron notificadas a la EMPRESA DE PRIMER ORDEN de conformidad y ' +
        'para efectos de los art�culos 427 de la Ley General de T�tulos y Operaciones de Cr�dito, 32 C del C�digo Fiscal ' +
        'de la Federac�on y 2038 del C�digo Civil Federal seg�n corresponda, para los efectos legales conducentes.<br/><br/>' +
        'Por otra parte, a partir del 17 de octubre de 2018, el PROVEEDOR ha manifestado bajo protesta de decir verdad, que s&iacute; ha emitido o emitir&aacute; a la '+
        'EMPRESA DE PRIMER ORDEN el CFDI por la operaci&oacute;n comercial que le dio origen a esta transacci&oacute;n, seg&uacute;n sea el caso y conforme establezcan las disposiciones fiscales vigentes.'+
        '</div>';

   iniciaPrograma.successAjaxFn = function(response, request){
      var jsonData                           = Ext.util.JSON.decode(response.responseText);
      globalVariables.bTipoFactoraje         = jsonData.bTipoFactoraje;
      globalVariables.bOperaFactorajeVencido = jsonData.bOperaFactorajeVencido;
      globalVariables.bOperaFactorajeDist    = jsonData.bOperaFactorajeDist;
		globalVariables.limitarRangoFechas    = jsonData.limitarRangoFechas;
		
      Ext.getBody().unmask();
      
      if(globalVariables.bTipoFactoraje=='true'){
         Ext.getCmp('idTipoFactoraje').show();
      }else{
         Ext.getCmp('idTipoFactoraje').hide();
      }
      
   };
   
   iniciaPrograma.iniciaPrograma = function(){ // Obtiene bTipoFactoraje
      Ext.getBody().mask('Iniciando Pantalla, por favor espere...');
      Ext.Ajax.request({
         url:     '13consulta5ext.data.jsp',
         method:  'POST',
         success: iniciaPrograma.successAjaxFn,
         params:  { informacion: 'IniciaPrograma' }
      });
   };
   
   iniciaPrograma.iniciaPrograma = function(ic_epo){ // Obtiene bTipoFactoraje
      Ext.getBody().mask('Cargando Pantalla, por favor espere...');
      Ext.Ajax.request({
         url:     '13consulta5ext.data.jsp',
         method:  'POST',
         success: iniciaPrograma.successAjaxFn,
         params:  { informacion: 'IniciaPrograma', ic_epo : ic_epo }
      });
   };
   //----------------------- CALCULA FECHA -------------------------------------
   function getDiferenciaEnDias(fechaIni, fechaFin){         
      var string1 			= "";
      var temp 				= "";
      var diferenciaEnDias	= "";

      string1 = fechaIni;         
      string = "" + string1;
      splitstring = string.split("/");
      var fechaInicial= new Date();
      fechaInicial.setDate(splitstring[0]);
      fechaInicial.setMonth(splitstring[1]-1);
      fechaInicial.setYear(splitstring[2]);
      string1 = fechaFin;

      string = "" + string1;
      splitstring = string.split("/");
      var fechaFinal= new Date();
      fechaFinal.setDate(splitstring[0]);
      fechaFinal.setMonth(splitstring[1]-1);
      fechaFinal.setYear(splitstring[2]);
      //Calcular el numero de milisegundos de un dia
      var milisegundosPorDia=1000*60*60*24;         
      //Calculate difference btw the two dates, and convert to days
      diferenciaEnDias = Math.ceil((fechaFinal.getTime()-fechaInicial.getTime())/(milisegundosPorDia));
      return diferenciaEnDias;
   }
   
   // Oculta Grid's y limpia Store's
   var limpiaGirds = function(){
      var grid          =  Ext.getCmp('grid');
      var gridTotales   =  Ext.getCmp('gridTotales');
      
      if(grid.isVisible){
         grid.hide();
         consultaDataGrid.removeAll();
      }
      if(gridTotales.isVisible){
         gridTotales.hide();
         resumenTotalesData.removeAll();
      }
      
   }
   //---------------------------------------------------------------------------               

	var busqAvanzadaSelect=0;
	Todas = Ext.data.Record.create([ 
		{name: "clave", type: "string"}, 
		{name: "descripcion", type: "string"}, 
		{name: "loadMsg", type: "string"}
	]);
	//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
	var procesarCatalogoEPO = function(store, arrRegistros, opts) {
		//Ext.getCmp("noBancoFondeo").setValue((store.getRange()[0].data).clave);	
	}
	var procesarCatalogoNombreIF = function(store, arrRegistros, opts) {
		Ext.getCmp("noIc_if").setValue(ic_if);		
	}

	function procesarArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var fp = Ext.getCmp('forma');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
/*
	function procesarArchivosPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var fp = Ext.getCmp('forma');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}
*/
	var procesarArchivosPDF = function(opts, success, response){
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		var fp = Ext.getCmp('forma');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			if(resp.estatusArchivo == 'FINAL'){
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
				btnGenerarPDF.setText('Generar Todo');
				btnGenerarPDF.enable();
				btnGenerarPDF.setIconClass('icoPdf');
			} else if(resp.estatusArchivo == 'ERROR'){
				Ext.Msg.alert('Error','No se gener� el archivo',function(btn){
					btnGenerarPDF.setText('Generar Todo');
					btnGenerarPDF.enable();
					btnGenerarPDF.setIconClass('icoPdf')
				});
			} else{
				btnGenerarPDF.setIconClass('loading-indicator');
				btnGenerarPDF.setText(resp.porcentaje);
				var cmpForma = Ext.getCmp('forma');
				var fecha2 = new Date();
				var fechaActual = fecha2.getDate() + '/' + parseInt(fecha2.getMonth() + 1) + '/' + fecha2.getFullYear();
				paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
				Ext.Ajax.request({
					url: '13consulta5ext.data.jsp',
					params: Ext.apply(paramSubmit,{
						informacion:            'IMPRIME_PDF',//PDF
						bOperaFactorajeVencido: globalVariables.bOperaFactorajeVencido,
						bOperaFactorajeDist:    globalVariables.bOperaFactorajeDist,
						ccAcuse3:               '',
						envia:                  'S',
						tipo_impresion:         'T',
						paginaNo:               'T',
						FechaActual:            fechaActual,
						estatusArchivo:         'PROCESANDO'
					}),
					callback:procesarArchivosPDF
				});
			}
		} else{
			btnGenerarPDF.setText('Generar Todo');
			btnGenerarPDF.enable();
			btnGenerarPDF.setIconClass('icoPdf')
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			
			var gridTotales = Ext.getCmp('gridTotales');
	
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				gridTotales.show();
				
				var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");  // Id barra paginacion
				var cmpForma = Ext.getCmp('forma');
				paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};		
				resumenTotalesData.load({
                        params   :  Ext.apply(paramSubmit,{
                           informacion :  'ResumenTotales',
                           start       :  cmpBarraPaginacion.cursor,
                           limit       :  cmpBarraPaginacion.pageSize
                        })             
            });
				
				Ext.getCmp('btnGenerarArchivo').enable();				
				Ext.getCmp('btnGenerarPDF').enable();				
				//Ext.getCmp('btnImprimirPDF').enable();
				Ext.getCmp('btnImprimirNotaC').enable();
				el.unmask();
            
            if(globalVariables.bTipoFactoraje!='true'){
               grid.getColumnModel().setHidden(8, true); // Oculta Tipo Factoraje
            }else{
               grid.getColumnModel().setHidden(8, false); // Muestra Tipo Factoraje
            }            
			} else {			
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnGenerarPDF').disable();
				//Ext.getCmp('btnImprimirPDF').disable();
				Ext.getCmp('btnImprimirNotaC').disable();
				gridTotales.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
   
	var procesarConsultaDataTotales = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}
	
			var el = gridTotales.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
			} else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('hid_nombre').setValue(jsonObj.pyme);
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);			
		}
	}
	
	var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta5ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NUEVAEPO'},
			{name: 'FECHA_NOT'},
			{name: 'CC_ACUSE'},
			{name: 'NOMPYME'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'CD_NOMBRE'},
			{name: 'FN_MONTO'},
			{name: 'FN_PORC_ANTICIPO'},
			{name: 'FN_MONTO_DSCTO'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'REFERENCIA_PYME'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	var resumenTotalesData = new Ext.data.JsonStore({		
		root : 'registros',
		url : '13consulta5ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'CD_NOMBRE'},
			{name: 'RESULT_SIZE'},
			{name: 'FN_MONTO'},
			{name: 'FN_MONTO_DSCTO'}			
		],
		totalProperty : 'total', 
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataTotales
		}		
	});
	var catalogoEPO = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '13consulta5ext.data.jsp',
		listeners: {
			load: procesarCatalogoEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: true
	});
	
	var catalogoTipoFactoraje = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion','msg'],
		url : '13consulta5ext.data.jsp',
		listeners: {
			load: procesarCatalogoEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoTipoFactoraje'
		},
		totalProperty : 'total',
		autoLoad: true
	});
	
	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion','ic_pyme','loadMsg'],
		url : '13consulta5ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	var gridTotales = new Ext.grid.EditorGridPanel({
		id: 'gridTotales',
		title: 'Totales',
		hidden: true,
		align: 'center',
		store: resumenTotalesData,
      loadMask: true,
		columns: [	
			{
				header: 'Tipo Moneda',
				dataIndex: 'CD_NOMBRE',
				width: 155,
				align: 'left',
				renderer: function(value){
					return 'Total ' + value;
				}
			},
			{
				header: 'Total de Documentos',
				dataIndex: 'RESULT_SIZE',
				width: 140,
				align: 'center'						
			},
			{
				header: 'Monto',
				dataIndex: 'FN_MONTO',
				width: 170,
				align: 'right',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					 return Ext.util.Format.usMoney(record.get('FN_MONTO'));
				}							
			},
			{
				header: 'Monto Descuento',
				dataIndex: 'FN_MONTO_DSCTO',
				width: 140,
				align: 'right',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					 return Ext.util.Format.usMoney(record.get('FN_MONTO_DSCTO'));
				}	
			}			
		],
		height: 100,		
		width: 610,
		style: 'margin:0 auto',
		frame: false
	});
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [{
				header: 'EPO<br />', tooltip: 'EPO',
				dataIndex: 'NUEVAEPO',
				sortable: true,
				width: 200, resizable: true,
				align: 'center', renderer: function(val){ return '<span style="text-align:left">' + val + '</span>'; }
			},{
				header: 'Fecha<br />Notificaci�n', tooltip: 'Fecha de notificaci�n',
				dataIndex: 'FECHA_NOT',
				sortable: true,	align: 'center',
				width: 75, resizable: true,
				align: 'center'
			},{
				header: 'Acuse<br />de notificaci�n', tooltip: 'Acuse de notificaci�n',
				dataIndex: 'CC_ACUSE',
				sortable: true,	align: 'center',
				width: 120, resizable: true,
				align: 'center'
			},{
				header: 'Nombre del proveedor/Cedente<br />', tooltip: 'Nombre del proveedor',
				dataIndex: 'NOMPYME',
				sortable: true,	align: 'center',
				width: 150, resizable: true,
				align: 'left'
			},{
				header: 'N�mero de Documento<br />', tooltip: 'N�mero de documento',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'left'
			},{
				header: 'Fecha<br />Emisi�n', tooltip: 'Fecha de emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,	align: 'center',
				width: 70, resizable: true,
				align: 'center'
			},{
				header: 'Fecha<br />Vencimiento', tooltip: 'Fecha de vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center'
			},{
				header: 'Moneda<br />', tooltip: 'Moneda',
				dataIndex: 'CD_NOMBRE',
				sortable: true,	align: 'center',
				width: 110, resizable: true,
				align: 'left'
			},{
				header: 'Tipo<br />Factoraje', tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,	align: 'center',
				width: 70, resizable: true,
				align: 'center'
			},{
				header: 'Monto<br />', tooltip: 'Monto',
				dataIndex: 'FN_MONTO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'right',
				renderer : 'usMoney'
			},{
				header: '%<br />Descuento', tooltip: 'Porcentaje de Descuento',
				dataIndex: 'FN_PORC_ANTICIPO',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center',
				renderer : function(value){ return value + ' %' }
			},{
				header: 'Recurso<br />Garantia', tooltip: 'Recurso en Garantia',
				dataIndex: 'FN_MONTO',
				sortable: true,	align: 'center',
				width: 100, resizable: true,
				align: 'right',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
					return Ext.util.Format.usMoney(record.get('FN_MONTO') - record.get('FN_MONTO_DSCTO'));
				}
			},{
				header: 'Monto<br />Descuento', tooltip: 'Monto Descuento',
				dataIndex: 'FN_MONTO_DSCTO',
				sortable: true,	align: 'center',
				width: 105, resizable: true,
				align: 'right',
				renderer: 'usMoney'
			},{
				header: 'Referencia', tooltip: 'Referencia Pyme', //FODEA 17
				dataIndex: 'REFERENCIA_PYME',
				sortable: true,	align: 'left',
				width: 150, resizable: true
			}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 926,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: false,
		bbar: {
		xtype: 'paging',
			id: 'barraPaginacion',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {					
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta5ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'XLS',
								informacion: 'Consulta',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize,
                        tipo: ''
							}),							
							callback:procesarArchivos
						});
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Detalle Nota de Cr�dito',
					id: 'btnImprimirNotaC',
					iconCls: 'icoXls',
					disabled : true,
					handler: function(boton, evento) {						
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var cmpForma = Ext.getCmp('forma');
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta5ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'XLSNotaC',
								informacion: 'Consulta',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize,
                        tipo: 'D',
                        bTipoFactoraje : globalVariables.bTipoFactoraje
							}),
							callback:procesarArchivos
						});
					}
				},
				/*'-',
				{
					xtype: 'button',
					text: 'Por P�gina PDF',
					id: 'btnImprimirPDF',
					iconCls: 'icoPdf',
					disabled : true,
					handler: function(boton, evento) {
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var cmpForma = Ext.getCmp('forma');
						var paginaNo = (parseInt(cmpBarraPaginacion.cursor)/15)+1; // Obtiene Numero de P�gina
                  var fecha2 = new Date();
                  var fechaActual = fecha2.getDate() + '/' + parseInt(fecha2.getMonth() + 1) + '/' + fecha2.getFullYear();
                  paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta5ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								informacion    :  'PDFPrint',
								operacion		:	'Generar',
								start          :  cmpBarraPaginacion.cursor,
								limit          :  cmpBarraPaginacion.pageSize,
                        bOperaFactorajeVencido	: 	globalVariables.bOperaFactorajeVencido,
                        bOperaFactorajeDist		:  globalVariables.bOperaFactorajeDist,
                        paginaNo       :  paginaNo,
                        tipo           :  '',
                        bTipoFactoraje :  globalVariables.bTipoFactoraje,
                        ccAcuse3       :  '',
                        envia          :  'S',
                        tipo_impresion :  paginaNo,
                        FechaActual    :  fechaActual,
                        ic_epo         :  globalVariables.ic_epo,
                        df_fecha_notMin  : Ext.util.Format.date(Ext.getCmp("df_fecha_notMin").getValue(),'d/m/Y'),
								df_fecha_notMax  : Ext.util.Format.date(Ext.getCmp("df_fecha_notMax").getValue(),'d/m/Y'),
								df_fecha_vencMin : Ext.util.Format.date(Ext.getCmp("df_fecha_vencMin").getValue(),'d/m/Y'),
								df_fecha_vencMax : Ext.util.Format.date(Ext.getCmp("df_fecha_vencMax").getValue(),'d/m/Y')
							}),
							callback:procesarArchivos							
						});
					}
				},*/
				'-',
				{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnGenerarPDF',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						//var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						var fecha2 = new Date();
						var fechaActual = fecha2.getDate() + '/' + parseInt(fecha2.getMonth() + 1) + '/' + fecha2.getFullYear();
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13consulta5ext.data.jsp',
							params: Ext.apply(paramSubmit,{
								informacion:            'IMPRIME_PDF',//PDF
								//operacion:              'Generar',
								bOperaFactorajeVencido: globalVariables.bOperaFactorajeVencido,
								bOperaFactorajeDist:    globalVariables.bOperaFactorajeDist,
								ccAcuse3:               '',
								//start:                  cmpBarraPaginacion.cursor,
								//limit:                  cmpBarraPaginacion.pageSize,
								envia:                  'S',
								tipo_impresion:         'T',
								paginaNo:               'T',
								FechaActual:            fechaActual,
								estatusArchivo:         'INICIO'
							}),
							callback:procesarArchivosPDF
						});
					}
				}
			]
		}
	};
   
	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'idIc_epo',
			hiddenName : 'ic_epo',
			fieldLabel: 'Nombre de la EPO',
			emptyText: 'Seleccione EPO',
			store: catalogoEPO,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			width:300,
         listeners:   {
            select : {
               fn: function(combo, record){  
                  globalVariables.ic_epo = combo.getValue();
                  iniciaPrograma.iniciaPrograma(combo.getValue());
                  var cmpForma = Ext.getCmp('forma');
                  paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};					
                  catalogoTipoFactoraje.load({
                     params   :  Ext.apply(paramSubmit,{
                        informacion : 'catalogoTipoFactoraje',
                        ic_epo      :  globalVariables.ic_epo
                     }),
                     callback : limpiaGirds
                  })
               }
            }
         }
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de notificaci�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_notMin',
					id: 'df_fecha_notMin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_vencMin',
					margins: '0 20 0 0', 
					formBind: true,
					format: 'd/m/Y',             
               value:      new Date()
				},{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},{
					xtype: 'datefield',
					name: 'df_fecha_notMax',
					id: 'df_fecha_notMax',
					startDay: 0,					
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					margins: '0 20 0 0',					
					format: 'd/m/Y',
					value:new Date()
				}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_vencMin',
					id: 'df_fecha_vencMin',
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_vencMin',
					margins: '0 20 0 0', 
					formBind: true,
					format: 'd/m/Y'					
				},
				
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},{
					xtype: 'datefield',
					name: 'df_fecha_vencMax',
					id: 'df_fecha_vencMax',
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_fecha_vencMax',
					margins: '0 20 0 0',
					format: 'd/m/Y'					
				}
			]
		},{
			xtype: 'compositefield',
			items:[{
				xtype: 'textfield',
				fieldLabel: 'Acuse de notificaci�n',
				name: 'cc_acuse3',
				id:'idCc_acuse3',
				width:250
			}]
		},{
         xtype       :  'combo',
         name        :  'tipoFactoraje',
         id          :  'idTipoFactoraje',
         hiddenName  :  'tipoFactoraje',
         fieldLabel  :  'Tipo Factoraje',
         emptyText   :  'Seleccione',
         store       :  catalogoTipoFactoraje,
         mode        :  'local',
         displayField:  'descripcion',
         valueField  :  'clave',
         forceSelection : true,
         hidden      :  true,
         triggerAction : 'all',
         typeAhead   :  true,
         minChars    :  1,
         margins     :  '0 20 0 0',
         anchor      :  '62%'
		}
	];
   
   var topLabel = {
      xtype:         'fieldset',
      title:         '&nbsp;',
      collapsible:   true,
      columnWidth:   0.5,
      anchor:        '100%',
      items:   [{
         xtype:      'label',
         html:       globalVariables.topLabel,
         cls:        'x-top-label',
         iconCls:    'info'
      }]
   };
   
	//Forma para hacer la busqueda filtrada
	var fp = {
		xtype: 'form',
		id: 'forma',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 600,
		titleCollapse: false,
		title: 'Avisos de Notificaci�n',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
      monitorValid:true,
		items:[topLabel,elementosForma],
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(boton, evento) {
               
					var df_fecha_notMin  = Ext.getCmp("df_fecha_notMin");
					var df_fecha_notMax  = Ext.getCmp("df_fecha_notMax");
					var df_fecha_vencMin = Ext.getCmp("df_fecha_vencMin");
					var df_fecha_vencMax = Ext.getCmp("df_fecha_vencMax");	
               var grid             = Ext.getCmp("grid");
					var message          = '';
               
					if(df_fecha_notMin.getValue()=='' && df_fecha_notMax.getValue()==''  && df_fecha_vencMin.getValue()=='' && df_fecha_vencMax.getValue()=='' ){
                  Ext.Msg.show({
							title    :  'Mensaje',
							msg      :  'Debe de capturar al menos un rango de fechas',
							icon     :  Ext.Msg.INFO,
							buttons  :  Ext.Msg.OK
						});
						return;
               }
					
               if((df_fecha_notMin.getValue()!='' && df_fecha_notMax.getValue()=='') || (df_fecha_notMin.getValue()=='' && df_fecha_notMax.getValue()!='')){
                  if((df_fecha_notMin.getValue()!='' && df_fecha_notMax.getValue()=='')){
                     df_fecha_notMax.markInvalid('Debe capturar la fecha de Notificaci�n final');
                  }
                  if(df_fecha_notMin.getValue()=='' && df_fecha_notMax.getValue()!=''){
                     df_fecha_notMin.markInvalid('Debe capturar la fecha de Notificaci�n inicial');
                  }
                  limpiaGirds();
                  return;
               }
					
					if( df_fecha_notMin.getValue()!='' && df_fecha_notMax.getValue()!=''   ){             		
						var Diferencia = Math.abs(getDiferenciaEnDias(Ext.util.Format.date(df_fecha_notMin.getValue(),'d/m/Y'), Ext.util.Format.date(df_fecha_notMax.getValue(),'d/m/Y')));
						if(globalVariables.limitarRangoFechas=='true'){
							if(Diferencia>7){
								Ext.Msg.show({
									title    :  'Excedio el rango de fecha',
									msg      :  'El rango de fechas de notificacion no debe sobrepasar 7 dias',
									icon     :  Ext.Msg.INFO,
									buttons  :  Ext.Msg.OK
								});
								limpiaGirds();
								return;
							}               
						}
					}
					
					
					
					 if((df_fecha_vencMin.getValue()!='' && df_fecha_vencMax.getValue()=='') || (df_fecha_vencMin.getValue()=='' && df_fecha_vencMax.getValue()!='')){
                  if((df_fecha_vencMin.getValue()!='' && df_fecha_vencMax.getValue()=='')){
                     df_fecha_vencMax.markInvalid('Debe capturar la fecha de Vencimiento final');
                  }
                  if(df_fecha_vencMin.getValue()=='' && df_fecha_vencMax.getValue()!=''){
                     df_fecha_vencMin.markInvalid('Debe capturar la fecha de Vencimiento inicial--');
                  }
                  limpiaGirds();
                  return;
               }
				 
					var fechaIni = Ext.util.Format.date(df_fecha_vencMin.getValue(),'d/m/Y');
					var fechaFin = Ext.util.Format.date(df_fecha_vencMax.getValue(),'d/m/Y');
						
					if( isdate(fechaIni)==false ){
						df_fecha_vencMin.markInvalid("Error en la fecha:formato dd/mm/aaaa "); 
						df_fecha_vencMin.focus();
						return;
					}
					if( isdate(fechaFin)==false ){
						df_fecha_vencMax.markInvalid("Error en la fecha:formato dd/mm/aaaa "); 
						df_fecha_vencMax.focus();
						return;
					}					
					
					if(df_fecha_vencMin.getValue()!='' && df_fecha_vencMax.getValue()!='' ){
						var Diferencia = Math.abs(getDiferenciaEnDias(Ext.util.Format.date(df_fecha_vencMin.getValue(),'d/m/Y'), Ext.util.Format.date(df_fecha_vencMax.getValue(),'d/m/Y')));
						if(globalVariables.limitarRangoFechas=='true'){
							if(Diferencia>7){
								Ext.Msg.show({
									title    :  'Excedio el rango de fecha',
									msg      :  'El rango de fechas de Vencimiento no debe sobrepasar 7 dias',
									icon     :  Ext.Msg.INFO,
									buttons  :  Ext.Msg.OK
								});
								limpiaGirds();
								return;
							}  
						}
					}
           
             //  grid.show();
            
               var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");  // Id barra paginacion
					var cmpForma = Ext.getCmp('forma');
					var fp = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};					
				
					consultaDataGrid.load({
						params   :  Ext.apply(paramSubmit,{
                     operacion   :  'Generar',
                     start       :  cmpBarraPaginacion.cursor,
                     limit       :  cmpBarraPaginacion.pageSize						
                  })/*,
                  callback :  function(){
                     resumenTotalesData.load({
                        params   :  Ext.apply(paramSubmit,{
                           informacion :  'ResumenTotales',
                           start       :  cmpBarraPaginacion.cursor,
                           limit       :  cmpBarraPaginacion.pageSize
                        })             
                     });
                  }*/
               });
				//resumenTotalesData.load({params: Ext.apply(fp.getForm().getValues(),{})})
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta5ext.jsp';
				}
			}
		]
	};//FIN DE LA FORMA

	//----------------------------------PRINCIPAL-----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10),
			grid,
			NE.util.getEspaciador(10),
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});
   iniciaPrograma.iniciaPrograma();
});
