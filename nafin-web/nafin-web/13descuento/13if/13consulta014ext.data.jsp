<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	com.netro.mandatodoc.*,	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %> 

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String tipoTasa = (request.getParameter("tipo_tasa")!=null)?request.getParameter("tipo_tasa"):"";
String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
String ic_mandante = (request.getParameter("ic_mandante")!=null)?request.getParameter("ic_mandante"):"";
String ic_if =iNoCliente,  infoRegresar ="", consulta ="";

Vector lovDatos 	= null;
Vector lovRegistro 	= null;	
HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

String tasasBorrar = "", lsNombreEPO = "Tasas Preferenciales/Negociadas para Mandato",  lsNombrePYME = "", lsCalificacion = "", 
	lsPlazo = "",  lsCadenaTasa = "", ldValorTotalTasa = "", fecha = "", lsValorTasaN  = "", 	lsAprobEPO  = "",
	lsAprobIF  = "", lsAprobNafin  = "", lsMandante ="";
		
int numRegistros =0;		

AutorizacionTasas BeanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
		

if (informacion.equals("catalogoTipoTasa")  ) { 

	for(int i=0; i<3; i++){
		datos = new HashMap();
		if(i==0) {
			datos.put("clave", "P");
			datos.put("descripcion", "PREFERENCIAL");	
			registros.add(datos);
		}
		if(i==1) {
			datos.put("clave", "N");
			datos.put("descripcion", "NEGOCIADA");	
			registros.add(datos);
		}
		if(i==2) {
			datos.put("clave", "TODAS");
			datos.put("descripcion", "TODAS");	
			registros.add(datos);
		}
	}
		
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
}else  if (informacion.equals("catalogoEPO")  ) {
	
	CatalogoEPODescTasas cat = new CatalogoEPODescTasas();
	cat.setCampoClave("cie.ic_epo");
	cat.setCampoDescripcion("ce.cg_razon_social");
	cat.setClaveIF(ic_if);	
	cat.setAmbos("S");
	cat.setParametro("PUB_EPO_OPERA_MANDATO");
	cat.setOrden("ce.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("catalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);	
	infoRegresar = catalogo.getJSONElementos();	


}else if (informacion.equals("CatalogoMandante") && !ic_epo.equals("")) {

	CatalogoMandante catalogo = new CatalogoMandante();
	catalogo.setCampoClave("ic_mandante ");
	catalogo.setCampoDescripcion("cg_razon_social");	
	catalogo.setClaveEpo(ic_epo);	
	catalogo.setTipoAfiliado("M");
	catalogo.setOrden("cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();	


}else if (informacion.equals("catalogoPlazo") ) {

	CatalogoPlazos catalogo = new CatalogoPlazos();
	catalogo.setCampoClave("ic_plazo ");
	catalogo.setCampoDescripcion("cg_descripcion");	
	catalogo.setClaveProducto("1");	
	catalogo.setOrden("in_plazo_dias");	
	infoRegresar = catalogo.getJSONElementos();	
	
}else if (informacion.equals("Consultar") ) {

	lovDatos = BeanTasas.consultaTasasMandatoC(ic_epo, ic_if, ic_moneda, ic_mandante , tipoTasa );
		
	for (int i= 0; i < lovDatos.size(); i++) {
		numRegistros++;
		lovRegistro = (Vector) lovDatos.elementAt(i);
		tasasBorrar 		= lovRegistro.elementAt(0).toString();
		lsNombreEPO 		= lovRegistro.elementAt(1).toString();
		lsNombrePYME  		= lovRegistro.elementAt(2).toString();
		lsCalificacion  		= lovRegistro.elementAt(3).toString();
		lsPlazo  		= lovRegistro.elementAt(4).toString();
		lsCadenaTasa  		= lovRegistro.elementAt(5).toString();
		ldValorTotalTasa  		= lovRegistro.elementAt(6).toString();
		fecha  		= lovRegistro.elementAt(7).toString();
		lsValorTasaN  		= lovRegistro.elementAt(8).toString();
		lsAprobEPO  		= lovRegistro.elementAt(9).toString();
		lsAprobIF  		= lovRegistro.elementAt(10).toString();
		lsAprobNafin  		= lovRegistro.elementAt(11).toString();
		

			
		datos = new HashMap();	
		datos.put("NOMBRE_PYME", lsNombrePYME);
		datos.put("REFERENCIA", lsCalificacion);
		datos.put("PLAZO", lsPlazo);
		datos.put("TIPO_TASA", lsCadenaTasa);
		datos.put("VALOR", ldValorTotalTasa);
		datos.put("FECHA", fecha);
		datos.put("VALOR_TASA", lsValorTasaN);
		datos.put("NOMBRE_EPO", lsAprobEPO);
		datos.put("NOMBRE_IF", lsAprobIF);
		datos.put("NAFIN", lsAprobNafin);	
		datos.put("TASAS_BORRAR", tasasBorrar);
		registros.add(datos);				
	
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	jsonObj.put("lsNombreEPO", lsNombreEPO);
	infoRegresar  = jsonObj.toString();
	
	
}else if (informacion.equals("EliminarTasas") ) {
	try {
		String [] tasaBorrar		= request.getParameterValues("tasaBorrar");
	
		List lstParamtasas = new ArrayList();
		if(tasaBorrar!=null && tasaBorrar.length>0){
			for(int i=0;i<tasaBorrar.length;i++){
				Vector vParamtasas = Comunes.explode(",",tasaBorrar[i]);
				lstParamtasas.add(vParamtasas);				
			}
			if(lstParamtasas!=null && lstParamtasas.size()>0){
				BeanTasas.eliminarTasasMandato(lstParamtasas);
			}
		}
	}catch(Exception e) { out.println(e.getMessage()); 
		System.out.println(" Error ::: "+e);
	}finally {	
		jsonObj.put("success", new Boolean(true));
	}
	
	infoRegresar  = jsonObj.toString();
}


%>

<%=infoRegresar%>
