<% String version = (String)session.getAttribute("version"); %>
<%if(version!=null){ %>
<!DOCTYPE html>
<%} %>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="	java.util.*, 
						java.sql.*, 
						java.math.*, 
						java.text.*,	
						netropology.utilerias.*, 
						com.netro.exception.*,	
						javax.naming.*,	
						com.netro.descuento.*, 
						com.jspsmart.upload.*"
						
		 errorPage="/00utils/error.jsp"							
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../13secsession.jspf" %>
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload" />
<%
	try {
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(20097152);
		myUpload.upload();
	}catch(Exception e){}

	String claveIF 				= "IF".equals(strTipoUsuario)?iNoCliente:((myUpload.getRequest().getParameter("claveIF")  == null)?"0"            :myUpload.getRequest().getParameter("claveIF"));
	String nombreIF 				= "IF".equals(strTipoUsuario)?strNombre :((myUpload.getRequest().getParameter("nombreIF") == null)?"NO DISPONIBLE":myUpload.getRequest().getParameter("nombreIF"));
 
	// Borrar historial de una insercion anterior
	session.removeAttribute("13CATERRORESWSIFMB_INSERTED");	
	
	try {
		
		// Cargar Archivo en Directorio Temporal
		String ruta2									= null;
		myUpload.save(strDirectorioTemp);
		com.jspsmart.upload.File myFile 		= myUpload.getFiles().getFile(0);
		ruta2											= strDirectorioTemp+myFile.getFileName();
		
		CatalogoErroresWSIF 				validador 	= new CatalogoErroresWSIF();
		ResultadosValidacionCatalogo 	resultados 	= validador.procesarCargaMasiva(strDirectorioTemp, ruta2, claveIF);
		// Obtener Registros Validos
		ArrayList		lista 				= (ArrayList) resultados.getListaDeRegistrosExitosos(); 	
		int 				icProceso 			= resultados.getIcProceso(); 											
		ArrayList 		registrosValidos 	= validador.getRegistrosValidos(lista,icProceso);
		// Construir lista con los Numeros de Linea de los Registros Validos
		StringBuffer 	listaNumerosLinea	= new StringBuffer();
		for(int i=0;i<lista.size();i++){
			if(i>0) listaNumerosLinea.append(",");
			listaNumerosLinea.append((String) lista.get(i));
		}
		
%>
<html>
  <head>
    <title>Cat�logo de Errores WebService (Masiva)</title>
	 
	 	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<%if(version==null){ %>
			<link rel="stylesheet" href="<%=strDirecCSS%>/css/<%=strClase%>">
			<script language="JavaScript" src="<%=strDirecCSS%>/css/<%=(String)session.getAttribute("sesTpUsuario")%>.js"></script>

			<!-- ExtJS Related -->
			<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/ext-all.css" />
			<script type="text/javascript" src="/nafin/00utils/json/JSON.js"></script>
			<!--link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/subastatheme.css" /-->
			<script type="text/javascript" src="/nafin/00utils/extjs/adapter/ext/ext-base.js"></script>
			<script type="text/javascript" src="/nafin/00utils/extjs/ext-all-debug.js"></script>
			<script type="text/javascript" src="/nafin/00utils/extjs/ext-lang-es.js"></script>
		<%} %>

		<script language="JavaScript1.2" src="/nafin/00utils/valida.js"></script>

		<%if(version!=null){ %>
			<%@ include file="/extjs.jspf" %>
			<%@ include file="/01principal/menu.jspf"%>
		<%} %>

		<script language="JavaScript">
			 
		 // CONSTRUIR FORMA PARA CONSERVAR EL ESTILO DE LOS BOTONES
		 Ext.onReady(function(){
		
			Ext.QuickTips.init();
 
			 // SE DEFINE EL FORM PANEL DE LOS BOTONES
			 var gridForm = new Ext.FormPanel({
				  id:           'catalogo',        
				  labelAlign:   'top',
				  plain:        true,		  
				  width:        705,
				  border:       false,
				  monitorValid: true,
				  frame:        false,
				  labelAlign:  'right',	
				  style: 'margin: 0 auto',
				  bodyStyle: Ext.isIE ? 'padding:20px 5px 15px; text-align: left;' : 'padding:10px 15px; text-align: -moz-left;',
				  items: [
				  	{
                xtype: 'container',
                layout: 'hbox',
                height: 75,
                id: 'contenedorTipoCarga',
                layoutConfig: {
                    align: 'middle',
						  pack: 'center'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'Individual',
                        width: 100,
                        id: 'botonCargaIndividual',
								handler: function(){
									gridForm.getForm().getEl().dom.action = '/nafin/13descuento/13if/13CatErroresWSIni.do';
									gridForm.getForm().getEl().dom.submit();
								}
                    },
						  {
								xtype: 'label',
								text: 'x',
								style: 'color:white;',
								id: 'espacioBoton'
						  },
						  {
                        xtype: 'button',
                        text: 'Masiva',
                        width: 100,
                        id: 'botonCargaMasiva',
								pressed: 'true'
                    }
                ]
					}
				],		
					renderTo: Ext.get("formaCatalogo")
			});
					
			 			  
		});
  
		// Muestra la Tabla del Layout
		function mostrarTabla(nomdiv){
			showdiv(nomdiv);
		}
				
		function showdiv(id) {
			if (document.getElementById) { 
				document.getElementById(id).style.display = 'block';
			} else {
				if (document.layers) { 
					document.id.display = 'block';
				} else { 
					document.all.id.style.display = 'block';
				}
			}
		}
				
		// Oculta la Tabla del Layout
		function ocultarTabla(nomdiv){
			hidediv(nomdiv);
		}
				
		function hidediv(id) {
			if (document.getElementById) { 
				document.getElementById(id).style.display = 'none';
			} else {
				if (document.layers) { 
					document.id.display = 'none';
				} else { 
					document.all.id.style.display = 'none';
				}
			}
		}

		function doRecargaArchivo(){
			var f = document.forma;
			
			<%-- Validar que se haya especificado un archivo de texto --%>
			var Archivo = f.txtarchivo.value;
			var ext 		= "txt";
			var tipo 	= 6;
			if(f.txtarchivo.value==""){
				alert("Favor de capturar una ruta valida");
				return;
			}
			if(!formatoValido(Archivo, tipo)) {
				alert('El formato del archivo de origen no es el correcto. Debe tener extensi�n '+ext);
				f.txtarchivo.focus();
				f.txtarchivo.select();
				return;
			}

			f.submit();
		}
 
		function doRegresar(){
			var f 	= document.forma;
			f.action = "13caterroreswsifm.jsp";
			f.submit();
		}
		
		function doContinuarCarga(){
			var f 	= document.forma;
			
			var continuarCarga =window.confirm("Al confirmar la carga se sustituira la informaci�n\npreviamente cargada. �Desea continuar?");
			if(continuarCarga){
				f.action = "13caterroreswsifmb.jsp";
			}else{
				f.action = "13caterroreswsifm.jsp";
			}
			
			f.submit();
		}
		
		function updateDivsHeight(){
			
			var alturaLimite = 270;
			
			// Obtener Alturas de las Tablas
			var alturaTablaDoctosSinErrores = document.getElementById('tblRegistrosSinErrores').offsetHeight;
			var alturaTablaDoctosConErrores = document.getElementById('tblRegistrosConErrores').offsetHeight;
			// Verificar cual tabla tiene la altura mas grande
			var alturaMasGrande = (alturaTablaDoctosConErrores > alturaTablaDoctosSinErrores)?alturaTablaDoctosConErrores:alturaTablaDoctosSinErrores;
			// Ajustar los div a la altura de la tabla
			alturaMasGrande += 2; // 2px = suma de los bordes del div
			var nuevaAltura = (alturaLimite < alturaMasGrande)?alturaLimite:alturaMasGrande;
			
			document.getElementById("divRegistrosSinErrores").style.height = nuevaAltura + 'px';
			document.getElementById("divRegistrosConErrores").style.height = nuevaAltura + 'px';
			
		}
	</script>
  </head>
  <%if(version==null){ %>
	  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="javascript:updateDivsHeight();" >  
	  <table cellspacing="8" >
		<tr>
			<td>
				<div id="formaCatalogo"></div>
			</td>
		</tr>
		<%-- FORMA DE CARGA MASIVA --%>
		<form name="forma" method="POST" enctype="multipart/form-data" action="13caterroreswsifma.jsp">
			<input type="hidden" name="lstNumerosLineaOk" 		value="<%= listaNumerosLinea.toString() %>" >
			<input type="hidden" name="icProceso" 					value="<%= icProceso                    %>" >
			<input type="hidden" name="claveIF" 			   	value="<%= claveIF                      %>" >
			<input type="hidden" name="nombreIF" 			   	value="<%= nombreIF                     %>" >
			
		<tr>
			<td>
				<table width="690" cellpadding="2" cellspacing="0" border="0" class="formas">
					<%-- Ruta del Archivo de Origen --%>
					<tr>
						<td class="formas" valign="middle" align="center" nowrap="nowrap" colspan="2">
							Ruta&nbsp;del&nbsp;Archivo&nbsp;de&nbsp;Origen:<br>
							<a href="JavaScript:mostrarTabla('Layout');">
								<img src="/nafin/00utils/gif/ihelp.gif" border="0" width="15">
							</a>&nbsp;
							<input type="File" name="txtarchivo" value="Examinar" size="45" >
						</td>
					</tr>
					<%-- Espacio         --%>
					<tr>
						<td colspan="2" height="15px" >&nbsp;</td>
					</tr>
					<%-- Boton continuar --%>
					<tr>
						<td class="formas">&nbsp;</td>
						<td class="formas" align="right">
							<table cellpadding="2" cellspacing="1" border="0" class="formas">
							<tr>
								<td class="celda02" align="center" width="50" height="25"><a href="javascript:doRecargaArchivo();" onmouseout="window.status=''; return true;" onmouseover ="window.status='Consultar'; return true;">Recargar&nbsp;Archivo</a></td>
							<tr>
							</table>
						</td>
					</tr>
					<%-- Mostrar Layout --%>
					<tr>
						<td colspan="2" align="center">
							<div id="Layout" style="display:none" >&nbsp;
								<%@ include file="/13descuento/13if/13caterroreswsifmLayout.jspf" %>
							</div>
						</td>
					</tr>
					<%-- Despliegue de resultados de la validacion --%>
					<%if(1 == 1){%>
					<%
						int totalDescripcionesAlta = 0;
					%>
					<tr>
						<td colspan="2" height="15px" >&nbsp;</td>
					</tr>
					<tr>
						<td class="formas" align="center">
							Registros sin Errores<br>
							<div id="divRegistrosSinErrores" style="border:0px; width:100%; height:270px; overflow:auto;">
								<table id="tblRegistrosSinErrores" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
									<tr>
										<td class="celda02" width="25px" align="center" height="25px">L&iacute;nea</td><td class="celda02" width="75px" align="center">Campo</td><td class="celda02" width="400px" align="center">Observaci&oacute;n</td>
									</tr>
									<% if( registrosValidos.size() > 0){%>
									<%for(int i=0;i<registrosValidos.size();i++){%>
										<% HashMap registro = (HashMap) registrosValidos.get(i);%>
										<%-- Campo Clave --%>
										<tr>
											<td class="formas" align="center" height="25px"><%= registro.get("LINEA")%></td><td class="formas" align="center">Clave</td><td class="formas" align="left"><%= registro.get("CLAVE")%></td>
										</tr>
										<%-- Campo Descripcion --%>
										<tr>
											<td class="formas" align="center" height="25px"><%= registro.get("LINEA")%></td><td class="formas" align="center">Descripci&oacute;n</td><td class="formas" align="left"><%= registro.get("DESCRIPCION")%></td>
										</tr>
										<%-- Calcular Numero de Tasas a dar de Alta --%>
										<%
											totalDescripcionesAlta++;
										%>
										<input type="hidden" name="clave" 			value="<%=registro.get("CLAVE")%>"  		 >
										<input type="hidden" name="descripcion" 	value="<%=registro.get("DESCRIPCION")%>"   >
									<%}%>
									<%}else{%>
									<tr>
										<td class="celda02" colspan="3" align="center"><b>No se encontraron registros sin error</b></td>
									</tr>
									<%}%>
								</table>
							</div><br>
							<table width="100%" style="border:0px;border-collapse:collapse;">
								<tr>
									<td class="formas"  width="60%" style="padding-left:0px;border-left:0px;" >Total</td>
									<td class="formas"  width="40%" style="border:2px solid #A5B8BF;" align="right" ><%=resultados.getNumRegOk()%></td>
								</tr>
							</table>
						</td>
						<td class="formas" align="center">
							Registros con Errores<br>
							<div id="divRegistrosConErrores" style="border:0px; width:100%; height:270px; overflow:auto;">
								<table id="tblRegistrosConErrores" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
									<tr>
										<td class="celda02" width="25px" align="center" height="25px">L&iacute;nea</td><td class="celda02" width="75px" align="center">Campo</td><td class="celda02" width="400px" align="center">Observaci&oacute;n</td>
									</tr>
									<% if( resultados.getNumRegErr() > 0 ){%>
										<% ArrayList registros = resultados.getRegistros();%>
										<%for(int i=0;i<registros.size();i++){%>
											<%ValidacionCatalogo v = (ValidacionCatalogo) registros.get(i);%>
											<%if(v.hayError()){%>
												<%-- Campo 1 CLAVE --%>
												<%if(v.hayErrorDeClave()){%>
												<tr>
													<td class="formas" align="center" height="25px"><%=v.getNumeroLinea()%></td><td class="formas" align="center">Clave</td><td class="formas" align="left"><%=v.getErroresClave()%></td>
												</tr>
												<%}%>
												<%-- Campo 2 DESCRIPCION --%>
												<%if(v.hayErrorDeDescripcion()){%>
												<tr>
													<td class="formas" align="center" height="25px"><%=v.getNumeroLinea()%></td><td class="formas" align="center">Descripci&oacute;n</td><td class="formas" align="left"><%=v.getErroresDescripcion()%></td>
												</tr>
												<%}%>
												<%-- Campo X Error Generico --%>
												<%if(v.hayErrorGenerico()){%>
												<tr>
													<td class="formas" align="center" height="25px"><%=v.getNumeroLinea()%></td><td class="formas" align="center">&nbsp;</td><td class="formas" align="left"><%=v.getErrorGenerico()%></td>
												</tr>
												<%}%>
											<%}%>
										<%}%>
									<%}else{%>
									<tr>
										<td class="celda02" colspan="3" align="center"><b>No se encontraron registros con error</b></td>
									</tr>
									<%}%>
								</table>
							</div><br>
							<table width="100%" style="border:0px;border-collapse:collapse;">
								<tr>
									<td class="formas"  width="60%" style="padding-left:0px;border-left:0px;" >Total</td>
									<td class="formas"  width="40%" style="border:2px solid #A5B8BF;" align="right" ><%=resultados.getNumRegErr()%></td>
								</tr>
							</table>
						</td>
					</tr>
					<%-- Botones para Continuar la Carga --%>
					<tr>
						<td colspan="2" height="15px" >&nbsp;</td>
					</tr>
					<tr>
						<td class="formas">&nbsp;</td>
						<td class="formas" align="right">
							<table cellpadding="2" cellspacing="0" border="0" class="formas">
							<tr>
								<td class="celda02" align="center" width="50" height="25" ><a href="javascript:doRegresar();"        onmouseout="window.status=''; return true;" onmouseover ="window.status='Regresar';        return true;">Regresar</a></td>
								<% if( registrosValidos.size() > 0){ %>
								<td class="celda02" align="center" width="80" height="25" style="border-left:10px solid white;"><a href="javascript:doContinuarCarga();"  onmouseout="window.status=''; return true;" onmouseover ="window.status='Continuar Carga'; return true;">Continuar Carga</a></td>
								<% } %>
							<tr>
							</table>
						</td>
					</tr>
					<%}%>
					<%-- --%>
				</table>
			</td>
		</tr>
			<%-- Variables de Estadistica de las Tasas --%>
			<%
				int totalTasasCargadas = resultados.getNumRegErr() + resultados.getNumRegOk();
			%>
			<input type="hidden" name="totalRegistrosCargados" 					value="<%= totalTasasCargadas        %>" 	>
			<input type="hidden" name="totalRegistrosCargadosCorrectamente" 	value="<%= resultados.getNumRegOk()  %>"  >	
			<input type="hidden" name="totalRegistrosCargadosError" 				value="<%= resultados.getNumRegErr() %>" 	>	
		</form>
	  </table>
	  </body>
	<%} %>

	<%if(version!=null){ %>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

	<%@ include file="/01principal/01if/cabeza.jspf"%>
		<div id="_menuApp"></div>
		<div id="Contcentral">
			<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
			<div id="areaContenido">

			  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="javascript:updateDivsHeight();" >  
			  <table cellspacing="8" >
				<tr>
					<td>
						<div id="formaCatalogo"></div>
					</td>
				</tr>
				<%-- FORMA DE CARGA MASIVA --%>
				<form name="forma" method="POST" enctype="multipart/form-data" action="13caterroreswsifma.jsp">
					<input type="hidden" name="lstNumerosLineaOk" 		value="<%= listaNumerosLinea.toString() %>" >
					<input type="hidden" name="icProceso" 					value="<%= icProceso                    %>" >
					<input type="hidden" name="claveIF" 			   	value="<%= claveIF                      %>" >
					<input type="hidden" name="nombreIF" 			   	value="<%= nombreIF                     %>" >
					
				<tr>
					<td>
						<table width="690" cellpadding="2" cellspacing="0" border="0" class="formas">
							<%-- Ruta del Archivo de Origen --%>
							<tr>
								<td class="formas" valign="middle" align="center" nowrap="nowrap" colspan="2">
									Ruta&nbsp;del&nbsp;Archivo&nbsp;de&nbsp;Origen:<br>
									<a href="JavaScript:mostrarTabla('Layout');">
										<img src="/nafin/00utils/gif/ihelp.gif" border="0" width="15">
									</a>&nbsp;
									<input type="File" name="txtarchivo" value="Examinar" size="45" >
								</td>
							</tr>
							<%-- Espacio         --%>
							<tr>
								<td colspan="2" height="15px" >&nbsp;</td>
							</tr>
							<%-- Boton continuar --%>
							<tr>
								<td class="formas">&nbsp;</td>
								<td class="formas" align="right">
									<table cellpadding="2" cellspacing="1" border="0" class="formas">
									<tr>
										<td class="celda02" align="center" width="50" height="25"><a href="javascript:doRecargaArchivo();" onmouseout="window.status=''; return true;" onmouseover ="window.status='Consultar'; return true;">Recargar&nbsp;Archivo</a></td>
									<tr>
									</table>
								</td>
							</tr>
							<%-- Mostrar Layout --%>
							<tr>
								<td colspan="2" align="center">
									<div id="Layout" style="display:none" >&nbsp;
										<%@ include file="/13descuento/13if/13caterroreswsifmLayout.jspf" %>
									</div>
								</td>
							</tr>
							<%-- Despliegue de resultados de la validacion --%>
							<%if(1 == 1){%>
							<%
								int totalDescripcionesAlta = 0;
							%>
							<tr>
								<td colspan="2" height="15px" >&nbsp;</td>
							</tr>
							<tr>
								<td class="formas" align="center">
									Registros sin Errores<br>
									<div id="divRegistrosSinErrores" style="border:0px; width:100%; height:270px; overflow:auto;">
										<table id="tblRegistrosSinErrores" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
											<tr>
												<td class="celda02" width="25px" align="center" height="25px">L&iacute;nea</td><td class="celda02" width="75px" align="center">Campo</td><td class="celda02" width="400px" align="center">Observaci&oacute;n</td>
											</tr>
											<% if( registrosValidos.size() > 0){%>
											<%for(int i=0;i<registrosValidos.size();i++){%>
												<% HashMap registro = (HashMap) registrosValidos.get(i);%>
												<%-- Campo Clave --%>
												<tr>
													<td class="formas" align="center" height="25px"><%= registro.get("LINEA")%></td><td class="formas" align="center">Clave</td><td class="formas" align="left"><%= registro.get("CLAVE")%></td>
												</tr>
												<%-- Campo Descripcion --%>
												<tr>
													<td class="formas" align="center" height="25px"><%= registro.get("LINEA")%></td><td class="formas" align="center">Descripci&oacute;n</td><td class="formas" align="left"><%= registro.get("DESCRIPCION")%></td>
												</tr>
												<%-- Calcular Numero de Tasas a dar de Alta --%>
												<%
													totalDescripcionesAlta++;
												%>
												<input type="hidden" name="clave" 			value="<%=registro.get("CLAVE")%>"  		 >
												<input type="hidden" name="descripcion" 	value="<%=registro.get("DESCRIPCION")%>"   >
											<%}%>
											<%}else{%>
											<tr>
												<td class="celda02" colspan="3" align="center"><b>No se encontraron registros sin error</b></td>
											</tr>
											<%}%>
										</table>
									</div><br>
									<table width="100%" style="border:0px;border-collapse:collapse;">
										<tr>
											<td class="formas"  width="60%" style="padding-left:0px;border-left:0px;" >Total</td>
											<td class="formas"  width="40%" style="border:2px solid #A5B8BF;" align="right" ><%=resultados.getNumRegOk()%></td>
										</tr>
									</table>
								</td>
								<td class="formas" align="center">
									Registros con Errores<br>
									<div id="divRegistrosConErrores" style="border:0px; width:100%; height:270px; overflow:auto;">
										<table id="tblRegistrosConErrores" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
											<tr>
												<td class="celda02" width="25px" align="center" height="25px">L&iacute;nea</td><td class="celda02" width="75px" align="center">Campo</td><td class="celda02" width="400px" align="center">Observaci&oacute;n</td>
											</tr>
											<% if( resultados.getNumRegErr() > 0 ){%>
												<% ArrayList registros = resultados.getRegistros();%>
												<%for(int i=0;i<registros.size();i++){%>
													<%ValidacionCatalogo v = (ValidacionCatalogo) registros.get(i);%>
													<%if(v.hayError()){%>
														<%-- Campo 1 CLAVE --%>
														<%if(v.hayErrorDeClave()){%>
														<tr>
															<td class="formas" align="center" height="25px"><%=v.getNumeroLinea()%></td><td class="formas" align="center">Clave</td><td class="formas" align="left"><%=v.getErroresClave()%></td>
														</tr>
														<%}%>
														<%-- Campo 2 DESCRIPCION --%>
														<%if(v.hayErrorDeDescripcion()){%>
														<tr>
															<td class="formas" align="center" height="25px"><%=v.getNumeroLinea()%></td><td class="formas" align="center">Descripci&oacute;n</td><td class="formas" align="left"><%=v.getErroresDescripcion()%></td>
														</tr>
														<%}%>
														<%-- Campo X Error Generico --%>
														<%if(v.hayErrorGenerico()){%>
														<tr>
															<td class="formas" align="center" height="25px"><%=v.getNumeroLinea()%></td><td class="formas" align="center">&nbsp;</td><td class="formas" align="left"><%=v.getErrorGenerico()%></td>
														</tr>
														<%}%>
													<%}%>
												<%}%>
											<%}else{%>
											<tr>
												<td class="celda02" colspan="3" align="center"><b>No se encontraron registros con error</b></td>
											</tr>
											<%}%>
										</table>
									</div><br>
									<table width="100%" style="border:0px;border-collapse:collapse;">
										<tr>
											<td class="formas"  width="60%" style="padding-left:0px;border-left:0px;" >Total</td>
											<td class="formas"  width="40%" style="border:2px solid #A5B8BF;" align="right" ><%=resultados.getNumRegErr()%></td>
										</tr>
									</table>
								</td>
							</tr>
							<%-- Botones para Continuar la Carga --%>
							<tr>
								<td colspan="2" height="15px" >&nbsp;</td>
							</tr>
							<tr>
								<td class="formas">&nbsp;</td>
								<td class="formas" align="right">
									<table cellpadding="2" cellspacing="0" border="0" class="formas">
									<tr>
										<td class="celda02" align="center" width="50" height="25" ><a href="javascript:doRegresar();"        onmouseout="window.status=''; return true;" onmouseover ="window.status='Regresar';        return true;">Regresar</a></td>
										<% if( registrosValidos.size() > 0){ %>
										<td class="celda02" align="center" width="80" height="25" style="border-left:10px solid white;"><a href="javascript:doContinuarCarga();"  onmouseout="window.status=''; return true;" onmouseover ="window.status='Continuar Carga'; return true;">Continuar Carga</a></td>
										<% } %>
									<tr>
									</table>
								</td>
							</tr>
							<%}%>
							<%-- --%>
						</table>
					</td>
				</tr>
					<%-- Variables de Estadistica de las Tasas --%>
					<%
						int totalTasasCargadas = resultados.getNumRegErr() + resultados.getNumRegOk();
					%>
					<input type="hidden" name="totalRegistrosCargados" 					value="<%= totalTasasCargadas        %>" 	>
					<input type="hidden" name="totalRegistrosCargadosCorrectamente" 	value="<%= resultados.getNumRegOk()  %>"  >	
					<input type="hidden" name="totalRegistrosCargadosError" 				value="<%= resultados.getNumRegErr() %>" 	>	
				</form>
			  </table>
			  </body>

			</div>
		</div>
		</div>
		<%@ include file="/01principal/01if/pie.jspf"%>
		<form id='formAux' name="formAux" target='_new'></form>
	
	</body>
	<%} %>

</html>
<% 
} catch (Exception error){
	error.printStackTrace();
	out.println(error.getMessage());
} 
%>

