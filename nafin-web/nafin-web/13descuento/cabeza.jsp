<%@ page import="netropology.utilerias.*, java.text.*, java.util.*"%>
<%@ page import="com.netro.distribuidores.*"%>
<%@ page import = "
			com.netro.seguridadbean.SeguException, 
			javax.naming.*"%>
<%//Fodea 051-2010 Distribuidores 
String numeroEpo =  "";
if( (session.getAttribute("strTipoUsuario").equals("PYME")) || (session.getAttribute("strTipoUsuario").equals("EPO"))){
numeroEpo 				= (String)session.getAttribute("iNoEPO");
}
String gsParSistemaD  = request.getParameter("parSistema")==null ? ""  : (String)request.getParameter("parSistema");
TimeZone.setDefault(TimeZone.getTimeZone("CST"));
String nombreMes[] = {"enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"};
String nombreMesIngles[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
Calendar fecha = Calendar.getInstance();
int dia = fecha.get(Calendar.DAY_OF_MONTH);
int mes = fecha.get(Calendar.MONTH);
int anio = fecha.get(Calendar.YEAR);
//out.print(dia+" de "+nombreMes[mes]+" del "+anio);
SimpleDateFormat formatoHora = new SimpleDateFormat ("hh:mm:ss");

boolean _esPymeInternacionalIngles = (session.getAttribute("strTipoUsuario").equals("PYME") && 
		session.getAttribute("sesPymeInternacional").equals("S") &&
		session.getAttribute("sesIdiomaUsuario").equals("EN"))?true:false;
boolean _esEpoInternacionalIngles = (session.getAttribute("strTipoUsuario").equals("EPO") && 
		session.getAttribute("sesEpoInternacional").equals("S") &&
		session.getAttribute("sesIdiomaUsuario").equals("EN"))?true:false;

String _logoAlterno = (String) session.getAttribute("sesLogoAlterno");
//Fodea 051-2010 Distribuidores 
String tipoCredito = ""; 
String ventaCartera ="";
if( !numeroEpo.equals("")){
ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
 tipoCredito = BeanParametro.obtieneTipoCredito (numeroEpo); 
 ventaCartera = BeanParametro.DesAutomaticoEpo(numeroEpo,"PUB_EPO_VENTA_CARTERA"); 
 if(ventaCartera.equals("")){
 ventaCartera = "N";
}
}
%>
<link rel="stylesheet" href="<%="/nafin/14seguridad/Seguridad"%>/css/<%=(String)session.getAttribute("strClase")%>">
<Script Language="JavaScript" src="/nafin/14seguridad/Seguridad/scripts/JsCanvas.js"> </script>
<Script Language="JavaScript">
  function mtdCambiaHora()
  {
	  var lsHoraTmp = "";
	  gdHoraServer.setSeconds(gdHoraServer.getSeconds()+1);
	  if (gdHoraServer.getHours() < 10)	    lsHoraTmp = "0" + gdHoraServer.getHours() + ":";
	  else		    lsHoraTmp = gdHoraServer.getHours() + ":";
	  if (gdHoraServer.getMinutes() < 10)	    lsHoraTmp += "0" + gdHoraServer.getMinutes() + ":";
	  else		    lsHoraTmp += gdHoraServer.getMinutes() + ":";
	  if (gdHoraServer.getSeconds() < 10)	    lsHoraTmp += "0" + gdHoraServer.getSeconds();
	  else		    lsHoraTmp += gdHoraServer.getSeconds();
	  cnvHora.mtdPRepinta("<font color='white'>" + lsHoraTmp + "</font>");
	  setTimeout("mtdCambiaHora()", 1000);
  }
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}  
function fpedidos() {
	MM_openBrWindow('http://www.nafin.com/portal/images/nafinsa/Good.html','Financiamiento','toolbar=no,width=820,height=620,toolbar=no,menubar=no,status=no,scrollbars=yes,resizable=no,top=10,left=50');
}
  
</script>
<table width="762" cellpadding="0" cellspacing="0" border="0" height="90">
<tr>
	<td><img src="/nafin/00utils/gif/vacio.gif" width="1" height="7" border="0" alt=""></td>
</tr>
<tr>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td><img src="/nafin/00utils/vacio.gif" width="13" height="1" border="0" alt=""></td>
		<%if (session.getAttribute("strLogo")==null) session.setAttribute("strLogo", "1.gif");%>
		<td valign="middle"><img src="/nafin/00archivos/15cadenas/15archcadenas/logos/<%=(_logoAlterno != null)?_logoAlterno:session.getAttribute("strLogo")%>" height="65" width="175" alt="" border="0"></td>
		<td valign="middle">
		<table width="100%" cellpadding="0" cellspacing="1" border="0">
		<% String sPais = session.getAttribute("strPais").toString();
		   if (!sPais.equals("MEXICO")) {%>
		<tr>
			<td class="formas"><b><%=sPais%></b></td>
		</tr>
		<%}%>		
		<tr>
			<td class="formas" ><b><%= (session.getAttribute("iNoNafinElectronico") == null?"":session.getAttribute("iNoNafinElectronico"))%></b></td>
		</tr>
		<tr>
			<td class="formas" align="left"><b><%=(("S".equals((String)session.getAttribute("sesExterno")))?"":session.getAttribute("strNombre"))%></b></td>
		</tr>
		<tr>
			<td class="formas" align="left" ><b><%= session.getAttribute("strNombreUsuario")%></b></td>
		</tr>
		<%if(gsParSistemaD.equals("FDIST") && "A".equals(tipoCredito)){%>
		<tr>
		<td class="formas" align="left" ><b>DESCUENTO/CCC</b></td>
		</tr>
		<%}%>
		<%if(gsParSistemaD.equals("FDIST") && "C".equals(tipoCredito)){%>
		<tr>
		<td class="formas" align="left" ><b>CREDITO CUENTA CORRIENTE</b></td>
		</tr>
		<%}%>
		<%if(gsParSistemaD.equals("FDIST") && "D".equals(tipoCredito) && "S".equals(ventaCartera)){%>
		<tr>
		<td class="formas" align="left" ><b>VENTA DE CARTERA</b></td>
		</tr>
		<%}%>
		<%if(gsParSistemaD.equals("FDIST") && "D".equals(tipoCredito) && "N".equals(ventaCartera)){%>
		<tr>
		<td class="formas" align="left" ><b>DESCUENTO MERCANTIL</b></td>
		</tr>
		<%}%>
		</table>
		</td>
		<%
		//out.println ((String)session.getAttribute("iNoClienteFondos"));
		//out.println ("**** "+(String)session.getAttribute("strTipoUsuario"));
		if(null==(String)session.getAttribute("iNoClienteFondos")) {  %>
			<%if(((String)session.getAttribute("strTipoUsuario")).equals("DIST")) {  %>		
				<td align="right"><img src="/nafin/00archivos/15cadenas/15archcadenas/banner/ba_<%=session.getAttribute("strLogo")%>" alt="" border="0"></td>
			<%}else{%>
				<td align="right"><a href="javascript:fpedidos()" title=""><img src="/nafin/00archivos/15cadenas/15archcadenas/banner/ba_<%=session.getAttribute("strLogo")%>" alt="" border="0"></a></td>
			<%}%>
		<%}else{%>
				<td align="right"><img src="/nafin/00archivos/15cadenas/15archcadenas/banner/ba_<%=session.getAttribute("strLogo")%>" alt="" border="0"></td>
		<%}%>
	</tr>
	</table>
	</td>
</tr>
<tr>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="3"><img src="../00utils/gif/vacio.gif" width="5" height="3" border="0" alt=""></td>
	</tr>
	<tr>
		<td><img src="/nafin/00utils/gif/vacio.gif" width="160" height="1" border="0" alt=""></td>
		<td height="14" width="15" align="right" class="bullet" valign="top"><img src="/nafin/00utils/gif/45.gif" width="15" height="14" border="0" alt=""></td>
		<td class="bullet" height="14" width="550" align="center">
		<table cellpadding="0" cellspacing="0" border="0" height="14">
		<tr>
			<td class="bullet" valign="top"><font color="white">
<%
			if (_esPymeInternacionalIngles || _esEpoInternacionalIngles) {
%>
				Mexico City. <%=nombreMesIngles[mes]%> <%=dia%>, <%=anio%>
<%
			} else {
%>
				M&eacute;xico D.F. <%=dia+" de "+nombreMes[mes]+" del "+anio%>
<%
			}
%>		
			</font>
			</td>
			<td class="bullet"><img src="../../00util/gif/vacio.gif" width="120" height="1" border="0" alt=""></td>
			<td class="bullet" valign="top"><font color="white"><Script Language="JavaScript">var gbIsNS4 = (document.layers ? true : false); if (!gbIsNS4){ var cnvHora = new JSCanvas("cnvHoraIE"); cnvHora.mtdPDibuja("5","5","5","white");var gdHoraServer = new Date(); gdHoraServer.setHours(<%=new java.util.Date().getHours()%>);gdHoraServer.setMinutes(<%=new java.util.Date().getMinutes()%>);gdHoraServer.setSeconds(<%=new java.util.Date().getSeconds()%>);mtdCambiaHora();}else window.document.write("<%=formatoHora.format(new java.util.Date())%>");</script></font>
			</td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
</html-->