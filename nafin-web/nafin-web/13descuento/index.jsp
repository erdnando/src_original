<%@ page import="java.sql.*, java.text.*, netropology.utilerias.*"%>
<%
//El siguiente c�digo debe ser lo primero en ejecutarse, el cual determina si la sesi�n est� activa.
//De lo contrario en lugar de mostrar "Su sesi�n ha expirado..." lanzar� un NullPointer Exception
String gsCveUsuario = (String) session.getAttribute("sesCveUsuario");
if(gsCveUsuario == null){
%>
		<script language="JavaScript">
			alert("Su sesi�n ha expirado, por favor vuelva a entrar.");
			top.location="/nafin/15cadenas/15salir.jsp"
		</script>
<%
	return;
}


AccesoDB con = new AccesoDB();
try {
	String strTipoUsuario = (String)session.getAttribute("strTipoUsuario"); // POSIBLES VALORES NAFIN-EPO-PYME-IF
	String iNoUsuario = (String)session.getAttribute("iNoUsuario"); //
	String iNoCliente = (String)session.getAttribute("iNoCliente");//NO DE CLIENTE DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
	String strNombre = (String)session.getAttribute("strNombre"); // NOMBRE COMERCIAL O DE LA PERSONA DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
	String strNombreUsuario = (String)session.getAttribute("strNombreUsuario"); //
	String iNoEPO = (String)session.getAttribute("iNoEPO");//NUMERO DE LA EPO A LA CUAL PERTENECE LA PYME
	Long iNoNafinElectronico = (Long)session.getAttribute("iNoNafinElectronico");//NUMERO DE LA EPO A LA CUAL PERTENECE LA PYME
	String strLogin = (String)session.getAttribute("Clave_usuario");//LOGIN DEL USUARIO QUE SE LOGUEA
	String strLogo = (String)session.getAttribute("strLogo");//Logotipo
	String strClase = (String)session.getAttribute("strClase");//Clase css
	String bIfRelacionada = (String)session.getAttribute("bIfRelacionada");//Indica si la If esta relacionada o no.
	String strTipoAfiliacion = (String)session.getAttribute("strTipoAfiliacion");//TIPO DE AFILIACION DEL USUARIO B(BAJO),A(ALTO)
	String strSerial = (String)session.getAttribute("strSerial"); //VARIABLE QUE CONTIENE EL NUMERO DE SERIE DEL CERTIFICADO
	//out.print("strSerial: " + strSerial + "<br>");
	//Archivo que determina que tipo de usuario entro a N@FINET y redirecciona al sitio que le pertenece.
	String gsParSistema = request.getParameter("parSistema");
	con.conexionDB();

	String qryPass = "", qryAforo = "", qrySentencia = "", operaDescuento = "";
	PreparedStatement pstmt = null;
	ResultSet rs = null;

	String  gsIdioma = "";

	// Variables para PyME�s Internacionales SMJ 30/08/2005
	gsIdioma= (String)session.getAttribute("sesIdiomaUsuario");

	//qryPass = "select cg_password, '13descuento/index.jsp' ORIGENQUERY From  Where cg_login = ? ";
	qryAforo =
		" SELECT MIN (x.fn_aforo) / 100, MIN (x.fn_aforo_dl) / 100, '13descuento/index.jsp' origenquery"   +
		"   FROM (SELECT fn_aforo, fn_aforo_dl"   +
		"           FROM comrel_producto_epo"   +
		"          WHERE ic_producto_nafin = 1 AND ic_epo = ?"   +
		"         UNION"   +
		"         SELECT fn_aforo, fn_aforo_dl"   +
		"           FROM comcat_producto_nafin"   +
		"          WHERE ic_producto_nafin = 1) x"  ;
    //out.println(strTipoUsuario);
	if ("EPO".equals(strTipoUsuario)) {
		qrySentencia = "select cs_opera_descuento from comcat_epo where ic_epo = ? ";
		pstmt = con.queryPrecompilado(qrySentencia);
		pstmt.setLong(1, Long.parseLong(iNoCliente));
		rs = pstmt.executeQuery();
		if (rs.next()) {
			operaDescuento = rs.getString(1);
		}
		rs.close();
		pstmt.clearParameters();
		if(pstmt!=null) pstmt.close();

		qrySentencia =
			" SELECT cpe.cs_opera_notas_cred, cpe.cs_fecha_venc_pyme"   +
			"   FROM comrel_producto_epo cpe"   +
			"  WHERE cpe.ic_producto_nafin = 1"   +
			"    AND cpe.ic_epo = ?"  ;
		pstmt = con.queryPrecompilado(qrySentencia);
		pstmt.setLong(1, Long.parseLong(iNoCliente));
		rs = pstmt.executeQuery();
		if (rs.next()) {
			String operaNC = rs.getString(1)==null?"N":rs.getString(1);
			String operaFVPyme = rs.getString(2)==null?"N":rs.getString(2);
			session.setAttribute("operaNC", operaNC);
			session.setAttribute("operaFVPyme", operaFVPyme);
		}
		rs.close();
		pstmt.clearParameters();
		if(pstmt!=null) pstmt.close();
		if("S".equals(operaDescuento)){
			pstmt = con.queryPrecompilado(qryAforo);
			pstmt.setLong(1, Long.parseLong(iNoCliente));
			rs = pstmt.executeQuery();
			if (rs.next()) {
				session.setAttribute("strAforo", rs.getString(1));
				session.setAttribute("strAforoDL", rs.getString(2));
			}
			rs.close();
			pstmt.clearParameters();
			if(pstmt!=null) pstmt.close();

			//out.println("Entro " + strTipoUsuario + "Nombre de usuario " + strNombre + "<br>");
			if(strSerial.equals("")) {
				/* foda 006-2005
				pstmt = con.queryPrecompilado(qryPass);
				pstmt.setString(1, strLogin);
				rs = pstmt.executeQuery();
				if(rs.next()){
					response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD="+rs.getString(1));
				}
				rs.close();
				pstmt.clearParameters();
				if(pstmt!=null) pstmt.close();*/
				%>
				<script language="JavaScript">
					window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
				</script>
				<%
				//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
			} else {
				response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
				//out.println("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
			}
		}else{
			response.sendRedirect("13logout.jsp");
		}
	} else if ("PYME".equals(strTipoUsuario)) {
		pstmt = con.queryPrecompilado(qryAforo);
		pstmt.setLong(1, Long.parseLong(iNoEPO));
		rs = pstmt.executeQuery();
		if(rs.next()) {
			session.setAttribute("strAforo",rs.getString(1));
			session.setAttribute("strAforoDL",rs.getString(2));
		}
		rs.close();
		pstmt.clearParameters();
		if(pstmt!=null) pstmt.close();
		//out.println("Entro " + strTipoUsuario + "<br>");
		//out.println("Nombre de usuario " + strNombre);
		qrySentencia =
			"SELECT cs_aceptacion, '13descuento/index.jsp' ORIGENQUERY FROM comrel_pyme_epo " +
			"WHERE ic_epo= ? AND ic_pyme= ? " ;
		pstmt = con.queryPrecompilado(qrySentencia);
		pstmt.setLong(1, Long.parseLong(iNoEPO));
		pstmt.setLong(2, Long.parseLong(iNoCliente));
		rs = pstmt.executeQuery();
		pstmt.clearParameters();
		if (rs.next()) {
			char aceptacion = rs.getString(1).charAt(0);
			switch (aceptacion){
				case 'R':
					response.sendRedirect("13pyme/13forma3.jsp?parSistema="+gsParSistema);
					break;
				case 'H':
					String bPymeRelacionada = "N";
					String qryRelPyemEpo =
						"SELECT count(1) as numRelacionesPymeEpo, '13descuento/index.jsp' ORIGENQUERY "+
						"FROM comrel_pyme_epo WHERE ic_pyme = ? ";
					pstmt = con.queryPrecompilado(qryRelPyemEpo);
					pstmt.setLong(1, Long.parseLong(iNoCliente));
					rs = pstmt.executeQuery();
					while(rs.next())
						bPymeRelacionada = (rs.getInt(1) > 1)?"S":"N";
					rs.close();
					pstmt.clearParameters();
					if(pstmt!=null) pstmt.close();
					if("S".equals(bPymeRelacionada)){
						//response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
						if (gsIdioma != null &&  "EN".equals(gsIdioma)){
							//System.out.println("\n\n\n\n............Entro al if en ingles 13consulta5_ing.jsp");
							response.sendRedirect("13pyme/13consulta5_ing.jsp?parSistema="+gsParSistema);
						}else{
							//System.out.println("\n\n\n\n............Entro al if en espa�ol 13consulta5.jsp");
							response.sendRedirect("13pyme/13consulta5.jsp?parSistema="+gsParSistema);
						}
					} else {
						//System.out.println("\n\n\n\n............Entro a ejecutar el query");
						String queryPropuesta =
							" SELECT fn_monto_credito"   +
							"   FROM com_mensaje_pyme pro"   +
							"  WHERE pro.cs_estatus_envio = 'A'"   +
							"    AND pro.ic_epo = (SELECT ic_epo"   +
							"                        FROM comrel_pyme_epo"   +
							"                       WHERE ic_pyme = pro.ic_pyme)"   +
							"    AND pro.ic_pyme = ?"  +
							"	 and to_char(df_publicacion,'mm/yyyy') = to_char(sysdate,'mm/yyyy')";

						pstmt = con.queryPrecompilado(queryPropuesta);
						pstmt.setLong(1, Long.parseLong(iNoCliente));
						rs = pstmt.executeQuery();
						String fg_monto = "";
						while(rs.next())
							fg_monto = rs.getString(1)==null?"":rs.getString(1);
						rs.close();
						pstmt.clearParameters();
						if(pstmt!=null) pstmt.close();
						if("".equals(fg_monto)){
							System.out.println("\n\n\n\n............No encontr� monto");
							response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
						}else{
							System.out.println("\n\n\n\n............Si encontr� monto");
							response.sendRedirect("13pyme/13credicadenas.jsp?fg_monto="+fg_monto);
//							response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
						}
					}
					break;
				case 'S':
					response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
					//response.sendRedirect("13pyme/13mensaje2.jsp"); mod RJV 071002 FODA 99
					break;
				case 'N':
					response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
					//response.sendRedirect("cierra.html"); mod RJV 071002 FODA 99
					break;
				default:
					response.sendRedirect("cierra.html");
					break;
			}
		}
		rs.close();
		if(pstmt!=null) pstmt.close();
	}  else if ("MANDANTE".equals(strTipoUsuario)) {
		response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
	} else if ("IF".equals(strTipoUsuario)) {
		//out.println("Entro " + strTipoUsuario + "Nombre de usuario " + strNombre + "<br>");
		qrySentencia = "select cs_opera_descuento, '13descuento/index.jsp' ORIGENQUERY from comcat_if where ic_if = ? ";
		pstmt = con.queryPrecompilado(qrySentencia);
		pstmt.setLong(1, Long.parseLong(iNoCliente));
		rs = pstmt.executeQuery();
		if(rs.next())
			operaDescuento = rs.getString(1);
		rs.close();
		pstmt.clearParameters();
		if(pstmt!=null) pstmt.close();
		if("S".equals(operaDescuento)){
			if(strSerial.equals("")) {
				/*foda 006-2005
				pstmt = con.queryPrecompilado(qryPass);
				pstmt.setString(1, strLogin);
				rs = pstmt.executeQuery();
				if(rs.next()){
					out.println("EL SERIAL = "+strSerial);
					//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD="+rs.getString(1));
				}
				rs.close();
				pstmt.clearParameters();
				if(pstmt!=null) pstmt.close();*/
				//out.println("EL SERIAL = "+strSerial);
				%>
					<script language="JavaScript">
						window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
						'Certificado',
						'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
					</script>
				<%	
				//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
			} else {
				if ("S".equals(bIfRelacionada))
					response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
				else
					response.sendRedirect("../14seguridad/Ventana.jsp?parSistema="+gsParSistema);
			}
		}else{
			response.sendRedirect("13logout.jsp");
		}
	} else if("NAFIN".equals(strTipoUsuario)) {
		//out.println("Tipo Usuario " + strTipoUsuario + "Nombre de usuario " + strNombre + "<br>");
		if(strSerial.equals("") && (!"CONSUL BANOBRAS".equals(session.getAttribute("sesPerfil"))) ) {
			/*foda 006-2005
			pstmt = con.queryPrecompilado(qryPass);
			pstmt.setString(1, strLogin);
			rs = pstmt.executeQuery();
			if(rs.next()){
				response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD="+rs.getString(1));
			}
			rs.close();
			pstmt.clearParameters();
			if(pstmt != null) pstmt.close();*/
			%>
			<script language="JavaScript">
				window.open('/cgi-bin/cgi_certifica/get_page?USERNAME=<%=strLogin%>&PASSWORD=',
				'Certificado',
				'toolbar=no,width=800,height=600,toolbar=no,menubar=no,status=yes,scrollbars=yes,resizable=yes,top=1,left=1');
			</script>
			<%	
			//response.sendRedirect("/cgi-bin/cgi_certifica/get_page?USERNAME="+strLogin+"&PASSWORD=");
		} else {
			response.sendRedirect("../14seguridad/Ventana.jsp?parSistema=" + gsParSistema);
		}
	}
} catch(Exception e) {
	out.println("Servicio no disponible<br>");
	out.println(e.getMessage());
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
}
%>