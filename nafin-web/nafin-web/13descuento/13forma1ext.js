    var doctoAnt = "";
var totalSaldo = 0;

Ext.onReady(function() {
	var cveEpoOriginal= '';
	var comboEpoVacio= false;
	var cveIFtmp;
	var strUsuario = Ext.getDom('hidStrUsuario').value;
	var strUsr = Ext.getDom('hidStrUsr').value;
	var validaCder = true;
	//var strUsuario = 'PYME';
	//Objeto para validar si ya estan cargados los combos necesarios antes de realizar
	//una consulta automatica, basandose en los parametros recibidos
	var Inicializacion = {
		catalogoMoneda : false,
		catalogoEpo: false
	};

	var objCalc={
		totalDescuento:0,
		totalMonto:0,
		totalDescuento:0,
		totalIntereses:0,
		totalRecibir:0,
		totaldocs:0,
		esBancoDeFondeoBANCOMEXT: null,
		tipoCambio: null,
		montoLimiteUtilizadoPorIFI: null,
		montoLimiteMaximoPorIFI: null,
		montoLimiteUtilizadoPorEPO: null,
		montoLimiteMaximoPorEPO: null,
		montoLimiteUtilizadoPorPYME: null,
		montoLimiteMaximoPorPYME: null,
		hayMontoLimiteParametrizadoPorPYME: null,
		hayMontoLimiteParametrizadoPorIFI: null,
		montoLimiteComprometidoPorPYME: null,
		montoLimiteComprometidoPorEPO: null
	}

	var objGral={
		inicial:'S',
		strTipoUsuario:null,
		pymeBloq:null,
		tipo_servicio:null,
		iNoEPO:null,
		urlCapturaDatos:null,
		fecDiaSigHabil:null,
		msgDiaSigHabil:null,
		msgOperFact24hrs:null,
		msgFecVencPyme:null,
		//msgCboIf:null,
		valorTC: null,
		paramEpoPef: null,
		sLimiteActivo: null,
		numCamposDinamicos: null,
		mapNombres:null,
		operaNotasDeCredito: null,
		aplicarNotasDeCreditoAVariosDoctos: null,
		icDoctosNotas: null,
		icNotasAp: null,
		bancoDeFondeo: null,
		limites: null,
		sTipoLimite: null,
		monto: null,
		descuento: null,
		interes: null,
		recibir: null,
		valEPO: null,
		valIF: null,
		sFecVencLineaCred: 'N',
		msgError: null,
		consultaIni:'S',
		operaMontosMenores:null
	}

//-----------------------------FUNCTIONS-------------------------------------
	var fnTransmitirDerechoCallback = function(vpkcs7, vtextoFirmar, vokResp, vbtn){
		if (Ext.isEmpty(vpkcs7)) {
			vbtn.enable();
			return;	//Error en la firma. Termina...
		}else{
			realizaConfirmacionNAFIN(vokResp, vpkcs7, vtextoFirmar);
		}
	}
	
	var fnTransmitirDerecho = function(btn, certificado){
		
		if((strUsuario=='PYME' || strUsuario=='NAFIN') && certificado!=true && validaCder){

			var win = new NE.cesion.WinCesionDerechos({
				getResultValid:resulValidCesion,
				cvePerfProt: 'PPYMAUTOR',
				cveFacultad: '13PYME13AUTORIZA',
				strUsuario: strUsr,
				tipoUsuario: strUsuario
			}).show();
			win.el.dom.scrollIntoView();

		}else{
			btn.disable();
			var infoTotal = '';
			storeTotalData.each(function(record) {
				infoTotal=infoTotal+
							record.data['EPO']+'|'+
							record.data['TOTALMONTO']+'|'+
							record.data['TOTALDESCTO']+'|'+
							record.data['TOTALINTERES']+'|'+
							record.data['TOTALIMPORT']+'|\n';
			});

			var textoFirmar = "E P O-DATOS|Total Monto Documento|Total Monto Descuento|Total Monto de Inter�s|Total Importe a Recibir\n"+ infoTotal +
			"Al solicitar el factoraje electr�nico o descuento electr�nico del documento que selecciono e identifico, transmito los derechos"+
			" que sobre el mismo ejerzo, de acuerdo con lo establecido por los art�culos 427 de la Ley General de T�tulos y Operaciones de Cr�dito,"+
			" 32 c del C�digo Fiscal de la Federaci�n y 2038 del C�digo Civil Federal, haci�ndome sabedor de su contenido y alcance, condicionado a"+
			" que se efect�e el descuento electr�nico o el factoraje electr�nico";

			
			NE.util.obtenerPKCS7(fnTransmitirDerechoCallback, textoFirmar, 'S', btn);
			
		}

	}

	var sobrepasaLimitePorIFI = function (monto){
		var resultado					= false;
		var montoTotalUtilizado 	= parseFloat(monto,10) + parseFloat(objCalc.montoLimiteUtilizadoPorIFI);

		if( montoTotalUtilizado > parseFloat(objCalc.montoLimiteMaximoPorIFI) ){
			resultado = true;
		}

		return resultado;
	}

	// VERIFICAR SI SE SOBREPASA LIMITE POR EPO
	var sobrepasaLimitePorEPO = function (monto){
		var resultado					= false;
		var montoTotalUtilizado 	= parseFloat(monto,10)  + parseFloat(objCalc.montoLimiteUtilizadoPorEPO) + parseFloat(objCalc.montoLimiteComprometidoPorEPO);

		if( montoTotalUtilizado > parseFloat(objCalc.montoLimiteMaximoPorEPO)  ){
			resultado = true;
		}

		return resultado;
	}

	// VERIFICAR SI SE SOBREPASA LIMITE POR PYME
	var sobrepasaLimitePorPYME = function(monto){
		var resultado					= false;
		var montoTotalUtilizado 	= parseFloat(monto,10)  + parseFloat(objCalc.montoLimiteUtilizadoPorPYME) + parseFloat(objCalc.montoLimiteComprometidoPorPYME);

		if( montoTotalUtilizado > parseFloat(objCalc.montoLimiteMaximoPorPYME) ){
			resultado = true;
		}

		return resultado;
	}

	//realiza calculo de seleccion de doctos
	var recalcular = function(selModel,record, rowIndex, isSelect){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var cboIf = Ext.getCmp('cboIf1');
		var cboMoneda = Ext.getCmp('cboMoneda1');

		var objeto		= record.data['SELECHID'];//f.seleccionados;
		var notascred	= record.data['SELECNOTAS'];//f.notascred.value;
		var aplicado		= record.data['APLICADO'];//f.aplicado;
		var notnotas = record.data['SELECBOOL'];//f.aplicado;
		var montoMini = record.data['SELEMONTOMINI'];//f.aplicado;

		if (cboIf.getValue() != ''  ){
			
			if ( montoMini  ==='S' ){
			
			if (!panelTotales.isVisible()) {
				var recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());
				panelTotales.setTitle('TOTALES - '+recordMoneda.get(cboMoneda.displayField));
				contenedorPrincipalCmp.add(panelTotales);
				contenedorPrincipalCmp.doLayout();
			}

			if(notascred!=''){
				selModel.selectRow(rowIndex,true);
				isSelect = true;
			}

			if (isSelect && aplicado=="N"){ //true --> Suma
				var totalDescto = 0;

				if (cboIf.getValue() == ''){
					if(notascred!="") return false;
					Ext.MessageBox.alert('Aviso','Debes seleccionar un Intermediario');
					selModel.deselectRow(rowIndex);
					return 'e';
				}else if(objGral.msgOperFact24hrs!=''){
					Ext.MessageBox.alert('Aviso',objGral.msgOperFact24hrs);
					selModel.deselectRow(rowIndex);
					return false;
				}

				totalDescto = roundOff(parseFloat(objCalc.totalDescuento) + parseFloat(record.data['MONTODESC']),2);

				if(notnotas){
					var sobrepasaLimite	= false;
					var montoDoctos		= totalDescto;

					// Solo para el caso de Banco de Fondeo BANCOMEXT, se valida que el Monto a Recibir no sobrepase el limite por IFI
					if( objCalc.esBancoDeFondeoBANCOMEXT && objCalc.hayMontoLimiteParametrizadoPorIFI && sobrepasaLimitePorIFI(montoDoctos) ){
						sobrepasaLimite = true;
					}
					// Se valida que el Monto del Descuento no sobrepase el Limite por EPO
					if( sobrepasaLimitePorEPO(montoDoctos)){
						sobrepasaLimite = true;
					}

					// Se valida que el Monto del Descuento no sobrepase el limite por PYME si este se encuentra parametrizado
					if( objCalc.hayMontoLimiteParametrizadoPorPYME && sobrepasaLimitePorPYME(montoDoctos) ){
						sobrepasaLimite = true;
					}

					if (!sobrepasaLimite){ // Paso las validaciones de los limites exitosamente
						objCalc.totalMonto 		= roundOff(parseFloat(objCalc.totalMonto) + parseFloat(record.data['MONTO']),2);
						objCalc.totalDescuento 	= roundOff(parseFloat(objCalc.totalDescuento) + parseFloat(record.data['MONTODESC']),2);
						objCalc.totalIntereses 	= roundOff(parseFloat(objCalc.totalIntereses) + parseFloat(record.data['IMPORTINT']),2);
						objCalc.totalRecibir 	= roundOff(parseFloat(objCalc.totalRecibir) + parseFloat(record.data['IMPORTREC']),2);
						objCalc.totaldocs 		= parseInt(objCalc.totaldocs) + 1;
					} else {
						var mensaje 	=	"Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.\n\n"+
								"Por favor comuniquese al Centro de Atencion a Clientes al telefono 50-89-61-07 o\n"+
								"del interior al 01-800-NAFINSA (01-800-6234672).";

						Ext.MessageBox.alert('Aviso',mensaje);
						record.data['SELECCION']='N';
						selModel.deselectRow(rowIndex);
						isSelect = false;
						return false;
					}
				}else {
					objCalc.totalMonto 		= roundOff(parseFloat(objCalc.totalMonto) + parseFloat(record.data['MONTO']),2);
					objCalc.totalDescuento 	= roundOff(parseFloat(objCalc.totalDescuento) + parseFloat(record.data['MONTODESC']),2);
					objCalc.totalIntereses 	= roundOff(parseFloat(objCalc.totalIntereses) + parseFloat(record.data['IMPORTINT']),2);
					objCalc.totalRecibir 	= roundOff(parseFloat(objCalc.totalRecibir) + parseFloat(record.data['IMPORTREC']),2);
					objCalc.totaldocs 		= parseInt(objCalc.totaldocs) + 1;
				}
				if(notascred!=""){
					record.data['APLICADO']="S";
					aplicado=record.data['APLICADO'];
				}
			}else if(aplicado=="N")	{
				objCalc.totalMonto 		= roundOff(parseFloat(objCalc.totalMonto) - parseFloat(record.data['MONTO']),2);
				objCalc.totalDescuento 	= roundOff(parseFloat(objCalc.totalDescuento) - parseFloat(record.data['MONTODESC']),2);
				objCalc.totalIntereses 	= roundOff(parseFloat(objCalc.totalIntereses) - parseFloat(record.data['IMPORTINT']),2);
				objCalc.totalRecibir 	= roundOff(parseFloat(objCalc.totalRecibir) - parseFloat(record.data['IMPORTREC']),2);
				objCalc.totaldocs = parseInt(objCalc.totaldocs) - 1;
			}

			Ext.getCmp('totalDoctosDesp1').setValue(objCalc.totaldocs);
			Ext.getCmp('totalMontoDesp1').setValue('$'+formatoFlotante(objCalc.totalMonto,"aplicar"));
			Ext.getCmp('totalMontoDescDesp1').setValue('$'+formatoFlotante(objCalc.totalDescuento,"aplicar"));
			Ext.getCmp('totalImpIntDesp1').setValue('$'+formatoFlotante(objCalc.totalIntereses,"aplicar"));
			Ext.getCmp('totalImpRecibDesp1').setValue('$'+formatoFlotante(objCalc.totalRecibir,"aplicar"));
			}
		}else{
			if (isSelect){ //true --> Suma
				var totalDescto = 0;
				Ext.MessageBox.alert('Aviso','Debes seleccionar un Intermediario');
				selModel.deselectRow(rowIndex);
				isSelect = false;
				return false;
			}
		}
		return true;

	}

	var terminar = function() {
		var cboIf = Ext.getCmp('cboIf1');
		var cboEpo = Ext.getCmp('cboEpo1');

		if (cboEpo.getValue() == ''){
			Ext.MessageBox.alert('Aviso','Debes seleccionar una Cadena');
			return;
		}else if (cboIf.getValue() != ''){
			if (objCalc.totalMonto == 0) {
				Ext.MessageBox.alert('Aviso','No ha seleccionado documentos');
			} else {

				var sobrepasaLimite	= false;
				var monto				= objCalc.totalDescuento;

				// Solo para el caso de Banco de Fondeo BANCOMEXT, se valida que el Monto a Recibir no sobrepase el limite por IFI
				if( objCalc.esBancoDeFondeoBANCOMEXT && objCalc.hayMontoLimiteParametrizadoPorIFI && sobrepasaLimitePorIFI(monto) ){
					sobrepasaLimite = true;
				}
				// Se valida que el Monto del Descuento no sobrepase el Limite por EPO
				if( sobrepasaLimitePorEPO(monto)){
					sobrepasaLimite = true;
				}
				// Se valida que el Monto del Descuento no sobrepase el limite por PYME si este se encuentra parametrizado
				if( objCalc.hayMontoLimiteParametrizadoPorPYME && sobrepasaLimitePorPYME(monto) ){
					sobrepasaLimite = true;
				}

				if (!sobrepasaLimite){ // Paso las validaciones de los limites exitosamente
					//CODIGO PARA PREACUSE
					generaPreAcuse();

				}else{ // Sobrepaso alguno de los limites en cuestion
					var mensaje 	=	"Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.\n\n"+
							"Por favor comuniquese al Centro de Atencion a Clientes al telefono 50-89-61-07 o\n"+
							"del interior al 01-800-NAFINSA (01-800-6234672).";
					Ext.MessageBox.alert('Aviso',mensaje);
					return;
				}
			}
		}else{
			Ext.MessageBox.alert('Aviso','Debes seleccionar un Intermediario');
			return;
		}
	}

	//FUNCION: genera la pantalla de Preacuse manipulando los objetos involucrados en el pantalla
	// y generando el grid de preacuse para verificar que los documentos son los seleccionados
	var generaPreAcuse = function() {
		var store = grid.getStore();
		var columnModelGrid = grid.getColumnModel();
		var noRegistros = false;
		var numRegistro = -1;
		var registrosPreAcu = [];

		//se recorre grid principal para detectar los documentos seleccionados
		store.each(function(record) {
			numRegistro = store.indexOf(record);
			if(record.data['SELECCION']=='S' && record.data['SELEMONTOMINI']=='S'  ){
				registrosPreAcu.push(record);
				noRegistros = true;
			}
		});

		//si existen doctos seleccionados, entonces se muestra el grid de preacuse
		if(noRegistros){
			var gridColumnMod = gridPreAcu.getColumnModel();
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var strTipoUsuario = objGral.strTipoUsuario;
			var paramEpoPef = objGral.paramEpoPef;
			var numCamposDinamicos = objGral.numCamposDinamicos;
			var mapNombres = objGral.mapNombres;
			grid.stopEditing();

			var ftotales = Ext.getCmp('panelTotales1');
			var objTotales = ftotales.getForm().getValues();
			var vcboEpo = Ext.getCmp('cboEpo1');
			var vrecord = vcboEpo.findRecord(vcboEpo.valueField, vcboEpo.getValue());
			
			var dataTotales = [
					[vrecord.get(vcboEpo.displayField), objTotales.totalMontoDesp, objTotales.totalMontoDescDesp, objTotales.totalImpIntDesp, objTotales.totalImpRecibDesp ]
				];


			//se le carga informacion al grid de preacuse y al grid de totales
			storePreAcuData.add(registrosPreAcu);
			storeTotalData.loadData(dataTotales);
			//Se incian validaciones para determinar el layout que se mostrara en el grid preacuse
			if(strTipoUsuario=='PYME'){//campos adicionales del indice 12-16
				var indice = 10;
				for(var i=10;i<15;i++){
					gridColumnMod.setHidden(i,true);
				}

				for(var i=0; i<numCamposDinamicos; i++){
					var nombreColumna = eval("mapNombres.CAMPO"+i)
					gridColumnMod.setColumnHeader(indice,nombreColumna);
					gridColumnMod.setHidden(indice,false);
					indice = indice+1;
				}
			}
			//se verifica si la epo esta parametrizada como EpoPEF para mostrar cierto campos
			if(!paramEpoPef){//campos EPO PEF del indice 17-20
				for(var i=15;i<19;i++){
					gridColumnMod.setHidden(i,true);
				}
			}else{
				for(var i=15;i<19;i++){
					gridColumnMod.setHidden(i,false);
				}
			}

			//se valida si el grid de preacuse ya es visible
			if (!gridPreAcu.isVisible()) {
				contenedorPrincipalCmp.findById('forma').hide();
				contenedorPrincipalCmp.findById('intermediariosS1').hide();
				contenedorPrincipalCmp.findById('gridDoctos').hide();
				contenedorPrincipalCmp.findById('panelTotales1').hide();

				if(contenedorPrincipalCmp.findById('gridDoctosPreAcu')){
					contenedorPrincipalCmp.findById('gridDoctosPreAcu').show();
					contenedorPrincipalCmp.findById('gridTotales1').show();
					contenedorPrincipalCmp.findById('pnmsgLegal1').show();
				}else{
					contenedorPrincipalCmp.insert(5,gridPreAcu);
					contenedorPrincipalCmp.insert(6,gridTotales);
					contenedorPrincipalCmp.add(panelMsgLegal);
				}


				contenedorPrincipalCmp.doLayout();
			}

		}

	}//fin function generaPreAcuse

	//FUNCION:retorna de la pabtalla de Pre Acuse a la parte de seleccion de documentos
	//para poder verificar la seleccion que se realizo
	var regresaAseleccion = function(){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		contenedorPrincipalCmp.findById('forma').show();
		contenedorPrincipalCmp.findById('intermediariosS1').show();
		contenedorPrincipalCmp.findById('gridDoctos').show();
		contenedorPrincipalCmp.findById('panelTotales1').show();

		contenedorPrincipalCmp.findById('gridDoctosPreAcu').hide();
		contenedorPrincipalCmp.findById('gridTotales1').hide();
		contenedorPrincipalCmp.findById('gridDoctosPreAcu').getStore().removeAll(true);
		contenedorPrincipalCmp.findById('gridTotales1').getStore().removeAll(true);

		contenedorPrincipalCmp.doLayout();
	}


	var realizaConfirmacionNAFIN = function(okResp, pkcs7, textoFirmar){
		var registrosEnviar = [];
		if(okResp=='S'){


			Ext.Ajax.request({
				url: '13forma1ext.data.jsp',
				params: {
					informacion: 'validaHorario',
					cboIf: objGral.valIF,
					cboEpo: objGral.valEPO
				},
				callback: function(opts, success, response) {
					if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
						var resp = 	Ext.util.JSON.decode(response.responseText);

						if(resp.msgError!=''){
							Ext.MessageBox.alert('Mensaje',resp.msgError);
							Ext.getCmp('btnTransmitir').enable();
						}else{
							if(resp.msgDiaSigHabil!=''){
								Ext.Msg.confirm('Confirmaci�n', resp.msgDiaSigHabil, function(btn){
									if(btn=='yes'){
										if(resp.msgOperFact24hrs!=''){
											Ext.MessageBox.alert('Mensaje',resp.msgOperFact24hrs);
											Ext.getCmp('btnTransmitir').enable();
										}else{
											storePreAcuData.each(function(record){
												registrosEnviar.push(record.data);
											});

											Ext.Ajax.request({
												url: '13forma1ext.data.jsp',
												params: {
													informacion: 'confirmaClaveCesion',
													registros: Ext.encode(registrosEnviar),
													totalDescuento: objCalc.totalDescuento,
													totalMonto: objCalc.totalMonto,
													totalDescuento: objCalc.totalDescuento,
													totalIntereses: objCalc.totalIntereses,
													totalRecibir: objCalc.totalRecibir,
													sTipoLimite: objGral.sTipoLimite,
													svalorTC: objGral.valorTC,
													cboIf: objGral.valIF,
													cboEpo: objGral.valEPO,
													cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
													valCve:okResp,
													strUsr:strUsr,
													correo:'',
													email: '',
													Pkcs7: pkcs7,
													TextoFirmado: textoFirmar
												},
												callback: procesarSuccessConfirmaCesion
											});
										}
									}else{
										Ext.getCmp('btnTransmitir').enable();
									}
								});
							}else{

								storePreAcuData.each(function(record){
									registrosEnviar.push(record.data);
								});

								Ext.Ajax.request({
									url: '13forma1ext.data.jsp',
									params: {
										informacion: 'confirmaClaveCesion',
										registros: Ext.encode(registrosEnviar),
										totalDescuento: objCalc.totalDescuento,
										totalMonto: objCalc.totalMonto,
										totalDescuento: objCalc.totalDescuento,
										totalIntereses: objCalc.totalIntereses,
										totalRecibir: objCalc.totalRecibir,
										sTipoLimite: objGral.sTipoLimite,
										svalorTC: objGral.valorTC,
										cboIf: objGral.valIF,
										cboEpo: objGral.valEPO,
										cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
										valCve:okResp,
										strUsr:strUsr,
										correo:'',
										email: '',
										Pkcs7: pkcs7,
										TextoFirmado: textoFirmar
									},
									callback: procesarSuccessConfirmaCesion
								});
							}
						}
					} else {
						NE.util.mostrarConnError(response,opts);
					}
				}
			});



		}

	}

	var resulValidCesion= function(okResp, siCorreo, email, errorMessage, vTipoUsuario){
		var registrosEnviar = [];
		if(okResp=='S'){
			if(vTipoUsuario!='NAFIN'){
				Ext.Ajax.request({
					url: '13forma1ext.data.jsp',
					params: {
						informacion: 'validaHorario',
						cboIf: objGral.valIF,
						cboEpo: objGral.valEPO
					},
					callback: function(opts, success, response) {
						if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
							var resp = 	Ext.util.JSON.decode(response.responseText);

							if(resp.msgError!=''){
								Ext.MessageBox.alert('Mensaje',resp.msgError);
							}else{
								if(resp.msgDiaSigHabil!=''){
									Ext.Msg.confirm('Confirmaci�n', resp.msgDiaSigHabil, function(btn){
										if(btn=='yes'){
											if(resp.msgOperFact24hrs!=''){
												Ext.MessageBox.alert('Mensaje',resp.msgOperFact24hrs);
											}else{
												storePreAcuData.each(function(record){
													registrosEnviar.push(record.data);
												});


												Ext.Ajax.request({
													url: '13forma1ext.data.jsp',
													params: {
														informacion: 'confirmaClaveCesion',
														registros: Ext.encode(registrosEnviar),
														totalDescuento: objCalc.totalDescuento,
														totalMonto: objCalc.totalMonto,
														totalDescuento: objCalc.totalDescuento,
														totalIntereses: objCalc.totalIntereses,
														totalRecibir: objCalc.totalRecibir,
														sTipoLimite: objGral.sTipoLimite,
														svalorTC: objGral.valorTC,
														cboIf: objGral.valIF,
														cboEpo: objGral.valEPO,
														cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
														valCve:okResp,
														correo:siCorreo,
														email: email
													},
													callback: procesarSuccessConfirmaCesion
												});
											}
										}
									});

								}else{
									storePreAcuData.each(function(record){
										registrosEnviar.push(record.data);
									});


									Ext.Ajax.request({
										url: '13forma1ext.data.jsp',
										params: {
											informacion: 'confirmaClaveCesion',
											registros: Ext.encode(registrosEnviar),
											totalDescuento: objCalc.totalDescuento,
											totalMonto: objCalc.totalMonto,
											totalDescuento: objCalc.totalDescuento,
											totalIntereses: objCalc.totalIntereses,
											totalRecibir: objCalc.totalRecibir,
											sTipoLimite: objGral.sTipoLimite,
											svalorTC: objGral.valorTC,
											cboIf: objGral.valIF,
											cboEpo: objGral.valEPO,
											cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
											valCve:okResp,
											correo:siCorreo,
											email: email
										},
										callback: procesarSuccessConfirmaCesion
									});
								}
							}
						} else {
							NE.util.mostrarConnError(response,opts);
						}
					}
				});
			}else{
				fnTransmitirDerecho(Ext.getCmp('btnTransmitir'), true);
			}
		}if(okResp=='R'){
			Ext.Msg.alert('Aviso', 'El folio de seguridad se envi� exitosamente al Sujeto de Apoyo', function(btn){
			var cboEpo = Ext.getCmp('cboEpo1');
			var record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
			var valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
			window.location.href='13forma1ext.jsp?tipoFact=V&cvePyme='+cvePyme.value+'&cboEpo='+valEPO+'&cboMoneda='+hidCboMoneda.value+"&strUsr="+strUsr;
			});
		}
	}
//-----------------------------HANDLERS--------------------------------------
	//FUNCION: determina valores que se necesitan al momento de cargar la pantalla por primera vez
	var procesarSuccessValidaSiTieneContrato = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);
			if(!resp.contratoAceptado){
				comboEpoVacio = true;
				var objMsg = Ext.getCmp('mensajes1');
					objMsg.body.update('<center>Debe solicitar al proveedor la aceptaci�n del clausulado electr�nico, para poder realizar esta operaci�n</center>');
					objMsg.show();

					iniRequest(storeCatEpoData);
			}else{

			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessValoresIni = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			var resp = 	Ext.util.JSON.decode(response.responseText);
			validaCder = resp.validaCder;
			
			objGral.inicial = 'N'
         objGral.icgrupo = resp.icgrupo;
			objGral.strTipoUsuario = resp.strTipoUsuario;
			objGral.pymeBloq = resp.pymeBloq;
			objGral.tipo_servicio = resp.tipo_servicio;
			objGral.urlCapturaDatos = resp.urlCapturaDatos;
			objGral.fecDiaSigHabil = resp.fecDiaSigHabil;
			objGral.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGral.msgOperFact24hrs = resp.msgOperFact24hrs;
			objGral.msgFecVencPyme = resp.msgFecVencPyme;
			objGral.iNoEPO = resp.iNoEPO;

			if( resp.bloqueoPymeEpo != 'B'  ){

				if(objGral.pymeBloq != 'S' && resp.msgError=='' &&  resp.bloqueoPymeEpo != 'B'  ){
				//alert(objGral.urlCapturaDatos);
					var valorInicialCboEpo = Ext.getDom("hidCboEpo").value;
					var cboEpo = Ext.getCmp('cboEpo1');

					if(cboEpo.getValue()=='' && valorInicialCboEpo == ''){
						if (storeCatEpoData.findExact("clave", objGral.iNoEPO) != -1 ) {
							cboEpo.setValue(objGral.iNoEPO);
						}
						Inicializacion.catalogoEpo = true;
					} else if (valorInicialCboEpo != ''){
						if (storeCatEpoData.findExact("clave", valorInicialCboEpo) != -1 ) {
							cboEpo.setValue(valorInicialCboEpo);
						}
						Inicializacion.catalogoEpo = true;
						if(NE.util.allTrue(Inicializacion)) {
							//Si cuando llega aqui ya estan inicializados los valores iniciales necesarios,
							//realiza la consulta. Esta secci�n solo aplica cuando la consulta recibe parametros iniciales
							consultar();
						}
					}

					if(objGral.pymeBloq=='N'){
						if(objGral.strTipoUsuario=='PYME' && objGral.icgrupo!=''){
							window.location(objGral.urlCapturaDatos);
						}
					}

					if(resp.msgError!=''){
						var objMsg = Ext.getCmp('mensajes1');
						objMsg.body.update(resp.msgError);
						objMsg.show();
						fp.hide();
					}else if(objGral.msgDiaSigHabil!='' || objGral.msgFecVencPyme!=''){
						var objMsg = Ext.getCmp('mensajes1');
						objMsg.body.update(objGral.msgDiaSigHabil+'<br>'+objGral.msgFecVencPyme);
						objMsg.show();
						fp.show();
						Ext.getCmp('cfFechaVenc').doLayout();

					}else{
						fp.show();
						Ext.getCmp('cfFechaVenc').doLayout();
					}

				}else{
					var objMsg = Ext.getCmp('mensajes1');

					if(resp.msgError!=''){
						objMsg.body.update(resp.msgError);
					}else{
						objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
								'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
								'Cd. De M�xico 50-89-61-07. <br>'+
								'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
					}

					objMsg.show();
					fp.hide();
				}

			}else{
				var objMsg = Ext.getCmp('mensajes1');
				objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
							'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
							'Cd. De M�xico 50-89-61-07. <br>'+
							'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
				objMsg.show();
				fp.hide();
			}



		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//FUNCION: asigna valores generales que se detrminan por la epo, if y moneda seleccionados,
	// esto sucede cada vez que se consulta o se hace un cambio en la seleccion del IF
	var procesarSuccessValoresGrales = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			if(resp.bloqueoPymeEpoIF=='S') {
				Ext.MessageBox.alert("Mensaje","Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado- IF. Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes Cd. De M�xico 50-89-61-07. Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)",
				function(){ 
					Ext.getCmp('cboIf1').setValue(''); 
				});
			} 
			
				var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				var panelMsgMotosMenos = Ext.getCmp('panelMsgMotosMenos');
				var cboIf = Ext.getCmp('cboIf1');				
				if (cboIf.getValue() !== ''){
					objGral.operaMontosMenores = resp.operaMontosMenores;
					
					if(objGral.operaMontosMenores==='N' ) {
						contenedorPrincipalCmp.add(panelMsgMotosMenos);		
						panelMsgMotosMenos.show();
					}else  if(objGral.operaMontosMenores==='S' ) {							
							panelMsgMotosMenos.hide();
					}
					contenedorPrincipalCmp.doLayout();						
				}
				
				objGral.msgDiaSigHabil = resp.msgDiaSigHabil;
				objGral.strTipoUsuario = resp.strTipoUsuario;
				objGral.pymeBloq = resp.pymeBloq;
				objGral.tipo_servicio = resp.tipo_servicio;
				objGral.urlCapturaDatos = resp.urlCapturaDatos;
				objGral.fecDiaSigHabil = resp.fecDiaSigHabil;
				objGral.msgDiaSigHabil = resp.msgDiaSigHabil;
				objGral.msgOperFact24hrs = resp.msgOperFact24hrs;
				objGral.msgFecVencPyme = resp.msgFecVencPyme;
				objGral.msgError = resp.msgError;
				objGral.sFecVencLineaCred = resp.sFecVencLineaCred;
	
				objGral.valorTC = resp.valorTC;
				objGral.paramEpoPef = resp.paramEpoPef;
				objGral.sLimiteActivo = resp.sLimiteActivo;
				objGral.numCamposDinamicos = resp.numCamposDinamicos;
				objGral.mapNombres = resp.mapNombres;
				objGral.operaNotasDeCredito = resp.operaNotasDeCredito;
				objGral.aplicarNotasDeCreditoAVariosDoctos = resp.aplicarNotasDeCreditoAVariosDoctos;
				objGral.bancoDeFondeo = resp.bancoDeFondeo;
				objGral.limites = resp.limites;
				objGral.sTipoLimite = resp.sTipoLimite;
				objGral.noTasasAceptada = resp.noTasasAceptada;
	
				//se asignan datos al objeto creado para el calculo de montos
				objCalc.totalMonto = 0;
				objCalc.totalDescuento = 0;
				objCalc.totalIntereses = 0;
				objCalc.totalRecibir = 0;
				objCalc.totaldocs = 0;
				objCalc.esBancoDeFondeoBANCOMEXT = (objGral.bancoDeFondeo=="BANCOMEXT" )?true:false;
				objCalc.tipoCambio = parseFloat(objGral.valorTC,10);
				objCalc.montoLimiteUtilizadoPorIFI = objGral.limites.LIMITE_UTILIZADO_POR_IFI?objGral.limites.LIMITE_UTILIZADO_POR_IFI:0;
				objCalc.montoLimiteMaximoPorIFI = objGral.limites.LIMITE_MAXIMO_POR_IFI?objGral.limites.LIMITE_MAXIMO_POR_IFI:0;
				objCalc.montoLimiteUtilizadoPorEPO = objGral.limites.LIMITE_UTILIZADO_POR_EPO?objGral.limites.LIMITE_UTILIZADO_POR_EPO:0;
				objCalc.montoLimiteMaximoPorEPO = objGral.limites.LIMITE_MAXIMO_POR_EPO?objGral.limites.LIMITE_MAXIMO_POR_EPO:0;
				objCalc.montoLimiteUtilizadoPorPYME = objGral.limites.LIMITE_UTILIZADO_POR_PYME?objGral.limites.LIMITE_UTILIZADO_POR_PYME:0;
				objCalc.montoLimiteMaximoPorPYME = objGral.limites.LIMITE_MAXIMO_POR_PYME?objGral.limites.LIMITE_MAXIMO_POR_PYME:0;
				objCalc.hayMontoLimiteParametrizadoPorPYME = objGral.limites.LIMITE_POR_PYME_PARAMETRIZADO?((objGral.limites.LIMITE_POR_PYME_PARAMETRIZADO=='true')?true:false):false;
				objCalc.hayMontoLimiteParametrizadoPorIFI = objGral.limites.LIMITE_POR_IFI_PARAMETRIZADO?((objGral.limites.LIMITE_POR_IFI_PARAMETRIZADO=='true')?true:false):false;
				objCalc.montoLimiteComprometidoPorPYME = objGral.limites.LIMITE_COMPROMETIDO_POR_PYME?objGral.limites.LIMITE_COMPROMETIDO_POR_PYME:0;
				objCalc.montoLimiteComprometidoPorEPO = objGral.limites.LIMITE_COMPROMETIDO_POR_EPO?objGral.limites.LIMITE_COMPROMETIDO_POR_EPO:0;
	
				var cboIf = Ext.getCmp('cboIf1');
	
				if(cboIf.getValue()!=''){
					if(objGral.msgDiaSigHabil=='' && objGral.msgFecVencPyme==''){
						var objMsg = Ext.getCmp('mensajes1');
						objMsg.hide();
					}else{
						var objMsg = Ext.getCmp('mensajes1');
						objMsg.body.update(objGral.msgDiaSigHabil+'<br>'+objGral.msgFecVencPyme);
						objMsg.show();
					}
					if(objGral.operaNotasDeCredito || objGral.aplicarNotasDeCreditoAVariosDoctos){
						objGral.icDoctosNotas = resp.icDoctosNotas;
						objGral.icNotasAp = resp.icNotasAp;
						if (!Ext.isEmpty(objGral.icDoctosNotas) || !Ext.isEmpty(objGral.icNotasAp)){
							Ext.getCmp('fpNotas').show();
						}
					}
				}
				storeDoctosData.loadData(resp);
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	var procesarSuccessDatCboIf = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			if(resp.bloqueoPymeEpo !='B'){

				if(resp.horarioValido){
					var aplicaOfertaTasa = resp.aplicaOfertaTasa;
					var cboIf = Ext.getCmp('cboIf1');

					cboIf.emptyText = resp.msgCboIf;

					if(aplicaOfertaTasa){
						var objMsg = Ext.getCmp('avisoOferta1');
						objMsg.body.update('Los IFs le ofrecen una Tasa especial para descontar sus documentos. Para conocer la oferta de clic <div id="aquiOferta">Aqu&iacute;</div>');

						objMsg.show();

						Ext.get('aquiOferta').on('click',
							function() {
								var formaHTML = fp.getForm().getEl().dom;
								if(objGral.strTipoUsuario=='NAFIN'){
									formaHTML.action = NE.appWebContextRoot + '/13descuento/13pki/13nafin/13seldocofertasa01ext.jsp?idMenu=13PYMESELDOCOF';
								}else{
									formaHTML.action = NE.appWebContextRoot + '/13descuento/13pyme/13seldocofertasa01ext.jsp?idMenu=13PYMESELDOCOF';
								}
								formaHTML.method = "POST";
								formaHTML.submit();
							}
						);
						//fp.hide();
					}

					cveIFtmp = resp.cveIFtmp;
					storeCatIfData.loadData(resp)
				}else{

					var objMsgAviso = Ext.getCmp('avisoOferta1');
					var panelInterm = Ext.getCmp('intermediariosS1');
					var panelTotales = Ext.getCmp('panelTotales1');
					if(objMsg)
						objMsg.hide();
					if(panelInterm)
						panelInterm.hide();
					if(panelTotales)
						panelTotales.hide();

					var objMsg = Ext.getCmp('mensajes1');
					objMsg.body.update("Servicio No Disponible por el momento. Intente mas tarde");
					objMsg.show();
					fp.hide();
					pnl.el.unmask();
				}

			}else  {
				var objMsg = Ext.getCmp('mensajes1');
				objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
								'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
								'Cd. De M�xico 50-89-61-07. <br>'+
								'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
				objMsg.show();
				fp.hide();
				var panelInterm = Ext.getCmp('intermediariosS1');
				var panelTotales = Ext.getCmp('panelTotales1');
				panelInterm.hide();
				panelTotales.hide();
				pnl.el.unmask();
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//FUNCION: despues de generar el combo IF, se encarga de realizar la consulta
	//de los documentos seleccionados en base a los criterios seleccionados
	var procesarConsulta = function(store, arrRegistros, opts) {
		var cboIf = Ext.getCmp('cboIf1');
		var panelInterm = Ext.getCmp('intermediariosS1');
		panelInterm.show();
		cboIf.show();
		if(store.getTotalCount()>0 &&  Ext.isEmpty(arrRegistros[0].data.loadMsg)){
			pnl.el.unmask();
			pnl.el.mask('Consultando...', 'x-mask-loading');

			var cboEpo = Ext.getCmp('cboEpo1');
			var record;
			var valEPO;
			var storeIf = cboIf.getStore();

			if(comboEpoVacio){
				valEPO = cveEpoOriginal+'|'+'SI ALFO MAS';
			}else{
				record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
				valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
			}

			objGral.valEPO = valEPO;

			var valIF='';
			if(cboIf.getValue()==''){
				//var record = storeIf.getAt(0);

				//cveIFtmp
				storeIf.each(function(record) {
					if(record.data['lsIf']==cveIFtmp){
						valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
					}
				});

				//valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
				//if(objGral.consultaIni != 'S')
					//cboIf.setValue(record.data['lsIf']);
			}else{
				var record = cboIf.findRecord(cboIf.valueField, cboIf.getValue());
				if(record){
					valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
				}else{
					var record = storeIf.getAt(0);
					valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
					//if(objGral.consultaIni != 'S')
						cboIf.setValue(record.data['lsIf']);
				}
			}

			objGral.valIF = valIF;

			Ext.Ajax.request({
				url: '13forma1ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'GeneraInfoCons',
					cboEpo: objGral.valEPO,
					cboIf: objGral.valIF
				}),
				callback: procesarSuccessValoresGrales
			});
			objGral.consultaIni = 'N';
		}else{
			cboIf.clearValue();
			pnl.el.unmask();
		}
	}

	//FUNCION: despues de cargar la informacion del grid, realiza la vaidacion en
	//base a la parametrizacion y tipo de usuario, para determinar los campos a mostrar
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		pnl.el.unmask();

		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridColumnMod = grid.getColumnModel();
		var strTipoUsuario = objGral.strTipoUsuario;
		var paramEpoPef = objGral.paramEpoPef;
		var numCamposDinamicos = objGral.numCamposDinamicos;
		var mapNombres = objGral.mapNombres;
		panelTotales.getForm().reset();

		if(objGral.sFecVencLineaCred!='S'){
			grid.show();
			panelTotales.show();
			if (arrRegistros != null) {
				if (!grid.isVisible()) {
					contenedorPrincipalCmp.add(grid);					
					contenedorPrincipalCmp.doLayout();
				}else{
					selectDefaultDoctos(grid);
				}

				var el = grid.getGridEl();
				if(store.getTotalCount() > 0) {
					//Logica para mostrar u ocultar columnas--------------------------------INI
						//se verifica si tipoUsuario es PYME y se checa si hay camposAdicionales
						if(strTipoUsuario=='PYME'){//campos adicionales del indice 12-16
							var indice = 12;
							for(var i=12;i<17;i++){
								gridColumnMod.setHidden(i,true);
							}

							for(var i=0; i<numCamposDinamicos; i++){
								var nombreColumna = eval("mapNombres.CAMPO"+i)
								gridColumnMod.setColumnHeader(indice,nombreColumna);
								gridColumnMod.setHidden(indice,false);
								indice = indice+1;
							}
						}
						//se verifica si la epo esta parametrizada como EpoPEF para mostrar cierto campos
						if(!paramEpoPef){//campos EPO PEF del indice 17-20
							for(var i=17;i<21;i++){
								gridColumnMod.setHidden(i,true);
							}
						}else{
							for(var i=17;i<21;i++){
								gridColumnMod.setHidden(i,false);
							}
						}
					//Logica para mostrar u ocultar columnas--------------------------------FIN
					Ext.getCmp('btnTerminar').enable();

					if(objGral.msgError!=''){
						var cboIf = Ext.getCmp('cboIf1');
						if(cboIf.getValue() != ''){
							Ext.getCmp('btnTerminar').disable();
							var objMsg = Ext.getCmp('mensajes1');
							objMsg.body.update(objGral.msgError);
							objMsg.show();

              var existe = (objGral.msgError).indexOf('Servicio No Disponible');
              var mensajeFinal = objGral.msgError;
              if(existe>=0)
                mensajeFinal = 'Fuera del horario de servicio para el Intermediario Financiero seleccionado, favor de seleccionar otro'

							Ext.MessageBox.alert('Mensaje',mensajeFinal);
						}
					}

					el.unmask();
				}else {
					Ext.getCmp('btnTerminar').disable();
					if(objGral.msgError!=''){

						var cboIf = Ext.getCmp('cboIf1');
						if(cboIf.getValue() != ''){
							Ext.getCmp('btnTerminar').disable();
							//var objMsg = Ext.getCmp('mensajes1');
							//objMsg.body.update(objGral.msgError);
							//objMsg.show();

              var existe = (objGral.msgError).indexOf('Servicio No Disponible');
              var mensajeFinal = objGral.msgError;
              if(existe>=0)
                mensajeFinal = 'Fuera del horario de servicio para el Intermediario Financiero seleccionado, favor de seleccionar otro'
							Ext.MessageBox.alert('Mensaje', mensajeFinal);
						}

						el.mask(objGral.msgError, 'x-mask');
					}else{
						if(objGral.noTasasAceptada){
							el.mask('No se ha encontrado una tasa certificada por EPO-IF-NAFIN"', 'x-mask');
						}else{
							el.mask('No se encontr� ning�n registro', 'x-mask');
						}

					}
				}
			}
		}else{
			grid.hide();
			panelTotales.hide();
			Ext.MessageBox.alert('Aviso','Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.\n	Por favor comun�quese al Centro de Atenci�n a Clientes al tel�fono 50-89-61-07 � \n del interior de la Republica  al 01-800-NAFINSA (01800 623-4672).')
		}
	}

	//FUNCION: selecciona los documentos por deafult debido a las notas de credito
	var selectDefaultDoctos = function(grid){
		var indiceSm = 0;
		grid.getStore().each(function(record) {
			if(record.data['SELECBOOL']){
				if(record.data['SELECNOTAS']!=''){
					selectModel.selectRow(indiceSm,true);
					//record.data['SELECCION']=true;
				}
			}
			indiceSm = indiceSm+1;
		});
	}


	var procesarSuccessConfirmaCesion = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');


			if(resp.msgError=='' && resp.esCveCorrecta){

				var acuseCifras = [
					['N�mero de Acuse', resp.objCifras.numAcuse],
					['Fecha de Carga', resp.objCifras.fecCarga],
					['Hora de Carga', resp.objCifras.horaCarga],
					['Usuario de Captura', resp.objCifras.captUser]
				];

				storeCifrasData.loadData(acuseCifras);

				//se muestran componentes para el acuse de la seleccion de doctos

				if(strUsuario=='NAFIN'){
					panelMsgAcuse.html='<p align="center"><b>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br>Recibo: '+resp._acuse+'</b></p>';
					contenedorPrincipalCmp.insert(1,panelMsgAcuse);
				}
				contenedorPrincipalCmp.findById('gridDoctosPreAcu').setTitle('Acuse - Selecci�n Documentos');
				contenedorPrincipalCmp.remove(panelMsgLegal);

				contenedorPrincipalCmp.insert(2,gridCifrasCtrl);
				contenedorPrincipalCmp.insert(3, panelMsgCifrado);
				contenedorPrincipalCmp.insert(4, NE.util.getEspaciador(10));
				contenedorPrincipalCmp.doLayout();
				//se muestran botones para salir o generar acuse en pdf
				Ext.getCmp('btnRevisar').hide();
				Ext.getCmp('btnAbrirPDF').show();
				Ext.getCmp('btnTransmitir').hide()
				Ext.getCmp('btnSalir').show();
				Ext.getCmp('btnCancelar').hide();

				Ext.getCmp('btnAbrirPDF').setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = resp.urlArchivo;
					forma.submit();
				});

			}else if(resp.msgError!='' && !resp.esCveCorrecta){
				Ext.MessageBox.alert('Aviso',resp.msgError);
			}else if(resp.msgError!=''){

			}


		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarNotaSimple = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var elG = gridNotaSimple.getGridEl();
			if(store.getTotalCount() > 0) {
				/*var totalNotas = 0;
				var saldo = 0;
				var jsonData = store.reader.jsonData.registros;
				Ext.each(jsonData, function(registro, index, arrItems){
					totalNotas	+= parseFloat(registro.MONTONOTA);
					saldo 		= parseFloat(registro.MONTODOCTO) - totalNotas;
				});
				var reg = gridTotalNotasData.getAt(0);
				reg.set('TOTAL_MONTO',totalNotas);
				reg.set('SALDO_DOCTO',saldo);*/
				elG.unmask();
			}else{
				elG.mask('No encontro ning�n cambio para este documento');
			}
		}
	}

	var procesarNotaVarias = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var elG = gridNotaVarias.getGridEl();
			if(store.getTotalCount() > 0) {
				elG.unmask();
			}else{
				elG.mask('No encontro ning�n registro');
			}
		}
	}

	var procesarGenerarPdfNotas =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfNotas');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNotas');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	//STORES---------------------------------------------------------------------

	var gridTotalNotasData = new Ext.data.JsonStore({
		fields:	[{name: 'TOTAL_MONTO',type: 'float'},{name: 'SALDO_DOCTO',type: 'float'}],
		data:		[{'TOTAL_MONTO':0,'SALDO_DOCTO':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridNotaSimpleData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '13forma1ext.data.jsp',
		baseParams: {
			informacion: 'obtenNotaSimple'
		},
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields:	[{name: 'NUMNOTA'},
					{name: 'FECHAEMISION',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'MONEDA'},
					{name: 'DOCTOAPLICADO'},
					{name: 'DOCTOAPLICADOMONTO'},
					{name: 'MONTONOTA',	type: 'float'},
					{name: 'MONTODOCTO',type: 'float'},
					{name: 'SALDO',	type: 'float'},
					{name: 'TIPOFACTORAJE'}
					]
		}),
		groupField: 'DOCTOAPLICADOMONTO',	//sortInfo:{field: 'DOCTOAPLICADOMONTO', direction: "DESC"},
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
				Ext.apply(options.params, {icDocto:objGral.icDoctosNotas,icNotas: objGral.icNotasAp});
				}
			},
			load: procesarNotaSimple,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarNotaSimple(null, null, null);
				}
			}
		}
	});

	var gridNotaVariasData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '13forma1ext.data.jsp',
		baseParams: {
			informacion: 'obtenNotaVarias'
		},
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'NOTA_MONTO_CONCAT'},
			{name: 'IG_NUMERO_DOCTO_CREDITO'},
			{name: 'IG_NUMERO_DOCTO_DOCTO'},
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO',type: 'float'},
			{name: 'MONEDA'},
			{name: 'MONTO_DOCTO',type: 'float'},
			{name: 'MONTO_APLICA',	type: 'float'},
			{name: 'SALDO_DOCTO',	type: 'float'},
			{name: 'TIPO_FACTORAJE'}
      ]
		}),
		groupField: 'NOTA_MONTO_CONCAT',	//sortInfo:{field: 'NOTA_MONTO_CONCAT', direction: "ASC"},
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			load: procesarNotaVarias,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarNotaVarias(null, null, null);
				}
			}
		}
	});

	var storeCatIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['lsIf', 'lsNombreCuenta', 'lsTipoPiso','rs_tipo_lim','loadMsg'],
		//url : '13forma1ext.data.jsp',
		/*baseParams: {
			informacion: 'ConsultarGeneraCboIf'
		},*/
		totalProperty : 'totalCount',
		autoLoad: false,
		listeners: {
			load: procesarConsulta,
			exception: NE.util.mostrarDataProxyError
			//beforeload: NE.util.initMensajeCargaCombo
		}
	});


	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'catalogoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, option){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						if(objGral.inicial=='S'){
							iniRequest(store);
						}
					}else if(store.getTotalCount()<1 ){
						cveEpoOriginal = Ext.getDom("hidCboEpo").value;
						validaContratoEpoPyme(cveEpoOriginal);
					}
			}
		}
	});

	var storeCatMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store, records, oprion){
				if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
					var valorInicialCboMoneda = Ext.getDom("hidCboMoneda").value;
					var cboMoneda = Ext.getCmp('cboMoneda1');
					if(cboMoneda.getValue()=='' && valorInicialCboMoneda == ''){
						cboMoneda.setValue(records[0].data['clave']);
						Inicializacion.catalogoMoneda = true;
					} else if (valorInicialCboMoneda != '') {
						cboMoneda.setValue(valorInicialCboMoneda);
						Inicializacion.catalogoMoneda = true;
						if(NE.util.allTrue(Inicializacion)) {
							//Si cuando llega aqui ya estan inicializados los valores iniciales necesarios,
							//realiza la consulta. Esta secci�n solo aplica cuando la consulta recibe parametros iniciales
							consultar();
						}
					}
				}
			}
		}
	});


	var storeDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		fields: [
			{name: 'NUMDOCTO'},
			{name: 'FECEMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECVENC',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO'},
			{name: 'TASA'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORCDESC', type: 'float'},
			{name: 'MONTODESC', type: 'float'},
			{name: 'SELECCION'},  //--   aqui va el check de seleccion
			{name: 'SELECBOOL'},
			{name: 'SELECHID'},
			{name: 'SELECNOTAS'},
			{name: 'APLICADO'},
			{name: 'IMPORTINT', type: 'float'},
			{name: 'IMPORTREC', type: 'float'},
			{name: 'CAMPO0'},
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'FECRECEP'},
			{name: 'TIPOCOMPRA'},
			{name: 'CLASIFICADOR'},
			{name: 'PLAZOMAX'},
			{name: 'ENTIDAD_GOBIERNO'},
			{name: 'TIPO_USUARIO'},
			{name:'SELEMONTOMINI'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}

	});

	var storePreAcuData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMDOCTO'},
			{name: 'FECEMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECVENC',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO'},
			{name: 'TASA'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORCDESC', type: 'float'},
			{name: 'MONTODESC', type: 'float'},
			{name: 'SELECCION'},  //--   aqui va el check de seleccion
			{name: 'SELECBOOL'},
			{name: 'SELECHID'},
			{name: 'SELECNOTAS'},
			{name: 'APLICADO'},
			{name: 'IMPORTINT', type: 'float'},
			{name: 'IMPORTREC', type: 'float'},
			{name: 'CAMPO0'},
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'FECRECEP'},
			{name: 'TIPOCOMPRA'},
			{name: 'CLASIFICADOR'},
			{name: 'PLAZOMAX'},
			{name: 'ENTIDAD_GOBIERNO'}	,
			{name: 'TIPO_USUARIO'}
		]
	});

	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });

	var storeTotalData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'EPO'},
			  {name: 'TOTALMONTO', type: 'float'},
			  {name: 'TOTALDESCTO', type: 'float'},
			  {name: 'TOTALINTERES', type: 'float'},
			  {name: 'TOTALIMPORT', type: 'float'}
		  ]
	 });

	//COMPONENTES---------------------------------------------------------
	var elementosForma = [
		{
			xtype: 'displayfield',
			fieldLabel: 'No. Nafin El�ctronico Pyme',
			value: strUsuario=='NAFIN'?(Ext.getDom("strNePymeAsigna").value+' '+Ext.getDom("strNombrePymeAsigna").value):'',
			hidden: strUsuario=='NAFIN'?false:true
		},
		{
			xtype: 'combo',
			name: 'cboEpo',
			id: 'cboEpo1',
			fieldLabel: 'EPO',
			mode: 'local',
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEpo',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEpoData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'cboMoneda',
			id: 'cboMoneda1',
			fieldLabel: 'Moneda',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMoneda',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			id: 'cfFechaVenc',
			fieldLabel: 'Fecha Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txtFechaVencDe',
					id: 'txtFechaVencDe',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecha: 'txtFechaVenca',
					margins: '0 20 0 0',
					value: Ext.getDom('hidTxtFechaVencDe').value
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtFechaVenca',
					id: 'txtFechaVenca',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'txtFechaVencDe',
					margins: '0 20 0 0',
					value: Ext.getDom('hidTxtFechaVenca').value
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		},{
			xtype: 'panel',
			id:	'fpNotas',
			hidden: true,
			layout:  'form',
			buttonAlign: 'center',
			html: '<div><center><b>Se&nbsp;aplicaran&nbsp;Notas&nbsp;de&nbsp;Cr&eacute;dito&nbsp;a&nbsp;sus&nbsp;documentos,&nbsp;para</br>'+
					'&nbsp;consultarlos&nbsp;presione&nbsp;el&nbsp;boton</b></br></div>',
			buttons: [
				{
					id: 'btnNotas',
					tooltip: 'Notas de Cr�dito',
					iconCls: 'icoBuscar',
					text: 'Notas de Cr�dito',
					handler: function() {
						if(objGral.operaNotasDeCredito && !objGral.aplicarNotasDeCreditoAVariosDoctos){
							doctoAnt = "";
							totalSaldo = 0;
							var ventana = Ext.getCmp('verNotas');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									modal: true,
									resizable: false,
									layout: 'form',
									x: 300,
									width: 667,
									height: 310,
									id: 'verNotas',
									closeAction: 'hide',
									items: [gridNotaSimple],
									title: 'Cr�ditos'
								}).show();
							}
							gridNotaSimpleData.load();

						}else if(objGral.operaNotasDeCredito && objGral.aplicarNotasDeCreditoAVariosDoctos){

							var ventana = Ext.getCmp('winNotas');
							var btnPdfNotas = Ext.getCmp('btnPdfNotas');
							var btnBajarPdfNotas = Ext.getCmp('btnBajarPdfNotas');
							if (ventana) {
								btnPdfNotas.enable();
								btnBajarPdfNotas.hide();
								ventana.show();
							}else{
								new Ext.Window({
									modal: true,
									resizable: false,
									x: 200,
									width: 800,
									height: 360,
									id: 'winNotas',
									closeAction: 'hide',
									items: [gridNotaVarias],
									title: 'Cr�ditos',
									bbar: {
										xtype: 'toolbar',
										buttons: ['->','-',{xtype: 'button',text: 'Generar PDF',	id: 'btnPdfNotas'},{xtype: 'button',text: 'Bajar PDF',id: 'btnBajarPdfNotas',hidden: true}]
									}
								}).show();
							}

							var btnGenerar = Ext.getCmp('btnPdfNotas');
							btnGenerar.setHandler(
								function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
									Ext.Ajax.request({
										url: '/nafin/13descuento/13pyme/13consulta02ext_not_pdf.jsp',
										callback: procesarGenerarPdfNotas
									});
								}
							);
							gridNotaVariasData.load();
						}
					}
				}
			]
		}
	]

	var selectModel = new Ext.grid.CheckboxSelectionModel({
        checkOnly: true,
		  renderer: function(v, p, record){
				if (record.data['SELECBOOL']){										
					
					if(objGral.operaMontosMenores=='N' && parseFloat(record.data['IMPORTINT'])<0.01 ){
						record.data['SELEMONTOMINI']='N';						
						return '<div>&#160;</div>';											
						
					}else  {
						return '<div class="x-grid3-row-checker">&#160;</div>';
					}	
					
				}else{					
					return '<div>&#160;</div>';
					
				}
			},
		  listeners: {
            rowselect: function(selectModel, rowIndex, record) {
					 if(record.data['SELECCION']!='S')
						if(recalcular(selectModel, record, rowIndex, true))
							record.data['SELECCION']='S';
            },
				rowdeselect: function(selectModel, rowIndex, record) {
					var cboIf = Ext.getCmp('cboIf1');


					if (cboIf.getValue() != ''){
						if(record.data['SELECBOOL'] && record.data['SELECNOTAS']!='' && record.data['SELECCION']=='S'){
							selectModel.selectRow(rowIndex,true);
						}else{
						 if(record.data['SELECCION']!='N')
							if(recalcular(selectModel, record, rowIndex, false))
								record.data['SELECCION']='N';
						}
					}
            },
				beforerowselect: function( selectModel, rowIndex, keepExisting, record ){
					var mensaje = '';
					if(record.data['SELECBOOL']){
						if( record.data['SELEMONTOMINI']==='S'   ){
							if(objGral.sLimiteActivo=='N'){
								mensaje 	=	"Por el momento no se puede realizar el descuento del documento con el Intermediario seleccionado.\n\n"+
									"Por favor comuniquese al Centro de Atencion a Clientes al telefono 50-89-61-07 o\n"+
									"del interior al 01-800-NAFINSA (01-800-6234672).";
	
							}else if(objGral.msgOperFact24hrs!=''){
								mensaje = objGral.msgOperFact24hrs;
	
							}else if(record.data['ENTIDAD_GOBIERNO']=='S' &&  record.data['TIPO_USUARIO']=='NAFIN' ){
								mensaje = 'No se puede operar el documento por que el Proveedor es Entidad de Gobierno';
	
							}else if(record.data['ENTIDAD_GOBIERNO']=='S' &&  record.data['TIPO_USUARIO']=='PYME' ){
								mensaje = 'No es posible realizar el descuento del documento. Por favor comun�quese al Centro de Atenci�n a Clientes Cd. M�xico 50-89-61-07. Sin costo desde el interior al 01-800-NAFINSA (01-800-623-4672)';
							}
						}
					}else{
						mensaje  = 'No es posible seleccionar el documento';
					}

					if(mensaje!=''){
						Ext.MessageBox.alert('Aviso',mensaje);
						return false;
					}
				}
        }
    });

	var consultar = function(boton, evento) {
		pnl.el.mask('Consultando...', 'x-mask-loading');
		grid.hide();
		Ext.getCmp('fpNotas').hide();
		var cboEpo = Ext.getCmp('cboEpo1');
		var txtFechaVencDe = Ext.getCmp('txtFechaVencDe');
		var txtFechaVenca = Ext.getCmp('txtFechaVenca');
		
		Ext.getCmp('panelMsgMotosMenos').hide();
		

		var record;
		var valEPO;

		/*storeCatIfData.load({
			params: Ext.apply(fp.getForm().getValues(),{
				cboEpo: valEPO
			})
		});*/
		if(comboEpoVacio){
			valEPO = cveEpoOriginal+'|'+'SI ALFO MAS';
		}else{
			record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
			valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
		}

		Ext.Ajax.request({
			url: '13forma1ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ConsultarGeneraCboIf',
				cboEpo: valEPO,
				cvePyme: Ext.getDom("cvePyme").value,
				cveEpoOriginal: cveEpoOriginal
			}),
			callback: procesarSuccessDatCboIf
		});
	}

	//CONTENEDORES--------------------------------------------------------

	var gridTotalNotas = {
		xtype: 'grid',
		store: gridTotalNotasData,
		id: 'gridTotalNotas',
		hidden:	false,
		columns: [
			{header: 'Monto Total de Notas', tooltip:'Monto Total de Notas',dataIndex: 'TOTAL_MONTO',width: 325,	align: 'right',renderer: Ext.util.Format.numberRenderer('$ 0,0.00')},
			{header: 'Saldo del documento',tooltip:'Saldo del documento',dataIndex: 'SALDO_DOCTO',width: 325,	align: 'right',renderer: Ext.util.Format.numberRenderer('$ 0,0.00')}
		],
		view: new Ext.grid.GridView({forceFit:true,markDirty: false}),
		columnLines:true,
		width: 655,
		height: 50,
		//title: 'Totales',
		frame: false
	};

	// define a custom summary function
    Ext.ux.grid.GroupSummary.Calculations['totalSaldo'] =  function(value, record, field){
					var doctoAplicado = record.data.DOCTOAPLICADOMONTO
					if (doctoAnt != doctoAplicado){
						totalSaldo = 0;
					}
					totalSaldo += record.data.MONTONOTA;
					doctoAnt = doctoAplicado;
					return record.data.MONTODOCTO-totalSaldo;
    };

	var summary = new Ext.ux.grid.GroupSummary();

	var gridNotaSimple = new Ext.grid.GridPanel({
		id: 'gridNotaSimple',
		store: gridNotaSimpleData,
		columns: [
			{	header: 'No. Docto. Aplicado',tooltip: 'No. Docto. Aplicado',	dataIndex: 'DOCTOAPLICADOMONTO',	width: 150,	align: 'left', hidden: true, 	hideable: false},
			{	header: 'No. Nota',tooltip: 'No. Nota',	dataIndex: 'NUMNOTA',	width: 130,	align: 'left',
				summaryRenderer: function(value, summaryData, dataIndex) {
					 return 'Saldo del documento';
				}
			},
			{	header: 'Fecha de Emisi�n',tooltip: 'Fecha de Emisi�n',	dataIndex: 'FECHAEMISION',	sortable : true, width : 120, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				summaryType: 'totalSaldo',
				summaryRenderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},
			{	header: 'Moneda',tooltip: 'Moneda',	dataIndex: 'MONEDA',	width: 140,	align: 'left',
				summaryRenderer: function(value, summaryData, dataIndex) {
					 return 'Monto Total de Notas';
				}
			},
			{	header: 'Monto',tooltip: 'Monto',	dataIndex: 'MONTONOTA',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),
				summaryType: 'sum',
				summaryRenderer: function(value, summaryData, dataIndex) {
					 return Ext.util.Format.number(value,'$ 0,0.00');
				}
			},
			{	header: 'Tipo Factoraje',tooltip: 'Tipo Factoraje',	dataIndex: 'TIPOFACTORAJE',	width: 150,	align: 'left'}
		],
		stripeRows: true,
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
		plugins: summary,
		deferRowRender: false,
		columnLines:true,
		loadMask: true,
		monitorResize: true,
		height: 280,
		width: 655,
		title: '',
		frame: false,
		hidden: false
	});

	var gridNotaVarias = new Ext.grid.GridPanel({
		id: 'gridNotaVarias',
		store: gridNotaVariasData,
		columns: [
			{header: 'No. Nota',tooltip: 'No. Nota',	dataIndex: 'NOTA_MONTO_CONCAT',	width: 250,	align: 'left', hidden:true, hideable:false},
			{header: 'No. Docto.',tooltip: 'No. Docto.',	dataIndex: 'IG_NUMERO_DOCTO_DOCTO',	width: 120,	align: 'left'},
			{header: 'Fecha de Emisi�n',tooltip: 'Fecha de Emisi�n',	dataIndex: 'FECHA_EMISION',	sortable : true, width : 120, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Moneda',tooltip: 'Moneda',	dataIndex: 'MONEDA',	width: 140,	align: 'left'},
			{header: 'Monto Docto',tooltip: 'Monto Docto',	dataIndex: 'MONTO_DOCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Aplicado',tooltip: 'Monto Aplicado',	dataIndex: 'MONTO_APLICA',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Saldo Docto',tooltip: 'Saldo Docto',	dataIndex: 'SALDO_DOCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Tipo Factoraje',tooltip: 'Tipo Factoraje',	dataIndex: 'TIPO_FACTORAJE',	width: 140,	align: 'left'}
		],
		stripeRows: true,
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
		deferRowRender: false,
		loadMask: true,
		monitorResize: true,
		height: 300,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

	var gridCifrasCtrl = new Ext.grid.GridPanel({
	id: 'gridCifrasCtrl1',
	store: storeCifrasData,
	margins: '20 0 0 0',
	hideHeaders : true,
	columns: [
		{
			header : 'Etiqueta',
			dataIndex : 'etiqueta',
			width : 150,
			sortable : true
		},
		{
			header : 'Informacion',
			tooltip: 'Nombre Beneficiario',
			dataIndex : 'informacion',
			width : 230,
			sortable : true,
			renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 400,
	style: 'margin:0 auto;',
	autoHeight : true,
	title: 'Cifras de Control',
	frame: true
	});


	var gridTotales = new Ext.grid.GridPanel({
	id: 'gridTotales1',
	store: storeTotalData,
	margins: '20 0 0 0',
	columns: [
		{
			header : 'EPO',
			dataIndex : 'EPO',
			width : 200,
			sortable : true,
			renderer:  function (causa, columna, registro){
						var cboEPO = Ext.getCmp('cboEpo1');
						var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
						causa = recordEPO.get(cboEPO.displayField);
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
		},
		{
			header : 'PYME',
			dataIndex : 'PYME',
			width : 200,
			sortable : true,
			hidden: strUsuario=='NAFIN'?false:true,
			renderer:  function (causa, columna, registro){
					if(strUsuario=='NAFIN'){
						causa = Ext.getDom("strNombrePymeAsigna").value;
					}
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
			}
		},
		{
			header : 'Total Monto Documento',
			tooltip: 'Total Monto Documento',
			dataIndex : 'TOTALMONTO',
			width : 169,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Total Monto Descuento',
			tooltip: 'Total Monto Descuento',
			dataIndex : 'TOTALDESCTO',
			width : 166,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Total Monto Interes',
			tooltip: 'Total Monto Interes',
			dataIndex : 'TOTALINTERES',
			width : 166,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header : 'Total Importe Recibir',
			tooltip: 'Total Importe Recibir',
			dataIndex : 'TOTALIMPORT',
			width : 166,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 100,
	width: 900,
	style: 'margin:0 auto;',
	title: ' ',
	frame: true
	});


	var gridPreAcu = new Ext.grid.GridPanel({
	id: 'gridDoctosPreAcu',
	store: storePreAcuData,
	margins: '20 0 0 0',
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [
		{//1
			header: 'IF Seleccionado',
			tooltip: 'IF Seleccionado',
			dataIndex: 'IFSELECT',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
						var cboIFs = Ext.getCmp('cboIf1');
						var recordIF = cboIFs.findRecord(cboIFs.valueField, cboIFs.getValue());
						causa = recordIF.get(cboIFs.displayField);
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
		},
		{//2
			header: 'EPO',
			tooltip: 'EPO',
			dataIndex: 'EPOSELECT',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left',
			renderer:  function (causa, columna, registro){
						var cboEPO = Ext.getCmp('cboEpo1');
						var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
						causa = recordEPO.get(cboEPO.displayField);
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}

		},
		{//3 -- se queda pendiente para la parte NAFIN
			header: 'PYME',
			tooltip: 'PYME',
			dataIndex: 'PYMESELECT',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: strUsuario=='NAFIN'?false:true,
			renderer:  function (causa, columna, registro){
						if(strUsuario=='NAFIN'){
							causa = Ext.getDom("strNombrePymeAsigna").value;
						}
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
						}
		},
		{//4
			header: 'N�mero de Documento',
			tooltip: 'N�mero de Documento',
			dataIndex: 'NUMDOCTO',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left'
		},
		{//5
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			sortable : true
		},
		{//6
			header : 'Monto Documento',
			tooltip: 'Monto',
			dataIndex : 'MONTO',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//7
			header : 'Porcentaje de Descuento',
			tooltip: 'Porcentaje de Descuento',
			dataIndex : 'PORCDESC',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('%0,0.00')
		},
		{//8
			header : 'Monto a Descontar',
			tooltip: 'Monto a Descontar',
			dataIndex : 'MONTODESC',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//9
			header : 'Importe de Intereses',
			tooltip: 'Importe de Intereses',
			dataIndex : 'IMPORTINT',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//10
			header : 'Importe a Recibir',
			tooltip: 'Importe a Recibir',
			dataIndex : 'IMPORTREC',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : 'Campo 1',
			tooltip: 'Adicional 1',
			dataIndex : 'CAMPO0',
			sortable : true,
			hidden: true,
			width : 100,
			align: 'center'
		},
		{//12
			header : 'Campo 2',
			tooltip: 'Adicional 2',
			dataIndex : 'CAMPO1',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//13
			header : 'Campo 3',
			tooltip: 'Adicional 3',
			dataIndex : 'CAMPO2',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//14
			header : 'Campo 4',
			tooltip: 'Adicional 4',
			dataIndex : 'CAMPO3',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//15
			header : 'Campo 5',
			tooltip: 'Adicional 5',
			dataIndex : 'CAMPO4',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//16
			header : 'Fecha de Recepci�n',
			tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
			dataIndex : 'FECRECEP',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//17
			header : 'Tipo de Compra',
			tooltip: 'Tipo de Compra (Procedimientos)',
			dataIndex : 'TIPOCOMPRA',
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//18
			header : 'Clasificador por Objeto del Gasto',
			tooltip: 'Clasificador por Objeto del Gasto',
			dataIndex : 'CLASIFICADOR',
			sortable : true,
			width : 80,
			align: 'center'
		},
		{//19
			header : 'Plazo M�ximo',
			tooltip: 'Plazo M�ximo',
			dataIndex : 'PLAZOMAX',
			width : 80,
			sortable : true,
			align: 'center'
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	style: 'margin:0 auto;',
	width: 900,
	title: 'PRE ACUSE - SELECCION DE DOCUMENTOS',
	frame: true,
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Revisar',
			id: 'btnRevisar',
			handler: regresaAseleccion
			},
			{
			text: 'Generar PDF',
			id: 'btnGenerarPDF',
			hidden: true
			//handler:
			},
			{
			text: 'Abrir PDF',
			id: 'btnAbrirPDF',
			hidden: true
			//handler:
			},
			'-',
			{
			text: 'Transmitir Derecho',
			id: 'btnTransmitir',
			handler:  fnTransmitirDerecho
			},
			{
			text: 'Salir',
			id: 'btnSalir',
			hidden: true,
			handler: function(){
				var cboEpo = Ext.getCmp('cboEpo1');
				var record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
				var valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
				window.location.href='13forma1ext.jsp?tipoFact=V&cvePyme='+cvePyme.value+'&cboEpo='+valEPO+'&cboMoneda='+hidCboMoneda.value+"&strUsr="+strUsr;

				}
			},
			'-',
			{
			text: 'Cancelar',
			id: 'btnCancelar',
			handler: function(){
					Ext.Msg.confirm('Confirmaci�n', '�Est� usted seguro de cancelar la operaci�n?', function(btn){
						if(btn=='yes'){
							//window.location.href='13forma1ext.jsp';
							var cboEpo = Ext.getCmp('cboEpo1');
							var record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
							var valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
							window.location.href='13forma1ext.jsp?tipoFact=V&cvePyme='+cvePyme.value+'&cboEpo='+valEPO+'&cboMoneda='+hidCboMoneda.value+"&strUsr="+strUsr;


						}
					});
				}
			}

		]
	}
	});

	var grid = new Ext.grid.EditorGridPanel({
	id: 'gridDoctos',
	store: storeDoctosData,
	margins: '20 0 0 0',
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      },
	  getRowClass: function (record, rowIndex, rowParams, store) {	  
		if(record.get('SELEMONTOMINI') === 'N'){				
			return 'user-mci';
		}
	  }
   },
	sm: selectModel,	
	columns: [
		selectModel,		
		{//1
			header: 'N�mero Documento',
			tooltip: 'N�mero Documento',
			dataIndex: 'NUMDOCTO',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false			
		},
		{//2
			header: 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex: 'FECEMISION',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//3
			header: 'Fecha de Vencimiento',
			tooltip: 'Fecha de Vencimiento',
			dataIndex: 'FECVENC',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//4
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO',
			sortable : true,
			width : 100,
			align: 'right'
		},
		{//5
			header : 'Tasa a Aplicar',
			tooltip: 'Tasa',
			dataIndex : 'TASA',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('%0,0.00000')
		},
		{//6
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			sortable : true
		},
		{//7
			header : 'Monto',
			tooltip: 'Monto',
			dataIndex : 'MONTO',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//8
			header : 'Porcentaje de Descuento',
			tooltip: 'Porcentaje de Descuento',
			dataIndex : 'PORCDESC',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('%0,0.00')
		},
		{//9
			header : 'Monto a Descontar',
			tooltip: 'Monto a Descontar',
			dataIndex : 'MONTODESC',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : 'Importe de Intereses',
			tooltip: 'Importe de Intereses',
			dataIndex : 'IMPORTINT',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//12
			header : 'Importe a Recibir',
			tooltip: 'Importe a Recibir',
			dataIndex : 'IMPORTREC',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//13
			header : 'Campo 1',
			tooltip: 'Adicional 1',
			dataIndex : 'CAMPO0',
			sortable : true,
			hidden: true,
			width : 100,
			align: 'center'
		},
		{//14
			header : 'Campo 2',
			tooltip: 'Adicional 2',
			dataIndex : 'CAMPO1',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//15
			header : 'Campo 3',
			tooltip: 'Adicional 3',
			dataIndex : 'CAMPO2',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//16
			header : 'Campo 4',
			tooltip: 'Adicional 4',
			dataIndex : 'CAMPO3',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//17
			header : 'Campo 5',
			tooltip: 'Adicional 5',
			dataIndex : 'CAMPO4',
			hidden: true,
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//18
			header : 'Fecha de Recepci�n',
			tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
			dataIndex : 'FECRECEP',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//19
			header : 'Tipo de Compra',
			tooltip: 'Tipo de Compra (Procedimientos)',
			dataIndex : 'TIPOCOMPRA',
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//20
			header : 'Clasificador por Objeto del Gasto',
			tooltip: 'Clasificador por Objeto del Gasto',
			dataIndex : 'CLASIFICADOR',
			sortable : true,
			width : 80,
			align: 'center'
		},
		{//21
			header : 'Plazo M�ximo',
			tooltip: 'Plazo M�ximo',
			dataIndex : 'PLAZOMAX',
			width : 80,
			sortable : true,
			align: 'center'
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 900,
	style: 'margin:0 auto;',
	title: '',
	frame: true,
	listeners: {
		viewReady: selectDefaultDoctos
	},
	bbar: {
		xtype: 'toolbar',
		items: [
			'->',
			'-',
			{
			text: 'Terminar',
			id: 'btnTerminar',
			handler: terminar
			}
		]
	}
	});

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: 'margin:0 auto;',
		title: 'Selecci�n Documentos',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				id:'btnConsultar',
				formBind: true,
				handler: consultar
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('intermediariosS1').hide();
					Ext.getCmp('panelTotales1').hide();
					Ext.getCmp('gridDoctos').hide();
					Ext.getCmp('avisoOferta1').hide();
					Ext.getCmp('gridTotales1').hide();
					Ext.getCmp('pnmsgLegal1').hide();
					Ext.getCmp('cboMoneda1').setValue('');
					Ext.getCmp('cboEpo1').setValue('');					
					Ext.getCmp('panelMsgMotosMenos').hide();					
				}

			}
		]
	});

	var panelTotales = new Ext.form.FormPanel({
		name: 'panelTotales',
		id: 'panelTotales1',
		title:'-',
		width: 900,
		height: 90,
		style: 'margin:0 auto;',
		frame: true,
		layout:'absolute',
		defaultType: 'textfield',
      labelWidth: 0,
		items:[
			{
			  xtype:'label',
			  y:5,
			  x:142,
			  text:'Total Documentos',
			  style:'color:#777777'
			},
			{
			  xtype:'label',
			  y:5,
			  x:262,
			  text:'Monto',
			  style:'color:#777777'
			},
			{
			  xtype:'label',
			  y:5,
			  x:382,
			  text:'Monto a Descontar',
			  style:'color:#777777'
			},
			{
			  xtype:'label',
			  y:5,
			  x:502,
			  text:'Importe de Interes',
			  style:'color:#777777'
			},
			{
			  xtype:'label',
			  y:5,
			  x:622,
			  text:'Importe a Recibir',
			  style:'color:#777777'
			},
			{
			  name: 'totalDoctosDesp', // campo ID
			  id:'totalDoctosDesp1',
			  x:142,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			{
			  name: 'totalMontoDesp', // campo ID
			  id:'totalMontoDesp1',
			  x:262,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {
			  name: 'totalMontoDescDesp', // campo ID
			  id:'totalMontoDescDesp1',
			  x:382,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {
			  name: 'totalImpIntDesp', // campo ID
			  id:'totalImpIntDesp1',
			  x:502,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {
			  name: 'totalImpRecibDesp', // campo ID
			  id:'totalImpRecibDesp1',
			  x:622,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 }

		]
		});

	var panelMsgAcuse = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'pnmsgAcuse',
		id: 'pnmsgAcuse1',
		width: 400,
		style: 'margin:0 auto;',
		frame: true
	});

	var panelMsgLegal = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'pnmsgLegal',
		id: 'pnmsgLegal1',
		width: 900,
		style: 'margin:0 auto;',
		frame: true,
		html:'Al solicitar el factoraje electr&oacute;nico o descuento electr&oacute;nico del documento que selecciono e identifico, '+
				'transmito los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art&iacute;culos 427 de la Ley General de T&iacute;tulos y Operaciones de Cr&eacute;dito, '+
				'32 c del C&oacute;digo Fiscal de la Federaci&oacute;n y 2038 del C&oacute;digo Civil Federal, haci&eacute;ndome sabedor de su contenido y alcance, condicionado a que se efect&uacute;e '+
				'el descuento electr&oacute;nico o el factoraje electr&oacute;nico.<br/><br/>'+
                'Asimismo, en este acto manifiesto bajo protesta de decir verdad, que s&iacute; he emitido o emitir&eacute; a la EMPRESA DE PRIMER ORDEN el CFDI ' +
                'por la operaci&oacute;n comercial que le dio origen a esta transacci&oacute;n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.'
	});

	var panelMsgCifrado = new Ext.Panel({//FUNCION: Panel para mostrar avisos y al momento de firmar y en el PDF
		name: 'panelMsgCifrado',
		id: 'panelMsgCifrado1',
		width: 900,
		style: 'margin:0 auto;',
		frame: true,
		html:'Al solicitar el factoraje electr&oacute;nico o descuento electr&oacute;nico del documento que selecciono e identifico, '+
				'transmito los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art&iacute;culos 427 de la Ley General de T&iacute;tulos y Operaciones de Cr&eacute;dito, '+
				'32 c del C&oacute;digo Fiscal de la Federaci&oacute;n y 2038 del C&oacute;digo Civil Federal, haci&eacute;ndome sabedor de su contenido y alcance, condicionado a que se efect&uacute;e '+
				'el descuento electr&oacute;nico o el factoraje electr&oacute;nico.<br/><br/>'+
                'Asimismo, en este acto manifiesto bajo protesta de decir verdad, que s&iacute; he emitido o emitir&eacute; a la EMPRESA DE PRIMER ORDEN el CFDI ' +
                'por la operaci&oacute;n comercial que le dio origen a esta transacci&oacute;n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.'
	});


	var panelMsgMotosMenos = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'panelMsgMotosMenos',
		id: 'panelMsgMotosMenos',
		width: 900,
		style: 'margin:0 auto;',
		frame: false,
		html:'<b>Nota:</b> Los documentos que se encuentran inhabilitados no podr�n ser operados por el IF.'
	});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		style: 'margin:0 auto;',
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			{//Panel para mostrar avisos
				xtype: 'panel',
				name: 'avisoOferta',
				id: 'avisoOferta1',
				width: 600,
				frame: true,
				style: 'margin:0 auto;',
				bodyStyle: 'text-align:center',
				hidden: true
			},
			{//Panel para mostrar avisos
				xtype: 'panel',
				name: 'mensajes',
				id: 'mensajes1',
				width: 600,
				frame: true,
				style: 'margin:0 auto;',
				hidden: true
			},
			NE.util.getEspaciador(20),
			fp,
			{
				xtype: 'panel',
				name: 'intermediariosS',
				id: 'intermediariosS1',
				title:'Selecci�n de Intermediario Financiero (Intermediarios Financieros con l�neas activas)',
				width: 600,
				style: 'margin:0 auto;',
				frame: true,
				items:[
					{
						xtype: 'combo',
						name: 'cboIf',
						id: 'cboIf1',
						hidden: true,
						editable: false,
						fieldLabel: 'Intermediario',
						mode: 'local',
						displayField : 'lsNombreCuenta',
						valueField : 'lsIf',
						hiddenName : 'cboIf',
						emptyText: 'Seleccionar',
						width: 580,
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store : storeCatIfData,
						listeners:{
							select : function(cboIf, record, index ) {
								pnl.el.unmask();
								pnl.el.mask('Consultando...', 'x-mask-loading');

								var cboEpo = Ext.getCmp('cboEpo1');
								var record;
								var valEPO;

								if(comboEpoVacio){
									valEPO = cveEpoOriginal+'|'+'SI ALFO MAS';
								}else{
									record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
									valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
								}

								var storeIf = cboIf.getStore();
								var valIF='';
								if(cboIf.getValue()==''){
									var record = storeIf.getAt(0);
									valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
									//cboIf.setValue(record.data['lsIf']);
								}else{
									var record = cboIf.findRecord(cboIf.valueField, cboIf.getValue());
									if(record){
										valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
									}else{
										var record = storeIf.getAt(0);
										valIF = record.data['lsIf']+'|'+record.data['lsNombreCuenta']+'|'+record.data['lsTipoPiso']+'|'+record.data['rs_tipo_lim'];
										cboIf.setValue(record.data['lsIf']);
									}
								}

								objGral.valIF = valIF;
								Ext.Ajax.request({
									url: '13forma1ext.data.jsp',
									params: Ext.apply(fp.getForm().getValues(),{
										informacion: 'GeneraInfoCons',
										cboEpo: valEPO,
										cboIf: valIF
									}),
									callback: procesarSuccessValoresGrales
								});
							}
						},
						tpl : NE.util.getTemplateMensajeCargaCombo('{lsNombreCuenta}')
					}
				],
				hidden: true
			},
			NE.util.getEspaciador(20)
		]
	});

//--------------------

	storeCatEpoData.load();
	storeCatMonedaData.load();


	var validaContratoEpoPyme = function(cveEpo){
		Ext.Ajax.request({
			url: '13forma1ext.data.jsp',
			params: {
				informacion: 'validaContrato',
				cveEpoOriginal: cveEpo
				},
			callback: procesarSuccessValidaSiTieneContrato
		});
	}

	var iniRequest = function(storeEpo){
		var valorEpo = '';

		var cboEpo = Ext.getCmp('cboEpo1');
		//alert(cboEpo);
		if(strUsuario=='NAFIN'){
			var record = '';

			if(Ext.getDom("hidCboEpo").value!='' || comboEpoVacio ){
				cveEpoOriginal = Ext.getDom("hidCboEpo").value;


				if(comboEpoVacio){
					valorEpo = cveEpoOriginal+'|'+'IF';
				}else{
					if(storeEpo.findExact("clave", Ext.getDom("hidCboEpo").value) != -1 ){
						cboEpo.setValue(Ext.getDom("hidCboEpo").value);
						record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
						valorEpo = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
					}else{
						record = storeEpo.getAt(0);
						cboEpo.setValue(record.data['clave']);
						valorEpo = cboEpo.getValue()+'|'+record.data['descripcion'];
					}
				}
			}
		}


		Ext.Ajax.request({
			url: '13forma1ext.data.jsp',
			params: {
				informacion: 'valoresIniciales',
				cboEpo: valorEpo,
				cvePyme: Ext.getDom("cvePyme").value,
				cveEpoOriginal: cveEpoOriginal,
				strUsr: strUsr
				},
			callback: procesarSuccessValoresIni
		});
	}



});