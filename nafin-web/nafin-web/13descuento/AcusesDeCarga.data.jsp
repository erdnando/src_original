<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.text.*,
	java.util.*,
	java.io.File,
	org.apache.commons.logging.Log,
	com.netro.pdf.*,
	com.netro.exception.*,
	netropology.utilerias.*,
	netropology.utilerias.CrearArchivosAcuses,
	com.netro.cesion.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion   = request.getParameter("informacion")  == null?"":(String)request.getParameter("informacion");
String acuse         = request.getParameter("acuse")        == null?"":(String)request.getParameter("acuse");
String tipoArchivo   = request.getParameter("tipo_archivo") == null?"":(String)request.getParameter("tipo_archivo");
String nombreArchivo = "";
String mensaje       = "";
String infoRegresar  = "";
JSONObject resultado = new JSONObject();
JSONArray registros  = new JSONArray();
boolean success      = true;

log.debug("<<<informacion: " + informacion + ">>>>");

CrearArchivosAcuses archivos = new CrearArchivosAcuses();

if(informacion.equals("Consultar")){

	String versionCarga  = "";
	HashMap mapaArchivos = new HashMap();
	archivos.setAcuse(acuse);
	versionCarga = archivos.obtieneTipoCarga();

	if(versionCarga.equals("PANTALLA")){

		mapaArchivos = new HashMap();
		mapaArchivos.put("IC_ACUSE",       acuse                );
		mapaArchivos.put("NOMBRE_ARCHIVO", "PUBLICACIÓN NAFINET");
		mapaArchivos.put("TIPO_ARCHIVO",   "PDF"                );
		registros.add(mapaArchivos);

		mapaArchivos = new HashMap();
		mapaArchivos.put("IC_ACUSE",       acuse                );
		mapaArchivos.put("NOMBRE_ARCHIVO", "PUBLICACIÓN NAFINET");
		mapaArchivos.put("TIPO_ARCHIVO",   "CSV"                );
		registros.add(mapaArchivos);

	} else if(versionCarga.equals("WEBSERVICE")){

		mapaArchivos = new HashMap();
		mapaArchivos.put("IC_ACUSE",       acuse                    );
		mapaArchivos.put("NOMBRE_ARCHIVO", "PUBLICACIÓN WEB SERVICE");
		mapaArchivos.put("TIPO_ARCHIVO",   "XML"                    );
		registros.add(mapaArchivos);

	} else if(versionCarga.equals("ENLACES")){

		mapaArchivos = new HashMap();
		mapaArchivos.put("IC_ACUSE",       acuse                    );
		mapaArchivos.put("NOMBRE_ARCHIVO", "PUBLICACIÓN ENLACES"    );
		mapaArchivos.put("TIPO_ARCHIVO",   "CSV"                    );
		registros.add(mapaArchivos);

	}

	resultado .put("success",  new Boolean(success));
	resultado.put("total",     "" +registros.size());
	resultado.put("registros", registros.toString());
	infoRegresar = resultado.toString();

} else if(informacion.equals("Descarga_Archivo")){

	try{
		int existeArchivo = 0;
		archivos.setAcuse(acuse);
		archivos.setTipoArchivo(tipoArchivo);
		archivos.setStrDirectorioTemp(strDirectorioTemp);
		existeArchivo = archivos.existerArcAcuse();
		if(existeArchivo > 0){
			nombreArchivo = archivos.desArchAcuse();
			success = true;
		} else{
			mensaje = "No se encontró el archivo solicitado";
			success = false;
		}
	} catch(Throwable e){
		success = false;
		throw new AppException("Error al generar el archivo. ", e);
	}
	resultado.put("success", new Boolean(true));
	resultado.put("mensaje", mensaje);
	resultado.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar = resultado.toString();

}
%>
<%=infoRegresar%>