Ext.onReady(function() {

	var meses = new Array('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
	var sTipoCredito;
	var dTotal_MontoOperado;
	var dTotal_CapitalVigente;
	var dTotal_CapitalVencido;
	var dTotal_InteresVigente;
	var dTotal_InteresVencido;
  var dTotal_Moras;
  var dTotal_TotalAdeudo;
  var dTotal_Descuentos;
  var strNombre;
  var strIF;
  var strNaf;
  var fCorte;
  var fCarga = new Date();
  var nota;
  var observaciones;
  var strNafinFirma;
  var strIFFirma;
  var fEnvioFirma;
  var sFirmaIF;
  var sFirmaNafin;
  var sPuestoIF;
  var sNumeroSirac;
  var fechaCarga, horaCarga;
  var serial;
  var sFechaEnvio;
  var cantidadRegistros = 0;

  var objGral = {
    strPerfil   :''
  }

  function validaPerfil(perfil){
    if(perfil =='LINEA CREDITO' || perfil =='IF LI' || perfil =='IF 4CP' || perfil =='IF 5CP' ||
       perfil =='IF 4MIC' || perfil =='IF 5MIC' ){
      return 'CREDITO';
    }else{
      return 'DESCUENTO';
    }
  }

  var procesarObtenerSiracFinanciera =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp =  Ext.util.JSON.decode(response.responseText);

      //objGral.sirac = resp.NUMERO_SIRAC;
      //objGral.financiera = resp.IC_FINANCIERA;
      objGral.strPerfil = resp.strPerfil;
      Ext.getCmp('numSirac').setValue(resp.NUMERO_SIRAC);
      if(validaPerfil(objGral.strPerfil)!='CREDITO'){
        if(resp.NUMERO_SIRAC=='' || resp.IC_FINANCIERA==''){
          Ext.getCmp("tipoLinea").hide();
          if(resp.IC_FINANCIERA==''){
            Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
            //Ext.getCmp("cliente").setValue(resp.NUMERO_SIRAC);
            //objGral.ic_if = 12;

          }
        }else{
          Ext.getCmp("tipoLinea").show();
        }
      }else{
        Ext.getCmp("tipoLinea").setValue("tipoLineaC", true);
        //Ext.getCmp("cliente").setValue(objGral.sirac);
        //objGral.ic_if = 12;
      }

      catalogoFecha.load({
        params   :  {
          tipoLinea: Ext.getCmp("tipoLinea").getValue().value,
          numSirac: Ext.getCmp('numSirac').getValue()
        }
      });
		} else {

			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPDF =  function(opts, success, response) {
		var btnArchivoPDF = Ext.getCmp('btnImprimePDF');
		btnArchivoPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConfirmarCertificado = function(result, request){
		var res = new Object();
		res = Ext.decode(result.responseText);
		Ext.getCmp('observacionesForm').hide();

		if(res.iExiste<=0){
			Ext.getCmp('frmExisteNO').show();
			Ext.getCmp('observacionesConfirma').setValue(Ext.getCmp('sObservaciones').getValue());
			Ext.getCmp('notaConfirma').setText(Ext.getCmp('lblNota').text);
			Ext.getCmp('leyendaConfirma').setText('<div style="text-align:center;margin:0 auto;">Confirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de<br>'+strNombre+'</div>',false);
			Ext.getCmp('strLogin').setValue('<div style="position:absolute;top:70px;text-align:left;margin-left:0px; width:300px;text-align:center"><b>Usuario: </b>' + res.login + '</span>');
			Ext.getCmp('strNombreUsuario').setValue('<div style="position:absolute;top:75px;text-align:left;margin-left:0px;width:300px;text-align:center"><b>Nombre: </b>' + res.nombre + '</span>');
			Ext.getCmp('serial').setValue('<div style="position:absolute;top:92px;text-align:left;margin-left:0px; width:300px;text-align:center"><b>Serial: </b>' + res.serial + '</span>');
			var fhCarga = fCarga.getDate() + '/' + (fCarga.getMonth()+1) + '/' + fCarga.getFullYear() + ' ' + fCarga.getHours()+':'+fCarga.getMinutes()+':'+fCarga.getSeconds();
			Ext.getCmp('ahoraMismo').setValue('<div style="position:absolute;top:95px;text-align:left;margin-left:0px; width:300px;text-align:center"><b>Fecha/Hora: </b>' + fhCarga + '</span>');

         fechaCarga  =  fCarga.getDate() + '/' + (fCarga.getMonth()+1) + '/' + fCarga.getFullYear();
         horaCarga   =  fCarga.getHours()+':'+fCarga.getMinutes()+':'+fCarga.getSeconds();
         serial   =  res.serial;

			var cmpStrNafin = Ext.getCmp('strNafinConfirma');
			var cmpStrIf = Ext.getCmp('strIFConfirma');
			var cmpFenvio = Ext.getCmp('fenvioConfirma');
			cmpStrNafin.setText(strNafinFirma,false);
			cmpStrIf.setText(strIFFirma,false);
			cmpFenvio.setText(fEnvioFirma,false);

		}else{
			Ext.getCmp('lblTitle').hide();
			Ext.getCmp('grid').hide();
			Ext.getCmp('gridTotales').hide();
			Ext.getCmp('frmExiste').show();
		}
	}


	var procesarConsultaDataConfirma = function(store, arrRegistros, opts){

		var gridConfirma = Ext.getCmp('gridConfirma');
		var jsonData = store.reader.jsonData;

		if(arrRegistros != null){
			if(!gridConfirma.isVisible()){
				gridConfirma.show();
			}

			var el = gridConfirma.getGridEl();

			if(jsonData.strTipoBanco=='NB'){
				gridConfirma.getColumnModel().setHidden(0, false);
				gridConfirma.getColumnModel().setHidden(1, false);
			}else if(jsonData.strTipoBanco=='B'){
				gridConfirma.getColumnModel().setHidden(2, false);
				gridConfirma.getColumnModel().setHidden(3, false);
				gridConfirma.getColumnModel().setHidden(4, false);
			}

			if(store.getTotalCount() > 0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts){
		var grid = Ext.getCmp('grid');
		var jsonData = store.reader.jsonData;
      cantidadRegistros = parseInt(jsonData.cantidadRegistros);

	  var cmpForma = Ext.getCmp('forma');
	  paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
	  consultaDataGridTotales.load({
			params: Ext.apply(paramSubmit,{
				informacion: 'Consultar',
				operacion: 'Totales',
				sFechaCorte: Ext.getCmp('id_sFechaCorte').getValue(),
				sClaveMoneda: Ext.getCmp('id_sClaveMoneda').getValue()
			})
		});

      if(jsonData.cantidadRegistros=='0'){
         Ext.getCmp('lblTitle').hide();
         Ext.getCmp('grid').hide();
		 Ext.getCmp('gridTotales').hide();
         Ext.getCmp('observacionesForm').hide();
         Ext.getCmp('frmExiste').hide();
         Ext.getCmp('frmExisteNO').hide();

         var fechFormated = Ext.getCmp('id_sFechaCorte').getValue().match(/\d+/g);
         // FECHA DEL ENCABEZADO
         var myNewFech = fechFormated[0] + ' DE ' + meses[parseInt(fechFormated[1])-1] + ' DE ' + fechFormated[2];
         Ext.getCmp('lblMsgSinRegistros').setText('<div style="padding: 30px;margin: 0 auto; width:100%; text-align:center;font-size:14px;">RESUMEN DE LA CONCILIACI�N DE SALDOS AL ' + myNewFech + '<br /><br />(' +  jsonData.sNumeroSirac + ') ' + jsonData.strNombreIF + '<br />' +Ext.getCmp('id_sClaveMoneda').getRawValue()+'</div>', false);
         Ext.getCmp('frmSinRegistros').show();
      }else{
         var fechFormated = Ext.getCmp('id_sFechaCorte').getValue().match(/\d+/g);
         // FECHA DEL ENCABEZADO
         var myNewFech = fechFormated[0] + ' DE ' + meses[parseInt(fechFormated[1])-1] + ' DE ' + fechFormated[2];
         fCorte = 'RESUMEN DE LA CONCILIACI�N DE SALDOS AL ' + myNewFech + '<br /><br />(' +  jsonData.sNumeroSirac + ') ' + jsonData.strNombreIF + '<br />' +Ext.getCmp('id_sClaveMoneda').getRawValue();
         Ext.getCmp('oneLbl').setValue('<div style="border:0px solid;color:#006699;font-weight:bold;text-align:center">' + fCorte + '</div>');
         Ext.getCmp('lblConfirma').setValue('<div style="text-align:center;margin:0 180px 0 -50px;">Confirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de<br />'+jsonData.strNombreIF+'</div>');
         sTipoCredito = store.getAt(0).get('TIPOCREDITO');
         strNombre 		= jsonData.strNombreIF;

         sNumeroSirac = jsonData.sNumeroSirac;

         Ext.Ajax.request({
            url:'13consulta12ext.data.jsp',
            params: {
               informacion: 'Nota',
               sFechaCorte: Ext.getCmp('id_sFechaCorte').getValue(),
               sClaveMoneda: Ext.getCmp('id_sClaveMoneda').getValue(),
			   tipoLinea: Ext.getCmp("tipoLinea").getValue().value,
			   numSirac: Ext.getCmp('numSirac').getValue()
            },
            success: function(result, request){
               var res = new Object();
               res = Ext.decode(result.responseText);
               nota = res.nota;
               //Ext.getCmp('lblNota').setValue('' + res.nota + '');
			   Ext.getCmp('lblNota').setText(nota);
            },
            failure: function() {
               Ext.Msg.show({
                  title: "Failure",
                  msg: "Error, failed response",
                  buttons: Ext.Msg.OK,
                  icon: Ext.MessageBox.ERROR
               })
            }
         });

         if(arrRegistros != null){
            if(!grid.isVisible()){
               grid.show();
            }

            var el = grid.getGridEl();

            if(jsonData.strTipoBanco=='NB'){
               grid.getColumnModel().setHidden(0, false);
               grid.getColumnModel().setHidden(1, false);
            }else if(jsonData.strTipoBanco=='B'){
               grid.getColumnModel().setHidden(2, false);
               grid.getColumnModel().setHidden(3, false);
               grid.getColumnModel().setHidden(4, false);
            }

            if(store.getTotalCount() > 0){
               el.unmask();
            }else{
               el.mask('No se encontr� ning�n registro', 'x-mask');
            }
         }
      }
	}
	var procesarConsultaDataTotales = function(store, arrRegistros, opts){
      if(cantidadRegistros==0){
		 Ext.getCmp('gridTotales').hide();
      }else{
         var gridTotales = Ext.getCmp('gridTotales');
         var jsonData = store.reader.jsonData;
         if(arrRegistros != null){
            if(!gridTotales.isVisible()){
			   gridTotales.show();
            }

            sFirmaIF    = jsonData.sFirmaIF;
            sFirmaNafin =  jsonData.sFirmaNafin;
            sPuestoIF   =  jsonData.sPuestoIF;
            sPuestoNafin=  jsonData.sPuestoNafin;

            strIF = '<b>' + jsonData.sFirmaIF + '</b><br>' + jsonData.sPuestoIF + '<br >' + jsonData.strNombreIF;
            strNaf= '<b>' + jsonData.sFirmaNafin + '</b><br>' + jsonData.sPuestoNafin + '<br >NACIONAL FINANCIERA S.N.C.';
            strNafinFirma =   '<div style="float:right;margin:0 0 0 400px"><img src="/nafin/00utils/gif/firma_pagos_cartera.gif" border="0" width="228" height="100" style="border-bottom:1px solid"><p style="width:228px;text-align:center">'+strNaf+'</p></div>';// + '<div style="border: 1px solid red;"><b>' + jsonData.sFirmaNafin + '</b><br>zxcxc' + jsonData.sPuestoNafin + '</div>';
            strIFFirma=       '<div style="border-top:1px solid;margin:100px 0; "><span><b>' + jsonData.sFirmaIF + '</b></span><p>' + jsonData.sPuestoIF + '</p><p>' + jsonData.strNombreIF + '</p><br>&nbsp;</div>';
            fEnvioFirma=      '<div style="text-align:right">Fecha de env�o: '+jsonData.sFechaEnvio+'&nbsp;&nbsp;</div> &nbsp;&nbsp;&nbsp;';

            sFechaEnvio = jsonData.sFechaEnvio;

            var cmpStrNafin = Ext.getCmp('strNafin');
            var cmpStrIf = Ext.getCmp('strIF');
            var cmpFenvio = Ext.getCmp('fenvio');
            cmpStrNafin.setText(strNafinFirma,false);
            cmpStrIf.setText(strIFFirma,false);
            cmpFenvio.setText(fEnvioFirma,false);
            dTotal_MontoOperado 		= store.getAt(0).get('MONTOOPERADOT');
            dTotal_CapitalVigente 	= store.getAt(0).get('CAPVIGENTET');
            dTotal_CapitalVencido 	= store.getAt(0).get('CAPVENCIDOT');
            dTotal_InteresVigente 	= store.getAt(0).get('INTERESVIGENTET');
            dTotal_InteresVencido 	= store.getAt(0).get('INTERESVENCIDOT');
            dTotal_Moras	 			= store.getAt(0).get('MORAST');
            dTotal_TotalAdeudo 		= store.getAt(0).get('TOTALADEUDOT');
            dTotal_Descuentos 		= store.getAt(0).get('DESCUENTOST');

            var el = gridTotales.getGridEl();

            if(jsonData.strTipoBanco=='NB'){
               gridTotales.getColumnModel().setHidden(1, false);
            }else if(jsonData.strTipoBanco=='B'){
               gridTotales.getColumnModel().setHidden(2, false);
            }

            if(store.getTotalCount() > 0){
               el.unmask();
            }else{
               el.mask('No se encontr� ning�n registro', 'x-mask');
            }
         }
      }
	}

	var consultaDataGrid =  new Ext.data.JsonStore({
		root: 'registros',
		url: '13consulta12ext.data.jsp',
		baseParams:{
			informacion: 'Consultar'
		},
		fields:[
			{name:'TIPOCREDITO'},
			{name:'MONTOOPERADO'},
			{name:'SUBAPP'},
			{name:'CONCEPTOSUBAPP'},
			{name:'SALDOINSOLUTO'},
			{name:'CAPVIGENTE'},
			{name:'CAPVENCIDO'},
			{name:'INTERESVIGENTE'},
			{name:'INTERESVENCIDO'},
			{name:'MORAS'},
			{name:'TOTALADEUDO'},
			{name:'DESCUENTOS'}
		],
		totalProperty : 'total',
		meesageProperty : 'msg',
		autoLoad: false,
		listeners:{
			load : procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

	var consultaDataGridTotales =  new Ext.data.JsonStore({
		root: 'registros',
		url: '13consulta12ext.data.jsp',
		baseParams:{
			informacion: 'Consultar',
			operacion: 'Totales'
		},
		fields:[
			{name:'TIPOCREDITOT'},
			{name:'MONTOOPERADOT'},
			{name:'SUBAPPT'},
			{name:'CONCEPTOSUBAPPT'},
			{name:'SALDOINSOLUTOT'},
			{name:'CAPVIGENTET'},
			{name:'CAPVENCIDOT'},
			{name:'INTERESVIGENTET'},
			{name:'INTERESVENCIDOT'},
			{name:'MORAST'},
			{name:'TOTALADEUDOT'},
			{name:'DESCUENTOST'},
			{name: 'LBLTOTAL'}
		],
		totalProperty : 'total',
		meesageProperty : 'msg',
		autoLoad: false,
		listeners:{
			load : procesarConsultaDataTotales,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});

	var catalogoMoneda = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave','descripcion'],
		url : '13consulta12ext.data.jsp',
		baseParams: { informacion: 'CatalogoMoneda' },
		totalProperty : 'total',
		autoLoad: true
	});

	var catalogoFecha = new Ext.data.JsonStore({
		root:'registros',
		fields: ['clave','descripcion'],
		url: '13consulta12ext.data.jsp',
		baseParams: { informacion: 'CatalogoFecha' },
		totalProperty : 'total',
		autoLoad : false
	});
	/****************************************************************************
	*											GRID 2� PAGINA										 *
	****************************************************************************/
	var grid = new Ext.grid.GridPanel({
		//xtype: 'grid',
		store: consultaDataGrid,
		id:'grid',
		hidden:true, height:100,
		enableColumnMove: false,
		enableDragDrop: false,
		enableHdMenu: false,
		view: new Ext.grid.GridView({forceFit:true}),
		columns:[{
			header:'Tipo de Cr�dito', dataIndex:'TIPOCREDITO', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left', hidden:true
		},{
			header:'Monto Operado', dataIndex:'MONTOOPERADO', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right', hidden:true,
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Sub Aplicaci�n', dataIndex:'SUBAPP', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right', hidden:true
		},{
			header:'Concepto de Sub Aplicaci�n', dataIndex:'CONCEPTOSUBAPP', menuDisabled:true,
			width:250, sorteable:false, resizable:true, align: 'right', hidden:true
		},{
			header:'Saldo Insoluto Nafin', dataIndex:'SALDOINSOLUTO', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right', hidden:true,
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Capital Vigente', dataIndex:'CAPVIGENTE', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Capital Vencido', dataIndex:'CAPVENCIDO', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Inter�s Vigente', dataIndex:'INTERESVIGENTE', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Inter�s Vencido', dataIndex:'INTERESVENCIDO', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Moras', dataIndex:'MORAS', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Total Adeudo', dataIndex:'TOTALADEUDO', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Descuentos', dataIndex:'DESCUENTOS', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'center'
		}]
	});

	var gridTotales = new Ext.grid.GridPanel({
		//xtype: 'grid',
		store: consultaDataGridTotales,
		id:'gridTotales',
		//hidden:true,
		height:75,
		enableColumnMove: false,
		enableDragDrop: false,
		enableHdMenu: false,
		hideHeaders:false,
		title:'Totales',
		view: new Ext.grid.GridView({forceFit:true}),
		columns:[{
			header:'', dataIndex:'LBLTOTAL', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'right'
		},{
			header:'Monto Operado', dataIndex:'MONTOOPERADOT', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left', hidden:true,
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Saldo Insoluto Nafin', dataIndex:'SALDOINSOLUTOT', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left', hidden:true,
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Capital Vigente', dataIndex:'CAPVIGENTET', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Capital Vencido', dataIndex:'CAPVENCIDOT', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Inter�s Vigente', dataIndex:'INTERESVIGENTET', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Inter�s Vencido', dataIndex:'INTERESVENCIDOT', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Moras', dataIndex:'MORAST', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Total Adeudo', dataIndex:'TOTALADEUDOT', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left',
			renderer: function(val){ return '$ ' + val; }
		},{
			header:'Descuentos', dataIndex:'DESCUENTOST', menuDisabled:true,
			width:100, sorteable:false, resizable:true, align: 'left'
		}]
	});

	var fp = new Ext.form.FormPanel({
		//xtype:'form',
		id:'forma',
		title: 'C�dula de Conciliaci�n',
		collapsible: true,
		frame:true,
		width:500,
		labelWidth: 150,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[{
      xtype: 'radiogroup',
      id:'tipoLinea',
      name: 'tipoLinea',
      fieldLabel: 'Tipo de Linea',
      //hidden:true,
      items: [
         {boxLabel: 'LINEA CREDITO', name: 'tipoLinea', id:'tipoLineaC', inputValue: 'C', value:'C' },
         {boxLabel: 'LINEA DESCUENTO', name: 'tipoLinea', id:'tipoLineaD', inputValue: 'D', checked:true, value:'D' }
      ],
      listeners:{
        change: function(rgroup, radio){
          catalogoFecha.load({
            params   :  {
              tipoLinea: radio.value,
              numSirac: Ext.getCmp('numSirac').getValue()
            }
          });
        }
      }
    },{
			xtype:'combo',
			id:'id_sClaveMoneda',
			name:'sClaveMoneda',
			hiddenName:'sClaveMoneda',
			fieldLabel:'Moneda',
			emptyText:'- Seleccione -',
			store: catalogoMoneda,
			mode:'local',
			displayField: 'descripcion',
			valueField: 'clave',
			allowBlank: false,
         triggerAction:'all'
		},{
			xtype:'combo',
			id:'id_sFechaCorte',
			name:'sFechaCorte',
			hiddenName:'sFechaCorte',
			fieldLabel:'Fecha de Corte',
			emptyText: '- Seleccione -',
			store : catalogoFecha,
			mode:'local',
			displayField:'descripcion',
			valueField:'clave',
			forceSelection:true,
			allowBlank: false,
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all'
		},
    {
      xtype: 'hidden',
      id: 'numSirac',
      name: 'numSirac',
      value: ''
    }],
		buttons:[{
			text: 'Consultar',
			id: 'btnConsultar',
			iconCls: 'icoBuscar',
			handler: function(boton){
				var cmpForma = Ext.getCmp('forma');
				var sClaveMoneda = Ext.getCmp('id_sClaveMoneda');
				var sFechaCorte = Ext.getCmp('id_sFechaCorte');

				if(sClaveMoneda.getValue()==''){
					Ext.Msg.show({
						title:'Mensaje',
						msg:'Debes seleccionar el tipo de moneda',
						buttons:Ext.Msg.OK,
						icon: Ext.Msg.INFO
					});
					return false;
				}
				if(sFechaCorte.getValue()==''){
					Ext.Msg.show({
						title:'Mensaje',
						msg:'Debe de seleccionar una fecha de corte',
						buttons:Ext.Msg.OK,
						icon: Ext.Msg.INFO
					});
					return false;
				}
				cmpForma.hide();
				Ext.getCmp('lblTitle').show();
				Ext.getCmp('observacionesForm').show();
				paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
				consultaDataGrid.load({
					params: Ext.apply(paramSubmit,{
						informacion: 'Consultar',
						sFechaCorte: Ext.getCmp('id_sFechaCorte').getValue(),
						sClaveMoneda: Ext.getCmp('id_sClaveMoneda').getValue()
					})
				});


			}
		},{
			text:'Limpiar',
			iconCls:'icoLimpiar',
			handler: function(){
				window.location = '13consulta12ext.jsp';
			}
		}]
	});


	var lblTitle =  new Ext.form.FormPanel({
		id:'lblTitle',
		title: 'NACIONAL FINANCIERA, S.N.C.',
		bodyStyle:'padding:10px 100px 0 0',
      style :  'margin:0;padding:0',
		//hidden:true,
		items:[{
			xtype:'displayfield',
			id: 'oneLbl'
		},{
			xtype:'displayfield',
			id: 'twoLbl'
		}]
	});

	var observaciones = new Ext.form.FormPanel({
		//xtype:'form',
		id:'observacionesForm',
		bodyStyle:'padding:20px 0',
      style :  'margin:0;padding:0',
		//hidden:true,
		items:[
		/*{
			xtype:'compositefield',
			width:500,
			msgTarget: 'side',
			fieldLabel:'<span style="font-weight:bold;">Nota</span>',
			items:[{
				xtype:'displayfield',
				name: 'lblNota',
				id: 'lblNota',
				value : 'algo de Nota',

				width: 100
			}]
		},*/
		{
			xtype: 'label',
			name: 'lblNota',
			id: 'lblNota',
			fieldLabel:'<b>Nota</b>',
			//html: 'seeee',
			//width: 600,
			frame: false
			//style: 'margin:0 auto;'
		},{
			xtype     : 'textarea',
			grow      : true,
			name      : 'sObservaciones', id: 'sObservaciones',
			fieldLabel: '<b>Observaciones</b>',
			anchor    : '80%'
		},{
			xtype		: 'displayfield',
			id 		: 'lblConfirma',
			value		: '---'
		},{
			xtype:'compositefield',
			height:160,
			bodyStyle:'padding:30px 0',
			items:[{ // FIRMA 1
				xtype:'label',
				id:'strIF',
				width:280, height:200
			},{
				xtype:'displayfield', value:'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			},{ // FIRMA 1
				xtype:'label',
				id:'strNafin',
				width:280, height:200
			}]
		},{
			xtype:'label',
			html:'<div style="text-align:left;margin-top:25px">Nota: Si en un plazo de 10 d&iacute;as naturales contados a partir de la recepci&oacute;n del<br>presente Estado de Cuenta no se reciben las observaciones correspondientes se<br>dar&aacute;n por aceptadas las cifras.</div>'
		},{
			xtype:'label',
			id: 'fenvio'
		}],
		bbar:{
			items:['->','-',{
				xtype:'button',text:'Confirmar',iconCls:'icoContinuar',
				handler: function(boton){
					var cmpForma = Ext.getCmp('forma');
					var textoFirmado = "Cedula de conciliaci�n "+
						"\nIF: "+strNombre+
						"\nMoneda: "+Ext.getCmp('id_sClaveMoneda').getRawValue()+
						"\nTipo Cr�dito: "+sTipoCredito+
						"\nMonto Operado: "+Ext.util.Format.number(dTotal_MontoOperado,0.00)+
						"\nCapital Vigente: "+Ext.util.Format.number(dTotal_CapitalVigente,0.00)+
						"\nCapital Vencido: "+Ext.util.Format.number(dTotal_CapitalVencido,0.00)+
						"\nInteres Vigente: "+Ext.util.Format.number(dTotal_InteresVigente,0.00)+
						"\nInteres Vencido: "+Ext.util.Format.number(dTotal_InteresVencido,0.00)+
						 "\nMoras: "+Ext.util.Format.number(dTotal_Moras,0.00)+
						 "\nTotal Adeudo: "+Ext.util.Format.number(dTotal_TotalAdeudo,0.00)+
						 "\nDescuentos: "+Ext.util.Format.number(dTotal_Descuentos,0)+
						"\nAl transmitir este MENSAJE DE DATOS, usted esta enviando"+
						"\nla CEDULA DE CONCILIACI�N a Nacional Financiera";

					NE.util.obtenerPKCS7(fnConfirmaCertCallback, textoFirmado,
							Ext.getCmp('id_sFechaCorte').getValue(),
							Ext.getCmp('id_sClaveMoneda').getValue(),
							Ext.getCmp('tipoLinea').getValue().value,
							Ext.getCmp('numSirac').getValue());
				}
			}]
		}
	});

	var fnConfirmaCertCallback = function(vpkcs7, vtextoFirmado, vidsFechaCorte, vidsClaveMoneda, vtipoLinea, vnumSirac){
		if (Ext.isEmpty(vpkcs7) || vpkcs7 == 'error:noMatchingCert') {
			Ext.Msg.show({
				title:'Firmar',
				msg:'Error en el proceso de Firmado. \nNo se puede continuar',
				buttons: Ext.Msg.OK,
				icon: Ext.MessageBox.ERROR
			});
			return false;	//Error en la firma. Termina...
		}
		Ext.Ajax.request({
			url:'13consulta12ext.data.jsp',
			params:{
				informacion:'ConfirmarCertificado',
				sFechaCorte: vidsFechaCorte,
				sClaveMoneda: vidsClaveMoneda,
				tipoLinea:  vtipoLinea,
				numSirac: vnumSirac,
				Pkcs7: vpkcs7,
				TextoFirmado: vtextoFirmado
			},
			failure: function(){
				Ext.Msg.show({
					title: 'Firmar',
					msg: 'Problemas para confirmar...',
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.ERROR
				});
			},
			success: procesarConfirmarCertificado
		});
	}

	var frmExiste = new Ext.form.FormPanel({
		//xtype:'form',
		id:'frmExiste',
		title: 'NACIONAL FINANCIERA, S.N.C.',
		bodyStyle:'padding:50px 0',
		//hidden:true,
		items:[{
			xtype:'label',
			id: 'LblYaExiste',
         html     :  '<div style="padding: 30px;margin: 0 auto; width:100%; color:#0B4C5F; background: #F2F2F2; border:0px #0B4C5F solid; text-align:center;font-size:14px;"><b>LA C�DULA YA EXISTE, S�lo se podr� enviar nuevamente cuando Nafin la elimine.</b></div>'
		}],
		bbar:{
			items:['->','-',{
				xtype: 'button',
				text: 'Regresar',
            iconCls:'cancelar',
				handler:function(){
					window.location = '13consulta12ext.jsp';
				}
			}]
		}
	});

	/****************************************************************************
	*										3� PAGINA												 *
	****************************************************************************/
	var frmExisteNO = new Ext.form.FormPanel({
		//xtype:'form',
		id:'frmExisteNO',
		bodyStyle:'padding:20px 0',
		//hidden:true,
		items:[/*{
			xtype:'compositefield',
			width:500,
			msgTarget: 'side',
			fieldLabel:'<span style="font-weight:bold;">Nota</span>',
			items:[{
				xtype:'displayfield',
				name: 'notaConfirma',
				id: 'notaConfirma',
				value : '---',

				width: 100
			}]
		}*/{
			xtype: 'label',
			name: 'notaConfirma',
			id: 'notaConfirma',
			fieldLabel:'<b>Nota</b>',
			//html: 'seeee',
			//width: 600,
			frame: false
			//style: 'margin:0 auto;'
		},{
			xtype     : 'displayfield',
			id			 : 'observacionesConfirma',
			fieldLabel: '<b>Observaciones</b>',
			value		 : ' '
		},{
			xtype 	: 'label',
			id 		: 'leyendaConfirma',
			fieldLable:'',
			value		: '---'
		},{
			xtype		: 'displayfield',
			value		: '---',
			id			: 'strLogin'
		},{
			xtype		: 'displayfield',
			value		: '---',
			id			: 'strNombreUsuario'
		},{
			xtype		: 'displayfield',
			value		: '---',
			id			: 'serial'
		},{
			xtype		: 'displayfield',
			value		: '---',
			id			: 'ahoraMismo'
		},{
			xtype:'compositefield',
			height:160,
			bodyStyle:'padding:30px 0',
			items:[{
				xtype:'label',
				id:'strIFConfirma',
				width:280
			},{
				xtype:'displayfield', value:'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			},{
				xtype:'label',
				id:'strNafinConfirma',
				width:280, height:200
			}]
		},{
			xtype:'label',
			html:'<div style="text-align:left;margin-top:25px">Nota: Si en un plazo de 10 d&iacute;as naturales contados a partir de la recepci&oacute;n del<br>presente Estado de Cuenta no se reciben las observaciones correspondientes se<br>dar&aacute;n por aceptadas las cifras.</div>'
		},{
			xtype:'label',
			id: 'fenvioConfirma'
		}],
		bbar:{
			items:['->','-',{
				xtype:'button',
				text:'Imprimir PDF',
				id:'btnImprimePDF',
				iconCls:'icoPdf',
				handler: function(boton, evento) {
					//boton.disable();
					boton.setIconClass('loading-indicator');



					Ext.Ajax.request({
						url: '13consulta12ext.data.jsp',
						params: {
							informacion    :  'ImprimePDF',
							sFechaCorte    :  Ext.getCmp('id_sFechaCorte').getValue(),
							sClaveMoneda   :  Ext.getCmp('id_sClaveMoneda').getValue(),
							sObservaciones :  Ext.getCmp('sObservaciones').getValue(),
							tipoLinea	   :  Ext.getCmp('tipoLinea').getValue().value,
							numSirac	   : Ext.getCmp('numSirac').getValue(),
							notaConfirma   : Ext.getCmp('notaConfirma').text,
							sFirmaIF       :  sFirmaIF,
							sFirmaNafin    :  sFirmaNafin,
							sPuestoIF      :  sPuestoIF,
							sPuestoNafin   :  sPuestoNafin,
							sNumeroSirac   :  sNumeroSirac,
							fechaCarga     :  fechaCarga,
							horaCarga      :  horaCarga,
							serial         :  serial,
							sFechaEnvio    :  sFechaEnvio
						}
						,callback: procesarGenerarPDF
					});
				}
			},{
				xtype:'button',
				text:'Bajar PDF',
				id:'btnBajarPDF',
				hidden:true
			},{
				xtype:'button',
				text:'Salir',
				iconCls:'cancelar',
				handler: function(){
					window.location = '13consulta12ext.jsp';
				}
			}]
		}
	});

	var frmSinRegistros = new Ext.form.FormPanel({
		//xtype    :  'form',
		id       :  'frmSinRegistros',
		bodyStyle:  'padding:0px',
      style    :  'margin:50px auto;',
      title    :  'NACIONAL FINANCIERA, S.N.C',
      width    :  800,
		//hidden   :  true,
		items    :  [{
         xtype    :  'label',
         id       :  'lblMsgSinRegistros',
         html     :  ''
      },{
         xtype    :  'label',
         html     :  '<div style="padding: 30px;margin: 0 auto; width:100%; color:#0B4C5F; background: #F2F2F2; border:0px #0B4C5F solid; text-align:center;font-size:14px;"><b>No se encontr� ning�n registro</b></div>'
      }],
		bbar     :  {
			items:['->','-',{
				xtype    :  'button',
				text     :  'Regresar',
            iconCls  :  'cancelar',
				hidden   :  false,
            handler  :  function(   ){
               window.location   =  '13consulta12ext.jsp';
            }
			}]
		}
	});

	var pnl2 = new Ext.Container({
		id: 'contenedorPrincipal2',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [lblTitle,grid,gridTotales,observaciones,frmExiste,frmExisteNO] //NE.util.getEspaciador(10)
	});

	var pnlSinRegistros = new Ext.Container({
		id       : 'contenedorPrincipalSinRegistros',
		applyTo  : 'areaContenido',
		width    : 940,
		height   : 'auto',
		items    : [frmSinRegistros]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [fp]
	});

  Ext.Ajax.request({
    url: '13consulta12ext.data.jsp',
    params: {
      informacion:'ObtenerSiracFinanciera'
    }
    ,callback: procesarObtenerSiracFinanciera
  });

  Ext.getCmp("tipoLinea").hide();
  lblTitle.hide();
  gridTotales.hide();
  observaciones.hide();
  frmSinRegistros.hide();
  frmExisteNO.hide();
  frmExiste.hide();
});