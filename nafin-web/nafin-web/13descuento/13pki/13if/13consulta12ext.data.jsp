<%@ page language="java" %>
<%@ page import="
	java.util.*,
	netropology.utilerias.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	com.netro.descuento.*, 
	com.netro.pdf.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession.jspf"%>
<%@ include file="../certificado.jspf" %>
<%!public static final String PRODUCTO_DESCUENTO = "1";%>
<%
strTipoBanco =(strTipoBanco==null || strTipoBanco.equals(""))?"B":strTipoBanco;
CQueryHelperRegExtJS queryHelperRegExtJS;
String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
String informacion	= request.getParameter("informacion")==null?"": request.getParameter("informacion");
String operacion		= request.getParameter("operacion")==null?"": request.getParameter("operacion");
String sFechaCorte 	= (request.getParameter("sFechaCorte")==null)?"":request.getParameter("sFechaCorte");
String sClaveMoneda  = (request.getParameter("sClaveMoneda")==null)?"":request.getParameter("sClaveMoneda");
String sNombreMoneda = (request.getParameter("sNombreMoneda")==null)?"":request.getParameter("sNombreMoneda");
String sOpcion      	= (request.getParameter("sOpcion")==null)?"":request.getParameter("sOpcion");
String numSirac      	= (request.getParameter("numSirac")==null)?"":request.getParameter("numSirac");
String tipoLinea      	= (request.getParameter("tipoLinea")==null)?"":request.getParameter("tipoLinea");



double dTotal_MontoOperado=0, dTotal_CapitalVigente=0, dTotal_CapitalVencido=0;
double dTotal_InteresVigente=0, dTotal_InteresVencido=0, dTotal_Moras=0 ,dTotal_TotalAdeudo=0, dTotal_Descuentos=0;
double dSaldoInsoluto=0;
String sNombreIF="", sMontoOperado="", sCapitalVigente="", sCapitalVencido="",sInteresVencido="";
String sMoras="", sTotalAdeudo="", sDescuentos="", sFechaEnvio="", sTipoCredito="", sInteresVigente="";
String sClaveSubAplic = "", sSaldoInsoluto = "", sNumeroSirac ="";
String sClaveFirmaIF="", sFirmaIF="", sPuestoIF="", sClaveFirmaNafin="", sFirmaNafin="", sPuestoNafin="";
		
String query 			= null;
String infoRegresar 	= "";

if(informacion.equals("ObtenerSiracFinanciera")){
  PagosIFNB  beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class); 
  
  JSONObject jsonObj = new JSONObject();
  jsonObj.put("strPerfil", strPerfil);
  jsonObj.put("success", new Boolean(true));
  
  if(!"LINEA CREDITO".equals(strPerfil)){
    HashMap hmDataIf = beanPagos.getSiracFinaciera(iNoCliente);
	numSirac = (hmDataIf.get("NUMERO_SIRAC")==null || ((String)hmDataIf.get("NUMERO_SIRAC")).equals(""))?"0":(String)hmDataIf.get("NUMERO_SIRAC");
    jsonObj.put("IC_FINANCIERA", hmDataIf.get("IC_FINANCIERA"));
    jsonObj.put("NUMERO_SIRAC", numSirac);
  }else{
    numSirac = beanPagos.getSiracLineaCredito(iNoCliente);
    jsonObj.put("IC_FINANCIERA","");
	jsonObj.put("NUMERO_SIRAC", ("".equals(numSirac)?"0":numSirac));
  }
  infoRegresar = jsonObj.toString();
}

else if(informacion.equals("CatalogoMoneda")){
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setClave("ic_moneda");
	cat.setDescripcion("cd_nombre");
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();
}

else if(informacion.equals("CatalogoFecha")){
	PagosIFNB pagosIFNB = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
	ArrayList alFechasCorte = pagosIFNB.getFechasCorte(6, "-",iNoCliente);
	if(!"C".equals(tipoLinea)){
		alFechasCorte = pagosIFNB.getFechasCorte(6, "-",iNoCliente,tipoLinea,"","");
	}else{
		alFechasCorte = pagosIFNB.getFechasCorte(6, "-","12",tipoLinea, "", numSirac);
	}
	List reg = new ArrayList();
	HashMap datos;
	
	for(int i=0; i< alFechasCorte.size(); i++) {
		datos = new HashMap();
		datos.put("clave",alFechasCorte.get(i));
		datos.put("descripcion",alFechasCorte.get(i));
		reg.add(datos);
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", reg);
	infoRegresar = jsonObj.toString();
}

else if(informacion.equals("Consultar")){
	List reg = new ArrayList();
	int registros=0;
	AccesoDB con = new AccesoDB();
	try{
		con.conexionDB();
		
			String qrySentencia =
					" SELECT   /*+ USE_NL(C,I) INDEX (C) */"   +
					"        i.cg_razon_social, cg_subapl_desc tipo_credito,"   +
					"        SUM (c.fg_montooperado) monto_operado,"   +
					"        SUM (c.fg_capitalvigente) capital_vigente, "   +
					"        SUM (c.fg_capitalvencido) capital_vencido,"   +
					"        SUM (c.fg_interesprovi) interes_vigente, "   +
					"        SUM (c.fg_interesvencido) interes_vencido,"   +
					"        SUM (c.FG_INTERESMORAT + c.FG_SOBRETASAMOR) moras, "   +
					"        SUM (c.fg_adeudototal) total_adeudo, "   +
					"        COUNT (c.ig_prestamo) descuentos,"   +
					"        ig_subapl subaplic, "   +
					"        SUM (c.fg_saldoinsoluto) saldo_insoluto"   +
					(("C".equals(tipoLinea))?" ,c.ig_cliente as ic_financiera ":" ,i.ic_financiera ") +
					"   FROM com_estado_cuenta c, comcat_if i"   +
					"  WHERE c.ic_if = i.ic_if"   +
					"    AND c.df_fechafinmes >= TO_DATE (?, 'DD/MM/YYYY')"   +
					"    AND c.df_fechafinmes < TO_DATE (?, 'DD/MM/YYYY')+1"   +
					"    AND c.ic_moneda = ?"   +
					"    AND c.ic_if = ?"   +
          (("C".equals(tipoLinea))?" AND c.ig_cliente = ? ":"") +
					"  GROUP BY i.cg_razon_social,  cg_subapl_desc, ig_subapl "+(("C".equals(tipoLinea))?" ,c.ig_cliente ":" ,i.ic_financiera ");
					
		PreparedStatement ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1, sFechaCorte);
		ps.setString(2, sFechaCorte);
		ps.setString(3, sClaveMoneda);
    if(!"C".equals(tipoLinea)){
		ps.setString(4, iNoCliente);
    }else{
      ps.setLong(4, 12);
      ps.setLong(5, Long.parseLong(numSirac));
    }
		ResultSet rs = ps.executeQuery();
		
		ArrayList alAtributos = new ArrayList();
		ArrayList alRegistros= new ArrayList();
		HashMap datos;
		
		while (rs.next()) {			
			datos = new HashMap();
			alAtributos = new ArrayList();
			sNombreIF        = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
			sTipoCredito     = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
			sMontoOperado    = (rs.getString(3) == null) ? "0" : rs.getString(3).trim();
			sCapitalVigente  = (rs.getString(4) == null) ? "0" : rs.getString(4).trim();
			sCapitalVencido  = (rs.getString(5) == null) ? "0" : rs.getString(5).trim();
			sInteresVigente  = (rs.getString(6) == null) ? "0" : rs.getString(6).trim();
			sInteresVencido  = (rs.getString(7) == null) ? "0" : rs.getString(7).trim();
			sMoras           = (rs.getString(8) == null) ? "0" : rs.getString(8).trim();
			sTotalAdeudo     = (rs.getString(9) == null) ? "0" : rs.getString(9).trim();
			sDescuentos      = (rs.getString(10) == null) ? "0" : rs.getString(10).trim();
			sClaveSubAplic	 = (rs.getString(11) == null) ? "" : rs.getString(11).trim();
			sSaldoInsoluto	 = (rs.getString(12) == null) ? "0" : rs.getString(12).trim();
			sNumeroSirac	 = (rs.getString("ic_financiera") == null) ? "" : rs.getString("ic_financiera").trim();
			dTotal_MontoOperado   += Double.parseDouble(sMontoOperado);
			dTotal_CapitalVigente += Double.parseDouble(sCapitalVigente);
			dTotal_CapitalVencido += Double.parseDouble(sCapitalVencido);
			dTotal_InteresVigente += Double.parseDouble(sInteresVigente);
			dTotal_InteresVencido += Double.parseDouble(sInteresVencido);
			dTotal_Moras          += Double.parseDouble(sMoras);
			dTotal_TotalAdeudo    += Double.parseDouble(sTotalAdeudo);
			dTotal_Descuentos     += Double.parseDouble(sDescuentos);
			dSaldoInsoluto	   	 += Double.parseDouble(sSaldoInsoluto);

			datos.put("TIPOCREDITO",sTipoCredito);
			datos.put("MONTOOPERADO",Comunes.formatoDecimal(sMontoOperado,2));
			datos.put("SUBAPP",Comunes.formatoDecimal(sClaveSubAplic,0));
			datos.put("CONCEPTOSUBAPP",sTipoCredito);
			datos.put("SALDOINSOLUTO",Comunes.formatoDecimal(sSaldoInsoluto,2));
			datos.put("CAPVIGENTE",Comunes.formatoDecimal(sCapitalVigente,2));
			datos.put("CAPVENCIDO",Comunes.formatoDecimal(sCapitalVencido,2));
			datos.put("INTERESVIGENTE",Comunes.formatoDecimal(sInteresVigente,2));
			datos.put("INTERESVENCIDO",Comunes.formatoDecimal(sInteresVencido,2));
			datos.put("MORAS",Comunes.formatoDecimal(sMoras,2));
			datos.put("TOTALADEUDO",Comunes.formatoDecimal(sTotalAdeudo,2));
			datos.put("DESCUENTOS",Comunes.formatoDecimal(sDescuentos,0));
			reg.add(datos);
	
			alAtributos.add(sTipoCredito.replace(',',' '));
			alAtributos.add(Comunes.formatoDecimal(sMontoOperado,2,false));
			alAtributos.add(Comunes.formatoDecimal(sCapitalVigente,2,false));
			alAtributos.add(Comunes.formatoDecimal(sCapitalVencido,2,false));
			alAtributos.add(Comunes.formatoDecimal(sInteresVigente,2,false));
			alAtributos.add(Comunes.formatoDecimal(sInteresVencido,2,false));
			alAtributos.add(Comunes.formatoDecimal(sMoras,2,false));
			alAtributos.add(Comunes.formatoDecimal(sTotalAdeudo,2,false));
			alAtributos.add(Comunes.formatoDecimal(sDescuentos,0,false));
			alAtributos.add(Comunes.formatoDecimal(sClaveSubAplic,2,false));
			alAtributos.add(Comunes.formatoDecimal(sSaldoInsoluto,0,false));
			alRegistros.add(alAtributos);
			registros++;
		}//while
		session.setAttribute("Registros", alRegistros);
		rs.close();
		ps.close();
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
		
		if(operacion.equals("Totales")){
			try{
				con = new AccesoDB();
				con.conexionDB();
				String qrySentencia =
							" SELECT"   +
								 " IC_FIRMA_CEDULA FIRMA_IF " +
									"       ,CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO NOMBRE_IF " +
									" 	  ,CG_PUESTO PUESTO_IF " +
									" 	  ,(SELECT IC_FIRMA_CEDULA FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S'  AND IC_CLIENTE_EXTERNO IS NULL) FIRMA_NAFIN " +
								 " 	  ,(SELECT CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO RESP_IF "   +
									"        FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) RESP_NAFIN " +
									" 	  ,(SELECT CG_PUESTO FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) PUESTO_NAFIN " +
									"	  ,TO_CHAR(sysdate,'DD/MM/YYYY') FECHA " +
								 " FROM "   +
								 " 	  COMCAT_FIRMA_CEDULA"   +
								 " WHERE "   +
								 //"       IC_IF=? "   +
                 ((!"LINEA CREDITO".equals(strPerfil))?" IC_IF  = ? ":" IC_CLIENTE_EXTERNO = ? ") +
								 " 	  AND CS_ACTIVO='S'"  ;
				//out.println("queryFirmas:"+queryFirmas+"<br>");
				//PreparedStatement ps = con.queryPrecompilado(qrySentencia);
				PreparedStatement ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1, iNoCliente);
				ResultSet rs = ps.executeQuery();
				rs = ps.executeQuery();
				//rs = ps.executeQuery();
				
				while (rs.next()) {					
					sClaveFirmaIF    = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
					sFirmaIF         = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
					sPuestoIF        = (rs.getString(3) == null) ? "" : rs.getString(3).trim();
					sClaveFirmaNafin = (rs.getString(4) == null) ? "" : rs.getString(4).trim();
					sFirmaNafin      = (rs.getString(5) == null) ? "" : rs.getString(5).trim();
					sPuestoNafin     = (rs.getString(6) == null) ? "" : rs.getString(6).trim();
					sFechaEnvio      = (rs.getString(7) == null) ? "" : rs.getString(7).trim();
				}//fin while
				//fin query para obtener las firmas
				
				HashMap alTotales;
				reg = new ArrayList();
				if(registros!=0){
					alTotales = new HashMap();
					alTotales.put("MONTOOPERADOT",Comunes.formatoDecimal(dTotal_MontoOperado,2));
					alTotales.put("SALDOINSOLUTOT",Comunes.formatoDecimal(dSaldoInsoluto,2));
					alTotales.put("CAPVIGENTET",Comunes.formatoDecimal(dTotal_CapitalVigente,2));
					alTotales.put("CAPVENCIDOT",Comunes.formatoDecimal(dTotal_CapitalVencido,2));
					alTotales.put("INTERESVIGENTET",Comunes.formatoDecimal(dTotal_InteresVigente,2));
					alTotales.put("INTERESVENCIDOT",Comunes.formatoDecimal(dTotal_InteresVencido,2));
					alTotales.put("MORAST",Comunes.formatoDecimal(dTotal_Moras,2));
					alTotales.put("TOTALADEUDOT",Comunes.formatoDecimal(dTotal_TotalAdeudo,2));
					alTotales.put("DESCUENTOST",Comunes.formatoDecimal(dTotal_Descuentos,0));
					alTotales.put("LBLTOTAL","TOTAL");
					session.setAttribute("Totales", alTotales);
					reg.add(alTotales);
				}
				rs.close();
				ps.close();
			}catch(Exception e) {
				out.println(e.getMessage()); 
				e.printStackTrace();
			}finally {	
				if(con.hayConexionAbierta()){ 
					con.cierraConexionDB();	
				}
			}
		}
		
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("sNumeroSirac",sNumeroSirac);
	jsonObj.put("strNombreIF",strNombre);
	jsonObj.put("strTipoBanco",strTipoBanco);
	jsonObj.put("sFirmaIF",sFirmaIF);
	jsonObj.put("sPuestoIF",sPuestoIF);
	jsonObj.put("sFirmaNafin",sFirmaNafin);
	jsonObj.put("sPuestoNafin",sPuestoNafin);
	jsonObj.put("sFechaEnvio",sFechaEnvio);
	jsonObj.put("registros", reg);
	jsonObj.put("cantidadRegistros", Integer.toString(registros));
	infoRegresar = jsonObj.toString();
}

else if(informacion.equals("Nota")){
	List reg = new ArrayList();
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	try{
		con.conexionDB();
		String qrySentencia = "";
		
		if(!"C".equals(tipoLinea)){
	
			qrySentencia =
					"select cg_observaciones from com_observaciones_cedula "+
					"where trunc(df_fecha_corte) = to_date( ?, 'dd/mm/yyyy') "+
					"and ic_if = ? "+
					"and ic_moneda = ?";
			ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,sFechaCorte);
		ps.setString(2,iNoCliente);
		ps.setString(3,sClaveMoneda);
			rs = ps.executeQuery();
		}else{
			qrySentencia =
					"select cg_observaciones from com_observaciones_cedula "+
					"where trunc(df_fecha_corte) = to_date( ?, 'dd/mm/yyyy') "+
					"and ic_if = ? "+
					"and ic_moneda = ?" +
					"and ig_cliente  = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sFechaCorte);
			ps.setString(2,"12");
			ps.setString(3,sClaveMoneda);
			ps.setString(4,numSirac);
			rs = ps.executeQuery();
		}
		String sNota = ""; 
		if(rs.next()) {sNota = rs.getString("cg_observaciones");}
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("nota",sNota);
		jsonObj.put("strTipoBanco",strTipoBanco);
		infoRegresar = jsonObj.toString();
		
		rs.close();
		ps.close();
		
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
}

else if(informacion.equals("ConfirmarCertificado")){
	ArrayList alRegistros 	= (ArrayList)session.getAttribute("Registros");
	ArrayList alTotales 		= new ArrayList();//(ArrayList)session.getAttribute("Totales");
	String sObservaciones   = (request.getParameter("sObservaciones")==null)?"":request.getParameter("sObservaciones");
	String pkcs7 				= (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
	String externContent 	= (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
	String serial 				= _serial;
	String folioCert 			= "";
	
	//FODEA023-2010 FVR
	externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
	char getReceipt = 'Y';
	AccesoDB con = new AccesoDB();
	try{
		con.conexionDB();
		PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
		
		String fechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		String horaCarga = (new SimpleDateFormat ("hh:mm:ss")).format(new java.util.Date());
		
		try	{
			/*EGOY java.security.cert.X509Certificate certs [] 	  = (java.security.cert.X509Certificate [])	request.getAttribute("javax.servlet.request.X509Certificate");
				for(int _i = 0; _i<certs.length; _i++) {
                                if(strSerial.length()>20){
                                    System.out.println("SHA2---------------------------"+certs[_i].getSerialNumber().toString(16) );
                                    _serial=""+certs[_i].getSerialNumber().toString(16);
                                }else{
                                    byte x [] = certs[_i].getSerialNumber().toByteArray();
                                    _serial = new String (x);
                                }
				}*/
		} catch(Exception e) {
			System.out.println(e+": Error al leer el certificado");
		}
		
      //serial = _serial;
		Acuse acuse = new Acuse(Acuse.ACUSE_IF, PRODUCTO_DESCUENTO);
		if (!serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
			folioCert = acuse.toString();
			Seguridad s = new Seguridad();
			if (s.autenticar(folioCert, serial, pkcs7, externContent, getReceipt)) {
			//if (true) {
				String _acuse = s.getAcuse();
				PreparedStatement ps = null;
				if(!"LINEA CREDITO".equals(strPerfil)){
					if(!"C".equals(tipoLinea) ){
						query = " select COUNT(1) from com_cedula_concilia "   +
								" where TRUNC(DF_FECHA_CORTE) = TO_DATE(?,'DD/MM/YYYY')"   +
								" AND IC_IF = ?"   +
								" AND IC_MONEDA = ?"  ;
				
						ps = con.queryPrecompilado(query);
						ps.setString(1, sFechaCorte);
						ps.setString(2, iNoCliente);
						ps.setString(3, sClaveMoneda);
					}else{
				query = " select COUNT(1) from com_cedula_concilia "   +
								   " where TRUNC(DF_FECHA_CORTE) = TO_DATE(?,'DD/MM/YYYY')"   +
								   " AND IC_IF = ?"   +
							" AND IC_MONEDA = ?"  +
							" AND IN_NUMERO_SIRAC = ?"  ;
						ps = con.queryPrecompilado(query);
						ps.setString(1, sFechaCorte);
						ps.setString(2, iNoCliente);
						ps.setString(3, sClaveMoneda);
						ps.setString(4, numSirac);
					}
				}else{
					query = " select COUNT(1) from com_cedula_concilia "   +
							" where TRUNC(DF_FECHA_CORTE) = TO_DATE(?,'DD/MM/YYYY')"   +
							//" AND IC_IF = ?"   +
							//(("C".equals(tipoLinea))?" AND IC_IF  = ? ":" AND IC_CLIENTE_EXTERNO = ? ") +
							" AND IC_CLIENTE_EXTERNO = ? " +
								   " AND IC_MONEDA = ?"  ;
					ps = con.queryPrecompilado(query);
				ps.setString(1, sFechaCorte);
				ps.setString(2, iNoCliente);
				ps.setString(3, sClaveMoneda);
				}
				
                        
				ResultSet rs = ps.executeQuery();
				int iExiste = 0;
				while (rs.next()) {
					iExiste = rs.getInt(1);
				}    
            
            alRegistros = (ArrayList)session.getAttribute("Registros");
            
            System.err.println("alRegistros " + alRegistros.size() + " / iExiste " + iExiste);
				System.out.println("tipoLinea ========================================== "+tipoLinea);
            if(iExiste<=0){
						for(int i=0; i < alRegistros.size(); i++){
							ArrayList alTemp = new ArrayList();
							alTemp=(ArrayList)alRegistros.get(i);
							System.err.println("alRegistros.get(i)" + alRegistros.get(i));
							CedulaConciliacionIFNB cedula = new CedulaConciliacionIFNB();
							cedula.setTipoCredito((String)alTemp.get(0));	
							cedula.setMontoOperado((String)alTemp.get(1));	
							cedula.setCapitalVigente((String)alTemp.get(2));
							cedula.setCapitalVencido((String)alTemp.get(3));
							cedula.setInteresVigente((String)alTemp.get(4));
							cedula.setInteresVencido((String)alTemp.get(5));
							cedula.setInteresMoratorio((String)alTemp.get(6));
							cedula.setTotalAdeudo((String)alTemp.get(7));
							cedula.setDescuentos((String)alTemp.get(8));
							cedula.setFechaCorte(sFechaCorte);
					cedula.setTipoLinea(tipoLinea);
					if("C".equals(tipoLinea) ){
						if(strTipoUsuario.equals("CLIENTE")){
							cedula.setCveClienteExterno(iNoCliente);
							cedula.setNumeroSirac(numSirac);
							//cedula.setTipoLinea(tipoLinea);
						}else{
							cedula.setClaveIF(iNoCliente);
							cedula.setNumeroSirac(numSirac);
						}
					}else{
							cedula.setClaveIF(iNoCliente);
					}
					
							cedula.setMoneda(sClaveMoneda);
							cedula.setClaveFirmaNafin(sClaveFirmaNafin);
							cedula.setClaveFirmaIF(sClaveFirmaIF);
							cedula.setObservaciones(sObservaciones);
							cedula.setClaveSubAplic((String)alTemp.get(9));
							cedula.setSaldoInsoluto((String)alTemp.get(10));
							cedula.setIcUsuario(strLogin);
							cedula.setNombreUsuario(strNombreUsuario);
							cedula.setCveCertificado(serial);
							bean.cedulaConciliacion(cedula);
						} //fin del for						
						//se insertara en bitacora fodea 012
				if("C".equals(tipoLinea) ){
					if(strTipoUsuario.equals("CLIENTE")){
						bean.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,"","",numSirac,"","",serial,"C", iNoCliente, "C");
					}else{
						bean.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,"","",numSirac,"",iNoCliente,serial,"C", "", "C");
					}
					}else{
						bean.insertarDatosConsultaPCIf(strLogin,strNombreUsuario,"","","","",iNoCliente,serial,"C");   
            }
				}
            
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("login",strLogin);
				jsonObj.put("nombre",strNombreUsuario);
				jsonObj.put("serial",serial);
				jsonObj.put("iExiste",Integer.toString(iExiste));
				infoRegresar = jsonObj.toString();
			}
		}
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
}

else if(informacion.equals("ImprimePDF")){	
	String sObservaciones = (request.getParameter("sObservaciones")==null)?"":request.getParameter("sObservaciones").replace('_',' ');
	String fechaCarga	  = (request.getParameter("fechaCarga")==null)?"":request.getParameter("fechaCarga");
	String horaCarga	  = (request.getParameter("horaCarga")==null)?"":request.getParameter("horaCarga");
	String serial	  = (request.getParameter("serial")==null)?"":request.getParameter("serial");
	String nota	  = (request.getParameter("notaConfirma")==null)?"":request.getParameter("notaConfirma");
	
	ArrayList alTotales = new ArrayList();
	ArrayList alAtributos = new ArrayList();
	ArrayList alRegistros = new ArrayList();
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer("");
	String nombreArchivo = archivo.nombreArchivo()+".pdf";
	AccesoDB con = new AccesoDB();
   
   
	try{
		con.conexionDB();
			String qrySentencia =
				" SELECT   /*+ USE_NL(C,I) INDEX (C) */"   +
				"        i.cg_razon_social, cg_subapl_desc tipo_credito,"   +
				"        SUM (c.fg_montooperado) monto_operado,"   +
				"        SUM (c.fg_capitalvigente) capital_vigente, "   +
				"        SUM (c.fg_capitalvencido) capital_vencido,"   +
				"        SUM (c.fg_interesprovi) interes_vigente, "   +
				"        SUM (c.fg_interesvencido) interes_vencido,"   +
				"        SUM (c.FG_INTERESMORAT + c.FG_SOBRETASAMOR) moras, "   +
				"        SUM (c.fg_adeudototal) total_adeudo, "   +
				"        COUNT (c.ig_prestamo) descuentos,"   +
				"        ig_subapl subaplic, "   +
				"        SUM (c.fg_saldoinsoluto) saldo_insoluto"   +
				(("C".equals(tipoLinea))?" ,c.ig_cliente as ic_financiera ":" ,i.ic_financiera ") +
				"   FROM com_estado_cuenta c, comcat_if i"   +
				"  WHERE c.ic_if = i.ic_if"   +
				"    AND c.df_fechafinmes >= TO_DATE (?, 'DD/MM/YYYY')"   +
				"    AND c.df_fechafinmes < TO_DATE (?, 'DD/MM/YYYY')+1"   +
				"    AND c.ic_moneda = ?"   +
				"    AND c.ic_if = ?"   +
        (("C".equals(tipoLinea))?" AND c.ig_cliente = ? ":"") +
				"  GROUP BY i.cg_razon_social,  cg_subapl_desc, ig_subapl"+ (("C".equals(tipoLinea))?" ,c.ig_cliente ":" ,i.ic_financiera ");
					
		PreparedStatement ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1, sFechaCorte);
		ps.setString(2, sFechaCorte);
		ps.setString(3, sClaveMoneda);
		if(!"C".equals(tipoLinea)){
		ps.setString(4, iNoCliente);
    }else{
      ps.setString(4, "12");
      ps.setString(5, numSirac);
    }
    
		ResultSet rs = ps.executeQuery();
		int registros=0;
		
		while (rs.next()) {		
			sNombreIF        = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
			sTipoCredito     = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
			sMontoOperado    = (rs.getString(3) == null) ? "0" : rs.getString(3).trim();
			sCapitalVigente  = (rs.getString(4) == null) ? "0" : rs.getString(4).trim();
			sCapitalVencido  = (rs.getString(5) == null) ? "0" : rs.getString(5).trim();
			sInteresVigente  = (rs.getString(6) == null) ? "0" : rs.getString(6).trim();
			sInteresVencido  = (rs.getString(7) == null) ? "0" : rs.getString(7).trim();
			sMoras           = (rs.getString(8) == null) ? "0" : rs.getString(8).trim();
			sTotalAdeudo     = (rs.getString(9) == null) ? "0" : rs.getString(9).trim();
			sDescuentos      = (rs.getString(10) == null) ? "0" : rs.getString(10).trim();
			sClaveSubAplic	 = (rs.getString(11) == null) ? "" : rs.getString(11).trim();
			sSaldoInsoluto	 = (rs.getString(12) == null) ? "0" : rs.getString(12).trim();
			sNumeroSirac	 = (rs.getString("ic_financiera") == null) ? "" : rs.getString("ic_financiera").trim();
			
         
			dTotal_MontoOperado   += Double.parseDouble(sMontoOperado);
			dTotal_CapitalVigente += Double.parseDouble(sCapitalVigente);
			dTotal_CapitalVencido += Double.parseDouble(sCapitalVencido);
			dTotal_InteresVigente += Double.parseDouble(sInteresVigente);
			dTotal_InteresVencido += Double.parseDouble(sInteresVencido);
			dTotal_Moras          += Double.parseDouble(sMoras);
			dTotal_TotalAdeudo    += Double.parseDouble(sTotalAdeudo);
			dTotal_Descuentos     += Double.parseDouble(sDescuentos);
			dSaldoInsoluto	   	 += Double.parseDouble(sSaldoInsoluto);
         
			alAtributos = new ArrayList();
			alAtributos.add(sTipoCredito.replace(',',' '));
			alAtributos.add(Comunes.formatoDecimal(sMontoOperado,2,false));
			alAtributos.add(Comunes.formatoDecimal(sCapitalVigente,2,false));
			alAtributos.add(Comunes.formatoDecimal(sCapitalVencido,2,false));
			alAtributos.add(Comunes.formatoDecimal(sInteresVigente,2,false));
			alAtributos.add(Comunes.formatoDecimal(sInteresVencido,2,false));
			alAtributos.add(Comunes.formatoDecimal(sMoras,2,false));
			alAtributos.add(Comunes.formatoDecimal(sTotalAdeudo,2,false));
			alAtributos.add(Comunes.formatoDecimal(sDescuentos,0,false));
			alAtributos.add(Comunes.formatoDecimal(sClaveSubAplic,2,false));
			alAtributos.add(Comunes.formatoDecimal(sSaldoInsoluto,2,false));
			alRegistros.add(alAtributos);
			registros++;
		}//while
		rs.close();
		ps.close();
		
		con = new AccesoDB();
		con.conexionDB();
		qrySentencia =
			" SELECT"   +
				 " IC_FIRMA_CEDULA FIRMA_IF " +
				"       ,CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO NOMBRE_IF " +
				" 	  ,CG_PUESTO PUESTO_IF " +
				" 	  ,(SELECT IC_FIRMA_CEDULA FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S'  AND IC_CLIENTE_EXTERNO IS NULL) FIRMA_NAFIN " +
				 " 	  ,(SELECT CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO RESP_IF "   +
				"        FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) RESP_NAFIN " +
				" 	  ,(SELECT CG_PUESTO FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) PUESTO_NAFIN " +
				"	  ,TO_CHAR(sysdate,'DD/MM/YYYY') FECHA " +
				 " FROM "   +
				 " 	  COMCAT_FIRMA_CEDULA"   +
				 " WHERE "   +
				 //"       IC_IF=? "   +
         ((!"LINEA CREDITO".equals(strPerfil))?" IC_IF  = ? ":"IC_CLIENTE_EXTERNO = ? ") +
				 " 	  AND CS_ACTIVO='S'"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1, iNoCliente);
		rs = ps.executeQuery();
		//rs = ps.executeQuery();
		//rs = ps.executeQuery();
		
		while (rs.next()) {					
			sClaveFirmaIF    = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
			sFirmaIF         = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
			sPuestoIF        = (rs.getString(3) == null) ? "" : rs.getString(3).trim();
			sClaveFirmaNafin = (rs.getString(4) == null) ? "" : rs.getString(4).trim();
			sFirmaNafin      = (rs.getString(5) == null) ? "" : rs.getString(5).trim();
			sPuestoNafin     = (rs.getString(6) == null) ? "" : rs.getString(6).trim();
			sFechaEnvio      = (rs.getString(7) == null) ? "" : rs.getString(7).trim();
		}//fin while
		//fin query para obtener las firmas
		
		if(registros!=0){
			alTotales.add(Comunes.formatoDecimal(dTotal_MontoOperado,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_CapitalVigente,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_CapitalVencido,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_InteresVigente,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_InteresVencido,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_Moras,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_TotalAdeudo,2,false));
			alTotales.add(Comunes.formatoDecimal(dTotal_Descuentos,0,false));
			alTotales.add(Comunes.formatoDecimal(dSaldoInsoluto,2,false));
		}
		rs.close();
		ps.close();
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
   
   
   
	try{
		
		ComunesPDF pdfDoc    = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String fechaActual   = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual     = fechaActual.substring(0,2);
		String mesActual     = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual    = fechaActual.substring(6,10);
		String horaActual    = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		float widths[]       = {1,	 2,1};
		pdfDoc.setTable(3, 100, widths);
		pdfDoc.setCellImage(strDirectorioPublicacion+"00archivos/15cadenas/15archcadenas/logos/nafinsa.gif",ComunesPDF.LEFT, 75);
		String encabezado = "NACIONAL FINANCIERA, S.N.C.\n"; 
		encabezado+="SUBDIRECCION DE OPERACIONES DE CREDITO\n";
		encabezado+="RESUMEN DE LA CONCILIACIÓN DE SALDOS AL ";
		encabezado+= "" + sFechaCorte.substring(0,2) + " DE " + meses[Integer.parseInt(sFechaCorte.substring(3,5))-1].toUpperCase() + " DE " + sFechaCorte.substring(6,10)+"\n\n";
		encabezado+="(" + sNumeroSirac + ") " +strNombre +" - " + sNombreMoneda;
		pdfDoc.setCell(encabezado, "formasrepB", ComunesPDF.CENTER,1,1,0);
		pdfDoc.setCell("", "formasrepB", ComunesPDF.CENTER,1,1,0);
		pdfDoc.addTable();
	
		
		int numCols = 9;
		float widths2[] = {.8f, 2, 1, 1, 1, 1, 1, 1, 1, .7f};
		float widths3[] = {2, 1, 1, 1, 1, 1, 1, 1, .7f};
	
		if("B".equals(strTipoBanco)){
			numCols++;
			pdfDoc.setTable(numCols, 100, widths2);
		} else {
			pdfDoc.setTable(numCols, 100, widths3);
		}
		if("NB".equals(strTipoBanco)) {
			pdfDoc.setCell("Tipo de Crédito", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Operado", "formasmenB", ComunesPDF.CENTER);
		} else if("B".equals(strTipoBanco)) {
			pdfDoc.setCell("Sub aplicación", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Concepto Sub aplicación", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Saldo insoluto Nafin", "formasmenB", ComunesPDF.CENTER);
		}
		pdfDoc.setCell("Capital Vigente", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Capital Vencido", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Interés Vigente", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Interés Vencido", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Moras", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Total Adeudo", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell("Descuentos", "formasmenB", ComunesPDF.CENTER);
		
		//ArrayList alRegistros = (ArrayList)session.getAttribute("Registros");
		for(int i=0; i < alRegistros.size(); i++){
			ArrayList alTemp = new ArrayList();
			alTemp = (ArrayList)alRegistros.get(i);
			if("NB".equals(strTipoBanco)) {
				pdfDoc.setCell(alTemp.get(0).toString(), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(1).toString(), true), "formasmen", ComunesPDF.RIGHT);
			} else if("B".equals(strTipoBanco)) {
				pdfDoc.setCell(Comunes.formatoDecimal(alTemp.get(9).toString(),0), "formasmen", ComunesPDF.RIGHT);
				pdfDoc.setCell(alTemp.get(0).toString(), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(10).toString(), true), "formasmen", ComunesPDF.RIGHT);
			}// else if("B".equals(strTipoBanco)) {
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(2).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(3).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(4).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(5).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(6).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMoneda2(alTemp.get(7).toString(), true), "formasmen", ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(alTemp.get(8).toString(), 0), "formasmen", ComunesPDF.RIGHT);
		}
		//ArrayList alTotales = (ArrayList)session.getAttribute("Totales");
		int colSpan = 1;
		if("B".equals(strTipoBanco))
			colSpan++;	
		pdfDoc.setCell("TOTAL", "formasmenB", ComunesPDF.CENTER, colSpan);
		if("NB".equals(strTipoBanco)) {
			pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(0).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		} else if("B".equals(strTipoBanco)) {
			pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(8).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		}
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(1).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(2).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(3).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(4).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(5).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMoneda2(alTotales.get(6).toString(), true), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoDecimal(alTotales.get(7).toString(), 0), "formasmenB", ComunesPDF.RIGHT);
		pdfDoc.addTable();
		
		//String nota="";// = alRegistros.get(alRegistros.size()-1).toString();
		
		pdfDoc.addText("\nNOTA: "+nota, "formasB", ComunesPDF.LEFT);
		
		pdfDoc.addText("Observaciones:  " + sObservaciones, "formasB", ComunesPDF.LEFT);
		
		pdfDoc.addText("\nConfirmo que los saldos operativos de Nacional Financiera, S.N.C. han sido conciliados con los registros contables de", "formas", ComunesPDF.CENTER);
		pdfDoc.addText(strNombre, "formasB", ComunesPDF.CENTER);
		
		pdfDoc.setTable(3, 85);
		
		pdfDoc.setCell("\nUsuario: "+strLogin+"\nNombre: "+strNombreUsuario+"\nNo. serie certificado: "+serial+"\nFecha/Hora: "+fechaCarga+" "+horaCarga, "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCellImage(strDirectorioPublicacion + "00utils/gif/firma_pagos_cartera.gif", ComunesPDF.CENTER,100);
	
		pdfDoc.setCell("_____________________________\n"+sFirmaIF, "formasB", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("_____________________________\n"+sFirmaNafin, "formasB", ComunesPDF.CENTER, 1, 1, 0);
		
		pdfDoc.setCell(sPuestoIF, "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell(sPuestoNafin, "formas", ComunesPDF.CENTER, 1, 1, 0);
		
		pdfDoc.setCell(strNombre, "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.setCell("NACIONAL FINANCIERA S.N.C.", "formas", ComunesPDF.CENTER, 1, 1, 0);
		pdfDoc.addTable();
		
		float tWid[] = {1.5f, 1};
		
		pdfDoc.setTable(2, 100, tWid);
	
		pdfDoc.setCell("Nota: Si en un plazo de 10 días hábiles contados a partir de la recepción del presente Estado de Cuenta no se reciben las observaciones correspondientes se darán por aceptadas las cifras.", "formas", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Fecha de Envío:  "+sFechaEnvio, "formas", ComunesPDF.RIGHT, 1, 1, 0);
		pdfDoc.addTable();
		
		pdfDoc.endDocument();
	} catch(Exception e) {
		e.printStackTrace();
		System.out.println(e + ": Error al Generar el Archivo");
	}
	
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>

