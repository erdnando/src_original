<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,com.netro.descuento.*, netropology.utilerias.*, java.sql.*,net.sf.json.JSONObject" 
	
	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "", consulta = "";
JSONObject jsonObj = new JSONObject();

if (informacion.equals("ConsultaLimitesPorEPO")) {
	LimitesPorEPO beanLimites = ServiceLocator.getInstance().lookup("LimitesPorEPOEJB", LimitesPorEPO.class);
	Registros reg = beanLimites.ovgetLimitesExt(iNoCliente); 
	
		
	while(reg.next()){
	
		String ic_epo = (reg.getString("IC_EPO") == null) ? "" : reg.getString("IC_EPO");
		HashMap monedas = beanLimites.validaMonedas(ic_epo , iNoCliente);	
		reg.setObject("MONEDA_MN",monedas.get("MONEDA_MN").toString());	
		reg.setObject("MONEDA_DL",monedas.get("MONEDA_DL").toString());		 	
	}
	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();  		
		
}

%>
<%=infoRegresar%>


