<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	com.netro.mandatodoc.*,	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String infoRegresar ="";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String ig_numero_docto = (request.getParameter("ig_numero_docto")!=null)?request.getParameter("ig_numero_docto"):"";
String df_fecha_venc = (request.getParameter("df_fecha_venc")!=null)?request.getParameter("df_fecha_venc"):"";
String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
String fn_montoMin = (request.getParameter("fn_montoMin")!=null)?request.getParameter("fn_montoMin"):"";
String fn_montoMax = (request.getParameter("fn_montoMax")!=null)?request.getParameter("fn_montoMax"):"";
String ic_estatus = (request.getParameter("ic_estatus")!=null)?request.getParameter("ic_estatus"):"";
String txt_nafelec = (request.getParameter("num_electronico")!=null)?request.getParameter("num_electronico"):"";

String  ic_if =iNoCliente;
int numRegistrosMN = 0,  numRegistrosDL = 0, numRegistros =0;
double Recurso;	
boolean autOperSF;
BigDecimal montoTotalMN = new BigDecimal("0.00");
BigDecimal montoTotalDL = new BigDecimal("0.00");
BigDecimal montoTotalDescuentoMN = new BigDecimal("0.00");
BigDecimal montoTotalDescuentoDL = new BigDecimal("0.00");
BigDecimal importeTotalInteresMN = new BigDecimal("0.00");
BigDecimal importeTotalInteresDL = new BigDecimal("0.00");
BigDecimal importeTotalRecibirMN = new BigDecimal("0.00");
BigDecimal importeTotalRecibirDL = new BigDecimal("0.00");
BigDecimal importeTotalRecGarMN = new BigDecimal("0.00");
BigDecimal importeTotalRecGarDL = new BigDecimal("0.00");
String icDocumento ="", nombreEpo ="", numeroDocumento ="", fechaVencimiento ="", estatusDocumento ="",
		icMoneda ="", monedaNombre ="",  monto ="", Porcentaje ="", montoDescuento ="", importeInteres ="",
		importeRecibir ="", icEstatus ="", nombrePyme ="", consulta ="", estatusNuevo ="", autOperSF2="N";			

String DoctosSeleccionados[] = null;
HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
HashMap  datosTot = new HashMap();	
JSONArray registrosTotales = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

IMantenimiento BeanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);
AutorizacionDescuento beanAutDesc = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

boolean manejaNotasDeCreditoMultiples = false;
String  manejaNotasDeCreditoMultiples2 = "N";
if(!ic_epo.equals("")){
	manejaNotasDeCreditoMultiples = BeanParamDscto.hasAplicarNotaDeCreditoAVariosDoctosEnabled(ic_epo);
}
if(manejaNotasDeCreditoMultiples ==true)  manejaNotasDeCreditoMultiples2 ="S";


if (informacion.equals("catalogoEPO")  ) {

	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.set_claveIf(ic_if);
	cat.setOrden("CE.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
	
}else if (informacion.equals("catalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	


}else if (informacion.equals("catalogoEstatus")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_estatus_docto");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_estatus_docto");		
	catalogo.setOrden("ic_estatus_docto");
	catalogo.setValoresCondicionIn("3,24", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
	
		
}else if (informacion.equals("catalogoEstatusAsignar")  ) {

		String estatus1 = request.getParameter("estatus1")==null?"":request.getParameter("estatus1");
		String estatus2 = request.getParameter("estatus2")==null?"":request.getParameter("estatus2");
		
		HashMap info = new HashMap();		
		
		info = new HashMap();
		info.put("clave", "");
		info.put("descripcion", "--- Conservar estatus actual ---");	
		registros.add(info);
		
		info = new HashMap();
		info.put("clave", "N");
		info.put("descripcion", "Negociable");	
		registros.add(info);
		
		if(estatus1.equals("OP") ) {
			info = new HashMap();
			info.put("clave", "OP");
			info.put("descripcion", "Operado con Fondeo propio");	
			registros.add(info);
		}
		if(estatus2.equals("SP")){	
			info = new HashMap();
			info.put("clave", "SP");
			info.put("descripcion", "Seleccionada Pyme");	
			registros.add(info);
		}	
		
		infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	//System.out.println("infoRegresar "+infoRegresar);

}else if (informacion.equals("catalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_estatus_docto");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_estatus_docto");		
	catalogo.setOrden("ic_estatus_docto");
	catalogo.setValoresCondicionIn("3,24,23", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
	
	
} else if (informacion.equals("busquedaAvanzada") ) {

	String rfc_pyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
	String nombre_pyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String ic_pyme = request.getParameter("ic_pyme")==null?"":request.getParameter("ic_pyme");
	String num_pyme = request.getParameter("noPyme")==null?"":request.getParameter("noPyme");
	
	CatalogoPymeBusqAvanzadaIF cat = new CatalogoPymeBusqAvanzadaIF();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion("crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");
	cat.setIc_epo(ic_epo);
	cat.setRfc_pyme(rfc_pyme);
	cat.setNombre_pyme(nombre_pyme);	
	cat.setNum_pyme(num_pyme);	
	cat.setIc_pyme("");	
	cat.setOrden("p.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
	
}  else if (informacion.equals("pymeNombre")  ) {
	
	String ic_pyme = "",txtNombre ="";
	String num_electronico = request.getParameter("num_electronico")==null?"":request.getParameter("num_electronico");
	
	List datosPymes =  datosPymes(num_electronico, ic_epo ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	infoRegresar = jsonObj.toString();		



}else if (informacion.equals("Consultar")  ||  informacion.equals("ConsultaTotales")  ) {  
		Vector vDocumentosSelPyme= new Vector();
	try {
	 vDocumentosSelPyme = BeanMantenimiento.ovgetDocumentosSelPyme(ic_epo, ig_numero_docto, df_fecha_venc, ic_moneda, fn_montoMin, fn_montoMax, ic_estatus, iNoCliente, DoctosSeleccionados, true, txt_nafelec);
	} catch (NafinException errDoc) {
	
	}
	
	for (int i = 0; i < vDocumentosSelPyme.size(); i++) {
		numRegistros ++;
		Vector vDatosDocumentosSelPyme = (Vector) vDocumentosSelPyme.get(i);
			icDocumento = vDatosDocumentosSelPyme.get(0).toString();
			numeroDocumento = vDatosDocumentosSelPyme.get(1).toString();
			fechaVencimiento = vDatosDocumentosSelPyme.get(2).toString();
			monto = (vDatosDocumentosSelPyme.get(3).toString().equals(""))?"0":vDatosDocumentosSelPyme.get(3).toString();
			nombreEpo = vDatosDocumentosSelPyme.get(4).toString();
			estatusDocumento  = vDatosDocumentosSelPyme.get(5).toString();
			icMoneda = vDatosDocumentosSelPyme.get(6).toString();
			monedaNombre = vDatosDocumentosSelPyme.get(7).toString();
			Porcentaje = vDatosDocumentosSelPyme.get(8).toString();
			montoDescuento = (vDatosDocumentosSelPyme.get(9).toString().equals(""))?"0":vDatosDocumentosSelPyme.get(9).toString();
			importeInteres = vDatosDocumentosSelPyme.get(10).toString();
			importeRecibir = vDatosDocumentosSelPyme.get(11).toString();
			icEstatus		= vDatosDocumentosSelPyme.get(12).toString();
			autOperSF		= ((Boolean)vDatosDocumentosSelPyme.get(13)).booleanValue();
			nombrePyme	= (vDatosDocumentosSelPyme.get(15).toString().equals(""))?"0":vDatosDocumentosSelPyme.get(15).toString();
			String ref = vDatosDocumentosSelPyme.get(16).toString();
			
			Recurso = new Double(monto).doubleValue() - new Double(montoDescuento).doubleValue();
			if(autOperSF ==true) autOperSF2 ="S";
										
			if (icMoneda.equals("1")) {
				numRegistrosMN++;
				montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
				montoTotalDescuentoMN = montoTotalDescuentoMN.add(new BigDecimal(montoDescuento));
				if(!"".equals(importeInteres)){
					importeTotalInteresMN = importeTotalInteresMN.add(new BigDecimal(importeInteres));
				}
				if(!"".equals(importeRecibir)){
					importeTotalRecibirMN = importeTotalRecibirMN.add(new BigDecimal(importeRecibir));
				}
				importeTotalRecGarMN = importeTotalRecGarMN.add(new BigDecimal(Recurso));
			} else { //IC_MONEDA=54
				numRegistrosDL++;
				montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
				montoTotalDescuentoDL = montoTotalDescuentoDL.add(new BigDecimal(montoDescuento));
				if(!"".equals(importeInteres)){
					importeTotalInteresDL = importeTotalInteresDL.add(new BigDecimal(importeInteres));
				}
				if(!"".equals(importeRecibir)){
					importeTotalRecibirDL = importeTotalRecibirDL.add(new BigDecimal(importeRecibir));
				}
				importeTotalRecGarDL = importeTotalRecGarDL.add(new BigDecimal(Recurso));
			}
						
			datos = new HashMap();			
			datos.put("IC_DOCUMENTO", icDocumento);
			datos.put("NOMBRE_EPO", nombreEpo);
			datos.put("NOMBRE_PYME", nombrePyme);
			datos.put("NUM_DOCTO", numeroDocumento);
			datos.put("F_VENCIMIENTO", fechaVencimiento);
			datos.put("IC_ESTATUS", icEstatus);	
			datos.put("ESTATUS", estatusDocumento);
			datos.put("MONEDA", monedaNombre);
			datos.put("MONTO", monto);  
			datos.put("POR_DESC", Porcentaje);
			datos.put("POR_GARANTIA", String.valueOf(Recurso));
			datos.put("MONTO_DESC", montoDescuento);
			datos.put("MONTO_INTERES", importeInteres);
			datos.put("MONTO_OPERAR", importeRecibir);
			datos.put("ESTATUS_ASIGNAR", "");	
			datos.put("AUTO_OPER_SF", autOperSF2);			
			datos.put("REFERENCIA",ref);
			registros.add(datos);	
			
		} //for 
		
		if( informacion.equals("Consultar") ){
			consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
			jsonObj.put("ic_estatus",ic_estatus);
			jsonObj.put("manejaNotasDeCreditoMultiples",manejaNotasDeCreditoMultiples2);		
		}
		
		if( informacion.equals("ConsultaTotales") ){
			
			for(int t =0; t<4; t++) {		
				datosTot = new HashMap();		
				if(t==0){ 	
					datosTot.put("MONEDA", "Total Moneda Nacional");
					datosTot.put("TOTAL_DOCTOS",String.valueOf(numRegistrosMN) );
					datosTot.put("TOTAL_MONTO", montoTotalMN);		
					datosTot.put("TOTAL_RECURSOS",importeTotalRecGarMN );	
					datosTot.put("TOTA_MONTO_DESC", montoTotalDescuentoMN);
					datosTot.put("TOTAL_MONTO_INTERES", importeTotalInteresMN );
					datosTot.put("TOTAL_MONTO_OPERAR", importeTotalRecibirMN);										
				}		
				if(t==1){ 		
					datosTot.put("MONEDA", "Total Dólares ");
					datosTot.put("TOTAL_DOCTOS",String.valueOf(numRegistrosDL) );
					datosTot.put("TOTAL_MONTO", montoTotalDL);		
					datosTot.put("TOTAL_RECURSOS", importeTotalRecGarDL );	
					datosTot.put("TOTA_MONTO_DESC", montoTotalDescuentoDL);
					datosTot.put("TOTAL_MONTO_INTERES", importeTotalInteresDL );
					datosTot.put("TOTAL_MONTO_OPERAR", importeTotalRecibirDL);						
				}				
				
				registrosTotales.add(datosTot);
			}
			
			consulta =  "{\"success\": true, \"total\": \"" + 2 + "\", \"registros\": " + registrosTotales.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
		
		}

	infoRegresar=jsonObj.toString();


}else if (informacion.equals("ConsultarPre")  ||  informacion.equals("ConsultaTotalesPre")  ) {  

	String seleccionados = (request.getParameter("doctosSeleccionados")!=null)?request.getParameter("doctosSeleccionados"):"";
	String estatusDoctos2 = (request.getParameter("estatusDoctos")!=null)?request.getParameter("estatusDoctos"):"";
	
	String[] doctosSeleccionados;
	String[] estatusDoctos;
	String delimiter = ",";
	doctosSeleccionados = seleccionados.split(delimiter);
	estatusDoctos = estatusDoctos2.split(delimiter);

	// Fodea 002 - 2010
	boolean operaNotasDeCredito                = false;
	boolean aplicarNotasDeCreditoAVariosDoctos = false;
	boolean hayDoctosConNotasDeCreditoMultiple = false;
	
	// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
	operaNotasDeCredito                = BeanParamDscto.operaNotasDeCreditoParaDescuentoElectronico(ic_epo);
	// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
	aplicarNotasDeCreditoAVariosDoctos = BeanParamDscto.hasAplicarNotaDeCreditoAVariosDoctosEnabled(ic_epo);
	
		//	Fodea 002 - 2010
   String estatusEspecificado = null;
	
	if( operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos ){
		// Obtener estatus indicado
		for(int i=0;i<estatusDoctos.length;i++){
			if( estatusDoctos[i] != null && !estatusDoctos[i].trim().equals("") ){
				estatusEspecificado =  estatusDoctos[i];
				break;
			}
		}
		// Actualizar seleccion de documentos
		ArrayList lista = BeanMantenimiento.actualizaDoctosSeleccionados(doctosSeleccionados,estatusDoctos,estatusEspecificado);
		
		doctosSeleccionados						= (String []) lista.get(0);
		estatusDoctos								= (String []) lista.get(1);
		hayDoctosConNotasDeCreditoMultiple 	= ((Boolean)  lista.get(2)).booleanValue(); 
	}
	
	
	Vector vDocumentosSelPyme = BeanMantenimiento.ovgetDocumentosSelPyme(ic_epo, "", "", "", "", "", "", iNoCliente, doctosSeleccionados, estatusDoctos, false, "");

	for (int i = 0; i < vDocumentosSelPyme.size(); i++) {
		Vector vDatosDocumentosSelPyme = (Vector) vDocumentosSelPyme.get(i);

		icDocumento = vDatosDocumentosSelPyme.get(0).toString();
		numeroDocumento = vDatosDocumentosSelPyme.get(1).toString();
		fechaVencimiento = vDatosDocumentosSelPyme.get(2).toString();
		monto = vDatosDocumentosSelPyme.get(3).toString();
		nombreEpo = vDatosDocumentosSelPyme.get(4).toString();
		estatusDocumento  = vDatosDocumentosSelPyme.get(5).toString();
		icMoneda = vDatosDocumentosSelPyme.get(6).toString();
		monedaNombre = vDatosDocumentosSelPyme.get(7).toString();
		Porcentaje = vDatosDocumentosSelPyme.get(8).toString();
		montoDescuento = vDatosDocumentosSelPyme.get(9).toString();
		importeInteres = vDatosDocumentosSelPyme.get(10).toString();
		importeRecibir = vDatosDocumentosSelPyme.get(11).toString();
		estatusNuevo	= vDatosDocumentosSelPyme.get(14).toString();
		nombrePyme	= vDatosDocumentosSelPyme.get(15).toString();
		String ref = vDatosDocumentosSelPyme.get(16).toString();
		
		Recurso = new Double(monto).doubleValue() - new Double(montoDescuento).doubleValue();
		String estatusNuevo2  ="";
		if(estatusNuevo.equals("N")) estatusNuevo2 = "Negociable";
		if(estatusNuevo.equals("O")) estatusNuevo2 = "Operado con Fondeo Propio";
		if(estatusNuevo.equals("SP")) estatusNuevo2 ="Seleccionada Pyme";	
		
		if (icMoneda.equals("1")) {
			numRegistrosMN++;
			montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
			montoTotalDescuentoMN = montoTotalDescuentoMN.add(new BigDecimal(montoDescuento));
			if(!"".equals(importeInteres)){
				importeTotalInteresMN = importeTotalInteresMN.add(new BigDecimal(importeInteres));
			}
			if(!"".equals(importeRecibir)){
				importeTotalRecibirMN = importeTotalRecibirMN.add(new BigDecimal(importeRecibir));
			}
			importeTotalRecGarMN = importeTotalRecGarMN.add(new BigDecimal(Recurso));
		} else { //IC_MONEDA=54
			numRegistrosDL++;
			montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
			montoTotalDescuentoDL = montoTotalDescuentoDL.add(new BigDecimal(montoDescuento));
			if(!"".equals(importeInteres)){
				importeTotalInteresDL = importeTotalInteresDL.add(new BigDecimal(importeInteres));
			}
			if(!"".equals(importeRecibir)){
				importeTotalRecibirDL = importeTotalRecibirDL.add(new BigDecimal(importeRecibir));
			}
			importeTotalRecGarDL = importeTotalRecGarDL.add(new BigDecimal(Recurso));
		}
		
		datos = new HashMap();			
		datos.put("IC_DOCUMENTO", icDocumento);
		datos.put("NOMBRE_EPO", nombreEpo);
		datos.put("NOMBRE_PYME", nombrePyme);
		datos.put("NUM_DOCTO", numeroDocumento);
		datos.put("F_VENCIMIENTO", fechaVencimiento);
		datos.put("ESTATUS_ANTERIOR", estatusDocumento);
		datos.put("ESTATUS_ASIGNAR", estatusNuevo);		
		datos.put("MONEDA", monedaNombre);
		datos.put("MONTO", monto);
		datos.put("POR_DESC", Porcentaje);
		datos.put("POR_GARANTIA", String.valueOf(Recurso));
		datos.put("MONTO_DESC", montoDescuento);
		datos.put("MONTO_INTERES", importeInteres);
		datos.put("MONTO_OPERAR", importeRecibir);
		datos.put("NUEVO_ESTATUS", estatusNuevo2);		
		datos.put("REFERENCIA", ref);		
		
		registros.add(datos);	

	}
	
	String mensajeEspecificado ="";
	if(hayDoctosConNotasDeCreditoMultiple){
		if(estatusEspecificado.equals("N")) {
			mensajeEspecificado =	 "<b> Nota: Al (los) Documento(s) Seleccionado(s) le(s) fue aplicado una o varias Notas de Crédito, dicha Nota de Crédito también afectó a otro(s) Documento(s), por lo que al Aceptar el Cambio de Estatus a Negociables se retornarán todos los Documentos relacionados. </b>";
		}else  if(estatusEspecificado.equals("SP")) {
			mensajeEspecificado =	 "<b> Nota: Al (los) Documento(s) Seleccionado(s) le(s) fue aplicado una o varias Notas de Crédito, dicha Nota de Crédito también afectó a otro(s) Documento(s), por lo que al Aceptar el Cambio de Estatus a Seleccionada Pyme se retornarán todos los Documentos relacionados.   </b>";
		
		}else  if(estatusEspecificado.equals("0")){
			mensajeEspecificado =	 "<b>Nota: Al (los) Documento(s) Seleccionado(s) le(s) fue aplicado una o varias Notas de Crédito, dicha Nota de Crédito también afectó a otro(s) Documento(s), por lo que al Aceptar el Cambio de Estatus a Operado con Fondeo propio se cambiarán también todos los Documentos relacionados.  </b>";
		
		}
	}
	
	if( informacion.equals("ConsultarPre") ){
		consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("estatusEspecificado",estatusEspecificado);	
		jsonObj.put("mensajeEspecificado",mensajeEspecificado);	
		
	}
		
	if( informacion.equals("ConsultaTotalesPre") ){
		for(int t =0; t<4; t++) {		
			datosTot = new HashMap();		
			if(t==0){ 
				
				datosTot.put("IC_MONEDA", "1");
				datosTot.put("MONEDA", "Total Moneda Nacional");
				datosTot.put("TOTAL_DOCTOS",String.valueOf(numRegistrosMN) );
				datosTot.put("TOTAL_MONTO", montoTotalMN);		
				datosTot.put("TOTAL_RECURSOS",importeTotalRecGarMN );	
				datosTot.put("TOTA_MONTO_DESC", montoTotalDescuentoMN);
				datosTot.put("TOTAL_MONTO_INTERES", importeTotalInteresMN );
				datosTot.put("TOTAL_MONTO_OPERAR", importeTotalRecibirMN);										
			}		
			if(t==1){ 	
				datosTot.put("IC_MONEDA", "54");
				datosTot.put("MONEDA", "Total Dólares ");
				datosTot.put("TOTAL_DOCTOS",String.valueOf(numRegistrosDL) );
				datosTot.put("TOTAL_MONTO", montoTotalDL);		
				datosTot.put("TOTAL_RECURSOS", importeTotalRecGarDL );	
				datosTot.put("TOTA_MONTO_DESC", montoTotalDescuentoDL);
				datosTot.put("TOTAL_MONTO_INTERES", importeTotalInteresDL );
				datosTot.put("TOTAL_MONTO_OPERAR", importeTotalRecibirDL);						
			}			
				
			registrosTotales.add(datosTot);
		}
		
		consulta =  "{\"success\": true, \"total\": \"" + 2 + "\", \"registros\": " + registrosTotales.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		
	}
		infoRegresar=jsonObj.toString();

}else if (informacion.equals("ConsultarAcuse")  ) { 

	String folioCert = "", claves = "", mensajeError ="", mensajeAcuse ="",clavesOper 	= "", clavesSeleccionadasPyme	= "",
	 acuse = "", 	fechaAct = "", horaAct = "", 	_acuse = "";
	char getReceipt = 'Y';
	ArrayList alClaves = null;
	Vector vDocumentosSelPyme =  new Vector();
	Seguridad s = new Seguridad();
	String externContent = (request.getParameter("textoFirmar")!=null)?request.getParameter("textoFirmar"):"";
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";	
	String seleccionados = (request.getParameter("doctosSeleccionados")!=null)?request.getParameter("doctosSeleccionados"):"";
	String estatusDoctos2 = (request.getParameter("estatusDoctos")!=null)?request.getParameter("estatusDoctos"):"";
	String ct_cambio_motivo = (request.getParameter("ct_cambio_motivo")!=null)?request.getParameter("ct_cambio_motivo"):"";
	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = archivo.nombreArchivo()+".pdf";	
   
   //System.out.println("\n\n=========================================================");
   //System.out.println(":: pkcs7 :: \n" + pkcs7);
   //System.out.println("=========================================================\n\n");
   
	String[] doctosSeleccionados;
	String[] estatusDoctos;
	String delimiter = ",";
	doctosSeleccionados = seleccionados.split(delimiter);
	estatusDoctos = estatusDoctos2.split(delimiter);					
	
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		folioCert = "123456789";	//Este numero no importa en esta pantalla!
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			
			try {
				
				// 1. Consultar datos de los documentos seleccionados
				vDocumentosSelPyme = BeanMantenimiento.ovgetDocumentosSelPyme(ic_epo, "", "", "", "", "", "", iNoCliente, doctosSeleccionados, estatusDoctos, false, "");
				
				// 2. Actualizar historial de cambios de estatus
				alClaves 					= (ArrayList)BeanMantenimiento.osactualizaHistoricoCambioEstatus(doctosSeleccionados, estatusDoctos,ct_cambio_motivo, strLogin + " " + strNombreUsuario);//FODEA 015 - 2009 ACF
				claves 						= (String)alClaves.get(0);
				clavesOper 					= (String)alClaves.get(1);
				clavesSeleccionadasPyme	= (String)alClaves.get(2);
				
				// 3. Cambiar estatus de los documentos
				// 3.1 Cambiar a estatus Negociable
				if(!claves.equals(""))
					BeanMantenimiento.bactualizaEstatusNegociable(claves);
				// 3.2 Cambiar a estatus Seleccionada Pyme
				if(!clavesSeleccionadasPyme.equals(""))
					BeanMantenimiento.bactualizaEstatusSeleccionadasPyme(clavesSeleccionadasPyme);
				
				// Obtener acuse de la operacion 
				_acuse = s.getAcuse();
				mensajeAcuse="<B>La autentificación se llevó acabo con éxito <br>Recibo:"+_acuse+"</b>";
				session.setAttribute("_acuse",_acuse);
				
				// Cambiar a estatus "Operado con Fondeo Propio"
				if(!clavesOper.equals("")){					
					acuse = beanAutDesc.OperaConFondeoPropio(iNoCliente,clavesOper);
					fechaAct = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
					horaAct = (new SimpleDateFormat ("hh:mm a")).format(new java.util.Date());	
				}
				
				// Generar PDF					
				ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
														session.getAttribute("iNoNafinElectronico").toString(),
											 (String)session.getAttribute("sesExterno"),
														(String) session.getAttribute("strNombre"),
															(String) session.getAttribute("strNombreUsuario"),
											 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
				pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
				pdfDoc.addText("La autentificación se llevó a cabo con éxito","formasB",ComunesPDF.CENTER);
				pdfDoc.addText("Recibo: "+_acuse,"formasB",ComunesPDF.CENTER);
				if(acuse!=null&&!acuse.equals("")){
					pdfDoc.setTable(2,65);
					pdfDoc.setCell("CIFRAS DE CONTROL","celda01",ComunesPDF.CENTER,2,1,1);
					pdfDoc.setCell("Número de Acuse","celda01",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("Fecha","celda01",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(fechaAct,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("Hora","celda01",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(horaAct,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("Usuario de captura","celda01",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(iNoUsuario+" - "+strNombreUsuario,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.addTable();
				}
				pdfDoc.setTable(14,95);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Nombre PyME/Cedente","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Número de Documento","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Estatus Anterior","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Recurso en garantía","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Monto Interés","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Monto a operar","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Nuevo Estatus","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER,1,2);
				
				//para la consulta 
				for (int i = 0; i < vDocumentosSelPyme.size(); i++) {
					numRegistros ++;
					Vector vDatosDocumentosSelPyme = (Vector) vDocumentosSelPyme.get(i);					
					icDocumento = vDatosDocumentosSelPyme.get(0).toString();
					numeroDocumento = vDatosDocumentosSelPyme.get(1).toString();
					fechaVencimiento = vDatosDocumentosSelPyme.get(2).toString();
					monto = vDatosDocumentosSelPyme.get(3).toString();
					nombreEpo = vDatosDocumentosSelPyme.get(4).toString();
					estatusDocumento  = vDatosDocumentosSelPyme.get(5).toString();
					icMoneda = vDatosDocumentosSelPyme.get(6).toString();
					monedaNombre = vDatosDocumentosSelPyme.get(7).toString();
					Porcentaje = vDatosDocumentosSelPyme.get(8).toString();
					montoDescuento = vDatosDocumentosSelPyme.get(9).toString();
					importeInteres = vDatosDocumentosSelPyme.get(10).toString();
					importeRecibir = vDatosDocumentosSelPyme.get(11).toString();
					estatusNuevo	= vDatosDocumentosSelPyme.get(14).toString();
					nombrePyme	= vDatosDocumentosSelPyme.get(15).toString();
					String ref = vDatosDocumentosSelPyme.get(16).toString();
					
					Recurso = new Double(monto).doubleValue() - new Double(montoDescuento).doubleValue();
					
					String estatusNuevo2  ="";
					if(estatusNuevo.equals("N")) estatusNuevo2 = "Negociable";
					if(estatusNuevo.equals("O")) estatusNuevo2 = "Operado con Fondeo Propio";
					if(estatusNuevo.equals("SP")) estatusNuevo2 ="Seleccionada Pyme";	
					
		
					if (icMoneda.equals("1")) {
						numRegistrosMN++;
						montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
						montoTotalDescuentoMN = montoTotalDescuentoMN.add(new BigDecimal(montoDescuento));
						if(!"".equals(importeInteres)){
							importeTotalInteresMN = importeTotalInteresMN.add(new BigDecimal(importeInteres));
						}
						if(!"".equals(importeRecibir)){
							importeTotalRecibirMN = importeTotalRecibirMN.add(new BigDecimal(importeRecibir));
						}
						importeTotalRecGarMN = importeTotalRecGarMN.add(new BigDecimal(Recurso));
					} else { //IC_MONEDA=54
						numRegistrosDL++;
						montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
						montoTotalDescuentoDL = montoTotalDescuentoDL.add(new BigDecimal(montoDescuento));
						if(!"".equals(importeInteres)){
							importeTotalInteresDL = importeTotalInteresDL.add(new BigDecimal(importeInteres));
						}
						if(!"".equals(importeRecibir)){
							importeTotalRecibirDL = importeTotalRecibirDL.add(new BigDecimal(importeRecibir));
						}
						importeTotalRecGarDL = importeTotalRecGarDL.add(new BigDecimal(Recurso));
					}
				
					datos = new HashMap();			
					datos.put("IC_DOCUMENTO", icDocumento);
					datos.put("NOMBRE_EPO", nombreEpo);
					datos.put("NOMBRE_PYME", nombrePyme);
					datos.put("NUM_DOCTO", numeroDocumento);
					datos.put("F_VENCIMIENTO", fechaVencimiento);
					datos.put("ESTATUS_ANTERIOR", estatusDocumento);
					datos.put("MONEDA", monedaNombre);
					datos.put("MONTO", monto);
					datos.put("POR_DESC", Porcentaje);
					datos.put("POR_GARANTIA", String.valueOf(Recurso));
					datos.put("MONTO_DESC", montoDescuento);
					datos.put("MONTO_INTERES", importeInteres);
					datos.put("MONTO_OPERAR", importeRecibir);
					datos.put("NUEVO_ESTATUS", estatusNuevo2);		
					datos.put("REFERENCIA", ref);
					registros.add(datos);	
										
					pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(numeroDocumento,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(estatusDocumento,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(monedaNombre,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(Comunes.formatoDecimal(Porcentaje,0)+"%","formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(Recurso),2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeInteres,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeRecibir,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(estatusNuevo2,	"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(ref,"formas",ComunesPDF.CENTER,1,2);
				}// for
				
				if (numRegistrosMN!=0) {
					pdfDoc.setCell("Total Moneda Nacional","formas",ComunesPDF.CENTER,6,2);
					pdfDoc.setCell(Comunes.formatoDecimal(montoTotalMN,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecGarMN,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDescuentoMN,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalInteresMN,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecibirMN,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
				}
				if (numRegistrosDL!=0) {	
					pdfDoc.setCell("Total Dolares","formas",ComunesPDF.CENTER,6,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDL,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecGarDL,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDescuentoDL,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalInteresDL,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecibirDL,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
				} //fin 
				if (numRegistrosMN!=0) {
					pdfDoc.setCell("Total de Documentos Moneda Nacional","formas",ComunesPDF.CENTER,6,2);
					pdfDoc.setCell(" "+numRegistrosMN,"formas",ComunesPDF.LEFT,8,2);
				}
				if (numRegistrosDL!=0) {
					pdfDoc.setCell("Total de Documentos Dolares","formas",ComunesPDF.CENTER,6,2);
					pdfDoc.setCell(" "+numRegistrosDL,"formas",ComunesPDF.LEFT,8,2);		
				}					
				pdfDoc.addTable();
				pdfDoc.endDocument();
							
			} catch (NafinException errDoc) { //Error de concurrencia o en los cambios			
				mensajeAcuse= " <B>La autentificación no se llevó acabo :</b><br>"+errDoc.getMsgError();	
				mensajeError = "<b>PROCESO CANCELADO</b>";
			}
		}else {
			mensajeAcuse= " La autentificación no se llevó acabo :</b><br>"+s.mostrarError();
			mensajeError = "<b>PROCESO CANCELADO</b>";
			
		}		
	}
		
	if( informacion.equals("ConsultarAcuse") ){	
		consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("mensajeAcuse",mensajeAcuse);
		jsonObj.put("mensajeError",mensajeError);		
		jsonObj.put("usuario",iNoUsuario+" - "+strNombreUsuario);	
		jsonObj.put("acuse",acuse);		
		jsonObj.put("fecha",fechaAct);
		jsonObj.put("hora",horaAct);	
		jsonObj.put("nombreArchivo",nombreArchivo);		
		jsonObj.put("numRegistrosMN",String.valueOf(numRegistrosMN) );
		jsonObj.put("montoTotalMN", montoTotalMN);		
		jsonObj.put("importeTotalRecGarMN",importeTotalRecGarMN );	
		jsonObj.put("montoTotalDescuentoMN", montoTotalDescuentoMN);
		jsonObj.put("importeTotalInteresMN", importeTotalInteresMN );
		jsonObj.put("importeTotalRecibirMN", importeTotalRecibirMN);	
		
		jsonObj.put("numRegistrosDL",String.valueOf(numRegistrosDL) );
		jsonObj.put("montoTotalDL", montoTotalDL);		
		jsonObj.put("importeTotalRecGarDL", importeTotalRecGarDL );	
		jsonObj.put("montoTotalDescuentoDL", montoTotalDescuentoDL);
		jsonObj.put("importeTotalInteresDL", importeTotalInteresDL );
		jsonObj.put("importeTotalRecibirDL", importeTotalRecibirDL);		
	}
	infoRegresar =jsonObj.toString();
					
} else if( informacion.equals("ConsultaTotalesAcuse") ){

	String numRegistrosMN2 = (request.getParameter("numRegistrosMN")!=null)?request.getParameter("numRegistrosMN"):"";	
	String montoTotalMN2 = (request.getParameter("montoTotalMN")!=null)?request.getParameter("montoTotalMN"):"";	
	String importeTotalRecGarMN2 = (request.getParameter("importeTotalRecGarMN")!=null)?request.getParameter("importeTotalRecGarMN"):"";	
	String montoTotalDescuentoMN2 = (request.getParameter("montoTotalDescuentoMN")!=null)?request.getParameter("montoTotalDescuentoMN"):"";	
	String importeTotalInteresMN2 = (request.getParameter("importeTotalInteresMN")!=null)?request.getParameter("importeTotalInteresMN"):"";	
	String importeTotalRecibirMN2 = (request.getParameter("importeTotalRecibirMN")!=null)?request.getParameter("importeTotalRecibirMN"):"";	
	
	String numRegistrosDL2 = (request.getParameter("numRegistrosDL")!=null)?request.getParameter("numRegistrosDL"):"";	
	String montoTotalDL2 = (request.getParameter("montoTotalDL")!=null)?request.getParameter("montoTotalDL"):"";	
	String importeTotalRecGarDL2 = (request.getParameter("importeTotalRecGarDL")!=null)?request.getParameter("importeTotalRecGarDL"):"";	
	String montoTotalDescuentoDL2 = (request.getParameter("montoTotalDescuentoDL")!=null)?request.getParameter("montoTotalDescuentoDL"):"";	
	String importeTotalInteresDL2 = (request.getParameter("importeTotalInteresDL")!=null)?request.getParameter("importeTotalInteresDL"):"";	
	String importeTotalRecibirDL2 = (request.getParameter("importeTotalRecibirDL")!=null)?request.getParameter("importeTotalRecibirDL"):"";	
			
		for(int t =0; t<4; t++) {		
			datosTot = new HashMap();		
			if(t==0){ 						
				datosTot.put("IC_MONEDA", "1");
				datosTot.put("MONEDA", "Total Moneda Nacional");
				datosTot.put("TOTAL_DOCTOS",numRegistrosMN2 );
				datosTot.put("TOTAL_MONTO", montoTotalMN2);		
				datosTot.put("TOTAL_RECURSOS",importeTotalRecGarMN2 );	
				datosTot.put("TOTA_MONTO_DESC", montoTotalDescuentoMN2);
				datosTot.put("TOTAL_MONTO_INTERES", importeTotalInteresMN2 );
				datosTot.put("TOTAL_MONTO_OPERAR", importeTotalRecibirMN2);										
			}		
			if(t==1){ 	
				datosTot.put("IC_MONEDA", "54");
				datosTot.put("MONEDA", "Total Dólares ");
				datosTot.put("TOTAL_DOCTOS",numRegistrosDL2 );
				datosTot.put("TOTAL_MONTO", montoTotalDL2);		
				datosTot.put("TOTAL_RECURSOS", importeTotalRecGarDL2);	
				datosTot.put("TOTA_MONTO_DESC", montoTotalDescuentoDL2);
				datosTot.put("TOTAL_MONTO_INTERES", importeTotalInteresDL2 );
				datosTot.put("TOTAL_MONTO_OPERAR", importeTotalRecibirDL2);						
			}			
						
			registrosTotales.add(datosTot);
		}
		consulta =  "{\"success\": true, \"total\": \"" + 2 + "\", \"registros\": " + registrosTotales.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		infoRegresar =jsonObj.toString();

} else if( informacion.equals("ArchivoPDF") ){

	String strNombreArchivo = (request.getParameter("strNombreArchivo")!=null)?request.getParameter("strNombreArchivo"):"";	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+strNombreArchivo);
	
	infoRegresar =jsonObj.toString();
	
}
%>
<%=infoRegresar%>

<% //System.out.println("   infoRegresar  "+infoRegresar); %> 

<%!
	public List  datosPymes(String  num_electronico , String ic_epo ){
		
		List datos =new ArrayList();
		String 				qrySentencia 	= null;
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		String mensaje = "", ic_pyme ="", txtNombre="";
		
		try{	
			con.conexionDB();
		
			if(!"".equals(num_electronico)) {// && !"".equals(icEPO)							
							
				qrySentencia =" SELECT distinct n.ic_epo_pyme_if  as Pyme,  p.cg_razon_social as nombre "   +
								  " FROM comrel_nafin n, comcat_pyme p "   +
								  " WHERE n.ic_epo_pyme_if = p.ic_pyme "+
								  " AND ic_nafin_electronico =  ? " ;
								  
			//System.out.println(qrySentencia);
					
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(num_electronico));
				rs = ps.executeQuery();
				if(rs.next()){
					ic_pyme = rs.getString(1)==null?"":rs.getString(1);
					txtNombre = rs.getString(2)==null?"":rs.getString(2);		
					datos.add(ic_pyme);
					datos.add(txtNombre);		
					
				} 
				rs.close();
				ps.close();
			}
	} catch(Exception e) {
	
		e.printStackTrace();
	} finally {	
		if(con.hayConexionAbierta()) 
			con.cierraConexionDB();	
	}
		return datos;
	}
%>