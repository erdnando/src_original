Ext.onReady(function() {

	var horarioValido = true;
	var seleccionados =[];

	var procesarEnvioPreAuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
					
			var fp 		= Ext.getDom('formDatos');
				fp.doctoSeleccionados.value =jsonData.doctoSeleccionados;
				fp.ic_pyme.value =jsonData.ic_pyme;
				fp.ic_epo.value =jsonData.ic_epo;
				fp.ic_banco_fondeo.value =jsonData.ic_banco_fondeo;
				fp.mensajeFondeo.value =jsonData.mensajeFondeo;
				fp.numeroDoctoMN.value =jsonData.numeroDoctoMN;
				fp.numeroDoctoDL.value =jsonData.numeroDoctoDL;
				fp.pantalla.value ="PreAcuse";
				fp.action 	= "13forma03Ext.jsp";
				fp.target	= "_self";
				fp.submit();

			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var generarSolicitudes =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(infoR.mensajeHorario!=''){	
				var mensaje;
				Ext.MessageBox.alert('Aviso',infoR.mensajeHorario);
			}else{

				var numeroDoctoMN=0;	
				var numeroDoctoDL=0;	
				seleccionados  = [];

				var  gridConsulta = Ext.getCmp('gridConsulta');
				var store = gridConsulta.getStore();

					store.each(function(record){
						if(record.data['SELECCIONAR']=='S'){

							if(record.data['MENSAJE']!='SobreGirado' &&  record.data['IC_MONEDA']==1){
								seleccionados.push(record.data['CLAVE_DOCUMENTO']);
								if(record.data['IC_MONEDA']=='1'){	 	numeroDoctoMN++;}
							}

							if(record.data['MENSAJE_DL']!='SobreGirado' &&  record.data['IC_MONEDA']==54 ){
								seleccionados.push(record.data['CLAVE_DOCUMENTO']);
								if(record.data['IC_MONEDA']=='54'){	 	numeroDoctoDL++;}
							}

						}
					});
					
					if (seleccionados == ''){
						Ext.MessageBox.alert('Mensaje','No ha seleccionado documentos');
						return;
					} else {
						if (Ext.getCmp("hayClaseDocto1").getValue() =="NO") {
							Ext.MessageBox.alert('Mensaje','No existe una clase de documento asociado para realizar Descuento Electronico.');
							return;
						} else {
						
						var ic_pyme = Ext.getCmp('ic_pyme1').getValue();
						var ic_epo = Ext.getCmp('ic_epo1').getValue();
						var ic_banco_fondeo = Ext.getCmp('ic_banco_fondeo1').getValue();				
						var mensajeFondeo = Ext.getCmp('mensajeFondeo1').getValue();
									
						Ext.Ajax.request({
								url: '13forma03Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									pantalla:"PreAcuse",
									informacion:"procesarEnvioPreAuse",
									seleccionados :seleccionados,
									ic_pyme:ic_pyme,
									ic_epo:ic_epo,
									ic_banco_fondeo:ic_banco_fondeo,
									mensajeFondeo:mensajeFondeo,										
									numeroDoctoMN:numeroDoctoMN,
									numeroDoctoDL:numeroDoctoDL	
								})					
								,callback: procesarEnvioPreAuse  
							});
											
							
						}
					}
				
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// DescargaArchivo es un servlet  para descargar los archivos
	function procesarSuccessFailureArchivoFijo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	
	function procesarSuccessFailureArchivoInterfase(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesarSuccessFailureArchivoVariable(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarSuccessFailureArchivoPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	// DescargaArchivo es un servlet  para descargar los archivos
	function procesarSuccessFailureArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Contenedor de Botones de Impresion
	var fpBotones = new Ext.Container({
		layout: 'table',		
		style: 'margin:0 auto;',
		id: 'fpBotones',	
		hidden: true,	
		width:	'800',
		heigth:	'auto',		
		items: [
			{
				xtype: 'button',
				text: 'Imp. Archivo',
				tooltip:	'Imp. Archivo',
				iconCls: 'icoXls',
				id: 'btnGenerarCSV',			
				handler: function(boton, evento) {

					Ext.Ajax.request({
						url: '13forma03Archivos.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV',
							tipoArchivo:'CSV'
						})					
						,callback: procesarSuccessFailureArchivo
					});
				}
			},
			{
				xtype: 'button',
				text: 'Imp. PDF',
				tooltip:	'Imp. PDF',
				iconCls: 'icoPdf',
				id: 'btnGenerarPDF',			
				handler: function(boton, evento) {
					
					Ext.Ajax.request({
						url: '13forma03Archivos.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoPDF',
							tipoArchivo:'PDF'
						})					
						,callback: procesarSuccessFailureArchivoPDF
					});
				}
			},			
			{
				xtype: 'button',
				text: 'Generar Solicitudes',
				tooltip:	'Generar Solicitudes',
				id: 'btnGenerarSolicitudes',	
				iconCls: 'icoAceptar',
				handler: function(){
					var cmbEPO = Ext.getCmp('ic_epo1');
					Ext.Ajax.request({
							url: '13forma03Ext.data.jsp',
							params: {
								informacion: 'ValidaHorario',
								ic_epo:cmbEPO.getValue()			
							},
							callback: generarSolicitudes
						});
				}
			},					
			{
				xtype: 'button',
				text: 'Exp. Interfase Variable',
				tooltip:	'Exp. Interfase Variable',
				id: 'btnGenerarVariable',	
				iconCls: 'icoXls',
				handler: function(boton, evento) {
				
					Ext.Ajax.request({
						url: '13forma03Archivos.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoVariable'							
						})					
						,callback: procesarSuccessFailureArchivoVariable
					});
				}
			},			
			{
				xtype: 'button',
				text: 'Exp. Interfase Fijo ',
				tooltip:	'Exportar Interfase Fijo',
				iconCls: 'icoTxt',
				id: 'btnGenerarFijo',			
				handler: function(boton, evento) {	
				
					Ext.Ajax.request({
						url: '13forma03Archivos.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoFijo'							
						})					
						,callback: procesarSuccessFailureArchivoFijo
					});
				}
			},
			{
				xtype: 'button',
				text: 'Imp. Interfase ',
				tooltip:	'Imp. Interfase',
				id: 'btnGenerarInterfase',	
				iconCls: 'icoPdf',
				handler: function(boton, evento) {
					Ext.Ajax.request({
						url: '13forma03Archivos.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoInterfase'							
						})					
						,callback: procesarSuccessFailureArchivoInterfase
					});
				}
			}
		]
	});
	
	// esto es para seleccionar los registros al ingresar a la pantalla
	var selectDefaultDoctos = function(grid){
		var indiceSm = 0;	
		grid.getStore().each(function(record) {
	
			if (record.data['MENSAJE']!='SobreGirado' &&  record.data['MENSAJE_DL']!='SobreGirado' ){
				selectModel.selectRow(indiceSm,true);
			}else  {				
				if (record.data['MENSAJE']!='SobreGirado' &&  record.data['IC_MONEDA']==1  )  {
					selectModel.selectRow(indiceSm,true);
				}
				if (record.data['MENSAJE_DL']!='SobreGirado' &&  record.data['IC_MONEDA']==54 )  {
					selectModel.selectRow(indiceSm,true);
				}			
			}
			indiceSm = indiceSm+1;		
		});
	}
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var jsonData = store.reader.jsonData;			
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
		
			if(jsonData.existe_mandato=='S'){
				gridConsulta.setTitle('Tiene documentos pendientes de autorizar de PyMEs que fueron apoyados con cr�dito y tienen Instrucci�n Irrevocable');	
			}
			
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
							
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			
			var fpBotones = Ext.getCmp('fpBotones');		
			var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');	
			
			Ext.getCmp("mensaje1").setValue(jsonData.mensaje);
			Ext.getCmp("nombreEPO1").setValue(jsonData.nombreEPO);
			Ext.getCmp("montoDi1").setValue(jsonData.montoDi);
			Ext.getCmp("hayClaseDocto1").setValue(jsonData.hayClaseDocto);			
			Ext.getCmp("numRegistros").setValue(jsonData.numRegistros);	
			Ext.getCmp("claveDoctoTodos").setValue(jsonData.claveDoctoTodos);	
			
			Ext.getCmp("mensaje_dl").setValue(jsonData.mensaje_dl);
			Ext.getCmp("montoDi_dl").setValue(jsonData.montoDi_dl);
			
			 var mensajeSobreGiro  = '<table width="600" align="center">'+
											 '<tr><td width="600"> <B> NOTA  </td></tr>';
											 
		
			if(jsonData.mensaje =='SobreGirado' &&  jsonData.mensaje_dl =='SobreGirado') {			
				Ext.MessageBox.alert('Mensaje','El Limite de la cadena '+jsonData.nombreEPO+'  se encuentra sobregirado por $'+jsonData.montoDi+'  en  Moneda Nacional y  por $'+jsonData.montoDi_dl+'  en  D�lar Americano.');
			
				mensajeSobreGiro  += '<tr><td width="600"> <br> El Limite de la cadena '+jsonData.nombreEPO+'  se encuentra sobregirado por $'+jsonData.montoDi+
											 ' en  Moneda Nacional y  por $'+jsonData.montoDi_dl+' en  D�lar Americano.'+
											 '</td></tr>';					
		
			} else  	if(jsonData.mensaje =='SobreGirado' || jsonData.mensaje_dl =='SobreGirado') {		
				if(jsonData.mensaje =='SobreGirado') {
					Ext.MessageBox.alert('Mensaje','El Limite de la cadena  '+jsonData.nombreEPO+'  se encuentra sobregirado por $'+jsonData.montoDi+'  en  Moneda Nacional.');
				
					mensajeSobreGiro  += '<tr><td width="600">'+
											 ' <br> El Limite de la cadena '+jsonData.nombreEPO+'  se encuentra sobregirado por $'+jsonData.montoDi+
											 ' en  Moneda Nacional. </td></tr>';	
			} 
				
				if(jsonData.mensaje_dl =='SobreGirado') {
					Ext.MessageBox.alert('Mensaje','El Limite de la cadena  '+jsonData.nombreEPO+'  se encuentra sobregirado por $'+jsonData.montoDi_dl+'  en  D�lar Americano.');
					
					mensajeSobreGiro  += '<tr><td width="600">'+
											 ' <br> El Limite de la cadena '+jsonData.nombreEPO+'  se encuentra sobregirado por  $'+jsonData.montoDi_dl+'  en  D�lar Americano.'+
											 '</td></tr>';	
				} 
			}
			 	
			 mensajeSobreGiro  +='<tr><td width="600"><br>Solo se podr�  procesar y descargar los registros que no tengan un sobregiro en el monto limite de su moneda.   </td></tr>';
			 mensajeSobreGiro  += '</table>';
			 
			
			Ext.getCmp("mensajeGiro").update(mensajeSobreGiro);
			if(jsonData.mensaje =='SobreGirado' || jsonData.mensaje_dl =='SobreGirado') {	
				Ext.getCmp("mensajeSobreGiro").show();
			}
			
			
			if(jsonData.bOperaFactorajeDist=='S' || jsonData.bOperaFactorajeVencInfonavit=='S'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_RECIBIR_BENE'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);
				
			}else  if(jsonData.bOperaFactorajeDist=='N' || jsonData.bOperaFactorajeVencInfonavit=='N'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_RECIBIR_BENE'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), true);
			}
			if(jsonData.bTipoFactoraje =='S') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
			}else if(jsonData.bTipoFactoraje =='N') {				
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			}
						
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');	
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');	
			var btnGenerarVariable = Ext.getCmp('btnGenerarVariable');	
			var btnGenerarFijo = Ext.getCmp('btnGenerarFijo');
			var btnGenerarInterfase = Ext.getCmp('btnGenerarInterfase');
			var btnGenerarSolicitudes = Ext.getCmp('btnGenerarSolicitudes');

			if(store.getTotalCount() > 0) {	
					
			selectDefaultDoctos(gridConsulta);
		
				consultaDataTotales.load({
					params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar'							
					})
				});
					
				fpBotones.show();
				gridConsultaTotales.show();
				
				if(horarioValido){
					btnGenerarCSV.enable();
					btnGenerarCSV.setTooltip('Imp. Archivo');
					
					btnGenerarPDF.enable();	
					btnGenerarPDF.setTooltip('Imp. PDF');
					
					btnGenerarSolicitudes.enable();	
					btnGenerarSolicitudes.setTooltip('Generar Solicitudes');
					
					btnGenerarVariable.enable();	
					btnGenerarVariable.setTooltip('Exp. Interfase Variable');
					
					btnGenerarFijo.enable();
					btnGenerarFijo.setTooltip('Exportar Interfase Fijo');
					
					btnGenerarInterfase.enable();
					btnGenerarInterfase.setTooltip('Imp. Interfase');
				}else{
					var botones = fpBotones.findByType('button');
					for (var x=0; x<botones.length;x++){
						Ext.getCmp(botones[x].id).disable();
						Ext.getCmp(botones[x].id).setTooltip('Lo sentimos est� usted fuera de los rangos de horario estable');
					}
				}
				
				if(jsonData.mensaje =='SobreGirado' &&  jsonData.mensaje_dl =='SobreGirado') {							
					Ext.getCmp('btnGenerarCSV').disable();	
					Ext.getCmp('btnGenerarPDF').disable();	
					Ext.getCmp('btnGenerarSolicitudes').disable();	
					Ext.getCmp('btnGenerarVariable').disable();	
					Ext.getCmp('btnGenerarFijo').disable();	
					Ext.getCmp('btnGenerarInterfase').disable();				
				}				
				el.unmask();					
			} else {		
				fpBotones.hide();
				gridConsultaTotales.hide();
				btnGenerarCSV.disable();	
				btnGenerarPDF.disable();	
				btnGenerarVariable.disable();	
				btnGenerarFijo.disable();
				btnGenerarInterfase.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
		
		//esto esta en duda como manejarlo
	var gruposConsulta = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 1, align: 'center'},
				{	header: 'Por Autorizar', colspan: 2, align: 'center'},	
				{	header: 'Seleccionadas ', colspan: 2, align: 'center'}
			]
		]
	});
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},
		hidden: true,
		fields: [	
			{name: 'IC_MONEDA'},
			{name: 'MONEDA'},
			{name: 'NUM_DOCTOS_AUTO'},
			{name: 'TOTAL_MONTOS_AUTO'},
			{name: 'NUM_DOCTOS_SELEC'},
			{name: 'TOTAL_MONTOS_SELEC'}				
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	
	var gridConsultaTotales = new Ext.grid.GridPanel({
		id: 'gridConsultaTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'C O N T R O L E S',
		hidden: true,
		plugins: gruposConsulta,	
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'N�mero de Documentos',
				tooltip: 'N�mero de Documentos',
				dataIndex : 'NUM_DOCTOS_AUTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'Montos',
				tooltip: 'Montos',
				dataIndex : 'TOTAL_MONTOS_AUTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
				{							
				header : 'N�mero de Documentos',
				tooltip: 'N�mero de Documentos',
				dataIndex : 'NUM_DOCTOS_SELEC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'Montos',
				tooltip: 'Montos',
				dataIndex : 'TOTAL_MONTOS_SELEC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 200,
		width: 660,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	
	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [	
			{name: 'COLOR_FUENTE'},
			{name: 'NOMBRE_EPO'},	
			{name: 'NU_SIRAC'},
			{name: 'NOMBRE_PROVEEDOR'},
			{name: 'NUMER0_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'MONEDA'},
			{name: 'IC_MONEDA'},			
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO'},
			{name: 'PORCE_DESCUENTO'},
			{name: 'RECURSO_GARANTIA'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'TASA'},
			{name: 'PLAZO'},
			{name: 'NUM_PROVEEDOR'},
			{name: 'BENEFICIARIO'},
			{name: 'BANCO_BENEFICIARIO'},
			{name: 'SUCURSAL_BENEFICIARIO'},
			{name: 'CUENTA_BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR_BENE'},
			{name: 'NETO_RECIBIR'},
			{name: 'OBSERVACION'},
			{name: 'SELECCIONAR'},
			{name: 'CLAVE_DOCUMENTO'},
			{name: 'MENSAJE'},
			{name: 'MENSAJE_DL'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	var total_montos_auto=0; 	
	var total_monto_select =0;	
	
var recalcular = function(selectModel, rowIndex, record){
		
		var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');
		var store = gridConsultaTotales.getStore();	
		var numRegistros = Ext.getCmp('numRegistros').getValue();
		
			num_docto_auto =0;
			total_monto_select =0;
			
		store.each(function(record2) {			
			
			
			// para quitar la seleccion
			if(record2.data['IC_MONEDA'] ==1 && record.data['IC_MONEDA'] ==1  && record.data['SELECCIONAR'] =='N' ) {
			
				var  num_docto_auto = record2.data['NUM_DOCTOS_AUTO'];
				var  num_docto_select = record2.data['NUM_DOCTOS_SELEC'];														
				if(num_docto_select!=0){
					num_docto_select --;								
					total_monto_select = parseFloat(record2.data['TOTAL_MONTOS_SELEC'])-parseFloat(record.data['MONTO_OPERAR']); 
				}
				total_montos_auto = parseFloat(record2.data['TOTAL_MONTOS_AUTO'])+parseFloat(record.data['MONTO_OPERAR']); 
				num_docto_auto ++;
				if(num_docto_select==0)  {
					total_monto_select =0;
				}	
				record2.data['NUM_DOCTOS_AUTO'] =num_docto_auto;
				record2.data['NUM_DOCTOS_SELEC'] =num_docto_select;						
				record2.data['TOTAL_MONTOS_AUTO'] = total_montos_auto;
				record2.data['TOTAL_MONTOS_SELEC'] = total_monto_select;	
			}
			
			if(record2.data['IC_MONEDA'] ==54 && record.data['IC_MONEDA'] ==54  && record.data['SELECCIONAR'] =='N') {
				var  num_docto_auto = record2.data['NUM_DOCTOS_AUTO'];
				var  num_docto_select = record2.data['NUM_DOCTOS_SELEC'];														
				if(num_docto_select!=0){
					num_docto_select --;								
					total_monto_select = parseFloat(record2.data['TOTAL_MONTOS_SELEC'])-parseFloat(record.data['MONTO_OPERAR']); 
				}
				total_montos_auto = parseFloat(record2.data['TOTAL_MONTOS_AUTO'])+parseFloat(record.data['MONTO_OPERAR']); 
				num_docto_auto ++;
				if(num_docto_select==0)  {
					total_monto_select =0;
				}		
				record2.data['NUM_DOCTOS_AUTO'] =num_docto_auto;
				record2.data['NUM_DOCTOS_SELEC'] =num_docto_select;						
				record2.data['TOTAL_MONTOS_AUTO'] = total_montos_auto;
				record2.data['TOTAL_MONTOS_SELEC'] = total_monto_select;													
			}
			
			// para agregar la  la seleccion
			if(record2.data['IC_MONEDA'] ==1 && record.data['IC_MONEDA'] ==1  && record.data['SELECCIONAR'] =='S') {
				var  num_docto_auto = record2.data['NUM_DOCTOS_AUTO'];
				var  num_docto_select = record2.data['NUM_DOCTOS_SELEC'];	
				if(num_docto_select !=numRegistros )  {
					if(num_docto_auto!=0){
						num_docto_auto --;
						total_montos_auto = parseFloat(record2.data['TOTAL_MONTOS_AUTO'])- parseFloat(record.data['MONTO_OPERAR']); 
					}
						
					total_monto_select = parseFloat(record2.data['TOTAL_MONTOS_SELEC'])+parseFloat(record.data['MONTO_OPERAR']); 
					num_docto_select ++;	
					
					if(num_docto_auto==0)  {
						total_montos_auto =0;
					}	
				
					record2.data['NUM_DOCTOS_AUTO'] =num_docto_auto;											
					record2.data['TOTAL_MONTOS_AUTO'] = total_montos_auto;
					record2.data['NUM_DOCTOS_SELEC'] =num_docto_select;	
					record2.data['TOTAL_MONTOS_SELEC'] = total_monto_select;	
				}
			}
			if(record2.data['IC_MONEDA'] ==54 && record.data['IC_MONEDA'] ==54  && record.data['SELECCIONAR'] =='S') {
				var  num_docto_auto = record2.data['NUM_DOCTOS_AUTO'];
				var  num_docto_select = record2.data['NUM_DOCTOS_SELEC'];														
				if(num_docto_auto!=0){
					num_docto_auto --;
					total_montos_auto = parseFloat(record2.data['TOTAL_MONTOS_AUTO'])-parseFloat(record.data['MONTO_OPERAR']); 
				}
				total_monto_select = parseFloat(record2.data['TOTAL_MONTOS_SELEC'])+parseFloat(record.data['MONTO_OPERAR']); 
				num_docto_select ++;	
				if(num_docto_auto==0)  {
					total_montos_auto =0;
				}	
				record2.data['NUM_DOCTOS_AUTO'] =num_docto_auto;
				record2.data['NUM_DOCTOS_SELEC'] =num_docto_select;						
				record2.data['TOTAL_MONTOS_AUTO'] = total_montos_auto;
				record2.data['TOTAL_MONTOS_SELEC'] = total_monto_select;									
			}
		 
		 record2.commit();
		});					
	}
		
	var selectModel = new Ext.grid.CheckboxSelectionModel( {
		checkOnly: true,
		header:"<div>&#160;</div>",		
	  	listeners: {
			rowdeselect: function(selectModel, rowIndex, record) {	// cuando se quita la selecci�n
		
				record.data['SELECCIONAR']='N';
				record.commit();				
				recalcular	(selectModel, rowIndex, record);				  
			},
			//Se activa cuando una fila est� seleccionada, return false para cancelar.
			//rowselect: function(selectModel, rowIndex, record) {
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ //Se activa cuando una fila est� seleccionada, return false para cancelar.		
		
			record.data['SELECCIONAR']='S';	
				record.commit();
				recalcular	(selectModel, rowIndex, record);							
			}		
		},
		renderer: function(value, metadata, record, rowindex, colindex, store) {
			if ( (  record.data['IC_MONEDA']==1 &&  record.data['MENSAJE']=='SobreGirado') 
				|| (record.data['IC_MONEDA']==54 &&  record.data['MENSAJE_DL']=='SobreGirado' ) )  {							
				return '<div>&#160;</div>';			
         }else  {
				return '<div class="x-grid3-row-checker">&#160;</div>';
			}
				
		}
	});

	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
			{							
				header : 'Epo relacionada',
				tooltip: 'Epo relacionada',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{							
				header : 'No. Cliente Sirac',
				tooltip: 'No. Cliente Sirac',
				dataIndex : 'NU_SIRAC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{							
				header : 'Proveedor/Cedente',
				tooltip: 'Proveedor/Cedente',
				dataIndex : 'NOMBRE_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUMER0_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{							
				header : 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{							
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				//renderer:pctChange
				//renderer: 'usMoney'
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">'+ Ext.util.Format.number(value,'$0,0.00')+ '</span>';
					}else {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}	
				
			},	
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORCE_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">'+ Ext.util.Format.number(value,'0.00%')+ '</span>';
					}else {
						return Ext.util.Format.number(value,'0.00%');
					}
				}
			},	
			{							
				header : 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex : 'RECURSO_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">'+ Ext.util.Format.number(value,'$0,0.00')+'</span>';
					}else {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			},	
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">'+ Ext.util.Format.number(value,'$0,0.00')+ '</span>';
					}else {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			},	
			{							
				header : 'Monto Inter�s',
				tooltip: 'Monto Inter�s',
				dataIndex : 'MONTO_INTERES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">'+ Ext.util.Format.number(value,'$0,0.00')+ '</span>';
					}else {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			},	
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">'+ Ext.util.Format.number(value,'$0,0.00')+ '</span>';
					}else {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			},	
			{							
				header : 'Tasa',
				tooltip: 'Tasa',
				dataIndex : 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">'+ Ext.util.Format.number(value,'0.00%')+ '</span>';
					}else {
						return Ext.util.Format.number(value,'0.00%');
					}
				}
			},	
			{							
				header : 'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}				
			},				
			{							
				header : 'No. Proveedor',
				tooltip: 'No. Proveedor',
				dataIndex : 'NUM_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}	
			},	
			{							
				header : 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex : 'BENEFICIARIO',
				sortable: true,				
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}	
			},	
			{							
				header : 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex : 'BANCO_BENEFICIARIO',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}	
			},	
			{							
				header : 'Sucursal Beneficiaria',
				tooltip: 'Sucursal Beneficiaria',
				dataIndex : 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}	
			},	
			{							
				header : 'Cuenta Beneficiaria',
				tooltip: 'Cuenta Beneficiaria',
				dataIndex : 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}	
			},	
			{							
				header : '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{							
				header : 'Importe a Recibir Beneficiario',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex : 'IMPORTE_RECIBIR_BENE',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Neto a Recibir PyME',
				tooltip: 'Neto a Recibir PyME',
				dataIndex : 'NETO_RECIBIR',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Observaci�n',
				tooltip: 'Observaci�n',
				dataIndex : 'OBSERVACION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',
				renderer:function(value,metadata,registro){  
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}				
			},			
			selectModel
		],
		columnLines : true,
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		margins: '20 0 0 0',
		loadMask: true,		
		stripeRows: true,
		height: 400,
		width: 900,		
		title: 'Autorizaci�n de Descuento',
		frame: false,
		sm:selectModel,
		listeners: {
			viewReady: selectDefaultDoctos
		}
	});
	
			
	var procesarValidaFondeo =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {		
			horarioValido = true;
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			var fondeoP = Ext.getCmp('fondeoP');
			var doctoAplicado = Ext.getCmp('doctoAplicado');
			var forma = Ext.getCmp('forma');
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');			
			var gridConsulta = Ext.getCmp('gridConsulta');
			var fpBotones = Ext.getCmp('fpBotones');
			
			var mensajehora = Ext.getCmp('mensajehora');
			var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');  
			
			if(jsonObj.vhoraEnvOpeIf!='' || jsonObj.mensajeHorario!=''){	
				gridConsulta.hide();				
				fpBotones.hide();
				
				var mensaje;
				if(jsonObj.vhoraEnvOpeIf!=''){
					mensaje  = '<table width="600" align="center"><tr><td width="600">El horario de operaci�n es a partir de las '+jsonObj.vhoraEnvOpeIf+' hrs.</td></tr></table>';
				}
				if(jsonObj.mensajeHorario!=''){
					mensaje  = '<table width="600" align="center"><tr><td align="center" width="600"><font color="red">'+jsonObj.mensajeHorario+'</font></td></tr></table>';
					horarioValido = false;
				}
				
				Ext.getCmp("mensaje").setValue(mensaje);
					
				mensajehora.show();
				//gridConsultaTotales.hide();
				
			}else {
				mensajehora.hide();
				
				fp.el.mask('Enviando...', 'x-mask-loading');					
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						operacion: 'Generar'							
					})
				});										
			}
				
			Ext.getCmp("mensajeFondeo1").setValue(jsonObj.mensajeFondeo);
				
				if(jsonObj.mensajeFondeo=='S'){	
					fondeoP.show();
				} 	else {
					fondeoP.hide();
				}
				if(jsonObj.mensajeDoctoAplicado=='S'){	
					doctoAplicado.show();
				} 	else {
					doctoAplicado.hide();
				}								
					
				
			//}
									
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}	
	
	var mensajehora = new Ext.Panel({
		layout: 'table',		
		id: 'mensajehora',							
		width:	'600',
		heigth:	'auto',
		frame:true,
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensaje', value: '', style: 'margin:0 auto;' }							
		]
	});
				
	var mensajeSobreGiro = new Ext.Panel({
		layout: 'table',		
		id: 'mensajeSobreGiro',							
		width:	'600',
		heigth:	'auto',
		frame:false,
		hidden: true,
		style: 'margin:0 auto;',
			items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeGiro', value: '', style: 'margin:0 auto;' }							
		]
	});
			
	var storeCatalogoEPO = new Ext.data.JsonStore({
		id: 'storeCatalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeCatalogoPYME = new Ext.data.JsonStore({
		id: 'storeCatalogoPYME',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
 var procesarCatalogoBancoFondeo= function(store, records, oprion){
   if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
    var ic_banco_fondeo1 = Ext.getCmp('ic_banco_fondeo1');
	 if(ic_banco_fondeo1.getValue()==''){
	  ic_banco_fondeo1.setValue(records[0].data['clave']);
	  
	  	var cmbEPO = Ext.getCmp('ic_epo1');
		 cmbEPO.store.load({
			params: {
				ic_banco_fondeo:records[0].data['clave']
				}
		});
								
	  
	 }
   }
  }
  
	
	var storeCatalogoBancoFondeo = new Ext.data.JsonStore({
		id: 'storeCatalogoBancoFondeo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBancoFondeo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoBancoFondeo,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

	var elementosForma = [	
	
		{ 	xtype: 'textfield',   id: 'numRegistros', hidden: true, value: '' }, 	
		{ 	xtype: 'textfield',   id: 'claveDoctoTodos', hidden: true, value: '' }, 	
		
		{
			xtype: 'textfield',
			name: 'mensajeFondeo',
			id: 'mensajeFondeo1',
			fieldLabel: 'mensajeFondeo',
			allowBlank: true,
			startDay: 0,
			maxLength: 50,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			hidden: true,
			msgTarget: 'side',						
			margins: '0 20 0 0'			
		},
		{
			xtype: 'textfield',
			name: 'mensaje',
			id: 'mensaje1',
			fieldLabel: 'mensaje',
			allowBlank: true,
			startDay: 0,
			maxLength: 50,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			hidden: true,
			msgTarget: 'side',						
			margins: '0 20 0 0'			
		},
		{
			xtype: 'textfield',
			name: 'nombreEPO',
			id: 'nombreEPO1',
			fieldLabel: 'nombreEPO',
			hidden: true,
			allowBlank: true,
			startDay: 0,
			maxLength: 300,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0'			
		},
		{
			xtype: 'textfield',
			name: 'montoDi',
			id: 'montoDi1',
			fieldLabel: 'montoDi',
			hidden: true,
			allowBlank: true,
			startDay: 0,
			maxLength: 30,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0'			
		},		
		{
			xtype: 'textfield',
			name: 'hayClaseDocto',
			id: 'hayClaseDocto1',
			fieldLabel: 'hayClaseDocto',
			hidden: true,
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0'			
		}	, 
	{ 
			xtype:   'label',
			id:	'doctoAplicado',
			hidden: true,
			html:		'<span style="color:black;"><b>Existe informaci�n en la pantalla "Documentos aplicados a cr�ditos de anticipos</b></span>',  
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},		
		{ 
			xtype:   'label',
			id:	'fondeoP',
			hidden: true,
			html:		'<span style="color:red;"><b> Las siguientes solicitudes de descuento se realizar�n con Fondeo propio<b></span>',  
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},			
		{
			xtype: 'compositefield',
			fieldLabel: 'Banco de Fondeo',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [		
				{
					xtype: 'combo',
					name: 'ic_banco_fondeo',
					id: 'ic_banco_fondeo1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccione...',					
					valueField: 'clave',
					hiddenName : 'ic_banco_fondeo',
					fieldLabel: 'Banco de Fondeo:',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: storeCatalogoBancoFondeo,				
					listeners: {
						select: {
							fn: function(combo) {
								var cmbEPO = Ext.getCmp('ic_epo1');
								cmbEPO.setValue('');
								//cmbEPO.setDisabled(false);
								cmbEPO.store.load({
									params: {
										ic_banco_fondeo:combo.getValue()
									}
								});
							}
						}
					}			
				}
			]
		},
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione  EPO...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO Relacionada',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeCatalogoEPO,
			listeners: {
				select: {
					fn: function(combo) {					
																						
						Ext.Ajax.request({
							url: '13forma03Ext.data.jsp',
							params: {
								informacion: 'ValidaFondeo',
								ic_epo:combo.getValue()			
							},
							callback: procesarValidaFondeo
						});	
						
						var cmbPYME = Ext.getCmp('ic_pyme1');
						cmbPYME.setValue('');
						cmbPYME.store.load({
							params: {
								ic_epo:combo.getValue()
							}
						});	
					
					}
				}
			}
		},
		{
			xtype: 'combo',
			name: 'ic_pyme',
			id: 'ic_pyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione PYME...',			
			valueField: 'clave',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Nombre del Proveedor',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeCatalogoPYME,
			listeners: {
				select: {
					fn: function(combo) {
					
					fp.el.mask('Enviando...', 'x-mask-loading');	
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar'							
						})
					});
					
					}
				}
			}
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'montoDi_dl', 	value: '' }	,		
		{ 	xtype: 'textfield',  hidden: true, id: 'mensaje_dl', 	value: '' }			
		
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Autorizaci�n de Descuento',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma, 
		monitorValid: true,
		collapsible: false		
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),			
			fp,	
			NE.util.getEspaciador(10),
			mensajehora,
			NE.util.getEspaciador(10),
			gridConsulta,
			NE.util.getEspaciador(20),
			gridConsultaTotales,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),
			mensajeSobreGiro,
			NE.util.getEspaciador(20)
			
		]
	});
	
	
	storeCatalogoBancoFondeo.load();

			
});