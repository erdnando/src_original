<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.sql.*,
		java.util.*,
		netropology.utilerias.*,
		com.netro.exception.*,
		com.netro.pdf.*,
		net.sf.json.JSONObject, 
		com.netro.descuento.*"
errorPage="/00utils/error_extjs.jsp"%>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
 
<%! public static final boolean SIN_COMAS = false;%>
<%	//DECLARACION DE VARIABLES

String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String monto_descuento_mn = (request.getParameter("monto_descuento_mn")!=null)?request.getParameter("monto_descuento_mn"):"";
String monto_interes_mn = (request.getParameter("monto_interes_mn")!=null)?request.getParameter("monto_interes_mn"):"";
String monto_operar_mn = (request.getParameter("monto_operar_mn")!=null)?request.getParameter("monto_operar_mn"):"";
String monto_descuento_dl = (request.getParameter("monto_descuento_dl")!=null)?request.getParameter("monto_descuento_dl"):"";
String monto_interes_dl = (request.getParameter("monto_interes_dl")!=null)?request.getParameter("monto_interes_dl"):"";
String monto_operar_dl = (request.getParameter("monto_operar_dl")!=null)?request.getParameter("monto_operar_dl"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")!=null)?request.getParameter("ic_banco_fondeo"):"";
String acuse = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
String hidFechaCarga = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):"";
String hidHoraCarga = (request.getParameter("hora")!=null)?request.getParameter("hora"):"";
String  hidRecibo =  (request.getParameter("recibo")!=null)?request.getParameter("recibo"):"";
String  mensajeFondeo =  (request.getParameter("mensajeFondeo")!=null)?request.getParameter("mensajeFondeo"):"";
String  accion =  (request.getParameter("accion")!=null)?request.getParameter("accion"):"";

Hashtable alParamEPO = new Hashtable (); 
Vector registros;
int numRegistros = 0, numRegistrosMN = 0,  numRegistrosDL = 0, numCamposAdicionales = 0;
String [] campoAdicional;
String numSirac ="", nombrePyme="", folio="", numDocumento="", fechaDocumento ="", fechaVencimiento ="", claveMoneda="", 
			nombreMoneda="",  monto ="", porcentajeAnticipo="", recursoGarantia ="", importeInteres ="",  montoDescuento ="", 
			importeRecibir ="", tasaAceptada ="",  plazo ="", numProveedorInterno ="", descuentoEspecial ="", tipoFactoraje = "",
			beneficiario = "", bancoBeneficiario = "", sucursalBeneficiario = "",  cuentaBeneficiario = "", porcBeneficiario = "",
			importeRecibirBenef = "", netoRecibirPyme = "", nombreEpo ="" , cc_acuse="" ,	claveOficina ="" , clavePyme ="" ,
			claveIf  ="" , nombreIf ="" , fechaAlta ="" , fechaSolicitud ="" , tasaIf ="" , referencia ="",referenciaFide="" ;

double dblRegistros = 0, dblTotalMontoPesos = 0, dblTotalDescuentoPesos = 0, dblTotalOperarPesos = 0,dblTotalRecursosPesos = 0, dblTotalInteresPesos = 0,
		dblTotalMontoDolares = 0,  dblTotalDescuentoDolares = 0, dblTotalOperarDolares = 0, dblTotalRecursosDolares = 0,  dblTotalInteresDolares = 0;
			
boolean bOperaFactorajeVencido = false, bOperaFactorajeDist = false, bOperaFactorajeMand = false, bOperaFactorajeVencInfonavit = false,  bTipoFactoraje = false;

JSONObject jsonObj = new JSONObject();


//_________________________ INICIO DEL PROGRAMA _________________________

	AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

CrearArchivosAcuses  generaAch =  new CrearArchivosAcuses();
generaAch.setAcuse(acuse);
generaAch.setTipoArchivo("PDF");	
generaAch.setUsuario(iNoUsuario);
generaAch.setVersion("PANTALLA");	
generaAch.setStrDirectorioTemp(strDirectorioTemp);

try { 

	int existeArch= 	generaAch.existerArcAcuse();
	
	alParamEPO = BeanParamDscto.getParametrosEPO(ic_epo, 1);
	bOperaFactorajeVencido = ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?true:false;
	bOperaFactorajeDist = ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?true:false;
	bOperaFactorajeMand = ("S".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?true:false;
	bOperaFactorajeVencInfonavit = ("S".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?true:false;
	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDist || bOperaFactorajeMand || bOperaFactorajeVencInfonavit)?true:false;
	//obtenemos el nombre de la EPO
	nombreEpo  = BeanAutDescuento.descripcionEPO(ic_epo);
	
	numCamposAdicionales = BeanAutDescuento.getNumCamposAdicionales(ic_epo);

	campoAdicional = BeanAutDescuento.getCamposAdicionales(ic_epo);
		
	registros = BeanAutDescuento.getDoctosProcesados(acuse, ic_epo, iNoCliente);
	numRegistros = registros.size();
	int totalCamposAdic = 5 - numCamposAdicionales;
	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = archivo.nombreArchivo()+".pdf";
		
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
	 pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	                         session.getAttribute("iNoNafinElectronico").toString(),
			    					 (String)session.getAttribute("sesExterno"),
	                         (String) session.getAttribute("strNombre"),
                            (String) session.getAttribute("strNombreUsuario"),
						    		 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	
		if(informacion.equals("ArchivoPDF") && existeArch==0 ) {
		pdfDoc.addText("  ","formasB",ComunesPDF.CENTER);
		pdfDoc.addText(" ","formasB",ComunesPDF.CENTER);
	
		pdfDoc.addText("La autentificación se llevó a cabo con éxito","formasB",ComunesPDF.CENTER);
		pdfDoc.addText("Recibo: "+hidRecibo,"formasB",ComunesPDF.CENTER);
		pdfDoc.setTable(7,65);
		pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("Moneda Nacional","celda01",ComunesPDF.CENTER,3);
		pdfDoc.setCell("Dólares","celda01",ComunesPDF.CENTER,3);
		pdfDoc.setCell("Total Monto Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Total Monto Interés","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Total Monto Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Total Monto Interés","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(ic_epo+" "+nombreEpo,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_descuento_mn,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_interes_mn,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_operar_mn,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_descuento_dl,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_interes_dl,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto_operar_dl,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.addTable();

		pdfDoc.setTable(2,65);
		pdfDoc.setCell("Cifras de Control","celda01",ComunesPDF.CENTER,2);
		pdfDoc.setCell("No. de acuse","formas");
		pdfDoc.setCell(acuse,"formas");
		pdfDoc.setCell("Fecha de carga","formas");
		pdfDoc.setCell(hidFechaCarga,"formas");
		pdfDoc.setCell("Hora de carga","formas");
		pdfDoc.setCell(hidHoraCarga,"formas");
		pdfDoc.setCell("Usuario","formas");
		pdfDoc.setCell(iNoUsuario+" - "+strNombreUsuario,"formas");
		String mensaje = "";
		if("1".equals(ic_banco_fondeo)){
			mensaje = mensaje_param.getMensaje("I13forma3.NAFIN.Leyenda04", sesIdiomaUsuario);
		}
		if("2".equals(ic_banco_fondeo)){
			mensaje = mensaje_param.getMensaje("I13forma3.BANCOMEXT.Leyenda07", sesIdiomaUsuario);
		}
		pdfDoc.setCell(mensaje,"celda02",ComunesPDF.JUSTIFIED,2);
		pdfDoc.addTable();
		int cols = 13;
		int colsB = 0;
		if(bTipoFactoraje)
			cols++;
		pdfDoc.setTable(cols);
		pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("No. Cliente SIRAC","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Proveedor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("No. Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Emisión","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		if(bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
			pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Recurso en Garantía","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto Interés","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a  Operar","celda01",ComunesPDF.CENTER);
		colsB = 5;
		pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("No. Solicitud","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("No. Proveedor","celda01",ComunesPDF.CENTER);
		if(bOperaFactorajeDist || bOperaFactorajeVencInfonavit){//FODEA 042 - 2009 ACF
			colsB+=7;
			pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta Beneficiaria","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Neto Recibir Pyme","celda01",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
		colsB++;
		pdfDoc.setCell("","celda01",ComunesPDF.CENTER,cols-colsB);
	
		for (int i = 0; i < numRegistros; i++) {
			Vector registro = (Vector) registros.get(i);
				
			numSirac = registro.get(0).toString();
			nombrePyme = registro.get(1).toString();
			numDocumento = registro.get(2).toString();
			fechaDocumento = registro.get(3).toString();
			fechaVencimiento = registro.get(4).toString();
			claveMoneda = registro.get(5).toString();
			nombreMoneda = registro.get(6).toString();
			monto = registro.get(7).toString();
			montoDescuento = registro.get(8).toString();
			porcentajeAnticipo = registro.get(9).toString();
			recursoGarantia = registro.get(10).toString();
			importeInteres = registro.get(11).toString();
			importeRecibir = registro.get(12).toString();
			tasaAceptada = registro.get(13).toString();
			plazo = registro.get(14).toString();
			numProveedorInterno = registro.get(15).toString();
			descuentoEspecial = registro.get(16).toString();
			folio = registro.get(17).toString();		
			beneficiario = registro.get(29).toString();
			bancoBeneficiario = registro.get(30).toString();
			sucursalBeneficiario = registro.get(31).toString();
			cuentaBeneficiario = registro.get(32).toString();
			porcBeneficiario = registro.get(33).toString();
			importeRecibirBenef = registro.get(34).toString();
			netoRecibirPyme = registro.get(35).toString();
			tipoFactoraje = registro.get(36).toString();
			referenciaFide = registro.get(37).toString();
			if (claveMoneda.equals("1")) {
				numRegistrosMN++;
			} else if (claveMoneda.equals("54")) {
				numRegistrosDL++;
			}
			String clase = "formas";
			if(i%5==4)
				clase = "celda02";
			else
				clase = "formas";
			pdfDoc.setCell("A",clase,ComunesPDF.CENTER);
			pdfDoc.setCell(numSirac,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(numDocumento,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(fechaDocumento,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(nombreMoneda,clase,ComunesPDF.CENTER);
			if(bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
				pdfDoc.setCell(tipoFactoraje,clase,ComunesPDF.CENTER);
			}
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(monto,2),clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(porcentajeAnticipo,0)+" %",clase,ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(recursoGarantia,2),clase,ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDescuento,2),clase,ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(importeInteres,2),clase,ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(importeRecibir,2),clase,ComunesPDF.CENTER);
			pdfDoc.setCell("B",clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(tasaAceptada,5)+" %",clase,ComunesPDF.CENTER);
			pdfDoc.setCell(plazo,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(folio.substring(0,10)+"-"+folio.substring(10,11),clase,ComunesPDF.CENTER);
			pdfDoc.setCell(numProveedorInterno,clase,ComunesPDF.CENTER);
			if(bOperaFactorajeDist || bOperaFactorajeVencInfonavit) {//FODEA 042 - 2009 ACF
				pdfDoc.setCell(beneficiario,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(bancoBeneficiario,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(sucursalBeneficiario,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(cuentaBeneficiario,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(porcBeneficiario,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoMoneda2(importeRecibirBenef,true),clase,ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoMoneda2(netoRecibirPyme,true),clase,ComunesPDF.CENTER);
			}
			pdfDoc.setCell("",clase,ComunesPDF.CENTER);
			
			pdfDoc.setCell("",clase,ComunesPDF.CENTER,cols-colsB);
			} //fin del for
	
			if (numRegistrosMN!=0) {
				pdfDoc.setCell("Número de documentos M.N.","formasB",ComunesPDF.CENTER,3);
				pdfDoc.setCell(new Integer(numRegistrosMN).toString(),"formasB",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,cols-5);
			}
			if (numRegistrosDL!=0) {
				pdfDoc.setCell("Número de documentos Dólares","formasB",ComunesPDF.CENTER,3);
				pdfDoc.setCell(new Integer(numRegistrosDL).toString(),"formasB",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,cols-5);
			}
			pdfDoc.addTable();
			
		
			
	}else if(informacion.equals("ArchivoInterfaz")) {
	
		if(mensajeFondeo.equals("S")) {
			pdfDoc.addText("Las siguientes solicitudes de descuento se realizarán con Fondeo propio","formas",ComunesPDF.CENTER);
		}

		pdfDoc.addText("NACIONAL FINANCIERA, S.N.C","formas",ComunesPDF.CENTER);
		
		pdfDoc.setTable(12, 100);
		pdfDoc.setCell("Fecha Generación :","celda01",ComunesPDF.LEFT,2);
		pdfDoc.setCell(fechaActual,"formas",ComunesPDF.LEFT,10);
		pdfDoc.setCell("Reporte: ","celda01",ComunesPDF.LEFT,2);
		pdfDoc.setCell("Documentos Autorizados","formas",ComunesPDF.LEFT,10);
		
		pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Folio","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Acuse Asignado","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Clave de Oficina","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Clave de PYMEC","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Número Cliente SIRAC","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Clave de EPO","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre de EPO","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Clave IF","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
			
		pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		if(bTipoFactoraje) {
			pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Porcentaje Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Recurso Garantia","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto de Interes ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto Operar ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tasa de Interes PYME","celda01",ComunesPDF.CENTER);
		if(!bTipoFactoraje) {
			pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("C","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Alta","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Solicitud","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("TipoSolicitud","celda01",ComunesPDF.CENTER);
		if(numCamposAdicionales>0){
			for (int i = 0; i<numCamposAdicionales; i++) {
				pdfDoc.setCell(campoAdicional[i], "celda01",ComunesPDF.CENTER);
			}
		}
		if(totalCamposAdic>0){
			pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, totalCamposAdic);
		}
		if(bOperaFactorajeDist || bOperaFactorajeVencInfonavit) { 
			pdfDoc.setCell("D","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Importe a Recubir del  Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Neto a Recibir Pyme","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("","celda01",ComunesPDF.CENTER,4);
		
		}

	

		for (int i = 0; i < numRegistros; i++) {
		
			Vector registro = (Vector) registros.get(i);
			numSirac = registro.get(0).toString();
			nombrePyme = registro.get(1).toString();
			numDocumento = registro.get(2).toString();
			fechaDocumento = registro.get(3).toString();
			fechaVencimiento = registro.get(4).toString();
			claveMoneda = registro.get(5).toString();
			nombreMoneda  = registro.get(6).toString();
			monto = registro.get(7).toString();
			montoDescuento = registro.get(8).toString();
			porcentajeAnticipo = registro.get(9).toString();
			recursoGarantia = registro.get(10).toString();
			importeInteres = registro.get(11).toString();
			importeRecibir = registro.get(12).toString();
			tasaAceptada = registro.get(13).toString();
			plazo = registro.get(14).toString();
			numProveedorInterno = registro.get(15).toString();
			descuentoEspecial = registro.get(16).toString();
			folio = registro.get(17).toString();
			cc_acuse = registro.get(18).toString();
			claveOficina = registro.get(19).toString();
			clavePyme = registro.get(20).toString();
			nombreEpo = registro.get(21).toString();
			claveIf = registro.get(22).toString();
			nombreIf = registro.get(23).toString();
			fechaAlta = registro.get(24).toString();
			fechaSolicitud = registro.get(25).toString();
			tasaIf = registro.get(26).toString();
			referencia = registro.get(27).toString();
			beneficiario = registro.get(29).toString();
			bancoBeneficiario = registro.get(30).toString();
			sucursalBeneficiario = registro.get(31).toString();
			cuentaBeneficiario = registro.get(32).toString();
			porcBeneficiario = registro.get(33).toString();
			importeRecibirBenef = registro.get(34).toString();
			netoRecibirPyme = registro.get(35).toString();
	      tipoFactoraje = registro.get(36).toString();
		
			if (claveMoneda.equals("1")) {		//M.N.
				numRegistrosMN++;
				dblTotalMontoPesos += new Double(monto).doubleValue();
				dblTotalDescuentoPesos += new Double(montoDescuento).doubleValue();
				dblTotalOperarPesos += new Double(importeRecibir).doubleValue();

			   dblTotalRecursosPesos += new Double(recursoGarantia).doubleValue();
			   dblTotalInteresPesos += new Double(importeInteres).doubleValue();
			} else if (claveMoneda.equals("54")) {	//DLS
				numRegistrosDL++;
				dblTotalMontoDolares += new Double(monto).doubleValue();
				dblTotalDescuentoDolares += new Double(montoDescuento).doubleValue();
				dblTotalOperarDolares += new Double(importeRecibir).doubleValue();

			   dblTotalRecursosDolares += new Double(recursoGarantia).doubleValue();
			   dblTotalInteresDolares += new Double(importeInteres).doubleValue();
			}
			
			pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(folio,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numDocumento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(cc_acuse,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(claveOficina,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numProveedorInterno,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numSirac,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(ic_epo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(claveIf,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombreIf,"formas",ComunesPDF.LEFT);
			
		
			pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaDocumento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombreMoneda,"formas",ComunesPDF.CENTER);
			if(bTipoFactoraje) {
				pdfDoc.setCell(tipoFactoraje,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(porcentajeAnticipo,0)+"%","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(recursoGarantia,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(importeInteres,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(importeRecibir,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(tasaAceptada,5),"formas",ComunesPDF.CENTER);
			if(!bTipoFactoraje) {
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);
			}
			
			
			pdfDoc.setCell("C","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("Operado","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaAlta,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaSolicitud,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(referencia,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell("Interna","formas",ComunesPDF.CENTER);
					
			if(numCamposAdicionales>0){
				for (int j = 1; j<=numCamposAdicionales; j++) {
					pdfDoc.setCell(registro.get(36 + j).toString(), "formas",ComunesPDF.CENTER);
				}
			}
			if(totalCamposAdic>0){
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, totalCamposAdic);
			}
				
			if(bOperaFactorajeDist || bOperaFactorajeVencInfonavit) { 				
				pdfDoc.setCell("D","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(beneficiario,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(bancoBeneficiario,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(sucursalBeneficiario,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cuentaBeneficiario,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2)+"%","formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(importeRecibirBenef,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(netoRecibirPyme,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,4);
			}
			
		}//for 
		
		pdfDoc.setCell("Total Registros MN: ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoDecimal(numRegistrosMN,0),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Documentos MN : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalMontoPesos,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Recurso Garantia MN : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalRecursosPesos,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Descuento MN : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalDescuentoPesos,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Interes MN : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalInteresPesos,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Operar MN  : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalOperarPesos,2),"formas",ComunesPDF.RIGHT);
		
		pdfDoc.setCell("Total Registros DL  : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(numRegistrosDL,0),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Documentos DL  : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalMontoDolares,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Recurso Garantia DL  : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalRecursosDolares,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Descuento DL : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalDescuentoDolares,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Interes DL  : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalInteresDolares,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Operar DL  : ","celda01",ComunesPDF.RIGHT);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalOperarDolares,2),"formas",ComunesPDF.RIGHT);
	
		
		pdfDoc.addTable();
		
	}

	pdfDoc.endDocument();

	if(informacion.equals("ArchivoPDF") && existeArch==0 ) {
	
		generaAch.setRutaArchivo(strDirectorioTemp+nombreArchivo);
		generaAch.guardarArchivo() ; //Guardar	
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("accion",accion);
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
	}else if(informacion.equals("ArchivoPDF") && existeArch>0 ) {
	
		
		nombreArchivo = generaAch.desArchAcuse() ; //Descargar
	
		jsonObj.put("success", new Boolean(true));		
		jsonObj.put("accion",accion);
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

	}else  {	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("accion",accion);
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
	
} catch (Exception exception) {
		
		java.io.StringWriter outSW = new java.io.StringWriter();
		exception.printStackTrace(new java.io.PrintWriter(outSW));
		String stackTrace = outSW.toString();
			
		generaAch.setDesError(stackTrace);	
		
		generaAch.guardaBitacoraArch();	
	
	throw new NafinException("SIST0001");
	
} 

%>
<%=jsonObj%>