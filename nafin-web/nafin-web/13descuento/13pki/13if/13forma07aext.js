Ext.onReady(function() {
 
 var ic_epo  = Ext.getDom('ic_epo').value;
 var ct_cambio_motivo  = Ext.getDom('ct_cambio_motivo').value;
 var doctosSeleccionados  = Ext.getDom('doctosSeleccionados').value;
 var estatusDoctos  = Ext.getDom('estatusDoctos').value;
 var doctosSeleccionados2  = [];
 var estatusDoctos2  = [];
 
  
   // # Formulario Env�o por POST  --> Se agreg�: 01/11/2012 
   var formaDinamica = new Ext.form.FormPanel({
      xtype:            'form',
      id:               'formaDinamica',
      url:              '13forma07ext.jsp',
      standardSubmit:   true,
      frame:            false,
      border:           false,
      style:            'border:#fff 0px;',
      items:            [{ xtype: 'hidden', name:'pantalla', value:'Acuse' }]
   }); // #
 
	var confirmar = function(pkcs7, textoFirmar, formaDinamicaVar){
	
	if (Ext.isEmpty(pkcs7)) {
		return;	//Error en la firma. Termina...
	}else{
		formaDinamicaVar.add({ xtype:'hidden', name:'ct_cambio_motivo', value:ct_cambio_motivo });
		formaDinamicaVar.add({ xtype:'hidden', name:'ic_epo', value:ic_epo });
		formaDinamicaVar.add({ xtype:'hidden', name:'textoFirmar', value:textoFirmar });
		formaDinamicaVar.add({ xtype:'hidden', name:'pkcs7', value:pkcs7 });
		formaDinamicaVar.doLayout();
		formaDinamicaVar.getForm().submit(); // --> 
	}
	
	}
		
	var  ProcesarAceptar = function() {
	
		var datos ='';
		var totales='';
		doctosSeleccionados  = [];
		estatusDoctos  = [];
	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
      var formaDinamicaVar = Ext.getCmp('formaDinamica'); // --> Se agreg�: 01/11/2012
		
		datos = 'Nombre EPO|Nombre PyME|Numero de Documento|Fecha de Vencimiento|Estatus Anterior|Moneda|Monto|Porcentaje Anticipo|Recurso Garantia|Monto a Descontar|Monto Interes|Monto a operar|Nuevo Estatus \n ';
      
		store.each(function(record) {
         estatusDoctos2.push(record.data['ESTATUS_ASIGNAR']);
         doctosSeleccionados2.push(record.data['IC_DOCUMENTO']);
				
         datos += record.data['NOMBRE_EPO'] +"|"+ record.data['NOMBRE_PYME'] +"|"+ record.data['NUM_DOCTO'] +"|"+ record.data['F_VENCIMIENTO'] +"|"+ 
                   record.data['ESTATUS_ANTERIOR'] +"|"+ record.data['MONEDA'] +"|"+ record.data['MONTO'] +"|"+ record.data['POR_DESC'] +"|"+ 
                   record.data['POR_GARANTIA'] +"|"+ record.data['MONTO_DESC'] +"|"+ record.data['MONTO_INTERES'] +"|"+ record.data['MONTO_OPERAR'] +"|"+ 
                   record.data['NUEVO_ESTATUS']+'\n';
                   
         
         // # --> Se agreg�: 06/11/2012
         formaDinamicaVar.add({ xtype:'hidden', name:'estatusDoctos', value:record.data['ESTATUS_ASIGNAR'] });
         formaDinamicaVar.add({ xtype:'hidden', name:'doctosSeleccionados', value:record.data['IC_DOCUMENTO'] });
         // #
				
		});
		
			
		var gridTotales = Ext.getCmp('gridTotales');
		var store2 = gridTotales.getStore();
				
		store2.each(function(record) {
				if(record.data['IC_MONEDA']==1  && record.data['TOTAL_DOCTOS'] !=0 ) {
					totales+= 'Monto Total MN: '+record.data['TOTAL_MONTO']+'\n'+
							 'Recurso en Garantia Total MN: '+record.data['TOTAL_RECURSOS']+'\n'+
							 'Monto a Descontar Total MN: '+record.data['TOTA_MONTO_DESC']+'\n'+
							 'Monto Interes Total MN: '+record.data['TOTAL_MONTO_INTERES']+'\n'+
							 'Monto a Operar Total MN: '+record.data['TOTAL_MONTO_OPERAR']+'\n';
				}
				
				if(record.data['IC_MONEDA']==54   && record.data['TOTAL_DOCTOS'] !=0 ) {
					totales+= 'Monto Total D�lares: '+record.data['TOTAL_MONTO']+'\n'+
							 'Recurso en Garantia Total D�lares: '+record.data['TOTAL_RECURSOS']+'\n'+
							 'Monto a Descontar Total D�lares: '+record.data['TOTA_MONTO_DESC']+'\n'+
							 'Monto Interes Total D�lares: '+record.data['TOTAL_MONTO_INTERES']+'\n'+
							 'Monto a Operar Total D�lares: '+record.data['TOTAL_MONTO_OPERAR']+'\n';
				}			
		});
		
		var  textoFirmar =datos+totales;
		
		
		NE.util.obtenerPKCS7(confirmar, textoFirmar, formaDinamicaVar);
		
		
      
      
      
//		var parametros = "pantalla=Acuse"				
//								+"&doctosSeleccionados="+doctosSeleccionados2
//								+"&estatusDoctos="+estatusDoctos2
//								+"&ct_cambio_motivo="+ct_cambio_motivo
//								+"&ic_epo="+ic_epo
//								+"&pkcs7="+pkcs7										
//								+"&textoFirmar="+textoFirmar;								
								
		//document.location.href = "13forma07ext.jsp?"+parametros;		
	}
	
	var mensajeCambio = new Ext.Container({
		layout: 'table',		
		id: 'mensajeCambio',							
		width:	'500',		
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 2
		},
		items: [	
			{
				xtype: 'displayfield',
				id: 'ct_cambio_motivo1',
				value: '<b>Motivo de cambio de estatus:</b>',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}
			},			
			{
				xtype: 'displayfield',
				id:	'ct_cambio_motivo2',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}
			}	
		]
	});	
	
	if(ct_cambio_motivo !="") {
		Ext.getCmp("ct_cambio_motivo2").setValue(ct_cambio_motivo);
	}


	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',
		align: 'center',
		items: [		
		{
			xtype: 'button',
			text: 'Aceptar',
			tooltip:	'Aceptar',
			iconCls: 'icoAceptar',
			id: 'btnAceptar'
			,handler:ProcesarAceptar	
		},
		{
			xtype: 'button',
			text: 'Cancelar',			
			iconCls: 'icoLimpiar',
			id: 'btnCancelar',		 
			handler: function() {
				window.location = '13forma07ext.jsp';
			}
		}
	]
	});
	
//_--------------------------------- HANDLERS -------------------------------

	var procesarTotalesData = function(store, arrRegistros, opts) 	{
			
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}					
							
			//edito el titulo de la columna
			var cm = gridTotales.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
		
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
				
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}			
							
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;	
			
			if(jsonData.estatusEspecificado !==''){
				Ext.getCmp("mensajeEspecificado").setValue(jsonData.mensajeEspecificado);	
			}
				
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaTotalesPre'
		},
		hidden: true,
		fields: [	
			{name: 'IC_MONEDA'},
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCTOS'},		
			{name: 'TOTAL_MONTO'},	
			{name: 'TOTAL_RECURSOS'},	
			{name: 'TOTA_MONTO_DESC'},	
			{name: 'TOTAL_MONTO_INTERES'},	
			{name: 'TOTAL_MONTO_OPERAR'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}			
	});
	
	
	
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'',
		//hidden: true,		
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right'	
			},	
			{							
				header : 'Total Documentos',
				tooltip: 'Total Documentos',
				dataIndex : 'TOTAL_DOCTOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},		
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'TOTAL_MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Recursos en garantia',
				tooltip: 'Recursos en garantia ',
				dataIndex : 'TOTAL_RECURSOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'TOTA_MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},		
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'TOTAL_MONTO_INTERES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'TOTAL_MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 150,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarPre'
		},
		hidden: true,
		fields: [			
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_PYME'},	
			{name: 'NUM_DOCTO'},
			{name: 'F_VENCIMIENTO'},
			{name: 'ESTATUS_ASIGNAR'},		
			{name: 'ESTATUS_ANTERIOR'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'POR_DESC'},
			{name: 'POR_GARANTIA'},
			{name: 'MONTO_DESC'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'NUEVO_ESTATUS'},
			{name: 'REFERENCIA'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Cambio de Estatus',
		//hidden: true,
		columns: [
			{							
				header : 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Nombre PyME/Cedente',
				tooltip: 'Nombre PyME/Cedente',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Numero de Documento',
				tooltip: 'Numero de Documento',
				dataIndex : 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'F_VENCIMIENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Estatus Anterior',
				tooltip: 'Estatus Anterior',
				dataIndex : 'ESTATUS_ANTERIOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'POR_DESC',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{							
				header : 'Recurso en garantia',
				tooltip: 'Recurso en garantia',
				dataIndex : 'POR_GARANTIA',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESC',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'MONTO_INTERES',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{							
				header : 'Nuevo Estatus',
				tooltip: 'Nuevo Estatus',
				dataIndex : 'NUEVO_ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{
				header : '<center>Referencia<center>',
				tooltip: 'Referencia',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true,
				width: 200,					
				align: 'left'
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		margins: '20 0 0 0',
		loadMask: true,		
		stripeRows: true,
		height: 300,
		width: 900,				
		frame: false		
	});
	

	var mensajeEspecificado1 = new Ext.Container({
		layout: 'table',		
		id: 'mensajeEspecificado1',							
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		items: [		
			{ 
				xtype:   'displayfield',
				id:	'mensajeEspecificado',				
				value:		'',  
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}	
			}
		]
	});
	


	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [			
			NE.util.getEspaciador(20),
			mensajeEspecificado1,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20),
			mensajeCambio,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),
         formaDinamica
		]
	});

 
 
	consultaData.load({
		params: {
			informacion: 'ConsultarPre',
			ic_epo:ic_epo,
			ct_cambio_motivo:ct_cambio_motivo,
			doctosSeleccionados:doctosSeleccionados,
			estatusDoctos:estatusDoctos		
		}
	});
	
	consultaDataTotales.load({
		params: {
			informacion: 'ConsultaTotalesPre',
			ic_epo:ic_epo,
			ct_cambio_motivo:ct_cambio_motivo,
			doctosSeleccionados:doctosSeleccionados,
			estatusDoctos:estatusDoctos		
		}
	});
			
					
//-------------------------------- ----------------- -----------------------------------
	
	
});