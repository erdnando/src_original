Ext.onReady(function() {

	var doctoSeleccionados  = Ext.getDom('doctoSeleccionados').value;
	var ic_pyme  = Ext.getDom('ic_pyme').value;
	var ic_epo  = Ext.getDom('ic_epo').value;
	var ic_banco_fondeo  = Ext.getDom('ic_banco_fondeo').value;
	var mensajeFondeo = Ext.getDom('mensajeFondeo').value;
	var numeroDoctoMN = Ext.getDom('numeroDoctoMN').value;
	var numeroDoctoDL = Ext.getDom('numeroDoctoDL').value;
	var leyenda;	
	var seleccionados =[];

	var procesarGenerarInterfaz =  function(opts, success, response) {
		var btnArchivoInterfase = Ext.getCmp('btnArchivoInterfase');
		btnArchivoInterfase.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarInterfase = Ext.getCmp('btnBajarInterfase');
			btnBajarInterfase.show();
			btnBajarInterfase.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarInterfase.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoInterfase.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
// DescargaArchivo es un servlet  para descargar los archivos
	function procesarGenerarFijo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};	
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarVariable =  function(opts, success, response) {
		var btnArchivoVariable = Ext.getCmp('btnArchivoVariable');
		btnArchivoVariable.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarVariable = Ext.getCmp('btnBajarVariable');
			btnBajarVariable.show();
			btnBajarVariable.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarVariable.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoVariable.enable();
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			//NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnArchivoCSV = Ext.getCmp('btnArchivoCSV');
		btnArchivoCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//para desacargar el archivo PDF
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnArchivoPDF = Ext.getCmp('btnArchivoPDF');
		btnArchivoPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);	
			if(jsonData.accion=='button')  {				
				var btnBajarPDF = Ext.getCmp('btnBajarPDF');
				btnBajarPDF.show();
				btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarPDF.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
			}
		} else {
			btnArchivoPDF.enable();
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			//NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailurePDF =  function(opts, success, response) {
		var btnNotasGenerarPDF = Ext.getCmp('btnNotasGenerarPDF');
		btnNotasGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnNotasGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var  cerrarDetalle = function() {
		var ventana = Ext.getCmp('verNotasDocumentos');	
			if (ventana) {
				ventana.destroy();
			}		
		}


	//para mostrar los detalles de los documentos  Agregados 
	var  procesaVentana = function() {
		
	consultaDataNotasDocumentos.load({  	
		params: {  
			informacion: 'consultaDataNotasDocumentos'
		}  		
	});
	
		var ventana = Ext.getCmp('verNotasDocumentos');	
		if (ventana) {
			ventana.show();
		}else {	
			new Ext.Window({			
				width: 910,
				height: 300,
				autoDestroy:true,
				closable:false,
				resizable: false,
				autoScroll:true, 
				closeAction: 'hide',
				id: 'verNotasDocumentos',
				align: 'center',
				items: [					
					gridNotasDocumentos					
				],
				title: 'Ver Notas Documentos',
				bbar: {
					xtype: 'toolbar',
					buttons: [	
						'->',
						'-',
						{
							xtype: 'button',
							iconCls: 'icoAceptar',
							text: 'Cerrar',
							id: 'btnCerrar',
							handler: cerrarDetalle
						},
						{
							xtype: 'button',
							iconCls: 'icoPdf',
							text: 'Generar PDF',
							id: 'btnNotasGenerarPDF',
							handler: function(boton, evento) {
								boton.setIconClass('loading-indicator');
								Ext.Ajax.request({
									url: '13forma03Ext.data.jsp',
									params: {
										informacion:'ArchivoNotasDocumentos'										
									}			
									,callback: procesarSuccessFailurePDF
								});
							}					
							
						},
						'-',
						{
							xtype: 'button',
							text: 'Bajar Archivo',
							tooltip:	'Bajar Archivo',
							id: 'btnBajarPDF',
							hidden: true
						}
					]
				}
			}).show();
		}
	}
	
	var procesarConsultaDataNotasDocumentos = function(store, arrRegistros, opts) 	{
	
		var jsonData = store.reader.jsonData;			
		
		var gridNotasDocumentos = Ext.getCmp('gridNotasDocumentos');	
		var el = gridNotasDocumentos.getGridEl();
		
		if (arrRegistros != null) {
				
			if (!gridNotasDocumentos.isVisible()) {
				gridNotasDocumentos.show();
			}		
			
			var btnNotasGenerarPDF = Ext.getCmp('btnNotasGenerarPDF');	
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');	
			
			if(store.getTotalCount() > 0) {
				btnNotasGenerarPDF.enable();
				el.unmask();	
			} else {	
				btnNotasGenerarPDF.disable();
				btnBajarPDF.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var consultaDataNotasDocumentos = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'consultaDataNotasDocumentos'
		},
		hidden: true,
		fields: [
			{name: 'NUM_DOCTO'},	
			{name: 'FECHA_EMISION'},
			{name: 'MONEDA'},		
			{name: 'MONTO'},	
			{name: 'MONTO_APLICADO'},		
			{name: 'SALDO_DOCTO'},
			{name: 'TIPO_FACTORAJE'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaDataNotasDocumentos,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaDataNotasDocumentos(null, null, null);						
					}
				}
			}			
	});
	
	var gridNotasDocumentos = {		
		xtype: 'grid',	
		id: 'gridNotasDocumentos',				
		store: consultaDataNotasDocumentos,	
		style: 'margin:0 auto;',
		columns: [
			{							
				header : 'No. Docto.',
				tooltip: 'No. Docto.',
				dataIndex :'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex :'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex :'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Monto Docto',
				tooltip: 'Monto Docto',
				dataIndex :'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto Aplicado',
				tooltip: 'Monto Aplicado',
				dataIndex :'MONTO_APLICADO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Saldo Docto',
				tooltip: 'Saldo Docto',
				dataIndex :'SALDO_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex :'TIPO_FACTORAJE',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		margins: '20 0 0 0',
		loadMask: true,		
		stripeRows: true,
		height: 200,
		width: 900,		
		title: '',
		frame: false
	};
	
	var  ProcesarCancelar = function() {
	
		Ext.MessageBox.confirm('Confirmaci�n','�Esta usted seguro de cancelar la operaci�n?', function(resp){
			if(resp=='yes'){
				document.location.href = "13forma03Ext.jsp?";
			}
		});		
	}
	
	var procesarSuccessFailureAcuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);			
			if (jsonData != null){
			
				Ext.getCmp("autentificac").setValue(jsonData.mensajeAutentificacion);
				Ext.getCmp("mensajeAutentificacion").show();
				
				if(jsonData.recibo !='') {
					Ext.getCmp("acuse").setValue(jsonData.acuse);
					Ext.getCmp("fecha").setValue(jsonData.fecha);
					Ext.getCmp("hora").setValue(jsonData.hora);
					Ext.getCmp("usuario").setValue(jsonData.usuario);
					Ext.getCmp("recibo").setValue(jsonData.recibo);
				
					Ext.getCmp("strNombreArchivo").setValue(jsonData.strNombreArchivo);
					Ext.getCmp("nombreArchivoPDF").setValue(jsonData.nombreArchivoPDF);					
					Ext.getCmp("strNombreArchivoFijo").setValue(jsonData.strNombreArchivoFijo);
					Ext.getCmp("nombreArchivoCSV").setValue(jsonData.nombreArchivoCSV);
					Ext.getCmp("mensajesFondeoPre1").setValue(jsonData.mensajesFondeoPre);	
					Ext.getCmp("monto_descuento_mn").setValue(jsonData.monto_descuento_mn);	
					Ext.getCmp("monto_interes_mn").setValue(jsonData.monto_interes_mn);					
					Ext.getCmp("monto_operar_mn").setValue(jsonData.monto_operar_mn);	
					Ext.getCmp("monto_descuento_dl").setValue(jsonData.monto_descuento_dl);	
					Ext.getCmp("monto_interes_dl").setValue(jsonData.monto_interes_dl);	
					Ext.getCmp("monto_operar_dl").setValue(jsonData.monto_operar_dl);	
									
					var acuseCifras = [
						['N�mero de Acuse', jsonData.acuse2],
						['Fecha de Carga ', jsonData.fecha],
						['Hora de Carga', jsonData.hora],
						['Usuario de Captura ', jsonData.usuario]
					];
					storeCifrasData.loadData(acuseCifras);
					gridCifrasControl.show();
								
					Ext.getCmp("hayDoctosAgregados3").hide();
					Ext.getCmp("fondePropio").hide();
					Ext.getCmp("btnCancelar").hide();
					Ext.getCmp("constancia").hide();
					Ext.getCmp("gridConsulta2").hide();
					Ext.getCmp("gridTotales2").hide();
					Ext.getCmp("facultado").hide();
					Ext.getCmp("btnSolicitar").hide();					
					
					Ext.getCmp("btnArchivoVariable").show();
					Ext.getCmp("btnArchivoFijo").show();
					Ext.getCmp("btnArchivoInterfase").show();				
					Ext.getCmp("btnSalir").show();
					
					Ext.getCmp("fpBotones2").show();
					Ext.getCmp("btnArchivoPDF").show();
					Ext.getCmp("btnArchivoCSV").show();
					Ext.getCmp("btnSalir").show();			
				}else {
				
					Ext.getCmp("hayDoctosAgregados3").hide();
					Ext.getCmp("fondePropio").hide();
					Ext.getCmp("btnCancelar").hide();
					Ext.getCmp("constancia").hide();
					Ext.getCmp("gridConsulta2").hide();
					Ext.getCmp("gridTotales2").hide();
					Ext.getCmp("facultado").hide();
					Ext.getCmp("btnSolicitar").hide();	
					
					Ext.getCmp("gridConsultaTotales").hide();
					Ext.getCmp("gridCifrasControl").hide();
					Ext.getCmp("mensaje").hide();
					Ext.getCmp("fpBotones").hide();
					Ext.getCmp("gridConsulta").hide();
					Ext.getCmp("gridTotales1").hide();
					Ext.getCmp("constancia").hide();
					Ext.getCmp("fpBotones2").hide();
					Ext.getCmp("gridConsulta2").hide();
					Ext.getCmp("gridTotales2").hide();
					Ext.getCmp("facultado").hide();
				}
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 150,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 400,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 570,		
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});
	
	var mensajeAutentificacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutentificacion',							
		width:	'300',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		items: [					
			{
				xtype: 'displayfield',
				id:	'autentificac',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}		
			},			
			{ 	xtype: 'displayfield', 	hidden: true, id: 'acuse', 	value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'fecha', 	value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, id: 'hora', 	value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'usuario', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'strNombreArchivo', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'strNombreArchivoFijo', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'nombreArchivoCSV', value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, id: 'nombreArchivoPDF', value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, id: 'recibo', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'monto_descuento_mn', value: '' },			
			{ 	xtype: 'displayfield', 	hidden: true, id: 'monto_interes_mn', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'monto_operar_mn', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'monto_descuento_dl', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'monto_interes_dl', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'monto_operar_dl', value: '' }
					
		]
	});
	
		//Contenedor para los botones de imprimir y Salir 
	var fpBotones2 = new Ext.Container({
		layout: 'table',
		id: 'fpBotones2',			
		width:	'400',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		items: [	
			{
				xtype: 'button',
				text: 'Exportar Interfase Variable',		
				iconCls: 'icoXls',
				id: 'btnArchivoVariable',		 
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					
					var strNombreArchivo = Ext.getCmp("strNombreArchivo").getValue();
					var _acuse = Ext.getCmp("acuse").getValue();
					
					Ext.Ajax.request({
						url: '13forma03Ext.data.jsp',
						params: {
							informacion:'ArchivoVariable',
							strNombreArchivo:strNombreArchivo,
							_acuse:_acuse
						}					
						,callback: procesarGenerarVariable
					});
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				iconCls: 'icoXls',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarVariable',
				hidden: true
			},				
			{
				xtype: 'button',
				text: 'Exportar Interfase Fijo',		
				iconCls: 'icoTxt',
				id: 'btnArchivoFijo',		 
				handler: function(boton, evento) {					
									
					var strNombreArchivoFijo = Ext.getCmp("strNombreArchivoFijo").getValue();					
					Ext.Ajax.request({
						url: '13forma03Ext.data.jsp',
						params: {
							informacion:'ArchivoFijo',
							strNombreArchivoFijo:strNombreArchivoFijo						
						}					
						,callback: procesarGenerarFijo
					});
					
				}
			},				
			{
				xtype: 'button',
				text: 'Imprimir Interfase',			
				id: 'btnArchivoInterfase',	
				iconCls: 'icoPdf',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');				
					
					var acuse = Ext.getCmp("acuse").getValue();
					var fecha = Ext.getCmp("fecha").getValue();
					var hora = Ext.getCmp("hora").getValue();	
					var usuario = Ext.getCmp("usuario").getValue();
					var recibo = Ext.getCmp("recibo").getValue();
					var monto_descuento_mn  = Ext.getCmp("monto_descuento_mn").getValue();
					var monto_interes_mn  = Ext.getCmp("monto_interes_mn").getValue();
					var monto_operar_mn  = Ext.getCmp("monto_operar_mn").getValue();
					var monto_descuento_dl  = Ext.getCmp("monto_descuento_dl").getValue();
					var monto_interes_dl  = Ext.getCmp("monto_interes_dl").getValue();
					var monto_operar_dl  = Ext.getCmp("monto_operar_dl").getValue();
					
					Ext.Ajax.request({
						url: '13forma03bpdfExt.jsp',
						params: {
							informacion:'ArchivoInterfaz',	
							ic_epo:ic_epo, 
							monto_descuento_mn:monto_descuento_mn,
							monto_interes_mn:monto_interes_mn,
							monto_operar_mn:monto_operar_mn,
							monto_descuento_dl:monto_descuento_dl,
							monto_interes_dl:monto_interes_dl,
							monto_operar_dl:monto_operar_dl,
							acuse:acuse,
							fecha:fecha,
							hora:hora,
							usuario:usuario,
							ic_banco_fondeo: ic_banco_fondeo,
							mensajeFondeo:mensajeFondeo,
							recibo:recibo
						}					
						,callback: procesarGenerarInterfaz
					});					
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarInterfase',
				iconCls: 'icoPdf',
				hidden: true				
			}		
		]
	});
	
	var fnProcSolicitarDescuentoCallback = function(vpkcs7, vtextoFirmar, vic_pyme, vic_epo, vic_banco_fondeo, 
			vmensajeFondeo, vdoctoSeleccionados, vmonto_descuento_mn, vmonto_interes_mn, vmonto_operar_mn, 
			vmonto_descuento_dl, vmonto_interes_dl, vmonto_operar_dl, vsPlazoBO){
			
		if (Ext.isEmpty(vpkcs7) || vpkcs7 == 'error:noMatchingCert' ) {
			Ext.getCmp("btnSolicitar").enable();
			return;	//Error en la firma. Termina...
		}	else  {		
			Ext.getCmp("btnSolicitar").disable();	
			var sPlazoBO = Ext.getCmp("sPlazoBO").getValue();
			Ext.Ajax.request({
				url: '13forma03Ext.data.jsp',
				params: {
					informacion:'GenerarAcuse',				
						pkcs7: vpkcs7,
						textoFirmado: vtextoFirmar,
						ic_pyme: vic_pyme,
						ic_epo: vic_epo, 					
						ic_banco_fondeo: vic_banco_fondeo,
						mensajeFondeo: vmensajeFondeo,
						doctoSeleccionados: vdoctoSeleccionados,
						monto_descuento_mn: vmonto_descuento_mn,
						monto_interes_mn: vmonto_interes_mn,
						monto_operar_mn: vmonto_operar_mn,
						monto_descuento_dl: vmonto_descuento_dl,
						monto_interes_dl: vmonto_interes_dl,
						monto_operar_dl: vmonto_operar_dl,
						sPlazoBO: vsPlazoBO
					}						
				,callback: procesarSuccessFailureAcuse				
			});
		}
	}
	
	var  ProcesarSolicitarDescuento = function() {
	
		var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');
		var store1 = gridConsultaTotales.getStore();	
		var cTotales;
		var leyendam;
		var monto_descuento_mn= 0;
		var monto_interes_mn= 0;
		var monto_operar_mn= 0;
		var monto_descuento_dl= 0;
		var monto_interes_dl= 0;
		var monto_operar_dl= 0;
		seleccionados =[];
		
		store1.each(function(record) {
			cTotales ='EPO:'+ record.data['EPO'] +"\n"
							+'Monto Descto MN:'+ record.data['MONTO_DESCUENTO_MN'] +"\n"
							+'Monto de Intereses MN:'+ record.data['MONTO_INTERES_MN'] +"\n"
							+'Monto a Operar MN:'+ record.data['MONTO_OPERAR_MN'] +"\n"
							+'Monto Descto DL:'+ record.data['MONTO_DESCUENTO_DL'] +"\n"
							+'Monto de Intereses D:'+ record.data['MONTO_INTERES_DL'] +"\n"
							+'Monto a Operar DL:'+ record.data['MONTO_OPERAR_DL'] +"\n";
			
			 monto_descuento_mn= record.data['MONTO_DESCUENTO_MN'];
			 monto_interes_mn= record.data['MONTO_INTERES_MN'];
			 monto_operar_mn= record.data['MONTO_OPERAR_MN'] ;
			 monto_descuento_dl= record.data['MONTO_DESCUENTO_DL'] ;
			 monto_interes_dl= record.data['MONTO_INTERES_DL'];
			 monto_operar_dl= record.data['MONTO_OPERAR_DL'];
		
		});
	
		if(ic_banco_fondeo==1) {
			leyendam='Al aceptar realizar el factoraje electr�nico o descuento electr�nico de los derechos que solicita la mipyme, me obligo a su fondeo en los t�rminos y condiciones establecidas con la mipyme. En este acto me obligo a notificar a la empresa de primer orden mi calidad de titular de los derechos sobre el documento descontado o factoreado y reiterarle su obligaci�n de pagarme en tiempo y forma la cantidad a que se refiere el documento aqu� descontado o factoreado. Las operaciones que aparecen en esta p�gina fueron notificadas a la empresa de primer orden de conformidad y para efectos de los art�culos 427 de la Ley General de T�tulos y Operaciones de Cr�dito, 32 C del C�digo Fiscal de la Federaci�n y 2038 del C�digo Civil Federal seg�n corresponda, para los efectos legales conducentes.\n';		
		}else if(ic_banco_fondeo==2) {
			leyendam='<B>LEYENDAS LEGALES</B> Al aceptar realizar el factoraje electr�nico o descuento electr�nico de los derechos que solicita la mipyme, me obligo a su fondeo en los t�rminos y condiciones establecidas con la mipyme. En este acto me obligo a notificar a la empresa de primer orden mi calidad de titular de los derechos sobre el documento descontado o factoreado y reiterarle su obligaci�n de pagarme en tiempo 	y forma la cantidad a que se refiere el documento 	aqu� descontado o factoreado.\n';		
		}
	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store2 = gridConsulta.getStore();	
		var datos ='No. Cliente Sirac|Proveedor|Numero de Documentos|Fecha de Emision|Fecha de Vencimiento|Moneda|Tipo Factoraje'+
						'|Monto|Porcentaje de Descuento|Recurso en Garantia|Monto Descuento|Monto Interes|Monto a operar|Tasa|Plazo|No. Proveedor \n';
		
		store2.each(function(record) {
		seleccionados.push(record.data['CLAVE_DOCUMENTO']);
			datos += record.data['NU_SIRAC'] +"|"+ record.data['NOMBRE_PROVEEDOR'] +"|"+ record.data['NUMER0_DOCTO'] +"|"+record.data['FECHA_EMISION'] +"|"
					 + record.data['FECHA_VENCIMIENTO'] +"|"+ record.data['MONEDA'] +"|"+record.data['TIPO_FACTORAJE'] +"|"+record.data['MONTO'] +"|"
					 + record.data['PORCE_DESCUENTO'] +"|"+ record.data['RECURSO_GARANTIA'] +"|"+record.data['MONTO_DESCUENTO'] +"|"+record.data['MONTO_INTERES'] +"|"
					 + record.data['MONTO_OPERAR'] +"|"+ record.data['TASA'] +"|"+record.data['PLAZO'] +"|"+record.data['NUM_PROVEEDOR'] +"|"
					 + record.data['BENEFICIARIO'] +"|"+ record.data['BANCO_BENEFICIARIO'] +"|"+record.data['SUCURSAL_BENEFICIARIO'] +"|"+record.data['CUENTA_BENEFICIARIO'] +"|"
					 + record.data['PORC_BENEFICIARIO'] +"|"+ record.data['IMPORTE_RECIBIR_BENE'] +"|"+record.data['NETO_RECIBIR']+"\n";
		
		});
		
		var gridTotales1 = Ext.getCmp('gridTotales1');
		var store3 = gridTotales1.getStore();	
		var totales;
		store3.each(function(record) {
			if(record.data['IC_MONEDA']==1){
			totales ='Numero de documentos MN:'+record.data['TOTAL_DOCTO']+'\n';
			}
			if(record.data['IC_MONEDA']==54){
				totales ='Numero de documentos DL:'+record.data['TOTAL_DOCTO']+'\n';
			}			
		});
		
		textoFirmar =cTotales+leyendam+datos+totales;
		
		var sPlazoBO = Ext.getCmp("sPlazoBO").getValue();
		
		NE.util.obtenerPKCS7(fnProcSolicitarDescuentoCallback, textoFirmar, ic_pyme, ic_epo, ic_banco_fondeo, 
			mensajeFondeo, doctoSeleccionados, monto_descuento_mn, monto_interes_mn, monto_operar_mn, 
			monto_descuento_dl, monto_interes_dl, monto_operar_dl, sPlazoBO);
		
		
		
	}
	
	
	if(ic_banco_fondeo==1) {
		leyenda='<table width="900" align="center" >'+
					'<tr><td class="formas" style="text-align: justify;" colspan="3" >Al aceptar realizar el factoraje electr�nico o descuento electr�nico de los derechos que solicita la mipyme, me obligo a su fondeo en los t�rminos y condiciones establecidas con la mipyme. En este acto me obligo a notificar a la empresa de primer orden mi calidad de titular de los derechos sobre el documento descontado o factoreado y reiterarle su obligaci�n de pagarme en tiempo y forma la cantidad a que se refiere el documento aqu� descontado o factoreado. Las operaciones que aparecen en esta p�gina fueron notificadas a la empresa de primer orden de conformidad y para efectos de los art�culos 427 de la Ley General de T�tulos y Operaciones de Cr�dito, 32 C del C�digo Fiscal de la Federaci�n y 2038 del C�digo Civil Federal seg�n corresponda, para los efectos legales conducentes.   </td></tr></table>';		
	}else if(ic_banco_fondeo==2) {
		leyenda='<table width="900" align="center" >'+
					'<tr>'+
					'<td class="formas" style="text-align: justify;" colspan="3">'+
					'<B>LEYENDAS LEGALES</B> Al aceptar realizar el factoraje electr�nico o descuento electr�nico de los derechos que solicita la mipyme, me obligo a su fondeo en los t�rminos y condiciones establecidas con la mipyme. En este acto me obligo a notificar a la empresa de primer orden mi calidad de titular de los derechos sobre el documento descontado o factoreado y reiterarle su obligaci�n de pagarme en tiempo 	y forma la cantidad a que se refiere el documento aqu� descontado o factoreado.  '+  
					'</td>'+
					'</tr></table>';		
	}	
	
	var mensaje = new Ext.Container({
		layout: 'table',		
		id: 'mensaje',							
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		leyenda,
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}	
			}			
		]
	});
	
	
	var  hayDoctosAgregados2='<table width="650" align="center" >'+
		'<tr>'+
		'<td class="formas" style="color:red;align:justified;font-weight:bold">'+
		'Se han incluido Documentos No Seleccionados debido a que comparten al menos una Nota de Cr�dito M�ltiple con los Documentos previamente especificados.Para ver la relaci�n de los Documentos Seleccionados con las Notas de Cr�dito M�ltiple, dar click	'+
		'</td>'+
		'</tr></table>';	
	var  hayDoctosAgregados21='<table width="650" align="center" >'+
		'<tr>'+
		'<td class="formas" style="color:red;align:justified;font-weight:bold">'+
		'<br> Se indican en color rojo los Documentos que han sido agregados.</br>'+
		'</td>'+
		'</tr></table>';	
		
	var hayDoctosAgregados3 = new Ext.Container({
		layout: 'table',		
		id: 'hayDoctosAgregados3',	
		hidden: true,
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		items: [			
			{ 
				xtype:   'label',  
				html:		hayDoctosAgregados2				
			},
			{
				xtype: 'button',
				text: 'Aqui',			
				id: 'btnAqui',	 
				iconCls: 'iconoLupa',
				handler: procesaVentana
			},
			{ 
				xtype:   'label',  
				html:		hayDoctosAgregados21				
			}
		]
	});

	
	var fondePropio = new Ext.Container({
		layout: 'table',		
		id: 'fondePropio',							
		width:	'900',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 
				xtype:   'label',
				id:	'fondeoP',			
				html:		'<span style="color:red;"><b> Las siguientes solicitudes de descuento se realizar�n con Fondeo propio<b></span>',  
				cls:		'x-form-item', 
				style: { 
					width:   		'100%', 
					textAlign: 		'left'
				} 
			}
		]
	});
	
	if(mensajeFondeo=='S'){	
		var fondePropio = Ext.getCmp('fondePropio');
		fondePropio.show();
	} 	else {
		var fondePropio = Ext.getCmp('fondePropio');
		fondePropio.hide();
	}
	
	
	//Contenedor para los botones de imprimir y Salir 
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'400',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [	
			{
				xtype: 'button',
				text: 'Cancelar',			
				iconCls: 'icoLimpiar',
				id: 'btnCancelar',		 
				handler: ProcesarCancelar
			},
			{
				xtype: 'button',
				text: 'Solicitar Descuento',
				tooltip:	'Solicitar Descuento',
				iconCls: 'icoAceptar',
				id: 'btnSolicitar'
				,handler:ProcesarSolicitarDescuento		
			},
			{
				xtype: 'button',
				text: 'Generar Archivo',
				iconCls: 'icoXls',
				id: 'btnArchivoCSV',	
				hidden: true,
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					
					var nombreArchivoCSV = Ext.getCmp("nombreArchivoCSV").getValue();
					var acuse = Ext.getCmp("acuse").getValue();
					var fecha = Ext.getCmp("fecha").getValue();
					var hora = Ext.getCmp("hora").getValue();	
					var usuario = Ext.getCmp("usuario").getValue();
					var recibo = Ext.getCmp("recibo").getValue();
					var monto_descuento_mn  = Ext.getCmp("monto_descuento_mn").getValue();
					var monto_interes_mn  = Ext.getCmp("monto_interes_mn").getValue();
					var monto_operar_mn  = Ext.getCmp("monto_operar_mn").getValue();
					var monto_descuento_dl  = Ext.getCmp("monto_descuento_dl").getValue();
					var monto_interes_dl  = Ext.getCmp("monto_interes_dl").getValue();
					var monto_operar_dl  = Ext.getCmp("monto_operar_dl").getValue();
					
					Ext.Ajax.request({
						url: '13forma03Ext.data.jsp',
						params: {
							informacion:'nombreArchivoCSV',
							ic_epo:ic_epo, 
							monto_descuento_mn:monto_descuento_mn,
							monto_interes_mn:monto_interes_mn,
							monto_operar_mn:monto_operar_mn,
							monto_descuento_dl:monto_descuento_dl,
							monto_interes_dl:monto_interes_dl,
							monto_operar_dl:monto_operar_dl,
							acuse:acuse,
							fecha:fecha,
							hora:hora,
							usuario:usuario,
							ic_banco_fondeo:ic_banco_fondeo,
							recibo:recibo,
							accion:'button'					
						}					
						,callback: procesarGenerarCSV
					});
						
				}
			},
			{
				xtype: 'button',
				iconCls: 'icoXls',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarCSV',
				hidden: true
			},	
			{
				xtype: 'button',
				text: 'Generar PDF',
				iconCls: 'icoPdf',
				id: 'btnArchivoPDF',
				hidden: true,
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');								
					var acuse = Ext.getCmp("acuse").getValue();
					var fecha = Ext.getCmp("fecha").getValue();
					var hora = Ext.getCmp("hora").getValue();	
					var usuario = Ext.getCmp("usuario").getValue();
					var recibo = Ext.getCmp("recibo").getValue();
					var monto_descuento_mn  = Ext.getCmp("monto_descuento_mn").getValue();
					var monto_interes_mn  = Ext.getCmp("monto_interes_mn").getValue();
					var monto_operar_mn  = Ext.getCmp("monto_operar_mn").getValue();
					var monto_descuento_dl  = Ext.getCmp("monto_descuento_dl").getValue();
					var monto_interes_dl  = Ext.getCmp("monto_interes_dl").getValue();
					var monto_operar_dl  = Ext.getCmp("monto_operar_dl").getValue();
					
					Ext.Ajax.request({
						url: '13forma03bpdfExt.jsp',
						params: {
							informacion:'ArchivoPDF',	
							ic_epo:ic_epo, 
							monto_descuento_mn:monto_descuento_mn,
							monto_interes_mn:monto_interes_mn,
							monto_operar_mn:monto_operar_mn,
							monto_descuento_dl:monto_descuento_dl,
							monto_interes_dl:monto_interes_dl,
							monto_operar_dl:monto_operar_dl,
							acuse:acuse,
							fecha:fecha,
							hora:hora,
							usuario:usuario,
							ic_banco_fondeo:ic_banco_fondeo,
							recibo:recibo,
							accion:'button'
						}					
						,callback: procesarGenerarPDF
					});				
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarPDF',
				iconCls: 'icoPdf',
				hidden: true
			},	
			{
				xtype: 'button',
				text: 'Salir',		
				iconCls: 'icoLimpiar',
				id: 'btnSalir',	
				hidden: true,
				handler: function() {
					window.location = "13forma03Ext.jsp";
				}
			}
		]
	});
	
	
	var constancia = new Ext.Container({
		layout: 'table',		
		id: 'constancia',							
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		items: [	
			{ 
				xtype:   'displayfield',
				id:	'const',			
				value:		'<span style="color:blue;"><b> Constancia de dep�sito de documentos a descontar Cadenas Productivas<b></span>',  
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}	
			},			
			{
				xtype: 'displayfield',
				id:	'mensajesFondeoPre1',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}			
			},				
			{
				xtype: 'compositefield',
				fieldLabel: '',
				id:'nombreInter0',
				msgTarget: 'side',
				combineErrors: false,
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				},	
				items: [
					{
								xtype: 'displayfield',
								value: '<b>Nombre del Intermediario Financiero</b>'
					},
					{
								xtype: 'displayfield',
								id:	'nombreInter1',
								value: ''
					}	
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: '',
				id:'fechaConvenio0',
				msgTarget: 'side',
				combineErrors: false,
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				},	
				items: [
					{
								xtype: 'displayfield',
								value: '<b>Fecha del Convenio de Descuento Electr�nico</b>'
					},
					{
								xtype: 'displayfield',
								id:	'fechaConvenio1',
								value: ''
					}	
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: '',
				id:'fechaDesc0',
				msgTarget: 'side',
				combineErrors: false,
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				},	
				items: [
					{
								xtype: 'displayfield',
								value: '<b>Fecha del Descuento</b>'
					},
					{
								xtype: 'displayfield',
								id:	'fechaDesc1',
								value: ''
					}	
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: '',
				id:'nota0',
				msgTarget: 'side',
				combineErrors: false,
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				},	
				items: [
					{
								xtype: 'displayfield',
								value: '<b>Nota</b>'
					},
					{
								xtype: 'displayfield',
								id:	'nota1',
								value: ''
					}	
				]
			}		
		]
	});
	
	var facultado = new Ext.Container({
		layout: 'table',		
		id: 'facultado',							
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		items: [	
			{ 
				xtype:   'displayfield',
				id:	'nombreFacultado',			
				value:		'', 			
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}	
			},				
			{ 
				xtype:   'displayfield',
				id:	'descripFacultado',			
				value:		'',  
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}	
			},
			{ 
				xtype:   'displayfield',
				id:	'sPlazoBO',
				hidden: true,
				value:		'',  
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}	
			}
		]
	});
	
	
	
	
	var consultaTotales1 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'consultaTotales1'
		},
		hidden: true,
		fields: [			
			{name: 'IC_MONEDA'},
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCTO'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {				
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.											
					}
				}
			}			
	});
	
	
	var gridTotales1 = new Ext.grid.GridPanel({
		id: 'gridTotales1',				
		store: consultaTotales1,	
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		title:'',
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'Total de Documentos',
				tooltip: 'Total de Documentos',
				dataIndex : 'TOTAL_DOCTO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'center'				
			},
			{							
				header : '',
				tooltip: '',
				dataIndex : '',
				sortable: true,
				width: 480,
				resizable: true,				
				align: 'right'							
			}	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 90,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	

	var consultaTotales2 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'consultaTotales2'
		},
		hidden: true,
		fields: [						
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCTO'},
			{name: 'TOTAL_IMPORTE_DOCTO'},
			{name: 'TOTAL_IMPORTE_DESC'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {				
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.											
					}
				}
			}			
	});
	
	
	var gridTotales2 = new Ext.grid.GridPanel({
		id: 'gridTotales2',				
		store: consultaTotales2,	
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		title:'',
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'Total de Documentos',
				tooltip: 'Total de Documentos',
				dataIndex : 'TOTAL_DOCTO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'center'				
			},		
			{							
				header : 'Total Importe Documento',
				tooltip: 'Total Importe Documento',
				dataIndex : 'TOTAL_IMPORTE_DOCTO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Total Importe Descuento',
				tooltip: 'Total Importe Descuento',
				dataIndex : 'TOTAL_IMPORTE_DESC',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : '',
				tooltip: '',
				dataIndex : '',
				sortable: true,
				width: 90,
				resizable: true,				
				align: 'right'							
			}	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 90,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var procesarconsultaDataTotales = function(store, arrRegistros, opts) 	{
	
		var jsonData = store.reader.jsonData;			
		
		var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');	
		var el = gridConsultaTotales.getGridEl();
		
		if (arrRegistros != null) {
				
			if (!gridConsultaTotales.isVisible()) {
				gridConsultaTotales.show();
			}				
						
			if(store.getTotalCount() > 0) {				
				if(jsonData.msg!=''){
					Ext.MessageBox.alert('Aviso', jsonData.msg);
					Ext.getCmp("btnSolicitar").disable();		
				}else  {
					Ext.getCmp("btnSolicitar").enable();	
				}
				
				el.unmask();	
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotalesPre'
		},
		hidden: true,
		fields: [						
			{name: 'EPO'},
			{name: 'MONTO_DESCUENTO_MN'},
			{name: 'MONTO_INTERES_MN'},
			{name: 'MONTO_OPERAR_MN'},			
			{name: 'MONTO_DESCUENTO_DL'},
			{name: 'MONTO_INTERES_DL'},
			{name: 'MONTO_OPERAR_DL'}		
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarconsultaDataTotales,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarconsultaDataTotales(null, null, null);						
					}
				}
			}	
	});
	
		//esto esta en duda como manejarlo
	var gruposConsulta = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 1, align: 'center'},
				{	header: 'Moneda Nacional', colspan: 3, align: 'center'},	
				{	header: 'D�lares ', colspan: 3, align: 'center'}
			]
		]
	});
	
	var gridConsultaTotales = new Ext.grid.GridPanel({
		id: 'gridConsultaTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		title:'',
		//hidden: true,
		plugins: gruposConsulta,	
		columns: [
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'Monto Descto.',
				tooltip: 'Monto Descto.',
				dataIndex : 'MONTO_DESCUENTO_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_INTERES_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
				{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},	
				{							
				header : 'Monto Descto.',
				tooltip: 'Monto Descto.',
				dataIndex : 'MONTO_DESCUENTO_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_INTERES_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 120,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
	
		var jsonData = store.reader.jsonData;			
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
				
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}				
							
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();				
			
			var hayDoctosAgregados3 = Ext.getCmp('hayDoctosAgregados3');			
						
			Ext.getCmp("nombreInter1").setValue(jsonData.nombreInter1);
			Ext.getCmp("fechaDesc1").setValue(jsonData.fechaDesc1);
			Ext.getCmp("fechaConvenio1").setValue(jsonData.fechaConvenio1);
			Ext.getCmp("nota1").setValue(jsonData.nota1);
			Ext.getCmp("mensajesFondeoPre1").setValue(jsonData.mensajesFondeoPre);		
			
			Ext.getCmp("nombreFacultado").setValue(jsonData.nombreFacultado);		
			Ext.getCmp("descripFacultado").setValue(jsonData.descripFacultado);	
			
			Ext.getCmp("sPlazoBO").setValue(jsonData.sPlazoBO);		
						
			if(jsonData.hayDoctosAgregados=='S'){
				hayDoctosAgregados3.show();
			}
			
			if(jsonData.bOperaFactorajeDist=='S' || jsonData.bOperaFactorajeVencInfonavit=='S'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_RECIBIR_BENE'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);
			}
			if(jsonData.bTipoFactoraje =='S') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
			}
			
			if(store.getTotalCount() > 0) {
				el.unmask();	
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse'
		},
		hidden: true,
		fields: [	
			{name: 'COLOR_FUENTE'},			
			{name: 'NOMBRE_EPO'},	
			{name: 'NU_SIRAC'},
			{name: 'NOMBRE_PROVEEDOR'},
			{name: 'NUMER0_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO'},
			{name: 'PORCE_DESCUENTO'},
			{name: 'RECURSO_GARANTIA'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'TASA'},
			{name: 'PLAZO'},
			{name: 'NUM_PROVEEDOR'},
			{name: 'BENEFICIARIO'},
			{name: 'BANCO_BENEFICIARIO'},
			{name: 'SUCURSAL_BENEFICIARIO'},
			{name: 'CUENTA_BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR_BENE'},
			{name: 'NETO_RECIBIR'},
			{name: 'CLAVE_DOCUMENTO'},
			{name: 'REFERENCIA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
	//	hidden: true,
		columns: [
			{							
				header : 'No. Cliente Sirac',
				tooltip: 'No. Cliente Sirac',
				dataIndex : 'NU_SIRAC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'
			},
			{
				header : 'Proveedor/Cedente',
				tooltip: 'Proveedor/Cedente',
				dataIndex : 'NOMBRE_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUMER0_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){  				
					if( registro.data['COLOR_FUENTE']=='S'){
						return '<span style="color:red;">' + value + '</span>';
					}else {
						return value;
					}
				}
			},	
			{							
				header : 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},	
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},	
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'left'			
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORCE_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{							
				header : 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex : 'RECURSO_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Monto Inter�s',
				tooltip: 'Monto Inter�s',
				dataIndex : 'MONTO_INTERES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Tasa',
				tooltip: 'Tasa',
				dataIndex : 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia',
				dataIndex:'REFERENCIA',
				sortable: true,
				hidden:	true,
				width: 130,
				resizable: true,				
				align: 'left'
			},
			{							
				header : 'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},				
			{							
				header : 'No. Proveedor',
				tooltip: 'No. Proveedor',
				dataIndex : 'NUM_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex : 'BENEFICIARIO',
				sortable: true,				
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},	
			{							
				header : 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex : 'BANCO_BENEFICIARIO',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},	
			{							
				header : 'Sucursal Beneficiaria',
				tooltip: 'Sucursal Beneficiaria',
				dataIndex : 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'				
			},	
			{							
				header : 'Cuenta Beneficiaria',
				tooltip: 'Cuenta Beneficiaria',
				dataIndex : 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'				
			},	
			{							
				header : '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{							
				header : 'Importe a Recibir Beneficiario',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex : 'IMPORTE_RECIBIR_BENE',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Neto a Recibir PyME',
				tooltip: 'Neto a Recibir PyME',
				dataIndex : 'NETO_RECIBIR',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		margins: '20 0 0 0',
		loadMask: true,		
		stripeRows: true,
		height: 200,
		width: 900,		
		title: '',
		frame: false
	});
	
		
	var consultaData2 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse2'
		},
		hidden: true,
		fields: [						
			{name: 'NOMBRE_EMISIOR'},	
			{name: 'NOMBRE_EMPRESA'},			
			{name: 'DESCRIPCION_DOCTO'},
			{name: 'LUGAR_SUSCRIPCION'},
			{name: 'FECHA_SUSCRIPCION'},			
			{name: 'IMPORTE_DOCTO'},
			{name: 'IMPORTE_DESC'},		
			{name: 'PERIODICIDAD'},	
			{name: 'FECHA_VTO_DES'},	
			{name: 'TASA_DESCUENTO'},	
			{name: 'MONEDA'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);																					
					}
				}
			}
		});
			
			
	
	var gridConsulta2 = new Ext.grid.GridPanel({
		id: 'gridConsulta2',				
		store: consultaData2,	
		style: 'margin:0 auto;',
	//	hidden: true,
		columns: [
			{							
				header : 'Nombre del Emisor',
				tooltip: 'Nombre del Emisor',
				dataIndex : 'NOMBRE_EMISIOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Nombre de la Empresa Acreditada',
				tooltip: 'Nombre de la Empresa Acreditada',
				dataIndex : 'NOMBRE_EMPRESA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Descripci�n del Documento',
				tooltip: 'Descripci�n del Documento',
				dataIndex : 'DESCRIPCION_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Lugar Suscripci�n',
				tooltip: 'Lugar Suscripci�n',
				dataIndex : 'LUGAR_SUSCRIPCION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Fecha Suscripci�no',
				tooltip: 'Fecha Suscripci�n',
				dataIndex : 'FECHA_SUSCRIPCION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},	
			{							
				header : 'Importe Documento',
				tooltip: 'Importe Documento',
				dataIndex : 'IMPORTE_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{							
				header : 'Importe Descuento',
				tooltip: 'Importe Descuento',
				dataIndex : 'IMPORTE_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Periodicidad pago Capital e Inter�s',
				tooltip: 'Periodicidad pago Capital e Inter�s',
				dataIndex : 'PERIODICIDAD',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'left'			
			},
			{							
				header : 'Fecha Vto. Descuento',
				tooltip: 'Fecha Vto. Descuento',
				dataIndex : 'FECHA_VTO_DES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Tasa Descuentoo',
				tooltip: 'Tasa Descuento',
				dataIndex : 'TASA_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Nombre de la Moneda',
				tooltip: 'Nombre de la Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		margins: '20 0 0 0',
		loadMask: true,		
		stripeRows: true,
		height: 200,
		width: 900,		
		title: '',
		frame: false
	});
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		hidden: true,
		width: 500,
		title: 'Autorizaci�n de Descuento',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',		
		monitorValid: false,
		collapsible: false		
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fp,
			NE.util.getEspaciador(20),
			hayDoctosAgregados3,
			mensajeAutentificacion,
			NE.util.getEspaciador(20),
			fondePropio,
			NE.util.getEspaciador(20),
			gridConsultaTotales,
			NE.util.getEspaciador(20),
			gridCifrasControl,
			mensaje,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales1,
			NE.util.getEspaciador(20),
			constancia,
			fpBotones2,
			NE.util.getEspaciador(20),
			gridConsulta2,
			gridTotales2,
			NE.util.getEspaciador(20),
			facultado,
			NE.util.getEspaciador(20)
		]
	});
	
	Ext.getCmp("btnSolicitar").disable();	
	
	consultaData.load({  	
		params: {  
			informacion: 'ConsultarPreAcuse', 	
			doctoSeleccionados:doctoSeleccionados, 
			ic_pyme:ic_pyme,  	
			ic_epo:ic_epo, 
			ic_banco_fondeo:ic_banco_fondeo,
			numeroDoctoMN:numeroDoctoMN,
			numeroDoctoDL:numeroDoctoDL
		}  		
	});	
	
	consultaData2.load({  	
		params: {  
			informacion: 'ConsultarPreAcuse2', 	
			doctoSeleccionados:doctoSeleccionados, 
			ic_pyme:ic_pyme,  	
			ic_epo:ic_epo, 
			ic_banco_fondeo:ic_banco_fondeo,
			numeroDoctoMN:numeroDoctoMN,
			numeroDoctoDL:numeroDoctoDL
		}  	
	});	
	
	
	consultaDataTotales.load({  	
		params: {  
			informacion: 'ConsultarTotalesPre', 	
			doctoSeleccionados:doctoSeleccionados, 
			ic_pyme:ic_pyme,  	
			ic_epo:ic_epo, 
			ic_banco_fondeo:ic_banco_fondeo,
			numeroDoctoMN:numeroDoctoMN,
			numeroDoctoDL:numeroDoctoDL
		}  	
	});	
	
	
	consultaTotales1.load({  	
		params: {  
			informacion: 'consultaTotales1', 	
			doctoSeleccionados:doctoSeleccionados, 
			ic_pyme:ic_pyme,  	
			ic_epo:ic_epo, 
			ic_banco_fondeo:ic_banco_fondeo,
			numeroDoctoMN:numeroDoctoMN,
			numeroDoctoDL:numeroDoctoDL
		}  	
	});	
	
	consultaTotales2.load({  	
		params: {  
			informacion: 'consultaTotales2', 	
			doctoSeleccionados:doctoSeleccionados, 
			ic_pyme:ic_pyme,  	
			ic_epo:ic_epo, 
			ic_banco_fondeo:ic_banco_fondeo,
			numeroDoctoMN:numeroDoctoMN,
			numeroDoctoDL:numeroDoctoDL
		}  	
	});	
	
				
});