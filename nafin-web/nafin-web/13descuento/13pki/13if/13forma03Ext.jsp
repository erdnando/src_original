<!DOCTYPE html>
<%@ page import="
		java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
String pantalla = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";
String ic_pyme = (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")!=null)?request.getParameter("ic_banco_fondeo"):"";
String mensajeFondeo = (request.getParameter("mensajeFondeo")!=null)?request.getParameter("mensajeFondeo"):"N";
String numeroDoctoMN = (request.getParameter("numeroDoctoMN")!=null)?request.getParameter("numeroDoctoMN"):"0";
String numeroDoctoDL = (request.getParameter("numeroDoctoDL")!=null)?request.getParameter("numeroDoctoDL"):"0";

String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
String textoFirmado = (request.getParameter("textoFirmado")!=null)?request.getParameter("textoFirmado"):"";
String monto_descuento_mn = (request.getParameter("monto_descuento_mn")!=null)?request.getParameter("monto_descuento_mn"):"";
String monto_interes_mn = (request.getParameter("monto_interes_mn")!=null)?request.getParameter("monto_interes_mn"):"";
String monto_operar_mn = (request.getParameter("monto_operar_mn")!=null)?request.getParameter("monto_operar_mn"):"";
String monto_descuento_dl = (request.getParameter("monto_descuento_dl")!=null)?request.getParameter("monto_descuento_dl"):"";
String monto_interes_dl = (request.getParameter("monto_interes_dl")!=null)?request.getParameter("monto_interes_dl"):"";
String monto_operar_dl = (request.getParameter("monto_operar_dl")!=null)?request.getParameter("monto_operar_dl"):"";
String sPlazoBO = (request.getParameter("sPlazoBO")!=null)?request.getParameter("sPlazoBO"):"";
							
								
String	seleccionados[]		=	request.getParameterValues("seleccionados");
String doctoSeleccionados = (request.getParameter("doctoSeleccionados")!=null)?request.getParameter("doctoSeleccionados"):"";

if (seleccionados != null) {
	for(int i = 0; i < seleccionados.length; i++) {
		doctoSeleccionados +=seleccionados[i]+",";
	}
}
%>
<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<%if(pantalla.equals("")){ %>
<script type="text/javascript" src="13forma03Ext.js?<%=session.getId()%>"></script>
<%}else  if(pantalla.equals("PreAcuse")){%>
<script type="text/javascript" src="13forma03aExt.js?<%=session.getId()%>"></script>
<%}else  if(pantalla.equals("Acuse")){%>
<script type="text/javascript" src="13forma03bExt.js?<%=session.getId()%>"></script>
<%}%>

<%-- El jsp que contiene el dise?o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>

<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../certificado.jspf" %>

</head>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01if/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
	
	<form id='formAux' name="formAux" target='_new'></form>
	
	<form id='formDatos' method="post"  name="formDatos">
		<input type="hidden" id="doctoSeleccionados" name="doctoSeleccionados" value="<%=doctoSeleccionados%>"/>
		<input type="hidden" id="ic_pyme" name="ic_pyme" value="<%=ic_pyme%>"/>
		<input type="hidden" id="ic_epo" name="ic_epo" value="<%=ic_epo%>"/>
		<input type="hidden" id="ic_banco_fondeo" name="ic_banco_fondeo" value="<%=ic_banco_fondeo%>"/>
		<input type="hidden" id="mensajeFondeo" name="mensajeFondeo" value="<%=mensajeFondeo%>"/>		
		<input type="hidden" id="numeroDoctoMN" name="numeroDoctoMN" value="<%=numeroDoctoMN%>"/>
		<input type="hidden" id="numeroDoctoDL" name="numeroDoctoDL" value="<%=numeroDoctoDL%>"/>
		<input type="hidden" id="pantalla" name="pantalla" value="<%=pantalla%>"/>
	
	</form>
	
	
	<form id='formParametros' name="formParametros">
		<input type="hidden" id="pkcs7" name="pkcs7" value="<%=pkcs7%>"/>
		<input type="hidden" id="textoFirmado" name="textoFirmado" value="<%=textoFirmado%>"/>
		<input type="hidden" id="monto_descuento_mn" name="monto_descuento_mn" value="<%=monto_descuento_mn%>"/>
		<input type="hidden" id="monto_interes_mn" name="monto_interes_mn" value="<%=monto_interes_mn%>"/>
		<input type="hidden" id="monto_operar_mn" name="monto_operar_mn" value="<%=monto_operar_mn%>"/>
		<input type="hidden" id="monto_descuento_dl" name="monto_descuento_dl" value="<%=monto_descuento_dl%>"/>
		<input type="hidden" id="monto_interes_dl" name="monto_interes_dl" value="<%=monto_interes_dl%>"/>
		<input type="hidden" id="monto_operar_dl" name="monto_operar_dl" value="<%=monto_operar_dl%>"/>
		<input type="hidden" id="sPlazoBO" name="sPlazoBO" value="<%=sPlazoBO%>"/>	
	</form>
</body>

</html>