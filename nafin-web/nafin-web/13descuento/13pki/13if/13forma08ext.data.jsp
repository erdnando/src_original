<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	com.netro.mandatodoc.*,	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String proceso = (request.getParameter("proceso")!=null)?request.getParameter("proceso"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String tipoTasa = (request.getParameter("tipoTasa")!=null)?request.getParameter("tipoTasa"):"P";
String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"1";
String ic_pyme = (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
String txtPuntos = (request.getParameter("txtPuntos")!=null)?request.getParameter("txtPuntos"):"";
String lstPlazo = (request.getParameter("lstPlazo")!=null)?request.getParameter("lstPlazo"):"";
String dlpuntos2 = (request.getParameter("dlpuntos2")!=null)?request.getParameter("dlpuntos2"):"";
String ldTotal2 = (request.getParameter("ldTotal2")!=null)?request.getParameter("ldTotal2"):"";
String [] tasas		= request.getParameterValues("tasas");
String [] ValorTasas = request.getParameterValues("valorTasa");
String acuse2 = (request.getParameter("acuse2")!=null)?request.getParameter("acuse2"):"";
String fechaCarga = (request.getParameter("fechaCarga")!=null)?request.getParameter("fechaCarga"):"";
String horaCarga = (request.getParameter("horaCarga")!=null)?request.getParameter("horaCarga"):"";
String recibo = (request.getParameter("recibo")!=null)?request.getParameter("recibo"):"";
String acuse3 = (request.getParameter("acuse3")!=null)?request.getParameter("acuse3"):"";
String aplicaFloating = (request.getParameter("aplicaFloating") != null) ? request.getParameter("aplicaFloating") : "";
String operaFloating = "";
if ("S".equals(aplicaFloating)  ) {
	operaFloating = " / Floating";
}

double ldPuntos	= 0,  ldValorTotalTasa = 0,  ldTotal 	= 0;
Vector lovDatos 	= null;
Vector lovRegistro = null;
int numRegistros =0;
String  ic_if =iNoCliente,  infoRegresar ="", lsCveEpo  ="", lsNombre = "", 	lsCalificacion  ="",  lsCadenaTasa 	=  "", 
lsTemp ="", lsPlazo	="", lsDescripPlazo ="", ic_tasa ="",  nueValorTasa  ="", consulta ="", lsValorTasaN 	= "", lsFecha ="",
mensajeEliminacion ="", estaEliminar ="";

if (txtPuntos.equals(""))
	txtPuntos = "0";
ldPuntos = Double.parseDouble(txtPuntos);
if(tipoTasa.equals("N")) tipoTasa = "NG";
StringBuffer contenidoArchivo = new StringBuffer("");
CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = null;
	
HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

AutorizacionTasas BeanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
	

if (informacion.equals("catalogoEPO")  ) {

	CatalogoEPODescTasas cat = new CatalogoEPODescTasas();
	cat.setCampoClave("cie.ic_epo");
	cat.setCampoDescripcion("ce.cg_razon_social");
	cat.setClaveIF(ic_if);
	cat.setLsTipoTasa(tipoTasa);   
	cat.setAmbos("A");
	cat.setParametro("TIPO_TASA");
	cat.setOrden("ce.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
	
}else if (informacion.equals("catalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	


}else if (informacion.equals("CatalogoPYME") && !ic_epo.equals("") && !ic_moneda.equals("") ) {

	CatalogoPYMEDescTasas catalogo = new CatalogoPYMEDescTasas();
	catalogo.setCampoClave("ccb.ic_pyme ");
	catalogo.setCampoDescripcion("cp.cg_razon_social");	
	catalogo.setClaveEPO(ic_epo);	
	catalogo.setClaveIF(ic_if);
	catalogo.setClaveMoneda(ic_moneda);	
	catalogo.setOrden("cp.cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();	


}else if (informacion.equals("catalogoPlazo") ) {

	CatalogoPlazos catalogo = new CatalogoPlazos();
	catalogo.setCampoClave("ic_plazo ");
	catalogo.setCampoDescripcion("cg_descripcion");	
	catalogo.setClaveProducto("1");	
	catalogo.setOrden("in_plazo_dias");	
	infoRegresar = catalogo.getJSONElementos();	

}else if (informacion.equals("OperaFideicomiso") ) {
	
	String bandera = BeanTasas.operaFideicomiso(iNoCliente);
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("OPERA",bandera);	
	infoRegresar  = jsonObj.toString();

}else if (informacion.equals("ValidaPYME")  && tipoTasa.equals("P") ) {

	boolean sihayTasas = false;
	String sihayTasasC = "N";
	sihayTasas = 	BeanTasas.tipodeTasas(ic_pyme,"");	
	if(sihayTasas) { sihayTasasC ="S";  }
	
	String opeSucepFloating  =  BeanTasas.suceptibleFloating(ic_pyme, "");
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("sihayTasas",sihayTasasC);	
	jsonObj.put("opeSucepFloating",opeSucepFloating);	
	infoRegresar  = jsonObj.toString();

}else if (informacion.equals("Consultar")   ) {

	
	List datosTasa = new ArrayList();	
	List datosPlazo = new ArrayList();	
	int tasasRojo = 0;
	String dTasa_aplicar = "N";
	HashMap	datosTasasAux = new HashMap();	
	Vector regis = new Vector();
	
	if(proceso.equals("Procesar") ){	
	
	  regis =  BeanTasas.getTasaAutIF(ic_epo, "", ic_moneda, "N");	  
	
		for (int i = 0; i < regis.size(); i++) {
			Vector	registro		= (Vector) regis.get(i);
						
			String	relmat_tasa 	= registro.get(9).toString();
			String	puntos_tasa 	= registro.get(10).toString();
			String	valor_tasa 		= registro.get(11).toString();
			String   rango_plazo = registro.get(8).toString();
			String 	tasa_aplicar 	= "";	
			
			if(relmat_tasa.equals("+")) {
				tasa_aplicar = (new BigDecimal(valor_tasa).add(new BigDecimal(puntos_tasa))).toPlainString();
			}
			if(relmat_tasa.equals("-")) {
				tasa_aplicar = (new BigDecimal(valor_tasa).subtract(new BigDecimal(puntos_tasa))).toPlainString();
			}
			if(relmat_tasa.equals("/")) {
				if (!puntos_tasa.equals("0")) {
					tasa_aplicar = (new BigDecimal(valor_tasa).divide(new BigDecimal(puntos_tasa),5)).toPlainString();
				}
			}
			if(relmat_tasa.equals("*")) {
				tasa_aplicar = (new BigDecimal(valor_tasa).multiply(new BigDecimal(puntos_tasa))).toPlainString();
			}	
			
			datosTasa.add(tasa_aplicar);	
			datosPlazo.add(rango_plazo);		
			datosTasasAux.put("tasa_aplicar"+i,tasa_aplicar);	
			datosTasasAux.put("rango_plazo"+i,rango_plazo);	
			
			
		}	
	}

	if(tipoTasa.equals("P")) { //Preferenciales
		lovDatos = BeanTasas.procesarTasaPreferencial(ic_if, ic_moneda, ic_epo, ic_pyme, ldPuntos, tipoTasa, aplicaFloating );
		for (int i= 0; i < lovDatos.size(); i++) {
			numRegistros++;
			lovRegistro = (Vector) lovDatos.elementAt(i);	
				
			lsCveEpo  		= lovRegistro.elementAt(0).toString();
			lsNombre 		= lovRegistro.elementAt(1).toString();
			lsCalificacion 	= (lovRegistro.elementAt(2)==null)?" ":lovRegistro.elementAt(2).toString();
			lsCadenaTasa 	= lovRegistro.elementAt(3).toString();
			ldValorTotalTasa= ((Double)lovRegistro.elementAt(4)).doubleValue();
			lsTemp 			= lovRegistro.elementAt(5).toString();
			lsPlazo			= (lovRegistro.elementAt(6)==null)?"":lovRegistro.elementAt(6).toString();
			ldTotal 		= ((Double)lovRegistro.elementAt(10)).doubleValue();
			lsDescripPlazo =(lovRegistro.elementAt(7)==null)?"":lovRegistro.elementAt(7).toString();
			ic_tasa =(lovRegistro.elementAt(8)==null)?"0":lovRegistro.elementAt(8).toString();
			nueValorTasa =  (lovRegistro.elementAt(9)==null)?"0":lovRegistro.elementAt(9).toString();
	
			if(proceso.equals("Procesar") ){			
				ldTotal = ldTotal;
			} else 	if (proceso.equals("Consulta") ){		
				ldTotal = ((double) (Math.round(ldValorTotalTasa*100000) - Math.round(Double.parseDouble(nueValorTasa)*100000))) / 100000;
				ldPuntos = ( (double) (Math.round(ldValorTotalTasa*100000))-(double)(Math.round(ldTotal*100000)) ) / 100000;
      	} 
			
			datos = new HashMap();				
			datos.put("IC_EPO", lsCveEpo);
			datos.put("EPO_RELACIONADA", lsNombre);
			datos.put("REFERENCIA", lsCalificacion);
			datos.put("TIPO_TASA", lsCadenaTasa);
			datos.put("VALOR", Comunes.formatoDecimal(ldValorTotalTasa,5) );
			datos.put("PLAZO", lsDescripPlazo);
			datos.put("REL_MAT", "-");
			datos.put("PUNTOS", Double.toString (ldPuntos));						
			datos.put("FECHA", "");			
			datos.put("VALOR_TASA", Comunes.formatoDecimal(ldTotal,5) );
			
			int  rojo =0;
			List valorTasa = new ArrayList();
			if(proceso.equals("Procesar")) {
				for (int x= 0; x < datosTasa.size(); x++) {
					String   rango_plazo = datosPlazo.get(x).toString();	
					if(  rango_plazo.equals(lsDescripPlazo)) {
						valorTasa.add(datosTasa.get(x));
					}
				}
				//System.out.println(ldTotal+"---valorTasa<---"+valorTasa);				
				for (int x= 0; x < valorTasa.size(); x++) {
					double dTasa_aplicarV = Double.parseDouble((String)valorTasa.get(x).toString()) ;
					if (ldTotal < dTasa_aplicarV  ){
						rojo++;													
					}				
				}
			}	
			
			//System.out.println(datosTasa.size()+"---<---"+rojo);
			if(proceso.equals("Procesar") && rojo== valorTasa.size() ){
				datos.put("ROJO","S");	
				tasasRojo++;
			}else{
				datos.put("ROJO","N");
			}
			
			datos.put("TASA_SELEC", "P" );	
			datos.put("LD_TOTAL", Double.toString (ldTotal));	
			datos.put("IC_TASA", ic_tasa);
			datos.put("LS_PLAZO", lsPlazo);			
			registros.add(datos);
		}// for
		
		if(tasasRojo>0) {
			dTasa_aplicar = "S";
		}
		
			
	}else  if(tipoTasa.equals("NG")) { //Negociables
	
		lovDatos = BeanTasas.procesarTasaNegociada(ic_if, ic_moneda, ic_epo, ic_pyme, lstPlazo,ic_pyme);
	
		for (int i= 0; i < lovDatos.size(); i++) {
				lovRegistro = (Vector) lovDatos.elementAt(i);
				
				lsCveEpo  		= lovRegistro.elementAt(0).toString();
				lsNombre 		= lovRegistro.elementAt(1).toString();
				lsCalificacion 	= lovRegistro.elementAt(2).toString();
				lsCadenaTasa 	= lovRegistro.elementAt(3).toString();
				ldValorTotalTasa = ((Double) lovRegistro.elementAt(4)).doubleValue();
				lsValorTasaN 	= lovRegistro.elementAt(5).toString();
				lsFecha 		= lovRegistro.elementAt(6).toString();
				lsTemp 			= lovRegistro.elementAt(7).toString();
				lsPlazo			= lovRegistro.elementAt(8).toString(); 
				lsDescripPlazo = lovRegistro.elementAt(9).toString();				
				ic_tasa =(lovRegistro.elementAt(10)==null)?"0":lovRegistro.elementAt(10).toString();

				if(proceso.equals("Procesar")) {										
					lsValorTasaN = "";
				}else if (proceso.equals("Consulta")) {
					lsValorTasaN 	= lovRegistro.elementAt(5).toString();
				}
								
				datos = new HashMap();		
				datos.put("IC_EPO", lsCveEpo);
				datos.put("EPO_RELACIONADA", lsNombre);
				datos.put("REFERENCIA", lsCalificacion);
				datos.put("TIPO_TASA", lsCadenaTasa);
				datos.put("VALOR", Comunes.formatoDecimal(ldValorTotalTasa,5) );
				datos.put("PLAZO", lsDescripPlazo);
				datos.put("REL_MAT", "");
				datos.put("PUNTOS", Double.toString (ldPuntos) );
				datos.put("FECHA", lsFecha);
				datos.put("VALOR_TASA",  lsValorTasaN);		
				datos.put("TASA_SELEC", "NG" );	
				datos.put("LD_TOTAL", Double.toString (ldTotal));	
				datos.put("IC_TASA", ic_tasa);
				datos.put("LS_PLAZO", lsPlazo);	
				registros.add(datos);
		}	
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
	
	jsonObj = JSONObject.fromObject(consulta);	
	jsonObj.put("tipoTasa",tipoTasa);
	jsonObj.put("proceso",proceso);
	jsonObj.put("dlpuntos2",Double.toString (ldPuntos));
	jsonObj.put("ldTotal2",Double.toString (ldTotal));
	jsonObj.put("TasaAplicar",""+dTasa_aplicar);
	jsonObj.put("datosTasasAux",""+datosTasasAux);
	jsonObj.put("totalTasasAux",Double.toString (regis.size()));
	
	
	
	infoRegresar = jsonObj.toString();	

}else if (informacion.equals("EliminarTasas")   ) {	
	if(tipoTasa.equals("NG")) tipoTasa = "N";
	estaEliminar= BeanTasas.eliminarTasas(ic_if,ic_moneda, ic_pyme, tipoTasa, tasas, ValorTasas, strNombre, dlpuntos2, ldTotal2);

	if(estaEliminar.equals("E")){
	 mensajeEliminacion= "Las Tasas fueron Eliminadas";
	}
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("eliminacion",mensajeEliminacion);		
	infoRegresar  = jsonObj.toString();

}else if (informacion.equals("GuardarTasas")   ) {

	if(tipoTasa.equals("NG")) tipoTasa = "N";
	String folioCert = "",  _acuse = "", mensajeAutentificacion ="", fechaHoy  ="", horaHoy ="";
	String pkcs7 = request.getParameter("pkcs7");	
	String externContent = request.getParameter("textoFirmar");
	char getReceipt = 'Y';
	Seguridad s = new Seguridad();
	Acuse acuse = null;
	
	fechaHoy = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
	horaHoy= (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
	aplicaFloating = (request.getParameter("aplicaFloating") != null) ? request.getParameter("aplicaFloating") : "N";
		
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		 acuse = new Acuse(Acuse.ACUSE_IF,"1");
		folioCert = acuse.toString();		
		
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			_acuse = s.getAcuse();			
			
			BeanTasas.guardarTasas(ic_if, ic_moneda, ic_pyme, tipoTasa, tasas, ValorTasas,acuse.toString(), iNoUsuario,_acuse, iNoUsuario + " - " +strNombreUsuario, dlpuntos2, aplicaFloating);
			
			mensajeAutentificacion = "<b>La autentificación se llevó a cabo con éxito  <b>Recibo:"+_acuse+"</b>";
		
		}else { //autenticación fallida
			mensajeAutentificacion = "<b>La autentificación no se llevó  a cabo </b>";
		}
	} else { //autenticación fallida
		mensajeAutentificacion = "<b>La autentificación no se llevó a cabo </b>";
	}	
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensajeAutentificacion",mensajeAutentificacion);
	if(acuse !=null)jsonObj.put("acuse",acuse.formatear());	
	if(acuse ==null)jsonObj.put("acuse",acuse);	
	jsonObj.put("recibo",_acuse);
	jsonObj.put("acuse2",acuse.toString());
	jsonObj.put("fecha",fechaHoy);
	jsonObj.put("hora",horaHoy);
	jsonObj.put("usuario",iNoUsuario+" - "+strNombreUsuario);	
	jsonObj.put("ic_epo",ic_epo);
	jsonObj.put("ic_if",ic_if);
	jsonObj.put("ic_moneda",ic_moneda);
	jsonObj.put("ic_pyme",ic_pyme);
	jsonObj.put("tipoTasa",tipoTasa);
	jsonObj.put("lstPlazo",lstPlazo);
	infoRegresar  = jsonObj.toString();

}else if (informacion.equals("ConsultarAcuse")   ) {

	lovDatos = BeanTasas.consultaTasaNegociada(ic_epo,  ic_if,  ic_moneda,  ic_pyme ,  tipoTasa,  lstPlazo , acuse2  );
		
	for (int i= 0; i < lovDatos.size(); i++) {
		numRegistros++;
		lovRegistro = (Vector) lovDatos.elementAt(i);	
		String lsNombreEPO 		= lovRegistro.elementAt(0).toString();
		String lsNombrePYME  		= lovRegistro.elementAt(1).toString();
		lsCalificacion  		= lovRegistro.elementAt(2).toString();
		lsCadenaTasa  		= lovRegistro.elementAt(3).toString();
		lsPlazo  		= lovRegistro.elementAt(4).toString();
		String ldValorTotalTasas  		= lovRegistro.elementAt(5).toString();
		String ldValorTasa  		= lovRegistro.elementAt(6).toString();
		
		datos = new HashMap();		
		datos.put("EPO_RELACIONADA", lsNombreEPO);
		datos.put("REFERENCIA", lsCalificacion + operaFloating);
		datos.put("NOMBRE_PYME", lsNombrePYME);
		datos.put("TIPO_TASA", lsCadenaTasa);
		datos.put("PLAZO", lsPlazo);
		datos.put("VALOR", ldValorTotalTasas);
		datos.put("VALOR_TASA", ldValorTasa);
		registros.add(datos);				
	
	}

	consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar  = jsonObj.toString();
	
	
}else if (informacion.equals("ArchivoPDF")   ) {
	
	lovDatos = BeanTasas.consultaTasaNegociada(ic_epo,  ic_if,  ic_moneda,  ic_pyme ,  tipoTasa,  lstPlazo , acuse2  );


	nombreArchivo = archivo.nombreArchivo()+".pdf";	
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
			
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
											session.getAttribute("iNoNafinElectronico").toString(),
											(String)session.getAttribute("sesExterno"),
											(String) session.getAttribute("strNombre"),
											(String) session.getAttribute("strNombreUsuario"),
											(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				
	pdfDoc.addText("   ","formasB",ComunesPDF.CENTER);
				
	pdfDoc.setTable(2,65);
	pdfDoc.setCell("Recibo Número :   "+recibo,"celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de Acuse","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(acuse3,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de Autorización","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(fechaCarga,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Hora de Autorización","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(horaCarga,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Usuario de Captura","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(iNoUsuario+" - "+strNombreUsuario,"formas",ComunesPDF.CENTER);
	pdfDoc.addTable();
				
	pdfDoc.setTable(7,90);
	pdfDoc.setCell("Epo Relacionada","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Nombre de la PyME","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo Tasa","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Valor Tasa","celda01",ComunesPDF.CENTER);
	
	for (int i= 0; i < lovDatos.size(); i++) {
		numRegistros++;
		lovRegistro = (Vector) lovDatos.elementAt(i);	
		String lsNombreEPO 		= lovRegistro.elementAt(0).toString();
		String lsNombrePYME  		= lovRegistro.elementAt(1).toString();
		lsCalificacion  		= lovRegistro.elementAt(2).toString();
		lsCadenaTasa  		= lovRegistro.elementAt(3).toString();
		lsPlazo  		= lovRegistro.elementAt(4).toString();
		String ldValorTotalTasas  		= lovRegistro.elementAt(5).toString();
		String ldValorTasa  		= lovRegistro.elementAt(6).toString();
				
		pdfDoc.setCell(lsNombreEPO,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsNombrePYME,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsCalificacion+operaFloating,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsCadenaTasa,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsPlazo,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(Comunes.formatoDecimal(ldValorTotalTasas,5),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(Comunes.formatoDecimal(ldValorTasa,5),"formas",ComunesPDF.CENTER);
	
	}
	pdfDoc.addTable();
	pdfDoc.endDocument();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar  = jsonObj.toString();


}else if (informacion.equals("ArchivoCSV")   ) {

	lovDatos = BeanTasas.consultaTasaNegociada(ic_epo,  ic_if,  ic_moneda,  ic_pyme ,  tipoTasa,  lstPlazo , acuse2  );

	contenidoArchivo.append(
		"Recibo Número:,"+recibo+
		"\nNúmero de Acuse,"+acuse3+
		"\nFecha de Autorización,"+fechaCarga+
		"\nHora de Autorización,"+horaCarga+
		"\nUsuario de Captura,"+strNombreUsuario.replace(',',' ')+
		"\n,\n,\n"+
		"Epo Relacionada,Nombre de la PyME,Referencia,Tipo Tasa,Plazo,Valor,Valor Tasa\n");

	for (int i= 0; i < lovDatos.size(); i++) {
		numRegistros++;
		lovRegistro = (Vector) lovDatos.elementAt(i);	
		String lsNombreEPO 		= lovRegistro.elementAt(0).toString();
		String lsNombrePYME  		= lovRegistro.elementAt(1).toString();
		lsCalificacion  		= lovRegistro.elementAt(2).toString()+ operaFloating;
		lsCadenaTasa  		= lovRegistro.elementAt(3).toString();
		lsPlazo  		= lovRegistro.elementAt(4).toString();
		String ldValorTotalTasas  		= lovRegistro.elementAt(5).toString();
		String ldValorTasa  		= lovRegistro.elementAt(6).toString();
				
		contenidoArchivo.append(
			lsNombreEPO.replace(',',' ')+","+
			lsNombrePYME.replace(',',' ')+","+
			lsCalificacion.replace(',',' ')+","+
			lsCadenaTasa.replace(',',' ')+","+
			 "="+'"'+ lsPlazo+'"'+","+	
			Comunes.formatoDecimal(ldValorTotalTasas,5).replace(',',' ')+","+
			Comunes.formatoDecimal(ldValorTasa,5).replace(',',' ')+"\n");
		}
		
		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	
		infoRegresar  = jsonObj.toString();
}
%>
<%=infoRegresar%>

