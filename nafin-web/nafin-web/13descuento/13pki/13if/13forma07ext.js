Ext.onReady(function() {

//_------- -------------------------- HANDLERS -------------------------------
	var  doctosSeleccionados  = [];
	var estatusDoctos  = [];
	var doctosFondeo  = [];
   
   
   // # Formulario Env�o por POST  --> Se agreg�: 01/11/2012 
   var formaDinamica = new Ext.form.FormPanel({
      xtype:            'form',
      id:               'formaDinamica',
      url:              '13forma07ext.jsp',
      standardSubmit:   true,
      frame:            false,
      border:           false,
      style:            'border:#fff 0px;',
//      hidden:           true,
      items:            [{ xtype: 'hidden', name:'pantalla', value:'PreAcuse' }]
   }); // #
		
	var cambioEstatus =  function(opts, success, response) {
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
      var formaDinamicaVar = Ext.getCmp('formaDinamica'); // --> Se agreg�: 01/11/2012
      //formaDinamicaVar.hide();
		
		var  notas_Credito_multiple1 =   Ext.getCmp("notas_Credito_multiple1").getValue();
		var  ct_cambio_motivo =   Ext.getCmp("ct_cambio_motivo").getValue();
		var  ic_epo =   Ext.getCmp("ic_epo1").getValue();
		
		doctosSeleccionados  = [];
		estatusDoctos  = [];
		doctosFondeo  = [];
		
		store.each(function(record) {		
			
			if(notas_Credito_multiple1=='S' && record.data['ESTATUS_ASIGNAR']=='OP' ) {				
				doctosFondeo.push(record.data['ESTATUS_ASIGNAR']);	
            
            // # --> Se agreg�: 01/11/2012
            formaDinamicaVar.add({ xtype:'hidden', name:'estatusDoctos', value:record.data['ESTATUS_ASIGNAR'] });
            // #
			}else {
				if(record.data['ESTATUS_ASIGNAR']!=''){					
				estatusDoctos.push(record.data['ESTATUS_ASIGNAR']);
				doctosSeleccionados.push(record.data['IC_DOCUMENTO']);
            
            formaDinamicaVar.add({ xtype:'hidden', name:'estatusDoctos', value:record.data['ESTATUS_ASIGNAR'] });
            formaDinamicaVar.add({ xtype:'hidden', name:'doctosSeleccionados', value:record.data['IC_DOCUMENTO'] });
				}
			}
		});
	
		if (doctosFondeo != '')	{
			Ext.MessageBox.alert('Mensaje','El cambio de estatus a "Operado con Fondeo Propio" no aplica para EPOs que operan con Notas de Cr�dito a varios documentos.');
			return true ;
		}
				
		if (estatusDoctos == '')	{
			Ext.MessageBox.alert('Mensaje','Por favor, es necesario seleccionar al menos un registro.');
			return;
		}
      
		// # Se agrega componentes al contenedor de la forma para ser enviado por POST --> Se agreg�: 01/11/2012
      formaDinamicaVar.add({ xtype:'hidden', name:'ct_cambio_motivo', value:ct_cambio_motivo });
      formaDinamicaVar.add({ xtype:'hidden', name:'ic_epo', value:ic_epo });
      //formaDinamicaVar.add({ xtype:'textarea', name:'pkcs7', value:pkcs7 });
      //formaDinamicaVar.add({ xtype:'textarea', name:'textoFirmar', textoFirmar:pkcs7 });
      formaDinamicaVar.doLayout();      
      formaDinamicaVar.getForm().submit(); // --> Se agreg�: 01/11/2012
	}
   
	var catalogoEstatusAsignar = new Ext.data.JsonStore({
		id: 'catalogoEstatusAsignar',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatusAsignar'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	//para la creacion del combo en el grid
	var comboEstatusAsignar = new Ext.form.ComboBox({  
      triggerAction : 'all',  
      displayField : 'descripcion',  
      valueField : 'clave', 
      typeAhead: true,
      width: 250,
      minChars : 1,
      allowBlank: true,
      store :catalogoEstatusAsignar
	});

	
	Ext.util.Format.comboRenderer = function(comboEstatusAsignar){	
		return function(value,metadata,registro,rowIndex,colIndex,store){				
			var valor = registro.data['ESTATUS_ASIGNAR'];					
			if(valor !=''){
				var record = comboEstatusAsignar.findRecord(comboEstatusAsignar.valueField, value);
				return record ? record.get(comboEstatusAsignar.displayField) : comboEstatusAsignar.valueNotFoundText;
			} 
			if(valor !=''){
				return valor;
			}
         if(valor==''){
            return '--- Conservar estatus actual ---';
         }
		}
	}	
	
	
	var fpCausa = new Ext.form.FormPanel({
		id: 'formaCausa',
		width: 700,
		style: ' margin:0 auto;',
		hidden:true,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 50,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [			
			{
				xtype:'textarea',
				id:	'ct_cambio_motivo',
				name:	'ct_cambio_motivo',
				height:	40,
				width:	490,
				maxLength:260,
				fieldLabel:'Causa',
				allowBlank: false,
				enableKeyEvents: true,
				listeners:{
					'keydown':	function(txtA){
									if (	!Ext.isEmpty(txtA.getValue())	){
										var numero = (txtA.getValue().length);
										Ext.getCmp('contador').setValue(260-numero);
										if (	Ext.getCmp('contador').getValue() < 0	) {
											var cadena = (txtA.getValue()).substring(0,260);
											txtA.setValue(cadena);
											Ext.getCmp('contador').setValue(0);
										}
									}
								},
					'keyup':	function(txtA){
									if (	Ext.isEmpty(txtA.getValue())	){
										Ext.getCmp('contador').reset();
										fpCausa.getForm().reset();
									}
								},
					'blur':	function(txtA){
									if (	Ext.isEmpty(txtA.getValue())	){
										fpCausa.getForm().reset();
									}									
								}
				}
			},
			{
				xtype: 'compositefield',
				combineErrors: false,
				msgTarget: 'side',
				items: [
					{
						xtype:'displayfield',	id:'disEspacio',	width: 1,	text:''
					},{
						xtype:'displayfield',	id:'disChar',	width: 120,	value:'Caracteres Restantes:'
					},{
						xtype: 'numberfield',	maxLength:3,	value:	260,	width: 30,	id:	'contador',	name:	'contador',	readOnly: true
					}
				]
			}			
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Cambiar Estatus ',
				id: 'btnCambia',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: cambioEstatus	
			}
		]
	});
	

	
	var procesarTotalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}			
							
			//edito el titulo de la columna
			var cm = gridTotales.getColumnModel();			
			var jsonData = store.reader.jsonData;			
		
			var ic_estatus1 = Ext.getCmp('ic_estatus1').getValue();
			if(ic_estatus1 ==23){
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_RECURSOS'), true);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTA_MONTO_DESC'), true);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_INTERES'), true);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_OPERAR'), true);				
			}else {
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_RECURSOS'), false);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTA_MONTO_DESC'), false);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_INTERES'), false);
				gridTotales.getColumnModel().setHidden(cm.findColumnIndex('TOTAL_MONTO_OPERAR'), false);
			}
		
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
							
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
			var gridTotales = Ext.getCmp('gridTotales');	
			
			Ext.getCmp("notas_Credito_multiple1").setValue(jsonData.manejaNotasDeCreditoMultiples);
			
			if(jsonData.ic_estatus=="3" || jsonData.ic_estatus=="24"  ||  jsonData.ic_estatus=="" ){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('POR_DESC'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('POR_GARANTIA'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESC'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INTERES'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPERAR'), false);			
			}	
			if(jsonData.ic_estatus=="23" ){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('POR_DESC'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('POR_GARANTIA'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_DESC'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_INTERES'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_OPERAR'), true);			
			}	
			
			var formaCausa = Ext.getCmp('formaCausa');	
			if(store.getTotalCount() > 0) {	
				formaCausa.show();
				gridTotales.show();
				el.unmask();					
			} else {	
				formaCausa.hide();
				gridTotales.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaTotales'
		},
		hidden: true,
		fields: [	
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCTOS'},			
			{name: 'TOTAL_MONTO'},	
			{name: 'TOTAL_RECURSOS'},	
			{name: 'TOTA_MONTO_DESC'},	
			{name: 'TOTAL_MONTO_INTERES'},	
			{name: 'TOTAL_MONTO_OPERAR'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}			
	});
			
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'',
		hidden: true,
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right'	
			},	
			{							
				header : 'Total Documentos',
				tooltip: 'Total Documentos',
				dataIndex : 'TOTAL_DOCTOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},		
			
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'TOTAL_MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Recursos en garantia',
				tooltip: 'Recursos en garantia ',
				dataIndex : 'TOTAL_RECURSOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'TOTA_MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'TOTAL_MONTO_INTERES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'TOTAL_MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 150,
		width: 943,			
		frame: false
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [	
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_PYME'},	
			{name: 'NUM_DOCTO'},
			{name: 'F_VENCIMIENTO'},			
			{name: 'ESTATUS'},
			{name: 'IC_ESTATUS'},			
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'POR_DESC'},
			{name: 'POR_GARANTIA'},
			{name: 'MONTO_DESC'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'ESTATUS_ASIGNAR'},
			{name: 'AUTO_OPER_SF'},			
			{name: 'REFERENCIA'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Cambio de Estatus',
		hidden: true,
		clicksToEdit: 1,
		columns: [
			{							
				header : 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Nombre PyME/Cedente',
				tooltip: 'Nombre PyME/Cedente',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'F_VENCIMIENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'POR_DESC',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{							
				header : 'Recurso en garantia',
				tooltip: 'Recurso en garantia',
				dataIndex : 'POR_GARANTIA',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESC',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'MONTO_INTERES',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},									
			{
				header : 'Estatus por asignar',
				tooltip: 'Estatus por asignar',
				dataIndex: 'ESTATUS_ASIGNAR',
				sortable: true,
				resizable: true,
				width: 200,					
				align: 'center',				
				editor:comboEstatusAsignar,
				renderer: Ext.util.Format.comboRenderer(comboEstatusAsignar)	
			},
			{
				header : '<center>Referencia<center>',
				tooltip: 'Referencia',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true,
				width: 200,					
				align: 'left'
			}				
		],					
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 943,
		align: 'center',
		frame: false,
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
				var estatus1;
				var estatus2;
				
				if(campo == 'ESTATUS_ASIGNAR'){
				
					if(record.data['IC_ESTATUS']==24 &&  record.data['AUTO_OPER_SF'] =='S'){
						estatus1 = 'OP'
					}
					if(record.data['IC_ESTATUS']==24 ){
						estatus2 = 'SP'
					}
					
					catalogoEstatusAsignar.load({
						params: {
							informacion: 'catalogoEstatusAsignar',
							estatus1:estatus1, 
							estatus2:estatus2
						}						
					});									
					return true; // es editable 					
				}				
			}	
		}
		
	});
	
//-------------------------------- PRINCIPAL -----------------------------------


	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		
	var catalogoEstatus = new Ext.data.JsonStore({
		id: 'catalogoEstatus',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
		
		
	var procesarStoreBusqAvanzPyme= function(store, records, oprion){
		if(store.getTotalCount()==0 ){
			Ext.MessageBox.alert("Mensaje","No existe informaci�n en los criterios determinados" );
		}
	}	
	
	
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarStoreBusqAvanzPyme,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

	var successAjaxFn = function(opts, success, response) { 

	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
}

	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'noPyme',
			id: 'noPyme1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);		
							var ic_epo = Ext.getCmp("ic_epo1").getValue();	
							
							storeBusqAvanzPyme.load({
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
										ic_epo:ic_epo						
									})																	
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
		
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});
	
	
	
	var elementosForma = [
		{
			xtype: 'textfield',
			name: 'notas_Credito_multiple',
			id: 'notas_Credito_multiple1',
			fieldLabel: 'notas_Credito_multiple',					
			allowBlank: true,
			hidden: true,
			startDay: 0,
			maxLength: 15,	
			width: 100,
			msgTarget: 'side',						
			margins: '0 20 0 0'			
		},				
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione  EPO...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO Relacionada',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero de Documento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [		
				{
					xtype: 'textfield',
					name: 'ig_numero_docto',
					id: 'ig_numero_docto',
					fieldLabel: 'N�mero de Documento',					
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 200,
					msgTarget: 'side',						
					margins: '0 20 0 0'			
				}	
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [			
				{
					xtype: 'datefield',
					name: 'df_fecha_venc',
					id: 'df_fecha_venc1',
					fieldLabel: ' * ',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha',					
					margins: '0 20 0 0'
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar Moneda...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',
					fieldLabel: 'Moneda',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoMoneda
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto entre',
			combineErrors: false,
			msgTarget: 'side',
			width: 300,
			items: [		
				{
					width:         100,
					//maxLength: 15,//ver el tama�o maximo del numero en BD para colocar este igual
					xtype:         'bigdecimal',
					name:          'fn_montoMin',
					id:            'fn_montoMin1',
					fieldLabel:    'Monto entre',
					msgTarget:     'side',
					margins:       '0 20 0 0',
					maxText:       'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
					maxValue:      '9999999999999.99',
					format:        '0000000000000.00',
					vtype:         'rangoValor',
					campoFinValor: 'fn_montoMax1',
					allowDecimals: true,
					allowNegative: false,
					allowBlank:    true
				},
				{
					xtype: 'displayfield',
					value: ' y',
					width: 50
				},
				{
					//maxLength: 15,//ver el tama�o maximo del numero en BD para colocar este igual
					width:            100,
					xtype:            'bigdecimal',
					name:             'fn_montoMax',
					id:               'fn_montoMax1',
					msgTarget:        'side',
					margins:          '0 20 0 0',
					maxText:          'El tama�o m�ximo del campos es de 17 enteros 2 decimales',
					maxValue:         '9999999999999.99',
					format:           '0000000000000.00',
					vtype:            'rangoValor',
					campoInicioValor: 'fn_montoMin1',
					allowDecimals:    true,
					allowNegative:    false,
					allowBlank:       true
				}	
				
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Estatus',
			combineErrors: false,
			msgTarget: 'side',
			width: 520,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_estatus',
					id: 'ic_estatus1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar Estatus...',			
					valueField: 'clave',
					hiddenName : 'ic_estatus',
					fieldLabel: 'Estatus',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoEstatus
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero Nafin Electr�nico',
			combineErrors: false,
			msgTarget: 'side',			
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){
						
							var ic_epo = Ext.getCmp("ic_epo1");
							if (Ext.isEmpty(ic_epo.getValue()) ){
								ic_epo.markInvalid('Debe de Capturar una EPO.');
								return;
							}
						 // Petici�n b�sica  
							Ext.Ajax.request({  
								url: '13forma07ext.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
								params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue()
								}  
							}); 	
						}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 300,
					disabled:true,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
				xtype: 'button',
				text: 'B�squeda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
					}
				}
			}
			]
		}
	];


	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 800,
		title: 'Cambio de Estatus',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,		
				handler: function(boton, evento) {
				
					var ic_epo = Ext.getCmp("ic_epo1");
					var fn_montoMin1 = Ext.getCmp("fn_montoMin1");
					var fn_montoMax1 = Ext.getCmp("fn_montoMax1");
			
					if (Ext.isEmpty(ic_epo.getValue()) ){
						ic_epo.markInvalid('Debe de Capturar una EPO.');
						return;
					}
					if (!Ext.isEmpty(fn_montoMin1.getValue())   && Ext.isEmpty(fn_montoMax1.getValue())   ){
						fn_montoMax1.markInvalid('Debe capturar el monto final.');
						return;
					}
					if (!Ext.isEmpty(fn_montoMin1.getValue())   && !Ext.isEmpty(fn_montoMax1.getValue())   ){
					
						if(parseFloat(fn_montoMin1.getValue(),10)>parseFloat(fn_montoMax1.getValue(),10)){
							fn_montoMax1.markInvalid('El monto final debe ser mayor o igual al inicial.');
							return;
						}					
					}
						
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar'
						})
					});	
						
					consultaDataTotales.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'ConsultaTotales'
						})
					});					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13forma07ext.jsp';
				}
			}
		]
	});
	

	
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20),
			fpCausa,
			NE.util.getEspaciador(20),
			NE.util.getEspaciador(20),
			formaDinamica
		]
	});

	
//-------------------------------- ----------------- -----------------------------------
	
	catalogoEPO.load();
	catalogoMoneda.load();
	catalogoEstatus.load();
	//catalogoEstatusAsignar.load();
	
});