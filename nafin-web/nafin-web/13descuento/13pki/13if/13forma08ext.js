Ext.onReady(function() {
	////Agregado Fodea 17	
	Opera_Fideicomiso = "";
	var datosTasasAux;
	var totalTasasAux;
	////
	Ext.form.Field.prototype.msgTarget = 'side';
	var tasas = [];
	var valorTasa  = [];

	var procesarPYME =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			jsonData = Ext.util.JSON.decode(response.responseText);
			
			if(jsonData.sihayTasas =='S'){
				Ext.MessageBox.alert('Mensaje','El Proveedor seleccionado opera con Oferta de Tasas por Montos, es necesario borrar su parametrizaci�n para poder aplicar Tasa Preferencial');
			}
			
			Ext.getCmp('suceptibleFloating').enable();
			Ext.getCmp('aplicaFloating2').setValue(false);
			Ext.getCmp('aplicaFloating1').setValue(false);
			if(jsonData.opeSucepFloating ==='N')  {				
				Ext.getCmp('suceptibleFloating').disable();
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarFideicomiso = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			Opera_Fideicomiso = info.OPERA;
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}

	
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var mensajeAutentificacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutentificacion',							
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		layoutConfig: {
			columns: 1
		},
		items: [					
			{
				xtype: 'displayfield',
				id:	'autentificac',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}		
			},
			{ 	xtype: 'displayfield', 	hidden: true, id: 'acuse', 	value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'acuse3', 	value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'fechaCarga', 	value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, id: 'horaCarga', 	value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'recibo', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'ic_epo', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'ic_if', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'ic_moneda', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'ic_pyme', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'tipoTasa', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'lstPlazo', value: '' }	
		]
	});
	

	// respuesta de Guardar de  tasas Prenegociables /Negociables 
	function procesarSuccessFailureGuardar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			
			radioTipoTasa.hide();
			fp.hide();
			gridConsulta.hide();
			
			var mensajeAutentificacion = Ext.getCmp('mensajeAutentificacion');		
			
			Ext.getCmp("acuse").setValue(jsonData.acuse2);
			Ext.getCmp("acuse3").setValue(jsonData.acuse);
			Ext.getCmp("fechaCarga").setValue(jsonData.fecha);
			Ext.getCmp("horaCarga").setValue(jsonData.hora);
			Ext.getCmp("recibo").setValue(jsonData.recibo);
			Ext.getCmp("ic_epo").setValue(jsonData.ic_epo);
			Ext.getCmp("ic_if").setValue(jsonData.ic_if);
			Ext.getCmp("ic_moneda").setValue(jsonData.ic_moneda);
			Ext.getCmp("ic_pyme").setValue(jsonData.ic_pyme);
			Ext.getCmp("tipoTasa").setValue(jsonData.tipoTasa);
			Ext.getCmp("lstPlazo").setValue(jsonData.lstPlazo);
			
			if(jsonData.acuse =='N'){			
				
				Ext.getCmp("autentificac").setValue(jsonData.mensajeAutentificacion);
				mensajeAutentificacion.show();
			}
			if(jsonData.acuse !='N'){
				var acuseCifras = [
					['N�mero de Acuse', jsonData.acuse],
					['Fecha de Autorizaci�n ', jsonData.fecha],
					['Hora de Autorizaci�n', jsonData.hora],
					['Usuario de Autorizaci�n ', jsonData.usuario]
				];
			
				storeCifrasData.loadData(acuseCifras);	
				gridCifrasControl.show();
								
				consultaAcuseData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarAcuse',
						tipoTasa:tipoTasa,
						acuse2:jsonData.acuse2,
						fechaCarga:jsonData.fecha,
						horaCarga:jsonData.hora,
						ic_epo:jsonData.ic_epo,
						ic_if:jsonData.ic_if,
						ic_moneda:jsonData.ic_moneda,
						ic_pyme:jsonData.ic_pyme,
						tipoTasa:jsonData.tipoTasa,
						lstPlazo:jsonData.lstPlazo
					})
				});
				
				gridAcuse.show();
				
				gridCifrasControl.setTitle('Recibo N�mero    '+jsonData.recibo);		
			}				
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var confirmar = function(pkcs7, vtextoFirmar, vtipoTasa, vdlpuntos2, vldTotal2, vtasas, vvalorTasa){
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}				
		Ext.Ajax.request({
			url : '13forma08ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				pkcs7:pkcs7,
				textoFirmar:vtextoFirmar,				
				tipoTasa:vtipoTasa,
				dlpuntos2:vdlpuntos2,
				ldTotal2:vldTotal2,
				tasas:vtasas, 
				valorTasa:vvalorTasa,							
				informacion: 'GuardarTasas'
				}),				
			callback: procesarSuccessFailureGuardar
		});
	}

	// proceso para Guardar tasas Prenegociables /Negociables 
	var procesarGuardar= function() 	{
		var textoFirmar ='';
		var tipoTasa;
		var  total =0;
		var  totalSe =0;
		var tipoTasa1 = Ext.getCmp("tipoTasa1");	
		var tipoTasa2 = Ext.getCmp("tipoTasa2");		
		if(tipoTasa1.getValue()==true){ tipoTasa ='P'; }
		if(tipoTasa2.getValue()==true) { tipoTasa ='N'; }
		var dlpuntos2 = 	Ext.getCmp("dlpuntos2").getValue();
		var ldTotal2 =	Ext.getCmp("ldTotal2").getValue();
				
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		if(tipoTasa =='P'){
			textoFirmar='EPO Relacionada|Referencia|Tipo Tasa|Valor|Rel. Mat|Puntos|Valor Tasa \n';
		}
		if(tipoTasa =='N'){
			textoFirmar='EPO Relacionada|Referencia|Tipo Tasa|Valor|Fecha|Valor Tasa \n';
		}		
		
		 tasas = [];
		 valorTasa  = [];
	
		
		store.each(function(record) {	
			total++;
			if(tipoTasa =='P'){
				textoFirmar += record.data['EPO_RELACIONADA'] +"|"+ record.data['REFERENCIA'] +"|"+ record.data['TIPO_TASA'] +"|"+ 
						record.data['VALOR'] +"|"+ record.data['REL_MAT'] +"|"+ record.data['PUNTOS'] +"|"+record.data['VALOR_TASA']+"\n";
				tasas.push(record.data['IC_EPO'] +'|'+record.data['PUNTOS'] +'|'+ record.data['LS_PLAZO']+'|'+ record.data['IC_TASA']+'|'+ record.data['LD_TOTAL']);
			}
		
			if(tipoTasa =='N'){
				textoFirmar += record.data['EPO_RELACIONADA'] +"|"+ record.data['REFERENCIA'] +"|"+ record.data['TIPO_TASA'] +"|"+ 
						record.data['VALOR'] +"|"+ record.data['FECHA'] +"|"+record.data['VALOR_TASA'] +"\n";
				tasas.push(record.data['IC_EPO'] +'|'+record.data['FECHA'] +'|'+ record.data['LS_PLAZO']+'|'+ record.data['IC_TASA']+'|'+ record.data['VALOR']);
			}	
						
			if(record.data['VALOR_TASA']!=''){
				totalSe++
				valorTasa.push(record.data['VALOR_TASA']);
			}			
		});
				
		if(total !=totalSe) {
			Ext.MessageBox.alert('Mensaje','Debes de especificar el valor de todas las tasas negociadas');
			return false;	
		}	
		
		NE.util.obtenerPKCS7(confirmar, textoFirmar, tipoTasa, dlpuntos2, ldTotal2, tasas, valorTasa );	
		
	}

	// respuesta de eliminaci�n de  tasas Prenegociables /Negociables 
	function procesarSuccessFailureEliminar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var param = Ext.util.JSON.decode(response.responseText);
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				Ext.MessageBox.alert('Mensaje',param.eliminacion);	
				var gridConsulta = Ext.getCmp('gridConsulta');
				gridConsulta.hide();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	// proceso para eliminar  tasas Prenegociables /Negociables 
	var procesarEliminar= function() 	{
		var tipoTasa1 = Ext.getCmp("tipoTasa1");	
		var tipoTasa2 = Ext.getCmp("tipoTasa2");
		var tipoTasa;
		var mensaje;
			
		if(tipoTasa1.getValue()==true) {mensaje	 ='Preferenciales'; tipoTasa ='P'; }
		if(tipoTasa2.getValue()==true) {  mensaje	 ='Negociadas'; tipoTasa ='N'; }
		var dlpuntos2 = 	Ext.getCmp("dlpuntos2").getValue();
		var ldTotal2 =	Ext.getCmp("ldTotal2").getValue();
		
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
			
		if(!confirm('�Esta Seguro de querer eliminar las Tasas'+mensaje +' ?' )) {
			return;			
		}else {	
			 tasas = [];
			valorTasa  = [];
			store.each(function(record) {	
				
				if(tipoTasa =='P'){
					tasas.push(record.data['IC_EPO'] +'|'+record.data['PUNTOS'] +'|'+ record.data['LS_PLAZO']+'|'+ record.data['IC_TASA']+'|'+ record.data['LD_TOTAL']);
				}	
				if(tipoTasa =='N'){
					tasas.push(record.data['IC_EPO'] +'|'+record.data['FECHA'] +'|'+ record.data['LS_PLAZO']+'|'+ record.data['IC_TASA']+'|'+ record.data['VALOR']);
				}
				valorTasa.push(record.data['VALOR_TASA']);			
			});
			
			Ext.Ajax.request({
				url : '13forma08ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'EliminarTasas',
					tipoTasa:tipoTasa,
					dlpuntos2:dlpuntos2,
					ldTotal2:ldTotal2,
					tasas:tasas, 
					valorTasa:valorTasa
				}),				
				callback: procesarSuccessFailureEliminar
			});	
		}
		
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
							
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();	
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
			var btnEliminarPreferenciales = Ext.getCmp('btnEliminarPreferenciales');	
			var btnEliminarNegociables = Ext.getCmp('btnEliminarNegociables');	
			var btnGuardar = Ext.getCmp('btnGuardar');	
			var btnCancelar = Ext.getCmp('btnCancelar');	
			
			Ext.getCmp("dlpuntos2").setValue(jsonData.dlpuntos2);
			Ext.getCmp("ldTotal2").setValue(jsonData.ldTotal2);

			if(jsonData.tipoTasa=='P'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('REL_MAT'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PUNTOS'), false);
			}
			if(jsonData.tipoTasa=='NG'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('REL_MAT'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PUNTOS'), true);
			}
						
			if(jsonData.proceso=='Procesar' && (  jsonData.tipoTasa=='P'  ||   jsonData.tipoTasa=='NG' ) ){
				 btnEliminarNegociables.hide();
				 btnEliminarPreferenciales.hide();			
			}
			if(jsonData.proceso=='Consulta'){
				if(jsonData.tipoTasa=='P') {
					btnEliminarPreferenciales.show();	
					btnEliminarNegociables.hide();				
				}else  if(jsonData.tipoTasa=='NG') {				
					btnEliminarPreferenciales.hide();	
					btnEliminarNegociables.show();	
				}
			}		
			
			datosTasasAux = jsonData.datosTasasAux;
			totalTasasAux = jsonData.totalTasasAux;
			
				
			if(store.getTotalCount() > 0) {	
				el.unmask();				
				if(jsonData.TasaAplicar="S" && Opera_Fideicomiso == 'S'){
					Ext.getCmp('lblTasas').show();
					btnGuardar.disable();
				}else{
					Ext.getCmp('lblTasas').hide();
					btnGuardar.enable();
				}
				btnCancelar.enable();				
			} else {		
				btnEliminarNegociables.hide();
				btnEliminarPreferenciales.hide();	
				btnGuardar.disable();
				btnCancelar.disable();
				Ext.getCmp('lblTasas').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		height: 150,
		style: 'margin:0 auto;',		
		frame: true
	});
	
	
	var consultaAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma08ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarAcuse'
		},
		hidden: true,
		fields: [	
		
			{name: 'EPO_RELACIONADA'},	
			{name: 'REFERENCIA'},
			{name: 'NOMBRE_PYME'},			
			{name: 'TIPO_TASA'},	
			{name: 'PLAZO'},				
			{name: 'VALOR'},				
			{name: 'VALOR_TASA'}
				
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {				
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);						
											
					}
				}
			}			
	});
	
	
	var gridAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridAcuse',				
		store: consultaAcuseData,	
		style: 'margin:0 auto;',
		title:'Tasas Preferenciales/Negociadas',
		hidden: true,
		clicksToEdit: 1,
		columns: [
			{							
				header : 'EPO Relacionada',
				tooltip: 'EPO_RELACIONADA',
				dataIndex : 'EPO_RELACIONADA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Nombre de la PYME',
				tooltip: 'Nombre de la PYME',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Referencia',
				tooltip: 'REFERENCIA',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo de Tasa',
				tooltip: 'TIPO_TASA',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Plazo',
				tooltip: 'PLAZO',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Valor',
				tooltip: 'Valor',
				dataIndex : 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{							
				header : 'Valor Tasa',
				tooltip: 'Valor Tasa',
				dataIndex : 'VALOR_TASA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'	
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {
				
					var acuse = Ext.getCmp("acuse").getValue();
					var fechaCarga = Ext.getCmp("fechaCarga").getValue();
					var horaCarga = Ext.getCmp("horaCarga").getValue();
					var recibo = Ext.getCmp("recibo").getValue();
					var ic_epo = Ext.getCmp("ic_epo").getValue();
					var ic_if = Ext.getCmp("ic_if").getValue();
					var ic_moneda = Ext.getCmp("ic_moneda").getValue();
					var ic_pyme = Ext.getCmp("ic_pyme").getValue();
					var tipoTasa = Ext.getCmp("tipoTasa").getValue();
					var lstPlazo = Ext.getCmp("lstPlazo").getValue();
					var acuse3 = Ext.getCmp("acuse3").getValue();						
					var aplicaFloating = Ext.getCmp("aplicaFloating1").getValue();		
					if(aplicaFloating===true){
						aplicaFloating ='S';
					}					
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13forma08ext.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								acuse2:acuse,
								acuse3:acuse3,
								fechaCarga:fechaCarga,
								horaCarga:horaCarga,
								recibo:recibo,
								ic_epo:ic_epo,
								ic_if:ic_if,
								ic_moneda:ic_moneda,
								ic_pyme:ic_pyme,
								tipoTasa:tipoTasa,
								lstPlazo:lstPlazo,
								aplicaFloating:aplicaFloating
							}					
							,callback: procesarGenerarPDF
						});					
					}
				},				
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					tooltip:	'Bajar Archivo',
					id: 'btnBajarPDF',
					hidden: true
				},	
				'-',
				{
					xtype: 'button',
					text: 'Generar CSV',
					tooltip:	'Generar CSV',
					id: 'btnGenerarCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
					
					var acuse = Ext.getCmp("acuse").getValue();
					var fechaCarga = Ext.getCmp("fechaCarga").getValue();
					var horaCarga = Ext.getCmp("horaCarga").getValue();
					var recibo = Ext.getCmp("recibo").getValue();
					var ic_epo = Ext.getCmp("ic_epo").getValue();
					var ic_if = Ext.getCmp("ic_if").getValue();
					var ic_moneda = Ext.getCmp("ic_moneda").getValue();
					var ic_pyme = Ext.getCmp("ic_pyme").getValue();
					var tipoTasa = Ext.getCmp("tipoTasa").getValue();
					var lstPlazo = Ext.getCmp("lstPlazo").getValue();
					var acuse3 = Ext.getCmp("acuse3").getValue();	
					var aplicaFloating = Ext.getCmp("aplicaFloating1").getValue();	
					if(aplicaFloating===true){
						aplicaFloating ='S';
					}						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13forma08ext.data.jsp',
							params: {
								informacion:'ArchivoCSV',
								acuse2:acuse,
								acuse3:acuse3,
								fechaCarga:fechaCarga,
								horaCarga:horaCarga,
								recibo:recibo,
								ic_epo:ic_epo,
								ic_if:ic_if,
								ic_moneda:ic_moneda,
								ic_pyme:ic_pyme,
								tipoTasa:tipoTasa,
								lstPlazo:lstPlazo,
								aplicaFloating:aplicaFloating
							}					
							,callback: procesarGenerarCSV
						});					
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					tooltip:	'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Salir',
					tooltip:	'Salir',
					iconCls: 'icoLimpiar',
					id: 'btnSalir',
					handler: function(boton, evento) {
						window.location = '13forma08ext.jsp';		
					}
				}
			]
		}
	});
			
				
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma08ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [	
			{name: 'IC_EPO'},
			{name: 'EPO_RELACIONADA'},	
			{name: 'REFERENCIA'},	
			{name: 'TIPO_TASA'},	
			{name: 'VALOR'},	
			{name: 'PLAZO'},	
			{name: 'REL_MAT'},	
			{name: 'PUNTOS'},	
			{name: 'FECHA'},	
			{name: 'VALOR_TASA'},
			{name: 'TASA_SELEC'},
			{name: 'LD_TOTAL'},
			{name: 'IC_TASA'},
			{name: 'ROJO'},
			{name: 'LS_PLAZO'}		
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});

	var validaTasaN =  function( opts, success, response, registro) { 
		var valor = parseFloat(registro.data['VALOR']);  
		var valor_tasa = parseFloat(registro.data['VALOR_TASA']); 
		var valor_plazo= registro.data['PLAZO']; 
		var btnGuardar = Ext.getCmp('btnGuardar');
	
		if(valor_tasa <0  ||  valor_tasa ==0 ){
			Ext.MessageBox.alert('Mensaje','La tasa debe ser mayor a 0');
			registro.data['VALOR_TASA']= '';
			return false;
		}
		
		if(valor_tasa >  valor){
			Ext.MessageBox.alert('Mensaje','La tasa no debe ser mayor al Valor de la Tasa Actual');
			registro.data['VALOR_TASA']= '';
			registro.commit();				
			return false;
		}
		
		//Agregado Fodea 17	
		var auxTasa;
		var auxplazo;
		var totalRojo = 0;
		var tasaValor = [];
		
		if( Opera_Fideicomiso == 'S'){
			for(i=0;i<totalTasasAux;i++){
				auxTasa			= eval('datosTasasAux.tasa_aplicar'+i);  
				auxplazo			= eval('datosTasasAux.rango_plazo'+i);  		
				//alert(valor_tasa   +"<"+auxTasa + "----"+valor_plazo+"=="+auxplazo);				
				if ( valor_plazo ==auxplazo ) {
					tasaValor.push(auxTasa);
				}
			}
			
			for(i=0;i<tasaValor.length;i++){
				var  auxTasa2  = tasaValor[i];				
				if (  valor_tasa < auxTasa2 ) {
					totalRojo++;
				}
			}
			
			if(totalRojo==tasaValor.length){
				Ext.MessageBox.alert('Mensaje','La tasa debe de ser mayor o igual a la Tasa Actual de los IFs de la EPO');
				registro.data['VALOR_TASA']= '';
				registro.commit();	
				btnGuardar.disable();
				return false;
			}
		}
		
		btnGuardar.enable();
	}
	var revisarDecimal =  function( opts, success, response, registro) { 
		var numero = registro.data['VALOR_TASA']; 	
		if (isNaN(numero)) {
			alert ("El valor introducido no es un n�mero v�lido");
			registro.data['VALOR_TASA']= '';
			registro.commit();	
		} else {
			if (numero.indexOf('.') == -1)
				numero += ".";
			var dectext = numero.substring(numero.indexOf('.')+1, numero.length);
			if (dectext.length > 5) {
				alert ("Por favor introduzca un n�mero con m�ximo 5 decimales.");
				registro.data['VALOR_TASA']= '';
				registro.commit();	
			} else {
				return true;
			}
		}
	}

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Tasas Preferenciales/Negociadas',
		hidden: true,
		clicksToEdit: 1,
		columns: [
			{							
				header : 'EPO Relacionada',
				tooltip: 'EPO_RELACIONADA',
				dataIndex : 'EPO_RELACIONADA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Referencia',
				tooltip: 'REFERENCIA',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo de Tasa',
				tooltip: 'TIPO_TASA',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Valor',
				tooltip: 'Valor',
				dataIndex : 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Rel. Mat',
				tooltip: 'Rel. Mat',
				dataIndex : 'REL_MAT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Puntos',
				tooltip: 'Puntos',
				dataIndex : 'PUNTOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Fecha',
				tooltip: 'Fecha',
				dataIndex : 'FECHA',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'center'	
			},
			{							
				header : 'Valor Tasa',
				tooltip: 'Valor Tasa',
				dataIndex : 'VALOR_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				editor: {
					xtype : 'textfield',		
					maxValue : 999999999999.99999,
					allowBlank: false
					
				},					
				renderer:function(value,metadata,registro){
					if( registro.data['TASA_SELEC'] =='NG'){				
						if(registro.data['ROJO'] == 'S' && Opera_Fideicomiso == 'S') //Validaciones Fodea 17
							return  '<font color="red">'+ NE.util.colorCampoEdit( Ext.util.Format.number(value.replace(/[\$]/g, ''), '.00000'),metadata,registro)+'</font>';									 
						else
							return NE.util.colorCampoEdit( Ext.util.Format.number(value.replace(/[\$]/g, ''), '.00000'),metadata,registro);
					}else if( registro.data['TASA_SELEC'] =='P'){	
						if(registro.data['ROJO'] == 'S' && Opera_Fideicomiso == 'S')
							return '<font color="red">'+ Ext.util.Format.number(value.replace(/[\$]/g, ''), '.00000')+'</font>';
						else
							return Ext.util.Format.number(value.replace(/[\$]/g, ''), '.00000');
					}
					
				}				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
							
				if(campo == 'VALOR_TASA'){				
					if( record.data['TASA_SELEC'] =='NG'){
						return true; // es editable 					
					}else if( record.data['TASA_SELEC'] =='P'){
						return false; //  no es editable 					
					}
				}	
								
			},
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
						
				if(campo=='VALOR_TASA') { 	 	
					revisarDecimal('', '', '', record); 
					validaTasaN('', '', '', record); 
					
				}
			}
		},		
		bbar: {
			items: [				
				{
					xtype:'label',
					id: 'lblTasas',
					html:'<font color = "red">    * Las Tasas marcadas en color rojo son menores a las tasas a aplicar de los IFs de la EPO </font>'				
				},
				'->','-',
				{
					xtype: 'button',
					text: 'Eliminar Tasas Negociadas',
					tooltip:	'Eliminar Tasas Negociadas',
					id: 'btnEliminarNegociables',
					iconCls: 'borrar',
					hidden: true,
					handler: procesarEliminar 
				},
				{
					xtype: 'button',
					text: 'Eliminar Tasas Preferenciales',
					tooltip:	'Eliminar Tasas Preferenciales',
					id: 'btnEliminarPreferenciales',
					iconCls: 'borrar',
					hidden: true,
					handler: procesarEliminar 
				},
				'-',
				{
					xtype: 'button',
					text: 'Guardar',					
					tooltip:	'Guardar',
					iconCls: 'icoAceptar',
					id: 'btnGuardar',
					handler: procesarGuardar
				},			
				'-',
				{
					xtype: 'button',
					text: 'Cancelar',
					tooltip:	'Cancelar',
					iconCls: 'icoLimpiar',
					id: 'btnCancelar',
					handler: function(boton, evento) {
						window.location = '13forma08ext.jsp';		
					}
				}
			]
		}
	});
	
	var procesarCatalogoMonedaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_moneda1 = Ext.getCmp('ic_moneda1');
			if(ic_moneda1.getValue()==''){
				ic_moneda1.setValue(records[0].data['clave']);
			}
		}
	}		
		
	var storeCatalogoPYME = new Ext.data.JsonStore({
		id: 'storeCatalogoPYME',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma08ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma08ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPlazo = new Ext.data.JsonStore({
		id: 'catalogoPlazo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma08ext.data.jsp',
		baseParams: {
			informacion: 'catalogoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma08ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	

	var radioTipoTasa = new  Ext.form.RadioGroup({
		hideLabel: true,
		name: 'tipoTasa',	
		style: 'margin:0 auto;',
		columns: 3,
		items: [
			{ 
				boxLabel:    'PREFERENCIAL',
				name:  'tipoTasa',
				id: 	'tipoTasa1',
				inputValue: 'P',
				value: 'P',
				checked: true
			},
			{ 
				boxLabel:    'NEGOCIADA',
				name:   'tipoTasa',
				id: 	'tipoTasa2',
				inputValue: 'N',
				value: 'N'
			},
			{ 
				boxLabel:    'OFERTA TASAS POR MONTOS',
				name:        'tipoTasa',
				id: 	'tipoTasa3',
				inputValue: 'O',
				value: 'O'
			}
		],
		style: { paddingLeft: '10px' } ,
		listeners:{
			change: function(grupo, radio){
				var lstTipoTasa = radio.inputValue;
				
				Ext.getCmp('suceptibleFloating').hide();
				Ext.getCmp('aplicaFloating2').setValue(false);
			   Ext.getCmp('aplicaFloating1').setValue(false);
				
				if(lstTipoTasa=='N' || lstTipoTasa=='P'){				
					catalogoEPO.load({
						params: {
							informacion: 'catalogoEPO',
							tipoTasa:lstTipoTasa						
						}						
					});							
				}
				if(lstTipoTasa=='O' ){				
					window.location.href='/nafin/13descuento/13pki/13if/13ofertatasas1ext.jsp?lstTipoTasa='+lstTipoTasa;
				}
				var lstPlazo2 = Ext.getCmp("lstPlazo2");
				var txtPuntos2 = Ext.getCmp("txtPuntos2");
				if(lstTipoTasa=='N'){		
					lstPlazo2.show();
					txtPuntos2.hide();				
				}else if(lstTipoTasa=='P'){	
					lstPlazo2.hide();
					txtPuntos2.show();
					Ext.getCmp('suceptibleFloating').show();
				}				
			}
		}
	});
	
	var elementosForma = [		
		{
			xtype: 'textfield',
			name: 'dlpuntos',
			id: 'dlpuntos2',				
			hidden: true,
			startDay: 0,
			maxLength: 15,	
			width: 100					
		},	
		{
			xtype: 'textfield',
			name: 'ldTotal',
			id: 'ldTotal2',				
			hidden: true,
			startDay: 0,
			maxLength: 15,	
			width: 100					
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'EPO Relacionada',
			combineErrors: false,
			msgTarget: 'side',
			id: 'lstEpo2',			
			width: 500,
			items: [	
				{
					xtype: 'multiselect',
					fieldLabel: 'EPO Relacionada',
					name: 'ic_epo',
					id: 'ic_epo1',
					width: 300,
					height: 200, 
					autoLoad: false,
					allowBlank:false,
					store:catalogoEPO,
					displayField: 'descripcion',
					valueField: 'clave',		 
					tbar:[{
						text: 'Limpiar',
						handler: function(){
							fp.getForm().findField('ic_epo1').reset();
						}
					}],
					ddReorder: true,
					listeners: {
						change: function(combo){
							var cmbPYME = Ext.getCmp('ic_pyme1');
							cmbPYME.setValue('');
							cmbPYME.store.load({
								params: {
									ic_epo:Ext.getCmp('ic_epo1').getValue(),
									ic_moneda:Ext.getCmp('ic_moneda1').getValue()	
								}
							});
						}
					}		
				}
			]
		 },
		 {
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar Moneda...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',
					fieldLabel: 'Moneda',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoMoneda,
					listeners: {
						change: function(combo){
							var cmbPYME = Ext.getCmp('ic_pyme1');
							cmbPYME.setValue('');
							cmbPYME.store.load({
								params: {
									ic_epo:Ext.getCmp('ic_epo1').getValue(),
									ic_moneda:Ext.getCmp('ic_moneda1').getValue()	
								}
							});
						}
					}	
				}
			]
		},
		{
			xtype: 'combo',
			name: 'ic_pyme',
			id: 'ic_pyme1',
			mode: 'local',
			width: 500,
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione PYME...',			
			valueField: 'clave',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Nombre del Proveedor',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeCatalogoPYME,
			listeners: {
				select: {
					fn: function(combo) {
						var cmbPYME = Ext.getCmp('ic_pyme1');
						var tipoTasa1 = Ext.getCmp("tipoTasa1");	
							
						if(tipoTasa1.getValue()==true){ tipoTasa ='P'; }
												
						if( tipoTasa =='P') {
							Ext.Ajax.request({
								url: '13forma08ext.data.jsp',
								params: {
									informacion:'ValidaPYME',
									ic_pyme :combo.getValue(),
									tipoTasa:tipoTasa
								}					
								,callback: procesarPYME
							});
						}
					}
				}
			}			
		},
		{  
			xtype:  'radiogroup',   
			fieldLabel: "Aplica Floating",    
			name: 'suceptibleFloating',   
			id: 'suceptibleFloating', 
			align: 'center',
			width: 200,							
				items: [						
					{ 
						boxLabel:    "Si",             
						name:        'aplicaFloating', 
						id: 'aplicaFloating1',
						inputValue:  "S" ,
						value: 'S',
						checked: false,
						width: 20
					}, 				
					{           
						boxLabel: "No",             
						name: 'aplicaFloating', 
						id: 'aplicaFloating2',
						inputValue:  "N", 
						value: 'N',							
						width: 20						
					} 				
				]
			},
		{
			xtype: 'compositefield',
			fieldLabel: 'Puntos',
			id: 'txtPuntos2',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'textfield',
					name: 'txtPuntos',
					id: 'txtPuntos1',
					fieldLabel: 'Puntos',
					allowBlank: true,
					startDay: 0,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 
							field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '.00000'));  
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo',
			combineErrors: false,
			msgTarget: 'side',
			id: 'lstPlazo2',
			hidden: true,
			width: 500,
			items: [	
			{
				xtype: 'combo',
				name: 'lstPlazo',
				id: 'lstPlazo1',
				mode: 'local',				
				displayField: 'descripcion',
				emptyText: 'Seleccione  Plazo...',			
				valueField: 'clave',
				hiddenName : 'lstPlazo',
				fieldLabel: 'Plazo',			
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				allowBlank: true,
				msgTarget: 'side',	
				store: catalogoPlazo,
				tpl : NE.util.templateMensajeCargaCombo					
			}	
			]
		}		
	];
	

	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 800,
		title: 'Carga Individual  Tasas Preferenciales/Negociadas',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
				Ext.getCmp("txtPuntos1").clearInvalid();
				var tipoTasa;
					var ic_pyme = Ext.getCmp("ic_pyme1");
					if (Ext.isEmpty(ic_pyme.getValue()) ){
						ic_pyme.markInvalid('Debe Seleccionar un Proveedor.');
						return;
					}
					var tipoTasa1 = Ext.getCmp("tipoTasa1");	
					if(tipoTasa1.getValue()==true) {	
						tipoTasa='P';
						var txtPuntos = Ext.getCmp("txtPuntos1");
						if (!Ext.isEmpty(txtPuntos.getValue()) ){
							txtPuntos.markInvalid('Los Puntos no son un criterio de b�squeda.');
							return;
						}
					}				
					
					var tipoTasa2 = Ext.getCmp("tipoTasa2");	
					if(tipoTasa2.getValue()==true) {	
						tipoTasa='N';
						var lstPlazo = Ext.getCmp("lstPlazo1");
						if (!Ext.isEmpty(lstPlazo.getValue()) ){
							lstPlazo.markInvalid('El Plazo no es un criterio de b�squeda.');
							return;
						}
					}
				
				
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							proceso: 'Consulta',
							tipoTasa:tipoTasa
						})
					});
					
			}
		},
		{
			text: 'Procesar',
			id: 'btnProcesar',
			iconCls: 'icoAceptar',
			formBind: true,		
			handler: function(boton, evento) {
				Ext.getCmp("txtPuntos1").clearInvalid();
				var tipoTasa;
				var ic_pyme = Ext.getCmp("ic_pyme1");
				if (Ext.isEmpty(ic_pyme.getValue()) ){
					ic_pyme.markInvalid('Debe Seleccionar un Proveedor.');
					return;
				}
				var tipoTasa1 = Ext.getCmp("tipoTasa1");	
				if(tipoTasa1.getValue()==true) {	
					tipoTasa='P';
					var txtPuntos = Ext.getCmp("txtPuntos1");
					if (Ext.isEmpty(txtPuntos.getValue()) ){
						txtPuntos.markInvalid('Debe de indicar los Puntos ');
						return;
					}
                                        if (txtPuntos.getValue()<0) {
                                            txtPuntos.markInvalid('Debe de indicar solo Puntos positivos ');
                                            return;
                                        }
				}
								
				var tipoTasa2 = Ext.getCmp("tipoTasa2");	
				if(tipoTasa2.getValue()==true) {	
					tipoTasa='N';
					var lstPlazo = Ext.getCmp("lstPlazo1");
					if (Ext.isEmpty(lstPlazo.getValue()) ){
						lstPlazo.markInvalid('Debe Seleccionar un Plazo');
						return;
					}
				}
								
				fp.el.mask('Enviando...', 'x-mask-loading');			
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar',
						proceso: 'Procesar',
						tipoTasa:tipoTasa
					})
				});
					
			}
		}	
		]
	});
	 
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			mensajeAutentificacion,
			NE.util.getEspaciador(20),			
			radioTipoTasa,
			fp,			
			gridCifrasControl, 
			NE.util.getEspaciador(20),
			gridConsulta,
			gridAcuse,
			NE.util.getEspaciador(20)			
		]
	});

	catalogoMoneda.load();
	catalogoEPO.load();
	catalogoPlazo.load();
	
	//Comprueba si opera Fideicomiso
	Ext.Ajax.request({
		url: '13forma08ext.data.jsp',
		params: {
			informacion: 'OperaFideicomiso'
		},
		callback: procesarFideicomiso
	});	
		
	
//-------------------------------- ----------------- -----------------------------------
	
});