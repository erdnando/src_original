<%@ page 
	contentType=
		"application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.pdf.*,
		com.netro.zip.*,
		java.sql.*,
		org.apache.commons.logging.Log,
		net.sf.json.JSONArray,net.sf.json.JSONObject,
		javax.naming.Context,
		com.netro.descuento.*,
		java.io.*,
		com.netro.exception.*, 
		netropology.utilerias.*,
		com.netro.cadenas.*"
	errorPage=
		"/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

String informacion 	= (request.getParameter("informacion")	== null)?"":request.getParameter("informacion");
String operacion		= (request.getParameter("operacion") 	== null)?"":request.getParameter("operacion");

String infoRegresar	= "";

log.debug("informacion = <"+informacion+">");

if (        informacion.equals("CargaArchivo.inicializacion") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	boolean		hayAviso				= false;
	String 		estadoSiguiente 	= null;
	String 		aviso 				= null;
	String 		claveIF 				= iNoCliente;
	
	// 1. Obtener instancia de EJB de Seguridad

	
	// 2. Validar Numero Fiso


	// 3. Remover variables de sesion

	
	// Obtener instancia del EJB de Garantias

	
	if(!hayAviso){
		
		String saldosLayout = "";	
		resultado.put("saldosLayout", 	saldosLayout			);
		
	}
	
	if(!hayAviso){
		estadoSiguiente 	= "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES";
	}
	
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	estadoSiguiente 		);
	resultado.put("aviso",					aviso						);
	infoRegresar = resultado.toString();

} else if (          informacion.equals("CargaArchivo.enviarSolicitudDeCarga") )	{
	
	JSONObject	resultado 			= new JSONObject();
	boolean		success	 			= true;
	
	String claveMes 					= (request.getParameter("claveMes")						== null)?"":request.getParameter("claveMes");
	String numeroRegistros 			= (request.getParameter("numeroRegistros")			== null)?"":request.getParameter("numeroRegistros");
	String sumatoriaSaldosFinMes 	= (request.getParameter("sumatoriaSaldosFinMes")	== null)?"":request.getParameter("sumatoriaSaldosFinMes");
	
	resultado.put("claveMes",					claveMes						);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("sumatoriaSaldosFinMes",	sumatoriaSaldosFinMes	);
 
	// Enviar resultado de la operacion
	resultado.put("success", 				new Boolean(success)	);
	resultado.put("estadoSiguiente", 	"ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES" 		);
	infoRegresar = resultado.toString();
	
} else if (        	informacion.equals("CargaArchivo.subirArchivo") 				)	{	
	
	// Para que pueda ser parseada la respuesta, esta debe ser de tipo: text/html
	String myContentType = "text/html;charset=UTF-8";
	response.setContentType(myContentType						);
	request.setAttribute("myContentType", myContentType	);
 
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	// Cargar en memoria el contenido del archivo
	com.jspsmart.upload.SmartUpload myUpload = new com.jspsmart.upload.SmartUpload();
	try {
		
		myUpload.initialize(pageContext);
		myUpload.setTotalMaxFileSize(2097152);
		myUpload.upload();
		
	} catch(Exception e) {
			
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Cargar en memoria el contenido del archivo");
		e.printStackTrace();
		if(myUpload.getSize() > 2097152) {
			throw new AppException("Error, el Archivo es muy Grande, excede el Límite que es de 8 MB.");
		} else {
			throw new AppException("Problemas al Subir el Archivo."); 
		}
			
	}
 
	// Leer parametros adicionales
	com.jspsmart.upload.Request myRequest 		= myUpload.getRequest();
	String claveMes 					= (myRequest.getParameter("claveMes1")						== null)?"":myRequest.getParameter("claveMes1");
	String numeroRegistros 			= (myRequest.getParameter("numeroRegistros1")			== null)?"":myRequest.getParameter("numeroRegistros1");
	String sumatoriaSaldosFinMes	= (myRequest.getParameter("sumatoriaSaldosFinMes1")	== null)?"":myRequest.getParameter("sumatoriaSaldosFinMes1");

	// Guardar Archivo en Disco
	int numFiles = 0;
	try {

		com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
		myFile.saveAs(strDirectorioTemp + iNoUsuario + "." + myFile.getFileName().replaceAll("\\s+"," ")	 );
		resultado.put("fileName", 	myFile.getFileName().replaceAll("\\s+"," ")			);

	}catch(Exception e){
		
		success		= false;
		log.error("CargaArchivo.subirArchivo(Exception): Guardar Archivo en Disco");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al guardar el archivo");
	}
	
	// Enviar parametros adicionales
	resultado.put("claveMes",					claveMes						);
	resultado.put("numeroRegistros",			numeroRegistros			);
	resultado.put("sumatoriaSaldosFinMes",	sumatoriaSaldosFinMes	);
 
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 		"REALIZAR_VALIDACION" 	);
	// Enviar resultado de la operacion
	resultado.put("success", 					new Boolean(success)		);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("CargaArchivo.realizarValidacion") 				)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
 
	String 		fileName 					= (request.getParameter("fileName")						== null)?"":request.getParameter("fileName");
	
	String 		rutaArchivo 				= strDirectorioTemp + iNoUsuario + "." + fileName	;
	String 		claveIF 						= iNoCliente;
	
	// Obtener instancia del EJB de Garantias
	
	PagosIFNB bean=null;
	try {
				
		
			
		bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
		
		
		
	} catch(Exception e) {
	 
		log.error("CargaArchivo.realizarValidacion(Exception): Obtener instancia del EJB de Garantías");
		e.printStackTrace();
		throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
		
	}
	List resultados = bean.procesarArchivo(strDirectorioTemp, iNoUsuario+"."+fileName, strTipoBanco);
		
		
		String registrosConErrores = (String) resultados.get(0);
		String erroresVsCifrasControl = (String) resultados.get(1);
		String registrosSinErrores = (String) resultados.get(2);
		int numProceso = ((Integer) resultados.get(3)).intValue();
			

	String claveProceso 		= numProceso+"";
	

	//  Leer Resultado de la Validacion ( Registros sin Errores )
	//String 		 registrosSinErrores 				  = rg.getCorrectos();
	
		StringBuffer 	buffer 	= new StringBuffer();
		JSONArray		registrosSinErroresDataArray = new JSONArray();
		int				ctaRegistros = 0;
		int compara=registrosSinErrores.length();
		for(int i=0;i<registrosSinErrores.length();i++){
			char lastChar = registrosSinErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosSinErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosSinErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		// Si el primer registro dice: Claves de Financiamiento, borrarlo
		if( 	registrosSinErroresDataArray.size() > 0 ){
				
			JSONArray registro = (JSONArray) registrosSinErroresDataArray.get(0);
			if("Claves de Financiamiento:".equals( registro.getString(1) )){
				registrosSinErroresDataArray.remove(0);
			}
			
		}
 
	//String numeroRegistrosSinErrores 		= String.valueOf(rg.getNumRegOk());
 
	//  Leer Resultado de la Validacion ( Registros con Errores )
	

		buffer 			= new StringBuffer();
		JSONArray		registrosConErroresDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<registrosConErrores.length();i++){
			char lastChar = registrosConErrores.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				registrosConErroresDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			registrosConErroresDataArray.add(registro);
			buffer.setLength(0);
		}
		
 
	// Leer Resultado de la Validacion (  Errores vs cifras de control )
	
		buffer 			= new StringBuffer();
		JSONArray		erroresVsCifrasControlDataArray = new JSONArray();
		ctaRegistros 	= 0;
		
		for(int i=0;i<erroresVsCifrasControl.length();i++){
			char lastChar = erroresVsCifrasControl.charAt(i);
			if(lastChar == '\n'){
				JSONArray registro = new JSONArray();
				registro.add(String.valueOf(ctaRegistros++));
				registro.add(buffer.toString());
				erroresVsCifrasControlDataArray.add(registro);
				buffer.setLength(0);
			} else {
				buffer.append(lastChar);
			}
		}
		if(buffer.length() > 0){
			JSONArray registro = new JSONArray();
			registro.add(String.valueOf(ctaRegistros++));
			registro.add(buffer.toString());
			erroresVsCifrasControlDataArray.add(registro);
			buffer.setLength(0);
		}
		String nombreArchivo = "";
		resultado.put("urlArchivo",nombreArchivo);
	if (erroresVsCifrasControl.length() > 0) {
		CreaArchivo archivo = new CreaArchivo();
		
		if(!archivo.make(erroresVsCifrasControl, strDirectorioTemp, ".txt")) {
			out.print("<--!Error al generar el archivo-->");
		} else {
			nombreArchivo = archivo.nombre;
			resultado.put("urlArchivo",											strDirecVirtualTemp+nombreArchivo			);
		}
	}
	// Determinar si se mostrará el boton continuar carga	
	Boolean continuarCarga 								= (compara>0&&erroresVsCifrasControl.length()==0)
																					?new Boolean(true):new Boolean(false);
	// Enviar resultado general de la validacion
	
	
	
	resultado.put("claveProceso",											claveProceso										);

	resultado.put("registrosSinErroresDataArray",					registrosSinErroresDataArray					);

	resultado.put("registrosConErroresDataArray",					registrosConErroresDataArray					);
	
	resultado.put("erroresVsCifrasControlDataArray",				erroresVsCifrasControlDataArray				);
	resultado.put("continuarCarga",										continuarCarga										);
		
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", "ESPERAR_DECISION_VALIDACION" );
	// Enviar resultado de la operacion
	resultado.put("success", 			new Boolean(success)	);
 
	infoRegresar = resultado.toString();
	
} else if (    informacion.equals("CargaArchivo.presentarPreacuse") 			)	{
	
	JSONObject	jsonObj	= new JSONObject();
	boolean		success		= true;
	
	JSONArray 	registrosPorAgregarDataArray 		= new JSONArray();
	JSONArray	registro									= null;
	
	
	AccesoDB con = new AccesoDB();
	String error = "";
	String claveProceso = (request.getParameter("claveProceso")==null)?"":request.getParameter("claveProceso");
	StringBuffer seguridad=new StringBuffer("");
	HashMap	datos;
	List listaRegEncabezados=new ArrayList();
	List listaRegDetalles=new ArrayList();
	List listaRegTotales=new ArrayList();
	List reg=new ArrayList();
	int total=0;
	try {
	con.conexionDB();
	
	double granTotalMontoMN = 0;
	double granTotalMontoDL = 0;
	int numTotalPrestamos = 0;

	String strSQL = 
			" SELECT EP.IC_ENCABEZADO_PAGO_TMP, " +
			" 	EP.IC_IF, EP.IG_SUCURSAL, " +
			" 	EP.IC_MONEDA, M.CD_NOMBRE as nombreMoneda, " +
			" 	TO_CHAR(EP.DF_PERIODO_FIN, 'DD/MM/YYYY') as DF_PERIODO_FIN, " +
			" 	TO_CHAR(EP.DF_PROBABLE_PAGO, 'DD/MM/YYYY') as DF_PROBABLE_PAGO, " +
			" 	TO_CHAR(EP.DF_DEPOSITO, 'DD/MM/YYYY') as DF_DEPOSITO, " +
			" 	EP.IG_IMPORTE_DEPOSITO, " +
			" 	EP.IC_FINANCIERA, F.CD_NOMBRE as nombreFinanciera " +
			" FROM comtmp_encabezado_pago ep, comcat_moneda m, " +
			" 	comcat_financiera f " +
			" WHERE ep.ic_moneda = m.ic_moneda " +
			" 	AND ep.ic_financiera = f.ic_financiera " +
			" 	AND ic_proc = " + claveProceso +
			" ORDER BY ic_encabezado_pago_tmp ";
	ResultSet rs = con.queryDB(strSQL);
	
	while (rs.next()) {	//Encabezado
	total++;
	reg=new ArrayList();
		String claveEncabezado = rs.getString("IC_ENCABEZADO_PAGO_TMP");
		int claveMoneda = rs.getInt("IC_MONEDA");
		String sucursal = rs.getString("IG_SUCURSAL");
		String nombreMoneda = rs.getString("NOMBREMONEDA");
		String fechaVencimiento = rs.getString("DF_PERIODO_FIN")==null?"":rs.getString("DF_PERIODO_FIN");
		String fechaProbablePago = rs.getString("DF_PROBABLE_PAGO");
		String fechaDeposito = rs.getString("DF_DEPOSITO");
		String importeDeposito = rs.getString("IG_IMPORTE_DEPOSITO");
		String nombreFinanciera = rs.getString("NOMBREFINANCIERA");
		
		seguridad.append(
				"Direccion estatal|Moneda|Banco servicio|Fecha vencimiento|" +
				"Fecha probable de pago|Fecha de deposito|Importe de deposito\\n"
		);
		
		seguridad.append(
				claveEncabezado + "|" + sucursal + "|" + nombreMoneda + "|" +
				fechaVencimiento + "|" + fechaProbablePago + "|" +
				fechaDeposito + "|" + importeDeposito + "|" + nombreFinanciera + "\\n"
		);
		
		//Arma el Obj Json
		datos=new HashMap();
		datos.put("IC_ENCABEZADO_PAGO_TMP",claveEncabezado);
		datos.put("IC_MONEDA",claveMoneda+"");
		datos.put("IG_SUCURSAL",sucursal);
		datos.put("NOMBREMONEDA",nombreMoneda);
		datos.put("DF_PERIODO_FIN",fechaVencimiento);
		datos.put("DF_PROBABLE_PAGO",fechaProbablePago);
		datos.put("DF_DEPOSITO",fechaDeposito);
		datos.put("IG_IMPORTE_DEPOSITO",importeDeposito);
		datos.put("NOMBREFINANCIERA",nombreFinanciera);
		reg.add(datos);
		
		//listaRegEncabezados.add(reg);
		jsonObj.put("registrosEncabezados"+total, reg);
		reg=new ArrayList();
		//Detalles
		seguridad.append(
				"Subaplicacion|Prestamo|Capital|" +
				"Intereses|Moratorios|Subsidio|Comision" +
				"IVA|Importe Pago|Concepto Pago|Origen Pago\\n"
		);
		
		String strSQLDet = 
				" SELECT ig_subaplicacion, ig_prestamo, ig_cliente, fg_amortizacion, fg_interes,"   +
				"        fg_subsidio, fg_comision, fg_iva, fg_totalexigible, cg_concepto_pago,"   +
				"        cg_origen_pago, fg_interes_mora, ig_sucursal, cg_acreditado,"   +
				"        TO_CHAR (df_operacion, 'dd/mm/yyyy') AS df_operacion,"   +
				"        TO_CHAR (df_vencimiento, 'dd/mm/yyyy') AS df_vencimiento,"   +
				"        fn_saldo_insoluto, TO_CHAR (df_pago, 'dd/mm/yyyy') AS df_pago, ig_tasa,"   +
				"        ig_dias"   +
				"   FROM comtmp_detalle_pago"  +
				" WHERE " +
				" 	ic_encabezado_pago_tmp = " + claveEncabezado + 
				" ORDER BY ic_encabezado_pago_tmp ";
		
		ResultSet rsDet = con.queryDB(strSQLDet);

		double dblTotalCapitalMN = 0;
		double dblTotalInteresesMN = 0;
		double dblTotalMoratoriosMN = 0;
		double dblTotalSubsidioMN = 0;
		double dblTotalComisionMN = 0;
		double dblTotalIVAMN = 0;
		double dblTotalImportePagoMN = 0;
		double dblTotalCapitalDL = 0;
		double dblTotalInteresesDL = 0;
		double dblTotalMoratoriosDL = 0;
		double dblTotalSubsidioDL = 0;
		double dblTotalComisionDL = 0;
		double dblTotalIVADL = 0;
		double dblTotalImportePagoDL = 0;
		
		double dblTotalSaldoInsolutoDL = 0;
		double dblTotalSaldoInsolutoMN = 0;
		
		while (rsDet.next()) {	//Detalle
			numTotalPrestamos++;
			String subaplicacion = rsDet.getString("ig_subaplicacion");
			String prestamo = rsDet.getString("ig_prestamo");
			String claveSiracCliente = rsDet.getString("ig_cliente");
			double capital = rsDet.getDouble("fg_amortizacion");
			double intereses = rsDet.getDouble("fg_interes");
			double moratorios = rsDet.getDouble("fg_interes_mora");
			double subsidio = rsDet.getDouble("fg_subsidio");
			double comision = rsDet.getDouble("fg_comision");
			double iva = rsDet.getDouble("fg_iva");
			double importePago = rsDet.getDouble("fg_totalexigible");
			
			String rs_sucursal = rsDet.getString("ig_sucursal");
			String rs_acreditado = rsDet.getString("cg_acreditado");
			String rs_fechaOperacion = rsDet.getString("df_operacion");
			String rs_fechaVenc = rsDet.getString("df_vencimiento");
			String rs_fechaPago = rsDet.getString("df_pago");
			double rs_saldoInsoluto = rsDet.getDouble("fn_saldo_insoluto");
			String rs_tasa = rsDet.getString("ig_tasa");
			String rs_dias = rsDet.getString("ig_dias");
			
			String conceptoPago = rsDet.getString("cg_concepto_pago")==null?"":rsDet.getString("cg_concepto_pago");
			String nombreConceptoPago = "";
			if (conceptoPago.equals("AN")) {
				nombreConceptoPago = "Anticipado";
			} else if (conceptoPago.equals("VE")) {
				nombreConceptoPago = "Vencimiento";
			} else if (conceptoPago.equals("CV")) {
				nombreConceptoPago = "Cartera Vencida";
			} else if (conceptoPago.equals("CO")) {
				nombreConceptoPago = "Comision";
			}
			
			String origenPago = rsDet.getString("cg_origen_pago")==null?"":rsDet.getString("cg_origen_pago");
			String nombreOrigenPago = "";
			if (origenPago.equals("A")) {
				nombreOrigenPago = "Acreditado";
			} else if (origenPago.equals("I")) {
				nombreOrigenPago = "Intermediario";
			}
			
			if (claveMoneda == 1) {
				dblTotalCapitalMN += capital;
				dblTotalInteresesMN += intereses;
				dblTotalMoratoriosMN += moratorios;
				dblTotalSubsidioMN += subsidio;
				dblTotalComisionMN += comision;
				dblTotalIVAMN += iva;
				dblTotalImportePagoMN += importePago;
				granTotalMontoMN += importePago;
				dblTotalSaldoInsolutoMN +=rs_saldoInsoluto;
			} else if (claveMoneda == 54) {
				dblTotalCapitalDL += capital;
				dblTotalInteresesDL += intereses;
				dblTotalMoratoriosDL += moratorios;
				dblTotalSubsidioDL += subsidio;
				dblTotalComisionDL += comision;
				dblTotalIVADL += iva;
				dblTotalImportePagoDL += importePago;
				granTotalMontoDL += importePago;
				dblTotalSaldoInsolutoDL +=rs_saldoInsoluto;
			}
			
			seguridad.append(
					subaplicacion + "|" + prestamo + "|" + 
					capital + "|" + intereses + "|" +
					moratorios + "|" + subsidio + "|" + subsidio + "|" +
					subsidio + "|" + comision + "|" + iva + "|" +
					importePago + "|" + nombreConceptoPago + "|" +
					nombreConceptoPago + "|" + nombreOrigenPago + "\\n"
			);
			
			if("NB".equals(strTipoBanco)) {
				datos=new HashMap();
				
				datos.put("SUBAPLICACION",subaplicacion);
				datos.put("PRESTAMO",prestamo);
				datos.put("CLAVE_SIRAC",claveSiracCliente);
				datos.put("CAPITAL","$"+Comunes.formatoDecimal(capital,2));
				datos.put("INTERESES","$"+Comunes.formatoDecimal(intereses,2));
				datos.put("MORATORIOS","$"+Comunes.formatoDecimal(moratorios,2));
				datos.put("SUBSIDIO","$"+Comunes.formatoDecimal(subsidio,2));
				datos.put("COMISION","$"+Comunes.formatoDecimal(comision,2));
				datos.put("IVA","$"+Comunes.formatoDecimal(iva,2));
				datos.put("IMPORTE_PAGO","$"+Comunes.formatoDecimal(importePago,2));
				datos.put("CONCEPTO_PAGO",nombreConceptoPago);
				datos.put("ORIGEN_PAGO",nombreOrigenPago);
				
				reg.add(datos);
			}
			else if("B".equals(strTipoBanco)) {	
				datos=new HashMap();
				
				datos.put("SUCURSAL",rs_sucursal);
				datos.put("SUBAPLICACION",subaplicacion);
				datos.put("PRESTAMO",prestamo);
				datos.put("ACREDITADO",rs_acreditado);
				datos.put("FECHA_OPERACION",rs_fechaOperacion);
				datos.put("FECHA_VENCIMIENTO",rs_fechaVenc);
				datos.put("FECHA_PAGO",rs_fechaPago);
				datos.put("SALDO","$"+Comunes.formatoDecimal(rs_saldoInsoluto,2));
				datos.put("TASA",rs_tasa);
				datos.put("CAPITAL",Comunes.formatoDecimal(capital,2));
				datos.put("DIAS",rs_dias+"");
				datos.put("INTERESES",Comunes.formatoDecimal(intereses,2));
				datos.put("COMISION",Comunes.formatoDecimal(comision,2));
				datos.put("IVA",Comunes.formatoDecimal(iva,2));
				datos.put("IMPORTE_PAGO",Comunes.formatoDecimal(importePago,2));
				
				reg.add(datos);
			}
		} //fin de los detalles
		
		//listaRegDetalles.add(reg);
		jsonObj.put("registrosDetalles"+total, reg);
		reg=new ArrayList();
		
		if("NB".equals(strTipoBanco)) {
		if (claveMoneda == 1) {
		datos=new HashMap();
				
				datos.put("TOTAL","Total M.N.");
				datos.put("SUBAPLICACION","$"+Comunes.formatoDecimal(dblTotalCapitalMN,2));
				datos.put("PRESTAMO","$"+Comunes.formatoDecimal(dblTotalInteresesMN,2));
				datos.put("ACREDITADO","$"+Comunes.formatoDecimal(dblTotalMoratoriosMN,2));
				datos.put("FECHA_OPERACION","$"+Comunes.formatoDecimal(dblTotalSubsidioMN,2));
				datos.put("FECHA_VENCIMIENTO","$"+Comunes.formatoDecimal(dblTotalComisionMN,2));
				datos.put("FECHA_PAGO","$"+Comunes.formatoDecimal(dblTotalIVAMN,2));
				datos.put("SALDO","$"+Comunes.formatoDecimal(dblTotalImportePagoMN,2));
				
				
				reg.add(datos);
		} else if (claveMoneda == 54) {
				datos=new HashMap();
				
				datos.put("TOTAL","Total D.L.");
				datos.put("SUBAPLICACION","$"+Comunes.formatoDecimal(dblTotalCapitalDL,2));
				datos.put("PRESTAMO","$"+Comunes.formatoDecimal(dblTotalInteresesDL,2));
				datos.put("ACREDITADO","$"+Comunes.formatoDecimal(dblTotalMoratoriosDL,2));
				datos.put("FECHA_OPERACION","$"+Comunes.formatoDecimal(dblTotalSubsidioDL,2));
				datos.put("FECHA_VENCIMIENTO","$"+Comunes.formatoDecimal(dblTotalComisionDL,2));
				datos.put("FECHA_PAGO","$"+Comunes.formatoDecimal(dblTotalIVADL,2));
				datos.put("SALDO","$"+Comunes.formatoDecimal(dblTotalImportePagoDL,2));
				
				
				reg.add(datos);
		}
		} else if("B".equals(strTipoBanco)) {
		if (claveMoneda == 1) {
		datos=new HashMap();
				
				datos.put("TOTAL","Total M.N.");
				datos.put("SUBAPLICACION","$"+Comunes.formatoDecimal(dblTotalSaldoInsolutoMN,2));
				datos.put("PRESTAMO","$"+Comunes.formatoDecimal(dblTotalCapitalMN,2));
				datos.put("ACREDITADO","$"+Comunes.formatoDecimal(dblTotalInteresesMN,2));
				datos.put("FECHA_OPERACION","$"+Comunes.formatoDecimal(dblTotalImportePagoMN,2));
				
				reg.add(datos);
		} else if (claveMoneda == 54) {
			datos=new HashMap();
				
				datos.put("TOTAL","Total D.L.");
				datos.put("SUBAPLICACION","$"+Comunes.formatoDecimal(dblTotalSaldoInsolutoDL,2));
				datos.put("PRESTAMO","$"+Comunes.formatoDecimal(dblTotalCapitalDL,2));
				datos.put("ACREDITADO","$"+Comunes.formatoDecimal(dblTotalInteresesDL,2));
				datos.put("FECHA_OPERACION","$"+Comunes.formatoDecimal(dblTotalImportePagoDL,2));
				
				reg.add(datos);
			}
		}
		seguridad=new StringBuffer(seguridad.toString().replace('\u00e1','a').replace('\u00e9','e').replace('\u00ed','i').replace('\u00f3','o').replace('\u00fa','u'));
		seguridad=new StringBuffer(seguridad.toString().replace('\u00c4','A').replace('\u00c9','E').replace('\u00cd','I').replace('\u00d3','O').replace('\u00da','U'));
		
		jsonObj.put("granTotalMontoMN",granTotalMontoMN+"");
		jsonObj.put("granTotalMontoDL",granTotalMontoDL+"");
		jsonObj.put("numTotalPrestamos",numTotalPrestamos+"");
		jsonObj.put("textoFirmar",seguridad.toString());
		jsonObj.put("registrosTotales"+total, reg);
		jsonObj.put("strTipoBanco",strTipoBanco);
		jsonObj.put("totales",total+"");
		rsDet.close();
		con.cierraStatement();
		
		}
	} catch (Exception e) {
	e.printStackTrace();
	error = "Error inesperado " +e.getMessage();
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
	}
	
	
	// Hacer eco de los parametros de la carga
	jsonObj.put("claveProceso",								claveProceso);
	

	// Especificar el estado siguiente
	jsonObj.put("estadoSiguiente", "ESPERAR_DECISION_PREACUSE" );
	// Enviar resultado de la operacion
	jsonObj.put("success", 			new Boolean(success)	);
 
	infoRegresar = jsonObj.toString();
	
} else if (    informacion.equals("CargaArchivo.transmitirRegistros")		)  {
 
	JSONObject		resultado							= new JSONObject();
	String 			estadoSiguiente					= null;
	boolean			success								= true;
	boolean			hayError								= false;
	String numProceso = (request.getParameter("numProceso")==null)?"":request.getParameter("numProceso");
	
	String granTotalMontoMN = (request.getParameter("granTotalMontoMN")==null)?"":request.getParameter("granTotalMontoMN");
	String granTotalMontoDL = (request.getParameter("granTotalMontoDL")==null)?"":request.getParameter("granTotalMontoDL");
	String numTotalPrestamos = (request.getParameter("numTotalPrestamos")==null)?"":request.getParameter("numTotalPrestamos");
	
	SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat sdf2=new SimpleDateFormat("HH:mm:ss");
	
	String fechaAcuse = sdf.format(new java.util.Date());
	String horaAcuse = sdf2.format(new java.util.Date());
	String 			msg 									= "";
	
	// Leer campos del preacuse

	// Leer campos de la firma digital
	String textoFirmado				= (request.getParameter("textoFirmado")	 			== null)?""			:request.getParameter("textoFirmado");
	String pkcs7						= (request.getParameter("pkcs7")			 	 			== null)?""			:request.getParameter("pkcs7");

	 Acuse3 acuse = new Acuse3(Acuse.ACUSE_IF,"1");
	
	if( !hayError ){
		
		// Obtener instancia del EJB de Garantias
		PagosIFNB bean=null;
		try {
					
			bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);

		} catch(Exception e) {
			
			log.error("CargaArchivo.transmitirRegistros(Exception): Obtener instancia del EJB de Garantías");
			e.printStackTrace();
			throw new AppException("Ocurrió un error al obtener instancia del EJB de Garantías.");
			
		}
		
		// Se declaran variables adicionales
		String 									folioCert 		= "";
		String 									externContent 	= textoFirmado;
		char 										getReceipt 		= 'Y';
		netropology.utilerias.Seguridad 	s 					= null;
		String 									claveIF 			= iNoCliente;
		String	_acuse = "500";
		
		/* Nota: Para otros navegadores diferentes de IE y Mozilla
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		*/
			
		// Autenticar certificado
		if ( !_serial.equals("") && !externContent.equals("") && !pkcs7.equals("") ) {
			
			folioCert = acuse.toString();
			s 				= new netropology.utilerias.Seguridad();
			
			if ( s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {	
				_acuse = s.getAcuse();
				acuse.setClaveUsuario(iNoUsuario);
				acuse.setFechaHora(fechaAcuse + " " + horaAcuse);
				acuse.setReciboElectronico(_acuse);
				acuse.setMontoMN(granTotalMontoMN);
				acuse.setMontoDL(granTotalMontoDL);		
				bean.transmitirPagos( numProceso, acuse);
				
				estadoSiguiente 	= "MOSTRAR_ACUSE_TRANSMISION_REGISTROS";
			}else{
				msg 					= "La autentificacion no se llevo a cabo: "+s.mostrarError();
				estadoSiguiente 	= "ESPERAR_DECISION";
				hayError				= true;
			}
			
		}else{
			
			try {
				throw new NafinException("GRAL0021");
			}catch(Exception e){
				estadoSiguiente 	= "ESPERAR_DECISION";
				msg					= e.getMessage();
				hayError				= true;
			}
			
		}
 
	}
	
	// Poner en sesion el resultado de la transmicion de solicitudes
	if( "MOSTRAR_ACUSE_TRANSMISION_REGISTROS".equals(estadoSiguiente) ){
		
		//resultado.put("", 	"Su solicitud fue recibida con el número de folio: " );
	
	
		// Construir Array con los datos del acuse
		JSONArray 	registrosAgregadosDataArray 	= new JSONArray();
		JSONArray	registro								= null;
		
		
		
		
		registro = new JSONArray();
		registro.add("N&uacute;mero de Acuse");
		registro.add(acuse.formatear());
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Fecha de carga");
		registro.add(fechaAcuse);
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Hora de carga");
		registro.add(horaAcuse);
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Numero de Prestamos");
		registro.add(numTotalPrestamos);
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Monto en M.N.");
		registro.add(granTotalMontoMN);
		registrosAgregadosDataArray.add(registro);
		
		registro = new JSONArray();
		registro.add("Monto en DL.");
		registro.add(granTotalMontoDL);
		registrosAgregadosDataArray.add(registro);
		
	
 
		resultado.put("registrosAgregadosDataArray",	registrosAgregadosDataArray);
		
		// Hacer eco de los parametros de la carga
	
	}
	resultado.put("acuse",acuse.toString());
	resultado.put("msg",								msg									);	
	// Especificar el estado siguiente
	resultado.put("estadoSiguiente", 			estadoSiguiente			 		);
	// Enviar resultado de la operacion
	resultado.put("success", 						new Boolean(success)				);
	
	infoRegresar = resultado.toString();
 
} else if (    informacion.equals("CargaArchivo.fin") 							)	{

	// La siguiente línea se pone por compatibilidad
	//response.sendRedirect("29cargasaldos01ext.jsp"); 
	
} else if (    informacion.equals("CatalogoMes")									)	{
	
	boolean		success		= true;
	JSONObject	resultado	= new JSONObject();
		
	// Se declaran las variables...
	String 	meses[] 	= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	Calendar cal 		= Calendar.getInstance();
	cal.add(Calendar.MONTH,-11);
	
	// Obtener lista de los ultimos 12 meses
	String 		mesanio 		= null;
	JSONArray 	registros 	= new JSONArray();	
	JSONObject 	registro 	= new JSONObject();
	for(int i=0;i<12;i++){
		
		mesanio = new SimpleDateFormat("yyyyMM").format(cal.getTime());
 
		registro.put("clave",			mesanio );
		registro.put("descripcion",	meses[cal.get(Calendar.MONTH)]+" - " + cal.get(Calendar.YEAR));
		registros.add(registro);
		
		cal.add(Calendar.MONTH,1);
		
	}
 
	// Enviar resultado
	resultado.put("success", 	new Boolean(true)	);
	resultado.put("total", 		new Integer(registros.size())	);
	resultado.put("registros", registros			);
 
	infoRegresar = resultado.toString();

} else if (    informacion.equals("GeneraArchivoPDF")	)	{
	
	JSONObject	resultado	= new JSONObject();
	boolean		success		= true;
	String 		msg 			= "";
	
	// Leer parametros
	String totalRegistros 	= (request.getParameter("totalRegistros")	== null)?"":request.getParameter("totalRegistros");
	String montoTotal 		= (request.getParameter("montoTotal")		== null)?"":request.getParameter("montoTotal");
	String claveMes 			= (request.getParameter("claveMes")			== null)?"":request.getParameter("claveMes");
	
	String 	urlArchivo 		= "";
	HashMap 	cabecera 		= null;
	try {
		
		// Crear map con la informacion del cabcera del PDF
		cabecera = new HashMap();
		cabecera.put("strPais", 				(String) session.getAttribute("strPais"));
		cabecera.put("iNoNafinElectronico", (String) ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()));
		cabecera.put("sesExterno", 			(String) session.getAttribute("sesExterno"));
		cabecera.put("strNombre", 				(String) session.getAttribute("strNombre"));
		cabecera.put("strNombreUsuario", 	(String) session.getAttribute("strNombreUsuario"));
		cabecera.put("strLogo", 				(String) session.getAttribute("strLogo"));
		

	}catch(Exception e){
			
		log.error("GeneraArchivoPDF(Exception)");
		e.printStackTrace();
		
		log.error("strDirectorioTemp        = <" + strDirectorioTemp                              + ">");
		log.error("cabecera                 = <" + cabecera                                       + ">");
		log.error("strDirectorioPublicacion = <" + strDirectorioPublicacion                       + ">");
		log.error("totalRegistros           = <" + totalRegistros                                 + ">");
		log.error("montoTotal               = <" + montoTotal                                     + ">");
		log.error("claveMes                 = <" + claveMes                                       + ">");
		log.error("iNoUsuario               = <" + iNoUsuario                                     + ">");
		log.error("strNombreUsuario         = <" + strNombreUsuario                               + ">");
			
		msg		= e.getMessage();
		success	= false;
			
	}
		
	// En caso de que haya alguno mensaje
	resultado.put("msg",				msg);	
	// Enviar resultados
	resultado.put("urlArchivo", 	urlArchivo ); 
	// Enviar resultado de la operacion
	resultado.put("success", 		new Boolean(success)						);
	
	infoRegresar = resultado.toString();
					
} else {
	
	throw new AppException("La acción: "+ informacion + " no se encuentra registrada.");
	
}
 

%>
<%=infoRegresar%>