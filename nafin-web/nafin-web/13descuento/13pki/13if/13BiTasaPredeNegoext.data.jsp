<%@ page language="java" %>
<%@ page import="
	java.util.*,
	com.netro.afiliacion.*,
	com.netro.cadenas.*,
	com.netro.descuento.*,
	com.netro.exception.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/15cadenas/015secsession.jspf"%>
<% 

CQueryHelperRegExtJS queryHelperRegExtJS;

String infoRegresar 	= "";
String noNafinEpo = "";
String lses_ic_if 	= iNoCliente;
String ses_cg_razon_social = strNombre;
String nombreUsuario = strNombreUsuario;
String informacion	= request.getParameter("informacion")==null?"": request.getParameter("informacion");
String estatus 		= (request.getParameter("estatus") == null)? "" : request.getParameter("estatus");
String lsCveMoneda 	= (request.getParameter("lstMoneda") == null)? "" : request.getParameter("lstMoneda");
String ic_pyme			= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
String ic_epo			= (request.getParameter("noIc_epo")==null)?"":request.getParameter("noIc_epo");
String df_fecha_registro_de= (request.getParameter("df_fecha_registro_de") == null) ? "": request.getParameter("df_fecha_registro_de");
String df_fecha_registro_a = (request.getParameter("df_fecha_registro_a") == null) ? "" : request.getParameter("df_fecha_registro_a");
String lsTipoTasa 	= (request.getParameter("lsTipoTasa") == null)? "" : request.getParameter("lsTipoTasa");
String lsCadenaEpo 	= (request.getParameter("noIc_epo") == null)? "" : request.getParameter("noIc_epo");
String lsPYME 			= (request.getParameter("lstPYME") == null)? "" : request.getParameter("lstPYME");
String lstPlazo		= (request.getParameter("lstPlazo") == null)? "" : request.getParameter("lstPlazo");
String lsPuntos		= (request.getParameter("txtPuntos") == null)? "" : request.getParameter("txtPuntos");
String lsAccion     	= (request.getParameter("hdnAccion") == null)? "" : request.getParameter("hdnAccion");
String txt_nafelec   = (request.getParameter("txt_nafelec") == null)? "" : request.getParameter("txt_nafelec");
String operacion 		= (request.getParameter("operacion")==null)?"":request.getParameter("operacion");

String num_pyme   	= (request.getParameter("num_pyme") == null)? "" : request.getParameter("num_pyme");
String rfc_pyme   	= (request.getParameter("rfc_pyme") == null)? "" : request.getParameter("rfc_pyme");
String nombre_pyme   = (request.getParameter("nombre_pyme") == null)? "" : request.getParameter("nombre_pyme");
String hid_nombre   = (request.getParameter("hid_nombre") == null)? "" : request.getParameter("hid_nombre");
String aplicaFloating = (request.getParameter("aplicaFloating") != null) ? request.getParameter("aplicaFloating") : "";
if (!"P".equals(lsTipoTasa)  ) {
	aplicaFloating = "";
}
int start = 0;
int limit = 0;

if(informacion.equals("Consulta")) { 
	try {
		start = Integer.parseInt((request.getParameter("start")==null)?"0":request.getParameter("start"));
		limit = Integer.parseInt((request.getParameter("limit")==null)?"0":request.getParameter("limit"));				
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	ConsBitaTasaPrefeNego paginador = new ConsBitaTasaPrefeNego();
	/* SE PASAN LOS VALORES AL OBJETO PAGINADOR PARA REALIZAR LA CONSULTA */
	paginador.setIc_epo(ic_epo);
   paginador.setIc_pyme(ic_pyme);
	paginador.setDf_fecha_registro_de(df_fecha_registro_de);
	paginador.setDf_fecha_registro_a(df_fecha_registro_a);
	paginador.setEstatus(estatus);
	paginador.setLstMoneda(lsCveMoneda);
	paginador.setLstTipoTasa(lsTipoTasa);
	paginador.setLses_ic_if(lses_ic_if);
	paginador.setLstEPO(lsCadenaEpo);
	paginador.setAplicaFloating(aplicaFloating);
	
	queryHelperRegExtJS	= new CQueryHelperRegExtJS(paginador); 
	
	if((informacion.equals("Consulta") && operacion.equals("Generar")) || (informacion.equals("Consulta") && operacion.equals(""))){ 
		try {
			if(operacion.equals("Generar")){ //Nueva consulta
				queryHelperRegExtJS.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			String cadena = queryHelperRegExtJS.getJSONPageResultSet(request,start,limit);			
			infoRegresar	= cadena;
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}
	}
	if(operacion.equals("XLS")){
		JSONObject jsonObj = new JSONObject();
		String nombreArchivo = queryHelperRegExtJS.getCreatePageCustomFile(request, start,limit, strDirectorioTemp,operacion);
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);
		infoRegresar = jsonObj.toString();
	}
}

else if(informacion.equals("catalogoNombreEPO")){
	List reg 					= new ArrayList();
	AccesoDB con 				= null;
	PreparedStatement ps 	= null;
	ResultSet rs 				= null;
	String qrySentencia		= "";
	String ambos 				= "A";
	
	if(lsTipoTasa.equals("N")){
		lsTipoTasa = "NG";
	}
		
	try{
		con = new AccesoDB();
		con.conexionDB();
					
		if(!lsTipoTasa.equals("O")){
			qrySentencia = 
				" SELECT cie.ic_epo, cg_razon_social "   +
				" FROM comrel_if_epo cie, comcat_epo ce " +						
				"  ,com_parametrizacion_epo epo "		+			
				" WHERE cie.ic_epo = ce.ic_epo"   +
				" AND cie.ic_if = "+ lses_ic_if   +						
				" AND cie.cs_vobo_nafin = 'S'"   +
				" AND cie.cs_aceptacion = 'S'"   +
				" AND CE.cs_habilitado='S'" +						 
				" AND EPO.IC_EPO = CE.IC_EPO  "+
				"	AND epo.cc_parametro_epo ='TIPO_TASA' "+           
				" AND epo.cg_valor in('"+lsTipoTasa+ "','"+ambos+ "')"+						 
				" order by cg_razon_social";
		}else{
			qrySentencia = 
				"SELECT  DISTINCT cie.ic_epo, cg_razon_social " +
				"    FROM comrel_if_epo cie, comcat_epo ce,  " +
				"    	com_oferta_tasa_if_epo co " +
				"   WHERE cie.ic_epo = ce.ic_epo " +
				"     AND ce.ic_epo = co.ic_epo " +
				"     AND cie.ic_if = co.ic_if " +
				"     AND cie.cs_vobo_nafin = 'S' " +
				"     AND cie.cs_aceptacion = 'S' " +
				"     AND ce.cs_habilitado = 'S' " +
				"     AND cie.ic_if =  "+ lses_ic_if +
				"     ORDER BY cg_razon_social ";
		}
			
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery(); 
			HashMap datos;
			
			while(rs.next()){
				datos = new HashMap();
				datos.put("clave", rs.getString(1));
				datos.put("descripcion", rs.getString(2));
				reg.add(datos);
			} 
			
			rs.close();
			ps.close();
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("registros", reg);
	infoRegresar = jsonObj.toString();	
}

if(informacion.equals("Moneda")){
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre"); 
	cat.setValoresCondicionIn("1,54", Integer.class);
	cat.setOrden("2");
	infoRegresar = cat.getJSONElementos();
}

else if(informacion.equals("pymeNombre") || operacion.equals("pymeNombre")){
	List reg 					= new ArrayList();
	AccesoDB con 				= null;
	PreparedStatement ps 	= null;
	ResultSet rs 				= null;
	String qrySentencia		= "";
	String ambos 				= "A";
	JSONObject jsonObj = new JSONObject();
	String sucepFloating 	= "N";
	try{
		con = new AccesoDB();
		con.conexionDB();

	if(!"".equals(txt_nafelec) && !"".equals(ic_pyme) && !"".equals(ic_epo)) {
		String queryPyme =
					" SELECT pym.ic_pyme, pym.cg_razon_social "   +
					"   FROM comrel_nafin crn, comrel_pyme_epo cpe, comcat_pyme pym , comcat_epo E"   +
					"  WHERE crn.ic_epo_pyme_if = cpe.ic_pyme"   +
					"    AND cpe.ic_pyme = pym.ic_pyme"   +
					"    AND crn.ic_nafin_electronico = ?"   +
					"    AND crn.cg_tipo = 'P'"   +
					"    AND cpe.cs_habilitado = 'S'"   +
					"    AND cpe.cg_pyme_epo_interno IS NOT NULL"  +
					"	 AND cpe.ic_epo = E.ic_epo " +					
					"    AND cpe.ic_epo = ? ";
				ps = con.queryPrecompilado(queryPyme);
				ps.setInt(1,Integer.parseInt(txt_nafelec));				
				ps.setInt(2,Integer.parseInt(ic_epo));
				ResultSet rsPyme = ps.executeQuery();
				
				if(rsPyme.next()){
					ic_pyme = rsPyme.getString(1)==null?"":rsPyme.getString(1);
					nombre_pyme = rsPyme.getString(2)==null?"":rsPyme.getString(2);
				} else {
					noNafinEpo = "No";					
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("noNafinEpo",noNafinEpo);
				}
	}
		
		if(!noNafinEpo.equals("No") &&  !txt_nafelec.equals("")  ){
			qrySentencia = 
				" Select n.ic_epo_pyme_if  as Pyme,  p.cg_razon_social as nombre,  p.CS_TASA_FLOATING as floating "+
				" FROM comrel_nafin n, comcat_pyme p "+
				" where n.ic_epo_pyme_if = p.ic_pyme AND ic_nafin_electronico = "+txt_nafelec;
		
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery(); 
			HashMap datos;
			
			if(rs.next()){
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("ic_pyme",rs.getString("Pyme"));
				jsonObj.put("txt_nombre",rs.getString("nombre"));
				jsonObj.put("noNafinEpo",noNafinEpo);
				jsonObj.put("sucepFloating",rs.getString("floating"));
			} 
		rs.close();
		ps.close();
		}
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
	infoRegresar = jsonObj.toString();
}

else if (informacion.equals("CatalogoBuscaAvanzada")) {
	JSONArray reg 				= new JSONArray();
	AccesoDB con 				= null;
	PreparedStatement ps 	= null;
	ResultSet rs 				= null;
	String condicion 			= "", qrySentencia = "";
	JSONObject jsonObj = new JSONObject();
	
	try{
		con = new AccesoDB();
		con.conexionDB();
		
		if(!"".equals(ic_pyme)) {
			condicion = "";
			if(!"".equals(ic_epo))
				condicion += "    AND r.ic_epo = ?";

			qrySentencia = 
				" SELECT /*+index(crn CP_COMREL_NAFIN_PK) use_nl(r p)*/"   +
				"        p.ic_pyme, 1,"+
//				"        r.cg_pyme_epo_interno num_prov, "+
				"        trim(p.cg_razon_social),"   +
				"        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega, "   +
//				"        crn.ic_nafin_electronico || ' ' || p.cg_razon_social|| ' (PROV:' ||r.cg_pyme_epo_interno||',DIST:' || r.cg_num_distribuidor ||')' despliega, "   +
				"        crn.ic_nafin_electronico, p.cg_rfc "   +
				"   FROM comrel_nafin crn, comrel_pyme_epo r, comcat_pyme p"   +
				"  WHERE crn.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND p.ic_pyme = r.ic_pyme"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND p.ic_pyme = ?"   +
				"    AND r.ic_pyme = ?"+condicion+
				"  GROUP BY p.ic_pyme,1,p.cg_razon_social,crn.ic_nafin_electronico || ' ' || p.cg_razon_social,crn.ic_nafin_electronico, p.cg_rfc ";
					
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_pyme));
			ps.setInt(2,Integer.parseInt(ic_pyme));
			if(!"".equals(ic_epo))
				ps.setInt(3,Integer.parseInt(ic_epo));
		}else {
			condicion = "";
			if(!"".equals(ic_epo))
				condicion += "    AND pe.ic_epo = ?";
			if(!"".equals(num_pyme))
				condicion += "    AND (cg_pyme_epo_interno LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') OR CG_NUM_DISTRIBUIDOR LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%'))";
			if(!"".equals(rfc_pyme))
				condicion += "    AND cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
			if(!"".equals(nombre_pyme))
				condicion += "    AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
			qrySentencia = 
				" SELECT /*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe p)*/ "   +
				"        p.ic_pyme, 1," +
//				"       pe.cg_pyme_epo_interno, "+
				"       trim(p.cg_razon_social),"   +
//				"        RPAD (crn.ic_nafin_electronico, 10, ' ') || p.cg_razon_social|| ' (PROV:' ||pe.cg_pyme_epo_interno||',DIST:' || pe.cg_num_distribuidor ||')' despliega,"   +
				"        RPAD (crn.ic_nafin_electronico, 10, ' ') || p.cg_razon_social despliega,"   +
				"        crn.ic_nafin_electronico, p.cg_rfc"   +
				"   FROM comrel_pyme_epo pe, comrel_nafin crn, comcat_pyme p"   +
				"  WHERE p.ic_pyme = pe.ic_pyme"   +
				"    AND p.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND pe.cs_habilitado = 'S'"   +
				//"    AND pe.cg_pyme_epo_interno IS NOT NULL "+
				condicion+" "+
				"    AND p.cs_habilitado = 'S'"   +
				"  GROUP BY p.ic_pyme,1,"   +
//				"           pe.cg_pyme_epo_interno,"   +
				"           p.cg_razon_social,"   +
//				"           RPAD (crn.ic_nafin_electronico, 10, ' ') || p.cg_razon_social|| ' (PROV:' ||pe.cg_pyme_epo_interno||',DIST:' || pe.cg_num_distribuidor ||')',"   +
				"           RPAD (crn.ic_nafin_electronico, 10, ' ') || p.cg_razon_social,"   +
				"           crn.ic_nafin_electronico, p.cg_rfc"   +
				"  ORDER BY p.cg_razon_social"  ;
			
			ps = con.queryPrecompilado(qrySentencia);
			int cont = 0;
			if(!"".equals(ic_epo)) {
				cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
			}
			if(!"".equals(num_pyme)) {
				cont++;ps.setString(cont,num_pyme);
				cont++;ps.setString(cont,num_pyme);				
			}
			if(!"".equals(rfc_pyme)) {
				cont++;ps.setString(cont,rfc_pyme);
			}
			if(!"".equals(nombre_pyme)) {
				cont++;ps.setString(cont,nombre_pyme);
			}
		} // fin else
		
			rs = ps.executeQuery();
			HashMap datos;
			
			while(rs.next()){
				String nPyme = (rs.getString(3)==null ? "" : rs.getString(3));
				String nombre= (rs.getString(4)==null ? "" : rs.getString(4));
				
				datos = new HashMap();
				datos.put("g",rs.getString(1));
				datos.put("s",rs.getString(2));
				datos.put("descripcion",nombre + " " + nPyme);
				datos.put("clave",rs.getString(5));
				datos.put("h",rs.getString(6));
				reg.add(datos);
			} 
			
				jsonObj.put("success", new Boolean(true));
				if (reg.size() == 0){
					jsonObj.put("total", "0");
					jsonObj.put("registros", "" );
				}else if (reg.size() > 0 && reg.size() < 1001 ){
					jsonObj.put("total",  Integer.toString(reg.size()));
					jsonObj.put("registros", reg.toString() );
				}else if (reg.size() > 1000 ){
					jsonObj.put("total", "excede" );
					jsonObj.put("registros", "" );
				}
				jsonObj.put("registros", reg);
			
			rs.close();
			ps.close();
	}catch(Exception e) {
		out.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
	infoRegresar = jsonObj.toString();	
}
%>

<%=infoRegresar%>
