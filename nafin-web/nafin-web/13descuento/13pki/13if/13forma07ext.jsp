<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf"%>
<% 
String pantalla = (request.getParameter("pantalla")!=null)?request.getParameter("pantalla"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String ct_cambio_motivo = (request.getParameter("ct_cambio_motivo")!=null)?request.getParameter("ct_cambio_motivo"):"";
String textoFirmar = (request.getParameter("textoFirmar")!=null)?request.getParameter("textoFirmar"):"";
String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";


								
String	doctosSeleccionados[]		=	request.getParameterValues("doctosSeleccionados");
String	doctosSeleccionados2 ="";
if (doctosSeleccionados != null) {
	for(int i = 0; i < doctosSeleccionados.length; i++) {
		doctosSeleccionados2 +=doctosSeleccionados[i]+",";
	}
}

String	estatusDoctos[]		=	request.getParameterValues("estatusDoctos");
String	estatusDoctos2 ="";
if (doctosSeleccionados != null) {
	for(int i = 0; i < estatusDoctos.length; i++) {
		if(estatusDoctos[i].equals("OP") ) estatusDoctos[i]= "O";
		estatusDoctos2 +=estatusDoctos[i]+",";
	}
}

%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="<%=appWebContextRoot%>/00utils/extjs/ux/BigDecimal.js"></script>
<%if(pantalla.equals("")){ %>
<script type="text/javascript" src="13forma07ext.js?<%=session.getId()%>"></script>
<%}else  if(pantalla.equals("PreAcuse")){%>
<script type="text/javascript" src="13forma07aext.js?<%=session.getId()%>"></script>
<%}else  if(pantalla.equals("Acuse")){%>
<script type="text/javascript" src="13forma07bext.js?<%=session.getId()%>"></script>
<%}%>

<%@ include file="/00utils/componente_firma.jspf" %>


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01if/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01if/menuLateralFlotante.jspf"%>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01if/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
	
	<form id='formParametros' name="formParametros">
		<input type="hidden" id="ic_epo" name="ic_epo" value="<%=ic_epo%>"/>
		<input type="hidden" id="ct_cambio_motivo" name="ct_cambio_motivo" value="<%=ct_cambio_motivo%>"/>
		<input type="hidden" id="doctosSeleccionados" name="doctosSeleccionados" value="<%=doctosSeleccionados2%>"/>
		<input type="hidden" id="estatusDoctos" name="estatusDoctos" value="<%=estatusDoctos2%>"/>
		<input type="hidden" id="textoFirmar" name="textoFirmar" value="<%=textoFirmar%>"/>
		<input type="hidden" id="pkcs7" name="pkcs7" value="<%=pkcs7%>"/>
	</form>

</body>

</html>