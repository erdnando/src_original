Ext.onReady(function() {

	var doctoSeleccionados  = Ext.getDom('doctoSeleccionados').value;
	var ic_pyme  = Ext.getDom('ic_pyme').value;
	var ic_epo  = Ext.getDom('ic_epo').value;
	var ic_banco_fondeo  = Ext.getDom('ic_banco_fondeo').value;	
	var mensajeFondeo = Ext.getDom('mensajeFondeo').value;
	var pkcs7  = Ext.getDom('pkcs7').value;
	var textoFirmado  = Ext.getDom('textoFirmado').value;
	var doctoSeleccionados  = Ext.getDom('doctoSeleccionados').value;
	var monto_descuento_mn  = Ext.getDom('monto_descuento_mn').value;
	var monto_interes_mn  = Ext.getDom('monto_interes_mn').value;
	var monto_operar_mn  = Ext.getDom('monto_operar_mn').value;	
	var monto_descuento_dl  = Ext.getDom('monto_descuento_dl').value;
	var monto_interes_dl  = Ext.getDom('monto_interes_dl').value;
	var monto_operar_dl  = Ext.getDom('monto_operar_dl').value;
	var sPlazoBO  = Ext.getDom('sPlazoBO').value;
	
	
	var constancia = new Ext.Container({
		layout: 'table',		
		id: 'constancia',							
		width:	'900',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		items: [				
			{
				xtype: 'displayfield',
				id:	'mensajesFondeoPre1',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}			
			}				
		]
	});
	
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnArchivoCSV = Ext.getCmp('btnArchivoCSV');
		btnArchivoCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	// DescargaArchivo es un servlet  para descargar los archivos
	function procesarGenerarFijo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
			
	
	var procesarGenerarVariable =  function(opts, success, response) {
		var btnArchivoVariable = Ext.getCmp('btnArchivoVariable');
		btnArchivoVariable.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarVariable = Ext.getCmp('btnBajarVariable');
			btnBajarVariable.show();
			btnBajarVariable.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarVariable.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoVariable.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarGenerarInterfaz =  function(opts, success, response) {
		var btnArchivoInterfase = Ext.getCmp('btnArchivoInterfase');
		btnArchivoInterfase.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarInterfase = Ext.getCmp('btnBajarInterfase');
			btnBajarInterfase.show();
			btnBajarInterfase.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarInterfase.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoInterfase.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//para desacargar el archivo PDF
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnArchivoPDF = Ext.getCmp('btnArchivoPDF');
		btnArchivoPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Contenedor para los botones de imprimir y Salir 
	var fpBotones2 = new Ext.Container({
		layout: 'table',
		id: 'fpBotones2',			
		width:	'900',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		items: [	
			{
				xtype: 'button',
				text: 'Exportar Interfase Variable',		
				iconCls: 'icoXls',
				id: 'btnArchivoVariable',		 
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					
					var strNombreArchivo = Ext.getCmp("strNombreArchivo").getValue();
					
					Ext.Ajax.request({
						url: '13forma03Ext.data.jsp',
						params: {
							informacion:'ArchivoVariable',
							strNombreArchivo:strNombreArchivo						
						}					
						,callback: procesarGenerarVariable
					});
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				iconCls: 'icoXls',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarVariable',
				hidden: true
			},				
			{
				xtype: 'button',
				text: 'Exportar Interfase Fijo',		
				iconCls: 'icoTxt',
				id: 'btnArchivoFijo',		 
				handler: function(boton, evento) {					
									
					var strNombreArchivoFijo = Ext.getCmp("strNombreArchivoFijo").getValue();					
					Ext.Ajax.request({
						url: '13forma03Ext.data.jsp',
						params: {
							informacion:'ArchivoFijo',
							strNombreArchivoFijo:strNombreArchivoFijo						
						}					
						,callback: procesarGenerarFijo
					});
					
				}
			},				
			{
				xtype: 'button',
				text: 'Imprimir Interfase',			
				id: 'btnArchivoInterfase',	
				iconCls: 'icoPdf',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');				
					
					var acuse = Ext.getCmp("acuse").getValue();
					var fecha = Ext.getCmp("fecha").getValue();
					var hora = Ext.getCmp("hora").getValue();	
					var usuario = Ext.getCmp("usuario").getValue();
					var recibo = Ext.getCmp("recibo").getValue();
					Ext.Ajax.request({
						url: '13forma03bpdfExt.jsp',
						params: {
							informacion:'ArchivoInterfaz',	
							ic_epo:ic_epo, 
							monto_descuento_mn:monto_descuento_mn,
							monto_interes_mn:monto_interes_mn,
							monto_operar_mn:monto_operar_mn,
							monto_descuento_dl:monto_descuento_dl,
							monto_interes_dl:monto_interes_dl,
							monto_operar_dl:monto_operar_dl,
							acuse:acuse,
							fecha:fecha,
							hora:hora,
							usuario:usuario,
							ic_banco_fondeo: ic_banco_fondeo,
							mensajeFondeo:mensajeFondeo,
							recibo:recibo
						}					
						,callback: procesarGenerarInterfaz
					});					
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarInterfase',
				iconCls: 'icoPdf',
				hidden: true				
			},				
			{
				xtype: 'button',
				text: 'Salir',	
				iconCls: 'icoLimpiar',
				id: 'btnSalir',		 
				handler: function() {
					window.location = "13forma03Ext.jsp";
				}
			}
		]
	});
	
	
		//Contenedor para los botones de imprimir y Salir 
	var fpBotones1 = new Ext.Container({
		layout: 'table',
		id: 'fpBotones1',			
		width:	'400',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		items: [	
			{
				xtype: 'button',
				text: 'Generar Archivo',
				iconCls: 'icoXls',
				id: 'btnArchivoCSV',		 
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					
					var nombreArchivoCSV = Ext.getCmp("nombreArchivoCSV").getValue();
					
					Ext.Ajax.request({
						url: '13forma03Ext.data.jsp',
						params: {
							informacion:'nombreArchivoCSV',
							nombreArchivoCSV:nombreArchivoCSV						
						}					
						,callback: procesarGenerarCSV
					});
						
				}
			},
			{
				xtype: 'button',
				iconCls: 'icoXls',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarCSV',
				hidden: true
			},	
			{
				xtype: 'button',
				text: 'Generar PDF',
				iconCls: 'icoPdf',
				id: 'btnArchivoPDF',		 
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');								
					var acuse = Ext.getCmp("acuse").getValue();
					var fecha = Ext.getCmp("fecha").getValue();
					var hora = Ext.getCmp("hora").getValue();	
					var usuario = Ext.getCmp("usuario").getValue();
					var recibo = Ext.getCmp("recibo").getValue();
					
					Ext.Ajax.request({
						url: '13forma03bpdfExt.jsp',
						params: {
							informacion:'ArchivoPDF',	
							ic_epo:ic_epo, 
							monto_descuento_mn:monto_descuento_mn,
							monto_interes_mn:monto_interes_mn,
							monto_operar_mn:monto_operar_mn,
							monto_descuento_dl:monto_descuento_dl,
							monto_interes_dl:monto_interes_dl,
							monto_operar_dl:monto_operar_dl,
							acuse:acuse,
							fecha:fecha,
							hora:hora,
							usuario:usuario,
							ic_banco_fondeo:ic_banco_fondeo,
							recibo:recibo
						}					
						,callback: procesarGenerarPDF
					});				
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarPDF',
				iconCls: 'icoPdf',
				hidden: true
			},	
			{
				xtype: 'button',
				text: 'Salir',		
				iconCls: 'icoLimpiar',
				id: 'btnSalir',		 
				handler: function() {
					window.location = "13forma03Ext.jsp";
				}
			}
		]
	});
	
	
	var fondePropio = new Ext.Container({
		layout: 'table',		
		id: 'fondePropio',							
		width:	'900',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		hidden: true,
		items: [					
			{ 
			xtype:   'label',
			id:	'fondeoP',			
			html:		'<span style="color:red;"><b> Las siguientes solicitudes de descuento se realizar�n con Fondeo propio<b></span>',  
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		}
		]
	});
	
	var mensajeAutentificacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutentificacion',							
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		items: [					
			{
				xtype: 'displayfield',
				id:	'autentificac',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}		
			},
			{ 	xtype: 'displayfield', 	hidden: true, id: 'acuse', 	value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'fecha', 	value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, id: 'hora', 	value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'usuario', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'strNombreArchivo', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'strNombreArchivoFijo', value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'nombreArchivoCSV', value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, id: 'recibo', value: '' }
		]
	});
	
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotalesPre'
		},
		hidden: true,
		fields: [						
			{name: 'EPO'},
			{name: 'MONTO_DESCUENTO_MN'},
			{name: 'MONTO_INTERES_MN'},
			{name: 'MONTO_OPERAR_MN'},			
			{name: 'MONTO_DESCUENTO_DL'},
			{name: 'MONTO_INTERES_DL'},
			{name: 'MONTO_OPERAR_DL'}		
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {				
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.											
					}
				}
			}			
	});
	
		//esto esta en duda como manejarlo
	var gruposConsulta = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 1, align: 'center'},
				{	header: 'Moneda Nacional', colspan: 3, align: 'center'},	
				{	header: 'D�lares ', colspan: 3, align: 'center'}
			]
		]
	});
	

	var gridConsultaTotales = new Ext.grid.GridPanel({
		id: 'gridConsultaTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		title:'',
		hidden: true,
		plugins: gruposConsulta,	
		columns: [
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'Monto Descto.',
				tooltip: 'Monto Descto.',
				dataIndex : 'MONTO_DESCUENTO_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_INTERES_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
				{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},	
				{							
				header : 'Monto Descto.',
				tooltip: 'Monto Descto.',
				dataIndex : 'MONTO_DESCUENTO_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_INTERES_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 120,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});
	
	
	
	var consultaTotales1 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'consultaTotales1Acuse'
		},
		hidden: true,
		fields: [			
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCTO'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {				
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.											
					}
				}
			}			
	});
	
	
	var gridTotales1 = new Ext.grid.GridPanel({
		id: 'gridTotales1',				
		store: consultaTotales1,	
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		title:'',
		hidden: true,
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'Total de Documentos',
				tooltip: 'Total de Documentos',
				dataIndex : 'TOTAL_DOCTO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'center'				
			},
			{							
				header : '',
				tooltip: '',
				dataIndex : '',
				sortable: true,
				width: 480,
				resizable: true,				
				align: 'right'							
			}	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 90,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
	
		var jsonData = store.reader.jsonData;			
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
			
						
			Ext.getCmp("autentificac").setValue(jsonData.mensajeAutentificacion);
			Ext.getCmp("acuse").setValue(jsonData.acuse);
			Ext.getCmp("fecha").setValue(jsonData.fecha);
			Ext.getCmp("hora").setValue(jsonData.hora);
			Ext.getCmp("usuario").setValue(jsonData.usuario);
			Ext.getCmp("recibo").setValue(jsonData.recibo);
			
			Ext.getCmp("strNombreArchivo").setValue(jsonData.strNombreArchivo);
			Ext.getCmp("strNombreArchivoFijo").setValue(jsonData.strNombreArchivoFijo);
			Ext.getCmp("nombreArchivoCSV").setValue(jsonData.nombreArchivoCSV);
			Ext.getCmp("mensajesFondeoPre1").setValue(jsonData.mensajesFondeoPre);	
			
					
			if(jsonData.recibo !='') {
						
				Ext.getCmp('constancia').show();
				var fondePropio = Ext.getCmp('fondePropio');
				if(jsonData.mensajeFondeo=='S') {
					fondePropio.show();
				}else {
					fondePropio.hide();
				}
				
				consultaTotales1.load({  	
					params: {  
						informacion: 'consultaTotales1Acuse', 	
						numRegistrosMNA:jsonData.numRegistrosMNA, 
						numRegistrosDLA:jsonData.numRegistrosDLA			
					}  	
				});
					
				var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');
				var gridCifrasControl = Ext.getCmp('gridCifrasControl');
				var fpBotones1 = Ext.getCmp('fpBotones1');
				var gridTotales1 = Ext.getCmp('gridTotales1');
				var fpBotones2 = Ext.getCmp('fpBotones2');
				
				gridConsultaTotales.show();			
				gridCifrasControl.show();
				fpBotones1.show();
				fpBotones2.show();
				gridTotales1.show();
			
				var acuseCifras = [
					['N�mero de Acuse', jsonData.acuse2],
					['Fecha de Carga ', jsonData.fecha],
					['Hora de Carga', jsonData.hora],
					['Usuario de Captura ', jsonData.usuario]
				];
			
				storeCifrasData.loadData(acuseCifras);	
							
				if (!gridConsulta.isVisible()) {
					gridConsulta.show();
				}				
							
				//edito el titulo de la columna
				var cm = gridConsulta.getColumnModel();				
				if(jsonData.bOperaFactorajeDist=='S' || jsonData.bOperaFactorajeVencInfonavit=='S'){
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BANCO_BENEFICIARIO'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('SUCURSAL_BENEFICIARIO'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CUENTA_BENEFICIARIO'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMPORTE_RECIBIR_BENE'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);
				}
				if(jsonData.bTipoFactoraje =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				
			}
			
			if(store.getTotalCount() > 0) {
				el.unmask();	
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
		
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse'
		},
		hidden: true,
		fields: [						
			{name: 'NOMBRE_EPO'},	
			{name: 'NU_SIRAC'},
			{name: 'NOMBRE_PROVEEDOR'},
			{name: 'NUMER0_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO'},
			{name: 'PORCE_DESCUENTO'},
			{name: 'RECURSO_GARANTIA'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'TASA'},
			{name: 'PLAZO'},
			{name: 'NUMERO_SOLICITUD'},			
			{name: 'NUM_PROVEEDOR'},
			{name: 'BENEFICIARIO'},
			{name: 'BANCO_BENEFICIARIO'},
			{name: 'SUCURSAL_BENEFICIARIO'},
			{name: 'CUENTA_BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR_BENE'},
			{name: 'NETO_RECIBIR'},
			{name: 'CLAVE_DOCUMENTO'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title: '',
		hidden: true,
		columns: [
			{							
				header : 'No. Cliente Sirac',
				tooltip: 'No. Cliente Sirac',
				dataIndex : 'NU_SIRAC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Proveedor',
				tooltip: 'Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUMER0_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},	
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},	
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'left'			
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORCE_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{							
				header : 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex : 'RECURSO_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Monto a Descuento',
				tooltip: 'Monto a Descuento',
				dataIndex : 'MONTO_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Monto Inter�s',
				tooltip: 'Monto Inter�s',
				dataIndex : 'MONTO_INTERES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Tasa',
				tooltip: 'Tasa',
				dataIndex : 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{							
				header : 'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},		
			{							
				header : 'N�mero Solicitud',
				tooltip: 'N�mero Solicitud',
				dataIndex : 'NUMERO_SOLICITUD',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},		
			{							
				header : 'No. Proveedor',
				tooltip: 'No. Proveedor',
				dataIndex : 'NUM_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Beneficiario',
				tooltip: 'Beneficiario',
				dataIndex : 'BENEFICIARIO',
				sortable: true,				
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},	
			{							
				header : 'Banco Beneficiario',
				tooltip: 'Banco Beneficiario',
				dataIndex : 'BANCO_BENEFICIARIO',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},	
			{							
				header : 'Sucursal Beneficiaria',
				tooltip: 'Sucursal Beneficiaria',
				dataIndex : 'SUCURSAL_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'				
			},	
			{							
				header : 'Cuenta Beneficiaria',
				tooltip: 'Cuenta Beneficiaria',
				dataIndex : 'CUENTA_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'				
			},	
			{							
				header : '% Beneficiario',
				tooltip: '% Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{							
				header : 'Importe a Recibir Beneficiario',
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex : 'IMPORTE_RECIBIR_BENE',
				sortable: true,
				width: 130,
				resizable: true,	
				hidden: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Neto a Recibir PyME',
				tooltip: 'Neto a Recibir PyME',
				dataIndex : 'NETO_RECIBIR',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}	
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		margins: '20 0 0 0',
		loadMask: true,		
		stripeRows: true,
		height: 200,
		width: 900,			
		frame: false
	});
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		hidden: true,
		width: 500,
		title: 'Autorizaci�n de Descuento',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',		
		monitorValid: true,
		collapsible: false		
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fp,
			NE.util.getEspaciador(20),
			mensajeAutentificacion,
			NE.util.getEspaciador(20),
			fondePropio,
			NE.util.getEspaciador(20),
			gridConsultaTotales,
			NE.util.getEspaciador(20),
			gridCifrasControl,
			constancia,
			NE.util.getEspaciador(20),
			fpBotones1,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales1,
			NE.util.getEspaciador(20),
			fpBotones2,
			NE.util.getEspaciador(20)
			
		]
	});
	
	
	
	consultaData.load({  	
		params: {  
			informacion: 'GenerarAcuse', 	
			ic_epo:ic_epo, 
			ic_pyme:ic_pyme,
			pkcs7:pkcs7,
			textoFirmado:textoFirmado,
			sPlazoBO:sPlazoBO,
			mensajeFondeo:mensajeFondeo,
			doctoSeleccionados:doctoSeleccionados,
			monto_descuento_mn:monto_descuento_mn,
			monto_interes_mn:monto_interes_mn,
			monto_operar_mn:monto_operar_mn,
			monto_descuento_dl:monto_descuento_dl,
			monto_interes_dl:monto_interes_dl,
			monto_operar_dl:monto_operar_dl
		}  	
	});
		
	consultaDataTotales.load({  	
		params: {  
			informacion: 'ConsultarTotalesAcuse', 	
			ic_epo:ic_epo, 
			monto_descuento_mn:monto_descuento_mn,
			monto_interes_mn:monto_interes_mn,
			monto_operar_mn:monto_operar_mn,
			monto_descuento_dl:monto_descuento_dl,
			monto_interes_dl:monto_interes_dl,
			monto_operar_dl:monto_operar_dl
		}  	
	});
	
				
});