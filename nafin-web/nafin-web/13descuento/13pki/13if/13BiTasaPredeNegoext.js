Ext.onReady(function() {	
	
	
	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);	
			
			if(jsonObj.noNafinEpo!='No'){
				Ext.getCmp('txt_nombre').setValue(jsonObj.txt_nombre);
				Ext.getCmp('hid_nombre').setValue(jsonObj.txt_nombre);
				Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);
			}else{
			   Ext.getCmp('txt_nombre').setValue('');
				Ext.getCmp('hid_nombre').setValue('');
				Ext.getCmp('ic_pyme').setValue('');				
				
				Ext.getCmp('btnConsultar').disable();
				Ext.Msg.alert("Mensaje",
					"El nafin electronico no corresponde a una \nPyME afiliada a la EPO o no existe.", 
					function(){
						window.location("13BiTasaPredeNegoext.jsp");
					});
			}
			
			Ext.getCmp('suceptibleFloating').enable();
			Ext.getCmp('aplicaFloating2').setValue(false);
			Ext.getCmp('aplicaFloating1').setValue(false);
			if(jsonObj.sucepFloating ==='N')  {				
				Ext.getCmp('suceptibleFloating').disable();
				
			}
			
		}
	}
	 /*** PROCESAR **/
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var grid = Ext.getCmp('grid');
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnGenerarArchivo').enable();
				Ext.getCmp('btnBajarArchivo').hide();
				el.unmask();
			} else {
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnBajarArchivo').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
		var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('id_cmb_num_ne').focus();
		}
	}
	var procesarCatalogoMoneda = function(store, arrRegistros, opts) {
		Ext.getCmp("id_lstMoneda").setValue((store.getRange()[1].data).clave);		
	}
	/*** FIN DE PROCESAR ***/
	
	/**** CATALOGOS ****/
	var catalogoNombreData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion','ic_pyme','loadMsg'],
		url : '13BiTasaPredeNegoext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoTasa = new Ext.data.SimpleStore({
		fields : ['clave', 'descripcion'],
		data : [['P','Preferencial'],['N','Negociada'],['O','Oferta de Tasas']]
	});
	var catalogoEstatus = new Ext.data.SimpleStore({
		fields : ['clave', 'descripcion'],
		data : [['A','Alta'],['E','Eliminar'],['M','Modificacion']]
	});
	var catalogoNombreEPO = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '13BiTasaPredeNegoext.data.jsp',
		listeners: {		
			//load: procesarCatalogoNombreEPO,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'catalogoNombreEPO'			
		},
		totalProperty : 'total',
		autoLoad: true
	});
	var catalogoMoneda = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields : ['clave', 'descripcion'],
		url : '13BiTasaPredeNegoext.data.jsp',
		listeners: {		
			load: procesarCatalogoMoneda,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		},
		baseParams: {
			informacion: 'Moneda'			
		},
		totalProperty : 'total',
		autoLoad: true
	});
	/*** FIN DE CATALOGOS ***/
	
	
	
	var consultaDataGrid = new Ext.data.JsonStore({
		root : 'registros',
		url : '13BiTasaPredeNegoext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'IC_BITACORA'},
			{name: 'IC_CUENTA_BANCARIA'},
			{name: 'IC_IF'},
			{name: 'IC_EPO'},
			{name: 'CG_TIPOTASA'},
			{name: 'IC_PLAZO'},
			{name: 'IC_ORDEN'},
			{name: 'NOMBREEPO'},
			{name: 'NOMBREPROVEEDOR'},
			{name: 'MONEDA'},
			{name: 'PLAZO_DIAS'},
			{name: 'MONTO_DESDE'},
			{name: 'MONTO_HASTA'},
			{name: 'DATOS_ANTERIORES'},
			{name: 'DATOS_ACTUALES'},
			{name: 'TIPOTASA'},
			{name: 'ESTATUS'},
			{name: 'ESTATUSTASA'},
			{name: 'PUNTOS'},
			{name: 'VALOR'},
			{name: 'FECHAREGISTRO'},
			{name: 'USUARIO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var grid = {	
		store: consultaDataGrid,
		xtype:'grid',
		id: 'grid',
		hidden: true,
		columns: [{
				header: 'EPO', tooltip: 'EPO',
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 180, resizable: true,
				align: 'left'
			},{
				header: 'Proveedor', tooltip: 'Proveedor',
				dataIndex: 'NOMBREPROVEEDOR',
				sortable: true,	align: 'center',
				width: 150, resizable: true,
				align: 'left'
			},
			{
				header: 'Tipo Tasa', tooltip: 'Tipo Tasa',
				dataIndex: 'TIPOTASA',
				sortable: true,	align: 'center',
				width: 120, resizable: true,
				align: 'left'
			},
			{
				header: 'Estatus', tooltip: 'Estatus',
				dataIndex: 'ESTATUSTASA',
				sortable: true,	align: 'center',
				width: 100, resizable: true,
				align: 'left',
            renderer :  function(value, metaData, record, rowIdx, colIdx, store, view){
               var valor = '';
               if(record.get('ESTATUS')==='A'){
                  valor = 'Alta';						
               } else if(record.get('ESTATUS')==='E'){
                  valor = 'Eliminar';						
               } else if(record.get('ESTATUS')==='M'){
                  valor = 'Modificacion';
						
               }
				
               return valor;
            }
			},{
				header: 'Puntos', tooltip: 'Puntos',
				dataIndex: 'PUNTOS',
				sortable: true,	align: 'center',
				width: 50, resizable: true,
				align: 'center'
			},{
				header: 'Valor', tooltip: 'ValorE',
				dataIndex: 'VALOR',
				sortable: true,	align: 'center',
				width: 80, resizable: true,
				align: 'center',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
					var valor = record.get('CG_TIPOTASA')=='O' ? 'N/A' : record.get('VALOR');
					valor = valor=='N/A' ? valor : Ext.util.Format.number(record.get('VALOR'),'0.00000');
					
					return valor;
				}
			},{
				header: 'Plazo', tooltip: 'Plazo',
				dataIndex: 'PLAZO_DIAS',
				sortable: true,	align: 'center',
				width: 50, resizable: true,
				align: 'center',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
					return record.get('CG_TIPOTASA')=='O' ? 'N/A' : record.get('PLAZO_DIAS');
				}
			},{
				header: 'Desde', tooltip: 'Desde',
				dataIndex: 'MONTO_DESDE',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'center',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
					var tMontoDesde = record.get('CG_TIPOTASA')!='O' ? 'N/A' : record.get('MONTO_DESDE');
					return tMontoDesde=='N/A' ? tMontoDesde : Ext.util.Format.number(tMontoDesde, '0.00');
				}
			},{
				header: 'Hasta', tooltip: 'Hasta',
				dataIndex: 'MONTO_HASTA',
				sortable: true,	align: 'center',
				width: 90, resizable: true,
				align: 'left',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
					var tMontoHasta = record.get('CG_TIPOTASA')!='O' ? 'N/A' : record.get('MONTO_HASTA');
					return tMontoHasta=='N/A' ? tMontoHasta : Ext.util.Format.number(tMontoHasta, '0.00');
				}
			},{
				header: 'Fecha Registro', tooltip: 'Fecha Registro',
				dataIndex: 'FECHAREGISTRO',
				sortable: true,	align: 'center',
				width: 130, resizable: true,
				align: 'left',
				renderer: function(val){ return val=='' ? '0' : val; }
			},{
				header: 'Usuario', tooltip: 'Usuario',
				dataIndex: 'USUARIO',
				sortable: true,	align: 'center',
				width: 180, resizable: true,
				align: 'left'
			},{
				header: 'Datos Anteriores', tooltip: 'Datos Anteriores',
				dataIndex: 'DATOS_ANTERIORES',
				sortable: true,	align: 'center',
				width: 180, resizable: true,
				align: 'center',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
					return record.get('CG_TIPOTASA')!='O' ? 'N/A' : record.get('DATOS_ANTERIORES');
				}
			},{
				header: 'Datos Actuales', tooltip: 'Datos Actuales',
				dataIndex: 'DATOS_ACTUALES',
				sortable: true,	align: 'center',
				width: 180, resizable: true,
				align: 'center',
				renderer: function(value, metaData, record, rowIdx, colIdx, store, view){
					return record.get('CG_TIPOTASA')!='O' ? 'N/A' : record.get('DATOS_ACTUALES');
				}
			}],
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 433,
		width: 806,
		colunmWidth: true,
		frame: true,
		style: 'margin:0 auto;',// para centrar el grid
		collapsible: true,
		bbar: {
		xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			displayInfo: true,
			id: 'barraPaginacion',
			store: consultaDataGrid,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					disabled : true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpForma = Ext.getCmp('forma');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
						Ext.Ajax.request({
							url: '13BiTasaPredeNegoext.data.jsp',
							params: Ext.apply(paramSubmit,{
								operacion:'XLS',
								informacion: 'Consulta',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				}
			]
		}
	};
	
	
	/*** COMBOS ***/
	var comboTasa = {
		xtype: 'combo',
		name:'lsTipoTasa',
		id:'id_lsTipoTasa',
		hiddenName:'lsTipoTasa',
		fieldLabel:'Tipo de Tasa',
		emptyText:'- Seleccione -',
		store:catalogoTasa,
		mode:'local',
		displayField:'descripcion',
		valueField:'clave',
		triggerAction:'all',
		editable : false,
		anchor: '50%',
		listeners:{
			select: function(self, model){
			
				Ext.getCmp("suceptibleFloating").hide();
				Ext.getCmp('aplicaFloating2').setValue(false);
				Ext.getCmp('aplicaFloating1').setValue(false);
				if(self.value==='P'){
					Ext.getCmp("suceptibleFloating").show();								
				}
							
				Ext.getCmp("id_noIc_epo").reset();
				catalogoNombreEPO.load({params:{informacion:'catalogoNombreEPO',lsTipoTasa: self.getValue()}});
		  }
		}
	}
	var comboEpo  = {
		xtype:'combo',
		name:'noIc_epo',
		hiddenName:'noIc_epo',
		id:'id_noIc_epo',
		disabled: false,
		fieldLabel:'Epo Relacionada',
		emptyText:'- Seleccione -',
		store:catalogoNombreEPO,
		model: 'local',
		displayField:'descripcion',
		valueField:'clave',
		width: 300,
		triggerAction: 'all',
		editable:false,
		listeners: {
			select: function(self, model){
				Ext.Ajax.request({
					waitMsg: 'Please Wait',
					url:'13BiTasaPredeNegoext.data.jsp',
					method:'POST',
					callback: successAjaxFn,
					params:{operacion:'pymeNombre', txt_nafelec: Ext.getCmp('txt_nafelec').getValue(), ic_pyme:Ext.getCmp('ic_pyme').getValue(), noIc_epo:Ext.getCmp('id_noIc_epo').getValue()}
				});
			}
		}
	}	
	var comboMoneda = {
		xtype:'combo',
		name:'lstMoneda',
		hiddenName:'lstMoneda',
		id:'id_lstMoneda',
		fieldLabel:'Moneda',
		emptyText:'- Seleccione -',
		store: catalogoMoneda,
		model: 'local',
		displayField:'descripcion',
		valueField:'clave',
		anchor: '50%',
		triggerAction: 'all',
		editable:false
	}
	var comboEstatus = {
		xtype: 'combo',
		name:'estatus',
		id:'id_estatus',
		hiddenName:'estatus',
		fieldLabel:'Estatus',
		emptyText:'- Seleccione -',
		store:catalogoEstatus,
		mode:'local',
		displayField:'descripcion',
		valueField:'clave',
		triggerAction:'all',
		editable : false,
		anchor: '50%'
	}
	var elementosFecha = [
	
	
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Registro',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_registro_de',
					id: 'df_fecha_registro_de',
					allowBlank: true,
					startDay: 0,
					width: 190,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_registro_a',					
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 30
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_registro_a',
					id: 'df_fecha_registro_a',
					allowBlank: true,
					startDay: 1,
					width: 190,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_registro_de',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}
	];
	var elementosFloating = [
		{  
			xtype:  'radiogroup',   
			fieldLabel: "Aplica Floating",    
			name: 'suceptibleFloating',   
			id: 'suceptibleFloating',  
			align: 'center',
			hidden: true,
			width: 100,							
				items: [						
					{ 
						boxLabel:    "Si",             
						name:        'aplicaFloating', 
						id: 'aplicaFloating1',
						inputValue:  "S" ,
						value: 'S',
						checked: false,
						width: 10
					}, 				
					{           
						boxLabel: "No",             
						name: 'aplicaFloating', 
						id: 'aplicaFloating2',
						inputValue:  "N", 
						value: 'N',							
						width: 10						
					} 				
				]
			}
		];
	
	function verificaPyme() {
		Ext.Ajax.request({
			waitMsg: 'Please Wait',
			url:'13BiTasaPredeNegoext.data.jsp',
			method:'POST',
			callback: successAjaxFn,
			params:{informacion:'pymeNombre', txt_nafelec: Ext.getCmp('txt_nafelec').getValue(), ic_pyme:Ext.getCmp('ic_pyme').getValue(), noIc_epo:Ext.getCmp('id_noIc_epo').getValue()}
		});
	}
	var elementosForma = {
		xtype: 'compositefield',
		width: '800',
		fieldLabel: 'Proveedor',
		labelWidth: 200,
		items:[{
			xtype:'textfield',
			name:'txt_nafelec',
			id:'txt_nafelec',
			width:50,
			listeners:{
				'blur': function(){
					verificaPyme();
				}
			}
		},{
			xtype:'textfield',
			readOnly: true,
			id:'hid_nombre',
			name:'hid_nombre',
			hiddenName:'hid_nombre',
			mode: 'local',
			resizable:true,
			triggerAction:'all',
			width:245
		},{
			xtype:'hidden',
			readOnly: true,
			id:'txt_nombre',
			name:'txt_nombre',
			hiddenName:'txt_nombre',
			mode: 'local',
			resizable:true,
			triggerAction:'all',
			width:45
		},{
			xtype:'hidden',
			readOnly: true,
			id:'ic_pyme',
			name:'ic_pyme',
			hiddenName:'ic_pyme',
			mode: 'local',
			resizable:true,
			triggerAction:'all',
			width:45
		},{
			xtype:'button',
			text:'B�squeda Avanzada',
			id:'btnAvanzada',
			iconCls: 'icoBuscar',
			handler: function(boton,evento){            
            var winBuscaA = Ext.getCmp('winBuscaA');
            var fpWinBusca = Ext.getCmp('fpWinBusca');
            var fpWinBuscaB = Ext.getCmp('fpWinBuscaB');
            fpWinBusca.getForm().reset();
            fpWinBuscaB.getForm().reset();
            winBuscaA.show();
			}
		}]
	}
	
   var winBuscaA = new Ext.Window({
      id:'winBuscaA',
      x:300, 
		y:100,
      width:600,
		heigth:100,
      modal:true,
		resizable: false,
      closeAction:'hide',
      title:'B�squeda Avanzada',
      items:[{
         xtype:'form',
         id:'fpWinBusca',
         frame: true,
         border: false,
         style: 'margin: 0 auto',
         bodyStyle:'padding:10px',
         defaults: {
            msgTarget: 'side',
            anchor: '-20'
         },
         labelWidth: 140,
         items:[{
            xtype:'displayfield',
            frame:true,
            border: false,
            value:'Utilice el * para b�squeda gen�rica'
         },{
            xtype:'displayfield',
            frame:true,
            border: false,
            value:''
         },{
            xtype: 'textfield',
            name: 'nombre_pyme',
            id:	'txtNombre',
            fieldLabel:'Nombre',
            maxLength:	100
         },{
            xtype: 'textfield',
            name: 'rfc_pyme',
            id:	'txtRfc',
            fieldLabel:'RFC',
            maxLength:	20,
            anchor: '55%'
         },{
            xtype: 'textfield',
            name: 'num_pyme',
            id:	'num_pyme',
            fieldLabel:'N�mero Proveedor',
            maxLength:	20,
            anchor: '55%'
         }],
         buttons:[{
            text:'Buscar',
            iconCls:'icoBuscar',
            handler: function(boton) {
               /*if ( Ext.isEmpty(Ext.getCmp('txtNombre').getValue()) && Ext.isEmpty(Ext.getCmp('txtRfc').getValue()) && Ext.isEmpty(Ext.getCmp('txtNe').getValue()) ){
                  Ext.Msg.alert(boton.text,'No existe informaci�n con los criterios determinados');
                  return;
               }	*/
               catalogoNombreData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
            }
         },{
            text:'Cancelar',
            iconCls: 'icoLimpiar',
            handler: function() {
               Ext.getCmp('winBuscaA').hide();
            }
         }]
      },{
         xtype:'form',
         frame: true,
         id:'fpWinBuscaB',
         style: 'margin: 0 auto',
         bodyStyle:'padding:10px',
         monitorValid: true,
         defaults: {
            msgTarget: 'side',
            anchor: '-20'
         },
         items:[{
            xtype: 'combo',
            id:	'id_cmb_num_ne',
            name: 'cmb_num_ne',
            hiddenName : 'cmb_num_ne',
            fieldLabel: 'Nombre',
            emptyText: 'Seleccione . . .',
            displayField: 'descripcion',
            valueField: 'clave',
            triggerAction : 'all',
            forceSelection:true,
            allowBlank: false,
            typeAhead: true,
            mode: 'local',
            minChars : 1,
            store: catalogoNombreData,
            tpl : NE.util.templateMensajeCargaCombo,
            listeners: {
               select: function(combo, record, index) {
                  busqAvanzadaSelect=index;
               }
            }
         }],
         buttons:[{
            text:'Aceptar',
            iconCls:'aceptar',
            formBind:true,
            handler: function() {
               if (!Ext.isEmpty(Ext.getCmp('id_cmb_num_ne').getValue())){
                  var reg = Ext.getCmp('id_cmb_num_ne').getStore().getAt(busqAvanzadaSelect).get('ic_pyme');
                  Ext.getCmp('ic_pyme').setValue(reg);
                  var disp = Ext.getCmp('id_cmb_num_ne').lastSelectionText;
                  var desc = disp.slice(disp.indexOf(" ")+1);
                  Ext.getCmp('txt_nafelec').setValue(Ext.getCmp('id_cmb_num_ne').getValue());
                  Ext.getCmp('hid_nombre').setValue(desc);
                  Ext.getCmp('winBuscaA').hide();
						verificaPyme();
               }
            }
         },{
            text:'Cancelar',
            iconCls: 'icoLimpiar',
            handler: function() {	Ext.getCmp('winBuscaA').hide();	}
         }]
      }]
   });
   
   
   
	/**** fp ***/
	var fp = {
		xtype: 'form',
		id: 'forma',
		title: 'Bit�cora de Tasas Preferenciales / Negociadas',
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		width: 585,
		titleCollapse: false,
		bodyStyle: 'padding: 30px 6px 6px 6px;',
      monitorValid:true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[comboTasa,comboEpo,comboMoneda,comboEstatus,elementosForma,elementosFecha, elementosFloating],
		buttons: [{
			text: 'Consultar',
			id: 'btnConsultar',
			iconCls: 'icoBuscar',
			handler: function(boton, evento) {
           
				var cmpForma = Ext.getCmp('forma');
            var df_fecha_registro_de   =  Ext.getCmp('df_fecha_registro_de');
            var df_fecha_registro_a    =  Ext.getCmp('df_fecha_registro_a');
          
				
				if(  ( !Ext.isEmpty(df_fecha_registro_de.getValue()) &&  Ext.isEmpty(df_fecha_registro_a.getValue()) )   
				|| ( Ext.isEmpty(df_fecha_registro_de.getValue()) &&  !Ext.isEmpty(df_fecha_registro_a.getValue()) )  ){
					df_fecha_registro_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
					df_fecha_registro_de.focus();
					df_fecha_registro_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
					df_fecha_registro_a.focus();
					return;					
				}
				
				if(! Ext.isEmpty(df_fecha_registro_de.getValue()) &&  !Ext.isEmpty(df_fecha_registro_a.getValue())){								
						
					var fechaIni_ = Ext.util.Format.date(df_fecha_registro_de.getValue(),'d/m/Y');
					var fechaFin_ = Ext.util.Format.date(df_fecha_registro_a.getValue(),'d/m/Y');					
					if(datecomp(fechaIni_,fechaFin_)==1) {
						df_fecha_registro_de.markInvalid("La fecha de este campo debe de ser anterior a."+fechaIni_);
						df_fecha_registro_de.focus();
						return;
					}
				}
				
              // Ext.getCmp('grid').show;
               paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
               consultaDataGrid.load({
                  params: Ext.apply(paramSubmit,{
                     operacion: 'Generar',
                     start:0,
                     limit:15							
                  })
               });
            
			} //fin handler
		},			
		{
			text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13BiTasaPredeNegoext.jsp';
				}
			}
		]
	};
	/*** fin fp ***/
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [fp,NE.util.getEspaciador(10),grid]
	}).show();

});