<%@ page contentType="application/json;charset=UTF-8" import="
		java.sql.*,
		java.util.*,
		netropology.utilerias.*,
		com.netro.descuento.*,
		net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%
	JSONObject jsonObj = new JSONObject();
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");

	LimitesPorEPO beanLimites = ServiceLocator.getInstance().lookup("LimitesPorEPOEJB", LimitesPorEPO.class);

	String  usuarioBloqueo = strLogin +"-"+strNombreUsuario;
	String  dependenciaBloqueo = strNombre;
	
	// ----- Variables de seguridad -----
	String pkcs7 = request.getParameter("Pkcs7");
	String folioCert = "";
	String externContent = request.getParameter("TextoFirmado");
	char getReceipt = 'Y';
	String acuseRecibo = "";
	
	// ----- Autentificación -----
	Seguridad s = new Seguridad();
	Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		folioCert = acuse.toString();
		
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
				
			acuseRecibo = s.getAcuse();
			
			List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
			//System.out.println(arrRegistrosModificados);
			
			beanLimites.bactualizaLimitesExt(arrRegistrosModificados, iNoCliente, iNoUsuario, acuseRecibo, usuarioBloqueo, dependenciaBloqueo);

			jsonObj.put("success", new Boolean(true));
		} else {	//autenticación fallida
			String _error = s.mostrarError();
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
		}
	}
%>
<%=jsonObj%>