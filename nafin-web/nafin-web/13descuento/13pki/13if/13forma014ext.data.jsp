<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	com.netro.mandatodoc.*,	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String tipoTasa = (request.getParameter("tipo_tasa")!=null)?request.getParameter("tipo_tasa"):"";
String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
String ic_mandante = (request.getParameter("ic_mandante")!=null)?request.getParameter("ic_mandante"):"";
String txtPuntos = (request.getParameter("txtPuntos")!=null)?request.getParameter("txtPuntos"):"";
String lstPlazo = (request.getParameter("lstPlazo")!=null)?request.getParameter("lstPlazo"):"";
String [] tasas		= request.getParameterValues("tasas");
String [] ValorTasas = request.getParameterValues("valorTasa");
String acuse2 = (request.getParameter("acuse2")!=null)?request.getParameter("acuse2"):"";
String fechaCarga = (request.getParameter("fechaCarga")!=null)?request.getParameter("fechaCarga"):"";
String horaCarga = (request.getParameter("horaCarga")!=null)?request.getParameter("horaCarga"):"";
String recibo = (request.getParameter("recibo")!=null)?request.getParameter("recibo"):"";


double ldPuntos 		= 0, ldValorTotalTasa = 0,  ldTotal 	= 0;
String  ic_if =iNoCliente,  infoRegresar ="", lsCveEpo  ="", lsNombre ="",  lsCalificacion ="", 
lsCadenaTasa ="",  lsTemp ="",  lsPlazo	= "", consulta = "", lsValorTasaN  ="", tasasS ="", 
lsFecha ="";


System.out.println("ic_epo "+ic_epo);
System.out.println("tipoTasa "+tipoTasa);
System.out.println("ic_moneda "+ic_moneda);
System.out.println("ic_mandante "+ic_mandante);
System.out.println("txtPuntos "+txtPuntos);
System.out.println("lstPlazo "+lstPlazo);
System.out.println("ic_if "+ic_if);

int numRegistros  =0;

if (txtPuntos.equals(""))
	txtPuntos = "0";
	
ldPuntos = Double.parseDouble(txtPuntos);

Vector lovDatos 	= null;
Vector lovRegistro 	= null;


StringBuffer contenidoArchivo = new StringBuffer("");
CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = null;
	
HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

AutorizacionTasas BeanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
	



if (informacion.equals("catalogoTipoTasa")  ) {

	for(int i=0; i<2; i++){
		datos = new HashMap();
		if(i==0) {
			datos.put("clave", "P");
			datos.put("descripcion", "PREFERENCIAL");	
			registros.add(datos);
		}
		if(i==1) {
			datos.put("clave", "N");
			datos.put("descripcion", "NEGOCIADA");	
			registros.add(datos);
		}
	}
		
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
}else  if (informacion.equals("catalogoEPO")  ) {
	
	CatalogoEPODescTasas cat = new CatalogoEPODescTasas();
	cat.setCampoClave("cie.ic_epo");
	cat.setCampoDescripcion("ce.cg_razon_social");
	cat.setClaveIF(ic_if);	
	cat.setParametro("PUB_EPO_OPERA_MANDATO");
	cat.setOrden("ce.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("catalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	


}else if (informacion.equals("CatalogoMandante") && !ic_epo.equals("")) {

	CatalogoMandante catalogo = new CatalogoMandante();
	catalogo.setCampoClave("ic_mandante ");
	catalogo.setCampoDescripcion("cg_razon_social");	
	catalogo.setClaveEpo(ic_epo);	
	catalogo.setTipoAfiliado("M");
	catalogo.setOrden("cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();	


}else if (informacion.equals("catalogoPlazo") ) {

	CatalogoPlazos catalogo = new CatalogoPlazos();
	catalogo.setCampoClave("ic_plazo ");
	catalogo.setCampoDescripcion("cg_descripcion");	
	catalogo.setClaveProducto("1");	
	catalogo.setOrden("in_plazo_dias");	
	infoRegresar = catalogo.getJSONElementos();	


}else if (informacion.equals("Consultar")   ) {

	if(tipoTasa.equals("P")) {
		lovDatos = BeanTasas.procesarTasaPreferencialMandante(ic_if, ic_moneda, ic_epo, ic_mandante, ldPuntos);
		for (int i= 0; i < lovDatos.size(); i++) {
			lovRegistro = (Vector) lovDatos.elementAt(i);
			numRegistros++;
			lsCveEpo  		= lovRegistro.elementAt(0).toString();
			lsNombre 		= lovRegistro.elementAt(1).toString();
			lsCalificacion 	= lovRegistro.elementAt(2).toString();
			lsCadenaTasa 	= lovRegistro.elementAt(3).toString();
			ldValorTotalTasa= ((Double)lovRegistro.elementAt(4)).doubleValue();
			lsTemp 			= lovRegistro.elementAt(5).toString();
			lsPlazo			= (lovRegistro.elementAt(6)==null)?"":lovRegistro.elementAt(6).toString();
			ldTotal 		= ((Double)lovRegistro.elementAt(7)).doubleValue();
			
			 tasasS = lsCveEpo+"|"+ldPuntos+"|"+lsPlazo;
			
		
			datos = new HashMap();				
			datos.put("EPO_RELACIONADA", lsNombre);
			datos.put("REFERENCIA", lsCalificacion);
			datos.put("TIPO_TASA", lsCadenaTasa);
			datos.put("VALOR", Comunes.formatoDecimal(ldValorTotalTasa,2) );
			datos.put("REL_MAT", "-");
			datos.put("PUNTOS", Double.toString (ldPuntos) );		
			datos.put("VALOR_TASA",  Double.toString (ldTotal));	
			datos.put("TIPO", "P");
			datos.put("TASA_SELEC", tasasS);
			
			registros.add(datos);			
		}
	}
	
	if(tipoTasa.equals("N")) {
		lovDatos = BeanTasas.procesarTasaNegociadaMandante(ic_if, ic_moneda, ic_epo, ic_mandante, lstPlazo);
		for (int i= 0; i < lovDatos.size(); i++) {
			lovRegistro = (Vector) lovDatos.elementAt(i);
			lsCveEpo  		= lovRegistro.elementAt(0).toString();
			lsNombre 		= lovRegistro.elementAt(1).toString();
			lsCalificacion 	= lovRegistro.elementAt(2).toString();
			lsCadenaTasa 	= lovRegistro.elementAt(3).toString();
			ldValorTotalTasa = ((Double) lovRegistro.elementAt(4)).doubleValue();
			lsValorTasaN 	= lovRegistro.elementAt(5).toString();
			lsFecha 		= lovRegistro.elementAt(6).toString();
			lsTemp 			= lovRegistro.elementAt(7).toString();
			lsPlazo			= lovRegistro.elementAt(8).toString(); 
			tasasS = 	lsCveEpo+"|"+lsFecha+"|"+lsPlazo;
			datos = new HashMap();				
			datos.put("EPO_RELACIONADA", lsNombre);
			datos.put("REFERENCIA", lsCalificacion);
			datos.put("TIPO_TASA", lsCadenaTasa);
			datos.put("VALOR", Comunes.formatoDecimal(ldValorTotalTasa,2) );			
			datos.put("FECHA", lsFecha);		
			datos.put("VALOR_TASA",  lsValorTasaN);	
			datos.put("TIPO", "N");
			datos.put("TASA_SELEC", tasasS);
			registros.add(datos);		
			
		}			
	}		
			
	consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	jsonObj.put("tipoTasa",tipoTasa);
	infoRegresar  = jsonObj.toString();

}else if (informacion.equals("GuardarTasas")   ) {
	
	String folioCert = "",  _acuse = "", mensajeAutentificacion ="", fechaHoy  ="", horaHoy ="";
	String pkcs7 = request.getParameter("pkcs7");	
	String externContent = request.getParameter("textoFirmar");
	char getReceipt = 'Y';
	Seguridad s = new Seguridad();
	Acuse acuse = null;
	
	fechaHoy = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
	horaHoy= (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
	
		
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		 acuse = new Acuse(Acuse.ACUSE_IF,"1");
		folioCert = acuse.toString();		
		
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			_acuse = s.getAcuse();
			
			
			BeanTasas.guardarTasasMandato(ic_if, ic_moneda, ic_mandante, tipoTasa, tasas, ValorTasas,acuse.toString(),iNoUsuario,_acuse);
			
			mensajeAutentificacion = "<b>La autentificación se llevo a cabo con exito  <b>Recibo:"+_acuse+"</b>";
		
		}else { //autenticación fallida
			mensajeAutentificacion = "<b>La autentificación no se llevo a cabo </b>";
		}
	} else { //autenticación fallida
		mensajeAutentificacion = "<b>La autentificación no se llevo a cabo </b>";
	}	
	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensajeAutentificacion",mensajeAutentificacion);
	if(acuse !=null)jsonObj.put("acuse",acuse.formatear());	
	if(acuse ==null)jsonObj.put("acuse","N");	
	jsonObj.put("recibo",_acuse);
	if(acuse !=null) jsonObj.put("acuse2",acuse.toString());
	jsonObj.put("fecha",fechaHoy);
	jsonObj.put("hora",horaHoy);
	jsonObj.put("usuario",iNoUsuario+" - "+strNombreUsuario);	
	infoRegresar  = jsonObj.toString();


}else if (informacion.equals("ConsultarAcuse")   ) {

	lovDatos = BeanTasas.consultaTasasMandato(ic_epo,  ic_if,  ic_moneda,  ic_mandante ,  tipoTasa,  lstPlazo , acuse2  );
	
	System.out.println("lovDatos "+lovDatos);
	
	for (int i= 0; i < lovDatos.size(); i++) {
		numRegistros++;
		lovRegistro = (Vector) lovDatos.elementAt(i);	
		String lsNombreEPO 		= lovRegistro.elementAt(0).toString();
		String lsNombrePYME  		= lovRegistro.elementAt(1).toString();
		lsCalificacion  		= lovRegistro.elementAt(2).toString();
		lsCadenaTasa  		= lovRegistro.elementAt(3).toString();
		lsPlazo  		= lovRegistro.elementAt(4).toString();
		String ldValorTotalTasas  		= lovRegistro.elementAt(5).toString();
		String ldValorTasa  		= lovRegistro.elementAt(6).toString();
		
		datos = new HashMap();		
		datos.put("EPO_RELACIONADA", lsNombreEPO);
		datos.put("REFERENCIA", lsCalificacion);
		datos.put("NOMBRE_PYME", lsNombrePYME);
		datos.put("TIPO_TASA", lsCadenaTasa);
		datos.put("PLAZO", lsPlazo);
		datos.put("VALOR", ldValorTotalTasas);
		datos.put("VALOR_TASA", ldValorTasa);
		registros.add(datos);				
	
	}

	consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar  = jsonObj.toString();

} else if (informacion.equals("ArchivoPDF")   ) {
	
	lovDatos = BeanTasas.consultaTasasMandato(ic_epo,  ic_if,  ic_moneda,  ic_mandante ,  tipoTasa,  lstPlazo , acuse2  );
	

	nombreArchivo = archivo.nombreArchivo()+".pdf";	
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
			
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
											session.getAttribute("iNoNafinElectronico").toString(),
											(String)session.getAttribute("sesExterno"),
											(String) session.getAttribute("strNombre"),
											(String) session.getAttribute("strNombreUsuario"),
											(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				
	pdfDoc.addText("   ","formasB",ComunesPDF.CENTER);
				
	pdfDoc.setTable(2,65);
	pdfDoc.setCell("Recibo Número :   "+recibo,"celda01",ComunesPDF.CENTER,2);
	pdfDoc.setCell("Número de Acuse","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(acuse2,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Fecha de Autorización","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(fechaCarga,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Hora de Autorización","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(horaCarga,"formas",ComunesPDF.CENTER);
	pdfDoc.setCell("Usuario de Captura","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell(iNoUsuario+" - "+strNombreUsuario,"formas",ComunesPDF.CENTER);
	pdfDoc.addTable();
				
	pdfDoc.setTable(7,90); 
	pdfDoc.setCell("EPO que opera Factoraje con Mandato","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Mandante","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo Tasa","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER);
	pdfDoc.setCell("Valor Tasa","celda01",ComunesPDF.CENTER);
	
	for (int i= 0; i < lovDatos.size(); i++) {
		numRegistros++;
		lovRegistro = (Vector) lovDatos.elementAt(i);	
		String lsNombreEPO 		= lovRegistro.elementAt(0).toString();
		String lsNombrePYME  		= lovRegistro.elementAt(1).toString();
		lsCalificacion  		= lovRegistro.elementAt(2).toString();
		lsCadenaTasa  		= lovRegistro.elementAt(3).toString();
		lsPlazo  		= lovRegistro.elementAt(4).toString();
		String ldValorTotalTasas  		= lovRegistro.elementAt(5).toString();
		String ldValorTasa  		= lovRegistro.elementAt(6).toString();
				
		pdfDoc.setCell(lsNombreEPO,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsNombrePYME,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsCalificacion,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsCadenaTasa,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(lsPlazo,"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(Comunes.formatoDecimal(ldValorTotalTasas,2),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(ldValorTasa,"formas",ComunesPDF.CENTER);
	
	}
	pdfDoc.addTable();
	pdfDoc.endDocument();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar  = jsonObj.toString();


}else if (informacion.equals("ArchivoCSV")   ) {

	lovDatos = BeanTasas.consultaTasasMandato(ic_epo,  ic_if,  ic_moneda,  ic_mandante ,  tipoTasa,  lstPlazo , acuse2  );
	
	contenidoArchivo.append(
		"Recibo Número:,"+recibo+
		"\nNúmero de Acuse,"+acuse2+
		"\nFecha de Autorización,"+fechaCarga+
		"\nHora de Autorización,"+horaCarga+
		"\nUsuario de Captura,"+strNombreUsuario.replace(',',' ')+
		"\n,\n,\n"+
		"EPO que opera Factoraje con Mandato,Mandante,Referencia,Tipo Tasa,Plazo,Valor,Valor Tasa\n");


	for (int i= 0; i < lovDatos.size(); i++) {
		numRegistros++;
		lovRegistro = (Vector) lovDatos.elementAt(i);	
		String lsNombreEPO 		= lovRegistro.elementAt(0).toString();
		String lsNombrePYME  		= lovRegistro.elementAt(1).toString();
		lsCalificacion  		= lovRegistro.elementAt(2).toString();
		lsCadenaTasa  		= lovRegistro.elementAt(3).toString();
		lsPlazo  		= lovRegistro.elementAt(4).toString();
		String ldValorTotalTasas  		= lovRegistro.elementAt(5).toString();
		String ldValorTasa  		= lovRegistro.elementAt(6).toString();
				
		contenidoArchivo.append(
			lsNombreEPO.replace(',',' ')+","+
			lsNombrePYME.replace(',',' ')+","+
			lsCalificacion.replace(',',' ')+","+
			lsCadenaTasa.replace(',',' ')+","+
			lsPlazo.replace(',',' ')+","+
			Comunes.formatoDecimal(ldValorTotalTasas,4).replace(',',' ')+","+
			ldValorTasa.replace(',',' ')+"\n");
		}
		
		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}	
		infoRegresar  = jsonObj.toString();  
}	

%>
<%=infoRegresar%>

