<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	java.io.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.nafin.descuento.ws.*,
	netropology.utilerias.*, 
	org.apache.commons.logging.Log" 
	errorPage="/00utils/error_extjs.jsp" 
%>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String nombreArchivo = (request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";
String n_documentos = (request.getParameter("n_documentos")!=null)?request.getParameter("n_documentos"):"";
String doctosEstatus2 = (request.getParameter("doctosEstatus2")!=null)?request.getParameter("doctosEstatus2"):"";
String doctosEstatus4 = (request.getParameter("doctosEstatus4")!=null)?request.getParameter("doctosEstatus4"):"";
String lDocumentos = (request.getParameter("lDocumentos")!=null)?request.getParameter("lDocumentos"):"";
String regtotales[]	 = request.getParameterValues("regtotales");
String cifrasControl[] = request.getParameterValues("cifrasControl");
String total2 = (request.getParameter("total2")!=null)?request.getParameter("total2"):"0";
String total4 = (request.getParameter("total4")!=null)?request.getParameter("total4"):"0";

if(!doctosEstatus2.equals("")){
	doctosEstatus2 = doctosEstatus2.substring(0,doctosEstatus2.length()-1);
}
if(!doctosEstatus4.equals("")){
	doctosEstatus4 = doctosEstatus4.substring(0,doctosEstatus4.length()-1);
}


String infoRegresar ="", consulta ="", documentos ="";
JSONObject 	jsonObj	= new JSONObject();
StringBuffer leyenda0 = new StringBuffer();
StringBuffer leyenda1 = new StringBuffer();
StringBuffer leyenda2 = new StringBuffer();
StringBuffer leyenda3 = new StringBuffer();

if(!n_documentos.equals("")){
	documentos = n_documentos.substring(0,n_documentos.length()-1);
}
//--Paginador  
ConsConfirmacionDoctos paginador = new ConsConfirmacionDoctos ();
paginador.setTipoConsulta(informacion);
paginador.setIc_documentos(documentos);
paginador.setDoctosEstatus2(doctosEstatus2);
paginador.setDoctosEstatus4(doctosEstatus4); 
paginador.setRegtotales(regtotales);
paginador.setCifrasControl(cifrasControl);


CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
 
if (informacion.equals("CargaArchivo")) {

	HashMap  registros =  paginador.cargaArchivoConfDocto(strDirectorioTemp, nombreArchivo, iNoCliente  );
	 
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("REG_ERROR", registros.get("REG_ERROR").toString());
	jsonObj.put("REG_BIEN", registros.get("REG_BIEN").toString());
	jsonObj.put("MENSAJE", registros.get("MENSAJE").toString());
	jsonObj.put("totalRegistros", registros.get("numLinea").toString());
	jsonObj.put("n_documentos", registros.get("n_documentos").toString());
	jsonObj.put("doctosEstatus4", registros.get("doctosEstatus4").toString());
	jsonObj.put("doctosEstatus2", registros.get("doctosEstatus2").toString());
	jsonObj.put("lDocumentos", registros.get("lDocumentos").toString());	
	jsonObj.put("total2", registros.get("total2").toString());	
	jsonObj.put("total4", registros.get("total4").toString()); 
	infoRegresar = jsonObj.toString(); 	
	
}else  if (informacion.equals("DescargaArchivoError")) {

	String datosErrores = (request.getParameter("datosErrores")!=null)?request.getParameter("datosErrores"):"";
	String datos =  datosErrores.replaceAll("<br>", "\n") ;

	nombreArchivo =paginador.generacionArcErrores( strDirectorioTemp,  datos);		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	infoRegresar = jsonObj.toString(); 
	
} else if (informacion.equals("ConsultarPreAcuse") || informacion.equals("ConsultarPreAcuse2") 
			|| informacion.equals("consultaTotales1")   || informacion.equals("consultaTotales2")  ) {

	Registros reg	=	queryHelper.doSearch(); 
	
	int totalMN = 0, totalDL =0;
	BigDecimal montoTotalNacional = new BigDecimal(0.0);
	BigDecimal montoTotalDolar = new BigDecimal(0.0);
			
	if (informacion.equals("ConsultarPreAcuse")) {
	
		while(reg.next()){
			
			String ic_moneda = (reg.getString("IC_MONEDA") == null) ? "" : reg.getString("IC_MONEDA");
			String  monto = (reg.getString("MONTO") == null) ? "0" : reg.getString("MONTO");
			String ic_documento = (reg.getString("IC_DOCUMENTO") == null) ? "" : reg.getString("IC_DOCUMENTO");
			
			ArrayList aList= new ArrayList(Arrays.asList(lDocumentos.split(",")));			
		
			for (int i = 0; i <aList.size(); i++) { 
				String  regis=  aList.get(i).toString();
				VectorTokenizer vt = new VectorTokenizer(regis,"|");
				Vector lineaArchivo = vt.getValuesVector();
				
				String ic_docto = (String)lineaArchivo.get(0); 
				String claveEstatus = (String)lineaArchivo.get(1); 
				String causaRechazo = (String)lineaArchivo.get(2);
				
				if(ic_docto.equals(ic_documento)) {
					reg.setObject("CAUSA_RECHAZO", causaRechazo);	
					reg.setObject("ESTATUS_ASIGNAR", claveEstatus);			
				}				
			}
			
			if(ic_moneda.equals("1")) {
				totalMN++;
				montoTotalNacional = montoTotalNacional.add(new BigDecimal(monto));	
			}else  if(ic_moneda.equals("54")) {
				totalDL++;
				montoTotalDolar = montoTotalDolar.add(new BigDecimal(monto));	
			}
			
		}
	}
	
	leyenda1.append("<table align='center'  style='text-align: justify;' border='0' width='900' cellpadding='0' cellspacing='0'> "+
				" <tr> <td align='justify' class='formas'>"+
				" <br>Al aceptar realizar el factoraje electrónico o descuento electrónico de los derechos que solicita la mipyme, me obligo a su fondeo en los términos y condiciones establecidas con la mipyme. "+
				" En este acto me obligo a notificar a la empresa de primer orden mi calidad de titular de los derechos sobre el documento descontado o factoreado y reiterarle su obligación de pagarme en tiempo y "+
				" forma la cantidad a que se refiere el documento aquí descontado o factoreado. Las operaciones que aparecen en esta página fueron notificadas a la empresa de primer orden de conformidad y para efectos "+
				" de los artículos 427 de la Ley General de Títulos y Operaciones de Crédito, 32 C del Código Fiscal de la Federación y 2038 del Código Civil Federal según corresponda, para los efectos legales conducentes "+
				" </td></tr><table> <br>");
 	
	Vector vNombrePuesto = BeanAutDescuento.getNombreyPuesto(iNoCliente);		
			
	leyenda3.append("<table align='center'  style='text-align: center;' border='0' width='900' cellpadding='0' cellspacing='0'> "+
				" <tr> <td align='center' class='formas'>"+ 
				" <br><span style='color:blue;'><b>Facultado por  "+strNombre+"<b></span>"+
				" <br><span style='color:blue;'><b>"+(vNombrePuesto.size()==0?"":vNombrePuesto.get(1))+"   "+(vNombrePuesto.size()==0?"":vNombrePuesto.get(0))+"<b></span>"+
				" </td></tr><table> <br>");
	
	Vector vFechas = BeanAutDescuento.getFechasConvenioyDescuento(iNoCliente, "1");		
	leyenda2.append("<table align='center'  style='text-align: justify;' border='0' width='900' cellpadding='0' cellspacing='0'> "+
				" <tr> <td align='justify' class='formas'>"+				
				" <center> <span style='color:blue;'><b>Constancia de depósito de documentos a descontar Cadenas Productivas.</b> </span></center>  <br>" +				
				" <br>De conformidad con el convenio que este Banco "+strNombre+" celebró con Nacional Financiera, S.N.C., y en virtud del descuento electrónico de (los) documento (s) electrónico (s), en este acto cedemos a Nacional Financiera, S.N.C., los derechos de crédito que se derivan de los mismos. <br>"+
				" <br> Así mismo, bajo protesta de decir verdad, hacemos constar en este acto que para todos los efectos legales a que haya lugar nos asumimos como depositarios sin derecho a honorarios y asumiendo la responsabilidad civil y penal correspondiente al carácter de depositario judicial, del (los) documento (s) electrónico (s) en que se consignan "+
				" los derechos de crédito derivados de las operaciones de factoraje financiero electrónicas, celebradas con los clientes que se describen en el cuadro anexo en donde se especifica la fecha y lugar de presentación, importe total y nombre de la empresa acreditada. <br> "+
				" <br> La información del (los) "+totalMN+" documento (s) electrónico (s) en Moneda Nacional por un importe de $ "+Comunes.formatoDecimal(montoTotalNacional,2) +" y "+totalDL+" documento (s) electrónico (s) en Dólares por un importe de $ "+Comunes.formatoDecimal(montoTotalDolar,2) +" citado (s), está registrada en el sistema denominado Nafin Electrónico a disposición de Nacional Financiera, S.N.C., cuando ésta la requiera.<br>"+
				" <br> En términos del artículo 2044 del Código Civil para el Distrito Federal, la cesión a que se refiere el primer párrafo la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en que permanezcan vigentes los adeudos y hasta su liquidación total. <br>" +
				" <br> Así mismo  y en términos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro de (los) documento (s) electrónicos que se listan a continuación y que amparan los derechos de crédito y/o ejercer los derechos de cobro consignados en el (los) documento (s) electrónico (s) señalados conserven su valor y demás derechos que les correspondan. <br> "+
				" <br> "+
				" <b>Nombre del Intermediario Financiero: </b>"+strNombre+"  <br>" +
				" <b>Fecha del Convenio de Descuento Electrónico:</b> "+ vFechas.get(0).toString()+"  <br>"+
				" <b>Fecha del Descuento :</b> México D.F. "+ vFechas.get(1).toString()+" <br> " +
				" <b>Nota: </b> La Fecha de Operación es Informativa.  <br>"+
				" </td></tr><table> <br>");
				

	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj =  JSONObject.fromObject(consulta);
	jsonObj.put("leyenda1", leyenda1.toString());
	jsonObj.put("leyenda2", leyenda2.toString()); 
	jsonObj.put("leyenda3", leyenda3.toString()); 
	infoRegresar = jsonObj.toString();  
	
	
}else  if (informacion.equals("ConsultarTotales")) {
	
	Registros reg	=	queryHelper.doSearch(); 
	
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj =  JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();   
	
}else  if (informacion.equals("ConfirmaRegistros")) {

	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";
	String textoFirmado = (request.getParameter("textoFirmado")!=null)?request.getParameter("textoFirmado"):"";
	String ic_documentos[]		= request.getParameterValues("ic_documentos");	
	String mensajeError =  "", fechaHoy		= "",  horaCarga="", acuse = "", recibo ="";	
	try{
		fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		horaCarga = new java.text.SimpleDateFormat("HH:mm a").format(new java.util.Date());
	}catch(Exception e){
		fechaHoy = "";
		horaCarga = "";
	}
	
	ProcesoIFWSInfoV2 resultadoProceso = null;
	DocumentoIFWSV2[] docs = new DocumentoIFWSV2[ic_documentos.length];
	
	for(int i=0;i<ic_documentos.length;i++){
		String registro =  ic_documentos[i] ;
		
		VectorTokenizer vt = new VectorTokenizer(registro,"|");
		Vector lineaArchivo = vt.getValuesVector();
		
		docs[i]=new DocumentoIFWSV2();
		docs[i].setClaveDocumento((String)lineaArchivo.get(0));		
		docs[i].setClaveEstatus((String)lineaArchivo.get(1) );	
		docs[i].setCausaRechazo((String)lineaArchivo.get(2));	 
		docs[i].setAcusePyme((String)lineaArchivo.get(3) );	
		docs[i].setClaveBancoFondeo((String)lineaArchivo.get(4) );	
		docs[i].setClaveEpo((String)lineaArchivo.get(5) );	
		docs[i].setClaveMoneda((String)lineaArchivo.get(6) );	
		docs[i].setFechaEmision((String)lineaArchivo.get(7) );	
		docs[i].setFechaSeleccionPyme((String)lineaArchivo.get(8) );	
		docs[i].setFechaPublicacion((String)lineaArchivo.get(9) );	
		docs[i].setFechaVencimiento((String)lineaArchivo.get(10) );	
		docs[i].setMontoDescuento((String)lineaArchivo.get(11) );	
		docs[i].setTasaAceptada((String)lineaArchivo.get(12) );	
		docs[i].setImporteInteres((String)lineaArchivo.get(13) );	
		docs[i].setImporteRecibir((String)lineaArchivo.get(14) );	
		docs[i].setNombreEpo((String)lineaArchivo.get(15) );	
		docs[i].setNombreMoneda((String)lineaArchivo.get(16) );	
		docs[i].setNombrePyme((String)lineaArchivo.get(17) );	
		docs[i].setNumeroDocumento((String)lineaArchivo.get(18) );	
		docs[i].setNumeroSirac((String)lineaArchivo.get(19) );	
		docs[i].setTipoFactoraje((String)lineaArchivo.get(20) );	 
		docs[i].setTipoProceso("PantallaIF");		
	} //fin del for
	
	
	ProcesoIFWSInfoV2 procV2=BeanAutDescuento.confirmacionDoctosIFWSV2( strLogin, "",  iNoCliente,docs,pkcs7,_serial,"S");
	
	mensajeError = procV2.getResumenEjecucion();
	
			
	DocumentoIFWSV2[] arrDocumentosV2 = procV2.getDocumentos(); 
	if(arrDocumentosV2!=null){
		DocumentoIFWS[] arrDocumentos=new DocumentoIFWS[arrDocumentosV2.length];
		for(int i=0;i<arrDocumentosV2.length;i++){
			arrDocumentos[i]=new DocumentoIFWS();
			if(!arrDocumentosV2[i].getErrorDetalle().equals("")){
				mensajeError += arrDocumentosV2[i].getErrorDetalle()+"<br>";				
			}
		}		
	}	
	   
	acuse =  procV2.getAcuse();  
	recibo = procV2.getRecibo();  
	  
	System.out.println("recibo ===>"+recibo); 	
	System.out.println("acuse ===>"+acuse); 
	System.out.println("mensajeError ===>"+mensajeError);      
	
	if(!acuse.equals("") ) {
	
		leyenda0.append("<table align='center'  style='text-align: center;' border='0' width='900' cellpadding='0' cellspacing='0'> "+
					" <tr> <td align='center' class='formas'>"+ 
					" <b> La autentificación se llevó a cabo con éxito <br> <align='center'> <FONT SIZE=3>Recibo: "+recibo+" </font></align> </b>"+
					" </td></tr><table> <br>");
	}else  {
			leyenda0.append("<table align='center'  style='text-align: center;' border='0' width='900' cellpadding='0' cellspacing='0'> "+
					" <tr> <td align='center' class='formas'>"+ 
					" <b> "+mensajeError +" </b>"+
					" </td></tr><table> <br>");
	}
	  
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensajeError", mensajeError);	
	jsonObj.put("leyenda0", leyenda0.toString());		
	jsonObj.put("acuse", acuse);	
	jsonObj.put("fechaCarga", fechaHoy);	
	jsonObj.put("horaCarga", horaCarga);	
	jsonObj.put("usuarioCaptura",  iNoUsuario+" - "+strNombreUsuario);		
	jsonObj.put("recibo",  recibo);			
	infoRegresar = jsonObj.toString();  	

}else  if (informacion.equals("ArchivoAcuse")  || informacion.equals("ArchivoAcuse_Interface")) {
	
	String tipoArch = (request.getParameter("tipoArch")!=null)?request.getParameter("tipoArch"):"";
	
	try {
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArch);				
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo", e);
	}
	infoRegresar = jsonObj.toString(); 		

}


%>
<%=infoRegresar%>