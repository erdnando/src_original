<%@ page contentType=
		"application/json;charset=UTF-8"
		import=
		"com.jspsmart.upload.*, 
		net.sf.json.JSONArray,
		net.sf.json.JSONObject, 
		java.io.*, java.util.*, 
		com.netro.exception.*,
		com.netro.pdf.*,
		com.netro.descuento.*, netropology.utilerias.* "
		errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../../13secsession.jspf" %>
<%@ include file="../certificado.jspf" %>

<%
//>>CREA REPORTE PDF
CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = archivo.nombreArchivo()+".pdf";
//<<CREA REPORTE PDF
boolean validaPDF=false;

AccesoDB con = new AccesoDB();
String error = "";

String acuse = (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
String acuseFormateado = (request.getParameter("acuseFormateado")==null)?"":request.getParameter("acuseFormateado");
String fechaAcuse = (request.getParameter("fecha")==null)?"":request.getParameter("fecha");
String horaAcuse = (request.getParameter("hora")==null)?"":request.getParameter("hora");

try {
	con.conexionDB();
%>


<%
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	//ComunesPDF pdfDocExt = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

    pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                                 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		    					 (String)session.getAttribute("sesExterno"),
                                 (String) session.getAttribute("strNombre"),
                                    (String) session.getAttribute("strNombreUsuario"),
					    		 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

	double granTotalMontoMN = 0;
	double granTotalMontoDL = 0;
	int numTotalPrestamos = 0;
	String strSQL = 
			" SELECT EP.IC_ENCABEZADO_PAGO, " +
			" 	EP.IC_IF, EP.IG_SUCURSAL, " +
			" 	EP.IC_MONEDA, M.CD_NOMBRE as nombreMoneda, " +
			" 	TO_CHAR(EP.DF_PERIODO_FIN, 'DD/MM/YYYY') as DF_PERIODO_FIN, " +
			" 	TO_CHAR(EP.DF_PROBABLE_PAGO, 'DD/MM/YYYY') as DF_PROBABLE_PAGO, " +
			" 	TO_CHAR(EP.DF_DEPOSITO, 'DD/MM/YYYY') as DF_DEPOSITO, " +
			" 	EP.IG_IMPORTE_DEPOSITO, " +
			" 	EP.IC_FINANCIERA, F.CD_NOMBRE as nombreFinanciera " +
			" FROM com_encabezado_pago ep, comcat_moneda m, " +
			" 	comcat_financiera f " +
			" WHERE ep.ic_moneda = m.ic_moneda " +
			" 	AND ep.ic_financiera = f.ic_financiera " +
			" 	AND ep.cc_acuse = '" + acuse + "'" +
			" ORDER BY ic_encabezado_pago ";

	ResultSet rs = con.queryDB(strSQL);
	while (rs.next()) {	//Encabezado
		String claveEncabezado = rs.getString("IC_ENCABEZADO_PAGO");
		int claveMoneda = rs.getInt("IC_MONEDA");
		String sucursal = rs.getString("IG_SUCURSAL");
		String nombreMoneda = rs.getString("NOMBREMONEDA");
		String fechaVencimiento = rs.getString("DF_PERIODO_FIN");
		String fechaProbablePago = rs.getString("DF_PROBABLE_PAGO");
		String fechaDeposito = rs.getString("DF_DEPOSITO");
		String importeDeposito = rs.getString("IG_IMPORTE_DEPOSITO");
		String nombreFinanciera = rs.getString("NOMBREFINANCIERA");
		//pdfDocExt.setTable(1,100);
		int numColsEnc = 7;
		if("B".equals(strTipoBanco))
			numColsEnc--;
		pdfDoc.setTable(numColsEnc--,100);
		pdfDoc.setCell("Encabezado","formasmenB",ComunesPDF.LEFT,7);//celda01
		pdfDoc.setCell("Dirección estatal","formasmenB",ComunesPDF.CENTER);//celda02
		pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Banco servicio","formasmenB",ComunesPDF.CENTER);
		if("NB".equals(strTipoBanco))
			pdfDoc.setCell("Fecha vencimiento","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha probable de pago","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de depósito","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Importe de depósito","formasmenB",ComunesPDF.CENTER);

		pdfDoc.setCell(sucursal,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(nombreMoneda,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(nombreFinanciera,"formasmen",ComunesPDF.CENTER);
		if("NB".equals(strTipoBanco))
			pdfDoc.setCell(fechaVencimiento,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(fechaProbablePago,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(fechaDeposito,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(importeDeposito,2),"formasmen",ComunesPDF.CENTER);
		
		//pdfDocExt.setCell(pdfDoc.getTable());
		pdfDoc.addTable();
		//pdfDocExt.setCell("");
		int numColsDet = 12;
		if("B".equals(strTipoBanco))
			numColsDet = 15;
		pdfDoc.setTable(numColsDet,100);
		pdfDoc.setCell("Detalles","formasmenB",ComunesPDF.LEFT,numColsDet);
		if("NB".equals(strTipoBanco)) {
			pdfDoc.setCell("Subaplicación","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Préstamo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Clave SIRAC","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Capital","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Moratorios","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Subsidio","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Comisión","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("IVA","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Importe Pago","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Concepto pago","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Origen pago","formasmenB",ComunesPDF.CENTER);
		} else if("B".equals(strTipoBanco)) {
			pdfDoc.setCell("Sucursal","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Subaplicación","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Préstamo","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Acreditado","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Operación","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Pago","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Saldo Insoluto","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Tasa","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Capital","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Dias","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Interés","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Comisión","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("IVA","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Importe Pago","formasmenB",ComunesPDF.CENTER);
		}
		String strSQLDet = 
				" SELECT ig_subaplicacion, ig_prestamo, ig_cliente, fg_amortizacion, fg_interes,"   +
				"        fg_subsidio, fg_comision, fg_iva, fg_totalexigible, cg_concepto_pago,"   +
				"        cg_origen_pago, fg_interes_mora, ig_sucursal, cg_acreditado,"   +
				"        TO_CHAR (df_operacion, 'dd/mm/yyyy') AS df_operacion,"   +
				"        TO_CHAR (df_vencimiento, 'dd/mm/yyyy') AS df_vencimiento,"   +
				"        fn_saldo_insoluto, TO_CHAR (df_pago, 'dd/mm/yyyy') AS df_pago, ig_tasa,"   +
				"        ig_dias"   +
				"   FROM com_detalle_pago"  +
				" WHERE " +
				" 	ic_encabezado_pago = " + claveEncabezado + 
				" ORDER BY ic_encabezado_pago ";
		
		ResultSet rsDet = con.queryDB(strSQLDet);

		double dblTotalCapitalMN = 0;
		double dblTotalInteresesMN = 0;
		double dblTotalMoratoriosMN = 0;
		double dblTotalSubsidioMN = 0;
		double dblTotalComisionMN = 0;
		double dblTotalIVAMN = 0;
		double dblTotalImportePagoMN = 0;
		double dblTotalCapitalDL = 0;
		double dblTotalInteresesDL = 0;
		double dblTotalMoratoriosDL = 0;
		double dblTotalSubsidioDL = 0;
		double dblTotalComisionDL = 0;
		double dblTotalIVADL = 0;
		double dblTotalImportePagoDL = 0;
		
		double dblTotalSaldoInsolutoDL = 0;
		double dblTotalSaldoInsolutoMN = 0;

		
		while (rsDet.next()) {	//Detalle
			numTotalPrestamos++;
			String subaplicacion = rsDet.getString("ig_subaplicacion");
			String prestamo = rsDet.getString("ig_prestamo");
			String claveSiracCliente = rsDet.getString("ig_cliente");
			double capital = rsDet.getDouble("fg_amortizacion");
			double intereses = rsDet.getDouble("fg_interes");
			double moratorios = rsDet.getDouble("fg_interes_mora");
			double subsidio = rsDet.getDouble("fg_subsidio");
			double comision = rsDet.getDouble("fg_comision");
			double iva = rsDet.getDouble("fg_iva");
			double importePago = rsDet.getDouble("fg_totalexigible");
			
			String rs_sucursal = rsDet.getString("ig_sucursal");
			String rs_acreditado = rsDet.getString("cg_acreditado");
			String rs_fechaOperacion = rsDet.getString("df_operacion");
			String rs_fechaVenc = rsDet.getString("df_vencimiento");
			String rs_fechaPago = rsDet.getString("df_pago");
			double rs_saldoInsoluto = rsDet.getDouble("fn_saldo_insoluto");
			String rs_tasa = rsDet.getString("ig_tasa");
			String rs_dias = rsDet.getString("ig_dias");
			
			String conceptoPago = rsDet.getString("cg_concepto_pago")==null?"":rsDet.getString("cg_concepto_pago");
			String nombreConceptoPago = "";
			if (conceptoPago.equals("AN")) {
				nombreConceptoPago = "Anticipado";
			} else if (conceptoPago.equals("VE")) {
				nombreConceptoPago = "Vencimiento";
			} else if (conceptoPago.equals("CV")) {
				nombreConceptoPago = "Cartera Vencida";
			} else if (conceptoPago.equals("CO")) {
				nombreConceptoPago = "Comision";
			}
			
			String origenPago = rsDet.getString("cg_origen_pago")==null?"":rsDet.getString("cg_origen_pago");

			String nombreOrigenPago = "";
			if (origenPago.equals("A")) {
				nombreOrigenPago = "Acreditado";
			} else if (origenPago.equals("I")) {
				nombreOrigenPago = "Intermediario";
			}
			
			if (claveMoneda == 1) {
				dblTotalCapitalMN += capital;
				dblTotalInteresesMN += intereses;
				dblTotalMoratoriosMN += moratorios;
				dblTotalSubsidioMN += subsidio;
				dblTotalComisionMN += comision;
				dblTotalIVAMN += iva;
				dblTotalImportePagoMN += importePago;
				granTotalMontoMN += importePago;
				dblTotalSaldoInsolutoMN +=rs_saldoInsoluto;
			} else if (claveMoneda == 54) {
				dblTotalCapitalDL += capital;
				dblTotalInteresesDL += intereses;
				dblTotalMoratoriosDL += moratorios;
				dblTotalSubsidioDL += subsidio;
				dblTotalComisionDL += comision;
				dblTotalIVADL += iva;
				dblTotalImportePagoDL += importePago;
				granTotalMontoDL += importePago;
				dblTotalSaldoInsolutoDL +=rs_saldoInsoluto;
			}
			
			if("NB".equals(strTipoBanco)) {
				pdfDoc.setCell(subaplicacion,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(prestamo,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(claveSiracCliente,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(capital,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(intereses,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(moratorios,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(subsidio,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(comision,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(iva,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(importePago,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(nombreConceptoPago,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(nombreOrigenPago,"formasmen",ComunesPDF.LEFT);
			} else if("B".equals(strTipoBanco)){
				pdfDoc.setCell(rs_sucursal,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(subaplicacion,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(prestamo,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(rs_acreditado,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(rs_fechaOperacion,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(rs_fechaVenc,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(rs_fechaPago,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(rs_saldoInsoluto,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(rs_tasa,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(capital,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(rs_dias,"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(intereses,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(comision,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell(" "+Comunes.formatoDecimal(iva,2),"formasmen",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(importePago,2),"formasmen",ComunesPDF.LEFT);
			}
		} //fin de los detalles
		rsDet.close();
		con.cierraStatement();
		if("NB".equals(strTipoBanco)) {
			if (claveMoneda == 1) {
				pdfDoc.setCell("Total MN","formasmenB",ComunesPDF.LEFT,3);
				//pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
				//pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalCapitalMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalInteresesMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalMoratoriosMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalSubsidioMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalComisionMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalIVAMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalImportePagoMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER,2);
				//pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
			} else if (claveMoneda == 54) {
				pdfDoc.setCell("Total DL","formasmenB",ComunesPDF.LEFT,3);
				//pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
				//pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalCapitalDL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalInteresesDL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalMoratoriosDL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalSubsidioDL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalComisionDL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalIVADL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalImportePagoDL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
			}
		} else if("B".equals(strTipoBanco)) {
			if (claveMoneda == 1) {
				pdfDoc.setCell("Total MN","formasmenB",ComunesPDF.LEFT,3);
				pdfDoc.setCell("","formasmenB",ComunesPDF.LEFT,4);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalSaldoInsolutoMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("","formasmenB",ComunesPDF.LEFT,1);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalCapitalMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("","formasmenB",ComunesPDF.LEFT,1);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalInteresesMN,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("","formasmenB",ComunesPDF.LEFT,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalImportePagoMN,2),"formasmenB",ComunesPDF.LEFT);
			} else if (claveMoneda == 54) {
				pdfDoc.setCell("Total DL","formasmenB",ComunesPDF.LEFT,3);
				pdfDoc.setCell("","formasmenB",ComunesPDF.LEFT,4);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalSaldoInsolutoDL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("","formasmenB",ComunesPDF.LEFT,1);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalCapitalDL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("","formasmenB",ComunesPDF.LEFT,1);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalInteresesDL,2),"formasmenB",ComunesPDF.LEFT);
				pdfDoc.setCell("","formasmenB",ComunesPDF.LEFT,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalImportePagoDL,2),"formasmenB",ComunesPDF.LEFT);			}
		}
		//pdfDocExt.setCell(pdfDoc.getTable());
		//pdfDocExt.addTable();
		pdfDoc.addTable();
	}//fin de los encabezados
	rs.close();
	con.cierraStatement();
		//pdfDocExt.endDocument();
		
		pdfDoc.setTable(2,30);
		pdfDoc.setCell("Datos del Acuse","formasmenB",ComunesPDF.CENTER,2);		
		pdfDoc.setCell("Número de Acuse","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell(acuseFormateado,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell(fechaAcuse,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("Hora","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell(horaAcuse,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Préstamos","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(numTotalPrestamos),"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto en M.N.","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+(Comunes.formatoDecimal(granTotalMontoMN,2)).toString(),"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto en DL.","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+(Comunes.formatoDecimal(granTotalMontoDL,2)).toString(),"formasmen",ComunesPDF.CENTER);
		
		pdfDoc.addTable();
		//pdfDocExt.setCell(pdfDoc.getTable());
		
		pdfDoc.endDocument();
		validaPDF = true;
} catch (Exception e) {
	error = "Error inesperado " +e.getMessage();
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
	if (error.length() > 0 ) {
%>

<%
	}
}
%>


<%
	JSONObject	resultado	= new JSONObject();
	String infoRegresar="";
  if(validaPDF) {
									
										resultado.put("msg",				"");	
										// Enviar resultados
										resultado.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo ); 
										// Enviar resultado de la operacion
										resultado.put("success", 		new Boolean(true)						);
										
										infoRegresar = resultado.toString();
						
							
	}
	else {

	}%>
<%=infoRegresar%>