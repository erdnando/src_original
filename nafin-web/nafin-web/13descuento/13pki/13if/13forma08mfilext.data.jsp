<%@ page contentType="text/html; charset=UTF-8"
	import="
		javax.naming.*,	
		java.text.*,
		java.math.*,
		com.netro.pdf.*,
		java.sql.*,
		com.netro.exception.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*, 	
		netropology.utilerias.*, 
		netropology.utilerias.caracterescontrol.*,
		com.netro.parametrosgrales.*,
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,		
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String rutaArchivoTemporal  ="";
	ParametrosRequest req = null;
	String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String nombreArchivo= "";
	System.out.println("ServletFileUpload.isMultipartContent(request)=="+ServletFileUpload.isMultipartContent(request));
	
	boolean codificacionValida = true;
	String codificacionArchivo = "";
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	
		req = new ParametrosRequest(upload.parseRequest(request));
			 
		FileItem fItem = (FileItem)req.getFiles().get(0);
		itemArchivo		= (String)fItem.getName();
		InputStream archivo = fItem.getInputStream();
		rutaArchivo = PATH_FILE+itemArchivo;
		int tamanio			= (int)fItem.getSize();
		
		nombreArchivo= Comunes.cadenaAleatoria(16)+ ".txt";
		rutaArchivoTemporal = PATH_FILE + "/" + nombreArchivo;
		fItem.write(new File(rutaArchivoTemporal));
		
		if(tamanio>2097152){
			error_tam ="El Archivo es muy Grande, excede el Límite que es de 2 MB.";
		}		
	}
	if(!error_tam.equals("")){
		nombreArchivo="";
	}
	
	
String [] mensajeEstatus = {""};
String mensajeResumen = "";
int status = 0;
boolean hayCaractCtrl = false;
String nombreArchivoCaractEsp = "";

ParametrosGrales paramGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
boolean continuar = true;
if(paramGrales.validaCodificacionArchivoHabilitado()){
	String codificacionDetectada[] = {""};
	CodificacionArchivo codificaArchivo = new CodificacionArchivo();
	codificaArchivo.setProcessTxt(true);
	codificaArchivo.setProcessZip(true);
	
	if(codificaArchivo.esCharsetNoSoportado(rutaArchivoTemporal,codificacionDetectada)){
		codificacionArchivo = codificacionDetectada[0];
		codificacionValida = false;
		continuar = false;
	}
}

//------
	JSONArray arrRegistrosValidos = new JSONArray();
	JSONArray arrRegistrosError = new JSONArray();
	JSONArray arrResumenAccion = new JSONArray();
	JSONObject valoresResultadoProceso = new JSONObject();
//------

if(continuar){
	ResumenBusqueda resumenBusqueda = new ResumenBusqueda();
	BuscaCaracteresControlThread buscaCCtrlThread = new BuscaCaracteresControlThread();
	buscaCCtrlThread.setRutaArchivo(rutaArchivoTemporal);
	buscaCCtrlThread.setRegistrador(resumenBusqueda);
	buscaCCtrlThread.setCreaArchivoDetalle(true);
	buscaCCtrlThread.setDirectorioTemporal(strDirectorioTemp);
	buscaCCtrlThread.setSeparadorCampo("|");
	buscaCCtrlThread.start();
	buscaCCtrlThread.join();
	
	status = buscaCCtrlThread.getStatus(mensajeEstatus);
	ResumenBusqueda resumen = (ResumenBusqueda)buscaCCtrlThread.getRegistrador();
	hayCaractCtrl = resumen.hayCaracteresControl();
	mensajeResumen = resumen.getMensaje()==null?"":resumen.getMensaje();
	nombreArchivoCaractEsp = resumen.getNombreArchivoDetalle();
	
	if(!hayCaractCtrl && status==300 ){
	
	String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
	String tipoTasa = (request.getParameter("tipoTasa")!=null)?request.getParameter("tipoTasa"):"";
	String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
	String lstPlazo = (request.getParameter("lstPlazo")!=null)?request.getParameter("lstPlazo"):"";
	
	boolean esTasaNegociable	= tipoTasa.equals("N")?true:false;

	
	
	
	
	AutorizacionTasas BeanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
	
	ResultadosValidacionTasas resultados = BeanTasas.procesarCargaMasivaTasas(
			strDirectorioTemp, rutaArchivoTemporal, 
			esTasaNegociable, ic_moneda, 
			ic_epo, iNoCliente, lstPlazo);
	
	List lista = (List) resultados.getListaDeRegistrosExitosos(); 	
	int icProceso = resultados.getIcProceso(); 											
	Registros registrosValidos = BeanTasas.getRegistrosValidosExt(lista,icProceso);
	
	
	System.out.println ("esTasaNegociable  "+esTasaNegociable );
	
	String[] campos = new String[] {"RFC", "PUNTOS_TASA", "ESTATUS" };
	String[] camposDescripcion = new String[] {"RFC", "Puntos/Valor Tasa", "Estatus"};
	
	if(esTasaNegociable==false){		
		campos = new String[] {"RFC", "PUNTOS_TASA", "ESTATUS","TIPOTASA" };
		camposDescripcion = new String[] {"RFC", "Puntos/Valor Tasa", "Estatus","Tipo Tasa"};
	}
	
	int totalTasasCargadas 	= registrosValidos.getNumeroRegistros();
	int totalTasasEliminar 	= 0;
	int totalTasasAlta		= 0;
	
	
	while(registrosValidos.next()) {
		JSONObject registro = new JSONObject();
		for (int i = 0; i< campos.length; i++) {
			registro.put("LINEA",registrosValidos.getString("LINEA"));
			registro.put("CAMPO",campos[i]);
			if( campos[i].equals("ESTATUS")) {
				String estatus = registrosValidos.getString(campos[i]);
				if (estatus.equals("A")) {
					totalTasasAlta++;
				} else if (estatus.equals("E")) {
					totalTasasEliminar++;
				}
			}
			registro.put("OBSERVACION",registrosValidos.getString(campos[i]));
			arrRegistrosValidos.add(registro);
		}
	}
	
	
	JSONObject registroResumenAccion1 = new JSONObject();
	registroResumenAccion1.put("ID","total_tasas_cargadas");
	registroResumenAccion1.put("DESCRIPCION","No. Total de Tasas Cargadas");
	registroResumenAccion1.put("TOTAL",new Integer(totalTasasCargadas));
	JSONObject registroResumenAccion2 = new JSONObject();
	registroResumenAccion2.put("ID","total_tasas_alta");
	registroResumenAccion2.put("DESCRIPCION","No. Total de Tasas " + ((tipoTasa.equals("P"))?"Preferencial":"Negociada") +" Alta");
	registroResumenAccion2.put("TOTAL",new Integer(totalTasasAlta));
	JSONObject registroResumenAccion3 = new JSONObject();
	registroResumenAccion3.put("ID","total_tasas_eliminadas");
	registroResumenAccion3.put("DESCRIPCION","No. Total de Tasas " + ((tipoTasa.equals("P"))?"Preferencial":"Negociada") +" Eliminadas");
	registroResumenAccion3.put("TOTAL",new Integer(totalTasasEliminar));
	arrResumenAccion.add(registroResumenAccion1);
	arrResumenAccion.add(registroResumenAccion2);
	arrResumenAccion.add(registroResumenAccion3);
	
		
	// Construir lista con los Numeros de Linea de los Registros Validos
	StringBuffer 	listaNumerosLinea	= new StringBuffer();
	for(int i=0;i<lista.size();i++){
		if(i>0) listaNumerosLinea.append(",");
		listaNumerosLinea.append((String) lista.get(i));
	}
	
	
	if( resultados.getNumRegErr() > 0 ) {
		ArrayList registros = resultados.getRegistros();
		Iterator it = registros.iterator();
		while(it.hasNext()) {
			ValidacionTasa v = (ValidacionTasa) it.next();
			if(v.hayError()) {
				JSONObject registro = new JSONObject();
				if(v.hayErrorRFC()){
					registro.put("LINEA",new Integer(v.getNumeroLinea()));
					registro.put("CAMPO",camposDescripcion[0]);
					registro.put("OBSERVACION",v.getErroresRfc());
					arrRegistrosError.add(registro);
				}
				if(v.hayErrorPuntosValorTasa()){
					registro.put("LINEA",new Integer(v.getNumeroLinea()));
					registro.put("CAMPO",camposDescripcion[1]);
					registro.put("OBSERVACION",v.getErroresPuntosValorTasa());
					arrRegistrosError.add(registro);
				}
				if(v.hayErrorEstatus()){
					registro.put("LINEA",new Integer(v.getNumeroLinea()));
					registro.put("CAMPO",camposDescripcion[2]);
					registro.put("OBSERVACION",v.getErroresEstatus());
					arrRegistrosError.add(registro);
				}
				
				if(v.hayErrorTipoTasa()){
					registro.put("LINEA",new Integer(v.getNumeroLinea()));
					registro.put("CAMPO",camposDescripcion[3]);
					registro.put("OBSERVACION",v.getErrorTipoTasa());
					arrRegistrosError.add(registro);
				} 
				
	
				if(v.hayErrorGenerico()){
					registro.put("LINEA",new Integer(v.getNumeroLinea()));
					registro.put("CAMPO","");
					registro.put("OBSERVACION",v.getErrorGenerico());
					arrRegistrosError.add(registro);
				}
			}
		}
	}
	String btnContinuar  ="N";
	if(lista.size()>0) { btnContinuar  ="S";  }
		
	//JSONObject valoresResultadoProceso = new JSONObject();
	valoresResultadoProceso.put("totalRegistrosValidos", new Integer(lista.size()));
	valoresResultadoProceso.put("totalRegistrosInvalidos", new Integer(resultados.getNumRegErr()));
	valoresResultadoProceso.put("proceso", new Integer(icProceso));
	valoresResultadoProceso.put("totalRegistrosValidos", new Integer(lista.size()));
	//valoresResultadoProceso.put("listaNumerosLineaValidos", listaNumerosLinea.toString());
	valoresResultadoProceso.put("lstNumerosLineaOk", listaNumerosLinea.toString());
	valoresResultadoProceso.put("btnContinuar", btnContinuar);	
	}
}
%>
({
	"success": true,
	"registrosValidos": <%=arrRegistrosValidos%>,
	"registrosInvalidos": <%=arrRegistrosError%>,
	"resumenAccion": <%=arrResumenAccion%>,
	"valoresResultadoProceso": <%=valoresResultadoProceso%>,
	"statusThread": '<%=String.valueOf(status)%>',
	"mensajeEstatus":	'<%=mensajeEstatus[0]%>',
	"mensajeResumen":	'<%=net.sf.json.util.JSONUtils.quote(mensajeResumen)%>',
	"hayCaractCtrl":	<%=(hayCaractCtrl)?"true":"false"%>,
	"urlArchivo":	'<%=strDirecVirtualTemp+nombreArchivoCaractEsp%>',
	"codificacionValida": <%=(codificacionValida)?"true":"false"%>,
	"codificacionArchivo": '<%=codificacionArchivo%>'
})


