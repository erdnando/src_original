
	function selecESTATUS(check, rowIndex, colIds){
		var  gridLimites = Ext.getCmp('gridLimites');	
		var store = gridLimites.getStore();
		var reg = gridLimites.getStore().getAt(rowIndex);
		
		if(check.checked==true)  {
			reg.set('CS_ACTIVA_LIMITE', 'true');	
			reg.set('AUX_ESTATUS', "checked");	
			reg.set('SELECCIONADOS', "S");
			reg.set('IC_IF_EPO_BLOQ_DES', reg.data['IC_IF']);
			
			gridLimites.startEditing(rowIndex, colIds);		
		}else  {
		 reg.set('CS_ACTIVA_LIMITE', 'false'); 
		 reg.set('AUX_ESTATUS', "");	
		 reg.set('SELECCIONADOS', "S");
		 reg.set('IC_IF_EPO_BLOQ_DES', reg.data['IC_IF']);
			gridLimites.startEditing(rowIndex, colIds);		
		}	
	}
	

Ext.onReady(function() {

//_--------------------------------- HANDLERS -------------------------------

	var procesarSuccessFailureGuardar =  function(opts, success, response) {
		var gridLim = Ext.getCmp('gridLimites');
		gridLim.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true)  {
			gridLim.getStore().commitChanges();
			Ext.MessageBox.alert('Cambios Guardados','La actualizaci�n ha sido realizada con �xito');
			consultaData.load({
				params: {
					informacion: 'ConsultaLimitesPorEPO'						
				}
			});
				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var confirmar = function(pkcs7, vtextoFirmar, vregistrosEnviar){
	
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else{		
						
			Ext.Ajax.request({
				url : '13forma05exta.jsp',
				params : {					
					Pkcs7: pkcs7,
					TextoFirmado: vtextoFirmar,
					registros : vregistrosEnviar
				},
				callback: procesarSuccessFailureGuardar
			});
		}
	}
	
	var onSave = function() {
		var gridLim = Ext.getCmp('gridLimites');
		var store = gridLim.getStore();
		var modificados = store.getModifiedRecords();
		var columnModelGrid = gridLim.getColumnModel();
		var numRegistro = -1;
		var errorValidacion = false;
				
		store.each(function(record) {
			numRegistro = store.indexOf(record);
			if( Ext.isEmpty(record.data['NUEVO_MONTO_LIMITE']) ) {	//Para registros nuevos el monto del limite podria ir vacio????, por eso se da la sig. validaci�n.
				errorValidacion = true;
				Ext.MessageBox.alert('Error de validaci�n',
						'El monto del l�mite no puede ir vac�o',
						function(){
							gridLim.startEditing(numRegistro, columnModelGrid.findColumnIndex('NUEVO_MONTO_LIMITE'));
						}
				);
				return false;
			}
			if( record.data['CS_FECHA_LIMITE'] == true && 
					Ext.isEmpty(record.data['FEC_VENC_LINEA']) && 
					Ext.isEmpty(record.data['FEC_CAMBIO_ADMON']) ) {
				errorValidacion = true;
				Ext.MessageBox.alert('Error de validaci�n',
						'Debe capturar la Fecha de Vencimiento L�nea de Cr�dito<br>o la Fecha Cambio de Administraci�n',
						function(){
							gridLim.startEditing(numRegistro, columnModelGrid.findColumnIndex('FEC_VENC_LINEA'));
						}
				);
				return false;
			}
		});
		
		if (errorValidacion) {
				return;
		}
		
		if (modificados.length > 0) {
		
			//validacion de registros modificados...
			Ext.each(modificados, function(record) {			
				numRegistro = store.indexOf(record);
				if( !Ext.isEmpty(record.data['FEC_VENC_LINEA']) && Ext.isEmpty(record.data['IG_AVISO_VENC_LINEA']) ) {
					errorValidacion = true;
					Ext.MessageBox.alert('Error de validaci�n',
							'Si especifica Fecha de Vencimiento de la Linea de Credito<br>son requeridos los Dias de Aviso Previo',
							function(){
								gridLim.startEditing(numRegistro, columnModelGrid.findColumnIndex('IG_AVISO_VENC_LINEA'));
							}
					);
					return false;
				}
				if( !Ext.isEmpty(record.data['FEC_CAMBIO_ADMON']) && Ext.isEmpty(record.data['IG_AVISO_CAMBIO_ADMON']) ) {
					errorValidacion = true;
					Ext.MessageBox.alert('Error de validaci�n',
							'Si existe Fecha de Cambio de Administracion<br>es requerido los Dias de Aviso Previo',
							function(){
								gridLim.startEditing(numRegistro, columnModelGrid.findColumnIndex('IG_AVISO_CAMBIO_ADMON'));
							}
					);
					return false;
				}
			});
			//--------------------------------------
			
			if (errorValidacion) {
				return;
			}
			
			var textoFirmar = "Los siguientes registros ser�n modificados:\n" +
					"Raz�n Social|Limite Anterior|Limite Nuevo MN|Limite Nuevo DL|Linea Activa|" +
					"Fecha Vencimiento|Dias de Aviso Vencimiento|" +
					"Fecha Cambio Admin.|Dias de Aviso Cambio Admin|\n";
			
			Ext.each(modificados, function(record) {		
				textoFirmar += record.data['CG_RAZON_SOCIAL'] + "|" +
						record.data['IN_LIMITE_EPO'] + "|" +
						record.data['NUEVO_MONTO_LIMITE'] + "|" +
						record.data['NUEVO_MONTO_LIMITE_DL'] + "|" +
						( (record.data['CS_ACTIVA_LIMITE'])?"S":"N" ) + "|" +
						Ext.util.Format.date(record.data['FEC_VENC_LINEA'],'d/m/Y') + "|" +
						( (record.data['IG_AVISO_VENC_LINEA']==null)?'':record.data['IG_AVISO_VENC_LINEA'] ) + "|" +
						Ext.util.Format.date(record.data['FEC_CAMBIO_ADMON'],'d/m/Y') + "|" +
						( (record.data['IG_AVISO_CAMBIO_ADMON']==null)?'':record.data['IG_AVISO_CAMBIO_ADMON'] ) + "|" +
						"\n";
			});
			var registrosEnviar = [];
			Ext.each(modificados, function(record) {
				registrosEnviar.push(record.data);
			});
			registrosEnviar = Ext.encode(registrosEnviar);

			NE.util.obtenerPKCS7(confirmar, textoFirmar, registrosEnviar );			
		}
	}
//-------------------------------- STORES -----------------------------------
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma05ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaLimitesPorEPO'
		},
		fields: [
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'IN_LIMITE_EPO', type: 'float'},
			{name: 'NUEVO_MONTO_LIMITE', type: 'float', mapping: 'IN_LIMITE_EPO'},   //Campo capturable
			{name: 'MONTOLIMITE', type: 'float'},
			{name: 'CS_ACTIVA_LIMITE', convert: NE.util.string2boolean},
			{name: 'FEC_VENC_LINEA', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_AVISO_VENC_LINEA', type: 'int', convert: NE.util.zero2null},
			{name: 'FEC_CAMBIO_ADMON', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IG_AVISO_CAMBIO_ADMON', type: 'int', convert: NE.util.zero2null},
			{name: 'IC_EPO', type: 'int'},
			{name: 'LIMITECOMPROMETIDO', type: 'float'},
			{name: 'MONTOTOTALLIMITE', type: 'float'},
			{name: 'ACTIVA_PROCESO', defaultValue: "N"},	//Campo capturable
			{name: 'ACTIVA_PROCESOB', defaultValue: "N"}, //Campo capturable
			{name: 'CS_FECHA_LIMITE', convert: NE.util.string2boolean}, //Campo capturable			
			{name: 'IN_LIMITE_EPO_DL', type: 'float'},
			{name: 'NUEVO_MONTO_LIMITE_DL', type: 'float', mapping: 'IN_LIMITE_EPO_DL'},  //Campo capturable
			{name: 'MONEDA_MN', defaultValue: "N"},
			{name: 'MONEDA_DL', defaultValue: "N"},
			{name: 'LIMITECOMPROMETIDO_DL', type: 'float'},
			{name: 'MONTOTOTALLIMITE_DL', type: 'float'},
			{name: 'MONTOLIMITE_DL', type: 'float'},
			{name: 'DESCRIPCION_ESTATUS'},
			{name: 'FECHA_BLOQ_DESB'},
			{name: 'USUARIO_BLOQ_DESB'},
			{name: 'DEPENDENCIA_BLOQ_DESB'},
			{name: 'IC_IF_EPO_BLOQ_DES'},
			{name: 'AUX_ESTATUS'},	
			{name: 'SELECCIONADOS'},					
			{name: 'IC_IF'}	
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
		
	});

//-------------------------------- ----------------- -----------------------------------

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 5, align: 'center'},							
				{header: 'Vencimiento L�nea de Cr�dito', colspan: 2, align: 'center'},
				{header: 'Cambio de Administraci�n', colspan: 2, align: 'center'},
				{header: '', colspan: 1, align: 'center'},
				{header: 'Cambio de Estatus', colspan: 4, align: 'center'}				
			]
		]
	});

	var campoAvisos = new Ext.form.NumberField({
		maxValue : 999999999999.99,
		allowBlank: false
	});
	
	var campoFechas = new Ext.form.DateField({
		startDay: 0
	});
	


	
    // create the Grid
	var grid = new Ext.grid.EditorGridPanel({
		id: 'gridLimites',
		title: 'Limites por EPO',
		style: 'margin:0 auto;',
		columns: [
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false				
			},
			{
				header: '<center> Monto L�mite Actual<br> Moneda Nacional',
				tooltip: 'Monto del L�mite Actual Moneda Nacional',
				dataIndex: 'IN_LIMITE_EPO',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){ 
					if(record.data['MONEDA_MN']== 'N')  {
						return ""; 
					 }else  {
						return Ext.util.Format.number(value, '$0,0.00');
					}
				}
			},
			{
				header : '<center>Nuevo Monto L�mite <br> Moneda Nacional',
				tooltip: 'Nuevo Monto del L�mite ',
				dataIndex : 'NUEVO_MONTO_LIMITE',
				sortable : true,
				hideable : false,
				width : 130,
				align: 'right',				
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				},
				renderer: function(value, metadata, record, rowIndex, colIndex, store){ 
					if(record.data['MONEDA_MN']== 'N')  {
						return ""; 
					 }else  {
						return NE.util.colorCampoEdit(Ext.util.Format.number(value, '$0,0.00'),metadata,record);					
					}
				}
			},
			{
				header: '<center> Monto L�mite Actual <br>D�lar Americano',
				tooltip: 'Monto del L�mite Actual Dolar Americano',
				dataIndex: 'IN_LIMITE_EPO_DL',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'right',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){ 
					if(record.data['MONEDA_DL']== 'N')  {
						return ""; 
					 }else  {
						return Ext.util.Format.number(value, '$0,0.00');
					}
				}
			},
			{
				header : '<center>Nuevo Monto L�mite <br>D�lar Americano ',
				tooltip: 'Nuevo Monto del L�mite Dolar Americano ',
				dataIndex : 'NUEVO_MONTO_LIMITE_DL',
				sortable : true,
				hideable : false,
				width : 130,
				align: 'right',				
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: false
				},
			   renderer: function(value, metadata, record, rowIndex, colIndex, store){ 
					 if(record.data['MONEDA_DL']== 'N')  {
						return ""; 
					 }else  {
						return NE.util.colorCampoEdit(Ext.util.Format.number(value, '$0,0.00'),metadata,record);					
					}
				}
			},	
			{
				header : 'Fecha',
				tooltip: 'Fecha de Vencimiento de la L�nea de Cr�dito',
				dataIndex : 'FEC_VENC_LINEA',
				sortable : true,
				width : 100,
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				editor: campoFechas
			},
			{
				header : 'Avisos',
				tooltip: 'Avisos D�as Previos',
				dataIndex : 'IG_AVISO_VENC_LINEA',
				sortable : true,
				width : 50,
				editor: campoAvisos
			},
			{
				header : 'Fecha',
				tooltip: 'Fecha de Cambio de Administracion',
				dataIndex : 'FEC_CAMBIO_ADMON',
				sortable : true,
				width : 100,
				renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				editor: campoFechas
			},
			{
				header : 'Avisos',
				tooltip: 'Avisos D�as Previos',
				dataIndex : 'IG_AVISO_CAMBIO_ADMON',
				sortable : true,
				width : 50,
				editor: campoAvisos
			},
			{
				xtype: 'checkcolumn',
				header : 'Validar Fechas',
				tooltip: 'Validar Fechas L�mite',
				dataIndex : 'CS_FECHA_LIMITE',
				width : 50,
				sortable : false
			},	
			{				
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'CS_ACTIVA_LIMITE',
				sortable: true,
				 hideable: false,
				width: 150,			
				resizable: true,									
				align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
					if(record.data['IC_IF_EPO_BLOQ_DES']!='') {
						if(record.data['IC_IF_EPO_BLOQ_DES']!=record.data['IC_IF'] && (  record.data['CS_ACTIVA_LIMITE'] =='false' || record.data['CS_ACTIVA_LIMITE'] ==false)  ){					
							return '<input id="chkESTATUS"'+ rowIndex + 'value="S" type="checkbox" DISABLED '+record.data['AUX_ESTATUS']+' onclick="selecESTATUS(this,'+rowIndex +','+colIndex+');"/>'+ " &nbsp;"+record.data['DESCRIPCION_ESTATUS']+ "&nbsp;(EPO)";              									
						}	else {
							return '<input id="chkESTATUS"'+ rowIndex + 'value="S" type="checkbox"   '+record.data['AUX_ESTATUS']+' onclick="selecESTATUS(this,'+rowIndex +','+colIndex+');"/>'+ " &nbsp;"+record.data['DESCRIPCION_ESTATUS']+"&nbsp;(IF)&nbsp;";              															
						}
					}else  {
						return '<input id="chkESTATUS"'+ rowIndex + 'value="S" type="checkbox"   '+record.data['AUX_ESTATUS']+' onclick="selecESTATUS(this,'+rowIndex +','+colIndex+');"/>'+ " &nbsp;"+record.data['DESCRIPCION_ESTATUS']+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";            													
					}
				}
			},						
			{
				header: 'Fecha de <BR>  Bloqueo/Desbloqueo',
				tooltip: 'Fecha de Bloqueo/Desbloqueo',
				dataIndex: 'FECHA_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: '<center> Usuario de  <BR>  Bloqueo/Desbloqueo ',
				tooltip: 'Usuario de Bloqueo/Desbloqueo ',
				dataIndex: 'USUARIO_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},			
			{
				header: '<center> Dependencia que <BR>  Bloqueo/Desbloqueo',
				tooltip: 'Dependencia que Bloqueo/Desbloqueo ',
				dataIndex: 'DEPENDENCIA_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			}
		],
		store: consultaData,
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
//		autoHeight: true,
		//maxHeight: 400,
		height: 400,
		width: 885,
		frame: false,
		plugins: grupos,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
					text: 'Guardar Cambios',
					handler: onSave
				},
				'-',
				{
					text: 'Cancelar',
					handler: function() {
						var gridLim = Ext.getCmp('gridLimites');
						var store = gridLim.getStore();
						store.rejectChanges();
					}
				}

			]
		},
		listeners: {
			beforeedit: {
				fn: function(obj) {
					 if (obj.field == 'IG_AVISO_VENC_LINEA') {
						if (Ext.isEmpty(obj.record.data['FEC_VENC_LINEA'])) {
							Ext.MessageBox.alert('Error de validaci�n','Se requiere Fecha de Vencimiento de la L�nea<br>para capturar este dato');
							obj.cancel = true;
						}
					} else if (obj.field == 'IG_AVISO_CAMBIO_ADMON') {
						if (Ext.isEmpty(obj.record.data['FEC_CAMBIO_ADMON'])) {
							Ext.MessageBox.alert('Error de validaci�n','Se requiere Fecha de Cambio de Administraci�n<br>para capturar este dato');
							obj.cancel = true;
						}
					}
										
					if(obj.field =='NUEVO_MONTO_LIMITE'){					
						if(obj.record.data['MONEDA_MN'] =='S' ){
							return true;
						}else {
							return false;
						}
					}
					
					if(obj.field =='NUEVO_MONTO_LIMITE_DL'){					
						if(obj.record.data['MONEDA_DL'] =='S' ){
							return true;
						}else {
							return false;
						}
					}
				}
			},
			validateedit: {
				fn: function(obj) {
					//propiedades de obj: grid, record, field, value, originalValue, row, column, cancel
					if (obj.field == 'NUEVO_MONTO_LIMITE') {
						if (obj.value < obj.record.data['MONTOLIMITE']) {
							obj.cancel = true;
							//�CUIDADO!: Recordar que los mensajes en ExtJS no detienen la ejecuci�n del metodo
							Ext.Msg.alert('Error de Validaci�n', 
								"Para la Epo:"+ "  "+ obj.record.data['CG_RAZON_SOCIAL']+",el monto debe ser mayor a " + Ext.util.Format.number(obj.record.data['MONTOLIMITE'],'$0,0.00'),
								function() {
									obj.grid.startEditing(obj.row, obj.column);
								});
						} else {
							if (obj.record.data['LIMITECOMPROMETIDO'] && obj.record.data['LIMITECOMPROMETIDO'] > 0) {
								if (obj.value < obj.record.data['LIMITECOMPROMETIDO']) {
									Ext.Msg.confirm("Confirmaci�n", 
											"Para la Epo:"+ "  "+ obj.record.data['CG_RAZON_SOCIAL']+
											",hay documentos Programados Pyme de Factoraje 24 hrs. �Desea regresarlos a Negociables para poder bajar el l�mite de cr�dito de la EPO?",
											function(boton) {
												if (boton == 'ok') {
													obj.record.data['ACTIVA_PROCESO']='S';
													obj.cancel = false;
												} else if (boton == 'no') {
													obj.record.data['ACTIVA_PROCESO']='N';
													obj.cancel = true;
													obj.grid.startEditing(obj.row, obj.column);
												}
											}
									);
								} else {
									obj.record.data['ACTIVA_PROCESO']='N';
								}
							}
						}
					} //FIN NUEVO_MONTO_LIMITE
					
					
					
					if (obj.field == 'NUEVO_MONTO_LIMITE_DL') {
						if (obj.value < obj.record.data['MONTOLIMITE_DL']) {
							obj.cancel = true;
							//�CUIDADO!: Recordar que los mensajes en ExtJS no detienen la ejecuci�n del metodo
							Ext.Msg.alert('Error de Validaci�n', 
								"Para la Epo:"+ "  "+ obj.record.data['CG_RAZON_SOCIAL']+",el monto debe ser mayor a " + Ext.util.Format.number(obj.record.data['MONTOLIMITE_DL'],'$0,0.00'),
								function() {
									obj.grid.startEditing(obj.row, obj.column);
								});
						} else {
							if (obj.record.data['LIMITECOMPROMETIDO_DL'] && obj.record.data['LIMITECOMPROMETIDO_DL'] > 0) {
								if (obj.value < obj.record.data['LIMITECOMPROMETIDO_DL']) {
									Ext.Msg.confirm("Confirmaci�n", 
											"Para la Epo:"+ "  "+ obj.record.data['CG_RAZON_SOCIAL']+
											",hay documentos Programados Pyme de Factoraje 24 hrs. �Desea regresarlos a Negociables para poder bajar el l�mite de cr�dito de la EPO?",
											function(boton) {
												if (boton == 'ok') {
													obj.record.data['ACTIVA_PROCESO']='S';
													obj.cancel = false;
												} else if (boton == 'no') {
													obj.record.data['ACTIVA_PROCESO']='N';
													obj.cancel = true;
													obj.grid.startEditing(obj.row, obj.column);
												}
											}
									);
								} else {
									obj.record.data['ACTIVA_PROCESO']='N';
								}
							}
						}
					} //FIN NUEVO_MONTO_LIMITE_DL
					
					
					
				}
			},
			afteredit: {
				fn: function(obj) {				
					//propiedades de obj: grid, record, field, value, originalValue, row, column
					if (obj.field == 'CS_ACTIVA_LIMITE') {					
						if(obj.value) {	//limite activado
							obj.record.data['ACTIVA_PROCESOB']='N';
						} else {				//limite desactivado
							if(obj.record.data['LIMITECOMPROMETIDO'] > 0){
								Ext.Msg.confirm("Confirmaci�n", 
									"Para la Epo:"+ "  "+ obj.record.data['CG_RAZON_SOCIAL']+
									",hay documentos Programados Pyme de Factoraje 24 hrs. �Desea regresarlos a Negociables para poder Inactivar la Cuenta?",
									function(boton) {
										if (boton == 'ok') {
											obj.record.data['ACTIVA_PROCESOB']='S';
										} else if (boton == 'no') {
											obj.record.data['ACTIVA_PROCESOB']='N';
											obj.record.data['CS_ACTIVA_LIMITE']=false;
										}
									}
								);
							}
							
							if(obj.record.data['LIMITECOMPROMETIDO_DL'] > 0){
								Ext.Msg.confirm("Confirmaci�n", 
									"Para la Epo:"+ "  "+ obj.record.data['CG_RAZON_SOCIAL']+
									",hay documentos Programados Pyme de Factoraje 24 hrs. �Desea regresarlos a Negociables para poder Inactivar la Cuenta?",
									function(boton) {
										if (boton == 'ok') {
											obj.record.data['ACTIVA_PROCESOB']='S';
										} else if (boton == 'no') {
											obj.record.data['ACTIVA_PROCESOB']='N';
											obj.record.data['CS_ACTIVA_LIMITE']=false;
										}
									}
								);
							}
							
						}
					} else if (obj.field == 'FEC_VENC_LINEA') {
						if (Ext.isEmpty(obj.value)) {
							obj.record.set('IG_AVISO_VENC_LINEA' , null);
						}
					} else if (obj.field == 'FEC_CAMBIO_ADMON') {
						if (Ext.isEmpty(obj.value)) {
							obj.record.set('IG_AVISO_CAMBIO_ADMON' , null);
						}
					}
				}
			}
		}
	});

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		//layout: 'vbox',
		width: 890,
		height: 'auto',
		//height: 3000,
		//layoutConfig: {
		//	align:'center'
		//},
		items: [
			grid
		]
	});

//-------------------------------- ----------------- -----------------------------------

});