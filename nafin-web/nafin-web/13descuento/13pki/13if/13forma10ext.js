Ext.onReady(function(){
   
   Ext.namespace('CARTERA.captura');

   function ObjGeneral(){
      this.urlRequestData        = '13forma10ext.data.jsp';    
      this.bandera               = false;
      this.comboConceptoPago     = "";
      this.conceptoPagoIni       = "V";
      this.sEstado_E             = "";
      this.sClaveIF_E            = "";
      this.sClaveEstado_E        = "";
      this.sClaveMoneda_E        = "";
      this.sFechaDeposito_E      = "";
      this.sMoneda_E             = "";
      this.sImporteDeposito_E    = "";
      this.sImporteDeposito_E    = "";
      this.sBancoServicio_E      = "";
      this.sRefBanco_E           = "";
      this.sFechaProbPago_E      = "";
      this.sFechaProbPago_E      = "";
      this.sRefInter_E           = "";
      this.sFechaVencimiento_E   = "";
      this.sClaveRefInter_E      = "";
      this.sNombreBanco_E        = "";
      this.folioCert             = "";
      this.recibo                = "";
      this.importePago           = 0;
      this.arrSucursal           = [];
      this.arrSubAplicacion      = [];
      this.arrPrestamo           = [];
      this.arrAcreditado         = [];
      this.arrFechaOperacion     = [];
      this.arrFechaVencimientol  = [];
      this.arrFechaPago          = [];
      this.arrFechaInsoluto      = [];
      this.arrSaldoInsoluto      = [];
      this.arrTasa               = [];
      this.arrCapital            = [];
      this.arrDias               = [];
      this.arrIntereses          = [];
      this.arrComision           = [];
      this.arrIva                = [];
      this.arrConceptoPago       = [];
      this.arrImportePago        = [];
   }
   
   ObjGeneral.prototype.init = function(){      
      this.tipoIF = Ext.getDom('tipoIF').value;
      this.estado = 'iniciaPantalla';
      pnl.removeAll();
   }
   
   ObjGeneral.prototype.controller = function(){
      switch(this.tipoIF){
         case 'B' : this.agregaComponentesEncabezadosTipoIf_B();  break;
         default  : this.agregaComponentesEncabezados();          break;
      }
   }
      
   // Agrega los componente cuando TipoIF sea diferente de 'B' al contenedor Encabezado
   ObjGeneral.prototype.agregaComponentesEncabezados = function(){ // TipoIF != B          
      var store = comboConceptoPago.getStore();         
      var record = [];
      
      // Seteo el combo Concepto Pago con el valor de ConceptoPagoIni
      comboConceptoPago.setValue(this.conceptoPagoIni);
      // Crear registros del Combo Concepto Pago
      record.push(new Ext.data.Record({ clave:"A", descripcion: "Anticipado" }));
      record.push(new Ext.data.Record({ clave:"V", descripcion: "Vencimiento" }));
      // Agrego los registros al Store y actualizo el combo Concepto Pago
      store.add(record);
      store.commitChanges();
      
      // Cargao el grid Captura Individual 1 con el parametro informacion: 'storeCapturaIndividual_1'
      gridCapturaIndividual_1.getStore().load({params:{informacion: 'storeCapturaIndividual_1'}});
      
      // Agrego el combo Concepto Pago al Contenedor Principal
      pnl.add({xtype: 'fieldset', width: 400, style: 'margin:0 auto', title: 'Concepto Pago', items:[comboConceptoPago]});         
      pnl.add(NE.util.getEspaciador(10));
      // Agrego grid Captura Individual al Contenedor Principal
      pnl.add(gridCapturaIndividual_1);
      pnl.add(NE.util.getEspaciador(10));
      // Actualizo el Contenedor Principal
      pnl.doLayout();
   }
   
   // Agrega los componente cuando TipoIF sea igual a 'B' al contenedor Encabezado
   ObjGeneral.prototype.agregaComponentesEncabezadosTipoIf_B = function(){ // TipoIF == B          
      var store = comboConceptoPago.getStore();         
      var record = [];
      
      // Seteo el combo Concepto Pago con el valor de ConceptoPagoIni
      comboConceptoPago.setValue(this.conceptoPagoIni);
      // Crear registros del Combo Concepto Pago
      record.push(new Ext.data.Record({ clave:"A", descripcion: "Anticipado" }));
      record.push(new Ext.data.Record({ clave:"V", descripcion: "Vencimiento" }));
      
      // Cargao el grid Captura Individual 1 con el parametro informacion: 'storeCapturaIndividual_2'
      gridCapturaIndividual_1.getStore().load({params:{informacion: 'storeCapturaIndividual_2'}});
      
      // Agrego los registros al Store y actualizo el combo Concepto Pago
      store.add(record);
      store.commitChanges();
      
      // Agrego el combo Concepto Pago al Contenedor Principal
      pnl.add({xtype: 'fieldset', width: 400, style: 'margin:0 auto', title: 'Concepto Pago', items:[comboConceptoPago]});         
      pnl.add(NE.util.getEspaciador(10));
      // Agrego grid Captura Individual al Contenedor Principal
      pnl.add(gridCapturaIndividual_1);
      pnl.add(NE.util.getEspaciador(10));
      // Actualizo el Contenedor Principal
      pnl.doLayout();
   }
   
   // Setea componentes y variables del contenedor de Encabezado con los valores previamente obtenidos
   ObjGeneral.prototype.mostrarEncabezado = function(reg){
      this.comboConceptoPago = comboConceptoPago.getValue();
      this.estado = 'llenaEncabezado';
      CARTERA.captura.ObjGeneral.bandera = true;
      
      // seteo el combo Banco de Servicio
      Ext.getCmp('id_sBancoServicio_E').setValue("");
      
      Ext.getCmp('id_sEstado_E').setValue(reg.get('NOMBRE_ESTADO'));
      Ext.getCmp('id_sFechaProbPago_E').setValue(reg.get('FECHA_PROB_PAGO'));
      Ext.getCmp('id_sFechaDeposito_E').setValue(this.comboConceptoPago=='V'?reg.get('FECHA_DEPOSITO'):'');
      Ext.getCmp('id_sImporteDeposito_E').setValue(this.comboConceptoPago=='V'?reg.get('IMPORTE_DEPOSITO'):'');
      Ext.getCmp('id_sRefInter_E').setValue(reg.get('REF_INTER'));
      Ext.getCmp('id_sMoneda_E').setValue(reg.get('CLAVE_MONEDA'));
      Ext.getCmp('id_sClaveMoneda_E').setValue(reg.get('MONEDA'));
      
      Ext.getCmp('id_sClaveIF_E').setValue(reg.get('CLAVE_IF'));
      Ext.getCmp('id_sClaveEstado_E').setValue(reg.get('CLAVE_ESTADO'));
      
      Ext.getCmp('id_sFechaVencimiento_E').setValue(reg.get('FECHA_VENCIMIENTO'));
      Ext.getCmp('id_sClaveRefInter_E').setValue(reg.get('CLAVE_REF_INTER'));
      
      
      this.sClaveEstado_E        = reg.get('CLAVE_ESTADO');
      this.sClaveMoneda_E        = reg.get('CLAVE_MONEDA');
      this.sMoneda_E             = reg.get('MONEDA');
      this.sFechaVencimiento_E   = reg.get('FECHA_VENCIMIENTO');
      this.sFechaProbPago_E      = reg.get('FECHA_PROB_PAGO');
      this.sFechaDeposito_E      = reg.get('FECHA_DEPOSITO');
      this.sEstado_E             = reg.get('NOMBRE_ESTADO');
      this.sClaveIF_E            = reg.get('CLAVE_IF');
      this.sRefInter_E           = reg.get('REF_INTER');
      this.sClaveRefInter_E      = reg.get('CLAVE_REF_INTER');
      this.sImporteDeposito_E    = reg.get('IMPORTE_DEPOSITO');
      this.sRefBanco_E           = reg.get("sRefBanco_E");
      this.sNombreBanco_E        = reg.get("sNombreBanco_E");
   }
   
   // Accion que muestra el ACUSE cuando el Transmitir la Solicitud se haya accionado satisfactoriamente
   ObjGeneral.prototype.transmiteSolExitoso = function(response, request){
      var jsonData = Ext.decode(response.responseText);
      CARTERA.captura.ObjGeneral.folioCert   = jsonData.folioCert;
      CARTERA.captura.ObjGeneral.recibo      = jsonData.acuse;
      
      Ext.getCmp('pnlBtnTransmiteSol').hide();  
      elementosForma.setTitle("Acuse de Solicitud");
      elementosForma.add({xtype: 'displayfield', fieldLabel: 'Mensaje', html: jsonData.mensaje});
      elementosForma.add({xtype: 'displayfield', fieldLabel: 'Recibo', html: CARTERA.captura.ObjGeneral.recibo});
      elementosForma.doLayout();
      elementosForma.show();
      pnl.insert(0,elementosForma);
      pnl.add(NE.util.getEspaciador(10));
      pnl.add(
         {
            xtype: 'panel', width:500, style:'margin:0 auto', title: 'Datos del Acuse', items: [
               {
                  xtype: 'fieldset', title: 'Datos del Acuse', style: 'margin: 20px', labelAlign: 'right', labelWidth: 150, items: [
                     {xtype: 'displayfield', fieldLabel: 'N�mero de Acuse', html: CARTERA.captura.ObjGeneral.folioCert},
                     {xtype: 'displayfield', fieldLabel: 'Fecha', html: jsonData.sFecha},
                     {xtype: 'displayfield', fieldLabel: 'Hora', html: jsonData.sHora},
                     {xtype: 'displayfield', fieldLabel: 'N�mero de Pr�stamos', html: jsonData.NumPrestamo},
                     {xtype: 'displayfield', fieldLabel: 'Monto', html: jsonData.dImporte}
                  ]
               }
            ],
            bbar: [
               '->','-',
               {text:'Imprimir PDF', id:'btnAcuseSolPdf', iconCls: 'icoPdf', handler: fnAcuseSolPdf},
               {text:'Descargar PDF', id:'btnAcuseSolPdf_descarga', iconCls: 'icoPdf', hidden: true},
               '-',
               {text:'Regresar', iconCls: 'rechazar', handler: fnCancelarGral}
            ]
         }
      );
      pnl.doLayout();
   }
   
   // Agregar Grid Editable debajo del Contenedor de Encabezado
   ObjGeneral.prototype.agregarGridEditable = function(){      
      gridDetallesEditor.show();  
      pnl.add(gridDetallesEditor);
      pnl.add(gridDetallesEditorTotales);
      pnl.doLayout();      
      
      gridDetallesEditor.getStore().removeAll();
      
      if(comboConceptoPago.getValue()=='V'){
         gridDetallesEditor.getStore().load(
            {
               params:{
                  informacion:'storeDetalles_2', 
                  estado: 'enviarFormaEncabezado',  
                  mostrarTotales: 'No',
                  sEstado_E :          CARTERA.captura.ObjGeneral.sEstado_E,
                  sClaveIF_E :         CARTERA.captura.ObjGeneral.sClaveIF_E,
                  sClaveEstado_E :     CARTERA.captura.ObjGeneral.sClaveEstado_E,
                  sClaveMoneda_E :     CARTERA.captura.ObjGeneral.sClaveMoneda_E,
                  sFechaDeposito_E :   CARTERA.captura.ObjGeneral.sFechaDeposito_E,
                  sMoneda_E :          CARTERA.captura.ObjGeneral.sMoneda_E,
                  sImporteDeposito_E : CARTERA.captura.ObjGeneral.sImporteDeposito_E,
                  sBancoServicio_E :   CARTERA.captura.ObjGeneral.sBancoServicio_E,
                  sRefBanco_E :        CARTERA.captura.ObjGeneral.sRefBanco_E,
                  sFechaProbPago_E :   CARTERA.captura.ObjGeneral.sFechaProbPago_E,
                  sRefInter_E :        CARTERA.captura.ObjGeneral.sRefInter_E,
                  sFechaVencimiento_E :CARTERA.captura.ObjGeneral.sFechaVencimiento_E,
                  sClaveRefInter_E :   CARTERA.captura.ObjGeneral.sClaveRefInter_E,
                  sNombreBanco_E :     CARTERA.captura.ObjGeneral.sNombreBanco_E
               }         
            }
         );
      }
            
      // Iniciamos el Grid con campos sin valor debajo de los ya existentes
      var record = [];      
      record.push(new Ext.data.Record({
         SUCURSAL:         '',
         SUBAPLICACION:    '',
         PRESTAMO:         '',
         ACREDITADO:       '',
         FECHA_OPERACION:  '',
         FECHA_VENCIMIENTO:'',
         FECHA_PAGO:       '',
         SALDO_INSOLUTO:   '0',
         TASA:             '0',
         CAPITAL:          '0',
         DIAS:             '',
         INTERESES:        '0',
         COMISION:         '0',
         IVA:              '0',
         CONCEPTOPAGO:     '', 
         IMPORTEPAGO:      '0'        
      }));      
      gridDetallesEditor.getStore().add(record);
      gridDetallesEditor.getStore().commitChanges();
      
      fnActualizaGridEditable(null, null, null);
   }
   
   // Confirma la edicion del grid Editable, almacenando los registros ingresados en variables de arreglos
   function fnActualizaGridEditable(grid, rowIdx, colIds){
      CARTERA.captura.ObjGeneral.sImporteDeposito_E = Ext.getCmp('id_sImporteDeposito_E').getValue();
      CARTERA.captura.ObjGeneral.importePago        = 0;    
      
      // Valido si todos los campos son correctos para poder agregar una fila mas al grid
      if(!fnValidaAgregarFilaGridDetallesEditor(rowIdx) && rowIdx!=null){
         return;
      }
      
      // Almaceno los valores del grid en arreglos
      gridDetallesEditor.getStore().each(function(record){            
         CARTERA.captura.ObjGeneral.arrSucursal.push(record.data.SUCURSAL);
         CARTERA.captura.ObjGeneral.arrSubAplicacion.push(record.data.SUBAPLICACION);
         CARTERA.captura.ObjGeneral.arrPrestamo.push(record.data.PRESTAMO);
         CARTERA.captura.ObjGeneral.arrAcreditado.push(record.data.ACREDITADO);
         CARTERA.captura.ObjGeneral.arrFechaOperacion.push(record.data.FECHA_OPERACION);
         CARTERA.captura.ObjGeneral.arrFechaVencimientol.push(record.data.FECHA_VENCIMIENTO);
         CARTERA.captura.ObjGeneral.arrFechaPago.push(record.data.FECHA_PAGO);
         CARTERA.captura.ObjGeneral.arrFechaInsoluto.push(record.data.FECHA_INSOLUTO);
         CARTERA.captura.ObjGeneral.arrSaldoInsoluto.push(record.data.SALDO_INSOLUTO);
         CARTERA.captura.ObjGeneral.arrTasa.push(record.data.TASA);
         CARTERA.captura.ObjGeneral.arrCapital.push(record.data.CAPITAL);
         CARTERA.captura.ObjGeneral.arrDias.push(record.data.DIAS);
         CARTERA.captura.ObjGeneral.arrIntereses.push(record.data.INTERESES);
         CARTERA.captura.ObjGeneral.arrComision.push(record.data.COMISION);
         CARTERA.captura.ObjGeneral.arrIva.push(record.data.IVA);
         CARTERA.captura.ObjGeneral.arrConceptoPago.push(record.data.CONCEPTOPAGO);
         CARTERA.captura.ObjGeneral.arrImportePago.push(record.data.IMPORTEPAGO);
      });         
      
      var record = [];      
      record.push(new Ext.data.Record({
         SUCURSAL:         '',
         SUBAPLICACION:    '',
         PRESTAMO:         '',
         ACREDITADO:       '',
         FECHA_OPERACION:  '',
         FECHA_VENCIMIENTO:'',
         FECHA_PAGO:       '',
         SALDO_INSOLUTO:   '0',
         TASA:             '0',
         CAPITAL:          '0',
         DIAS:             '',
         INTERESES:        '0',
         COMISION:         '0',
         IVA:              '0',
         CONCEPTOPAGO:     '',
         IMPORTEPAGO:      '0'        
      }));                  
      gridDetallesEditor.getStore().add(record);
      gridDetallesEditor.getStore().commitChanges();
      
      fnActualizaGridEditableTotales(); //Actualiza grid Totales
   }
   
   // Valido si todos los campos son correctos para poder agregar una fila mas al grid
   function fnValidaAgregarFilaGridDetallesEditor(rowIdx){
      var mensaje = '';
      var record = gridDetallesEditor.getStore().getAt(rowIdx);            
      
      if(record){
         mensaje += (record.data.PRESTAMO==''?'<br />No puede dejar el campo Pr�stamo vac�o':'');
         mensaje += (record.data.ACREDITADO==''?'<br />No puede dejar el campo Acreditado vac�o':'');
         mensaje += (record.data.FECHA_OPERACION==''?'<br />No puede dejar el campo de Fecha de Operacion vac�o':'');
         mensaje += (record.data.FECHA_VENCIMIENTO==''?'<br />No puede dejar el campo de Fecha de Vencimiento vac�o':'');
         mensaje += (record.data.FECHA_PAGO==''?'<br />No puede dejar el campo de Fecha de Pago vac�o':'');
         mensaje += (record.data.SALDO_INSOLUTO==''?'<br />No puede dejar el campo de Saldo Insoluto vac�o':'');
         mensaje += (record.data.TASA==''?'<br />No puede dejar el campo de Tasa Base vac�o':'');
         mensaje += (record.data.CAPITAL==''?'<br />No puede dejar el campo de Capital vac�o':'');
         mensaje += (record.data.DIAS==''?'<br />No puede dejar el campo de Dias vac�o':'');
         mensaje += (record.data.INTERESES==''?'<br />No puede dejar el campo de Interes vac�o':'');
         mensaje += (record.data.COMISION==''?'<br />No puede dejar el campo de Comisi�n vac�o':'');
         mensaje += (record.data.IVA==''?'<br />No puede dejar el campo de IVA vac�o':'');
         mensaje += (record.data.CONCEPTOPAGO==''?'<br />No puede dejar el campo de Concepto vac�o':'');
         mensaje += (record.data.IMPORTEPAGO==''?'<br />No puede dejar el campo de Importe vac�o':'');
         
         if(mensaje!=''){
            Ext.Msg.show({
               title:   'Campos vac�os',
               msg:     mensaje,
               buttons: Ext.Msg.OK,
               icon:    Ext.Msg.INFO
            });
            return false;
         }
      }
      return true;      
   }
   
   // Actualiza Grid Totales
   function fnActualizaGridEditableTotales(){
      var tSaldoInsoluto = 0;
      var tTasa = 0;
      var tCapital = 0;
      var tInteres = 0;
      var tComision= 0;
      var tIva = 0;
      var tImportePago = 0;
      // Sumo los valores de los registros del grid editor...
      for(var i=0; i<CARTERA.captura.ObjGeneral.arrSucursal.length; i++){
         tSaldoInsoluto += Number(CARTERA.captura.ObjGeneral.arrSaldoInsoluto     [i]);
         tTasa          += Number(CARTERA.captura.ObjGeneral.arrTasa              [i]);
         tCapital       += Number(CARTERA.captura.ObjGeneral.arrCapital           [i]);
         tInteres       += Number(CARTERA.captura.ObjGeneral.arrIntereses         [i]);
         tComision      += Number(CARTERA.captura.ObjGeneral.arrComision          [i]);
         tIva           += Number(CARTERA.captura.ObjGeneral.arrIva               [i]);
         tImportePago   += Number(CARTERA.captura.ObjGeneral.arrImportePago       [i]);
      }
      // creo los records del grid de totales
      var record = [];      
      record.push(new Ext.data.Record({
         LABEL_TOTAL:      '<b>TOTALES</b>',
         SALDO_INSOLUTO:   Ext.util.Format.number( tSaldoInsoluto, '$0,0.00' ),
         TASA:             Ext.util.Format.number( tTasa,          '$0,0.00' ),
         CAPITAL:          Ext.util.Format.number( tCapital,       '$0,0.00' ),
         INTERESES:        Ext.util.Format.number( tInteres,       '$0,0.00' ),
         COMISION:         Ext.util.Format.number( tComision,      '$0,0.00' ),
         IVA:              Ext.util.Format.number( tIva,           '$0,0.00' ),
         IMPORTEPAGO:      Ext.util.Format.number( tImportePago,   '$0,0.00' )        
      }));
      
      gridDetallesEditorTotales.getStore().removeAll();
      gridDetallesEditorTotales.getStore().add(record);
      gridDetallesEditorTotales.getStore().commitChanges();      
   }
   
   // Obtiene Importe Pago del Grid Editable
   function fnObtieneImportePago(boton, valor){
      if(!Ext.isEmpty(gridDetallesEditor.getStore().getModifiedRecords())){
         if(!Ext.isEmpty(valor)){
            // realizo la suma
            CARTERA.captura.ObjGeneral.importePago += Number(valor==''?'0':valor);
            CARTERA.captura.ObjGeneral.importePago -= Number(boton.gridEditor.field.value==''?'0':boton.gridEditor.field.value);
            
            // Actualizo el campo IMPORTE PAGO del grid de acuerdo a la suma de los campos del grid editable
            var record = gridDetallesEditor.getStore();            
            record.getAt(gridDetallesEditor.getStore().getCount()-1).data.IMPORTEPAGO = Ext.util.Format.number(CARTERA.captura.ObjGeneral.importePago, '0,0.00');
            record.getAt(gridDetallesEditor.getStore().getCount()-1).commit();
         }      
      }
   }
   
   // Accion de la construccion del PDF del Acuse de la Transferencia de Solicitud
   var fnAcuseSolPdf = function(boton){
      boton.disable();
		boton.setIconClass('loading-indicator');
      
      Ext.Ajax.request({
         url: CARTERA.captura.ObjGeneral.urlRequestData,
         params: {
            informacion:         'AcuseSolPDF',
            folioCert:           CARTERA.captura.ObjGeneral.folioCert,
            recibo:              CARTERA.captura.ObjGeneral.recibo,
            idBoton :            boton.id,
            estado:              CARTERA.captura.ObjGeneral.estado,
            sEstado_E :          CARTERA.captura.ObjGeneral.sEstado_E,
            sClaveIF_E :         CARTERA.captura.ObjGeneral.sClaveIF_E,
            sClaveEstado_E :     CARTERA.captura.ObjGeneral.sClaveEstado_E,
            sClaveMoneda_E :     CARTERA.captura.ObjGeneral.sClaveMoneda_E,
            sFechaDeposito_E :   CARTERA.captura.ObjGeneral.sFechaDeposito_E,
            sMoneda_E :          CARTERA.captura.ObjGeneral.sMoneda_E,
            sImporteDeposito_E : CARTERA.captura.ObjGeneral.sImporteDeposito_E,
            sBancoServicio_E :   CARTERA.captura.ObjGeneral.sBancoServicio_E,
            sRefBanco_E :        CARTERA.captura.ObjGeneral.sRefBanco_E,
            sFechaProbPago_E :   CARTERA.captura.ObjGeneral.sFechaProbPago_E,
            sRefInter_E :        CARTERA.captura.ObjGeneral.sRefInter_E,
            sFechaVencimiento_E :CARTERA.captura.ObjGeneral.sFechaVencimiento_E,
            sClaveRefInter_E :   CARTERA.captura.ObjGeneral.sClaveRefInter_E,
            sNombreBanco_E :     CARTERA.captura.ObjGeneral.sNombreBanco_E
         },
         success : function(response, request){
            var jsonData = Ext.decode(response.responseText);
            var btnGenerarPDF = Ext.getCmp(jsonData.id);
            btnGenerarPDF.setIconClass('icoPdf');
            
            if(jsonData.success == true) {
               var btnBajarPDF = Ext.getCmp(jsonData.id + '_descarga');
               btnGenerarPDF.hide();
               btnBajarPDF.show();
               btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
               btnBajarPDF.setHandler( function(boton, evento) {
                  var forma = Ext.getDom('formAux');
                  forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
                  forma.submit();
               });
            } else {
               btnGenerarPDF.enable();
               NE.util.mostrarConnError(response,opts);
            }
         }
      });
   }
   
   // Submit del Formulario del Encabezado
   var fnEnviaFormaEncabezado = function(boton){
      
      panelEncabezado.buttons[0].setVisible(false);
      panelEncabezado.buttons[1].setVisible(false);
      
      CARTERA.captura.ObjGeneral.estado = 'enviarFormaEncabezado';        
      CARTERA.captura.ObjGeneral.sBancoServicio_E      = Ext.getCmp('id_sBancoServicio_E').getValue();
      CARTERA.captura.ObjGeneral.sMoneda_E             = Ext.getCmp('id_sClaveMoneda_E').getValue();
      
      
      if((CARTERA.captura.ObjGeneral.sImporteDeposito_E!=Ext.getCmp('id_sImporteDeposito_E').getValue()) && boton.id!='btncontinuarEditable'){
         CARTERA.captura.ObjGeneral.agregarGridEditable();
      }else{      
         pnl.removeAll();
                  
         var ObjDetallesBtn = {
            xtype: 'panel', id:'pnlBtnTransmiteSol', width: 900, style: 'margin: 0 auto',
            bbar: [
               '-','->','-',
               {xtype: 'button', text: 'Transmitir Solicitudes', iconCls:'icoContinuar', id:'btnTransferDetalles', handler:fnTransmiritSolicitudes},
               '-',
               {xtype: 'button', text: 'Cancelar', iconCls: 'cancelar', handler:fnCancelarGral}
            ]
         };
                    
         gridEncabezado.show();
         gridEncabezado.getStore().load({
            params: {
               estado:              CARTERA.captura.ObjGeneral.estado,
               sEstado_E :          CARTERA.captura.ObjGeneral.sEstado_E,
               sClaveIF_E :         CARTERA.captura.ObjGeneral.sClaveIF_E,
               sClaveEstado_E :     CARTERA.captura.ObjGeneral.sClaveEstado_E,
               sClaveMoneda_E :     CARTERA.captura.ObjGeneral.sClaveMoneda_E,
               sFechaDeposito_E :   CARTERA.captura.ObjGeneral.sFechaDeposito_E,
               sMoneda_E :          CARTERA.captura.ObjGeneral.sMoneda_E,
               sImporteDeposito_E : CARTERA.captura.ObjGeneral.sImporteDeposito_E,
               sBancoServicio_E :   CARTERA.captura.ObjGeneral.sBancoServicio_E,
               sRefBanco_E :        CARTERA.captura.ObjGeneral.sRefBanco_E,
               sFechaProbPago_E :   CARTERA.captura.ObjGeneral.sFechaProbPago_E,
               sRefInter_E :        CARTERA.captura.ObjGeneral.sRefInter_E,
               sFechaVencimiento_E :CARTERA.captura.ObjGeneral.sFechaVencimiento_E,
               sClaveRefInter_E :   CARTERA.captura.ObjGeneral.sClaveRefInter_E,
               sNombreBanco_E :     CARTERA.captura.ObjGeneral.sNombreBanco_E
            }
         });
         gridEncabezado.getStore().commitChanges();
            
         if(CARTERA.captura.ObjGeneral.tipoIF=='B'){              
            gridDetalles_2.show();
            gridDetalles_2.getStore().load({
               params: {
                  estado:              CARTERA.captura.ObjGeneral.estado,
                  conceptoPagoIni:     CARTERA.captura.ObjGeneral.conceptoPagoIni,
                  sEstado_E :          CARTERA.captura.ObjGeneral.sEstado_E,
                  sClaveIF_E :         CARTERA.captura.ObjGeneral.sClaveIF_E,
                  sClaveEstado_E :     CARTERA.captura.ObjGeneral.sClaveEstado_E,
                  sClaveMoneda_E :     CARTERA.captura.ObjGeneral.sClaveMoneda_E,
                  sFechaDeposito_E :   CARTERA.captura.ObjGeneral.sFechaDeposito_E,
                  sMoneda_E :          CARTERA.captura.ObjGeneral.sMoneda_E,
                  sImporteDeposito_E : CARTERA.captura.ObjGeneral.sImporteDeposito_E,
                  sBancoServicio_E :   CARTERA.captura.ObjGeneral.sBancoServicio_E,
                  sRefBanco_E :        CARTERA.captura.ObjGeneral.sRefBanco_E,
                  sFechaProbPago_E :   CARTERA.captura.ObjGeneral.sFechaProbPago_E,
                  sRefInter_E :        CARTERA.captura.ObjGeneral.sRefInter_E,
                  sFechaVencimiento_E :CARTERA.captura.ObjGeneral.sFechaVencimiento_E,
                  sClaveRefInter_E :   CARTERA.captura.ObjGeneral.sClaveRefInter_E,
                  sNombreBanco_E :     CARTERA.captura.ObjGeneral.sNombreBanco_E
               }
            });
            gridDetalles_2.getStore().commitChanges();
                     
            pnl.insert(0, gridEncabezado);
            pnl.insert(1, NE.util.getEspaciador(10));
            pnl.insert(2, gridDetalles_2);
            pnl.insert(3, ObjDetallesBtn);
            pnl.doLayout();
         } else {                
            gridDetalles.show();
            gridDetalles.getStore().load({
               params: {
                  estado:              CARTERA.captura.ObjGeneral.estado,
                  sEstado_E :          CARTERA.captura.ObjGeneral.sEstado_E,
                  sClaveIF_E :         CARTERA.captura.ObjGeneral.sClaveIF_E,
                  sClaveEstado_E :     CARTERA.captura.ObjGeneral.sClaveEstado_E,
                  sClaveMoneda_E :     CARTERA.captura.ObjGeneral.sClaveMoneda_E,
                  sFechaDeposito_E :   CARTERA.captura.ObjGeneral.sFechaDeposito_E,
                  sMoneda_E :          CARTERA.captura.ObjGeneral.sMoneda_E,
                  sImporteDeposito_E : CARTERA.captura.ObjGeneral.sImporteDeposito_E,
                  sBancoServicio_E :   CARTERA.captura.ObjGeneral.sBancoServicio_E,
                  sRefBanco_E :        CARTERA.captura.ObjGeneral.sRefBanco_E,
                  sFechaProbPago_E :   CARTERA.captura.ObjGeneral.sFechaProbPago_E,
                  sRefInter_E :        CARTERA.captura.ObjGeneral.sRefInter_E,
                  sFechaVencimiento_E :CARTERA.captura.ObjGeneral.sFechaVencimiento_E,
                  sClaveRefInter_E :   CARTERA.captura.ObjGeneral.sClaveRefInter_E,
                  sNombreBanco_E :     CARTERA.captura.ObjGeneral.sNombreBanco_E
               }
            });
            gridDetalles.getStore().commitChanges();
                     
            pnl.insert(0, gridEncabezado);
            pnl.insert(1, NE.util.getEspaciador(10));
            pnl.insert(2, gridDetalles);
            pnl.insert(3, ObjDetallesBtn);
            pnl.doLayout();
         }  
      }
   }
   
   // Accion del boton Cancelar
   function fnCancelarGral(){
      window.location.href='13forma10ext.jsp';
   }
   
   
   var confirmarAcuse = function(pkcs7, textoFirmado){
     
      if (Ext.isEmpty(pkcs7) ) {
         Ext.Msg.show({
            title:'Firmar',
            msg:'Error en el proceso de Firmado. <br/>No se puede continuar',
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.ERROR
         });
         return false;	//Error en la firma. Termina...
		 
      }else  {
	  
		  Ext.Ajax.request({
			 url:CARTERA.captura.ObjGeneral.urlRequestData,
			 params:{
				informacion:         'ConfirmarCertificado', 
				Pkcs7:               pkcs7, 
				TextoFirmado:        textoFirmado,
				estado:              CARTERA.captura.ObjGeneral.estado,
				sEstado_E :          CARTERA.captura.ObjGeneral.sEstado_E,
				sClaveIF_E :         CARTERA.captura.ObjGeneral.sClaveIF_E,
				sClaveEstado_E :     CARTERA.captura.ObjGeneral.sClaveEstado_E,
				sClaveMoneda_E :     CARTERA.captura.ObjGeneral.sClaveMoneda_E,
				sFechaDeposito_E :   CARTERA.captura.ObjGeneral.sFechaDeposito_E,
				sMoneda_E :          CARTERA.captura.ObjGeneral.sMoneda_E,
				sImporteDeposito_E : CARTERA.captura.ObjGeneral.sImporteDeposito_E,
				sBancoServicio_E :   CARTERA.captura.ObjGeneral.sBancoServicio_E,
				sRefBanco_E :        CARTERA.captura.ObjGeneral.sRefBanco_E,
				sFechaProbPago_E :   CARTERA.captura.ObjGeneral.sFechaProbPago_E,
				sRefInter_E :        CARTERA.captura.ObjGeneral.sRefInter_E,
				sFechaVencimiento_E :CARTERA.captura.ObjGeneral.sFechaVencimiento_E,
				sClaveRefInter_E :   CARTERA.captura.ObjGeneral.sClaveRefInter_E,
				sNombreBanco_E :     CARTERA.captura.ObjGeneral.sNombreBanco_E
			 },
			 failure: function(){
				Ext.Msg.show({
				   title: 'Firmar',
				   msg: 'Ocurri� un error durante el Proceso de Firma',
				   buttons: Ext.Msg.OK,
				   icon: Ext.MessageBox.ERROR
				});
			 },
			 success: CARTERA.captura.ObjGeneral.transmiteSolExitoso
		  });
	  
	  }
	 
   
   }
   
   
   
   
   
   // Accion del Boton Transmitir solicitudes
   function fnTransmiritSolicitudes(boton){
      var textoFirmado = "Encabezado "+
			"\nDireccion Estatal: "+ CARTERA.captura.ObjGeneral.sEstado_E+
			"\nMoneda: "+CARTERA.captura.ObjGeneral.sMoneda_E+
			"\nBancoServicio: "+CARTERA.captura.ObjGeneral.sNombreBanco_E+
			"\nFecha Vencimiento: "+CARTERA.captura.ObjGeneral.sFechaVencimiento_E+
			"\nFecha Probable de Pago: "+CARTERA.captura.ObjGeneral.sFechaProbPago_E+
			"\nFecha de Deposito: "+CARTERA.captura.ObjGeneral.sFechaDeposito_E+
			"\nImporte Deposito: "+Ext.util.Format.number(CARTERA.captura.ObjGeneral.sImporteDeposito_E,'0,0.00')+
			"\nAl transmitir este MENSAJE DE DATOS, usted esta enviando"+
			"\nel PAGO que ser� registrado en Nacional Financiera";
      
     textoFirmado=textoFirmado.replace('�','a').replace('�','e').replace('�','i').replace('�','o').replace('�','u');
     textoFirmado=textoFirmado.replace('�','A').replace('�','E').replace('�','I').replace('�','O').replace('�','U');
     
      
	  NE.util.obtenerPKCS7(confirmarAcuse , textoFirmado );
	  
	  
	  
   }
   
   function validaPrestamoEditable(boton, valor){
      Ext.Ajax.request({
         url: CARTERA.captura.ObjGeneral.urlRequestData,
         params:{ informacion: 'validaPrestamoEditable', prestamo: valor },
         success: function(response, request){
            var jsonData = Ext.decode(response.responseText);
            if(jsonData.numeroPrestamoValido===false){
               Ext.Msg.show({
                  title:   'N�mero inv�lido',
                  msg:     jsonData.mensaje,
                  icon:    Ext.Msg.INFO,
                  buttons: Ext.Msg.OK,
                  fn:      function(){ 
                     var store            = gridDetallesEditor.getStore();
                     var row              = boton.gridEditor.focus().row;
                     var columnModelGrid  = gridDetallesEditor.getColumnModel();
                     
                     boton.gridEditor.focus().record.data.PRESTAMO="";
                     gridDetallesEditor.startEditing(row, columnModelGrid.findColumnIndex('PRESTAMO'));
                     store.commitChanges();
                  }
               });
            }
         }
      });
   }
// -------------------------- COMPONENTES --------------------------------------
   var gridCapturaIndividual_1 = new Ext.grid.EditorGridPanel({
      id:      'gridCapturaIndividual_1',
      store:   new Ext.data.JsonStore({
         root: 'registros',
         id:   'storeCapturaIndividual_1',
         url:  '13forma10ext.data.jsp',
         baseParams: {
            //informacion:   'storeCapturaIndividual_1'
         },
         fields:[ 
            {	name: 'CLAVE_MONEDA'       },
            {	name: 'MONEDA'				   },
            {	name: 'FECHA_VENCIMIENTO'	},
            {	name: 'FECHA_PROB_PAGO'		},
            {	name: 'IMPORTE_DEPOSITO'	},
            {	name:	'FECHA_DEPOSITO'		},
            {  name: 'NOMBRE_ESTADO'      },
            {  name: 'CLAVE_ESTADO'       },
            {  name: 'REF_INTER'          },
            {  name: 'CLAVE_REF_INTER'    },
            {  name: 'CLAVE_IF'           },
            {  name: 'conceptoPagoIni'    }
         ],
         totalProperty		: 	'total',
         messageProperty	: 	'msg',
         autoLoad				:	false
      }),
      stripeRows: true,loadMask: true,monitorResize: true,frame: false,style:'margin: 0 auto',
      width:   700, height:  300,
      columns: [{         
         header: 'Moneda',
         tooltip: 'Moneda',
         dataIndex: 'MONEDA',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Fecha vencimiento',
         tooltip: 'Fecha vencimiento',
         dataIndex: 'FECHA_VENCIMIENTO',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center'
      },{         
         header: 'Fecha probable de pago',
         tooltip: 'Fecha probable de pago',
         dataIndex: 'FECHA_PROB_PAGO',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center'
      },{         
         header: 'Fecha de dep�sito',
         tooltip: 'Fecha de dep�sito',
         dataIndex: 'FECHA_DEPOSITO',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center'
      },{         
         header: 'Total a Pagar',
         tooltip: 'Total a Pagar',
         dataIndex: 'IMPORTE_DEPOSITO',
         sortable: true,
         hideable: false,
         width: 118,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{
         xtype :  'actioncolumn',
         header:  'Editar',
         dataIndex:  '',
         renderer :  function(){ return 'Editar / Enviar ' },
         items :  [{
            getClass:   function(valor, metadata, registro, rowIndex, colIndex, store) {	
               return 'icoContinuar'
            },
            tooltip  :  'Editar / Enviar',
            handler  :  function(grid, rowIdx, colIds){   
               // Muestro botones del formulario del encabezado
               panelEncabezado.buttons[0].setVisible(true);
               panelEncabezado.buttons[1].setVisible(true);
               // Oculto grid's
               if(gridDetallesEditorTotales.isVisible()){
                  gridDetallesEditorTotales.hide();
                  gridDetallesEditor.hide();
               }
                              
               if(!CARTERA.captura.ObjGeneral.bandera){
                  panelEncabezado.insert(0, new formaEncabezado());
                  panelEncabezado.setTitle('Encabezado');
                  panelEncabezado.show();
                  pnl.insert(5, panelEncabezado);
                  pnl.doLayout();
               }
               CARTERA.captura.ObjGeneral.mostrarEncabezado(grid.getStore().getAt(rowIdx));
            }
         }]        
      }]
   });
   
   var gridEncabezado = new Ext.grid.EditorGridPanel({
      id:      'gridEncabezado',
      title:   'Encabezado',
      store:   new Ext.data.JsonStore({
         root: 'registros',
         id:   'storeEncabezado',
         url:  '13forma10ext.data.jsp',
         baseParams: {
            informacion:   'storeEncabezado'
         },
         fields:[ 
            {	name: 'CLAVE_MONEDA'          },
            {	name: 'SMONEDA_E'				   },
            {	name: 'SFECHAVENCIMIENTO_E'	},
            {	name: 'SFECHAPROBPAGO_E'		},
            {	name: 'SIMPORTEDEPOSITO_E'	   },
            {	name:	'SFECHADEPOSITO_E'		},
            {  name: 'SESTADO_E'             },
            {  name: 'CLAVE_ESTADO'          },
            {  name: 'REF_INTER'             },
            {  name: 'CLAVE_REF_INTER'       },
            {  name: 'CLAVE_IF'              },
            {  name: 'SBANCOSERVICIO_E'      },
            {  name: 'NOMBRE_BANCO'          }
         ],
         totalProperty		: 	'total',
         messageProperty	: 	'msg',
         autoLoad				:	false,
         listeners         :  {
            beforeLoad : {
               fn: function(store, options){ }
            },
            exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					}
				}
         }
      }),
      stripeRows: true,	loadMask: true,	monitorResize: true,frame: false,style: 'margin: 0 auto',
      width:   900, height:  200,
      columns: [{         
         header: 'Direcci�n Estatal',
         tooltip: 'Direcci�n Estatal',
         dataIndex: 'SESTADO_E',
         sortable: true,
         hideable: false,
         width: 150,
         align: 'center'
      },{         
         header: 'Moneda',
         tooltip: 'Moneda',
         dataIndex: 'SMONEDA_E',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Banco Servicio',
         tooltip: 'Banco Servicio',
         dataIndex: 'SNOMBREBANCO_E',
         sortable: true,
         hideable: false,
         width: 150,
         align: 'center'
      },{         
         header: 'Fecha vencimiento',
         tooltip: 'Fecha vencimiento',
         dataIndex: 'SFECHAVENCIMIENTO_E',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center'
      },{         
         header: 'Fecha probable de pago',
         tooltip: 'Fecha probable de pago',
         dataIndex: 'SFECHAPROBPAGO_E',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center'
      },{         
         header: 'Fecha de dep�sito',
         tooltip: 'Fecha de dep�sito',
         dataIndex: 'SFECHADEPOSITO_E',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center'
      },{         
         header: 'Importe de Dep�sito',
         tooltip: 'Importe de Dep�sito',
         dataIndex: 'SIMPORTEDEPOSITO_E',
         sortable: true,
         hideable: false,
         width: 118,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      }]
   });
   
   var gridDetalles = new Ext.grid.EditorGridPanel({
      id:      'gridDetalles',
      title:   'Detalles',
      store:   new Ext.data.JsonStore({
         root: 'registros',
         id:   'storeDetalles',
         url:  '13forma10ext.data.jsp',
         baseParams: {
            informacion:   'storeDetalles'
         },
         fields:[ 
            {	name: 'SUBAPLICACION'      },
            {	name: 'PRESTAMO'				},
            {	name: 'SIRAC'	            },
            {	name: 'CAPITAL'		      },
            {	name: 'INTERES'	         },
            {	name:	'MORATORIOS'		   },
            {  name: 'SUBSIDIO'           },
            {  name: 'COMISION'           },
            {  name: 'IVA'                },
            {  name: 'IMPPORTEPAGO'       },
            {  name: 'CONCEPTOPAGO'       },
            {  name: 'ORIGENPAGO'         }
         ],
         totalProperty		: 	'total',
         messageProperty	: 	'msg',
         autoLoad				:	true
      }),
      stripeRows: true,	loadMask: true,	monitorResize: true,frame: false,style: 'margin: 0 auto',
      width:   900, height:  200,
      columns: [{         
         header: 'Subaplicaci�n',
         tooltip: 'Subaplicaci�n',
         dataIndex: 'SUBAPLICACION',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Pr�stamo',
         tooltip: 'Pr�stamo',
         dataIndex: 'PRESTAMO',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Clave SIRAC',
         tooltip: 'Clave SIRAC',
         dataIndex: 'SIRAC',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Capital',
         tooltip: 'Capital',
         dataIndex: 'CAPITAL',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'Inter�s',
         tooltip: 'Inter�s',
         dataIndex: 'INTERES',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'Moratorios',
         tooltip: 'Moratorios',
         dataIndex: 'MORATORIOS',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'Subsidio',
         tooltip: 'Subsidio',
         dataIndex: 'SUBSIDIO',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'Comisi�n',
         tooltip: 'Comisi�n',
         dataIndex: 'COMISION',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'IVA',
         tooltip: 'IVA',
         dataIndex: 'IVA',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'Importe Pago',
         tooltip: 'Importe Pago',
         dataIndex: 'IMPORTEPAGO',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'Concepto pago',
         tooltip: 'Concepto pago',
         dataIndex: 'CONCEPTOPAGO',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Origen pago',
         tooltip: 'Origen pago',
         dataIndex: 'ORIGENPAGO',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      }]
   });
   
   var gridDetalles_2 = new Ext.grid.EditorGridPanel({
      id:      'gridDetalles_2',
      title:   'Detalles',
      store:   new Ext.data.JsonStore({
         root: 'registros',
         id:   'storeDetalles',
         url:  '13forma10ext.data.jsp',
         baseParams: {
            informacion:   'storeDetalles_2'
         },
         fields:[ 
            {  name: 'SUCURSAL'           },
            {	name: 'SUBAPLICACION'      },
            {	name: 'PRESTAMO'				},
            {  name: 'ACREDITADO'         },
            {	name: 'FECHA_OPERACION'	   },
            {	name: 'FECHA_VENCIMIENTO'	},
            {	name: 'FECHA_PAGO'	      },
            {	name:	'SALDO_INSOLUTO'		},
            {  name: 'TASA'               },
            {  name: 'CAPITAL'            },
            {  name: 'DIAS'               },
            {  name: 'INTERESES'          },
            {  name: 'COMISION'           },
            {  name: 'IVA'                },
            {  name: 'CONCEPTOPAGO'       },
            {  name: 'IMPORTEPAGO'        }
         ],
         totalProperty		: 	'total',
         messageProperty	: 	'msg',
         autoLoad				:	true
      }),
      stripeRows: true,	loadMask: true,	monitorResize: true,frame: false,style: 'margin: 0 auto',
      width:   900, height:  300,
      columns: [{         
         header: 'Sucursal',
         tooltip: 'Sucursal',
         dataIndex: 'SUCURSAL',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Subaplicaci�n',
         tooltip: 'Subaplicaci�n',
         dataIndex: 'SUBAPLICACION',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Pr�stamo',
         tooltip: 'Pr�stamo',
         dataIndex: 'PRESTAMO',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Acreditado',
         tooltip: 'Acreditado',
         dataIndex: 'ACREDITADO',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Fecha de<br>Operaci�n',
         tooltip: 'Fecha de Operaci�n',
         dataIndex: 'FECHA_OPERACION',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Fecha de <br>Vencimiento',
         tooltip: 'Fecha de Vencimiento',
         dataIndex: 'FECHA_VENCIMIENTO',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Fecha de Pago',
         tooltip: 'Fecha de <br>Pago',
         dataIndex: 'FECHA_PAGO',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Saldo Insoluto',
         tooltip: 'Saldo <br>Insoluto',
         dataIndex: 'SALDO_INSOLUTO',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '0,0.00');}
      },{         
         header: 'Tasa',
         tooltip: 'Tasa',
         dataIndex: 'TASA',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center'
      },{         
         header: 'Capital',
         tooltip: 'Capital',
         dataIndex: 'CAPITAL',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'D�as',
         tooltip: 'D�as',
         dataIndex: 'DIAS',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{         
         header: 'Inter�s',
         tooltip: 'Inter�s',
         dataIndex: 'INTERESES',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'Comisi�n',
         tooltip: 'Comisi�n',
         dataIndex: 'COMISION',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'IVA',
         tooltip: 'IVA',
         dataIndex: 'IVA',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'Importe Pago',
         tooltip: 'Importe Pago',
         dataIndex: 'IMPORTEPAGO',
         sortable: true,
         hideable: false,
         width: 110,
         align: 'center',
         renderer: function(o){return Ext.util.Format.number(o, '$ 0,0.00');}
      },{         
         header: 'Concepto pago',
         tooltip: 'Concepto pago',
         dataIndex: 'CONCEPTOPAGO',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      }]
   });
   
   var gridDetallesEditor = new Ext.grid.EditorGridPanel({
      id:      'gridDetallesEditor',
      title:   'Detalles',
      store:   new Ext.data.JsonStore({
         root: 'registros',
         id:   'storeDetalles',
         url:  '13forma10ext.data.jsp',
         baseParams: {
            informacion:   'storeDetallesEditor',
            estado:        'enviarFormaEncabezado'
         },
         fields:[ 
            {  name: 'SUCURSAL'           },
            {	name: 'SUBAPLICACION'      },
            {	name: 'PRESTAMO'				},
            {  name: 'ACREDITADO'         },
            {	name: 'FECHA_OPERACION'	   },
            {	name: 'FECHA_VENCIMIENTO'	},
            {	name: 'FECHA_PAGO'	      },
            {	name:	'SALDO_INSOLUTO'		},
            {  name: 'TASA'               },
            {  name: 'CAPITAL'            },
            {  name: 'DIAS'               },
            {  name: 'INTERESES'          },
            {  name: 'COMISION'           },
            {  name: 'IVA'                },
            {  name: 'CONCEPTOPAGO'       },
            {  name: 'IMPORTEPAGO'        }
         ],
         totalProperty		: 	'total',
         messageProperty	: 	'msg',
         autoLoad				:	true
      }),
      stripeRows: true,	loadMask: true,	monitorResize: true,frame: false,style: 'margin: 0 auto',clicksToEdit:1,
      width:   940, height:  300,
      columns: [{         
         header: 'Sucursal',
         tooltip: 'Sucursal',
         dataIndex: 'SUCURSAL',
         sortable: true,
         hideable: false,
         width: 50,
         align: 'center'
      },{         
         header: 'Subaplicaci�n',
         tooltip: 'Subaplicaci�n',
         dataIndex: 'SUBAPLICACION',
         sortable: true,
         hideable: false,
         width: 50,
         align: 'center'
      },{         
         editor:{xtype:'textfield', allowBlank:false, listeners: { change: validaPrestamoEditable}},
         header: 'Pr�stamo',
         tooltip: 'Pr�stamo',
         dataIndex: 'PRESTAMO',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         editor:{xtype:'textfield', allowBlank:false},      
         header: 'Acreditado',
         tooltip: 'Acreditado',
         dataIndex: 'ACREDITADO',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{ 
         editor:{xtype:'datefield', allowBlank:false},        
         header: 'Fecha de<br>Operaci�n',
         tooltip: 'Fecha de Operaci�n',
         dataIndex: 'FECHA_OPERACION',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{  
         editor:{xtype:'datefield', allowBlank:false},       
         header: 'Fecha de <br>Vencimiento',
         tooltip: 'Fecha de Vencimiento',
         dataIndex: 'FECHA_VENCIMIENTO',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         editor:{xtype:'datefield', allowBlank:false},      
         header: 'Fecha de <br>Pago',
         tooltip: 'Fecha de Pago',
         dataIndex: 'FECHA_PAGO',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{  
         editor:{xtype:'textfield', allowBlank:false},       
         header: 'Saldo <br>Insoluto',
         tooltip: 'Saldo Insoluto',
         dataIndex: 'SALDO_INSOLUTO',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{    
         editor:{xtype:'textfield', allowBlank:false},     
         header: 'Tasa',
         tooltip: 'Tasa',
         dataIndex: 'TASA',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{ 
         editor:{xtype:'textfield', allowBlank:false, listeners:{ change: fnObtieneImportePago }},      
         header: 'Capital',
         tooltip: 'Capital',
         dataIndex: 'CAPITAL',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{     
         editor:{xtype:'textfield', allowBlank:false},    
         header: 'D�as',
         tooltip: 'D�as',
         dataIndex: 'DIAS',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         editor:{xtype:'textfield', allowBlank:false, listeners:{ change: fnObtieneImportePago }},      
         header: 'Inter�s',
         tooltip: 'Inter�s',
         dataIndex: 'INTERESES',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         editor:{xtype:'textfield', allowBlank:false, listeners:{ change: fnObtieneImportePago }},      
         header: 'Comisi�n',
         tooltip: 'Comisi�n',
         dataIndex: 'COMISION',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         editor:{xtype:'textfield', allowBlank:false, listeners:{ change: fnObtieneImportePago }},      
         header: 'IVA',
         tooltip: 'IVA',
         dataIndex: 'IVA',
         sortable: true,
         hideable: false,
         width: 70,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         editor:{xtype:'textfield', allowBlank:false},      
         header: 'Importe <br>Pago',
         tooltip: 'Importe Pago',
         dataIndex: 'IMPORTEPAGO',
         sortable: true,
         hideable: false,
         width: 80,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{    
         editor: {
            xtype: 'combo',
            triggerAction: 'all',
            mode:          'local',
            displayField:  'descripcion',
            valueField:    'clave',
            typeAhead:     true, allowBlank:false,
            editable:      false,
            forceSelection:true,
            store:         new Ext.data.SimpleStore({
               fields : ['clave', 'descripcion'],
               data:    [["A", "Anticipado"],["V","Vencimiento"]]
            }),
            valueField: 'clave',
            displayField: 'descripcion'
         },
         header: 'Concepto <br>pago',
         tooltip: 'Concepto pago',
         dataIndex: 'CONCEPTOPAGO',
         sortable: true,
         hideable: false,
         width: 130,
         align: 'center'
      },{
         xtype :  'actioncolumn',
         header:  'Seleccionar',
         dataIndex:  '',
         renderer :  function(){ return 'Agregar ' },
         items :  [{
            getClass:   function(valor, metadata, registro, rowIndex, colIndex, store) {	
               return 'aceptar'
            },
            tooltip  :  'Agregar',
            handler  :  fnActualizaGridEditable
         }]        
      }],
      bbar: [
         '->','-',
         {text: 'Continuar', id:'btncontinuarEditable', iconCls: 'icoContinuar', handler:fnEnviaFormaEncabezado},
         '-',
         {text: 'Limpiar', id:'clean', iconCls: 'cancelar', handler:fnActualizaGridEditable},
         '-'
      ]
   });
   
   var gridDetallesEditorTotales = new Ext.grid.EditorGridPanel({
      id:      'gridDetallesEditorTotales',
      title:   '',
      store:   new Ext.data.JsonStore({
         root: 'registros',
         id:   'storeDetalles',
         url:  '13forma10ext.data.jsp',
         baseParams: { },
         fields:[ 
            {  name: 'LABEL_TOTAL'        },
            {	name:	'SALDO_INSOLUTO'		},
            {  name: 'TASA'               },
            {  name: 'CAPITAL'            },
            {  name: 'DIAS'               },
            {  name: 'INTERESES'          },
            {  name: 'COMISION'           },
            {  name: 'IVA'                },
            {  name: 'CONCEPTOPAGO'       },
            {  name: 'IMPORTEPAGO'        }
         ],
         totalProperty		: 	'total',
         messageProperty	: 	'msg',
         autoLoad				:	true
      }),
      stripeRows: true,	loadMask: true,	monitorResize: true,frame: false,style: 'margin: 0 auto',clicksToEdit:1,
      width:   940, height:  50,
      columns: [{  
         header: '',
         tooltip: '',
         dataIndex: 'LABEL_TOTAL',
         sortable: true,
         hideable: false,
         width: 95,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{  
         header: 'Saldo Insoluto',
         tooltip: 'Saldo Insoluto',
         dataIndex: 'SALDO_INSOLUTO',
         sortable: true,
         hideable: false,
         width: 120,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{    
         header: 'Tasa',
         tooltip: 'Tasa',
         dataIndex: 'TASA',
         sortable: true,
         hideable: false,
         width: 120,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{ 
         header: 'Capital',
         tooltip: 'Capital',
         dataIndex: 'CAPITAL',
         sortable: true,
         hideable: false,
         width: 120,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         header: 'Inter�s',
         tooltip: 'Inter�s',
         dataIndex: 'INTERESES',
         sortable: true,
         hideable: false,
         width: 120,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         header: 'Comisi�n',
         tooltip: 'Comisi�n',
         dataIndex: 'COMISION',
         sortable: true,
         hideable: false,
         width: 120,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         header: 'IVA',
         tooltip: 'IVA',
         dataIndex: 'IVA',
         sortable: true,
         hideable: false,
         width: 120,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      },{   
         editor:{xtype:'textfield', allowBlank:false},      
         header: 'Importe Pago',
         tooltip: 'Importe Pago',
         dataIndex: 'IMPORTEPAGO',
         sortable: true,
         hideable: false,
         width: 120,
         align: 'center',
         renderer: function(value,metadata,registro){return value==''?NE.util.colorCampoEdit(value,metadata,registro):value;}
      }]
   });
   
   /* Obj elementos Forma 1 */
   var formaEncabezado = function(){
      var ObjFormaEncabezado = ({
         xtype: 'panel',
         frame: true,
         items:[{
            xtype:               'fieldset',
            title:               'Encabezado',
            columnWidth:         0.5,
            collapsible:         true,
            items:[{
               xtype:            'compositefield',
               combineErrors:    false,
               items: [{
                  xtype:         'textfield',
                  fieldLabel:    'Direcci�n estatal',
                  style:         'background: none; border: 1px solid #c4c4c4',
                  name:          'sEstado_E',
                  id:            'id_sEstado_E',
                  readOnly:      true,
                  width:         175
               },{
                  xtype:         'displayfield',
                  value:         'Fecha de dep�sito',
                  style:         'margin: 0 0 0 70px',
                  width:         200
               },{
                  xtype:         'datefield',
                  name:          'sFechaDeposito_E',
                  id:            'id_sFechaDeposito_E',
                  vtype:         'rangofecha',
                  width:            135
               }]
            },{
               xtype:            'compositefield',
               combineErrors:    false,
               items:[{
                  xtype:            'combo',
                  name:             'sClaveMoneda_E',
                  id:               'id_sClaveMoneda_E',
                  hiddenName:       'sClaveMoneda_E',
                  fieldLabel:       'Moneda',
                  mode:             'local',
                  displayField:     'descripcion',
                  valueField:       'clave',
                  emptyText:        '- Seleccione -',
                  triggerAction:    'all',
                  typeAhead:        true,
                  minChars:         1,
                  allowBlank:       false,
                  store:            new Ext.data.JsonStore({
                     id:      'catalogoComboMoneda',
                     root:    'registros',
                     fields:  ['clave','descripcion'],
                     url:     '13forma10ext.data.jsp',
                     baseParams: {
                        informacion:   'catalogoMoneda'
                     },
                     totalProperty:    'total',
                     autoLoad:         true
                  }),
                  width :           175
               },{
                  xtype:            'displayfield',
                  value:            'Importe de dep�sito',
                  style:            'margin: 0 0 0 70px',
                  width:            200
               },{
                  xtype:            'textfield',
                  allowBlank:       false,
                  name:             'sImporteDeposito_E',
                  id:               'id_sImporteDeposito_E',
                  enableKeyEvents  :  true
               }]
            },{
               xtype:            'compositefield',
               combineErrors:    false,
               items: [{
                  xtype:            'combo',
                  name:             'sBancoServicio_E',
                  id:               'id_sBancoServicio_E',
                  hiddenName:       'sBancoServicio_E',
                  fieldLabel:       'Banco de Servicio',
                  mode:             'local',
                  displayField:     'descripcion',
                  valueField:       'clave',
                  emptyText:        '- Seleccione -',
                  forceSelection:   true,
                  triggerAction:    'all',
                  typeAhead:        true,
                  minChars:         1,
                  allowBlank:       false,
                  store:            new Ext.data.JsonStore({
                     id:      'catalogoBancoServicio',
                     root:    'registros',
                     fields:  ['clave','descripcion'],
                     url:     '13forma10ext.data.jsp',
                     baseParams: {
                        informacion:   'catalogoBancoServicio'
                     },
                     totalProperty:    'total',
                     autoLoad:         true
                  }),
                  width :           175
               },{
                  xtype:            'displayfield',
                  value:            'Referencia Banco',
                  style:            'margin: 0 0 0 70px',
                  width:            200
               },{
                  xtype:            'textfield',
                  allowBlank:       false,
                  name:             'sRefBanco_E',
                  id:               'id_sRefBanco_E'
               }]
            },{
               xtype:            'compositefield',
               combineErrors:    false,
               items:            [{
                  xtype:            'textfield',
                  id:               'id_sFechaProbPago_E',
                  name:             'sFechaProbPago_E',
                  fieldLabel:       'Fecha probable de pago',
                  style:            'background: none; border: 1px solid #c4c4c4',
                  readOnly:         true,
                  width:            175
               },{
                  xtype:            'displayfield',
                  value:            'Referencia Intermediario',
                  style:            'margin: 0 0 0 70px',
                  width:            200
               },{
                  xtype:            'textfield',
                  id:               'id_sRefInter_E',
                  name:             'sRefInter_E',
                  style:            'background: none; border: 1px solid #c4c4c4',
                  readOnly:         true,
                  width:            133
               }]
            }]
         },{
            xtype:	'hidden',
            id:		'id_sNombreArc',
            name:	   'sNombreArc'
         },{
            xtype:	'hidden',
            id:		'id_sClaveIF_E',
            name:	   'sClaveIF_E'
         },{
            xtype:	'hidden',
            id:		'id_sClaveEstado_E',
            name:	   'sClaveEstado_E'
         },{
            xtype:	'hidden',
            id:		'id_sEstado_E',
            name:	   'sEstado_E'
         },{
            xtype:	'hidden',
            id:		'id_sMoneda_E',
            name:	   'sMoneda_E'
         },{
            xtype:	'hidden',
            id:		'id_sImporteDeposito_E',
            name:	   'sImporteDeposito_E'
         },{
            xtype:	'hidden',
            id:		'id_sFechaProbPago_E',
            name:	   'sFechaProbPago_E'
         },{
            xtype:	'hidden',
            id:		'id_sFechaVencimiento_E',
            name:	   'sFechaVencimiento_E'
         },{
            xtype:	'hidden',
            id:		'id_sRefInter_E',
            name:	   'sRefInter_E'
         },{
            xtype:	'hidden',
            id:		'id_sClaveRefInter_E',
            name:	   'sClaveRefInter_E'
         },{
            xtype:	'hidden',
            id:		'id_sNombreBanco_E',
            name:	   'sNombreBanco_E'
         }]
      });
      return ObjFormaEncabezado;
   }
   
   var comboConceptoPago = creaCmbConceptoPago();
   function creaCmbConceptoPago(){
      return new Ext.form.ComboBox({
         name:          'cmbComboOrigenPago',
         hiddenName:    'cmbComboOrigenPago',
         fieldLabel:    'Concepto Pago',
         triggerAction: 'all',
         mode:          'local',
         displayField:  'descripcion',
         valueField:    'clave',
         typeAhead:     true,
         editable:      false,
         forceSelection:true,
         store:         new Ext.data.SimpleStore({
            fields : ['clave', 'descripcion'],
            data:    [],
            autoLoad:true
         }),
         valueField: 'clave',
         displayField: 'descripcion'
      });
   }
   
   
   
   var panelEncabezado = new Ext.FormPanel({
      id:'panelEncabezado',
      frame:   false,   
      title:   ' ',
      width:   800,
      style:   'margin: 0 auto',
      labelWidth:150,      
      hidden:  'true',
      monitorValid:true,
      items:[],
      buttons: [
         {text: 'Enviar', formBind:true, iconCls: 'icoContinuar', handler: fnEnviaFormaEncabezado},
         {text: 'Cancelar', iconCls: 'cancelar', handler:fnCancelarGral}
      ]
   });
   
   
   var elementosForma = new Ext.FormPanel({
      id:      'forma',
      hidden:  'true',
      width:   400,
      title:   'Captura Individual de Pagos',
      frame:   false,
      style:   'margin: 0 auto; padding: 15px',
      bodyStyle: 'padding: 15px',
      labelWidth:50,
		labelAlign: 'right'
   });
   
   var pnl = new Ext.Container({
		id:      'contenedorPrincipal',
		applyTo: 'areaContenido',
		style:   'margin:0 auto;',
		width:   949,
		items: [
			NE.util.getEspaciador(10)
		]
	});
      
   CARTERA.captura.ObjGeneral = new ObjGeneral();
   CARTERA.captura.ObjGeneral.init();
   CARTERA.captura.ObjGeneral.controller();
   
});