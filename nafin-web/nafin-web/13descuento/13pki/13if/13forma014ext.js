Ext.onReady(function() {
	var tasas =  [];
	var valorTasa =  [];
		
	
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var mensajeAutentificacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutentificacion',							
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		layoutConfig: {
			columns: 1
		},
		items: [					
			{
				xtype: 'displayfield',
				id:	'autentificac',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}		
			},
			{ 	xtype: 'displayfield', 	hidden: true, id: 'acuse', 	value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'fechaCarga', 	value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, id: 'horaCarga', 	value: '' },	
			{ 	xtype: 'displayfield', 	hidden: true, 	id: 'recibo', value: '' }
		]
	});
	
	var consultaAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma014ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarAcuse'
		},
		hidden: true,
		fields: [	
		
			{name: 'EPO_RELACIONADA'},	
			{name: 'REFERENCIA'},
			{name: 'NOMBRE_PYME'},			
			{name: 'TIPO_TASA'},	
			{name: 'PLAZO'},				
			{name: 'VALOR'},				
			{name: 'VALOR_TASA'}
				
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {				
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);						
											
					}
				}
			}			
	});
	
	
	var gridAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridAcuse',				
		store: consultaAcuseData,	
		style: 'margin:0 auto;',
		title:'Tasas Preferenciales/Negociadas para Mandato',
		hidden: true,
		clicksToEdit: 1,
		columns: [
			{							
				header : 'EPO que opera Factoraje con Mandato',
				tooltip: 'EPO que opera Factoraje con Mandato',
				dataIndex : 'EPO_RELACIONADA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Referencia',
				tooltip: 'REFERENCIA',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo de Tasa',
				tooltip: 'TIPO_TASA',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Plazo',
				tooltip: 'PLAZO',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Valor',
				tooltip: 'Valor',
				dataIndex : 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{							
				header : 'Valor Tasa',
				tooltip: 'Valor Tasa',
				dataIndex : 'VALOR_TASA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'	
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {				
					
						var acuse = Ext.getCmp("acuse").getValue();
						var fechaCarga = Ext.getCmp("fechaCarga").getValue();
						var horaCarga = Ext.getCmp("horaCarga").getValue();
						var recibo = Ext.getCmp("recibo").getValue();
						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13forma014ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoPDF',
								acuse2:acuse,
								fechaCarga:fechaCarga,
								horaCarga:horaCarga,
								recibo:recibo	
							}),																
							callback: procesarGenerarPDF
						});				
					}
				},				
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					tooltip:	'Bajar Archivo',
					id: 'btnBajarPDF',
					hidden: true
				},	
				'-',
				{
					xtype: 'button',
					text: 'Generar CSV',
					tooltip:	'Generar CSV',
					id: 'btnGenerarCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
					
						var acuse = Ext.getCmp("acuse").getValue();
						var fechaCarga = Ext.getCmp("fechaCarga").getValue();
						var horaCarga = Ext.getCmp("horaCarga").getValue();
						var recibo = Ext.getCmp("recibo").getValue();
						
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13forma014ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoCSV',
								acuse2:acuse,
								fechaCarga:fechaCarga,
								horaCarga:horaCarga,
								recibo:recibo	
							}),																
							callback: procesarGenerarCSV
						});					
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					tooltip:	'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Salir',
					tooltip:	'Salir',
					iconCls: 'icoLimpiar',
					id: 'btnSalir',
					handler: function(boton, evento) {
						window.location = '13forma014ext.jsp';		
					}
				}
			]
		}
	});
	
	
		//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		style: 'margin:0 auto;',
		height: 150,
		frame: true
	});
	
	
		// respuesta de Guardar de  tasas Prenegociables /Negociables 
	function procesarSuccessFailureGuardar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			fp.hide();
			gridConsulta.hide();
			
			var mensajeAutentificacion = Ext.getCmp('mensajeAutentificacion');		
			
			Ext.getCmp("acuse").setValue(jsonData.acuse2);
			Ext.getCmp("fechaCarga").setValue(jsonData.fecha);
			Ext.getCmp("horaCarga").setValue(jsonData.hora);
			Ext.getCmp("recibo").setValue(jsonData.recibo);
			
			if(jsonData.acuse =='N'){			
				Ext.getCmp("autentificac").setValue(jsonData.mensajeAutentificacion);
				mensajeAutentificacion.show();
			}
			
			if(jsonData.acuse !='N'){
				var acuseCifras = [
					['N�mero de Acuse', jsonData.acuse],
					['Fecha de Autorizaci�n ', jsonData.fecha],
					['Hora de Autorizaci�n', jsonData.hora],
					['Usuario de Autorizaci�n ', jsonData.usuario]
				];
			
				storeCifrasData.loadData(acuseCifras);	
				gridCifrasControl.show();
				gridCifrasControl.setTitle('Recibo N�mero    '+jsonData.recibo);	
				
				
				consultaAcuseData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarAcuse',
					//	tipoTasa:tipoTasa,
						acuse2:jsonData.acuse2,
						fechaCarga:jsonData.fecha,
						horaCarga:jsonData.hora				
					})
				});
				
				gridAcuse.show();				
					
			}				
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var fnProcesarGuardarCallback = function(vpkcs7, vtextoFirmar, vtipoTasa, vtasas, vvalorTasa){
		if (Ext.isEmpty(vpkcs7)) {
			return;	//Error en la firma. Termina...
		}	
		
		Ext.Ajax.request({
			url : '13forma014ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'GuardarTasas',
				tipoTasa: vtipoTasa,
				tasas: vtasas, 
				valorTasa: vvalorTasa,
				textoFirmar: vtextoFirmar,
				pkcs7: vpkcs7
			}),				
			callback: procesarSuccessFailureGuardar
		});
	}
	
	var procesarGuardar= function() 	{
	
		var tipoTasa = Ext.getCmp("tipo_tasa1").getValue();
		var textoFirmar;
		var totalSe =0;
		if(tipoTasa =='P'){  textoFirmar='EPO Relacionada|Referencia|Tipo Tasa|Valor|Rel. Mat|Puntos|Valor Tasa \n'; 	}
		if(tipoTasa =='N'){ 	textoFirmar='EPO Relacionada|Referencia|Tipo Tasa|Valor|Fecha|Valor Tasa \n'; }		
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		tasas =  [];
		valorTasa =  [];
		store.each(function(record) {	
			if(tipoTasa =='P'){
				textoFirmar += record.data['EPO_RELACIONADA'] +"|"+ record.data['REFERENCIA'] +"|"+ record.data['TIPO_TASA'] +"|"+ 
						record.data['VALOR'] +"|"+ record.data['REL_MAT'] +"|"+ record.data['PUNTOS'] +"|"+record.data['VALOR_TASA']+"\n";
			}		
			
			if(tipoTasa =='N'){
				textoFirmar += record.data['EPO_RELACIONADA'] +"|"+ record.data['REFERENCIA'] +"|"+ record.data['TIPO_TASA'] +"|"+ 
						record.data['VALOR'] +"|"+ record.data['FECHA'] +"|"+record.data['VALOR_TASA'] +"\n";
			}	
			
			if(record.data['VALOR_TASA']!=''){
				totalSe++
				tasas.push(record.data['TASA_SELEC']);
				valorTasa.push(record.data['VALOR_TASA']);
			}
		});
		
		if(totalSe ==0) {
			Ext.MessageBox.alert('Mensaje','Debes de especificar el valor de todas las tasas negociadas');
			return false;	
		}	
		
		
		NE.util.obtenerPKCS7(fnProcesarGuardarCallback, textoFirmar, tipoTasa, tasas, valorTasa);
		
		
						
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
							
			//edito el titulo de la columna
			var el = gridConsulta.getGridEl();	
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
					
			var btnGuardar = Ext.getCmp('btnGuardar');	
			var btnCancelar = Ext.getCmp('btnCancelar');	
				

			if(jsonData.tipoTasa=='P'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('REL_MAT'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PUNTOS'), false);
			}
			if(jsonData.tipoTasa=='N'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('REL_MAT'), true);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA'), false);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PUNTOS'), true);
			}					
					
			if(store.getTotalCount() > 0) {	
				el.unmask();	
				btnGuardar.enable();
				btnCancelar.enable();				
			} else {		
				
				btnGuardar.disable();
				btnCancelar.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma014ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [	
			{name: 'IC_EPO'},
			{name: 'EPO_RELACIONADA'},	
			{name: 'REFERENCIA'},	
			{name: 'TIPO_TASA'},	
			{name: 'VALOR'},				
			{name: 'REL_MAT'},	
			{name: 'PUNTOS'},	
			{name: 'FECHA'},	
			{name: 'VALOR_TASA'},
			{name: 'TIPO'},
			{name: 'TASA_SELEC'}		
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});

	var validaTasaN =  function( opts, success, response, registro) { 
		var valor = parseFloat(registro.data['VALOR']);  
		var valor_tasa = parseFloat(registro.data['VALOR_TASA']); 
	
		if(valor_tasa <0  ||  valor_tasa ==0 ){
			Ext.MessageBox.alert('Mensaje','La tasa debe ser mayor a 0');
			registro.data['VALOR_TASA']= '';
			return false;
		}
		
		if(valor_tasa >  valor){
			Ext.MessageBox.alert('Mensaje','La tasa no debe ser mayor al Valor de la Tasa Actual');
			registro.data['VALOR_TASA']= '';
			registro.commit();	
			return false;
		}

	}
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Tasas Preferenciales/Negociadas para Mandato',
		hidden: true,
		clicksToEdit: 1,
		columns: [
			{							
				header : 'EPO que opera Factoraje con Mandato',
				tooltip: 'EPO que opera Factoraje con Mandato',
				dataIndex : 'EPO_RELACIONADA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Referencia',
				tooltip: 'REFERENCIA',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo de Tasa',
				tooltip: 'TIPO_TASA',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Valor',
				tooltip: 'Valor',
				dataIndex : 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{							
				header : 'Rel. Mat',
				tooltip: 'Rel. Mat',
				dataIndex : 'REL_MAT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Puntos',
				tooltip: 'Puntos',
				dataIndex : 'PUNTOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			
			{							
				header : 'Fecha',
				tooltip: 'Fecha',
				dataIndex : 'FECHA',
				sortable: true,
				hidden: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			
			{							
				header : 'Valor Tasa',
				tooltip: 'Valor Tasa',
				dataIndex : 'VALOR_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				editor: {
					xtype : 'numberfield',
					maxValue : 999999999999.99,
					allowBlank: true
				}, 
				renderer:function(value,metadata,registro){
					if( registro.data['TIPO'] =='N'){				
					 return  NE.util.colorCampoEdit(value,metadata,registro);
					}else if( registro.data['TIPO'] =='P'){	
						return value;
					}
				}	
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false,
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
							
				if(campo == 'VALOR_TASA'){				
					if( record.data['TIPO'] =='N'){
						return true; // es editable 					
					}else if( record.data['TIPO'] =='P'){
						return false; //  no es editable 					
					}
				}	
								
			},
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
			
				if(campo=='VALOR_TASA') { 	 	
					validaTasaN('', '', '', record); 			
				}
			}
		},		
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Guardar',					
					tooltip:	'Guardar',
					iconCls: 'icoAceptar',
					id: 'btnGuardar',
					handler: procesarGuardar
				},			
				'-',
				{
					xtype: 'button',
					text: 'Cancelar',
					tooltip:	'Cancelar',
					iconCls: 'icoLimpiar',
					id: 'btnCancelar',
					handler: function(boton, evento) {
						window.location = '13forma014ext.jsp';		
					}
				}
			]
		}
	});
		
		
		
		
// ********************** datos para la  Forma 	******************************
		
	var procesarCatalogoMonedaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_moneda1 = Ext.getCmp('ic_moneda1');
			if(ic_moneda1.getValue()==''){
				ic_moneda1.setValue(records[0].data['clave']);
			}
		}
	}		
	
	var procesarCatalogoTipoTasaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var tipo_tasa1 = Ext.getCmp('tipo_tasa1');
			if(tipo_tasa1.getValue()==''){
				tipo_tasa1.setValue(records[0].data['clave']);
			}
		}
	}
	
		
	
	var catalogoTipoTasa = new Ext.data.JsonStore({
		id: 'catalogoTipoTasa',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma014ext.data.jsp',
		baseParams: {
			informacion: 'catalogoTipoTasa'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoTipoTasaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
		
	var storeCatalogoMandante = new Ext.data.JsonStore({
		id: 'storeCatalogoMandante',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma014ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMandante'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma014ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPlazo = new Ext.data.JsonStore({
		id: 'catalogoPlazo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma014ext.data.jsp',
		baseParams: {
			informacion: 'catalogoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma014ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	
	var elementosForma = [	
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de Tasa',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'tipo_tasa',
					id: 'tipo_tasa1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccione  Tipo de Tasa...',			
					valueField: 'clave',
					hiddenName : 'tipo_tasa',
					fieldLabel: 'Tipo de Tasa',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoTipoTasa,
					listeners: {
						select: {
							fn: function(combo) {
							var lstPlazo2 = Ext.getCmp('lstPlazo2');
							var txtPuntos2 = Ext.getCmp('txtPuntos2');					
								if(combo.getValue() =='P'){   		txtPuntos2.show(); 	 lstPlazo2.hide();	}
								if(combo.getValue() =='N'){   		lstPlazo2.show(); 	 txtPuntos2.hide(); 	}						
							}
						}
					}
				}
			]
		 },			 
		 {
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione  EPO...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',
			fieldLabel: 'Epo que opera Factoraje con Mandato',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			listeners: {
				select: {
					fn: function(combo) {
						var cmbMandante = Ext.getCmp('ic_mandante1');
						cmbMandante.setValue('');
						cmbMandante.store.load({
							params: {
								ic_epo:combo.getValue()
							}
						});
					}
				}
			}
		},	
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar Moneda...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',
					fieldLabel: 'Moneda',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoMoneda
				}
			]
		},
		{
			xtype: 'combo',
			name: 'ic_mandante',
			id: 'ic_mandante1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione Mandante...',			
			valueField: 'clave',
			hiddenName : 'ic_mandante',
			fieldLabel: 'Mandante',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,			
			store: storeCatalogoMandante					
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Puntos',
			id: 'txtPuntos2',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'numberfield',
					name: 'txtPuntos',
					id: 'txtPuntos1',
					fieldLabel: 'Puntos',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo',
			combineErrors: false,
			msgTarget: 'side',
			id: 'lstPlazo2',
			hidden: true,
			width: 500,
			items: [	
			{
				xtype: 'combo',
				name: 'lstPlazo',
				id: 'lstPlazo1',
				mode: 'local',				
				displayField: 'descripcion',
				emptyText: 'Seleccione  Plazo...',			
				valueField: 'clave',
				hiddenName : 'lstPlazo',
				fieldLabel: 'Plazo',			
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				allowBlank: true,
				msgTarget: 'side',	
				store: catalogoPlazo,
				tpl : NE.util.templateMensajeCargaCombo					
			}	
			]
		}		
	];
	

	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: ' Captura Tasas Preferenciales/Negociadas para Mandato',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200, 
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
			text: 'Procesar',
			id: 'btnProcesar',
			iconCls: 'icoAceptar',
			formBind: true,		
			handler: function(boton, evento) {
				
				var ic_epo = Ext.getCmp("ic_epo1");
				if (Ext.isEmpty(ic_epo.getValue()) ){
					ic_epo.markInvalid('Debe Seleccionar  la EPO.');
					return;
				}
				
				var ic_mandante = Ext.getCmp("ic_mandante1");
				if (Ext.isEmpty(ic_mandante.getValue()) ){
					ic_mandante.markInvalid('Debe Seleccionar el Mandante.');
					return;
				}
				var tipo_tasa = Ext.getCmp('tipo_tasa1');
				if(tipo_tasa.getValue() =='P') {
					var txtPuntos = Ext.getCmp("txtPuntos1");
					if (Ext.isEmpty(txtPuntos.getValue()) ){
						txtPuntos.markInvalid('Debe de indicar los Puntos ');
						return;
					}
				}
				
				if(tipo_tasa.getValue() =='N') {
					var lstPlazo = Ext.getCmp("lstPlazo1");
					if (Ext.isEmpty(lstPlazo.getValue()) ){
						lstPlazo.markInvalid('Debe Seleccionar un plazo ');
						return;
					}
				}
				
								
				fp.el.mask('Enviando...', 'x-mask-loading');			
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar'									
					})
				});					
			}
		}	
		]
	});
	 
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),							
			fp,			
			mensajeAutentificacion,
			NE.util.getEspaciador(20),
			gridCifrasControl,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridAcuse,	
			NE.util.getEspaciador(20)			
		]
	});

	catalogoMoneda.load();
	catalogoEPO.load();
	catalogoPlazo.load();
	catalogoTipoTasa.load();
	
//-------------------------------- ----------------- -----------------------------------
	
});