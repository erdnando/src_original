<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	com.netro.mandatodoc.*,	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String tipoIF = (String)session.getAttribute("strTipoBanco");
/*
if("B".equals(tipoIF))
	response.sendRedirect("13forma10fr.jsp");
*/
String texto = "";
String sAccion				= (request.getParameter("sAccion")==null)?"":request.getParameter("sAccion");
String sContador			= (request.getParameter("sContador")==null)?"":request.getParameter("sContador");
String informacion      = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion        = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String estado           = (request.getParameter("estado")!=null)?request.getParameter("estado"):""; 
String conceptoPagoIni	= (request.getParameter("conceptoPagoIni")==null)?"V":request.getParameter("conceptoPagoIni");

   /*
if(!records.equals("")){
   JSONObject jsonRequest = JSONObject.fromObject(records);
   String prestamo = jsonRequest.getString("PRESTAMO");
   
   System.err.println("\n\n\n\nREGISTROS: " + prestamo + "\n\n\n");
}
*/

/////////////////////////DETALLE
String sSubaplicacion_D[]	= request.getParameterValues("sSubaplicacion_D");
String sPrestamo_D[]		= request.getParameterValues("sPrestamo_D");
String sClaveSirac_D[]		= request.getParameterValues("sClaveSirac_D");
String sCapital_D[]			= request.getParameterValues("sCapital_D");
String sInteres_D[]			= request.getParameterValues("sInteres_D");
String sMoratorios_D[]		= request.getParameterValues("sMoratorios_D");
String sSubsidio_D[]		= request.getParameterValues("sSubsidio_D");
String sComision_D[]		= request.getParameterValues("sComision_D");
String sIva_D[]				= request.getParameterValues("sIva_D");
String sImporte_D[]			= request.getParameterValues("sImporte_D");
String Concepto_D[]			= request.getParameterValues("Concepto_D");
String sOrigen_D[]			= request.getParameterValues("sOrigen_D");
String hidChecked[]			= request.getParameterValues("hidChecked");

/////////////////////////ENCABEZADO
String sClaveIF_E          = (request.getParameter("sClaveIF_E")==null)?"":request.getParameter("sClaveIF_E");
String sClaveMoneda_E      = (request.getParameter("sClaveMoneda_E")==null)?"":request.getParameter("sClaveMoneda_E");
String sMoneda_E           = (request.getParameter("sMoneda_E")==null)?"":request.getParameter("sMoneda_E");
String sFechaVencimiento_E = (request.getParameter("sFechaVencimiento_E")==null)?"":request.getParameter("sFechaVencimiento_E");
String sFechaProbPago_E    = (request.getParameter("sFechaProbPago_E")==null)?"":request.getParameter("sFechaProbPago_E");
String sFechaDeposito_E    = (request.getParameter("sFechaDeposito_E")==null)?"":request.getParameter("sFechaDeposito_E");
String sClaveEstado_E      = (request.getParameter("sClaveEstado_E")==null)?"":request.getParameter("sClaveEstado_E");
String sEstado_E           = (request.getParameter("sEstado_E")==null)?"":request.getParameter("sEstado_E");
String sRefInter_E         = (request.getParameter("sRefInter_E")==null)?"":request.getParameter("sRefInter_E");
String sClaveRefInter_E    = (request.getParameter("sClaveRefInter_E")==null)?"":request.getParameter("sClaveRefInter_E");
String sImporteDeposito_E  = (request.getParameter("sImporteDeposito_E")==null)?"":request.getParameter("sImporteDeposito_E");
String sBancoServicio_E    = (request.getParameter("sBancoServicio_E")==null)?"":request.getParameter("sBancoServicio_E");
String sRefBanco_E         = (request.getParameter("sRefBanco_E")==null)?"":request.getParameter("sRefBanco_E");
String sNombreBanco_E      = (request.getParameter("sNombreBanco_E")==null)?"":request.getParameter("sNombreBanco_E");
String sTotalImporte_E      = (request.getParameter("sTotalImporte_E")==null)?"":request.getParameter("sTotalImporte_E");

String sOpcion      = (request.getParameter("sOpcion")==null)?"":request.getParameter("sOpcion");
String sNombreArc      = (request.getParameter("sNombreArc")==null)?"":request.getParameter("sNombreArc");

String   respuesta         = "";
String	sClaveIF			   = "";
String	sClaveMoneda		= "";
String	sMoneda				= "";
String	sFechaVencimiento	= "";
String	sFechaProbPago		= "";
String	sClaveEstado		= "";
String	sEstado				= "";
String	sRefInter			= "";
String	sClaveRefInter		= "";
String	sFechaDeposito		= "";
String	dTotalPagar 		= "";
int 	registros			= 0;
PreparedStatement 	ps		= null;
ResultSet			rs 		= null;
String cs_tipo="";
String guia="";

double dCapital=0, dInteres=0, dMoratorios=0, dSubsidio=0, dComision=0, dIva=0, dImporte=0; 

session.removeAttribute("f");
ArrayList al = (ArrayList)session.getAttribute("f");

ArrayList alRegistro = new ArrayList();
if(al==null)
	al = new ArrayList();
if(sSubaplicacion_D!=null){
	for(int i=0;i<sSubaplicacion_D.length;i++){
		if(i==sSubaplicacion_D.length-1&&!"A".equals(sAccion))
			break;
		alRegistro = new ArrayList();
		alRegistro.add(sSubaplicacion_D[i]);
		alRegistro.add(sPrestamo_D[i]);
		alRegistro.add(sClaveSirac_D[i]);
		alRegistro.add(sCapital_D[i]);
		alRegistro.add(sInteres_D[i]);
		alRegistro.add(sMoratorios_D[i]);
		alRegistro.add(sSubsidio_D[i]);
		alRegistro.add(sComision_D[i]);
		alRegistro.add(sIva_D[i]);
		alRegistro.add(sImporte_D[i]);
		alRegistro.add(Concepto_D[i]);
		alRegistro.add(sOrigen_D[i]);
		alRegistro.add(hidChecked[i]);
		al.add(alRegistro);
	}
}
session.setAttribute("f",al);

AccesoDB con = new AccesoDB();

if(   informacion.equals("catalogoMoneda")  ){
   CatalogoMoneda cat = new CatalogoMoneda();
   cat.setClave("ic_moneda");
   cat.setDescripcion("cd_nombre");
   cat.setOrden("2");
   respuesta = cat.getJSONElementos();
}

else if(  informacion.equals("catalogoBancoServicio")  ){
   CatalogoFinanciera cat = new CatalogoFinanciera();
   cat.setClave("ic_financiera");
   cat.setDescripcion("cd_nombre");
   cat.setTipo("1");
   respuesta = cat.getJSONElementos();
}

else if  (  informacion.equals("GridEncabezado")  ){      
      HashMap datos = new HashMap();
      List reg = new ArrayList();
      
      datos.put("DIR_ESTATAL",sEstado_E);
      datos.put("MONEDA",sClaveMoneda_E);
      datos.put("BANCO_SERV",sBancoServicio_E);
      datos.put("FECHA_VENC",sFechaVencimiento_E);
      datos.put("FECHA_PROB_PAGO",sFechaProbPago_E);
      datos.put("FECHA_DEPOSITO",sFechaDeposito_E);
      datos.put("IMPORTE_DEPOSITO","$ " + Comunes.formatoDecimal(sImporteDeposito_E,2));
      reg.add(datos);
      
      JSONObject jsonObj = new JSONObject();
      jsonObj.put("success",new Boolean(true));
      jsonObj.put("registros", reg);
      respuesta = jsonObj.toString();
}

else if  (  informacion.equals("GridEncabezadoDetalles")  ){
   /***** 13forma10b.jsp *****/
   //AccesoDB con = new AccesoDB();
   try	{
      con.conexionDB();
      PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
   
   //Parámetros provenienen de la página anterior y de la actual
   String hidOperacion = (request.getParameter("hidOperacion")==null)?"":request.getParameter("hidOperacion");
   //out.println("hidOperacion:"+hidOperacion+"<br>");
   
   /*ArrayList*/ alRegistro = new ArrayList();
      for(int i=0;i<sSubaplicacion_D.length-1;i++){
         if("S".equals(hidChecked[i])){
            alRegistro = new ArrayList();
            alRegistro.add(sSubaplicacion_D[i]);
            alRegistro.add(sPrestamo_D[i]);
            alRegistro.add(sClaveSirac_D[i]);
            alRegistro.add(sCapital_D[i]);
            alRegistro.add(sInteres_D[i]);
            alRegistro.add(sMoratorios_D[i]);
            alRegistro.add(sSubsidio_D[i]);
            alRegistro.add(sComision_D[i]);
            alRegistro.add(sIva_D[i]);
            alRegistro.add(sImporte_D[i]);
            alRegistro.add(Concepto_D[i]);
            alRegistro.add(sOrigen_D[i]);
            al.add(alRegistro);
         }
      }
      session.setAttribute("f",al);
      
      String sImportePesos="";
      String sImporteDolares="";
      if(sClaveMoneda_E.equals("1")){
         sImportePesos   = sImporteDeposito_E;
         sImporteDolares = "";
      }else{
         sImportePesos   = "";
         sImporteDolares = sImporteDeposito_E;
      }
      EncabezadoPagoIFNB encabezado = new EncabezadoPagoIFNB();
      encabezado.setClaveIF(iNoCliente);
      encabezado.setClaveDirEstatal(sClaveEstado_E);
      encabezado.setClaveMoneda(sClaveMoneda_E);
      encabezado.setFechaVencimiento(sFechaVencimiento_E);
      encabezado.setFechaProbablePago(sFechaProbPago_E);
      encabezado.setFechaDeposito(sFechaDeposito_E);
      encabezado.setImporteDeposito(sImporteDeposito_E);
      encabezado.setBancoServicio(sBancoServicio_E);
      encabezado.setReferenciaInter(sClaveRefInter_E);
      encabezado.setReferenciaBanco(sRefBanco_E);
      encabezado.setNombreArchivo(sNombreArc);
      
      HashMap datos = new HashMap();
      List reg = new ArrayList();
      
      datos.put("DIR_ESTATAL",sEstado_E);
      datos.put("MONEDA",sClaveMoneda_E);
      datos.put("BANCO_SERV",sBancoServicio_E);
      datos.put("FECHA_VENC",sFechaVencimiento_E);
      datos.put("FECHA_PROB_PAGO",sFechaProbPago_E);
      datos.put("FECHA_DEPOSITO",sFechaDeposito_E);
      datos.put("IMPORTE_DEPOSITO","$ " + Comunes.formatoDecimal(sImporteDeposito_E,2));
      reg.add(datos);
      
      JSONObject jsonObj = new JSONObject();
      jsonObj.put("success",new Boolean(true));
      jsonObj.put("registros", reg);
      respuesta = jsonObj.toString();
      
   } catch(Exception e){
   
   }
}

//------------------------------------------------------------------------------
//                   CAPTURA INDIVIDUAL

else if( informacion.equals("storeCapturaIndividual_1") ){
   String sCondicion="";
	String sOrden="";
	String sFechaDel="";
	String sFechaAl="";
	List  	lFilas		= new ArrayList();
	Map		mColumnas	= null;
   
   try{
      con.conexionDB();      
      PagosIFNB beanPagos = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
   
      //cs_tipo=beanPagos.getTipoIF(iNoCliente);
      sOrden=" V.com_fechaprobablepago ";

      String query = " SELECT "   +
		               "      /*+"	+
                       "      use_nl(V)"	+
                       "      index(V CP_COM_VENCIMIENTO_PK)"	+
                       "      */"	+
	    	           "       V.ic_if CLAVE_IF"   +
					   "       ,V.ic_moneda CLAVE_MONEDA"	+
      				   "       ,M.cd_nombre MONEDA"		+
					   "       ,TO_CHAR(V.df_periodofin,'DD/MM/YYYY') FECHA_VENCIMIENTO"   +
					   "       ,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY') FECHA_PROB_PAGO"   +
	                   "       ,F.ic_estado CLAVE_ESTADO"	+
	                   "       ,E.cd_nombre NOMBRE_ESTADO"		+
	                   "       ,RI.cg_descripcion REF_INTER"	+
	                   "       ,RI.ic_referencia_inter CLAVE_REF_INTER"	+
					   " 	   ,V.df_periodofin"   +
					   " 	   ,V.com_fechaprobablepago"   +
	  				   "	   ,SUM(V.fg_totalvencimiento) IMPORTE_DEPOSITO"	+
					   "	   ,TO_CHAR(SYSDATE,'dd/mm/yyyy') as FECHA_DEPOSITO"+
					   "	   ,count(1) as numreg"+
					   " FROM"   +
					   "       com_vencimiento V"   +
					   "       ,comcat_moneda M"   +
				 	   "       ,comcat_if I"	+
                	   "       ,comcat_financiera F"	+
	                   "       ,comcat_estado E"	+
					   "       ,comcat_Referencia_inter RI"	+
					   " WHERE"   +
					   "       V.ic_moneda=M.ic_moneda"		+
	  				   "       AND V.ic_if=I.ic_if"		+
               	       "       AND I.ic_financiera=F.ic_financiera"		+
	                   "       AND F.ic_estado=E.ic_estado"		+
	  				   "       AND V.ic_if=RI.ic_if"	+
					   "       AND V.ic_if = ?"   +
					   sCondicion+
					   " GROUP BY"   +
					   "       V.ic_if"   +
					   //"       ,V.ig_sucursal"   +
					   "       ,V.ic_moneda"	+
					   "       ,M.cd_nombre"   +
					   "       ,TO_CHAR(V.df_periodofin,'DD/MM/YYYY')"   +
					   "       ,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY')"	+
					   "       ,F.ic_estado"	+
					   "       ,E.cd_nombre"	+
	                   "       ,RI.cg_descripcion"		+
					   "       ,RI.ic_referencia_inter"	+
					   " 	   ,V.df_periodofin"   +
					   " 	   ,V.com_fechaprobablepago"   +
					   " ORDER BY"	+
					   sOrden;
		//out.println("query:"+query+"<br>");
		ps = con.queryPrecompilado(query);
		ps.setString(1, iNoCliente);
		/*ps.setString(2, sFechaDel);
		ps.setString(3, sFechaAl);*/
		rs = ps.executeQuery();
		double dImporteDepositoTotal=0;
		while (rs.next()) {
        	sClaveIF			= (rs.getString("CLAVE_IF") == null) ? "" : rs.getString("CLAVE_IF").trim();
        	sClaveMoneda		= (rs.getString("CLAVE_MONEDA") == null) ? "" : rs.getString("CLAVE_MONEDA").trim();
        	sMoneda				= (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA").trim();
        	sFechaVencimiento	= (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO").trim();
        	sFechaProbPago		= (rs.getString("FECHA_PROB_PAGO") == null) ? "" : rs.getString("FECHA_PROB_PAGO").trim();
        	sClaveEstado		= (rs.getString("CLAVE_ESTADO") == null) ? "" : rs.getString("CLAVE_ESTADO").trim();
        	sEstado				= (rs.getString("NOMBRE_ESTADO") == null) ? "" : rs.getString("NOMBRE_ESTADO").trim();
        	sRefInter			= (rs.getString("REF_INTER") == null) ? "" : rs.getString("REF_INTER").trim();
        	sClaveRefInter		= (rs.getString("CLAVE_REF_INTER") == null) ? "" : rs.getString("CLAVE_REF_INTER").trim();
			dTotalPagar			= ""+rs.getDouble("IMPORTE_DEPOSITO");
			sFechaDeposito		= (rs.getString("FECHA_DEPOSITO") == null) ? "" : rs.getString("FECHA_DEPOSITO").trim();
         
			mColumnas 			= new HashMap();
			mColumnas.put(    "CLAVE_IF",           sClaveIF            );
			mColumnas.put(    "CLAVE_MONEDA",       sClaveMoneda        );
			mColumnas.put(    "MONEDA",             sMoneda             );
			mColumnas.put(    "FECHA_VENCIMIENTO",  sFechaVencimiento   );
			mColumnas.put(    "FECHA_PROB_PAGO",    sFechaProbPago      );
			mColumnas.put(    "CLAVE_ESTADO",       sClaveEstado        );
			mColumnas.put(    "NOMBRE_ESTADO",      sEstado             );
			mColumnas.put(    "REF_INTER",          sRefInter           );
			mColumnas.put(    "CLAVE_REF_INTER",    sClaveRefInter      );
			mColumnas.put(    "IMPORTE_DEPOSITO",   dTotalPagar         );
			mColumnas.put(    "FECHA_DEPOSITO",     sFechaDeposito      );
			lFilas.add(mColumnas);
		}
    }catch(Exception e){ 
      e.printStackTrace();
      out.println(e); 
   }finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
   JSONObject jsonObj = new JSONObject();
   jsonObj.put(   "success",     new Boolean(true)                );
   jsonObj.put(   "registros",   lFilas                           );
   jsonObj.put(   "total",       Integer.toString(lFilas.size())  );
   respuesta = jsonObj.toString();
} else if( informacion.equals("storeCapturaIndividual_2") ){
   String sCondicion="";
	String sOrden="";
	String sFechaDel="";
	String sFechaAl="";
	List   lFilas		= new ArrayList();
	Map		mColumnas	= null;
   
   try{  
      con.conexionDB();      
   
      sOrden=" V.com_fechaprobablepago ";

	String query = " SELECT "   +
		               "      /*+"	+
                       "      use_nl(V)"	+
                       "      index(V CP_COM_VENCIMIENTO_PK)"	+
                       "      */"	+
	    	           "       V.ic_if CLAVE_IF"   +
					   "       ,V.ic_moneda CLAVE_MONEDA"	+
      				   "       ,M.cd_nombre MONEDA"		+
					   "       ,TO_CHAR(V.df_periodofin,'DD/MM/YYYY') FECHA_VENCIMIENTO"   +
					   "       ,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY') FECHA_PROB_PAGO"   +
	                   "       ,F.ic_estado CLAVE_ESTADO"	+
	                   "       ,E.cd_nombre NOMBRE_ESTADO"		+
	                   "       ,RI.cg_descripcion REF_INTER"	+
	                   "       ,RI.ic_referencia_inter CLAVE_REF_INTER"	+
					   " 	   ,V.df_periodofin"   +
					   " 	   ,V.com_fechaprobablepago"   +
	  				   "	   ,SUM(V.fg_totalvencimiento) IMPORTE_DEPOSITO"	+
					   "	   ,TO_CHAR(SYSDATE,'dd/mm/yyyy') as FECHA_DEPOSITO"+
					   "	   ,count(1) as numreg"+
					   " FROM"   +
					   "       com_vencimiento V"   +
					   "       ,comcat_moneda M"   +
				 	   "       ,comcat_if I"	+
                	   "       ,comcat_financiera F"	+
	                   "       ,comcat_estado E"	+
					   "       ,comcat_Referencia_inter RI"	+
					   " WHERE"   +
					   "       V.ic_moneda=M.ic_moneda"		+
	  				   "       AND V.ic_if=I.ic_if"		+
               	       "       AND I.ic_financiera=F.ic_financiera"		+
	                   "       AND F.ic_estado=E.ic_estado"		+
	  				   "       AND V.ic_if=RI.ic_if"	+
					   "       AND V.ic_if = ?"   +
					   sCondicion+
					   " GROUP BY"   +
					   "       V.ic_if"   +
					   //"       ,V.ig_sucursal"   +
					   "       ,V.ic_moneda"	+
					   "       ,M.cd_nombre"   +
					   "       ,TO_CHAR(V.df_periodofin,'DD/MM/YYYY')"   +
					   "       ,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY')"	+
					   "       ,F.ic_estado"	+
					   "       ,E.cd_nombre"	+
	                   "       ,RI.cg_descripcion"		+
					   "       ,RI.ic_referencia_inter"	+
					   " 	   ,V.df_periodofin"   +
					   " 	   ,V.com_fechaprobablepago"   +   
					   " ORDER BY"	+
					   sOrden;
		ps = con.queryPrecompilado(query);
      ps.setString(1, iNoCliente);
		rs = ps.executeQuery();
		double dImporteDepositoTotal=0;
		while (rs.next()) {
        	sClaveIF			= (rs.getString("CLAVE_IF") == null) ? "" : rs.getString("CLAVE_IF").trim();
        	sClaveMoneda		= (rs.getString("CLAVE_MONEDA") == null) ? "" : rs.getString("CLAVE_MONEDA").trim();
        	sMoneda				= (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA").trim();
        	sFechaVencimiento	= (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO").trim();
        	sFechaProbPago		= (rs.getString("FECHA_PROB_PAGO") == null) ? "" : rs.getString("FECHA_PROB_PAGO").trim();
        	sClaveEstado		= (rs.getString("CLAVE_ESTADO") == null) ? "" : rs.getString("CLAVE_ESTADO").trim();
        	sEstado				= (rs.getString("NOMBRE_ESTADO") == null) ? "" : rs.getString("NOMBRE_ESTADO").trim();
        	sRefInter			= (rs.getString("REF_INTER") == null) ? "" : rs.getString("REF_INTER").trim();
        	sClaveRefInter		= (rs.getString("CLAVE_REF_INTER") == null) ? "" : rs.getString("CLAVE_REF_INTER").trim();
			dTotalPagar			= ""+rs.getDouble("IMPORTE_DEPOSITO");
			sFechaDeposito		= (rs.getString("FECHA_DEPOSITO") == null) ? "" : rs.getString("FECHA_DEPOSITO").trim();
         
			mColumnas 			= new HashMap();
			mColumnas.put(    "CLAVE_IF",           sClaveIF            );
			mColumnas.put(    "CLAVE_MONEDA",       sClaveMoneda        );
			mColumnas.put(    "MONEDA",             sMoneda             );
			mColumnas.put(    "FECHA_VENCIMIENTO",  sFechaVencimiento   );
			mColumnas.put(    "FECHA_PROB_PAGO",    sFechaProbPago      );
			mColumnas.put(    "CLAVE_ESTADO",       sClaveEstado        );
			mColumnas.put(    "NOMBRE_ESTADO",      sEstado             );
			mColumnas.put(    "REF_INTER",          sRefInter           );
			mColumnas.put(    "CLAVE_REF_INTER",    sClaveRefInter      );
			mColumnas.put(    "IMPORTE_DEPOSITO",   dTotalPagar         );
			mColumnas.put(    "FECHA_DEPOSITO",     sFechaDeposito      );
			lFilas.add(mColumnas);
		}
    }catch(Exception e){ 
      e.printStackTrace();
      out.println(e); 
   }finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
   JSONObject jsonObj = new JSONObject();
   jsonObj.put(   "success",     new Boolean(true)                );
   jsonObj.put(   "registros",   lFilas                           );
   jsonObj.put(   "total",       Integer.toString(lFilas.size())  );
   respuesta = jsonObj.toString();
} else if( informacion.equals("storeEncabezado_1") ){
   try{
      String sNombre1="";
	String sFechaDelTmp="";
	String sFechaAlTmp="";
	sNombre1="V";
	sFechaDelTmp=sFechaVencimiento_E;
	sFechaAlTmp=sFechaVencimiento_E;

    String sNombre2 = Comunes.rellenaCeros(String.valueOf(iNoCliente),5);
    DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT,new Locale("ES","MX"));
  	SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMyy");  
  	String sNombre3 = ("".equals(sFechaDelTmp)?"":sdf1.format(df.parse(sFechaDelTmp)));

  	java.util.Date date2 = new java.util.Date();
  	SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMyyyy");  
  	String sNombre4 = sdf2.format(date2); 			
	String sLike=sNombre1+sNombre2+sNombre3+sNombre4;
	String sQueryArchivo = "SELECT COUNT(1) CONSECUTIVO FROM com_encabezado_pago "+
	                       " WHERE ic_if = ? AND cg_nombre_archivo LIKE '%"+sLike+"%' ";
	//out.println("sQueryArchivo:"+sQueryArchivo+"<br>");
	ps = con.queryPrecompilado(sQueryArchivo);
	ps.setString(1, iNoCliente);
	rs = ps.executeQuery();
	String sConsecutivo="";
	while (rs.next()) {
	        	sConsecutivo  = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
	}//fin while 3
	rs.close();
	if(ps!=null) ps.close();

	String sNombre5="";
	if(Integer.parseInt(sConsecutivo)==0){
		sNombre5="-1";
	}else{
		sNombre5="-"+String.valueOf(Integer.parseInt(sConsecutivo)+1);
	}

	sNombreArc = sNombre1+sNombre2+sNombre3+sNombre4+sNombre5;
   
   	dCapital=0; dInteres=0; dMoratorios=0; dSubsidio=0; dComision=0; dIva=0; dImporte=0; 
      if(conceptoPagoIni.equals("A")&&!sOpcion.equals("C3")){
         sImporteDeposito_E	= "";
         sFechaDeposito_E 	= "";
      }
      
      List lista = new ArrayList();   
      HashMap hm = new HashMap();
      JSONObject jsonObj = new JSONObject();
      jsonObj.put(   "success",     new Boolean(true)                );

         
      hm.put("SESTADO_E",           sEstado_E);
      hm.put("SCLAVEIF_E",          sClaveIF_E);
      hm.put("SCLAVEESTADO_E",      sClaveEstado_E);
      hm.put("SCLAVEMONEDA_E",      sClaveMoneda_E);
      hm.put("SFECHADEPOSITO_E",    sFechaDeposito_E);
      hm.put("SMONEDA_E",           sMoneda_E);
      hm.put("SIMPORTEDEPOSITO_E",  sImporteDeposito_E);
      hm.put("SIMPORTEDEPOSITO_E",  sImporteDeposito_E);
      hm.put("SBANCOSERVICIO_E",    sBancoServicio_E);
      hm.put("SREFBANCO_E",         sRefBanco_E);
      hm.put("SFECHAPROBPAGO_E",    sFechaProbPago_E);
      hm.put("SFECHAPROBPAGO_E",    sFechaProbPago_E);
      hm.put("SREFINTER_E",         sRefInter_E);
      hm.put("SFECHAVENCIMIENTO_E", sFechaVencimiento_E);
      hm.put("SCLAVEREFINTER_E",    sClaveRefInter_E);
      hm.put("SNOMBREBANCO_E",      sNombreBanco_E);     
      lista.add(hm);      
      respuesta = jsonObj.toString();
    }catch(Exception e){ 
      e.printStackTrace();
      new RuntimeException(e);
   }finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
}

else if(estado.equals("enviarFormaEncabezado")){
   con = new AccesoDB();
   try	{
      con.conexionDB();
     PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
   
      String hidOperacion  = (request.getParameter("hidOperacion")==null)?"":request.getParameter("hidOperacion");
      
      String sImportePesos="";
      String sImporteDolares="";
      if(sClaveMoneda_E.equals("1")){
         sImportePesos   = sImporteDeposito_E;
         sImporteDolares = "";
      }else{
         sImportePesos   = "";
         sImporteDolares = sImporteDeposito_E;
      }
      EncabezadoPagoIFNB encabezado = new EncabezadoPagoIFNB();
      encabezado.setClaveIF(sClaveIF_E);
      encabezado.setClaveDirEstatal(sClaveEstado_E);
      encabezado.setClaveMoneda(sClaveMoneda_E);
      encabezado.setFechaVencimiento(sFechaVencimiento_E);
      encabezado.setFechaProbablePago(sFechaProbPago_E);
      encabezado.setFechaDeposito(sFechaDeposito_E);
      encabezado.setImporteDeposito(sImporteDeposito_E);
      encabezado.setBancoServicio(sBancoServicio_E);
      encabezado.setReferenciaInter(sClaveRefInter_E);
      encabezado.setReferenciaBanco(sRefBanco_E);
      encabezado.setNombreArchivo(sNombreArc);
      al = bean.consultaDetalles(encabezado);
      session.setAttribute("f", al);
      
      
      List lista = new ArrayList();   
      HashMap hm = new HashMap();
      JSONObject jsonObj = new JSONObject();
      jsonObj.put("success",        new Boolean(true));  
      
      if( informacion.equals("storeEncabezado") ){
         dCapital=0; dInteres=0; dMoratorios=0; dSubsidio=0; dComision=0; dIva=0; dImporte=0; 
         ArrayList alTemp = new ArrayList();
         
         for(int i=0; i < al.size(); i++){
            alTemp=(ArrayList)al.get(i);
            String sConceptoLocal="";
            String sOrigenLocal="";
            if(alTemp.get(10).equals("AN")){sConceptoLocal="Anticipado";}
            if(alTemp.get(10).equals("VE")){sConceptoLocal="Vencimiento";}
            if(alTemp.get(10).equals("CV")){sConceptoLocal="Cartera Vencida";}
            if(alTemp.get(10).equals("CO")){sConceptoLocal="Comision";}
            if(alTemp.get(11).equals("I")){sOrigenLocal="Intermediario";}
            if(alTemp.get(11).equals("A")){sOrigenLocal="Acreditado";}            
            
            hm = new HashMap(); 
            hm.put("SESTADO_E",           sEstado_E);
            hm.put("SCLAVEIF_E",          sClaveIF_E);
            hm.put("SCLAVEESTADO_E",      sClaveEstado_E);
            hm.put("SCLAVEMONEDA_E",      sClaveMoneda_E);
            hm.put("SFECHADEPOSITO_E",    sFechaDeposito_E);
            hm.put("SMONEDA_E",           sMoneda_E);
            hm.put("SIMPORTEDEPOSITO_E",  sImporteDeposito_E);
            hm.put("SIMPORTEDEPOSITO_E",  sImporteDeposito_E);
            hm.put("SBANCOSERVICIO_E",    sBancoServicio_E);
            hm.put("SREFBANCO_E",         sRefBanco_E);
            hm.put("SFECHAPROBPAGO_E",    sFechaProbPago_E);
            hm.put("SFECHAPROBPAGO_E",    sFechaProbPago_E);
            hm.put("SREFINTER_E",         sRefInter_E);
            hm.put("SFECHAVENCIMIENTO_E", sFechaVencimiento_E);
            hm.put("SCLAVEREFINTER_E",    sClaveRefInter_E);
            hm.put("SNOMBREBANCO_E",      sNombreBanco_E);      
            lista.add(hm); 
            
            dCapital += Double.parseDouble((String)alTemp.get(3));
            dInteres += Double.parseDouble((String)alTemp.get(4));
            dMoratorios += Double.parseDouble((String)alTemp.get(5));
            dSubsidio += Double.parseDouble((String)alTemp.get(6));
            dComision += Double.parseDouble((String)alTemp.get(7));
            dIva += Double.parseDouble((String)alTemp.get(8));
            dImporte += Double.parseDouble((String)alTemp.get(9));
         }         
      }
      
      else if( informacion.equals("storeDetalles") ){
         dCapital=0; dInteres=0; dMoratorios=0; dSubsidio=0; dComision=0; dIva=0; dImporte=0; 
         ArrayList alTemp = new ArrayList();
         
         for(int i=0; i < al.size(); i++){
            alTemp=(ArrayList)al.get(i);
            String sConceptoLocal="";
            String sOrigenLocal="";
            if(alTemp.get(10).equals("AN")){sConceptoLocal="Anticipado";}
            if(alTemp.get(10).equals("VE")){sConceptoLocal="Vencimiento";}
            if(alTemp.get(10).equals("CV")){sConceptoLocal="Cartera Vencida";}
            if(alTemp.get(10).equals("CO")){sConceptoLocal="Comision";}
            if(alTemp.get(11).equals("I")){sOrigenLocal="Intermediario";}
            if(alTemp.get(11).equals("A")){sOrigenLocal="Acreditado";}            
            
            hm = new HashMap();     
            hm.put("SUBAPLICACION",    alTemp.get(0));
            hm.put("PRESTAMO",         alTemp.get(1));
            hm.put("SIRAC",            alTemp.get(2));
            hm.put("CAPITAL",          alTemp.get(3));
            hm.put("INTERES",          alTemp.get(4));
            hm.put("MORATORIOS",       alTemp.get(5));
            hm.put("SUBSIDIO",         alTemp.get(6));
            hm.put("COMISION",         alTemp.get(7));
            hm.put("IVA",              alTemp.get(8));
            hm.put("IMPORTEPAGO",      alTemp.get(9));
            hm.put("CONCEPTOPAGO",     sConceptoLocal);
            hm.put("ORIGENPAGO",       sOrigenLocal);
            lista.add(hm); 
            
            dCapital += Double.parseDouble((String)alTemp.get(3));
            dInteres += Double.parseDouble((String)alTemp.get(4));
            dMoratorios += Double.parseDouble((String)alTemp.get(5));
            dSubsidio += Double.parseDouble((String)alTemp.get(6));
            dComision += Double.parseDouble((String)alTemp.get(7));
            dIva += Double.parseDouble((String)alTemp.get(8));
            dImporte += Double.parseDouble((String)alTemp.get(9));
         }
         
         // totales
         hm = new HashMap();     
         hm.put("SUBAPLICACION",    "<b>TOTAL</b>");
         hm.put("PRESTAMO",         "");
         hm.put("SIRAC",            "");
         hm.put("CAPITAL",          dCapital+"");
         hm.put("INTERES",          dInteres+"");
         hm.put("MORATORIOS",       dMoratorios+"");
         hm.put("SUBSIDIO",         dSubsidio+"");
         hm.put("COMISION",         dComision+"");
         hm.put("IVA",              dIva+"");
         hm.put("IMPORTEPAGO",      dImporte+"");
         hm.put("CONCEPTOPAGO",     "");
         hm.put("ORIGENPAGO",       "");
         lista.add(hm);
      }
      
      else if( informacion.equals("storeDetalles_2") ){
         String mostrarTotales  = (request.getParameter("mostrarTotales")==null)?"":request.getParameter("mostrarTotales");
         al = bean.consultaDetallesIFB(encabezado);
         session.setAttribute("f", al);
         dCapital=0; dInteres=0; dMoratorios=0; dSubsidio=0; dComision=0; dIva=0; dImporte=0; 
         ArrayList alTemp = new ArrayList();
         
         for(int i=0; i < al.size(); i++){
            alTemp = new ArrayList();
            alTemp=(ArrayList)al.get(i);
            String sConceptoLocal="";
            String sOrigenLocal="";
            if(alTemp.get(15).equals("AN")){sConceptoLocal="Anticipado";}
            if(alTemp.get(15).equals("VE")){sConceptoLocal="Vencimiento";}
            if(alTemp.get(15).equals("CV")){sConceptoLocal="Cartera Vencida";}
            if(alTemp.get(15).equals("CO")){sConceptoLocal="Comision";}          
            
            hm = new HashMap();     
            hm.put("SUCURSAL",    alTemp.get(0));   
            hm.put("SUBAPLICACION",         alTemp.get(1));
            hm.put("PRESTAMO",            alTemp.get(2));
            hm.put("ACREDITADO",          alTemp.get(3));
            hm.put("FECHA_OPERACION",          alTemp.get(4));
            hm.put("FECHA_VENCIMIENTO",       alTemp.get(5));
            hm.put("FECHA_PAGO",         alTemp.get(6));
            hm.put("SALDO_INSOLUTO",         alTemp.get(7));
            hm.put("TASA",              alTemp.get(8));
            hm.put("CAPITAL",      alTemp.get(9));
            hm.put("DIAS",      alTemp.get(10));
            hm.put("INTERESES",      alTemp.get(11));
            hm.put("COMISION",      alTemp.get(12));
            hm.put("IVA",      alTemp.get(13));     
            hm.put("IMPORTEPAGO",      alTemp.get(14));
            hm.put("CONCEPTOPAGO",      sConceptoLocal);
            lista.add(hm); 
            
            dCapital += Double.parseDouble((String)alTemp.get(9));
            dInteres += Double.parseDouble((String)alTemp.get(11));
            dComision += Double.parseDouble((String)alTemp.get(12));
            dIva += Double.parseDouble((String)alTemp.get(13));
            dImporte += Double.parseDouble((String)alTemp.get(14));
         }
         
         if(mostrarTotales.equals("") || !mostrarTotales.equals("No")){
            // totales
            hm = new HashMap();     
            hm.put("SUBAPLICACION",    "<b>TOTAL</b>");
            hm.put("PRESTAMO",         "");
            hm.put("SIRAC",            "");
            hm.put("CAPITAL",          dCapital+"");
            hm.put("INTERES",          dInteres+"");
            hm.put("MORATORIOS",       dMoratorios+"");
            hm.put("SUBSIDIO",         dSubsidio+"");
            hm.put("COMISION",         dComision+"");
            hm.put("IVA",              dIva+"");
            hm.put("IMPORTEPAGO",      dImporte+"");
            hm.put("CONCEPTOPAGO",     "");
            hm.put("ORIGENPAGO",       "");
            lista.add(hm);
         }
      }
      
      else if( informacion.equals("storeDetallesEditor") ){
         System.err.println(estado + "." + informacion);
         String sSucursal_D[]			= request.getParameterValues("sSucursal_D");
         String sAcreditado_D[]			= request.getParameterValues("sAcreditado_D");
         String sFechaOperacion_D[]		= request.getParameterValues("sFechaOperacion_D");
         String sFechaVencimiento_D[]	= request.getParameterValues("sFechaVencimiento_D");
         String sFechaPago_D[]			= request.getParameterValues("sFechaPago_D");
         String sSaldoInsoluto_D[]		= request.getParameterValues("sSaldoInsoluto_D");
         String sTasaBase_D[]			= request.getParameterValues("sTasaBase_D");
         String sDias_D[]				= request.getParameterValues("sDias_D");
         
         session.removeAttribute("f");
         al = (ArrayList)session.getAttribute("f");
         
         alRegistro = new ArrayList();
         if(al==null)
            al = new ArrayList();
         if(sSubaplicacion_D!=null){
            for(int i=0;i<sSubaplicacion_D.length;i++){
               if(i==sSubaplicacion_D.length-1&&!"A".equals(sAccion))
                  break;
               alRegistro = new ArrayList();
               alRegistro.add(sSucursal_D[i]);
               alRegistro.add(sSubaplicacion_D[i]);
               alRegistro.add(sPrestamo_D[i]);
               alRegistro.add(sAcreditado_D[i]);
               alRegistro.add(sFechaOperacion_D[i]);
               alRegistro.add(sFechaVencimiento_D[i]);
               alRegistro.add(sFechaPago_D[i]);
               alRegistro.add(sSaldoInsoluto_D[i]);
               alRegistro.add(sTasaBase_D[i]);
               alRegistro.add(sCapital_D[i]);
               alRegistro.add(sDias_D[i]);
               alRegistro.add(sInteres_D[i]);
               alRegistro.add(sComision_D[i]);
               alRegistro.add(sIva_D[i]);
               alRegistro.add(sImporte_D[i]);
               alRegistro.add(Concepto_D[i]);
               alRegistro.add(hidChecked[i]);
               al.add(alRegistro);
               
               hm = new HashMap();     
               hm.put("SUCURSAL",    alRegistro.get(0));   
               hm.put("SUBAPLICACION",         alRegistro.get(1));
               hm.put("PRESTAMO",            alRegistro.get(2));
               hm.put("ACREDITADO",          alRegistro.get(3));
               hm.put("FECHA_OPERACION",          alRegistro.get(4));
               hm.put("FECHA_VENCIMIENTO",       alRegistro.get(5));
               hm.put("FECHA_PAGO",         alRegistro.get(6));
               hm.put("SALDO_INSOLUTO",         alRegistro.get(7));
               hm.put("TASA",              alRegistro.get(8));
               hm.put("CAPITAL",      alRegistro.get(9));
               hm.put("DIAS",      alRegistro.get(10));
               hm.put("INTERESES",      alRegistro.get(11));
               hm.put("COMISION",      alRegistro.get(12));
               hm.put("IVA",      alRegistro.get(13));     
               hm.put("IMPORTEPAGO",      alRegistro.get(14));
               hm.put("CONCEPTOPAGO",      alRegistro.get(15));
               lista.add(hm); 
            }
         }
         session.setAttribute("f",al);
         
         String sCondicion="";
         String sOrden="";
         String sFechaDel="";
         String sFechaAl="";
         List  	lFilas		= new ArrayList();
         Map		mColumnas	= null;
      
         sOrden=" V.com_fechaprobablepago ";
      
         String query = " SELECT "   +
                           "      /*+"	+
                             "      use_nl(V)"	+
                             "      index(V CP_COM_VENCIMIENTO_PK)"	+
                             "      */"	+
                          "       V.ic_if CLAVE_IF"   +
                        "       ,V.ic_moneda CLAVE_MONEDA"	+
                           "       ,M.cd_nombre MONEDA"		+
                        "       ,TO_CHAR(V.df_periodofin,'DD/MM/YYYY') FECHA_VENCIMIENTO"   +
                        "       ,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY') FECHA_PROB_PAGO"   +
                            "       ,F.ic_estado CLAVE_ESTADO"	+
                            "       ,E.cd_nombre NOMBRE_ESTADO"		+
                            "       ,RI.cg_descripcion REF_INTER"	+
                            "       ,RI.ic_referencia_inter CLAVE_REF_INTER"	+
                        " 	   ,V.df_periodofin"   +
                        " 	   ,V.com_fechaprobablepago"   +
                        "	   ,SUM(V.fg_totalvencimiento) IMPORTE_DEPOSITO"	+
                        "	   ,TO_CHAR(SYSDATE,'dd/mm/yyyy') as FECHA_DEPOSITO"+
                        "	   ,count(1) as numreg"+
                        " FROM"   +
                        "       com_vencimiento V"   +
                        "       ,comcat_moneda M"   +
                        "       ,comcat_if I"	+
                           "       ,comcat_financiera F"	+
                            "       ,comcat_estado E"	+
                        "       ,comcat_Referencia_inter RI"	+
                        " WHERE"   +
                        "       V.ic_moneda=M.ic_moneda"		+
                        "       AND V.ic_if=I.ic_if"		+
                               "       AND I.ic_financiera=F.ic_financiera"		+
                            "       AND F.ic_estado=E.ic_estado"		+
                        "       AND V.ic_if=RI.ic_if"	+
                       // "       AND V.ic_if = ?"   +
                        sCondicion+
                        " GROUP BY"   +
                        "       V.ic_if"   +
                        //"       ,V.ig_sucursal"   +
                        "       ,V.ic_moneda"	+
                        "       ,M.cd_nombre"   +
                        "       ,TO_CHAR(V.df_periodofin,'DD/MM/YYYY')"   +
                        "       ,TO_CHAR(V.com_fechaprobablepago,'DD/MM/YYYY')"	+
                        "       ,F.ic_estado"	+
                        "       ,E.cd_nombre"	+
                            "       ,RI.cg_descripcion"		+
                        "       ,RI.ic_referencia_inter"	+
                        " 	   ,V.df_periodofin"   +
                        " 	   ,V.com_fechaprobablepago"   +
                        " ORDER BY"	+
                        sOrden;
            //out.println("query:"+query+"<br>");
            ps = con.queryPrecompilado(query);
            //ps.setString(1, iNoCliente);
            /*ps.setString(2, sFechaDel);
            ps.setString(3, sFechaAl);*/
            rs = ps.executeQuery();
            double dImporteDepositoTotal=0;
            while (rs.next()) {
               sClaveIF			= (rs.getString("CLAVE_IF") == null) ? "" : rs.getString("CLAVE_IF").trim();
               sClaveMoneda		= (rs.getString("CLAVE_MONEDA") == null) ? "" : rs.getString("CLAVE_MONEDA").trim();
               sMoneda				= (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA").trim();
               sFechaVencimiento	= (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO").trim();
               sFechaProbPago		= (rs.getString("FECHA_PROB_PAGO") == null) ? "" : rs.getString("FECHA_PROB_PAGO").trim();
               sClaveEstado		= (rs.getString("CLAVE_ESTADO") == null) ? "" : rs.getString("CLAVE_ESTADO").trim();
               sEstado				= (rs.getString("NOMBRE_ESTADO") == null) ? "" : rs.getString("NOMBRE_ESTADO").trim();
               sRefInter			= (rs.getString("REF_INTER") == null) ? "" : rs.getString("REF_INTER").trim();
               sClaveRefInter		= (rs.getString("CLAVE_REF_INTER") == null) ? "" : rs.getString("CLAVE_REF_INTER").trim();
               dTotalPagar			= ""+rs.getDouble("IMPORTE_DEPOSITO");
               sFechaDeposito		= (rs.getString("FECHA_DEPOSITO") == null) ? "" : rs.getString("FECHA_DEPOSITO").trim();
               mColumnas 			= new HashMap();
               mColumnas.put("sClaveIF",sClaveIF);
               mColumnas.put("sClaveMoneda",sClaveMoneda);
               mColumnas.put("sMoneda",sMoneda);
               mColumnas.put("sFechaVencimiento",sFechaVencimiento);
               mColumnas.put("sFechaProbPago",sFechaProbPago);
               mColumnas.put("sClaveEstado",sClaveEstado);
               mColumnas.put("sEstado",sEstado);
               mColumnas.put("sRefInter",sRefInter);
               mColumnas.put("sClaveRefInter",sClaveRefInter);
               mColumnas.put("dTotalPagar",dTotalPagar);
               mColumnas.put("sFechaDeposito",sFechaDeposito);
               lFilas.add(mColumnas);
            }
            for(registros=0;registros<lFilas.size();registros++){
               mColumnas = (Map)lFilas.get(registros);
               if(registros==0) {//imprime encabezado
               
               }
               if(registros==0){
               
               }
            }//for  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            
            int i=0;
            if(conceptoPagoIni.equals("V")&&al.size()==0){
               
               String qrySentencia = 
                  " SELECT /*+ use_nl(V) "	+
                  "			index(V CP_COM_VENCIMIENTO_PK) */ "	+
                  "			V.ig_sucursal SUCURSAL"+
                  "			,V.ig_subaplicaCion SUBAPLICACION"   +
                  "			,V.ig_prestamo PRESTAMO"   +
                  "			,v.cg_nombrecliente ACREDITADO"+
                  "			,to_char(v.df_periodoinic,'dd/mm/yyyy') FECHA_OPERACION"+
                  "			,to_char(v.df_periodofin,'dd/mm/yyyy') FECHA_VENCIMIENTO"+
                  "			,to_char(v.com_fechaprobablepago,'dd/mm/yyyy') FECHA_PAGO"+
                  "			,V.ig_tasabase TASA"+
                  "			,V.fg_amortizacion CAPITAL"   +
                  "			,v.ig_dias DIAS"+
                  "			,V.fg_interes INTERESES"   +
                  "			,V.fg_subsidio SUBSIDIO"   +
                  "			,V.fg_comision COMISION"   +
                  "			,V.fg_totalvencimiento IMPORTE_PAGO"   +
                  "   FROM COM_VENCIMIENTO V"   +
                  " WHERE V.ic_if = ?"   +
                  "     AND V.ic_moneda = ?"   +
                  "     AND trunc(V.df_periodofin) = to_date(?,'dd/mm/yyyy')"   +
                  "     AND trunc(V.com_fechaprobablepago) = to_date(?,'dd/mm/yyyy')"  ;
               ps = con.queryPrecompilado(qrySentencia);
               ps.setString(1,iNoCliente);
               ps.setString(2,sClaveMoneda_E);
               ps.setString(3,sFechaVencimiento_E);
               ps.setString(4,sFechaProbPago_E);
               rs = ps.executeQuery();
               while(rs.next()){
                  String sConceptoLocal="Vencimiento";
                  alRegistro = new ArrayList();
                  alRegistro.add(rs.getString("SUBAPLICACION"));
                  alRegistro.add(rs.getString("PRESTAMO"));
                  //alRegistro.add(rs.getString("CLAVE_SIRAC"));
                  alRegistro.add(rs.getString("CAPITAL"));
                  alRegistro.add(rs.getString("INTERESES"));
                  alRegistro.add("0.00");		//moratorios
                  alRegistro.add(rs.getString("SUBSIDIO"));
                  alRegistro.add(rs.getString("COMISION"));
                  alRegistro.add("0.00");		//iva
                  alRegistro.add(rs.getString("IMPORTE_PAGO"));
                  alRegistro.add("VE");	//concepto pago
                  alRegistro.add("I");	// origen pago
                  alRegistro.add("S");	// checked
                  al.add(alRegistro);
                  
                     dCapital += rs.getDouble("CAPITAL");
                  dInteres += rs.getDouble("INTERESES");
                  //dMoratorios += Double.parseDouble((String)alTemp.get(5));
                  dSubsidio += rs.getDouble("SUBSIDIO");
                  dComision += rs.getDouble("COMISION");
                  //dIva += Double.parseDouble((String)alTemp.get(8));
                  dImporte += rs.getDouble("IMPORTE_PAGO");
                  i++;
               }//while
               
               rs.close();
               ps.close();
            }else{			// if V
            //out.println("size:"+al.size()+"<br>");
            for(i=0; i < al.size(); i++){
               //out.println("numero"+i+": "+al.get(i)+"<br>");
               ArrayList alTemp = new ArrayList();
               alTemp=(ArrayList)al.get(i);
               String sConceptoLocal="";
               String sOrigenLocal="";
               if(alTemp.get(10).equals("AN")){sConceptoLocal="Anticipado";}
               if(alTemp.get(10).equals("VE")){sConceptoLocal="Vencimiento";}
               if(alTemp.get(10).equals("CV")){sConceptoLocal="Cartera Vencida";}
               if(alTemp.get(10).equals("CO")){sConceptoLocal="Comision";}
               if(alTemp.get(11).equals("I")){sOrigenLocal="Intermediario";}
               if(alTemp.get(11).equals("A")){sOrigenLocal="Acreditado";}
               
               if("S".equals((String)alTemp.get(16))){
                  dCapital += Double.parseDouble((String)alTemp.get(9));
                  dInteres += Double.parseDouble((String)alTemp.get(11));
                  dComision += Double.parseDouble((String)alTemp.get(12));
                  dIva += Double.parseDouble((String)alTemp.get(13));
                  dImporte += Double.parseDouble((String)alTemp.get(14));
               }
            }//FIN ROR al
         }	//else         
      }
      
      jsonObj.put(   "success",     new Boolean(true)                );
      jsonObj.put(   "registros",   lista                           );
      jsonObj.put(   "total",       Integer.toString(lista.size())  );
      respuesta = jsonObj.toString();
        
   }catch(Exception e){
      e.printStackTrace();
      throw new RuntimeException(e);
   }finally{ if(con.hayConexionAbierta()) con.cierraConexionDB(); }

}

if(informacion.equals("ConfirmarCertificado")){
	String pkcs7 				= (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
	String externContent 	= (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
	String serial 				= "";
	String folioCert 			= "";
	String mensaje          = null;
   
	//FODEA023-2010 FVR
	externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
	char getReceipt = 'Y';
   String _acuse = "";
   
	con = new AccesoDB();
	try{
      con.conexionDB();
      PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
		
		//	Variables para uso en esta página
      Acuse3 acuse;
      //Parámetros provenienen de la página anterior y de la actual
      String hidOperacion = (request.getParameter("hidOperacion")==null)?"":request.getParameter("hidOperacion");
     
      //_________________________ INICIO DEL PROGRAMA _________________________
		try	{
		 //java.security.cert.X509Certificate certs [] = (java.security.cert.X509Certificate []) request.getAttribute("javax.servlet.request.X509Certificate");
		/*java.security.cert.X509Certificate certs [] 	  = (java.security.cert.X509Certificate [])	request.getAttribute("javax.servlet.request.X509Certificate");
			for(int _i = 0; _i<certs.length; _i++) {
				byte x [] = certs[_i].getSerialNumber().toByteArray();
				serial = new String (x);
			}*/
         serial = _serial;
		}
		catch(Exception e) {
			mensaje = "Error al leer el certificado";
		}
      //serial = _serial;
		//if (hidOperacion.equals("ACEPTAR"))	{
			acuse = new Acuse3(Acuse.ACUSE_IF,"1");
			if (!serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
				folioCert = acuse.toString();
				Seguridad s = new Seguridad();
				
				if (s.autenticar(folioCert, serial, pkcs7, externContent, getReceipt)) {            
					_acuse = s.getAcuse();
					//out.println("_acuse_"+_acuse+"<br>");
					acuse.setClaveUsuario(iNoUsuario);
				    acuse.setReciboElectronico(_acuse);
					String sImportePesos="";
					String sImporteDolares="";
					if(sClaveMoneda_E.equals("1")){
						sImportePesos   = sImporteDeposito_E;
						sImporteDolares = "";
					}else{
						sImportePesos   = "";
						sImporteDolares = sImporteDeposito_E;
					}
				    acuse.setMontoMN(sImportePesos);
				    acuse.setMontoDL(sImporteDolares);
					EncabezadoPagoIFNB encabezado = new EncabezadoPagoIFNB();
					encabezado.setClaveIF(sClaveIF_E);
					encabezado.setClaveDirEstatal(sClaveEstado_E);
					encabezado.setClaveMoneda(sClaveMoneda_E);
					encabezado.setFechaVencimiento(sFechaVencimiento_E);
					encabezado.setFechaProbablePago(sFechaProbPago_E);
					encabezado.setFechaDeposito(sFechaDeposito_E);
					encabezado.setImporteDeposito(sImporteDeposito_E);
					encabezado.setBancoServicio(sBancoServicio_E);
					encabezado.setReferenciaInter(sClaveRefInter_E);
					encabezado.setReferenciaBanco(sRefBanco_E);
					encabezado.setAcuse(acuse.getAcuse());
					encabezado.setNombreArchivo(sNombreArc);
					try {
						al = bean.cargaIndividualPagos(acuse, encabezado);
						session.setAttribute("f", al);
						//INSERTO CORRECTAMENTE, PONER LO SIGUIENTE
                  
                  mensaje = "Los datos fueron enviados con éxito";
                  List lista = new ArrayList();
                                    
                  //Encabezado
                  HashMap hm = new HashMap();    
                  hm.put("SESTADO_E",           sEstado_E);
                  hm.put("SMONEDA_E",           sMoneda_E);
                  hm.put("SNOMBREBANCO_E",      sNombreBanco_E); 
                  hm.put("SFECHAVENCIMIENTO_E", sFechaVencimiento_E);
                  hm.put("SFECHAPROBPAGO_E",    sFechaProbPago_E); 
                  hm.put("SIMPORTEDEPOSITO_E",  sImporteDeposito_E);
                  hm.put("SIMPORTEDEPOSITO_E",  sImporteDeposito_E);
                  lista.add(hm); 
                  
                  // Detalles
                  dCapital=0; dInteres=0; dMoratorios=0; dSubsidio=0; dComision=0; dIva=0; dImporte=0; 
                  for(int i=0; i < al.size(); i++){
                     ArrayList alTemp = new ArrayList();
                     alTemp=(ArrayList)al.get(i);
                     String sConceptoLocal="";
                     String sOrigenLocal="";
                     if(alTemp.get(10).equals("AN")){sConceptoLocal="Anticipado";}
                     if(alTemp.get(10).equals("VE")){sConceptoLocal="Vencimiento";}
                     if(alTemp.get(10).equals("CV")){sConceptoLocal="Cartera Vencida";}
                     if(alTemp.get(10).equals("CO")){sConceptoLocal="Comision";}
                     if(alTemp.get(11).equals("I")){sOrigenLocal="Intermediario";}
                     if(alTemp.get(11).equals("A")){sOrigenLocal="Acreditado";}
                                          
                     hm = new HashMap();     
                     hm.put("SUBAPLICACION",    alTemp.get(0));
                     hm.put("PRESTAMO",         alTemp.get(1));
                     hm.put("SIRAC",            alTemp.get(2));
                     hm.put("CAPITAL",          alTemp.get(3));
                     hm.put("INTERES",          alTemp.get(4));
                     hm.put("MORATORIOS",       alTemp.get(5));
                     hm.put("SUBSIDIO",         alTemp.get(6));
                     hm.put("COMISION",         alTemp.get(7));
                     hm.put("IVA",              alTemp.get(8));
                     hm.put("IMPORTEPAGO",      alTemp.get(9));
                     hm.put("CONCEPTOPAGO",     sConceptoLocal);
                     hm.put("ORIGENPAGO",       sOrigenLocal);
                     lista.add(hm); 
                     
                     
                     dCapital += Double.parseDouble((String)alTemp.get(3));
                     dInteres += Double.parseDouble((String)alTemp.get(4));
                     dMoratorios += Double.parseDouble((String)alTemp.get(5));
                     dSubsidio += Double.parseDouble((String)alTemp.get(6));
                     dComision += Double.parseDouble((String)alTemp.get(7));
                     dIva += Double.parseDouble((String)alTemp.get(8));
                     dImporte += Double.parseDouble((String)alTemp.get(9));
                  }//FIN ROR al
                  
                   // totales
                  hm = new HashMap();     
                  hm.put("SUBAPLICACION",    "<b>TOTAL</b>");
                  hm.put("PRESTAMO",         "");
                  hm.put("SIRAC",            "");
                  hm.put("CAPITAL",          dCapital+"");
                  hm.put("INTERES",          dInteres+"");
                  hm.put("MORATORIOS",       dMoratorios+"");
                  hm.put("SUBSIDIO",         dSubsidio+"");
                  hm.put("COMISION",         dComision+"");
                  hm.put("IVA",              dIva+"");
                  hm.put("IMPORTEPAGO",      dImporte+"");
                  hm.put("CONCEPTOPAGO",     "");
                  hm.put("ORIGENPAGO",       "");
                  lista.add(hm);
                  
                  // Datos del Acuse
                  hm = new HashMap();     
                  hm.put("NUMERO_ACUSE",    folioCert);
                  
                  String query = " SELECT "   +
                              " 	  TO_CHAR(DF_FECHA_HORA,'DD/MM/YYYY') FECHA"   +
                              " 	  ,TO_CHAR(DF_FECHA_HORA,'HH24:MI') HORA "   +
                              " 	  FROM COM_ACUSE3 "   +
                              " WHERE "   +
                              "       CC_ACUSE=?"  ;	
                  //out.println("query:"+query+"<br>");
                  ps = con.queryPrecompilado(query);
                  ps.setString(1, folioCert);
                  rs = ps.executeQuery();
                  String sFecha="", sHora="";
                  while (rs.next()) {
                        sFecha  = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
                        sHora   = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
                  }//while
                  rs.close();
                  if(ps!=null) ps.close();
                                    
                  JSONObject jsonObj = new JSONObject();
                  jsonObj.put(   "success",     new Boolean(true)               );
                  jsonObj.put(   "registros",   lista                           );
                  jsonObj.put(   "mensaje",     mensaje                         );
                  jsonObj.put(   "acuse",       _acuse                          );
                  jsonObj.put(   "folioCert",   folioCert                       );
                  jsonObj.put(   "sFecha",      sFecha                          );
                  jsonObj.put(   "sHora",       sHora                           );
                  jsonObj.put(   "NumPrestamo", String.valueOf(al.size())       );
                  jsonObj.put(   "dImporte",    String.valueOf(dImporte)        );
                  jsonObj.put(   "total",       Integer.toString(lista.size())  );
                  respuesta = jsonObj.toString();
                  
            } catch (NafinException ne){
                System.err.println(ne);        
            }
         }
      }     
      con.cierraConexionDB();
	}catch(Exception e) {
		System.err.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
}

if(informacion.equals("ConfirmarCertificado_editable")){
	String pkcs7 				= (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
	String externContent 	= (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
	String serial 				= "";
	String folioCert 			= "";
	String mensaje          = null;
   
	//FODEA023-2010 FVR
	externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
	char getReceipt = 'Y';
   String _acuse = "";
   
	con = new AccesoDB();
	try{
      con.conexionDB();
      PagosIFNB bean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
		
		//	Variables para uso en esta página
      Acuse3 acuse;
      //Parámetros provenienen de la página anterior y de la actual
      String hidOperacion = (request.getParameter("hidOperacion")==null)?"":request.getParameter("hidOperacion");
     
      //_________________________ INICIO DEL PROGRAMA _________________________
		try	{
		 //java.security.cert.X509Certificate certs [] = (java.security.cert.X509Certificate []) request.getAttribute("javax.servlet.request.X509Certificate");
		/*java.security.cert.X509Certificate certs [] 	  = (java.security.cert.X509Certificate [])	request.getAttribute("javax.servlet.request.X509Certificate");
			for(int _i = 0; _i<certs.length; _i++) {
				byte x [] = certs[_i].getSerialNumber().toByteArray();
				serial = new String (x);
			}*/
         serial = _serial;
		}
		catch(Exception e) {
			mensaje = "Error al leer el certificado";
		}
      //serial = _serial;
		//if (hidOperacion.equals("ACEPTAR"))	{
			acuse = new Acuse3(Acuse.ACUSE_IF,"1");
			if (!serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
				folioCert = acuse.toString();
				Seguridad s = new Seguridad();
				
				if (s.autenticar(folioCert, serial, pkcs7, externContent, getReceipt)) {
            
					_acuse = s.getAcuse();
					//out.println("_acuse_"+_acuse+"<br>");
					acuse.setClaveUsuario(iNoUsuario);
				    acuse.setReciboElectronico(_acuse);
					String sImportePesos="";
					String sImporteDolares="";
					if(sClaveMoneda_E.equals("1")){
						sImportePesos   = sImporteDeposito_E;
						sImporteDolares = "";
					}else{
						sImportePesos   = "";
						sImporteDolares = sImporteDeposito_E;
					}
				    acuse.setMontoMN(sImportePesos);
				    acuse.setMontoDL(sImporteDolares);
					EncabezadoPagoIFNB encabezado = new EncabezadoPagoIFNB();
					encabezado.setClaveIF(iNoCliente);
					encabezado.setClaveDirEstatal(sClaveEstado_E);
					encabezado.setClaveMoneda(sClaveMoneda_E);
					encabezado.setFechaVencimiento(sFechaVencimiento_E);
					encabezado.setFechaProbablePago(sFechaProbPago_E);
					encabezado.setFechaDeposito(sFechaDeposito_E);
					encabezado.setImporteDeposito(sImporteDeposito_E);
					encabezado.setBancoServicio(sBancoServicio_E);
					encabezado.setReferenciaInter(sClaveRefInter_E);
					encabezado.setReferenciaBanco(sRefBanco_E);
					encabezado.setAcuse(acuse.getAcuse());
					encabezado.setNombreArchivo(sNombreArc);
					try {
						bean.capturaIndividualPagosIFB(acuse, encabezado, al);
						//INSERTO CORRECTAMENTE, PONER LO SIGUIENTE
                  
                  mensaje = "Los datos fueron enviados con éxito";
                  List lista = new ArrayList();
                                    
                  //Encabezado
                  HashMap hm = new HashMap();    
                  hm.put("SESTADO_E",           sEstado_E);
                  hm.put("SMONEDA_E",           sMoneda_E);
                  hm.put("SNOMBREBANCO_E",      sNombreBanco_E); 
                  hm.put("SFECHAVENCIMIENTO_E", sFechaVencimiento_E);
                  hm.put("SFECHAPROBPAGO_E",    sFechaProbPago_E); 
                  hm.put("SIMPORTEDEPOSITO_E",  sImporteDeposito_E);
                  hm.put("SIMPORTEDEPOSITO_E",  sImporteDeposito_E);
                  lista.add(hm); 
                  
                  // Detalles
                  dCapital=0; dInteres=0; dMoratorios=0; dSubsidio=0; dComision=0; dIva=0; dImporte=0; 
                 for(int i=0; i < al.size(); i++){
                     ArrayList alTemp = new ArrayList();
                     alTemp=(ArrayList)al.get(i);
                     String sConceptoLocal="";
                     String sOrigenLocal="";
                     if(alTemp.get(15).equals("AN")){sConceptoLocal="Anticipado";}
                     if(alTemp.get(15).equals("VE")){sConceptoLocal="Vencimiento";}
                     if(alTemp.get(15).equals("CV")){sConceptoLocal="Cartera Vencida";}
                     if(alTemp.get(15).equals("CO")){sConceptoLocal="Comision";}
                                          
                     hm = new HashMap();     
                     hm.put("SUCURSAL",         alTemp.get(0));
                     hm.put("SUBAPLICACION",    alTemp.get(1));
                     hm.put("PRESTAMO",         alTemp.get(2));
                     hm.put("ACREDITADO",       alTemp.get(3));
                     hm.put("FECHA_OPERACION",  alTemp.get(4));
                     hm.put("FECHA_VENCIMIENTO",alTemp.get(5));
                     hm.put("FECHA_PAGO",       alTemp.get(6));
                     hm.put("SALDO_INSOLUTO",   alTemp.get(7));
                     hm.put("TASA",             alTemp.get(8));
                     hm.put("CAPITAL",          alTemp.get(9));
                     hm.put("DIAS",             alTemp.get(10));
                     hm.put("INTERESES",        alTemp.get(11));
                     hm.put("COMISION",         alTemp.get(12));
                     hm.put("IVA",              alTemp.get(13));
                     hm.put("IMPORTEPAGO",      alTemp.get(14));
                     hm.put("CONCEPTOPAGO",     sConceptoLocal);
                     lista.add(hm); 
                     
                                    
                     dCapital += Double.parseDouble((String)alTemp.get(9));
                     dInteres += Double.parseDouble((String)alTemp.get(11));
                     dComision += Double.parseDouble((String)alTemp.get(12));
                     dIva += Double.parseDouble((String)alTemp.get(13));
                     dImporte += Double.parseDouble((String)alTemp.get(14));
                  }//FIN ROR al
                  
                   // totales
                  hm = new HashMap();      
                     hm.put("SUCURSAL",         "<b>TOTAL</b>");
                     hm.put("SUBAPLICACION",    "");
                     hm.put("PRESTAMO",         "");
                     hm.put("ACREDITADO",       "");
                     hm.put("FECHA_OPERACION",  "");
                     hm.put("FECHA_VENCIMIENTO","");
                     hm.put("FECHA_PAGO",       "");
                     hm.put("SALDO_INSOLUTO",   "");
                     hm.put("TASA",             "");
                     hm.put("CAPITAL",          String.valueOf(dCapital));
                     hm.put("DIAS",             "");
                     hm.put("INTERESES",        String.valueOf(dInteres));
                     hm.put("COMISION",         String.valueOf(dComision));
                     hm.put("IVA",              String.valueOf(dIva));
                     hm.put("IMPORTEPAGO",      String.valueOf(dImporte));
                     hm.put("CONCEPTOPAGO",     "");
                  lista.add(hm);
                  
                  // Datos del Acuse
                  hm = new HashMap();     
                  hm.put("NUMERO_ACUSE",    folioCert);
                  
                  String query = " SELECT "   +
                              " 	  TO_CHAR(DF_FECHA_HORA,'DD/MM/YYYY') FECHA"   +
                              " 	  ,TO_CHAR(DF_FECHA_HORA,'HH24:MI') HORA "   +
                              " 	  FROM COM_ACUSE3 "   +
                              " WHERE "   +
                              "       CC_ACUSE=?"  ;	
                  //out.println("query:"+query+"<br>");
                  ps = con.queryPrecompilado(query);
                  ps.setString(1, folioCert);
                  rs = ps.executeQuery();
                  String sFecha="", sHora="";
                  while (rs.next()) {
                        sFecha  = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
                        sHora   = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
                  }//while
                  rs.close();
                  if(ps!=null) ps.close();
                                    
                  JSONObject jsonObj = new JSONObject();
                  jsonObj.put(   "success",     new Boolean(true)               );
                  jsonObj.put(   "registros",   lista                           );
                  jsonObj.put(   "mensaje",     mensaje                         );
                  jsonObj.put(   "acuse",       _acuse                          );
                  jsonObj.put(   "folioCert",   folioCert                       );
                  jsonObj.put(   "sFecha",      sFecha                          );
                  jsonObj.put(   "sHora",       sHora                           );
                  jsonObj.put(   "NumPrestamo", String.valueOf(al.size())       );
                  jsonObj.put(   "dImporte",    String.valueOf(dImporte)        );
                  jsonObj.put(   "total",       Integer.toString(lista.size())  );
                  respuesta = jsonObj.toString();
                  
            } catch (NafinException ne){
                System.err.println(ne);        
            }
         }
      }     
      con.cierraConexionDB();
	}catch(Exception e) {
		System.err.println(e.getMessage()); 
		e.printStackTrace();
	}finally {	
		if(con.hayConexionAbierta()){ 
			con.cierraConexionDB();	
		}
	}
}

else if( informacion.equals("validaPrestamoEditable") ){
   PagosIFNB pagosBean = ServiceLocator.getInstance().lookup("PagosIFNBEJB", PagosIFNB.class);
   String prestamo 	            = request.getParameter("prestamo"); 
   String mensaje                = null;
   ArrayList alSucSub            = null;
   boolean numeroPrestamoValido  = true;
   
	try{
		alSucSub = pagosBean.getDatosPrestamos(prestamo);
	}catch(Exception e){
		numeroPrestamoValido = false;
      mensaje = "El numero de prestamo no es valido";
	}
	if(numeroPrestamoValido&&alSucSub.size()==0){
		numeroPrestamoValido = false;  
      mensaje = "El numero de prestamo no es valido";
   }
      
                     
   JSONObject jsonObj = new JSONObject();
   jsonObj.put( "success",                 new Boolean(true)                  );
   jsonObj.put( "prestamo",                prestamo                           );
   jsonObj.put( "mensaje",                 mensaje                            );
   jsonObj.put( "numeroPrestamoValido",    new Boolean(numeroPrestamoValido)  );
   respuesta = jsonObj.toString();
}


else if(   informacion.equals("AcuseSolPDF") ){
   String folioCert  = request.getParameter("folioCert")==null?"":request.getParameter("folioCert");
   String _acuse     = request.getParameter("recibo")==null?"":request.getParameter("recibo");
   String idBoton    = request.getParameter("idBoton")==null?"":request.getParameter("idBoton");
   
   //>>CREA REPORTE PDF
   CreaArchivo archivo = new CreaArchivo();
   String nombreArchivo = archivo.nombreArchivo()+".pdf";
   //<<CREA REPORTE PDF	
   
   try{
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

	    pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	                                 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			    					 (String)session.getAttribute("sesExterno"),
	                                 (String) session.getAttribute("strNombre"),
                                     (String) session.getAttribute("strNombreUsuario"),
						    		 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

		pdfDoc.addText("Los datos fueron enviados con éxito\n Recibo:"+_acuse,"formasB",ComunesPDF.CENTER);

		pdfDoc.setTable(7,100);
		pdfDoc.setCell("Encabezado","formasmenB",ComunesPDF.LEFT,7);		
		pdfDoc.setCell("Dirección estatal","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Banco servicio","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha vencimiento","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha probable de pago","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de depósito","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Importe de depósito","formasmenB",ComunesPDF.CENTER);
		
		pdfDoc.setCell(sEstado_E,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(sMoneda_E,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(sNombreBanco_E,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(sFechaVencimiento_E,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(sFechaProbPago_E,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(sFechaDeposito_E,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(sImporteDeposito_E,2),"formasmen",ComunesPDF.CENTER);
		
		pdfDoc.addTable();
		
		pdfDoc.setTable(12,100);
		pdfDoc.setCell("Detalles","formasmenB",ComunesPDF.LEFT,12);		
		pdfDoc.setCell("Subaplicación","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Préstamo","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Clave SIRAC","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Capital","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Interés","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Moratorios","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Subsidio","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Comisión","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("IVA","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Importe Pago","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Concepto pago","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("Origen pago","formasmenB",ComunesPDF.CENTER);

		dCapital=0; dInteres=0; dMoratorios=0; dSubsidio=0; dComision=0; dIva=0; dImporte=0;
		int tamanio_al=al.size();
		for(int i=0; i < al.size(); i++){
			ArrayList alTemp = new ArrayList();
			alTemp=(ArrayList)al.get(i);
			String sConceptoLocal="";
			String sOrigenLocal="";
			if(alTemp.get(10).equals("AN")){sConceptoLocal="Anticipado";}
			if(alTemp.get(10).equals("VE")){sConceptoLocal="Vencimiento";}
			if(alTemp.get(10).equals("CV")){sConceptoLocal="Cartera Vencida";}
			if(alTemp.get(10).equals("CO")){sConceptoLocal="Comision";}
			if(alTemp.get(11).equals("I")){sOrigenLocal="Intermediario";}
			if(alTemp.get(11).equals("A")){sOrigenLocal="Acreditado";}
			
			pdfDoc.setCell(alTemp.get(0).toString(),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell(alTemp.get(1).toString(),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell(alTemp.get(2).toString(),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoMoneda2((String)alTemp.get(3),false),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoMoneda2((String)alTemp.get(4),false),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoMoneda2((String)alTemp.get(5),false),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoMoneda2((String)alTemp.get(6),false),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoMoneda2((String)alTemp.get(7),false),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoMoneda2((String)alTemp.get(8),false),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoMoneda2((String)alTemp.get(9),false),"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell(sConceptoLocal,"formasmen",ComunesPDF.LEFT);
			pdfDoc.setCell(sOrigenLocal,"formasmen",ComunesPDF.LEFT);
	
			dCapital += Double.parseDouble((String)alTemp.get(3));
			dInteres += Double.parseDouble((String)alTemp.get(4));
			dMoratorios += Double.parseDouble((String)alTemp.get(5));
			dSubsidio += Double.parseDouble((String)alTemp.get(6));
			dComision += Double.parseDouble((String)alTemp.get(7));
			dIva += Double.parseDouble((String)alTemp.get(8));
			dImporte += Double.parseDouble((String)alTemp.get(9));
		}//FIN ROR al
	
		pdfDoc.setCell("Total","formasmenB",ComunesPDF.LEFT);
		pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(dCapital,2),"formasmenB",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(dInteres,2),"formasmenB",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(dMoratorios,2),"formasmenB",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(dSubsidio,2),"formasmenB",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(dComision,2),"formasmenB",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(dIva,2),"formasmenB",ComunesPDF.LEFT);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(dImporte,2),"formasmenB",ComunesPDF.LEFT);
		pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
		pdfDoc.addTable();
		
		String query = " SELECT "   +
					   " 	  TO_CHAR(DF_FECHA_HORA,'DD/MM/YYYY') FECHA"   +
					   " 	  ,TO_CHAR(DF_FECHA_HORA,'HH24:MI') HORA "   +
					   " 	  FROM COM_ACUSE3 "   +
					   " WHERE "   +
					   "       CC_ACUSE=?"  ;	
      System.err.println(folioCert);
		ps = con.queryPrecompilado(query);
		ps.setString(1, folioCert);
		rs = ps.executeQuery();
		String sFecha="", sHora="";
		while (rs.next()) {
	        	sFecha  = (rs.getString(1) == null) ? "" : rs.getString(1).trim();
	        	sHora   = (rs.getString(2) == null) ? "" : rs.getString(2).trim();
		}//while
		rs.close();
		if(ps!=null) ps.close();
		
		pdfDoc.setTable(2,30);
		pdfDoc.setCell("Datos del Acuse","formasmenB",ComunesPDF.CENTER,2);		
		pdfDoc.setCell("Numero de Acuse","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell(folioCert,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell(sFecha,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("Hora","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell(sHora,"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Préstamos","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(tamanio_al),"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","formasmenB",ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+(Comunes.formatoDecimal(dImporte,2)).toString(),"formasmen",ComunesPDF.CENTER);
		
		pdfDoc.addTable();
		
		pdfDoc.endDocument();
		
		con.cierraConexionDB();
	} catch (Exception e) {
		e.printStackTrace();
	}
                                    
   JSONObject jsonObj = new JSONObject();
   jsonObj.put(   "success",     new Boolean(true));
   jsonObj.put(   "id",          idBoton);
   jsonObj.put(   "urlArchivo",   strDirecVirtualTemp+nombreArchivo);
   respuesta = jsonObj.toString();
}

System.out.println(respuesta);
%>

<%=respuesta%>