<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	netropology.utilerias.negocio.*,
	com.netro.mandatodoc.*,	
	netropology.utilerias.*" 
	errorPage="/00utils/error_extjs.jsp"
%>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%!
public static final boolean SIN_COMAS = false;
public static final int MONTO_MN = 0;
public static final int MONTO_INTERES_MN = 1;
public static final int MONTO_RECIBIR_MN = 2;
public static final int MONTO_DL = 3;
public static final int MONTO_INTERES_DL = 4;
public static final int MONTO_RECIBIR_DL = 5;
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")!=null)?request.getParameter("ic_banco_fondeo"):"1";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String ic_pyme = (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
String doctoSeleccionados = (request.getParameter("doctoSeleccionados")!=null)?request.getParameter("doctoSeleccionados"):"";
String monto_descuento_mn = (request.getParameter("monto_descuento_mn")!=null)?request.getParameter("monto_descuento_mn"):"";
String monto_interes_mn = (request.getParameter("monto_interes_mn")!=null)?request.getParameter("monto_interes_mn"):"";
String monto_operar_mn = (request.getParameter("monto_operar_mn")!=null)?request.getParameter("monto_operar_mn"):"";
String monto_descuento_dl = (request.getParameter("monto_descuento_dl")!=null)?request.getParameter("monto_descuento_dl"):"";
String monto_interes_dl = (request.getParameter("monto_interes_dl")!=null)?request.getParameter("monto_interes_dl"):"";
String monto_operar_dl = (request.getParameter("monto_operar_dl")!=null)?request.getParameter("monto_operar_dl"):"";
String mensajeFondeo = (request.getParameter("mensajeFondeo")!=null)?request.getParameter("mensajeFondeo"):"";
String _acuse = (request.getParameter("_acuse")!=null)?request.getParameter("_acuse"):"";
String accion = (request.getParameter("accion")!=null)?request.getParameter("accion"):"";



if(ic_pyme.equals(""))  ic_pyme="TODOS";
String  ic_if =iNoCliente;
String infoRegresar = "",  claveDocumento ="", numSirac ="",  nombrePyme ="", numDocumento ="", numProveedorInterno ="",  nombreEpo ="",
		fechaDocumento ="",  fechaVencimiento ="", claveMoneda ="", nombreMoneda ="", monto ="", importeInteres ="",
		importeRecibir  ="", tasaAceptada  ="", montoDescuento ="",  plazo  ="",  porcentajeAnticipo ="", 
		recursoGarantia ="", 	estatusDocumento ="", fechaAlta ="", referencia ="",  descuentoEspecial ="", 
		claveEstatusDocumento ="",  	beneficiario ="",  bancoBeneficiario  ="",  sucursalBeneficiario ="", cuentaBeneficiario ="",
		porcBeneficiario ="",  importeRecibirBenef ="", netoRecibirPyme ="", tipoFactorajeFijo ="", tipoFactoraje ="",
		numNaeBeneficiario ="",  bOperaFactorajeVencido ="N",  bOperaFactorajeDist ="N", bOperaFactorajeMand ="N", 
		bOperaFactorajeVencInfonavit ="N", bTipoFactoraje ="N",  vhoraEnvOpeIf = "",  datosFondeo = "N|N", fondeoP ="",
		tipoFondeo = "", 	recorteSolic = "", consulta="",  mensajesFondeoPre ="";
boolean horaValidaEnvIf = true;
int numRegistrosMN = 0;
int numRegistrosDL = 0;
BigDecimal montoTotalMN = new BigDecimal("0.00");
BigDecimal montoTotalDL = new BigDecimal("0.00");
double dblRegistros = 0;
double dblTotalMontoPesos = 0;
double dblTotalDescuentoPesos = 0;
double dblTotalOperarPesos = 0;
double dblTotalRecursosPesos = 0;
double dblTotalInteresPesos = 0;
double dblTotalMontoDolares = 0;
double dblTotalDescuentoDolares = 0;
double dblTotalOperarDolares = 0;
double dblTotalRecursosDolares = 0;
double dblTotalInteresDolares = 0;	
HashMap  registrosTot = new HashMap();	
JSONArray registrosTotales = new JSONArray();	
Hashtable alParamEPO = new Hashtable (); 
HashMap datos = new HashMap();
JSONArray registrosC = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();
SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
java.util.Date d1 = null;
java.util.Date d2 = null;
java.util.Date d3 = null;
java.util.Date d4 = null;
java.util.Date d5 = null;
HashMap		documentosAgregados						= null;
boolean 		hayDoctosAgregados 						= false;

CreaArchivo archivo = new CreaArchivo();
String contenidoArchivo="";
String strArchivoVble="";
String strArchivoFijo = "";
String nombreArchivo="";
String strNombreArchivo = "";
String strNombreArchivoFijo = "";
ComunesPDF pdfDoc = new ComunesPDF();


AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
MandatoDocumentos mandatoDocumentos = ServiceLocator.getInstance().lookup("MandatoDocumentosEJB", MandatoDocumentos.class);

boolean hayClaseDocto = false, operaNotasDeCredito =false , aplicarNotasDeCreditoAVariosDoctos =false;
String hayClaseDocto2="NO", operaNotasDeCredito2="N", aplicarNotasDeCreditoAVariosDoctos2 ="N", bOperaFactorajeIF ="N";

CrearArchivosAcuses  generaAch =  new CrearArchivosAcuses();
	
if(!ic_epo.equals(""))  {
	
	nombreEpo  = BeanAutDescuento.descripcionEPO(ic_epo);
	
	HashMap ohDatosFondeo = BeanAutDescuento.getDatosFondeo(iNoCliente, ic_epo, "1");
	if(ohDatosFondeo.size()>0){ 
		datosFondeo = ohDatosFondeo.get("TIPO_FONDEO").toString()+"|"+ohDatosFondeo.get("RECORTE_SOLIC").toString();
	}
		
	if(BeanAutDescuento.hayFondeoPropio(iNoCliente,ic_epo)) {	
		fondeoP="Las siguientes solicitudes de descuento se realizarán con Fondeo propio";
	}
	
	alParamEPO = BeanParamDscto.getParametrosEPO(ic_epo, 1);
	horaValidaEnvIf = BeanParamDscto.validaHoraEnvOpeIFs(ic_epo);
	
	if(!horaValidaEnvIf){
		vhoraEnvOpeIf = alParamEPO.get("HORA_ENV_OPE_IF")==null?"":(String)alParamEPO.get("HORA_ENV_OPE_IF");
	 }		
	if (!ic_epo.equals("")) {		
			bOperaFactorajeVencido = alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString();	
			bOperaFactorajeDist = alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString();
			bOperaFactorajeMand = alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString();
			bOperaFactorajeVencInfonavit = alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString();
			bOperaFactorajeIF = alParamEPO.get("FACTORAJE_IF").toString();
		}
		if( bOperaFactorajeVencido.equals("S") || bOperaFactorajeDist.equals("S") || bOperaFactorajeMand.equals("S") || bOperaFactorajeVencInfonavit.equals("S")  ||  bOperaFactorajeIF.equals("S") ) {
		 bTipoFactoraje ="S";
	  }
		
	hayClaseDocto = BeanAutDescuento.hayClaseDoctoAsociado(ic_epo);
	if(!hayClaseDocto){   hayClaseDocto2 ="NO";  
	 }else  {   	hayClaseDocto2 ="SI";   	}
	

// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
	operaNotasDeCredito                = BeanParamDscto.operaNotasDeCreditoParaDescuentoElectronico(ic_epo);
	if(operaNotasDeCredito) operaNotasDeCredito2="S";
	// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
	aplicarNotasDeCreditoAVariosDoctos = BeanParamDscto.hasAplicarNotaDeCreditoAVariosDoctosEnabled(ic_epo);
	if(aplicarNotasDeCreditoAVariosDoctos) aplicarNotasDeCreditoAVariosDoctos2="S";
	
}


 
if (informacion.equals("CatalogoBancoFondeo")) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_banco_fondeo");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_banco_fondeo");		
	catalogo.setOrden("ic_banco_fondeo");	
	infoRegresar = catalogo.getJSONElementos();	
	
}else if (informacion.equals("CatalogoEPO")  && !ic_banco_fondeo.equals("")) {

	CatalogoEPO cat = new CatalogoEPO();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setBancofondeo(ic_banco_fondeo);
	cat.set_claveIf(ic_if);
	cat.setHabilitado("S");
	cat.setOrden("CE.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
			
}if (informacion.equals("CatalogoPYME") && !ic_epo.equals("")) {

	CatalogoPYMEDes catalogo = new CatalogoPYMEDes();
	catalogo.setCampoClave("p.ic_pyme");
	catalogo.setCampoDescripcion("p.cg_razon_social");
	catalogo.setEstatusDocumentos("3");
	catalogo.setClaveEPO(ic_epo);	
	catalogo.setClaveIF(ic_if);
	catalogo.setOrden("p.cg_razon_social");	
	infoRegresar = catalogo.getJSONElementos();	


}if (informacion.equals("ValidaHorario") && !ic_epo.equals("")) {
	
	String  mensajeHorario ="";
	
	try {
		if (Comunes.esNumero(ic_epo)) {
			//Horario.validarHorario(1, strTipoUsuario, ic_epo, iNoCliente);
			Horario.validarHorarioIF(1, strTipoUsuario, ic_epo, iNoCliente);
		}		
	} catch (NafinException ne) {
		ne.printStackTrace();
		mensajeHorario =ne.getMsgError();
		
	} catch (Exception e) {
		out.println("Error: " + e.getMessage());
		System.out.println("13pki/13if/13forma03Ext.jsp(Exception): " + e.getMessage());
	  e.printStackTrace();
	}		 
	
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("mensajeHorario",mensajeHorario);
	infoRegresar  = jsonObj.toString();	

}
if (informacion.equals("ValidaFondeo") && !ic_epo.equals("")) {
	
	String  mensajeDoctoAplicado= "N", mensajeHorario ="";
	
	if(BeanAutDescuento.hayFondeoPropio(iNoCliente,ic_epo)) {	
		mensajeFondeo="S";		
	}
	
	if (BeanAutDescuento.hayDoctosAplicCred(iNoCliente)) {	
			mensajeDoctoAplicado="S";
	}
	
	try {
		if (Comunes.esNumero(ic_epo)) {
			//Horario.validarHorario(1, strTipoUsuario, ic_epo, iNoCliente);
			Horario.validarHorarioIF(1, strTipoUsuario, ic_epo, iNoCliente);  
		}		
	} catch (NafinException ne) {
		ne.printStackTrace();
		mensajeHorario =ne.getMsgError();
		
	} catch (Exception e) {
		out.println("Error: " + e.getMessage());
		System.out.println("13pki/13if/13forma03Ext.jsp(Exception): " + e.getMessage());
	  e.printStackTrace();
	}		
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("mensajeFondeo",mensajeFondeo);	
	jsonObj.put("mensajeDoctoAplicado",mensajeDoctoAplicado);
	jsonObj.put("vhoraEnvOpeIf",vhoraEnvOpeIf);	
	jsonObj.put("mensajeHorario",mensajeHorario);
	infoRegresar  = jsonObj.toString();	

}if (informacion.equals("Consultar")   || informacion.equals("ConsultarTotales")  ) {

		Vector registros;
		String existe_mandato2 ="N", mensaje ="", montoDi ="", mensaje_dl ="", montoDi_dl ="0";
		int numRegistros =0;
		StringBuffer claveDoctoTodos = new StringBuffer();
				
	try {
		
		registros = BeanAutDescuento.getDoctosSelecPyme(ic_pyme,ic_epo,iNoCliente,"");
		numRegistros = registros.size();
		boolean existe_mandato = mandatoDocumentos.existeMandatoDocumentos(registros);//FODEA 041 - 2009 ACF
		if(existe_mandato)  existe_mandato2="S";
		HashMap  validaLimiteEpo = BeanAutDescuento.validaLimiteEpo(ic_epo, iNoCliente);
		mensaje  = validaLimiteEpo.get("MENSAJE").toString();
		montoDi  = validaLimiteEpo.get("MONTO_DIFERIENCIA").toString().replace('-',' ');		
		
		mensaje_dl  = validaLimiteEpo.get("MENSAJE_DL").toString();		
		montoDi_dl  = validaLimiteEpo.get("MONTO_DIFERIENCIA_DL").toString().replace('-',' ');
		
		for (int i = 0; i < numRegistros; i++) {
			Vector registro = (Vector) registros.get(i);
			claveDocumento = registro.get(0).toString();
			numSirac = registro.get(1).toString();
			nombrePyme = registro.get(2).toString();
			numDocumento = registro.get(3).toString();
			numProveedorInterno = registro.get(4).toString();
			nombreEpo = registro.get(5).toString();
			fechaDocumento = registro.get(6).toString();
			fechaVencimiento = registro.get(7).toString();
			claveMoneda = registro.get(8).toString();
			nombreMoneda = registro.get(9).toString();
			monto = registro.get(10).toString();
			importeInteres = registro.get(11).toString();
			importeRecibir = registro.get(12).toString();
			tasaAceptada = registro.get(13).toString();
			montoDescuento = registro.get(14).toString();
			plazo = registro.get(15).toString();
			porcentajeAnticipo = registro.get(16).toString();
			recursoGarantia = registro.get(17).toString();
			estatusDocumento = registro.get(18).toString();	
			fechaAlta = registro.get(19).toString();
			referencia = registro.get(20).toString();
			descuentoEspecial = registro.get(21).toString();
			claveEstatusDocumento = registro.get(22).toString();
			beneficiario = registro.get(23).toString();
			bancoBeneficiario = registro.get(24).toString();
			sucursalBeneficiario = registro.get(25).toString();
			cuentaBeneficiario = registro.get(26).toString();
			porcBeneficiario = registro.get(27).toString();
			importeRecibirBenef = registro.get(28).toString();
			netoRecibirPyme = registro.get(29).toString();			
			tipoFactorajeFijo = registro.get(21).toString();
			tipoFactoraje = registro.get(30).toString();
			numNaeBeneficiario = registro.get(31).toString(); 	
			String colorfuente = "N";
			String msgCausa1 ="";
			String msgCausa2 ="";
			String msgCausa3 ="";//FODEA 041 - 2009 ACF
			java.util.Date d6 = formatter.parse(fechaVencimiento);
	
			if(d1 != null){
				if(d6.getTime()>=d1.getTime() ){
					colorfuente="S";
					msgCausa1 = "Vencimiento Línea de Crédito";
				}
			}
			if(d3 != null){
				if(d6.getTime()>=d3.getTime()){
					colorfuente="S";
					if(!msgCausa1.equals(""))
						msgCausa2 = " / Cambio de Administración";
					else
						msgCausa2 = "Cambio de Administración";
				}
			}
			String  observaciones1 =!msgCausa1.equals("")?msgCausa1:"";
			String  observaciones2 =!msgCausa2.equals("")?msgCausa2:"";
			String  observaciones3 =!msgCausa3.equals("")?msgCausa3:"";
			String  observaciones= observaciones1 +observaciones2 +observaciones3;


			if (claveMoneda.equals("1")) {	//M.N.
				numRegistrosMN++;
				if (!importeRecibir.equals("")) {
					montoTotalMN = montoTotalMN.add(new BigDecimal(importeRecibir));
				}

				dblTotalMontoPesos += new Double(monto).doubleValue();
				dblTotalDescuentoPesos += new Double(montoDescuento).doubleValue();
				dblTotalOperarPesos += new Double(importeRecibir).doubleValue();

				dblTotalRecursosPesos += new Double(recursoGarantia).doubleValue();
				dblTotalInteresPesos += new Double(importeInteres).doubleValue();
			} else if (claveMoneda.equals("54")) {	//DLS
				numRegistrosDL++;
				if (!importeRecibir.equals("")) {
					montoTotalDL = montoTotalDL.add(new BigDecimal(importeRecibir));
				}

				dblTotalMontoDolares += new Double(monto).doubleValue();
				dblTotalDescuentoDolares += new Double(montoDescuento).doubleValue();
				dblTotalOperarDolares += new Double(importeRecibir).doubleValue();

				dblTotalRecursosDolares += new Double(recursoGarantia).doubleValue();
				dblTotalInteresDolares += new Double(importeInteres).doubleValue();
			}
			 
			datos = new HashMap();
			datos.put("COLOR_FUENTE", colorfuente);
			datos.put("NOMBRE_EPO", nombreEpo);
			datos.put("NU_SIRAC", numSirac);
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUMER0_DOCTO", numDocumento);
			datos.put("FECHA_EMISION", fechaDocumento);
			datos.put("FECHA_VENCIMIENTO", fechaVencimiento);
			datos.put("IC_MONEDA", claveMoneda);
			datos.put("MONEDA", nombreMoneda);
			datos.put("TIPO_FACTORAJE", tipoFactoraje);
			datos.put("MONTO", monto);
			datos.put("PORCE_DESCUENTO",porcentajeAnticipo);
			datos.put("RECURSO_GARANTIA", recursoGarantia);
			datos.put("MONTO_DESCUENTO", montoDescuento);
			datos.put("MONTO_INTERES", importeInteres);
			datos.put("MONTO_OPERAR", importeRecibir);
			datos.put("TASA", tasaAceptada);
			datos.put("PLAZO", plazo);
			datos.put("NUM_PROVEEDOR", numProveedorInterno);
			datos.put("BENEFICIARIO", beneficiario);
			datos.put("BANCO_BENEFICIARIO", bancoBeneficiario);
			datos.put("SUCURSAL_BENEFICIARIO", sucursalBeneficiario);
			datos.put("CUENTA_BENEFICIARIO", cuentaBeneficiario);
			datos.put("PORC_BENEFICIARIO", porcBeneficiario);
			datos.put("IMPORTE_RECIBIR_BENE", importeRecibirBenef);
			datos.put("NETO_RECIBIR", netoRecibirPyme);
			datos.put("OBSERVACION", observaciones);
			datos.put("SELECCIONAR", "S");		  	
			datos.put("CLAVE_DOCUMENTO", claveDocumento);
			datos.put("MENSAJE", mensaje);
			datos.put("MENSAJE_DL", mensaje_dl);
			registrosC.add(datos);	
						
			if( !mensaje.equals("SobreGirado")   &&   claveMoneda.equals("1")) {
				claveDoctoTodos.append(claveDocumento+",");
			}
			if( !mensaje_dl.equals("SobreGirado")   &&   claveMoneda.equals("54")) {
				claveDoctoTodos.append(claveDocumento+",");
			}
		}
		if(!claveDoctoTodos.toString().equals(""))  {
			claveDoctoTodos.deleteCharAt(claveDoctoTodos.length()-1);
		}
		
		for(int t =0; t<2; t++) {		
			registrosTot = new HashMap();		
			if(t==0){ 					
				registrosTot.put("IC_MONEDA", "1");
				registrosTot.put("MONEDA", "Nacional");		
				registrosTot.put("NUM_DOCTOS_AUTO", "0" );	
				registrosTot.put("TOTAL_MONTOS_AUTO", "0");				
				if( !mensaje.equals("SobreGirado")  ) {
					registrosTot.put("NUM_DOCTOS_AUTO", "0" );	
					registrosTot.put("TOTAL_MONTOS_AUTO", "0");	
					registrosTot.put("NUM_DOCTOS_SELEC", String.valueOf(numRegistrosMN));
					registrosTot.put("TOTAL_MONTOS_SELEC", montoTotalMN.toString());		
				}else  {
					registrosTot.put("NUM_DOCTOS_AUTO", String.valueOf(numRegistrosMN));
					registrosTot.put("TOTAL_MONTOS_AUTO", montoTotalMN.toString());	
					registrosTot.put("NUM_DOCTOS_SELEC", "0");
					registrosTot.put("TOTAL_MONTOS_SELEC", "0");	
				}
			}		
			if(t==1){ 				
				registrosTot.put("IC_MONEDA", "54");
				registrosTot.put("MONEDA", "Dólares ");		
				registrosTot.put("NUM_DOCTOS_AUTO", "0" );	
				registrosTot.put("TOTAL_MONTOS_AUTO", "0");				
				if( !mensaje_dl.equals("SobreGirado")  ) {
					registrosTot.put("NUM_DOCTOS_AUTO", "0" );	
					registrosTot.put("TOTAL_MONTOS_AUTO", "0");
					registrosTot.put("NUM_DOCTOS_SELEC",  String.valueOf(numRegistrosDL) );
					registrosTot.put("TOTAL_MONTOS_SELEC", montoTotalDL.toPlainString());							
				}else  {
					registrosTot.put("NUM_DOCTOS_AUTO",  String.valueOf(numRegistrosDL) );
					registrosTot.put("TOTAL_MONTOS_AUTO", montoTotalDL.toPlainString());		
					registrosTot.put("NUM_DOCTOS_SELEC",  "0" );
					registrosTot.put("TOTAL_MONTOS_SELEC", "0");	
					
				}
			}		
			registrosTotales.add(registrosTot);
		}
		
	} catch (NafinException ne) {
		mensaje = ne.getMsgError();	
	}
	
	if (informacion.equals("Consultar")  ) {	
		consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registrosC.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		jsonObj.put("existe_mandato",existe_mandato2);
		jsonObj.put("bOperaFactorajeVencido",bOperaFactorajeVencido);
		jsonObj.put("bOperaFactorajeDist",bOperaFactorajeDist);
		jsonObj.put("bOperaFactorajeMand",bOperaFactorajeMand);
		jsonObj.put("bOperaFactorajeVencInfonavit",bOperaFactorajeVencInfonavit);
		jsonObj.put("bTipoFactoraje",bTipoFactoraje);	
		jsonObj.put("mensaje",mensaje);
		jsonObj.put("nombreEPO",nombreEpo);
		jsonObj.put("montoDi",Comunes.formatoDecimal(montoDi,2) );	
		jsonObj.put("mensaje_dl",mensaje_dl);
		jsonObj.put("montoDi_dl",Comunes.formatoDecimal(montoDi_dl,2) );	
		jsonObj.put("hayClaseDocto",hayClaseDocto2);	
		jsonObj.put("numRegistros",String.valueOf(numRegistros));	
		jsonObj.put("claveDoctoTodos",claveDoctoTodos.toString());
		
	}
	
	
	
	if( informacion.equals("ConsultarTotales")  ) {
		consulta =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
	}
	
	infoRegresar  = jsonObj.toString();	
	
}if (informacion.equals("ConsultarPreAcuse")  ||  informacion.equals("ConsultarPreAcuse2") 
	||  informacion.equals("ConsultarTotalesPre")   ||  informacion.equals("consultaTotales2")  
	||  informacion.equals("consultaTotales1")  ) {  

	String  hayDoctosAgregados2 ="N";
	double importeMN = 0, importeDol = 0;
	int iTotMN = 0, iTotDol = 0;
	Hashtable hPlazoBO = new Hashtable();
	String sPlazoBO = "",   msg ="";
	
	String[] seleccionados;
	String delimiter = ",";
	seleccionados = doctoSeleccionados.split(delimiter);
	 	
	if(operaNotasDeCredito2.equals("S") && aplicarNotasDeCreditoAVariosDoctos2.equals("S") ){	
		ArrayList lista = BeanAutDescuento.actualizaDoctosSeleccionados(seleccionados);	
		seleccionados			= (String []) lista.get(0);
		documentosAgregados	= (HashMap) lista.get(1);
		hayDoctosAgregados 	= ((Boolean) lista.get(2)).booleanValue(); 
	}
	if(hayDoctosAgregados) hayDoctosAgregados2="S";
	tipoFondeo = datosFondeo.substring(0,1);
	recorteSolic = datosFondeo.substring(2,3);
	
	// Valida la Base de Operación Obtenida con la Moneda.
	
	int iMN = Integer.parseInt(request.getParameter("numeroDoctoMN"));
	int iDol = Integer.parseInt(request.getParameter("numeroDoctoDL"));
	int iNoMonedas[] = new int[2];
	if(iMN != 0) {
		iNoMonedas[0] = 1;
	}
	if(iDol != 0) {
		if(iMN == 0)
			iNoMonedas[0] = 54;
		else
			iNoMonedas[1] = 54;
	}
		
	//Cambio Foda 046 - 2004 EGB
	
	if(tipoFondeo.equals("N") || (tipoFondeo.equals("P") && recorteSolic.equals("S"))) {
	
		hPlazoBO = BeanAutDescuento.getPlazoBO(ic_epo, iNoCliente, iNoMonedas);
		sPlazoBO = hPlazoBO.get("sPlazoBO").toString();
		boolean bTienePlazoMN = false, bTienePlazoDol = false;
		if(!sPlazoBO.equals("")) {
			VectorTokenizer vt = new VectorTokenizer(sPlazoBO,":");
			Vector vdatos = vt.getValuesVector();
			String sReg = "";	int iMoneda = 0;
			for(int i=0; i<(vdatos.size()-1); i++) {
				sReg = vdatos.get(i).toString();
				iMoneda = Integer.parseInt(sReg.substring(0, sReg.indexOf("|")));
				if(iMoneda==1)
					bTienePlazoMN = true;
				if(iMoneda==54)
					bTienePlazoDol = true;
			}
		}
		if(sPlazoBO.equals("") || (iMN!=0 && !bTienePlazoMN) || (iDol!=0 && !bTienePlazoDol) ) {	
			 msg = hPlazoBO.get("sMsgError").toString();
		}
	}

	
	// para el Grid de la Primer Consulta
	Vector registros = new Vector();
	registros = BeanAutDescuento.getDoctosProcesar(seleccionados, iNoCliente, ic_epo);
	int numRegistros = registros.size();
		
	Vector vFechas = BeanAutDescuento.getFechasConvenioyDescuento(iNoCliente, "1");
	
	String nombreFacultado= "<span style='color:blue;'><b>Facultado por  "+strNombre+"<b></span>";
	Vector vNombrePuesto = BeanAutDescuento.getNombreyPuesto(iNoCliente);
	String descripFacultado = "<span style='color:blue;'><b>"+(vNombrePuesto.size()==0?"":vNombrePuesto.get(1))+"   "+(vNombrePuesto.size()==0?"":vNombrePuesto.get(0))+"<b></span>";
	    
		
	if(informacion.equals("ConsultarPreAcuse")){
		
		for (int i = 0; i < numRegistros; i++) {
					
			Vector registro = (Vector) registros.get(i);
			claveDocumento = registro.get(0).toString();
			numSirac = registro.get(1).toString();
			nombrePyme = registro.get(2).toString();
			numDocumento = registro.get(3).toString();
			fechaDocumento = registro.get(4).toString();
			fechaVencimiento = registro.get(5).toString();
			claveMoneda = registro.get(6).toString();
			nombreMoneda = registro.get(7).toString();
			monto = registro.get(8).toString();
			montoDescuento = registro.get(9).toString();
			porcentajeAnticipo = registro.get(10).toString();
			recursoGarantia = registro.get(11).toString();
			importeInteres = registro.get(12).toString();
			importeRecibir = registro.get(13).toString();
			tasaAceptada = registro.get(14).toString();
			plazo = registro.get(15).toString();
			numProveedorInterno = registro.get(16).toString();
			descuentoEspecial = registro.get(17).toString();
			beneficiario = registro.get(18).toString();
			bancoBeneficiario = registro.get(19).toString();
			sucursalBeneficiario = registro.get(20).toString();
			cuentaBeneficiario = registro.get(21).toString();
			porcBeneficiario = registro.get(22).toString();
			importeRecibirBenef = registro.get(23).toString();
			netoRecibirPyme = registro.get(24).toString();			
			tipoFactoraje = registro.get(26).toString();
			referencia = registro.get(27).toString();
			if(i==0){
				if (numSirac.equals("99999")){
				nombreFacultado ="<span style='color:blue;'><b>Facultado por  "+nombrePyme+"<b></span>";
				}
			}	
			if(claveMoneda.equals("1")) {
				numRegistrosDL++;
				iTotMN++;
				importeMN += new Double(registro.get(13).toString()).doubleValue();
			} else if(claveMoneda.equals("54")) {
				numRegistrosMN++;
				iTotDol++;
				importeDol += new Double(registro.get(13).toString()).doubleValue();
			}
			String color = "N";
			if( operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos && esUnDocumentoAgregado(claveDocumento,documentosAgregados)){
				color = "S";
			}
			
			datos = new HashMap();
			datos.put("COLOR_FUENTE", "");
			datos.put("NOMBRE_EPO", nombreEpo);
			datos.put("NU_SIRAC", numSirac);
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUMER0_DOCTO", numDocumento);
			datos.put("FECHA_EMISION", fechaDocumento);
			datos.put("FECHA_VENCIMIENTO", fechaVencimiento);
			datos.put("MONEDA", nombreMoneda);
			datos.put("TIPO_FACTORAJE", tipoFactoraje);
			datos.put("MONTO", monto);
			datos.put("PORCE_DESCUENTO",porcentajeAnticipo);
			datos.put("RECURSO_GARANTIA", recursoGarantia);
			datos.put("MONTO_DESCUENTO", montoDescuento);
			datos.put("MONTO_INTERES", importeInteres);
			datos.put("MONTO_OPERAR", importeRecibir);
			datos.put("TASA", tasaAceptada);
			datos.put("PLAZO", plazo);			
			datos.put("NUM_PROVEEDOR", numProveedorInterno);
			datos.put("BENEFICIARIO", beneficiario);
			datos.put("BANCO_BENEFICIARIO", bancoBeneficiario);
			datos.put("SUCURSAL_BENEFICIARIO", sucursalBeneficiario);
			datos.put("CUENTA_BENEFICIARIO", cuentaBeneficiario);
			datos.put("PORC_BENEFICIARIO", porcBeneficiario);
			datos.put("IMPORTE_RECIBIR_BENE", importeRecibirBenef);
			datos.put("NETO_RECIBIR", netoRecibirPyme);			
			datos.put("SELECCIONAR", "N");
			datos.put("CLAVE_DOCUMENTO", claveDocumento);
			datos.put("REFERENCIA",referencia);
			registrosC.add(datos);		
		}//for
		
		if("1".equals(ic_banco_fondeo)) {
		mensajesFondeoPre=mensaje_param.getMensaje("I13forma3.NAFIN.Leyenda02", sesIdiomaUsuario)+strNombre+
				mensaje_param.getMensaje("I13forma3.NAFIN.Leyenda12", sesIdiomaUsuario)+iTotMN+
				mensaje_param.getMensaje("I13forma3.NAFIN.Leyenda22", sesIdiomaUsuario)+Comunes.formatoDecimal(importeMN,2,true)+" y "+ iTotDol + mensaje_param.getMensaje("I13forma3.NAFIN.Leyenda32", sesIdiomaUsuario)+Comunes.formatoDecimal(importeDol,2,true) + mensaje_param.getMensaje("I13forma3.NAFIN.Leyenda42", sesIdiomaUsuario);
		
		} else if("2".equals(ic_banco_fondeo)) {
			mensajesFondeoPre=mensaje_param.getMensaje("I13forma3.BANCOMEXT.Leyenda03", sesIdiomaUsuario)+strNombre+
					mensaje_param.getMensaje("I13forma3.BANCOMEXT.Leyenda13", sesIdiomaUsuario)+iTotMN+
					mensaje_param.getMensaje("I13forma3.BANCOMEXT.Leyenda23", sesIdiomaUsuario)+Comunes.formatoDecimal(importeMN,2,true)+" y "+ iTotDol + mensaje_param.getMensaje("I13forma3.BANCOMEXT.Leyenda33", sesIdiomaUsuario)+Comunes.formatoDecimal(importeDol,2,true) + mensaje_param.getMensaje("I13forma3.BANCOMEXT.Leyenda43", sesIdiomaUsuario);
		}
		consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registrosC.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
		jsonObj.put("bOperaFactorajeVencido",bOperaFactorajeVencido);
		jsonObj.put("bOperaFactorajeDist",bOperaFactorajeDist);
		jsonObj.put("bOperaFactorajeMand",bOperaFactorajeMand);
		jsonObj.put("bOperaFactorajeVencInfonavit",bOperaFactorajeVencInfonavit);
		jsonObj.put("bTipoFactoraje",bTipoFactoraje);	
		jsonObj.put("hayDoctosAgregados",hayDoctosAgregados2);			
		jsonObj.put("mensajesFondeoPre",mensajesFondeoPre);
		jsonObj.put("nombreInter1",strNombre);	
		jsonObj.put("fechaConvenio1",vFechas.get(0).toString());	
		jsonObj.put("fechaDesc1","México D.F."+vFechas.get(1).toString());	
		jsonObj.put("nota1","La Fecha de Operación es Informativa.");			
		jsonObj.put("nombreFacultado",nombreFacultado);	
		jsonObj.put("descripFacultado",descripFacultado);
		jsonObj.put("msg",msg);
		jsonObj.put("sPlazoBO",sPlazoBO);
	}


	if(informacion.equals("consultaTotales1")){
		for (int i = 0; i < numRegistros; i++) {
			Vector registro = (Vector) registros.get(i);	
			if(registro.get(6).toString().equals("1")) {
				numRegistrosMN ++;
			} else {
				numRegistrosDL++;
			}
		}		
		
		for(int t =0; t<2; t++) {		
			registrosTot = new HashMap();		
			if(t==0 && numRegistrosMN>0){ 	
				registrosTot.put("IC_MONEDA", "1");	
				registrosTot.put("MONEDA", "Nacional");		
				registrosTot.put("TOTAL_DOCTO",  String.valueOf(numRegistrosMN) );												
			}		
			if(t==1 && numRegistrosDL>0  ){ 
				registrosTot.put("IC_MONEDA", "54");	
				registrosTot.put("MONEDA", "Dólares");		
				registrosTot.put("TOTAL_DOCTO", String.valueOf(numRegistrosDL) );										
			}		
			registrosTotales.add(registrosTot);
		}
			
		consulta =  "{\"success\": true, \"total\": \"" + 2 + "\", \"registros\": " + registrosTotales.toString()+"}";
		
		jsonObj = JSONObject.fromObject(consulta);
	}
	
		
	if(informacion.equals("ConsultarPreAcuse2")){
		for (int i = 0; i < numRegistros; i++) {
			Vector registro = (Vector) registros.get(i);		
			String nombreEmisior = registro.get(25).toString();		
			datos = new HashMap();
			datos.put("NOMBRE_EMISIOR", registro.get(25).toString());
			datos.put("NOMBRE_EMPRESA", registro.get(2).toString());
			datos.put("DESCRIPCION_DOCTO", "Factura");
			datos.put("LUGAR_SUSCRIPCION", "Mexico, D.F.");
			datos.put("FECHA_SUSCRIPCION", registro.get(4).toString());
			datos.put("IMPORTE_DOCTO", registro.get(8).toString());
			datos.put("IMPORTE_DESC", registro.get(13).toString());		
			datos.put("PERIODICIDAD", "Al Vto.");
			datos.put("FECHA_VTO_DES",registro.get(5).toString() );
			datos.put("TASA_DESCUENTO", registro.get(14).toString());
			datos.put("MONEDA", registro.get(7).toString());	
			registrosC.add(datos);	
		}
		consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registrosC.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
	}
	
	if(informacion.equals("consultaTotales2")){
		double impDoctoMN = 0, impDsctoMN = 0, impDoctoDol = 0, impDsctoDol = 0;
		
		for (int i = 0; i < numRegistros; i++) {
			Vector registro = (Vector) registros.get(i);	
			if(registro.get(6).toString().equals("1")) {
				numRegistrosMN ++;
				impDoctoMN += new Double(registro.get(8).toString()).doubleValue();
				impDsctoMN += new Double(registro.get(13).toString()).doubleValue();
			} else {
				numRegistrosDL++;
				impDoctoDol += new Double(registro.get(8).toString()).doubleValue();
				impDsctoDol += new Double(registro.get(13).toString()).doubleValue();
			}
		}
		
	
		
		for(int x =0; x<2; x++) {		
			registrosTot = new HashMap();				
			if(x==0 && numRegistrosMN>0){ 			
				registrosTot.put("MONEDA", "Nacional");		
				registrosTot.put("TOTAL_DOCTO",  String.valueOf(numRegistrosMN) );	
				registrosTot.put("TOTAL_IMPORTE_DOCTO", Double.toString (impDoctoMN));
				registrosTot.put("TOTAL_IMPORTE_DESC", Double.toString (impDsctoMN) );						
			}		
			if(x==1 && numRegistrosDL>0  ){ 				
				registrosTot.put("MONEDA", "Dólares");		
				registrosTot.put("TOTAL_DOCTO", String.valueOf(numRegistrosDL) );	
				registrosTot.put("TOTAL_IMPORTE_DOCTO", Double.toString (impDoctoDol));
				registrosTot.put("TOTAL_IMPORTE_DESC", Double.toString (impDsctoDol) );					
			}		
			registrosTotales.add(registrosTot);
		}
			
		consulta =  "{\"success\": true, \"total\": \"" + 2 + "\", \"registros\": " + registrosTotales.toString()+"}";
		
		jsonObj = JSONObject.fromObject(consulta);
	}
	if(informacion.equals("ConsultarTotalesPre")){ 
		String [] montosTotales;
		montosTotales = BeanAutDescuento.getTotalesDoctosProcesar (seleccionados, ic_epo);		
		datos = new HashMap();	
		datos.put("EPO", ic_epo+"  "+nombreEpo);
		datos.put("MONTO_DESCUENTO_MN", montosTotales[MONTO_MN]);
		datos.put("MONTO_INTERES_MN", montosTotales[MONTO_INTERES_MN]);
		datos.put("MONTO_OPERAR_MN", montosTotales[MONTO_RECIBIR_MN]);		
		datos.put("MONTO_DESCUENTO_DL", montosTotales[MONTO_DL]);
		datos.put("MONTO_INTERES_DL", montosTotales[MONTO_INTERES_DL]);		
		datos.put("MONTO_OPERAR_DL", montosTotales[MONTO_RECIBIR_DL]);		
		registrosC.add(datos);	
		consulta =  "{\"success\": true, \"total\": \"" + 1 + "\", \"registros\": " + registrosC.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
			
	}
	jsonObj.put("msg",msg);
	infoRegresar=jsonObj.toString();

} else  if(informacion.equals("consultaDataNotasDocumentos") || informacion.equals("ArchivoNotasDocumentos")  ){ 

	ArrayList lista = null;
	String		doctoAnt			= "";
	String 	searchNotaAplicada  				= (request.getParameter("searchNotaAplicada")	== null)?"":request.getParameter("searchNotaAplicada");
	String 	icNotaCredito  					= (request.getParameter("icNotaCredito")			== null)?"":request.getParameter("icNotaCredito");
	boolean 	buscarNotaDeCreditoAplicada 	= searchNotaAplicada.equals("true")?true:false;
	String 	searchNotasAplicadasADocto		= (request.getParameter("searchNotasDocto")		== null)?"":request.getParameter("searchNotasDocto");
	String 	icDocumento							= (request.getParameter("icDocumento")				== null)?"":request.getParameter("icDocumento");
	boolean 	buscarNotasAplicadasADocto		= searchNotasAplicadasADocto.equals("true")?true:false;
	String 	searchNotasAplicadasADoctos	= (request.getParameter("searchNotasDoctos")		== null)?"":request.getParameter("searchNotasDoctos");
	String 	icDocumentos[]						= (request.getParameterValues("claveDocumento")	== null)?null:request.getParameterValues("claveDocumento");
	boolean 	buscarNotasAplicadasADoctos	= searchNotasAplicadasADoctos.equals("true")?true:false;
	// CONSULTAR DETALLE NOTA DE CREDITO QUE YA HA SIDO APLICADA
	if(buscarNotaDeCreditoAplicada){
		lista = (ArrayList)getDetalleNotaAplicada(icNotaCredito);
   // CONSULTAR QUE NOTAS SE APLICARON AL DOCUMENTO
	}else if(buscarNotasAplicadasADocto){
		// Obtener notas de credito donde aparece el documento
		ArrayList listaIdsNotasDeCredito = (ArrayList) getNotasDeCredito(icDocumento);
		// Obtener lista con las notas de credito a mostrar
		lista = (ArrayList) getDetalleNotaAplicada(listaIdsNotasDeCredito);
	// CONSULTAR QUE NOTAS SE APLICARON A LOS DOCUMENTOS
	}else if(buscarNotasAplicadasADoctos){
		// Obtener notas de credito donde aparece el documento
		ArrayList listaIdsNotasDeCredito = (ArrayList) getNotasDeCredito(icDocumentos);
		// Obtener lista con las notas de credito a mostrar
		lista = (ArrayList) getDetalleNotaAplicada(listaIdsNotasDeCredito);
	// TOMAR DETALLE DE SESION DE LAS NOTAS DE CREDITO QUE SERAN APLICADAS
	}else{
		lista = (ArrayList)	session.getAttribute("listaNotasCreditoMultiple");
		lista = lista != null?lista:new ArrayList();
	}

	// 1. OBTENER LISTA DE LAS NOTAS DE CREDITO 
	HashSet listaClavesNotas = new HashSet();
	for(int k=0;k<lista.size();k++){
		NotaCreditoMultiple n = (NotaCreditoMultiple)lista.get(k);
		listaClavesNotas.add(n.getIcNotaCredito());
	}
	// Construir lista con las claves de las Notas de Credito a consultar
	String listaNotasDeCredito = "";
	Iterator it = listaClavesNotas.iterator();
	for (int k = 0; it.hasNext(); k++) {
		if(k>0){
			listaNotasDeCredito += ",";
		}
		listaNotasDeCredito += it.next();
	}
	
	// 2. OBTENER LISTA DE LOS DOCUMENTOS 
	HashSet listaClavesDocumentos = new HashSet();
	for(int k=0;k<lista.size();k++){
		NotaCreditoMultiple n = (NotaCreditoMultiple)lista.get(k);
		listaClavesDocumentos.add(n.getIcDocumento());
	}
	// Construir lista con las claves de los Documentos a consultar
	String listaDocumentos = "";
	it = listaClavesDocumentos.iterator();
	for (int k = 0; it.hasNext(); k++) {
		if(k>0){
			listaDocumentos += ",";
		}
		listaDocumentos += it.next();
	}
	// 3. OBTENER MONTOS DE LAS NOTAS DE CREDITO 
	HashMap notasCredito = getDatosNotasCredito(listaClavesNotas);
   // 4. OBTENER DATOS ADICIONALES DE LOS DOCUMENTOS 	
	HashMap documentos 	= getDatosDocumentos(listaClavesDocumentos);
	int  numRegistros = 0;
	
	
	 if(informacion.equals("ArchivoNotasDocumentos") ){
	 
		nombreArchivo = archivo.nombreArchivo()+".pdf";	
		pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
		 pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
										 session.getAttribute("iNoNafinElectronico").toString(),
										 (String)session.getAttribute("sesExterno"),
										 (String) session.getAttribute("strNombre"),
										 (String) session.getAttribute("strNombreUsuario"),
										 (String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
			
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
	
		pdfDoc.setTable(7,70);		
		
	 }
	for(int i=0;i<lista.size();i++){
	   numRegistros++;
		NotaCreditoMultiple n = (NotaCreditoMultiple)lista.get(i);
		String igNota				= getPropiedad(n.getIcNotaCredito(),"IG_NUMERO_DOCTO",notasCredito);
		String igDocto				= getPropiedad(n.getIcDocumento(),"IG_NUMERO_DOCTO",documentos);
		String fechaEmision		= getPropiedad(n.getIcDocumento(),"FECHA_EMISION",documentos);
		double montoNotaCredito = Double.parseDouble(getPropiedad(n.getIcNotaCredito(),"FN_MONTO",notasCredito));
		String moneda				= getPropiedad(n.getIcDocumento(),"MONEDA",documentos);
		String tipoFactoraje2		= getPropiedad(n.getIcDocumento(),"TIPO_FACTORAJE",documentos);	
		double montoAplicado		= (new BigDecimal(n.getMontoNotaAplicado())).doubleValue();
		double saldoDocto			= (new BigDecimal(n.getSaldoDocto())).doubleValue();
		double montoDocto			= montoAplicado + saldoDocto; 
	
		datos = new HashMap();		
		datos.put("NUM_DOCTO", igDocto);
		datos.put("FECHA_EMISION", fechaEmision);
		datos.put("MONEDA",moneda);		
		datos.put("MONTO", Double.toString (montoDocto)  );
		datos.put("MONTO_APLICADO", Double.toString (montoAplicado) );		
		datos.put("SALDO_DOCTO", Double.toString (saldoDocto));		
		datos.put("TIPO_FACTORAJE", tipoFactoraje2);	
		registrosC.add(datos);		

		if(informacion.equals("ArchivoNotasDocumentos") ){ 
			doctoAnt = n.getIcNotaCredito();
			if (!doctoAnt.equals(n.getIcNotaCredito())){			
			
			pdfDoc.setCell("No. Nota.","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell(igNota,"celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell(Comunes.formatoDecimal(montoNotaCredito,2),"celda01",ComunesPDF.CENTER,4);	
			pdfDoc.setCell("No. Docto.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Emisión","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Docto","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Aplicado","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Saldo Docto","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
		}
		
		
			pdfDoc.setCell(igDocto,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(Double.toString (montoDocto),2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(Double.toString (montoAplicado),2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(Double.toString (saldoDocto),2),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(tipoFactoraje2,"formas",ComunesPDF.CENTER);		
		}
	}//  for
	if(informacion.equals("ArchivoNotasDocumentos") ){ 
		pdfDoc.addTable();
		pdfDoc.endDocument();	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
	if(informacion.equals("consultaDataNotasDocumentos")){
		consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registrosC.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		
	}
	infoRegresar=jsonObj.toString();	
	
} else  if(informacion.equals("GenerarAcuse")){ 

	
	String pkcs7 = (request.getParameter("pkcs7")==null)?"":request.getParameter("pkcs7");	
	pkcs7 = (request.getHeader("User-Agent").indexOf("MSIE") != -1)?pkcs7:pkcs7.replaceAll("\\r","");
	String externContent = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
	externContent = (request.getHeader("User-Agent").indexOf("MSIE") != -1)?externContent:externContent.replaceAll("\\r","");
	String sPlazoBO = (request.getParameter("sPlazoBO")!=null)?request.getParameter("sPlazoBO"):"";	
	
	char getReceipt = 'Y';
	Acuse acuse;
	String fechaCarga="", horaCarga="", mensajeAutentificacion="",  folioCert = "",	nombreArchivoPDF = "", strNombreArchivoCSV  ="";
	
	
	Hashtable hProceSolicitud = new Hashtable (); 
	
	if (!ic_epo.equals(""))
		Horario.validarHorario(1, strTipoUsuario, ic_epo, iNoCliente);
		
		acuse= new Acuse(Acuse.ACUSE_IF, "1");
		
		if (!_serial.equals("") && externContent!=null && pkcs7!=null ) {
			folioCert = acuse.toString();
			Seguridad s = new Seguridad();
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
				_acuse = s.getAcuse();
				
				fechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
				horaCarga = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
				String horaCargaF = (new SimpleDateFormat ("kk:mm:ss")).format(new java.util.Date());				
				
				hProceSolicitud = BeanAutDescuento.generarSolicitudInterna(Comunes.explode(",",doctoSeleccionados)  , acuse.toString(),
																					ic_epo, nombreEpo, monto_descuento_mn, monto_interes_mn,
																					monto_operar_mn, monto_descuento_dl, monto_interes_dl, 
																					monto_operar_dl, iNoUsuario, _acuse, datosFondeo, iNoCliente,
																					fechaCarga, horaCarga, horaCargaF, strNombreUsuario.replace(',',' '), 
																					sPlazoBO, 
																					strDirectorioTemp, (String)session.getAttribute("strPais"),  session.getAttribute("iNoNafinElectronico").toString(), 
											  strNombre,  strLogo,  strDirectorioPublicacion );
				  
				mensajeAutentificacion = "<b>La autentificación se llevo a cabo con exito <b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recibo:"+_acuse+"</b>";
	
	
			} else { //autenticación fallida
				mensajeAutentificacion = "<b>La autentificación no se llevo a cabo </b>";
			}		
			
		} else { //autenticación fallida
				mensajeAutentificacion = "<b>Existieron inconsistencias durante el proceso, intente mas tarde.</b>";
		}	
		
		if(hProceSolicitud.size()>0) {
			String folio= "";
			Vector registros = (Vector)hProceSolicitud.get("vSolicitudes");
			for (int i = 0; i < registros.size(); i++) {
				Vector registro = (Vector) registros.get(i);
				numSirac = registro.get(0).toString();
				nombrePyme = registro.get(1).toString();
				numDocumento = registro.get(2).toString();
				fechaDocumento = registro.get(3).toString();
				fechaVencimiento = registro.get(4).toString();			
				nombreMoneda = registro.get(6).toString();
				monto = registro.get(7).toString();
				montoDescuento = registro.get(8).toString();
				porcentajeAnticipo = registro.get(9).toString();
				recursoGarantia = registro.get(10).toString();
				importeInteres = registro.get(11).toString();
				importeRecibir = registro.get(12).toString();
				tasaAceptada = registro.get(13).toString();
				plazo = registro.get(14).toString();
				numProveedorInterno = registro.get(15).toString();
				descuentoEspecial = registro.get(16).toString();
				folio = registro.get(17).toString();				
				beneficiario = registro.get(29).toString();
				bancoBeneficiario = registro.get(30).toString();
				sucursalBeneficiario = registro.get(31).toString();
				cuentaBeneficiario = registro.get(32).toString();
				porcBeneficiario = registro.get(33).toString();
				importeRecibirBenef = registro.get(34).toString();
				netoRecibirPyme = registro.get(35).toString();
				tipoFactoraje = registro.get(36).toString();				
				
				datos = new HashMap();
				datos.put("NOMBRE_EPO", nombreEpo);
				datos.put("NU_SIRAC", numSirac);
				datos.put("NOMBRE_PROVEEDOR",nombrePyme);
				datos.put("NUMER0_DOCTO", numDocumento);
				datos.put("FECHA_EMISION", fechaDocumento);
				datos.put("FECHA_VENCIMIENTO", fechaVencimiento);
				datos.put("MONEDA", nombreMoneda);
				datos.put("TIPO_FACTORAJE", tipoFactoraje);
				datos.put("MONTO", monto);
				datos.put("PORCE_DESCUENTO",porcentajeAnticipo);
				datos.put("RECURSO_GARANTIA", recursoGarantia);
				datos.put("MONTO_DESCUENTO", montoDescuento);
				datos.put("MONTO_INTERES", importeInteres);
				datos.put("MONTO_OPERAR", importeRecibir);
				datos.put("TASA", tasaAceptada);
				datos.put("PLAZO", plazo);
				datos.put("NUMERO_SOLICITUD", folio.substring(0,10)+"-"+folio.substring(10,11));
				datos.put("NUM_PROVEEDOR", numProveedorInterno);
				datos.put("BENEFICIARIO", beneficiario);
				datos.put("BANCO_BENEFICIARIO", bancoBeneficiario);
				datos.put("SUCURSAL_BENEFICIARIO", sucursalBeneficiario);
				datos.put("CUENTA_BENEFICIARIO", cuentaBeneficiario);
				datos.put("PORC_BENEFICIARIO", porcBeneficiario);
				datos.put("IMPORTE_RECIBIR_BENE", importeRecibirBenef);
				datos.put("NETO_RECIBIR", netoRecibirPyme);			
				registrosC.add(datos);		
			}
			
			numRegistrosMN = new Integer(hProceSolicitud.get("numRegistrosMN").toString()).intValue();
			numRegistrosDL = new Integer(hProceSolicitud.get("numRegistrosDL").toString()).intValue();
				
			strNombreArchivoCSV  =  	hProceSolicitud.get("nombreArchivoCSV").toString();
			nombreArchivoPDF = 	hProceSolicitud.get("nombreArchivoPDF").toString();			
					
			
			strArchivoVble = hProceSolicitud.get("strArchivoVble").toString();
			if (!archivo.make(strArchivoVble, strDirectorioTemp, ".csv"))
				out.print("<--!Error al generar el archivo variable de interfase -->");
			else
				strNombreArchivo = archivo.nombre;
			
			strArchivoFijo = hProceSolicitud.get("strArchivoFijo").toString();
			if (!archivo.make(strArchivoFijo, strDirectorioTemp, ".txt"))
				out.print("<--!Error al generar el archivo fijo de interfase -->");
			else
				strNombreArchivoFijo = archivo.nombre;
		}
	if("1".equals(ic_banco_fondeo)) {
		mensajesFondeoPre=mensaje_param.getMensaje("I13forma3.NAFIN.Leyenda03", sesIdiomaUsuario);
		
	} else if("2".equals(ic_banco_fondeo)) {
		mensajesFondeoPre=mensaje_param.getMensaje("I13forma3.BANCOMEXT.Leyenda02", sesIdiomaUsuario);
	}
		
	consulta =  "{\"success\": true, \"total\": \"" + registrosC.size() + "\", \"registros\": " + registrosC.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("mensajeAutentificacion",mensajeAutentificacion);
	jsonObj.put("mensajeFondeo",mensajeFondeo);
	jsonObj.put("mensajesFondeoPre",mensajesFondeoPre);
	jsonObj.put("acuse",acuse.toString());
	jsonObj.put("acuse2",acuse.formatear());
	jsonObj.put("recibo",_acuse);
	jsonObj.put("fecha",fechaCarga);
	jsonObj.put("hora",horaCarga);
	jsonObj.put("usuario",iNoUsuario+" - "+strNombreUsuario);
	jsonObj.put("strNombreArchivo",strNombreArchivo);
	jsonObj.put("nombreArchivoPDF",nombreArchivoPDF);
	jsonObj.put("strNombreArchivoFijo",strNombreArchivoFijo);
	jsonObj.put("nombreArchivoCSV",strNombreArchivoCSV);	
	jsonObj.put("numRegistrosMNA",String.valueOf(numRegistrosMN));
	jsonObj.put("numRegistrosDLA",String.valueOf(numRegistrosDL));
	jsonObj.put("monto_descuento_mn",monto_descuento_mn);  
	jsonObj.put("monto_interes_mn",monto_interes_mn);  
	jsonObj.put("monto_operar_mn",monto_operar_mn);  
	jsonObj.put("monto_descuento_dl",monto_descuento_dl);  
	jsonObj.put("monto_interes_dl",monto_interes_dl);  
	jsonObj.put("monto_operar_dl",monto_operar_dl);   
	infoRegresar =jsonObj.toString(); 
	
} else  if(informacion.equals("ConsultarTotalesAcuse")){ 

		datos = new HashMap();	
		datos.put("EPO", ic_epo+"  "+nombreEpo);
		datos.put("MONTO_DESCUENTO_MN", monto_descuento_mn);
		datos.put("MONTO_INTERES_MN", monto_interes_mn);
		datos.put("MONTO_OPERAR_MN", monto_operar_mn);		
		datos.put("MONTO_DESCUENTO_DL", monto_descuento_dl);
		datos.put("MONTO_INTERES_DL", monto_interes_dl);		
		datos.put("MONTO_OPERAR_DL", monto_operar_dl);		
		registrosC.add(datos);	
		consulta =  "{\"success\": true, \"total\": \"" + 1 + "\", \"registros\": " + registrosC.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar=jsonObj.toString();
		

} else	if(informacion.equals("consultaTotales1Acuse")){
	
	String numRegistrosMNA = (request.getParameter("numRegistrosMNA")==null)?"":request.getParameter("numRegistrosMNA");	
	String numRegistrosDLA = (request.getParameter("numRegistrosDLA")==null)?"":request.getParameter("numRegistrosDLA");	
	for(int t =0; t<2; t++) {		
			registrosTot = new HashMap();		
			if(t==0 && Integer.parseInt(numRegistrosMNA) >0){ 				
				registrosTot.put("MONEDA", "Nacional");		
				registrosTot.put("TOTAL_DOCTO", numRegistrosMNA );												
			}		
			if(t==1 && Integer.parseInt(numRegistrosDLA)>0 ){ 				
				registrosTot.put("MONEDA", "Dólares");		
				registrosTot.put("TOTAL_DOCTO", numRegistrosDLA );										
			}		
			registrosTotales.add(registrosTot);
		}
			
		consulta =  "{\"success\": true, \"total\": \"" + 2 + "\", \"registros\": " + registrosTotales.toString()+"}";
		
		jsonObj = JSONObject.fromObject(consulta);
		infoRegresar=jsonObj.toString();
	
} else  if(informacion.equals("ArchivoVariable")){ 

	generaAch.setAcuse(_acuse);
	generaAch.setTipoArchivo("CSV");
	generaAch.setStrDirectorioTemp(strDirectorioTemp);
	nombreArchivo = generaAch.desArchAcuse() ; //Descargar

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("accion",accion);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	infoRegresar=jsonObj.toString();
	
} else  if(informacion.equals("ArchivoFijo")){ 

		strNombreArchivoFijo = (request.getParameter("strNombreArchivoFijo")==null)?"":request.getParameter("strNombreArchivoFijo");	
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+strNombreArchivoFijo);	
		infoRegresar=jsonObj.toString();	
		
} else  if(informacion.equals("nombreArchivoCSV") ){ 

  String acuse = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
	String hidFechaCarga = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):"";
	String hidHoraCarga = (request.getParameter("hora")!=null)?request.getParameter("hora"):"";
	String  hidRecibo =  (request.getParameter("recibo")!=null)?request.getParameter("recibo"):"";
	
	
	HashMap parametros = new HashMap();
	parametros.put("acuse", acuse);
	parametros.put("ic_epo", ic_epo);
	parametros.put("ic_if", iNoCliente);
		
	parametros.put("hidRecibo", hidRecibo);
	parametros.put("monto_descuento_mn", monto_descuento_mn);
	parametros.put("monto_interes_mn", monto_interes_mn);
	parametros.put("monto_operar_mn", monto_operar_mn);
	parametros.put("monto_descuento_dl", monto_descuento_dl);
	parametros.put("monto_interes_dl", monto_interes_dl);
	parametros.put("monto_operar_dl", monto_operar_dl);
	parametros.put("iNoUsuario", iNoUsuario);
	parametros.put("strNombreUsuario", strNombreUsuario);
	parametros.put("hidFechaCarga", hidFechaCarga);
	parametros.put("hidHoraCarga", hidHoraCarga);
	
	parametros.put("strDirectorioTemp", strDirectorioTemp); 
	parametros.put("strPais", (String)session.getAttribute("strPais") );
	parametros.put("iNoNafinElectronico", session.getAttribute("iNoNafinElectronico").toString());
	parametros.put("strNombre", strNombre);
	parametros.put("strLogo", strLogo);
	parametros.put("strDirectorioPublicacion", strDirectorioPublicacion); 
	
	String  nombreArch =   BeanAutDescuento.generaArchivosIFDesAutoCSV(parametros);	 
   
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArch);	
	infoRegresar=jsonObj.toString();	

} else  if(informacion.equals("procesarEnvioPreAuse")){ 

	String numeroDoctoMN = (request.getParameter("numeroDoctoMN")!=null)?request.getParameter("numeroDoctoMN"):"";
	String numeroDoctoDL = (request.getParameter("numeroDoctoDL")!=null)?request.getParameter("numeroDoctoDL"):"";


	String seleccionados[] = request.getParameterValues("seleccionados");
	String doctoSeleccionados2 ="";
	if (seleccionados != null) {
		for(int i = 0; i < seleccionados.length; i++) {
			doctoSeleccionados2 +=seleccionados[i]+",";
		}
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("doctoSeleccionados", doctoSeleccionados2);	
	jsonObj.put("ic_pyme", ic_pyme);	
	jsonObj.put("ic_epo", ic_epo);	
	jsonObj.put("ic_banco_fondeo", ic_banco_fondeo);	
	jsonObj.put("mensajeFondeo", mensajeFondeo);	
	jsonObj.put("numeroDoctoMN", numeroDoctoMN );	
	jsonObj.put("numeroDoctoDL", numeroDoctoDL );		
	infoRegresar=jsonObj.toString();	


		
} 


%>
<%=infoRegresar%>

<%!
// metodousado en la cosulta del Preacuse 
	private boolean esUnDocumentoAgregado(String documento,HashMap listaDocumentos){
		
		System.out.println("13forma3a.jsp::esUnDocumentoAgregado(E)");
		
		boolean esDoctoAgregado = false;
		if((documento == null ) || (listaDocumentos == null)){
			System.out.println("13forma3a.jsp::esUnDocumentoAgregado(S)");	
			return false;
		}
		
		String agregado = (String) listaDocumentos.get(documento);
		if(agregado != null && agregado.equals("true")){
			esDoctoAgregado = true;
		}
		
		System.out.println("13forma3a.jsp::esUnDocumentoAgregado(S)");
		return esDoctoAgregado;
	}
	
	
	public HashMap getDatosDocumentos(HashSet listaDocumentos)
		throws Exception{
		
			System.out.println("getDatosDocumentos(E)");
		
			HashMap			resultado		= new HashMap();
			if(listaDocumentos == null || listaDocumentos.size() == 0){
				System.out.println("getDatosDocumentos(S)");
				return	resultado;
			}
				
			AccesoDB 		con 				= new AccesoDB();
			StringBuffer	qrySentencia	= new StringBuffer();
			ResultSet		registros		= null;
			List 				lVarBind			= new ArrayList();
			
			try {	
 
				con = new AccesoDB();
				con.conexionDB();

				String lista = "";
				Iterator it = listaDocumentos.iterator();
				int k=0;
				while(it.hasNext()) {
					if(k>0){
						lista += ",";
					}
					lista  += (String)it.next();
					k++;
				}	
        
        qrySentencia.append(	
			   "SELECT " +
				 " d.ic_documento," +
				 " TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') AS fechaEmision, "  + 
				 " m.cd_nombre AS moneda, " +
				 " f.cg_nombre AS tipoFactoraje, "  +
				 " d.ig_numero_docto "  +
				"FROM com_documento d, "  +
				" comcat_moneda m, " +
				" comcat_tipo_factoraje f "  +
				" WHERE ic_documento IN ( "+lista+" ) and " +
				"  d.cs_dscto_especial = f.cc_tipo_factoraje and " +
				" m.ic_moneda = d.ic_moneda");

				registros = con.queryDB(qrySentencia.toString());
				while(registros != null && registros.next()){
					
					String igNumeroDocto = registros.getString("ig_numero_docto");
					String icDocumento 	= registros.getString("ic_documento");
					String fechaEmision  = registros.getString("fechaEmision");
					String moneda			= registros.getString("moneda");
					String tipoFactoraje = registros.getString("tipoFactoraje");
					
					HashMap registro = new HashMap();
					registro.put("IG_NUMERO_DOCTO", 	igNumeroDocto);
					registro.put("IC_DOCUMENTO",     icDocumento);
					registro.put("FECHA_EMISION",		fechaEmision);
					registro.put("MONEDA",				moneda);
					registro.put("TIPO_FACTORAJE",	tipoFactoraje);
					 
					resultado.put(icDocumento,registro);
          
				}
				con.cierraStatement();
		}catch(Exception e){
			
			System.out.println("getDatosDocumentos(Exception)");
			System.out.println("getDatosDocumentos.listaClavesNotas = <"+listaDocumentos+">");
			e.printStackTrace();
			throw new Exception("Error al Consultar Notas de Credito");
			
		}finally{
			if(registros != null) registros.close();
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("getDatosDocumentos(S)13");
		}

		return resultado;	
	}
	
	private String getPropiedad(String id,String propiedad,HashMap bd){
		
		if(id == null || id.equals("")) return "";
		if(propiedad == null) return "";
		if(bd == null) return "";
		
		HashMap registro = (HashMap) bd.get(id);
		String resultado = (String) registro.get(propiedad);
		if(resultado == null){
			return "";	
		}
		return resultado;
		
	}
	
	private HashMap getDatosNotasCredito(HashSet listaClavesNotas)
		throws Exception{
		
			System.out.println("getDatosNotasCredito(E)");
		
			HashMap			resultado		= new HashMap();
			if(listaClavesNotas == null || listaClavesNotas.size() == 0){
				System.out.println("getDatosNotasCredito(S)");
				return	resultado;
			}
				
			AccesoDB 		con 				= new AccesoDB();
			StringBuffer	qrySentencia	= new StringBuffer();
			ResultSet		registros		= null;
			List 				lVarBind			= new ArrayList();
			
			try {	
 
				con = new AccesoDB();
				con.conexionDB();

				String lista = "";
				Iterator it = listaClavesNotas.iterator();
				int k=0;
				while(it.hasNext()) {
					if(k>0){
						lista += ",";
					}
					lista  += (String)it.next();
					k++;
				}	
        
        qrySentencia.append(	
						"SELECT " + 
						"	ic_documento, "  +
						"  ig_numero_docto, "  +
						"  fn_monto "  +
						"FROM "  + 
						"  com_documento "  +
						"WHERE "  +
						"	ic_documento IN ( "+lista+" )");

				registros = con.queryDB(qrySentencia.toString());
				while(registros != null && registros.next()){
					
					String icDocumento 	= registros.getString("ic_documento");
					String igNumeroDocto = registros.getString("ig_numero_docto");
					String fnMonto			= registros.getString("fn_monto");
					
					HashMap registro = new HashMap();
					registro.put("IG_NUMERO_DOCTO", igNumeroDocto);
					registro.put("FN_MONTO",        fnMonto);
					 
					resultado.put(icDocumento,registro);
          
				}
				con.cierraStatement();
		}catch(Exception e){
			
			System.out.println("getDatosNotasCredito(Exception)");
			System.out.println("getDatosNotasCredito.listaClavesNotas = <"+listaClavesNotas+">");
			e.printStackTrace();
			throw new Exception("Error al Consultar Notas de Credito");
			
		}finally{
			if(registros != null) registros.close();
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("getDatosNotasCredito(S)");
		}

		return resultado;	
	}
	
	public List getDetalleNotaAplicada(String icNotaCredito)
	throws Exception{
		ArrayList lista = new ArrayList();
		lista.add(icNotaCredito);
		return getDetalleNotaAplicada(lista);
	}
	
	public List getDetalleNotaAplicada(ArrayList listaIdNotaCredito)
    	throws Exception{
		
		System.out.println("getDetalleNotaAplicada(E)");
		ArrayList lista = new ArrayList();
		
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();
		
		// Validar que la lista no venga vacia
		if(listaIdNotaCredito == null || (listaIdNotaCredito.size() == 0)){
			System.out.println("getDetalleNotaAplicada(S)");
			return lista;
		}
		// Validar que haya al menos un parametro valido
		boolean hayParametrosValidos = false;
		for(int j=0;j<listaIdNotaCredito.size();j++){
				String icNotaCredito = (String) listaIdNotaCredito.get(j);
				if(icNotaCredito != null && !icNotaCredito.equals("")){
					hayParametrosValidos = true;
				}
		}
		if(!hayParametrosValidos){
			System.out.println("getDetalleNotaAplicada(S)");
			return lista;
		}
		
		try{
 
			StringBuffer listaQuestionSign = new StringBuffer();
			for(int i=0;i<listaIdNotaCredito.size();i++){
				String icNotaCredito = (String) listaIdNotaCredito.get(i);
				if(icNotaCredito != null && !icNotaCredito.equals("")){
					if(i>0) listaQuestionSign.append(",");
					listaQuestionSign.append("?");
					lVarBind.add(icNotaCredito);
				}
			}
			
			con.conexionDB();
			qrySentencia.append(
					"SELECT                "  +
					"	IC_NOTA_CREDITO,    "  +
					"  IC_DOCUMENTO,       "  +
					"  FN_MONTO_APLICADO,  "  +
					"  FN_SALDO_DOCTO      "  +
					"FROM                  "  +
					"	COMREL_NOTA_DOCTO   "  +
					"WHERE                 "  +
					"	IC_NOTA_CREDITO in ("+listaQuestionSign.toString()+") ");
			
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			while(registros != null && registros.next()){
				
				String idNotaCredito  	= registros.getString("IC_NOTA_CREDITO");
				String idDocumento 	  	= registros.getString("IC_DOCUMENTO");
				String montoAplicado		= registros.getString("FN_MONTO_APLICADO");
				String saldoDocto		  	= registros.getString("FN_SALDO_DOCTO");
				
				lista.add(
					new NotaCreditoMultiple(
						idNotaCredito,
						idDocumento,
						montoAplicado,
						saldoDocto
					)
				);
			}
			
		}catch(Exception e){
			System.out.println("getDetalleNotaAplicada(Exception)");
			System.out.println("getDetalleNotaAplicada.icNotaCredito = <"+listaIdNotaCredito+">");
			e.printStackTrace();
			throw new Exception("Error al consultar datos de las Notas de Credito");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("getDetalleNotaAplicada(S)");
		}
		
		return lista;
	}
	
	public List getNotasDeCredito(String icDocumento)
     throws Exception{
		  
		 System.out.println("getNotasDeCredito(E)");
		 String []icDocumentos 	= new String[1];
		 icDocumentos[0] 			= icDocumento;
		 List 	lista 			= getNotasDeCredito(icDocumentos);
		 System.out.println("getNotasDeCredito(S)");
		 
		 return lista;
	}
	 
	public List getNotasDeCredito(String []icDocumentos)
     throws Exception{
		
		System.out.println("getNotasDeCredito(E)");
		ArrayList lista = new ArrayList();
		
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();
		
		if(icDocumentos == null || icDocumentos.length == 0){
			System.out.println("getNotasDeCredito(S)");
			return lista;
		}
		
		try{
			
			con.conexionDB();
			
			StringBuffer listaVariablesBind = new StringBuffer();
			
			for(int i=0;i<icDocumentos.length;i++){
				if(i>0) listaVariablesBind.append(",");
				listaVariablesBind.append("?");
			}
			
			qrySentencia.append(
					"SELECT                				"  +
					"	DISTINCT IC_NOTA_CREDITO    	"  +
					"FROM                  				"  +
					"	COMREL_NOTA_DOCTO   				"  +
					"WHERE                 				"  +
					"	IC_DOCUMENTO in ("+listaVariablesBind.toString()+") ");

			for(int i=0;i<icDocumentos.length;i++){
				String icDocumento = (String)icDocumentos[i]; 
				lVarBind.add(icDocumento);
			}
			
			registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
			while(registros != null && registros.next()){
				
				String idNotaCredito  = registros.getString("IC_NOTA_CREDITO");
				lista.add(idNotaCredito);
			}
			
		}catch(Exception e){
			System.out.println("getNotasDeCredito(Exception)");
			System.out.println("getNotasDeCredito.icDocumento = <"+icDocumentos+">");
			e.printStackTrace();
			throw new Exception("Error al consultar datos de las Notas de Credito");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("getNotasDeCredito(S)");
		}
		
		return lista;
	}
%>
