var showPanelLayoutDeCarga;
var proceso;
var consultasEncabezados;
var gridsEncabezados;
var consultasDetalles;
var gridDetalles;
var consultasTotales;
var gridsTotales;
var strTipoBanco;
var textoFirmar;
var granTotalMontoMN;
var granTotalMontoDL;
var numTotalPrestamos;
var acuse;
var urlArchivo1;
Ext.onReady(function() {
	
	//-------------------------------- VALIDACIONES -----------------------------------
 
	//----------------------------------- HANDLERS ------------------------------------

function procesarArchivoSuccess() {
						
			urlArchivo1 = urlArchivo1.replace('/nafin','');
			var params = {nombreArchivo: urlArchivo1};				
			panelFormaCargaArchivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			panelFormaCargaArchivo.getForm().getEl().dom.submit();
	}


var creaComponentes=function(numeroCmp,respuesta){
	
	consultasEncabezados=new Array(numeroCmp);
	gridsEncabezados=new Array(numeroCmp);
	consultasDetalles=new Array(numeroCmp);
	gridDetalles=new Array(numeroCmp);
	consultasTotales=new Array(numeroCmp);
	gridsTotales=new Array(numeroCmp);
	var cad;
	for(var i=1;i<=numeroCmp;i++){
		cad='registrosEncabezados'+i;
		consultasEncabezados[i-1] = new Ext.data.JsonStore({
						fields: [
								{name: 'IC_ENCABEZADO_PAGO_TMP'},
								{name: 'IC_MONEDA'},
								{name: 'IG_SUCURSAL'},
								{name: 'NOMBREMONEDA'},
								{name: 'DF_PERIODO_FIN'},
								{name: 'DF_PROBABLE_PAGO'},
								{name: 'DF_DEPOSITO'},
								{name: 'IG_IMPORTE_DEPOSITO'},
								{name: 'NOMBREFINANCIERA'}
						],
						//data: [{'IC_ENCABEZADO_PAGO_TMP':'','IC_MONEDA':'','IG_SUCURSAL':'','NOMBREMONEDA':'','DF_PERIODO_FIN':'',
							//		'DF_PROBABLE_PAGO':'','DF_DEPOSITO':'','IG_IMPORTE_DEPOSITO':'','NOMBREFINANCIERA':''}],
						//autoLoad: true,
						listeners: {exception: NE.util.mostrarDataProxyError}
					});
					
						gridsEncabezados[i-1] = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,	monitorResize: true,	height: 90,	width: 790,frame: false,style: 'margin: 0 auto',
							title: 'Encabezado',
							//id: 'encabezado'+i,
							store: consultasEncabezados[i-1],
							columns: [
								{header: 'Direcci&oacute;n estatal',tooltip: 'Direcci&oacute;n estatal',	dataIndex: 'IG_SUCURSAL',	width: 100,	align: 'center'},
								{header: 'Moneda',tooltip: 'Moneda',	dataIndex: 'NOMBREMONEDA',	width: 100,	align: 'center'},
								{header: 'Banco servicio',tooltip: 'Banco servicio',	dataIndex: 'NOMBREFINANCIERA',	width: 100,	align: 'center'},
								{header: 'Fecha vencimiento',tooltip: 'Fecha vencimiento',	dataIndex: 'DF_PERIODO_FIN',	width: 100,	align: 'center'},
								{header: 'Fecha probable de pago',tooltip: 'Fecha probable de pago',	dataIndex: 'DF_PROBABLE_PAGO',	width: 100,	align: 'center' },
								{header: 'Fecha de dep&oacute;sito',tooltip: 'Fecha de dep&oacute;sito',	dataIndex: 'DF_DEPOSITO',	width: 100,	align: 'center' },
								{header: 'Importe de dep�sito',tooltip: 'Importe de dep�sito',	dataIndex: 'IG_IMPORTE_DEPOSITO',	width: 100,	align: 'center' }
							]
						});
						
						consultasEncabezados[i-1].loadData(respuesta[cad]);
						
						
						
						cad='registrosDetalles'+i;
		consultasDetalles[i-1] = new Ext.data.JsonStore({
						fields: [
							{name: 'CLAVE_IF'},
							{name: 'CLAVE_MONEDA'},
							{name: 'MONEDA'},
							{name: 'FECHA_VENCIMIENTO'},
							{name: 'FECHA_PROB_PAGO'},
							{name: 'IMPORTE_DEPOSITO'},
							{name: 'CLAVE_ESTADO'},
							{name: 'IMPORTE_DEPOSITO'},
							{name: 'NOMBRE_ESTADO'},
							{name: 'SUBAPLICACION'},
							{name: 'PRESTAMO'},
							{name: 'CLAVE_SIRAC'},
							{name: 'CAPITAL'},
							{name: 'INTERESES'},
							{name: 'MORATORIOS'},
							{name: 'SUBSIDIO'},
							{name: 'COMISION'},
							{name: 'IVA'},
							{name: 'IMPORTE_PAGO'},
							{name: 'CONCEPTO_PAGO'},
							{name: 'ORIGEN_PAGO'},
							{name: 'SUCURSAL'},
							{name: 'ACREDITADO'},
							{name: 'FECHA_OPERACION'},
							{name: 'FECHA_PAGO'},
							{name: 'SALDO'},
							{name: 'TASA'},
							{name: 'DIAS'},
							{name: 'INTERESES'},
							{name: 'IMPORTE_PAGO'}
						],
						//data: [{'IC_ENCABEZADO_PAGO_TMP':'','IC_MONEDA':'','IG_SUCURSAL':'','NOMBREMONEDA':'','DF_PERIODO_FIN':'',
						//			'DF_PROBABLE_PAGO':'','DF_DEPOSITO':'','IG_IMPORTE_DEPOSITO':'','NOMBREFINANCIERA':''}],
						//autoLoad: true,
						listeners: {exception: NE.util.mostrarDataProxyError}
					});
					
					if(strTipoBanco=='NB'){
					
						gridDetalles[i-1] = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,	monitorResize: true,	height: 150,	width: 790,frame: false,style: 'margin: 0 auto',
							title: 'Detalles',
							//id: 'detalles'+i,
							store: consultasDetalles[i-1],
							columns: [
								{header: 'Subaplicaci&oacute;n',tooltip: 'Subaplicaci&oacute;n',	dataIndex: 'SUBAPLICACION',	width: 100,	align: 'center'},
								{header: 'Pr&eacute;stamo',tooltip: 'Pr&eacute;stamo',	dataIndex: 'PRESTAMO',	width: 100,	align: 'center'},
								{header: 'Clave SIRAC',tooltip: 'Clave SIRAC',	dataIndex: 'CLAVE_SIRAC',	width: 100,	align: 'center'},
								{header: 'Capital',tooltip: 'Capital',	dataIndex: 'CAPITAL',	width: 100,	align: 'center'},
								{header: 'Inter&eacute;s',tooltip: 'Inter&eacute;s',	dataIndex: 'INTERESES',	width: 100,	align: 'center' },
								{header: 'Moratorios',tooltip: 'Moratorios',	dataIndex: 'MORATORIOS',	width: 100,	align: 'center' },
								{header: 'Subsidio',tooltip: 'Subsidio',	dataIndex: 'SUBSIDIO',	width: 100,	align: 'center' },
								{header: 'Comisi&oacute;n',tooltip: 'Comisi&oacute;n',	dataIndex: 'COMISION',	width: 100,	align: 'center' },
								{header: 'IVA',tooltip: 'IVA',	dataIndex: 'IVA',	width: 100,	align: 'center' },
								{header: 'Importe Pago',tooltip: 'Importe Pago',	dataIndex: 'IMPORTE_PAGO',	width: 100,	align: 'center' },
								{header: 'Concepto pago',tooltip: 'Concepto pago',	dataIndex: 'CONCEPTO_PAGO',	width: 100,	align: 'center' },
								{header: 'Origen pago',tooltip: 'Origen pago',	dataIndex: 'ORIGEN_PAGO',	width: 100,	align: 'center' }
							]
						});
						}else {
							gridDetalles[i-1] = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,	monitorResize: true,	height: 150,	width: 790,frame: false,style: 'margin: 0 auto',
							title: 'Detalles',
							//id: 'detalles'+i,
							store: consultasDetalles[i-1],
							columns: [
								{header: 'Sucursal',tooltip: 'Sucursal',	dataIndex: 'SUCURSAL',	width: 100,	align: 'center'},
								{header: 'Subaplicaci&oacute;n',tooltip: 'Subaplicaci&oacute;n',	dataIndex: 'SUBAPLICACION',	width: 100,	align: 'center'},
								{header: 'Pr&eacute;stamo',tooltip: 'Pr&eacute;stamo',	dataIndex: 'PRESTAMO',	width: 100,	align: 'center'},
								{header: 'Acreditado',tooltip: 'Acreditado',	dataIndex: 'ACREDITADO',	width: 100,	align: 'center'},
								{header: 'Fecha de Operaci&oacute;n',tooltip: 'Fecha de Operaci&oacute;n',	dataIndex: 'FECHA_OPERACION',	width: 100,	align: 'center'},
								{header: 'Fecha de Vencimiento',tooltip: 'Fecha de Vencimiento',	dataIndex: 'FECHA_VENCIMIENTO',	width: 100,	align: 'center'},
								{header: 'Fecha de Pago',tooltip: 'Fecha de Pago',	dataIndex: 'FECHA_PAGO',	width: 100,	align: 'center'},
								{header: 'Saldo Insoluto',tooltip: 'Saldo Insoluto',	dataIndex: 'SALDO',	width: 100,	align: 'center'},
								{header: 'Tasa',tooltip: 'Tasa',	dataIndex: 'TASA',	width: 100,	align: 'center'},
								{header: 'Capital',tooltip: 'Capital',	dataIndex: 'CAPITAL',	width: 100,	align: 'center'},
								{header: 'D&iacute;as',tooltip: 'D&iacute;as',	dataIndex: 'DIAS',	width: 100,	align: 'center'},
								{header: 'Inter&eacute;s',tooltip: 'Inter&eacute;s',	dataIndex: 'INTERESES',	width: 100,	align: 'center'},
								{header: 'Comisi&oacute;n',tooltip: 'Comisi&oacute;n',	dataIndex: 'COMISION',	width: 100,	align: 'center' },
								{header: 'IVA',tooltip: 'IVA',	dataIndex: 'IVA',	width: 100,	align: 'center' },
								{header: 'Importe Pago',tooltip: 'Importe Pago',	dataIndex: 'IMPORTE_PAGO',	width: 100,	align: 'center' }
								
							]
						});
						
						}
						consultasDetalles[i-1].loadData(respuesta[cad]);
						
						
						cad='registrosTotales'+i;
		consultasTotales[i-1] = new Ext.data.JsonStore({
						fields: [
							{name: 'TOTAL'},
							{name: 'SUBAPLICACION'},
							{name: 'PRESTAMO'},
							{name: 'ACREDITADO'},
							{name: 'FECHA_OPERACION'},
							{name: 'FECHA_VENCIMIENTO'},
							{name: 'FECHA_PAGO'},
							{name: 'SALDO'}
						],
						//data: [{'IC_ENCABEZADO_PAGO_TMP':'','IC_MONEDA':'','IG_SUCURSAL':'','NOMBREMONEDA':'','DF_PERIODO_FIN':'',
						//			'DF_PROBABLE_PAGO':'','DF_DEPOSITO':'','IG_IMPORTE_DEPOSITO':'','NOMBREFINANCIERA':''}],
						//autoLoad: true,
						listeners: {exception: NE.util.mostrarDataProxyError}
					});
					
					if(strTipoBanco=='NB'){
					
						gridsTotales[i-1] = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,	monitorResize: true,	height: 90,	width: 790,frame: false,style: 'margin: 0 auto',
							title: 'Totales',
							//id: 'detalles'+i,
							store: consultasTotales[i-1],
							columns: [
								{header: '',tooltip: '',	dataIndex: 'TOTAL',	width: 90,	align: 'center'},
								{header: 'Total Capital',tooltip: 'Total Capital',	dataIndex: 'SUBAPLICACION',	width: 98,	align: 'center'},
								{header: 'Total Intereses',tooltip: 'Total Intereses',	dataIndex: 'PRESTAMO',	width: 98,	align: 'center'},
								{header: 'Total Moratorios',tooltip: 'Total Moratorios',	dataIndex: 'ACREDITADO',	width: 98,	align: 'center'},
								{header: 'Total Subsidio',tooltip: 'Total Subsidio',	dataIndex: 'FECHA_OPERACION',	width: 100,	align: 'center' },
								{header: 'Total Comisi�n',tooltip: 'Total Comisi�n',	dataIndex: 'FECHA_VENCIMIENTO',	width: 100,	align: 'center' },
								{header: 'Total IVA',tooltip: 'Total IVA',	dataIndex: 'FECHA_PAGO',	width: 100,	align: 'center' },
								{header: 'Total Importe Pago',tooltip: 'Total Importe Pago',	dataIndex: 'SALDO',	width: 100,	align: 'center' }
								]
						});
						}else {
							gridsTotales[i-1] = new Ext.grid.GridPanel({
							stripeRows: true,	loadMask: true,	monitorResize: true,	height: 90,	width: 790,frame: false,style: 'margin: 0 auto',
							title: 'Totales',
							//id: 'detalles'+i,
							store: consultasTotales[i-1],
							columns: [
								{header: '',tooltip: '',	dataIndex: 'TOTAL',	width: 90,	align: 'center'},
								{header: 'Total Saldo Insoluto',tooltip: 'Total Saldo Insoluto',	dataIndex: 'SUBAPLICACION',	width: 98,	align: 'center'},
								{header: 'Total Capital',tooltip: 'Total Capital',	dataIndex: 'PRESTAMO',	width: 98,	align: 'center'},
								{header: 'Total Intereses',tooltip: 'Total Intereses',	dataIndex: 'ACREDITADO',	width: 100,	align: 'center'},
								{header: 'Total Importe Pago',tooltip: 'Total Importe Pago',	dataIndex: 'FECHA_OPERACION',	width: 100,	align: 'center' }
								
								]
						});
						
						}
						consultasTotales[i-1].loadData(respuesta[cad]);
						
						
						
						
						
						
						pnl.add(NE.util.getEspaciador(1));
						pnl.add(gridsEncabezados[i-1]);
						pnl.add(NE.util.getEspaciador(5));
						
						pnl.add(gridDetalles[i-1]);
						pnl.add(gridsTotales[i-1]);
						pnl.add(NE.util.getEspaciador(30));
						//pnl.insert(i+1,gridreg+i);
						
		
		
	}
	
	textoFirmar=respuesta.textoFirmar;
	numTotalPrestamos=respuesta.numTotalPrestamos;
	granTotalMontoDL=respuesta.granTotalMontoDL;
	granTotalMontoMN=respuesta.granTotalMontoMN;
	pnl.add(botones);
	pnl.doLayout();
}
 
	var procesaCargaArchivo = function(opts, success, response) {

		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			// Procesar respuesta
			if(!Ext.isEmpty(resp.msg)){
				Ext.Msg.alert(
					'Mensaje',
					resp.msg,
					function(btn, text){
						cargaArchivo(resp.estadoSiguiente,resp);
					}
				);
			} else {
				cargaArchivo(resp.estadoSiguiente,resp);
			}
			
		}else{
			
			// Suprimir mascaras segun se requiera
		
			
			// Ocultar ventana de avance validacion si esta esta siendo mostrada
         hideWindowAvanceValidacion();
         
         // Ocultar mascara del panel de resultados de la validacion
         element = Ext.getCmp("panelResultadosValidacion").getEl();
			if( element.isMasked()){
				element.unmask();
			}
 
			// Ocultar mascara del panel de preacuse
			element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	//----------------------------- "MAQUINA DE ESTADO" -------------------------------
	var cargaArchivo = function(estado, respuesta ){
 
		if(					estado == "INICIALIZACION"					){
			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma11Ext.data.jsp',
				params: 	{
					informacion:		'CargaArchivo.inicializacion'
				},
				callback: 				procesaCargaArchivo
			});
			
		} else if(			estado == "MOSTRAR_AVISO"								){

			if(!Ext.isEmpty(respuesta.aviso)){
				
				var labelAviso 	= Ext.getCmp('labelAviso');
				labelAviso.setText(respuesta.aviso,false);
				
				
				
			}
			
		} else if(			estado == "ESPERAR_CAPTURA_SOLICITUD_DE_CARGA"	){
 
			


			
		} else if(			estado == "ENVIAR_SOLICITUD_DE_CARGA" 				){
			

			
			// Determinar el estado siguiente
			Ext.Ajax.request({
				url: 						'13forma11Ext.data.jsp',
				params: 	Ext.apply ( 
					{
						informacion:		'CargaArchivo.enviarSolicitudDeCarga'
					},
					respuesta
				),
				callback: 				procesaCargaArchivo
			});
 
		} else if(			estado == "ESPERAR_CAPTURA_ARCHIVO_SOLICITUDES"	){
			
			// Ocultar panel con la solicitud de carga
			
 
			// Esperar decision
			Ext.getCmp("claveMes1").setValue(respuesta.claveMes);
			Ext.getCmp("numeroRegistros1").setValue(respuesta.numeroRegistros);
			Ext.getCmp("sumatoriaSaldosFinMes1").setValue(respuesta.sumatoriaSaldosFinMes);
 
			// Mostrar panel de carga de archivo
			var panelFormaCargaArchivo = Ext.getCmp("panelFormaCargaArchivo");
			panelFormaCargaArchivo.show();
			
		} else if(			estado == "SUBIR_ARCHIVO"								){
			
			Ext.getCmp("panelFormaCargaArchivo").getForm().submit({
				clientValidation: 	true,
				url: 						'13forma11Ext.data.jsp?informacion=CargaArchivo.subirArchivo',
				waitMsg:   				'Subiendo archivo...',
				waitTitle: 				'Por favor espere',
				success: function(form, action) {
					 procesaCargaArchivo(null,  true,  action.response );
				},
				failure: function(form, action) {
				 	 action.response.status = 200;
					 procesaCargaArchivo(null,  false, action.response );
				}
			});
			
		} else if(			estado == "REALIZAR_VALIDACION"         			){
 
			showWindowAvanceValidacion();
			
			Ext.Ajax.request({
				url: 		'13forma11Ext.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.realizarValidacion',
					fileName:									respuesta.fileName
				},
				callback: 										procesaCargaArchivo
			});
		
		} else if(  estado == "ESPERAR_DECISION_VALIDACION" ){
 
			var claveMes 												= respuesta.claveMes;
			var numeroRegistros 										= respuesta.numeroRegistros;
			var sumatoriaSaldosFinMes 								= respuesta.sumatoriaSaldosFinMes;
			
			proceso 											= respuesta.claveProceso;
			var totalRegistros 										= respuesta.totalRegistros;
			var montoTotal 											= respuesta.montoTotal;      // Debug info: por el momento no se usa
			var montoTotalPagar 										= respuesta.montoTotalPagar; // Debug info: por el momento no se usa
			
			//var numeroRegistrosSinErrores 						= respuesta.numeroRegistrosSinErrores;
			//var sumatoriaSaldosFinMesSinErrores 				= respuesta.sumatoriaSaldosFinMesSinErrores;
			
			var numeroRegistrosConErrores 						= respuesta.numeroRegistrosConErrores;
			var sumatoriaSaldosFinMesConErrores 				= respuesta.sumatoriaSaldosFinMesConErrores;
 
			var continuarCarga 									= respuesta.continuarCarga;
 
			// SETUP PARAMETROS VALIDACION
			// Recordar numero de proceso y el total de registros
   
         // Inicializar panel de resultados
			//Ext.getCmp("numeroRegistrosSinErrores").setValue(numeroRegistrosSinErrores);
			//Ext.getCmp("sumatoriaSaldosFinMesSinErrores").setValue(sumatoriaSaldosFinMesSinErrores);

			// Determinar si se mostrar� el bot�n de continuar carga
			if(continuarCarga){
         	Ext.getCmp('botonContinuarCarga').enable();
         }else{
         	Ext.getCmp('botonContinuarCarga').disable();
         }
         
			// MOSTRAR COMPONENTES
			// Resetear campo de carga de archivo
			Ext.getCmp("archivo").reset();
			
			// Ocultar ventana de avance validacion
			hideWindowAvanceValidacion();
			
			 // Remover contenido anterior de los grids
         var registrosSinErroresData 		= Ext.StoreMgr.key('registrosSinErroresDataStore');
         var registrosConErroresData 		= Ext.StoreMgr.key('registrosConErroresDataStore');
         //var erroresVsCifrasControlData 	= Ext.StoreMgr.key('erroresVsCifrasControlDataStore');
         //registrosSinErroresData.removeAll();
         //registrosConErroresData.removeAll();
         //erroresVsCifrasControlData.removeAll();
			urlArchivo1=respuesta.urlArchivo;
			if(urlArchivo1!=''){
				Ext.getCmp('registrosError').setVisible(true);
			}else{
				Ext.getCmp('registrosError').setVisible(false)
			}
         // Mostrar espaciador del Panel de resultados
			Ext.getCmp("espaciadorPanelResultadosValidacion").show();
			// Mostrar Panel de resultados
         Ext.getCmp('panelResultadosValidacion').show();

         // Mostrar Panel de Resultados de la validacion
         var tabPanelResultadosValidacion = Ext.getCmp('tabPanelResultadosValidacion');
         //tabPanelResultadosValidacion.setActiveTab(2);
         //erroresVsCifrasControlData.loadData(respuesta.erroresVsCifrasControlDataArray);
        // Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
         tabPanelResultadosValidacion.setActiveTab(1);
         registrosConErroresData.loadData(respuesta.registrosConErroresDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
         tabPanelResultadosValidacion.setActiveTab(0);
         registrosSinErroresData.loadData(respuesta.registrosSinErroresDataArray);
         Ext.getCmp('panelResultadosValidacion').doLayout(false,true);
		   
      } else if(  estado == "PRESENTAR_PREACUSE"         ){
      	
      	Ext.getCmp("panelResultadosValidacion").el.mask("Generando Pre-Acuse...","x-mask-loading");
      	
      	Ext.Ajax.request({
				url: 		'13forma11Ext.data.jsp',
				params: 	{
					informacion:								'CargaArchivo.presentarPreacuse',
					claveProceso: 								proceso
					},
				callback: 										procesaCargaArchivo
			});
      	
		} else if(  estado == "ESPERAR_DECISION_PREACUSE"         ){
			
		
			// Suprimir Mascara
			Ext.getCmp("panelResultadosValidacion").el.unmask();
			
			// Ocultar panel de Carga
			Ext.getCmp("panelFormaCargaArchivo").hide();
			// Ocultar espaciador del panel de resultados
			Ext.getCmp("espaciadorPanelResultadosValidacion").hide();
			// Ocultar panel de resultados
			Ext.getCmp("panelResultadosValidacion").hide();
			// Ocultar espaciador del layout
			Ext.getCmp("espaciadorPanelLayoutDeCarga").hide();
			// Ocultar panel con el layout de carga
			Ext.getCmp("panelLayoutDeCarga").hide();
			
			// Mostrar panel de preacuse
			//Ext.getCmp("panelPreacuseCargaArchivo").show();
			
			
			strTipoBanco=respuesta.strTipoBanco;
			creaComponentes(respuesta.totales,respuesta);
			


      } else if(  estado == "ESPERAR_DECISION"  			){
      	
      	// Ocultar mascara del panel de preacuse
			var element = Ext.getCmp("panelPreacuseCargaArchivo").getEl();
			if( element.isMasked()){
				element.unmask();
			}
			
      	return;
      	
		} else if(  estado == "TRANSMITIR_REGISTROS" 		){
			
		
			Ext.getCmp("panelPreacuseCargaArchivo").el.mask("Transmitiendo solicitudes...","x-mask-loading");
			

         
		} else if(	estado == "MOSTRAR_ACUSE_TRANSMISION_REGISTROS"	){
 

			

		
			// Agregar mensaje transmitiendo solicitudes
			//Ext.getCmp("panelPreacuseCargaArchivo").el.unmask();
			Ext.getCmp('botonesTransmitir').hide();
			pnl.el.unmask();
			// Mostrar panel de preacuse
			pnl.add(panelAcuseCargaArchivo);
			pnl.doLayout();
			Ext.getCmp("panelAcuseCargaArchivo").show();
			
			// Mostrar el folio de la validacion
		
			
			// Resetear cualquier url de archivo que pudiera existir anteriormente
			Ext.getCmp("botonImprimirPDF").urlArchivo = "";
			
			// Cargar resultados
			Ext.StoreMgr.key('registrosAgregadosDataStore').loadData(respuesta.registrosAgregadosDataArray);
			acuse=respuesta.acuse;
		} else if(	estado == "FIN"								){
			
			// Determinar el estado siguiente... ir a la pantalla de inicio
			var forma 		= Ext.getDom('formAux');
			forma.action 	= "13forma11Ext.data.jsp";
			forma.target	= "_self";
			forma.submit();
 
		}
		
		return;
		
	}
 
	//-------------------------- 7. PANEL ACUSE CARGA CUENTAS -------------------------
	
	var procesarSuccessFailureGeneraArchivoPDF =  function(opts, success, response) {
 
		var botonImprimirPDF = Ext.getCmp('botonImprimirPDF');
		botonImprimirPDF.enable();
		
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			botonImprimirPDF.setIconClass('icoPdf');
			botonImprimirPDF.setText('Descargar PDF');
			
			botonImprimirPDF.urlArchivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			botonImprimirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		
		} else {
			 
			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						botonImprimirPDF.setIconClass('icoImprimir');
						botonImprimirPDF.setText('Imprimir PDF');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				botonImprimirPDF.setIconClass('icoImprimir');
				botonImprimirPDF.setText('Imprimir PDF');
				NE.util.mostrarConnError(response,opts);
			}
 
		}
 
	}
	
	var procesarConsultaRegistrosAgregados = function(store, registros, opts){
		
		var gridRegistrosAgregados 			= Ext.getCmp('gridRegistrosAgregados');
		
		if (registros != null) {
			
			var el 							= gridRegistrosAgregados.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
				
			}
			
		}
		
	}
	
	var registrosAgregadosData = new Ext.data.ArrayStore({
			
		storeId: 'registrosAgregadosDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosAgregados,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosAgregados(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenidoGridAcuse = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: left !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;
		
	}
	
	var elementosAcuseCargaArchivo = [
	
		{
			xtype: 	'label',
			id:	 	'labelAcuseCifrasControl',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Datos del Acuse'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		173,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			registrosAgregadosData,
						xtype: 			'grid',
						id:				'gridRegistrosAgregados',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '80%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		185,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								align:		'right',
								renderer: 	renderContenidoGridAcuse
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					text: 		'Imprimir PDF',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoImprimir',
					id: 			'botonImprimirPDF',
					urlArchivo: '',
					handler:    function(boton, evento) {
 
						if(Ext.isEmpty(boton.urlArchivo)){
							
							// Cambiar icono
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							// Genera Archivo PDF
							Ext.Ajax.request({
								url: 			'13forma11ExtPDF.jsp',
								params: 		{ 
									acuse:				acuse,
									acuseFormateado:				registrosAgregadosData.getAt(0).get('CONTENIDO'),
									fecha:				registrosAgregadosData.getAt(1).get('CONTENIDO'),
									hora:				registrosAgregadosData.getAt(2).get('CONTENIDO')
									
								},
								callback: 	procesarSuccessFailureGeneraArchivoPDF
							});
							
						// Descargar el archivo generado	
						} else {
							
							var forma 		= Ext.getDom('formAux');
							forma.action 	= boton.urlArchivo;
							forma.submit();
							
						}
						
					},
					style: {
						width: 137
					}
				},
				{
					xtype: 		'button',
					text: 		'Salir',
					width: 		137,
					margins: 	' 3',
					iconCls: 	'icoAceptar',
					id: 			'botonSalir',
					handler:    function(boton, evento) {
						location.reload();
					},
					style: {
						width: 137
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.claveProceso',
        id: 	'panelAcuseCargaArchivo.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.totalRegistros',
        id: 	'panelAcuseCargaArchivo.totalRegistros'
      },
      // INPUT HIDDEN: MONTO TOTAL
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.montoTotal',
        id: 	'panelAcuseCargaArchivo.montoTotal'
      },
      // INPUT HIDDEN: MONTO TOTAL A PAGAR
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.montoTotalPagar',
        id: 	'panelAcuseCargaArchivo.montoTotalPagar'
      },
      // INPUT HIDDEN: NUMERO DEL MES
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.claveMes',
        id: 	'panelAcuseCargaArchivo.claveMes'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.numeroRegistros',
        id: 	'panelAcuseCargaArchivo.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE SALDOS DE FIN DE MES
		{  
        xtype:	'hidden',  
        name:	'panelAcuseCargaArchivo.sumatoriaSaldosFinMes',
        id: 	'panelAcuseCargaArchivo.sumatoriaSaldosFinMes'
      }
	];
	
	var panelAcuseCargaArchivo = {
		title:			'Acuse de la Carga',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelAcuseCargaArchivo',
		width: 			650, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosAcuseCargaArchivo
	}
	
	//----------------------------- 6. PANEL PREACUSE CARGA ---------------------------
	
	var procesarConsultaSolicitudesPorAgregar = function(store, registros, opts){
		
		var gridSolicitudesPorAgregar 			= Ext.getCmp('gridSolicitudesPorAgregar');
		
		if (registros != null) {
			
			var el 							= gridSolicitudesPorAgregar.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('Se present� un error al leer los par�metros', 'x-mask');
				
			}
			
		}
		
	}
	
	var registrosPorAgregarData = new Ext.data.ArrayStore({
			
		storeId: 'registrosPorAgregarDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'DESCRIPCION',  mapping: 0 },
			{ name: 'CONTENIDO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaSolicitudesPorAgregar,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaSolicitudesPorAgregar(null, null, null);						
				}
			}
		}
		
	});
	
	var renderContenidoGridPreacuse = function(value, metaData, record, rowIndex, colIndex, store) {
		
		if( record.get('DESCRIPCION') == 'Tipo de Operaci�n' ){
			metaData.style += 'text-align: left !important';
		} else {
			metaData.style += 'text-align: right !important';
		}
		return value;
		
	}
	
	var elementosPreacuseCargaArchivo = [
		{
			xtype: 	'label',
			id:	 	'labelPreacuseCargaArchivo',
			cls:		'x-form-item',
			style: 	'font-weight:bold;text-align:center;margin:14px;',
			html:  	'Cifras de Control'
		},
		{
			xtype: 		'panel',
			//width: 	465, // Como no se puede utilizar anchor para expresar un porcentaje...
			height:		95,
			style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '100%'
			},
			layout:		'border', // Centrar una tabla con borderlayout, si no es imposible desaparecer los scrolls de la tabla
			renderHidden: true,
			items: [
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'west'
					},
					{
						store: 			registrosPorAgregarData,
						xtype: 			'grid',
						id:				'gridSolicitudesPorAgregar',
						stripeRows: 	true,
						loadMask: 		true,
						// width: 		435, // Como no se puede utilizar anchor para expresar un porcentaje...
						autoHeight: 	true,
						hideHeaders: 	true,
						region: 			'center',
						style: {
							borderWidth: 1,
							width: '80%'
						},
						viewConfig: {
							autoFill: 		true,
							scrollOffset:	0
						},
						columns: [
							{
								header: 		'Descripcion',
								tooltip: 	'Descripcion',
								dataIndex: 	'DESCRIPCION',
								sortable: 	true,
								resizable: 	true,
								width: 		185,
								hidden: 		false,
								hideable:	false,
								fixed:		true
							},
							{
								header: 		'Contenido',
								tooltip: 	'Contenido',
								dataIndex: 	'CONTENIDO',
								sortable: 	true,
								resizable: 	true,
								//width: 		285,
								hidden: 		false,
								hideable:	false,
								//align:		'right',
								renderer: 	renderContenidoGridPreacuse
							}
						]
					},
					{
						xtype: 	'box',	
						style: { // Como no se puede utilizar anchor para expresar un porcentaje...
							width: '10%'
						},
						region:	'east'
					}
			]
		},
		{
			xtype: 		'panel',
			style: 		'margin: 0px;padding-top:14px;padding-bottom:14px;',
			layout: {
				type: 	'hbox',
				pack: 	'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonPreacuseCancelar',
					margins: 	' 3',
					handler:    function(boton, evento) {
						
						Ext.MessageBox.confirm(
							'Confirm', 
							"�Esta usted seguro de cancelar la operaci�n?", 
							function(confirmBoton){   
								if( confirmBoton == "yes" ){
									cargaArchivo("FIN",null);
								}
							}
						);
						
					},
					style: {
						width: 100
					}
				},
				{
					xtype: 		'button',
					//width: 		80,
					text: 		'Transmitir Registros',
					iconCls: 	'icoContinuar',
					id: 			'botonTransmitirRegistros',
					margins: 	' 3',
					handler:    function(boton, evento) {
												
						// Si no hay firma, cancelar la operacion
						if( Ext.isEmpty(pkcs7) ){
							return;
						// Transmitir registros
						} else {
							var respuesta 					= new Object();
							respuesta['pkcs7'] 			= pkcs7;
							respuesta['textoFirmado'] 	= textoFirmar;
							
							cargaArchivo("TRANSMITIR_REGISTROS",respuesta);
						}
						
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.claveProceso',
        id: 	'panelPreacuseCargaArchivo.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.totalRegistros',
        id: 	'panelPreacuseCargaArchivo.totalRegistros'
      },
      // INPUT HIDDEN: MONTO TOTAL
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.montoTotal',
        id: 	'panelPreacuseCargaArchivo.montoTotal'
      },
      // INPUT HIDDEN: MONTO TOTAL A PAGAR 
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.montoTotalPagar',
        id: 	'panelPreacuseCargaArchivo.montoTotalPagar'
      },
      // INPUT HIDDEN: NUMERO DEL MES 
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.claveMes',
        id: 	'panelPreacuseCargaArchivo.claveMes'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD 
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.numeroRegistros',
        id: 	'panelPreacuseCargaArchivo.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE SALDOS DE FIN DE MES 
      {  
        xtype:	'hidden',  
        name:	'panelPreacuseCargaArchivo.sumatoriaSaldosFinMes',
        id: 	'panelPreacuseCargaArchivo.sumatoriaSaldosFinMes'
      }
	];

	var panelPreacuseCargaArchivo = {
		title:			'Pre-Acuse',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelPreacuseCargaArchivo',
		width: 			550, //949,
		frame: 			true,
		style: 			'margin: 0 auto;',
		items: 			elementosPreacuseCargaArchivo
	}
	
	//---------------------------- 3. PANEL LAYOUT DE CARGA ------------------------------
	
	var hidePanelLayoutDeCarga = function(){
		Ext.getCmp('panelLayoutDeCarga').hide();
		Ext.getCmp('espaciadorPanelLayoutDeCarga').hide();
	}
	
	showPanelLayoutDeCarga = function(){
		Ext.getCmp('panelLayoutDeCarga').show();
		Ext.getCmp('espaciadorPanelLayoutDeCarga').show();
	}
	                             
	var elementosLayoutDeCarga = [
		{
			xtype: 	'label',
			id:		'labelLayoutCarga',
			name:		'labelLayoutCarga',
			html: 	'',
			cls:		'x-form-item',
			style: {
				width: 			'100%',
				marginBottom: 	'10px', 
				textAlign:		'center',
				color:			'#ff0000'
			}
		}
	];
	
	var panelLayoutDeCarga = {
		xtype:			'panel',
		id: 				'panelLayoutDeCarga',
		hidden:			true,
		width: 			700,
		title: 			'Descripci�n del Layout de Carga',
		frame: 			true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		items: 			elementosLayoutDeCarga,
		tools: [
			{
				id:			'close',
				handler: function(event, toolEl, panel){
					hidePanelLayoutDeCarga();
				}
    		}
    	],
      toolTemplate: new Ext.XTemplate(
        '<tpl>',
            '<div class="x-tool x-tool-{id}">&#160;</div>',
        '</tpl>'
      )
	}
	
	var espaciadorPanelLayoutDeCarga = {
		xtype: 	'box',
		id:		'espaciadorPanelLayoutDeCarga',
		hidden:	true,
		height: 	10,
		width: 	800
	}
	
	//------------------------- 5. PANEL RESULTADOS VALIDACION ---------------------------
 
	var procesarConsultaRegistrosSinErrores 	= function(store, registros, opts){
		
		var gridRegistrosSinErrores 						= Ext.getCmp('gridRegistrosSinErrores');
		
		if (registros != null) {
			
			var el 							= gridRegistrosSinErrores.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaRegistrosConErrores = function(store, registros, opts){
		
		var gridRegistrosConErrores						= Ext.getCmp('gridRegistrosConErrores');
		
		if (registros != null) {
			
			var el 							= gridRegistrosConErrores.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var procesarConsultaErroresVsCifrasControl = function(store, registros, opts){
		
		var gridErroresVsCifrasControl = Ext.getCmp('gridErroresVsCifrasControl');
		
		if (registros != null) {
			
			var el 							= gridErroresVsCifrasControl.getGridEl();	
			
			if(store.getTotalCount() > 0) {
				
				el.unmask();
				
			} else {
				
				el.mask('No se encontr� ning�n registro', 'x-mask');
				
			}
			
		}
		
	}
	
	var registrosSinErroresData = new Ext.data.ArrayStore({
			
		storeId: 'registrosSinErroresDataStore',
		autoDestroy: true,
		fields:  [
			{ name: 'NUMERO_LINEA',   			mapping: 0, type: 'int' },
			{ name: 'CLAVE_FINANCIAMIENTO', 	mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosSinErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosSinErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var registrosConErroresData = new Ext.data.ArrayStore({
			
		storeId: 'registrosConErroresDataStore',
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' },
			{ name: 'DESCRIPCION', 		mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaRegistrosConErrores,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaRegistrosConErrores(null, null, null);						
				}
			}
		}
		
	});
	
	var erroresVsCifrasControlData  = new Ext.data.ArrayStore({
			
		storeId: 'erroresVsCifrasControlDataStore',
		fields:  [
			{ name: 'NUMERO_LINEA',   	mapping: 0, type: 'int' },
			{ name: 'DESCRIPCION', 		mapping: 1 }
		],
		idIndex: 		0,
		autoLoad: 		false,
		listeners: {
			load: 	procesarConsultaErroresVsCifrasControl,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaErroresVsCifrasControl(null, null, null);						
				}
			}
		}
		
	});

	var elementosResultadosValidacion = [
		{ 
			xtype: 			'tabpanel',
			id:				'tabPanelResultadosValidacion',
			activeTab:		0,
			plain:			true,
			height: 			400,
			bodyStyle:		'padding:10px',
			items: [
				{
					xtype:		'container',
					title: 		'Registros sin Errores',
					layout:		'vbox',
               /*layout: {			// Nota: usando esta configuracion flex no funciona bien
						type:'vbox',
                  align:'stretch'
					},
					*/
					items: [
						{
							store: 		registrosSinErroresData,
							xtype: 		'grid',
							id:			'gridRegistrosSinErrores',
							stripeRows: true,
							loadMask: 	true,	
							frame: 		false,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'Claves de Financiamiento',
									tooltip: 	'Claves de Financiamiento',
									dataIndex: 	'CLAVE_FINANCIAMIENTO',
									sortable: 	true,
									resizable: 	true,
									width: 		675,
									hidden: 		false
								}
							]					
						},
						{
							xtype: 		'panel',
							layout:		'form',
							labelWidth: 260,
							style:		"padding:8px;",
							items: [
								// TEXTFIELD Total de Registros
							]
						}
					]
				},
				{
					xtype:		'container',
					title: 		'Registros con Errores',
					layout:		'vbox',
					 /*layout: {			// Nota: usando esta configuracion flex no funciona bien
						type:'vbox',
                  align:'stretch'
					},
					*/
					items: [
						{
							store: 		registrosConErroresData,
							xtype: 		'grid',
							id:			'gridRegistrosConErrores',
							stripeRows: true,
							loadMask: 	true,	
							frame: 		false,
							flex:			1,
							columns: [
								{
									header: 		'Linea',
									tooltip: 	'Linea',
									dataIndex: 	'NUMERO_LINEA',
									sortable: 	true,
									resizable: 	true,
									width: 		50,
									hidden: 		true,
									hideable:	false
								},
								{
									header: 		'Descripci�n',
									tooltip: 	'Descripci�n',
									dataIndex: 	'DESCRIPCION',
									sortable: 	true,
									resizable: 	true,
									width: 		1150,
									hidden: 		false
								}
							]
						},
						{
							xtype: 		'container',
							layout:		'form',
							hidden:		true,
							labelWidth: 260,
							style:		"padding:8px;",
							items: [
								// TEXTFIELD Total de Registros
								
							]
						}
					]
				}
			]
		},
		{
			xtype: 			'container',
			width: 			'100%',
			style: 			'margin: 8px;padding-top:15px;',
			renderHidden: 	true,
			layout: {
				type: 		'hbox',
				pack: 		'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		80,
					text: 		'Registros con Error',
					id: 			'registrosError',
					hidden:		true,
					margins: 	' 3',
					handler:    function(boton, evento) {
							procesarArchivoSuccess();
					},
					style: {
						width: 	80
					}
				}
			] 
		},
		{
			xtype: 			'container',
			width: 			'100%',
			style: 			'margin: 8px;padding-top:15px;',
			renderHidden: 	true,
			layout: {
				type: 		'hbox',
				pack: 		'center'
			},
			items: [
				
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'botonCancelarInsercion',
					margins: 	' 3',
					handler:    function(boton, evento) {
						location.reload();
					},
					style: {
						width: 	100
					}
				},
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Continuar Carga',
					iconCls: 	'icoContinuar',
					id: 			'botonContinuarCarga',
					margins: 	' 3',
					handler:    function(boton, evento) {
						cargaArchivo("PRESENTAR_PREACUSE",null);
					},
					style: {
						width: 100
					}
				}
			] 
		},
		// INPUT HIDDEN: NUMERO DE PROCESO
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.claveProceso',
        id: 	'panelResultadosValidacion.claveProceso'
      },
      // INPUT HIDDEN: NUMERO TOTAL DE REGISTROS
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.totalRegistros',
        id: 	'panelResultadosValidacion.totalRegistros'
      },
      // INPUT HIDDEN: MONTO TOTAL
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.montoTotal',
        id: 	'panelResultadosValidacion.montoTotal'
      },
      // INPUT HIDDEN: MONTO TOTAL A PAGAR
		{  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.montoTotalPagar',
        id: 	'panelResultadosValidacion.montoTotalPagar'
      },
      // INPUT HIDDEN: NUMERO DEL MES
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.claveMes',
        id: 	'panelResultadosValidacion.claveMes'
      },
      // INPUT HIDDEN: N�MERO DE REGISTROS QUE CONTIENE LA SOLICITUD
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.numeroRegistros',
        id: 	'panelResultadosValidacion.numeroRegistros'
      },
      // INPUT HIDDEN: SUMATORIA DE SALDOS DE FIN DE MES
      {  
        xtype:	'hidden',  
        name:	'panelResultadosValidacion.sumatoriaSaldosFinMes',
        id: 	'panelResultadosValidacion.sumatoriaSaldosFinMes'
      }
 
	];
	
	var panelResultadosValidacion = {
		title:			'Resultado de la Validaci�n',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosValidacion',
		width: 			700, //949,
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 10px',
		items: 			elementosResultadosValidacion
	};

	var espaciadorPanelResultadosValidacion = {
		xtype: 	'box',
		id:		'espaciadorPanelResultadosValidacion',
		hidden:	true,
		height: 	7,
		width: 	800
	}
	
	//---------------------------- PANEL AVANCE VALIDACION ------------------------------
	
	var elementosAvanceValidacion = [
		{
			xtype: 	'label',	
			cls: 		'x-form-item',
			style: {
				textAlign: 		'left',
				paddingBottom: '15px'
			},
			text: 	'Revisando solicitudes...'
		},
		{
			xtype: 'progress',
			id:	 'barrarAvanceValidacion',
			name:	 'barrarAvanceValidacion',
			cls:	 'center-align',
			value: 0.3756,
			text:  '37.56 %'
		}
	];
	
	var panelAvanceValidacion = new Ext.Panel({
		xtype:			'panel',
		id: 				'panelAvanceValidacion',
		frame: 			true,
		width:			500,
		height: 			134,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding: 20px',
		items: 			elementosAvanceValidacion
	});
	
	var showWindowAvanceValidacion = function(){
		
		var barrarAvanceValidacion		= Ext.getCmp("barrarAvanceValidacion");
 
		barrarAvanceValidacion.updateProgress(0.00,"0.00 %",true);
		barrarAvanceValidacion.wait({
            interval:	200,
            increment:	15
      });
 
		var ventana = Ext.getCmp('windowAvanceValidacion');
		if (ventana) {
			ventana.show();
		} else {
			
			new Ext.Window({
				title:			'Estatus del Proceso',
				layout: 			'fit',
				width:			300,
				resizable:		false,
				id: 				'windowAvanceValidacion',
				modal:			true,
				closable: 		false,
				items: [
					Ext.getCmp("panelAvanceValidacion")
				]
			}).show();
			
		}
		
	}	

	var hideWindowAvanceValidacion = function(){
		
		var ventanaAvanceValidacion = Ext.getCmp('windowAvanceValidacion');
		
		if(ventanaAvanceValidacion && ventanaAvanceValidacion.isVisible() ){
			var barrarAvanceValidacion	 = Ext.getCmp("barrarAvanceValidacion");
			barrarAvanceValidacion.reset(); 
			
			
			if (ventanaAvanceValidacion) {
				ventanaAvanceValidacion.hide();
			}
      }
      
	}
 
	//-------------------------- 4. FORMA DE CARGA DE ARCHIVO ---------------------------
	
	var confirmarAcuse = function(pkcs7, vtextoFirmar, vproceso,  vgranTotalMontoMN ,  vgranTotalMontoDL, vnumTotalPrestamos){
	
		if (Ext.isEmpty(pkcs7)) {
			return;
		}else  {
			Ext.Ajax.request({
				url: 												'13forma11Ext.data.jsp',
				params: {	
					informacion: 'CargaArchivo.transmitirRegistros',
					pkcs7: 		pkcs7,
					numProceso:		vproceso,
					granTotalMontoMN:  vgranTotalMontoMN,
					granTotalMontoDL: vgranTotalMontoDL,
					numTotalPrestamos: 	vnumTotalPrestamos,
					textoFirmado: vtextoFirmar
				},
				callback: procesaCargaArchivo
			});							
		}	
	
	}
	
	var botones = [
		{
			xtype: 'buttongroup',
			id: 'botonesTransmitir',
        columns: 3,
		  frame:false,
		  style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
			items: [
				{
					xtype: 'button',
					text: 'Transmitir Solicitudes',
					id: 'btnAgregarD',
					width: 	130,
					handler: function(){
					
									
						//pnl.el.mask("Generando Acuse...","x-mask-loading");
						
						NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar, proceso,  granTotalMontoMN ,  granTotalMontoDL, numTotalPrestamos   );
						
						
					}
					
				},
				{
					xtype: 'displayfield',
					value: '   ',
					id: 'tituloC', 
					width: 80
				},
				{
					xtype: 'button',
					text: 'Cancelar',
					iconCls: 'Cancelar',
					id: 'btnLimpiarD',			
					width: 	75,
					handler: function() {
					
					Ext.MessageBox.confirm(
							'Confirm', 
							"�Esta usted seguro de cancelar la operaci�n?", 
							function(confirmBoton){   
								if( confirmBoton == "yes" ){
									location.reload();
								}
							}
						);
						 
				}
				}
			]
		}
	];
 
	var elementosFormaCargaArchivo = [
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivo',
		  name: 			'archivo',
		  emptyText: 	'Seleccione...',
		  fieldLabel: 	"Ruta del Archivo de Registros", 
		  buttonText: 	null,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'-20'
		  //vtype: 		'archivotxt'
		},
		{
			xtype: 		'panel',
			anchor: 		'100%',
			style: {
				marginTop: 		'10px'
			},
			layout: {
				type: 'hbox',
				pack: 'end',
				align: 'middle'
			},
			items: [
				{
					xtype: 			'button',
					text: 			'Continuar',
					iconCls: 		'icoContinuar',
					id: 				'botonContinuarCargaArchivo',				
					handler:    function(boton, evento) {
						
						// Revisar si la forma es invalida
						var forma = Ext.getCmp("panelFormaCargaArchivo").getForm();
						if(!forma.isValid()){
							return;
						}
	 
						// Validar que se haya especificado un archivo
						var archivo = Ext.getCmp("archivo");
						if( Ext.isEmpty( archivo.getValue() ) ){
							archivo.markInvalid("Debe especificar un archivo");
							return;
						}
						
						// Revisar el tipo de extension del archivo
						var myRegexTXT = /^.+\.([tT][xX][tT])$/;
						var myRegexDOT = /^.+\.([dD][oO][tT])$/;
						var myRegexXLS = /^.+\.([xX][lL][sS])$/;
						var myRegexPDF = /^.+\.([pP][dD][fF])$/;
						var myRegexDOC = /^.+\.([dD][oO][cC])$/;
						var myRegexPPT = /^.+\.([pP][pP][tT])$/;
						var myRegexCSV = /^.+\.([cC][sS][vV])$/;
						
						var nombreArchivo 	= archivo.getValue();
						var numeroRegistros 	= Ext.getCmp("numeroRegistros1").getValue();
						 if( !myRegexTXT.test(nombreArchivo)&&!myRegexDOT.test(nombreArchivo)
						 &&!myRegexXLS.test(nombreArchivo)&& !myRegexPDF.test(nombreArchivo)
						 && !myRegexDOC.test(nombreArchivo)&& !myRegexPPT.test(nombreArchivo)
						 && !myRegexCSV.test(nombreArchivo) ) {
							archivo.markInvalid("El formato del archivo de origen no es el correcto para el tipo de archivo seleccionado");
							return;
						}
						
						// Cargar archivo
						cargaArchivo("SUBIR_ARCHIVO",null);

					}
				}
			]
		},
		{
			xtype: 	'hidden',
			name:		'claveMes1',
			id:		'claveMes1'
		},
		{
			xtype: 	'hidden',
			name:		'numeroRegistros1',
			id:		'numeroRegistros1'
		},
		{
			xtype: 	'hidden',
			name:		'sumatoriaSaldosFinMes1',
			id:		'sumatoriaSaldosFinMes1'
		}
	];
	
	var panelFormaCargaArchivo = new Ext.form.FormPanel({
		id: 				'panelFormaCargaArchivo',
		width: 			700,
		title: 			'Carga de Archivo',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		fileUpload: 	true,
		hidden:			false,
		renderHidden:	true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth: 	190,
		defaultType: 	'textfield',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementosFormaCargaArchivo,
		monitorValid: 	false
	});
 
	//-------------------------------Grids de Pre-Acuse --------------------------------
	
 
	//------------------------------------ 1. PANEL AVISOS ---------------------------------------
	

	//-------------------------------- CONTENEDOR PRINCIPAL -----------------------------------			
	var pnl = new Ext.Container({
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		disabled: 	false,
		items: 	[
			
			panelFormaCargaArchivo,
			espaciadorPanelResultadosValidacion,
			panelResultadosValidacion,
			espaciadorPanelLayoutDeCarga,
			panelLayoutDeCarga,
			panelPreacuseCargaArchivo
			
			
		]
	});

	//---------------------------- ACCIONES DE INICIALIZACION -----------------------------	
	cargaArchivo("ESPERAR_CAPTURA_SOLICITUD_DE_CARGA",null);
 
});;