Ext.onReady(function() {
//VAR GLOBALES------------------------------------------------------------------
	var mostrarAcuse = 'N';
	var asignaPymes = 'N';
//HANDLERS----------------------------------------------------------------------
	//FUNCION: despues de cargar la informacion del grid, realiza la vaidacion en
	//base a la parametrizacion y tipo de usuario, para determinar los campos a mostrar
	var procesarConsultaTasas = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			var comboEpo = Ext.getCmp('cboEpo1');
			var recordEpo = comboEpo.findRecord(comboEpo.valueField, comboEpo.getValue());
			var cboMoneda = Ext.getCmp('cboMoneda1');
			var recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());
			
			
			gridOfertaTasas.setTitle(recordEpo.get(comboEpo.displayField)+' - '+recordMoneda.get(cboMoneda.displayField));
			if (!gridOfertaTasas.isVisible()) {
				contenedorPrincipalCmp.add(gridOfertaTasas);
				contenedorPrincipalCmp.doLayout();
			}
			
			var el = gridOfertaTasas.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('mensajes1').hide();
				Ext.getCmp('btnEliminarRango').enable();
				Ext.getCmp('btnAsignaPymes').enable();
				Ext.getCmp('btnCambiaEstatus').enable();
				if(mostrarAcuse=='S'){
					var registrosEnviar = [];
					
					storeTasasData.each(function(record) {
						registrosEnviar.push(record);
					});
					
					storeAcuseTasas.add(registrosEnviar);
					mostrarAcuse='N';
					
					gridOfertaTasas.hide();
					fp.hide();
					
					gridAcuOfertaTasas.setTitle(gridOfertaTasas.title);
					contenedorPrincipalCmp.add(gridAcuOfertaTasas);
					contenedorPrincipalCmp.doLayout();
					gridAcuseEncabezado.show();
				}
				
				if(asignaPymes=='S'){
					var registrosEnviar = [];
					var toolAcuTasas = Ext.getCmp('toolAcuTasas1');
					
					storeTasasData.each(function(record) {
						registrosEnviar.push(record);
					});
					
					storeAcuseTasas.add(registrosEnviar);
					asignaPymes='N';
					
					gridOfertaTasas.hide();
					toolAcuTasas.hide();
					fp.hide();
					
					gridAcuOfertaTasas.setTitle(gridOfertaTasas.title);
					contenedorPrincipalCmp.add(gridAcuOfertaTasas);
					contenedorPrincipalCmp.add(fpPymes);
					contenedorPrincipalCmp.doLayout();
				}
				
			}else{
				Ext.getCmp('mensajes1').show();
				Ext.getCmp('btnEliminarRango').disable();
				Ext.getCmp('btnAsignaPymes').disable();
				//Ext.getCmp('btnGuardarRango').disable();
				Ext.getCmp('btnCambiaEstatus').disable();
				onAgregarRangoTasa();
			}
		}
	}
	
	var procesarSuccessSaveRangos = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var ObjJsonb = Ext.util.JSON.decode(response.responseText);
			var msgAcuse = ObjJsonb.msgExito;
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			mostrarAcuse = 'S';
			
			var acuseGral = [
				
				['N�mero de Acuse', ObjJsonb.acuse],
				['Fecha Autorizaci�n', ObjJsonb.fecha],
				['Hora Autorizaci�n', ObjJsonb.hora],
				['Usuario de Captura', ObjJsonb.usuario]
			];
			
			storeAcuseEncData.loadData(acuseGral);
			radioTipoTasa.hide();
			gridAcuseEncabezado.setTitle('N�mero de Recibo: '+ ObjJsonb.recibo);
			contenedorPrincipalCmp.insert(0,gridAcuseEncabezado);
			contenedorPrincipalCmp.doLayout();
			
			Ext.MessageBox.alert('Aviso',msgAcuse);
			storeTasasData.load({
				params: Ext.apply(fp.getForm().getValues())
			});
		}else{
			NE.util.mostrarConnError(response,opts);
		}
		
	}
	
	var procesarSuccessDeleteRangos = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var ObjJsonb = Ext.util.JSON.decode(response.responseText);
			var msgAcuse = ObjJsonb.msgExito;
			//mostrarAcuse = 'S';
			Ext.MessageBox.alert('Aviso',msgAcuse);
			storeTasasData.load({
				params: Ext.apply(fp.getForm().getValues())
			});
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessCambiaEstatusRangos = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var ObjJsonb = Ext.util.JSON.decode(response.responseText);
			var msgAcuse = ObjJsonb.msgExito;
			//mostrarAcuse = 'S';
			Ext.MessageBox.alert('Aviso',msgAcuse);
			storeTasasData.load({
				params: Ext.apply(fp.getForm().getValues())
			});
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessSavePymes = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var ObjJsonb = Ext.util.JSON.decode(response.responseText);
			var msgAcuse = ObjJsonb.msgExito;
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var regAsignPymes = [];
			//mostrarAcuse = 'S';}
			pnl.el.unmask();
			Ext.MessageBox.alert('Aviso',msgAcuse);
			
			var acuseGral = [
				
				['N�mero de Acuse', ObjJsonb.acuse],
				['Fecha Autorizaci�n', ObjJsonb.fecha],
				['Hora Autorizaci�n', ObjJsonb.hora],
				['Usuario de Captura', ObjJsonb.usuario]
			];
			
			storeAcuseEncData.removeAll(true);
			storeAcuseEncData.loadData(acuseGral);
			gridAcuseEncabezado.setTitle('N�mero de Recibo: '+ ObjJsonb.recibo);
			
			storeAsignaPymeData.each(function(record) {
				if(record.data['PARAMTASAS'] != record.data['SELECCION']){
					regAsignPymes.push(record);
				}
			});
			
			storeAcusePymesData.add(regAsignPymes);
			
			radioTipoTasa.hide();
			fpPymes.hide();
			gridAsignaPymes.hide();
			gridAcuseEncabezado.show();
			
			if(contenedorPrincipalCmp.find('id','gridAcuseEncabezado1')=='')
				contenedorPrincipalCmp.insert(0,gridAcuseEncabezado);
			
			contenedorPrincipalCmp.add(gridAcuseAsigPymes);
			contenedorPrincipalCmp.doLayout();
			
			
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var onInicio = function(){
		window.location.href='13ofertatasas1ext.jsp';
	}
	
	//Agrega rango de montos para una oferta de tasa
	var onAgregarRangoTasa = function(){
		var storeRango = gridOfertaTasas.getStore();
		var columnModelGrid = gridOfertaTasas.getColumnModel();
		var correcto = true;
		
		storeRango.each(function(record) {
			var numRegistro = storeRango.indexOf(record);
			var montoI = (record.data['MONTODESDE']==undefined)?'':record.data['MONTODESDE'];
			var montoF = (record.data['MONTOHASTA']==undefined)?'':record.data['MONTOHASTA'];
			var puntos = (record.data['PUNTOSPREF']==undefined)?'':record.data['PUNTOSPREF'];
		
			if(montoI=='' || montoF=='' || puntos==''){
				var txtColumn = (montoI=='')?'MONTODESDE':(montoF=='')?'MONTOHASTA':(puntos=='')?'PUNTOSPREF':'';
				Ext.MessageBox.alert('Aviso!','Faltan Datos!',
					function(){
						gridOfertaTasas.startEditing(numRegistro, columnModelGrid.findColumnIndex(txtColumn));
					}
				);
				correcto = false;
				return false;
			}else if(montoI >= montoF){
				Ext.MessageBox.alert('Error Rangos','El Monto Publicado DESDE $'+montoI+' debe ser menor al Monto Pubicado HASTA $'+montoF,
					function(){
						gridOfertaTasas.startEditing(numRegistro, columnModelGrid.findColumnIndex('MONTOHASTA'));
					}
				);
				correcto = false;
				return false;
			}else	if(numRegistro>0){//validacion de rangos consecutivos
				var oldRecord = storeRango.getAt(numRegistro-1);
		
				if( (oldRecord.data['MONTOHASTA']+.01)!=montoI ){
					Ext.MessageBox.alert('Error en Rangos','Verifique que el monto "Desde" sea consecutivos con el monto anterior "Hasta".',
						function(){
							gridOfertaTasas.startEditing(numRegistro, columnModelGrid.findColumnIndex('MONTODESDE'));
						}
					);
					correcto = false;
					return false;
				}else			
				if( puntos == 0.0){
					Ext.MessageBox.alert('Error en Rangos','Puntos Preferenciales deben ser mayor a cero',
						function(){
							gridOfertaTasas.startEditing(numRegistro, columnModelGrid.findColumnIndex('PUNTOSPREF'));
						}
					);
					correcto = false;
					return false;
				}
			}
		});
		
		if(correcto){
			var RangoTasa = Ext.data.Record.create([
				{name:'CVETASA'},
				{name:'CVEIF'},
				{name:'CVEEPO'},
				{name:'TIPOTASA'},
				{name:'MONTODESDE', type: 'float'},
				{name:'MONTOHASTA', type: 'float'},
				{name:'PUNTOSPREF', type: 'float'},
				{name:'ESTATUS'},
				{name:'CVEMONEDA'},
				{name:'ACUSE'}
			]);
				
			gridOfertaTasas.getStore().add(new RangoTasa());
			if(gridOfertaTasas.getStore().getCount()>1){
				Ext.getCmp('mensajes1').hide();
			}
		}

	}
	
	
	var confirmar = function(pkcs7, textoFirmar, registrosEnviar){
	
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else{
		
			registrosEnviar = Ext.encode(registrosEnviar);
						
			Ext.Ajax.request({
				url : '13ofertatasas1ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					registros : registrosEnviar,
					informacion:'guardarOfertaTasas',
					Pkcs7: pkcs7,
					TextoFirmado: textoFirmar
				}),
				callback: procesarSuccessSaveRangos
			});
		}
	}
	
	
	
	var onGuardarRangos = function(){
		var storeRango = gridOfertaTasas.getStore();
		var columnModelGrid = gridOfertaTasas.getColumnModel();
		var registrosEnviar = [];
		var correcto = true;
		storeRango.commitChanges();
		
		var comboEpo = Ext.getCmp('cboEpo1');
		var recordEpo = comboEpo.findRecord(comboEpo.valueField, comboEpo.getValue());
		var cboMoneda = Ext.getCmp('cboMoneda1');
		var recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());
		//
		
		
		Ext.MessageBox.confirm('Confirmaci�n','Est� seguro de guardar la informaci�n', function(resp){
			
			if(resp=='yes'){
		
				var textoFirmar = recordEpo.get(comboEpo.displayField)+"\n"+
						"Moneda|Monto Desde|Monto Hasta|PuntosPref|";
						
				storeRango.each(function(record) {
					var numRegistro = storeRango.indexOf(record);
					var montoI = (record.data['MONTODESDE']==undefined)?'':record.data['MONTODESDE'];
					var montoF = (record.data['MONTOHASTA']==undefined)?'':record.data['MONTOHASTA'];
					var puntos = (record.data['PUNTOSPREF']==undefined)?'':record.data['PUNTOSPREF'];
					
					textoFirmar += "\n"+recordMoneda.get(cboMoneda.displayField)+"|"+Ext.util.Format.number(montoI,'$0,0.00')+"|"+
										Ext.util.Format.number(montoF,'$0,0.00')+"|"+Ext.util.Format.number(puntos,'0,0.0000%');
					
					
					//validacion de campos vacios
					if(montoI=='' || montoF=='' || puntos==''){
						var txtColumn = (montoI=='')?'MONTODESDE':(montoF=='')?'MONTOHASTA':(puntos=='')?'PUNTOSPREF':'';
						Ext.MessageBox.alert('Aviso!','Faltan Datos!',
							function(){
								gridOfertaTasas.startEditing(numRegistro, columnModelGrid.findColumnIndex(txtColumn));
							}
						);
						correcto = false;
						return false;
					}else if(montoI >= montoF){
						Ext.MessageBox.alert('Error Rangos','El Monto Publicado DESDE $'+montoI+' debe ser menor al Monto Pubicado HASTA $'+montoF,
							function(){
								gridOfertaTasas.startEditing(numRegistro, columnModelGrid.findColumnIndex('MONTODESDE'));
							}
						);
						correcto = false;
						return false;
					}else		
					if(numRegistro>0){//validacion de rangos consecutivos
						var oldRecord = storeRango.getAt(numRegistro-1);
						
						if( (oldRecord.data['MONTOHASTA']+.01)!=montoI ){
							Ext.MessageBox.alert('Error en Rangos','Verifique que el monto "Desde" sea consecutivos con el monto anterior "Hasta"',
								function(){
									gridOfertaTasas.startEditing(numRegistro, columnModelGrid.findColumnIndex('MONTODESDE'));
								}
							);
							correcto = false;
							return false;
						}else			
						if( puntos == 0.0){
							Ext.MessageBox.alert('Error en Rangos','Puntos Preferenciales deben ser mayor a cero',
								function(){
									gridOfertaTasas.startEditing(numRegistro, columnModelGrid.findColumnIndex('PUNTOSPREF'));
								}
							);
							correcto = false;
							return false;
						}
					}
			
					registrosEnviar.push(record.data);
					
				});
				
				if(!correcto)return;
				
				if(correcto){
					
					NE.util.obtenerPKCS7(confirmar, textoFirmar, registrosEnviar );
									
				}
			}//fin if(resp)
		});
	}
	
	var onEliminarRango = function(){
		var storeRango = gridOfertaTasas.getStore();
		var columnModelGrid = gridOfertaTasas.getColumnModel();
		var registrosEnviar = [];
		var correcto = false;
		
		
		var comboEpo = Ext.getCmp('cboEpo1');
		var recordEpo = comboEpo.findRecord(comboEpo.valueField, comboEpo.getValue());
		var cboMoneda = Ext.getCmp('cboMoneda1');
		var recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());

		var textoFirmar = recordEpo.get(comboEpo.displayField)+"\n"+
				"Moneda|Monto Desde|Monto Hasta|PuntosPref|";
		
		storeRango.each(function(record) {
			var numRegistro = storeRango.indexOf(record);
			var montoI = (record.data['MONTODESDE']==undefined)?'':record.data['MONTODESDE'];
			var montoF = (record.data['MONTOHASTA']==undefined)?'':record.data['MONTOHASTA'];
			var puntos = (record.data['PUNTOSPREF']==undefined)?'':record.data['PUNTOSPREF'];
			
			if(record.data['CVETASA'] && record.data['SELECCION']=='S'){
				textoFirmar += "\n"+recordMoneda.get(cboMoneda.displayField)+"|"+Ext.util.Format.number(montoI,'$0,0.00')+"|"+
								Ext.util.Format.number(montoF,'$0,0.00')+"|"+Ext.util.Format.number(puntos,'0,0.0000%');
				
				registrosEnviar.push(record.data);
				correcto = true;
			}
		});
		
		registrosEnviar = Ext.encode(registrosEnviar);
		
		if(correcto){
			Ext.MessageBox.confirm('Confirmaci�n','Est� seguro de eliminar la informaci�n', function(resp){
				if(resp=='yes'){
					var pkcs7 = NE.util.firmar(textoFirmar);
					if (Ext.isEmpty(pkcs7)) {
						return;	//Error en la firma. Termina...
					}else{
						Ext.Ajax.request({
							url : '13ofertatasas1ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								registros : registrosEnviar,
								informacion:'eliminarOfertaTasas',
								Pkcs7: pkcs7,
								TextoFirmado: textoFirmar
							}),
							callback: procesarSuccessDeleteRangos
						});
					}
				}
			});
		}else{
			Ext.MessageBox.alert('Aviso','No hay resgistros seleccionados');
		}
		
	}
	
	var onCambiaEstatusRango = function(){
		var storeRango = gridOfertaTasas.getStore();
		var columnModelGrid = gridOfertaTasas.getColumnModel();
		var registrosEnviar = [];
		var correcto = false;
		
		var comboEpo = Ext.getCmp('cboEpo1');
		var recordEpo = comboEpo.findRecord(comboEpo.valueField, comboEpo.getValue());
		var cboMoneda = Ext.getCmp('cboMoneda1');
		var recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());

		var textoFirmar = recordEpo.get(comboEpo.displayField)+"\n"+
				"Moneda|Monto Desde|Monto Hasta|PuntosPref|";
		
		storeRango.each(function(record) {
			var numRegistro = storeRango.indexOf(record);
			var montoI = (record.data['MONTODESDE']==undefined)?'':record.data['MONTODESDE'];
			var montoF = (record.data['MONTOHASTA']==undefined)?'':record.data['MONTOHASTA'];
			var puntos = (record.data['PUNTOSPREF']==undefined)?'':record.data['PUNTOSPREF'];
			
			if(record.data['CVETASA'] && record.data['SELECCION']=='S'){
				textoFirmar += "\n"+recordMoneda.get(cboMoneda.displayField)+"|"+Ext.util.Format.number(montoI,'$0,0.00')+"|"+
								Ext.util.Format.number(montoF,'$0,0.00')+"|"+Ext.util.Format.number(puntos,'0,0.0000%');
				registrosEnviar.push(record.data);
				
				correcto = true;
			}
		});
		
		registrosEnviar = Ext.encode(registrosEnviar);
		
		if(correcto){
			Ext.MessageBox.confirm('Confirmaci�n','Est� seguro de guardar la informaci�n', function(resp){
				if(resp=='yes'){
					var pkcs7 = NE.util.firmar(textoFirmar);
					if (Ext.isEmpty(pkcs7)) {
						return;	//Error en la firma. Termina...
					}else{
						Ext.Ajax.request({
							url : '13ofertatasas1ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								registros : registrosEnviar,
								informacion:'cambiarEstatusOfertaTasas',
								Pkcs7: pkcs7,
								TextoFirmado: textoFirmar
							}),
							callback: procesarSuccessCambiaEstatusRangos
						});
					}
				}
			});
		}else{
			Ext.MessageBox.alert('Aviso','No hay resgistros seleccionados');
		}
	}
	
	var onAsignaPymes = function(){
		gridAcuseEncabezado.hide();
		var btnGenerarArch  = Ext.getCmp('btnGenerarArch');
		var btnGenerarPDF  = Ext.getCmp('btnGenerarPDF');
		var btnSalir  = Ext.getCmp('btnSalir');
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var toolAcuTasas = Ext.getCmp('toolAcuTasas1');
		
		btnGenerarArch.disable();
		btnGenerarPDF.disable();
		btnSalir.disable();
		toolAcuTasas.hide();
		contenedorPrincipalCmp.add(fpPymes);
		contenedorPrincipalCmp.doLayout();
	}
	
	var onAsignaPymesIni = function(){
		asignaPymes = 'S';
		storeTasasData.load({
			params: Ext.apply(fp.getForm().getValues())
		});

	}
	
	var procesarConsultaAsignPymes = function(store, arrRegistros, opts) {
		var fpPyme = Ext.getCmp('fpPymes1');
		fpPyme.el.unmask();
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!gridAsignaPymes.isVisible()) {
				contenedorPrincipalCmp.add(gridAsignaPymes);
				contenedorPrincipalCmp.doLayout();
			}else{
				selectDefaultDoctos(gridAsignaPymes);
			}
			
			var el = gridAsignaPymes.getGridEl();
			if(store.getTotalCount() > 0) {
				el.unmask();
				
				if(store.getTotalCount()<=10){
					var height = 88 + (store.getTotalCount()*21);
					gridAsignaPymes.setHeight(height);
				}else{
					gridAsignaPymes.setHeight(300);
				}
				
			}else{
				el.mask('No hay Pymes en la relacion IF - EPO','x-mask')
			}
		}
	}
	
	var selectDefaultDoctos = function(grid){
		var indiceSm = 0;
		grid.getStore().each(function(record) {
			record.data['SELECCION']='N';
			if (record.data['PARAMTASAS']=='S'){
					selectModelPyme.selectRow(indiceSm,true);
			}
			indiceSm = indiceSm+1;
		});
	}
	
	
	
	var confirmarPymes = function(pkcs7, textoFirmar, regAcuseTasas, regAsignPymes){
	
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else{
		
			pnl.el.mask('Procesando...','x-mask');
			
			Ext.Ajax.request({
				url : '13ofertatasas1ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion:'guardarAsignPyme',
					registrosTasas : regAcuseTasas,
					registrosPymes : regAsignPymes,
					Pkcs7: pkcs7,
					TextoFirmado: textoFirmar
				}),
				callback: procesarSuccessSavePymes
			});
		}
	}
	
	var onGuardarAsigPyme = function(){
		var regAcuseTasas = [];
		var regAsignPymes = [];
		var actualiza = false;

		var comboEpo = Ext.getCmp('cboEpo1');
		var recordEpo = comboEpo.findRecord(comboEpo.valueField, comboEpo.getValue());
		var cboMoneda = Ext.getCmp('cboMoneda1');
		var recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());
		
		var textoFirmar = recordEpo.get(comboEpo.displayField)+"\n"+
				"Moneda|Monto Desde|Monto Hasta|PuntosPref|";
		
		storeAcuseTasas.each(function(record) {
			var numRegistro = storeAcuseTasas.indexOf(record);
			var montoI = (record.data['MONTODESDE']==undefined)?'':record.data['MONTODESDE'];
			var montoF = (record.data['MONTOHASTA']==undefined)?'':record.data['MONTOHASTA'];
			var puntos = (record.data['PUNTOSPREF']==undefined)?'':record.data['PUNTOSPREF'];
			
			textoFirmar += "\n"+recordMoneda.get(cboMoneda.displayField)+"|"+Ext.util.Format.number(montoI,'$0,0.00')+"|"+
								Ext.util.Format.number(montoF,'$0,0.00')+"|"+Ext.util.Format.number(puntos,'0,0.0000%');
			
			regAcuseTasas.push(record.data);
		});
		
		textoFirmar += "\n\nPymes";
		
		storeAsignaPymeData.each(function(record) {
			if(record.data['PARAMTASAS'] != record.data['SELECCION']){
				regAsignPymes.push(record.data);
				actualiza = true;
				
				var rfcPyme = (record.data['RFCPYME']==undefined)?'':record.data['RFCPYME'];
				var razonSocial = (record.data['RAZONSOCIAL']==undefined)?'':record.data['RAZONSOCIAL'];
				var paramTasas = (record.data['PARAMTASAS']==undefined)?'':record.data['PARAMTASAS'];
				
				textoFirmar += "\n"+rfcPyme+"|"+razonSocial+"|"+(paramTasas=='S'?'ELIMINAR':'AGREGAR');
				
			}
		});
		
		if(actualiza){
			Ext.MessageBox.confirm('Confirmaci�n','Est� seguro de guardar la informaci�n', function(resp){
				if(resp=='yes'){
						
					regAcuseTasas = Ext.encode(regAcuseTasas);
					regAsignPymes = Ext.encode(regAsignPymes);
					
					NE.util.obtenerPKCS7(confirmarPymes, textoFirmar, regAcuseTasas, regAsignPymes  );

				}	
			});
		}else{
			Ext.MessageBox.alert('Aviso','No hay cambios realizados');
		}
	}
	
	var onGenerarCsv = function(btn){
		var storeAcuEnc = gridAcuseEncabezado.getStore();
		var storeAcuTasa = gridAcuOfertaTasas.getStore();
		var registrosAcuEnc = [];
		var registrosAcuTasas = [];
		btn.setIconClass('loading-indicator');
		btn.disable();
	
		storeAcuEnc.each(function(record){
			registrosAcuEnc.push(record.data);
		});
		registrosAcuEnc = Ext.encode(registrosAcuEnc);
		
		storeAcuTasa.each(function(record){
			registrosAcuTasas.push(record.data);
		});
		registrosAcuTasas = Ext.encode(registrosAcuTasas);
		
		Ext.Ajax.request({
			url: '13ofertatasas1ext.data.jsp',
			params: {
					informacion: 'generarArchivo',
					registrosAcuEnc : registrosAcuEnc,
					registrosAcuTasas : registrosAcuTasas,
					strTitleEnc: gridAcuseEncabezado.title,
					strEpoMoneda: gridAcuOfertaTasas.title,
					tipoArchivo : 'csv'
					},
			callback: procesarSuccessFailureGenerarArchivo
		});
	}
	
	var onGenerarCsv2 = function(btn){
		var storeAcuEnc = gridAcuseEncabezado.getStore();
		var storeAcuTasa = gridAcuOfertaTasas.getStore();
		var storeAcuPymes = gridAcuseAsigPymes.getStore();
		var registrosAcuEnc = [];
		var registrosAcuTasas = [];
		var registrosAcuPymes = [];
		btn.setIconClass('loading-indicator');
		btn.disable();
	
		storeAcuEnc.each(function(record){
			registrosAcuEnc.push(record.data);
		});
		registrosAcuEnc = Ext.encode(registrosAcuEnc);
		
		storeAcuTasa.each(function(record){
			registrosAcuTasas.push(record.data);
		});
		registrosAcuTasas = Ext.encode(registrosAcuTasas);
		
		storeAcuPymes.each(function(record){
			registrosAcuPymes.push(record.data);
		});
		registrosAcuPymes = Ext.encode(registrosAcuPymes);
		
		
		Ext.Ajax.request({
			url: '13ofertatasas1ext.data.jsp',
			params: {
					informacion: 'generarArchivo',
					registrosAcuEnc : registrosAcuEnc,
					registrosAcuTasas : registrosAcuTasas,
					registrosAcuPymes : registrosAcuPymes,
					strTitleEnc: gridAcuseEncabezado.title,
					strEpoMoneda: gridAcuOfertaTasas.title,
					tipoAcuse: '2',
					tipoArchivo : 'csv'
					},
			callback: procesarSuccessFailureGenerarArchivo
		});
	}
	
	var onGenerarPdf = function(btn){
		var storeAcuEnc = gridAcuseEncabezado.getStore();
		var storeAcuTasa = gridAcuOfertaTasas.getStore();
		var registrosAcuEnc = [];
		var registrosAcuTasas = [];
		btn.setIconClass('loading-indicator');
		btn.disable();
	
		storeAcuEnc.each(function(record){
			registrosAcuEnc.push(record.data);
		});
		registrosAcuEnc = Ext.encode(registrosAcuEnc);
		
		storeAcuTasa.each(function(record){
			registrosAcuTasas.push(record.data);
		});
		registrosAcuTasas = Ext.encode(registrosAcuTasas);
		
		Ext.Ajax.request({
			url: '13ofertatasas1ext.data.jsp',
			params: {
					informacion: 'generarArchivo',
					registrosAcuEnc : registrosAcuEnc,
					registrosAcuTasas : registrosAcuTasas,
					strTitleEnc: gridAcuseEncabezado.title,
					strEpoMoneda: gridAcuOfertaTasas.title,
					tipoArchivo : 'pdf'
					},
			callback: procesarSuccessFailureGenerarArchivo
		});
	}
	
	var onGenerarPdf2 = function(btn){
		var storeAcuEnc = gridAcuseEncabezado.getStore();
		var storeAcuTasa = gridAcuOfertaTasas.getStore();
		var storeAcuPymes = gridAcuseAsigPymes.getStore();
		var registrosAcuEnc = [];
		var registrosAcuTasas = [];
		var registrosAcuPymes = [];
		btn.setIconClass('loading-indicator');
		btn.disable();
	
		storeAcuEnc.each(function(record){
			registrosAcuEnc.push(record.data);
		});
		registrosAcuEnc = Ext.encode(registrosAcuEnc);
		
		storeAcuTasa.each(function(record){
			registrosAcuTasas.push(record.data);
		});
		registrosAcuTasas = Ext.encode(registrosAcuTasas);
		
		storeAcuPymes.each(function(record){
			registrosAcuPymes.push(record.data);
		});
		registrosAcuPymes = Ext.encode(registrosAcuPymes);
		
		
		Ext.Ajax.request({
			url: '13ofertatasas1ext.data.jsp',
			params: {
					informacion: 'generarArchivo',
					registrosAcuEnc : registrosAcuEnc,
					registrosAcuTasas : registrosAcuTasas,
					registrosAcuPymes : registrosAcuPymes,
					strTitleEnc: gridAcuseEncabezado.title,
					strEpoMoneda: gridAcuOfertaTasas.title,
					tipoAcuse: '2',
					tipoArchivo : 'pdf'
					},
			callback: procesarSuccessFailureGenerarArchivo
		});
	}
	
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var tipoArchivo = Ext.util.JSON.decode(response.responseText).tipoArchivo;
			var tipoAcuse = Ext.util.JSON.decode(response.responseText).tipoAcuse
			if(tipoArchivo=="csv"){
				var btnGenerarArch;
				var btnAbrirArch;
				if(tipoAcuse=='2'){
					btnGenerarArch = Ext.getCmp('btnGenerarArch2');
					btnAbrirArch = Ext.getCmp('btnAbrirArch2');
				}else{
					btnGenerarArch = Ext.getCmp('btnGenerarArch');
					btnAbrirArch = Ext.getCmp('btnAbrirArch');
				}
				btnGenerarArch.setIconClass('');
				btnGenerarArch.disable();
				btnAbrirArch.show();
				btnAbrirArch.el.highlight('FFF700', {duration: 8, easing:'bounceOut'});
				btnAbrirArch.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
			}
			if(tipoArchivo=="pdf"){
				var btnGenerarPDF;
				var btnAbrirPDF;
				if(tipoAcuse=='2'){
					btnGenerarPDF = Ext.getCmp('btnGenerarPDF2');
					btnAbrirPDF = Ext.getCmp('btnAbrirPDF2');
				}else{
					btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
					btnAbrirPDF = Ext.getCmp('btnAbrirPDF');
				}
				btnGenerarPDF.setIconClass('');
				btnGenerarPDF.disable();
				btnAbrirPDF.show();
				btnAbrirPDF.el.highlight('FFF700', {duration: 8, easing:'bounceOut'});
				btnAbrirPDF.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//STORES------------------------------------------------------------------------
	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'catalogoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13ofertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeCatMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13ofertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var storeTasasData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13ofertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'consultaOfertaTasas'
		},
		fields: [
			{name:'CVETASA'},
			{name:'CVEIF'},
			{name:'CVEEPO'},
			{name:'TIPOTASA'},
			{name:'MONTODESDE', type: 'float'},
			{name:'MONTOHASTA', type: 'float'},
			{name:'PUNTOSPREF', type: 'float'},
			{name:'ESTATUS'},
			{name:'CVEMONEDA'},
			{name:'ACUSE'},
			{name:'SELECCION'}
			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			add: function(store, records, index){
				if(store.getCount()>1){
					var oldRecord = store.getAt(index-1);
					var newRecord = store.getAt(index);
					newRecord.data['MONTODESDE'] = oldRecord.data['MONTOHASTA']+0.01;
				}
			},
			load: procesarConsultaTasas,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaTasas(null, null, null);
				}
			}
		}
	});
	
	var storeAcuseTasas = new Ext.data.ArrayStore({
		fields: [
			{name:'CVETASA'},
			{name:'CVEIF'},
			{name:'CVEEPO'},
			{name:'TIPOTASA'},
			{name:'MONTODESDE', type: 'float'},
			{name:'MONTOHASTA', type: 'float'},
			{name:'PUNTOSPREF', type: 'float'},
			{name:'ESTATUS'},
			{name:'CVEMONEDA'},
			{name:'ACUSE'},
			{name:'SELECCION'}
		]
	});
	
	var storeAcuseEncData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	 var storeAsignaPymeData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13ofertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'consultaPymesParam'
		},
		fields: [
			{name:'CVEPYME'},
			{name:'RFCPYME'},
			{name:'CTABANC'},
			{name:'RAZONSOCIAL'},
			{name:'PARAMTASAS'},
			{name:'SELECCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners:{
			load: procesarConsultaAsignPymes,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaAsignPymes(null, null, null);
				}
			}
		}
	});
	
	var storeAcusePymesData = new Ext.data.ArrayStore({
		  fields: [
				{name:'CVEPYME'},
				{name:'RFCPYME'},
				{name:'CTABANC'},
				{name:'RAZONSOCIAL'},
				{name:'PARAMTASAS'},
				{name:'SELECCION'}
		  ]
	 });
	 
	var storeBusqAvanzFiado = new Ext.data.JsonStore({
		id: 'storeBusqAvanzFiado',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13ofertatasas1ext.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

//COMPONENTES------------------------------------------------------------------

	var radioTipoTasa = new  Ext.form.RadioGroup({
		//fieldLabel: 'Operar Descuento Autom�tico',
		hideLabel: true,
		style: 'margin:0 auto;',
		name: 'tipoTasa',
		id: 	'tipoTasa',
		columns: 3,
		items: [
					{ 
						boxLabel:    'PREFERENCIAL',
						name:        'tipoTasa',
						inputValue: 'P'
					},
					{ 
						boxLabel:    'NEGOCIADA',
						name:        'tipoTasa',
						inputValue: 'N'
					},
					{ 
						boxLabel:    'OFERTA TASAS POR MONTOS',
						name:        'tipoTasa',
						inputValue: 'O',
						checked: true
					}
				],
		style: { paddingLeft: '10px' } ,
		listeners:{
			change: function(grupo, radio){
				var lstTipoTasa = radio.inputValue;
				if(lstTipoTasa=='N' || lstTipoTasa=='P'){
					window.location.href='/nafin/13descuento/13pki/13if/13forma08ext.jsp?lstTipoTasa='+lstTipoTasa;
				}
			}
		}
	});
	
	var elementosForma = [
		//radioTipoTasa,
		{
			xtype: 'combo',
			name: 'cboEpo',
			id: 'cboEpo1',
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEpo',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEpoData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select: function(combo, record, index){
					var comboMoneda = Ext.getCmp('cboMoneda1');
					if(combo.getValue()!='' && comboMoneda.getValue()!=''){
						fp.el.mask('Consultando...', 'x-mask');
						storeTasasData.load({
							params: Ext.apply(fp.getForm().getValues())
						});
					}
					
				}
			}
		},
		{
			xtype: 'combo',
			name: 'cboMoneda',
			id: 'cboMoneda1',
			fieldLabel: 'Moneda',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMoneda',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMonedaData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				select: function(combo, record, index){
					var comboEpo = Ext.getCmp('cboEpo1');
					if(combo.getValue()!='' && comboEpo.getValue()!=''){
						fp.el.mask('Consultando...', 'x-mask');
						storeTasasData.load({
							params: Ext.apply(fp.getForm().getValues())
						});
					}
					
				}
			}
		}
	];
	
	var elementsFormBusq = [
		{ 
			xtype:   'label',  
			html:		'<b>Campos de B�squeda:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cboPyme1');
							var epoComboCmp = Ext.getCmp('cboEpo1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);
							//fpBusqAvanzada.el.mask('Buscando...','x-mask-loading');
							storeBusqAvanzFiado.load({
									params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
										icEpo: epoComboCmp.getValue()
									})
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{ 
			xtype:   'label',  
			html:		'<hr>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{ 
			xtype:   'label',  
			html:		'<b>Resultados:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'combo',
			name: 'cboPyme',
			id: 'cboPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_fiado',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzFiado,
			tpl : NE.util.templateMensajeCargaCombo
		}
	];
	
	var selectModel = new Ext.grid.CheckboxSelectionModel({
        checkOnly: true,
		  singleSelect: false,
		  renderer: function(v, p, record){
				if (record.data['CVETASA']){
					return '<div class="x-grid3-row-checker">&#160;</div>';
				}else{
					return '<div>&#160;</div>';
				}
			},
		  listeners: {
            rowselect: function(selectModel, rowIndex, record) {
						record.data['SELECCION']='S';
					 
            },
				rowdeselect: function(selectModel, rowIndex, record) {
					if (record.data['CVETASA']){
						record.data['SELECCION']='N';
					}
            }
        }
    });
	 
	 var selectModelPyme = new Ext.grid.CheckboxSelectionModel({
			checkOnly: true,
			singleSelect: false,
			listeners: {
				rowselect: function(selectModel, rowIndex, record) {
					record.data['SELECCION']='S';
					 
				},
				rowdeselect: function(selectModel, rowIndex, record) {
					record.data['SELECCION']='N';
				}
			}
    });
//CONTENEDORES------------------------------------------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		style: 'margin:0 auto;',
		title: 'Oferta de Tasas por Montos',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true
	});
	
	var gridOfertaTasas = new Ext.grid.EditorGridPanel({
		id: 'gridOfertaTasas1',
		store: storeTasasData,
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		clicksToEdit: 1,
		title:' ',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		sm: selectModel,
		columns: [
			selectModel,
			{
				header : 'Monto Pub. Desde',
				tooltip: 'Monto Publicado Desde',
				dataIndex : 'MONTODESDE',
				sortable : true,
				width : 150,
				align: 'right',
				editor: {
						 xtype: 'numberfield',
						 maxValue : 99999999999.99,
						 allowBlank: false
					},
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto Pub. Hasta',
				tooltip: 'Monto Publicado Hasta',
				dataIndex : 'MONTOHASTA',
				sortable : true,
				width : 150,
				align: 'right',
				editor: {
						 xtype: 'numberfield',
						 maxValue : 99999999999.99,
						 allowBlank: false
					},
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Puntos Preferenciales',
				tooltip: 'Puntos Preferenciales',
				dataIndex : 'PUNTOSPREF',
				width : 150,
				sortable : true,
				align: 'center',
				editor: {
						 xtype: 'numberfield',
						 maxValue : 999.9999,
						 decimalPrecision: 4,
						 allowBlank: false
					},
				renderer: Ext.util.Format.numberRenderer('0,0.0000%')
			},
			{
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				width : 130,
				sortable : true,
				align: 'center',
				renderer: function (causa, columna, registro){
						if(causa==null || causa=='' || causa=='N') causa = 'Inactivo';
						else causa = 'Activo';
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		//height: 300,
		autoHeight: true,
		width: 600,
		bodyStyle:'text-align:left',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Eliminar',
				id: 'btnEliminarRango',
				handler: onEliminarRango
				},
				'-',
				{
				text: 'Guardar',
				id: 'btnGuardarRango',
				handler: onGuardarRangos
				},
				'-',
				{
				text: 'Agregar Rango',
				handler: onAgregarRangoTasa
				},
				'-',
				{
				text: 'Cambio Estatus',
				id: 'btnCambiaEstatus',
				handler: onCambiaEstatusRango
				},
				'-',
				{
				text: 'Asignar Pymes',
				id: 'btnAsignaPymes',
				handler: onAsignaPymesIni
				},
				'-',
				{
				text: 'Cancelar',
				id: 'btnCancelar',
				handler: function(){
						storeTasasData.load({
							params: Ext.apply(fp.getForm().getValues())
						});
					}
				}
			]
		}
	});
	
	var gridAcuOfertaTasas = new Ext.grid.GridPanel({
		id: 'gridAcuOfertaTasas1',
		style: 'margin:0 auto;',
		store: storeAcuseTasas,
		margins: '20 0 0 0',
		title:' ',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'Monto Pub. Desde',
				tooltip: 'Monto Publicado Desde',
				dataIndex : 'MONTODESDE',
				sortable : true,
				width : 150,
				align: 'right',
				editor: {
						 xtype: 'numberfield',
						 maxValue : 99999999999.99,
						 allowBlank: false
					},
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto Pub. Hasta',
				tooltip: 'Monto Publicado Hasta',
				dataIndex : 'MONTOHASTA',
				sortable : true,
				width : 150,
				align: 'right',
				editor: {
						 xtype: 'numberfield',
						 maxValue : 99999999999.99,
						 allowBlank: false
					},
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Puntos Preferenciales',
				tooltip: 'Puntos Preferenciales',
				dataIndex : 'PUNTOSPREF',
				width : 150,
				sortable : true,
				align: 'center',
				editor: {
						 xtype: 'numberfield',
						 maxValue : 999.9999,
						 decimalPrecision: 4,
						 allowBlank: false
					},
				renderer: Ext.util.Format.numberRenderer('0,0.0000%')
			},
			{
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				width : 135,
				sortable : true,
				align: 'center',
				renderer: function (causa, columna, registro){
						if(causa==null || causa=='' || causa=='N') causa = 'Inactivo';
						else causa = 'Activo';
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		//height: 300,
		autoHeight: true,
		width: 600,
		bodyStyle:'text-align:left',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			id:'toolAcuTasas1',
			items: [
				'->',
				'-',
				{
				text: 'Asignar Pymes',
				handler: onAsignaPymes
				},
				'-',
				{
				text: 'Generar Archivo',
				id: 'btnGenerarArch',
				handler: onGenerarCsv
				},
				{
				text: 'Abrir Archivo',
				id: 'btnAbrirArch',
				hidden: true
				//handler: onEliminarRango
				},
				'-',
				{
				text: 'Generar PDF',
				id: 'btnGenerarPDF',
				handler: onGenerarPdf
				},
				{
				text: 'Abrir PDF',
				id: 'btnAbrirPDF',
				hidden: true
				//handler: onEliminarRango
				},
				'-',
				{
				text: 'Salir',
				id: 'btnSalir',
				handler: onInicio
				}
			]
		}
	});
	
	var gridAcuseEncabezado = new Ext.grid.GridPanel({
		id: 'gridAcuseEncabezado1',
		store: storeAcuseEncData,
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 150,
				sortable : true
			},
			{
				header : 'Informacion',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'informacion',
				width : 230,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 400,
		height: 130,
		title: ' ',
		frame: true
	});
	
	var gridAsignaPymes = new Ext.grid.EditorGridPanel({
		id: 'gridAsignaPymes1',
		store: storeAsignaPymeData,
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		clicksToEdit: 1,
		title:'Parametrizaci�n de PyME(s)',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		sm: selectModelPyme,
		columns: [
			{
				header : 'RFC',
				tooltip: 'RFC',
				dataIndex : 'RFCPYME',
				sortable : true,
				width : 150,
				align: 'left'
			},
			{
				header : 'Nombre de la PyME',
				tooltip: 'Nombre de la PyME',
				dataIndex : 'RAZONSOCIAL',
				sortable : true,
				width : 396,
				align: 'left'
			},
			selectModelPyme
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		//height: 300,
		//autoHeight: true,
		width: 600,
		bodyStyle:'text-align:left',
		frame: true,
		listeners: {
			viewReady: selectDefaultDoctos
		},
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Guardar',
				handler: onGuardarAsigPyme
				},
				'-',
				{
				text: 'Cancelar',
				id:'onCancelarAsigPyme',
				handler: onInicio
				}
			]
		}
	});
	
	var gridAcuseAsigPymes = new Ext.grid.EditorGridPanel({
		id: 'gridAcuseAsigPymes1',
		store: storeAcusePymesData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		clicksToEdit: 1,
		title:'PyME(s) Parametrizadas',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'RFC',
				tooltip: 'RFC',
				dataIndex : 'RFCPYME',
				sortable : true,
				width : 150,
				align: 'left'
			},
			{
				header : 'Nombre de la PyME',
				tooltip: 'Nombre de la PyME',
				dataIndex : 'RAZONSOCIAL',
				sortable : true,
				width : 350,
				align: 'left'
			},
			{
				header : 'Accion',
				tooltip: 'Accion',
				dataIndex : 'PARAMTASAS',
				sortable : true,
				width : 100,
				align: 'left',
				renderer:  function (valor, columna, registro){
					var accion = "";
					if(valor=='N')
						accion = 'Alta';
					if(valor=='S')
						accion = 'Baja';
					return accion;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		//height: 300,
		autoHeight: true,
		width: 600,
		bodyStyle:'text-align:left',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Generar Archivo',
				id: 'btnGenerarArch2',
				handler: onGenerarCsv2
				},
				{
				text: 'Abrir Archivo',
				id: 'btnAbrirArch2',
				hidden: true
				//handler: onEliminarRango
				},
				'-',
				{
				text: 'Generar PDF',
				id: 'btnGenerarPDF2',
				handler: onGenerarPdf2
				},
				{
				text: 'Abrir PDF',
				id: 'btnAbrirPDF2',
				hidden: true
				//handler: onEliminarRango
				},
				'-',
				{
				text: 'Salir',
				id: 'btnSalir2',
				handler: onInicio
				}
			]
		}
	});
	
	var fpPymes = new Ext.form.FormPanel({
		id: 'fpPymes1',
		width: 600,
		style: 'margin:0 auto;',
		title: 'Asignaci�n de Oferta de Tasas',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 140,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},			
		items: [
			{
				xtype: 'panel',
				name: 'notaPymes',
				id: 'notaPymes1',
				html: '"Las Pymes con tasas Preferenciales no ser�n mostradas, es necesario borrar su parametrizaci�n para poder aplicar Oferta de Tasas por Montos."',
				width: 600,
				height: 40,
				frame: false
			},
			{
			xtype: 'compositefield',
			fieldLabel: 'Num. Electronico PyME',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'numberfield',
					name: 'numElecPyme',
					id: 'numElecPyme1',
					allowBlank: true,
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'button',
					name: 'btnBuscar',
					id: 'btnBuscar1',
					text: 'Buscar',
					handler: function(){
						var numElec = Ext.getCmp('numElecPyme1').getValue();
						if(numElec != ''){
							fpPymes.el.mask('Consultando...','x-mask');
							storeAsignaPymeData.load({
								params: Ext.apply(fp.getForm().getValues(),{
									numElecPyme: numElec
								})
							});
						}else{
							Ext.MessageBox.alert('Aviso','Se requiere N�mero Electr�nico de la PyME');
						}
					}
				}
				]
			}
		
		],
		buttons: [
			{
				text: 'Busqueda Avanzada',
				iconCls: 'icoBuscar',
				id:'btnConsultar',
				handler: function(){
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 			fpBusqAvanzada
							}).show();
					}
					/*fpPymes.el.mask('Consultando...','x-mask');
					var numElec = Ext.getCmp('numElecPyme1').getValue();
					storeAsignaPymeData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							numElecPyme: numElec
						})
					});*/
				}
			},
			{
				text: 'Todas las Pymes',
				iconCls: 'icoBuscar',
				id:'btnConsultarTodas',
				handler: function(){
					fpPymes.el.mask('Consultando...','x-mask');
					var numElec = Ext.getCmp('numElecPyme1').getValue();
					storeAsignaPymeData.load({
						params: Ext.apply(fp.getForm().getValues())
					});
				}
			},
			{
				text: 'Pymes no Parametrizadas',
				iconCls: 'icoBuscar',
				id:'btnConsultarParam',
				handler: function(){
					fpPymes.el.mask('Consultando...','x-mask');
					var numElec = Ext.getCmp('numElecPyme1').getValue();
					storeAsignaPymeData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							csNoParam: 'S'
						})
					});
				}
			}
		]
	});
	
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementsFormBusq,
		monitorValid: 	true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cboPyme = Ext.getCmp("cboPyme1");
					var storeCmb = cboPyme.getStore();
					var numElecPyme = Ext.getCmp('numElecPyme1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cboPyme.getValue())) {
						cboPyme.markInvalid('Seleccione Pyme');
						return;
					}
					var record = cboPyme.findRecord(cboPyme.valueField, cboPyme.getValue());
					
					record =  record ? record.get(cboPyme.displayField) : cboPyme.valueNotFoundText;
					numElecPyme.setValue(cboPyme.getValue());
					//txtNombreProv.setValue(record);
					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
				
			}
		]
	});
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			radioTipoTasa,
			fp,
			{//FUNCION: Panel para mostrar avisos
				xtype: 'panel',
				name: 'mensajes',
				id: 'mensajes1',
				html: 'La EPO no ha sido parametrizada con Oferta de Tasas. De clic en Agregar Rango, '+
						'capture los criterios y de clic en Guardar para llevar a cabo la parametrizaci�n',
				width: 600,
				frame: true,
				hidden: true
			}
		]
	});
	
	storeCatEpoData.load();
	storeCatMonedaData.load();

});