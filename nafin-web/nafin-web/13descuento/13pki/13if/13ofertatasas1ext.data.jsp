<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.descuento.*,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoEPO,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoPymeBusqAvazada,
		javax.naming.*,
		net.sf.json.*,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String msgError = "";
try{

	AutorizacionTasas EJBautorizacionTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

	String sesCveIf = iNoCliente;
	if (informacion.equals("valoresIniciales")) {
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("msgError", msgError);
		infoRegresar= jsonObj.toString();
	
	}else if (informacion.equals("catalogoEpo")) {
		CatalogoEPO cat = new CatalogoEPO();
		cat.setCampoClave("ic_epo");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setClaveIf(sesCveIf);
		cat.setHabilitado("S");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	
	}else if (informacion.equals("catalogoMoneda")) {
		CatalogoMoneda cat = new CatalogoMoneda();
		cat.setCampoClave("ic_moneda");
		cat.setCampoDescripcion("cd_nombre");
		cat.setOrden("1");
		infoRegresar = cat.getJSONElementos();
	
	}else if(informacion.equals("busquedaAvanzada")){
	
		String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
		String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
		String icEpo = request.getParameter("icEpo")==null?"":request.getParameter("icEpo");
		
		CatalogoPymeBusqAvazada cat = new CatalogoPymeBusqAvazada();
		cat.setCampoClave("crn.ic_nafin_electronico");
		cat.setCampoDescripcion("RPAD (crn.ic_nafin_electronico, 10, ' ') || p.cg_razon_social");
		if(!nombrePyme.equals(""))
			cat.setNombre_pyme(nombrePyme);
		if(!rfcPyme.equals(""))
			cat.setRfc_pyme(rfcPyme);
		if(!icEpo.equals(""))
			cat.setIc_epo(icEpo);
		//cat.setOrden("p.cg_razon_social");
		infoRegresar = cat.getJSONElementos();

	}else if(informacion.equals("consultaOfertaTasas")){
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		JSONArray jsObjArray = new JSONArray();
		List lstTasasOfer = EJBautorizacionTasas.getTasasOfertaIf(sesCveIf, cboEpo, cboMoneda);
		jsObjArray = JSONArray.fromObject(lstTasasOfer);
		
		infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";		
	}else if(informacion.equals("consultaPymesParam")){
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		String numElecPyme = (request.getParameter("numElecPyme")!=null)?request.getParameter("numElecPyme"):"";
		String csNoParam = (request.getParameter("csNoParam")!=null)?request.getParameter("csNoParam"):"";
		
		JSONArray jsObjArray = new JSONArray();
		List lstPymesOferTasa = EJBautorizacionTasas.getPymesOferTasas(sesCveIf, cboEpo, cboMoneda, numElecPyme, csNoParam);
		
		jsObjArray = JSONArray.fromObject(lstPymesOferTasa);
		infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";		
		
	}else if(informacion.equals("guardarOfertaTasas")){
		JSONObject jsonObj = new JSONObject();
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		List lstTasasOfer = new ArrayList();
		lstTasasOfer = JSONArray.fromObject(jsonRegistros);
		
		// ----- Variables de seguridad -----
		String pkcs7 = request.getParameter("Pkcs7");
		String folioCert = "";
		String externContent = request.getParameter("TextoFirmado");
		char getReceipt = 'Y';
		String acuseRecibo = "";
		
		Seguridad s = new Seguridad();
		Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
		
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folioCert = acuse.toString();	//Este numero no importa en esta pantalla!
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
				
				acuseRecibo = s.getAcuse();
				
				java.util.Date date = new java.util.Date();
				String fecha = (new SimpleDateFormat ("dd/MM/yyyy")).format(date);
				String hora = (new SimpleDateFormat ("HH:mm:ss")).format(date);
				
				acuse.setReciboElectronico(acuseRecibo);
				acuse.setFechaHora(fecha+" "+hora);
				EJBautorizacionTasas.setTasasOfertaIf(sesCveIf, cboEpo, cboMoneda, lstTasasOfer, acuse, iNoUsuario);
				
				jsonObj.put("acuse", acuse.toString());
				jsonObj.put("recibo", acuseRecibo);
				jsonObj.put("fecha", fecha);
				jsonObj.put("hora", hora);
				jsonObj.put("usuario", strNombreUsuario);
				jsonObj.put("msgExito", "<p align='center'>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br><p>");
				jsonObj.put("success", new Boolean(true));
			} else {	//autenticación fallida
				String _error = s.mostrarError();
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
			}
		}
		
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("eliminarOfertaTasas")){
		JSONObject jsonObj = new JSONObject();
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		List lstTasasOfer = new ArrayList();
		lstTasasOfer = JSONArray.fromObject(jsonRegistros);
		
		// ----- Variables de seguridad -----
		String pkcs7 = request.getParameter("Pkcs7");
		String folioCert = "";
		String externContent = request.getParameter("TextoFirmado");
		char getReceipt = 'Y';
		String acuseRecibo = "";
		
		Seguridad s = new Seguridad();
		Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
		
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folioCert = acuse.toString();	//Este numero no importa en esta pantalla!
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
				
				acuseRecibo = s.getAcuse();
				
				java.util.Date date = new java.util.Date();
				String fecha = (new SimpleDateFormat ("dd/MM/yyyy")).format(date);
				String hora = (new SimpleDateFormat ("HH:mm:ss")).format(date);
				
				acuse.setReciboElectronico(acuseRecibo);
				acuse.setFechaHora(fecha+" "+hora);
				EJBautorizacionTasas.deleteTasasOfertaIf(sesCveIf, cboEpo, cboMoneda, lstTasasOfer, acuse, iNoUsuario);
				
				jsonObj.put("acuse", acuse.toString());
				jsonObj.put("recibo", acuseRecibo);
				jsonObj.put("fecha", fecha);
				jsonObj.put("hora", hora);
				jsonObj.put("usuario", strNombreUsuario);
				jsonObj.put("msgExito", "<p align='center'>El rango ha sido eliminado<br><p>");
				jsonObj.put("success", new Boolean(true));
			} else {	//autenticación fallida
				String _error = s.mostrarError();
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
			}
		}
		
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("cambiarEstatusOfertaTasas")){
		JSONObject jsonObj = new JSONObject();
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		List lstTasasOfer = new ArrayList();
		lstTasasOfer = JSONArray.fromObject(jsonRegistros);
		
		// ----- Variables de seguridad -----
		String pkcs7 = request.getParameter("Pkcs7");
		String folioCert = "";
		String externContent = request.getParameter("TextoFirmado");
		char getReceipt = 'Y';
		String acuseRecibo = "";
		
		Seguridad s = new Seguridad();
		Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
		
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folioCert = acuse.toString();	//Este numero no importa en esta pantalla!
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
				
				acuseRecibo = s.getAcuse();
				
				java.util.Date date = new java.util.Date();
				String fecha = (new SimpleDateFormat ("dd/MM/yyyy")).format(date);
				String hora = (new SimpleDateFormat ("HH:mm:ss")).format(date);
				
				acuse.setReciboElectronico(acuseRecibo);
				acuse.setFechaHora(fecha+" "+hora);
				EJBautorizacionTasas.cambiaEstatusTasasOfertaIf(sesCveIf, cboEpo, cboMoneda, lstTasasOfer, acuse, iNoUsuario);
				
				jsonObj.put("acuse", acuse.toString());
				jsonObj.put("recibo", acuseRecibo);
				jsonObj.put("fecha", fecha);
				jsonObj.put("hora", hora);
				jsonObj.put("usuario", strNombreUsuario);
				jsonObj.put("msgExito", "<p align='center'>El estatus del rango ha sido modificado<br><p>");
				jsonObj.put("success", new Boolean(true));
			} else {	//autenticación fallida
				String _error = s.mostrarError();
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
			}
		}
		
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("guardarAsignPyme")){
		JSONObject jsonObj = new JSONObject();
		String jsonRegTasas = (request.getParameter("registrosTasas") == null)?"":request.getParameter("registrosTasas");
		String jsonRegPymes = (request.getParameter("registrosPymes") == null)?"":request.getParameter("registrosPymes");
		String cboEpo = (request.getParameter("cboEpo")!=null)?request.getParameter("cboEpo"):"";
		String cboMoneda = (request.getParameter("cboMoneda")!=null)?request.getParameter("cboMoneda"):"";
		List lstTasasOfer = new ArrayList();
		List lstPymes = new ArrayList();
		lstTasasOfer = JSONArray.fromObject(jsonRegTasas);
		lstPymes = JSONArray.fromObject(jsonRegPymes);
		
		// ----- Variables de seguridad -----
		String pkcs7 = request.getParameter("Pkcs7");
		String folioCert = "";
		String externContent = request.getParameter("TextoFirmado");
		char getReceipt = 'Y';
		String acuseRecibo = "";
		
		Seguridad s = new Seguridad();
		Acuse acuse = new Acuse(Acuse.ACUSE_IF,"1");
		
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folioCert = acuse.toString();	//Este numero no importa en esta pantalla!
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
				
				acuseRecibo = s.getAcuse();
				
				java.util.Date date = new java.util.Date();
				String fecha = (new SimpleDateFormat ("dd/MM/yyyy")).format(date);
				String hora = (new SimpleDateFormat ("HH:mm:ss")).format(date);
				
				acuse.setReciboElectronico(acuseRecibo);
				acuse.setFechaHora(fecha+" "+hora);
				System.out.println("lstTasasOfer====="+lstTasasOfer.size());
				System.out.println("lstPymes====="+lstPymes.size());
				EJBautorizacionTasas.setRelPymeOfertaTasas(sesCveIf, cboEpo, cboMoneda, lstTasasOfer, lstPymes, acuse, iNoUsuario);
				
				jsonObj.put("acuse", acuse.toString());
				jsonObj.put("recibo", acuseRecibo);
				jsonObj.put("fecha", fecha);
				jsonObj.put("hora", hora);
				jsonObj.put("usuario", strNombreUsuario);
				jsonObj.put("msgExito", "<p align='center'>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br><p>");
				jsonObj.put("success", new Boolean(true));
			} else {	//autenticación fallida
				String _error = s.mostrarError();
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
			}
		}
		
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("generarArchivo")){
		JSONObject jsonObj = new JSONObject();
		String  tipoArchivo = (request.getParameter("tipoArchivo") == null)?"":request.getParameter("tipoArchivo");
		String registrosAcuEnc = (request.getParameter("registrosAcuEnc") == null)?"":request.getParameter("registrosAcuEnc");
		String registrosAcuTasas = (request.getParameter("registrosAcuTasas") == null)?"":request.getParameter("registrosAcuTasas");
		String registrosAcuPymes = (request.getParameter("registrosAcuPymes") == null)?"":request.getParameter("registrosAcuPymes");
		String strEpoMoneda = (request.getParameter("strEpoMoneda") == null)?"":request.getParameter("strEpoMoneda");
		String strTitleEnc = (request.getParameter("strTitleEnc") == null)?"":request.getParameter("strTitleEnc");
		String tipoAcuse = (request.getParameter("tipoAcuse") == null)?"":request.getParameter("tipoAcuse");
		String nombreArchivo = "";
		
		if("pdf".equals(tipoArchivo)){
		
			List lstAcuEnc = JSONArray.fromObject(registrosAcuEnc);
			List lstAcuTasas = JSONArray.fromObject(registrosAcuTasas);
			Iterator itAcuEnc = lstAcuEnc.iterator();
			Iterator itAcuTasas = lstAcuTasas.iterator();
		
			CreaArchivo archivo = new CreaArchivo();
			nombreArchivo = archivo.nombreArchivo()+"."+tipoArchivo;	
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
												session.getAttribute("iNoNafinElectronico").toString(),
												(String)session.getAttribute("sesExterno"),
												(String) session.getAttribute("strNombre"),
												(String) session.getAttribute("strNombreUsuario"),
												(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(2,60);
			pdfDoc.setCell(strTitleEnc,"celda01",ComunesPDF.CENTER,2,2);
			
			while (itAcuEnc.hasNext()) {
				JSONObject registro = (JSONObject)itAcuEnc.next();			
				pdfDoc.setCell(registro.getString("etiqueta"),"formas",ComunesPDF.LEFT,1,2);
				pdfDoc.setCell(registro.getString("informacion"),"formas",ComunesPDF.CENTER,1,2);
			} //fin del for
			
			pdfDoc.addTable();
			
			pdfDoc.setTable(3,80);
			pdfDoc.setCell(strEpoMoneda,"celda01",ComunesPDF.CENTER,3,2);
			pdfDoc.setCell("Monto Pub. Desde","celda01",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell("Monto Pub. Hasta","celda01",ComunesPDF.CENTER,1,2);
			pdfDoc.setCell("Puntos Preferenciales","celda01",ComunesPDF.CENTER,1,2);
			
			while (itAcuTasas.hasNext()) {
				JSONObject registro = (JSONObject)itAcuTasas.next();			
				pdfDoc.setCell(registro.getString("MONTODESDE"),"formas",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell(registro.getString("MONTOHASTA"),"formas",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell(registro.getString("PUNTOSPREF"),"formas",ComunesPDF.CENTER,1,2);
			} //fin del for

			pdfDoc.addTable();
			
			if("2".equals(tipoAcuse)){
				List lstAcuPymes = JSONArray.fromObject(registrosAcuPymes);
				Iterator itAcuPymes = lstAcuPymes.iterator();
			
				pdfDoc.setTable(3,80);
				pdfDoc.setCell("Pymes Parametrizadas","celda01",ComunesPDF.CENTER,3,2);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Nombre de la Pyme","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Acción","celda01",ComunesPDF.CENTER,1,2);
				
				
				while (itAcuPymes.hasNext()) {
					JSONObject registro = (JSONObject)itAcuPymes.next();			
					String accion = (registro.getString("PARAMTASAS")).equals("S")?"Baja":"Alta";
					pdfDoc.setCell(registro.getString("RFCPYME"),"formas",ComunesPDF.LEFT,1,2);
					pdfDoc.setCell(registro.getString("RAZONSOCIAL"),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(accion,"formas",ComunesPDF.CENTER,1,2);
				} //fin del for
				
				pdfDoc.addTable();
			}
			
			pdfDoc.endDocument();
			
		}else if("csv".equals(tipoArchivo)){
			CreaArchivo archivo = new CreaArchivo();
			StringBuffer 	contenidoArchivo 	= new StringBuffer();
			List lstAcuEnc = JSONArray.fromObject(registrosAcuEnc);
			List lstAcuTasas = JSONArray.fromObject(registrosAcuTasas);
			Iterator itAcuEnc = lstAcuEnc.iterator();
			Iterator itAcuTasas = lstAcuTasas.iterator();
			
			contenidoArchivo.append(strTitleEnc+"\n");
			
			int index = 0;
			while (itAcuEnc.hasNext()) {
				JSONObject registro = (JSONObject)itAcuEnc.next();			
				contenidoArchivo.append(registro.getString("etiqueta")+","+registro.getString("informacion")+"\n");
			} //fin del for
			
			contenidoArchivo.append("\n"+Comunes.filtraComas(strEpoMoneda)+"\n");
			contenidoArchivo.append("MONTO DESDE,MONTO HASTA,PUNTOS PREFERENCIALES\n");
			
			while (itAcuTasas.hasNext()) {
				JSONObject registro = (JSONObject)itAcuTasas.next();			
				contenidoArchivo.append(registro.getString("MONTODESDE")+","+registro.getString("MONTOHASTA")+","+registro.getString("PUNTOSPREF")+"\n");
			} //fin del for
			
			if("2".equals(tipoAcuse)){
				List lstAcuPymes = JSONArray.fromObject(registrosAcuPymes);
				Iterator itAcuPymes = lstAcuPymes.iterator();
				
				contenidoArchivo.append("\n PYMES PARAMETRIZADAS \n");
				contenidoArchivo.append("RFC, NOMBRE PYME, ACCION \n");
				
				while (itAcuPymes.hasNext()) {
					JSONObject registro = (JSONObject)itAcuPymes.next();			
					String accion = (registro.getString("PARAMTASAS")).equals("S")?"Baja":"Alta";
					
					contenidoArchivo.append(registro.getString("RFCPYME")+","+Comunes.filtraComas(registro.getString("RAZONSOCIAL"))+","+accion+"\n");

				} //fin del for
			}
			
			if(archivo.make(contenidoArchivo.toString(), strDirectorioTemp, "."+tipoArchivo))
				nombreArchivo = archivo.nombre;
		}
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("tipoArchivo", tipoArchivo);
		jsonObj.put("tipoAcuse", tipoAcuse);
		
		infoRegresar = jsonObj.toString();
	}
}catch(Throwable t){
	t.printStackTrace();
}finally{
	System.out.println("infoRegresar = " + infoRegresar);
}
%>


<%=infoRegresar%>