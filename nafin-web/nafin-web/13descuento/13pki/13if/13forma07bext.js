Ext.onReady(function() {

 var ic_epo  = Ext.getDom('ic_epo').value;
 var ct_cambio_motivo  = Ext.getDom('ct_cambio_motivo').value;
 var doctosSeleccionados  = Ext.getDom('doctosSeleccionados').value;
 var estatusDoctos  = Ext.getDom('estatusDoctos').value;
 var textoFirmar  = Ext.getDom('textoFirmar').value;
 var pkcs7  = Ext.getDom('pkcs7').value;
 

 
//_--------------------------------- HANDLERS -------------------------------


	var procesarGenerarPDF =  function(opts, success, response) {
		var btnArchivoPDF = Ext.getCmp('btnArchivoPDF');
		btnArchivoPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',	
		hidden: true,										
		width:	'200',
		heigth:	'auto',
		style: 'margin:0 auto;',		
		items: [	
			{
				xtype: 'button',
				text: 'Imprimir PDF',		
				iconCls: 'icoPdf',
				hidden: true,
				id: 'btnArchivoPDF',		 
				handler: function(boton, evento) {
				
				var  nombreArchivo =   Ext.getCmp("nombreArchivo").getValue();				
				
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13forma07ext.data.jsp',
						params: {
							informacion:'ArchivoPDF',
							strNombreArchivo:nombreArchivo						
						}					
						,callback: procesarGenerarPDF
					});
					
				}
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarPDF',
				hidden: true
			},			
			{
				xtype: 'button',
				text: 'Salir',		
				iconCls: 'icoLimpiar',
				id: 'btnSalir',		 
				handler: function() {
					window.location = "13forma07ext.jsp";
				}
			}
		]
	});
	
	
	var procesarTotalesData = function(store, arrRegistros, opts) 	{
			
		var gridTotales = Ext.getCmp('gridTotales');	
		var el = gridTotales.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}					
							
			//edito el titulo de la columna
			var cm = gridTotales.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
		
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	

	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
						
							
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;
			
			var gridCifrasControl = Ext.getCmp('gridCifrasControl');	
			var fpBotones = Ext.getCmp('fpBotones');	
			fpBotones.show();
			var btnArchivoPDF = Ext.getCmp('btnArchivoPDF');	
			
			if (!gridConsulta.isVisible()  &&  jsonData.mensajeError =='' ) {
			
				Ext.getCmp("nombreArchivo").setValue(jsonData.nombreArchivo);
			
				gridConsulta.show();
				btnArchivoPDF.show();
				
				consultaDataTotales.load({
					params: {
						informacion: 'ConsultaTotalesAcuse',
						numRegistrosMN:jsonData.numRegistrosMN,
						montoTotalMN:jsonData.montoTotalMN,
						importeTotalRecGarMN:jsonData.importeTotalRecGarMN,	
						montoTotalDescuentoMN:jsonData.montoTotalDescuentoMN,
						importeTotalInteresMN:jsonData.importeTotalInteresMN,
						importeTotalRecibirMN:jsonData.importeTotalRecibirMN,
						numRegistrosDL:jsonData.numRegistrosDL,
						montoTotalDL:jsonData.montoTotalDL,
						importeTotalRecGarDL:jsonData.importeTotalRecGarDL,
						montoTotalDescuentoDL:jsonData.montoTotalDescuentoDL,
						importeTotalInteresDL:jsonData.importeTotalInteresDL,
						importeTotalRecibirDL:jsonData.importeTotalRecibirDL
					}
				});				
			}
			
			if(jsonData.acuse !='') {
			
				var acuseCifras = [
						['N�mero de Acuse', jsonData.acuse],
						['Fecha de Carga ', jsonData.fecha],
						['Hora de Carga', jsonData.hora],
						['Usuario de Captura ', jsonData.usuario]
					];			
				storeCifrasData.loadData(acuseCifras)
				gridCifrasControl.show();
			}
				
			
			if(jsonData.mensajeAcuse !='' ) {
				Ext.getCmp("mensajeAcuse1").setValue(jsonData.mensajeAcuse);
			}
			if(jsonData.mensajeError !='' ) {
				Ext.getCmp("mensajeError").setValue(jsonData.mensajeError);			
			}
						
				
			if(store.getTotalCount() > 0) {	
				el.unmask();					
			} else {		
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var mensajeAcuse = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAcuse',							
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		width:	'400',
		heigth:	'auto',		
		items: [	
			{ 
				xtype:   'displayfield',
				id:	'nombreArchivo',				
				value:		'', 
				hidden:true
			},
			{ 
				xtype:   'displayfield',
				id:	'mensajeAcuse1',				
				value:		'',  
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}	
			},
			{ 
				xtype:   'displayfield',
				id:	'mensajeError',				
				value:		'',  
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}	
			}
		]
	});
	
	
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 560,
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});
	
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaTotalesAcuse'
		},
		hidden: true,
		fields: [	
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCTOS'},		
			{name: 'TOTAL_MONTO'},	
			{name: 'TOTAL_RECURSOS'},	
			{name: 'TOTA_MONTO_DESC'},	
			{name: 'TOTAL_MONTO_INTERES'},	
			{name: 'TOTAL_MONTO_OPERAR'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarTotalesData(null, null, null);						
					}
				}
			}			
	});
	
	
	
	
	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'',
		hidden: true,			
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Total Documentos',
				tooltip: 'Total Documentos',
				dataIndex : 'TOTAL_DOCTOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'TOTAL_MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Recursos en garantia',
				tooltip: 'Recursos en garantia ',
				dataIndex : 'TOTAL_RECURSOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'TOTA_MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},		
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'TOTAL_MONTO_INTERES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'TOTAL_MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 150,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma07ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarAcuse'
		},
		hidden: true,
		fields: [	
			{name: 'NOMBRE_EPO'},
			{name: 'NOMBRE_PYME'},	
			{name: 'NUM_DOCTO'},
			{name: 'F_VENCIMIENTO'},			
			{name: 'ESTATUS_ANTERIOR'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'POR_DESC'},
			{name: 'POR_GARANTIA'},
			{name: 'MONTO_DESC'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'NUEVO_ESTATUS'},
			{name: 'REFERENCIA'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Cambio de Estatus',
		hidden: true,
		columns: [
			{							
				header : 'Nombre EPO',
				tooltip: 'Nombre EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Nombre PyME/Cedente',
				tooltip: 'Nombre PyME/Cedente',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Numero de Documento',
				tooltip: 'Numero de Documento',
				dataIndex : 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'F_VENCIMIENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Estatus Anterior',
				tooltip: 'Estatus Anterior',
				dataIndex : 'ESTATUS_ANTERIOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'POR_DESC',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{							
				header : 'Recurso en garantia',
				tooltip: 'Recurso en garantia',
				dataIndex : 'POR_GARANTIA',
				sortable: true,				
				width: 130,
				resizable: true,			
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESC',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'MONTO_INTERES',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,			
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},
			{							
				header : 'Nuevo Estatus',
				tooltip: 'Nuevo Estatus',
				dataIndex : 'NUEVO_ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{
				header : '<center>Referencia<center>',
				tooltip: 'Referencia',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true,
				width: 200,					
				align: 'left'
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		margins: '20 0 0 0',
		loadMask: true,		
		stripeRows: true,
		height: 300,
		width: 900,			
		frame: false		
	});
	



//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		style: 'margin:0 auto;',
		items: [	
			NE.util.getEspaciador(20),
			mensajeAcuse,
			NE.util.getEspaciador(20),
			gridCifrasControl,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20)
			
		]
	});						
	 
	consultaData.load({
		params: {
		informacion: 'ConsultarAcuse',
			ic_epo:ic_epo,
			ct_cambio_motivo:ct_cambio_motivo,
			doctosSeleccionados:doctosSeleccionados,
			estatusDoctos:estatusDoctos,
			textoFirmar:textoFirmar,
			pkcs7:pkcs7
		}
	});
		
//-------------------------------- ----------------- -----------------------------------
	
	
});