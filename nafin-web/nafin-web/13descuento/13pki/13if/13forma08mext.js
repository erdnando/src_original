Ext.onReady(function() {
	
	Ext.form.Field.prototype.msgTarget = 'side';
	var listaTasasInsertar = [];
	var listaTasasEliminar  = [];
	var TasasPreferenciales  = [];
	var TasasNegociables  = [];
	
	var Opera_Fideicomiso = "";	

//********************************************Agregado Fodea 17

	var procesarFideicomiso = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			Opera_Fideicomiso = info.OPERA;
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}


//*****************************************para mostrar el Acuse ******************************

	var mensajeAutentificacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutentificacion',							
		width:	400,
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		columns: 1,
		items: [					
			{
				xtype: 'displayfield',
				id:	'autentificac',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}		
			}			
		]
	});
	
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var botImprimir = new Ext.Container({
		layout: 'table',		
		id: 'botImprimir',							
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		columns: 1,		
		items: [			
			{
				xtype: 'button',
				text: 'Generar PDF',
				tooltip:	'Generar PDF',
				id: 'btnGenerarPDF',
				hidden: true,
				iconCls: 'icoPdf',
				handler: function(boton, evento) {
				
					var recibo = Ext.getCmp('recibo').getValue();
					var acuse2 = Ext.getCmp('acuse2').getValue();
					var fecha = Ext.getCmp('fecha').getValue();
					var hora = Ext.getCmp('hora').getValue();				
					var tipoTasaA = Ext.getCmp('tipoTasaA').getValue();
					var totalTasasCargadas;
					var totalTasasAlta;
					var totalTasasEliminar;
					TasasPreferenciales  = [];
					TasasNegociables  = [];
					
					resumenAccionData.each(function(record) {
						if(record.data['ID']=='total_tasas_cargadas') { totalTasasCargadas = record.data['TOTAL'];  }
						if(record.data['ID']=='total_tasas_alta') { totalTasasAlta = record.data['TOTAL'];  }
						if(record.data['ID']=='total_tasas_eliminadas') { totalTasasEliminar = record.data['TOTAL'];  }						 
							
					});
						
					// Prenegociables 
					if(tipoTasaA =='P') {			
						var  gridPrenegociable = Ext.getCmp('gridPrenegociable');
						var store1 = gridPrenegociable.getStore();
						store1.each(function(record) {			
							TasasPreferenciales.push(record.data['EPO_RELACIONADA'] +"|"+record.data['NOM_PROVEEDOR'] +"|"+record.data['ESTATUS'] +"|"+
													record.data['REFERENCIA'] +"|"+record.data['PLAZO'] +"|"+record.data['TIPO_TASA'] +"|"+
													record.data['VALOR'] +"|"+record.data['REL_MAT']+"|"+record.data['PUNTOS']	+"|"+	record.data['VALOR_TASA']);
						});
					}
					
					// negociables
					if(tipoTasaA =='N') {
						var  gridNegociable = Ext.getCmp('gridNegociable');
						var store2 = gridNegociable.getStore();
						store2.each(function(record) {			
							TasasNegociables.push(record.data['EPO_RELACIONADA'] +"|"+record.data['NOM_PROVEEDOR'] +"|"+record.data['ESTATUS'] +"|"+
														record.data['REFERENCIA'] +"|"+record.data['PLAZO'] +"|"+record.data['TIPO_TASA'] +"|"+
														record.data['VALOR'] +"|"+record.data['VALOR_TASA']);																			
						});
					}
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13forma08mext.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								recibo:recibo,
								acuse2:acuse2,
								fechaCarga:fecha,
								horaCarga:hora,								
								tipoTasa:tipoTasaA,
								TasasNegociables:TasasNegociables,
								TasasPreferenciales:TasasPreferenciales,
								totalTasasCargadas:totalTasasCargadas,
								totalTasasAlta:totalTasasAlta,
								totalTasasEliminar:totalTasasEliminar
							}					
							,callback: procesarGenerarPDF
						});					
					}
				},				
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					tooltip:	'Bajar Archivo',
					id: 'btnBajarPDF',					
					hidden: true
				},					
				{
					xtype: 'button',
					text: 'Generar CSV',
					tooltip:	'Generar CSV',
					id: 'btnGenerarCSV',
					hidden: true,
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						var recibo = Ext.getCmp('recibo').getValue();
						var acuse2 = Ext.getCmp('acuse2').getValue();
						var fecha = Ext.getCmp('fecha').getValue();
						var hora = Ext.getCmp('hora').getValue();						
						var tipoTasaA = Ext.getCmp('tipoTasaA').getValue();	
						var totalTasasCargadas;
						var totalTasasAlta;
						var totalTasasEliminar;
						resumenAccionData.each(function(record) {
							if(record.data['ID']=='total_tasas_cargadas') { totalTasasCargadas = record.data['TOTAL'];  }
							if(record.data['ID']=='total_tasas_alta') { totalTasasAlta = record.data['TOTAL'];  }
							if(record.data['ID']=='total_tasas_eliminadas') { totalTasasEliminar = record.data['TOTAL'];  }						 
								
						});
						TasasPreferenciales  = [];
						TasasNegociables  = [];
						
						// Prenegociables 
						if(tipoTasaA =='P') {			
							var  gridPrenegociable = Ext.getCmp('gridPrenegociable');
							var store1 = gridPrenegociable.getStore();
							store1.each(function(record) {			
								TasasPreferenciales.push(record.data['EPO_RELACIONADA'] +"|"+record.data['NOM_PROVEEDOR'] +"|"+record.data['ESTATUS'] +"|"+
													record.data['REFERENCIA'] +"|"+record.data['PLAZO'] +"|"+record.data['TIPO_TASA'] +"|"+
													record.data['VALOR'] +"|"+record.data['REL_MAT']+"|"+record.data['PUNTOS']	+"|"+	record.data['VALOR_TASA']);
							});
						}
						// negociables
						if(tipoTasaA =='N') {
							var  gridNegociable = Ext.getCmp('gridNegociable');
							var store2 = gridNegociable.getStore();
							store2.each(function(record) {			
								TasasNegociables.push(record.data['EPO_RELACIONADA'] +"|"+record.data['NOM_PROVEEDOR'] +"|"+record.data['ESTATUS'] +"|"+
													record.data['REFERENCIA'] +"|"+record.data['PLAZO'] +"|"+record.data['TIPO_TASA'] +"|"+
													record.data['VALOR'] +"|"+record.data['VALOR_TASA']);																			
							});
						}
								
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13forma08mext.data.jsp',
							params: {
								informacion:'ArchivoCSV',
								recibo:recibo,
								acuse2:acuse2,
								fechaCarga:fecha,
								horaCarga:hora,							
								tipoTasa:tipoTasaA,
								TasasNegociables:TasasNegociables,
								TasasPreferenciales:TasasPreferenciales,
								totalTasasCargadas:totalTasasCargadas,
								totalTasasAlta:totalTasasAlta,
								totalTasasEliminar:totalTasasEliminar
							}					
							,callback: procesarGenerarCSV
						});					
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					tooltip:	'Bajar Archivo',
					id: 'btnBajarCSV',
					hidden: true
				},							
				{
				xtype: 'button',
				text: 'Salir',
				tooltip:	'Salir',
				iconCls: 'icoLimpiar',
				id: 'btnSalir',
				handler: function(boton, evento) {
					window.location = '13forma08mext.jsp';		
				}
			},		
			{ xtype:   'displayfield', id:	'recibo',	hidden: true,  value:''},
			{ xtype:   'displayfield', id:	'acuse2',	hidden: true,  value:''},
			{ xtype:   'displayfield', id:	'fecha',	hidden: true,  value:''},
			{ xtype:   'displayfield', id:	'hora',	hidden: true,  value:''},
			{ xtype:   'displayfield', id:	'tipoTasaA',	hidden: true,  value:''}
		]
	});
	
	
	
	// respuesta de Guardar de  tasas Prenegociables /Negociables 
	function procesarSuccessFailureConfirma(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			
			
			var mensajeAutentificacion = Ext.getCmp('mensajeAutentificacion');
			var gridNegociable = Ext.getCmp('gridNegociable');
			var gridPrenegociable = Ext.getCmp('gridPrenegociable');
			var botConfirmar = Ext.getCmp('botConfirmar');	
			Ext.getCmp("autentificac").setValue(jsonData.mensajeAutentificacion);	
			var botImprimir = Ext.getCmp('botImprimir');	
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');		
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var gridResumenAccion = Ext.getCmp('gridResumenAccion');
				
			if(jsonData.recibo !='') {
			
				Ext.getCmp("recibo").setValue(jsonData.recibo);
				Ext.getCmp("acuse2").setValue(jsonData.acuse2);
				Ext.getCmp("fecha").setValue(jsonData.fecha);
				Ext.getCmp("hora").setValue(jsonData.hora);			
				Ext.getCmp("tipoTasaA").setValue(jsonData.tipoTasa);							
				
				var registroResumen = Ext.data.Record.create(['ID', 'DESCRIPCION', 'TOTAL']);
				resumenAccionData.add( 	
					new registroResumen({ 
						ID: 'fecha_carga',	DESCRIPCION: 'Fecha de Carga', 	TOTAL: jsonData.fecha 		
					})
				);
				
				resumenAccionData.add( 	
					new registroResumen({ 
						ID: 'hora_carga', 	DESCRIPCION: 'Hora de Carga', 	TOTAL: jsonData.hora
					})
				);
				resumenAccionData.add(
					new registroResumen({ 
						ID: 'usuario_carga', 	DESCRIPCION: 'Usuario', 		TOTAL: jsonData.usuario
					})
				);
				resumenAccionData.add(
					new registroResumen({
						ID: 'recibo_carga',  	DESCRIPCION: 'Recibo',  		TOTAL: jsonData.recibo
					})
				);
				
				contenedorPrincipalCmp.doLayout();
				mensajeAutentificacion.show();				
				botImprimir.show();
				btnGenerarPDF.show();	
				btnGenerarCSV.show();
				botConfirmar.hide();
				
			}else {
			
				gridNegociable.hide();
				gridPrenegociable.hide();	
				mensajeAutentificacion.show();
				botImprimir.show();
				btnGenerarPDF.hide();	
				btnGenerarCSV.hide();
				botConfirmar.hide();
				gridResumenAccion.hide();
			}			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var confirmar = function(pkcs7, vtextoFirmar, vtipoTasa, vtasasInsertar, vtasasEliminar ){
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}else{					
			var parametros =  Ext.apply(
								fp.getForm().getValues(),
								{
									pkcs7:			pkcs7,
									informacion: 	'ConfirmacionTasas',
									textoFirmar:	vtextoFirmar,									
									tipoTasa:		vtipoTasa		
								}
							);
			
			if( vtasasInsertar.length > 0 ){
				parametros.listaTasasInsertar = vtasasInsertar;
			}
			if( vtasasEliminar.length > 0 ){
				parametros.listaTasasEliminar = vtasasEliminar;
			}
		
			Ext.Ajax.request({
				url: 			'13forma08mext.data.jsp',
				params: 		parametros,				
				callback: 	procesarSuccessFailureConfirma
			});
	}
}

//*****************************************para mostrar el PreAcuse  ******************************

	var procesarConfirmarTasas= function() 	{
		
		var tipoTasa;
		var tipoTasa1 = Ext.getCmp("tipoTasa1");	
		var tipoTasa2 = Ext.getCmp("tipoTasa2");		
		if(tipoTasa1.getValue()==true){ tipoTasa ='P'; }
		if(tipoTasa2.getValue()==true) { tipoTasa ='N'; }
			
		var tasasInsertar		= Ext.getCmp("tasas_insertar" + tipoTasa ).getValue();
		var tasasEliminar		= Ext.getCmp("tasas_eliminar" + tipoTasa ).getValue();
 
		tasasInsertar			= Ext.decode( tasasInsertar ); 
		tasasEliminar			= Ext.decode( tasasEliminar );
 
		var totales='';
		var negociables ='';
		var prenegociables ='';
				
		resumenAccionData.each(function(record) {
			totales += record.data['DESCRIPCION'] +" "+record.data['TOTAL']+'\n';	
		});
						
		// Prenegociables 
		if(tipoTasa =='P') {			
			prenegociables='EPO Relacionada|Nombre Proveedor|Estatus|Referencia|Plazo|Tipo Tasa|Valor|Rel. Mat.|Puntos|Valor Tasa \n';		
			var  gridPrenegociable = Ext.getCmp('gridPrenegociable');
			var store1 = gridPrenegociable.getStore();
			store1.each(function(record) {			
				prenegociables += record.data['EPO_RELACIONADA'] +"|"+record.data['NOM_PROVEEDOR'] +"|"+record.data['ESTATUS'] +"|"+
									record.data['REFERENCIA'] +"|"+record.data['PLAZO'] +"|"+record.data['TIPO_TASA'] +"|"+
									record.data['VALOR'] +"|"+record.data['REL_MAT']+"|"+record.data['PUNTOS']	+"|"+
									record.data['VALOR_TASA']+'\n';
					
			});
		}

		// negociables
		if(tipoTasa =='N') {
			negociables='EPO Relacionada|Nombre Proveedor|Estatus|Referencia|Plazo|Tipo Tasa|Valor|Valor Tasa \n';
			var  gridNegociable = Ext.getCmp('gridNegociable');
			var store2 = gridNegociable.getStore();
			store2.each(function(record) {			
				negociables += record.data['EPO_RELACIONADA'] +"|"+record.data['NOM_PROVEEDOR'] +"|"+record.data['ESTATUS'] +"|"+
									record.data['REFERENCIA'] +"|"+record.data['PLAZO'] +"|"+record.data['TIPO_TASA'] +"|"+
									record.data['VALOR'] +"|"+record.data['VALOR_TASA']+'\n';
			});
		}
		
		var textoFirmar = totales+prenegociables+negociables;		
			
		NE.util.obtenerPKCS7(confirmar, textoFirmar, tipoTasa, tasasInsertar, tasasEliminar );	
		
		

	}
	
	 var botConfirmar = new Ext.Container({
		layout: 'table',		
		id: 'botConfirmar',							
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		columns: 1,		
		items: [				
			{
				xtype: 'button',
				text: 'Confirmar Tasas ',					
				tooltip:	'Confirmar Tasas ',
				iconCls: 'icoAceptar',
				id: 'btnConfirmarTasas',
				handler: procesarConfirmarTasas
			},
			{
				xtype: 'button',
				text: 'Cancelar',
				tooltip:	'Cancelar',
				iconCls: 'icoLimpiar',
				id: 'btnCancelar',
				handler: function(boton, evento) {
					window.location = '13forma08mext.jsp';		
				}
			},
			{
				xtype: 'hidden',
				id:	 'tasas_insertarN'
			},
			{
				xtype: 'hidden',
				id:	 'tasas_eliminarN'
			},
			{
				xtype: 'hidden',
				id:	 'tasas_insertarP'
			},
			{
				xtype: 'hidden',
				id:	 'tasas_eliminarP'
			}
		]
	});
	
	var procesarPrenegociablesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
					
		if (arrRegistros != null) {
			
			if (!gridPrenegociable.isVisible()) {
				gridPrenegociable.show();
			}	
		
			var el = gridPrenegociable.getGridEl();						
			var jsonData = store.reader.jsonData;
				
			Ext.getCmp('tasas_insertarP').setValue(Ext.encode(jsonData.tasas_insertar));
			Ext.getCmp('tasas_eliminarP').setValue(Ext.encode(jsonData.tasas_eliminar));
			
			var totalTasas = Ext.getCmp('totalTasas');
			var fp = Ext.getCmp('forma');
			var gridTotalesTasas = Ext.getCmp('gridTotalesTasas');
			var botConfirmar = Ext.getCmp('botConfirmar');
			var gridResumenAccion = Ext.getCmp('gridResumenAccion');
			var contenedorGrids = Ext.getCmp('contenedorGrids');
			gridResumenAccion.show();					
			contenedorGrids.hide();
			
			fp.hide();
			botConfirmar.show();
					
			if(store.getTotalCount() > 0) {	
				el.unmask();						
				if(jsonData.TasaAplicar=='S' && Opera_Fideicomiso == 'S'){
					Ext.getCmp('lblTasasPrenegociable').show();
					Ext.getCmp('btnConfirmarTasas').disable();
				}else{
					Ext.getCmp('lblTasasPrenegociable').hide();
					Ext.getCmp('btnConfirmarTasas').enable();
				}
			} else {						
				Ext.getCmp('lblTasasPrenegociable').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var procesarNegocibalesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
			var el = gridNegociable.getGridEl();						
			var jsonData = store.reader.jsonData;
			
		if (arrRegistros != null) {
			if (!gridNegociable.isVisible()) {
				gridNegociable.show();
			}					
				
			Ext.getCmp('tasas_insertarN').setValue(Ext.encode(jsonData.tasas_insertar));
			Ext.getCmp('tasas_eliminarN').setValue(Ext.encode(jsonData.tasas_eliminar));
			
			var totalTasas = Ext.getCmp('totalTasas');
			var fp = Ext.getCmp('forma');
			var gridTotalesTasas = Ext.getCmp('gridTotalesTasas');
			var botConfirmar = Ext.getCmp('botConfirmar');
			var gridResumenAccion = Ext.getCmp('gridResumenAccion');
			var contenedorGrids = Ext.getCmp('contenedorGrids');
			gridResumenAccion.show();					
			contenedorGrids.hide();
			fp.hide();
			botConfirmar.show();		
					
			if(store.getTotalCount() > 0) {	
				el.unmask();						
				if(jsonData.TasaAplicar=='S' && Opera_Fideicomiso == 'S'){
					Ext.getCmp('lblTasasNegociable').show();
					Ext.getCmp('btnConfirmarTasas').disable();
				}else{
					Ext.getCmp('lblTasasNegociable').hide();
					Ext.getCmp('btnConfirmarTasas').enable();
				}
			
			
			} else {						
				Ext.getCmp('lblTasasNegociable').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
				//el.mask('No se encontraron registros (Revisar que el plazo indicado sea valido', 'x-mask');				
			}
		}
	}
	
	var ConsNegociableData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma08mext.data.jsp',
		baseParams: {
			informacion: 'ConsultaNegociable'
		},
		hidden: true,
		fields: [	
			{name: 'EPO_RELACIONADA'},	
			{name: 'NOM_PROVEEDOR'},	
			{name: 'ESTATUS'},
			{name: 'REFERENCIA'},
			{name: 'PLAZO'},	
			{name: 'TIPO_TASA'},
			{name: 'VALOR'},			
			{name: 'VALOR_TASA'}	,
			{name: 'TASAS_INSERTAR'},	
			{name: 'ROJO'},
			{name: 'TASAS_ELIMINAR'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarNegocibalesData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarNegocibalesData(null, null, null);						
				}
			}
		}		
	});

		
	var gridNegociable= new Ext.grid.EditorGridPanel({
		id: 'gridNegociable',				
		store: ConsNegociableData,	
		style: 'margin:0 auto;',
		title:'Negociada  ',
		hidden: true,
		clicksToEdit: 1,		
		columns: [
			{							
				header :'EPO relacionada',
				tooltip: 'EPO relacionada',
				dataIndex : 'EPO_RELACIONADA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header :'Nombre Proveedor ',
				tooltip: 'Nombre Proveedor ',
				dataIndex : 'NOM_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header :'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo Tasa',
				tooltip: 'Tipo Tasa',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Valor',
				tooltip: 'Valor',
				dataIndex : 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{							
				header : 'Valor Tasa',
				tooltip: 'Valor Tasa',
				dataIndex : 'VALOR_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){
					if( registro.data['ROJO'] =='S'&& Opera_Fideicomiso == 'S'){				
						return  '<font color="red">'+ value +'</font>';									 
					}else 
						return value	
				}
			}
		],
		bbar: {
			id: 'barraNegociable',	
			items: [			
				{
					xtype:'label',					
					id: 'lblTasasNegociable',
					hidden: true,
					html:'<font color = "red">    * Las Tasas marcadas en color rojo son menores a las tasas a aplicar de las IFs de la EPO </font>'
				}
			]
		},
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false
	});
	
	
	var ConsPrenegociableData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma08mext.data.jsp',
		baseParams: {
			informacion: 'ConsultaPrenegociable'
		},
		hidden: true,
		fields: [				
			{name: 'EPO_RELACIONADA'},
			{name: 'NOM_PROVEEDOR'},	
			{name: 'ESTATUS'},
			{name: 'REFERENCIA'},
			{name: 'PLAZO'},	
			{name: 'TIPO_TASA'},
			{name: 'VALOR'},
			{name: 'REL_MAT'},
			{name: 'PUNTOS'},
			{name: 'VALOR_TASA'},
			{name: 'TASAS_INSERTAR'},	
			{name: 'ROJO'},
			{name: 'TASAS_ELIMINAR'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarPrenegociablesData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarPrenegociablesData(null, null, null);						
				}
			}
		}		
	});
	
	var gridPrenegociable= new Ext.grid.EditorGridPanel({
		id: 'gridPrenegociable',				
		store: ConsPrenegociableData,	
		style: 'margin:0 auto;',
		title:'Preferencial ',
		hidden: true,
		clicksToEdit: 1,
		columns: [
			{							
				header :'EPO relacionada',
				tooltip: 'EPO relacionada',
				dataIndex : 'EPO_RELACIONADA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header :'Nombre Proveedor ',
				tooltip: 'Nombre Proveedor ',
				dataIndex : 'NOM_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header :'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo Tasa',
				tooltip: 'Tipo Tasa',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Valor',
				tooltip: 'Valor',
				dataIndex : 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Rel. Mat',
				tooltip: 'Rel. Mat',
				dataIndex : 'REL_MAT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Puntos',
				tooltip: 'Puntos',
				dataIndex : 'PUNTOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Valor Tasa',
				tooltip: 'Valor Tasa',
				dataIndex : 'VALOR_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',				
				renderer:function(value,metadata,registro){
					if( registro.data['ROJO'] =='S' && Opera_Fideicomiso == 'S'){				
						return  '<font color="red">'+ value +'</font>';									 
					}else 
						return value;	
				}	
			}
		],
		bbar: {
			id:'barraPrenegociable',
			items: [			
				{
					xtype:'label',
					id: 'lblTasasPrenegociable',
					hidden: true,
					html:'<font color = "red">    * Las Tasas marcadas en color rojo son menores a las tasas a aplicar de los IFs de la EPO </font>'
				}
			]
		},
		displayInfo: true,		
		emptyMsg: "No hay registros.",
      loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false
	});

// ********************* Carga Masiva*************************************


	var procesoRegresar= function() 	{
		var tipoTasa;
		var tipoTasa1 = Ext.getCmp("tipoTasa1");	
		var tipoTasa2 = Ext.getCmp("tipoTasa2");		
		if(tipoTasa1.getValue()==true){ tipoTasa ='P'; }
		if(tipoTasa2.getValue()==true) { tipoTasa ='N'; }
		
		Ext.getCmp('ic_epo1').show();	
		Ext.getCmp('ic_moneda2').show();
		Ext.getCmp('tipoTasa1').show();	
		Ext.getCmp('tipoTasa2').show();	
		if(tipoTasa =='N') {
			Ext.getCmp('lstPlazo2').show();
		}
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var contenedorResultadosCmp = contenedorPrincipalCmp.get('contenedorGrids')
		contenedorPrincipalCmp.remove(contenedorGrids);
		contenedorPrincipalCmp.doLayout();
		contenedorResultadosCmp = contenedorPrincipalCmp.get('contenedorGrids');	
		Ext.getCmp("regresa").setValue('S');
		var contenedorGrids = Ext.getCmp('contenedorGrids');
		contenedorGrids.hide();
}

	var procesarContinuar= function() 	{
	
	  var lstNumerosLineaOk = Ext.getCmp('lstNumerosLineaOk');
		var proceso = Ext.getCmp('proceso');
	
		var tipoTasa;
		var tipoTasa1 = Ext.getCmp("tipoTasa1");	
		var tipoTasa2 = Ext.getCmp("tipoTasa2");
      
      var btnConfirmarTasas = Ext.getCmp('btnConfirmarTasas'); // -> Se agrego: garellano      
      var gridPreNegociable   = Ext.getCmp('gridPrenegociable');// -> Se agrego: garellano  
      var gridNegociable      = Ext.getCmp('gridNegociable');// -> Se agrego: garellano  
      var elPreNegociable     = gridPreNegociable.getGridEl();	// -> Se agrego: garellano  
      var elNegociable        = gridNegociable.getGridEl();	// -> Se agrego: garellano  
      
		if(tipoTasa1.getValue()==true){ tipoTasa ='P'; }
		if(tipoTasa2.getValue()==true) { tipoTasa ='N'; }
		
	
		fp.el.mask('Enviando...', 'x-mask-loading');	
      btnConfirmarTasas.setDisabled(true); // -> Se agrego: garellano
      gridPreNegociable.show(); // -> Se agrego: garellano
      gridNegociable.show(); // -> Se agrego: garellano
      
      elPreNegociable.mask('Cargando...');// -> Se agrego: garellano  
      elNegociable.mask('Cargando...');// -> Se agrego: garellano  
		
		ConsPrenegociableData.load({
			params: Ext.apply(fp.getForm().getValues(),{
			informacion: 'ConsultaPrenegociable',
			pantalla: 'PreAcuse',
			lstNumerosLineaOk:lstNumerosLineaOk.getValue(),
			proceso:proceso.getValue(),
			tipoTasa:tipoTasa
			}),
         callback:function(opts, success, response){   // -> Se agrego: garellano | ConsNegociableData.load se agreg� al callback  |   
            ConsNegociableData.load({
               params: Ext.apply(fp.getForm().getValues(),{
               informacion: 'ConsultaNegociable',
               pantalla: 'PreAcuse',
               lstNumerosLineaOk:lstNumerosLineaOk.getValue(),
               proceso:proceso.getValue(),
               tipoTasa:tipoTasa
               }),
               callback: function(opts, success, response){ // -> Se agrego: garellano
                  
						//btnConfirmarTasas.setDisabled(false); // -> Se agrego: garellano
               }
            });
         }
		});
	}
	
	var resumenAccionData = new Ext.data.JsonStore({
		root : 'resumenAccion',
		fields : ['ID', 'DESCRIPCION', 'TOTAL'],
		autoDestroy : false,
		autoLoad: false
	});
	
	var gridResumenAccion = {
		xtype: 'grid',
		hidden: true,
		id: 'gridResumenAccion',
		store: resumenAccionData,
		margins: '10 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header: ' ',
				dataIndex: 'DESCRIPCION',
				sortable: false,
				resizable: true,
				hidden: false,
				width: 250,
				align: 'left'
			},
			{
				header: 'Total',
				dataIndex: 'TOTAL',
				sortable: false,
				hideable: false,
				resizable: true,
				align: 'center',
				width: 250
			}
		],	
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		autoHeight: true,		
		width: 520,
		title: 'Acuse',
		frame: true,
		bbar: ''
	};
	
	
	var registrosValidosData = new Ext.data.JsonStore({
		root : 'registrosValidos',
		fields : ['LINEA', 'CAMPO', 'OBSERVACION'],
		autoDestroy : true,
		autoLoad: false
	});
	
	
	var gridValidos = {
		xtype: 'grid',
		id: 'gridValidos',	
		store: registrosValidosData,
		margins: '0 5 0 0',
		columns: [
			{
				header: 'L�nea',
				tooltip: 'Numero de L�nea del archivo',
				dataIndex: 'LINEA',
				sortable: false,
				width: 50,
				resizable: true,
				hidden: false
			},
			{
				header: 'Campo',
				tooltip: 'Nombre del Campo',
				dataIndex: 'CAMPO',
				sortable: false,
				hideable: false,
				width: 100,
				align: 'left'
			},
			{
				id: 'observaciones',
				header : 'Observaci�n',
				tooltip: 'Observaciones',
				dataIndex : 'OBSERVACION',
				width: 300,
				align: 'left',
				sortable : false,
				hideable : false
			}
		],
		autoExpandColumn: 'observaciones',
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 450,
		title: 'Detalle de Documentos sin Error',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [
				'Total Registros Validos: ',
				{
					xtype: 'tbtext',
					text: '',
					id: 'totalRegistrosValidos'
				}
			]
		}
	};
	
	
	var registrosInvalidosData = new Ext.data.JsonStore({
		root : 'registrosInvalidos',
		fields : ['LINEA', 'CAMPO', 'OBSERVACION'],
		autoDestroy : true,
		autoLoad: false
	});


	var gridInvalidos = {
		xtype: 'grid',
		id: 'gridInvalidos',
		store: registrosInvalidosData,
		margins: '0 0 0 0',
		columns: [
			{
				header: 'L�nea',
				tooltip: 'Numero de L�nea del archivo',
				dataIndex: 'LINEA',
				sortable: false,
				width: 50,
				resizable: true,
				hidden: false
			},
			{
				header: 'Campo',
				tooltip: 'Nombre del Campo',
				dataIndex: 'CAMPO',
				sortable: false,
				hideable: false,
				width: 100,
				align: 'left'
			},
			{
				id: 'observacionesErr',
				header : 'Observaci�n',
				tooltip: 'Observaciones',
				dataIndex : 'OBSERVACION',
				width: 300,
				align: 'left',
				sortable : false,
				hideable : false,
				renderer:  function (observacion, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(observacion) + '"';
						return observacion;
				}
			}
		],
		autoExpandColumn: 'observacionesErr',
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 450,
		title: 'Detalle de Documentos con Errores',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [
				'Total Registros Inv�lidos: ',
				{
					xtype: 'tbtext',
					text: '',
					id: 'totalRegistrosInvalidos'
				}
			]
		}
	};
	
	
		
	var contenedorGrids = {
		xtype: 'panel',
		id: 'contenedorGrids',
		hidden: true,
		title: ' ',
		hidden: false,
		width: 850,
		frame: true,
		margins: '20 0 0 0',
		layout: 'hbox',
		layoutConfig: {
			pack: 'center',
			align: 'center'
		},
		style: 'margin:0 auto;',
		items: [
			gridValidos,
			gridInvalidos
		],
		buttons: [	
			{
				xtype: 'button',
				text: 'Regresar',
				tooltip:	'Regresar',
				iconCls: 'icoLimpiar',
				id: 'btnRegresar',
				handler: procesoRegresar
			},			
			{
				xtype: 'button',
				text: 'Continuar',					
				tooltip:	'Continuar',
				iconCls: 'icoAceptar',
				id: 'btnContinuarC',
				handler: procesarContinuar
			}					
		]
	};
	

	
 var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMCAMPO'},
		  {name: 'CAMPO'},
		  {name: 'TIPODATO'},
		  {name: 'LONGITUD'},
		  {name: 'OBSERVACIONES'}
	  ]
	});
	
	var infoLayoutP = [
		['1','R.F.C. Proveedor','Alfanum�rico','14','R.F.C. del Proveedor'],
		['2','Puntos/Valor Tasa','Num�rico','7', ' 2 Enteros ,  5 Decimales'],
		['3 ','Estatus','Alfanum�rico','1','A = Alta de la Tasa E = Eliminar Tasa'],
		['4','Tipo Tasa','Alfanum�rico','3','P-F = Preferencial Floating']  
	];
	
	var infoLayoutN = [
		['1','R.F.C. Proveedor','Alfanum�rico','14','R.F.C. del Proveedor'],
		['2','Puntos/Valor Tasa','Num�rico','7', ' 2 Enteros ,  5 Decimales'],
		['3 ','Estatus','Alfanum�rico','1','A = Alta de la Tasa E = Eliminar Tasa']		
	];

	storeLayoutData.loadData(infoLayoutP);	
	
 
	var gridLayout = new Ext.grid.GridPanel({
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header : 'No.',
				dataIndex : 'NUMCAMPO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'CAMPO',
				tooltip: 'CAMPO',
				dataIndex : 'CAMPO',
				width : 150,
				align: 'left',
				sortable : true
			},
			{
				header : 'TIPO DATO',
				dataIndex : 'TIPODATO',
				width : 150,
				sortable : true,
				align: 'center'
			},
			{
				header : 'LONG. MAX',
				dataIndex : 'LONGITUD',
				width : 120,
				sortable : true,
				align: 'center'
			},
			{
				header : 'OBSERVACIONES',
				dataIndex : 'OBSERVACIONES',
				width : 280,
				sortable : true,
				align: 'left'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 820,
		height: 130,
		frame: true
	});

	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma08ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoPlazo = new Ext.data.JsonStore({
		id: 'catalogoPlazo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma08ext.data.jsp',
		baseParams: {
			informacion: 'catalogoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var procesarCatalogoMonedaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_moneda1 = Ext.getCmp('ic_moneda1');
			if(ic_moneda1.getValue()==''){
				ic_moneda1.setValue(records[0].data['clave']);
			}
		}
	}
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma08ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	


	var myValidFn = function(v) {
		var myRegex = /^.+\.([tT][xX][tT])$/;
			return myRegex.test(v);
		}
		Ext.apply(Ext.form.VTypes, {
			archivotxt 		: myValidFn,
			archivotxtText 	: 'El formato del archivo de origen no es el correcto para el tipo de archivo seleccionado'
		});
		
	var  elementosForma = [			
			{ xtype:   'displayfield', hidden: true, id:'lstNumerosLineaOk',	  value:''},
			{ xtype:   'displayfield', hidden: true, id: 'proceso',	 value:''},
			{ xtype:   'displayfield', hidden: true, id: 'regresa',	 value:''},
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione  EPO...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',
			fieldLabel: 'EPO Relacionada',			
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO	
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			id: 'ic_moneda2',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar Moneda...',			
					valueField: 'clave',
					hiddenName : 'ic_moneda',
					fieldLabel: 'Moneda',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store: catalogoMoneda
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Plazo',
			combineErrors: false,
			msgTarget: 'side',
			id: 'lstPlazo2',
			hidden: true,
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'lstPlazo',
					id: 'lstPlazo1',
					mode: 'local',				
					displayField: 'descripcion',
					emptyText: 'Seleccione  Plazo...',			
					valueField: 'clave',
					hiddenName : 'lstPlazo',
					fieldLabel: 'Plazo',			
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',	
					store: catalogoPlazo,
					tpl : NE.util.templateMensajeCargaCombo					
				}	
			]
		},		
		{
			xtype:	'panel',
			layout:	'column',
			width: 700,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					handler: function(){						
						if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}									
					}
				},				
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 175,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									emptyText: 'Nombre del Archivo',
									fieldLabel: 'Nombre del Archivo',
									name: 'archivoFormalizacion',   
									buttonText: ' ',
									width: 300,	  
									buttonCfg: {
										iconCls: 'upload-icon'
									},
									anchor: '100%',
									vtype: 'archivotxt'
								}						
							]
						}
					]
				}
			]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: 'Carga Masiva Tasas Preferenciales/Negociadas',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},		
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Continuar',
				id: 'btnContinuar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
					valoresResultadoProceso = null;
					var fp = Ext.getCmp('forma');

					var ic_epo = Ext.getCmp("ic_epo1");
					if (Ext.isEmpty(ic_epo.getValue()) ){
						ic_epo.markInvalid('Debe Seleccionar una EPO.');
						return;
					}
					
					var tipoTasa;
					var tipoTasa1 = Ext.getCmp("tipoTasa1");	
					var tipoTasa2 = Ext.getCmp("tipoTasa2");		
					if(tipoTasa1.getValue()==true){ tipoTasa ='P'; }
					if(tipoTasa2.getValue()==true) { tipoTasa ='N'; }
					var ic_moneda = Ext.getCmp("ic_moneda1");	
						
					var lstPlazo1 ='';					
					if(tipoTasa =='N') {
						 lstPlazo1 = Ext.getCmp("lstPlazo1");										
						if (Ext.isEmpty(lstPlazo1.getValue()) ){
							lstPlazo1.markInvalid('Debe Seleccionar un Plazo.');
							return;
						}				
					}
					
					var archivo = Ext.getCmp("archivo");										
					if (Ext.isEmpty(archivo.getValue()) ){
						archivo.markInvalid('Favor de capturar una ruta valida');
						return;
					}			
					
					var	parametros ="?tipoTasa="+tipoTasa+"&ic_epo="+ic_epo.getValue()+
											"&ic_moneda="+ic_moneda.getValue();
											
					
					if(tipoTasa =='N') {
						parametros+="&lstPlazo="+lstPlazo1.getValue();
					}
					
					var forma = fp.getForm();									
					forma.submit({
						url: '13forma08mfilext.data.jsp'+parametros,
						waitMsg: 'Enviando datos...',
						waitTitle :'Por favor, espere',		
						success: function(form, action) {
							
							
							if(!action.result.codificacionValida){
								new NE.cespcial.AvisoCodificacionArchivo({codificacion:action.result.codificacionArchivo}).show();
							}else if( action.result.statusThread=='300' && !action.result.hayCaractCtrl)	{
								registrosValidosData.loadData(action.result);
								registrosInvalidosData.loadData(action.result);
								
								resumenAccionData.loadData(action.result);
								
								valoresResultadoProceso = action.result.valoresResultadoProceso;	
								var regresa = Ext.getCmp("regresa").getValue();	
															
								var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
								var contenedorResultadosCmp = contenedorPrincipalCmp.get('contenedorGrids')
								
								if (Ext.isEmpty(contenedorResultadosCmp) || regresa =='S' ) {	//Si no existe el componente lo agrega
									contenedorPrincipalCmp.add(contenedorGrids);
									contenedorPrincipalCmp.doLayout();
									contenedorResultadosCmp = contenedorPrincipalCmp.get('contenedorGrids')
								}							
								if (tipoTasa == 'P') { 		tasa = 'PREFERENCIALES';} 
								if (tipoTasa == 'N')	 { 	tasa = 'NEGOCIADAS'; 		}
								
								contenedorResultadosCmp.setTitle('Tasas a cargar: ' + tasa);
								Ext.getCmp('totalRegistrosValidos').setText(valoresResultadoProceso.totalRegistrosValidos.toString());
								Ext.getCmp('totalRegistrosInvalidos').setText(valoresResultadoProceso.totalRegistrosInvalidos.toString());
								Ext.getCmp("lstNumerosLineaOk").setValue(valoresResultadoProceso.lstNumerosLineaOk);	
								Ext.getCmp("proceso").setValue(valoresResultadoProceso.proceso);	
				
								Ext.getCmp('ic_epo1').hide();	
								Ext.getCmp('ic_moneda2').hide();	
								Ext.getCmp('lstPlazo2').hide();	
								Ext.getCmp('tipoTasa1').hide();	
								Ext.getCmp('tipoTasa2').hide();	
							
								Ext.getCmp("regresa").setValue('N');
							
								if(valoresResultadoProceso.btnContinuar =='N') {
									 Ext.getCmp('btnContinuarC').hide();
								}else {
									Ext.getCmp('btnContinuarC').show();
								}
							}else{
								if(action.result.statusThread!='0' && action.result.statusThread!='300'){
									Ext.MessageBox.alert('Error',action.result.mensajeEstatus);	
								}else if(action.result.hayCaractCtrl){
									Ext.getCmp('pnlRadioGroup').hide();
									Ext.getCmp('gridLayout').hide();
									fp.hide();
									caracteresEspeciales.show();
									caracteresEspeciales.setMensaje(action.result.mensajeResumen);
									caracteresEspeciales.setHandlerDescargarArchivo(function(){
										var archivo = action.result.urlArchivo
										archivo = archivo.replace('/nafin','');
										var params = {nombreArchivo: archivo};
										
										fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
										fp.getForm().getEl().dom.submit();
									});
								}
							}
							
						},
						failure: NE.util.mostrarSubmitError
					});
				}
			}
		]
	});
	

	var radioTipoTasa = new  Ext.form.RadioGroup({
		hideLabel: true,
		name: 'tipoTasa',		
		columns: 2,
		style: 'margin:0 auto;',
		items: [
			{ 
				boxLabel:    'PREFERENCIAL',
				name:  'tipoTasa',
				id: 	'tipoTasa1',
				inputValue: 'P',
				value: 'P',
				checked: true				
			},
			{ 
				boxLabel:    'NEGOCIADA',
				name:   'tipoTasa',
				id: 	'tipoTasa2',
				inputValue: 'N',
				value: 'N'				
			}		
		],
		style: { paddingLeft: '10px' } ,
		listeners:{
			change: function(grupo, radio){
				var lstTipoTasa = radio.inputValue;
				var lstPlazo2 = Ext.getCmp("lstPlazo2");
				
				if(lstTipoTasa=='N'){		
					lstPlazo2.show();	
					storeLayoutData.loadData(infoLayoutN);
	
				}else if(lstTipoTasa=='P'){	
					lstPlazo2.hide();	
					storeLayoutData.loadData(infoLayoutP);
				}	
					
				if(lstTipoTasa=='N' || lstTipoTasa=='P'){				
					catalogoEPO.load({
						params: {
							informacion: 'catalogoEPO',
							tipoTasa:lstTipoTasa						
						}						
					});							
				}				
			}
		}
	});
	
	var caracteresEspeciales = new NE.cespcial.CaracteresEspeciales({hidden:true});
	caracteresEspeciales.setHandlerAceptar(function(){
		Ext.getCmp('forma').show();
		Ext.getCmp('pnlRadioGroup').show();
		caracteresEspeciales.hide();
	});
	
	
		
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			mensajeAutentificacion,
			gridResumenAccion,
			NE.util.getEspaciador(20),		
			new Ext.Panel({id:'pnlRadioGroup', width:300, border:false, style: 'margin:0 auto;', items:radioTipoTasa}),	
			fp,
			botConfirmar,
			botImprimir,
			NE.util.getEspaciador(20),			
			gridPrenegociable,			
			NE.util.getEspaciador(20),
			gridNegociable,
			gridLayout,					
			NE.util.getEspaciador(20),
			caracteresEspeciales
		]
	});

	catalogoMoneda.load();
	catalogoEPO.load();
	catalogoPlazo.load();


	//Comprueba si opera Fideicomiso
	Ext.Ajax.request({
		url: '13forma08mext.data.jsp',
		params: {
			informacion: 'OperaFideicomiso'
		},
		callback: procesarFideicomiso
	});
	
//-------------------------------- ----------------- -----------------------------------
	
});