Ext.onReady(function() {

	var ic_documentos =[];
	var regtotales =[];	
	var cifrasControl =[];
	
	var cancelar =  function() {  
		ic_documentos = [];		
	 }
	 
	 
	function procesarDescargaArchivos(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			Ext.getCmp('btnDescargaErrores').setIconClass('icoXls');
			Ext.getCmp('btnImprimir').setIconClass('icoPdf');
			Ext.getCmp('btnInterfaseCSV').setIconClass('icoXls');		
			
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			var fp = Ext.getCmp('fpCargaArchivo');			
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarSuccessFailureAcuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);			
			if (jsonData != null){
				
				Ext.getCmp('leyenda0').update(jsonData.leyenda0);
				Ext.getCmp('mensaje0').show();
					
				if(jsonData.acuse!='')  {
				
					var acuseCifras = [
						['No. Total de Registros a Negociables', Ext.getCmp('total2').getValue()],
						['No. Total de Registros a Operada', Ext.getCmp('total4').getValue()],
						['N�mero de Acuse', jsonData.acuse],
						['Fecha de Carga', jsonData.fechaCarga],
						['Hora de Carga', jsonData.horaCarga],
						['Usuario de Captura', jsonData.usuarioCaptura]
					];				
				
				var totalNegocioables =Ext.getCmp('total2').getValue();
				var totalOperada =Ext.getCmp('total4').getValue();
				
				cifrasControl.push(totalNegocioables +'|'+totalOperada +'|'+jsonData.acuse +'|'+
				jsonData.fechaCarga +'|'+jsonData.horaCarga +'|'+jsonData.usuarioCaptura+'|'+jsonData.recibo); 
				
					storeCifrasData.loadData(acuseCifras);	
					gridCifrasControl.show();
		
					Ext.getCmp('gridConsulta2').hide();
					Ext.getCmp('gridTotales2').hide();
					Ext.getCmp('mensaje2').hide();
					Ext.getCmp('mensaje3').hide();
									
					Ext.getCmp('btnCancelarP').hide();
					Ext.getCmp('btnTransRegistros').hide();
					
					Ext.getCmp('btnImprimir').show();
					Ext.getCmp('btnInterfaseCSV').show();	
					Ext.getCmp('btnSalir').show();
					Ext.getCmp('gridCifrasControl').show();
					Ext.getCmp('gridConsultaTotales').setTitle('Acuse');	
					
					Ext.getCmp('leyenda0').update(jsonData.leyenda0);
					Ext.getCmp('mensaje0').show();
				
				}else {
					Ext.getCmp('gridConsultaTotales').hide();
					Ext.getCmp('mensaje1').hide();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('gridTotales1').hide();
					Ext.getCmp('mensaje2').hide();
					Ext.getCmp('gridConsulta2').hide();
					Ext.getCmp('gridTotales2').hide();
					Ext.getCmp('mensaje3').hide();
					Ext.getCmp('btnCancelarP').hide();
					Ext.getCmp('btnTransRegistros').hide();
					Ext.getCmp('btnSalir').show();				
				}
				
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var transRegistros = function() 	{
		var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');		
		var store = gridConsultaTotales.getStore();
		var textoFirmar ='';
		
		store.each(function(record) {
		
		regtotales.push( record.data['NOMBRE_EPO']+"|"+record.data['ESTATUS']+'|'+
		record.data['TOTAL_DOCTOS_MN']+'|'+record.data['MONTO_DESCUENTO_MN']+"|"+	record.data['MONTO_INTERES_MN']+"|"+record.data['MONTO_OPERAR_MN']+"|"+
		record.data['TOTAL_DOCTOS_DL']+"|"+	record.data['MONTO_DESCUENTO_DL']+"|"+record.data['MONTO_INTERES_DL']+"|"+record.data['MONTO_OPERAR_DL']);
		
		});
		
		var gridHiddeDoctos = Ext.getCmp('gridHiddeDoctos');		
		var storeC = gridHiddeDoctos.getStore();
		
		cancelar();
		
		storeC.each(function(record) {
		textoFirmar +=record.data['IC_DOCUMENTO']+'|'+record.data['ESTATUS_ASIGNAR']+'|'+record.data['CAUSA_RECHAZO']+'|'+
						record.data['ACUSE_PYME']+'|'+record.data['CLAVE_BFONDEO']+'|'+record.data['IC_EPO']+'|'+record.data['IC_MONEDA']+'|'+
						record.data['FECHA_EMISION']+'|'+record.data['FECHA_SELECCION']+'|'+record.data['FECHA_PUBLICACION']+'|'+
						record.data['FECHA_VENCIMIENTO']+'|'+record.data['MONTO_DESCUENTO']+'|'+record.data['TASA']+'|'+
						record.data['MONTO_INTERES']+'|'+record.data['MONTO_OPERAR']+'|'+record.data['NOMBRE_EPO']+'|'+
						record.data['MONEDA']+'|'+record.data['NOMBRE_PROVEEDOR']+'|'+record.data['NUM_PROVEEDOR']+'|'+
						record.data['NUM_SIRAC']+'|'+	record.data['TIPO_FACTORAJE']+'\n';
      
		ic_documentos.push(record.data['IC_DOCUMENTO']+'|'+record.data['ESTATUS_ASIGNAR']+'|'+record.data['CAUSA_RECHAZO']+'|'+
						record.data['ACUSE_PYME']+'|'+record.data['CLAVE_BFONDEO']+'|'+record.data['IC_EPO']+'|'+record.data['IC_MONEDA']+'|'+
						record.data['FECHA_EMISION']+'|'+record.data['FECHA_SELECCION']+'|'+record.data['FECHA_PUBLICACION']+'|'+
						record.data['FECHA_VENCIMIENTO']+'|'+record.data['MONTO_DESCUENTO']+'|'+record.data['TASA']+'|'+
						record.data['MONTO_INTERES']+'|'+record.data['MONTO_OPERAR']+'|'+record.data['NOMBRE_EPO']+'|'+
						record.data['MONEDA']+'|'+record.data['NOMBRE_PROVEEDOR']+'|'+record.data['NUM_PROVEEDOR']+'|'+
						record.data['NUM_SIRAC']+'|'+	record.data['TIPO_FACTORAJE'] );			
		});
		
		NE.util.obtenerPKCS7(transRegistrosCallback, textoFirmar, ic_documentos);	
	
	}
	
	var transRegistrosCallback = function(vpkcs7, vtextoFirmar, vic_documentos) 	{
		if (Ext.isEmpty(vpkcs7)) {
			Ext.getCmp("btnTransRegistros").enable();	
			return;	//Error en la firma. Termina...
		}else  {			
			Ext.getCmp("btnTransRegistros").disable();		
			
			Ext.Ajax.request({
				url : '13ConfirmaDoctos.data.jsp',
				params : {
					pkcs7: vpkcs7,
					textoFirmado: vtextoFirmar,
					informacion: 'ConfirmaRegistros',
					ic_documentos: vic_documentos									
				},
				callback: procesarSuccessFailureAcuse
			});	
		}
	}
	
	//***********************	PANTALLA DE PRE ACUSE  ****************************+
	
	var mensaje0 = new Ext.Container({
		layout: 'table',		
		id: 'mensaje0',							
		width:	'900',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [
			{
				width		: 900,	
				xtype: 'label',
				id: 'leyenda0',	
				cls:		'x-form-item',								
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}	
			}	
		]
	});
	
	
	
	//***Totales de Arriba *******************
	var procesarconsultaDataTotales = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;			
		
		var gridConsultaTotales = Ext.getCmp('gridConsultaTotales');	
		var el = gridConsultaTotales.getGridEl();
		
		if (arrRegistros != null) {
				
			if (!gridConsultaTotales.isVisible()) {
				gridConsultaTotales.show();
			}			
			if(store.getTotalCount() > 0) {				
				el.unmask();	
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '13ConfirmaDoctos.data.jsp',
		baseParams: {
			informacion: 'ConsultarTotales'
		},		
		fields: [						
			{name: 'NOMBRE_EPO'},
			{name: 'ESTATUS'},
			{name: 'TOTAL_DOCTOS_MN'},
			{name: 'MONTO_DESCUENTO_MN'},
			{name: 'MONTO_INTERES_MN'},
			{name: 'MONTO_OPERAR_MN'},	
			{name: 'TOTAL_DOCTOS_DL'},
			{name: 'MONTO_DESCUENTO_DL'},
			{name: 'MONTO_INTERES_DL'},
			{name: 'MONTO_OPERAR_DL'}		
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarconsultaDataTotales,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarconsultaDataTotales(null, null, null);						
					}
				}
			}	
	});
	
	var gruposConsulta = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{	header: '  ', colspan: 2, align: 'center'},
				{	header: 'Moneda Nacional', colspan: 4, align: 'center'},	
				{	header: 'D�lares ', colspan: 4, align: 'center'}
			]
		]
	});
	
	var gridConsultaTotales = new Ext.grid.GridPanel({
		id: 'gridConsultaTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		title:'Pre Acuse',
		hidden: true,
		plugins: gruposConsulta,	
		columns: [
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Total Doctos',
				tooltip: 'Total Doctos',
				dataIndex : 'TOTAL_DOCTOS_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Monto Descto.',
				tooltip: 'Monto Descto.',
				dataIndex : 'MONTO_DESCUENTO_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_INTERES_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR_MN',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},				
			{							
				header : 'Total Doctos',
				tooltip: 'Total Doctos',
				dataIndex : 'TOTAL_DOCTOS_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Monto Descto.',
				tooltip: 'Monto Descto.',
				dataIndex : 'MONTO_DESCUENTO_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{							
				header : 'Monto de Intereses',
				tooltip: 'Monto de Intereses',
				dataIndex : 'MONTO_INTERES_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR_DL',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
		
	
				//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	 
	//Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',		
		hidden: true,	
		align: 'center',
		title:'Datos cifras de control',
		columns: [
			{
				header : '',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true,
				align: 'left'	
			},
			{
				header : '',			
				dataIndex : 'informacion',
				width : 350,
				align: 'left',
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 550,
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});
	
	var mensaje1 = new Ext.Container({
		layout: 'table',		
		id: 'mensaje1',							
		width:	'900',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [
			{
				width		: 900,	
				xtype: 'label',
				id: 'leyenda1',	
				cls:		'x-form-item',								
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}	
			}	
		]
	});
	
	//esta es la consulta para obtener los datos de todos los registros  cargados ya qque se requieres para el acuse
	var consHiddeData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13ConfirmaDoctos.data.jsp',
		baseParams: {
			informacion: 'ConsultarHide'
		},
		hidden: true,
		fields: [						
			{name: 'NUM_SIRAC'},
			{name: 'NOMBRE_PROVEEDOR'},			
			{name: 'NUMER0_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO'},
			{name: 'PORCE_DESCUENTO'},
			{name: 'RECURSO_GARANTIA'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'TASA'},
			{name: 'PLAZO'},
			{name: 'NUM_PROVEEDOR'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CAUSA_RECHAZO'},
			{name: 'ESTATUS_ASIGNAR'},
			{name: 'ACUSE_PYME'},
			{name: 'CLAVE_BFONDEO'},
			{name: 'IC_EPO'},
			{name: 'FECHA_SELECCION'},
			{name: 'FECHA_PUBLICACION'},
			{name: 'NOMBRE_EPO'},
			{name: 'IC_MONEDA'} 			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {					
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.																
					}
				}
			}			
	});
	
	var gridHiddeDoctos = new Ext.grid.GridPanel({
		id: 'gridHiddeDoctos',				
		store: consHiddeData,	
		style: 'margin:0 auto;',
	 	hidden: true,
		columns: [
			{							
				header : 'NUMER0_DOCTO',
				tooltip: 'NUMER0_DOCTO',
				dataIndex : 'NUMER0_DOCTO',
				sortable: true,
				width: 150,
				resizable: true,
				align: 'left'
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 90,
		width: 150,
		style: 'margin:0 auto;',		
		frame: false
	});
		
		
	//--- Consulta documentos -------------------
	
		
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
	
		var jsonData = store.reader.jsonData;	
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
				
			if (!gridConsulta.isVisible()) {
				gridConsulta.show(); 
			}				
							
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();				
			
			Ext.getCmp('leyenda1').update(jsonData.leyenda1);
			Ext.getCmp('mensaje1').show();	
			
			Ext.getCmp('leyenda2').update(jsonData.leyenda2);
			Ext.getCmp('mensaje2').show();	
			
			Ext.getCmp('leyenda3').update(jsonData.leyenda3);
			Ext.getCmp('mensaje3').show(); 	
			
						
						
			if(store.getTotalCount() > 0) {
				el.unmask();	
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13ConfirmaDoctos.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse'
		},
		hidden: true,
		fields: [						
			{name: 'NUM_SIRAC'},
			{name: 'NOMBRE_PROVEEDOR'},			
			{name: 'NUMER0_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO'},
			{name: 'PORCE_DESCUENTO'},
			{name: 'RECURSO_GARANTIA'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'TASA'},
			{name: 'PLAZO'},
			{name: 'NUM_PROVEEDOR'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CAUSA_RECHAZO'},
			{name: 'ESTATUS_ASIGNAR'},
			{name: 'ACUSE_PYME'},
			{name: 'CLAVE_BFONDEO'},
			{name: 'IC_EPO'},
			{name: 'FECHA_SELECCION'},
			{name: 'FECHA_PUBLICACION'},
			{name: 'NOMBRE_EPO'},
			{name: 'IC_MONEDA'} 			
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
	 	hidden: true,
		columns: [
			{							
				header : 'No. Cliente Sirac',
				tooltip: 'No. Cliente Sirac',
				dataIndex : 'NUM_SIRAC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'
			},
			{
				header : 'Proveedor/Cedente',
				tooltip: 'Proveedor/Cedente',
				dataIndex : 'NOMBRE_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUMER0_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex : 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},	
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},	
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'left'			
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORCE_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{							
				header : 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex : 'RECURSO_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Monto Inter�s',
				tooltip: 'Monto Inter�s',
				dataIndex : 'MONTO_INTERES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Tasa',
				tooltip: 'Tasa',
				dataIndex : 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Referencia',
				tooltip: 'Referencia',
				dataIndex:'REFERENCIA',
				sortable: true,
				hidden:	true,
				width: 130,
				resizable: true,				
				align: 'left'
			},
			{							
				header : 'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},				
			{							
				header : 'No. Proveedor',
				tooltip: 'No. Proveedor',
				dataIndex : 'NUM_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'IC_Documento',
				tooltip: 'IC_Documento',
				dataIndex : 'IC_DOCUMENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		margins: '20 0 0 0',
		loadMask: true,		
		stripeRows: true,
		height: 200,
		width: 900,		
		title: '',
		frame: false
	});
	
	
	var consultaTotales1 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13ConfirmaDoctos.data.jsp',
		baseParams: {
			informacion: 'consultaTotales1'
		},
		hidden: true,
		fields: [						
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCTO'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {				
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.											
					}
				}
			}			
	});
	
	
	var gridTotales1 = new Ext.grid.GridPanel({
		id: 'gridTotales1',				
		store: consultaTotales1,	
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		title:'',
		hidden: true,
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'Total de Documentos',
				tooltip: 'Total de Documentos',
				dataIndex : 'TOTAL_DOCTO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'center'				
			}			
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 90,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
			
	var mensaje2 = new Ext.Container({
		layout: 'table',		
		id: 'mensaje2',							
		width:	'900',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [
			{
				width		: 900,	
				xtype: 'label',
				id: 'leyenda2',	
				cls:		'x-form-item',								
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}	
			}	
		]
	});
	
	var consultaData2 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13ConfirmaDoctos.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse2'
		},
		hidden: true,
		fields: [						
			{name: 'NOMBRE_EMISIOR'},	
			{name: 'NOMBRE_PROVEEDOR'},			
			{name: 'DESCRIPCION_DOCTO'},
			{name: 'LUGAR_SUSCRIPCION'},
			{name: 'FECHA_EMISION'},			
			{name: 'MONTO'},
			{name: 'MONTO_DESCUENTO'},		
			{name: 'PERIODICIDAD'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'TASA'},	
			{name: 'MONEDA'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);																					
					}
				}
			}
		});
		
	var gridConsulta2 = new Ext.grid.GridPanel({
		id: 'gridConsulta2',				
		store: consultaData2,	
		style: 'margin:0 auto;',
		hidden: true,
		columns: [
			{							
				header : 'Nombre del Emisor',
				tooltip: 'Nombre del Emisor',
				dataIndex : 'NOMBRE_EMISIOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Nombre de la Empresa Acreditada',
				tooltip: 'Nombre de la Empresa Acreditada',
				dataIndex : 'NOMBRE_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Descripci�n del Documento',
				tooltip: 'Descripci�n del Documento',
				dataIndex : 'DESCRIPCION_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Lugar Suscripci�n',
				tooltip: 'Lugar Suscripci�n',
				dataIndex : 'LUGAR_SUSCRIPCION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Fecha Suscripci�n',
				tooltip: 'Fecha Suscripci�n',
				dataIndex : 'FECHA_EMISION',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},	
			{							
				header : 'Importe Documento',
				tooltip: 'Importe Documento',
				dataIndex : 'MONTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			},
			{							
				header : 'Importe Descuento',
				tooltip: 'Importe Descuento',
				dataIndex : 'MONTO_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{							
				header : 'Periodicidad pago Capital e Inter�s',
				tooltip: 'Periodicidad pago Capital e Inter�s',
				dataIndex : 'PERIODICIDAD',
				sortable: true,				
				width: 130,
				resizable: true,				
				align: 'center'			
			},
			{							
				header : 'Fecha Vto. Descuento',
				tooltip: 'Fecha Vto. Descuento',
				dataIndex : 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'				
			},	
			{							
				header : 'Tasa Descuento',
				tooltip: 'Tasa Descuento',
				dataIndex : 'TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},	
			{							
				header : 'Nombre de la Moneda',
				tooltip: 'Nombre de la Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		margins: '20 0 0 0',
		loadMask: true,		
		stripeRows: true,
		height: 200,
		width: 900,		
		title: '',
		frame: false
	});
	
	
	var consultaTotales2 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13ConfirmaDoctos.data.jsp',
		baseParams: {
			informacion: 'consultaTotales2'
		},
		hidden: true,
		fields: [						
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCTO'},
			{name: 'TOTAL_IMPORTE_DOCTO'},
			{name: 'TOTAL_IMPORTE_DESC'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {				
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.											
					}
				}
			}			
	});
	
	
	var gridTotales2 = new Ext.grid.GridPanel({
		id: 'gridTotales2',				
		store: consultaTotales2,	
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		hidden: true,
		title:'',
		columns: [
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'left'	
			},		
			{							
				header : 'Total de Documentos',
				tooltip: 'Total de Documentos',
				dataIndex : 'TOTAL_DOCTO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'center'				
			},		
			{							
				header : 'Total Importe Documento',
				tooltip: 'Total Importe Documento',
				dataIndex : 'TOTAL_IMPORTE_DOCTO',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},	
			{							
				header : 'Total Importe Descuento',
				tooltip: 'Total Importe Descuento',
				dataIndex : 'TOTAL_IMPORTE_DESC',
				sortable: true,
				width: 200,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 90,
		width: 900,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var mensaje3 = new Ext.Container({
		layout: 'table',		
		id: 'mensaje3',							
		width:	'900',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [
			{
				width		: 900,	
				xtype: 'label',
				id: 'leyenda3',	
				cls:		'x-form-item',								
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}	
			}	
		]
	});
	
	
	
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{
				xtype: 'button',
				text: 'Cancelar',			
				iconCls: 'icoLimpiar',
				id: 'btnCancelarP',				
				handler: function() {
					window.location = "13ConfirmaDoctosExt.jsp";
				}
			},			
			{
				xtype: 'button',
				text: 'Transmitir Registros',
				tooltip:	'Transmitir Registros',				
				iconCls: 'icoAceptar',
				id: 'btnTransRegistros',
				handler:transRegistros
			},
			{
				xtype: 'button',
				text: 'Exportar Interfase Variable',
				tooltip:	'Exportar Interfase Variable',
				hidden: true,
				iconCls: 'icoXls',
				id: 'btnInterfaseCSV',
				handler: function(boton, evento) {
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13ConfirmaDoctos.data.jsp',
						params: Ext.apply(fpCargaArchivo.getForm().getValues(),{
							informacion: 'ArchivoAcuse_Interface',
							n_documentos: Ext.getCmp('doctosEstatus4').getValue(),
							regtotales:regtotales,
							cifrasControl:cifrasControl,
							tipoArch:'CSV'
						}),
						callback: procesarDescargaArchivos
					});
				}
			},
			{
				xtype: 'button',
				text: 'Imprimir',
				tooltip:	'Imprimir',
				hidden: true,
				iconCls: 'icoPdf',
				id: 'btnImprimir',
				handler: function(boton, evento) {
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13ConfirmaDoctos.data.jsp',
						params: Ext.apply(fpCargaArchivo.getForm().getValues(),{
							informacion: 'ArchivoAcuse',
							n_documentos: Ext.getCmp('doctosEstatus4').getValue(),
							regtotales:regtotales,
							cifrasControl:cifrasControl,
							tipoArch:'PDF'
						}),
						callback: procesarDescargaArchivos
					});
				}
			},
			{
				xtype: 'button',
				text: 'Salir',			
				iconCls: 'icoLimpiar',
				hidden: true,
				id: 'btnSalir',		 
				handler: function() {
					window.location = "13ConfirmaDoctosExt.jsp";
				}
			}		
		]
	});
	
	
	
	
	
	
	
	//***********************	PANTALLA DE CARGA ****************************+
	
	function procesaCargaArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
			
			var fpCargaArchivo = Ext.getCmp('fpCargaArchivo');
				fpCargaArchivo.el.unmask();
				
				 Ext.getCmp('gridLayout').hide();
				 Ext.getCmp('n_documentos').setValue(jsonData.n_documentos);
				  Ext.getCmp('doctosEstatus4').setValue(jsonData.doctosEstatus4);
				 Ext.getCmp('total2').setValue(jsonData.total2);
				 Ext.getCmp('total4').setValue(jsonData.total4);
				 
				if(jsonData.MENSAJE!='') {
					Ext.MessageBox.alert("Mensaje",jsonData.MENSAJE);
				}else  {
				
					if(jsonData.REG_ERROR!='') {
						Ext.getCmp('panelResultadosValidacion').show(); 
						
						var cifrasBien = [ [jsonData.REG_BIEN] ];
						registrosSinErroresData.loadData(cifrasBien);	 
						
						var cifrasError = [ 		[jsonData.REG_ERROR] 	];
						registrosConErroresData.loadData(cifrasError);	
						
						Ext.getCmp('totalRegistros').setValue(jsonData.totalRegistros);
						Ext.getCmp('datosErrores').setValue(jsonData.REG_ERROR); 
						
						
						if(jsonData.REG_ERROR!=''){
							Ext.getCmp('btnDescargaErrores').setIconClass('icoXls');
							Ext.getCmp('btnDescargaErrores') .show();
						}else  {
							Ext.getCmp('btnDescargaErrores') .hide(); 							
						}					
					}else if(jsonData.REG_ERROR=='') {
						
						
						consultaDataTotales.load({  	
							params: {  
								informacion: 'ConsultarTotales', 	
								doctosEstatus2:jsonData.doctosEstatus2,
								doctosEstatus4:jsonData.doctosEstatus4 
							}  		
						});
						
						consultaData.load({  	
							params: {  
								informacion: 'ConsultarPreAcuse', 	
								n_documentos:jsonData.doctosEstatus4, 
								lDocumentos:jsonData.lDocumentos
							}  		
						});
						
						consultaData2.load({  	
							params: {  
								informacion: 'ConsultarPreAcuse2', 	
								n_documentos:jsonData.doctosEstatus4
							}  		
						});	
						
						consultaTotales1.load({  	
							params: {  
								informacion: 'consultaTotales1', 	
								n_documentos:jsonData.doctosEstatus4
							}  		
						});	
						consultaTotales2.load({  	
							params: {  
								informacion: 'consultaTotales2', 	
								n_documentos:jsonData.doctosEstatus4
							}  		
						});	
						
						
						//esta es la consulta para obtener los datos de todos los registros  cargados
						consHiddeData.load({  	
							params: {  
								informacion: 'ConsultarPreAcuse', 	
								n_documentos:jsonData.n_documentos, 
								lDocumentos:jsonData.lDocumentos
							}  		
						});
						
						
						Ext.getCmp('fpCargaArchivo').hide();
						Ext.getCmp('panelResultadosValidacion').hide();
					
						Ext.getCmp('gridConsultaTotales').show();
						Ext.getCmp('gridConsulta').show();
						Ext.getCmp('gridTotales1').show();	
						Ext.getCmp('gridConsulta2').show();	
						Ext.getCmp('gridTotales2').show();	
						Ext.getCmp('fpBotones').show();							
					}
				}
				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var registrosConErroresData = new Ext.data.ArrayStore({
		fields: [
			{ name: 'DESCRIPCION' }	
		]
	 });
	
	var registrosSinErroresData = new Ext.data.ArrayStore({
		fields: [
			{ name: 'CLAVE_DOCUMENTOS' }	
		]
	 });
	 
	
	var elementosResultadosValidacion = [
		{ 
			xtype: 			'tabpanel',
			id:				'tabPanelResultadosValidacion',
			activeTab:		0,
			plain:			true,
			height: 			400,
			bodyStyle:		'padding: 10px',			
			items: [
				{
					xtype:		'container',
					title: 		'Registros sin Errores',					    
					items: [
							{
								store: 		registrosSinErroresData,
								xtype: 		'grid',
								id:			'gridRegistrosSinErrores',
								stripeRows: true,
								loadMask: 	true,								
								width: 650,
								height: 300,
								columns: [
									{
										header: 		'Claves de Documentos',
										tooltip: 	'Claves de Documentos',
										dataIndex: 	'CLAVE_DOCUMENTOS',
										sortable: 	true,
										resizable: 	true,
										width: 		200,										
										hideable:	false
									}
								]					
							}					
					]
					
				},
				{
					xtype:		'container',
					title: 		'Registros con Errores',					    
					items: [
							{
								store: 		registrosConErroresData,
								xtype: 		'grid',
								id:			'gridRegistrosConErrores',
								stripeRows: true,
								loadMask: 	true,								
								width: 650,
								height: 300,
								columns: [
									{
										header: 		'Descripci�n',
										tooltip: 	'Descripci�n',
										dataIndex: 	'DESCRIPCION',
										sortable: 	true,
										resizable: 	true,
										width: 		600,										
										hideable:	false
									}
								]					
							}					
					]					
				}
			]
		},
		
		{
			xtype: 'panel',
			layout:		'form',
			labelWidth: 100,
			style:		"padding:8px;", 
			items: [
				{ 
					xtype: 			'textfield',
					fieldLabel:		'Total de Registros',
					name: 			'totalRegistros',
					id: 				'totalRegistros',
					readOnly:		true,
					hidden: 			false,
					maxLength: 		15,	
					msgTarget: 		'side',
					style: {
						textAlign: 'right'
					}
				}
			]
		},	
		{
			xtype: 			'container',
			width: 			'100%',
			style: 			'margin: 8px;padding-top:15px;',
			renderHidden: 	true,
			layout: {
				type: 		'hbox',
				pack: 		'center'
			},
			items: [
				{
					xtype: 		'button',
					width: 		100,
					text: 		'Cancelar',
					iconCls: 	'cancelar',
					id: 			'btnCancelarInsercion',				
					handler:    function(boton, evento) {
						window.location = '13ConfirmaDoctosExt.jsp';		
					}
				},				
				{
					xtype: 		'button',					
					text: 		'Descargar Errores',
					iconCls: 	'icoXls',										
					id: 			'btnDescargaErrores',					
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13ConfirmaDoctos.data.jsp',
							params: Ext.apply(fpCargaArchivo.getForm().getValues(),{
							informacion: 'DescargaArchivoError',
							datosErrores: Ext.getCmp('datosErrores').getValue()
						}),
						callback: procesarDescargaArchivos
						});						
					}
				}
			]
		}
	];
	
	var panelResultadosValidacion = {
		title:			'Resultado de la Validaci�n',
		hidden:			true,
		xtype:			'panel',
		id: 				'panelResultadosValidacion',
		width: 			700, //949,
		frame: 			true,
		style: 			'margin:  0 auto',
		bodyStyle:		'padding: 10px',
		items: 			elementosResultadosValidacion
	};
	
//---------------------

	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{	name: 'NOMBRE_CAMPO'},		
			{	name: 'DESCRIPCION'},
			{	name: 'TIPO_CAMPO'},
			{	name: 'LONGITUD'}	
		]
	});

	var infoLayoutAgrega = [
		['IC_DOCUMENTO','N�mero de documento con estatus en Seleccionada Pyme', 'N�merico', '38'],
		['Clave del Estatus ','Los documentos Seleccionada Pyme se pueden cambiar a: <br>Negociable (2) y Operada (4)', 'N�merico', '1'],
		['Causas del rechazo','Causas por las que se cambia el estatus del documento','Alfanum�rico','260' ]
	];
	
	storeLayoutData.loadData(infoLayoutAgrega);
	

	
	var gridLayout = {
		xtype: 'grid',	
		id: 'gridLayout',
		store: storeLayoutData,
		hidden: true,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		title: '<center>Layout de Confirmaci�n Doctos</center>',		
		columns: [
			{
				header : 'Nombre del Campo',
				tooltip: 'Nombre del Campo',
				dataIndex : 'NOMBRE_CAMPO',
				width : 120,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex : 'DESCRIPCION',
				width : 300,
				sortable : true,
				align: 'left'
			},
			{
				header : 'Tipo de Campo',
				dataIndex : 'TIPO_CAMPO',
				width : 100,
				sortable : true,
				align: 'center'
			},
			{
				header : 'Longitud',
				dataIndex : 'LONGITUD',
				width : 80,
				sortable : true,
				align: 'center'
			},
			{
				xtype: 'actioncolumn',
				header : 'Obligatorio',			
				sortable : true,
				align: 'center',
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {													
							return 'icoAceptar';																
						}						
					}
				]	
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 700,
		height: 180,
		frame: true
	};

//*****************Interfaz carga Archivo ****************+
	var enviarArchivo = function() {
		
		var archivo = Ext.getCmp('archivoCarga');
		if(Ext.isEmpty(archivo.getValue())){
			archivo.markInvalid('Favor de cargar un archivo');
			archivo.focus();
			return;
		}
		var fpCargaArchivo = Ext.getCmp('fpCargaArchivo');
		
		fpCargaArchivo.getForm().submit({
		url: '13ConfirmaDoctos.ma.jsp',									
			waitMsg: 'Enviando datos...',
			waitTitle :'Por favor, espere',			
			success: function(form, action) {
				var resp = action.result;				
				var mensaje = resp.mensaje;
				var nombreArchivo = resp.nombreArchivo;
				if(mensaje=='') {		
					fpCargaArchivo.el.mask('Procesando...', 'x-mask-loading');			
		
					Ext.Ajax.request({
						url: '13ConfirmaDoctos.data.jsp',
						params: {
							informacion: "CargaArchivo",							
							nombreArchivo:nombreArchivo							
						},
						callback: procesaCargaArchivo
					});
				}else {
					Ext.MessageBox.alert("Mensaje",mensaje);
					return;
				}								
			}
			,failure: NE.util.mostrarSubmitError
		});		
	}


	var myValidFnTXT = function(v) {
		var myRegex = /^.+\.([tT][xX][tT])$/;
		return myRegex.test(v);
	}
	Ext.apply(Ext.form.VTypes, {
		archivoTxt		: myValidFnTXT,
		archivoTxtText 	: 'El formato de el Archivo no es v�lido. Formato(s) soportado(s): TXT.'
	});
	
	var  elementosCargaArchivo= [
		{
			xtype: 'button',
			id: 'btnAyuda',
			columnWidth: .05,
			autoWidth: true,
			autoHeight: true,
			iconCls: 'icoAyuda',
			handler: function(){	
				var gridLayout = Ext.getCmp('gridLayout');
				if (!gridLayout.isVisible()) {
					gridLayout.show();
				}else{
					gridLayout.hide();
				}				
			}				
		},
		{
			xtype: 'panel',
			id:		'pnlArchivo',				
			layout: 'form',
			fileUpload: true,
			labelWidth: 150,					
			items: [
				{
					xtype: 'fileuploadfield',
					id: 'archivoCarga',
					width: 150,	  
					emptyText: 'Carga Archivo de',
					fieldLabel: 'Carga Archivo de',
					name: 'archivoCarga',
					buttonText: 'Examinar...',						
					anchor: '90%',
					vtype: 'archivoTxt'						
				}					
			]
		},
		{
			xtype: 'displayfield',
			value: 'El archivo a cargar deber� ser un .txt y deber� contener 350 registros m�ximo'				
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'datosErrores', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'n_documentos', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'doctosEstatus4', value: '' },		
		{ 	xtype: 'textfield',  hidden: true, id: 'total2', 	value: '' }, 
		{ 	xtype: 'textfield',  hidden: true, id: 'total4', 	value: '' } 
	];

	var fpCargaArchivo = new Ext.form.FormPanel({
		id: 				'fpCargaArchivo',
		width: 			700,
		title: 			'Confirmac�n Doctos',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		fileUpload: 	true,	
		renderHidden:	true,
		style:			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		labelWidth:		190,
		defaultType:	'textfield',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementosCargaArchivo,
		monitorValid: 	false,
		buttons: [
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivo
			},
			{
				text: 'Cancelar',
				id: 'btnCancelar',
				iconCls: 'icoRechazar', 
				formBind: true,
				handler: function() {
					window.location = '13ConfirmaDoctosExt.jsp';					
				}
			}
		]
	});
	
	
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			fpCargaArchivo,
			gridLayout,			
			panelResultadosValidacion,
			mensaje0,
			NE.util.getEspaciador(20)	,
			gridConsultaTotales,
			NE.util.getEspaciador(20)	,
			mensaje1,
			NE.util.getEspaciador(20)	,
			gridCifrasControl,
			NE.util.getEspaciador(20)	,
			gridConsulta,
			gridTotales1,
			NE.util.getEspaciador(20)	,
			mensaje2,
			NE.util.getEspaciador(20)	,
			gridConsulta2,
			gridTotales2,
			NE.util.getEspaciador(20),
			mensaje3,
			NE.util.getEspaciador(20),
			fpBotones,
			gridHiddeDoctos
		]
	});

			
});