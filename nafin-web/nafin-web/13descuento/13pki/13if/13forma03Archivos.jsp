<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	javax.naming.*,	
	java.text.*,
	java.util.*,
	java.math.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*,
	com.netro.pdf.*,
	com.netro.mandatodoc.*,
	netropology.utilerias.*" 
errorPage="/00utils/error_extjs.jsp"
%>
	
<%!
public static final boolean SIN_COMAS = false;
public static final int MAX_NUM_CAMPOS_ADICIONALES = 5;
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%  
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String tipoArchivo = (request.getParameter("tipoArchivo")!=null)?request.getParameter("tipoArchivo"):"";
String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")!=null)?request.getParameter("ic_banco_fondeo"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String ic_pyme = (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
String claveDoctoTodos = (request.getParameter("claveDoctoTodos")!=null)?request.getParameter("claveDoctoTodos"):"";
if(ic_pyme.equals(""))  ic_pyme="TODOS";
String  ic_if =iNoCliente;
String infoRegresar = "",  claveDocumento ="", numSirac ="",  nombrePyme ="", numDocumento ="", numProveedorInterno ="",  nombreEpo ="",
		fechaDocumento ="",  fechaVencimiento ="", claveMoneda ="", nombreMoneda ="", monto ="", importeInteres ="",
		importeRecibir  ="", tasaAceptada  ="", montoDescuento ="",  plazo  ="",  porcentajeAnticipo ="", 
		recursoGarantia ="", 	estatusDocumento ="", fechaAlta ="", referencia ="",  descuentoEspecial ="", 
		claveEstatusDocumento ="",  	beneficiario ="",  bancoBeneficiario  ="",  sucursalBeneficiario ="", cuentaBeneficiario ="",
		porcBeneficiario ="",  importeRecibirBenef ="", netoRecibirPyme ="", tipoFactorajeFijo ="", tipoFactoraje ="",
		numNaeBeneficiario ="",referenciaFondeo = "";	
String  bOperaFactorajeVencido ="N",  bOperaFactorajeDist ="N", bOperaFactorajeMand ="N", bOperaFactorajeVencInfonavit ="N", bTipoFactoraje ="N";
String vhoraEnvOpeIf = "";
boolean horaValidaEnvIf = true;		
JSONObject jsonObj = new JSONObject(); 
Hashtable alParamEPO = new Hashtable (); 
HashMap datos = new HashMap();
JSONArray registrosC = new JSONArray();
SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
java.util.Date d1 = null;
java.util.Date d2 = null;
java.util.Date d3 = null;
java.util.Date d4 = null;
java.util.Date d5 = null;
StringBuffer contenidoArchivo = new StringBuffer();

AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
MandatoDocumentos mandatoDocumentos = ServiceLocator.getInstance().lookup("MandatoDocumentosEJB", MandatoDocumentos.class);


alParamEPO = BeanParamDscto.getParametrosEPO(ic_epo, 1);
horaValidaEnvIf = BeanParamDscto.validaHoraEnvOpeIFs(ic_epo);
if(!horaValidaEnvIf){
	vhoraEnvOpeIf = alParamEPO.get("HORA_ENV_OPE_IF")==null?"":(String)alParamEPO.get("HORA_ENV_OPE_IF");
 }		
if (!ic_epo.equals("")) {		
		bOperaFactorajeVencido = alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString();	
		bOperaFactorajeDist = alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString();
		bOperaFactorajeMand = alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString();
		bOperaFactorajeVencInfonavit = alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString();
	}
   if( bOperaFactorajeVencido.equals("S") || bOperaFactorajeDist.equals("S") || bOperaFactorajeMand.equals("S") || bOperaFactorajeVencInfonavit.equals("S")) {
  bTipoFactoraje ="S";
  }	
	
CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = null;
ComunesPDF pdfDoc = new ComunesPDF();

String  strFecha ="",   strMes= "", strHora = "", strMinuto = "", strSegundo = "";
GregorianCalendar grcFecha = new GregorianCalendar();
java.util.Date	 dtmFecha = new java.util.Date();
grcFecha.setTime(dtmFecha);
strMes = Integer.toString(grcFecha.get(Calendar.MONTH) + 1);
strMes = (strMes.length() > 1) ? strMes.toString() : "0" + strMes.toString();
strHora = Integer.toString(grcFecha.get(Calendar.HOUR_OF_DAY));
strHora = (strHora.length() > 1) ? strHora.toString() : "0" + strHora.toString();
strMinuto = Integer.toString(grcFecha.get(Calendar.MINUTE));
strMinuto = (strMinuto.length() > 1) ? strMinuto.toString() : "0" + strMinuto.toString();
strSegundo = Integer.toString(grcFecha.get(Calendar.SECOND));
strSegundo = (strSegundo.length() > 1) ? strSegundo.toString() : "0" + strSegundo.toString();
strFecha = grcFecha.get(Calendar.DAY_OF_MONTH) + "/" +
strMes.toString() + "/" +
grcFecha.get(Calendar.YEAR);
/* El query para obtener los campos adicionales definidos por la epo en comrel_visor */
int numCamposAdicionales = 0;
String [] campoAdicional;
numCamposAdicionales = BeanAutDescuento.getNumCamposAdicionales(ic_epo);
campoAdicional = BeanAutDescuento.getCamposAdicionales(ic_epo);
String lineacd = "",lineacdfr = "";
int numRegistrosMN = 0;
int numRegistrosDL = 0;
double dblRegistros = 0;
double dblTotalMontoPesos = 0;
double dblTotalDescuentoPesos = 0;
double dblTotalOperarPesos = 0;
double dblTotalRecursosPesos = 0;
double dblTotalInteresPesos = 0;
double dblTotalMontoDolares = 0;
double dblTotalDescuentoDolares = 0;
double dblTotalOperarDolares = 0;
double dblTotalRecursosDolares = 0;
double dblTotalInteresDolares = 0;
BigDecimal montoTotalMN = new BigDecimal("0.00");
BigDecimal montoTotalDL = new BigDecimal("0.00");

int columnas =1;
if(!bOperaFactorajeDist.equals("S") || !bOperaFactorajeVencInfonavit.equals("S") ) columnas +=7;

int columnasI =5;
if(numCamposAdicionales<5)
	columnasI+=5-numCamposAdicionales;

if (informacion.equals("ArchivoCSV")  ||   informacion.equals("ArchivoPDF")  ||  informacion.equals("ArchivoVariable")  
	|| informacion.equals("ArchivoFijo") || informacion.equals("ArchivoInterfase")  ) {
	
	Vector registros;
	String existe_mandato2 ="N"; 
	registros = BeanAutDescuento.getDoctosSelecPyme(ic_pyme,ic_epo,iNoCliente, claveDoctoTodos);
	
	int numRegistros = registros.size();
	boolean existe_mandato = mandatoDocumentos.existeMandatoDocumentos(registros);//FODEA 041 - 2009 ACF
	if(existe_mandato)  existe_mandato2="S";
	
	// archivo de Exportar Interfase Variabl
	if(informacion.equals("ArchivoVariable")){
		contenidoArchivo.append(" H," + strFecha + "," + strHora + ":" + strMinuto + ":" + strSegundo + ",Documentos por Autorizar \n");
		contenidoArchivo.append(" D , Número Documento, Clave PYME, Número SIRAC,Nombre PYME, Clave EPO, "+
										"Nombre EPO, Clave IF, Nombre IF, Fecha Documento, Fecha Vencimiento, Clave Moneda, Moneda");
		if(bTipoFactoraje.equals("S")) contenidoArchivo.append(",Tipo Factoraje");
		 contenidoArchivo.append(" , Monto Documento, Porcentaje Descuento, Recurso Garantía, Monto Descuento, Monto Interés, Monto Operar, "+
										 " Tasa Interés, Plazo, Clave Estatus, Estatus, Referencia, CG_CAMPO1,	CG_CAMPO2,	CG_CAMPO3,	CG_CAMPO4,	CG_CAMPO5, "+
										" Fecha Alta ");		
		if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencInfonavit.equals("S") ){ 
			contenidoArchivo.append(" ,Beneficiario,Banco Beneficiario, Sucursal Beneficiaria, Cuenta Beneficiaria"+
									" ,% Beneficiario, Importe a Recibir del Beneficiario, Neto a Recibir PyME, No. N@E Beneficiario"); 
		}
		contenidoArchivo.append("\n");	
	}
	// archivo de CSV Normal
	if(informacion.equals("ArchivoCSV")){
		contenidoArchivo.append(" Epo Relacionada,No. Cliente SIRAC,Proveedor,Número de documento"+
				",Fecha Emisión,Fecha Vencimiento,Moneda ,Monto,Monto Descuento,Monto Interés,Monto a operar"+
				",Tasa,Plazo,No. Proveedor,Porcentaje de Descuento,Recurso en garantía");
	
		if(bTipoFactoraje.equals("S")) { contenidoArchivo.append(",Tipo Factoraje");  }
		if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencInfonavit.equals("S") ){//FODEA 042 - 2009 ACF
			contenidoArchivo .append(",Beneficiario,Banco Beneficiario, Sucursal Beneficiaria, Cuenta Beneficiaria"+
											 " ,% Beneficiario, Importe a Recibir del Beneficiario, Neto a Recibir PyME");
		}
		contenidoArchivo.append(",Observación \n");	
	}
	// archivo Exportar Interfase Fijo 
	if(informacion.equals("ArchivoFijo")){
		contenidoArchivo.append("H" + Comunes.formatoFijo(strFecha,10,"0","N") + strHora + ":" + strMinuto + ":" + strSegundo + "Documentos por Autorizar\n");
	}
	
	if(informacion.equals("ArchivoPDF")  || informacion.equals("ArchivoInterfase")  ){
		nombreArchivo = archivo.nombreArchivo()+".pdf";	
		Calendar fecha = Calendar.getInstance();
		pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String nombreMesIngles[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		int mes = fecha.get(Calendar.MONTH);

	   pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),  session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"), (String) session.getAttribute("strNombre"), (String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
									
		if (sesIdiomaUsuario.equals("EN")) {
    		pdfDoc.addText("Mexico City  "+ nombreMesIngles[mes]+" "+ diaActual+" "+anioActual+"-----------------"+horaActual,"formas",ComunesPDF.RIGHT);
		} else {
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		}
	}
	// archivo de PDF Normal
	if(informacion.equals("ArchivoPDF") ) {		
				
		pdfDoc.setTable(9,100);
		pdfDoc.setCell("Epo Relacionada","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("No. Cliente SIRAC","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Proveedor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Emisión","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto Descuento","celda01",ComunesPDF.CENTER);
		
		pdfDoc.setCell("Monto Interés","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a operar","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("No. Proveedor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Recurso en garantía","celda01",ComunesPDF.CENTER);
		if(bTipoFactoraje.equals("S")) { 
			pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER);
		}else {
			//pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER, 2);
			pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER);
		}
		
		if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencInfonavit.equals("S") ){//FODEA 042 - 2009 ACF
			pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta Beneficiaria","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Importe a Recibir del Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
		}	
		pdfDoc.setCell("Observación","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
		if(columnas>0){
			//pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER, columnas-1);	
		}
	}
	
	if(informacion.equals("ArchivoInterfase") ) {
		pdfDoc.setTable(12,100);
		pdfDoc.setCell("Fecha Generacion : ","celda01",ComunesPDF.LEFT,2);
		pdfDoc.setCell(strFecha + "," + strHora + ":" + strMinuto + ":" + strSegundo,"formas",ComunesPDF.LEFT, 10);
		pdfDoc.setCell("Reporte : ","celda01",ComunesPDF.LEFT,2);
		pdfDoc.setCell("Documentos por Autorizar ","formas",ComunesPDF.LEFT, 10);
		
		pdfDoc.setCell("A ","celda01",ComunesPDF.CENTER);		
		pdfDoc.setCell(" Número Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Clave PYME","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Número SIRAC","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Nombre PYME","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Clave EPO","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Nombre EPO","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Clave IF","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Nombre IF","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Fecha Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Fecha Vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Moneda","celda01",ComunesPDF.CENTER);
		
		pdfDoc.setCell("B ","celda01",ComunesPDF.CENTER);
		if(bTipoFactoraje.equals("S")) 
		pdfDoc.setCell(" Tipo Factoraje","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Porcentaje Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Recurso Garantía","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto Interés","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Monto Operar ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Tasa Interés ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Plazo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Estatus","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Referencia","celda01",ComunesPDF.CENTER);
		if(bTipoFactoraje.equals("N"))
		pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
		
		pdfDoc.setCell("C","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(" Fecha Alta ","celda01",ComunesPDF.CENTER);
		for (int i = 0; i<numCamposAdicionales; i++) {
			pdfDoc.setCell(campoAdicional[i],"celda01",ComunesPDF.CENTER);
		}		
		pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER, columnasI);

		if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencInfonavit.equals("S") ){
			pdfDoc.setCell("D","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta Beneficiaria","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Importe a Recibir del Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Neto a Recibir PyME ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("No. N@E Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER, 3);
		}
	}
		
	for (int i = 0; i < numRegistros; i++) {
		Vector registro = (Vector) registros.get(i);
		claveDocumento = registro.get(0).toString();
		numSirac = registro.get(1).toString();
		nombrePyme = registro.get(2).toString();
		numDocumento = registro.get(3).toString();
		numProveedorInterno = registro.get(4).toString();
		nombreEpo = registro.get(5).toString();
		fechaDocumento = registro.get(6).toString();
		fechaVencimiento = registro.get(7).toString();
		claveMoneda = registro.get(8).toString();
		nombreMoneda = registro.get(9).toString();
		monto = registro.get(10).toString();
		importeInteres = registro.get(11).toString();
		importeRecibir = registro.get(12).toString();
		tasaAceptada = registro.get(13).toString();
		montoDescuento = registro.get(14).toString();
		plazo = registro.get(15).toString();
		porcentajeAnticipo = registro.get(16).toString();
		recursoGarantia = registro.get(17).toString();
		estatusDocumento = registro.get(18).toString();	
		fechaAlta = registro.get(19).toString();
		referencia = registro.get(20).toString();
		descuentoEspecial = registro.get(21).toString();
		claveEstatusDocumento = registro.get(22).toString();
		beneficiario = registro.get(23).toString();
		bancoBeneficiario = registro.get(24).toString();
		sucursalBeneficiario = registro.get(25).toString();
		cuentaBeneficiario = registro.get(26).toString();
		porcBeneficiario = registro.get(27).toString();
		importeRecibirBenef = registro.get(28).toString();
		netoRecibirPyme = registro.get(29).toString();
		tipoFactorajeFijo = registro.get(21).toString();
      tipoFactoraje = registro.get(30).toString();
		numNaeBeneficiario = registro.get(31).toString();
		String colorfuente = "";
		String msgCausa1 ="";
		String msgCausa2 ="";
		String msgCausa3 ="";//FODEA 041 - 2009 ACF
		java.util.Date d6 = formatter.parse(fechaVencimiento);

		referenciaFondeo = registro.get(32).toString();//FODEA 017-2013
		
		if(d1 != null){
				if(d6.getTime()>=d1.getTime() ){				
					msgCausa1 = "Vencimiento Línea de Crédito";
				}
			}
			if(d3 != null){
				if(d6.getTime()>=d3.getTime()){				
					if(!msgCausa1.equals(""))
						msgCausa2 = " / Cambio de Administración";
					else
						msgCausa2 = "Cambio de Administración";
				}
			}
		String  observaciones1 =!msgCausa1.equals("")?msgCausa1:"";
		String  observaciones2 =!msgCausa2.equals("")?msgCausa2:"";
		String  observaciones3 =!msgCausa3.equals("")?msgCausa3:"";
		String  observaciones= observaciones1 +observaciones2 +observaciones3;

		if(porcBeneficiario.equals("")){ porcBeneficiario = "0.00"; }
		if(importeRecibirBenef.equals("")){ importeRecibirBenef = "0.00"; }

		if (claveMoneda.equals("1")) {	//M.N.
			numRegistrosMN++;
			if (!importeRecibir.equals("")) {
					montoTotalMN = montoTotalMN.add(new BigDecimal(importeRecibir));
				}

				dblTotalMontoPesos += new Double(monto).doubleValue();
				dblTotalDescuentoPesos += new Double(montoDescuento).doubleValue();
				dblTotalOperarPesos += new Double(importeRecibir).doubleValue();

				dblTotalRecursosPesos += new Double(recursoGarantia).doubleValue();
				dblTotalInteresPesos += new Double(importeInteres).doubleValue();
			} else if (claveMoneda.equals("54")) {	//DLS
				numRegistrosDL++;
				if (!importeRecibir.equals("")) {
					montoTotalDL = montoTotalDL.add(new BigDecimal(importeRecibir));
				}

				dblTotalMontoDolares += new Double(monto).doubleValue();
				dblTotalDescuentoDolares += new Double(montoDescuento).doubleValue();
				dblTotalOperarDolares += new Double(importeRecibir).doubleValue();

				dblTotalRecursosDolares += new Double(recursoGarantia).doubleValue();
				dblTotalInteresDolares += new Double(importeInteres).doubleValue();
			}
			lineacd = "";
			lineacdfr = ""; 
			
			for (int j = 1; j <= MAX_NUM_CAMPOS_ADICIONALES; j++) {
				if (j <= numCamposAdicionales) {
					lineacd +=  (registro.get(32 + j).toString()).replace(',',' ') + ",";
					lineacdfr += Comunes.formatoFijo(registro.get(32 + j).toString(),50," ","A");
				} else {
					lineacd += ",";	
					lineacdfr += Comunes.formatoFijo(" ",50," ","A");
				}
			}
			
		
		// archivo de Exportar Interfase Variable
		if(informacion.equals("ArchivoVariable")){
		
			contenidoArchivo.append("D, "+numDocumento + "," +
						numProveedorInterno + "," +
						numSirac + "," +
						nombrePyme.replace(',',' ') + "," +
						ic_epo + "," +
						nombreEpo.replace(',',' ') + "," +
						iNoCliente + "," +
						strNombre.replace(',',' ') + "," +
						fechaDocumento + "," +
						fechaVencimiento + "," +
						claveMoneda + "," +
						nombreMoneda + ",");
		if(bTipoFactoraje.equals("S"))
			contenidoArchivo.append(tipoFactoraje + ",");
		contenidoArchivo.append(Comunes.formatoDecimal(monto,2,SIN_COMAS) + "," +
						Comunes.formatoDecimal(porcentajeAnticipo,0,SIN_COMAS) + "," +
						Comunes.formatoDecimal(recursoGarantia,2,SIN_COMAS) + "," +
						Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS) + "," +
						Comunes.formatoDecimal(importeInteres,2,SIN_COMAS) + "," +
						Comunes.formatoDecimal(importeRecibir,2,SIN_COMAS) + "," +
						Comunes.formatoDecimal(tasaAceptada,5,SIN_COMAS) + "," +
						plazo + "," +
						claveEstatusDocumento + "," +
						estatusDocumento + "," +
						referencia.replace(',',' ') + "," +//se remplazan comas por un espacio en blanco para evitar salto de linea
						lineacd +
						fechaAlta);
			if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencInfonavit.equals("S")){//FODEA 034 - 2009 ACF			
				contenidoArchivo.append(","+ beneficiario.replace(',',' ') + ","+
												bancoBeneficiario.replace(',',' ') + "," +//FODEA 032 - 2010 REQ4 Rebos
												sucursalBeneficiario + ","+
												cuentaBeneficiario + ","+
												porcBeneficiario + ","+
												importeRecibirBenef + ","+
												netoRecibirPyme + ","+
												numNaeBeneficiario);//FODEA 032 - 2010 REQ4 Rebos
			}			
			contenidoArchivo.append("\n");
		}
		// archivo de CSV Normal
		if(informacion.equals("ArchivoCSV")){
				// Generacion de archivo normal.
			contenidoArchivo.append(nombreEpo.replace(',',' ')+
					"," + numSirac+","+nombrePyme.replace(',',' ')+
					"," + numDocumento+
					"," + fechaDocumento+","+fechaVencimiento+","+nombreMoneda+
					"," + Comunes.formatoDecimal(monto,2,SIN_COMAS)+
					"," + Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS)+
					"," + Comunes.formatoDecimal(importeInteres,2,SIN_COMAS)+
					"," + Comunes.formatoDecimal(importeRecibir,2,SIN_COMAS)+
					"," + Comunes.formatoDecimal(tasaAceptada,5,SIN_COMAS)+
					"," + plazo + "," + numProveedorInterno+
					"," + Comunes.formatoDecimal(porcentajeAnticipo,0,SIN_COMAS)+
					"," + Comunes.formatoDecimal(recursoGarantia,2,SIN_COMAS));
	
			if(bTipoFactoraje.equals("S"))
				contenidoArchivo.append("," + tipoFactoraje);
			
			if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencInfonavit.equals("S")){//FODEA 034 - 2009 ACF
				contenidoArchivo.append(","+ beneficiario.replace(',', ' ') + ","+
												bancoBeneficiario.replace(',', ' ') + ","+//FODEA 041 - 2009 ACF
												sucursalBeneficiario + ","+
												cuentaBeneficiario + ","+
												porcBeneficiario + ","+
												importeRecibirBenef + ","+
												netoRecibirPyme);	
			}			
			contenidoArchivo.append("," + observaciones+"\n");
		} 
		if(informacion.equals("ArchivoFijo")){
			contenidoArchivo.append("\nD" +
				Comunes.formatoFijo(numDocumento,15," ","A") +
				Comunes.formatoFijo(numProveedorInterno,25," ","A") +
				Comunes.formatoFijo(numSirac,12,"0","") +
				Comunes.formatoFijo(nombrePyme,100," ","A") +
				Comunes.formatoFijo(ic_epo,6," ","A") +
				Comunes.formatoFijo(nombreEpo,100," ","A") +
				Comunes.formatoFijo(iNoCliente,6," ","A") );
				if(ic_if.equals("9")) {
					contenidoArchivo.append(Comunes.formatoFijo(strNombre,60," ","A") );
				}else {
					contenidoArchivo.append(Comunes.formatoFijo(strNombre,100," ","A") );
				}				
			contenidoArchivo.append(Comunes.formatoFijo(fechaDocumento,10," ","") +
				Comunes.formatoFijo(fechaVencimiento,10," ","") +
				Comunes.formatoFijo(new Integer(claveMoneda).toString(),3," ","A") +
				Comunes.formatoFijo(nombreMoneda,30," ","A"));
		
			contenidoArchivo.append(Comunes.formatoFijo(tipoFactorajeFijo,1," ","A"));
			contenidoArchivo.append(Comunes.formatoFijo(Comunes.formatoDecimal(monto,2,SIN_COMAS),15,"0","") +
							Comunes.formatoFijo(Comunes.formatoDecimal(porcentajeAnticipo,0,SIN_COMAS),3,"0","") +
							Comunes.formatoFijo(Comunes.formatoDecimal(recursoGarantia,2,SIN_COMAS),15,"0","") +
							Comunes.formatoFijo(Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS),15,"0","") +
							Comunes.formatoFijo(Comunes.formatoDecimal(importeInteres,2,SIN_COMAS),15,"0","") +
							Comunes.formatoFijo(Comunes.formatoDecimal(importeRecibir,2,SIN_COMAS),15,"0","") +
							Comunes.formatoFijo(Comunes.formatoDecimal(tasaAceptada,5,SIN_COMAS),9,"0","") +
							Comunes.formatoFijo(plazo,3,"0","") +
							Comunes.formatoFijo(claveEstatusDocumento,2," ","A") +
							Comunes.formatoFijo(estatusDocumento,20," ","A") +
							Comunes.formatoFijo(referencia,50," ","A") +
							lineacdfr+
							Comunes.formatoFijo(fechaAlta,10," ",""));
							
			if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencInfonavit.equals("S")){//FODEA 034 - 2009 ACF
				contenidoArchivo.append(Comunes.formatoFijo(beneficiario,60," ","")+
					Comunes.formatoFijo(bancoBeneficiario,40," ","")+
					Comunes.formatoFijo(sucursalBeneficiario,40," ","")+
					Comunes.formatoFijo(cuentaBeneficiario,15," ","")+
					Comunes.formatoFijo(porcBeneficiario ,6,"0","2")+
					Comunes.formatoFijo(importeRecibirBenef,19,"0","2")+
					Comunes.formatoFijo(netoRecibirPyme,19,"0","2"));
			}		
			contenidoArchivo.append("\n");
		}
							
		// archivo de PDF Normal
		if(informacion.equals("ArchivoPDF")){

			pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(numSirac,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(numDocumento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaDocumento ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombreMoneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(importeInteres,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(importeRecibir,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(tasaAceptada,5)+"%","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numProveedorInterno,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(porcentajeAnticipo,0)+"%","formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(recursoGarantia,2),"formas",ComunesPDF.RIGHT);
			if(bTipoFactoraje.equals("S")) { 
				pdfDoc.setCell(tipoFactoraje,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("  ","formas",ComunesPDF.CENTER);
			}else {
				pdfDoc.setCell("  ","formas",ComunesPDF.CENTER, 2);	
			}
		
			if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencInfonavit.equals("S")){//FODEA 034 - 2009 ACF
				pdfDoc.setCell(beneficiario.replace(',', ' '),"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(bancoBeneficiario.replace(',', ' '),"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(sucursalBeneficiario,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(cuentaBeneficiario,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(porcBeneficiario+"%","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+importeRecibirBenef,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(netoRecibirPyme,2)  ,"formas",ComunesPDF.RIGHT);
			}			
			pdfDoc.setCell(observaciones,"formas",ComunesPDF.LEFT);	
			pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			
			if(columnas>0){
			//pdfDoc.setCell("  ","formas",ComunesPDF.CENTER, columnas-1);	
			}
		}
		
		
		if(informacion.equals("ArchivoInterfase")){
			pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(numDocumento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numProveedorInterno ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numSirac ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme.replace(',',' '),"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(ic_epo ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombreEpo.replace(',',' '),"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(iNoCliente ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(strNombre.replace(',',' ') ,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(fechaDocumento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencimiento ,"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(nombreMoneda ,"formas",ComunesPDF.CENTER);
			
			pdfDoc.setCell("B ","celda01",ComunesPDF.CENTER);
			if(bTipoFactoraje.equals("S"))
				pdfDoc.setCell(tipoFactoraje ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2,SIN_COMAS),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(porcentajeAnticipo,0)+"%" ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(recursoGarantia,2) ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescuento,2) ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(importeInteres,2) ,"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(importeRecibir,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(tasaAceptada,5)+"%" ,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(plazo ,"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell(estatusDocumento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(referencia.replace(',',' ') ,"formas",ComunesPDF.CENTER);	
			if(bTipoFactoraje.equals("N"))
				pdfDoc.setCell("" ,"formas",ComunesPDF.CENTER);
			
			pdfDoc.setCell("C ","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell(fechaAlta);
			for (int j = 1; j <= numCamposAdicionales ; j++) {	
				pdfDoc.setCell(registro.get(32 + j).toString(),"formas",ComunesPDF.CENTER);
			}		
			pdfDoc.setCell("  ","formas",ComunesPDF.CENTER, columnasI);	
			
			if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencInfonavit.equals("S")){	
				pdfDoc.setCell(" D ","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell(beneficiario.replace(',',' '),"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(bancoBeneficiario.replace(',',' '),"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(sucursalBeneficiario ,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(cuentaBeneficiario ,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(porcBeneficiario+"%" ,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(importeRecibirBenef,2)   ,"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(netoRecibirPyme,2)  ,"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(numNaeBeneficiario,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("  ","formas",ComunesPDF.CENTER, 3);	
			}			
			
		}
		
		
		
	}// FOR
	
	if(informacion.equals("ArchivoPDF")  || informacion.equals("ArchivoInterfase") ){
		pdfDoc.addTable();		
	}
	if(informacion.equals("ArchivoInterfase") ){
		pdfDoc.setTable(12,100);
		pdfDoc.setCell("Total Registros MN","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(Comunes.formatoDecimal(numRegistrosMN,0),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("Total Monto Documentos MN","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalMontoPesos,2), "formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Recursos Garantia MN","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalRecursosPesos,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Descuento MN","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalDescuentoPesos,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Interes MN","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalInteresPesos,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Operar MN","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalOperarPesos,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Registros DL","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell(Comunes.formatoDecimal(numRegistrosDL,0),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell("Total Monto Documentos DL","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalMontoDolares,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Recursos Garantia DL","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalRecursosDolares,2) ,"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Descuento DL","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalDescuentoDolares,2),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Interes DL","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalInteresDolares,2) ,"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("Total Monto Operar DL","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalOperarDolares,2),"formas",ComunesPDF.RIGHT);	
		pdfDoc.addTable();	
	}
	
	 if( informacion.equals("ArchivoVariable")){  
		// Guarda archivo variable.
		contenidoArchivo.append("T," +
					"Total Registros MN," + Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosMN,0,SIN_COMAS), 12, "0","") + "," +
					"Total Monto Documentos MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoPesos,2,SIN_COMAS), 15, "0","") + "," +
					"Total Recursos Garantia MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosPesos,2,SIN_COMAS), 15, "0","") + "," +
					"Total Monto Descuento MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoPesos,2,SIN_COMAS), 15, "0","") + "," +
					"Total Monto Interes MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresPesos,2,SIN_COMAS), 15, "0","") + "," +
					"Total Monto Operar MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarPesos,2,SIN_COMAS), 15, "0","") + "," +
					"Total Registros DL," + Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosDL,0,SIN_COMAS), 12, "0","") + "," +
					"Total Monto Documentos DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoDolares,2,SIN_COMAS), 15, "0","") + "," +
					"Total Recursos Garantia DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosDolares,2,SIN_COMAS), 15, "0","") + "," +
					"Total Monto Descuento DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoDolares,2,SIN_COMAS), 15, "0","") + "," +
					"Total Monto Interes DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresDolares,2,SIN_COMAS), 15, "0","") + "," +
					"Total Monto Operar DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarDolares,2,SIN_COMAS), 15, "0",""));					
	}	
	if( informacion.equals("ArchivoFijo")){  
		contenidoArchivo.append("\nT" +
			Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosMN,0,SIN_COMAS), 12, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoPesos,2,SIN_COMAS), 15, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosPesos,2,SIN_COMAS), 15, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoPesos,2,SIN_COMAS), 15, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresPesos,2,SIN_COMAS), 15, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarPesos,2,SIN_COMAS), 15, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosDL,0,SIN_COMAS), 12, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoDolares,2,SIN_COMAS), 15, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosDolares,2,SIN_COMAS), 15, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoDolares,2,SIN_COMAS), 15, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresDolares,2,SIN_COMAS), 15, "0","") +
			Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarDolares,2,SIN_COMAS), 15, "0",""));
			contenidoArchivo.append("\n\n");			
			
			if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")) {			
				nombreArchivo = archivo.nombre;	
				jsonObj.put("success", new Boolean(false));
				jsonObj.put("msg", "Error al generar el archivo");			
			} 	else {
				nombreArchivo = archivo.nombre;	
				jsonObj.put("success", new Boolean(true));						
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			}		
	}	
	
	if(informacion.equals("ArchivoPDF")  || informacion.equals("ArchivoInterfase") ){		
		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp + nombreArchivo);	
		
	} else  if(informacion.equals("ArchivoCSV") ||  informacion.equals("ArchivoVariable")){  
	
		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "Error al generar el archivo");
		} else {
			nombreArchivo = archivo.nombre;		
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
	}
	
	
}
System.out.println("jsonObj "+jsonObj);	
%>
<%=jsonObj%>

<%!





%>
