<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,		
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
	ParametrosRequest req = null;
	String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirectorioTemp;
	String nombreArchivo= "";
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	
		req = new ParametrosRequest(upload.parseRequest(request));
			 
		FileItem fItem = (FileItem)req.getFiles().get(0);
		itemArchivo		= (String)fItem.getName();
                
                //itemArchivo   = itemArchivo.substring(itemArchivo.lastIndexOf("\\"));
                
		InputStream archivo = fItem.getInputStream();
		rutaArchivo = PATH_FILE+itemArchivo;
		int tamanio			= (int)fItem.getSize();
		if(tamanio>2097152){
			error_tam ="El Archivo es muy Grande, excede el L�mite que es de 2 MB.";
		}		
		File file = new File(rutaArchivo);
		//fItem.write(new File(rutaArchivo));
                fItem.write(file);
	}
		
	if(!error_tam.equals("")){
		nombreArchivo="";
	}	

	%>
{
	"success": true,
	"archivo":	'<%=itemArchivo%>',	
	"error_tam":	'<%=error_tam%>'	
}



