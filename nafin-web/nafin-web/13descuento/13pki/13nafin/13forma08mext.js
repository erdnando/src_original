
var showGridLayout;

Ext.onReady(function() {
	
	Ext.form.Field.prototype.msgTarget 	= 'side';
	var listaTasasInsertar 					= [];
	var listaTasasEliminar  				= [];
	var TasasPreferenciales  				= [];
	var TasasNegociables  					= [];
		
//*****************************************para mostrar el Acuse ******************************

	var mensajeAutentificacion = new Ext.Container({
		layout: 	'table',		
		id: 		'mensajeAutentificacion',							
		width:	400,
		heigth:	'auto',
		style: 	'margin:0 auto;',
		hidden: 	true,
		columns: 1,
		items: [					
			{
				xtype: 'displayfield',
				id:	'autentificac',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'justify'
				}		
			}			
		]
	});
	
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
		} else {
			btnGenerarCSV.enable();
			btnGenerarCSV.setIconClass('icoXls');
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoPdf');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
		} else {
			btnGenerarPDF.enable();
			btnGenerarPDF.setIconClass('icoPdf');
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var botImprimir = new Ext.Container({
		layout: 	'table',		
		id: 		'botImprimir',							
		width:	'300',
		heigth:	'auto',
		style: 	'margin:0 auto;',
		hidden: 	true,
		columns: 1,	
		items: [			
			{
				xtype: 		'button',
				text: 		'Generar PDF',
				tooltip:		'Generar PDF',
				id: 			'btnGenerarPDF',
				hidden: 		true,
				iconCls: 	'icoPdf',
				//minWidth:	80,
				handler: function(boton, evento) {
				
					var recibo 		= Ext.getCmp('recibo').getValue();
					var acuse2 		= Ext.getCmp('acuse2').getValue();
					var fecha 		= Ext.getCmp('fecha').getValue();
					var hora 		= Ext.getCmp('hora').getValue();				
					var tipoTasaA 	= Ext.getCmp('tipoTasaA').getValue();
					
					var totalTasasCargadas;
					var totalTasasAlta;
					var totalTasasEliminar;
					
					TasasPreferenciales  = [];
					TasasNegociables  	= [];
					
					resumenAccionData.each(function(record) {
						if(record.data['ID']=='total_tasas_cargadas') 	{ totalTasasCargadas = record.data['TOTAL'];  }
						if(record.data['ID']=='total_tasas_alta') 		{ totalTasasAlta 		= record.data['TOTAL'];  }
						if(record.data['ID']=='total_tasas_eliminadas') { totalTasasEliminar = record.data['TOTAL'];  }		
					});
						
					// Preferencial 
					if( tipoTasaA == 'P' ) {
						
						var gridPreferencial = Ext.getCmp('gridPreferencial');
						var store1 				= gridPreferencial.getStore();
						store1.each(function(record) {			
							TasasPreferenciales.push(
								record.data['EPO_RELACIONADA'] 	+"|"+
								record.data['NOM_PROVEEDOR'] 		+"|"+
								record.data['ESTATUS'] 				+"|"+
								record.data['REFERENCIA'] 			+"|"+
								record.data['PLAZO'] 				+"|"+
								record.data['TIPO_TASA'] 			+"|"+
								record.data['VALOR'] 				+"|"+
								record.data['REL_MAT']				+"|"+
								record.data['PUNTOS']				+"|"+	
								record.data['VALOR_TASA']
							);
						});
						
					}
					
					// Negociada
					if(tipoTasaA =='N') {
						
						var gridNegociable 	= Ext.getCmp('gridNegociable');
						var store2 				= gridNegociable.getStore();
						store2.each(function(record) {			
							TasasNegociables.push(
								record.data['EPO_RELACIONADA'] 	+"|"+
								record.data['NOM_PROVEEDOR'] 		+"|"+
								record.data['ESTATUS'] 				+"|"+
								record.data['REFERENCIA'] 			+"|"+
								record.data['PLAZO'] 				+"|"+
								record.data['TIPO_TASA'] 			+"|"+
								record.data['VALOR']					+"|"+
								record.data['VALOR_TASA']
							);																			
						});
						
					}
					
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13forma08mext.data.jsp',
						params: {
								informacion:'ArchivoPDF',
								recibo:					recibo,
								acuse2:					acuse2,
								fechaCarga:				fecha,
								horaCarga:				hora,								
								tipoTasa:				tipoTasaA,
								TasasNegociables:		TasasNegociables,
								TasasPreferenciales:	TasasPreferenciales,
								totalTasasCargadas:	totalTasasCargadas,
								totalTasasAlta:		totalTasasAlta,
								totalTasasEliminar:	totalTasasEliminar
						},
						callback: procesarGenerarPDF
					});
				}
			},
			{
					xtype: 		'button',
					text: 		'Bajar Archivo',
					tooltip:		'Bajar Archivo',
					id: 			'btnBajarPDF',	
					iconCls:		'icoBotonPDF',
					hidden: 		true
					//minWidth:	80
			},			
			{
					xtype: 		'button',
					text: 		'Generar CSV',
					tooltip:		'Generar CSV',
					id: 			'btnGenerarCSV',
					hidden: 		true,
					iconCls: 	'icoXls',
					//minWidth:	80,
					handler: function(boton, evento) {
						var recibo = Ext.getCmp('recibo').getValue();
						var acuse2 = Ext.getCmp('acuse2').getValue();
						var fecha = Ext.getCmp('fecha').getValue();
						var hora = Ext.getCmp('hora').getValue();						
						var tipoTasaA = Ext.getCmp('tipoTasaA').getValue();	
						var totalTasasCargadas;
						var totalTasasAlta;
						var totalTasasEliminar;
						resumenAccionData.each(function(record) {
							if(record.data['ID']=='total_tasas_cargadas') { totalTasasCargadas = record.data['TOTAL'];  }
							if(record.data['ID']=='total_tasas_alta') { totalTasasAlta = record.data['TOTAL'];  }
							if(record.data['ID']=='total_tasas_eliminadas') { totalTasasEliminar = record.data['TOTAL'];  }						 
								
						});
						TasasPreferenciales  = [];
						TasasNegociables  = [];
						
						// Prenegociables 
						if(tipoTasaA =='P') {			
							var  gridPreferencial = Ext.getCmp('gridPreferencial');
							var store1 = gridPreferencial.getStore();
							store1.each(function(record) {			
								TasasPreferenciales.push(record.data['EPO_RELACIONADA'] +"|"+record.data['NOM_PROVEEDOR'] +"|"+record.data['ESTATUS'] +"|"+
													record.data['REFERENCIA'] +"|"+record.data['PLAZO'] +"|"+record.data['TIPO_TASA'] +"|"+
													record.data['VALOR'] +"|"+record.data['REL_MAT']+"|"+record.data['PUNTOS']	+"|"+	record.data['VALOR_TASA']);
							});
						}
						// negociables
						if(tipoTasaA =='N') {
							var  gridNegociable = Ext.getCmp('gridNegociable');
							var store2 = gridNegociable.getStore();
							store2.each(function(record) {			
								TasasNegociables.push(record.data['EPO_RELACIONADA'] +"|"+record.data['NOM_PROVEEDOR'] +"|"+record.data['ESTATUS'] +"|"+
													record.data['REFERENCIA'] +"|"+record.data['PLAZO'] +"|"+record.data['TIPO_TASA'] +"|"+
													record.data['VALOR'] +"|"+record.data['VALOR_TASA']);																			
							});
						}
								
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13forma08mext.data.jsp',
							params: {
								informacion:'ArchivoCSV',
								recibo:recibo,
								acuse2:acuse2,
								fechaCarga:fecha,
								horaCarga:hora,							
								tipoTasa:tipoTasaA,
								TasasNegociables:TasasNegociables,
								TasasPreferenciales:TasasPreferenciales,
								totalTasasCargadas:totalTasasCargadas,
								totalTasasAlta:totalTasasAlta,
								totalTasasEliminar:totalTasasEliminar
							}					
							,callback: procesarGenerarCSV
						});					
					}
				},
				{
					xtype: 		'button',
					text: 		'Bajar Archivo',
					tooltip:		'Bajar Archivo',
					id: 			'btnBajarCSV',
					iconCls:		'icoBotonXLS',
					hidden: 		true
					//minWidth:	80
				},	
				{
					xtype: 		'button',
					text: 		'Salir',
					tooltip:		'Salir',
					iconCls: 	'icoLimpiar',
					id: 			'btnSalir',
					handler: function(boton, evento) {
						window.location = '13forma08mext.jsp';		
					}
					//minWidth:	80
				},		
			{ xtype:   'displayfield', id:	'recibo',	 hidden: true,  value:'' },
			{ xtype:   'displayfield', id:	'acuse2',	 hidden: true,  value:'' },
			{ xtype:   'displayfield', id:	'fecha',		 hidden: true,  value:'' },
			{ xtype:   'displayfield', id:	'hora',		 hidden: true,  value:'' },
			{ xtype:   'displayfield', id:	'tipoTasaA', hidden: true,  value:'' }
		]
	});
 
	//---------------------------- PANEL CONFIRMAR PREACUSE ----------------------------
	
	// Manejar respuesta a la confirmacion de la carga de Tasas Preferenciales/Negociadas 
	function procesarSuccessFailureConfirma(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			jsonData = Ext.util.JSON.decode(response.responseText);
			var fp 	= Ext.getCmp('forma');
			fp.el.unmask();
 
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var gridNegociable 			= Ext.getCmp('gridNegociable');
			var gridPreferencial 		= Ext.getCmp('gridPreferencial');
			var botConfirmar 				= Ext.getCmp('botConfirmar');	
			var gridResumenAccion 		= Ext.getCmp('gridResumenAccion');
			var mensajeAutentificacion = Ext.getCmp('mensajeAutentificacion');
			Ext.getCmp("autentificac").setValue(jsonData.mensajeAutentificacion);	
			var botImprimir 				= Ext.getCmp('botImprimir');	
			var btnGenerarPDF 			= Ext.getCmp('btnGenerarPDF');		
			var btnGenerarCSV 			= Ext.getCmp('btnGenerarCSV');
 
			if(jsonData.recibo !='') {
			
				Ext.getCmp("recibo").setValue(jsonData.recibo); // Numero de Recibo de la carga
				Ext.getCmp("acuse2").setValue(jsonData.acuse2);	// Acuse sin formato
				Ext.getCmp("fecha").setValue(jsonData.fecha); 	// Fecha de Carga
				Ext.getCmp("hora").setValue(jsonData.hora);		// Hora de Carga		
				Ext.getCmp("tipoTasaA").setValue(jsonData.tipoTasa);							
				
				var registroResumen = Ext.data.Record.create(['ID', 'DESCRIPCION', 'TOTAL']);
				resumenAccionData.add( 	
					new registroResumen({ 
						ID: 'fecha_carga',	DESCRIPCION: 'Fecha de Carga', 	TOTAL: jsonData.fecha 		
					})
				);
				resumenAccionData.add( 	
					new registroResumen({ 
						ID: 'hora_carga', 	DESCRIPCION: 'Hora de Carga', 	TOTAL: jsonData.hora
					})
				);
				resumenAccionData.add(
					new registroResumen({ 
						ID: 'usuario_carga', DESCRIPCION: 'Usuario', 			TOTAL: jsonData.usuario
					})
				);
				resumenAccionData.add(
					new registroResumen({
						ID: 'recibo_carga',  DESCRIPCION: 'Recibo',  			TOTAL: jsonData.recibo
					})
				);
				
				contenedorPrincipalCmp.doLayout();
				
				mensajeAutentificacion.show();	
				
				botImprimir.show();
				btnGenerarPDF.show();	
				btnGenerarCSV.show();
				botConfirmar.hide();
				
			} else {
			
				gridNegociable.hide();
				gridPreferencial.hide();	
				
				mensajeAutentificacion.show();
				
				botImprimir.show();
				btnGenerarPDF.hide();	
				btnGenerarCSV.hide();
				botConfirmar.hide();
				gridResumenAccion.hide();
				
			}			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var confirmarCert = function(pkcs7, textoFirmar, claveIF,claveMoneda , claveEPO, tipoTasa, clavePlazo, tasasInsertar, tasasEliminar  ){
		
		// Solicitar la firma
			
		if ( Ext.isEmpty(pkcs7) ) {
			Ext.getCmp("btnConfirmarTasas").enable();
			return;	//Error en la firma. Termina...
			
		}else  {	
			Ext.getCmp("btnConfirmarTasas").disable();
			
			var parametros = {
			informacion: 			'ConfirmacionTasas',
				ic_if:					claveIF,
				ic_moneda:				claveMoneda,
				ic_epo:					claveEPO,
				tipoTasa:				tipoTasa,
				lstPlazo:				clavePlazo,				
				pkcs7:					pkcs7,
				textoFirmar:			textoFirmar		
			};
			if( tasasInsertar.length > 0 ){
				parametros.listaTasasInsertar = tasasInsertar;
			}
			if( tasasEliminar.length > 0){
				parametros.listaTasasEliminar = tasasEliminar;
			}
		
			Ext.Ajax.request({
				url: 			'13forma08mext.data.jsp',
				params:  	parametros,				
				callback: 	procesarSuccessFailureConfirma
			});
		}
	}
	
	var procesarConfirmarTasas = function(){
		
		var tipoTasa 			= Ext.getCmp("comboTipoDeTasa").getValue();
		var claveIF				= Ext.getCmp("comboNombreDelIF").getValue();
		var claveMoneda		= Ext.getCmp("ic_moneda1").getValue();
		var claveEPO			= Ext.getCmp("ic_epo1").getValue();
		var clavePlazo			= Ext.getCmp("lstPlazo1").getValue();
		var claveProceso		= Ext.getCmp("proceso").getValue();
		//var estatus 			= Ext.getCmp("estatus"); 
		var tasasInsertar		= Ext.getCmp("tasas_insertar" + tipoTasa ).getValue();
		var tasasEliminar		= Ext.getCmp("tasas_eliminar" + tipoTasa ).getValue();
 
		tasasInsertar			= Ext.decode( tasasInsertar ); 
		tasasEliminar			= Ext.decode( tasasEliminar );
 
		var totales				= '';
		var negociables 		= '';
		var preferenciales 	= '';
			 
		// Preparar cadena de texto con el resumen de los totales
		resumenAccionData.each(function(record) {
			totales += record.data['DESCRIPCION'] +" (Carga Masiva): "+record.data['TOTAL']+'\n';	
		});
 
		// Preparar cadena de texto con el detalle de las tasas preferenciales
		if( tipoTasa === 'P' ) {		
			
			var gridPreferencial = Ext.getCmp('gridPreferencial');
			var store1 				= gridPreferencial.getStore();
			
			preferenciales			='EPO Relacionada|Nombre Proveedor|Estatus|Referencia|Plazo|Tipo Tasa|Valor|Rel. Mat.|Puntos|Valor Tasa \n';		
			store1.each(function(record) {
					
				preferenciales += 
					record.data['EPO_RELACIONADA']+"|"+
					record.data['NOM_PROVEEDOR'] 	+"|"+
					record.data['ESTATUS'] 			+"|"+
					record.data['REFERENCIA'] 		+"|"+
					record.data['PLAZO'] 			+"|"+
					record.data['TIPO_TASA'] 		+"|"+
					record.data['VALOR'] 			+"|"+
					record.data['REL_MAT']			+"|"+
					record.data['PUNTOS']			+"|"+
					record.data['VALOR_TASA']		+'\n';
					
					//listaTasasInsertar.push(record.data['TASAS_INSERTAR']); 
					//listaTasasEliminar.push(record.data['TASAS_ELIMINAR']); 
					
			});
			
		// Preparar cadena de texto con el detalle de las tasas negociadas
		} else if(tipoTasa ==='N') {
			
			var gridNegociable	= Ext.getCmp('gridNegociable');
			var store2 				= gridNegociable.getStore();
			
			negociables 			= 'EPO Relacionada|Nombre Proveedor|Estatus|Referencia|Plazo|Tipo Tasa|Valor|Valor Tasa \n';
			
			store2.each(function(record) {
					
				negociables += 
					record.data['EPO_RELACIONADA'] 	+"|"+
					record.data['NOM_PROVEEDOR'] 		+"|"+
					record.data['ESTATUS'] 				+"|"+
					record.data['REFERENCIA'] 			+"|"+
					record.data['PLAZO'] 				+"|"+
					record.data['TIPO_TASA'] 			+"|"+
					record.data['VALOR'] 				+"|"+
					record.data['VALOR_TASA']			+'\n';				
			});
		}
		
		// Preparar texto a firmar
		var textoFirmar 	= totales+preferenciales+negociables;		
		
		NE.util.obtenerPKCS7(confirmarCert, textoFirmar, claveIF,claveMoneda , claveEPO, tipoTasa, clavePlazo, tasasInsertar, tasasEliminar  );
			 
	}
	
	var botConfirmar = new Ext.Container({
		layout: 			'table',		
		id: 				'botConfirmar',							
		width:			'300',
		heigth:			'auto',
		style: 			'margin: 0 auto;',
		hidden: 			true,
		columns: 		1,	
		layout: 			'hbox',
		layoutConfig: {
			pack: 		'center',
			align: 		'middle'
		},
		items: [				
			{
				xtype: 		'button',
				text: 		'Confirmar Tasas ',					
				tooltip:		'Confirmar Tasas ',
				iconCls: 	'icoAceptar',
				id: 			'btnConfirmarTasas',
				handler: 	procesarConfirmarTasas,
				minWidth: 	100
			},
			{
				xtype: 		'box',
				width:		2,
				autoWidth: 	false
			},
			{
				xtype: 		'button',
				text: 		'Cancelar',
				tooltip:		'Cancelar',
				iconCls: 	'icoLimpiar',
				id: 			'btnCancelar',
				handler: 	function(boton, evento) {
					window.location = '13forma08mext.jsp';		
				},
				minWidth: 	100
			},
			{ 
				xtype:   	'hidden', 
				id:			'tasas_insertarP'
			},
			{ 
				xtype:   	'hidden',
				id:			'tasas_eliminarP'
			},
			{ 
				xtype:   	'hidden', 
				id:			'tasas_insertarN'
			},
			{ 
				xtype:   	'hidden',
				id:			'tasas_eliminarN'
			}
		]
	});
 
	//--------------------------- GRID PREACUSE TASAS NEGOCIADAS --------------------------------------
	
	var epoRelacionadaRenderer02 	= function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var nomProveedorRenderer02 	= function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var tipoTasaRenderer02 			= function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var procesarNegocibalesData = function(store, arrRegistros, opts) {
		
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var el 			= gridNegociable.getGridEl();						
		var jsonData 	= store.reader.jsonData;
			
		if ( arrRegistros != null ) {
			
			if (!gridNegociable.isVisible()) {
				gridNegociable.show();
			}					
				
			// Ext.getCmp("estatus").setValue(jsonData.estatus); 
			Ext.getCmp("tasas_insertarN").setValue(Ext.encode(jsonData.tasas_insertar));
			Ext.getCmp("tasas_eliminarN").setValue(Ext.encode(jsonData.tasas_eliminar));
			
			var totalTasas 			= Ext.getCmp('totalTasas');
			/* var fp						= Ext.getCmp('forma'); */
			/* var contenedorGrids 		= Ext.getCmp('contenedorGrids'); */
			var gridTotalesTasas 	= Ext.getCmp('gridTotalesTasas');
			var botConfirmar 			= Ext.getCmp('botConfirmar');
			var gridResumenAccion 	= Ext.getCmp('gridResumenAccion');
 
			/* fp.hide(); */
			/* contenedorGrids.hide(); */
			gridResumenAccion.show();	
			botConfirmar.show();		
					
			if(store.getTotalCount() > 0) {	
				el.unmask();						
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');
				//el.mask('No se encontraron registros (Revisar que el plazo indicado sea valido', 'x-mask');				
			}
			
		}
	}
	
	var ConsNegociableData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma08mext.data.jsp',
		baseParams: {
			informacion: 'ConsultaNegociable'
		},
		hidden: true,
		fields: [	
			{name: 'EPO_RELACIONADA'},	
			{name: 'NOM_PROVEEDOR'},	
			{name: 'ESTATUS'},
			{name: 'REFERENCIA'},
			{name: 'PLAZO'},	
			{name: 'TIPO_TASA'},
			{name: 'VALOR'},			
			{name: 'VALOR_TASA'}	,
			{name: 'TASAS_INSERTAR'},	
			{name: 'TASAS_ELIMINAR'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarNegocibalesData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarNegocibalesData(null, null, null);						
				}
			}
		}		
	});

		
	var gridNegociable= new Ext.grid.EditorGridPanel({
		id: 'gridNegociable',				
		store: ConsNegociableData,	
		style: 'margin:0 auto;',
		title:'Negociada  ',
		hidden: true,
		clicksToEdit: 1,		
		columns: [
			{							
				header :'EPO relacionada',
				tooltip: 'EPO relacionada',
				dataIndex : 'EPO_RELACIONADA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',
				renderer: epoRelacionadaRenderer02
			},
			{							
				header :'Nombre Proveedor ',
				tooltip: 'Nombre Proveedor ',
				dataIndex : 'NOM_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',
				renderer: nomProveedorRenderer02
			},
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header :'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo Tasa',
				tooltip: 'Tipo Tasa',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: tipoTasaRenderer02
			},
			{							
				header : 'Valor',
				tooltip: 'Valor',
				dataIndex : 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},			
			{							
				header : 'Valor Tasa',
				tooltip: 'Valor Tasa',
				dataIndex : 'VALOR_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false
	});
	
	//--------------------------- GRID PREACUSE TASAS PREFERENCIALES --------------------------------------
		
	var epoRelacionadaRenderer01 	= function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var nomProveedorRenderer01 	= function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var tipoTasaRenderer01 			= function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var procesarPreferencialesData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
					
		if (arrRegistros != null) {
			
			if (!gridPreferencial.isVisible()) {
				gridPreferencial.show();
			}	
		
			var el = gridPreferencial.getGridEl();						
			var jsonData = store.reader.jsonData;
			
			// Ext.getCmp("estatus").setValue(jsonData.estatus); 
			Ext.getCmp("tasas_insertarP").setValue(Ext.encode(jsonData.tasas_insertar));
			Ext.getCmp("tasas_eliminarP").setValue(Ext.encode(jsonData.tasas_eliminar));
			
			var totalTasas = Ext.getCmp('totalTasas');
			var fp = Ext.getCmp('forma');
			var gridTotalesTasas = Ext.getCmp('gridTotalesTasas');
			var botConfirmar = Ext.getCmp('botConfirmar');
			var gridResumenAccion = Ext.getCmp('gridResumenAccion');
			var contenedorGrids = Ext.getCmp('contenedorGrids');
			gridResumenAccion.show();					
			contenedorGrids.hide();
			
			Ext.getCmp('panelModoPantalla').hide();
			fp.hide();
			botConfirmar.show();
					
			if(store.getTotalCount() > 0) {	
				el.unmask();						
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var ConsPreferencialesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma08mext.data.jsp',
		baseParams: {
			informacion: 'ConsultaPreferencial'
		},
		hidden: true,
		fields: [				
			{name: 'EPO_RELACIONADA'},
			{name: 'NOM_PROVEEDOR'},	
			{name: 'ESTATUS'},
			{name: 'REFERENCIA'},
			{name: 'PLAZO'},	
			{name: 'TIPO_TASA'},
			{name: 'VALOR'},
			{name: 'REL_MAT'},
			{name: 'PUNTOS'},
			{name: 'VALOR_TASA'},
			{name: 'TASAS_INSERTAR'},	
			{name: 'TASAS_ELIMINAR'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarPreferencialesData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.					
					procesarPreferencialesData(null, null, null);						
				}
			}
		}		
	});
	
	var gridPreferencial= new Ext.grid.EditorGridPanel({
		id: 'gridPreferencial',				
		store: ConsPreferencialesData,	
		style: 'margin:0 auto;',
		title:'Preferencial ',
		hidden: true,
		clicksToEdit: 1,
		columns: [
			{							
				header :'EPO relacionada',
				tooltip: 'EPO relacionada',
				dataIndex : 'EPO_RELACIONADA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',
				renderer: epoRelacionadaRenderer01
			},
			{							
				header :'Nombre Proveedor ',
				tooltip: 'Nombre Proveedor ',
				dataIndex : 'NOM_PROVEEDOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',
				renderer: nomProveedorRenderer01
			},
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header :'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Tipo Tasa',
				tooltip: 'Tipo Tasa',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: tipoTasaRenderer01
			},
			{							
				header : 'Valor',
				tooltip: 'Valor',
				dataIndex : 'VALOR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Rel. Mat',
				tooltip: 'Rel. Mat',
				dataIndex : 'REL_MAT',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Puntos',
				tooltip: 'Puntos',
				dataIndex : 'PUNTOS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Valor Tasa',
				tooltip: 'Valor Tasa',
				dataIndex : 'VALOR_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
      loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false
	});

	//---------------------------- GRID CON EL TOTAL DE LOS REGISTROS CARGADOS ----------------------------
	
	var resumenAccionData = new Ext.data.JsonStore({
		root: 'resumenAccion',
		fields: ['ID', 'DESCRIPCION', 'TOTAL'],
		autoDestroy: false,
		autoLoad: false
	});
	
	var gridResumenAccion = {
		xtype: 'grid',
		hidden: true,
		id: 'gridResumenAccion',
		store: resumenAccionData,
		margins: '10 0 0 0',
		style: 'margin:0 auto;',
		columns: [
			{
				header: ' ',
				dataIndex: 'DESCRIPCION',
				sortable: false,
				resizable: true,
				hidden: false,
				width: 250,
				align: 'left'
			},
			{
				header: 'Total',
				dataIndex: 'TOTAL',
				sortable: false,
				hideable: false,
				resizable: true,
				align: 'center',
				width: 250
			}
		],	
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		autoHeight: true,		
		width: 520,
		title: '',
		frame: true,
		bbar: ''
	};
	
	//---------------------------- GRID REGISTROS VALIDOS ----------------------------
	
	var registrosValidosData = new Ext.data.JsonStore({
		root : 'registrosValidos',
		fields : ['LINEA', 'CAMPO', 'OBSERVACION'],
		autoDestroy : true,
		autoLoad: false
	});
	
	
	var gridValidos = {
		xtype: 'grid',
		id: 'gridValidos',	
		store: registrosValidosData,
		margins: '0 5 0 0',
		columns: [
			{
				header: 'L�nea',
				tooltip: 'Numero de L�nea del archivo',
				dataIndex: 'LINEA',
				sortable: false,
				width: 50,
				resizable: true,
				hidden: false
			},
			{
				header: 'Campo',
				tooltip: 'Nombre del Campo',
				dataIndex: 'CAMPO',
				sortable: false,
				hideable: false,
				width: 100,
				align: 'left'
			},
			{
				id: 'observaciones',
				header : 'Observaci�n',
				tooltip: 'Observaciones',
				dataIndex : 'OBSERVACION',
				width: 300,
				align: 'left',
				sortable : false,
				hideable : false
			}
		],
		autoExpandColumn: 'observaciones',
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 416,
		title: 'Detalle de Documentos sin Error',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [
				'Total Registros V�lidos: ',
				{
					xtype: 'tbtext',
					text: '',
					id: 'totalRegistrosValidos'
				}
			]
		}
	};
	
	//---------------------------- GRID REGISTROS INVALIDOS ----------------------------
	
	var registrosInvalidosData = new Ext.data.JsonStore({
		root : 'registrosInvalidos',
		fields : ['LINEA', 'CAMPO', 'OBSERVACION'],
		autoDestroy : true,
		autoLoad: false
	});
 
	var gridInvalidos = {
		xtype: 'grid',
		id: 'gridInvalidos',
		store: registrosInvalidosData,
		margins: '0 0 0 0',
		columns: [
			{
				header: 'L�nea',
				tooltip: 'Numero de L�nea del archivo',
				dataIndex: 'LINEA',
				sortable: false,
				width: 50,
				resizable: true,
				hidden: false
			},
			{
				header: 'Campo',
				tooltip: 'Nombre del Campo',
				dataIndex: 'CAMPO',
				sortable: false,
				hideable: false,
				width: 100,
				align: 'left'
			},
			{
				id: 'observacionesErr',
				header : 'Observaci�n',
				tooltip: 'Observaciones',
				dataIndex : 'OBSERVACION',
				width: 300,
				align: 'left',
				sortable : false,
				hideable : false,
				renderer:  function (observacion, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(observacion) + '"';
						return observacion;
				}
			}
		],
		autoExpandColumn: 'observacionesErr',
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 416,
		title: 'Detalle de Documentos con Errores',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			items: [
				'Total Registros Inv�lidos: ',
				{
					xtype: 'tbtext',
					text: '',
					id: 'totalRegistrosInvalidos'
				}
			]
		}
	};
	
	
	//------------- PANEL CONTENEDOR DEL DETALLE DE LA VALIDACION DEL ARCHIVO CARGADO -------------	
	
	var procesoRegresar = function() 	{
		
		var tipoTasa = Ext.getCmp('comboTipoDeTasa').getValue();
		
		// Mostrar todos los campos de captura de la solicitud
		Ext.getCmp('comboTipoDeTasa').show();	
		Ext.getCmp('ic_epo1').show();	
		Ext.getCmp('comboNombreDelIF').show();	
		Ext.getCmp('ic_moneda1').show();	
		if(tipoTasa === 'N' ) {
			Ext.getCmp('lstPlazo1').show();	
		}
 
		// Indiciar que se regres� al panel principal
		Ext.getCmp("regresa").setValue('S');
		
		// Ocultar contenedor de grids
		var contenedorGrids 				= Ext.getCmp('contenedorGrids');
		contenedorGrids.hide();
		
	}

	var procesarContinuar = function() 	{
	
		var lstNumerosLineaOk 	= Ext.getCmp('lstNumerosLineaOk');
		var proceso 				= Ext.getCmp('proceso');
		
		// Obtener valores de los campos de la carga
		var tipoTasa				= Ext.getCmp("comboTipoDeTasa").getValue();
		var claveIF					= Ext.getCmp("comboNombreDelIF").getValue();
		var claveMoneda			= Ext.getCmp("ic_moneda1").getValue();
		var claveEPO				= Ext.getCmp("ic_epo1").getValue();
		var clavePlazo				= Ext.getCmp("lstPlazo1").getValue();
		var claveProceso			= Ext.getCmp("proceso").getValue();
		var numerosLineaOk  		= Ext.getCmp("lstNumerosLineaOk").getValue();
				
		// Deshabilitar boton Confirmar Tasas hasta que hayan cargado los Grids Preferencial y Negociada
      var btnConfirmarTasas 	= Ext.getCmp('btnConfirmarTasas'); 
      btnConfirmarTasas.setDisabled(true); 
      
      var gridPreferencial  			= Ext.getCmp('gridPreferencial');
      var gridNegociable      		= Ext.getCmp('gridNegociable');
      var contenedorResultadosCmp 	= Ext.getCmp('contenedorGrids');
      var elPreferencial    			= gridPreferencial.getGridEl();	
      var elNegociable       			= gridNegociable.getGridEl();	
 
      gridPreferencial.show();
      gridNegociable.show();
      
      Ext.getCmp('panelModoPantalla').hide();
      fp.hide();
      contenedorResultadosCmp.hide();
      Ext.getCmp('gridLayout').hide();
      
      /*fp.el.mask('Enviando...', 'x-mask-loading');*/	
      elPreferencial.mask('Cargando...');  
      elNegociable.mask('Cargando...');  
 				
		ConsPreferencialesData.load({
			params: {
				informacion: 		 'ConsultaPreferencial',
				pantalla: 			 'PreAcuse',
				tipoTasa:			 tipoTasa,	
				ic_if:				 claveIF,
				ic_moneda:			 claveMoneda,
				ic_epo:				 claveEPO,
				lstPlazo:			 clavePlazo,
				proceso:				 claveProceso,
				lstNumerosLineaOk: numerosLineaOk
			},
			callback: function(){      
					ConsNegociableData.load({
							params: {
								informacion: 		 'ConsultaNegociable',
								pantalla: 			 'PreAcuse',
								tipoTasa:			 tipoTasa,	
								ic_if:				 claveIF,
								ic_moneda:			 claveMoneda,
								ic_epo:				 claveEPO,
								lstPlazo:			 clavePlazo,
								proceso:				 claveProceso,
								lstNumerosLineaOk: numerosLineaOk
							},
							callback: function(){ 
								btnConfirmarTasas.setDisabled(false); 
							}
					});
            }
		});
	}
	
	var contenedorGrids = {
		xtype: 	'panel',
		id: 		'contenedorGrids',
		hidden: 	true,
		title: 	' ',
		hidden: 	false,
		width: 	850,
		frame: 	true,
		margins: '20 0 0 0',
		layout: 	'hbox',
		layoutConfig: {
			pack: 	'center',
			align: 	'middle'
		},
		style: 'margin:0 auto;',
		items: [
			gridValidos,
			gridInvalidos
		],
		buttons: [	
			{
				xtype: 	'button',
				text: 	'Regresar',
				tooltip:	'Regresar',
				iconCls: 'icoLimpiar',
				id: 		'btnRegresar',
				handler: procesoRegresar
			},			
			{
				xtype: 	'button',
				text: 	'Continuar',					
				tooltip:	'Continuar',
				iconCls: 'icoAceptar',
				id: 		'btnContinuarC',
				handler: procesarContinuar
			}					
		]
	};
	
	//-------------------------- GRID CON LA AYUDA DE LA CARGA --------------------------	
	
	showGridLayout = function(){						
		if (!gridLayout.isVisible()) {
			gridLayout.show();
		}else{
			gridLayout.hide();
		}			
	}
 
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
		  {name: 'NUMCAMPO'		},
		  {name: 'CAMPO'			},
		  {name: 'TIPODATO'		},
		  {name: 'LONGITUD'		},
		  {name: 'OBSERVACIONES'}
	  ]
	});
	
	var infoLayout = [
		['1',	'R.F.C. Proveedor',	'Alfanum�rico',	'14',	'R.F.C. del Proveedor'						 ],
		['2',	'Puntos/Valor Tasa',	'Num�rico',			'7', 	' 2 Enteros ,  5 Decimales'				 ],
		['3 ','Estatus',				'Alfanum�rico',	'1',	'A = Alta de la Tasa, E = Eliminar Tasa']
	];

	storeLayoutData.loadData(infoLayout);
 
	var gridLayout = new Ext.grid.GridPanel({
		id: 		'gridLayout',
		store: 	storeLayoutData,
		hidden: 	true,
		margins: '20 0 0 0',
		style: 	'margin:0 auto;',
		columns: [
			{
				header: 		'No.',
				dataIndex:	'NUMCAMPO',
				width: 		100,
				sortable: 	true,
				align: 		'center'
			},
			{
				header: 		'CAMPO',
				tooltip: 	'CAMPO',
				dataIndex:	'CAMPO',
				width: 		150,
				align: 		'left',
				sortable: 	true
			},
			{
				header: 		'TIPO DATO',
				dataIndex: 	'TIPODATO',
				width: 		150,
				sortable: 	true,
				align: 		'center'
			},
			{
				header: 		'LONG. MAX',
				dataIndex:	'LONGITUD',
				width: 		120,
				sortable: 	true,
				align: 		'center'
			},
			{
				header: 		'OBSERVACIONES',
				dataIndex: 	'OBSERVACIONES',
				width: 		312,
				sortable: 	true,
				align: 		'left'
			}
		],
		stripeRows: 	true,
		columnLines:	true,
		loadMask:		true,
		width:			850,
		height:			110,
		frame:			true
	});

	//-------------------------- PANEL DE CARGA MASIVA DE TASAS --------------------------	
 
	var catalogoPlazo = new Ext.data.JsonStore({
		id: 'catalogoPlazo',
		root: 'registros',
		fields: ['clave', 'descripcion','loadMsg'],
		url: '13forma08mext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPlazo'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		},
		autoDestroy: true	
	});
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma08mext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: function(store,records,options){
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue = String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index = store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						Ext.getCmp("ic_moneda1").setValue(defaultValue);
					}
									
				} 
								
			},
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		},
		autoDestroy: true	
	});
 
	var catalogoIF = new Ext.data.JsonStore({
		id: 					'catalogoIF',
		root: 				'registros',
		fields: 				['clave', 'descripcion','loadMsg'],
		url: 					'13NBiTasaPredeNego01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIF'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			load: function(store,records,options){
				
				var comboNombreDelIF = Ext.getCmp('comboNombreDelIF');
				
				// Avisar en caso de que la consulta no haya traido ningun intermediario financiero.
				if( records.length === 0 ){
					comboNombreDelIF.emptyText = "No se encontr� ning�n Intemediario Financiero";
					comboNombreDelIF.setValue('');
					comboNombreDelIF.applyEmptyText();
					comboNombreDelIF.clearInvalid();
					return;
				}
				
				// Leer valor default
				var defaultValue 		= null;
				var existeParametro	= true;
				try {
					defaultValue 		= String(options.params.defaultValue);
				}catch(err){
					existeParametro	= false;
					defaultValue 		= null;
				}
								
				// Si se especific� un valor por default
				if( existeParametro ){ 
									
					var index 				= store.findExact( 'clave', defaultValue ); 
					// El registro fue encontrado por lo que hay que seleccionarlo
					if (index >= 0){
						comboNombreDelIF.setValue(defaultValue);
					} 
									
				} 
								
			},
			exception: 		NE.util.mostrarDataProxyError,
			beforeload: 	NE.util.initMensajeCargaCombo
		},
		autoDestroy: true
	});
	 
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13forma08mext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		},
		autoDestroy: true
	});
		
	var catalogoTipoDeTasa = new Ext.data.ArrayStore({
		id: 		'catalogoTipoDeTasa',
		fields:	[ 'clave', 'descripcion', 'loadMsg' ],
		data:  	[
			[ 'P', 'PREFERENCIAL' ],
			[ 'N', 'NEGOCIADA'    ]
		],
		autoDestroy: 	true
	});
	
	var procesaCambioSeleccion = function(componente){
		
		var actualizarComboIF = false;
		
		// 1. Si el componente no es v�lido, abortar la operaci�n
		if( componente && !componente.isValid() ){
			return;
		}
		
		// 2. Resetear combo tipo de tasa
		if( 		componente 	&& componente.id === 'comboTipoDeTasa'   	){
			
			var comboTipoDeTasa 	= Ext.getCmp("comboTipoDeTasa");
			var lstPlazo1 			= Ext.getCmp("lstPlazo1");
			if( 			comboTipoDeTasa.getValue() === 'P' ){
				lstPlazo1.clearValue();
				lstPlazo1.hide();
			} else if( 	comboTipoDeTasa.getValue() === 'N' ){
				lstPlazo1.clearValue();
				lstPlazo1.show();
			}
		
		} else if( componente && componente.id === 'ic_epo1'    			){

			// Actualizar combo de IF
			actualizarComboIF = true;
			
		} else if( componente && componente.id === 'ic_moneda1' 			){
		
			// Actualizar combo de IF
			actualizarComboIF = true;
			
		}
		
		// 3. En caso de que se requiera, actualizar ComboIF
		if( actualizarComboIF ){
			
			var comboEpoRelacionada	= Ext.getCmp("ic_epo1");
			var comboMoneda			= Ext.getCmp("ic_moneda1");
			
			if( 			!comboEpoRelacionada.isValid() 	){
				return;
			} else if(	!comboMoneda.isValid()				){
				return;
			}
			
			var comboNombreDelIF 		= Ext.getCmp('comboNombreDelIF');
			var claveIFSeleccionado		= comboNombreDelIF.getValue();
			
			comboNombreDelIF.emptyText = "Seleccionar...";
			comboNombreDelIF.setValue('');
			comboNombreDelIF.applyEmptyText();
			comboNombreDelIF.clearInvalid();
		
			comboNombreDelIF.getStore().load({
				params: { 
					ic_epo:			comboEpoRelacionada.getValue(),
					ic_moneda:		comboMoneda.getValue(),
					defaultValue:	claveIFSeleccionado
				}
			});
			
		}
 		
	}
			
	var  elementosForma = [			
		{ xtype:   'displayfield', hidden: true, id: 'lstNumerosLineaOk',	value:''  }, 
		{ xtype:   'displayfield', hidden: true, id: 'proceso',	 			value:''  }, 
		{ xtype:   'displayfield', hidden: true, id: 'regresa',	 			value:''	 }, 
		// COMBO TIPO DE TASA
		{
			xtype: 				'combo',
			name: 				'comboTipoDeTasa',
			id: 					'comboTipoDeTasa',
			fieldLabel: 		'Tipo de Tasa',
			width:				125,
			allowBlank: 		false,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'tipoTasa',
			emptyText: 			'Seleccionar...', 
			blankText:			'Debe seleccionar una Tasa',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoTipoDeTasa,
			tpl: 					NE.util.templateMensajeCargaCombo,
			autoLoad: 			false,
			//anchor:				'-20'
			listeners:			{
				collapse: procesaCambioSeleccion
			},
			value: "P" // SELECCIONAR LA TASA PREFERENCIAL POR DEFAULT
		},
		// COMBO EPO RELACIONADA
		{
			xtype: 			'multiselect',
			fieldLabel: 	'EPO Relacionada',
			name: 			'ic_epo',
			id: 				'ic_epo1',
			width: 			583,
			height: 			200, 
			autoLoad: 		false,
			allowBlank:		false,
			store: 			catalogoEPO,
			blankText:		'Debe seleccionar una EPO',
			displayField: 	'descripcion',
			valueField:		'clave',
			/*tbar:[{
				text: 		'Limpiar',
				handler: function(){
					Ext.StoreMgr.key('catalogoIF').removeAll();
					fp.getForm().reset();
				}
			}],*/
			ddReorder: false,
			listeners: {
				change: procesaCambioSeleccion
			}		
		},
		// COMBO NOMBRE DEL IF
		{
			xtype: 				'combo',
			name: 				'ic_if',
			id: 					'comboNombreDelIF',
			fieldLabel: 		'Nombre del IF',
			allowBlank: 		false,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'ic_if',
			emptyText: 			'Seleccionar...', 
			blankText:			'Debe seleccionar un IF',
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				catalogoIF, 
			tpl: 					NE.util.templateMensajeCargaComboConDescripcionCompleta,
			anchor:				'75%'
		},
		// COMBO MONEDA
		{
			xtype: 			'combo',
			name: 			'ic_moneda',
			id: 				'ic_moneda1',
			mode: 			'local',
			allowBlank: 	false,
			autoLoad: 		false,
			displayField: 	'descripcion',
			emptyText: 		'Seleccionar...',
			blankText:		'Debe seleccionar una Moneda',
			valueField: 	'clave',
			hiddenName: 	'ic_moneda',
			fieldLabel: 	'Moneda',			
			forceSelection: true,
			triggerAction: 'all',
			typeAhead: 		true,
			minChars: 		1,
			store: 			catalogoMoneda,
			listeners: {
				collapse: procesaCambioSeleccion
			}
		},
		// COMBO PLAZO
		{
			xtype: 				'combo',
			name: 				'lstPlazo',
			id: 					'lstPlazo1',
			mode: 				'local',				
			displayField:		'descripcion',
			emptyText: 			'Seleccionar...',			
			valueField: 		'clave',
			hiddenName: 		'lstPlazo',
			fieldLabel: 		'Plazo',
			hidden: 				true,
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			allowBlank: 		true,
			msgTarget: 			'side',	
			store: 				catalogoPlazo, 
			tpl: 					NE.util.templateMensajeCargaCombo					
		},		
		// FILE INPUT: NOMBRE DEL ARCHIVO
		{
		  xtype: 		'fileuploadfield',
		  id: 			'archivo',
		  name: 			'archivo',
		  emptyText: 	'Seleccionar...',
		  fieldLabel:  'Nombre del Archivo',
		  name: 			'archivoFormalizacion', 
		  buttonText: 	null,
		  buttonCfg: {
			  iconCls: 	'upload-icon'
		  },
		  anchor: 		'-100'
		  //vtype: 		'archivotxt'
		}
		/*
		{
			xtype:	'panel',
			layout:	'column',
			width: 700,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'button',
					id: 'btnAyuda',
					columnWidth: .05,
					autoWidth: true,
					autoHeight: true,
					iconCls: 'icoAyuda',
					style: 'border: 1px solid blue;',
					handler: function(){						
						if (!gridLayout.isVisible()) {
							gridLayout.show();
						}else{
							gridLayout.hide();
						}									
					}
				},				
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 175,
					style: 'border: 1px solid blue;',
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									emptyText: 'Nombre del Archivo',
									fieldLabel: 'Nombre del Archivo',
									name: 'archivoFormalizacion',   
									buttonText: ' ',
									width: 300,	  
									buttonCfg: {
										iconCls: 'upload-icon'
									},
									anchor: '100%',
									vtype: 'archivotxt'
								}						
							]
						}
					]
				}
			]
		}
		*/
	];

	var fp = new Ext.form.FormPanel({
		id: 				'forma',
		width: 			850,
		title: 			'Carga Masiva Tasas Preferenciales/Negociadas',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin:0 auto;',
		bodyStyle: 		'padding: 6px',
		labelWidth: 	150,
		defaultType: 	'textfield',
		items: 			elementosForma,
		monitorValid: 	false,
		fileUpload: 	true,
		/*defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},	*/	
		layoutConfig: {
			fieldTpl: new Ext.XTemplate(
				'<tpl if="id==\'archivo\'">',
           		 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">',
						  		'<a class="x-tool x-tool-ayudaLayout" onfocus="blur()" style="float:left;" onclick="javascript:showGridLayout();"></a>&nbsp;',
						  		'{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
						  '</div>',
					 '</div>',
			  '</tpl>',
			  '<tpl if="id!=\'archivo\'">',
					 '<div class="x-form-item {itemCls}" tabIndex="-1">',
						  '<label for="{id}" style="{labelStyle}" class="x-form-item-label">{label}{labelSeparator}</label>',
						  '<div class="x-form-element" id="x-form-el-{id}" style="{elementStyle}">',
						  '</div><div class="{clearCls}"></div>',
					 '</div>',
			  '</tpl>'
		   )
		},
		buttons: [
			{
				text: 	'Continuar',
				id: 		'btnContinuar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
					
					var forma 		= Ext.getCmp('forma');
					var validForm 	= true;
					
					// 1. Validar Forma de Manera General
					validForm 		= forma.getForm().isValid();
				
					// 2. Validar campos de la forma
					
					// 2.1 Validacion: "Debe seleccionar una TASA" 		[IMPLEMENTADA EN EL COMPONENTE]
					var comboTipoDeTasa	= Ext.getCmp("comboTipoDeTasa");
					
					// 2.2 Validacion: "Debe seleccionar una EPO"  		[IMPLEMENTADA EN EL COMPONENTE]
					var comboEPO 			= Ext.getCmp("ic_epo1");
					
					// 2.3 Validacion: "Debe seleccionar un IF"    		[IMPLEMENTADA EN EL COMPONENTE]
					var comboIF				= Ext.getCmp("comboNombreDelIF");
					
					// 2.4 Validacion: "Debe seleccionar una Moneda" 	[IMPLEMENTADA EN EL COMPONENTE]
					var comboMoneda		= Ext.getCmp("ic_moneda1");
					
					// 2.5 Validacion: "Debe seleccionar un Plazo", solo cuando
					// se hayan seleccionado tasas negociadas.
					var comboPlazo = Ext.getCmp("lstPlazo1");
					if( comboTipoDeTasa.getValue() === "N" ){ // Tasa negociada
						if( comboPlazo.isValid() && Ext.isEmpty( comboPlazo.getValue() ) ){
							comboPlazo.markInvalid("Debe selecionar un Plazo");
							validForm = false;
						}
					}
 
				   // 2.6 Validar archivo		
					var archivo 		= Ext.getCmp("archivo");
					var myRegexTXT 	= /^.+\.([tT][xX][tT])$/;
					var nombreArchivo = archivo.getValue();
					
					// 2.6.1 Validacion: "Favor de capturar una ruta valida" [IMPLEMENTADA EN EL COMPONENTE]
					if( Ext.isEmpty( archivo.getValue() ) ){
						archivo.markInvalid("Favor de capturar una ruta valida");
						validForm = false;
					// 2.6.2 Validacion: "El formato del archivo de origen no es el correcto. Debe tener extensi�n txt"
					} else if( !myRegexTXT.test(nombreArchivo) ) { 
						archivo.markInvalid("El formato del archivo de origen no es el correcto. Debe tener extensi�n txt.");
						validForm = false;
					}
						
					// 3. La forma es invalida, se suspende la operacion
					if( !validForm ) return;
	
					// 4. Realizar carga del archivo
					var parametros = {
						tipoTasa:	comboTipoDeTasa.getValue(),
						ic_epo:		comboEPO.getValue(),
						ic_if:		comboIF.getValue(),
						ic_moneda:	comboMoneda.getValue()
					};
					if( comboTipoDeTasa.getValue() === "N" ) {
						parametros['lstPlazo'] = comboPlazo.getValue();
					}
							
					var forma = fp.getForm();									
					forma.submit({
						url: 			'13forma08mfilext.data.jsp?'+Ext.urlEncode(parametros),
						waitMsg: 	'Enviando datos...',
						waitTitle:	'Por favor, espere',		
						success: 	function(form, action) {
							
							// Cargar detalle de los registros que fueron validados exitosamente
							registrosValidosData.loadData(action.result);
							// Cargar detalle de los registros con error
							registrosInvalidosData.loadData(action.result);
							// Cargar detalle con el total de los registros cargados [ NO SE OCUPA ]
							resumenAccionData.loadData(action.result);
							
							// Leer resultados de la carga
							valoresResultadoProceso 		= action.result.valoresResultadoProceso;
							
							// Leer valor componente hidden: regresar
							var regresa 						= Ext.getCmp("regresa").getValue();
							// Obtener referencia al contenedor con todos los componentes
							var contenedorPrincipalCmp 	= Ext.getCmp('contenedorPrincipal');
							// Obtener referencia al contenedor del grid de resultados
							var contenedorResultadosCmp 	= contenedorPrincipalCmp.get('contenedorGrids');
							
							// Si regresa = 'S', agregar contendor de grid de resultados a la pantalla
							if ( Ext.isEmpty(contenedorResultadosCmp) || regresa == 'S' ) {	//Si no existe el componente lo agrega
								contenedorPrincipalCmp.add(contenedorGrids);
								contenedorPrincipalCmp.doLayout();
								contenedorResultadosCmp = contenedorPrincipalCmp.get('contenedorGrids');
							}		
							
							// Obtener nombre de los tipos de tasa cargados
							if (        comboTipoDeTasa.getValue() === 'P' ) {	
								tasa = 'PREFERENCIALES';	
							} else if ( comboTipoDeTasa.getValue() === 'N' ) { 	
								tasa = 'NEGOCIADAS'; 		
							}
							
							// Asignar titulo al panel con el detalle de las tasas cargadas
							contenedorResultadosCmp.setTitle('Tasas a cargar: ' + tasa);
							// Mostrar n�mero de registros sin error
							Ext.getCmp('totalRegistrosValidos').setText(valoresResultadoProceso.totalRegistrosValidos.toString());
							// Mostrar n�mero de registros con error
							Ext.getCmp('totalRegistrosInvalidos').setText(valoresResultadoProceso.totalRegistrosInvalidos.toString());
							
							// Guardar numero de registros sin error
							Ext.getCmp("lstNumerosLineaOk").setValue(valoresResultadoProceso.lstNumerosLineaOk);	
							// Guardar id del proceso
							Ext.getCmp("proceso").setValue(valoresResultadoProceso.proceso);	
							// regresa = 'N' => Solo se muestra el campo de captura de archivo TXT
							Ext.getCmp("regresa").setValue('N');
							
							// S�lo permitir al usuario que vuelva a recargar el archivo
							Ext.getCmp('comboTipoDeTasa').hide();	
							Ext.getCmp('ic_epo1').hide();	
							Ext.getCmp('comboNombreDelIF').hide();	
							Ext.getCmp('ic_moneda1').hide();	
							Ext.getCmp('lstPlazo1').hide();	
 
							// Desplegar boton continuar, el cual mostrar� un preacuse de la carga
							if(valoresResultadoProceso.btnContinuar =='N') {
								Ext.getCmp('btnContinuarC').hide();
							}else {
								Ext.getCmp('btnContinuarC').show();
							}		
							
						},
						failure: NE.util.mostrarSubmitError
					});
				}
			}
		]
	});
 
	//------------------------ PANEL MODO PANTALLA --------------------------
	var panelModoPantalla = {
		id:			'panelModoPantalla',
      xtype:		'panel',
      frame:		false,
      border:		false,
      bodyStyle:	'background:transparent;',
      layout: 		'hbox',
      style: 		'margin: 0 auto',
      width:		500,
      height: 		75,
      layoutConfig: {
        align:	'middle',
        pack:	'center'
      },
      items: [
		  {	
			  xtype: 	'button',
			  text: 	  	'Individual',
			  width:		126,
			  id: 		'botonIndividual',
			  handler: function(){			
				  // Determinar el estado siguiente... ir a la pantalla de inicio
				  var forma 	= Ext.getDom('formAux');
				  forma.action = "13NBiTasaPredeNego01ext.jsp";
				  forma.target = "_self";
				  forma.submit();
			  }
		  },
		  {
			  xtype: 	'box',
			  width:		8,
			  autoWidth:false
		  },
		  {
			  xtype: 	'button',
			  text: 	   'Masiva',
			  width:		126,
			  pressed:  true,
			  id: 		'botonMasiva'
		  }
     ]
   };
   
	// --------------------------- CONTENEDOR PRINCIPAL -------------------------	
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 	'margin:0 auto;',
		width: 	949,
		items: [
			mensajeAutentificacion,
			gridResumenAccion,
			NE.util.getEspaciador(20),	
			panelModoPantalla,
			fp,
			botConfirmar,
			botImprimir,
			NE.util.getEspaciador(20),			
			gridPreferencial,			
			NE.util.getEspaciador(20),
			gridNegociable,
			gridLayout,					
			NE.util.getEspaciador(20)			
		]
	});

	// --------------------------- INICIALIZACI�N DE LOS CAT�LOGOS -------------------------
	// Inicializar Catalogo de Tipo de Tasa
	// catalogoTipoDeTasa.load(); 
	// Inicializar Cat�logo de EPOs
	catalogoEPO.load();
	// Inicializar Cat�logo de Monedas
	catalogoMoneda.load({
		params: {
			defaultValue: 1 // Seleccionar MONEDA NACIONAL por default
		}
	});
	// Inicializar Cat�logo de Plazos
	catalogoPlazo.load();
 
	var comboEpoRelacionadaView 				= Ext.getCmp("ic_epo1").view;
	comboEpoRelacionadaView.multiSelect 	= false;
	comboEpoRelacionadaView.singleSelect 	= true;
	//-------------------------------- ----------------- -----------------------------------
	
});
