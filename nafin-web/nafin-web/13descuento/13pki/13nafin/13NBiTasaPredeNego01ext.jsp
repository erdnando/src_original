<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf"%>
  
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<% if( esEsquemaExtJS ){ %>
	<%@ include file="/01principal/menu.jspf"%>
<% } %>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="13NBiTasaPredeNego01ext.js?<%=session.getId()%>"></script>
<%@ include file="/00utils/componente_firma.jspf" %>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<% } %>
	<div id="_menuApp"></div>
	<div id="Contcentral">
			<% if( esEsquemaExtJS ){ %>
				<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<% } %>
		<div id="areaContenido"><div style="height:230px"></div></div>
	</div>
	</div>
	<% if( esEsquemaExtJS ){ %>
		<%@ include file="/01principal/01nafin/pie.jspf"%>
	<% } %>
	<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>