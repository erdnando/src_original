<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	com.netro.mandatodoc.*,	
	netropology.utilerias.*,
	org.apache.commons.logging.Log" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion 			= (request.getParameter("informacion")	!=null)?request.getParameter("informacion")	:"";
String proceso 				= (request.getParameter("proceso")		!=null)?request.getParameter("proceso")		:"";
String ic_epo 					= (request.getParameter("ic_epo")		!=null)?request.getParameter("ic_epo")			:"";
String tipoTasa 				= (request.getParameter("tipoTasa")		!=null)?request.getParameter("tipoTasa")		:"";
String ic_moneda 				= (request.getParameter("ic_moneda")	!=null)?request.getParameter("ic_moneda")		:"";
String lstPlazo 				= (request.getParameter("lstPlazo")		!=null)?request.getParameter("lstPlazo")		:"";
String pantalla 				= (request.getParameter("pantalla")		!=null)?request.getParameter("pantalla")		:"";
String lstNumerosLineaOk 	= (request.getParameter("lstNumerosLineaOk")!=null)?request.getParameter("lstNumerosLineaOk"):"";
// String estatus 				= (request.getParameter("estatus")		!=null)?request.getParameter("estatus")		:""; 
String acuse2 					= (request.getParameter("acuse2")		!=null)?request.getParameter("acuse2")			:"";
String fechaCarga 			= (request.getParameter("fechaCarga")	!=null)?request.getParameter("fechaCarga")	:"";
String horaCarga 				= (request.getParameter("horaCarga")	!=null)?request.getParameter("horaCarga")		:"";
String recibo 					= (request.getParameter("recibo")		!=null)?request.getParameter("recibo")			:"";
String ic_if 					= (request.getParameter("ic_if")			!= null)?request.getParameter("ic_if")			:"";

String totalTasasCargadas2 = (request.getParameter("totalTasasCargadas")	!=null)?request.getParameter("totalTasasCargadas")	:"";
String totalTasasAlta2 		= (request.getParameter("totalTasasAlta")			!=null)?request.getParameter("totalTasasAlta")		:"";
String totalTasasEliminar2 = (request.getParameter("totalTasasEliminar")	!=null)?request.getParameter("totalTasasEliminar")	:"";

double ldPuntos	= 0,  ldValorTotalTasa = 0,  ldTotal 	= 0, ldTasa =0, ldTasaBase =0;
Vector lovDatos 	= null;
Vector lovRegistro = null;
int numRegistros =0,  totalTasasCargadas 	= 0,  totalTasasEliminar 	= 0,  totalTasasAlta		= 0;
String  infoRegresar ="", consulta ="", nombreTasa= "",totalSinError ="", totalError ="", btnContinuar ="N",
		  lsCveEpo ="", lsNombre  ="", lsCalificacion ="", lsCadenaTasa ="",  lsTemp  ="", lsDescripPlazo ="",
		  lsPlazo ="",  lsNombrePyme	="",  lsEstatus ="", ic_tasa	="",  lsCvePyme ="", lsValorTasaP ="",
		  lsValorTasaN ="", lsFecha ="",  lsPuntos ="";
			
boolean esTasaNegociable	= tipoTasa.equals("N")?true:false;
boolean esTasaPreferencial	= "P".equals(tipoTasa)?true:false;
ArrayList listaTasasPreferenciales 	= null;
ArrayList listaTasasNegociadas		= null;	

HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();
 
StringBuffer 	contenidoArchivo 	= new StringBuffer("");
CreaArchivo 	archivos 			= new CreaArchivo();
String 			nombreArchivo 		= null;
 
AutorizacionTasas beanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

if (informacion.equals("CatalogoEPO")  ) {

	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();	
 
}else if (informacion.equals("CatalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
 
} else if ( informacion.equals("CatalogoIF") ){
	
	JSONObject	catalogo		= new JSONObject();
	registros					= new JSONArray();
	
	lovDatos = !ic_epo.equals("") && !ic_moneda.equals("")?beanTasas.getIntermediario(ic_moneda, ic_epo):new Vector();	
	for (int i=0; i < lovDatos.size(); i++) {
			
		lovRegistro       	= (Vector) lovDatos.elementAt(i);
		String lsClaveIF  	= lovRegistro.elementAt(0).toString();
		String lsNombreIF 	= lovRegistro.elementAt(1).toString();
 
		JSONObject	registro = new JSONObject();
		registro.put("clave",		lsClaveIF	);
		registro.put("descripcion",lsNombreIF	);
		registros.add(registro);
		
	} // for

	// Enviar resultado de la operacion
	catalogo.put("success", 	new Boolean(true)						);
	catalogo.put("total",    	String.valueOf(registros.size()) );
	catalogo.put("registros",	registros			 					);
	
	infoRegresar = catalogo.toString();	
 
}else if (informacion.equals("CatalogoPlazo") ) {
 
	CatalogoPlazos catalogo = new CatalogoPlazos();
	catalogo.setCampoClave("ic_plazo");
	catalogo.setCampoDescripcion("cg_descripcion");	
	catalogo.setClaveProducto("1");	
	catalogo.setOrden("in_plazo_dias");	
	infoRegresar = catalogo.getJSONElementos();	

} else if ( ( informacion.equals("ConsultaPreferencial") || informacion.equals("ConsultaNegociable") ) && pantalla.equals("PreAcuse") ) {

	JSONArray listaTasasInsertar = new JSONArray();  
	JSONArray listaTasasEliminar = new JSONArray();
	
	if(informacion.equals("ConsultaPreferencial") ) {
	
		if(esTasaPreferencial) {
		
			listaTasasPreferenciales 	= beanTasas.procesarTasasPreferenciales( ic_if, ic_moneda, ic_epo, lstPlazo, proceso, lstNumerosLineaOk  );
			if(listaTasasPreferenciales.size()>0){
				for(int i=0;i<listaTasasPreferenciales.size();i++){ 
					
					lovRegistro = (Vector) listaTasasPreferenciales.get(i);	
					numRegistros++;
					
					lsCveEpo  			= (lovRegistro.elementAt(0)  == null)?"":lovRegistro.elementAt(0).toString();
					lsNombre 			= (lovRegistro.elementAt(1)  == null)?"":lovRegistro.elementAt(1).toString();
					lsCalificacion 	= "PREFERENCIAL";
					lsCadenaTasa 		= (lovRegistro.elementAt(3)  == null)?"":lovRegistro.elementAt(3).toString().replaceAll("&nbsp;"," ");
					ldValorTotalTasa	= (lovRegistro.elementAt(4)  == null || (lovRegistro.elementAt(4).toString()).trim().equals(""))?0.00:Double.parseDouble(lovRegistro.elementAt(4).toString());
					lsTemp 				= (lovRegistro.elementAt(5)  == null)?"":lovRegistro.elementAt(5).toString();
					lsDescripPlazo 	= (lovRegistro.elementAt(7)  == null)?"":lovRegistro.elementAt(7).toString();
					lsPlazo				= (lovRegistro.elementAt(6)  == null)?"":lovRegistro.elementAt(6).toString();
					ldTotal 				= (lovRegistro.elementAt(9)  == null || (lovRegistro.elementAt(9).toString()).trim().equals(""))?0.00:Double.parseDouble(lovRegistro.elementAt(9).toString());
					lsNombrePyme		= (lovRegistro.elementAt(10) == null)?"":lovRegistro.elementAt(10).toString();
					ldPuntos 			= (lovRegistro.elementAt(11) == null || (lovRegistro.elementAt(11).toString()).trim().equals(""))?0.00:Double.parseDouble(lovRegistro.elementAt(11).toString());
					lsEstatus			= (lovRegistro.elementAt(12) == null)?"":lovRegistro.elementAt(12).toString();
					ic_tasa				= (lovRegistro.elementAt(8)  == null)?"0":lovRegistro.elementAt(8).toString();
					lsEstatus			= "A".equals(lsEstatus)?"Alta":("E".equals(lsEstatus)?"Eliminar":"");
					lsCvePyme			= (lovRegistro.elementAt(13) == null)?"":lovRegistro.elementAt(13).toString();
					lsValorTasaP		= (lovRegistro.elementAt(14) == null)?"0":lovRegistro.elementAt(14).toString();
					
					if(lsEstatus.equals("Alta") || ldPuntos != 0) {				
						ldTotal = ldTotal;
					} else 	if (lsEstatus.equals("Eliminar") && ldPuntos == 0 && (Double.parseDouble(lsValorTasaP) != 0) ) {			
						ldTotal = (double) (Math.round(ldValorTotalTasa*100000) - Math.round(Double.parseDouble(lsValorTasaP)*100000)) / 100000;
					} else 	if (lsEstatus.equals("Eliminar") && ldPuntos == 0 && (Double.parseDouble(lsValorTasaP) == 0) ) {
						ldTotal = 0;
					}
					String vfloating = "N";
					if("Alta".equals(lsEstatus)){
						listaTasasInsertar.add(lsCvePyme + "|" + lsPlazo + "|" + "0" + "|" + ldPuntos + "|" + ic_tasa + "|" + ldTotal+ "|" + vfloating );
						//estatus ="Alta"; 
					} 
					if("Eliminar".equals(lsEstatus)){
						listaTasasEliminar.add(lsCvePyme + "|" + lsPlazo + "|" + ldTotal + "|" + ldPuntos + "|" + ic_tasa);
						//estatus ="Eliminar";  
					}
															
					datos = new HashMap();
					datos.put("EPO_RELACIONADA", 	lsNombre										);
					datos.put("NOM_PROVEEDOR", 	lsNombrePyme								);
					datos.put("ESTATUS", 			lsEstatus									);
					datos.put("REFERENCIA", 		lsCalificacion								);
					datos.put("PLAZO", 				lsDescripPlazo								);
					datos.put("TIPO_TASA", 			lsCadenaTasa								);
					datos.put("VALOR", 				Double.toString (ldValorTotalTasa)	);
					datos.put("REL_MAT", 			"-"											);
					datos.put("PUNTOS", 				Double.toString (ldPuntos) 			);
					datos.put("VALOR_TASA", 		Double.toString (ldTotal)				);							
					registros.add(datos);
					
				}
			}
		}
		consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		
	} else if(informacion.equals("ConsultaNegociable") ) {
		
		if(esTasaNegociable) {
			listaTasasNegociadas = beanTasas.procesarTasasNegociadas( ic_if, ic_moneda, ic_epo, lstPlazo, proceso, lstNumerosLineaOk );
			if(listaTasasNegociadas.size()>0){
				for(int i=0;i<listaTasasNegociadas.size();i++){ 
					
					lovRegistro = (Vector) listaTasasNegociadas.get(i);
					
					lsCveEpo  			= (lovRegistro.elementAt(0)  == null)?"":lovRegistro.elementAt(0).toString();
					lsNombre 			= (lovRegistro.elementAt(1)  == null)?"":lovRegistro.elementAt(1).toString();
					lsCalificacion 	= "NEGOCIADA";
					lsCadenaTasa 		= (lovRegistro.elementAt(3)  == null)?"":lovRegistro.elementAt(3).toString().replaceAll("&nbsp;"," ");
					ldValorTotalTasa 	= (lovRegistro.elementAt(4)  == null || (lovRegistro.elementAt(4).toString()).trim().equals(""))?0.00:Double.parseDouble(lovRegistro.elementAt(4).toString());
					lsValorTasaN 		= (lovRegistro.elementAt(5)  == null)?"":lovRegistro.elementAt(5).toString();
					lsFecha 				= (lovRegistro.elementAt(6)  == null)?"":lovRegistro.elementAt(6).toString();
					lsTemp 				= (lovRegistro.elementAt(7)  == null)?"":lovRegistro.elementAt(7).toString();
					lsPlazo				= (lovRegistro.elementAt(8)  == null)?"":lovRegistro.elementAt(8).toString(); 
					lsDescripPlazo 	= (lovRegistro.elementAt(9)  == null)?"":lovRegistro.elementAt(9).toString();
					ic_tasa 				= (lovRegistro.elementAt(10) == null)?"0":lovRegistro.elementAt(10).toString();
					ldTasa 				= (lovRegistro.elementAt(11) == null || (lovRegistro.elementAt(11).toString()).trim().equals(""))?0.00:Double.parseDouble(lovRegistro.elementAt(11).toString());
					lsEstatus			= (lovRegistro.elementAt(12) == null)?"":lovRegistro.elementAt(12).toString();
					lsEstatus			= "A".equals(lsEstatus)?"Alta":("E".equals(lsEstatus)?"Eliminar":"");
					lsNombrePyme  		= (lovRegistro.elementAt(13) == null)?"":lovRegistro.elementAt(13).toString();
					lsCvePyme			= (lovRegistro.elementAt(14) == null)?"":lovRegistro.elementAt(14).toString();
					lsPuntos				= "0";
					ldTasaBase 			= (lovRegistro.elementAt(15)  == null || (lovRegistro.elementAt(15).toString()).trim().equals(""))?0.00:Double.parseDouble(lovRegistro.elementAt(15).toString());
					if("Eliminar".equals(lsEstatus) && lsValorTasaN.trim().equals("")){ lsValorTasaN = "0"; }
					String tasa_valor = "Alta".equals(lsEstatus)?String.valueOf(ldTasa):("Eliminar".equals(lsEstatus)?lsValorTasaN:"0.00");
					
					if("Alta".equals(lsEstatus)) {
						listaTasasInsertar.add(lsCvePyme + "|" + lsPlazo + "|" + ldTasa + "|" + "0" + "|" + ic_tasa +"|" + ldTasa);
						//estatus ="Alta";  
					}
					if("Eliminar".equals(lsEstatus)){
						listaTasasEliminar.add(lsCvePyme + "|" + lsPlazo + "|" + lsValorTasaN + "|" + "0" + "|" + ic_tasa);
						//estatus ="Eliminar";  
					}
																	
					datos = new HashMap();
					datos.put("EPO_RELACIONADA", 	lsNombre										);
					datos.put("NOM_PROVEEDOR", 	lsNombrePyme								);
					datos.put("ESTATUS", 			lsEstatus									);
					datos.put("REFERENCIA", 		lsCalificacion								);
					datos.put("PLAZO", 				lsDescripPlazo								);
					datos.put("TIPO_TASA", 			lsCadenaTasa								);
					datos.put("VALOR", 				Double.toString (ldValorTotalTasa)	); // Nota: la version anterior usa: ldTasaBase ( 13forma8mb.jsp:486) 
					datos.put("VALOR_TASA",			tasa_valor									);				
					registros.add(datos);			
					
				}
			}
		}
		consulta =  "{\"success\": true, \"total\": \"" + numRegistros + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);	
	}
	
	jsonObj.put("ic_epo",	 		ic_epo					);
	jsonObj.put("ic_moneda", 		ic_moneda				);
	jsonObj.put("tipoTasa",	 		tipoTasa					);
	jsonObj.put("lstPlazo",	 		lstPlazo					);	
	jsonObj.put("tasas_insertar",	listaTasasInsertar 	);					
	jsonObj.put("tasas_eliminar",	listaTasasEliminar	);	
	//jsonObj.put("es tatus", estatus		);  
	
	infoRegresar  = jsonObj.toString();	
	
} else if(informacion.equals("ConfirmacionTasas") ) {
 
	String _errorGuardar 				= "";
	String folioCert 						= "";
	String _acuse 							= "";
	String mensajeAutentificacion 	= "";
	String fechaHoy  						= "";
	String horaHoy 						= "";
	
	String pkcs7 							= request.getParameter("pkcs7");	
	String externContent 				= request.getParameter("textoFirmar");
	
	String [] listaTasasInsertar		= request.getParameterValues("listaTasasInsertar");
	String [] listaTasasEliminar 		= request.getParameterValues("listaTasasEliminar");

	char 			getReceipt 				= 'Y';
	Seguridad 	s 							= new Seguridad();
	Acuse 		acuse 					= null;
	
	fechaHoy = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
	horaHoy	= (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
 
	if ( !_serial.equals("") && externContent != null && pkcs7 != null ) {
		
		acuse 		= new Acuse(Acuse.ACUSE_IF,"1");
		folioCert 	= acuse.toString();		
		
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			
			_acuse = s.getAcuse();
			try {
				
				boolean hayExito = false;	
				hayExito = beanTasas.guardaModificacionesCargaMasiva(ic_if, ic_moneda, ic_epo, tipoTasa, acuse.toString(), iNoUsuario, _acuse, listaTasasInsertar, listaTasasEliminar, strNombre );

				mensajeAutentificacion = "<b>La autentificación se llevó a cabo con éxito  <b>Recibo:"+_acuse+"</b>";

				if(!hayExito){
					log.debug("Error al Guardar o Eliminar Tasas de la Carga Masiva");
					_errorGuardar 		= "Error al Guardar los Cambios";
					_acuse 				= "ERROR1";
				}
				
			}catch(Exception e){
				log.error("13forma08mext.data.jsp(Exception)");
				log.error("Error al Guardar o Eliminar Tasas de la Carga Masiva");
				e.printStackTrace();
				_errorGuardar 		= "Error al Guardar los Cambios";
				_acuse 				= "ERROR2";
			}	
				
		}else { //autenticación fallida
			mensajeAutentificacion = "<b>La autentificación no se llevó a cabo </b>";
		}
	} else { //autenticación fallida
		mensajeAutentificacion = "<b>La autentificación no se llevó a cabo </b>";
	}	
		
	jsonObj.put("success", 						new Boolean(true)			);
	jsonObj.put("mensajeAutentificacion",	mensajeAutentificacion	);
	
	if(acuse 	!= null) jsonObj.put("acuse",	acuse.formatear()	);	
	if(_acuse 	!= null) jsonObj.put("recibo",_acuse				);
	if(acuse 	!= null) jsonObj.put("acuse2",acuse.toString()	);
	if(acuse 	== null) jsonObj.put("acuse2",""						);
	
	jsonObj.put("fecha",			fechaHoy										);
	jsonObj.put("hora",			horaHoy										);
	jsonObj.put("usuario",		iNoUsuario+" - "+strNombreUsuario	);	
	jsonObj.put("tipoTasa",		tipoTasa										);
	
	infoRegresar  = jsonObj.toString();

} else 	if(informacion.equals("ArchivoCSV") ) {

	contenidoArchivo.append("Recibo,,"+recibo+"\n");
	contenidoArchivo.append(",,Total\n");
	contenidoArchivo.append("No. Total de Tasas Cargadas (Carga Masiva),,"+totalTasasCargadas2+"\n");
	contenidoArchivo.append("No. Total de Tasas "+(esTasaNegociable?"Negociada":(esTasaPreferencial?"Preferencial":""))+" Alta (Carga Masiva),,"+totalTasasAlta2+"\n");
	contenidoArchivo.append("No. Total de Tasas "+(esTasaNegociable?"Negociada":(esTasaPreferencial?"Preferencial":""))+" Eliminadas (Carga Masiva),,"+totalTasasEliminar2+"\n");
	contenidoArchivo.append("Fecha de Carga,,"+fechaCarga+"\n");
	contenidoArchivo.append("Hora de Carga,,"+horaCarga+"\n");
	contenidoArchivo.append("Usuario,,"+iNoUsuario+" - "+strNombreUsuario+"\n");
	
	if(tipoTasa.equals("P")) {
		contenidoArchivo.append("\n,\n");
		contenidoArchivo.append("EPO Relacionada,Nombre Proveedor,Estatus,Referencia,Plazo,Tipo Tasa,Valor,Rel. Mat.,Puntos,Valor Tasa\n");
		contenidoArchivo.append("\n");
	}
	
	if(tipoTasa.equals("N")) {
		contenidoArchivo.append("\n,\n");
		contenidoArchivo.append("EPO Relacionada,Nombre Proveedor,Estatus,Referencia,Plazo,Tipo Tasa,Valor,Valor Tasa\n");
		contenidoArchivo.append("\n");
	}
	
	String[] TasasNegociables		= request.getParameterValues("TasasNegociables");
	String[] TasasPreferenciales 	= request.getParameterValues("TasasPreferenciales");
	String[] registro 				= null;
	
	String 	nombreEpo 				= "";
	String 	nombrePyme 				= "";
	String 	estatus2 				= "";
	String 	referencia				= "";
	String 	plazo						= "";
	String 	tipoTasa2				= "";
	String 	valor 					= "";
	String 	relMat					= "";
	String 	puntos 					= "";
	String 	valorTasa2				= "";
	
	if(tipoTasa.equals("P")) {
	
		for(int i=0; i < TasasPreferenciales.length; i++) {
			
			registro		= TasasPreferenciales[i].split("\\|");
			
			nombreEpo 	= registro[0];
			nombrePyme	= registro[1];
			estatus2 	= registro[2];
			referencia	= registro[3];
			plazo			= registro[4];
			tipoTasa2	= registro[5]; 
			valor			= registro[6]; 
			relMat		= registro[7]; 
			puntos		= registro[8]; 
			valorTasa2	= registro[9]; 
			
			contenidoArchivo.append(nombreEpo.replace(',',' ')	); contenidoArchivo.append(",");
			contenidoArchivo.append(nombrePyme.replace(',',' ')); contenidoArchivo.append(",");
			contenidoArchivo.append(estatus2.replace(',',' ')	); contenidoArchivo.append(",");
			contenidoArchivo.append(referencia.replace(',',' ')); contenidoArchivo.append(",");
			contenidoArchivo.append(plazo.replace(',',' ')     ); contenidoArchivo.append("',");	
			contenidoArchivo.append(tipoTasa2.replace(',',' ')	); contenidoArchivo.append(",");
			contenidoArchivo.append(valor.replace(',',' ')		); contenidoArchivo.append(",");
			contenidoArchivo.append(relMat.replace(',',' ')		); contenidoArchivo.append(",");
			contenidoArchivo.append(puntos.replace(',',' ')		); contenidoArchivo.append(",");
			contenidoArchivo.append(valorTasa2.replace(',',' ')); contenidoArchivo.append("\n");
			
		}	
	}
	if(tipoTasa.equals("N")) {
	
		for(int i=0; i < TasasNegociables.length; i++) {
			
			registro		= TasasNegociables[i].split("\\|");
			
			nombreEpo 	= registro[0];
			nombrePyme	= registro[1];
			estatus2 	= registro[2];
			referencia	= registro[3];
			plazo			= registro[4];
			tipoTasa2	= registro[5]; 
			valor			= registro[6]; 
			valorTasa2	= registro[7]; 
			
			contenidoArchivo.append(nombreEpo.replace(',',' ')	);	contenidoArchivo.append(",");
			contenidoArchivo.append(nombrePyme.replace(',',' ')); contenidoArchivo.append(",");
			contenidoArchivo.append(estatus2.replace(',',' ')	);	contenidoArchivo.append(",");
			contenidoArchivo.append(referencia.replace(',',' ')); contenidoArchivo.append(",");
			contenidoArchivo.append(plazo.replace(',',' ')		);	contenidoArchivo.append("',");				
			contenidoArchivo.append(tipoTasa2.replace(',',' ')	);	contenidoArchivo.append(",");
			contenidoArchivo.append(valor.replace(',',' ')		);	contenidoArchivo.append(",");
			contenidoArchivo.append(valorTasa2.replace(',',' ')); contenidoArchivo.append("\n");
			
		}	
	}
	
	if(!archivos.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
			jsonObj.put("success", 	new Boolean(false)						);
			jsonObj.put("msg", 		"Error al generar el archivo"			);
	} else {
		nombreArchivo = archivos.nombre;		
		jsonObj.put("success", 		new Boolean(true)							);
		jsonObj.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo	);
	}
	
	infoRegresar  = jsonObj.toString();
 
} else if( informacion.equals("ArchivoPDF") ) {

	nombreArchivo 		= archivos.nombreArchivo()+".pdf";	
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
	String []TasasNegociables		= request.getParameterValues("TasasNegociables");
	String []TasasPreferenciales 	= request.getParameterValues("TasasPreferenciales");
	String []registro 				= null;
	
	String nombreEpo 					= "";
	String nombrePyme 				= "";
	String estatus2 					= "";
	String referencia					= "";
	String plazo						= "";
	String tipoTasa2					= "";
	String valor 						= "";
	String relMat						= "";
	String puntos 						= "";
	String valorTasa2					= "";
			
	String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    	= fechaActual.substring(0,2);
	String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   	= fechaActual.substring(6,10);
	String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	String nombreTipoTasa="";		
	
	pdfDoc.encabezadoConImagenes(
		pdfDoc,
		(String) session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String) session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String) session.getAttribute("strLogo"),
		strDirectorioPublicacion
	);	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
 
	pdfDoc.addText("   ","formasB",ComunesPDF.CENTER);
	
	pdfDoc.addText("La autentificación se llevó a cabo con éxito","formasB",ComunesPDF.CENTER);
	pdfDoc.addText("Recibo: "+recibo,"formasB",ComunesPDF.CENTER);
	pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
	
	pdfDoc.setTable(2,60);
	pdfDoc.setCell(" ","celda02",ComunesPDF.LEFT);
	pdfDoc.setCell("Total",					"celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("No. Total de Tasas Cargadas (Carga Masiva)","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(totalTasasCargadas2,	"formas",ComunesPDF.CENTER	);
	pdfDoc.setCell("No. Total de Tasas "+(esTasaNegociable?"Negociada":(esTasaPreferencial?"Preferencial":""))+" Alta (Carga Masiva)","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(totalTasasAlta2,		"formas",ComunesPDF.CENTER	);
	pdfDoc.setCell("No. Total de Tasas "+(esTasaNegociable?"Negociada":(esTasaPreferencial?"Preferencial":""))+" Eliminadas (Carga Masiva)","formas",ComunesPDF.LEFT);
	pdfDoc.setCell(totalTasasEliminar2,	"formas",ComunesPDF.CENTER	);
	pdfDoc.setCell("Fecha de Carga",		"formas",ComunesPDF.LEFT	);
	pdfDoc.setCell(fechaCarga,				"formas",ComunesPDF.CENTER	);
	pdfDoc.setCell("Hora de Carga",		"formas",ComunesPDF.LEFT	);
	pdfDoc.setCell(horaCarga,				"formas",ComunesPDF.CENTER	);
	pdfDoc.setCell("Usuario",				"formas",ComunesPDF.LEFT	);
	pdfDoc.setCell(iNoUsuario+" - "+strNombreUsuario,"formas",ComunesPDF.LEFT);
	pdfDoc.addTable();
	
	pdfDoc.setTable(8);
	pdfDoc.setCell("Preferencial","celda02",ComunesPDF.CENTER);
	pdfDoc.addTable();
		
	pdfDoc.setTable(10, 100);	
	pdfDoc.setCell("EPO Relacionada", "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Nombre Proveedor","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Estatus",         "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Referencia",      "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Plazo",           "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo Tasa",       "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Valor",           "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Rel. Mat.",    	 "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Puntos",       	 "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Valor Tasa",      "celda02",ComunesPDF.CENTER);

	
	if(tipoTasa.equals("P")) {
		
		for(int i=0; i < TasasPreferenciales.length; i++) {
			
			registro		= TasasPreferenciales[i].split("\\|");
			
			nombreEpo 	= registro[0];
			nombrePyme	= registro[1];
			estatus2 	= registro[2];
			referencia	= registro[3];
			plazo			= registro[4];
			tipoTasa2	= registro[5]; 
			valor			= registro[6]; 
			relMat		= registro[7]; 
			puntos		= registro[8]; 
			valorTasa2	= registro[9]; 
			
			pdfDoc.setCell(nombreEpo, 	"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme,	"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus2, 	"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(referencia,	"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(plazo, 		"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(tipoTasa2, 	"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(valor, 		"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(relMat, 		"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(puntos, 		"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(valorTasa2,	"formas",ComunesPDF.CENTER);
			
		}			
	} else { // No es Tasa Preferencial, Mostrar Fila Vacia
		for(int i=0;i<10;i++) pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);	
	}
	pdfDoc.addTable();
	
	pdfDoc.setTable(8);
	pdfDoc.setCell("Negociada","celda02",ComunesPDF.CENTER);
	pdfDoc.addTable();
	
	pdfDoc.setTable(8, 100);	
	pdfDoc.setCell("EPO Relacionada", "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Nombre Proveedor","celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Estatus",         "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Referencia",      "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Plazo",           "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Tipo Tasa",       "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Valor",           "celda02",ComunesPDF.CENTER);
	pdfDoc.setCell("Valor Tasa",      "celda02",ComunesPDF.CENTER);
 
	if(tipoTasa.equals("N")) {
		for(int i=0; i < TasasNegociables.length; i++) {
			
			registro		= TasasNegociables[i].split("\\|");
			
			nombreEpo 	= registro[0];
			nombrePyme	= registro[1];
			estatus2 	= registro[2];
			referencia	= registro[3];
			plazo			= registro[4];
			tipoTasa2	= registro[5];  
			valor			= registro[6]; 
			valorTasa2	= registro[7]; 
			
			pdfDoc.setCell(nombreEpo, 	"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme, "formas",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus2, 	"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(referencia, "formas",ComunesPDF.CENTER);
			pdfDoc.setCell(plazo, 		"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(tipoTasa2, 	"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(valor, 		"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(valorTasa2, "formas",ComunesPDF.CENTER);
			
		}	
	} else { // No es Tasa Preferencial, Mostrar Fila Vacia
		for(int i=0;i<8;i++) pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);	
	}
	pdfDoc.addTable();
	
	pdfDoc.endDocument();
	
	jsonObj.put("success", 		new Boolean(true)							);
	jsonObj.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo	);
	
	infoRegresar = jsonObj.toString();
	
}

%>
<%=infoRegresar%>
