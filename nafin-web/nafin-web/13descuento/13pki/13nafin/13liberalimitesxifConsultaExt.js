Ext.onReady(function() {


	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: 'Captura',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '/nafin/13descuento/13pki/13nafin/13liberalimitesxifext.jsp';
				}
			},	
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',
				text: '<h1><b>Consulta</h1></b>',			
				id: 'btnConsulta',					
				handler: function() {
					window.location = '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaext.jsp';
				}
			}	
		]
	});
	
	//*********************pantalla Principal ****************
	
	function procesarGenerarCSV(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
			btnGenerarCSV.setIconClass('icoXls');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
			
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');						
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;	
			var el = gridConsulta.getGridEl();	
				
			if(jsonData.strTipoUsuario=='NAFIN') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_FIDEICOMISO'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), true);					
			}else	if(jsonData.strTipoUsuario=='IF') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), false);
			}				
			
			if(store.getTotalCount() > 0) {					
				btnGenerarCSV.enable();
				el.unmask();					
			} else {		
				btnGenerarCSV.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [	
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_PYME'},	
			{name: 'NUM_DOCTO'},
			{name: 'TASA_FONDEO'},	
			{name: 'NUMERO_PRESTAMO'},
			{name: 'MONEDA'},
			{name: 'MONTO_DESC'},
			{name: 'FECHA_VENC'},
			{name: 'SELECCIONAR'},
			{name: 'IC_DOCUMENTO'},
			{name: 'IC_MONEDA'},
			{name: 'FECHA_PREPAGO'},
			{name: 'REFERENCIA'},
			{name :'NOMBRE_FIDEICOMISO' }			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});

	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Liberaci�n de L�mites por IF',
		hidden: true,
		clicksToEdit: 1,	
		columns: [	
			{							
				header : 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex : 'NOMBRE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',	
				hidden: true
			},
			{							
				header : 'Pyme',
				tooltip: 'Pyme',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'			
			},
			{							
				header : 'N�mero de documento',
				tooltip: 'N�mero de documento',
				dataIndex : 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},
			{							
				header : 'Tasa de Fondeo',
				tooltip: 'Tasa de Fondeo',
				dataIndex : 'TASA_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',				
				renderer:function(value,metadata,registro){    
					if(value =='0') {
						return '%';
					}else {
						return  Ext.util.Format.number(value, '0.0000%');
					}
				}
			},
			{							
				header : 'N�mero de Prestamo',
				tooltip: 'N�mero de Prestamo',
				dataIndex : 'NUMERO_PRESTAMO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){    
					if(value =='0') {
						return'';
					}else {
						return  value;
					}
				}
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},
			{							
				header : 'Nombre Fideicomiso',
				tooltip: 'Nombre Fideicomiso',
				dataIndex : 'NOMBRE_FIDEICOMISO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',
				hidden: true
			},		
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex : 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},
			{							
				header : 'Fecha de Prepago',
				tooltip: 'Fecha de Prepago',
				dataIndex : 'FECHA_PREPAGO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},			
			{							
				header : '<center>Referencia</center>',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'			
			}	
		],					
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,	
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip:	'Generar Archivo',
					id: 'btnGenerarCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaExt.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoCSV'
							})
							,callback: procesarGenerarCSV
						});
					}
				}
			]
		}
	});
	
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){
				var grid = Ext.getCmp("gridConsulta");
				//var cm = grid.getColumnModel();
			
				Ext.getCmp("fechaIni").setValue(jsonData.fechaHoy);
				Ext.getCmp("fechaFin").setValue(jsonData.fechaHoy);	
				Ext.getCmp("strTipoUsuario").setValue(jsonData.strTipoUsuario);
				
				if(jsonData.strTipoUsuario=='IF') {
					//grid.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), false);
					
					Ext.getCmp("ic_if1").hide();
					Ext.getCmp("num_electronicoC1").show();
					Ext.getCmp("fpBotones").hide();
					
				}else if(jsonData.strTipoUsuario=='NAFIN') {
					//grid.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), true);
					
					Ext.getCmp("ic_if1").show();
					Ext.getCmp("num_electronicoC1").hide();
					Ext.getCmp("fpBotones").show();					
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var successAjaxFn = function(opts, success, response) { 
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);	
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarCatalogoEPO= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = store.reader.jsonData;
			var ic_epo1 = Ext.getCmp('ic_epo1');
			if(ic_epo1.getValue()==''){
				ic_epo1.setValue(jsonData.ic_epo);
			}
		}
  }

	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaExt.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	
			load: procesarCatalogoEPO,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaExt.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaExt.data.jsp', 
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'noPyme',
			id: 'noPyme1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);		
							var ic_epo = Ext.getCmp("ic_epo1").getValue();	
							
							storeBusqAvanzPyme.load({
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
										ic_epo:ic_epo						
									})																	
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	})
	
	
	var elementosForma  = [
		{ 	xtype: 'displayfield', 	hidden: true, id: 'fechaHoy', 	value: '' },			
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEPO,
			listeners: {
				select: {
					fn: function(combo) {
						var ic_if = Ext.getCmp('ic_if1').getValue();						
						if(ic_if =='') {						
							var cmbIF = Ext.getCmp('ic_if1');
							cmbIF.setValue('');
							cmbIF.store.load({
								params: {
									ic_epo:combo.getValue()
								}
							});
						}
					}
				}
			}			
		},
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			name: 'ic_if',
			id: 'ic_if1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_if',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,
			store: catalogoIF,
			listeners: {
				select: {
					fn: function(combo) {
						var ic_epo = Ext.getCmp("ic_epo1").getValue();
						var cmbEPO = Ext.getCmp('ic_epo1');
						cmbEPO.setValue('');
						cmbEPO.store.load({
							params: {
								ic_if:combo.getValue(),
								ic_epo:ic_epo
							}
						});
					}
				}
			}			
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero Nafin Electr�nico',
			combineErrors: false,
			msgTarget: 'side',
			id: 'num_electronicoC1',
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'Proveedor',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){					
							
						 // Petici�n b�sica  
							Ext.Ajax.request({  
								url: '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaExt.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
								params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue()
								}  
							}); 	
						}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 300,
					disabled:true,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
				xtype: 'button',
				text: 'B�squeda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
					}
				}
			}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaIni',
					id: 'fechaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaFin',
					margins: '0 20 0 0' ,
					formBind: true,
					listeners:{
						blur: function(field){ 									
							if(field.getValue() != '') {								
								var fechaHoy = Ext.getCmp('fechaHoy').getValue();														
								if( Ext.util.Format.date(field.getValue(),'d/m/Y') <  fechaHoy )  {
									Ext.MessageBox.alert('Mensaje','La fecha debe ser igual o mayor al dia actual');
									Ext.getCmp("fechaIni").setValue(fechaHoy);									
								}
							}							
						}
					}
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaFin',
					id: 'fechaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fechaIni',
					margins: '0 20 0 0',
					formBind: true,
					listeners:{
						blur: function(field){ 									
							if(field.getValue() != '') {								
								var fechaHoy = Ext.getCmp('fechaHoy').getValue();														
								if( Ext.util.Format.date(field.getValue(),'d/m/Y') <  fechaHoy )  {
									Ext.MessageBox.alert('Mensaje','La fecha debe ser igual o mayor al dia actual');
									Ext.getCmp("fechaFin").setValue(fechaHoy);									
								}
							}							
						}
					}
				}
			]
		},
		{ 	xtype: 'displayfield', hidden:true, id: 'strTipoUsuario', 	value: '' },
		{ 	xtype: 'displayfield', hidden:true, id: 'ic_pyme', 	value: '' }	
	];		

	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		title: 'Consulta Liberaci�n de L�mites por IF',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,	  		
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
				
					var ic_epo = Ext.getCmp("ic_epo1");
					if (Ext.isEmpty(ic_epo.getValue()) ){
						ic_epo.markInvalid('Debe Seleccionar una EPO');
						return;
					}
					
					var fechaIni = Ext.getCmp("fechaIni");
					var fechaFin = Ext.getCmp("fechaFin");
					
					if(!Ext.isEmpty(fechaIni.getValue()) || !Ext.isEmpty(fechaFin.getValue())){
						if(Ext.isEmpty(fechaIni.getValue())){
							fechaIni.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaFin.getValue())){
							fechaFin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							fechaFin.focus();
							return;
						}
					}
					
					var fechaIni_ = Ext.util.Format.date(fechaIni.getValue(),'d/m/Y');
					var fechaFin_ = Ext.util.Format.date(fechaFin.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(fechaIni.getValue())){
						if(!isdate(fechaIni_)) { 
							fechaIni.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fechaIni.focus();
							return;
						}
					}
					if( !Ext.isEmpty(fechaFin.getValue())){
						if(!isdate(fechaFin_)) { 
							fechaFin.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							fechaFin.focus();
							return;
						}
					}

					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							ic_pyme:Ext.getCmp('ic_pyme').getValue()
						})
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					if(Ext.getCmp("strTipoUsuario").getValue()=='IF') {
						window.location = '/nafin/13descuento/13pki/13if/13liberalimitesxifConsultaExt.jsp';	
					}else if(Ext.getCmp("strTipoUsuario").getValue()=='NAFIN') {
						window.location = '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaext.jsp';
					}							
				}				
			}
		]
	});
	


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			fpBotones, 
			NE.util.getEspaciador(20),
			fp,				
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});


//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaExt.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});
	
	catalogoEPO.load();
	
});