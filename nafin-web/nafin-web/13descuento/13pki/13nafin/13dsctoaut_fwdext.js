/**
*	FODEA 025-2012: DESCUENTO AUTOM�TICO
*	@fecha 04 Ene 2013
*	@autor garellano
*/

Ext.onReady(function(){      
		/*============= Creo un namespace de la pantalla =======================*/
      Ext.namespace("DSCTO.orden");                  
      
      /*========================================================================
                        OBJETO PARA OBTENER LAS EPOS 
      ========================================================================*/
      DSCTO.orden.ObtieneEpos = new ObtieneEpos();
      
      function ObtieneEpos(){
         this.urlRequest   =  '13dsctoaut_fwdext.data.jsp';
         this.urlResponse  =  '13dsctoaut_fwdext.jsp';
         this.estado       =  'INICIAR';
      }
		
		function cancelar(){
			window.location =  DSCTO.orden.ObtieneEpos.urlResponse;
		}
      
		/*=================== Formulario Principal =============================*/
		var criteriosBusqueda = new Ext.form.FieldSet({
			style: 'margin: 20px;',
			title: 'Utilice el * para b�squeda gen�rica', 
			defaults: {
				xtype: 'textfield'
			},
			items: [
				{
					fieldLabel: 'N@E PyME',
					id: 'num_pyme'
				},
				/*{
					fieldLabel: 'Nombre',
					id: 'nombre_pyme'
				},*/
				{
					fieldLabel: 'RFC',
					id: 'rfc_pyme'
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'N�mero SIRAC',
					id: 'num_sirac_pyme'
				}
			]
		});
			
		var pnlBotones = new Ext.Panel({		
			border: false,
			style: 'margin: 20px;',
			items: [
				{
					xtype: 'button',
					text: 'Cancelar',
					style: 'float:right',
					iconCls: 'borrar',
					handler: cancelar
				},
				{
					xtype: 'label',
					html: ' &nbsp; ',
					width: 20,
					style: 'float:right'
				}, 
				{
					xtype: 'button',
					text: 'Consultar',
					style: 'float:right',
					iconCls: 'icoBuscar',
					handler: function(boton, evt){
						Ext.getCmp('cboPyme').getStore().load(
							{
								params: {
									num_pyme: Ext.getCmp('num_pyme').getValue(),
									//nombre_pyme: Ext.getCmp('nombre_pyme').getValue(),
									rfc_pyme: Ext.getCmp('rfc_pyme').getValue(),
									num_sirac_pyme: Ext.getCmp('num_sirac_pyme').getValue()
								}	
							}
						);
						
						Ext.getCmp('cboPyme').setDisabled(false);
						Ext.getCmp('cboPyme').show();
					}
				}
			]
		});
		
		/*=================== Formulario Principal =============================*/
		var frmPrincipal = new Ext.FormPanel({
				title: 'Criterios de B�squeda',
            style: 'margin: 0 auto;',
				width: 600,
				items: [
					criteriosBusqueda,
					pnlBotones,
					{
						xtype: 'combo',
						typeAhead: true,
						id: 'cboPyme',
						disabled: true,
						width: 300,
						editable: true,
						selectOnFocus: true,
						triggerAction: 'all',
						hidden:	true,
						fieldLabel: 'Nombre de la PyME',
						labelStyle: 'width: 130px; margin-left: 10px;',
						valueField: 'clave',
						displayField: 'descripcion',
						mode: 'local',
						store: new Ext.data.JsonStore({
							root : 'registros',
							fields : ['clave', 'descripcion'],
							url : DSCTO.orden.ObtieneEpos.urlRequest,
							baseParams: {
								informacion: 'obtienePYMES'
							},
							totalProperty : 'total',
							autoLoad: false,
							listeners: {
								exception: NE.util.mostrarDataProxyError,
								beforeload: NE.util.initMensajeCargaCombo
							}
						}),
						listeners: {
							select: function(cmb,record,index){
								grid.show();
								grid.getStore().load(
									{
										params: {
											ic_pyme: record.get('clave')
										}
									}
								);
							}
						}
					},
					{
						xtype: 'label',
						html: ' &nbsp;'
					}
				]
		});
		
		var grid = new Ext.grid.EditorGridPanel({
			store: new Ext.data.GroupingStore({
				root: 'registros',
				url: DSCTO.orden.ObtieneEpos.urlRequest,
				baseParams: {
					informacion: 'obtieneDireccion'
				},
				reader: new Ext.data.JsonReader({
					root: 'registros',
					totalProperty: 'total',
					fields:[
						{	name: 'colonia'	               },
						{	name: 'telefono1'	            },
						{	name: 'rfcPyme'	                  },
						{  name: 'codigoPostal'	            },
						{	name: 'domicilio'	                  }
					]
				}),
				totalProperty : 'total',
				messageProperty: 'msg',
				autoLoad: false
			}),
			loadMask:true,	
			frame: false,
			border: true,
			style: 'margin: 10px auto', 
			hidden: true,
			stripeRows: true,
			columnLines: true,
			enableColumnMove: false,
			enableColumnHide: false,
			height: 90,
			width: 800,
			columns:[
				{
					header: 'RFC: ',
					dataIndex: 'rfcPyme',
					sortable: true,
					hideable: false,
					width: 130,
					align: 'center'
				},
				{
					header: 'Calle y N�mero',
					dataIndex: 'domicilio',
					sortable: true,
					width: 300,
					align: 'center'
				},
				{
					header: 'Colonia',
					dataIndex: 'colonia',
					sortable: true,
					width: 210,
					align: 'center'
				},
				{
					header: 'Telefono',
					dataIndex: 'telefono1',
					sortable: true,
					width: 150,
					align: 'center'				}
			],
			buttons: [
				{
					text: 'Siguiente',
					iconCls: 'icoBuscar',
					handler: function(){
						window.location = '13dsctoaut02ext.jsp?ic_pyme='+Ext.getCmp('cboPyme').getValue();
					}
				},
				{
					text: 'Cancelar',
					iconCls: 'borrar',
					handler: cancelar
				}
			]
	});
		
		/*=================== Contenedor Principal =============================*/		
		var pnl = new Ext.Container({
				id: 		'contenedorPrincipal',
				applyTo: 'areaContenido',
				style: 	'margin:0 auto;',
				width: 	949,
				items: 	[frmPrincipal, grid]
		});
		
		/*========================= INICIA PROGRAMA ============================*/   
});