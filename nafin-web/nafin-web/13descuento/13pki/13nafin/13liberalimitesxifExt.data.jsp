<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.math.*, 
	java.text.*, 
	java.sql.*,
	netropology.utilerias.*,	
	com.netro.afiliacion.*,
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	com.netro.descuento.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String pantalla = (request.getParameter("pantalla") != null) ? request.getParameter("pantalla") : "";
String ic_epo = (request.getParameter("ic_epo") != null) ? request.getParameter("ic_epo") : "";
String consulta = (request.getParameter("consulta") != null) ? request.getParameter("consulta") : "";
String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String num_electronico = (request.getParameter("num_electronico") != null) ? request.getParameter("num_electronico") : "";
String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
String fechaIni = (request.getParameter("fechaIni") != null) ? request.getParameter("fechaIni") : "";
String fechaFin = (request.getParameter("fechaFin") != null) ? request.getParameter("fechaFin") : "";
String doctoSelec = (request.getParameter("doctoSelec") != null) ? request.getParameter("doctoSelec") : "";
String [] documentos		= request.getParameterValues("documentos");
String [] valores		= request.getParameterValues("valores");
String ic_if = (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") : "";

String  infoRegresar ="", txtNombre ="", estatus="4";
StringBuffer lisDocumentos 	= new StringBuffer();
StringBuffer texto = new StringBuffer();

if (strTipoUsuario.equals("IF")) { 
 ic_if =iNoCliente;
}


if(documentos !=null){
	for (int i=0; i<=documentos.length-1; i++) {
		lisDocumentos.append(documentos[i]+",");
	}
	lisDocumentos.deleteCharAt(lisDocumentos.length()-1);	
	doctoSelec =lisDocumentos.toString();
}
if (pantalla.equals("PantallaAcuse") )  {
	estatus ="11";
}

HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

LimitesPorEPO limitesxEpo = ServiceLocator.getInstance().lookup("LimitesPorEPOEJB", LimitesPorEPO.class);
ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

if (informacion.equals("valoresIniciales")  ) {

	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("fechaHoy", fechaHoy);
	jsonObj.put("strTipoUsuario", strTipoUsuario);
	infoRegresar = jsonObj.toString();		

} else if (informacion.equals("catalogoEPO")  ) {
		
	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");		
	catalogo.setClave_if(ic_if);
	catalogo.setOrden("ce.cg_razon_social");	
		
	jsonObj = JSONObject.fromObject(catalogo.getJSONElementos());	
	jsonObj.put("ic_epo", ic_epo);
	infoRegresar = jsonObj.toString();
	
	
} else if (informacion.equals("catalogoIF")  ) {
	LiberacionLimitesIF paginador = new LiberacionLimitesIF();
	List catEPO = paginador.getCatalogoIF(ic_epo);
	jsonObj.put("registros", catEPO);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();
	

} else if (informacion.equals("busquedaAvanzada") ) {

	String rfc_pyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
	String nombre_pyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String num_pyme = request.getParameter("noPyme")==null?"":request.getParameter("noPyme");
	
	CatalogoPymeBusqAvanzadaIF cat = new CatalogoPymeBusqAvanzadaIF();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion("crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");
	cat.setIc_epo(ic_epo);
	cat.setRfc_pyme(rfc_pyme);
	cat.setNombre_pyme(nombre_pyme);	
	cat.setNum_pyme(num_pyme);	
	cat.setIc_pyme("");	
	cat.setOrden("p.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}  else if (informacion.equals("pymeNombre")  ) {
	
	List datosPymes =  BeanParamDscto.datosPymes(num_electronico, ic_epo ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	jsonObj.put("ic_pyme", ic_pyme);
	infoRegresar = jsonObj.toString();		

} else if (informacion.equals("Consultar")   ||  informacion.equals("ConsultarPreAcuse")  ||  informacion.equals("ArchivoPDF") )  {
		
	
	LiberacionLimitesIF paginador = new LiberacionLimitesIF();
			
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
	
	paginador.setFechaIni(fechaIni);
	paginador.setFechaFin(fechaFin);
	
	paginador.setClaveEPO(ic_epo);
	paginador.setClaveIF(ic_if);
	paginador.setClavePyme(ic_pyme);
	paginador.setFechaHoy(fechaHoy);
	paginador.setTipoUsuario(strTipoUsuario);
	paginador.setDocumentos(doctoSelec); 
	paginador.setEstatus(estatus); 
	paginador.setPantalla("Captura");
	
	if(!informacion.equals("ArchivoPDF")){ 
	
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}	
			
		if (pantalla.equals("PantallaPreAcuse") )  {
			if (strTipoUsuario.equals("NAFIN")) {
				texto.append(" <b>Confirme la aplicación de la liberación de los vencimientos de los siguientes Intermediarios Financieros:<b>");
			} else {	
				texto.append("<b>Confirme la aplicación de la liberación de los vencimientos<b>");
			}
		}
	
		
		if (pantalla.equals("PantallaAcuse") )  {
			if (strTipoUsuario.equals("NAFIN")) {				
				texto.append("<b>Se realizó la liberación de los vencimientos de los siguientes Intermediarios Financieros:<b>");
			} else {	
				texto.append("<b> Se realizó la liberación de los vencimientos<b>");
			}
		}
		
		
		jsonObj = JSONObject.fromObject(consulta);	
		jsonObj.put("mensaje", texto.toString());
		jsonObj.put("pantalla", pantalla);
		jsonObj.put("strTipoUsuario", strTipoUsuario);
		infoRegresar = jsonObj.toString();	
	}
	
	if(informacion.equals("ArchivoPDF")){ 
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "pdf");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}			
	}
	
} else if (informacion.equals("Totales")  )  {

	for(int t =0; t<2; t++) {		
		datos = new HashMap();		
		if(t==0){ 	
			datos.put("IC_MONEDA", "1");
			datos.put("MONEDA", "Nacional");		
			datos.put("MONTO_TOTAL", "0" );													
		}		
		if(t==1){ 		
			datos.put("IC_MONEDA", "54");
			datos.put("MONEDA", "Dólares ");		
			datos.put("MONTO_TOTAL", "0" );											
		}		
		registros.add(datos);
	}
	
	consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();	


} else if (informacion.equals("LiberarVencimiento")  )  {

	String folioCert = "",  _acuse = "", horaHoy ="", mensajeAutentificacion ="";
	String pkcs7 = request.getParameter("pkcs7");	
	String externContent = request.getParameter("textoFirmar");
	char getReceipt = 'Y';
	Seguridad s = new Seguridad();
	Acuse acuse = null;		
	horaHoy= (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
	// convertimos array a String 
	StringBuffer  valoresT = new StringBuffer();
	for(int i=0;i<valores.length;i++) {			
		valoresT.append(valores[i]);			
	}	
	
	try{	
		if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
			acuse = new Acuse(Acuse.ACUSE_EPO,"1");
			folioCert = acuse.toString();		
			
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
				_acuse = s.getAcuse();			
				
				//  liberar documentos por IF 
				limitesxEpo.liberarLimitesxif(valoresT.toString(),fechaIni, fechaFin);	
				//actuara en Bitacora 
				limitesxEpo.bitacoraLimitesxif(valoresT.toString(), iNoUsuario+"-"+strNombreUsuario, _acuse); 	//Fodea 047-2009- Liberacion de Limites If 
	
				mensajeAutentificacion = "<b>La autentificación se llevo a cabo con exito  <b>Recibo:"+_acuse+"</b>";
			
			}else { //autenticación fallida
				mensajeAutentificacion = "<b>La autentificación no se llevo a cabo </b>";
			}
		} else { //autenticación fallida
			mensajeAutentificacion = "<b>La autentificación no se llevo a cabo </b>";
		}	
	}catch(Exception e){
		out.println("Error en jsp");
		System.out.println("error:::"+e);
		e.printStackTrace();
	}finally{
		System.out.println(" liberar documentos por IF  :::");
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("mensajeAutentificacion",mensajeAutentificacion);
	if(acuse !=null)jsonObj.put("acuse",acuse.formatear());	
	if(acuse ==null)jsonObj.put("acuse",acuse);	
	jsonObj.put("recibo",_acuse);
	if(acuse ==null)jsonObj.put("acuse2",acuse.toString());
	jsonObj.put("fecha",fechaHoy);
	jsonObj.put("hora",horaHoy);
	jsonObj.put("usuario",iNoUsuario+" - "+strNombreUsuario);
	jsonObj.put("doctoSelec",doctoSelec);	
	infoRegresar  = jsonObj.toString();
			
}
%>
<%=infoRegresar%>
