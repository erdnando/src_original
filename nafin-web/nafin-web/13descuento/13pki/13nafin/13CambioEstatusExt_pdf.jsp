<%@ page import="java.util.*, 
		org.apache.commons.logging.Log,
		com.netro.exception.*,
		com.netro.pdf.*,
		java.math.*,
		netropology.utilerias.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	contentType="application/json;charset=UTF-8"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>

<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

JSONObject jsonObj = new JSONObject();

try {

	//	DECLARACION DE VARIABLES
	String jsonAceptados	= (request.getParameter("jsonAceptados")!=null)?request.getParameter("jsonAceptados"):"";
	String acuse			= (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";

	String nombreArchivo = "";

	List arrRegistrosAcepta		= JSONArray.fromObject(jsonAceptados);
	Iterator itRegAcepta			= arrRegistrosAcepta.iterator();

	CreaArchivo archivo = new CreaArchivo();
	nombreArchivo = archivo.nombreArchivo()+".pdf";
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
											((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
											(String)session.getAttribute("sesExterno"),
											(String) session.getAttribute("strNombre"),
											(String) session.getAttribute("strNombreUsuario"),
											(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
	pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

	pdfDoc.addText("La autentificaci�n se llev� a cabo con �xito \n Recibo: "+acuse, "titulo", ComunesPDF.CENTER);
	pdfDoc.setTable(16, 100);

	pdfDoc.setCell("Nombre EPO", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("N�mero de Documento", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Fecha de Vencimiento", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Estatus Anterior", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Moneda", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Monto", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Porcentaje de Descuento", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Recurso en garant�a", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Monto a Descontar", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Nombre IF", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Monto Interes", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Monto a operar", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Nombre\nFIDEICOMISO", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Monto Interes\nFIDEICOMIS0", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Monto a operar\nFIDEICOMISO", "celda02", ComunesPDF.CENTER,1,2);
	pdfDoc.setCell("Nuevo Estatus", "celda02", ComunesPDF.CENTER,1,2);

	pdfDoc.setHeaders();

	int numRegistrosMN = 0;
	int numRegistrosDL = 0;
	BigDecimal montoTotalMN = new BigDecimal("0.00");
	BigDecimal montoTotalDL = new BigDecimal("0.00");
	BigDecimal montoTotalDescuentoMN = new BigDecimal("0.00");
	BigDecimal montoTotalDescuentoDL = new BigDecimal("0.00");
	BigDecimal importeTotalInteresMN = new BigDecimal("0.00");
	BigDecimal importeTotalInteresDL = new BigDecimal("0.00");
	BigDecimal importeTotalRecibirMN = new BigDecimal("0.00");
	BigDecimal importeTotalRecibirDL = new BigDecimal("0.00");
	BigDecimal importeTotalRecGarMN = new BigDecimal("0.00");
	BigDecimal importeTotalRecGarDL = new BigDecimal("0.00");
	
	int numRegistrosMNFIDE = 0;
	int numRegistrosDLFIDE = 0;
	BigDecimal montoTotalMNFIDE = new BigDecimal("0.00");
	BigDecimal montoTotalDLFIDE = new BigDecimal("0.00");
	BigDecimal montoTotalDescuentoMNFIDE = new BigDecimal("0.00");
	BigDecimal montoTotalDescuentoDLFIDE = new BigDecimal("0.00");
	BigDecimal importeTotalInteresMNFIDE = new BigDecimal("0.00");
	BigDecimal importeTotalInteresDLFIDE = new BigDecimal("0.00");
	BigDecimal importeTotalRecibirMNFIDE = new BigDecimal("0.00");
	BigDecimal importeTotalRecibirDLFIDE = new BigDecimal("0.00");
	BigDecimal importeTotalRecGarMNFIDE = new BigDecimal("0.00");
	BigDecimal importeTotalRecGarDLFIDE = new BigDecimal("0.00");
	
	BigDecimal importeTotalInteres2MNFIDE = new BigDecimal("0.00");
	BigDecimal importeTotalInteres2DLFIDE = new BigDecimal("0.00");
	BigDecimal importeTotalRecibir2MNFIDE = new BigDecimal("0.00");
	BigDecimal importeTotalRecibir2DLFIDE = new BigDecimal("0.00");

	//for(int i = 0; i < registros.size(); i++){
	int count = 0;
	while (itRegAcepta.hasNext()) {
		JSONObject registro = (JSONObject)itRegAcepta.next();			

		String nombreEpo			=	registro.getString("CG_RAZON_SOCIAL");
		String noDocumento1		=	registro.getString("IG_NUMERO_DOCTO");
		String fechaVen			=	registro.getString("DF_FECHA_VENC");
		String estatus1			=	registro.getString("NUEVO_ESTATUS");
		String moneda				=	registro.getString("CD_NOMBRE");	
		String monto				=	(registro.getString("FN_MONTO").equals(""))?"0":registro.getString("FN_MONTO");
		String porcentaje			=	registro.getString("FN_PORC_ANTICIPO");
		String montoDesc			=	(registro.getString("FN_MONTO_DSCTO").equals(""))?"0":registro.getString("FN_MONTO_DSCTO");
		String montoInteres		=	(registro.getString("IN_IMPORTE_INTERES").equals(""))?"0":registro.getString("IN_IMPORTE_INTERES");
		String montoOperar		=	(registro.getString("IN_IMPORTE_RECIBIR").equals(""))?"0":registro.getString("IN_IMPORTE_RECIBIR");
		String noDocumentoFinal	=	registro.getString("IC_DOCUMENTO");
		String ic_moneda			=	registro.getString("IC_MONEDA");
		String nombreIF			=	registro.getString("NOMBRE_IF");
		String nombreIFFIDE		=	registro.getString("NOMBRE_IF_FONDEO");
		String montoInteresFIDE	=	(registro.getString("IN_IMPORTE_INTERES_FONDEO").equals(""))?"N/A":registro.getString("IN_IMPORTE_INTERES_FONDEO");
		String montoOperarFIDE	=	(registro.getString("IN_IMPORTE_RECIBIR_FONDEO").equals(""))?"N/A":registro.getString("IN_IMPORTE_RECIBIR_FONDEO");
		String cs_opera_fiso		=	registro.getString("CS_OPERA_FISO");
		
		double recurso = new Double(monto).doubleValue() - new Double(montoDesc).doubleValue();
		
		if(cs_opera_fiso.equals("S")){
			if (ic_moneda.equals("1")) { // Moneda Nacional 
				numRegistrosMNFIDE++;
			
				montoTotalMNFIDE = montoTotalMNFIDE.add(new BigDecimal(monto));
				montoTotalDescuentoMNFIDE = montoTotalDescuentoMNFIDE.add(new BigDecimal(montoDesc));
			
				if(!"".equals(montoInteresFIDE)){
								importeTotalInteresMNFIDE = importeTotalInteresMNFIDE.add(new BigDecimal(montoInteresFIDE));
							}
				if(!"".equals(montoOperarFIDE)){
					importeTotalRecibirMNFIDE = importeTotalRecibirMNFIDE.add(new BigDecimal(montoOperarFIDE));
				}
				
				if(!"".equals(montoInteres)){
								importeTotalInteres2MNFIDE = importeTotalInteres2MNFIDE.add(new BigDecimal(montoInteres));
							}
				if(!"".equals(montoOperar)){
					importeTotalRecibir2MNFIDE = importeTotalRecibir2MNFIDE.add(new BigDecimal(montoOperar));
				}
				importeTotalRecGarMNFIDE = importeTotalRecGarMN.add(new BigDecimal(recurso));
			
			}else {  // Moneda Extranjera 
			
				numRegistrosDLFIDE++;
				montoTotalDLFIDE = montoTotalDLFIDE.add(new BigDecimal(monto));
				montoTotalDescuentoDLFIDE = montoTotalDescuentoDLFIDE.add(new BigDecimal(montoDesc));
				if(!"".equals(montoInteresFIDE)){
					importeTotalInteresDLFIDE = importeTotalInteresDLFIDE.add(new BigDecimal(montoInteresFIDE));
				}
				if(!"".equals(montoOperarFIDE)){
					importeTotalRecibirDLFIDE = importeTotalRecibirDLFIDE.add(new BigDecimal(montoOperarFIDE));
				}
				if(!"".equals(montoInteres)){
								importeTotalInteres2DLFIDE = importeTotalInteres2DLFIDE.add(new BigDecimal(montoInteres));
							}
				if(!"".equals(montoOperar)){
					importeTotalRecibir2DLFIDE = importeTotalRecibir2DLFIDE.add(new BigDecimal(montoOperar));
				}
					importeTotalRecGarDLFIDE = importeTotalRecGarDLFIDE.add(new BigDecimal(recurso));
			}
		}else{
						if (ic_moneda.equals("1")) { // Moneda Nacional 
				numRegistrosMN++;
			
				montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
				montoTotalDescuentoMN = montoTotalDescuentoMN.add(new BigDecimal(montoDesc));
			
				if(!"".equals(montoInteres)){
								importeTotalInteresMN = importeTotalInteresMN.add(new BigDecimal(montoInteres));
							}
				if(!"".equals(montoOperar)){
					importeTotalRecibirMN = importeTotalRecibirMN.add(new BigDecimal(montoOperar));
				}
				importeTotalRecGarMN = importeTotalRecGarMN.add(new BigDecimal(recurso));
			
			}else {  // Moneda Extranjera 
			
				numRegistrosDL++;
				montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
				montoTotalDescuentoDL = montoTotalDescuentoDL.add(new BigDecimal(montoDesc));
				if(!"".equals(montoInteres)){
					importeTotalInteresDL = importeTotalInteresDL.add(new BigDecimal(montoInteres));
				}
				if(!"".equals(montoOperar)){
					importeTotalRecibirDL = importeTotalRecibirDL.add(new BigDecimal(montoOperar));
				}
					importeTotalRecGarDL = importeTotalRecGarDL.add(new BigDecimal(recurso));
			}

		}

		pdfDoc.setCell(nombreEpo + "", "formas", ComunesPDF.JUSTIFIED,1,2);
		pdfDoc.setCell(noDocumento1 + "", "formas", ComunesPDF.JUSTIFIED,1,2);
		pdfDoc.setCell(fechaVen + "", "formas", ComunesPDF.JUSTIFIED,1,2);
		pdfDoc.setCell("En Proceso de Autorizaci�n" + "", "formas", ComunesPDF.JUSTIFIED,1,2);
		pdfDoc.setCell(moneda + "", "formas", ComunesPDF.JUSTIFIED,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2) + "", "formas", ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(porcentaje + "%", "formas", ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(recurso,2) + "", "formas", ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDesc,2) + "", "formas", ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(nombreIF + "", "formas", ComunesPDF.LEFT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoInteres,2) + "", "formas", ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoOperar,2) + "", "formas", ComunesPDF.RIGHT,1,2);
		
		pdfDoc.setCell(nombreIFFIDE + "", "formas", ComunesPDF.LEFT,1,2);
		
		if(cs_opera_fiso.equals("S")){
			pdfDoc.setCell((!montoInteresFIDE.equals("N/A"))?"$"+Comunes.formatoDecimal(montoInteresFIDE,2) + "":montoInteresFIDE, "formas", ComunesPDF.RIGHT,1,2);
			pdfDoc.setCell((!montoOperarFIDE.equals("N/A"))?"$"+Comunes.formatoDecimal(montoOperarFIDE,2) + "":montoOperarFIDE, "formas", ComunesPDF.RIGHT,1,2);
		}else{
			pdfDoc.setCell((!montoInteresFIDE.equals("N/A"))?"$"+Comunes.formatoDecimal(montoInteresFIDE,2) + "":montoInteresFIDE, "formas", ComunesPDF.CENTER,1,2);
			pdfDoc.setCell((!montoOperarFIDE.equals("N/A"))?"$"+Comunes.formatoDecimal(montoOperarFIDE,2) + "":montoOperarFIDE, "formas", ComunesPDF.CENTER,1,2);
	
		}
		pdfDoc.setCell( estatus1+ "", "formas", ComunesPDF.JUSTIFIED,1,2);
		
		count++;

	}
	if (numRegistrosMNFIDE!=0) {
		pdfDoc.setCell("Total Moneda Nacional FIDEICOMISO","formas",ComunesPDF.RIGHT,5,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalMNFIDE,2),"formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecGarMNFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDescuentoMNFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalInteres2MNFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecibir2MNFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalInteresMNFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecibirMNFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
	}
	if (numRegistrosMNFIDE!=0) {
		pdfDoc.setCell("Total de Documentos Moneda Nacional FIDEICOMISO: ","formas",ComunesPDF.RIGHT,5,2);
		pdfDoc.setCell(" "+numRegistrosMNFIDE,"formas",ComunesPDF.LEFT,11,2);
	}
	if (numRegistrosDLFIDE!=0) {	
		pdfDoc.setCell("Total Dolares FIDEICOMISO:","formas",ComunesPDF.RIGHT,5,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDLFIDE,2),"formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecGarDLFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDescuentoDLFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalInteres2DLFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecibir2DLFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalInteresDLFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecibirDLFIDE,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
	} 
	
	if (numRegistrosDLFIDE!=0) {
		pdfDoc.setCell("Total de Documentos Dolares FIDEICOMISO:","formas",ComunesPDF.RIGHT,5,2);
		pdfDoc.setCell(" "+numRegistrosDLFIDE,"formas",ComunesPDF.LEFT,11,2);		
	}
	
	if (numRegistrosMN!=0) {
		pdfDoc.setCell("Total Moneda Nacional","formas",ComunesPDF.RIGHT,5,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalMN,2),"formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecGarMN,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDescuentoMN,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalInteresMN,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecibirMN,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
	}
	if (numRegistrosMN!=0) {
		pdfDoc.setCell("Total de Documentos Moneda Nacional: ","formas",ComunesPDF.RIGHT,5,2);
		pdfDoc.setCell(" "+numRegistrosMN,"formas",ComunesPDF.LEFT,11,2);
	}
	if (numRegistrosDL!=0) {	
		pdfDoc.setCell("Total Dolares:","formas",ComunesPDF.RIGHT,5,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDL,2),"formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecGarDL,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDescuentoDL,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalInteresDL,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell("$"+Comunes.formatoDecimal(importeTotalRecibirDL,2),"formas",ComunesPDF.RIGHT,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
		pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,2);
	} 
	
	if (numRegistrosDL!=0) {
		pdfDoc.setCell("Total de Documentos Dolares:","formas",ComunesPDF.RIGHT,5,2);
		pdfDoc.setCell(" "+numRegistrosDL,"formas",ComunesPDF.LEFT,11,2);		
	}

	if (count == 0){
		pdfDoc.setCell("No existen registros","formas",ComunesPDF.CENTER,4);
	}
	pdfDoc.addTable();
	pdfDoc.endDocument();

	log.debug("strDirecVirtualTemp+nombreArchivo == "+strDirecVirtualTemp+nombreArchivo);
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	e.printStackTrace();
	jsonObj.put("success", new Boolean(false));
	jsonObj.put("msg", "Error al generar el archivo");
}
%>

<%=jsonObj%>