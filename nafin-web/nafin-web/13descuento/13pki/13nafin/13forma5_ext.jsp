<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*,
		com.netro.cadenas.* " contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>

<%

%>

<% 
request.setAttribute("producto","1");
request.setAttribute("facultad","/nafin/13descuento/13pki/13nafin/13forma5.jsp?sOrigenDscto=OperTel");
request.setAttribute("cveFacultad","13NAFIN13FORMA5FWD");
//String strPerfil = (String)session.getAttribute("sesPerfil");
String pagina = (strPerfil.equals("NAFIN OPERADOR") || strPerfil.equals("NAFIN CON"))?"/nafin/00utils/00buscapyme_operador_ext.js":"/nafin/00utils/00buscapyme_ext.js";
String ic_pyme = request.getParameter("ic_pyme")==null?"":request.getParameter("ic_pyme");
String strNombrePymeAsigna = request.getParameter("strNombrePymeAsigna")==null?"":request.getParameter("strNombrePymeAsigna");
String strNePymeAsigna = request.getParameter("strNePymeAsigna")==null?"":request.getParameter("strNePymeAsigna");
String hidProducto = request.getParameter("hidProducto")==null?"":request.getParameter("hidProducto");
String sOrigenDscto = request.getParameter("sOrigenDscto")==null?"":request.getParameter("sOrigenDscto");
String strUsr = request.getParameter("strUsr")==null?"":request.getParameter("strUsr");
String strNombreUsr = request.getParameter("strNombreUsr")==null?"":request.getParameter("strNombreUsr");
String strCorreoUsr = request.getParameter("strCorreoUsr")==null?"":request.getParameter("strCorreoUsr");

BusquedaPymeGral busqPymeGral = new BusquedaPymeGral();
List lstPymes = busqPymeGral.getProveedores("", strNePymeAsigna, "", "", "", hidProducto, "", "");	
if(lstPymes.size()>0)  {
	HashMap reg = (HashMap)lstPymes.get(0);
	strNombrePymeAsigna =reg.get("nombrePyme").toString();     
}

session.setAttribute("strNombrePymeAsigna",strNombrePymeAsigna ); 
session.setAttribute("strNePymeAsigna",strNePymeAsigna);
session.setAttribute("hidProducto",hidProducto);
session.setAttribute("ssOrigenDscto",sOrigenDscto);

%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>
<%-- El jsp que contiene el dise�o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important; 
		-khtml-user-select: text!important;
	}
</style>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/13descuento/13forma5_ext.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
<%}%>
			<div id="areaContenido"><div style="height:190px"></div>
<%if(esEsquemaExtJS){%>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>
<!-- Valores iniciales recibidos como parametros-->
<input type="hidden" name="ic_pyme" id="ic_pyme" value="<%=ic_pyme%>">
<input type="hidden" name="strNePymeAsigna" id="strNePymeAsigna" value="<%=strNePymeAsigna%>">
<input type="hidden" name="strNombrePymeAsigna" id="strNombrePymeAsigna" value="<%=strNombrePymeAsigna%>">

<input type="hidden" name="strUsr" id="strUsr" value="<%=strUsr%>">
<input type="hidden" name="strNombreUsr" id="strNombreUsr" value="<%=strNombreUsr%>">
<input type="hidden" name="strCorreoUsr" id="strCorreoUsr" value="<%=strCorreoUsr%>">

</body> 
</html>