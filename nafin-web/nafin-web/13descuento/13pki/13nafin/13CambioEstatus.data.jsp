<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.text.*,
		org.apache.commons.logging.Log,
		netropology.utilerias.*,
		com.netro.descuento.*, java.math.*,
		com.netro.model.catalogos.CatIFconEstatus24_b,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

log.debug("informacion = <"+informacion+">");

if (informacion.equals("CatalogoIf")) {

	CatIFconEstatus24_b catalogo = new CatIFconEstatus24_b();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setSeleccion("24");
	infoRegresar = catalogo.getJSONElementos();

}else if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion

	JSONObject jsonObj = new JSONObject();
	IMantenimiento BeanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);

	String ic_if				= (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
	String ig_numero_docto	= (request.getParameter("ig_numero_docto") == null)?"":request.getParameter("ig_numero_docto");

	int numRegistrosMN = 0, numRegistrosDL = 0;
	BigDecimal montoTotalMN = new BigDecimal("0.00");
	BigDecimal montoTotalDL = new BigDecimal("0.00");
	BigDecimal montoTotalDescuentoMN	= new BigDecimal("0.00");
	BigDecimal montoTotalDescuentoDL	= new BigDecimal("0.00");
	BigDecimal importeTotalInteresMN	= new BigDecimal("0.00");
	BigDecimal importeTotalInteresDL	= new BigDecimal("0.00");
	BigDecimal importeTotalRecibirMN	= new BigDecimal("0.00");
	BigDecimal importeTotalRecibirDL	= new BigDecimal("0.00");
	BigDecimal importeTotalRecGarMN	= new BigDecimal("0.00");
	BigDecimal importeTotalRecGarDL	= new BigDecimal("0.00");

	List regs = new ArrayList();
	List registros = new ArrayList();
	registros	=	BeanMantenimiento.ConsulCambioEstatusNaf(ic_if, ig_numero_docto);

	if (registros.size()>0) {

		for(int i = 0; i< registros.size(); i++){
			List datos = (List)registros.get(i);
			String nombreEpo			= (String)datos.get(0);
			String noDocumento		= (String)datos.get(1);
			String fechaVen			= (String)datos.get(2);
			String estatus				= (String)datos.get(3);
			String moneda				= (String)datos.get(4);
			String monto				= (datos.get(5).toString().equals(""))?"0":datos.get(5).toString();
			String porcentaje			= (String)datos.get(6);
			String montoDesc			= (datos.get(8).toString().equals(""))?"0":datos.get(8).toString();
			String montoInteres		= (datos.get(9).toString().equals(""))?"0":datos.get(9).toString();
			String montoOperar		= (datos.get(10).toString().equals(""))?"0":datos.get(10).toString();
			String noDocumentoFinal	= (String)datos.get(11);
			String ic_moneda			= (String)datos.get(12);
			String nombreIF			= (String)datos.get(13);
			String nombreIFFide		= (String)datos.get(14);
			String montoInteresFide = (String)datos.get(15);
			String montoOperarFide  = (String)datos.get(16);
			String operaFiso			= (String)datos.get(17);
			double recurso				= new Double(monto).doubleValue() - new Double(montoDesc).doubleValue();

			HashMap hash = new HashMap();
			hash.put("IC_MONEDA",			ic_moneda);
			hash.put("IC_DOCUMENTO",		noDocumentoFinal);
			hash.put("CG_RAZON_SOCIAL",	nombreEpo);
			hash.put("IG_NUMERO_DOCTO",	noDocumento);
			hash.put("DF_FECHA_VENC",		fechaVen);
			hash.put("CD_DESCRIPCION",		estatus);
			hash.put("CD_NOMBRE",			moneda);
			hash.put("FN_MONTO",				monto);
			hash.put("FN_PORC_ANTICIPO",	porcentaje);
			hash.put("RECURSO_GARANTIA",	String.valueOf(recurso));
			hash.put("FN_MONTO_DSCTO",		montoDesc);
			hash.put("IN_IMPORTE_INTERES",montoInteres);
			hash.put("IN_IMPORTE_RECIBIR",montoOperar);
			hash.put("FLAG_CHECK","");
			hash.put("NUEVO_ESTATUS",		"Selecionada Pyme");
			hash.put("NOMBRE_IF",	nombreIF);
			hash.put("NOMBRE_IF_FONDEO",		nombreIFFide);
			hash.put("IN_IMPORTE_INTERES_FONDEO",montoInteresFide);
			hash.put("IN_IMPORTE_RECIBIR_FONDEO",montoOperarFide);
			hash.put("CS_OPERA_FISO",operaFiso);
			String user = strLogin + " " + strNombreUsuario;
			hash.put("USUARIO",user);
			regs.add(hash);
		}

		if (regs.size()>0) {
			JSONArray jsObjArray = new JSONArray();
			jsObjArray = JSONArray.fromObject(regs);
			jsonObj.put("registros", jsObjArray);
		}
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Captura")) {

	JSONObject jsonObj		= new JSONObject();

	String noDocumento				= (request.getParameter("seleccionados") == null)?"":request.getParameter("seleccionados");
	String causas				= request.getParameter("ct_cambio_motivo");
	//	Variables de seguridad
	String pkcs7				= request.getParameter("Pkcs7");
	Calendar calendario		= Calendar.getInstance();
	SimpleDateFormat formatoCert = new SimpleDateFormat("ddMMyyyyHHmmss");
	String folioCert			= formatoCert.format(calendario.getTime());
	String estatus				= "3"; // Seleccionada Pyme
	String textoFirmado		= request.getParameter("TextoFirmado");
	char getReceipt			= 'Y';

	List datos			= new ArrayList();
	List registros		= new ArrayList();
	boolean success	= true;
	String msg			= "";
	
	try{				 

		datos.add(noDocumento);
		datos.add(causas);
		datos.add(_serial);
		datos.add(strNombreUsuario);
		datos.add(folioCert);
		datos.add(estatus);
		datos.add(pkcs7);
		datos.add(textoFirmado);

		IMantenimiento BeanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);

		String _acuse = BeanMantenimiento.getCambioEstatus(datos);
		jsonObj.put("_acuse", _acuse);
		
		if(!_acuse.equals("")){
			msg = "La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito.";
		}else{
			msg = " La Autentificaci&oacute;n NO se llevo acabo";
		}
		
	} catch(Exception e) {
		success = false;
		log.error(" CambioEstatus :: Exception"+e);
	}
	jsonObj.put("msg", msg);
	jsonObj.put("success", new Boolean(success));

	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>