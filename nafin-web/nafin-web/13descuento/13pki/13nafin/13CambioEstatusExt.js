Ext.onReady(function() {

	var param;
	var infoAcuse = {newData:null, acuse:null}

	function procesarSuccessFailureParametrizacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			param = Ext.util.JSON.decode(response.responseText);
			pnl.el.unmask();
			//fp.el.unmask();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureCaptura =  function(opts, success, response) {
		Ext.getCmp('btnAceptar').hide();
		Ext.getCmp('btnCancelar').hide();
		fpCausa2.hide();
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(!Ext.isEmpty(infoR._acuse)){

				infoAcuse.acuse = infoR._acuse;

				grid.setTitle('<div align="center">'+infoR.msg+' <br>Recibo: ' + infoR._acuse + '</div>');
				//Ext.MessageBox.alert('Cambios Guardados','<b>'+infoR.msg+'<br>Recibo: '+infoR._acuse+'</b>');
				Ext.getCmp('botonImprimirPDF').show();
				Ext.getCmp('btnSalir').show();

			}else{
			
				fpCausa.hide();
				grid.hide();
				if (Ext.getCmp('gridTotalesA').isVisible()){
					Ext.getCmp('gridTotalesA').hide();
				}
				var mensaje = infoR.msg;
				mensaje = (!mensaje)?'':Ext.util.Format.nl2br(mensaje);
				Ext.getCmp('tabla').hide();
				fpCausa2.show();
				Ext.getCmp('btnCancelar').setText("Salir");
				Ext.getCmp('btnCancelar').show();
				Ext.getCmp('panelError').body.update('<b>'+mensaje+'</b>');
				Ext.getCmp('panelError').show();
			}

		} else {
			fp.hide();
			fpCausa.hide();
			grid.hide();
			if (Ext.getCmp('gridTotalesA').isVisible()){
				Ext.getCmp('gridTotalesA').hide();
			}
			var jsonE = Ext.util.JSON.decode(response.responseText);
			var mensaje = jsonE.msg;
			mensaje = (!mensaje)?'':Ext.util.Format.nl2br(mensaje);
			Ext.getCmp('tabla').hide();
			fpCausa2.show();
			Ext.getCmp('btnCancelar').setText("Salir");
			Ext.getCmp('btnCancelar').show();
			Ext.getCmp('panelError').body.update('<b>'+mensaje+'</b>');
			Ext.getCmp('panelError').show();
			//NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaConsulta(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				consultaData.removeAll();
				if (!grid.isVisible()){
					grid.show();
				}
				if (infoR.registros != undefined){
					consultaData.loadData(infoR.registros);
					var cm = grid.getColumnModel();
					cm.setColumnHeader(cm.findColumnIndex('CD_DESCRIPCION'), 'Estatus');
					cm.setColumnTooltip(cm.findColumnIndex('CD_DESCRIPCION'), 'Estatus');
					cm.setHidden(cm.findColumnIndex('NUEVO_ESTATUS'), true);
					cm.setHidden(cm.findColumnIndex('FLAG_CHECK'), false);

					grid.getGridEl().unmask();
					if (!fpCausa.isVisible()){
						fpCausa.show();
					}

					resumenTotalesData.loadData(misTotales);
								resumenTotalesDataFISO.loadData(misTotales);
								var storeGrid = consultaData;
								var recurso = 0;
								var sumMN=0, montoTotalMN=0,montoTotalDescuentoMN=0,importeTotalInteresMN=0,importeTotalRecibirMN=0,importeTotalRecGarMN=0;
								var sumDL=0, montoTotalDL=0,montoTotalDescuentoDL=0,importeTotalInteresDL=0,importeTotalRecibirDL=0,importeTotalRecGarDL=0;
			
								storeGrid.each(function(registro){
										if(registro.get('CS_OPERA_FISO')=='N'){
											if (	Ext.isEmpty(registro.get('FN_MONTO_DSCTO'))	){
												registro.get('FN_MONTO_DSCTO') = 0;
											}
											if(registro.get('IC_MONEDA') == "1"){
												sumMN++;
												montoTotalMN				+=	registro.get('FN_MONTO');
												montoTotalDescuentoMN	+= registro.get('FN_MONTO_DSCTO');
												importeTotalInteresMN	+= registro.get('IN_IMPORTE_INTERES');
												importeTotalRecibirMN	+=	registro.get('IN_IMPORTE_RECIBIR');
												importeTotalRecGarMN		+=	registro.get('RECURSO_GARANTIA');
											}else if(registro.get('IC_MONEDA') == "54"){
												sumDL++;
												montoTotalDL				+=	registro.get('FN_MONTO');
												montoTotalDescuentoDL	+= registro.get('FN_MONTO_DSCTO');
												importeTotalInteresDL	+= registro.get('IN_IMPORTE_INTERES');
												importeTotalRecibirDL	+=	registro.get('IN_IMPORTE_RECIBIR');
												importeTotalRecGarDL		+=	registro.get('RECURSO_GARANTIA');
											}
									}
								});
								var regMN = resumenTotalesData.getAt(0);
								var regDL = resumenTotalesData.getAt(1);
								if (sumMN > 0){
									regMN.set('NOMMONEDA','MONEDA NACIONAL');
									regMN.set('TOTAL_REGISTROS',sumMN);
									regMN.set('TOTAL_MONTO_DOCUMENTOS',montoTotalMN);
									regMN.set('TOTAL_MONTO_RECGAR',importeTotalRecGarMN);
									regMN.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoMN);
									regMN.set('TOTAL_MONTO_INTERES',importeTotalInteresMN);
									regMN.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirMN);
								}
								if (sumDL > 0){
									regDL.set('NOMMONEDA','DOLARES AMERICANOS');
									regDL.set('TOTAL_REGISTROS',sumDL);
									regDL.set('TOTAL_MONTO_DOCUMENTOS',montoTotalDL);
									regDL.set('TOTAL_MONTO_RECGAR',importeTotalRecGarDL);
									regDL.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoDL);
									regDL.set('TOTAL_MONTO_INTERES',importeTotalInteresDL);
									regDL.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirDL);
								}
								var totalesACmp = Ext.getCmp('gridTotalesA');
								
								if(sumMN>0||sumDL>0){
									totalesACmp.setVisible(true);
								}else{
									totalesACmp.setVisible(false);		
								}
								
								var recurso = 0;
								var sumMN=0, montoTotalMN=0,montoTotalDescuentoMN=0,importeTotalInteresMN=0,importeTotalRecibirMN=0,importeTotalRecGarMN=0,importeTotalRecibirFisoMN=0,importeTotalIneteresFisoMN=0;
								var sumDL=0, montoTotalDL=0,montoTotalDescuentoDL=0,importeTotalInteresDL=0,importeTotalRecibirDL=0,importeTotalRecGarDL=0,importeTotalRecibirFisoDL=0,importeTotalIneteresFisoDL=0;
			
								storeGrid.each(function(registro){
										if(registro.get('CS_OPERA_FISO')=='S'){
											if (	Ext.isEmpty(registro.get('FN_MONTO_DSCTO'))	){
												registro.get('FN_MONTO_DSCTO') = 0;
											}
											if(registro.get('IC_MONEDA') == "1"){
												sumMN++;
												montoTotalMN				+=	registro.get('FN_MONTO');
												montoTotalDescuentoMN	+= registro.get('FN_MONTO_DSCTO');
												importeTotalInteresMN	+= registro.get('IN_IMPORTE_INTERES');
												importeTotalRecibirMN	+=	registro.get('IN_IMPORTE_RECIBIR');
												importeTotalRecGarMN		+=	registro.get('RECURSO_GARANTIA');
												importeTotalRecibirFisoMN +=parseFloat(registro.get('IN_IMPORTE_RECIBIR_FONDEO'));
												importeTotalIneteresFisoMN +=parseFloat(registro.get('IN_IMPORTE_INTERES_FONDEO'));
												
											}else if(registro.get('IC_MONEDA') == "54"){
												sumDL++;
												montoTotalDL				+=	registro.get('FN_MONTO');
												montoTotalDescuentoDL	+= registro.get('FN_MONTO_DSCTO');
												importeTotalInteresDL	+= registro.get('IN_IMPORTE_INTERES');
												importeTotalRecibirDL	+=	registro.get('IN_IMPORTE_RECIBIR');
												importeTotalRecGarDL		+=	registro.get('RECURSO_GARANTIA');
												importeTotalRecibirFisoDL +=parseFloat(registro.get('IN_IMPORTE_RECIBIR_FONDEO'));
												importeTotalIneteresFisoDL +=parseFloat(registro.get('IN_IMPORTE_INTERES_FONDEO'));
											}
									}
								});
								var regMN = resumenTotalesDataFISO.getAt(0);
								var regDL = resumenTotalesDataFISO.getAt(1);
								if (sumMN > 0){
									regMN.set('NOMMONEDA','MONEDA NACIONAL');
									regMN.set('TOTAL_REGISTROS',sumMN);
									regMN.set('TOTAL_MONTO_DOCUMENTOS',montoTotalMN);
									regMN.set('TOTAL_MONTO_RECGAR',importeTotalRecGarMN);
									regMN.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoMN);
									regMN.set('TOTAL_MONTO_INTERES',importeTotalInteresMN);
									regMN.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirMN);
									regMN.set('TOTAL_MONTO_INTERES_FISO',importeTotalIneteresFisoMN);
									regMN.set('TOTAL_MONTO_RECIBIR_FISO',importeTotalRecibirFisoMN);
								}
								if (sumDL > 0){
									regDL.set('NOMMONEDA','DOLARES AMERICANOS');
									regDL.set('TOTAL_REGISTROS',sumDL);
									regDL.set('TOTAL_MONTO_DOCUMENTOS',montoTotalDL);
									regDL.set('TOTAL_MONTO_RECGAR',importeTotalRecGarDL);
									regDL.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoDL);
									regDL.set('TOTAL_MONTO_INTERES',importeTotalInteresDL);
									regDL.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirDL);
									regDL.set('TOTAL_MONTO_INTERES_FISO',importeTotalIneteresFisoDL);
									regDL.set('TOTAL_MONTO_RECIBIR_FISO',importeTotalRecibirFisoDL);
									
								}
								if(sumMN>0||sumDL>0){
									gridTotalesAFISO.setVisible(true);
								}else{
									gridTotalesAFISO.setVisible(false);		
								}
					
					

				}else{
					grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesarArchivoSuccess(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
			var boton=Ext.getCmp('botonImprimirPDF');
			boton.setIconClass('');
			boton.enable();
			
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGeneraArchivoPDF =  function(opts, success, response) {
 
		var botonImprimirPDF = Ext.getCmp('botonImprimirPDF');
		botonImprimirPDF.enable();

		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {

			botonImprimirPDF.setIconClass('icoPdf');
			botonImprimirPDF.setText('Descargar PDF');

			botonImprimirPDF.urlArchivo = Ext.util.JSON.decode(response.responseText).urlArchivo;
			botonImprimirPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});

		} else {

			// Procesar respuesta
			if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
				Ext.Msg.alert(
					'Mensaje',
					response.msg,
					function(btn, text){
						botonImprimirPDF.setIconClass('icoPdf');
						botonImprimirPDF.setText('Generar PDF');
						NE.util.mostrarConnError(response,opts);
					}
				);
			} else {
				botonImprimirPDF.setIconClass('icoPdf');
				botonImprimirPDF.setText('Generar PDF');
				NE.util.mostrarConnError(response,opts);
			}
		}
	}

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'IG_NUMERO_DOCTO'},              //Num docto.
			{name: 'DF_FECHA_VENC'},
			{name: 'CD_DESCRIPCION'},
			{name: 'CD_NOMBRE'},                    //Moneda
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO'},
			{name: 'RECURSO_GARANTIA', type: 'float'},
			{name: 'FN_MONTO_DSCTO', type: 'float'},
			{name: 'IN_IMPORTE_INTERES', type: 'float'},
			{name: 'IN_IMPORTE_RECIBIR', type: 'float'},
			{name: 'FLAG_CHECK'},
			{name: 'NUEVO_ESTATUS'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_IF_FONDEO'},
			{name: 'IN_IMPORTE_INTERES_FONDEO'},
			{name: 'IN_IMPORTE_RECIBIR_FONDEO'},
			{name: 'CS_OPERA_FISO'}
			
			
			
		],
		data:[{
			'IC_MONEDA':'',	'IC_DOCUMENTO':'',	'CG_RAZON_SOCIAL':'',	'IG_NUMERO_DOCTO':'',	'DF_FECHA_VENC':'',	'CD_DESCRIPCION':'',	'CD_NOMBRE':'',
			'FN_MONTO':'',	'FN_PORC_ANTICIPO':'',	'FN_MONTO_DSCTO':'',	'IN_IMPORTE_INTERES':'',	'IN_IMPORTE_RECIBIR':'',	'FLAG_CHECK':'',
			'RECURSO_GARANTIA':'',	'NUEVO_ESTATUS':''	}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var misTotales = [
			{'MONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_DOCUMENTOS':'','TOTAL_MONTO_RECGAR':'','TOTAL_MONTO_DESCUENTO':'','TOTAL_MONTO_INTERES':'','TOTAL_MONTO_RECIBIR':'','TOTAL_MONTO_INTERES_FISO':'','TOTAL_MONTO_RECIBIR_FISO':''},
			{'MONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_DOCUMENTOS':'','TOTAL_MONTO_RECGAR':'','TOTAL_MONTO_DESCUENTO':'','TOTAL_MONTO_INTERES':'','TOTAL_MONTO_RECIBIR':'','TOTAL_MONTO_INTERES_FISO':'','TOTAL_MONTO_RECIBIR_FISO':''}
	];
	
	var resumenTotalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO_DOCUMENTOS'},
			{name: 'TOTAL_MONTO_RECGAR'},
			{name: 'TOTAL_MONTO_DESCUENTO'},
			{name: 'TOTAL_MONTO_INTERES'},
			{name: 'TOTAL_MONTO_RECIBIR'}
		],
		data: misTotales,
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var resumenTotalesDataFISO = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO_DOCUMENTOS'},
			{name: 'TOTAL_MONTO_RECGAR'},
			{name: 'TOTAL_MONTO_DESCUENTO'},
			{name: 'TOTAL_MONTO_INTERES'},
			{name: 'TOTAL_MONTO_RECIBIR'},
			{name: 'TOTAL_MONTO_INTERES_FISO'},
			{name: 'TOTAL_MONTO_RECIBIR_FISO'}
		],
		data: misTotales,
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});
	var procesarCatalogoIfData = function(store, arrRegistros, opts) {
		var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
		store.insert(	0,new reg({	clave: "",	descripcion: 'TODOS',	loadMsg: null	})	);
		Ext.getCmp('_ic_if').setValue("");
	}

	var catalogoIfData = new Ext.data.JsonStore({
		root:			'registros',
		fields:		['clave', 'descripcion', 'loadMsg'],
		url:			'13CambioEstatus.data.jsp',
		baseParams: {	informacion: 'CatalogoIf'	},
		totalProperty:'total',
		autoLoad:	false,
		listeners:	{
			load:procesarCatalogoIfData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var grupos2 = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: ' ', colspan: 5, align: 'center'},
					{header: 'Intermediario Financiero', colspan: 2, align: 'center'},
					{header: 'Fideicomiso', colspan: 2, align: 'center'}
				]
			]
	});
	var gridTotalesAFISO  = new Ext.grid.EditorGridPanel({ 
		xtype:	'grid',
		frame:	false,
		width:	940,
		plugins: grupos2,
		height:	110,
		title:	'Totales FIDEICOMISO',
		store:	resumenTotalesDataFISO,
		id:		'gridTotalesAFISO',
		hidden:	true,
		view:		new Ext.grid.GridView({markDirty: false, forceFit: true}),
		columns:[
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 190,	menuDisabled:true
			},{
				header: 'Registros',	tooltip:'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),	menuDisabled:true
			},{
				header: 'Monto Documentos',	tooltip:'Monto Documentos',	dataIndex: 'TOTAL_MONTO_DOCUMENTOS',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Recurso en Garant�a',	tooltip:'Monto Recurso <br>en Garant�a',	dataIndex: 'TOTAL_MONTO_RECGAR',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Descuento',	tooltip:'Monto Descuento',	dataIndex: 'TOTAL_MONTO_DESCUENTO',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Interes',	tooltip:'Monto Interes',	dataIndex: 'TOTAL_MONTO_INTERES',	width: 120,	align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Operar',	tooltip:'Monto Operar',	dataIndex: 'TOTAL_MONTO_RECIBIR',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Interes',	tooltip:'Monto Interes',	dataIndex: 'TOTAL_MONTO_INTERES_FISO',	width: 120,	align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Operar',	tooltip:'Monto Operar',	dataIndex: 'TOTAL_MONTO_RECIBIR_FISO',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			}
		],
		buttons:[
			{
				text:			'Generar PDF',
				id:			'botonImprimirPDF',
				iconCls:		'icoPdf',
				hidden:		true,
				urlArchivo: '',
				handler:		function(boton, event){

									if(Ext.isEmpty(boton.urlArchivo)){

										// Cambiar icono
										boton.disable();
										boton.setIconClass('loading-indicator');

										// Genera Archivo PDF
										Ext.Ajax.request({
											url: 			'13CambioEstatusExt_pdf.jsp',
											params: 		{
																jsonAceptados:	Ext.encode(infoAcuse.newData),
																acuse:			infoAcuse.acuse
															},
											callback: 	procesarArchivoSuccess
										});

									// Descargar el archivo generado	
									} else {
										
										var archivo = boton.urlArchivo;
										archivo = archivo.replace('/nafin','');
										var params = {nombreArchivo: archivo};
										fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
										fp.getForm().getEl().dom.submit();
										
									}
									
								}
			},{
				text:			'Salir',
				id:			'btnSalir',
				iconCls:		'icoLimpiar',
				hidden:		true,
				handler:		function(boton, event){
									window.location = '13CambioEstatusExt.jsp';
								}
			}
		]
		
	});
	
	var gridTotalesA = {
		xtype:	'grid',
		frame:	false,
		width:	940,
		height:	110,
		title:	'Totales Intermediario Financiero',
		store:	resumenTotalesData,
		id:		'gridTotalesA',
		hidden:	true,
		view:		new Ext.grid.GridView({markDirty: false, forceFit: true}),
		columns:[
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 190,	menuDisabled:true
			},{
				header: 'Registros',	tooltip:'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000'),	menuDisabled:true
			},{
				header: 'Monto Documentos',	tooltip:'Monto Documentos',	dataIndex: 'TOTAL_MONTO_DOCUMENTOS',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Recurso en Garant�a',	tooltip:'Monto Recurso <br>en Garant�a',	dataIndex: 'TOTAL_MONTO_RECGAR',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Descuento',	tooltip:'Monto Descuento',	dataIndex: 'TOTAL_MONTO_DESCUENTO',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Interes',	tooltip:'Monto Interes',	dataIndex: 'TOTAL_MONTO_INTERES',	width: 120,	align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			},{
				header: 'Monto Operar',	tooltip:'Monto Operar',	dataIndex: 'TOTAL_MONTO_RECIBIR',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),	menuDisabled:true
			}
		]
		/*,
		tools: [
			{
				id:		'close',
				handler:	function(evento, toolEl, panel, tc) {
								panel.hide();
							}
			}
		]*/
	};
	
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: ' ', colspan: 9, align: 'center'},
					{header: 'Intermediario Financiero', colspan: 3, align: 'center'},
					{header: 'Fideicomiso', colspan: 3, align: 'center'},
					{header: '', colspan: 1, align: 'center'}
				]
			]
	});

	var grid = new Ext.grid.EditorGridPanel({
		store:			consultaData,
		hidden:			true,
		plugins: 			grupos,
		header:			true,
		title:			undefined,
		clicksToEdit:	1,
		stripeRows:		true,
		loadMask:		true,
		height:			400,
		width:			940,
		frame:			false,
		view:				new Ext.grid.GridView({markDirty: false}),
		columns:[
			{
				header: '<div align="center">Nombre EPO</div>', tooltip: 'Nombre EPO',	dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true, width: 200, resizable: true, hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header: 'N�mero de <br>documento', tooltip: 'N�mero del documento',	dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true, width: 150, resizable: true, hidden: false,align: 'center'
			},{
				header : 'Fecha de <br>Vencimiento', tooltip: 'Fecha de Vencimiento',	dataIndex : 'DF_FECHA_VENC',
				sortable : true, width : 120, align: 'center',	hidden: false
			},{
				header : 'Estatus', tooltip: 'Estatus',	dataIndex : 'CD_DESCRIPCION',	// && Estatus Anterior
				sortable : true, width : 200, hidden: false,hideable: false, align: 'center'
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'CD_NOMBRE',
				sortable : true, width : 150, hidden: false,align: 'center'
			},{
				header : 'Monto', tooltip: 'Monto',	dataIndex : 'FN_MONTO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, hideable: true
			},{
				header : 'Porcentaje de <br>Descuento', tooltip: 'Porcentaje de Descuento',	dataIndex : 'FN_PORC_ANTICIPO',
				sortable : true, width : 110,	align: 'center', resizable: true, hidden: false,hideable: true,
				renderer: Ext.util.Format.numberRenderer('0%')
			},{
				header : 'Recurso en <br>garant�a', tooltip: 'Recurso en garant�a',	dataIndex : 'RECURSO_GARANTIA',
				sortable : true, width : 110, align: 'right', hidden: false, hideable: true,	renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},{
				header : 'Monto a <br>Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'FN_MONTO_DSCTO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, hideable: true
			},
			{
				header: '<div align="center">Nombre</div>', tooltip: 'Nombre',	dataIndex: 'NOMBRE_IF',
				sortable: true, width: 200, resizable: true, hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
							}
			},{
				header : 'Monto Interes', tooltip: 'Monto Interes',	dataIndex : 'IN_IMPORTE_INTERES',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, hideable: true
			},{
				header : 'Monto a Operar', tooltip: 'Monto a Operar',	dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, hideable: true
			},{
				header: '<div align="center">Nombre</div>', tooltip: 'Nombre',	dataIndex: 'NOMBRE_IF_FONDEO',
				sortable: true, width: 200, resizable: true, hidden: false,
				renderer:function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								if(value=='N/A'){
									return '<center>'+value+'</center>';
								}else{
									return value;
								}
							}
			},{
				header : 'Monto Interes', tooltip: 'Monto Interes',	dataIndex : 'IN_IMPORTE_INTERES_FONDEO',
				sortable : true, width : 110, align: 'right', renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value=='N/A'){
									return '<center>'+value+'</center>';
								}
								else{
									return Ext.util.Format.number(value, '$0,0.00')
								}
							}, hidden: false, hideable: true
			},{
				header : 'Monto a Operar', tooltip: 'Monto a Operar',	dataIndex : 'IN_IMPORTE_RECIBIR_FONDEO',
				sortable : true, width : 110, align: 'right', renderer:function(value, metadata, record, rowindex, colindex, store) {
								if(value=='N/A'){
									return '<center>'+value+'</center>';
								}
								else{
									return Ext.util.Format.number(value, '$0,0.00')
									}
								}, hidden: false, hideable: true
			},{
				xtype: 'checkcolumn',
				header : 'Cambiar a estatus <br>Seleccionado Pyme',	tooltip:	'Seleccionar',	dataIndex : 'FLAG_CHECK',
				width : 150, align:'center',	sortable : false, hideable: false
			},{	//	14
				header : 'Nuevo Estatus', tooltip: 'Nuevo Estatus',	dataIndex : 'NUEVO_ESTATUS',
				sortable : true, width : 150, hidden:true,	hideable: false, align: 'center'
			}
		]
	});

	var fnAceptarCambioEstatus = function(vpkcs7, vtextoFirmar, vseleccionados){
		if (Ext.isEmpty(vpkcs7) || vpkcs7 == 'error:noMatchingCert') {
			Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
			return;	//Error en la firma. Termina...
		}
		
		pnl.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url:		'13CambioEstatus.data.jsp',
			params:	{
				informacion:		'Captura',
				seleccionados:		vseleccionados,
				ct_cambio_motivo:	Ext.getCmp('ct_cambio_motivo').getValue(),
				Pkcs7:				vpkcs7,
				TextoFirmado:		vtextoFirmar
			},
			callback:				procesarSuccessFailureCaptura
		});
	}
	
	var fpCausa2 = new Ext.form.FormPanel({
		id: 'formaCausa2',	width: 702,	autoHeight: true,	style: ' margin:0 auto;',	hidden:true,	frame: false, border:false, renderHidden:true,
		items: [
			{
				xtype:'panel',	frame:false,	id:'tabla',	layout:'table',	layoutConfig:{ columns:2 },defaults: {frame:false,	autoHeight: true,bodyStyle:'padding:5px'},
				items: [
					{	width:200,	border:false,	html:'<div align="center">Motivo de cambio de estatus:</div>'	},
					{	width:500,	border:true,	id: 'disCausa',	html: '&nbsp;'	}
				]
			},{
				xtype:'panel',	frame:false, id:'panelError', layout:'fit', border:true, autoHeight: true,	bodyStyle:'padding:5px',hidden:true,
				html:	'&nbsp'
			}
		],
		buttons: [
			{
				text:		'Aceptar',
				id:		'btnAceptar',
				iconCls:	'icoAceptar',
				handler:	function(){
								var seleccionados = "";
								var sumMN=0, montoTotalMN=0,montoTotalDescuentoMN=0,importeTotalInteresMN=0,importeTotalRecibirMN=0,importeTotalRecGarMN=0;
								var sumDL=0, montoTotalDL=0,montoTotalDescuentoDL=0,importeTotalInteresDL=0,importeTotalRecibirDL=0,importeTotalRecGarDL=0;
								textoFirmar =	"Nombre EPO|Numero de Documento|Fecha de Vencimiento|Estatus Anterior|Moneda|Monto|"+
													"Monto a Descontar|Monto Interes|Monto a operar|Nuevo Estatus|Porcentaje Anticipo|Recurso Garantia|\n";

								var jsonData = consultaData.data.items;
								var coma = 0;
								Ext.each(jsonData, function(item,index,arrItem){

										if(coma == 0){
											seleccionados += item.data.IC_DOCUMENTO;
										}else{
											seleccionados += "," + item.data.IC_DOCUMENTO;
										}
										coma++;

										textoFirmar +=	item.data.CG_RAZON_SOCIAL+"|"+item.data.IG_NUMERO_DOCTO+"|"+item.data.DF_FECHA_VENC+
										"|"+item.data.CD_DESCRIPCION+"|"+item.data.CD_NOMBRE+"|"+Ext.util.Format.number(item.data.FN_MONTO, '$0,0.00')+
										"|"+Ext.util.Format.number(item.data.FN_MONTO_DSCTO,		'$0,0.00')+
										"|"+Ext.util.Format.number(item.data.IN_IMPORTE_INTERES, '$0,0.00')+
										"|"+Ext.util.Format.number(item.data.IN_IMPORTE_RECIBIR, '$0,0.00')+
										"|"+item.data.NUEVO_ESTATUS+//nuevo_estatus
										"|"+Ext.util.Format.number(item.data.FN_PORC_ANTICIPO,	'0%')+
										"|"+Ext.util.Format.number(item.data.RECURSO_GARANTIA,	'$0,0.00')+"\n";

										if(item.data.IC_MONEDA == "1"){
											sumMN++;
											montoTotalMN				+=	item.data.FN_MONTO;
											montoTotalDescuentoMN	+= item.data.FN_MONTO_DSCTO;
											importeTotalInteresMN	+= item.data.IN_IMPORTE_INTERES;
											importeTotalRecibirMN	+=	item.data.IN_IMPORTE_RECIBIR;
											importeTotalRecGarMN		+=	item.data.RECURSO_GARANTIA;

										}else if(item.data.IC_MONEDA == "54"){
											sumDL++;
											montoTotalDL				+=	item.data.FN_MONTO;
											montoTotalDescuentoDL	+= item.data.FN_MONTO_DSCTO;
											importeTotalInteresDL	+= item.data.IN_IMPORTE_INTERES;
											importeTotalRecibirDL	+=	item.data.IN_IMPORTE_RECIBIR;
											importeTotalRecGarDL		+=	item.data.RECURSO_GARANTIA;

										}
								});

								if (sumMN > 0){
									textoFirmar += "\nMonto Total MN: "+					Ext.util.Format.number(montoTotalMN,			'$0,0.00');
									textoFirmar += "\nRecurso en Garantia Total MN: "+	Ext.util.Format.number(importeTotalRecGarMN,	'$0,0.00');
									textoFirmar += "\nMonto a Descontar Total MN: "+	Ext.util.Format.number(montoTotalDescuentoMN,'$0,0.00');
									textoFirmar += "\nMonto Interes Total MN: "+			Ext.util.Format.number(importeTotalInteresMN,'$0,0.00');
									textoFirmar += "\nMonto a Operar Total MN: "+		Ext.util.Format.number(importeTotalRecibirMN,'$0,0.00');
								}
								if (sumDL > 0){
									textoFirmar += "\nMonto Total Dolares: "+					Ext.util.Format.number(montoTotalDL,			'$0,0.00');
									textoFirmar += "\nRecurso en Garantia Total Dolares: "+Ext.util.Format.number(importeTotalRecGarDL,'$0,0.00');
									textoFirmar += "\nMonto a Descontar Total Dolares: "+	Ext.util.Format.number(montoTotalDescuentoDL,'$0,0.00');
									textoFirmar += "\nMonto Interes Total Dolares: "+		Ext.util.Format.number(importeTotalInteresDL,'$0,0.00');
									textoFirmar += "\nMonto a Operar Total Dolares: "+		Ext.util.Format.number(importeTotalRecibirDL,'$0,0.00');
								}

								
								NE.util.obtenerPKCS7(fnAceptarCambioEstatus, textoFirmar, seleccionados);
								/*var pkcs7 = NE.util.firmar(textoFirmar);
								if (Ext.isEmpty(pkcs7) || pkcs7 == 'error:noMatchingCert') {
									Ext.Msg.alert('Firmar','Error en el proceso de Firmado.\nNo se puede continuar.');
									return;	//Error en la firma. Termina...
								}
								pnl.el.mask('Procesando...', 'x-mask-loading');
								Ext.Ajax.request({
									url:		'13CambioEstatus.data.jsp',
									params:	{
										informacion:		'Captura',
										seleccionados:		seleccionados,
										ct_cambio_motivo:	Ext.getCmp('ct_cambio_motivo').getValue(),
										Pkcs7:				pkcs7,
										TextoFirmado:		textoFirmar
									},
									callback:				procesarSuccessFailureCaptura
								});*/
				}
			},{
				text:'Cancelar',
				id:	'btnCancelar',
				iconCls:'icoRechazar',
				handler: function() {
					window.location = '13CambioEstatusExt.jsp';
				}
			}
		]
	});

	var fpCausa = new Ext.form.FormPanel({
		id:			'formaCausa',
		width:		700,
		style:		'margin:0 auto;',
		hidden:		true,
		frame:		true,
		renderHidden:true,
		bodyStyle:	'padding: 6px',
		labelWidth:	50,
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype:				'textarea',
				id:					'ct_cambio_motivo',
				name:					'ct_cambio_motivo',
				height:				60,
				width:				300,
				maxLength:			260,
				fieldLabel:			'Causa',
				allowBlank:			false,
				enableKeyEvents:	true,
				listeners:{
					'keydown':	function(txtA){
									if (	!Ext.isEmpty(txtA.getValue())	){
										var numero = (txtA.getValue().length);
										Ext.getCmp('contador').setValue(260-numero);
										if (	Ext.getCmp('contador').getValue() < 0	) {
											var cadena = (txtA.getValue()).substring(0,260);
											txtA.setValue(cadena);
											Ext.getCmp('contador').setValue(0);
										}
									}
								},
					'keyup':	function(txtA){
									if (	Ext.isEmpty(txtA.getValue())	){
										Ext.getCmp('contador').reset();
										fpCausa.getForm().reset();
									}
								},
					'blur':	function(txtA){
									if (	Ext.isEmpty(txtA.getValue())	){
										fpCausa.getForm().reset();
									}									
								}
				}
			},{
				xtype:			'compositefield',
				combineErrors:	false,
				msgTarget:		'side',
				items:[
					{
						xtype:'displayfield',	id:'disEspacio',	width: 1,	text:''
					},{
						xtype:'displayfield',	id:'disChar',	width: 120,	value:'Caracteres Restantes:'
					},{
						xtype: 'numberfield',	maxLength:3,	value:	260,	width: 30,	id:	'contador',	name:	'contador',	readOnly: true
					}
				]
			}
		],
		monitorValid: true,
		buttons: [
			{
				text:		'Cambiar Estatus',
				id:		'btnCambia',
				iconCls:	'icoAceptar',
				formBind:true,
				handler:	function(boton, evento) {
								grupos.config.rows[0][0].colspan=7;
								var flag = false;
								var jsonData = consultaData.data.items;
								Ext.each(jsonData, function(item,index,arrItem){
									if (item.data.FLAG_CHECK){
										flag = true;
										return false;
									}
								});
								if (!flag){
									Ext.Msg.alert(boton.text,'Para poder cambiar el estatus debe seleccionar por lo menos un documento');
									return;
								}
								boton.setIconClass('loading-indicator');
								Ext.getCmp('disCausa').body.update('');
								consultaData.commitChanges();
								infoAcuse.newData = [];
								Ext.each(jsonData, function(item,index,arrItem){
									if (item.data.FLAG_CHECK){
											infoAcuse.newData.push(item.data);
									}
								});
								if (infoAcuse.newData.length > 0) {
									consultaData.loadData(infoAcuse.newData);
								}
								var cm = grid.getColumnModel();
								cm.setColumnHeader(cm.findColumnIndex('CD_DESCRIPCION'), 'Estatus Anterior');
								cm.setColumnTooltip(cm.findColumnIndex('CD_DESCRIPCION'), 'Estatus Anterior');
								cm.setHidden(cm.findColumnIndex('FLAG_CHECK'), true);
								cm.setHidden(cm.findColumnIndex('NUEVO_ESTATUS'), false);
								
								fp.hide();
								Ext.getCmp('disCausa').body.update(Ext.getCmp('ct_cambio_motivo').getValue());
								fpCausa.hide();
								fpCausa2.show();

								resumenTotalesData.loadData(misTotales);
								resumenTotalesDataFISO.loadData(misTotales);
								var storeGrid = consultaData;
								var recurso = 0;
								var sumMN=0, montoTotalMN=0,montoTotalDescuentoMN=0,importeTotalInteresMN=0,importeTotalRecibirMN=0,importeTotalRecGarMN=0;
								var sumDL=0, montoTotalDL=0,montoTotalDescuentoDL=0,importeTotalInteresDL=0,importeTotalRecibirDL=0,importeTotalRecGarDL=0;
			
								storeGrid.each(function(registro){
										if(registro.get('CS_OPERA_FISO')=='N'){
											if (	Ext.isEmpty(registro.get('FN_MONTO_DSCTO'))	){
												registro.get('FN_MONTO_DSCTO') = 0;
											}
											if(registro.get('IC_MONEDA') == "1"){
												sumMN++;
												montoTotalMN				+=	registro.get('FN_MONTO');
												montoTotalDescuentoMN	+= registro.get('FN_MONTO_DSCTO');
												importeTotalInteresMN	+= registro.get('IN_IMPORTE_INTERES');
												importeTotalRecibirMN	+=	registro.get('IN_IMPORTE_RECIBIR');
												importeTotalRecGarMN		+=	registro.get('RECURSO_GARANTIA');
											}else if(registro.get('IC_MONEDA') == "54"){
												sumDL++;
												montoTotalDL				+=	registro.get('FN_MONTO');
												montoTotalDescuentoDL	+= registro.get('FN_MONTO_DSCTO');
												importeTotalInteresDL	+= registro.get('IN_IMPORTE_INTERES');
												importeTotalRecibirDL	+=	registro.get('IN_IMPORTE_RECIBIR');
												importeTotalRecGarDL		+=	registro.get('RECURSO_GARANTIA');
											}
									}
								});
								var regMN = resumenTotalesData.getAt(0);
								var regDL = resumenTotalesData.getAt(1);
								if (sumMN > 0){
									regMN.set('NOMMONEDA','MONEDA NACIONAL');
									regMN.set('TOTAL_REGISTROS',sumMN);
									regMN.set('TOTAL_MONTO_DOCUMENTOS',montoTotalMN);
									regMN.set('TOTAL_MONTO_RECGAR',importeTotalRecGarMN);
									regMN.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoMN);
									regMN.set('TOTAL_MONTO_INTERES',importeTotalInteresMN);
									regMN.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirMN);
								}
								if (sumDL > 0){
									regDL.set('NOMMONEDA','DOLARES AMERICANOS');
									regDL.set('TOTAL_REGISTROS',sumDL);
									regDL.set('TOTAL_MONTO_DOCUMENTOS',montoTotalDL);
									regDL.set('TOTAL_MONTO_RECGAR',importeTotalRecGarDL);
									regDL.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoDL);
									regDL.set('TOTAL_MONTO_INTERES',importeTotalInteresDL);
									regDL.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirDL);
								}
								var totalesACmp = Ext.getCmp('gridTotalesA');
								
								if(sumMN>0||sumDL>0){
									totalesACmp.setVisible(true);
								}else{
									totalesACmp.setVisible(false);		
								}
								
								var recurso = 0;
								var sumMN=0, montoTotalMN=0,montoTotalDescuentoMN=0,importeTotalInteresMN=0,importeTotalRecibirMN=0,importeTotalRecGarMN=0,importeTotalRecibirFisoMN=0,importeTotalIneteresFisoMN=0;
								var sumDL=0, montoTotalDL=0,montoTotalDescuentoDL=0,importeTotalInteresDL=0,importeTotalRecibirDL=0,importeTotalRecGarDL=0,importeTotalRecibirFisoDL=0,importeTotalIneteresFisoDL=0;
			
								storeGrid.each(function(registro){
										if(registro.get('CS_OPERA_FISO')=='S'){
											if (	Ext.isEmpty(registro.get('FN_MONTO_DSCTO'))	){
												registro.get('FN_MONTO_DSCTO') = 0;
											}
											if(registro.get('IC_MONEDA') == "1"){
												sumMN++;
												montoTotalMN				+=	registro.get('FN_MONTO');
												montoTotalDescuentoMN	+= registro.get('FN_MONTO_DSCTO');
												importeTotalInteresMN	+= registro.get('IN_IMPORTE_INTERES');
												importeTotalRecibirMN	+=	registro.get('IN_IMPORTE_RECIBIR');
												importeTotalRecGarMN		+=	registro.get('RECURSO_GARANTIA');
												importeTotalRecibirFisoMN +=parseFloat(registro.get('IN_IMPORTE_RECIBIR_FONDEO'));
												importeTotalIneteresFisoMN +=parseFloat(registro.get('IN_IMPORTE_INTERES_FONDEO'));
												
											}else if(registro.get('IC_MONEDA') == "54"){
												sumDL++;
												montoTotalDL				+=	registro.get('FN_MONTO');
												montoTotalDescuentoDL	+= registro.get('FN_MONTO_DSCTO');
												importeTotalInteresDL	+= registro.get('IN_IMPORTE_INTERES');
												importeTotalRecibirDL	+=	registro.get('IN_IMPORTE_RECIBIR');
												importeTotalRecGarDL		+=	registro.get('RECURSO_GARANTIA');
												importeTotalRecibirFisoDL +=parseFloat(registro.get('IN_IMPORTE_RECIBIR_FONDEO'));
												importeTotalIneteresFisoDL +=parseFloat(registro.get('IN_IMPORTE_INTERES_FONDEO'));
											}
									}
								});
								var regMN = resumenTotalesDataFISO.getAt(0);
								var regDL = resumenTotalesDataFISO.getAt(1);
								if (sumMN > 0){
									regMN.set('NOMMONEDA','MONEDA NACIONAL');
									regMN.set('TOTAL_REGISTROS',sumMN);
									regMN.set('TOTAL_MONTO_DOCUMENTOS',montoTotalMN);
									regMN.set('TOTAL_MONTO_RECGAR',importeTotalRecGarMN);
									regMN.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoMN);
									regMN.set('TOTAL_MONTO_INTERES',importeTotalInteresMN);
									regMN.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirMN);
									regMN.set('TOTAL_MONTO_INTERES_FISO',importeTotalIneteresFisoMN);
									regMN.set('TOTAL_MONTO_RECIBIR_FISO',importeTotalRecibirFisoMN);
								}
								if (sumDL > 0){
									regDL.set('NOMMONEDA','DOLARES AMERICANOS');
									regDL.set('TOTAL_REGISTROS',sumDL);
									regDL.set('TOTAL_MONTO_DOCUMENTOS',montoTotalDL);
									regDL.set('TOTAL_MONTO_RECGAR',importeTotalRecGarDL);
									regDL.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoDL);
									regDL.set('TOTAL_MONTO_INTERES',importeTotalInteresDL);
									regDL.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirDL);
									regDL.set('TOTAL_MONTO_INTERES_FISO',importeTotalIneteresFisoDL);
									regDL.set('TOTAL_MONTO_RECIBIR_FISO',importeTotalRecibirFisoDL);
									
									
								}
								if(sumMN>0||sumDL>0){
									gridTotalesAFISO.setVisible(true);
								}else{
									gridTotalesAFISO.setVisible(false);		
								}

				} //fin handler
			}
		]
	});

	var elementosForma = [
		{
			xtype:			'combo',
			id:				'_ic_if',
			name:				'ic_if',
			hiddenName:		'ic_if',
			fieldLabel:		'Nombre del IF',
			emptyText:		'Seleccionar IF. . .',
			mode:				'local',
			displayField:	'descripcion',
			valueField:		'clave',
			forceSelection:false,
			triggerAction:	'all',
			typeAhead:		true,
			minChars:		1,
			store:			catalogoIfData,
			tpl:				NE.util.templateMensajeCargaCombo
		},{
			xtype:			'textfield',
			name:				'ig_numero_docto',
			id:				'_ig_numero_docto',
			allowBlank:		true,
			fieldLabel:		'N�mero de documento',
			anchor:			'50%',
			maxLength:		15
		}
	];

	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			700,
		style:			'margin:0 auto;',
		collapsible:	true,
		titleCollapse:	true,
		frame:			true,
		bodyStyle:		'padding: 6px',
		labelWidth:		150,
		defaults:{
			msgTarget: 'side',
			anchor: '-20'
		},
		items:			elementosForma,
		monitorValid:	true,
		buttons:[
			{
				text:		'Consultar',
				id:		'btnConsultar',
				iconCls:	'icoBuscar',
				formBind:true,
				handler:	function(boton, evento) {
					fpCausa.hide();
					fpCausa.getForm().reset();
					grid.hide();
					Ext.getCmp('btnCambia').setIconClass('icoAceptar');
					if (Ext.getCmp('gridTotalesA').isVisible()){
						Ext.getCmp('gridTotalesA').hide();
					}

					fp.el.mask('Procesando...', 'x-mask-loading');
					Ext.Ajax.request({
						url:		'13CambioEstatus.data.jsp',
						params:	Ext.apply(fp.getForm().getValues(),{informacion: 'Consulta'}),
						callback:procesaConsulta
					});
				} //fin handler
			},{
				text:		'Limpiar',
				iconCls:	'icoLimpiar',
				handler:	function() {
					window.location = '13CambioEstatusExt.jsp';	
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id:		'contenedorPrincipal',
		applyTo:	'areaContenido',
		width:	940,
		renderHidden:	true,
		height:	'auto',
		items:	[
			fp,	NE.util.getEspaciador(10),
			grid,	gridTotalesA,gridTotalesAFISO,	NE.util.getEspaciador(10),
			fpCausa,fpCausa2
		]
	});

	catalogoIfData.load();

});
