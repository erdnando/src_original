<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.math.*, 	
	netropology.utilerias.*,	
	com.netro.afiliacion.*,
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	com.netro.descuento.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String pantalla = (request.getParameter("pantalla") != null) ? request.getParameter("pantalla") : "";
String ic_epo = (request.getParameter("ic_epo") != null) ? request.getParameter("ic_epo") : "";
String consulta = (request.getParameter("consulta") != null) ? request.getParameter("consulta") : "";
String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
String num_electronico = (request.getParameter("num_electronico") != null) ? request.getParameter("num_electronico") : "";
String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
String fechaIni = (request.getParameter("fechaIni") != null) ? request.getParameter("fechaIni") : "";
String fechaFin = (request.getParameter("fechaFin") != null) ? request.getParameter("fechaFin") : "";
String ic_if = (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") : "";
String   infoRegresar ="", txtNombre="", estatus ="11";
if (strTipoUsuario.equals("IF")) { 
 ic_if =iNoCliente;
}
HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

if (informacion.equals("valoresIniciales")  ) {

	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("fechaHoy", fechaHoy);
	jsonObj.put("strTipoUsuario", strTipoUsuario);
	infoRegresar = jsonObj.toString();	
	
} else if (informacion.equals("catalogoEPO")  ) {

	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");		
	catalogo.setClave_if(ic_if);
	catalogo.setOrden("ce.cg_razon_social");	
	
	jsonObj = JSONObject.fromObject(catalogo.getJSONElementos());	
	jsonObj.put("ic_epo", ic_epo);
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("catalogoIF")  ) {
	LiberacionLimitesIF paginador = new LiberacionLimitesIF();
	List catEPO = paginador.getCatalogoIF(ic_epo);
	jsonObj.put("registros", catEPO);
	jsonObj.put("success",  new Boolean(true));
   infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("busquedaAvanzada") ) {

	String rfc_pyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
	String nombre_pyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String num_pyme = request.getParameter("noPyme")==null?"":request.getParameter("noPyme");
	
	CatalogoPymeBusqAvanzadaIF cat = new CatalogoPymeBusqAvanzadaIF();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion("crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");
	cat.setIc_epo(ic_epo);
	cat.setRfc_pyme(rfc_pyme);
	cat.setNombre_pyme(nombre_pyme);	
	cat.setNum_pyme(num_pyme);	
	cat.setIc_pyme("");	
	cat.setOrden("p.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}  else if (informacion.equals("pymeNombre")  ) {
	
	List datosPymes =  BeanParamDscto.datosPymes(num_electronico, ic_epo ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	jsonObj.put("ic_pyme", ic_pyme);
	infoRegresar = jsonObj.toString();		


} else if (informacion.equals("Consultar")    ||  informacion.equals("ArchivoCSV") )  {
	
	LiberacionLimitesIF paginador = new LiberacionLimitesIF();
			
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );
	
	paginador.setFechaIni(fechaIni);
	paginador.setFechaFin(fechaFin);	
	paginador.setClaveEPO(ic_epo);
	paginador.setClaveIF(ic_if);
	paginador.setClavePyme(ic_pyme);
	paginador.setFechaHoy(fechaHoy);
	paginador.setTipoUsuario(strTipoUsuario);	
	paginador.setEstatus(estatus); 
	paginador.setPantalla("Consulta");
	
	if(informacion.equals("Consultar")){ 
	
		try {
			Registros reg	=	queryHelper.doSearch();
			consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}				
	
		jsonObj = JSONObject.fromObject(consulta);	
		jsonObj.put("strTipoUsuario", strTipoUsuario);
		infoRegresar = jsonObj.toString();	
	}
	
	if(informacion.equals("ArchivoCSV")){ 
	
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "csv");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}			
	}
	
	
}

%>
<%=infoRegresar%>