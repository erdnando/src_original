<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	java.sql.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	com.netro.descuento.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String epo = (request.getParameter("epo")!=null)?request.getParameter("epo"):"";
String noElectronico = (request.getParameter("noElectronico")!=null)?request.getParameter("noElectronico"):"";
String noProveedor = (request.getParameter("noProveedor")!=null)?request.getParameter("noProveedor"):"";
String noDocto = (request.getParameter("noDocto")!=null)?request.getParameter("noDocto"):"";
String rfc = (request.getParameter("rfc")!=null)?request.getParameter("rfc"):"";
String ic_pyme = (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
String tipoPyme = (request.getParameter("tipoPyme")!=null)?request.getParameter("tipoPyme"):"";
String doctos = (request.getParameter("doctos") == null)?"":request.getParameter("doctos");
String causaReasignacion = (request.getParameter("causaReasignacion") == null)?"":request.getParameter("causaReasignacion");
String contadormn = (request.getParameter("contadormn") == null)?"":request.getParameter("contadormn");
String contadord = (request.getParameter("contadord") == null)?"":request.getParameter("contadord");
String contadormontomn = (request.getParameter("contadormontomn") == null)?"":request.getParameter("contadormontomn");
String contadormontod = (request.getParameter("contadormontod") == null)?"":request.getParameter("contadormontod");
String acuse = (request.getParameter("acuse") == null)?"":request.getParameter("acuse");
String fechaAcuse = (request.getParameter("fechaAcuse") == null)?"":request.getParameter("fechaAcuse");
String horaAcuse = (request.getParameter("horaAcuse") == null)?"":request.getParameter("horaAcuse");
String Fecha=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
String Hora=(new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
String externContent = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
String icPymeo = (request.getParameter("icPymeo") == null)?"":request.getParameter("icPymeo");
String icPymed = (request.getParameter("icPymed") == null)?"":request.getParameter("icPymed");
String nPymeo = (request.getParameter("nPymeo") == null)?"":request.getParameter("nPymeo");
String nPymed = (request.getParameter("nPymed") == null)?"":request.getParameter("nPymed");
String rfco = (request.getParameter("rfco") == null)?"":request.getParameter("rfco");
String rfcd = (request.getParameter("rfcd") == null)?"":request.getParameter("rfcd");
String direcciono = (request.getParameter("direcciono") == null)?"":request.getParameter("direcciono");
String direcciond = (request.getParameter("direcciond") == null)?"":request.getParameter("direcciond");
String coloniao = (request.getParameter("coloniao") == null)?"":request.getParameter("coloniao");
String coloniad = (request.getParameter("coloniad") == null)?"":request.getParameter("coloniad");
String telefonoo = (request.getParameter("telefonoo") == null)?"":request.getParameter("telefonoo");
String telefonod = (request.getParameter("telefonod") == null)?"":request.getParameter("telefonod");
String archivo = (request.getParameter("archivo") == null)?"":request.getParameter("archivo");	
String liNoArchivos = (request.getParameter("liNoArchivos") == null)?"1":request.getParameter("liNoArchivos");
String sRutaArchivo = 	strDirectorioTemp+archivo; 
	
if(!doctos.equals("")){
	int tamanio2 =	doctos.length();
	String valor =  doctos.substring(tamanio2-1, tamanio2);
	if(valor.equals(",")){
	doctos =doctos.substring(0,tamanio2-1);
	}
}

JSONObject   jsonObj = new JSONObject(); 
String infoRegresar	=	"", consulta ="", strCampos  ="", nomCampo1 ="", nomCampo2 ="", nomCampo3="", nomCampo4="", nomCampo5 ="",
usuario =iNoUsuario+" - "+strNombreUsuario;

//declaración de la Clase ;
ConsReasignacionDoctos paginador = new ConsReasignacionDoctos();
paginador.setIc_epo(epo);
paginador.setIc_pyme(ic_pyme);
paginador.setTotaMnacional(contadormn);
paginador.setTotaMDolar(contadord);
paginador.setAcuse(acuse);
paginador.setFechaAcuse(fechaAcuse);
paginador.setHoraAcuse(horaAcuse);
paginador.setUsuario(usuario);
paginador.setDocumentos(doctos);
paginador.setCausaReasignacion(causaReasignacion);

CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

//se obtienes los campos adicionales
Vector nombresCampo = new Vector(5);
int numeroCampos= 0;
if(!epo.equals(""))  {
	nombresCampo = paginador.getCamposAdicionales(epo);
	numeroCampos=  nombresCampo.size();
	
	for (int i=0; i<nombresCampo.size(); i++) { 			
		Vector lovRegistro = (Vector) nombresCampo.get(i);
		strCampos += (String) lovRegistro.get(0);	
		if(i==0) 	nomCampo1 = (String) lovRegistro.get(0);
		if(i==1) 	nomCampo2 = (String) lovRegistro.get(0);
		if(i==2) 	nomCampo3 = (String) lovRegistro.get(0); 
		if(i==3) 	nomCampo4 = (String) lovRegistro.get(0);
		if(i==4) 	nomCampo5 = (String) lovRegistro.get(0);			
	}	
}	
if (informacion.equals("catalogoEPO")) {

	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();
		
}else  if (informacion.equals("DatosPyme")) {

	List  registro =  paginador.getDatoPyme(epo, noElectronico, noProveedor ,  noDocto,  rfc, tipoPyme  );	
	HashMap datos = (HashMap)registro.get(0);	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("ic_pyme",datos.get("IC_YME").toString());
	jsonObj.put("nomPyme",datos.get("PYME").toString());
	jsonObj.put("r_f_c",datos.get("RFC").toString());
	jsonObj.put("direccion",datos.get("DIRECCION").toString());
	jsonObj.put("colonia",datos.get("COLONIA").toString());
	jsonObj.put("telefono",datos.get("TELEFONO").toString());
	jsonObj.put("mensaje",datos.get("MENSAJE").toString());
	jsonObj.put("epo",epo);
	infoRegresar = jsonObj.toString();

}else  if (informacion.equals("Consultar")) {	
	
	try {
		Registros reg	=	queryHelper.doSearch();
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		jsonObj = JSONObject.fromObject(consulta);
	}	catch(Exception e) {
		throw new AppException("Error en la paginación", e);
	}
	jsonObj.put("hayCamposAdicionales", String.valueOf(numeroCampos));
	jsonObj.put("nomCampo1",String.valueOf(nomCampo1));
	jsonObj.put("nomCampo2",String.valueOf(nomCampo2));
	jsonObj.put("nomCampo3",String.valueOf(nomCampo3));
	jsonObj.put("nomCampo4",String.valueOf(nomCampo4));
	jsonObj.put("nomCampo5",String.valueOf(nomCampo5));	
	infoRegresar	=	jsonObj.toString();	

}else  if (informacion.equals("CamposAdicionales")) {
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("hayCamposAdicionales", String.valueOf(numeroCampos));
	jsonObj.put("nomCampo1",String.valueOf(nomCampo1));
	jsonObj.put("nomCampo2",String.valueOf(nomCampo2));
	jsonObj.put("nomCampo3",String.valueOf(nomCampo3));
	jsonObj.put("nomCampo4",String.valueOf(nomCampo4));
	jsonObj.put("nomCampo5",String.valueOf(nomCampo5));	
	infoRegresar	=	jsonObj.toString();	

}else  if (informacion.equals("TrasmitirAcuse")) {

	IMantenimiento IMantenimientos = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);

	String extension = archivo.substring(archivo.length()-3,archivo.length());
	char getReceipt = 'Y';
	String folioCert  ="",  Error="",  mensaje ="";
	Acuse no_acuse = new Acuse(Acuse.ACUSE_EPO,"1");
	acuse = no_acuse.toString();
	
	List lAcuse = new ArrayList();
	lAcuse.add(acuse);
	lAcuse.add(contadormn);
	lAcuse.add(contadormontomn);
	lAcuse.add(contadord);
	lAcuse.add(contadormontod);
	lAcuse.add(strLogin);
	lAcuse.add("1");
	
	
	List lDatos = new ArrayList();	
	lDatos.add(epo);//0
	lDatos.add(icPymeo);//1
	lDatos.add(icPymed);//2
	lDatos.add(doctos);//3
	lDatos.add(causaReasignacion);//4
	lDatos.add(sRutaArchivo);//5
	lDatos.add(nPymeo);//6
	lDatos.add(nPymed);//7
	lDatos.add(rfco);//8
	lDatos.add(rfcd);//9
	lDatos.add(direcciono);//10
	lDatos.add(direcciond);//11
	lDatos.add(coloniao);//12
	lDatos.add(coloniad);//13
	lDatos.add(telefonoo);//14
	lDatos.add(telefonod);//15
	lDatos.add(strLogin);//16
	lDatos.add("0");//17
	lDatos.add(extension);//18
	lDatos.add(acuse);//19
	lDatos.add(liNoArchivos);//20
	
	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		folioCert = acuse.toString();			
		Seguridad s = new Seguridad();
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {			
			try {
				
			 IMantenimientos.guardarAcuseReasignacion(lAcuse, lDatos);
				Error="EXITO";
			} catch (NafinException ne){						
				mensaje = "La autentificación no se llevo acabo con éxito \n"+s.mostrarError()+"\n"+ne.getMsgError(sesIdiomaUsuario);
				Error="ERROR";
			}
		}else  {
			mensaje = "La autentificación no se llevo acabo con éxito \n "+s.mostrarError();
			Error="ERROR";
		}
	}else  {
		mensaje = "La autentificación no se llevo acabo con éxito \n";
		Error="ERROR";
	}
	if(Error.equals("EXITO"))  {
		mensaje = "La autentificación no se llevo acabo con éxito \n ";		
	}
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("respuesta",Error);
	jsonObj.put("mensaje",mensaje);
	jsonObj.put("mnacional", contadormn);
	jsonObj.put("mDolar",contadord);
	jsonObj.put("acuse",acuse);
	jsonObj.put("Fecha",Fecha);
	jsonObj.put("Hora",Hora);
	jsonObj.put("usuario",iNoUsuario+" - "+strNombreUsuario);
	jsonObj.put("causaReasignacion",causaReasignacion);
	jsonObj.put("documentos",doctos);
	infoRegresar	=	jsonObj.toString();	

}else  if (informacion.equals("ArchivoAcuse") ) {
	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");				
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);							
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo PDF", e);
	}	
	infoRegresar = jsonObj.toString();
}
%>

<%=infoRegresar%>

