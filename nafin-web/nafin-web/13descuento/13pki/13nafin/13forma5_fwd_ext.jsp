<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>

<%
if (!com.netro.web.seguridad.SeguridadWeb.esIPValida(request)) {
	throw new RuntimeException("Error. No se cuentan con los privilegios necesarios para accesar a esta pantalla");
}
%>

<% 
request.setAttribute("producto","1");
request.setAttribute("facultad","/nafin/13descuento/13pki/13nafin/13forma5.jsp?sOrigenDscto=OperTel");
request.setAttribute("cveFacultad","13NAFIN13FORMA5FWD");
//String strPerfil = (String)session.getAttribute("sesPerfil");
String pagina = (strPerfil.equals("NAFIN OPERADOR") || strPerfil.equals("NAFIN CON"))?"/nafin/00utils/00buscapyme_operador_ext.js":"/nafin/00utils/00buscapyme_ext.js";

%> 

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/menu.jspf"%>
<%}%>

<%-- El jsp que contiene el dise?o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
</style>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/00utils/00buscapyme_ext.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS){%>
<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
<%}%>
			<div id="areaContenido"><div style="height:190px"></div>

<%if(esEsquemaExtJS){%>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>
<!-- Valores iniciales recibidos como parametros-->
<input type="hidden" id="hidCveFacultad" value="13NAFIN13FORMA5FWD">
<input type="hidden" id="hidProducto" value="1">
<input type="hidden" id="sOrigenDscto" value="OperTel">
</body> 
</html>