<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,		
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
	ParametrosRequest req = null;
	String itemArchivo	="",  rutaArchivo ="",   error_tam =""; 
	String PATH_FILE	=	strDirTrabajo;
	String nombreArchivo= "";
	int tamanio=0;
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	
		req = new ParametrosRequest(upload.parseRequest(request));
			 
		FileItem fItem = (FileItem)req.getFiles().get(0);
		itemArchivo		= (String)fItem.getName();
		InputStream archivo = fItem.getInputStream();
		rutaArchivo = PATH_FILE+itemArchivo;		
		tamanio			= (int)fItem.getSize();
	
		
		if(tamanio>2097152){
			error_tam ="El Archivo es muy Grande, excede el L�mite que es de 2 MB.";
		}		
		
		fItem.write(new File(rutaArchivo));			
	}
		
	if(!error_tam.equals("")){
		nombreArchivo="";
	}	

	%>
{
	"success": true,
	"archivo":	'<%=itemArchivo%>',		
	"fileSize":	'<%=String.valueOf(tamanio)%>',	
	"error_tam":	'<%=error_tam%>'	
}



