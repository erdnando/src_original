Ext.onReady(function() {

	var documentos = [];
	var valores = []; 
	
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [	
			{
				xtype: 'button',
				text: '<h1><b>Captura</1></b>',			
				id: 'btnCaptura',					
				handler: function() {
					window.location = '/nafin/13descuento/13pki/13nafin/13liberalimitesxifext.jsp';
				}
			},		
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 'button',		id: 'btnConsulta',					

				text: 'Consulta',			
						handler: function() {
					window.location = '/nafin/13descuento/13pki/13nafin/13liberalimitesxifConsultaext.jsp';
				}
			}	
		]
	});
	
	
	function procesarGenerarPDF(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			btnGenerarPDF.setIconClass('icoPdf');
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	function procesarSuccessFailureLiberar(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			
			var gridPreAcuse = Ext.getCmp('gridPreAcuse');
			var autentificacion = Ext.getCmp('autentificacion');
			var leyenda = Ext.getCmp('leyenda');
			var btnSalirP = Ext.getCmp('btnSalirP');						
			Ext.getCmp("mensajeAutenta").setValue(jsonData.mensajeAutentificacion);
			autentificacion.show();				
			
			if(Ext.getCmp("strTipoUsuario").getValue()=='NAFIN') {
				Ext.getCmp("fpBotones").hide();			
			}
			
			if(jsonData.recibo !='')  { 
			
				var acuseCifras = [
					['N�mero de Acuse', jsonData.acuse],
					['Fecha de Liberaci�n ', jsonData.fecha],
					['Hora de Liberaci�n', jsonData.hora],
					['Usuario de Captura ', jsonData.usuario]
				];
							
				storeCifrasData.loadData(acuseCifras);	
				gridCifrasControl.show();
				
				Ext.getCmp("documentos").setValue(jsonData.doctoSelec);
				
				// consulta  para  el Acuse 		
				fp.el.mask('Enviando...', 'x-mask-loading');	
				consultaPreAcuseData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ConsultarPreAcuse',
						pantalla: 'PantallaAcuse',
						documentos:jsonData.doctoSelec
					})
				});
			} else {
				gridPreAcuse.hide();	
				btnSalirP.show();	
				leyenda.hide();	
				Ext.getCmp('forma').hide();
				Ext.getCmp('fpBotones').hide();
				Ext.getCmp('gridConsulta').hide();
				Ext.getCmp('gridTotales').hide();
			}			
						
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var autentificacion = new Ext.Container({
		layout: 'table',		
		id: 'autentificacion',							
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,		
		items: [					
			{
				xtype: 'displayfield',
				id:	'mensajeAutenta',
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}		
			}
		]
	});
	
	//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		title: 'Datos cifras de control',
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 570,
		height: 200,
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});
	


//*********************pantalla PreAcuse Perfil Nafin ****************

	var confirmarAcuse = function(pkcs7,  textoFirmar ,  vdocumentos ,  vvalores  ){
	
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnLiberarNafinC").enable();	
			return;	//Error en la firma. Termina...				
		}else  {	
			
			Ext.getCmp("btnLiberarNafinC").disable();					
			Ext.Ajax.request({
				url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'LiberarVencimiento',
					documentos:vdocumentos,
					valores:vvalores,
					textoFirmar:textoFirmar,
					pkcs7:pkcs7			
				}),				
				callback: procesarSuccessFailureLiberar
			});
		}		
	}
	
	
	var procesarLiberarNafin= function() 	{
		
		valores = [];				
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		documentos = [];
		var totalSe =0;
		var textoFirmar = "Confirmaci�n de la liberaci�n de los vencimientos de los siguientes Intermediarios Financieros";
			 textoFirmar += '\n Pyme, N�mero de documento,Tasa de Fondeo, Numero de Prestamo, Moneda, Monto a Descontar,Fecha de vencimiento \n';
	
		var store = gridConsulta.getStore();
		store.each(function(record) {	
			if(record.data['SELECCIONAR'] =='S') {
				totalSe++
				documentos.push(record.data['IC_DOCUMENTO']);
				valores.push(record.data['IC_IF'] +","+record.data['IC_MONEDA']  +","+record.data['IC_EPO'] +","+record.data['IC_DOCUMENTO']+","+record.data['MONTO_DESC']+"|");
				documentos.push(record.data['IC_DOCUMENTO']);
			
				textoFirmar += record.data['NOMBRE_IF'] +","+record.data['NOMBRE_PYME']+","+record.data['NUM_DOCTO']+","+record.data['TASA_FONDEO'] 
								+","+record.data['NUMERO_PRESTAMO'] +","+record.data['MONEDA'] +","+record.data['MONTO_DESC'] +","+record.data['FECHA_VENC']+"\n";
			}
		});
		
		if(totalSe ==0) {
			Ext.MessageBox.alert('Mensaje','Debes seleccionar al menos un Intermediario');
			return false;
			
		}else  {
			
			
			NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar ,  documentos ,  valores  );
			
		}
	}
	
//*********************pantalla PreAcuse Perfil  iF  ****************

	
	var confirmarAcuse1 = function(pkcs7, textoFirmar ,  mdocumentos ,  mvalores){
	
	
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnLiberarP").enable();	
			return;	//Error en la firma. Termina...
		}else  {	
			Ext.getCmp("btnLiberarP").disable();	
			Ext.Ajax.request({
				url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'LiberarVencimiento',
					documentos:mdocumentos,
					valores:mvalores,
					textoFirmar:textoFirmar,
					pkcs7:pkcs7			
				}),				
				callback: procesarSuccessFailureLiberar
			});
		}	
	}

	var procesarLiberarPre= function() 	{
		
		valores = [];		
		var textoFirmar;
		var gridPreAcuse = Ext.getCmp('gridPreAcuse');
		var store = gridPreAcuse.getStore();
		var mensaje = Ext.getCmp('mensaje').getValue();
		textoFirmar =mensaje +'\n';
		textoFirmar += 'Pyme, N�mero de documento,Tasa de Fondeo, Numero de Prestamo, Moneda, Monto a Descontar,Fecha de vencimiento \n';


		store.each(function(record) {	
			valores.push(record.data['IC_IF'] +","+record.data['IC_MONEDA']  +","+record.data['IC_EPO'] +","+record.data['IC_DOCUMENTO']+","+record.data['MONTO_DESC']+"|");
			documentos.push(record.data['IC_DOCUMENTO']);
		
			textoFirmar += record.data['NOMBRE_IF'] +","+record.data['NOMBRE_PYME']+","+record.data['NUM_DOCTO']+","+record.data['TASA_FONDEO'] 
							+","+record.data['NUMERO_PRESTAMO'] +","+record.data['MONEDA'] +","+record.data['MONTO_DESC'] +","+record.data['FECHA_VENC']+"\n";
		});	
			
		
		NE.util.obtenerPKCS7(confirmarAcuse1, textoFirmar ,  documentos ,  valores  );
		
		
	}
	
	var procesarConsultaPreAcuseData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridPreAcuse = Ext.getCmp('gridPreAcuse');	
		var el = gridPreAcuse.getGridEl();
		
		if (arrRegistros != null) {
			if (!gridPreAcuse.isVisible()) {
				gridPreAcuse.show();
			}	
			var cm = gridPreAcuse.getColumnModel();
			var jsonData = store.reader.jsonData;
			var gridTotales = Ext.getCmp('gridTotales');
			var gridConsulta = Ext.getCmp('gridConsulta');	
			var leyenda = Ext.getCmp('leyenda');
			
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnLiberarP = Ext.getCmp('btnLiberarP');
			var mensaje = Ext.getCmp('mensaje');
			
			//Mostrando u ocultando columna referencia
			if(jsonData.strTipoUsuario=='IF') {
					gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), false);			
			}else if(jsonData.strTipoUsuario=='NAFIN') {
					gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), true);	
			}
						
			Ext.getCmp("mensaje").setValue(jsonData.mensaje);
			mensaje.show();		
			gridTotales.hide();
			gridConsulta.hide();
			fp.hide();
			leyenda.show();
			
			if(Ext.getCmp("strTipoUsuario").getValue()=='NAFIN') {
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_FIDEICOMISO'), false);	
			}
			
			if(jsonData.pantalla =='PantallaAcuse'){
				
				if(jsonData.strTipoUsuario=='NAFIN') {
					gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);
				}else  {
					gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), true);
				}
				
				btnLiberarP.hide();
				btnGenerarPDF.show();
			}else {
				btnGenerarPDF.hide();
			}
				
			if(store.getTotalCount() > 0) {				
				el.unmask();					
			} else {						
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var btnSalirP = new Ext.Container({
		layout: 'table',		
		id: 'btnSalirP',							
		width:	'100',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,		
		items: [		
			{
				xtype: 'button',
				text: 'Salir',
				tooltip:	'Salir',
				id: 'btnSalirPP',		
				iconCls: 'icoLimpiar',
				handler: function() {
					if(Ext.getCmp("strTipoUsuario").getValue()=='IF') {
						window.location = '/nafin/13descuento/13pki/13if/13liberalimitesxifExt.jsp';
					}else if(Ext.getCmp("strTipoUsuario").getValue()=='NAFIN') {
						window.location = '/nafin/13descuento/13pki/13nafin/13liberalimitesxifext.jsp';
					}
				}
			}
		]
	});
	

	var leyenda = new Ext.Container({
		layout: 'table',		
		id: 'leyenda',							
		width:	'600',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,		
		items: [					
			{
				xtype: 'displayfield',
				id:	'mensaje',
				width:	'600',
				hidden: true,
				value: '',
				cls:		'x-form-item',
				style: { 
					width:   		'100%', 
					textAlign: 		'center'
				}		
			}, 
			{ 	xtype: 'displayfield', hidden:true, id: 'documentos', 	value: '' }	
		]
	});	
	
	var consultaPreAcuseData = new Ext.data.JsonStore({
		root : 'registros',
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp',
		baseParams: {
			informacion: 'ConsultarPreAcuse'
		},
		hidden: true,
		fields: [	
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_PYME'},	
			{name: 'NUM_DOCTO'},
			{name: 'TASA_FONDEO'},	
			{name: 'NUMERO_PRESTAMO'},
			{name: 'MONEDA'},
			{name: 'MONTO_DESC'},
			{name: 'FECHA_VENC'},
			{name: 'IC_DOCUMENTO'},
			{name: 'IC_MONEDA'},
			{name: 'IC_IF'},
			{name: 'IC_EPO'},
			{name: 'REFERENCIA'},
			{name: 'NOMBRE_FIDEICOMISO'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaPreAcuseData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaPreAcuseData(null, null, null);						
					}
				}
			}			
	});
	
	
	
	var gridPreAcuse = new Ext.grid.GridPanel({
		id: 'gridPreAcuse',				
		store: consultaPreAcuseData,	
		style: 'margin:0 auto;',
		title:'Liberaci�n de L�mites por IF',
		hidden: true,
		clicksToEdit: 1,	
		columns: [
			{							
				header : 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex : 'NOMBRE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'			
			},
			{							
				header : 'Pyme',
				tooltip: 'Pyme',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'			
			},
			{							
				header : 'N�mero de documento',
				tooltip: 'N�mero de documento',
				dataIndex : 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},
			{							
				header : 'Tasa de Fondeo',
				tooltip: 'Tasa de Fondeo',
				dataIndex : 'TASA_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){    
					if(value =='0') {
						return '%';
					}else {
						return  Ext.util.Format.number(value, '0.0000%');
					}
				}				
			},
			{							
				header : 'N�mero de Prestamo',
				tooltip: 'N�mero de Prestamo',
				dataIndex : 'NUMERO_PRESTAMO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){    
					if(value =='0') {
						return '';
					}else {
						return value;
					}
				}
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},
			{							
				header : 'Nombre Fideicomiso',
				tooltip: 'Nombre Fideicomiso',
				dataIndex : 'NOMBRE_FIDEICOMISO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',
				hidden: true
			},		
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex : 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},
			{							
				header : '<center>Referencia</center>',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'			
			}
		],					
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false,			
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar PDF',
					id: 'btnGenerarPDF',
					iconCls: 'icoPdf',
					handler: function(boton, evento) {					
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								documentos:documentos,
								pantalla: 'PantallaAcuse'							
							}					
							,callback: procesarGenerarPDF
						});
					}
				}, 					
				{
					xtype: 'button',
					text: 'Liberar Vencimientos',					
					tooltip:	'Liberar Vencimientos',
					iconCls: 'icoAceptar',
					id: 'btnLiberarP',
					handler: procesarLiberarPre
				},
				'-',
				{
					xtype: 'button',
					text: 'Salir',
					tooltip:	'Salir',
					id: 'btnSalir',
					iconCls: 'icoLimpiar',
					handler: function() {
						if(Ext.getCmp("strTipoUsuario").getValue()=='IF') {
							window.location = '/nafin/13descuento/13pki/13if/13liberalimitesxifExt.jsp';
						}else if(Ext.getCmp("strTipoUsuario").getValue()=='NAFIN') {
							window.location = '/nafin/13descuento/13pki/13nafin/13liberalimitesxifext.jsp';
						}
					}
				}
			]
		}
	});
	
	//*********************pantalla Principal Perfil IF ****************
	var procesarLiberar= function() 	{

		documentos = [];
		var totalSe =0;
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		store.each(function(record) {	
			if(record.data['SELECCIONAR'] =='S') {
				totalSe++
				documentos.push(record.data['IC_DOCUMENTO']);
			}
		});
		
		if(totalSe ==0) {
			Ext.MessageBox.alert('Mensaje','Debes seleccionar al menos un Intermediario');
			return false;	
		}
		
		fp.el.mask('Enviando...', 'x-mask-loading');	
		consultaPreAcuseData.load({
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ConsultarPreAcuse',
				documentos:documentos,
				pantalla: 'PantallaPreAcuse'
			})
		});					

	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		
		var gridConsulta = Ext.getCmp('gridConsulta');							
		//var cm = gridConsulta.getColumnModel();
		var el = gridConsulta.getGridEl();
		
		if (arrRegistros != null) {
			
			
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}					
			
			var gridTotales = Ext.getCmp('gridTotales');
			var btnLiberarC = Ext.getCmp('btnLiberarC');		
			
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();			
			var jsonData = store.reader.jsonData;	
			if(Ext.getCmp("strTipoUsuario").getValue()=='NAFIN') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_FIDEICOMISO'), false);	
			}
			//Mostrando u ocultando columna referencia
			if(jsonData.strTipoUsuario=='IF') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), false);			
			}else if(jsonData.strTipoUsuario=='NAFIN') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), true);					
			}
			
			if(store.getTotalCount() > 0) {	
			
				consultaDataTotales.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Totales'							
					})
				});									
				gridTotales.show();	
				if(jsonData.strTipoUsuario=='IF') {
					btnLiberarC.enable();
					Ext.getCmp('btnLiberarNafinC').hide();
				}else  if(jsonData.strTipoUsuario=='NAFIN') {
					Ext.getCmp('btnLiberarNafinC').enable();
				   Ext.getCmp('btnLiberarC').hide();
				}
				el.unmask();					
			} else {		
				gridTotales.hide();
				if(jsonData.strTipoUsuario=='IF') {
					btnLiberarC.disable();
					Ext.getCmp('btnLiberarNafinC').hide();
				}else  {
					Ext.getCmp('btnLiberarNafinC').disable();
				   Ext.getCmp('btnLiberarC').hide();
				}
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

	
	
	var consultaDataTotales = new Ext.data.JsonStore({
		root : 'registros',
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp',
		baseParams: {
			informacion: 'Totales'
		},
		hidden: true,
		fields: [	
			{name: 'IC_MONEDA'},
			{name: 'MONEDA'},
			{name: 'MONTO_TOTAL'}									
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);										
					}
				}
			}			
	});
	

	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales',				
		store: consultaDataTotales,	
		style: 'margin:0 auto;',
		title:'',
		hidden: true,		
		columns: [
			{							
				header : 'IC_MONEDA',
				tooltip: 'IC_MONEDA',
				dataIndex : 'IC_MONEDA',
				sortable: true,
				hidden: true,	
				width: 130,
				resizable: true,				
				align: 'center'	
			},	
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 100,
				resizable: true,				
				align: 'center'	
			},		
			{							
				header : 'Monto Total de Documentos Seleccionados',
				tooltip: 'Monto Total de Documentos Seleccionados',
				dataIndex : 'MONTO_TOTAL',
				sortable: true,
				width: 300,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		stripeRows: true,
		height: 100,
		width: 420,
		style: 'margin:0 auto;',		
		frame: false
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [	
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_PYME'},	
			{name: 'NUM_DOCTO'},
			{name: 'TASA_FONDEO'},	
			{name: 'NUMERO_PRESTAMO'},
			{name: 'MONEDA'},
			{name: 'MONTO_DESC'},
			{name: 'FECHA_VENC'},
			{name: 'SELECCIONAR'},
			{name: 'IC_DOCUMENTO'},
			{name: 'IC_MONEDA'},
			{name: 'IC_IF'},
			{name: 'IC_EPO'},
			{name: 'REFERENCIA'},
			{name: 'NOMBRE_FIDEICOMISO'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
		
	var selectModel = new Ext.grid.CheckboxSelectionModel( {
		checkOnly: true,			
		listeners: {
			rowdeselect: function(selectModel, rowIndex, record) {	// cuando se quita la selecci�n			
				record.data['SELECCIONAR']='N';
				record.commit();				
				var montosTotales=0;
				
				var gridTotales = Ext.getCmp('gridTotales');
				var store = gridTotales.getStore();	
				store.each(function(record2) {				
					
					if(record2.data['IC_MONEDA'] ==1 && record.data['IC_MONEDA'] ==1 && record.data['SELECCIONAR'] =='N') {					
						if(parseFloat(record2.data['MONTO_TOTAL'])>0){
						montosTotales = parseFloat(record2.data['MONTO_TOTAL'])-parseFloat(record.data['MONTO_DESC']); 
						record2.data['MONTO_TOTAL'] =montosTotales;		
						}						
					}
					if(record2.data['IC_MONEDA'] ==54 && record.data['IC_MONEDA'] ==54 && record.data['SELECCIONAR'] =='N') {						
						if(parseFloat(record2.data['MONTO_TOTAL'])>0){
							montosTotales = parseFloat(record2.data['MONTO_TOTAL'])-parseFloat(record.data['MONTO_DESC']); 
							record2.data['MONTO_TOTAL'] =montosTotales;		
						}						
					}											
					record2.commit();
				});
				
								  
			},
			//Se activa cuando una fila est� seleccionada, return false para cancelar.
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){ 
				
				var antes = record.data['SELECCIONAR'];
				
				record.data['SELECCIONAR']='S';
				record.commit();
				
				var despues = record.data['SELECCIONAR'];
				
				if(antes !=despues) {
					var montosTotales=0;
					
					var gridTotales = Ext.getCmp('gridTotales');
					var store = gridTotales.getStore();	
					store.each(function(record2) {
						
						if(record2.data['IC_MONEDA'] ==1 && record.data['IC_MONEDA'] ==1 && record.data['SELECCIONAR'] =='S') {											
							montosTotales = parseFloat(record2.data['MONTO_TOTAL'])+parseFloat(record.data['MONTO_DESC']); 
							record2.data['MONTO_TOTAL'] =montosTotales;					
						}	
						if(record2.data['IC_MONEDA'] ==54 && record.data['IC_MONEDA'] ==54 && record.data['SELECCIONAR'] =='S') {					
							montosTotales = parseFloat(record2.data['MONTO_TOTAL'])+parseFloat(record.data['MONTO_DESC']); 
							record2.data['MONTO_TOTAL'] =montosTotales;					
						}	
						record2.commit();				
					});
				}
			}
		}
	});
	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Liberaci�n de L�mites por IF',
		hidden: true,
		clicksToEdit: 1,	
		columns: [
			{							
				header : 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex : 'NOMBRE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'			
			},
			{							
				header : 'Pyme',
				tooltip: 'Pyme',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'			
			},
			{							
				header : 'N�mero de documento',
				tooltip: 'N�mero de documento',
				dataIndex : 'NUM_DOCTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},
			{							
				header : 'Tasa de Fondeo',
				tooltip: 'Tasa de Fondeo',
				dataIndex : 'TASA_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){    
					if(value =='0') {
						return '%';
					}else {
						return  Ext.util.Format.number(value, '0.0000%');
					}
				}
			},
			{							
				header : 'N�mero de Prestamo',
				tooltip: 'N�mero de Prestamo',
				dataIndex : 'NUMERO_PRESTAMO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer:function(value,metadata,registro){    
					if(value =='0') {
						return '';
					}else {
						return value;
					}
				}
				
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},
			{							
				header : 'Nombre Fideicomiso',
				tooltip: 'Nombre Fideicomiso',
				dataIndex : 'NOMBRE_FIDEICOMISO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left',	
				hidden: true
			},			
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Fecha de vencimiento',
				tooltip: 'Fecha de vencimiento',
				dataIndex : 'FECHA_VENC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'			
			},			
			{							
				header : '<center>Referencia</center>',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'			
			}		
			,selectModel
		],					
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,		
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,	
		sm:selectModel,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Liberar Vencimientos',					
					tooltip:	'Liberar Vencimientos',
					iconCls: 'icoAceptar',
					id: 'btnLiberarC',
					handler: procesarLiberar
				},				
				{
					xtype: 'button',
					text: 'Liberar Vencimientos',					
					tooltip:	'Liberar Vencimientos',
					iconCls: 'icoAceptar',
					id: 'btnLiberarNafinC',
					handler: procesarLiberarNafin
				}	
			]
		}
	});
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){			
				var grid = Ext.getCmp("gridConsulta");
				//var cm = grid.getColumnModel();
				
				Ext.getCmp("fechaIni").setValue(jsonData.fechaHoy);
				Ext.getCmp("fechaFin").setValue(jsonData.fechaHoy);
				Ext.getCmp("fechaHoy").setValue(jsonData.fechaHoy);
				
				Ext.getCmp("strTipoUsuario").setValue(jsonData.strTipoUsuario);
				
				if(jsonData.strTipoUsuario=='IF') {
					//grid.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), false);
					
					Ext.getCmp("ic_if1").hide();
					Ext.getCmp("num_electronicoC1").show();
					Ext.getCmp("fpBotones").hide();
					
				}else if(jsonData.strTipoUsuario=='NAFIN') {
					//grid.getColumnModel().setHidden(cm.findColumnIndex("REFERENCIA"), true);
					
					Ext.getCmp("ic_if1").show();
					Ext.getCmp("num_electronicoC1").hide();
					Ext.getCmp("fpBotones").show();					
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var successAjaxFn = function(opts, success, response) { 
	if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);	
			
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp', 
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	var procesarCatalogoEPO= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var jsonData = store.reader.jsonData;
			var ic_epo1 = Ext.getCmp('ic_epo1');
			if(ic_epo1.getValue()==''){
				ic_epo1.setValue(jsonData.ic_epo);
			}
		}
  }
	
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp', 
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {	
			load: procesarCatalogoEPO,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
		

	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
		
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'noPyme',
			id: 'noPyme1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);		
							var ic_epo = Ext.getCmp("ic_epo1").getValue();	
							
							storeBusqAvanzPyme.load({
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
										ic_epo:ic_epo						
									})																	
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	})
	
	
	var elementosForma  = [
		{ 	xtype: 'displayfield', 	hidden: true, id: 'fechaHoy', 	value: '' },				
		{
			xtype: 'combo',
			fieldLabel: 'Nombre de la EPO',
			name: 'ic_epo',
			id: 'ic_epo1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_epo',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,
			store: catalogoEPO,
			listeners: {
				select: {
					fn: function(combo) {
						var ic_if = Ext.getCmp('ic_if1').getValue();						
						if(ic_if =='') {
							var cmbIF = Ext.getCmp('ic_if1');
							cmbIF.setValue('');
							cmbIF.store.load({
								params: {									
									ic_epo:combo.getValue()
								}
							});
						}
					}
				}
			}			
		},
		{
			xtype: 'combo',
			fieldLabel: 'Intermediario Financiero',
			name: 'ic_if',
			id: 'ic_if1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',
			hiddenName : 'ic_if',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,			
			minChars : 1,
			width: 200,
			store: catalogoIF,
			listeners: {
				select: {
					fn: function(combo) {
					
						var ic_epo = Ext.getCmp("ic_epo1").getValue();
						var cmbEPO = Ext.getCmp('ic_epo1');
						cmbEPO.setValue('');
						cmbEPO.store.load({
							params: {
								ic_if:combo.getValue(),
								ic_epo: ic_epo									
							}
						});
					}
				}
			}
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'N�mero Nafin Electr�nico',
			combineErrors: false,
			msgTarget: 'side',	
			id: 'num_electronicoC1',
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					fieldLabel: 'Proveedor',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 80,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						'blur': function(){					
							
						 // Petici�n b�sica  
							Ext.Ajax.request({  
								url: '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp',  
								method: 'POST',  
								callback: successAjaxFn,  
								params: {  
									 informacion: 'pymeNombre' ,
									 num_electronico: Ext.getCmp('num_electronico1').getValue()
								}  
							}); 	
						}
					}//necesario para mostrar el icono de error
				},
				{
					xtype: 'textfield',
					name: 'txtNombre',
					id: 'txtNombre1',
					allowBlank: true,
					maxLength: 100,
					width: 300,
					disabled:true,
					msgTarget: 'side',
					margins: '0 20 0 0'//Necesario para mostrar el icono de error
				},				
				{
				xtype: 'button',
				text: 'B�squeda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
					}
				}
			}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fechaIni',
					id: 'fechaIni',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaFin',
					margins: '0 20 0 0' ,
					formBind: true,
					listeners:{
						blur: function(field){ 									
							if(field.getValue() != '') {								
								var fechaHoy = Ext.getCmp('fechaHoy').getValue();	
								fechaHoy = new Date();
								dia = fechaHoy.getDate();
								mes = fechaHoy.getMonth();
								anio = fechaHoy.getFullYear();
								fechaHoy = new Date(anio,mes,dia);
								fecha = Ext.util.Format.date(field.getValue(),'d/m/Y');
								dataFec = fecha.split("/");
								fecha = new Date(Number(dataFec[2]),Number(dataFec[1])-1,Number(dataFec[0]));
								
								if(fecha.getTime() < fechaHoy.getTime()){
									Ext.MessageBox.alert('Mensaje','La fecha debe ser igual o mayor al dia actual');
									Ext.getCmp("fechaIni").setValue(fechaHoy);
								}
								
							}							
						}
					}
				},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fechaFin',
					id: 'fechaFin',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fechaIni',
					margins: '0 20 0 0',
					formBind: true,
					listeners:{
						blur: function(field){ 									
							if(field.getValue() != '') {								
								var fechaHoy = Ext.getCmp('fechaHoy').getValue();										
								
									fechaHoy = new Date();
									dia = fechaHoy.getDate();
									mes = fechaHoy.getMonth();
									anio = fechaHoy.getFullYear();
									fechaHoy = new Date(anio,mes,dia);
									fecha = Ext.util.Format.date(field.getValue(),'d/m/Y');
									dataFec = fecha.split("/");
									fecha = new Date(Number(dataFec[2]),Number(dataFec[1])-1,Number(dataFec[0]));
									
									if(fecha.getTime() < fechaHoy.getTime()){
										Ext.MessageBox.alert('Mensaje','La fecha debe ser igual o mayor al dia actual');
										Ext.getCmp("fechaFin").setValue(fechaHoy);
									}
						
						
							}							
						}
					}
				}
			]
		},
		{ 	xtype: 'displayfield', hidden:true, id: 'strTipoUsuario', 	value: '' },
		{ 	xtype: 'displayfield', hidden:true, id: 'ic_pyme', 	value: '' }	
	];		

	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		title: 'Captura Liberaci�n de L�mites por IF',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,	  		
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoAceptar',
				formBind: true,		
				handler: function(boton, evento) {
								
					var ic_epo = Ext.getCmp("ic_epo1");
					if (Ext.isEmpty(ic_epo.getValue()) ){
						ic_epo.markInvalid('Debe Seleccionar una EPO');
						return;
					}
					
						var fechaIni = Ext.getCmp("fechaIni");
					var fechaFin = Ext.getCmp("fechaFin");
					
					if(Ext.isEmpty(fechaIni.getValue()) || Ext.isEmpty(fechaFin.getValue())){
						if(Ext.isEmpty(fechaIni.getValue())){
							fechaIni.markInvalid('Debe capturar ambas fechas');
							fechaIni.focus();
							return;
						}
						if(Ext.isEmpty(fechaFin.getValue())){
							fechaFin.markInvalid('Debe capturar ambas fechas');
							fechaFin.focus();
							return;
						}
					}
					
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Consultar',
							ic_pyme:Ext.getCmp('ic_pyme').getValue()
						})
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					if(Ext.getCmp("strTipoUsuario").getValue()=='IF') {
						window.location = '/nafin/13descuento/13pki/13if/13liberalimitesxifExt.jsp';
					}else if(Ext.getCmp("strTipoUsuario").getValue()=='NAFIN') {
						window.location = '/nafin/13descuento/13pki/13nafin/13liberalimitesxifext.jsp';
					}					
				}				
			}
		]
	});
	


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			fpBotones, 
			NE.util.getEspaciador(20),
			fp,		
			autentificacion, 
			NE.util.getEspaciador(20),
			gridCifrasControl,
			leyenda,
			btnSalirP,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridPreAcuse, 
			NE.util.getEspaciador(20),
			gridTotales,
			NE.util.getEspaciador(20)			
		]
	});


//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '/nafin/13descuento/13pki/13nafin/13liberalimitesxifExt.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});
	
	catalogoEPO.load();	
	catalogoIF.load();
	
});