
//funcion para seleccionar los registros 
function radioSELECCIONAR(check, rowIndex, colIds){

	var  gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
 
	store.each(function(record) {
		var numRegistro = store.indexOf(record);	
	
		if(numRegistro!=rowIndex && record.data['SELECCIONAR']== 'S')  {
			record.data['SELECCIONAR'] ='';
		}
		
		if(numRegistro==rowIndex && record.data['SELECCIONAR']== '' )  {
			record.data['SELECCIONAR'] ='S';			
		}
		record.commit();
		store.commitChanges();	
	});	
}
function checkSELECCIONAR(check, rowIndex, colIds){

	var gridConsulta = Ext.getCmp('gridConsulta');	
	var store = gridConsulta.getStore();
	var reg = gridConsulta.getStore().getAt(rowIndex);
	if(reg.get('SELECCIONAR') == 'checked'){
		check.checked = true
	}else{
		if(check.checked == true)  {
			reg.set('SELECCIONAR','S');
		}else{
			reg.set('SELECCIONAR','N');
		}	
	}

}



Ext.onReady(function() {
	
	// descarga el archivo PDF de Acuse 
	function procesarArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	// boton para generarPDF  de Acuse 		
	var generarPDF = function(store, arrRegistros, opts) 	{
		var  gridAcuse = Ext.getCmp('gridAcuse');
		var store = gridAcuse.getStore();		
		var nombreEPO;  
		var num_sirac;
		var nombre_pyme; 
		var num_docto;
		var estatus;
		var estatus_nuevo;
		var moneda;
		var monto_documento;
		var porce_descuento;
		var recurso_garant;
		var monto_desc;
		var monto_int;
		var monto_operar;	
		var nombreFideicomiso;
		var interesFideicomiso;
		var montoFideicomiso;
		var nombreIF;
                var ic_docdos_PDF=[];
	
		
		store.each(function(record) {
			nombreEPO=	record.data['NOMBRE_EPO'];
			num_sirac= record.data['NUM_SIRAC'];
			nombre_pyme =  record.data['NOMBRE_PYME'];
			num_docto =  record.data['NUM_DOCUMENTO'];
			estatus_nuevo = record.data['ESTATUS_NUEVO'];
			moneda =  record.data['MONEDA'];
			monto_documento =  record.data['MONTO_DOCUMENTO'];
			porce_descuento = record.data['PORCE_DESCUENTO'];
			recurso_garant =  record.data['RECURSO_GARANTIA'];
			monto_desc = record.data['MONTO_DESCONTAR'];
			monto_int =  record.data['MONTO_INTERES'];
			monto_operar = record.data['MONTO_OPERAR'];			
			nombreFideicomiso =  record.data['NOMBRE_FIDEICOMISO'];	
		   interesFideicomiso =  record.data['MONTO_INTERES_FONDEO'];	
		   montoFideicomiso =  record.data['MONTO_OPERAR_FONDEO'];	
		   nombreIF =  record.data['NOMBRE_IF'];
                   ic_docdos_PDF.push(record.data['NOMBRE_EPO']+'|'+record.data['NUM_SIRAC']+'|'+record.data['NOMBRE_PYME']+'|'+record.data['NUM_DOCUMENTO']+'|'+record.data['ESTATUS_NUEVO']+'|'+record.data['MONEDA']+'|'+record.data['MONTO_DOCUMENTO']+'|'+record.data['PORCE_DESCUENTO']+'|'+record.data['RECURSO_GARANTIA']+'|'+record.data['MONTO_DESCONTAR']+'|'+record.data['MONTO_INTERES']+'|'+record.data['MONTO_OPERAR']+'|'+record.data['NOMBRE_FIDEICOMISO']+'|'+record.data['MONTO_INTERES_FONDEO']+'|'+record.data['MONTO_OPERAR_FONDEO']+'|'+record.data['NOMBRE_IF']);
		});

		Ext.Ajax.request({
			url: '13consulta20ext.data.jsp',
			params: {
				informacion: 'ArchivoAcuse',														
				ic_folio:Ext.getCmp("ic_folio").getValue(),
				nombreEPO:nombreEPO,
				num_sirac:num_sirac, 
				nombre_pyme:nombre_pyme,
				num_docto:num_docto,				
				estatus_nuevo:estatus_nuevo,
				moneda:moneda, 
				monto_documento:monto_documento,
				porce_descuento:porce_descuento,
				recurso_garant:recurso_garant,
				monto_desc:monto_desc,
				monto_int:monto_int, 
				monto_operar:monto_operar,
				nombreFideicomiso:nombreFideicomiso,
				interesFideicomiso:interesFideicomiso,
				montoFideicomiso: montoFideicomiso,
				nombreIF:nombreIF,
                                ic_docdos_PDF:ic_docdos_PDF
			},
			callback: procesarArchivo
		});
	}				
	
	// respuesta de confirmaci�n de la cancelacion del documento
	function transmiteConfirmacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp("mensajeAuto").setValue('<table width="500" align="center" ><tr><td><H1>'+info.mensaje+'</H1></td></tr></table>');
			Ext.getCmp('mensajeAutorizacion').show();	
					
			if(info.respuesta !='Cancelacion_Error') {
							
				Ext.getCmp('ic_folio').setValue(info.ic_folio);					
				Ext.getCmp('gridPreAcuse').hide();	
				Ext.getCmp('gridAcuse').show();
					
			}else {
				Ext.getCmp('gridPreAcuse').hide();
				Ext.getCmp('gridAcuse').hide();
			}

		} else {
			NE.util.mostrarConnError(response,opts);	
			Ext.getCmp('gridPreAcuse').hide();
			Ext.getCmp('gridAcuse').hide();
		}
	}
	
	var fnProcesoConfirmacionCallback = function(vpkcs7, vtextoFirmado, vic_documento, vnewEstatusDocto,
			vcausaCancelacion, vfileSize, vnombreArchivo){
            console.log("fnProcesoConfirmacionCallback "+ vic_documento);                        
		if (Ext.isEmpty(vpkcs7)) {
			Ext.getCmp("btnRegresar").enable();	
			Ext.getCmp("btnConfirmar").enable();	
			return;	//Error en la firma. Termina...
		}else  {		
			Ext.getCmp('btnRegresar').disable();
			Ext.getCmp('btnConfirmar').disable();	
				
			Ext.Ajax.request({
				url: '13consulta20ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'cancelaDocto',					
					textoFirmado: vtextoFirmado,
					pkcs7: vpkcs7,
					//ic_documento: vic_documento,
                                        ic_docs_ic_est_nue: vic_documento,
					newEstatusDocto: vnewEstatusDocto,
					causaCancelacion: vcausaCancelacion,
					fileSize: vfileSize,
					nombreArchivo: vnombreArchivo
				}),
				callback: transmiteConfirmacion 
			});			
		}
	}
	
	// boton para Transmitir  la confirmaci�n de la cancelacion del documento		
	var procesoConfirmacion = function(store, arrRegistros, opts) 	{
		var  gridPreAcuse = Ext.getCmp('gridPreAcuse');
		var store = gridPreAcuse.getStore();		
		var ic_documento="";
                var ic_docs_ic_est_nue=[];
		var textoFirmado ="";
		
		store.each(function(record) {
			ic_documento = record.data['IC_DOCUMENTO'];
                        ic_docs_ic_est_nue.push(record.data['IC_DOCUMENTO']+'|'+record.data['IC_ESTATUS_NUEVO']);
			newEstatusDocto = record.data['IC_ESTATUS_NUEVO'];
			
			textoFirmado+="N�mero de Documento:" +record.data['NUM_DOCUMENTO']+"\n"+
							 "Estatus Actual:" +record.data['ESTATUS']+"\n"+
							 "Nuevo Estatus:" +record.data['ESTATUS_NUEVO']+"\n";
		});
		NE.util.obtenerPKCS7(fnProcesoConfirmacionCallback, textoFirmado, ic_docs_ic_est_nue, newEstatusDocto,
				Ext.getCmp('causaCancelacion1').getValue(), Ext.getCmp('fileSize').getValue(), 
				Ext.getCmp('nombreArchivo').getValue());
		
		/*var pkcs7 = NE.util.firmar(textoFirmado);
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnRegresar").enable();	
			Ext.getCmp("btnConfirmar").enable();	
			return;	//Error en la firma. Termina...
		}else  {		
			Ext.getCmp('btnRegresar').disable();
			Ext.getCmp('btnConfirmar').disable();	
				
			Ext.Ajax.request({
				url: '13consulta20ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'cancelaDocto',					
					textoFirmado:textoFirmado,
					pkcs7:pkcs7,
					ic_documento:ic_documento,
					newEstatusDocto:newEstatusDocto,
					causaCancelacion:Ext.getCmp('causaCancelacion1').getValue(),
					fileSize:Ext.getCmp('fileSize').getValue(),
					nombreArchivo:Ext.getCmp('nombreArchivo').getValue()
				}),
				callback: transmiteConfirmacion 
			});			
		}*/
	}
	
	
	// boton PreAcuse de  la confirmaci�n de la cancelacion del documento
	var procesarCancelacion = function(store, arrRegistros, opts) 	{

		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
		var jsonData = store.data.items;
		
		var totalDoc =0;
		var fechaDoctoFalse=0;
		var registrosPreAcu = []; //para agregar los registros al Preacuse  y acuse 
		var cargaArchivo=Ext.getCmp('cargaArchivo');
		var causaCancelacion1=Ext.getCmp('causaCancelacion1');
				
		store.each(function(record) {
				
			
			if(record.data['SELECCIONAR']=='S' ){		
				registrosPreAcu.push(record);					
				totalDoc++;
				
				if(record.data['DF_FECHA_DOCTO']==false){	
					fechaDoctoFalse++;
				}			
			}			
		});
		
		if(totalDoc==0) {
			Ext.MessageBox.alert('Mensaje','Por favor seleccione un documento para la cancelaci�n.');
			return;
		}
		
		if(fechaDoctoFalse>0) {
			Ext.MessageBox.alert('Mensaje','No es posible cancelar este registro debido a la fecha de Autorizaci�n de lF');
			return;
		}
		
		if (Ext.isEmpty(cargaArchivo.getValue()) ){
			cargaArchivo.markInvalid('Por favor indique el archivo a cargar.');
			return;
		}
		if (Ext.isEmpty(causaCancelacion1.getValue()) ){
			causaCancelacion1.markInvalid('Por favor escriba la causa de la cancelaci�n.');
			return;
		}
		
		var fp = Ext.getCmp('formaCambio');
		fp.el.mask('Procesando Carga de Archivo..', 'x-mask-loading');	
		fp.getForm().submit({
			url: '13consulta20ext_file.jsp',									
			waitMsg: 'Enviando datos...',
			waitTitle :'Por favor, espere',			
			success: function(form, action) {								
				var resp = action.result;
				var mArchivo =resp.archivo;
				var error_tam = resp.error_tam;	
				var fileSize = resp.fileSize;
				if(mArchivo!='') {
						
					consultaPreAcuseData.add(registrosPreAcu);
					consultaAcuseData.add(registrosPreAcu);
					Ext.getCmp('gridPreAcuse').show();
					Ext.getCmp('gridConsulta').hide();
					Ext.getCmp('formaCambio').hide();
					Ext.getCmp('forma').hide();
					Ext.getCmp('nombreArchivo').setValue(mArchivo);					
					Ext.getCmp('fileSize').setValue(fileSize);
				}	else{
					Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB.");
					return;
				}
			},
			failure: NE.util.mostrarSubmitError
		});
		fp.el.unmask();		
	}
	
	
//*******************Componente para mostrar mensaje  de error o autentificaci�n *********************************
		
	var mensajeAutorizacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutorizacion',							
		width:	'500',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeAuto', 	value: '' }				
		]
	});
	
	
	//*******************GRID DE CONSULTA ACUSE *********************************
	
	var consultaAcuseData = new Ext.data.ArrayStore({
		fields: [	
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'NUM_SIRAC'},
			{name: 'NOMBRE_PYME'},
			{name: 'NUM_DOCUMENTO'},
			{name: 'ESTATUS'},
			{name: 'IC_ESTATUS'},
			{name: 'MONEDA'},
			{name: 'MONTO_DOCUMENTO'},
			{name: 'PORCE_DESCUENTO'},
			{name: 'RECURSO_GARANTIA'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'SELECCIONAR' },
			{name: 'DF_FECHA_DOCTO', convert: NE.util.string2boolean},
			{name: 'FECHA_SOLICITUD'},
			{name: 'IC_ESTATUS_NUEVO'},
			{name: 'ESTATUS_NUEVO'}, 
			{name: 'ESTATUS_NUEVO_A'},
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_FIDEICOMISO'},	
			{name: 'MONTO_INTERES_FONDEO'},	
			{name: 'MONTO_OPERAR_FONDEO'}
		]
	});


	var gruposAcuse = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 10, align: 'center'},
					{header: 'INTERMEDIARIO FINANCIERO', colspan: 3, align: 'center'},
					{header: 'FIDEICOMISO', colspan: 3, align: 'center'}					
				]
			]
	});
	
	var gridAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridAcuse',				
		store: consultaAcuseData,	
		style: 'margin:0 auto;',
		title:'Cancelar Transacci�n',
		hidden: true,
		plugins: gruposAcuse,
		clicksToEdit: 1,
		columns: [
			{
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'
			},
			{
				header : 'Num Sirac',
				tooltip: 'Num Sirac',
				dataIndex : 'NUM_SIRAC',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header : 'PYME',
				tooltip: 'PYME',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'left'
			},
			{
				header : 'Num. Documento',
				tooltip: 'Num. Documento',
				dataIndex : 'NUM_DOCUMENTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS_NUEVO_A',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'center'
			},
			{
				header : 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex : 'MONTO_DOCUMENTO',
				sortable: true,
				width: 130,
				resizable: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORCE_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex : 'RECURSO_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontara',
				dataIndex : 'MONTO_DESCONTAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},
			{							
				header : 'Intereses del documento',
				tooltip: 'Intereses del documento',
				dataIndex : 'MONTO_INTERES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto a Recibir',
				tooltip: 'Monto a Recibir',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Nombre Fideicomiso',
				tooltip: 'Nombre Fideicomiso',
				dataIndex : 'NOMBRE_FIDEICOMISO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'MONTO_INTERES_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',				
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('MONTO_INTERES_FONDEO') =='0') {
						return 'N/A ';					
					}else  {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('MONTO_OPERAR_FONDEO') =='0') {
						return 'N/A ';					
					}else  {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			}
		],					
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',			
				{
					xtype: 'button',
					text: 'Imprimir',					
					tooltip:	'Imprimir ',
					iconCls: 'icoPdf',
					id: 'btnImprimir',					
					handler: generarPDF
				},
				{
					xtype: 'button',
					text: 'Salir',					
					tooltip:	'Salir ',
					iconCls: 'icoLimpiar',
					id: 'btnSalir',					
					handler: function(boton, evento) {	
						window.location = '13consulta20ext.jsp';	
					}
				}
			]
		}
	});
	//*******************GRID DE CONSULTA PREACUSE *********************************
	
	var consultaPreAcuseData = new Ext.data.ArrayStore({
		fields: [	
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'NUM_SIRAC'},
			{name: 'NOMBRE_PYME'},
			{name: 'NUM_DOCUMENTO'},
			{name: 'ESTATUS'},
			{name: 'IC_ESTATUS'},
			{name: 'MONEDA'},
			{name: 'MONTO_DOCUMENTO'},
			{name: 'PORCE_DESCUENTO'},
			{name: 'RECURSO_GARANTIA'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'SELECCIONAR' },
			{name: 'DF_FECHA_DOCTO', convert: NE.util.string2boolean},
			{name: 'FECHA_SOLICITUD'},
			{name: 'IC_ESTATUS_NUEVO'},
			{name: 'ESTATUS_NUEVO'}, 
			{name: 'ESTATUS_NUEVO_A'},
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_FIDEICOMISO'},	
			{name: 'MONTO_INTERES_FONDEO'},	
			{name: 'MONTO_OPERAR_FONDEO'}
		]
	});


	var gruposPre = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 11, align: 'center'},
					{header: 'INTERMEDIARIO FINANCIERO', colspan: 3, align: 'center'},
					{header: 'FIDEICOMISO', colspan: 3, align: 'center'}					
				]
			]
	});
	
	var gridPreAcuse = new Ext.grid.EditorGridPanel({
		id: 'gridPreAcuse',				
		store: consultaPreAcuseData,	
		style: 'margin:0 auto;',
		title:'Cancelar Transacci�n',
		hidden: true,
		plugins: gruposPre,
		clicksToEdit: 1,
		columns: [
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Num Sirac',
				tooltip: 'Num Sirac',
				dataIndex : 'NUM_SIRAC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'PYME',
				tooltip: 'PYME',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Num. Documento',
				tooltip: 'Num. Documento',
				dataIndex : 'NUM_DOCUMENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Estatus (Actual)',
				tooltip: 'Estatus (Actual)',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Estatus (Nuevo)',
				tooltip: 'Estatus (Nuevo)',
				dataIndex : 'ESTATUS_NUEVO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},				
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex : 'MONTO_DOCUMENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORCE_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex : 'RECURSO_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontara',
				dataIndex : 'MONTO_DESCONTAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},
			{							
				header : 'Intereses del documento',
				tooltip: 'Intereses del documento',
				dataIndex : 'MONTO_INTERES',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto a Recibir',
				tooltip: 'Monto a Recibir',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Nombre Fideicomiso',
				tooltip: 'Nombre Fideicomiso',
				dataIndex : 'NOMBRE_FIDEICOMISO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'MONTO_INTERES_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',				
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('MONTO_INTERES_FONDEO') =='0') {
						return 'N/A ';					
					}else  {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('MONTO_OPERAR_FONDEO') =='0') {
						return 'N/A ';					
					}else  {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			}
		],					
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Regresar',					
					tooltip:	'Regresar ',
					iconCls: 'icoLimpiar',
					id: 'btnRegresar',
					handler: function(boton, evento) {	
						window.location = '13consulta20ext.jsp';	
					}
				},
				{
					xtype: 'button',
					text: 'Confirmar',					
					tooltip:	'Confirmar ',
					iconCls: 'icoContinuar',
					id: 'btnConfirmar',
					handler: procesoConfirmacion
				}
			]
		}
	});
	
	
	//*******************GRID DE CONSULTA PRINICIPAL *******************************

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();

		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			//edito el titulo de la columna
			var cm = gridConsulta.getColumnModel();
			var jsonData = store.reader.jsonData;
			var formaCambio = Ext.getCmp('formaCambio');

			Ext.getCmp('total_grid').setValue(store.getTotalCount()); // Este campo se llena porque no puedo obtener el getTotalCount() directamente del m�todo

			if(store.getTotalCount() > 0) {
				formaCambio.show();
				el.unmask();
			} else {
				formaCambio.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '13consulta20ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: true,
		fields: [
			{name: 'IC_DOCUMENTO'},
			{name: 'NOMBRE_EPO'},
			{name: 'NUM_SIRAC'},
			{name: 'NOMBRE_PYME'},
			{name: 'NUM_DOCUMENTO'},
			{name: 'ESTATUS'},
			{name: 'IC_ESTATUS'},
			{name: 'MONEDA'},
			{name: 'MONTO_DOCUMENTO'},
			{name: 'PORCE_DESCUENTO'},
			{name: 'RECURSO_GARANTIA'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'MONTO_INTERES'},
			{name: 'MONTO_OPERAR'},
			{name: 'SELECCIONAR'},
			{name: 'DF_FECHA_DOCTO', convert: NE.util.string2boolean},
			{name: 'FECHA_SOLICITUD'},
			{name: 'IC_ESTATUS_NUEVO'},
			{name: 'ESTATUS_NUEVO'},
			{name: 'ESTATUS_NUEVO_A'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_FIDEICOMISO'},
			{name: 'MONTO_INTERES_FONDEO'},
			{name: 'MONTO_OPERAR_FONDEO'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad: {fn: function(store, options){
				// Envi� manualmente el getTotalCount() del grid para que no haya errores de paginaci�n.
				Ext.apply(options.params,{
					total: Ext.getCmp('total_grid').getValue()
				});
			}},
			load: procesarConsultaData,
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaData(null, null, null);
					}
				}
			}
	});

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: '', colspan: 10, align: 'center'},
					{header: 'INTERMEDIARIO FINANCIERO', colspan: 3, align: 'center'},
					{header: 'FIDEICOMISO', colspan: 3, align: 'center'},	
					{header: '', colspan: 1, align: 'center'}	
				]
			]
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',				
		store: consultaData,	
		style: 'margin:0 auto;',
		title:'Cancelar Transacci�n',
		hidden: true,
		plugins: grupos,
		clicksToEdit: 1,
		columns: [
			{							
				header : 'EPO',
				tooltip: 'EPO',
				dataIndex : 'NOMBRE_EPO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Num SiracE',
				tooltip: 'Num Sirac',
				dataIndex : 'NUM_SIRAC',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'PYME',
				tooltip: 'PYME',
				dataIndex : 'NOMBRE_PYME',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},
			{							
				header : 'Num. Documento',
				tooltip: 'Num. Documento',
				dataIndex : 'NUM_DOCUMENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex : 'MONTO_DOCUMENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORCE_DESCUENTO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Recurso en Garant�a',
				tooltip: 'Recurso en Garant�a',
				dataIndex : 'RECURSO_GARANTIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontara',
				dataIndex : 'MONTO_DESCONTAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'MONTO_INTERES',
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{							
				header : 'Nombre Fideicomiso',
				tooltip: 'Nombre Fideicomiso',
				dataIndex : 'NOMBRE_FIDEICOMISO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'				
			},
			{							
				header : 'Monto Interes',
				tooltip: 'Monto Interes',
				dataIndex : 'MONTO_INTERES_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',				
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('MONTO_INTERES_FONDEO') =='0') {
						return 'N/A ';					
					}else  {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			},
			{							
				header : 'Monto a Operar',
				tooltip: 'Monto a Operar',
				dataIndex : 'MONTO_OPERAR_FONDEO',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'right',
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
					if(registro.get('MONTO_OPERAR_FONDEO') =='0') {
						return 'N/A ';					
					}else  {
						return Ext.util.Format.number(value,'$0,0.00');
					}
				}
			},				
			{
				header: 'Seleccionar',
				dataIndex:'SELECCIONAR',
				align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store) {
					//return "<input onclick='radioSELECCIONAR(this,"+rowIndex+","+colIndex+")' type='checkbox' name = 'primaryRadio' " + (value ? "checked='checked'" : "") + ">";
                                    if(record.data['SELECCIONAR'] == ''){
                                                    if(record.data['SELECCIONAR']=='checked' ){
                                                            return '<input  id="primaryRadio" type="checkbox" checked  onclick="checkSELECCIONAR(this, '+rowIndex +','+colIndex+');" />';
                                                    }else{
                                                            return '<input  id="primaryRadio" type="checkbox"  onclick="checkSELECCIONAR(this, '+rowIndex +','+colIndex+');" />';
                                                    }
                                    
                                    }else{
                                            if(record.data['SELECCIONAR'] == 'S'){
                                                    return '<input  id="primaryRadio"  type="checkbox" checked  onclick="checkSELECCIONAR(this, '+rowIndex +','+colIndex+');" />';
                                            }else{
                                                    return '<input  id="primaryRadio" type="checkbox"  onclick="checkSELECCIONAR(this, '+rowIndex +','+colIndex+');" />';
                                            }
                                    }
				}
			},
			{
				header: 'DF_FECHA_DOCTO',
				dataIndex:'DF_FECHA_DOCTO',
				align: 'center',
				hidden:true
			}			
		],					
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros."
		}
	});


//*******************Forma de Carga de Documento y causa de cancelaci�n *********************************
		
	var elementosFormaCambio =[	
		{
			xtype: 'fileuploadfield',
			id: 'cargaArchivo',
			emptyText: 'Documento de Autorizaci�n',
			fieldLabel: 'Documento de Autorizaci�n',
			name: 'cargaArchivo_',   
			buttonText: ' ',
			width: 500,	  
			buttonCfg: {
				iconCls: 'upload-icon'
			}
		},
		{
			xtype: 'textfield',
			fieldLabel: '*Causas de la Cancelaci�n',		
			name: 'causaCancelacion',
			id: 'causaCancelacion1',
			allowBlank: true,
			maxLength: 50,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		}
	];

	var fpCambio = new Ext.form.FormPanel({
		id: 'formaCambio',
		width: 700,
		frame: true,	
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 180,
		defaultType: 'textfield',		
		defaults: {	msgTarget: 'side', 	anchor: '-20' 	},		
		items: elementosFormaCambio,			
		buttons: [		
			{
				text: 'Cambiar Estatus ',
				id: 'btnCambiaEstatusr',
				iconCls: 'icoContinuar',
				formBind: true,		
				handler: procesarCancelacion
			}
		]
	});
	
//*********************** Criterios de Busqueda ********************************
	var catalogoIF = new Ext.data.JsonStore({
		id: 'catalogoIF',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta20ext.data.jsp',
		baseParams: {
			informacion: 'catalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta20ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var catalogoBancoFondeo = new Ext.data.JsonStore({
		id: 'catalogoBancoFondeo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta20ext.data.jsp',
		baseParams: {
			informacion: 'catalogoBancoFondeo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var successAjaxFn = function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);			
			Ext.getCmp('txtNombre1').setValue(jsonObj.txtNombre);	
			Ext.getCmp('ic_pyme').setValue(jsonObj.ic_pyme);	
			
			if(jsonObj.txtNombre==''){
				Ext.MessageBox.alert("Mensaje","El nafin electronico no corresponde a una PyME o no existe");
				Ext.getCmp('num_electronico1').setValue('');	
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarStoreBusqAvanzPyme= function(store, records, oprion){
		if(store.getTotalCount()==0 ){
			Ext.MessageBox.alert("Mensaje","No existe informaci�n en los criterios determinados" );
		}
	}	
		
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta20ext.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarStoreBusqAvanzPyme,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0' 
		},
		{
			xtype: 'textfield',
			name: 'noPyme',
			id: 'noPyme1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 15,	
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0' 
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);		
							var ic_epo = Ext.getCmp("ic_epo1").getValue();	
													
							storeBusqAvanzPyme.load({
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
										ic_epo:ic_epo																				
									})																	
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}	
	];
	
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var num_electronico1 = Ext.getCmp('num_electronico1');
					var txtNombre1 = Ext.getCmp('txtNombre1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					var a = new Array();
					a = record.split(" ",1);
					var nombre = record.substring(a[0].length,record.length);
										
					num_electronico1.setValue(cmbPyme.getValue());
					txtNombre1.setValue(nombre);					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();				
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}				
			}
		]
	});


	var elementosForma =[{
		xtype:                   'combo',
		fieldLabel:              'EPO',
		name:                    'ic_epo',
		id:                      'ic_epo1',
		mode:                    'local',
		autoLoad:                false,
		displayField:            'descripcion',
		emptyText:               'Seleccionar ...',
		valueField:              'clave',
		hiddenName:              'ic_epo',
		forceSelection:          true,
		triggerAction:           'all',
		typeAhead:               true,
		minChars:                1,
		store:                   catalogoEPO,
		tpl:                     NE.util.templateMensajeCargaCombo,
		listeners: {
			select: {
				fn: function(combo) {
					Ext.getCmp('num_electronico1').enable();
					var cmbIF= Ext.getCmp('ic_if1');
					cmbIF.setValue('');
					cmbIF.store.load({
						params: {
							ic_epo:combo.getValue()
						}
					});
				}
			}
		}
	},{
		xtype:                   'combo',
		fieldLabel:              'Nombre de el IF',
		name:                    'ic_if',
		id:                      'ic_if1',
		mode:                    'local',
		autoLoad:                false,
		displayField:            'descripcion',
		emptyText:               'Seleccionar ...',
		valueField:              'clave',
		hiddenName:              'ic_if',
		forceSelection:          true,
		triggerAction:           'all',
		typeAhead:               true,
		minChars:                1,
		store:                   catalogoIF,
		tpl:                     NE.util.templateMensajeCargaCombo
	},{
		xtype:                   'compositefield',
		fieldLabel:              'NE de la PyME',
		combineErrors:           false,
		msgTarget:               'side',
		items: [{
			xtype:                'numberfield',
			name:                 'num_electronico',
			id:                   'num_electronico1',
			fieldLabel:           'N�mero Nafin Electr�nico',
			allowBlank:           true,
			disabled:             true,
			maxLength:            15,
			width:                80,
			msgTarget:            'side',
			margins:              '0 20 0 0',
			listeners: {
				'blur': function(){
					// Petici�n b�sica
					Ext.Ajax.request({
						url: '13consulta20ext.data.jsp',
						method: 'POST',
						callback: successAjaxFn,
						params: {
							informacion: 'pymeNombre',
							num_electronico: Ext.getCmp('num_electronico1').getValue(),
							ic_epo:Ext.getCmp("ic_epo1").getValue()
						}
					});
				}
			}
		},{
			xtype:                'textfield',
			name:                 'txtNombre',
			id:                   'txtNombre1',
			allowBlank:           true,
			maxLength:            100,
			width:                300,
			disabled:             true,
			msgTarget:            'side',
			margins:              '0 20 0 0'
		},{
			xtype:                'button',
			text:                 'B�squeda Avanzada...',
			id:                   'btnBusqAv',
			handler: function(boton, evento) {
				var ic_epo = Ext.getCmp("ic_epo1");
				if (Ext.isEmpty(ic_epo.getValue()) ){
					ic_epo.markInvalid('Debe de Capturar una EPO.');
					return;
				}
				var ventana = Ext.getCmp('winBusqAvan');
				if (ventana) {
					ventana.show();
				} else {
					new Ext.Window({
						title:       'B�squeda Avanzada',
						layout:      'fit',
						width:       400,
						height:      300,
						minWidth:    400,
						minHeight:   300,
						buttonAlign: 'center',
						id:          'winBusqAvan',
						closeAction: 'hide',
						items:       fpBusqAvanzada
					}).show();
				}
			}
		}]
	},{
		xtype:                   'compositefield',
		fieldLabel:              'Fecha Operaci�n',
		combineErrors:           false,
		msgTarget:               'side',
		items: [{
			xtype:                'datefield',
			name:                 'df_fecha_seleccion_de',
			id:                   'df_fecha_seleccion_de',
			allowBlank:           true,
			startDay:             0,
			width:                100,
			msgTarget:            'side',
			vtype:                'rangofecha',
			campoFinFecha:        'df_fecha_seleccion_a',
			margins:              '0 20 0 0'
		},{
			xtype:                'displayfield',
			value:                'al',
			width:                20
		},{
			xtype:                'datefield',
			name:                 'df_fecha_seleccion_a',
			id:                   'df_fecha_seleccion_a',
			allowBlank:           true,
			startDay:             1,
			width:                100,
			msgTarget:            'side',
			vtype:                'rangofecha',
			campoInicioFecha:     'df_fecha_seleccion_de',
			margins:              '0 20 0 0'
		}]
	},{
		xtype:                   'compositefield',
		fieldLabel:              'Fecha Autorizaci�n IF',
		combineErrors:           false,
		msgTarget:               'side',
		items: [{
			xtype:                'datefield',
			name:                 'df_fecha_autorizaif_de',
			id:                   'df_fecha_autorizaif_de',
			allowBlank:           true,
			startDay:             0,
			width:                100,
			msgTarget:            'side',
			vtype:                'rangofecha',
			campoFinFecha:        'df_fecha_autorizaif_a',
			margins:              '0 20 0 0'
		},{
			xtype:                'displayfield',
			value:                'al',
			width:                20
		},{
			xtype:                'datefield',
			name:                 'df_fecha_autorizaif_a',
			id:                   'df_fecha_autorizaif_a',
			allowBlank:           true,
			startDay:             1,
			width:                100,
			msgTarget:            'side',
			vtype:                'rangofecha',
			campoInicioFecha:     'df_fecha_autorizaif_de',
			margins:              '0 20 0 0'
		}]
	},{
		xtype:                   'compositefield',
		fieldLabel:              'N�mero Documento',
		combineErrors:           false,
		msgTarget:               'side',
		items: [{
			xtype:                'textfield',
			fieldLabel:           'N�mero Documento',
			name:                 'ig_numero_docto',
			id:                   'ig_numero_docto',
			allowBlank:           true,
			maxLength:            20,
			width:                200,
			msgTarget:            'side',
			margins:              '0 20 0 0'
		}]
	},
	{ xtype: 'displayfield', hidden: true, id: 'total_grid',    value: '' },
	{ xtype: 'displayfield', hidden: true, id: 'ic_pyme',       value: '' },
	{ xtype: 'displayfield', hidden: true, id: 'nombreArchivo', value: '' },
	{ xtype: 'displayfield', hidden: true, id: 'fileSize',      value: '' },
	{ xtype: 'displayfield', hidden: true, id: 'ic_folio',      value: '' }
	];


	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 800,
		title: 'Criterios de Busqueda',
		frame: true,		
		fileUpload: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,		
				handler: function(boton, evento) {
				
					var df_fecha_seleccion_de=Ext.getCmp('df_fecha_seleccion_de');
					var df_fecha_seleccion_a=Ext.getCmp('df_fecha_seleccion_a');
					
					var df_fecha_autorizaif_de=Ext.getCmp('df_fecha_autorizaif_de');
					var df_fecha_autorizaif_a=Ext.getCmp('df_fecha_autorizaif_a');
					
					var ic_epo=Ext.getCmp('ic_epo1');
									
					if (Ext.isEmpty(ic_epo.getValue()) ){
						ic_epo.markInvalid('Seleccione la EPO.');
						return;
					}
					
					if(!Ext.isEmpty(df_fecha_seleccion_de.getValue()) || !Ext.isEmpty(df_fecha_seleccion_a.getValue())){
						if(Ext.isEmpty(df_fecha_seleccion_de.getValue())){
							df_fecha_seleccion_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_seleccion_de.focus();
							return;
						}
						if(Ext.isEmpty(df_fecha_seleccion_a.getValue())){
							df_fecha_seleccion_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_seleccion_a.focus();
							return;
						}
					}
					
						if(!Ext.isEmpty(df_fecha_autorizaif_de.getValue()) || !Ext.isEmpty(df_fecha_autorizaif_a.getValue())){
						if(Ext.isEmpty(df_fecha_autorizaif_de.getValue())){
							df_fecha_autorizaif_de.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_autorizaif_de.focus();
							return;
						}
						if(Ext.isEmpty(df_fecha_autorizaif_a.getValue())){
							df_fecha_autorizaif_a.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
							df_fecha_autorizaif_a.focus();
							return;
						}
					}
				
					var df_fecha_seleccion_de_ = Ext.util.Format.date(df_fecha_seleccion_de.getValue(),'d/m/Y');
					var df_fecha_seleccion_a_ = Ext.util.Format.date(df_fecha_seleccion_a.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(df_fecha_seleccion_de.getValue())){
						if(!isdate(df_fecha_seleccion_de_)) { 
							df_fecha_seleccion_de.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_fecha_seleccion_de.focus();
							return;
						}
					}
					if( !Ext.isEmpty(df_fecha_seleccion_a.getValue())){
						if(!isdate(df_fecha_seleccion_a_)) { 
							df_fecha_seleccion_a.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_fecha_seleccion_a.focus();
							return;
						}
					}
									
					var df_fecha_autorizaif_de_ = Ext.util.Format.date(df_fecha_autorizaif_de.getValue(),'d/m/Y');
					var df_fecha_autorizaif_a_ = Ext.util.Format.date(df_fecha_autorizaif_a.getValue(),'d/m/Y');
					
					if(!Ext.isEmpty(df_fecha_autorizaif_de.getValue())){
						if(!isdate(df_fecha_autorizaif_de_)) { 
							df_fecha_autorizaif_de.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_fecha_autorizaif_de.focus();
							return;
						}
					}
					if( !Ext.isEmpty(df_fecha_autorizaif_a.getValue())){
						if(!isdate(df_fecha_autorizaif_a_)) { 
							df_fecha_autorizaif_a.markInvalid("La fecha es incorrecta.Verifique que el formato sea dd/mm/aaaa");
							df_fecha_autorizaif_a.focus();
							return;
						}
					}
					
				
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{							
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15,
							ic_pyme:Ext.getCmp('ic_pyme').getValue()
						})
					});
					
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta20ext.jsp';								
				}		
			}	
		]
	});

	var pnl = new Ext.Container({		
		id: 			'contenedorPrincipal',
		applyTo: 	'areaContenido',
		width: 		949,
		height: 		'auto',
		style: 'margin:0 auto;',
		disabled: 	false,		
		items: [
			NE.util.getEspaciador(20),
			fp,	
			mensajeAutorizacion,
			NE.util.getEspaciador(20),
			gridConsulta,
			gridPreAcuse,
			gridAcuse,
			NE.util.getEspaciador(20),
			fpCambio,
			NE.util.getEspaciador(20)
		]
	});

	catalogoBancoFondeo.load();
	catalogoEPO.load();
	var formaCambio = Ext.getCmp('formaCambio');	
	formaCambio.hide();
});