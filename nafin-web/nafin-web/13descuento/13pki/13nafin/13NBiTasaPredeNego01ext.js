Ext.onReady(function() {
	
	Ext.form.Field.prototype.msgTarget = 'side';
	var tasas 		= [];
	var valorTasa  = [];
 
	//------------------------------- COMPONENTES ESPECIALES ------------------------------------------
	
	Ext.namespace('Ext.ux.form');
	/**
	 * @class Ext.ux.form.PuntosTasa
	 * @extends Ext.form.TextField
	 * TextField que especializado en la captura de los Puntos de la Tasa.
	 * @xtype PuntosTasa
	 * @author jshernandez
	 * @since ( 15/03/2013 01:01:22 p.m. )
	 */
	Ext.ux.form.PuntosTasa = Ext.extend(
		Ext.form.TextField,  
		{
			minValue:			0,
			maxValue:			99999.99999, 
			nanText: 			'{0} no es un n�mero v�lido',
			minText: 			"Debe ser mayor o igual a {0}",
			maxText:				"El valor m�ximo para este campo es {0}",
			decimalSeparator: ".",
			initComponent: function(){
				Ext.form.TextField.superclass.initComponent.call(this);
			},
			parseValue: function(value) {
				value = parseFloat(String(value).replace(this.decimalSeparator, "."));
				return isNaN(value) ? '' : value;
         },
			getErrors: function(value) {
				var errors = Ext.form.TextField.superclass.getErrors.apply(this, arguments);
			  
			  	value = Ext.isDefined(value) ? value : this.processValue(this.getRawValue());
			  
			  	if (value.length < 1) { // if it's blank and textfield didn't flag it then it's valid
					 return errors;
			  	}
 
			  	value = String(value).replace(this.decimalSeparator, ".");
 
			  	if(isNaN(value)){
			  	  errors.push(String.format(this.nanText, value));
			  	}
			  
			   var num = this.parseValue(value);
			  
				if (num < this.minValue) {
			  	  errors.push(String.format(this.minText, this.minValue));
				}
			  
				if (num > this.maxValue) {
					errors.push(String.format(this.maxText, this.maxValue));
				}
			  
				return errors;
			},
			beforeBlur: function() {
			  var v = this.parseValue(this.getRawValue());
				  
			  if (!Ext.isEmpty(v)) {
			  	  this.setValue(
			  	  	  Ext.util.Format.number(v, '.00000')
			  	  );
			  }
			}
		}
	);
	Ext.reg('puntostasa', Ext.ux.form.PuntosTasa);
	
	//----------------------------- VALIDACIONES ESPECIALES -------------------------------------------
	
	function soloNumeros(field,mensaje){
	
		var CadenaValida = "0123456789"; /*variable con caracteres validos*/
		
		if( Ext.isEmpty(field.getValue()) ){ return; } // Si est� vac�o salir
		
		var numero = String(field.getValue());
		for(var i=0;i<numero.length;i++){
			if(CadenaValida.indexOf(numero.charAt(i))==-1){
				field.markInvalid(mensaje);
				return false;
			}
		}
 
		return true;
		
	}
	
	//--------------------------------------------------------------------------------------
	var procesarPYME =  function(opts, success, response) { 
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			jsonData = Ext.util.JSON.decode(response.responseText);
			
			if(jsonData.sihayTasas =='S'){
				Ext.MessageBox.alert('Mensaje','El Proveedor seleccionado opera con Oferta de Tasas por Montos, es necesario borrar su parametrizaci�n para poder aplicar Tasa Preferencial');
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('icoXls');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
		} else {
			btnGenerarCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('icoImprimir');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma    = Ext.getDom('formAux');
				forma.action = NE.appWebContextRoot+'/DescargaArchivo';
				forma.method = 'post';
				forma.target = '_self';
				// Preparar Archivo a Descargar
				var archivo  = Ext.util.JSON.decode(response.responseText).urlArchivo;				
				archivo 		 = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
				// Insertar archivo
				var inputNombreArchivo = Ext.DomHelper.insertFirst(
					forma, 
					{ 
						tag: 	'input', 
						type: 'hidden', 
						id: 	'nombreArchivo', 
						name: 'nombreArchivo', 
						value: archivo
					},
					true
				); 
				// Solicitar Archivo Al Servidor
				forma.submit();
				// Remover nodo agregado
				inputNombreArchivo.remove();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var mensajeAutentificacion = new Ext.Container({
		layout: 	'table',		
		id:		'mensajeAutentificacion',							
		width:	'500',
		heigth:	'auto',
		style: 	'margin:0 auto;',
		hidden: 	true,
		layoutConfig: {
			columns: 1
		},
		items: [					
			{
				xtype:	'displayfield',
				id:		'autentificac',
				value: 	'',
				cls:		'x-form-item',
				style: { 
					width:   	'100%', 
					textAlign: 	'justify'
				}		
			},
			{ 	xtype: 'displayfield', 	hidden: true, id: 'recibo', 					value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'acuseFormateado', 		value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'fechaDeAutorizacion',	value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'horaDeAutorizacion',	value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'usuarioDeCaptura', 		value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'lsCadenaEpo', 			value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'lstIF', 					value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'lsCveMoneda', 			value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'lsPYME', 					value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'lsTipoTasa', 				value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'inplazos', 				value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'lstPlazo', 				value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'acuse', 					value: '' },
			{ 	xtype: 'displayfield', 	hidden: true, id: 'horaActual', 				value: '' }
		]
	});
	

	// Respuesta de Guardar de  tasas Prenegociables /Negociables 
	function procesarSuccessFailureGuardar(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			jsonData = Ext.util.JSON.decode(response.responseText);
			var fp 	= Ext.getCmp('forma');
			//fp.el.unmask();        
			gridConsulta.el.unmask();
			
			Ext.getCmp('panelModoPantalla').hide();
			fp.hide();
			gridConsulta.hide();
 
			Ext.getCmp('recibo').setValue(jsonData.recibo);
			Ext.getCmp('acuseFormateado').setValue(jsonData.acuseFormateado);
			Ext.getCmp('fechaDeAutorizacion').setValue(jsonData.fechaDeAutorizacion);
			Ext.getCmp('horaDeAutorizacion').setValue(jsonData.horaDeAutorizacion);
			Ext.getCmp('usuarioDeCaptura').setValue(jsonData.usuarioDeCaptura);
			Ext.getCmp('lsCadenaEpo').setValue(jsonData.lsCadenaEpo);
			Ext.getCmp('lstIF').setValue(jsonData.lstIF);
			Ext.getCmp('lsCveMoneda').setValue(jsonData.lsCveMoneda);
			Ext.getCmp('lsPYME').setValue(jsonData.lsPYME);
			Ext.getCmp('lsTipoTasa').setValue(jsonData.lsTipoTasa);
			Ext.getCmp('inplazos').setValue(jsonData.inplazos);
			Ext.getCmp('lstPlazo').setValue(jsonData.lstPlazo);
			Ext.getCmp('acuse').setValue(jsonData.acuse);
			Ext.getCmp('horaActual').setValue(jsonData.horaActual);
			
			if( jsonData.acuse == 'N' ){			
				
				var mensajeAutentificacion = Ext.getCmp('mensajeAutentificacion');	
				Ext.getCmp("autentificac").setValue(jsonData.mensajeAutentificacion);
				mensajeAutentificacion.show();
				
			}
			if( jsonData.acuse != 'N' ){
				
				gridCifrasControl.setTitle('Recibo N�mero: '+jsonData.recibo);
				var acuseCifras = [
					['N�mero de Acuse', 			jsonData.acuseFormateado		],
					['Fecha de Autorizaci�n',	jsonData.fechaDeAutorizacion	],
					['Hora de Autorizaci�n',	jsonData.horaDeAutorizacion	],
					['Usuario de Captura', 		jsonData.usuarioDeCaptura		]
				];
				storeCifrasData.loadData(acuseCifras);	
				gridCifrasControl.show();
				
				consultaAcuseData.load({
					params: {
						informacion: 	'ConsultarAcuse',
						lsCadenaEpo:	jsonData.lsCadenaEpo,
						lstIF: 			jsonData.lstIF,
						lsCveMoneda: 	jsonData.lsCveMoneda,
						lsPYME: 			jsonData.lsPYME,
						lsTipoTasa: 	jsonData.lsTipoTasa,
						lstPlazo:		jsonData.lstPlazo,
						acuse: 			jsonData.acuse
					}
				});
				gridAcuse.show();

			}				
			
		} else {
			
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
 
	 var confirmarAcuse = function(pkcs7,  textoFirmar,   parametros   ){	
	 
		if (Ext.isEmpty(pkcs7)) {		
			return;
		}else  {
		
			parametros.Pkcs7  = pkcs7;
			
			Ext.Ajax.request({
				url: 			'13NBiTasaPredeNego01ext.data.jsp',
				params: 		parametros,				
				callback: 	procesarSuccessFailureGuardar
			});
			
		}
	 }
 
 
 
	// Funcion para Guardar tasas Preferenciales/Negociables 
	var procesarGuardar  = function() {
		
			var tipoTasa		= Ext.getCmp("comboTipoDeTasa").getValue();
			var gridConsulta	= Ext.getCmp("gridConsulta");
			var consultaData  = gridConsulta.getStore();
			var textoFirmar   = "";
			
			// 1. VALIDAR CAMPOS DE LA TASA PREFERENCIAL
			if ( tipoTasa === 'P' ){
			
				// 1.0 VALIDAR SI EL PROVEEDOR OPERA CON OFERTA DE TASAS, EN CASO DE SER AFIRMATIVO CANCELAR LA OPERACION
				var operaConOfertaDeTasas = Ext.getCmp("operaConOfertaDeTasas").getValue();
				if( operaConOfertaDeTasas === "true" ){
					Ext.Msg.alert("Aviso",Ext.getCmp("operaConOfertaDeTasasMsg").getValue());	
					return;
				}
				
				var puntos 	= parseFloat( gridConsulta.params.txtPuntos );

				// 1.1 VALIDAR QUE LOS PUNTOS CAPTURADOS NO SEAN MAYORES AL VALOR DE CADA UNA DE LAS TASAS.
				var valorPuntosEsMayor = false;
				var nombreTasaMayor    = null;
				consultaData.each( function(record){					
					if( puntos > parseFloat( record.data['VALOR'] ) ){
						valorPuntosEsMayor 	= true;
						nombreTasaMayor 		= record.data['TIPO_TASA'];
						return false;
					}
				});
				
								
				if( valorPuntosEsMayor ) {
					// Nota: se ajust� la redaccion, para que el usuario pueda indenficar facilmente la tasa.
					Ext.Msg.alert("Aviso","La tasa: " + nombreTasaMayor + " no debe ser mayor al Valor de la Tasa Actual.");	
					return;
				}
				
				// 1.2 PREPARAR CADENA DE FIRMADO
				textoFirmar = "EPO Relacionada|Referencia|Tipo Tasa|Valor|Rel. Mat|Puntos|Valor Tasa";
				consultaData.each( function(record){
					textoFirmar += record.data['CAD_TASA'];
				});
				
			// 2. VALIDAR CAMPOS DE LA TASA NEGOCIADA
			} else if ( tipoTasa === 'N' )  {
				
				// 2.1 VALIDAR QUE SE HAYA ESPECIFICADO EL VALOR DE TODAS LAS TASAS NEGOCIADAS
				var faltaEspecificarTasa = false;
				consultaData.each( function(record){
					if( Ext.isEmpty( record.data['VALOR_TASA'] ) ){
						faltaEspecificarTasa	= true;
						return false;
					}
				});
				if( faltaEspecificarTasa ) {
					Ext.Msg.alert("Aviso","Debes de especificar el valor de todas las tasas negociadas.");	
					return;
				}
				
				// 2.2 PREPARAR CADENA DE FIRMADO
				textoFirmar = "EPO Relacionada|Referencia|Tipo Tasa|Valor|Fecha|Valor Tasa";
				consultaData.each( function(record){
					textoFirmar += record.data['CAD_TASA'] + "|" +record.data['VALOR_TASA'];
				});
 
			} 
			// 3. FIRMAR TASAS						
			
			// 4. GUARDAR TASAS			
			
			// Preparar parametros
			var parametros 			= new Object();
			Ext.apply( parametros, gridConsulta.params );
			// Configurar parametros
			parametros.informacion	= 'GuardarTasas';
			parametros.proceso 		= "";
			// parametros.tasas:		tasas, 
			parametros.TextoFirmado = textoFirmar;
			
			// Recolectar el valor de los puntos
			if( parametros.tipoTasa === 'P' ){
				var arregloPuntos			 		= new Array();
				consultaData.each( function(record){
					arregloPuntos.push(record.data['LD_PUNTOS']); 
				});
				parametros.arregloPuntos 		= Ext.encode(arregloPuntos);
			}
			// Recolectar el valor de las tasas
			var arregloTasas			 		= new Array();
			consultaData.each( function(record){
				arregloTasas.push(record.data['TASAS']); 
			});
			parametros.arregloTasas  		= Ext.encode(arregloTasas);
			// Recolectar el valor de los campos Valor Tasa
			if( parametros.tipoTasa === 'N' ){
				var arregloValorTasas	 		= new Array();
				consultaData.each( function(record){
					arregloValorTasas.push(record.data['VALOR_TASA']); 
				});
				parametros.arregloValorTasas  = Ext.encode(arregloValorTasas);
			}
						
			
			NE.util.obtenerPKCS7(confirmarAcuse, textoFirmar,   parametros    );
			
	}
	 
	//--------------------------- GRID CIFRAS DE CONTROL ---------------------------
	 
	var storeCifrasData = new Ext.data.ArrayStore({
		fields: [
			 {name: 'etiqueta'		},
			 {name: 'informacion'	}
		],
		autoDestroy: true
	});
	 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 				'gridCifrasControl',
		store: 			storeCifrasData,
		margins: 		'20 0 0 0',
		hideHeaders: 	true,
		hidden: 			true,
		title: 			'Datos cifras de control',
		align: 			'center',
		columns: [
			{
				header: 		'Etiqueta',
				dataIndex: 	'etiqueta',
				width: 		200,
				sortable: 	true
			},
			{
				header: 		'Informacion',			
				dataIndex: 	'informacion',
				width: 		350,
				sortable: 	true,
				renderer:	function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			}
		],
		stripeRows: 	true,
		columnLines:	true,
		loadMask:		true,
		width:			560,
		height:			150,
		style:			'margin:0 auto;',		
		frame:			true
	});
	
	//------------------------------ GRID ACUSE -----------------------------
	
	var epoRelacionadaRenderer01 	= function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var nombrePymeRenderer01 		= function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var tipoTasaRenderer01 			= function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var consultaAcuseData = new Ext.data.JsonStore({
		root: 	'registros',
		url: 		'13NBiTasaPredeNego01ext.data.jsp',
		baseParams: {
			informacion: 'ConsultarAcuse'
		},
		hidden: 	true,
		fields:	[	
			{ name: 'EPO_RELACIONADA'	},	
			{ name: 'NOMBRE_PYME'		},		
			{ name: 'REFERENCIA'			},
			{ name: 'TIPO_TASA'			},	
			{ name: 'PLAZO'				},				
			{ name: 'VALOR'				},				
			{ name: 'VALOR_TASA'			}
		],	
		totalProperty: 	'total',
		messageProperty:	'msg',
		autoLoad: 			false,		
		listeners: {				
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);							
					}
				}
		},
		autoDestroy: 		true			
	});
 
	var gridAcuse = new Ext.grid.GridPanel({
		id: 		'gridAcuse',				
		store: 	consultaAcuseData,	
		style: 	'margin:0 auto;',
		title:	'Tasas Preferenciales/Negociadas',
		hidden: 	true,
		columns: [
			{							
				header: 		'EPO Relacionada',
				tooltip: 	'EPO_RELACIONADA',
				dataIndex:	'EPO_RELACIONADA',
				sortable: 	true,
				width: 		130,
				resizable:	true,				
				align: 		'left',
				renderer:	epoRelacionadaRenderer01
			},
			{							
				header: 		'Nombre de la PYME',
				tooltip: 	'Nombre de la PYME',
				dataIndex: 	'NOMBRE_PYME',
				sortable: 	true,
				width: 		130,
				resizable: 	true,				
				align: 		'left',
				renderer:	nombrePymeRenderer01
			},
			{							
				header: 		'Referencia',
				tooltip: 	'REFERENCIA',
				dataIndex: 	'REFERENCIA',
				sortable: 	true,
				width: 		130,
				resizable: 	true,				
				align: 		'center'	
			},
			{							
				header: 		'Tipo de Tasa',
				tooltip:		'TIPO_TASA',
				dataIndex:	'TIPO_TASA',
				sortable:	true,
				width: 		130,
				resizable: 	true,				
				align: 		'center',
				renderer:	tipoTasaRenderer01
			},
			{							
				header: 		'Plazo',
				tooltip: 	'PLAZO',
				dataIndex: 	'PLAZO',
				sortable: 	true,
				width: 		130,
				resizable:	true,				
				align: 		'center'	
			},
			{							
				header: 		'Valor',
				tooltip: 	'Valor',
				dataIndex:	'VALOR',
				sortable: 	true,
				width: 		130,
				resizable:	true,				
				align: 		'center'	
			},			
			{							
				header: 		'Valor Tasa',
				tooltip: 	'Valor Tasa',
				dataIndex:	'VALOR_TASA',
				sortable: 	true,
				width: 		130,
				resizable:	true,
				align: 		'center'	
			}
		],
		displayInfo: 	true,		
		emptyMsg: 		"No hay registros.",		
		loadMask: 		true,
		clicksToEdit: 	1, 
		margins: 		'20 0 0 0',
		stripeRows: 	true,
		height: 			400,
		width: 			900,
		align: 			'center',
		frame: 			false,
		bbar: {
			items: [
				'->',
				'-',
				{
					xtype: 	'button',
					text: 	'Imprimir PDF',
					tooltip:	'Imprimir PDF',
					id: 		'btnGenerarPDF',
					iconCls: 'icoImprimir',
					handler: function(boton, evento) {
				
						var recibo 					= Ext.getCmp('recibo').getValue();
						var acuseFormateado 		= Ext.getCmp('acuseFormateado').getValue();
						var fechaDeAutorizacion = Ext.getCmp('fechaDeAutorizacion').getValue();
						var horaDeAutorizacion 	= Ext.getCmp('horaDeAutorizacion').getValue();
						var usuarioDeCaptura 	= Ext.getCmp('usuarioDeCaptura').getValue();
						var lsCadenaEpo 			= Ext.getCmp('lsCadenaEpo').getValue();
						var lstIF 					= Ext.getCmp('lstIF').getValue();
						var lsCveMoneda 			= Ext.getCmp('lsCveMoneda').getValue();
						var lsPYME 					= Ext.getCmp('lsPYME').getValue();
						var lsTipoTasa 			= Ext.getCmp('lsTipoTasa').getValue();
						var lstPlazo 				= Ext.getCmp('lstPlazo').getValue();
						var acuse 					= Ext.getCmp('acuse').getValue();
 
						boton.disable();
						boton.setIconClass('loading-indicator');
					
						Ext.Ajax.request({
							url: '13NBiTasaPredeNego01ext.data.jsp',
							params: {
								informacion:			'ArchivoPDF',
								recibo: 					recibo,
								acuseFormateado: 		acuseFormateado,
								fechaDeAutorizacion: fechaDeAutorizacion,
								horaDeAutorizacion: 	horaDeAutorizacion,
								usuarioDeCaptura: 	usuarioDeCaptura,
								lsCadenaEpo: 			lsCadenaEpo,
								lstIF: 					lstIF,
								lsCveMoneda: 			lsCveMoneda,
								lsPYME: 					lsPYME,
								lsTipoTasa: 			lsTipoTasa,
								lstPlazo: 				lstPlazo,
								acuse: 					acuse
							},
							callback: procesarGenerarPDF
						});					
					}
					
				},				
				{
					xtype: 	'button',
					text: 	'Bajar Archivo',
					tooltip:	'Bajar Archivo',
					id: 		'btnBajarPDF',
					iconCls:	'icoBotonPDF',
					hidden: 	true
				},	
				'-',
				{
					xtype: 	'button',
					text: 	'Generar CSV',
					tooltip:	'Generar CSV',
					id: 		'btnGenerarCSV',
					iconCls: 'icoXls',
					handler: function(boton, evento) {
					
						var recibo 					= Ext.getCmp('recibo').getValue();
						var acuseFormateado 		= Ext.getCmp('acuseFormateado').getValue();
						var fechaDeAutorizacion = Ext.getCmp('fechaDeAutorizacion').getValue();
						var horaDeAutorizacion 	= Ext.getCmp('horaDeAutorizacion').getValue();
						var usuarioDeCaptura 	= Ext.getCmp('usuarioDeCaptura').getValue();
						var lsCadenaEpo 			= Ext.getCmp('lsCadenaEpo').getValue();
						var lstIF 					= Ext.getCmp('lstIF').getValue();
						var lsCveMoneda 			= Ext.getCmp('lsCveMoneda').getValue();
						var lsPYME 					= Ext.getCmp('lsPYME').getValue();
						var lsTipoTasa 			= Ext.getCmp('lsTipoTasa').getValue();
						var lstPlazo 				= Ext.getCmp('lstPlazo').getValue();
						var acuse 					= Ext.getCmp('acuse').getValue();
					
						boton.disable();
						boton.setIconClass('loading-indicator');
						
						Ext.Ajax.request({
							url: '13NBiTasaPredeNego01ext.data.jsp',
							params: {
								informacion:			'ArchivoCSV',
								recibo: 					recibo,
								acuseFormateado: 		acuseFormateado,
								fechaDeAutorizacion: fechaDeAutorizacion,
								horaDeAutorizacion: 	horaDeAutorizacion,
								usuarioDeCaptura: 	usuarioDeCaptura,
								lsCadenaEpo: 			lsCadenaEpo,
								lstIF: 					lstIF,
								lsCveMoneda: 			lsCveMoneda,
								lsPYME: 					lsPYME,
								lsTipoTasa: 			lsTipoTasa,
								lstPlazo: 				lstPlazo,
								acuse: 					acuse
							},
							callback: procesarGenerarCSV
						});					
					}
				},
				{
					xtype: 	'button',
					text: 	'Bajar Archivo',
					tooltip:	'Bajar Archivo',
					id: 		'btnBajarCSV',
					iconCls:	'icoBotonXLS',
					hidden: 	true
				},
				'-',
				{
					xtype: 	'button',
					text: 	'Salir',
					tooltip:	'Salir',
					iconCls: 'icoLimpiar',
					id: 		'btnSalir',
					handler: function(boton, evento ) {
						window.location = '13NBiTasaPredeNego01ext.jsp';		
					}
				}
			]
		}
	});
			
	//------------------------------ GRID CONSULTA TASAS -----------------------------
	
	// Handler que maneja la respuesta de la eliminaci�n de Tasas Preferenciales/Negociadas
	function procesarSuccessFailureEliminar(opts, success, response) {
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var param 	= Ext.util.JSON.decode(response.responseText);
			
			var fp 		= Ext.getCmp('forma');
			fp.el.unmask();
			
			Ext.MessageBox.alert('Mensaje',param.eliminacion);
			
			var gridConsulta = Ext.getCmp('gridConsulta');
			gridConsulta.params = null;
			gridConsulta.getStore().removeAll();
			gridConsulta.hide();
			
		} else {
			
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	// Funcion para eliminar Tasas Preferenciales/Negociadas 
	var procesarEliminarNegociadas 		= function(){
		procesarEliminar('Negociadas')
	}
	var procesarEliminarPreferenciales	= function(){
		procesarEliminar('Preferenciales') 
	}
	var procesarEliminar		= function(nombreTasa){

		Ext.MessageBox.confirm(
			'Confirm', 
			"�Est� seguro de querer eliminar las Tasas "+nombreTasa+"?", 
			function(confirmBoton){ 
				
				if( confirmBoton == "yes" ){
					
					var gridConsulta			= Ext.getCmp("gridConsulta");
					// Preparar parametros
					var parametros 			= new Object();
					Ext.apply( parametros, gridConsulta.params );
					// Configurar parametros
					parametros.informacion 	= 'EliminarTasas';
					parametros.proceso		= "";
					// Recolectar el valor de los puntos
					if( parametros.tipoTasa === 'P' ){
						var arregloPuntos			 		= new Array();
						consultaData.each( function(record){
							arregloPuntos.push(record.data['LD_PUNTOS']); 
						});
						parametros.arregloPuntos 		= Ext.encode(arregloPuntos);
					}
					// Recolectar el valor de las tasas
					var arregloTasas			 		= new Array();
					consultaData.each( function(record){
						arregloTasas.push(record.data['TASAS']); 
					});
					parametros.arregloTasas  		= Ext.encode(arregloTasas);
					// Recolectar el valor de los campos Valor Tasa
					if( parametros.tipoTasa === 'N' ){
						
						var arregloValorTasas	 		= new Array();
						consultaData.each( function(record){
							arregloValorTasas.push(record.data['VALOR_TASA']); 
						});
						parametros.arregloValorTasas  = Ext.encode(arregloValorTasas);
						
					}
					// Enviar parametro ldTotal2, aunque en realidad no se ocupe.
					parametros.ldTotal2	  			= ""; 
								
					Ext.Ajax.request({
						url: 			'13NBiTasaPredeNego01ext.data.jsp',
						params: 		parametros,				
						callback: 	procesarSuccessFailureEliminar
					});	
					
				}
				
			}
		);
 
	}
	
	/*
	var procesarEliminarOld= function() 	{
		
		var tipoTasa1 = Ext.getCmp("tipoTasa1");	
		var tipoTasa2 = Ext.getCmp("tipoTasa2");
		var tipoTasa;
		var mensaje;
			
		if(tipoTasa1.getValue()==true) {mensaje	 ='Preferenciales'; tipoTasa ='P'; }
		if(tipoTasa2.getValue()==true) {  mensaje	 ='Negociadas'; tipoTasa ='N'; }
		var dlpuntos2 = 	Ext.getCmp("dlpuntos2").getValue();
		var ldTotal2 =	Ext.getCmp("ldTotal2").getValue();
		
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();
			
		if(!confirm('�Esta Seguro de querer eliminar las Tasas'+mensaje +' ?' )) {
			return;			
		}else {	
			 tasas = [];
			valorTasa  = [];
			store.each(function(record) {	
				
				if(tipoTasa =='P'){
					tasas.push(record.data['IC_EPO'] +'|'+record.data['PUNTOS'] +'|'+ record.data['LS_PLAZO']+'|'+ record.data['IC_TASA']+'|'+ record.data['LD_TOTAL']);
				}	
				if(tipoTasa =='N'){
					tasas.push(record.data['IC_EPO'] +'|'+record.data['FECHA'] +'|'+ record.data['LS_PLAZO']+'|'+ record.data['IC_TASA']+'|'+ record.data['VALOR']);
				}
				valorTasa.push(record.data['VALOR_TASA']);			
			});
			
			Ext.Ajax.request({
				url : '13NBiTasaPredeNego01ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'EliminarTasas',
					tipoTasa:tipoTasa,
					dlpuntos2:dlpuntos2,
					ldTotal2:ldTotal2,
					tasas:tasas, 
					valorTasa:valorTasa
				}),				
				callback: procesarSuccessFailureEliminar
			});	
		}
		
	}
	*/
 
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
 
		var formaEl 		= Ext.getCmp('forma').el;
		var gridConsulta  = Ext.getCmp("gridConsulta");
		var gridEl			= gridConsulta.getGridEl();	
		var columnModel	= gridConsulta.getColumnModel();			
		var hayRegistros	= store.getTotalCount() > 0?true:false;
		
		// Copiar los argumentos de la consulta
		gridConsulta.params = new Object();
		Ext.apply(gridConsulta.params,opts.params); 
		
		var btnEliminarPreferenciales 	= Ext.getCmp('btnEliminarPreferenciales');	
		var btnEliminarNegociables 		= Ext.getCmp('btnEliminarNegociables');	
		var btnGuardar 						= Ext.getCmp('btnGuardar');	
		var btnCancelar 						= Ext.getCmp('btnCancelar');	
			
		var indiceColumnaFecha				= columnModel.findColumnIndex('FECHA');
		var indiceColumnaRelMat 			= columnModel.findColumnIndex('REL_MAT');
		var indiceColumnaPuntos 			= columnModel.findColumnIndex('PUNTOS');
		var indiceValorTasa					= columnModel.findColumnIndex('VALOR_TASA');
			
		if (!gridConsulta.isVisible()) {
			gridConsulta.show();
		}	
		
		formaEl.unmask();
		gridConsulta.el.unmask();
		gridEl.unmask();
 
		if( 			hayRegistros && opts.params.tipoTasa === 'P' ){
				
			columnModel.config[indiceColumnaFecha].hideable  = false;
			columnModel.config[indiceColumnaRelMat].hideable = true;
			columnModel.config[indiceColumnaPuntos].hideable = true;
				
			columnModel.setHidden(indiceColumnaFecha, 	true);	
			columnModel.setHidden(indiceColumnaRelMat,   false);
			columnModel.setHidden(indiceColumnaPuntos, 	false);
				
			columnModel.setEditable(indiceValorTasa, 		false);
			
			if( opts.params.proceso === 'Procesar' ){
				btnEliminarPreferenciales.hide();
				btnEliminarNegociables.hide();
			} else { // proceso === 'Consulta'
				btnEliminarPreferenciales.show();	
				btnEliminarNegociables.hide();
			}	
			
			btnEliminarPreferenciales.enable();
			btnEliminarNegociables.disable();
			
			btnGuardar.enable();
			btnCancelar.enable();
					
		} else if(	hayRegistros && opts.params.tipoTasa === 'N' ){
				
			columnModel.config[indiceColumnaFecha].hideable  = true;
			columnModel.config[indiceColumnaRelMat].hideable = false;
			columnModel.config[indiceColumnaPuntos].hideable = false;
				
			columnModel.setHidden(indiceColumnaFecha, 	false);	
			columnModel.setHidden(indiceColumnaRelMat,   true);
			columnModel.setHidden(indiceColumnaPuntos, 	true);
				
			columnModel.setEditable(indiceValorTasa, 		true);
			
			if( opts.params.proceso === 'Procesar' ){
				btnEliminarPreferenciales.hide();
				btnEliminarNegociables.hide();
			} else { // proceso === 'Consulta'
				btnEliminarPreferenciales.hide();	
				btnEliminarNegociables.show();
			}
				
			btnEliminarPreferenciales.disable();
			btnEliminarNegociables.enable();
				
			btnGuardar.enable();
			btnCancelar.enable();
				
		} else if(	!hayRegistros && opts.params.tipoTasa === 'P' ){
				
			columnModel.config[indiceColumnaFecha].hideable  = false;
			columnModel.config[indiceColumnaRelMat].hideable = true;
			columnModel.config[indiceColumnaPuntos].hideable = true;
				
			columnModel.setHidden(indiceColumnaFecha, 	true);	
			columnModel.setHidden(indiceColumnaRelMat,   false);
			columnModel.setHidden(indiceColumnaPuntos, 	false);
				
			columnModel.setEditable(indiceValorTasa, 		false);
			
			if( opts.params.proceso === 'Procesar' ){
				btnEliminarPreferenciales.hide();
				btnEliminarNegociables.hide();
			} else { // proceso === 'Consulta'
				btnEliminarPreferenciales.show();	
				btnEliminarNegociables.hide();
			}
				
			btnEliminarPreferenciales.disable();
			btnEliminarNegociables.disable();
				
			btnGuardar.disable();
			btnCancelar.disable();
					
			gridEl.mask('No se encontr� ning�n registro', 'x-mask');	
			
		} else if(	!hayRegistros && opts.params.tipoTasa === 'N' ){
				
			columnModel.config[indiceColumnaFecha].hideable  = true;
			columnModel.config[indiceColumnaRelMat].hideable = false;
			columnModel.config[indiceColumnaPuntos].hideable = false;
				
			columnModel.setHidden(indiceColumnaFecha, 	false);	
			columnModel.setHidden(indiceColumnaRelMat,   true);
			columnModel.setHidden(indiceColumnaPuntos, 	true);
				
			columnModel.setEditable(indiceValorTasa, 		true);
			
			if( opts.params.proceso === 'Procesar' ){
				btnEliminarPreferenciales.hide();
				btnEliminarNegociables.hide();
			} else { // proceso === 'Consulta'
				btnEliminarPreferenciales.hide();	
				btnEliminarNegociables.show();
			}
				
			btnEliminarPreferenciales.disable();
			btnEliminarNegociables.disable();
				
			btnGuardar.disable();
			btnCancelar.disable();
		
			gridEl.mask('No se encontr� ning�n registro', 'x-mask');	
			
		}
		 
	}
	
	var epoRelacionadaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	var referenciaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	var tipoTasaRenderer = function( value, metadata, record, rowIndex, colIndex, store){
 
		metadata.attr = 'style="white-space: normal; word-wrap:break-word; " ';
		return value;
		
	}
	
	var valorRenderer = function( value, metadata, record, rowIndex, colIndex, store ){
		
		return Ext.isEmpty(value)?'':Ext.util.Format.number(value,'0.00000');
		
	}
 
	var puntosRenderer = function( value, metadata, record, rowIndex, colIndex, store ){
		
		return Ext.isEmpty(value)?'':Ext.util.Format.number(value,'0.00000');
		
	}
	
	var valorTasaRenderer = function( value, metadata, record, rowIndex, colIndex, store ){
		// Si es Tasa Negociada, marcar el campo como editable
		if( record.data['CLAVE_TIPO_TASA'] === 'N' ){
			metadata.attr  = 'style="border: 1px solid gray;"'; 
		}
		return Ext.isEmpty(value)?'':Ext.util.Format.number(value,'0.00000');
	}
	
	var consultaData = new Ext.data.JsonStore({
		root: 	'registros',
		url: 		'13NBiTasaPredeNego01ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		hidden: 	true,
		fields: [	
			//{name: 'IC_EPO'}, // Debuf info: POR EL MOMENTO ESTE CAMPO EST� SIN USAR
			{name: 'EPO_RELACIONADA'				},	
			{name: 'REFERENCIA'						},	
			{name: 'TIPO_TASA'						},	
			{name: 'VALOR',		type: 'float'	},	
			{name: 'PLAZO'								},	
			{name: 'REL_MAT'							},	
			{name: 'PUNTOS' 							},	//, type: 'float'
			{name: 'FECHA'								},
			{name: 'VALOR_TASA', type: 'float'	},
			{name: 'TASAS'								},
			{name: 'LD_PUNTOS'						},
			{name: 'LD_TOTAL'							},
			{name: 'CAD_TASA'							},
			{name: 'CLAVE_TIPO_TASA'				}
		],	
		totalProperty: 	'total',
		messageProperty: 	'msg',
		autoLoad: 			false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			},
		 autoDestroy: true			
	});

	var validaTasaN =  function( opts, success, response, registro) { 
		var valor = parseFloat(registro.data['VALOR']);  
		var valor_tasa = parseFloat(registro.data['VALOR_TASA']); 
	
		if(valor_tasa <0  ||  valor_tasa ==0 ){
			Ext.MessageBox.alert('Mensaje','La tasa debe ser mayor a 0');
			registro.data['VALOR_TASA']= '';
			return false;
		}
		
		if(valor_tasa >  valor){
			Ext.MessageBox.alert('Mensaje','La tasa no debe ser mayor al Valor de la Tasa Actual');
			registro.data['VALOR_TASA']= '';
			registro.commit();	
			return false;
		}

	}
	var revisarDecimal =  function( opts, success, response, registro) { 
		var numero = registro.data['VALOR_TASA']; 	
		if (isNaN(numero)) {
			alert ("El valor introducido no es un n�mero v�lido");
			registro.data['VALOR_TASA']= '';
			registro.commit();	
		} else {
			numero = Ext.isNumber(numero)?String(numero):numero;
			if (numero.indexOf('.') == -1)
				numero += ".";
			var dectext = numero.substring(numero.indexOf('.')+1, numero.length);
			if (dectext.length > 5) {
				alert ("Por favor introduzca un n�mero con m�ximo 5 decimales.");
				registro.data['VALOR_TASA']= '';
				registro.commit();	
			} else {
				return true;
			}
		}
	}

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 					'gridConsulta',				
		store: 				consultaData,	
		style: 				'margin:0 auto;',
		title:				'Tasas Preferenciales/Negociadas',
		hidden: 				true,
		clicksToEdit: 		1,
		autoExpandColumn: 'EPO_RELACIONADA',
		columns: [
			{							
				header: 		'EPO Relacionada',
				tooltip: 	'EPO_RELACIONADA',
				id:			'EPO_RELACIONADA',
				dataIndex: 	'EPO_RELACIONADA',
				sortable: 	true,
				// width: 130,
				resizable: 	true,				
				align: 		'left',
				renderer: 	epoRelacionadaRenderer
			},
			{							
				header : 'Referencia',
				tooltip: 'REFERENCIA',
				dataIndex : 'REFERENCIA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: referenciaRenderer
			},
			{							
				header : 'Tipo de Tasa',
				tooltip: 'TIPO_TASA',
				dataIndex : 'TIPO_TASA',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'center',
				renderer: tipoTasaRenderer
			},
			{							
				header: 		'Valor',
				tooltip: 	'Valor',
				dataIndex: 	'VALOR',
				sortable: 	true,
				// width: 130,
				resizable: 	true,				
				align: 		'center',
				renderer: 	valorRenderer
			},
			{							
				header : 'Plazo',
				tooltip: 'Plazo',
				dataIndex : 'PLAZO',
				sortable: true,
				width: 72,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Rel. Mat',
				tooltip: 'Rel. Mat',
				dataIndex : 'REL_MAT',
				sortable: true,
				width: 56,
				resizable: true,				
				align: 'center'	
			},
			{							
				header : 'Puntos',
				tooltip: 'Puntos',
				dataIndex : 'PUNTOS',
				sortable: true,
				// width: 130,
				resizable: true,				
				align: 'center'
				// , renderer: puntosRenderer
			},
			{							
				header : 	'Fecha',
				tooltip: 	'Fecha',
				dataIndex : 'FECHA',
				sortable: 	true,
				// width: 		130,
				resizable: 	true,
				hidden:	 	true,
				align: 		'center'	
			},
			{							
				header: 		'Valor Tasa',
				tooltip: 	'Valor Tasa',
				dataIndex: 	'VALOR_TASA',
				sortable: 	true,
				// width: 	130,
				resizable: 	true,				
				align: 		'center',
				renderer:	valorTasaRenderer,
				editable:   false,
				editor: new Ext.form.NumberField({
            	// allowBlank: false,
            	// allowNegative: false,
            	style: (
            		Ext.isIE7  ?'top: 0px; padding-bottom: 2px;':
            		( Ext.isIE8?'top: 0px; padding-bottom: 2px;':
            		'top: 1px; padding-bottom: 2px;')
            	),
            	format: '0.00000',
            	decimalPrecision: 5,
            	minValue: -99999.99999, 
            	maxValue: 99999.99999
            })
							
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: true,
		listeners: {	
			beforeedit: function(e){	 //esta parte es para deshabilitar los campos editables 			
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
							
				if(campo == 'VALOR_TASA'){				
					if( record.data['TASA_SELEC'] =='NG'){
						return true; // es editable 					
					}else if( record.data['TASA_SELEC'] =='P'){
						return false; //  no es editable 					
					}
				}	
								
			},
			afteredit : function(e){// se dispara para calular datos que al capurar los campos editables 
				var record = e.record;
				var gridConsulta = e.gridConsulta; 
				var campo= e.field;
						
				if(campo=='VALOR_TASA') { 	 	
					revisarDecimal('', '', '', record); 
					validaTasaN('', '', '', record); 
				}
			}
		},		
		tbar: {
			items: [
				{
					xtype: 	'button',
					text: 	'Eliminar Tasas Negociadas',
					tooltip:	'Eliminar Tasas Negociadas',
					id: 		'btnEliminarNegociables',
					iconCls: 'icoEliminar',
					hidden: 	false,
					handler: procesarEliminarNegociadas 
				},
				{
					xtype: 	'button',
					text: 	'Eliminar Tasas Preferenciales',
					tooltip:	'Eliminar Tasas Preferenciales',
					id: 		'btnEliminarPreferenciales',
					iconCls: 'icoEliminar',
					hidden: 	false,
					hidden: 	false,
					handler: procesarEliminarPreferenciales
				},
				{
					xtype: 	'button',
					text: 	'Guardar',					
					tooltip:	'Guardar',
					iconCls: 'icoGuardar',
					id: 		'btnGuardar',
					handler: procesarGuardar
				},	
				{
					xtype: 	'button',
					text: 	'Cancelar',
					tooltip:	'Cancelar',
					iconCls: 'icoLimpiar',
					id: 		'btnCancelar',
					handler: function(boton, evento) {
						window.location = '13NBiTasaPredeNego01ext.jsp';		
					}
				}
			]
		}
	});
	
	//------------------------------- FORMA BUSQUEDA AVANZADA ---------------------------
	
	var procesaBusquedaProveedor = function(opts, success, response) {
		
		var clavePyme						= Ext.getCmp('ic_pyme');
		var operaConOfertaDeTasas  	= Ext.getCmp('operaConOfertaDeTasas');
		var operaConOfertaDeTasasMsg  = Ext.getCmp('operaConOfertaDeTasasMsg');
		var nombrePyme						= Ext.getCmp('nombrePyme');
		var numeroNafinElectronico 	= Ext.getCmp('numeroNafinElectronico');
		
		clavePyme.setValue("");
		operaConOfertaDeTasas.setValue(false);
		operaConOfertaDeTasasMsg.setValue("");
		nombrePyme.setValue("");
			
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
 
			var resp = 	Ext.util.JSON.decode(response.responseText);
 
			if( resp.operaConOfertaDeTasas ){
				
				clavePyme.setValue(  					resp.clavePyme						);
				operaConOfertaDeTasas.setValue( 		resp.operaConOfertaDeTasas 	);
				operaConOfertaDeTasasMsg.setValue(	resp.operaConOfertaDeTasasMsg );
				nombrePyme.setValue( 					resp.nombrePyme 					); 
				
				// El Proveedor seleccionado opera con Oferta de Tasas por Montos, es necesario borrar su parametrizaci�n para poder aplicar Tasa Preferencial.
				numeroNafinElectronico.markInvalid(resp.operaConOfertaDeTasasMsg);
				
			} else if(!resp.encontrado){ 
				
				// El nafin electronico no corresponde a una\nPyME afiliada a la EPO o no existe.
				numeroNafinElectronico.markInvalid(resp.msg);
				
			} else {
				
				clavePyme.setValue(  				resp.clavePyme  				);
				operaConOfertaDeTasas.setValue( 	resp.operaConOfertaDeTasas );
				nombrePyme.setValue( 				resp.nombrePyme 				);
				
			}
 
		} else {
 
			numeroNafinElectronico.markInvalid("Ocurri� un error inesperado al buscar Proveedor");
			// Mostrar mensaje de error
			NE.util.mostrarConnError(response,opts);
			
		}
		
	}
	
	var numeroNafinElectronicoOnBlurHandler = function(field){
							
		var numeroNafinElectronico = field.getValue();
							
		// Limpiar campos
		if(Ext.isEmpty(numeroNafinElectronico)){
		
			var clavePyme	= Ext.getCmp('ic_pyme');
			var nombre		= Ext.getCmp('nombrePyme');
			/* var rfc		= Ext.getCmp('rfcPyme'); */
								
			clavePyme.setValue('');
			nombre.setValue('');
			/* rfc.setValue(''); */
 
		// Consultar campos
		} else {
										
			if(soloNumeros(field,'El No. Nafin Electr�nico no es v�lido')){
			
				var error = false;
				
				var comboTipoDeTasa     = Ext.getCmp("comboTipoDeTasa");
				
				var comboEpoRelacionada	= Ext.getCmp("ic_epo1");					
				if(Ext.isEmpty(comboEpoRelacionada.getValue())){
					comboEpoRelacionada.markInvalid("Este campo es requerido");
					error = true;
				}
				
				var comboMoneda = Ext.getCmp("ic_moneda1");
				if(Ext.isEmpty(comboMoneda.getValue())){
					comboMoneda.markInvalid("Este campo es requerido");
					error = true;
				}
				
				var comboNombreDelIF = Ext.getCmp("comboNombreDelIF");
				if(Ext.isEmpty(comboNombreDelIF.getValue())){
					comboNombreDelIF.markInvalid("Este campo es requerido");
					error = true;
				}
				
				if( error ) return;
				
				Ext.Ajax.request({
					url: '13NBiTasaPredeNego01ext.data.jsp',
					params: {
						informacion: 				'BusquedaProveedor',
						tipoTasa:					comboTipoDeTasa.getValue(),
						numeroNafinElectronico: numeroNafinElectronico,
						ic_epo:						comboEpoRelacionada.getValue(),
						ic_moneda:					comboMoneda.getValue(),
						ic_if:						comboNombreDelIF.getValue()
					},
					callback: procesaBusquedaProveedor
				});
			
			}

			
		}
							
	}
 
	var resetFormaBusquedaAvanzada = function(){
		
		var fpBusquedaAvanzada = Ext.getCmp('busquedaAvanzada');
		fpBusquedaAvanzada.getForm().reset();
		
		var busquedaAvanzadaData = Ext.StoreMgr.key('busquedaAvanzadaDataStore');
		Ext.Ajax.abort(busquedaAvanzadaData.proxy.getConnection().transId);
		busquedaAvanzadaData.removeAll();
		
		// Deshabilitar boton aceptar
		Ext.getCmp("botonAceptar").disable();
		
		// Poner mensaje de empty text por default
		var cmbPyme = Ext.getCmp('cmbPyme1');
		cmbPyme.emptyText =  "Seleccione...";
		cmbPyme.setRawValue('');
		cmbPyme.applyEmptyText();
		cmbPyme.setDisabled(true);
		
		// Habilitar boton de Busqueda
		var botonBuscar = Ext.getCmp("botonBuscar");
		botonBuscar.enable();
		botonBuscar.setIconClass('iconoLupa');
		
	}
	
	var busquedaAvanzadaData = new Ext.data.JsonStore({
		id: 					'busquedaAvanzadaDataStore',
		root: 				'registros',
		fields: 				['clave', 'descripcion','loadMsg'],
		url: 					'13NBiTasaPredeNego01ext.data.jsp',
		baseParams: {
			informacion: 	'BusquedaAvanzada'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					// Remover animacion de carga
					var busquedaAvanzadaData = Ext.StoreMgr.key('busquedaAvanzadaDataStore');
					busquedaAvanzadaData.removeAll();		
					// Poner mensaje de empty text por default
					var cmbPyme = Ext.getCmp('cmbPyme1');
					cmbPyme.emptyText =  "Seleccione...";
					cmbPyme.setRawValue('');
					cmbPyme.applyEmptyText();
					cmbPyme.setDisabled(true);
					// Habilitar boton de Busqueda
					var botonBuscar = Ext.getCmp("botonBuscar");
					botonBuscar.enable();
					botonBuscar.setIconClass('iconoLupa');					
				}
			},
			beforeload:		NE.util.initMensajeCargaCombo,
			load:				function(store,records,options){
				
				// Verificar que el load realmente sea ocasionado por una busqueda del usuario
				// y no como consecuencia de la carga que se realiza para mostrar la animacion
				// de carga de datos.
				var busquedaAvanzadaData 	= false;
				try {
					busquedaAvanzadaData 	= options.params.busquedaAvanzadaData;
				}catch(err){
					busquedaAvanzadaData		= false;
				}
								
				// La busqueda es aut�ntica
				if( busquedaAvanzadaData ){ 
					
					// El registro fue encontrado por lo que hay que seleccionarlo
					var myCombo = Ext.getCmp("cmbPyme1");
					if ( store.getTotalCount() == 0){
						myCombo.emptyText =  "No existe informaci�n con los criterios determinados.";
						myCombo.setRawValue('');
						myCombo.applyEmptyText();
						Ext.getCmp('botonAceptar').setDisabled(true);
					} else {
						/*myCombo.emptyText =  "Seleccione...";
						myCombo.setRawValue('');
						myCombo.applyEmptyText();*/
						Ext.getCmp('botonAceptar').setDisabled(false);
					}
					
					// Habilitar boton de Busqueda
					var botonBuscar = Ext.getCmp("botonBuscar");
					botonBuscar.enable();
					botonBuscar.setIconClass('iconoLupa');
					
				}
			}
			
		},
		autoDestroy: true
		
	});
	
	var elementosFormaBusquedaAvanzada = [
		{
			xtype: 				'panel',
			id: 					'forma2',			
			frame: 				true,		
			titleCollapse: 	false,
			bodyStyle: 			'padding: 6px',			
			defaultType: 		'textfield',
			align: 				'center',
			html: 				'Utilice el * para realizar una b�squeda gen�rica.',
			anchor:				'100%'
		},
		NE.util.getEspaciador(20),
		{ 
			xtype: 		'textfield',
			name: 		'nombrePyme',
			id: 			'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 	100,	
			width: 		80,
			msgTarget: 	'side',
			margins: 	'0 20 0 0'  
		},
		{ 
			xtype: 		'textfield',
			name: 		'rfcPyme',
			id: 			'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 	15,	
			width: 		80,
			msgTarget: 	'side',
			margins: 	'0 20 0 0'  
		},		
		NE.util.getEspaciador(20),
		{
			xtype: 		'panel',
			border:		false,
			baseCls:		'x-plain',
			items: [
				{
					xtype: 		'button',
					width: 		80,
					height:		10,
					text: 		'Buscar',
					iconCls: 	'iconoLupa',
					name: 		'botonBuscar',
					id: 			'botonBuscar',
					style: 		'float:right',
					handler:    function(boton, evento) {
								
							boton.disable();
							boton.setIconClass('loading-indicator');
							
							var nombrePyme	= Ext.getCmp("nombrePyme1");
							var rfcPyme		= Ext.getCmp("rfcPyme1");
							 
							/*
							// Nota: Esta validacion no es necesaria
							if( 
									Ext.isEmpty(nombrePyme.getValue())  		&&  
									Ext.isEmpty(rfcPyme.getValue())					
								){
								nombrePyme.markInvalid('Es necesario que ingrese datos en al menos un campo');						
								return;
							}
							*/
							 
							var cmbPyme = Ext.getCmp('cmbPyme1');
							cmbPyme.emptyText =  "Seleccione...";
							cmbPyme.setRawValue('');
							cmbPyme.applyEmptyText();
							cmbPyme.setDisabled(false);
							
							var botonAceptar = Ext.getCmp('botonAceptar');
							botonAceptar.setDisabled(true);
							
							var busquedaAvanzada = Ext.getCmp("busquedaAvanzada");
							busquedaAvanzadaData.load({
									params: Ext.apply(busquedaAvanzada.getForm().getValues(),
									{
										busquedaAvanzadaData: true
									}
								)
							});
							
						}						
				}
			]
		},	
		NE.util.getEspaciador(5),
		{
			xtype: 				'combo',
			name: 				'cmbPyme',
			id: 					'cmbPyme1',
			mode: 				'local',
			autoLoad: 			false,
			displayField: 		'descripcion',
			emptyText: 			'Seleccione...',
			valueField: 		'clave',
			hiddenName: 		'clavePyme1',
			fieldLabel: 		'Nombre',
			disabled: 			true,
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				busquedaAvanzadaData,
			tpl:					NE.util.templateMensajeCarga
		}
	];
 
	var fpBusquedaAvanzada = new Ext.form.FormPanel({
		id: 					'busquedaAvanzada',
		width: 				450,
		frame: 				false,
		border:				false,
		collapsible: 		false,
		titleCollapse: 	false,
		bodyStyle: 			'padding: 6px',
		baseCls:				'x-plain',
		labelWidth: 		76,
		defaultType: 		'textfield',
		defaults: {
			msgTarget: 		'side',
			anchor: 			'-20'
		},
		items: 				elementosFormaBusquedaAvanzada,
		monitorValid: 		false, // true,
		buttons: [
			{
				text: 		'Aceptar',
				name:			'botonAceptar',
				id:			'botonAceptar',
				iconCls: 	'aceptar',
				formBind: 	true,
				disabled: 	true,
				align: 		'center',
				handler: 
					function(boton, evento){
						
						var cmbPyme							= Ext.getCmp("cmbPyme1");
 
						if (Ext.isEmpty(cmbPyme.getValue())) {
							cmbPyme.markInvalid('Seleccione una Pyme');
							return;
						}
 
						var numeroNafinElectronico = Ext.getCmp('numeroNafinElectronico');
						var clavePyme					= Ext.getCmp('ic_pyme');
						var nombre						= Ext.getCmp('nombrePyme');
						
						busquedaAvanzadaData.each(function(record){
							if(record.data['clave'] == cmbPyme.getValue()){
								numeroNafinElectronico.setValue(	record.json.nafin_electronico	);
								// Nota: Se omite la clave Pyme, para que numeroNafinElectronicoOnBlurHandler lo
								// llene en caso de que el proveedor sea valido.
								//clavePyme.setValue(					cmbPyme.getValue()				);
								nombre.setValue(						record.json.nombre_pyme			); 
								return;
							}
						});
 
						var ventana 	= Ext.getCmp('windowBusquedaAvanzada');
						resetFormaBusquedaAvanzada();
						ventana.hide();
						
						numeroNafinElectronicoOnBlurHandler(numeroNafinElectronico);
						
					}
					
			},
			{
				text: 		'Cancelar',
				iconCls: 	'icoLimpiar',
				handler: 
					function() {					
						var ventana = Ext.getCmp('windowBusquedaAvanzada');
						resetFormaBusquedaAvanzada();
						ventana.hide();					
					}				
			}
		]
	});
 
	//--------- CARGA INDIVIDUAL ~ TASAS PREFERENCIALES/NEGOCIADAS --------- 
	
	var procesaCambioSeleccion = function(componente){
		
		var comboMoneda			= Ext.getCmp("ic_moneda1");
		var comboEpoRelacionada = Ext.getCmp("ic_epo1");
		var origen;
		
		// RESETEAR COMBO TIPO DE TASA
		if( componente && componente.id === 'comboTipoDeTasa' ){
			
			var puntos 			= Ext.getCmp("txtPuntos1");
			var plazo  			= Ext.getCmp("lstPlazo1");
			var gridConsulta 	= Ext.getCmp("gridConsulta");
			
			if(        componente.getValue() === "P" ){
				
				comboEpoRelacionada.setValue('');
				puntos.setValue('');
				plazo.setValue('');
				
				plazo.hide();
				puntos.show();
				
				gridConsulta.hide();
				
			} else if( componente.getValue() === "N" ){

				comboEpoRelacionada.setValue('');
				puntos.setValue('');
				plazo.setValue('');
				
				puntos.hide();
				plazo.show();
				
				gridConsulta.hide();
				
			}
			
		}
		
		// MODIFICACIONES GENERALES		
 		
		// Resetear Combo Nombre Del IF
		var comboNombreDelIF = Ext.getCmp('comboNombreDelIF');
		comboNombreDelIF.emptyText = "Seleccionar...";
		comboNombreDelIF.setValue('');
		comboNombreDelIF.applyEmptyText();
		comboNombreDelIF.clearInvalid();
		comboNombreDelIF.store.load({
			params: {
				ic_epo:    comboEpoRelacionada.getValue(),
				ic_moneda: comboMoneda.getValue()
			},
			callback: function(record,options,success){
				if(Ext.isEmpty(options.params.ic_epo)){
					return;
				}
				if( success && record.length === 0 ){
					var comboNombreDelIF = Ext.getCmp('comboNombreDelIF');
					comboNombreDelIF.emptyText = "No se encontr� ning�n Intemediario Financiero";
					comboNombreDelIF.setValue('');
					comboNombreDelIF.applyEmptyText();
					comboNombreDelIF.clearInvalid();
				}
			}
		});
		// Resetear contenido del Proveedor
		Ext.getCmp("numeroNafinElectronico").reset();
		Ext.getCmp("ic_pyme").reset();
		Ext.getCmp("nombrePyme").reset();
		// Resetear Campos Puntos
		Ext.getCmp("txtPuntos1").reset();
		// Resetear Combo Plazo
		Ext.getCmp("lstPlazo1").reset();
					
	}
				
	var procesarCatalogoMonedaData= function(store, records, oprion){
		if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
			var ic_moneda1 = Ext.getCmp('ic_moneda1');
			if(ic_moneda1.getValue()==''){
				var index = store.findExact( 'clave', "1" ); // :: Moneda Nacional :: 
				if (index >= 0){ // El registro fue encontrado por lo que hay que seleccionarlo
					ic_moneda1.setValue("1");
				} else {
					ic_moneda1.setValue(records[0].data['clave']);
				}
			}
		}
	}		
		
	var storeCatalogoPYME = new Ext.data.JsonStore({
		id: 'storeCatalogoPYME',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13NBiTasaPredeNego01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPYME'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		},
		autoDestroy: true
	});
	
	var storeCatalogoTipoDeTasa = new Ext.data.ArrayStore({
		id: 		'storeCatalogoTipoDeTasa',
		fields:	[ 'clave', 'descripcion', 'loadMsg' ],
		data:  	[
			[ 'P', 'PREFERENCIAL' ],
			[ 'N', 'NEGOCIADA'    ]
		],
		autoDestroy: 	true
	});
	
	var storeCatalogoIF = new Ext.data.JsonStore({
		id: 					'storeCatalogoIF',
		root: 				'registros',
		fields: 				['clave', 'descripcion','loadMsg'],
		url: 					'13NBiTasaPredeNego01ext.data.jsp',
		baseParams: {
			informacion: 	'CatalogoIF'
		},
		totalProperty: 	'total',
		autoLoad: 			false,
		listeners: {
			exception: 		NE.util.mostrarDataProxyError,
			beforeload: 	NE.util.initMensajeCargaCombo
		},
		autoDestroy: true
	});
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13NBiTasaPredeNego01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		},
		autoDestroy: true
	});
	
	var catalogoPlazo = new Ext.data.JsonStore({
		id: 'catalogoPlazo',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13NBiTasaPredeNego01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoPlazo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		},
		autoDestroy: true
	});
	
	
	var catalogoMoneda = new Ext.data.JsonStore({
		id: 'catalogoMoneda',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13NBiTasaPredeNego01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		},
		autoDestroy: true	
	});
 
	var elementosForma = [
		// COMBO TIPO DE TASA
		{
			xtype: 				'combo',
			name: 				'comboTipoDeTasa',
			id: 					'comboTipoDeTasa',
			fieldLabel: 		'Tipo de Tasa',
			width:				125,
			allowBlank: 		false,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'tipoTasa',
			emptyText: 			'Seleccionar...', 
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				storeCatalogoTipoDeTasa,
			tpl: 					NE.util.templateMensajeCargaCombo,
			autoLoad: 			false,
			//anchor:				'-20'
			listeners:			{
				collapse: procesaCambioSeleccion
			},
			value: "P" // SELECCIONAR LA TASA PREFERENCIAL POR DEFAULT
		},
		// COMBO EPO RELACIONADA
		{
			xtype: 			'multiselect',
			fieldLabel: 	'EPO Relacionada',
			name: 			'ic_epo',
			id: 				'ic_epo1',
			width: 			583,
			height: 			200, 
			autoLoad: 		false,
			allowBlank:		false,
			store: 			catalogoEPO,
			displayField: 	'descripcion',
			valueField:		'clave',
			/*tbar:[{
				text: 		'Limpiar',
				handler: function(){
					Ext.StoreMgr.key('storeCatalogoIF').removeAll();
					fp.getForm().reset();
				}
			}],*/
			ddReorder: false,
			listeners: {
				change: procesaCambioSeleccion
			}		
		},
		// COMBO MONEDA
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					mode: 'local',
					allowBlank: false,
					autoLoad: false,
					displayField: 'descripcion',
					emptyText: 'Seleccionar Moneda...',			
					valueField: 'clave',
					hiddenName: 'ic_moneda',
					fieldLabel: 'Moneda',			
					forceSelection: true,
					triggerAction: 'all',
					typeAhead: true,
					minChars: 1,
					store: catalogoMoneda,
					listeners: {
						collapse: procesaCambioSeleccion
					}
				}
			]
		},
		// COMBO NOMBRE DEL IF
		{
			xtype: 				'combo',
			name: 				'ic_if',
			id: 					'comboNombreDelIF',
			fieldLabel: 		'Nombre del IF',
			allowBlank: 		false,
			mode: 				'local', 
			displayField: 		'descripcion',
			valueField: 		'clave',
			hiddenName: 		'ic_if',
			emptyText: 			'Seleccionar...', 
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			store: 				storeCatalogoIF,
			tpl: 					NE.util.templateMensajeCargaComboConDescripcionCompleta,
			anchor:				'75%',
			listeners:			{
				collapse: function(comboNombreDelIF){
					var numeroNafinElectronico = Ext.getCmp('numeroNafinElectronico');
					numeroNafinElectronicoOnBlurHandler(numeroNafinElectronico);
				}
			}
		},
		// CAMPO N@E PROVEEDOR
		{
			xtype: 				'compositefield',
			fieldLabel: 		'Proveedor',			
			msgTarget: 			'side',
			id: 					'compositefield1',
			combineErrors: 	false,
			items: [
				{ 
					xtype: 		'numberfield',
					name: 		'numeroNafinElectronico',
					id: 			'numeroNafinElectronico',
					nanText:		"{0} no es un No. Nafin Electr�nico v�lido.",
					allowBlank: false,
					hidden: 		false,
					maxLength: 	15,	
					msgTarget: 	'side',
					margins: 	'0 20 0 0',  //necesario para mostrar el icono de error
					listeners:{
						blur: numeroNafinElectronicoOnBlurHandler
					}
					
				},
				{
					xtype: 		'button',
					hidden: 		false,
					iconCls:		'icoBuscar',
					text: 		'B�squeda Avanzada...',
					id: 			'botonBusquedaAvanzada',
					handler: 
						function(boton, evento) {	
							
							/*
							var cboMoneda = Ext.getCmp("cboMoneda1");
							if (Ext.isEmpty(cboMoneda.getValue())  ) {
								cboMoneda.markInvalid('Debe Seleccionar  el tipo de Moneda');					
								return;
							}
							*/
				
							var ventana = Ext.getCmp('windowBusquedaAvanzada');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									layout: 			'fit',
									width: 			450,
									height: 			330,
									minWidth: 		450,
									minHeight: 		330,
									modal:			true,
									title: 			'B�squeda Avanzada',
									id: 				'windowBusquedaAvanzada',
									closeAction: 	'hide',
									items: [
										fpBusquedaAvanzada
									],
									listeners: 		{
										hide: resetFormaBusquedaAvanzada
									}
								}).show();
							}
						}
				}
				
			]
			
		},	
		// ID DE LA PYME: IC_PYME
		{  
        xtype:	'hidden',  
        name:	'ic_pyme',
        id: 	'ic_pyme'
      },
      // CAMPO OCULTO QUE INDICA SI EL PROVEEDOR OPERA CON OFERTA DE TASAS
		{
			xtype:'hidden',
			id:	'operaConOfertaDeTasas',	
			name: 'operaConOfertaDeTasas'
		},
		// CAMPO OCULTO CON EL MENSAJE QUE SE MUESTRA EN CASO DE QUE EL PROVEEDOR OPERE CON OFERTA DE TASAS
		{
			xtype:'hidden',
			id:	'operaConOfertaDeTasasMsg',	
			name: 'operaConOfertaDeTasasMsg'
		},
		// INPUT TEXT NOMBRE DEL PROVEEDOR
		{
			xtype: 				'textfield',
			name: 				'nombrePyme',
			id: 					'nombrePyme',
			readOnly: 			true,
			fieldLabel: 		'',
			allowBlank: 		true,
			maxLength: 			100,	
			msgTarget: 			'side',
			anchor:				'95%',
			style:				'background:gainsboro;',
			//cls:					'x-item-disabled',
			margins: 			'0 20 0 0'  //necesario para mostrar el icono de error
		},
		// NUMBERFIELD PUNTOS
		{
			xtype: 				'puntostasa',
			name: 				'txtPuntos',
			id: 					'txtPuntos1', 
			fieldLabel: 		'Puntos',
			allowBlank: 		true,
			maxLength: 			12,	
			width: 				100,
			msgTarget: 			'side',						
			margins: 			'0 20 0 0'
		},
		// COMBO PLAZO
		{
			xtype: 				'combo',
			name: 				'lstPlazo',
			id: 					'lstPlazo1',
			mode: 				'local',				
			displayField:		'descripcion',
			emptyText: 			'Seleccione Plazo...',			
			valueField: 		'clave',
			hiddenName: 		'lstPlazo',
			fieldLabel: 		'Plazo',			
			forceSelection: 	true,
			triggerAction: 	'all',
			typeAhead: 			true,
			minChars: 			1,
			allowBlank: 		true,
			msgTarget: 			'side',	
			store: 				catalogoPlazo,
			tpl: 					NE.util.templateMensajeCargaComboConDescripcionCompleta,
			hidden:				true
		}
	
	];
	

	
	var fp = new Ext.form.FormPanel({
		id: 				'forma',
		width: 			800,
		title: 			'Carga Individual ~ Tasas Preferenciales/Negociadas',
		frame: 			true,		
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin:0 auto;',
		bodyStyle: 		'padding: 6px',
		labelWidth:		150,
		defaultType: 	'textfield',
		items: 			elementosForma,			
		monitorValid: 	false,
		buttons: [
			{
				text: 		'Consultar',
				id: 			'btnConsultar',
				iconCls: 	'icoBuscar',
				formBind: 	true,		
				handler: function(boton, evento) {
					
					var forma 		= Ext.getCmp('forma');
					var validForm 	= true;
					
					// 0. Validar Forma de Manera General
					validForm 		= forma.getForm().isValid();
 
					// 1. Tipo de Tasa
					var comboTipoDeTasa = Ext.getCmp("comboTipoDeTasa");
					if( comboTipoDeTasa.isValid() && Ext.isEmpty( comboTipoDeTasa.getValue() ) ){
						comboTipoDeTasa.markInvalid("Debe Selecionar un Tipo de Tasa");
						validForm = false;
					}
					// 2. Epo Relacionada
					var comboEpoRelacionada = Ext.getCmp("ic_epo1");
					if( comboEpoRelacionada.isValid() && Ext.isEmpty( comboEpoRelacionada.getValue() ) ){
						comboEpoRelacionada.markInvalid("Debe Selecionar una EPO");
						validForm = false;
					}
					// 3. Moneda
					var comboMoneda = Ext.getCmp("ic_moneda1");
					if( comboMoneda.isValid() && Ext.isEmpty( comboMoneda.getValue() ) ){
						comboMoneda.markInvalid("Debe Selecionar una Moneda");
						validForm = false;
					}
					// 4. Nombre del IF
					var comboNombreDelIF = Ext.getCmp("comboNombreDelIF");
					if( comboNombreDelIF.isValid() && Ext.isEmpty( comboNombreDelIF.getValue() ) ){
						comboNombreDelIF.markInvalid("Debe Selecionar el Intermediario Financiero");
						validForm = false;
					}
					// 5. Proveedor
					var numeroNafinElectronico 	= Ext.getCmp("numeroNafinElectronico");
					var clavePyme						= Ext.getCmp("ic_pyme");
					var operaConOfertaDeTasas  	= Ext.getCmp('operaConOfertaDeTasas');
					var operaConOfertaDeTasasMsg 	= Ext.getCmp('operaConOfertaDeTasasMsg');
					if(        numeroNafinElectronico.isValid()   && Ext.isEmpty( numeroNafinElectronico.getValue() ) ){
						numeroNafinElectronico.markInvalid("Capture el Provedor");
						validForm = false;
					} else if( numeroNafinElectronico.isValid()   && Ext.isEmpty( clavePyme.getValue()              ) ){
						numeroNafinElectronico.markInvalid("El Proveedor capturado no es v�lido");
						validForm = false;
					// Para cuando el tipo de tasa seleccionada, es preferencial, se agrega la validacion que revisa que 
					// el proveedor no opere con oferta de tasas por montos.
					} else if( comboTipoDeTasa.getValue() === 'P' && operaConOfertaDeTasas.getValue() === "true"      ){ 
						numeroNafinElectronico.markInvalid( operaConOfertaDeTasasMsg.getValue() );
						/*validForm = false;*/ // Se les deja pasar, haciendo mimica a la pantalla original
					}
					// 6. Puntos
					if( comboTipoDeTasa.getValue() === "P" ){ // TASA PREFERENCIAL
						// var operaConOfertaDeTasas 	= Ext.getCmp("operaConOfertaDeTasas").getValue();
						var puntos 						= Ext.getCmp("txtPuntos1");
						if( puntos.isValid() && !Ext.isEmpty( puntos.getValue() ) ){
							puntos.markInvalid("Los Puntos no son un criterio de b�squeda");
							validForm = false;
						}
					}
					// 7. Plazo
					if( comboTipoDeTasa.getValue() === "N" ){ // TASA NEGOCIADA
						var comboPlazo = Ext.getCmp("lstPlazo1");
						if( comboPlazo.isValid() && !Ext.isEmpty( comboPlazo.getValue() ) ){
							comboPlazo.markInvalid("El Plazo no es un criterio de b�squeda");
							validForm = false;
						}
					}
 
					// 8. La forma es invalida, se suspende la operacion
					if( !validForm ) return;

					// 9. Realizar b�squeda
					forma.el.mask('Consultando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							informacion: 	'Consultar',
							proceso: 		'Consulta'
						})
					});
					
			}
		},
		{
			text: 		'Procesar',
			id: 			'btnProcesar',
			iconCls: 	'icoAceptar',
			formBind: 	true,		
			handler: function(boton, evento) {
				
				var forma 		= Ext.getCmp('forma');
				var validForm 	= true;
					
				// 0. Validar Forma de Manera General
				validForm 		= forma.getForm().isValid();
 
				// 1. Tipo de Tasa
				var comboTipoDeTasa = Ext.getCmp("comboTipoDeTasa");
				if( comboTipoDeTasa.isValid() && Ext.isEmpty( comboTipoDeTasa.getValue() ) ){
					comboTipoDeTasa.markInvalid("Debe Selecionar un Tipo de Tasa");
					validForm = false;
				}
				// 2. Epo Relacionada
				var comboEpoRelacionada = Ext.getCmp("ic_epo1");
				if( comboEpoRelacionada.isValid() && Ext.isEmpty( comboEpoRelacionada.getValue() ) ){
					comboEpoRelacionada.markInvalid("Debe Selecionar una EPO");
					validForm = false;
				}
				// 3. Moneda
				var comboMoneda = Ext.getCmp("ic_moneda1");
				if( comboMoneda.isValid() && Ext.isEmpty( comboMoneda.getValue() ) ){
					comboMoneda.markInvalid("Debe Selecionar una Moneda");
					validForm = false;
				}
				// 4. Nombre del IF
				var comboNombreDelIF = Ext.getCmp("comboNombreDelIF");
				if( comboNombreDelIF.isValid() && Ext.isEmpty( comboNombreDelIF.getValue() ) ){
					comboNombreDelIF.markInvalid("Debe Selecionar el Intermediario Financiero");
					validForm = false;
				}
				// 5. Proveedor
				var numeroNafinElectronico 	= Ext.getCmp("numeroNafinElectronico");
				var clavePyme						= Ext.getCmp("ic_pyme");
				var operaConOfertaDeTasas  	= Ext.getCmp('operaConOfertaDeTasas');
				var operaConOfertaDeTasasMsg 	= Ext.getCmp('operaConOfertaDeTasasMsg');
				if(        numeroNafinElectronico.isValid()   && Ext.isEmpty( numeroNafinElectronico.getValue() ) ){
					numeroNafinElectronico.markInvalid("Capture el Provedor");
					validForm = false;
				} else if( numeroNafinElectronico.isValid()   && Ext.isEmpty( clavePyme.getValue()              ) ){
					numeroNafinElectronico.markInvalid("El Proveedor capturado no es v�lido");
					validForm = false;
				// Para cuando el tipo de tasa seleccionada, es preferencial, se agrega la validacion que revisa que 
				// el proveedor no opere con oferta de tasas por montos.
				} else if( comboTipoDeTasa.getValue() === 'P' && operaConOfertaDeTasas.getValue() === "true"      ){ 
					numeroNafinElectronico.markInvalid( operaConOfertaDeTasasMsg.getValue() );
					/*validForm = false;*/ // Se les deja pasar, haciendo m�mica a la pantalla original
				}
				// 6. Si la tasa seleccionada es tipo Preferencial, validar que se hayan capturado los puntos
				if( comboTipoDeTasa.getValue() === "P" ){ // Tasa preferencial
					// var operaConOfertaDeTasas 	= Ext.getCmp("operaConOfertaDeTasas").getValue();
					var puntos 						= Ext.getCmp("txtPuntos1");
					if( puntos.isValid() && Ext.isEmpty( puntos.getValue() ) ){
						puntos.markInvalid("Debe indicar los puntos");
						validForm = false;
					}
				}
				// 7. Si la tasa seleccionada es tipo Negociada, validar que se haya capturado el Plazo
				if( comboTipoDeTasa.getValue() === "N" ){ // Tasa negociada
					var comboPlazo = Ext.getCmp("lstPlazo1");
					if( comboPlazo.isValid() && Ext.isEmpty( comboPlazo.getValue() ) ){
						comboPlazo.markInvalid("Debe selecionar un Plazo");
						validForm = false;
					}
				}
 
				// 8. La forma es invalida, se suspende la operacion
				if( !validForm ) return;

				// 9. Realizar b�squeda
				forma.el.mask('Procesando...', 'x-mask-loading');			
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 	'Consultar',
						proceso: 		'Procesar'
					})
				});
					
				/*
				var tipoTasa;
				var ic_pyme = Ext.getCmp("ic_pyme1");
				if (Ext.isEmpty(ic_pyme.getValue()) ){
					ic_pyme.markInvalid('Debe Seleccionar un Proveedor.');
					return;
				}
				var tipoTasa1 = Ext.getCmp("tipoTasa1");	
				if(tipoTasa1.getValue()==true) {	
					tipoTasa='P';
					var txtPuntos = Ext.getCmp("txtPuntos1");
					if (Ext.isEmpty(txtPuntos.getValue()) ){
						txtPuntos.markInvalid('Debe de indicar los Puntos ');
						return;
					}
				}
								
				var tipoTasa2 = Ext.getCmp("tipoTasa2");	
				if(tipoTasa2.getValue()==true) {	
					tipoTasa='N';
					var lstPlazo = Ext.getCmp("lstPlazo1");
					if (Ext.isEmpty(lstPlazo.getValue()) ){
						lstPlazo.markInvalid('Debe Seleccionar un Plazo');
						return;
					}
				}
								
				fp.el.mask('Enviando...', 'x-mask-loading');			
				consultaData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'Consultar',
						proceso: 'Procesar',
						tipoTasa:tipoTasa
					})
				});
				*/
			}
		}	
		]
	});

	//------------------------ PANEL MODO PANTALLA --------------------------
	
	var panelModoPantalla = {
		id:			'panelModoPantalla',
      xtype:		'panel',
      frame:		false,
      border:		false,
      bodyStyle:	'background:transparent;',
      layout: 		'hbox',
      style: 		'margin: 0 auto',
      width:		500,
      height: 		75,
      layoutConfig: {
        align:	'middle',
        pack:	'center'
      },
      items: [
		  {	
			  xtype: 		'button',
			  text: 	  	 	'Individual',
			  width:			126,
			  pressed:  	true,
			  id: 			'botonIndividual'
		  },
		  {
			  xtype: 		'box',
			  width:			8,
			  autoWidth: 	false
		  },
		  {
			  xtype: 	'button',
			  text: 	   'Masiva',
			  width:			126,
			  id: 		'botonMasiva',
			  handler: function(){			
				  // Determinar el estado siguiente... ir a la pantalla de inicio
				  var forma 		= Ext.getDom('formAux');
				  forma.action 	= "13forma08mext.jsp";
				  forma.target	   = "_self";
				  forma.submit();
			  }
		  }
     ]
   };
   
   //-------------------------------- PRINCIPAL -----------------------------------
   
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			mensajeAutentificacion,
			NE.util.getEspaciador(20),			
			/* radioTipoTasa, */
			panelModoPantalla,
			fp,			
			gridCifrasControl, 
			NE.util.getEspaciador(20),
			gridConsulta,
			gridAcuse,
			NE.util.getEspaciador(20)			
		]
	});

	// Inicializacion
	catalogoMoneda.load();
	catalogoEPO.load();
	catalogoPlazo.load();
	var comboEpoRelacionadaView 				= Ext.getCmp("ic_epo1").view;
	comboEpoRelacionadaView.multiSelect 	= false;
	comboEpoRelacionadaView.singleSelect 	= true;
	
  //------------------------------------------------------------------------------------

});