Ext.onReady(function() {

		// descarga el archivo PDF de Acuse 
	function procesarPDFAuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// boton para Transmitir  de Credito Electronico a  Garantias
	function transmiteAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {	
			var info = Ext.util.JSON.decode(response.responseText);
						
			Ext.getCmp('btnAceparAcuse').hide();
			Ext.getCmp('btnCancelarAcuse').hide();	
			Ext.getCmp('forma').hide();	
			Ext.getCmp('fpBotones').show();
				
			if(info.respuesta !='ERROR') {
				var preAcuseCifras = [
					['No. total de documentos reasignados Moneda Nacional', info.mnacional],
					['No. total de documentos reasignados D�lares', info.mDolar],
					['N�mero de Acuse', info.acuse],
					['Fecha de reasignaci�n', info.Fecha],
					['Hora de reasignaci�n', info.Hora],
					['Usuario', info.usuario]
				];
				storeCifrasData.loadData(preAcuseCifras);	
				Ext.getCmp('gridCifrasControl').show();
		
				Ext.getCmp('mnacional').setValue(info.mnacional);
				Ext.getCmp('mDolar').setValue(info.mDolar);
				Ext.getCmp('acuse').setValue(info.acuse);
				Ext.getCmp('fechaAcuse').setValue(info.Fecha);
				Ext.getCmp('horaAcuse').setValue(info.Hora);
				Ext.getCmp('documentos').setValue(info.documentos);
				
			}else  {
				Ext.getCmp('btnImprimir').hide();
				Ext.getCmp("mensajeAuto").setValue('<table width="400" align="center" ><tr><td><H1>'+info.mensaje+'</H1></td></tr></table>');
				Ext.getCmp('mensajeAutorizacion').show();	
				Ext.getCmp('gridPreAcuse').hide();
				Ext.getCmp('gridCifrasControl').hide();
			}

		} else {
			NE.util.mostrarConnError(response,opts);							
		}
	}
	
	
	
	var confirmarCarga = function(pkcs7, textoFirmado , contadormn , contadord , contadormontomn, contadormontod , doctos , causaReasignacion, archivo   ){
	
		
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnAceparAcuse").enable();	
			Ext.getCmp("btnCancelarAcuse").enable();	
			return;	//Error en la firma. Termina...
			
		}else  {		
			Ext.getCmp('btnAceparAcuse').disable();
			Ext.getCmp('btnCancelarAcuse').disable();	
			
			Ext.Ajax.request({
				url: '13reasignaciondoctosneg.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'TrasmitirAcuse',					
					textoFirmado:textoFirmado,
					pkcs7:pkcs7,
					contadormn:contadormn,
					contadord:contadord,
					contadormontomn:contadormontomn,
					contadormontod:contadormontod,
					epo: Ext.getCmp("epoO1").getValue(),
					icPymeo: Ext.getCmp('ic_pymeO').getValue(),
					icPymed: Ext.getCmp('ic_pymeD1').getValue(),
					doctos:doctos,
					causaReasignacion:causaReasignacion,
					archivo:archivo,
					nPymeo: Ext.getCmp('nomPymeO1').getValue(),
					nPymed: Ext.getCmp('nomPymeD1').getValue(),
					rfco:  Ext.getCmp('r_f_cO1').getValue(),
					rfcd: Ext.getCmp('r_f_cD1').getValue(),
					direcciono: Ext.getCmp('direccionO1').getValue(),
					direcciond: Ext.getCmp('direccionD1').getValue(),
					coloniao: Ext.getCmp('coloniaO1').getValue(),
					coloniad: Ext.getCmp('coloniaD1').getValue(),
					telefonoo: Ext.getCmp('telefonoO1').getValue(),
					telefonod:	 Ext.getCmp('telefonoD1').getValue()						
				}),
				callback: transmiteAcuse 
			});
		}
	
	}
	
	
	//  funcion para procesar el acuse 
	var procesarAcuse = function() {
		var textoFirmado ="";
		var contadormn=0;
		var contadord=0;
		var contadormontomn=0;
		var contadormontod=0;
		var doctos='';
		var causaReasignacion ='';
		var archivo='';
			
		var  gridPreAcuse = Ext.getCmp('gridPreAcuse');
		var store = gridPreAcuse.getStore();		
		
		store.each(function(record) {
			if(record.data['IC_MONEDA']=='1'){
				contadormn++;
				contadormontomn +=record.data['MONTO_DOCUMENTO'];				
			}else if(record.data['IC_MONEDA']=='54'){	
				contadord++;
				contadormontod +=record.data['MONTO_DOCUMENTO'];
			}				
			doctos +=record.data['IC_DOCUMENTO']+",";
			causaReasignacion =record.data['CAUSA_REASIGNACION'];
			archivo =record.data['ARCHIVO'];
			
			textoFirmado+= record.data['NOMBRE_EPO']+","+ record.data['NOMBRE_PROVEEDOR']+","+ 
								record.data['NO_DOCUMENTO']+","+record.data['FECHA_DOCUMENTO']+","+
								record.data['FECHA_VENCIMIENTO']+","+record.data['MONEDA']+","+
								record.data['TIPO_FACTORAJE']+","+record.data['MONTO_DOCUMENTO']+","+
								record.data['CAUSA_REASIGNACION']+"\n";								
			
		});			
		
		
		NE.util.obtenerPKCS7(confirmarCarga, textoFirmado , contadormn , contadord , contadormontomn, contadormontod , doctos , causaReasignacion, archivo );
				
		
	}
	
	
	//funcion para enviar el archivoa validar 	
	var procesarAceptar = function() {
		var cargaArchivo = Ext.getCmp("cargaArchivo");
		var causaReasignacion = Ext.getCmp("causaReasignacion1");	
		var nomPymeD1 = Ext.getCmp("nomPymeD1");	
		var fp = Ext.getCmp('forma');
		var gridPreAcuse = Ext.getCmp('gridPreAcuse');	
		
		if (Ext.isEmpty(nomPymeD1.getValue()) ) {
			Ext.MessageBox.alert('Mensaje','Debe de asignar la PYME destino ');
			return;
		}
		
		if (Ext.isEmpty(cargaArchivo.getValue()) ) {
			cargaArchivo.markInvalid('El valor de la Ruta es requerida.');	
			return;
		}
		
		if (Ext.isEmpty(causaReasignacion.getValue()) ) {
			causaReasignacion.markInvalid('Debe capturar las causas de reasignaci�n');	
			return;
		}
				
		
		fp.el.mask('Procesando Carga de Archivo..', 'x-mask-loading');	
		fp.getForm().submit({
			url: '13reasignaciondoctosnegfile.jsp',									
			waitMsg: 'Enviando datos...',
			waitTitle :'Por favor, espere',			
			success: function(form, action) {								
				var resp = action.result;
				var mArchivo =resp.archivo;
				var error_tam = resp.error_tam;	
				if(mArchivo!='') {
				
					// editar la columna de CAUSA_REASIGNACION
					var store = gridPreAcuse.getStore();	
					store.each(function(rec){ 
						rec.set('CAUSA_REASIGNACION', causaReasignacion.getValue()); 
						rec.set('ARCHIVO', mArchivo); 
					})
					store.commitChanges();
					
					//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
					var cm = gridPreAcuse.getColumnModel();			
					var el = gridPreAcuse.getGridEl();	
					gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAUSA_REASIGNACION'), false);	
					
					Ext.getCmp("causaReasignacion1").hide();
					Ext.getCmp("archivo").hide();
					Ext.getCmp('noElectronicoD1').hide();
					Ext.getCmp('noProveedorD1').hide();
					Ext.getCmp('rfcD1').hide();
					Ext.getCmp('busqueda').hide();
					Ext.getCmp('btnAceptar').hide();
					Ext.getCmp('btnRegresar').hide();
					
					Ext.getCmp('displayOrigen').show();
					Ext.getCmp('nomPymeO1').show();
					Ext.getCmp('r_f_cO1').show();
					Ext.getCmp('direccionO1').show();
					Ext.getCmp('coloniaO1').show();
					Ext.getCmp('telefonoO1').show();
					Ext.getCmp('btnAceparAcuse').show();
					Ext.getCmp('btnCancelarAcuse').show();
					
				}	else{
					Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB.");
					return;
				}
			}
			,failure: NE.util.mostrarSubmitError
		})
		fp.el.unmask();
	}
	
	
	var mensajeAutorizacion = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAutorizacion',							
		width:	'400',
		heigth:	'auto',
		hidden: true,
		style: 'margin:0 auto;',
		items: [	
			{ 	xtype: 'displayfield',  id: 'mensajeAuto', 	value: '' }				
		]
	});
	
		//Contenedor para los botones de imprimir y Salir 
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'250',
		heigth:	'auto',
		style: 'margin:0 auto;',
		hidden: true,
		items: [	
			{
				xtype: 'button',
				text: 'Imprimir PDF',					
				tooltip:	'Imprimir PDF ',
				iconCls: 'icoPdf',
				id: 'btnImprimir',
				handler: function(boton, evento) {
					Ext.Ajax.request({
						url: '13reasignaciondoctosneg.data.jsp',
						params: {
							informacion: 'ArchivoAcuse',
							contadormn: Ext.getCmp("mnacional").getValue(),
							contadord: Ext.getCmp("mDolar").getValue(),
							acuse: Ext.getCmp("acuse").getValue(),
							fechaAcuse: Ext.getCmp("fechaAcuse").getValue(),
							horaAcuse: Ext.getCmp("horaAcuse").getValue(),
							doctos: Ext.getCmp("documentos").getValue(),
							epo: Ext.getCmp("epoO1").getValue(),
							causaReasignacion: Ext.getCmp("causaReasignacion1").getValue()
						},
						callback: procesarPDFAuse
					});						
				}
			},
			{
				xtype: 'button',
				text: 'Salir',			
				id: 'btnSalir',	
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = "13reasignaciondoctosnegext.jsp";
				}
			}	
		]
	});
	
	//------------Cifras de Control ----------------------------------------
	
	var storeCifrasData = new Ext.data.ArrayStore({
		fields: [
			{name: 'etiqueta'},
			{name: 'informacion'}
		]
	 });
	
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',
		hideHeaders : true,
		hidden: true,
		align: 'center',
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 290,
				sortable : true
			},
			{
				header : 'Informacion',			
				dataIndex : 'informacion',
				width : 350,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 660,
		height: 180,
		style: 'margin:0 auto;',				
		title:'<div><div style="text-align: center;"> </div>',
		frame: true
	});
	
	var consultaPreAcuseData = new Ext.data.ArrayStore({
		fields: [	
			{ name: 'NOMBRE_EPO' },
			{ name: 'NOMBRE_PROVEEDOR' },
			{ name: 'NO_DOCUMENTO' },
			{ name: 'FECHA_DOCUMENTO' },
			{ name: 'FECHA_VENCIMIENTO' },
			{ name: 'IC_MONEDA' },			
			{ name: 'MONEDA' },
			{ name: 'TIPO_FACTORAJE' },
			{ name: 'MONTO_DOCUMENTO' },
			{ name: 'CAMPO_1' },
			{ name: 'CAMPO_2' },
			{ name: 'CAMPO_3' },
			{ name: 'CAMPO_4' },
			{ name: 'CAMPO_5' },
			{ name: 'IC_DOCUMENTO' },
			{ name: 'CAUSA_REASIGNACION' },
			{ name: 'ARCHIVO' }			
		]
	});
	
	var gridPreAcuse = new Ext.grid.EditorGridPanel({	
		store: consultaPreAcuseData,
		id: 'gridPreAcuse',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Documentos Negociables',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombe EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Nombre Proveedor',
				tooltip: 'Nombe Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'No. Documento',
				tooltip: 'No. Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Fecha  Documento',
				tooltip: 'Fecha Documento',
				dataIndex: 'FECHA_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Fecha  Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Campo1',				
				dataIndex: 'CAMPO_1',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},
			{
				header: 'Campo2',				
				dataIndex: 'CAMPO_2',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},
			{
				header: 'Campo3',				
				dataIndex: 'CAMPO_3',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},
			{
				header: 'Campo4',				
				dataIndex: 'CAMPO_4',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},
			{
				header: 'Campo5',				
				dataIndex: 'CAMPO_5',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},
			{
				header: 'Causa Reasignaci�n',				
				dataIndex: 'CAUSA_REASIGNACION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			}	
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 300,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Aceptar',					
					tooltip:	'Aceptar',
					iconCls: 'icoAceptar',
					id: 'btnAceparAcuse',
					hidden: true,
					handler: procesarAcuse					
				},
				'-',
				{
					xtype: 'button',
					text: 'Cancelar',					
					tooltip:	'Cancelar ',
					iconCls: 'icoLimpiar',
					id: 'btnCancelarAcuse',
					hidden: true,
					handler: function(boton, evento) {
						window.location = '13reasignaciondoctosnegext.jsp';
					}
				}			
			]
		}
	});
	
	
	// --------------------funci�n mostrar campos Adicionales  ---------------------------
	
	var procesarCamposAdicionales =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var gridPreAcuse = Ext.getCmp('gridPreAcuse');	
			var el = gridPreAcuse.getGridEl();	
			var  jsonData = Ext.util.JSON.decode(response.responseText);
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridPreAcuse.getColumnModel();			
			var el = gridPreAcuse.getGridEl();	
			
			if(jsonData.hayCamposAdicionales=='0'){
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='1'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='2'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_2'),jsonData.nomCampo2);
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='3'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_2'),jsonData.nomCampo2);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_3'),jsonData.nomCampo3);					
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), true);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='4'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_2'),jsonData.nomCampo2);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_3'),jsonData.nomCampo3);					
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='5'){
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_2'),jsonData.nomCampo2);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_3'),jsonData.nomCampo3);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_4'),jsonData.nomCampo4);
				gridPreAcuse.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_5'),jsonData.nomCampo5);						
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), false);	
				gridPreAcuse.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), false);
			}	
		} else {
			
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	// --------------------funci�n para la Resignaci�n ---------------------------
	
	var procesarReasignar = function() {	
		var gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();	
		var modificado = [];
		var registrosPreAcu = []; //para agregar los registros al Preacuse  y acuse 
		
		var gridPreAcuse = Ext.getCmp('gridPreAcuse');			
		var fp = Ext.getCmp('forma');
				
		store.each(function(record) {			
			if(record.data['SELECCIONAR']==true){
				registrosPreAcu.push(record);	
				modificado.push(record.data['IC_DOCUMENTO']);
			}
		});
		
		if(modificado =='') {
			Ext.MessageBox.alert('Mensaje','Debe seleccionar un  documento ');
			return false;	
		}else  {
			
			Ext.Ajax.request({
				url: '13reasignaciondoctosneg.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{															
					informacion:'CamposAdicionales',
					epo: Ext.getCmp("epoO1").getValue()
				}),
				callback: procesarCamposAdicionales
			});
			
			consultaPreAcuseData.add(registrosPreAcu);
			gridPreAcuse.show();
			gridConsulta.hide()
			fp.setTitle('<div align="center"> <b>PYME DESTINO </div>');
			
			Ext.getCmp('epoO1').hide();
			Ext.getCmp('noElectronicoO1').hide();
			Ext.getCmp('noProveedorO1').hide();
			Ext.getCmp('noDoctoO1').hide();
			Ext.getCmp('rfcO1').hide();
			Ext.getCmp('displayOrigen').hide();
			Ext.getCmp('nomPymeO1').hide();
			Ext.getCmp('r_f_cO1').hide();
			Ext.getCmp('direccionO1').hide();
			Ext.getCmp('coloniaO1').hide();
			Ext.getCmp('telefonoO1').hide();
			
			Ext.getCmp('btnBuscar').hide();
			Ext.getCmp('btnlimpiar').hide();
			
			Ext.getCmp('noElectronicoD1').show();
			Ext.getCmp('noProveedorD1').show();
			Ext.getCmp('rfcD1').show();
			Ext.getCmp('displayDestino').show();
			Ext.getCmp('nomPymeD1').show();
			Ext.getCmp('r_f_cD1').show();
			Ext.getCmp('direccionD1').show();
			Ext.getCmp('coloniaD1').show();
			Ext.getCmp('telefonoD1').show();
			Ext.getCmp('archivo').show();
			Ext.getCmp('causaReasignacion1').show();
			Ext.getCmp('btnAceptar').show();
			Ext.getCmp('btnRegresar').show();

		}
	}
	
//-------------------------------------GRID DE CONSULTA ---------------------------------------
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
							
		var griConsulta = Ext.getCmp('griConsulta');	
		var el = gridConsulta.getGridEl();	
	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();			
			var el = gridConsulta.getGridEl();	
			
			if(jsonData.hayCamposAdicionales=='0'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='1'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='2'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_2'),jsonData.nomCampo2);
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='3'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_2'),jsonData.nomCampo2);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_3'),jsonData.nomCampo3);					
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), true);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='4'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_2'),jsonData.nomCampo2);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_3'),jsonData.nomCampo3);					
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), true);
			}
			if(jsonData.hayCamposAdicionales=='5'){
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_1'),jsonData.nomCampo1);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_2'),jsonData.nomCampo2);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_3'),jsonData.nomCampo3);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_4'),jsonData.nomCampo4);
				gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO_5'),jsonData.nomCampo5);						
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_1'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_2'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_3'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_4'), false);	
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO_5'), false);
			}	
				
			if(store.getTotalCount() > 0) {			
				el.unmask();	
				Ext.getCmp('btnReasignar').enable();
				Ext.getCmp('btnCancelar').enable();
			} else {					
				el.mask('No se encontr� ning�n registro', 'x-mask');	
				Ext.getCmp('btnReasignar').disable();
				Ext.getCmp('btnCancelar').disable();
			}
		}
	}

	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13reasignaciondoctosneg.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [	
			{ name: 'NOMBRE_EPO' },
			{ name: 'NOMBRE_PROVEEDOR' },
			{ name: 'NO_DOCUMENTO' },
			{ name: 'FECHA_DOCUMENTO' },
			{ name: 'FECHA_VENCIMIENTO' },
			{ name: 'IC_MONEDA' },
			{ name: 'MONEDA' },
			{ name: 'TIPO_FACTORAJE' },
			{ name: 'MONTO_DOCUMENTO' },
			{ name: 'CAMPO_1' },
			{ name: 'CAMPO_2' },
			{ name: 'CAMPO_3' },
			{ name: 'CAMPO_4' },
			{ name: 'CAMPO_5' },
			{ name: 'IC_DOCUMENTO' },			
			{ name: 'SELECCIONAR'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Documentos Negociables',
		clicksToEdit: 1,
		hidden: true,		
		columns: [	
			{
				header: 'Nombre EPO',
				tooltip: 'Nombe EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'Nombre Proveedor',
				tooltip: 'Nombe Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'	
			},	
			{
				header: 'No. Documento',
				tooltip: 'No. Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Fecha  Documento',
				tooltip: 'Fecha Documento',
				dataIndex: 'FECHA_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Fecha  Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'	
			},	
			{
				header: 'Monto Documento',
				tooltip: 'Monto Documento',
				dataIndex: 'MONTO_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Campo1',				
				dataIndex: 'CAMPO_1',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},
			{
				header: 'Campo2',				
				dataIndex: 'CAMPO_2',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},
			{
				header: 'Campo3',				
				dataIndex: 'CAMPO_3',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},
			{
				header: 'Campo4',				
				dataIndex: 'CAMPO_4',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},
			{
				header: 'Campo5',				
				dataIndex: 'CAMPO_5',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left',
				hidden: true
			},				
			{
				xtype: 'checkcolumn',				
				header:'Seleccionar',
				tooltip: 'Seleccionar',
				dataIndex : 'SELECCIONAR',
				width : 150,
				align: 'center',
				sortable : false			
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Reasignar',					
					tooltip:	'Reasignar',
					iconCls: 'icoAceptar',
					id: 'btnReasignar',
					handler: procesarReasignar
				},
				'-',
				{
					xtype: 'button',
					text: 'Cancelar',					
					tooltip:	'Cancelar ',
					iconCls: 'icoLimpiar',
					id: 'btnCancelar',
					handler: function(boton, evento) {
						window.location = '13reasignaciondoctosnegext.jsp';
					}
				}		
			]
		}
	});
	
  //--------------CRITERIOS DE BUSQUEDA -----------------------
  	
	// Funci�n para  obtener los datos de la Pyme Destino
  	function procesarDatosPymeD(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			jsonData = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('nomPymeD1').setValue(jsonData.nomPyme);
			Ext.getCmp('r_f_cD1').setValue(jsonData.r_f_c);
			Ext.getCmp('direccionD1').setValue(jsonData.direccion);
			Ext.getCmp('coloniaD1').setValue(jsonData.colonia);
			Ext.getCmp('telefonoD1').setValue(jsonData.telefono);
			Ext.getCmp('ic_pymeD1').setValue(jsonData.ic_pyme);
			
			if(jsonData.mensaje!='') {
				Ext.MessageBox.alert('Mensaje',jsonData.mensaje);
				Ext.getCmp('noElectronicoD1').setValue('');
				Ext.getCmp('noProveedorD1').setValue('');
				Ext.getCmp('rfcD1').setValue('');				
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var fnPintarPymeD = function(){
		var noElectronico = Ext.getCmp("noElectronicoD1");
		var noProveedor = Ext.getCmp("noProveedorD1");		
		var rfc = Ext.getCmp("rfcD1");
		
		if( !Ext.isEmpty(noElectronico.getValue()) || !Ext.isEmpty(noProveedor.getValue()) || !Ext.isEmpty(rfc.getValue()) ){
			Ext.Ajax.request({
				url : '13reasignaciondoctosneg.data.jsp',
				params: {
					informacion: 'DatosPyme',	
					epo: Ext.getCmp("epoD1").getValue(),
					noElectronico: Ext.getCmp("noElectronicoD1").getValue(),
					noProveedor: Ext.getCmp("noProveedorD1").getValue(),					
					rfc: Ext.getCmp("rfcD1").getValue(),
					tipoPyme: 'destino'
				},
				callback: procesarDatosPymeD
			});
		}
	}
	
	// Funci�n para  obtener los datos de la Pyme Origen 
	
	function procesarDatosPyme(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var fp = Ext.getCmp('forma');
			fp.el.unmask();
			jsonData = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('nomPymeO1').setValue(jsonData.nomPyme);
			Ext.getCmp('r_f_cO1').setValue(jsonData.r_f_c);
			Ext.getCmp('direccionO1').setValue(jsonData.direccion);
			Ext.getCmp('coloniaO1').setValue(jsonData.colonia);
			Ext.getCmp('telefonoO1').setValue(jsonData.telefono);
			Ext.getCmp('ic_pymeO').setValue(jsonData.ic_pyme);
			Ext.getCmp('epoD1').setValue(jsonData.epo);
			
			if(jsonData.mensaje!='') {
				Ext.MessageBox.alert('Mensaje',jsonData.mensaje);
				Ext.getCmp('noElectronicoO1').setValue('');
				Ext.getCmp('noProveedorO1').setValue('');
				Ext.getCmp('noDoctoO1').setValue('');
				Ext.getCmp('rfcO1').setValue('');
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var fnPintarPyme = function(){
	
		var epo = Ext.getCmp("epoO1");
		var noElectronico = Ext.getCmp("noElectronicoO1");
		var noProveedor = Ext.getCmp("noProveedorO1");
		var noDocto = Ext.getCmp("noDoctoO1");
		var rfc = Ext.getCmp("rfcO1");
		
		if( !Ext.isEmpty(noElectronico.getValue()) || !Ext.isEmpty(noProveedor.getValue()) || !Ext.isEmpty(noDocto.getValue()) || !Ext.isEmpty(rfc.getValue()) ){
			if (Ext.isEmpty(epo.getValue()) ){
				epo.markInvalid('Debe de seleccionar una EPO');
				return;
			}else   {
				Ext.Ajax.request({
					url : '13reasignaciondoctosneg.data.jsp',
					params: {
						informacion: 'DatosPyme',
						epo: Ext.getCmp("epoO1").getValue(),
						noElectronico: Ext.getCmp("noElectronicoO1").getValue(),
						noProveedor: Ext.getCmp("noProveedorO1").getValue(),
						noDocto: Ext.getCmp("noDoctoO1").getValue(),
						rfc: Ext.getCmp("rfcO1").getValue(),
						tipoPyme: 'origen'
					},
					callback: procesarDatosPyme
				});
			}
		}
	}
	
	var catalogoEPO = new Ext.data.JsonStore({
		id: 'catalogoEPO',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13reasignaciondoctosneg.data.jsp',
		baseParams: {
			informacion: 'catalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	

	
	//----------------FORMA PARA LA PYME ORIGEN ------------------------------
	var  elementosForma  = [
		{ 	xtype: 'textfield',  hidden: true, id: 'ic_pymeO', 	value: '' },
		{
			xtype: 'combo',
			fieldLabel: 'EPO',
			name: 'epoO',
			id: 'epoO1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccionar ...',			
			valueField: 'clave',	
			hiddenName : 'epoO',
			forceSelection : true,			
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,			
			store: catalogoEPO,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select:{ 
					fn:function (combo) {		
						fnPintarPyme();
					}
				}
			}
		},	
		{
			xtype: 'displayfield',			
			value: '<div align="center"> <b>Criterios de B�squeda </div>',			
			width: 500,
			id: 'busqueda'
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'N�mero Electronico',		
			name: 'noElectronicoO',
			id: 'noElectronicoO1',
			allowBlank: true,
			maxLength: 38,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 																										
					if(field.getValue() != '') {						
						fnPintarPyme();
					}
				}
			}
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'N�mero Proveedor',		
			name: 'noProveedorO',
			id: 'noProveedorO1',
			allowBlank: true,
			maxLength: 25,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 																										
					if(field.getValue() != '') {
						fnPintarPyme();
					}
				}
			}
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'N�mero Documento',		
			name: 'noDoctoO',
			id: 'noDoctoO1',
			allowBlank: true,
			maxLength: 15,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 																										
					if(field.getValue() != '') {
						fnPintarPyme();
					}
				}
			}
		},
		{
			xtype: 'textfield',
			fieldLabel: 'RFC',		
			name: 'rfcO',
			id: 'rfcO1',
			allowBlank: true,
			maxLength: 15,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					fnPintarPyme();
				}
			}
		},
		{
			xtype: 'displayfield',			
			value: '<div align="center"> <b>Datos de la PyME ORIGEN </div>',
			width: 500,
			id: 'displayOrigen'
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'Nombre PyME',		
			name: 'nomPymeO',
			id: 'nomPymeO1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'RFC',		
			name: 'r_f_cO',
			id: 'r_f_cO1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'Direcci�n',		
			name: 'direccionO',
			id: 'direccionO1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'Colonia',		
			name: 'coloniaO',
			id: 'coloniaO1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'Telefono',		
			name: 'telefonoO',
			id: 'telefonoO1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0'			
		},
	 //---------------DATOS PARA  LA PYME DESTINO ------------------------------
		
		{ 	xtype: 'textfield',  hidden: true, id: 'ic_pymeD1', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'epoD1', 	value: '' },			
		{
			xtype: 'numberfield',
			fieldLabel: 'N�mero Electronico',		
			name: 'noElectronicoD',
			id: 'noElectronicoD1',
			allowBlank: true,
			maxLength: 38,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true,
			listeners:{
				blur: function(field){ 																										
					if(field.getValue() != '') {						
						fnPintarPymeD();
					}
				}
			}
		},
		{
			xtype: 'numberfield',
			fieldLabel: 'N�mero Proveedor',		
			name: 'noProveedorD',
			id: 'noProveedorD1',
			allowBlank: true,
			maxLength: 25,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true,
			listeners:{
				blur: function(field){ 																										
					if(field.getValue() != '') {
						fnPintarPymeD();
					}
				}
			}
		},		
		{
			xtype: 'textfield',
			fieldLabel: 'RFC',		
			name: 'rfcD',
			id: 'rfcD1',
			allowBlank: true,
			maxLength: 15,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true,
			listeners:{
				blur: function(field){ 
					fnPintarPymeD();
				}
			}
		},
		{
			xtype: 'displayfield',
			value: '<div align="center"> <b>Datos de la PyME DESTINO </div>',
			width: 500,
			id: 'displayDestino',
			hidden: true
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'Nombre PyME',		
			name: 'nomPymeD',
			id: 'nomPymeD1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'RFC',		
			name: 'r_f_cD',
			id: 'r_f_cD1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'Direcci�n',		
			name: 'direccionD',
			id: 'direccionD1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'Colonia',		
			name: 'coloniaD',
			id: 'coloniaD1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true
		},
		{
			xtype: 'displayfield',
			fieldLabel: 'Telefono',		
			name: 'telefonoD',
			id: 'telefonoD1',
			allowBlank: true,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			id: 'archivo',
			hidden: true,
			combineErrors: false,
			msgTarget: 'side',
			hidden: true,
			width: 500,
			items: [
				{
					xtype: 'fileuploadfield',
					id: 'cargaArchivo',
					emptyText: 'Documento de Autorizaci�n',
					fieldLabel: 'Documento de Autorizaci�n',
					name: 'cargaArchivo_',   
					buttonText: ' ',
					width: 500,	  
					buttonCfg: {
						iconCls: 'upload-icon'
					},
					anchor: '100%'
				}						
			]
		},								
		{
			xtype: 'textfield',
			fieldLabel: '*Causas de la Reasignaci�n',		
			name: 'causaReasignacion',
			id: 'causaReasignacion1',
			allowBlank: true,
			maxLength: 50,
			width: 500,
			msgTarget: 'side',
			margins: '0 20 0 0',
			hidden: true
		},
		{ 	xtype: 'textfield', hidden: true,  id: 'mnacional', 	value: '' },
		{ 	xtype: 'textfield', hidden: true,  id: 'mDolar', 	   value: '' },
		{ 	xtype: 'textfield', hidden: true,  id: 'acuse', 	   value: '' },
		{ 	xtype: 'textfield', hidden: true,  id: 'fechaAcuse', 	value: '' },
		{ 	xtype: 'textfield', hidden: true,  id: 'horaAcuse', 	value: '' },
		{ 	xtype: 'textfield', hidden: true,  id: 'documentos', 	value: '' }		
	];
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		title: '<div align="center"> <b>PYME ORIGEN</div>',
		frame: true,		
		fileUpload: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,							
		buttons: [		
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {					
				
					var epo = Ext.getCmp("epoO1");
					var noElectronico = Ext.getCmp("noElectronicoO1");
					var noProveedor = Ext.getCmp("noProveedorO1");
					var noDocto = Ext.getCmp("noDoctoO1");
					var rfc = Ext.getCmp("rfcO1");
					
					if (  Ext.isEmpty(epo.getValue()) ){
						epo.markInvalid('Debe de seleccionar una EPO');
						return;
					}
					if( Ext.isEmpty(noElectronico.getValue()) &&  Ext.isEmpty(noProveedor.getValue()) &&  Ext.isEmpty(noDocto.getValue()) &&  Ext.isEmpty(rfc.getValue()) ){
						Ext.MessageBox.alert('Mensaje',"Debe capturar al menos un dato de la PyME");
						return;
					}
									
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params:{
							informacion: 'Consultar',
							epo: Ext.getCmp("epoO1").getValue(),
							noElectronico: Ext.getCmp("noElectronicoO1").getValue(),
							noProveedor: Ext.getCmp("noProveedorO1").getValue(),
							noDocto: Ext.getCmp("noDoctoO1").getValue(),
							rfc: Ext.getCmp("rfcO1").getValue(),
							ic_pyme: Ext.getCmp("ic_pymeO").getValue()
						}
					});			
				}
			},
			{
				text: 'Limpiar',
				id: 'btnlimpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13reasignaciondoctosnegext.jsp';					
				}
			},
			{
				text: 'Aceptar',
				id: 'btnAceptar',
				iconCls: 'icoAceptar',
				formBind: true,	
				hidden: true,
				handler: procesarAceptar
			},
			{
				text: 'Regresar',
				id: 'btnRegresar',
				iconCls: 'icoLimpiar',
				hidden: true,
				handler: function() {
					window.location = '13reasignaciondoctosnegext.jsp';					
				}
			}
		]
	});
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,	
			gridCifrasControl,
			mensajeAutorizacion,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),	
			gridConsulta,
			gridPreAcuse,
			NE.util.getEspaciador(20)			
		]
	});
	
	catalogoEPO.load();
	
});	
