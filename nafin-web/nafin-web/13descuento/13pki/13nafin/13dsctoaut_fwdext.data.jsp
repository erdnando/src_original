<%@ page 
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.CatalogoEPOPyme,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.descuento.*, 
		com.netro.afiliacion.*,
		com.netro.exception.*,
		com.netro.seguridadbean.*,
		javax.naming.*,
		com.netro.pdf.*,
		net.sf.json.*"
		contentType="application/json;charset=UTF-8"
		errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
	request.setAttribute("producto","1");
	request.setAttribute("facultad","/nafin/13descuento/13pki/13nafin/15dsctoaut02ext.jsp");
	request.setAttribute("cveFacultad","13NAFINDSCTOAUTFWD");

	String informacion = request.getParameter("informacion")==null?"":request.getParameter("informacion");
	
	//Obtener los datos de la forma
	String num_pyme = request.getParameter("num_pyme")==null?"":request.getParameter("num_pyme");
	String nombre_pyme = request.getParameter("nombre_pyme")==null?"":request.getParameter("nombre_pyme");
	String rfc_pyme = request.getParameter("rfc_pyme")==null?"":request.getParameter("rfc_pyme");
	String num_sirac_pyme = request.getParameter("num_sirac_pyme")==null?"":request.getParameter("num_sirac_pyme");
	String ic_pyme = request.getParameter("ic_pyme")==null?iNoCliente:request.getParameter("ic_pyme");
	String ic_epo 			= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
	String regresa = null;
	
		
	if(informacion.equals("obtienePYMES")){
		HashMap datos = null;
		List pyme = null;
		
		ParametrizacionDsctoAutomaticoDE parametros 	= new ParametrizacionDsctoAutomaticoDE();
		pyme = parametros.getProveedores(ic_epo, num_pyme, rfc_pyme, nombre_pyme,ic_pyme,"1",num_sirac_pyme, strLogin);
	
      JSONObject  jsonObj   =  new JSONObject();
      jsonObj.put("success",   new Boolean(true));
      jsonObj.put("registros",   pyme);
      regresa = jsonObj.toString();
	}
	
	else if(informacion.equals("obtieneDireccion")){
		List domicilio = new ArrayList();
		
		System.out.println(ic_pyme);
		
		ParametrizacionDsctoAutomaticoDE parametros 	= new ParametrizacionDsctoAutomaticoDE();
		domicilio = parametros.getDomicilioPyme(ic_pyme);
		
      JSONObject  jsonObj   =  new JSONObject();
      jsonObj.put("success",   new Boolean(true));
      jsonObj.put("registros",   domicilio);
      regresa = jsonObj.toString();
	}
	System.err.println(regresa);
	
%>

<%=regresa%>