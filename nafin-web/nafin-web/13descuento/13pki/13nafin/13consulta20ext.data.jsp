<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,	
	netropology.utilerias.*,		
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	com.netro.dispersion.*,
	com.netro.descuento.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String ic_epo = (request.getParameter("ic_epo") != null) ? request.getParameter("ic_epo") : "";
String ic_if = (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") : "";
String num_electronico = (request.getParameter("num_electronico") != null) ? request.getParameter("num_electronico") : "";
String df_fecha_seleccion_de = (request.getParameter("df_fecha_seleccion_de") != null) ? request.getParameter("df_fecha_seleccion_de") : "";
String df_fecha_seleccion_a = (request.getParameter("df_fecha_seleccion_a") != null) ? request.getParameter("df_fecha_seleccion_a") : "";
String df_fecha_autorizaif_de = (request.getParameter("df_fecha_autorizaif_de") != null) ? request.getParameter("df_fecha_autorizaif_de") : "";
String df_fecha_autorizaif_a = (request.getParameter("df_fecha_autorizaif_a") != null) ? request.getParameter("df_fecha_autorizaif_a") : "";
String ig_numero_docto = (request.getParameter("ig_numero_docto") != null) ? request.getParameter("ig_numero_docto") : "";
String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String total = (request.getParameter("total") != null) ? request.getParameter("total") : "15";

SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
String fechaActual = formatter.format(Fecha.getSysDate());


String infoRegresar="",  consulta="", 	txtNombre ="";
int  start= 0, limit =0;

HashMap datos = new HashMap();
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

ConsCanTrans paginador = new ConsCanTrans(); // declaración del paginador 
	
if (informacion.equals("catalogoBancoFondeo")){

	CatalogoSimple catalogo = new CatalogoSimple(); 
	catalogo.setCampoClave("ic_banco_fondeo");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_banco_fondeo");	
	if("ADMIN BANCOMEXT".equals(strPerfil)) {
		catalogo.setValoresCondicionIn("2", Integer.class);
	}		
	infoRegresar = catalogo.getJSONElementos();
	
}else  if (informacion.equals("catalogoEPO")){

	CatalogoEPO catalogo = new CatalogoEPO();	
	List resultCatEpo = new ArrayList();	
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");		
	resultCatEpo	= catalogo.getListaElementosModalidad();
	if(resultCatEpo.size() >0 ){	
		for (int i= 0; i < resultCatEpo.size(); i++) {			
			String registro =  resultCatEpo.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
	consulta =  "{\"success\": true, \"total\": \"" + resultCatEpo.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	


}else  if (informacion.equals("catalogoIF")){

	List resultCatIF = new ArrayList();
	CatalogoIF catalogo = new CatalogoIF();	
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");		
	catalogo.setClaveEpo(ic_epo);
	resultCatIF	= catalogo.getListaElementosIF();
			
	if(resultCatIF.size() >0 ){	
		for (int i= 0; i < resultCatIF.size(); i++) {			
			String registro =  resultCatIF.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
	consulta =  "{\"success\": true, \"total\": \"" + resultCatIF.size() + "\", \"registros\": " + registros.toString()+"}";
	jsonObj = JSONObject.fromObject(consulta);	
	infoRegresar = jsonObj.toString();	
	
 } else if (informacion.equals("busquedaAvanzada") ) {

	String rfc_pyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
	String nombre_pyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
	String num_pyme = request.getParameter("noPyme")==null?"":request.getParameter("noPyme");
	
	CatalogoPymeBusqAvanzadaIF cat = new CatalogoPymeBusqAvanzadaIF();
	cat.setCampoClave("crn.ic_nafin_electronico");
	cat.setCampoDescripcion("crn.ic_nafin_electronico || ' ' || p.cg_razon_social ");
	cat.setIc_epo(ic_epo);
	cat.setRfc_pyme(rfc_pyme);
	cat.setNombre_pyme(nombre_pyme);	
	cat.setNum_pyme(num_pyme);	
	cat.setIc_pyme(ic_pyme);	
	cat.setOrden("p.cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}  else if (informacion.equals("pymeNombre")  ) {	
	
	List datosPymes =  paginador.busquedadatosPymes(ic_epo, num_electronico ); //para obtener 
	if(datosPymes.size()>0){
		ic_pyme= (String)datosPymes.get(0);
		txtNombre= (String)datosPymes.get(1);
	}
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("txtNombre", txtNombre);
	jsonObj.put("ic_pyme", ic_pyme);
	infoRegresar = jsonObj.toString();		
	
} else if(informacion.equals("Consultar")){

	paginador.setIc_epo(ic_epo);
	paginador.setIc_if(ic_if);
	paginador.setDf_fecha_seleccion_de(df_fecha_seleccion_de);
	paginador.setDf_fecha_seleccion_a(df_fecha_seleccion_a);
	paginador.setDf_fecha_autorizaif_de(df_fecha_autorizaif_de);
	paginador.setDf_fecha_autorizaif_a(df_fecha_autorizaif_a);
	paginador.setIg_numero_docto(ig_numero_docto);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_estatus_docto("4");
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);

	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	try {
		if (operacion.equals("Generar")) { //Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			/*
			Este es un truco barato para obtener el total de registros, ya que posteriormente se vuelve a hacer la misma consulta 
			pero solo obtiene 15 registros y provoca que la paginación no se muestre correctamente
			*/
			String consulta1 = queryHelper.getJSONPageResultSet(request,start,limit);
			consulta1 = consulta1.replaceAll("\"", "");
			String[] datosTmp = consulta1.split(",");
			String llave = "";
			String clave = "";
			label:
			for(int i=0; i<datosTmp.length; i++){
				try{
					String[] datoTmp = datosTmp[i].split(":");
					llave = datoTmp[0];
					clave = datoTmp[1];
					System.out.println("<"+ llave +">");
					if((llave.trim()).equals("total")){
						break label;
					}
				}catch(Exception e){
					System.out.println("Error al leer el String");
					break label;
				}
			}
			total = clave.trim();
		}

		Registros reg   = queryHelper.getPageResultSet(request,start,limit);

		int cont = 0;
		while(reg.next()){
			
			String fecha_solicitud = (reg.getString("FECHA_SOLICITUD")==null)?"":(reg.getString("FECHA_SOLICITUD"));
			String ic_documento = (reg.getString("IC_DOCUMENTO")==null)?"":(reg.getString("IC_DOCUMENTO"));
			String ic_estatus = (reg.getString("IC_ESTATUS")==null)?"":(reg.getString("IC_ESTATUS"));
			
			//fecha de operacion le sumamos dos dias habiles para despues comparala con la actual
			String nuevafecha = Fecha.sumaFechaDiasHabiles(fecha_solicitud, "dd/MM/yyyy", 2);
			//dias reales entre la fecha actual y la nueva sumandole 2 dias habiles a la del docto
			cont++;
			int diasreales = Fecha.restaFechas(nuevafecha,fechaActual);
			//si el numero de dias reales es mayor a cero significa que esa fecha supera los dos dias habiles perimitidos
			if(diasreales>0){
				reg.setObject("DF_FECHA_DOCTO","N");
			}else {
				reg.setObject("DF_FECHA_DOCTO","S");
			}

			int diasNuevoEStatus=  paginador.diasreales(ic_documento,fechaActual );
			int newEstatusDocto =0;
			String newNameEstatus ="";

			if("4".equals(ic_estatus)){
				newEstatusDocto = diasNuevoEStatus>0?31:42;
			}else if("11".equals(ic_estatus)){
				newEstatusDocto = 43;
			}

			if(newEstatusDocto == 42) {
				newNameEstatus = "Vencido sin Operar";
			} else if(newEstatusDocto == 43){
				newNameEstatus = "Pagado sin Operar";
			}

			reg.setObject("IC_ESTATUS_NUEVO",String.valueOf(newEstatusDocto));
			reg.setObject("ESTATUS_NUEVO",newNameEstatus);
			reg.setObject("ESTATUS_NUEVO_A",newNameEstatus);

		}

		//consulta = "{\"success\": true, \"total\": \""+ reg.getNumeroRegistros() +"\", \"registros\": " + reg.getJSONData()+"}";
		consulta = "{\"success\": true, \"total\": \""+ total +"\", \"registros\": " + reg.getJSONData()+"}";

	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar=jsonObj.toString();

} else if (informacion.equals("cancelaDocto")){

//String ic_documento = (request.getParameter("ic_documento") != null) ? request.getParameter("ic_documento") : "";
String ic_docs_ic_est_nue[] = request.getParameterValues("ic_docs_ic_est_nue");

String newEstatusDocto = (request.getParameter("newEstatusDocto") != null) ? request.getParameter("newEstatusDocto") : "";
String causaCancelacion = (request.getParameter("causaCancelacion") != null) ? request.getParameter("causaCancelacion") : "";
String fileSize = (request.getParameter("fileSize") != null) ? request.getParameter("fileSize") : "";
String nombreArchivo = (request.getParameter("nombreArchivo") != null) ? request.getParameter("nombreArchivo") : "";
String txtUploadPath = getServletConfig().getServletContext().getRealPath("/")+"/00archivos/13descuento/";
String extension = nombreArchivo.substring(nombreArchivo.length()-3,nombreArchivo.length());
String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
String textoFirmado = (request.getParameter("textoFirmado") != null) ? request.getParameter("textoFirmado") : "";


Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
	
HashMap resultado = dispersion.autenticaFirmaDigitalv2(pkcs7, textoFirmado, _serial, iNoCliente);
String ic_folio 	= (String)resultado.get("FOLIO");
String recibo 	= (String)resultado.get("RECIBO"); 
boolean hayAutentificacionExitosa = ((String)resultado.get("VERIFICACION_EXITOSA")).equals("true")?true:false;
String respuesta="", mensaje= "";

String ic_documento="";
if(hayAutentificacionExitosa){
	
	
        respuesta =   paginador.cancelaDocumentos(ic_docs_ic_est_nue,  newEstatusDocto, causaCancelacion , strNombreUsuario,  extension, fileSize , nombreArchivo ,  txtUploadPath  );
	if(respuesta.equals("Cancelacion_Exitosa")) {
		mensaje = "La autentificación se llevó a cabo con éxito  " +ic_folio;
	}else  {
		mensaje = "Autenticación con error favor de intentarlo de nuevo.";
		respuesta="Cancelacion_Error";
	}

}else  {
	mensaje = "Autenticación con error favor de intentarlo de nuevo.";
	respuesta="Cancelacion_Error";
}


jsonObj.put("success", new Boolean(true));	
jsonObj.put("respuesta", respuesta);
jsonObj.put("mensaje", mensaje);
jsonObj.put("ic_folio", ic_folio);
jsonObj.put("ic_documento", ic_documento);
infoRegresar = jsonObj.toString();		

}  else if (informacion.equals("ArchivoAcuse")  ) {	

	String nombreEPO = (request.getParameter("nombreEPO") != null) ? request.getParameter("nombreEPO") : "";
	String num_sirac = (request.getParameter("num_sirac") != null) ? request.getParameter("num_sirac") : "";
	String nombre_pyme = (request.getParameter("nombre_pyme") != null) ? request.getParameter("nombre_pyme") : "";
	String num_docto = (request.getParameter("num_docto") != null) ? request.getParameter("num_docto") : "";
	String estatus_nuevo = (request.getParameter("estatus_nuevo") != null) ? request.getParameter("estatus_nuevo") : "";
	String moneda = (request.getParameter("moneda") != null) ? request.getParameter("moneda") : "";
	String monto_documento = (request.getParameter("monto_documento") != null) ? request.getParameter("monto_documento") : "";
	String porce_descuento = (request.getParameter("porce_descuento") != null) ? request.getParameter("porce_descuento") : "";
	String recurso_garant = (request.getParameter("recurso_garant") != null) ? request.getParameter("recurso_garant") : "";
	String monto_desc = (request.getParameter("monto_desc") != null) ? request.getParameter("monto_desc") : "";
	String monto_int = (request.getParameter("monto_int") != null) ? request.getParameter("monto_int") : "";
	String monto_operar = (request.getParameter("monto_operar") != null) ? request.getParameter("monto_operar") : "";
	String ic_folio = (request.getParameter("ic_folio") != null) ? request.getParameter("ic_folio") : "";
	String nombreFideicomiso = (request.getParameter("nombreFideicomiso") != null) ? request.getParameter("nombreFideicomiso") : "";
	String interesFideicomiso = (request.getParameter("interesFideicomiso") != null) ? request.getParameter("interesFideicomiso") : "";
	String montoFideicomiso = (request.getParameter("montoFideicomiso") != null) ? request.getParameter("montoFideicomiso") : "";
	String nombreIF = (request.getParameter("nombreIF") != null) ? request.getParameter("nombreIF") : "";
        String ic_docs_ic_est_nue[] = request.getParameterValues("ic_docdos_PDF");
        List  datosArc =  new ArrayList();
        for (String tmpIcDocIcEsta: ic_docs_ic_est_nue){
            System.out.println(tmpIcDocIcEsta);
            StringTokenizer stTok = new StringTokenizer(tmpIcDocIcEsta, "|");
            while (stTok.hasMoreElements()) {
                List  regisArc =  new ArrayList();
                regisArc.add(stTok.nextElement().toString()); // 1
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString());	//5
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString()); //10
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(ic_folio);	
                regisArc.add(stTok.nextElement().toString()); // 14
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString());
                regisArc.add(stTok.nextElement().toString()); //17
                datosArc.add(regisArc);
            }
        }
        
	
	/*List datosArc =  new ArrayList();
	datosArc.add(nombreEPO); 
	datosArc.add(num_sirac);
	datosArc.add(nombre_pyme);
	datosArc.add(num_docto);
	datosArc.add(estatus_nuevo);	
	datosArc.add(moneda);
	datosArc.add(monto_documento);
	datosArc.add(porce_descuento);
	datosArc.add(recurso_garant);
	datosArc.add(monto_desc);
	datosArc.add(monto_int);
	datosArc.add(monto_operar);
	datosArc.add(ic_folio);	
	datosArc.add(nombreFideicomiso);
	datosArc.add(interesFideicomiso);
	datosArc.add(montoFideicomiso);
	datosArc.add(nombreIF);*/

	String nombreArchivo  = paginador.crearArchivoPDF (request, datosArc, strDirectorioTemp, ic_folio);
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);		
	infoRegresar = jsonObj.toString();
	
}

%>
<%=infoRegresar%>

