/**
*	FODEA 025-2012: DESCUENTO AUTOM�TICO
*	@fecha 04 Ene 2013
*	@autor garellano
*/
 
Ext.onReady(function(){
      var ic_pyme = Ext.getDom(clavePyme).value;
				
		/*============= Creo un namespace de la pantalla =======================*/
      Ext.namespace("DSCTO.auto");                  
      
      /*========================================================================
                        OBJETO PARA OBTENER LAS EPOS 
      ========================================================================*/
      DSCTO.auto.ObtieneEpos = new ObtieneEpos();
      
      function ObtieneEpos(){
         this.urlRequest   =  '13dsctoaut02ext.data.jsp';
			//this.urlRequest   =  '../15cadenas/15mantenimiento/15parametrizacion/15pyme/15dsctoaut02ext.data.jsp';
         this.urlResponse  =  '13dsctoaut02ext.jsp';
         this.totalEposVenc=  0;             //Total de Epos Vencimiento
	 this.totalEposDistri=  0;             //Total de Epos Distribuido
         this.totalEposAutoDesc =0;
         this.totalEpos    =  0;             //Total de Epos
         this.arrayEpos    =  new Object();  //Array de Epos
         this.tipoFactoraje=  'N';
         this.estado       =  'INICIAR';
         this.operaDsctoAut=  '';
			 this.operaDsctoAutAnt=  '';
			this.loadTabFactorajeVencido = false;
			this.loadTabFactorajeDistribuido = false;
                        this.loadTabDescuentoAutoEPO= false;
			this.existeVacio	=	false;
                        this.pymeDescAutoEPO ='N'; //es para saber si la pyme opera descuento automatico EPO
                        this.existeVacioAE =false;                       
         
         /*================ ARRAY CON LOS MODIFICADOS ========================*/
         this.arrIcEpoModificado       = new Array();    //Array con las EPOs Modificadas
         this.arrIcIfModificado        = new Array();    //Array con las IFs Modificadas
         this.arrIcPymeModificado      = new Array();    //Array con las PYMEs Modificadas
         this.arrIcMonedaModificado    = new Array();    //Array con la Moneda Modificado asociado a la EPO
         this.arrOrdenModificado       = new Array();    //Array con el Orden Modificado Asociado a la EPO y Moneda
         this.arrModalidadModificado   = new Array();    //Array con la Modalidad Modificado Asociado a la EPO y Moneda
         this.arrModificadosID         = new Array();    //ID que identifica los modificados contenidos en Arrays
         
         /*================ ARRAY CON LOS MODIFICADOS ========================*/
         this.arrIcEpoModificadoV       = new Array();    //Array con las EPOs Modificadas
         this.arrIcIfModificadoV        = new Array();    //Array con las IFs Modificadas
         this.arrIcPymeModificadoV      = new Array();    //Array con las PYMEs Modificadas
         this.arrIcMonedaModificadoV    = new Array();    //Array con la Moneda Modificado asociado a la EPO
         this.arrOrdenModificadoV       = new Array();    //Array con el Orden Modificado Asociado a la EPO y Moneda
         this.arrModalidadModificadoV   = new Array();    //Array con la Modalidad Modificado Asociado a la EPO y Moneda
         this.arrModificadosIDV         = new Array();    //ID que identifica los modificados contenidos en Arrays
	
	 this.arrIcEpoModificadoD  =[];     //Array con las EPOs Modificadas
         this.arrIcIfModificadoD  =[];    //Array con las IFs Modificadas
         this.arrIcPymeModificadoD  =[];   //Array con las PYMEs Modificadas
         this.arrIcMonedaModificadoD   =[];    //Array con la Moneda Modificado asociado a la EPO
         this.arrOrdenModificadoD    =[];   //Array con el Orden Modificado Asociado a la EPO y Moneda
         this.arrModalidadModificadoD =[];    //Array con la Modalidad Modificado Asociado a la EPO y Moneda
         this.arrModificadosIDD    =[];    //ID que identifica los modificados contenidos en Arrays
	
         this.arrIcEpoModificadoAE  =[];     //Array con las EPOs Modificadas
         this.arrIcIfModificadoAE  =[];    //Array con las IFs Modificadas
         this.arrIcPymeModificadoAE  =[];   //Array con las PYMEs Modificadas
         this.arrIcMonedaModificadoAE   =[];    //Array con la Moneda Modificado asociado a la EPO
         this.arrOrdenModificadoAE    =[];   //Array con el Orden Modificado Asociado a la EPO y Moneda
         this.arrModalidadModificadoAE =[];    //Array con la Modalidad Modificado Asociado a la EPO y Moneda
         this.arrModificadosIDAE    =[];    //ID que identifica los modificados contenidos en Arrays
	
         this.arrTemmporalEPO = new Array();	
         this.arrTemmporalORD = new Array();
         
			/*============ Array compara numeros consecutivos (ORDEN)============*/
			this.arrNumerosConsecutivosFaltantes	=	false; //Almacena numeros consecutivos faltantes en el ORDEN
      }
      
      ObtieneEpos.prototype.init = function(){
         Ext.Ajax.request(
            {
               url: DSCTO.auto.ObtieneEpos.urlRequest,
               method: 'POST',
               params: { informacion: 'obtieneEpos',  cc_tipo_factoraje: DSCTO.auto.ObtieneEpos.tipoFactoraje, operacion: 'obtieneEposVencimiento', ic_pyme: ic_pyme },
               success: function(response, request){
                  var jsonData = Ext.util.JSON.decode(response.responseText);
                  
                  try{
                     DSCTO.auto.ObtieneEpos.operaDsctoAut = jsonData.cs_dscto_automatico=='S'?'SI':'NO'; 
							DSCTO.auto.ObtieneEpos.operaDsctoAutAnt = jsonData.cs_dscto_automatico=='S'?'SI':'NO'; 
                     DSCTO.auto.ObtieneEpos.setArrayEpos(jsonData.registros);
                     DSCTO.auto.ObtieneEpos.setTotalEpos(jsonData.totalEpos);
                     DSCTO.auto.ObtieneEpos.totalEposVenc = jsonData.totalEposVenc;
		     DSCTO.auto.ObtieneEpos.totalEposDistri = jsonData.totalEposDistri;
                     DSCTO.auto.ObtieneEpos.totalEposAutoDesc = jsonData.totalEposAutoDesc;  
                     DSCTO.auto.ObtieneEpos.pymeDescAutoEPO = jsonData.pymeDescAutoEPO;   
                                        
                     DescuentoAutomatico(DSCTO.auto.ObtieneEpos.estado);
                  }catch(e){
                     Ext.Msg.show({
                        title: 'Error en la inesperado',
                        msg: (e) + '<br><br>Click para continuar...',
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.ERROR
                     });
                  }
               }
            }
         );
      }
      
      ObtieneEpos.prototype.setTotalEpos = function(totalEpos){         
         this.totalEpos = totalEpos;
      }
      
      ObtieneEpos.prototype.getTotalEpos = function(){
         return this.totalEpos;
      }  
      
      ObtieneEpos.prototype.setArrayEpos = function(arrayEpos){
         this.arrayEpos = arrayEpos;
      }
      
      ObtieneEpos.prototype.getArrayEpos = function(){
         return this.arrayEpos;
      }
		                        
		function DescuentoAutomatico(estado){			
			if( estado === 'INICIAR' ){	         
                        
				var txtConformidad = [
					'Autorizo expresamente que todos los DOCUMENTOS',
					'que me sean publicados en la CADENA PRODUCTIVA,',
					'sean descontados autom�ticamente con el INTERMEDIARIO FINANCIERO',
					'y los recursos que se generen por dicho descuento',
					'me sean depositados en la cuenta que aparece a continuaci�n.'
				]
								
				/* Lleno el panelSuperior */            
				panelSuperior.add(
					{
						value: txtConformidad.join(" ") // Concatenar leyenda
					},
					{
						xtype:	'fieldset',
						style:	'margin: 10px 0px 0px 0px',
						title:	'Operar Descuento Autm�tico',
						items: [
							getRadioOperarDsctoAut	
						]
					}	
				);
            
            /*
            *  Si NO Opera Descuento Automatico
            *  se ocultan los panel's
            */
            if(DSCTO.auto.ObtieneEpos.operaDsctoAut=='SI'){
               getRadioOperarDsctoAut.items[0].checked = true;
               getRadioOperarDsctoAut.items[1].checked = false;
               panelTabs.show();
            }else{
               getRadioOperarDsctoAut.items[0].checked = false;
               getRadioOperarDsctoAut.items[1].checked = true;
               panelTabs.hide();
            }
            
             
            /* 
            *  Si NO existen EPOS con Factoraje Vencido
            *  entonces se deshabilita la pesta�a...
            */
            var tabFactorajeVencido = Ext.getCmp('tabFactorajeVencido');
            if(DSCTO.auto.ObtieneEpos.totalEposVenc<1){
               tabFactorajeVencido.setDisabled(true);  
            }else{
               tabFactorajeVencido.setDisabled(false);
            }
	    
	    //2017_003
	     var tabFactorajeDistribuido = Ext.getCmp('tabFactorajeDistribuido');
            if(DSCTO.auto.ObtieneEpos.totalEposDistri<1){
               tabFactorajeDistribuido.setDisabled(true);  
            }else{
               tabFactorajeDistribuido.setDisabled(false);
            }
            //2019 QC
             var tabDescuentoAutoEPO = Ext.getCmp('tabDescuentoAutoEPO');   
             
             
             if(DSCTO.auto.ObtieneEpos.pymeDescAutoEPO ==='S'){
                if(DSCTO.auto.ObtieneEpos.totalEposAutoDesc<1){
                   tabDescuentoAutoEPO.setDisabled(true);  
                }else{
                   tabDescuentoAutoEPO.setDisabled(false);               
                }
            }       
            
				/* Se agrega panelSuperior a la forma principal */								
				frmPrincipal.add(panelSuperior);
				frmPrincipal.setTitle('Descuento Autom�tico - Parametrizaci�n');
				
				panelTabs.add(tabsFactoraje);
				panelTabs.doLayout();
            
				pnl.add(frmPrincipal);
				pnl.doLayout();  
            
            
				frmPrincipal.add(panelTabs);
				frmPrincipal.doLayout();
                                
               var tabPanel = Ext.getCmp('tabsFactoraje');                 
              if(DSCTO.auto.ObtieneEpos.pymeDescAutoEPO ==='N'){  
                 tabPanel.hideTabStripItem(3);
              }
             
            DSCTO.auto.ObtieneEpos.estado = 'CARGA_FACTORAJE_NORMAL';
            DescuentoAutomatico(DSCTO.auto.ObtieneEpos.estado);
            
			} 
         
         else if( estado == 'CARGA_FACTORAJE_NORMAL' ){
            /* Llena el Tab Factoraje Normal de forma Dinamica */
            fnCargaFactorajeNormal();		
         }
         
         else if( estado == 'CARGA_FACTORAJE_VENCIDO' ){            
            /* Llena el Tab Factoraje Normal de forma Dinamica */
            fnCargaFactorajeVencido();	
	 } else if( estado === 'CARGA_FACTORAJE_DISTRIBUIDO' ){    //2017_003
	    fnCargaFactorajeDistribuido();
            
         } else if( estado === 'CARGA_DESCUENTO_AUTO_EPO' ){    //2019-01
            fnCargaDescuentoAutoEPO();
         
         }
	};
		
      var fnValidaciones = function(){
         Ext.Ajax.request({
            url: '/nafin/00utils/NEcesionDerechos.jsp',
            params: Ext.apply(objFormLogCesion.getForm().getValues(),{
               informacion: 	'validaClaveCesionDerec',
               cesionAltaCve: 'N',
               metodo: 			'utilerias', 
					ic_pyme: ic_pyme
            }),
            callback: obj.procesarSuccessNEclaveCesion
         });
      };
      
      
		/*==== Carga de forma din�mica el contenido del tab Factoraje Normal ====*/
		function fnCargaFactorajeNormal(){
			var tabFactorajeNormal = Ext.getCmp('tabFactorajeNormal');
			var totalEpos = DSCTO.auto.ObtieneEpos.getTotalEpos();
         var arrayEpos   = DSCTO.auto.ObtieneEpos.getArrayEpos();
         
			for(var i = 0; i<totalEpos; i++){
            //Le paso como parametro el id Dinamico que contendr�n los grids
            var idDinamico = 'normal_' + i;
				var $gridFactorajeNormal = gridFactorajeNormal(idDinamico);
            var ic_epo = arrayEpos[i].IC_EPO;
            var nombre_epo = arrayEpos[i].NOMBRE_EPO;

            /*======================== CARGA GRID ============================*/
            $gridFactorajeNormal.getStore().load(
               {
                  params: {
                     ic_epo : arrayEpos[i].IC_EPO,
                     nombre_epo: nombre_epo
                  }
               }
            );
            
				var collapsed = i===0?false:true;
			
				tabFactorajeNormal.add(
					new Ext.form.FieldSet({
                 id: 'fieldset_' + idDinamico,
					  collapsible: true,
                 hidden: true,
					  collapsed: collapsed,
					  title: arrayEpos[i].NOMBRE_EPO,
					  style: 'margin: 20px',
                 autoHeight:true,
                 defaults: { selectOnFocus: true},
					  items: [
						  $gridFactorajeNormal
					  ],
                 listeners: {
                     beforeexpand:function(p){
                     
                     }
                 }
				  })	
				);				
			} /* Fin For */
			
			tabFactorajeNormal.doLayout();
		}
      
      
		/*==== Carga de forma din�mica el contenido del tab Factoraje VENCIDO ====*/
		function fnCargaFactorajeVencido(){
			var tabFactorajeVencido = Ext.getCmp('tabFactorajeVencido');
			var totalEpos   = DSCTO.auto.ObtieneEpos.getTotalEpos();
         var arrayEpos   = DSCTO.auto.ObtieneEpos.getArrayEpos();
                  
			for(var i = 0; i<totalEpos; i++){
            //Le paso como parametro el id Dinamico que contendr�n los grids
            var idDinamico = 'vencido_' + i;
				var $gridFactorajeVencido = gridFactorajeNormal(idDinamico);
            var ic_epo = arrayEpos[i].IC_EPO;
            var nombre_epo = arrayEpos[i].NOMBRE_EPO;

            /*======================== CARGA GRID ============================*/
            $gridFactorajeVencido.getStore().load(
               {
                  params: {
                     ic_epo : arrayEpos[i].IC_EPO,
                     nombre_epo: nombre_epo
                  }
               }
            );
            
            $gridFactorajeVencido.getColumnModel().setHidden(3, true);
            
				var collapsed = i===0?false:true;
			
				tabFactorajeVencido.add(
					new Ext.form.FieldSet({
                 id: 'fieldset_' + idDinamico,
                 hidden: true,
					  collapsible: true,
					  collapsed: collapsed,
					  title: arrayEpos[i].NOMBRE_EPO,
					  style: 'margin: 20px',
                 autoHeight:true,
                 defaults: { selectOnFocus: true},
					  items: [
						  $gridFactorajeVencido
					  ],
                 listeners: {
                     beforeexpand:function(p){
                     
                     }
                 }
				  })	
				);				
			} /* Fin For */
			
			tabFactorajeVencido.doLayout();
		}
		
		
		
		//-2017_003
    function fnCargaFactorajeDistribuido(){
	var tabFactorajeDistribuido = Ext.getCmp('tabFactorajeDistribuido');
	var totalEpos   = DSCTO.auto.ObtieneEpos.getTotalEpos();
	var arrayEpos   = DSCTO.auto.ObtieneEpos.getArrayEpos();
                  
	for(var i = 0; i<totalEpos; i++){
	    //Le paso como parametro el id Dinamico que contendr�n los grids
	    var idDinamico = 'distribuido_' + i;
	    var $gridFactorajeDistribuido = gridFactorajeNormal(idDinamico);	    
	    var nombre_epo = arrayEpos[i].NOMBRE_EPO;

	    /*======================== CARGA GRID ============================*/
	    $gridFactorajeDistribuido.getStore().load(   {
		params: {
		    ic_epo : arrayEpos[i].IC_EPO,
		    nombre_epo: nombre_epo
		}
	    });
                       
            var collapsed = i===0?false:true;
	    tabFactorajeDistribuido.add(
		new Ext.form.FieldSet({
		    id: 'fieldset_' + idDinamico,
		    hidden: true,
		    collapsible: true,
		    collapsed: collapsed,
		    title: arrayEpos[i].NOMBRE_EPO,
		    style: 'margin: 20px',
		    autoHeight:true,
		    defaults: { 
			selectOnFocus: true
		    },
		    items: [
			$gridFactorajeDistribuido
		    ],
		    listeners: {
			beforeexpand:function(p){
                     
			}
		    }
		})	
	    );				
	} /* Fin For */
	tabFactorajeDistribuido.doLayout();
    };
		
		
    function fnCargaDescuentoAutoEPO(){
	var tabDescuentoAutoEPO = Ext.getCmp('tabDescuentoAutoEPO');
	var totalEpos   = DSCTO.auto.ObtieneEpos.getTotalEpos();
	var arrayEpos   = DSCTO.auto.ObtieneEpos.getArrayEpos();
                  
	for(var i = 0; i<totalEpos; i++){
	    //Le paso como parametro el id Dinamico que contendr�n los grids
	    var idDinamico = 'descuentoAutoEPO_' + i;
	    var $gridDescuentoAutoEPO = gridFactorajeNormal(idDinamico);	    
	    var nombre_epo = arrayEpos[i].NOMBRE_EPO;

	    /*======================== CARGA GRID ============================*/
	    $gridDescuentoAutoEPO.getStore().load(   {
		params: {
		    ic_epo : arrayEpos[i].IC_EPO,
		    nombre_epo: nombre_epo
		}
	    });
                       
            var collapsed = i===0?false:true;
	    tabDescuentoAutoEPO.add(
		new Ext.form.FieldSet({
		    id: 'fieldset_' + idDinamico,
		    hidden: true,
		    collapsible: true,
		    collapsed: collapsed,
		    title: arrayEpos[i].NOMBRE_EPO,
		    style: 'margin: 20px',
		    autoHeight:true,
		    defaults: { 
			selectOnFocus: true
		    },
		    items: [
			$gridDescuentoAutoEPO
		    ],
		    listeners: {
			beforeexpand:function(p){
                     
			}
		    }
		})	
	    );				
	} /* Fin For */
        
	tabDescuentoAutoEPO.doLayout();
    };
    
		
		
		
		
		/*========= Permite esconder el contenedor de EPOS o mostrarlo =========*/
		function fnChkAutorizar(field, newValue, oldValue){
			var valueInput = newValue.inputValue;
         			
			if(valueInput==='SI'){
				panelTabs.show();
			}else{
				panelTabs.hide();
			}
		}
	
	  /*
	   * Funcion callback despues de obtener certificado y llave privada
	   */
	  var fnCertificadoCallback = function(vpkcs7, vtextoFirmado){
		  if (Ext.isEmpty(vpkcs7) || vpkcs7 == 'error:noMatchingCert') {
				Ext.Msg.show({
					title:'Firmar',
					msg:'Error en el proceso de Firmado. \nNo se puede continuar',
					buttons: Ext.Msg.OK,
					icon: Ext.Msg.ERROR
				});
				return false;	//Error en la firma. Termina...
			}
			
			frmPrincipal.setVisible(false);
			pnl.add(acusePanel);
			pnl.doLayout();		
			
			DSCTO.auto.ObtieneEpos.operaDsctoAut = getRadioOperarDsctoAut.getValue().inputValue;
						
			if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='N' ||   DSCTO.auto.ObtieneEpos.tipoFactoraje==='V'  ||   DSCTO.auto.ObtieneEpos.tipoFactoraje==='D' || DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){
				if( ( DSCTO.auto.ObtieneEpos.operaDsctoAut=='NO' ||  DSCTO.auto.ObtieneEpos.operaDsctoAut=='SI')  &&  ( DSCTO.auto.ObtieneEpos.arrModificadosID.length<1 &&  DSCTO.auto.ObtieneEpos.arrModificadosIDV.length<1   && DSCTO.auto.ObtieneEpos.arrModificadosIDD.length<1  && DSCTO.auto.ObtieneEpos.arrModificadosIDAE .length<1 )){
					Ext.getCmp('gridAcuse').getStore().load({
						params:{
							informacion:				'ConfirmarCertificado', 
							operacion: 					'actualizaOperarDescuentoAutomaticoAcuse',
							operarDsctoAut: DSCTO.auto.ObtieneEpos.operaDsctoAut, 
							ic_pyme: ic_pyme,
							Pkcs7:						vpkcs7,
							TextoFirmado: 				vtextoFirmado
						}
					});
					return;
				}else {
				
					var  num_Normal = DSCTO.auto.ObtieneEpos.arrModificadosID.length;
					var  num_Vencido = DSCTO.auto.ObtieneEpos.arrModificadosIDV.length;
					var  numDistribuido = DSCTO.auto.ObtieneEpos.arrModificadosIDD.length;
                                        var  numDescuentoAutoEPO  = DSCTO.auto.ObtieneEpos.arrModificadosIDAE.length;
					Ext.getCmp('gridAcuse').getStore().load({
						params:{
							informacion:				'ConfirmarCertificado', 
							operacion: 					'actualizaOperarDescuentoAutomatico',
							operarDsctoAut: 			DSCTO.auto.ObtieneEpos.operaDsctoAut,
							Pkcs7:						vpkcs7,
							TextoFirmado: 				vtextoFirmado, 
							ic_pyme: 					ic_pyme,						
							arrIcEpoModificado:     DSCTO.auto.ObtieneEpos.arrIcEpoModificado,
							arrOrdenModificado:     DSCTO.auto.ObtieneEpos.arrOrdenModificado,
							arrModalidadModificado: DSCTO.auto.ObtieneEpos.arrModalidadModificado,
							arrIcMonedaModificado:  DSCTO.auto.ObtieneEpos.arrIcMonedaModificado,
							arrModificadosID:       DSCTO.auto.ObtieneEpos.arrModificadosID,               
							arrIcIfModificado:      DSCTO.auto.ObtieneEpos.arrIcIfModificado,
							arrIcPymeModificado:    DSCTO.auto.ObtieneEpos.arrIcPymeModificado,
							num_Normal:num_Normal,
							num_Vencido:num_Vencido,
							numDistribuido:numDistribuido,
                                                        numDescuentoAutoEPO:numDescuentoAutoEPO,
							arrIcEpoModificadoV:     DSCTO.auto.ObtieneEpos.arrIcEpoModificadoV,
							arrOrdenModificadoV:     DSCTO.auto.ObtieneEpos.arrOrdenModificadoV,
							arrModalidadModificadoV: DSCTO.auto.ObtieneEpos.arrModalidadModificadoV,
							arrIcMonedaModificadoV:  DSCTO.auto.ObtieneEpos.arrIcMonedaModificadoV,
							arrModificadosIDV:       DSCTO.auto.ObtieneEpos.arrModificadosIDV,               
							arrIcIfModificadoV:      DSCTO.auto.ObtieneEpos.arrIcIfModificadoV,
							arrIcPymeModificadoV:    DSCTO.auto.ObtieneEpos.arrIcPymeModificadoV,
							
							arrIcEpoModificadoD :  DSCTO.auto.ObtieneEpos.arrIcEpoModificadoD, //2017_003
							arrIcIfModificadoD :  DSCTO.auto.ObtieneEpos.arrIcIfModificadoD,
							arrIcPymeModificadoD :  DSCTO.auto.ObtieneEpos.arrIcPymeModificadoD,
							arrIcMonedaModificadoD :  DSCTO.auto.ObtieneEpos.arrIcMonedaModificadoD,
							arrOrdenModificadoD :  DSCTO.auto.ObtieneEpos.arrOrdenModificadoD,
							arrModalidadModificadoD :  DSCTO.auto.ObtieneEpos.arrModalidadModificadoD,
							arrModificadosIDD :  DSCTO.auto.ObtieneEpos.arrModificadosIDD,
                                                        
							arrIcEpoModificadoAE:DSCTO.auto.ObtieneEpos.arrIcEpoModificadoAE,
							arrIcIfModificadoAE:DSCTO.auto.ObtieneEpos.arrIcIfModificadoAE,
							arrIcPymeModificadoAE:DSCTO.auto.ObtieneEpos.arrIcPymeModificadoAE,
							arrIcMonedaModificadoAE:DSCTO.auto.ObtieneEpos.arrIcMonedaModificadoAE,
							arrOrdenModificadoAE:DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE,
							arrModalidadModificadoAE:DSCTO.auto.ObtieneEpos.arrModalidadModificadoAE,
							arrModificadosIDAE:DSCTO.auto.ObtieneEpos.arrModificadosIDAE
                                                         

						}
					});			
				}
			}
			
			var boton = Ext.getCmp('btnGuardar');	
			boton.setDisabled(false);
			boton.setIconClass('icoGuardar');
	  }
	
	  /*=============== Funcion para guardar la parametrizacion ==============*/
      var fnCertificado = function(){
			var textoFirmado = "Autorizo expresamente que todos los DOCUMENTOS que me sean publicados en la CADENA PRODUCTIVA, sean descontados autom�ticamente con el INTERMEDIARIO FINANCIERO y los recursos que se generen por dicho descuento me sean depositados en la cuenta especificada";
			//var pkcs7 = NE.util.firmar(textoFirmado);
			NE.util.obtenerPKCS7(fnCertificadoCallback, textoFirmado);
			
		}
		
      
		//GUARDA OPERAR DESCUENTO AUTOMATICO
		function fnGuardarOperarDsctoAut(operarDsctoAut, pkcs7, textoFirmado){			
			Ext.Ajax.request({
				url: DSCTO.auto.ObtieneEpos.urlRequest,
				method: 'POST',
				params: {
					informacion: 'actualizaOperarDescuentoAutomaticoConfirmaCertificado',
					operarDsctoAut: DSCTO.auto.ObtieneEpos.operaDsctoAut
					, ic_pyme: ic_pyme,
						Pkcs7:						pkcs7,
						TextoFirmado: 				textoFirmado
				}
			});
		}
		
      // Guardar la parametrizacion
      function fnGuardarModificados(){
			//Setear variable con el valor del radio OPERAR DESCUENTO AUTOMATICO
			DSCTO.auto.ObtieneEpos.operaDsctoAut = getRadioOperarDsctoAut.getValue().inputValue;
			
			if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='N' || DSCTO.auto.ObtieneEpos.tipoFactoraje==='V' || DSCTO.auto.ObtieneEpos.tipoFactoraje==='D' || DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){
				fnCertificado();
				return;
			}
		}
    
	 
	  function validaCamposVaciosG(){
				var tempExisteVacio = true;
					
				if(DSCTO.auto.ObtieneEpos.arrIcEpoModificado.length !=0   || DSCTO.auto.ObtieneEpos.arrIcEpoModificadoV.length !=0 || DSCTO.auto.ObtieneEpos.arrIcEpoModificadoD.length !==0  || DSCTO.auto.ObtieneEpos.arrIcEpoModificadoAE.length !==0) {
				
					if(DSCTO.auto.ObtieneEpos.tipoFactoraje=='N' && DSCTO.auto.ObtieneEpos.operaDsctoAut=='SI'){
					
						DSCTO.auto.ObtieneEpos.existeVacio = true;
						
						for(var i=0; i<DSCTO.auto.ObtieneEpos.arrIcEpoModificado.length; i++){
							var id 			= 	DSCTO.auto.ObtieneEpos.arrModificadosID[i];						
							var modalidad 	= 	DSCTO.auto.ObtieneEpos.arrModalidadModificado[i];
							var orden 		= 	DSCTO.auto.ObtieneEpos.arrOrdenModificado[i];
							
							modalidad 	= 	modalidad==null?"":modalidad;
							orden			=	orden==null?"":orden;						
							if(!Ext.isEmpty(modalidad)){
								tempExisteVacio = false;
							}
													
							if(!Ext.isEmpty(orden)){
								tempExisteVacio = false;
							}
							
							if(modalidad!="N"  ){
								if(orden=='0' || orden==0 ){								
									tempExisteVacio = true;
								}	
							}
							
							if( modalidad!="U"  &&   modalidad!="P" && modalidad!="N"  || ( (modalidad!="N" && orden=="") || (modalidad=="" && orden!="") ) || tempExisteVacio){								
								DSCTO.auto.ObtieneEpos.existeVacio = true;
							}else{
								DSCTO.auto.ObtieneEpos.existeVacio = false;
							}
							
						}
					} else  if(DSCTO.auto.ObtieneEpos.tipoFactoraje=='V'  && DSCTO.auto.ObtieneEpos.operaDsctoAut=='SI' ){ 
						
						DSCTO.auto.ObtieneEpos.existeVacio = true;
						
						for(var i=0; i<DSCTO.auto.ObtieneEpos.arrIcEpoModificadoV.length; i++){
							var id 			= 	DSCTO.auto.ObtieneEpos.arrModificadosIDV[i];						
							var modalidad 	= 	DSCTO.auto.ObtieneEpos.arrModalidadModificadoV[i];
							modalidad 	= 	modalidad==null?"":modalidad;
							
						
							if(!Ext.isEmpty(modalidad)){
								tempExisteVacio = false;
							}
							
							if( modalidad!=="U"  &&  modalidad!=="P" && modalidad!=="N" ){							
								DSCTO.auto.ObtieneEpos.existeVacio = true;											
							}else {
								DSCTO.auto.ObtieneEpos.existeVacio = false;
							}						
						}
						
					} else  if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='D'  && DSCTO.auto.ObtieneEpos.operaDsctoAut==='SI' ){  //2017_003
						
						DSCTO.auto.ObtieneEpos.existeVacio = true;
						
						    for(var x=0; x<DSCTO.auto.ObtieneEpos.arrIcEpoModificadoD.length; x++){
							var modalidadx 	= 	DSCTO.auto.ObtieneEpos.arrModalidadModificadoD[x];
							var ordenx 		= 	DSCTO.auto.ObtieneEpos.arrOrdenModificadoD[x];
							
							modalidadx 	= 	modalidadx==null?"":modalidadx;
							ordenx			=	ordenx==null?"":ordenx;						
							if(!Ext.isEmpty(modalidadx)){
								tempExisteVacio = false;
							}
													
							if(!Ext.isEmpty(ordenx)){
								tempExisteVacio = false;
							}
							
							if(modalidadx!=="N"  && (ordenx==='0' || ordenx===0 ) ){								
							    tempExisteVacio = true;							    	
							}
							
							if( modalidadx!=="U"  &&   modalidadx!=="P" && modalidadx!=="N"  || ( (modalidadx!=="N" && ordenx==="") || (modalidadx==="" && ordenx!=="") ) || tempExisteVacio){								
							    DSCTO.auto.ObtieneEpos.existeVacio = true;					
							}else{
								DSCTO.auto.ObtieneEpos.existeVacio = false;
							}
							
						}
					
                                        } else  if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'  && DSCTO.auto.ObtieneEpos.operaDsctoAut==='SI' ){  //2019_003
						
						DSCTO.auto.ObtieneEpos.existeVacio = true;
                                                
                                                 
						    for(var a=0; a<DSCTO.auto.ObtieneEpos.arrIcEpoModificadoAE.length; a++){
							var modalidadxa 	= 	DSCTO.auto.ObtieneEpos.arrModalidadModificadoAE[a];
							var ordenxa 		= 	DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE[a];
							
							modalidadxa 	= 	modalidadxa==null?"":modalidadxa;
							ordenxa			=	ordenxa==null?"":ordenxa;	
                                                                                                                 
							if(!Ext.isEmpty(modalidadxa)){
								tempExisteVacio = false;
							}
													
							if(!Ext.isEmpty(ordenxa)){
								tempExisteVacio = false;
							}							  
                                                        
							if(modalidadxa!=="N"  && (ordenxa==='0' || ordenxa===0 ) ){								
							    tempExisteVacio = true;							    	
							}
							
							if( modalidadxa!=="U"  &&   modalidadxa!=="P" && modalidadxa!=="N" && modalidadxa!=="A" || ( (modalidadxa!=="N" && ordenxa==="") || (modalidadxa==="" && ordenxa!=="") ) || tempExisteVacio){								
							    DSCTO.auto.ObtieneEpos.existeVacio = true;					
							}else{
								DSCTO.auto.ObtieneEpos.existeVacio = false;
							}  
						}				
                                        		
					}else{
						DSCTO.auto.ObtieneEpos.existeVacio = false;
					}
				}
			 }
			 

      // Dispara el m�todo para guardar la parametrizacion
		function fnGuardar(boton, evt){
       
		   //valida si alguna EPO  con parametrizaci�n incompleta
			validaCamposVaciosG();
		                        
			boton.setDisabled(true);
                      boton.setIconClass('realizandoActividad');
		                    
                       
                       
                       //********ESTO ES PARA VALIDAR QUE NINGUN BLOQUE DEL DESCUENTO AUTOMATICO EPO QUEDE VACIA  (INICIA ) ********         
                    
                if( DSCTO.auto.ObtieneEpos.pymeDescAutoEPO=='S' && DSCTO.auto.ObtieneEpos.loadTabDescuentoAutoEPO ==true ){                  
                    var totalEpos   = DSCTO.auto.ObtieneEpos.getTotalEpos();  
                    var faltaRegParam=0;   
                    for(var i = 0; i<totalEpos; i++){  
                        
                        var totalRegD =0;
                        var conOrdenD =0;      
                        var totalRegN =0;
                        var conOrdenN =0;  
                        var icEPO =0;
                        var moneda =0;                         
                        var  gridEditable = Ext.getCmp('gridFactorajeNormal_descuentoAutoEPO_'+i);                                                        
                        var store = gridEditable.getStore();
                                                    
                        store.each(function(record){ 
                            
                            icEPO =  record.get('IC_EPO');
                            moneda = record.get('IC_MONEDA');                                
                                 
                            if( moneda ==  54 ){ 
                                totalRegD++;                                    
                            }else  if( moneda ==  1 ){ 
                                totalRegN++;   
                            }
                                   
                            if( moneda == 54 && record.get('CS_DSCTO_AUTOMATICO_DIA') == 'A' && record.get('CS_ORDEN')!==''  ) {
                                conOrdenD++;      
                            }else  if( moneda == 1 && record.get('CS_DSCTO_AUTOMATICO_DIA') == 'A' && record.get('CS_ORDEN')!==''  ) {                     
                                conOrdenN++;                                                             
                            }
                        });   //store.each(function(record)                       
                         
                        if( (  conOrdenD !=0 && totalRegD !==conOrdenD )  || (  conOrdenN !=0 && totalRegN !==conOrdenN ) ) {
                            faltaRegParam++;
                        } 
                    }                    
                    
                    DSCTO.auto.ObtieneEpos.existeVacioAE =false; 
                    if(faltaRegParam!=0){
                     DSCTO.auto.ObtieneEpos.existeVacioAE =true;   
                    } 
                    
                }
                    //**********************DESCUENTO AUTOMATICO EPO QUEDE VACIA (TERMINA) ***********
                
                
                    // Si existe alg�n valor vacio
                            
			if(DSCTO.auto.ObtieneEpos.existeVacio || DSCTO.auto.ObtieneEpos.existeVacioAE  ){
				Ext.Msg.show({
					title: 'Validacion ...',
					msg: 'Existe alguna Epo con parametrizaci�n incompleta',
					icon: Ext.Msg.INFO,
					buttons: Ext.Msg.OK,
					fn: function(){						
						boton.setDisabled(false);
						boton.setIconClass('icoGuardar'); 
					}
				});
				return;
			}
			
			DSCTO.auto.ObtieneEpos.operaDsctoAut = getRadioOperarDsctoAut.getValue().inputValue;	
			if(DSCTO.auto.ObtieneEpos.operaDsctoAut=='SI' && DSCTO.auto.ObtieneEpos.operaDsctoAutAnt == 'SI' && ( DSCTO.auto.ObtieneEpos.arrModificadosIDV.length<1 || DSCTO.auto.ObtieneEpos.arrModificadosID.length<1   || DSCTO.auto.ObtieneEpos.arrModificadosIDD.length<1 || DSCTO.auto.ObtieneEpos.arrModificadosIDAE.length<1 ) ) {
		
				if(DSCTO.auto.ObtieneEpos.arrNumerosConsecutivosFaltantes){
					Ext.Msg.alert('Mensaje:', 'Recuerde que debe parametrizar el Orden con n�meros consecutivos comenzando con el 1', function(){Ext.getCmp('btnGuardar').setDisabled(false); Ext.getCmp('btnGuardar').setIconClass('icoGuardar');});
					return;
				}
								 
				
				if(DSCTO.auto.ObtieneEpos.arrModificadosIDV.length<1 && DSCTO.auto.ObtieneEpos.arrModificadosID.length<1 && DSCTO.auto.ObtieneEpos.arrModificadosIDD.length<1  && DSCTO.auto.ObtieneEpos.arrModificadosIDAE.length<1 ){
					Ext.Msg.alert('Mensaje :','No hay cambios en la parametrizaci�n con Factoraje Normal , Factoraje Vencimiento, Factoraje Distribuido Y Descuento Autom�tico EPO.', function(){Ext.getCmp('btnGuardar').setDisabled(false); Ext.getCmp('btnGuardar').setIconClass('icoGuardar');});
					return;
				}
				
				if(DSCTO.auto.ObtieneEpos.tipoFactoraje == 'N' && DSCTO.auto.ObtieneEpos.arrModificadosID.length<1){
					Ext.Msg.alert('Mensaje :','No hay cambios en la parametrizaci�n con Factoraje Normal ...', function(){Ext.getCmp('btnGuardar').setDisabled(false); Ext.getCmp('btnGuardar').setIconClass('icoGuardar');});
					return;
				}
				
				if(DSCTO.auto.ObtieneEpos.tipoFactoraje == 'V' && DSCTO.auto.ObtieneEpos.arrModificadosIDV.length<1){
					Ext.Msg.alert('Mensaje :','No hay cambios en la parametrizaci�n con Factoraje Vencimiento ...', function(){Ext.getCmp('btnGuardar').setDisabled(false); Ext.getCmp('btnGuardar').setIconClass('icoGuardar');});
					return;
				}
				if(DSCTO.auto.ObtieneEpos.tipoFactoraje === 'D' && DSCTO.auto.ObtieneEpos.arrModificadosIDD.length<1){
				    Ext.Msg.alert('Mensaje :','No hay cambios en la parametrizaci�n con Factoraje Distribuido ...', function(){ 
					Ext.getCmp('btnGuardar').setDisabled(false); 
				        Ext.getCmp('btnGuardar').setIconClass('icoGuardar');
				    });
				    return;
				}
                                if(DSCTO.auto.ObtieneEpos.tipoFactoraje === 'A' && DSCTO.auto.ObtieneEpos.arrModificadosIDAE.length<1){
				    Ext.Msg.alert('Mensaje :','No hay cambios en la parametrizaci�n con Descuento Autom�tico EPO ...', function(){ 
					Ext.getCmp('btnGuardar').setDisabled(false); 
				        Ext.getCmp('btnGuardar').setIconClass('icoGuardar');
				    });
				    return;
				}
                                
			}
			fnGuardarModificados(DSCTO.auto.ObtieneEpos.operaDsctoAut);
			
         boton.setDisabled(false);
         boton.setIconClass('icoGuardar');         
		}
       
        	
      /*
      *  FACTORAJE NORMAL
      *  Funcion que permite guardar los valores de los campos modificados
      *  correspondientes al grid.
      */
      var fnGuardarValoresModificadosFactNormal = function(e) {         
          /*
            ID generado con el ic_epo + el numero de fila + Tipo Factoraje
            para identificar el valor del campo modificado en los arreglos...
          */
          var idValorModificado = e.record.get('IC_EPO') + e.row + DSCTO.auto.ObtieneEpos.tipoFactoraje;
          var idValorModificadoV = e.record.get('IC_EPO') + e.row +  DSCTO.auto.ObtieneEpos.tipoFactoraje;
	  var idValorModificadoD = e.record.get('IC_EPO') + e.row +  DSCTO.auto.ObtieneEpos.tipoFactoraje;
           var idValorModificadoDAE = e.record.get('IC_EPO') + e.row +  DSCTO.auto.ObtieneEpos.tipoFactoraje;
                   
          /* 
          * TRUE - Ya esxiste el campo modificado en el arreglo
          * FALSE - NO esxiste el campo modificado en el arreglo
          */
          var existeValorModificado = false;
          var posicionValorModificado = 0;
          
          var existeValorModificadoV = false;
          var posicionValorModificadoV = 0;
	  
	  var existeValorModificadoD= false; //2017_003
          var posicionValorModificadoD = 0; //2017_003
	  
          var existeValorModificadoDAE= false; //2017_003
          var posicionValorModificadoDAE = 0; //2017_003
          
          /*
          * Iterar para identificar si el campo modificado ya se
          * ha editado y se ha registrado en el arreglo,
          * si es el caso, asigna a la variable existeValorModificado TRUE
          * y le da la posicion del puntero a posicionValorModificado
          */                      
         if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='N'){ 
            for( var i=0; i<DSCTO.auto.ObtieneEpos.arrModificadosID.length; i++ ){                  
               if( DSCTO.auto.ObtieneEpos.arrModificadosID[i]==idValorModificado ){
                  existeValorModificado = true;
                  posicionValorModificado = i;
               }
            }
	 } else  if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='V'){ 
	    for( var i=0; i<DSCTO.auto.ObtieneEpos.arrModificadosIDV.length; i++ ){                  
               if( DSCTO.auto.ObtieneEpos.arrModificadosIDV[i]==idValorModificadoV ){
                  existeValorModificadoV = true;
                  posicionValorModificadoV = i;
               }
            }
	 
	 
         } else  if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='D'){  //2017_003
	 
            for( var x=0; x<DSCTO.auto.ObtieneEpos.arrModificadosIDD.length; x++ ){                  
               if( DSCTO.auto.ObtieneEpos.arrModificadosIDD[x]===idValorModificadoD ){
                  existeValorModificadoD = true;
                  posicionValorModificadoD = x;
               }
            }
            
         } else  if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){  //2017_003
	 
            for( var a=0; a<DSCTO.auto.ObtieneEpos.arrModificadosIDAE.length; a++ ){                  
               if( DSCTO.auto.ObtieneEpos.arrModificadosIDAE[a]===idValorModificadoDAE ){
                  existeValorModificadoDAE = true;
                  posicionValorModificadoDAE = a;
               }
            }
         }
          
          
          /*
          * Si se modifica el campo ORDEN entonces se agrega el 
          * valor y posicion a los arreglos correspondientes....
          */
          if(  e.field  == 'CS_ORDEN'  ){
            
               /*
               * Si cambio el valor que tiene seleccionado, entonces
               * agrego valores a los arreglos correspondientes...
               */
               if(   e.originalValue != e.value    ){ 
                  
                  /*
                     Si ya existe un registro con el mismo ID
                     generado con el ic_epo + el numero de fila + Tipo Factoraje,
                     s�lo se modificar� en el array el registro
                     seleccionado...
                  */
                  if( existeValorModificado || existeValorModificadoV || existeValorModificadoD || existeValorModificadoDAE ){
                        
                     /*
                     *  Modifico unicamente el registro de ORDEN
                     *  del campo editado...
                     */       
                      
                     if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='N'){
                        DSCTO.auto.ObtieneEpos.arrOrdenModificado[posicionValorModificado] = e.value;  
			
                     }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='V'){ 		     
                        DSCTO.auto.ObtieneEpos.arrOrdenModificadoV[posicionValorModificadoV] = e.value; 
			
		     }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='D'){  //2017_003
                        DSCTO.auto.ObtieneEpos.arrOrdenModificadoD[posicionValorModificadoD] = e.value;  
                        
                     }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){  //2019_001
                        DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE[posicionValorModificadoDAE] = e.value;  
                     }
                     
                  }
                  
                  /*
                     Si NO existe un registro con el mismo ID
                     generado con el ic_epo + el numero de fila + numero de columna,
                     llenamos los arreglos correspondientes...
                  */
                  else{
                     if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='N'){
                     
                         DSCTO.auto.ObtieneEpos.arrIcEpoModificado.push(e.record.get('IC_EPO'));
                         DSCTO.auto.ObtieneEpos.arrIcIfModificado.push(e.record.get('IC_IF'));
                         DSCTO.auto.ObtieneEpos.arrIcPymeModificado.push(e.record.get('IC_PYME'));
                         DSCTO.auto.ObtieneEpos.arrIcMonedaModificado.push(e.record.get('IC_MONEDA'));
                         DSCTO.auto.ObtieneEpos.arrOrdenModificado.push(e.value);
                         DSCTO.auto.ObtieneEpos.arrModalidadModificado.push(e.record.get('CS_DSCTO_AUTOMATICO_DIA'));
                         DSCTO.auto.ObtieneEpos.arrModificadosID.push(idValorModificado);
                         
		     } else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='V'){
                         DSCTO.auto.ObtieneEpos.arrIcEpoModificadoV.push(e.record.get('IC_EPO'));
                         DSCTO.auto.ObtieneEpos.arrIcIfModificadoV.push(e.record.get('IC_IF'));
                         DSCTO.auto.ObtieneEpos.arrIcPymeModificadoV.push(e.record.get('IC_PYME'));
                         DSCTO.auto.ObtieneEpos.arrIcMonedaModificadoV.push(e.record.get('IC_MONEDA'));
                         DSCTO.auto.ObtieneEpos.arrOrdenModificadoV.push(e.value);
                         DSCTO.auto.ObtieneEpos.arrModalidadModificadoV.push(e.record.get('CS_DSCTO_AUTOMATICO_DIA'));
                         DSCTO.auto.ObtieneEpos.arrModificadosIDV.push(idValorModificadoV);
			 
		     } else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='D'){   //2017_003
			
			DSCTO.auto.ObtieneEpos.arrIcEpoModificadoD.push(e.record.get('IC_EPO'));
                        DSCTO.auto.ObtieneEpos.arrIcIfModificadoD.push(e.record.get('IC_IF'));
                        DSCTO.auto.ObtieneEpos.arrIcPymeModificadoD.push(e.record.get('IC_PYME'));
                        DSCTO.auto.ObtieneEpos.arrIcMonedaModificadoD.push(e.record.get('IC_MONEDA'));
                        DSCTO.auto.ObtieneEpos.arrOrdenModificadoD.push(e.value);
                        DSCTO.auto.ObtieneEpos.arrModalidadModificadoD.push(e.record.get('CS_DSCTO_AUTOMATICO_DIA'));
                        DSCTO.auto.ObtieneEpos.arrModificadosIDD.push(idValorModificadoD);	
                        
                     }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){
                     
                         DSCTO.auto.ObtieneEpos.arrIcEpoModificadoAE.push(e.record.get('IC_EPO'));
                         DSCTO.auto.ObtieneEpos.arrIcIfModificadoAE.push(e.record.get('IC_IF'));
                         DSCTO.auto.ObtieneEpos.arrIcPymeModificadoAE.push(e.record.get('IC_PYME'));
                         DSCTO.auto.ObtieneEpos.arrIcMonedaModificadoAE.push(e.record.get('IC_MONEDA'));
                         DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE.push(e.value);
                         DSCTO.auto.ObtieneEpos.arrModalidadModificadoAE.push(e.record.get('CS_DSCTO_AUTOMATICO_DIA'));
                         DSCTO.auto.ObtieneEpos.arrModificadosIDAE.push(idValorModificadoDAE);
                     }
                   }
               }
          } // Termina Bloque ORDEN
                 
          /*
          * Si se modifica el campo MODALIDAD entonces se agrega el 
          * valor y posicion a los arreglos correspondientes....
          */
          else if(   e.field  == 'CS_DSCTO_AUTOMATICO_DIA'  ){ //MODALIDAD   
			 
					if(e.value==='N' || e.value==='D' ) {					
						var gridEditor = e.grid;
						var store = gridEditor.getStore();
						var registro = store.getAt(e.row);
						store.each(function(record){		
						  	if(registro.data['CS_ORDEN']!==''){						
								registro.set('CS_ORDEN','');	
							}
						});
					}
					
               /*
               * Si cambio el valor que tiene seleccionado, entonces
               * agrego valores a los arreglos correspondientes...
               */
               if(   e.originalValue != e.value    ){ 
                  
                  /*
                     Si ya existe un registro con el mismo ID
                     generado con el ic_epo + el numero de fila,
                     s�lo se modificar� en el array el registro
                     seleccionado...
                  */
                  if(   existeValorModificado || existeValorModificadoV  || existeValorModificadoD || existeValorModificadoDAE ){
                     
                     /*
                     *  Modific unicamente el registro de MODALIDAD
                     *  del campo editado...
                     */ 
                     if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='N'){
                        DSCTO.auto.ObtieneEpos.arrModalidadModificado[posicionValorModificado] = e.value;  
                     }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='V'){ 
                        DSCTO.auto.ObtieneEpos.arrModalidadModificadoV[posicionValorModificadoV] = e.value;  
                     }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='D'){ //2017_003
                        DSCTO.auto.ObtieneEpos.arrModalidadModificadoD[posicionValorModificadoD] = e.value; 
                        
                     }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){ //2017_003
                        DSCTO.auto.ObtieneEpos.arrModalidadModificadoAE[posicionValorModificadoDAE] = e.value;  
                     }
                     
                  }
                  
                  /*
                     Si NO existe un registro con el mismo ID
                     generado con el ic_epo + el numero de fila,
                     llenamos los arreglos correspondientes...
                  */
                  else{
                     if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='N'){
                         DSCTO.auto.ObtieneEpos.arrIcEpoModificado.push(e.record.get('IC_EPO'));
                         DSCTO.auto.ObtieneEpos.arrIcIfModificado.push(e.record.get('IC_IF'));
                         DSCTO.auto.ObtieneEpos.arrIcPymeModificado.push(e.record.get('IC_PYME'));
                         DSCTO.auto.ObtieneEpos.arrIcMonedaModificado.push(e.record.get('IC_MONEDA'));
                         DSCTO.auto.ObtieneEpos.arrOrdenModificado.push(e.record.get('CS_ORDEN'));
                         DSCTO.auto.ObtieneEpos.arrModalidadModificado.push(e.value); //MODALIDAD
                         DSCTO.auto.ObtieneEpos.arrModificadosID.push(idValorModificado);
			 
		     }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='V'){
		     
                         DSCTO.auto.ObtieneEpos.arrIcEpoModificadoV.push(e.record.get('IC_EPO'));
                         DSCTO.auto.ObtieneEpos.arrIcIfModificadoV.push(e.record.get('IC_IF'));
                         DSCTO.auto.ObtieneEpos.arrIcPymeModificadoV.push(e.record.get('IC_PYME'));
                         DSCTO.auto.ObtieneEpos.arrIcMonedaModificadoV.push(e.record.get('IC_MONEDA'));
                         DSCTO.auto.ObtieneEpos.arrOrdenModificadoV.push(e.record.get('CS_ORDEN'));
                         DSCTO.auto.ObtieneEpos.arrModalidadModificadoV.push(e.value); //MODALIDAD
                         DSCTO.auto.ObtieneEpos.arrModificadosIDV.push(idValorModificadoV);
			 
		    }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='D'){ //2017_003
                         DSCTO.auto.ObtieneEpos.arrIcEpoModificadoD.push(e.record.get('IC_EPO'));
                         DSCTO.auto.ObtieneEpos.arrIcIfModificadoD.push(e.record.get('IC_IF'));
                         DSCTO.auto.ObtieneEpos.arrIcPymeModificadoD.push(e.record.get('IC_PYME'));
                         DSCTO.auto.ObtieneEpos.arrIcMonedaModificadoD.push(e.record.get('IC_MONEDA'));
                         DSCTO.auto.ObtieneEpos.arrOrdenModificadoD.push(e.record.get('CS_ORDEN'));
                         DSCTO.auto.ObtieneEpos.arrModalidadModificadoD.push(e.value); //MODALIDAD
                         DSCTO.auto.ObtieneEpos.arrModificadosIDD.push(idValorModificadoD);	 
		    
                    }else if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){ //2017_003
                         DSCTO.auto.ObtieneEpos.arrIcEpoModificadoAE.push(e.record.get('IC_EPO'));
                         DSCTO.auto.ObtieneEpos.arrIcIfModificadoAE.push(e.record.get('IC_IF'));
                         DSCTO.auto.ObtieneEpos.arrIcPymeModificadoAE.push(e.record.get('IC_PYME'));
                         DSCTO.auto.ObtieneEpos.arrIcMonedaModificadoAE.push(e.record.get('IC_MONEDA'));
                         DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE.push(e.record.get('CS_ORDEN'));
                         DSCTO.auto.ObtieneEpos.arrModalidadModificadoAE.push(e.value); //MODALIDAD
                         DSCTO.auto.ObtieneEpos.arrModificadosIDAE.push(idValorModificadoDAE);
                      }
                   }
               }
          } // TERMINA BLOQUE DE MODALIDAD
          
          
			 //validaCamposVacios();
			 validaNumeracion();
			 
			 /**
			 *	valida numeracion
			 */
			 function validaNumeracion(){			
			 
				var gridEditor = e.grid;
				var store = gridEditor.getStore();
				var registro = store.getAt(e.row);
					
				if(e.field == 'CS_ORDEN' ){				
					var cont = 0;					
					 
					store.each(function(record){	
						
						if(record.get('CS_ORDEN') == e.value){
							if(record.get('IC_MONEDA') == e.record.get('IC_MONEDA') 	&&
							 record.get('CS_DSCTO_AUTOMATICO_DIA') == e.record.get('CS_DSCTO_AUTOMATICO_DIA')   &&
							  record.get('CC_TIPO_FACTORAJE') == e.record.get('CC_TIPO_FACTORAJE')   &&
							  record.get('CS_DSCTO_AUTOMATICO_DIA') == 'N'  && 
							   (   record.get('CC_TIPO_FACTORAJE') ==='N'   || record.get('CC_TIPO_FACTORAJE') ==='D'   || record.get('CC_TIPO_FACTORAJE') ==='A'    )   ){
							  	Ext.Msg.show({
									title: 'Aviso: ',
									msg: 'No esta permitido capturar el orden',
									icon: Ext.Msg.INFO,
									buttons: Ext.Msg.OK,
									fn: function(){
										registro.set('CS_ORDEN','');
										e.value = '';
										DSCTO.auto.ObtieneEpos.arrOrdenModificado[posicionValorModificado] = '';
										DSCTO.auto.ObtieneEpos.arrOrdenModificadoD[posicionValorModificadoD] = '';
                                                                                DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE[posicionValorModificadoDAE] = '';
										e.grid.startEditing(e.row, (3));
										e.record.markDirty();
										return;
									}
								});
							}
						}
						                                                
						if(e.row != cont){						
							if(record.get('CS_ORDEN') == e.value  && e.value !='' ){
								if(record.get('IC_MONEDA') == e.record.get('IC_MONEDA')    ){
									Ext.Msg.show({
										title: 'Aviso: ',
										msg: 'Ya existe �ste n�mero parametrizado ...',
										icon: Ext.Msg.INFO,
										buttons: Ext.Msg.OK,
										fn: function(){
											registro.set('CS_ORDEN','');
											e.value = '';
											DSCTO.auto.ObtieneEpos.arrOrdenModificado[posicionValorModificado] = '';
											DSCTO.auto.ObtieneEpos.arrOrdenModificadoD[posicionValorModificadoD] = '';
                                                                                        DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE[posicionValorModificadoDAE] = '';
											e.grid.startEditing(e.row, (3));
											e.record.markDirty();
											return;
										}
									});
								}
							}
						}
						cont++;                                                
					});		
					
					// BUSCA NUMEROS FALTANTES
					var arrTemmporal = new Array();	
                                       	
					store.each(function(record){
						if(record.get('IC_MONEDA') == e.record.get('IC_MONEDA')){
							arrTemmporal.push(record.get('CS_ORDEN'));                                                           
						}				
					});	
                                        
					   
					for (var i=1; i<arrTemmporal.length;i++){
						  for(var j=0;j<arrTemmporal.length-1;j++){
								 if (arrTemmporal[j] > arrTemmporal[j+1]){
											var temp = arrTemmporal[j];
											arrTemmporal[j]= arrTemmporal[j+1];
											arrTemmporal[j+1]= temp;
								 }
						  }
					}
                                                                               
                                
					if(arrTemmporal.length==1) {
						if(e.value>1){
							store.each(function(record){
								if(e.row != cont){
									if(record.get('CS_ORDEN') == e.value){
										if(record.get('IC_MONEDA') == e.record.get('IC_MONEDA')){
											Ext.Msg.show({
												title: 'Mensaje: ',
												msg: 'Recuerde que debe ingresar n�meros consecutivos comenzando con el 1...',
												icon: Ext.Msg.INFO,
												buttons: Ext.Msg.OK,
												fn: function(){
													registro.set('CS_ORDEN','');
													e.value = '';
													DSCTO.auto.ObtieneEpos.arrOrdenModificado[posicionValorModificado] = '';
													DSCTO.auto.ObtieneEpos.arrOrdenModificadoD[posicionValorModificadoD] = '';
                                                                                                        DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE[posicionValorModificadoDAE] = '';
													e.grid.startEditing(e.row, (3));
													e.record.markDirty();
													return;
												}
											});
										}
									}
								}
								cont++;
							});
							DSCTO.auto.ObtieneEpos.arrNumerosConsecutivosFaltantes = true;
						}else {
							DSCTO.auto.ObtieneEpos.arrNumerosConsecutivosFaltantes = false;
						}		
					
					}else if(arrTemmporal.length>1) {
						var c = 1;			
						for(var i=arrTemmporal.length-1; i>=0; i--){										
							if(c==1){
								var conta = 0;
								for(var x=arrTemmporal[i-1]; x<e.value; x++){
									conta++;
								}                                                              
                                                                
								if(e.value>arrTemmporal.length || conta>1){
									store.each(function(record){
										if(e.row != cont){
											if(record.get('CS_ORDEN') == e.value){
												if(record.get('IC_MONEDA') == e.record.get('IC_MONEDA')){
													Ext.Msg.show({
														title: 'Mensaje: ',
														msg: 'Recuerde que debe ingresar n�meros consecutivos comenzando con el 1 ...',
														icon: Ext.Msg.INFO,
														buttons: Ext.Msg.OK,
														fn: function(){
															registro.set('CS_ORDEN','');
															e.value = '';
															DSCTO.auto.ObtieneEpos.arrOrdenModificado[posicionValorModificado] = '';
															DSCTO.auto.ObtieneEpos.arrOrdenModificadoD[posicionValorModificadoD] = '';
                                                                                                                        DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE[posicionValorModificadoDAE] = '';
															e.grid.startEditing(e.row, (3));
															e.record.markDirty();
															return;
														}
													});
												}
											}
										}
										cont++;
									});					
									DSCTO.auto.ObtieneEpos.arrNumerosConsecutivosFaltantes = true;
								}else{
									DSCTO.auto.ObtieneEpos.arrNumerosConsecutivosFaltantes = false;
								}
							}
							c++;
						}	
						
						//Validar que los valores de Orden sean consecutivos
							var totalRegNP =0;
							var total = store.getTotalCount();
							store.each(function(record){						
								if(record.get('IC_MONEDA') == e.record.get('IC_MONEDA')){												
									if(record.get('CC_TIPO_FACTORAJE')=='N' || record.get('CC_TIPO_FACTORAJE')==='D' || record.get('CC_TIPO_FACTORAJE')==='A' ) {	
										if( record.get('CS_DSCTO_AUTOMATICO_DIA')=='N' ){
											total--;
										}
									}
								}
							});					
							store.each(function(record){						
							if(record.get('IC_MONEDA') == e.record.get('IC_MONEDA')){												
								if(record.get('CC_TIPO_FACTORAJE')==='N'  || record.get('CC_TIPO_FACTORAJE')==='D' || record.get('CC_TIPO_FACTORAJE')==='A') {														
									if(( record.get('CS_DSCTO_AUTOMATICO_DIA')=='P'  || record.get('CS_DSCTO_AUTOMATICO_DIA')=='U' || record.get('CS_DSCTO_AUTOMATICO_DIA')=='A') &&  record.get('CS_ORDEN')!=''  )  {
										var orden = record.get('CS_ORDEN');
										if(orden>total){
											totalRegNP++;	
										}
									}	
								}
							}														
						});
						
						if(totalRegNP>0) {						
							DSCTO.auto.ObtieneEpos.arrNumerosConsecutivosFaltantes = true;
						}else  {
							DSCTO.auto.ObtieneEpos.arrNumerosConsecutivosFaltantes = false;
						}	
						
					}	
				
				}	else  if(e.field == 'CS_DSCTO_AUTOMATICO_DIA')  {
				
					// Esto es para que  verificar el orden cuando se cambia la opci�n de la Modalidad a 'N'
					var totalRegNP= 0;
					var total = store.getTotalCount();
					store.each(function(record){						
						if(record.get('IC_MONEDA') == e.record.get('IC_MONEDA')){												
							if(record.get('CC_TIPO_FACTORAJE')==='N'  || record.get('CC_TIPO_FACTORAJE')==='D' || record.get('CC_TIPO_FACTORAJE')==='A') {	
								if( record.get('CS_DSCTO_AUTOMATICO_DIA')=='N' ){
									total--;
								}
							}
						}
					});
					
					store.each(function(record){						
						if(record.get('IC_MONEDA') == e.record.get('IC_MONEDA')){												
							if(record.get('CC_TIPO_FACTORAJE')==='N'  || record.get('CC_TIPO_FACTORAJE')==='D' || record.get('CC_TIPO_FACTORAJE')==='A' ) {														
								if(( record.get('CS_DSCTO_AUTOMATICO_DIA')==='P'  || record.get('CS_DSCTO_AUTOMATICO_DIA')==='U' || record.get('CS_DSCTO_AUTOMATICO_DIA')==='A') &&  record.get('CS_ORDEN')!==''  )  {
									var orden = record.get('CS_ORDEN');
									if(orden>total){
										totalRegNP++;	
									}
								}	
							}
						}														
					});
					
					if(totalRegNP>0) {						
						DSCTO.auto.ObtieneEpos.arrNumerosConsecutivosFaltantes = true;
					}else  {
						DSCTO.auto.ObtieneEpos.arrNumerosConsecutivosFaltantes = false;
					}
				}
				validaCamposVacios();
			}			 
			 
			 /*
			 *	Valida si existen campos vacios
			 *
			 */
			 function validaCamposVacios(){
				var tempExisteVacio = true;
				
				if(DSCTO.auto.ObtieneEpos.tipoFactoraje=='N'){
					DSCTO.auto.ObtieneEpos.existeVacio = true;
					
					for(var i=0; i<DSCTO.auto.ObtieneEpos.arrIcEpoModificado.length; i++){
                  var id 			= 	DSCTO.auto.ObtieneEpos.arrModificadosID[i];						
						var modalidad 	= 	DSCTO.auto.ObtieneEpos.arrModalidadModificado[i];
						var orden 		= 	DSCTO.auto.ObtieneEpos.arrOrdenModificado[i];
						
						modalidad 	= 	modalidad==null?"":modalidad;
						orden			=	orden==null?"":orden;						
						if(!Ext.isEmpty(modalidad)){
							tempExisteVacio = false;
						}
												
						if(!Ext.isEmpty(orden)){
							tempExisteVacio = false;
						}
						
						if(modalidad!="N"  ){
							if(orden=='0' || orden==0 ){								
								tempExisteVacio = true;
							}	
						}
						
						if( modalidad!="U"  &&   modalidad!="P" && modalidad!="N"  ){								
							DSCTO.auto.ObtieneEpos.existeVacio = true;
							e.grid.startEditing(e.row, (4));
							e.record.markDirty();	
												
						}else 	if(( (modalidad!="N" && orden=="") || (modalidad=="" && orden!="") ) || tempExisteVacio ){
							
								DSCTO.auto.ObtieneEpos.existeVacio = true;
															
								if(Ext.isEmpty(orden)){
									e.grid.startEditing(e.row, (3));
									e.record.markDirty();
								}
								if(Ext.isEmpty(modalidad)){
									e.grid.startEditing(e.row, (4));
									e.record.markDirty();
								}							
						}else{
							DSCTO.auto.ObtieneEpos.existeVacio = false;
						}
						
				    }
				} else  if(DSCTO.auto.ObtieneEpos.tipoFactoraje=='V'){
					
					DSCTO.auto.ObtieneEpos.existeVacio = true;
					
					for(var i=0; i<DSCTO.auto.ObtieneEpos.arrIcEpoModificadoV.length; i++){
                  var id 			= 	DSCTO.auto.ObtieneEpos.arrModificadosIDV[i];						
						var modalidad 	= 	DSCTO.auto.ObtieneEpos.arrModalidadModificadoV[i];
						modalidad 	= 	modalidad==null?"":modalidad;
						
					
						if(!Ext.isEmpty(modalidad)){
							tempExisteVacio = false;
						}
						
						if( modalidad!="U"  &&  modalidad!="P" && modalidad!="N" ){							
							DSCTO.auto.ObtieneEpos.existeVacio = true;
							e.grid.startEditing(e.row, (4));
							e.record.markDirty();						
						}else {
							DSCTO.auto.ObtieneEpos.existeVacio = false;
						}
						
					}
				
				} else  if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='D'){
					
					DSCTO.auto.ObtieneEpos.existeVacio = true;
					for(var x=0; x<DSCTO.auto.ObtieneEpos.arrIcEpoModificadoD.length; x++){
					    var modalidadx 	= 	DSCTO.auto.ObtieneEpos.arrModalidadModificadoD[x];
					    var ordenx 		= 	DSCTO.auto.ObtieneEpos.arrOrdenModificadoD[x];						
					    modalidadx 	= 	modalidadx===null?"":modalidadx;
					    ordenx			=	ordenx==null?"":ordenx;						
					    if(!Ext.isEmpty(modalidadx)){
						tempExisteVacio = false;
					    }
											
					    if(!Ext.isEmpty(ordenx)){
						tempExisteVacio = false;
					    }
					
					    if(modalidad!=="N"  && (orden==='0' || orden===0 ) ){								
						    tempExisteVacio = true;
					    }
						
					    if( modalidadx!=="U"  &&   modalidad!=="P" && modalidad!=="N"  ){								
						DSCTO.auto.ObtieneEpos.existeVacio = true;
						e.grid.startEditing(e.row, (4));
						e.record.markDirty();	
												
					    }else if(( (modalidadx!=="N" && ordenx==="") || (modalidadx==="" && ordenx!=="") ) || tempExisteVacio ){
						
						DSCTO.auto.ObtieneEpos.existeVacio = true;
														
						if(Ext.isEmpty(orden)){
						    e.grid.startEditing(e.row, (3));
						    e.record.markDirty();
						}
						if(Ext.isEmpty(modalidad)){
						    e.grid.startEditing(e.row, (4));
						    e.record.markDirty();
						}							
					    }else{
						DSCTO.auto.ObtieneEpos.existeVacio = false;
					    }
						
				    }					
				
                                 } else  if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){
					
					DSCTO.auto.ObtieneEpos.existeVacio = true;  
                                        
                                         
					for(var a=0; a<DSCTO.auto.ObtieneEpos.arrIcEpoModificadoAE.length; a++){
					    var modalidadxa 	= 	DSCTO.auto.ObtieneEpos.arrModalidadModificadoAE[a];
					    var ordenxa 		= 	DSCTO.auto.ObtieneEpos.arrOrdenModificadoAE[a];						
					    modalidadxa 	= 	modalidadxa===null?"":modalidadxa;
					    ordenxa			=	ordenxa==null?"":ordenxa;
                                                                              
                                             
					    if(!Ext.isEmpty(modalidadxa)){
						tempExisteVacio = false;
					    }
											
					    if(!Ext.isEmpty(ordenxa)){
						tempExisteVacio = false;
					    }
					
					    if(modalidad!=="N"  && (orden==='0' || orden===0 ) ){								
						    tempExisteVacio = true;
					    }
						
					    if( modalidadxa!=="A" ){								
						DSCTO.auto.ObtieneEpos.existeVacio = true;
						e.grid.startEditing(e.row, (4));
						e.record.markDirty();	
												
					    }else if(( (modalidadxa!=="A" && ordenxa==="") || (modalidadxa==="" && ordenxa!=="") ) || tempExisteVacio ){
						
						DSCTO.auto.ObtieneEpos.existeVacio = true;
														
						if(Ext.isEmpty(orden)){
						    e.grid.startEditing(e.row, (3));
						    e.record.markDirty();
						}
						if(Ext.isEmpty(modalidad)){
						    e.grid.startEditing(e.row, (4));
						    e.record.markDirty();
						}							
					    }else{
						DSCTO.auto.ObtieneEpos.existeVacio = false;
					    }
						
				    }					
				
                                	
				}else{
					DSCTO.auto.ObtieneEpos.existeVacio = false;
				}
			 }
			 
          /*
          * Funcion para activar el LOG, SE ELIMINARA AL SUBIR A PRODUCCION
          *
          */
         fnActivaLogActividad(false);          
         function fnActivaLogActividad(esActiva){
            /*
            *  TRUE para activar el log de actividad
            *  FALSE para desactivar el log de actividad
            */
            if( esActiva ){
				/*
               console.clear();
               for(var i=0; i<DSCTO.auto.ObtieneEpos.arrIcEpoModificado.length; i++){
                  console.log('========== �QUE CONTIENE EL ARRAY? ===============');
                  console.log(i);            
                  console.log('existeValorModificado ' + existeValorModificado);
                  console.log('posicionValorModificado ' + posicionValorModificado);
                  console.log('ID ' + DSCTO.auto.ObtieneEpos.arrModificadosID[i]);
                  console.log('IC_EPO ' + DSCTO.auto.ObtieneEpos.arrIcEpoModificado[i]);
                  console.log('ORDEN ' + DSCTO.auto.ObtieneEpos.arrOrdenModificado[i]);
                  console.log('MODALIDAD ' + DSCTO.auto.ObtieneEpos.arrModalidadModificado[i]);
                  console.log('MONEDA ' + DSCTO.auto.ObtieneEpos.arrIcMonedaModificado[i]);
               }
					*/
             }
         }  
         
         
           
         
      } //TERMINA VALIDACION FACTORAJE NORMAL
      		/*========================================================================
                             COMPONENTES EXTJS                                  
      ========================================================================*/		
		var getRadioOperarDsctoAut = new Ext.form.RadioGroup({
				id: 'getRadioOperarDsctoAut',
   			columns: 6,
   			items: [
					 {boxLabel: 'Si autorizo', name: 'autorizar', inputValue: 'SI'},
					 {boxLabel: 'No autorizo', name: 'autorizar', inputValue: 'NO'}
				],
				listeners: {
					change: fnChkAutorizar
				}
		});
      
      /*================== COMBO-BOX EDITOR GRID PANEL =======================*/
      /*
      var dataModalidad = new Ext.data.ArrayStore({	 
	  fields: ['clave','descripcion'  ],	 
	  autoLoad: false	  
	}); 
 
    if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){
        dataModalidad.loadData( [   
            ['N','No Autorizo Parametrizaci�n'],
            ['A','Descuento Autom�tico EPO']
        ] );	   
    }else  {
        dataModalidad.loadData(	[	
            ['N','No Autorizo Parametrizaci�n'],
            ['P','Primer d�a desde su publicaci�n'],
            ['U','�ltimo d�a desde su vencimiento considerando d�as m�nimos'] 		
        ]);  
    }
        
      var cmbModalidadEditorGridPanel = new Ext.form.ComboBox({
         typeAhead: true,
         editable: true,
         selectOnFocus: true,
         triggerAction: 'all',
         valueField: 'clave',
         displayField: 'descripcion',
         mode: 'local',
         store: dataModalidad     
      });
      */
          
		var acusePanel = new Ext.Panel({
			id: 'acusePanel',
			title: 'Recibo n�mero ',
			style: 'margin: 0 auto;',
			width: 600,
			items: [
				{
					xtype: 'fieldset',
					border: true,
					style: 'margin: 0px',
					items: [
						new Ext.grid.EditorGridPanel({
							id: 'gridAcuse',
							store: new Ext.data.JsonStore({
								root: 'registros',
								url: DSCTO.auto.ObtieneEpos.urlRequest,
								fields:[
									{	name: 'TITULO'	            },
									{	name: 'VALOR'	   			}
								],
								totalProperty : 'total',
								messageProperty: 'msg',
								autoLoad: false
							}),
							loadMask:true,	
							frame: false,
							border: true,
							style: 'margin: 10px auto', 
							stripeRows: true,
							columnLines: true,
							enableColumnMove: false,
							enableColumnHide: false,
							height: 110,
							columns:[
								{
									dataIndex: 'TITULO',
									sortable: false,
									hideable: false,
									width: 248,
									align: 'left',
									hidden: false,
									renderer: function(v){
										return '<b>' + v + '</b>';
									}
								},
								{
									dataIndex: 'VALOR',
									sortable: false,
									hideable: false,
									width: 310,
									align: 'left',
									hidden: false
								}
							]
						})							
					]
				}
			]
		});
		
		/*=============================== GRID =================================*/		
		function gridFactorajeNormal(idDinamico){
                
                        var desabilitado = false;
                
                    var dataModalidad = new Ext.data.ArrayStore({	 
                      fields: ['clave','descripcion'  ],	 
                      autoLoad: false	  
                    }); 
                    
                     if(DSCTO.auto.ObtieneEpos.tipoFactoraje==='A'){
                        dataModalidad.loadData( [   
                            ['N','No Autorizo Parametrizaci�n'],
                            ['A','Descuento Autom�tico EPO']
                        ] );	
                        
                        desabilitado = true;
                        
                    }else  {
                        dataModalidad.loadData(	[	
                            ['N','No Autorizo Parametrizaci�n'],
                            ['P','Primer d�a desde su publicaci�n'],
                            ['U','�ltimo d�a desde su vencimiento considerando d�as m�nimos'] 		
                        ]);  
                    }                                       
                    
                    var cmbModalidadEditorGridPanel = new Ext.form.ComboBox({
                     typeAhead: true,
                     editable: true,
                     selectOnFocus: true,
                     triggerAction: 'all',
                     valueField: 'clave',
                     displayField: 'descripcion',
                     mode: 'local',
                     store: dataModalidad     
                  });
      
                    
                    
			return new Ext.grid.EditorGridPanel({
					id: 'gridFactorajeNormal_' + idDinamico,
					store: new Ext.data.GroupingStore({
						root: 'registros',
						url: DSCTO.auto.ObtieneEpos.urlRequest,
						baseParams: {
							informacion: 'obtieneFactorajeNormal',
                     idDinamico: idDinamico,
							cc_tipo_factoraje: DSCTO.auto.ObtieneEpos.tipoFactoraje
							, ic_pyme: ic_pyme
						},
						reader: new Ext.data.JsonReader({
                     root: 'registros',
                     totalProperty: 'total',
                     fields:[
                        {	name: 'IC_PYME'	               },
                        {	name: 'NOMBRE_PYME'	            },
                        {	name: 'IC_EPO'	                  },
                        {  name: 'NOMBRE_EPO'	            },
                        {	name: 'IC_IF'	                  },
                        {	name: 'NOMBRE_IF'	               },
                        {  name: 'IC_MONEDA'	               },
                        {	name: 'NOMBRE_MONEDA'	         },
                        {	name: 'FECHA_LIBERACION'	      },
                        {  name: 'CS_ORDEN', type: 'string' },
                        {	name: 'CS_DSCTO_AUTOMATICO_DIA'	},
                        {	name: 'CS_DSCTO_AUTOMATICO_PROC'	},
                        {  name: 'CC_TIPO_FACTORAJE'	      }
                     ]
                  }),
                  groupField: 'NOMBRE_MONEDA',
                  sortInfo:{field: 'NOMBRE_MONEDA', direction: "ASC"},
                  totalProperty : 'total',
                  messageProperty: 'msg',
                  autoLoad: false,
                  listeners: {
                     load: function(store, arrRegistros, opts) {
                        if (arrRegistros != null) {                        
                           
                           if(store.getTotalCount() > 0) { 
                              Ext.getCmp('fieldset_' + idDinamico).setVisible(true);
                           }else {
                              Ext.getCmp('fieldset_' + idDinamico).setVisible(false);
                           }
                        }
                     },
                     exception: {
                        fn: function(proxy, type, action, optionRequest, response, args) {
                             NE.util.mostrarDataProxyError(proxy, type, action, optionRequest, response, args);
                        }
                     }
                  }
					}),
					loadMask:true,	
					frame: false,
					border: true,
					style: 'margin: 0 auto', 
               stripeRows: true,
               columnLines: true,
               clicksToEdit: 1,
               enableColumnMove: false,
               enableColumnHide: false,
					height: 180,
					autoHeight: true,
					columns:[
						{
							header: 'Moneda: ',
							dataIndex: 'NOMBRE_MONEDA',
							sortable: true,
                     hideable: false,
							width: 270,
							align: 'center',
                     hidden: true
						},
						{
							header: 'Intermediario Financiero',
							dataIndex: 'NOMBRE_IF',
							sortable: true,
							width: 270,
							align: 'left'
						},
						{
							header: 'Fecha y Hora de Liberaci�n',
							dataIndex: 'FECHA_LIBERACION',
							sortable: true,
							width: 200,
							align: 'center'
						},						
						{
                     editor: new Ext.form.NumberField({
                        allowBlank: true,
                        allowNegative: false,
                        maxLength: 3,
                        minValue: 1,
                        autocomplete: 'off'
                    }), 
							header: 'Orden',
							dataIndex: 'CS_ORDEN',
							sortable: true,
							width: 100,
							align: 'center',
							renderer: function(value,metadata,record){
								return NE.util.colorCampoEdit(value,metadata,record);
							}
						},
						{  
							header: 'Modalidad',
							dataIndex: 'CS_DSCTO_AUTOMATICO_DIA', //MODALIDAD
							sortable: false,
							hideable: false,
							width: 300,
							align: 'left',
                     editor: new Ext.form.ComboBox({
                        typeAhead: true,
                        editable: true,
                        allowBlank: false,
                        //selectOnFocus: true,
                        triggerAction: 'all',
                        valueField: 'clave',
                        displayField: 'descripcion',
                        mode: 'local',
                        store:dataModalidad,
                        disabled:desabilitado
                     }),
                     renderer: function(value, metadata, record, rowIndex, colIndex, store){  
                     
                        var index = cmbModalidadEditorGridPanel.getStore().find('clave',value);    
                        var valor = record.data['CS_DSCTO_AUTOMATICO_DIA'];	
                        if(valor=='U' ||  valor=='P' || valor=='N'  || valor=='A' ){ 
                           var record = cmbModalidadEditorGridPanel.getStore().getAt(index);  
                           return NE.util.colorCampoEdit(record.get('descripcion'),metadata,record);  
                        }
                        if(valor!=='U' ||  valor!=='P' || valor!=='N'  || valor!=='A' )  {
                            return  NE.util.colorCampoEdit('',metadata,record);
			}	                        
                     }
						}
					],
               view: new Ext.grid.GroupingView({
                  forceFit: true,
                  groupTextTpl: '{text}'
              }),
              listeners: {
                  afteredit: fnGuardarValoresModificadosFactNormal                
              }
			});
		}
      
		
		/*========================= Panel superior =============================*/
		var panelSuperior = new Ext.Panel({
			style:			'margin: 20px 20px 10px 10px; color: #086A87; font-weight: bold; text-align:center',
			bodyStyle:		'padding:0 20px 0 0',
			border:			false,
			height:			30,
			defaultType: 	'displayfield',
			items: [ ]
		});
      
		/*==================== Panel que contiene el TabPanel ==================*/
		var panelTabs = new Ext.Panel({
			border: false,
         autoHeight: true,
			items: [ ]
		});
      
		/*============================= Tabs ===================================*/
		var tabsFactoraje = new Ext.TabPanel({
			 activeTab: 0,
			 manageHeight: false,
                         id:'tabsFactoraje',
          forceLayout: true,
          autoHeight: true,
			 items: [{
				  title: 'Factoraje Normal',
				  id: 'tabFactorajeNormal',
              layout: 'form',
              autoHeight: true,
				  items: [ ]
			 },{
				  title: 'Factoraje Vencido',
				  id: 'tabFactorajeVencido',
              layout: 'form',
              autoHeight: true,
				  items: [ ]
			 },
			 {
			    title: 'Factoraje Distribuido',
			    id: 'tabFactorajeDistribuido',
			    layout: 'form',
			    autoHeight: true,
			    items: [ ]
			 },
                         {
			    title: 'Descuento Autom�tico EPO',
			    id: 'tabDescuentoAutoEPO',
			    layout: 'form',
                            hidden: true,
			    autoHeight: true,
			    items: [ ]
			 }
                         
                         ],
          listeners: {
            tabchange: function(obj, tab){
               if(tab.id === 'tabFactorajeNormal'){
                  DSCTO.auto.ObtieneEpos.tipoFactoraje = 'N';
               }
               else if(tab.id === 'tabFactorajeVencido'){
                  DSCTO.auto.ObtieneEpos.tipoFactoraje = 'V';
                  
                  if(DSCTO.auto.ObtieneEpos.loadTabFactorajeVencido==false){
                     DSCTO.auto.ObtieneEpos.loadTabFactorajeVencido = true;
                     DSCTO.auto.ObtieneEpos.init(); 
                     DSCTO.auto.ObtieneEpos.estado = 'CARGA_FACTORAJE_VENCIDO';
                  }
	       }else if(tab.id === 'tabFactorajeDistribuido'){
		DSCTO.auto.ObtieneEpos.tipoFactoraje = 'D';
                  
                  if(DSCTO.auto.ObtieneEpos.loadTabFactorajeDistribuido==false){
                     DSCTO.auto.ObtieneEpos.loadTabFactorajeDistribuido = true;
                     DSCTO.auto.ObtieneEpos.init(); 
                     DSCTO.auto.ObtieneEpos.estado = 'CARGA_FACTORAJE_DISTRIBUIDO';
                  }
	       
               }else if(tab.id === 'tabDescuentoAutoEPO'){               
                                      
                    DSCTO.auto.ObtieneEpos.tipoFactoraje = 'A';
                    
                    if(DSCTO.auto.ObtieneEpos.loadTabDescuentoAutoEPO===false){
                        DSCTO.auto.ObtieneEpos.loadTabDescuentoAutoEPO = true;
                        DSCTO.auto.ObtieneEpos.init(); 
                        DSCTO.auto.ObtieneEpos.estado = 'CARGA_DESCUENTO_AUTO_EPO';
                    }
                   
               }                
            }
          }
		});
		
		/*=================== Formulario Principal =============================*/
		var frmPrincipal = new Ext.FormPanel({
				title: 'Descuento Autom�tico',
            style: 'height: auto',
            autoHeight: true,
            forceLayout: true,
            layout: 'fit',
				items: [],
				bbar:['->','-',
					{
						xtype: 'button',
						text: 'Guardar',
						id: 'btnGuardar',
						iconCls: 'icoGuardar',
						handler: fnGuardar
					},'-'
				]
		});
		
		/*=================== Contenedor Principal =============================*/		
		var pnl = new Ext.Container({
				id: 		'contenedorPrincipal',
				applyTo: 'areaContenido',
				style: 	'margin:0 auto;',
            renderHidden: 	true,
            autoHeight: true,
				width: 	949,
				items: 	[]
		});
		
		/*========================= INICIA PROGRAMA ============================*/      
      DSCTO.auto.ObtieneEpos.init();     
});