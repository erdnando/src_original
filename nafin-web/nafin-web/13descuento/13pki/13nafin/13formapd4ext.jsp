<!DOCTYPE html>
<%@ page import="java.util.*,
		netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%
	String strNePymeAsigna = (session.getAttribute("strNePymeAsigna")==null)?"":(String)session.getAttribute("strNePymeAsigna");
	String strNombrePymeAsigna = (session.getAttribute("strNombrePymeAsigna")==null)?"":(String)session.getAttribute("strNombrePymeAsigna");
	String sOrigenDsctoss = (session.getAttribute("ssOrigenDscto")==null)?"":(String)session.getAttribute("ssOrigenDscto");
	
	String envia = (request.getParameter("envia")==null)?"":request.getParameter("envia");
	String cvePyme = (request.getParameter("cvePyme")==null)?"":request.getParameter("cvePyme");
	String cboEpo = (request.getParameter("cboEpo")==null)?"":request.getParameter("cboEpo");
	String cveEpo = (request.getParameter("cveEpo")==null)?"":request.getParameter("cveEpo");
	
	if(!"".equals(cboEpo)){
	  cboEpo = cboEpo.substring(0,cboEpo.indexOf('|'));
	}else  {
		cboEpo = cveEpo;
	}
%>
<html>   
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>

<%-- El jsp que contiene el dise�o de la forma se saca a un js de ser posible,
de manera que el archivo pueda ser comprimido (pensando en que se va meter
un filtro que comprima archivos *.js *.css para hacer mas rapida la carga
del GUI--%>
<style type="text/css">
	.x-selectable, .x-selectable * {
		-moz-user-select: text!important;
		-khtml-user-select: text!important;
	}
.x-grid3-row.user-mci .x-grid3-cell
{
	background-color: #C0C0C0;
}
</style>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<script language="JavaScript" src="/nafin/00utils/NEcesionDerechos.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/13descuento/13formapd4ext.js?<%=session.getId()%>"></script>
<script type="text/javascript" src="/nafin/00utils/extjs/ux/GroupSummary.js"></script>
<link rel="stylesheet" href="/nafin/00utils/extjs/ux/GroupSummary.css">
<%@ include file="/00utils/componente_firma.jspf" %>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
		<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
			<div id="areaContenido"><div style="height:190px"></div></div>
	</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>
<!-- Valores iniciales recibidos como parametros-->
<input type="hidden" id="hidStrUsuario" value="<%=strTipoUsuario%>">
<input type="hidden" id="hidCboEpo" value="<%=cboEpo%>">
<input type="hidden" id="cvePyme" value="<%=cvePyme%>">
<input type="hidden" id="strNePymeAsigna" value="<%=strNePymeAsigna%>">
<input type="hidden" id="strNombrePymeAsigna" value="<%=strNombrePymeAsigna%>">
<input type="hidden" id="envia" value="<%=envia%>">
<input type="hidden" id="hidCboMoneda" value="<%=(request.getParameter("cboMoneda")==null)?"":request.getParameter("cboMoneda")%>">
<input type="hidden" id="hidTxtFechaVencDe" value="<%=(request.getParameter("txtFechaVencDe")==null)?"":request.getParameter("txtFechaVencDe")%>">
<input type="hidden" id="hidTxtFechaVenca" value="<%=(request.getParameter("txtFechaVenca")==null)?"":request.getParameter("txtFechaVenca")%>">
<input type="hidden" id="hidStrUsr" value="<%=(request.getParameter("strUsr")==null)?"":request.getParameter("strUsr")%>">
</body>
</html>