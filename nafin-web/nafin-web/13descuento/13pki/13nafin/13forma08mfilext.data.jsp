<%@ page contentType="text/html; charset=UTF-8"
	import="
		javax.naming.*,	
		java.text.*,
		java.math.*,
		com.netro.pdf.*,
		java.sql.*,
		com.netro.exception.*,	
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*, 	
		netropology.utilerias.*, 
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,		
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		org.apache.commons.logging.Log"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%

	String 				informacion 			= (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	
	String 				rutaArchivoTemporal	= "";
	ParametrosRequest req 						= null;
	String 				itemArchivo				= "";
	String 				rutaArchivo 			= "";
	String 				error_tam 				= ""; 
	String 				PATH_FILE				= strDirectorioTemp;
	String 				nombreArchivo			= "";
	
	log.debug("ServletFileUpload.isMultipartContent(request)=="+ServletFileUpload.isMultipartContent(request));
	
	// 1. Guardar archivo txt con el detalle de las tasas a cargar/eliminar
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints		
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
	
		req = new ParametrosRequest(upload.parseRequest(request));
			 
		FileItem fItem			= (FileItem)req.getFiles().get(0);
		itemArchivo				= (String)fItem.getName();
		InputStream archivo	= fItem.getInputStream();
		rutaArchivo 			= PATH_FILE+itemArchivo;
		int tamanio				= (int)fItem.getSize();
		
		nombreArchivo			= Comunes.cadenaAleatoria(16)+ ".txt";
		rutaArchivoTemporal	= PATH_FILE + "/" + nombreArchivo;
		fItem.write(new File(rutaArchivoTemporal));
		
		if( tamanio > 8388608 ){
			error_tam ="El Archivo es muy Grande, excede el L�mite que es de 8 MB.";
		}		
		
	}
	if(!error_tam.equals("")){
		nombreArchivo="";
	}
		
	// 2. Leer parametros de la solicitud
	String 	tipoTasa 			= (request.getParameter("tipoTasa")	!= null)?request.getParameter("tipoTasa")	:"";
	String 	ic_epo 				= (request.getParameter("ic_epo")	!= null)?request.getParameter("ic_epo")	:"";
	String 	ic_if 				= (request.getParameter("ic_if")		!= null)?request.getParameter("ic_if")		:"";
	String 	ic_moneda			= (request.getParameter("ic_moneda")!= null)?request.getParameter("ic_moneda"):"";
	String 	lstPlazo				= (request.getParameter("lstPlazo")	!= null)?request.getParameter("lstPlazo")	:"";

	// 3. Revisar si las tasas a cargar son de tipo negociable
	boolean esTasaNegociable	= tipoTasa.equals("N")?true:false;
 
	// 4. Obtener instancia del EJB de Autorizacion Tasas
	AutorizacionTasas beanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
	// 5. Validar archivo de la solicitud
	ResultadosValidacionTasas resultados = beanTasas.procesarCargaMasivaTasas(
		strDirectorioTemp, 
		rutaArchivoTemporal, 
		esTasaNegociable, 
		ic_moneda, 
		ic_epo, 
		ic_if, 
		lstPlazo
	);

	// 6. Obtener Registros Validos
	
	// Obtener lista de registros exitosos
	List 			lista 					= (List) resultados.getListaDeRegistrosExitosos(); 
	// Obtener id del proceso de carga	
	int 			icProceso 				= resultados.getIcProceso(); 	
	Registros 	registrosValidos 		= beanTasas.getRegistrosValidosExt(lista,icProceso);
	// Construir lista con los Numeros de Linea de los Registros Validos
	StringBuffer 	listaNumerosLinea	= new StringBuffer();
	for(int i=0;i<lista.size();i++){
		if(i>0) listaNumerosLinea.append(",");
		listaNumerosLinea.append((String) lista.get(i));
	}
	int 			totalTasasCargadas 	= registrosValidos.getNumeroRegistros();
 
	// 7. Agregar detalle de los registros validos
	String[]  campos 						= new String[] { "RFC", "PUNTOS_TASA", "ESTATUS" };
	int 		 totalTasasEliminar 		= 0;
	int 		 totalTasasAlta			= 0;
	JSONArray arrRegistrosValidos 	= new JSONArray();	
	while(registrosValidos.next()) {
		JSONObject registro = new JSONObject();
		for (int i = 0; i< campos.length; i++) {
			registro.put("LINEA",registrosValidos.getString("LINEA"));
			registro.put("CAMPO",campos[i]);
			if( campos[i].equals("ESTATUS")) {
				String estatus = registrosValidos.getString(campos[i]);
				if (estatus.equals("A")) {
					totalTasasAlta++;
				} else if (estatus.equals("E")) {
					totalTasasEliminar++;
				}
			}
			registro.put("OBSERVACION",registrosValidos.getString(campos[i]));
			arrRegistrosValidos.add(registro);
		}
	}
 
	// 8. Agregar resumen de las tasas a cargar
	JSONArray arrResumenAccion = new JSONArray();
	
	JSONObject registroResumenAccion1 = new JSONObject();
	registroResumenAccion1.put("ID",				"total_tasas_cargadas"				);
	registroResumenAccion1.put("DESCRIPCION",	"No. Total de Tasas Cargadas"		);
	registroResumenAccion1.put("TOTAL",			new Integer(totalTasasCargadas)	);
	
	JSONObject registroResumenAccion2 = new JSONObject();
	registroResumenAccion2.put("ID",				"total_tasas_alta"					);
	registroResumenAccion2.put("DESCRIPCION",	"No. Total de Tasas " + ((tipoTasa.equals("P"))?"Preferencial":"Negociada") +" Alta");
	registroResumenAccion2.put("TOTAL",			new Integer(totalTasasAlta)		);
	
	JSONObject registroResumenAccion3 = new JSONObject();
	registroResumenAccion3.put("ID",				"total_tasas_eliminadas"			);
	registroResumenAccion3.put("DESCRIPCION",	"No. Total de Tasas " + ((tipoTasa.equals("P"))?"Preferencial":"Negociada") +" Eliminadas");
	registroResumenAccion3.put("TOTAL",			new Integer(totalTasasEliminar)	);
	
	arrResumenAccion.add(registroResumenAccion1);
	arrResumenAccion.add(registroResumenAccion2);
	arrResumenAccion.add(registroResumenAccion3);
 
	// 9. Agregar detalle de los registros con error
	String[] 	camposDescripcion 	= new String[] {"RFC", "Puntos/Valor Tasa", 	"Estatus"};
	JSONArray arrRegistrosError = new JSONArray();
	if( resultados.getNumRegErr() > 0 ) {
		
		ArrayList 	registros	= resultados.getRegistros();
		Iterator 	it 			= registros.iterator();
		
		while(it.hasNext()) {
			ValidacionTasa v = (ValidacionTasa) it.next();
			if(v.hayError()) {
				JSONObject registro = new JSONObject();
				if(v.hayErrorRFC()){
					registro.put("LINEA",new Integer(v.getNumeroLinea()));
					registro.put("CAMPO",camposDescripcion[0]);
					registro.put("OBSERVACION",v.getErroresRfc());
					arrRegistrosError.add(registro);
				}
				if(v.hayErrorPuntosValorTasa()){
					registro.put("LINEA",new Integer(v.getNumeroLinea()));
					registro.put("CAMPO",camposDescripcion[1]);
					registro.put("OBSERVACION",v.getErroresPuntosValorTasa());
					arrRegistrosError.add(registro);
				}
				if(v.hayErrorEstatus()){
					registro.put("LINEA",new Integer(v.getNumeroLinea()));
					registro.put("CAMPO",camposDescripcion[2]);
					registro.put("OBSERVACION",v.getErroresEstatus());
					arrRegistrosError.add(registro);
				}
				if(v.hayErrorGenerico()){
					registro.put("LINEA",new Integer(v.getNumeroLinea()));
					registro.put("CAMPO","");
					registro.put("OBSERVACION",v.getErrorGenerico());
					arrRegistrosError.add(registro);
				}
			}
		}
	}
	
	// 10. Si hay registros v�lidos, se permite continuar la carga
	String btnContinuar  = "N";
	if( registrosValidos.getNumeroRegistros()>0 ){ btnContinuar  ="S";  }

	JSONObject valoresResultadoProceso = new JSONObject();
	valoresResultadoProceso.put("proceso", 						new Integer(icProceso)						);
	valoresResultadoProceso.put("totalRegistrosInvalidos", 	new Integer(resultados.getNumRegErr())	);
	valoresResultadoProceso.put("totalRegistrosValidos", 		new Integer(resultados.getNumRegOk())	);
	//valoresResultadoProceso.put("listaNumerosLineaValidos", listaNumerosLinea.toString());
	valoresResultadoProceso.put("lstNumerosLineaOk", 			listaNumerosLinea.toString()				);
	valoresResultadoProceso.put("btnContinuar", 					btnContinuar									);	

%>
({
	"success": true,
	"registrosValidos": <%=arrRegistrosValidos%>,
	"registrosInvalidos": <%=arrRegistrosError%>,
	"resumenAccion": <%=arrResumenAccion%>,
	"valoresResultadoProceso": <%=valoresResultadoProceso%>
})


