<%--
	FODEA 025-2012: DESCUENTO AUTOM�TICO
	@fecha 27 Dic 2012
	@autor garellano
--%>

<%
	String clavePyme = request.getParameter("ic_pyme")==null?"":request.getParameter("ic_pyme");
%>

<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252"	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<% String version = (String)session.getAttribute("version"); %>

<html>
	<head>
		<title>Nafinet</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		
		<%@ include file="/extjs.jspf" %>
			<%if(version!=null){%>
				<%@ include file="/01principal/menu.jspf"%>
			<%}%>
		<title>Nafi@net - Descuento Autom�tico</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
      
		<script type="text/javascript" src="13dsctoaut02ext.js?<%=session.getId()%>"></script>
      <script language="javascript" src="/nafin/00utils/NEcesionDerechos.js?<%=session.getId()%>"></script>
<%@ include file="/00utils/componente_firma.jspf" %>

	</head>
	
	
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(version!=null){%>
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
<%}%>
<div id="_menuApp"></div>
	<div id="Contcentral">
		<%if(version!=null){%>
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>
		<%}%>
		<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
   </div>
</div>
<%if(version!=null){%>
<%@ include file="/01principal/01nafin/pie.jspf"%>
<%}%>
<form id='formAux' name="formAux" target='_new'></form>
	<input type="hidden" value="<%=clavePyme%>" id="clavePyme" name="clavePyme" />
	
</body>
	
</html>
